﻿Public Class frmTableLayout

    Private Declare Function GetForegroundWindow Lib "user32" () As IntPtr
    Private Declare Function MoveWindow Lib "User32" (hWnd As IntPtr, x As Int32, y As Int32, width As Int32, height As Int32, redraw As Boolean) As Boolean

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        NativeMethods.INI_Write(Me.Tag.ToString, "ForeColor", cboForeColor.BackColor.ToArgb.ToString, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "BackColor", cboBackColor.BackColor.ToArgb.ToString, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "SelForeColor", cboSelForeColor.BackColor.ToArgb.ToString, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "SelBackColor", cboSelBackColor.BackColor.ToArgb.ToString, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "ActForeColor", cboAktuellerFC.BackColor.ToArgb.ToString, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "ActBackColor", cboAktuellerBC.BackColor.ToArgb.ToString, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "NextForeColor", cboNächsterFC.BackColor.ToArgb.ToString, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "NextBackColor", cboNächsterBC.BackColor.ToArgb.ToString, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "PrevForeColor", cboLetzterFC.BackColor.ToArgb.ToString, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "PrevNextColor", cboLetzterBC.BackColor.ToArgb.ToString, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "Font", cboFont.Text, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "FontActual", cboActual.Text, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "FontNext", cboNext.Text, App_IniFile)
        NativeMethods.INI_Write(Me.Tag.ToString, "FontPrev", cboPrev.Text, App_IniFile)

        Write_ColumnLayout_ToINI(grdColums, Me.Tag.ToString)

        Me.Close()
    End Sub

    Private Sub chkAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkAll.CheckedChanged
        For i As Integer = 0 To grdColums.Rows.Count - 1
            grdColums.Rows(i).Cells("colSichtbar").Value = chkAll.Checked
        Next
    End Sub

    Private Sub btnUp_Click(sender As Object, e As EventArgs) Handles btnUp.Click
        With grdColums
            Dim pos As Integer = .SelectedCells(0).RowIndex
            If (pos > 0) Then
                .Rows.InsertCopy(pos, pos - 1)
                For x As Integer = 0 To .Columns.Count - 1
                    .Rows(pos - 1).Cells(x).Value = .Rows(pos + 1).Cells(x).Value
                Next
                .Rows.RemoveAt(pos + 1)
                .Rows(pos - 1).Cells(0).Selected = True
            End If
        End With
    End Sub

    Private Sub btnDown_Click(sender As Object, e As EventArgs) Handles btnDown.Click
        With grdColums
            Dim pos As Integer = .SelectedCells(0).RowIndex
            If (pos < .Rows.Count - 1) Then
                .Rows.InsertCopy(pos, pos + 2)
                For x As Integer = 0 To .Columns.Count - 1
                    .Rows(pos + 2).Cells(x).Value = .Rows(pos).Cells(x).Value
                Next
                .Rows.RemoveAt(pos)
                .Rows(pos + 1).Cells(0).Selected = True
            End If
        End With
    End Sub

    Private Sub Color_Click(sender As Object, e As EventArgs) Handles cboForeColor.Click, cboSelBackColor.Click, cboSelForeColor.Click,
        cboAktuellerBC.Click, cboAktuellerFC.Click, cboAktuellerBC.Click, cboAktuellerFC.Click, cboLetzterBC.Click, cboLetzterFC.Click,
        cboNächsterBC.Click, cboNächsterFC.Click, cboBackColor.Click

        Dim Th As New Threading.Thread(AddressOf MoveDialogColor)
        Th.Start()
        Using CD As New ColorDialog
            If CD.ShowDialog(Me) <> Windows.Forms.DialogResult.Cancel Then
                If cboForeColor.Focused Then cboForeColor.BackColor = CD.Color
                If cboBackColor.Focused Then cboBackColor.BackColor = CD.Color
                If cboSelBackColor.Focused Then cboSelBackColor.BackColor = CD.Color
                If cboSelForeColor.Focused Then cboSelForeColor.BackColor = CD.Color
                If cboAktuellerBC.Focused Then cboAktuellerBC.BackColor = CD.Color
                If cboAktuellerFC.Focused Then cboAktuellerFC.BackColor = CD.Color
                If cboNächsterBC.Focused Then cboNächsterBC.BackColor = CD.Color
                If cboNächsterFC.Focused Then cboNächsterFC.BackColor = CD.Color
                If cboLetzterBC.Focused Then cboLetzterBC.BackColor = CD.Color
                If cboLetzterFC.Focused Then cboLetzterFC.BackColor = CD.Color
            Else
                Th.Abort()
            End If
        End Using
        Th.Abort()
        grdColums.Focus()
    End Sub

    Private Sub MoveDialogColor()
        Do
            Threading.Thread.Sleep(10)
        Loop While Me Is Form.ActiveForm
        MoveWindow(GetForegroundWindow, CInt(Me.Left + Me.Width / 2 - 120), CInt(Me.Top + Me.Height / 2 - 170), 240, 340, True)
    End Sub

    Private Sub cboFont_Click(sender As Object, e As EventArgs) Handles cboFont.Click, cboActual.Click, cboNext.Click, cboPrev.Click

        Dim Th As New Threading.Thread(AddressOf MoveDialogFont)
        Th.Start()
        Using FD As New FontDialog
            If FD.ShowDialog(Me) <> Windows.Forms.DialogResult.Cancel Then
                Dim x As String = FD.Font.Name & ";" & FD.Font.Size & CStr(IIf(FD.Font.Bold, ";Fett", "")) & CStr(IIf(FD.Font.Italic, ";Kursiv", ""))
                If cboFont.Focused Then cboFont.Text = x
                If cboActual.Focused Then cboActual.Text = x
                If cboNext.Focused Then cboNext.Text = x
                If cboPrev.Focused Then cboPrev.Text = x
            Else
                Th.Abort()
            End If
        End Using
        Th.Abort()
        grdColums.Focus()
    End Sub

    Private Sub MoveDialogFont()
        Do
            Threading.Thread.Sleep(10)
        Loop While Me Is Form.ActiveForm
        MoveWindow(GetForegroundWindow, CInt(Me.Left + Me.Width / 2 - 225), CInt(Me.Top + Me.Height / 2 - 163), 450, 326, True)
    End Sub

    Private Sub btnDefault_Click(sender As Object, e As EventArgs) Handles btnDefault.Click
        cboForeColor.BackColor = Color.Black
        cboBackColor.BackColor = Color.White
        cboSelBackColor.BackColor = Color.DodgerBlue
        cboSelForeColor.BackColor = Color.White
        cboAktuellerBC.BackColor = Color.Red
        cboAktuellerFC.BackColor = Color.Gold
        cboNächsterBC.BackColor = Color.DarkCyan
        cboNächsterFC.BackColor = Color.WhiteSmoke
        cboLetzterBC.BackColor = Color.Gray
        cboLetzterFC.BackColor = Color.White
        cboFont.Text = "Microsoft Sans Serif;8,25"
        cboActual.Text = "Microsoft Sans Serif;8,25;Fett"
        cboNext.Text = "Microsoft Sans Serif;8,25"
        cboPrev.Text = "Microsoft Sans Serif;8,25"
    End Sub

    Private Sub frmLayout_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.Tag IsNot "Leader" Then
            Me.Height = Me.Height - 162
            cboAktuellerBC.Visible = False
            cboAktuellerFC.Visible = False
            cboNächsterBC.Visible = False
            cboNächsterFC.Visible = False
            cboLetzterBC.Visible = False
            cboLetzterFC.Visible = False
            lblAkt1.Visible = False
            lblAkt2.Visible = False
            lblNext1.Visible = False
            lblNext2.Visible = False
            lblPrev1.Visible = False
            lblPrev2.Visible = False
            cboActual.Visible = False
            cboNext.Visible = False
            cboPrev.Visible = False
            GroupBox2.Height = GroupBox2.Height - 162
            GroupBox3.Height = GroupBox3.Height - 162
            btnDefault.Top = GroupBox3.Top + 5
            btnSave.Top = btnSave.Top - 162
            btnExit.Top = btnExit.Top - 162
        End If
    End Sub

End Class