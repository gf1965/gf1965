﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLayout
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkAll = New System.Windows.Forms.CheckBox()
        Me.btnDown = New System.Windows.Forms.Button()
        Me.btnUp = New System.Windows.Forms.Button()
        Me.grdColums = New System.Windows.Forms.DataGridView()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cboBackColor = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboLetzterFC = New System.Windows.Forms.ComboBox()
        Me.lblPrev1 = New System.Windows.Forms.Label()
        Me.cboLetzterBC = New System.Windows.Forms.ComboBox()
        Me.lblPrev2 = New System.Windows.Forms.Label()
        Me.cboNächsterFC = New System.Windows.Forms.ComboBox()
        Me.lblNext1 = New System.Windows.Forms.Label()
        Me.cboNächsterBC = New System.Windows.Forms.ComboBox()
        Me.lblNext2 = New System.Windows.Forms.Label()
        Me.cboAktuellerFC = New System.Windows.Forms.ComboBox()
        Me.lblAkt1 = New System.Windows.Forms.Label()
        Me.cboAktuellerBC = New System.Windows.Forms.ComboBox()
        Me.lblAkt2 = New System.Windows.Forms.Label()
        Me.cboSelBackColor = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboSelForeColor = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboForeColor = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cboPrev = New System.Windows.Forms.ComboBox()
        Me.cboNext = New System.Windows.Forms.ComboBox()
        Me.cboActual = New System.Windows.Forms.ComboBox()
        Me.cboFont = New System.Windows.Forms.ComboBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnDefault = New System.Windows.Forms.Button()
        Me.colName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSichtbar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colBreite = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdColums, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkAll)
        Me.GroupBox1.Controls.Add(Me.btnDown)
        Me.GroupBox1.Controls.Add(Me.btnUp)
        Me.GroupBox1.Controls.Add(Me.grdColums)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(590, 232)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Spalten"
        '
        'chkAll
        '
        Me.chkAll.AutoSize = True
        Me.chkAll.Location = New System.Drawing.Point(381, 206)
        Me.chkAll.Name = "chkAll"
        Me.chkAll.Size = New System.Drawing.Size(81, 17)
        Me.chkAll.TabIndex = 3
        Me.chkAll.Text = "alle Spalten"
        Me.chkAll.UseVisualStyleBackColor = True
        '
        'btnDown
        '
        Me.btnDown.Image = Global.Gewichtheben.My.Resources.Resources.arrow_down_24
        Me.btnDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDown.Location = New System.Drawing.Point(549, 109)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(32, 32)
        Me.btnDown.TabIndex = 2
        Me.btnDown.UseVisualStyleBackColor = True
        '
        'btnUp
        '
        Me.btnUp.Image = Global.Gewichtheben.My.Resources.Resources.arrow_up_24
        Me.btnUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUp.Location = New System.Drawing.Point(549, 77)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(32, 32)
        Me.btnUp.TabIndex = 1
        Me.btnUp.UseVisualStyleBackColor = True
        '
        'grdColums
        '
        Me.grdColums.AllowUserToAddRows = False
        Me.grdColums.AllowUserToDeleteRows = False
        Me.grdColums.AllowUserToResizeColumns = False
        Me.grdColums.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdColums.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdColums.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdColums.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colName, Me.colBezeichnung, Me.colSichtbar, Me.colBreite})
        Me.grdColums.Location = New System.Drawing.Point(11, 24)
        Me.grdColums.MultiSelect = False
        Me.grdColums.Name = "grdColums"
        Me.grdColums.RowHeadersWidth = 24
        Me.grdColums.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.grdColums.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.grdColums.ShowCellErrors = False
        Me.grdColums.ShowRowErrors = False
        Me.grdColums.Size = New System.Drawing.Size(530, 175)
        Me.grdColums.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnSave.Location = New System.Drawing.Point(529, 487)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 35)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "Speichern"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboBackColor)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.cboLetzterFC)
        Me.GroupBox2.Controls.Add(Me.lblPrev1)
        Me.GroupBox2.Controls.Add(Me.cboLetzterBC)
        Me.GroupBox2.Controls.Add(Me.lblPrev2)
        Me.GroupBox2.Controls.Add(Me.cboNächsterFC)
        Me.GroupBox2.Controls.Add(Me.lblNext1)
        Me.GroupBox2.Controls.Add(Me.cboNächsterBC)
        Me.GroupBox2.Controls.Add(Me.lblNext2)
        Me.GroupBox2.Controls.Add(Me.cboAktuellerFC)
        Me.GroupBox2.Controls.Add(Me.lblAkt1)
        Me.GroupBox2.Controls.Add(Me.cboAktuellerBC)
        Me.GroupBox2.Controls.Add(Me.lblAkt2)
        Me.GroupBox2.Controls.Add(Me.cboSelBackColor)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.cboSelForeColor)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cboForeColor)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 255)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(228, 308)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Farben"
        '
        'cboBackColor
        '
        Me.cboBackColor.BackColor = System.Drawing.Color.White
        Me.cboBackColor.FormattingEnabled = True
        Me.cboBackColor.Location = New System.Drawing.Point(133, 58)
        Me.cboBackColor.Name = "cboBackColor"
        Me.cboBackColor.Size = New System.Drawing.Size(82, 21)
        Me.cboBackColor.TabIndex = 24
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(10, 61)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 13)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Hintergrund"
        '
        'cboLetzterFC
        '
        Me.cboLetzterFC.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cboLetzterFC.FormattingEnabled = True
        Me.cboLetzterFC.Location = New System.Drawing.Point(133, 247)
        Me.cboLetzterFC.Name = "cboLetzterFC"
        Me.cboLetzterFC.Size = New System.Drawing.Size(82, 21)
        Me.cboLetzterFC.TabIndex = 21
        '
        'lblPrev1
        '
        Me.lblPrev1.AutoSize = True
        Me.lblPrev1.Location = New System.Drawing.Point(10, 277)
        Me.lblPrev1.Name = "lblPrev1"
        Me.lblPrev1.Size = New System.Drawing.Size(106, 13)
        Me.lblPrev1.TabIndex = 22
        Me.lblPrev1.Text = "Vorheriger Heber HG"
        '
        'cboLetzterBC
        '
        Me.cboLetzterBC.BackColor = System.Drawing.Color.Gray
        Me.cboLetzterBC.FormattingEnabled = True
        Me.cboLetzterBC.Location = New System.Drawing.Point(133, 274)
        Me.cboLetzterBC.Name = "cboLetzterBC"
        Me.cboLetzterBC.Size = New System.Drawing.Size(82, 21)
        Me.cboLetzterBC.TabIndex = 19
        '
        'lblPrev2
        '
        Me.lblPrev2.AutoSize = True
        Me.lblPrev2.Location = New System.Drawing.Point(10, 250)
        Me.lblPrev2.Name = "lblPrev2"
        Me.lblPrev2.Size = New System.Drawing.Size(120, 13)
        Me.lblPrev2.TabIndex = 20
        Me.lblPrev2.Text = "Vorheriger Heber Schrift"
        '
        'cboNächsterFC
        '
        Me.cboNächsterFC.BackColor = System.Drawing.Color.White
        Me.cboNächsterFC.FormattingEnabled = True
        Me.cboNächsterFC.Location = New System.Drawing.Point(133, 193)
        Me.cboNächsterFC.Name = "cboNächsterFC"
        Me.cboNächsterFC.Size = New System.Drawing.Size(82, 21)
        Me.cboNächsterFC.TabIndex = 17
        '
        'lblNext1
        '
        Me.lblNext1.AutoSize = True
        Me.lblNext1.Location = New System.Drawing.Point(10, 223)
        Me.lblNext1.Name = "lblNext1"
        Me.lblNext1.Size = New System.Drawing.Size(107, 13)
        Me.lblNext1.TabIndex = 18
        Me.lblNext1.Text = "Nächste 2 Heber HG"
        '
        'cboNächsterBC
        '
        Me.cboNächsterBC.BackColor = System.Drawing.Color.DarkCyan
        Me.cboNächsterBC.FormattingEnabled = True
        Me.cboNächsterBC.Location = New System.Drawing.Point(133, 220)
        Me.cboNächsterBC.Name = "cboNächsterBC"
        Me.cboNächsterBC.Size = New System.Drawing.Size(82, 21)
        Me.cboNächsterBC.TabIndex = 15
        '
        'lblNext2
        '
        Me.lblNext2.AutoSize = True
        Me.lblNext2.Location = New System.Drawing.Point(10, 196)
        Me.lblNext2.Name = "lblNext2"
        Me.lblNext2.Size = New System.Drawing.Size(121, 13)
        Me.lblNext2.TabIndex = 16
        Me.lblNext2.Text = "Nächste 2 Heber Schrift"
        '
        'cboAktuellerFC
        '
        Me.cboAktuellerFC.BackColor = System.Drawing.Color.Red
        Me.cboAktuellerFC.FormattingEnabled = True
        Me.cboAktuellerFC.Location = New System.Drawing.Point(133, 139)
        Me.cboAktuellerFC.Name = "cboAktuellerFC"
        Me.cboAktuellerFC.Size = New System.Drawing.Size(82, 21)
        Me.cboAktuellerFC.TabIndex = 13
        '
        'lblAkt1
        '
        Me.lblAkt1.AutoSize = True
        Me.lblAkt1.Location = New System.Drawing.Point(10, 169)
        Me.lblAkt1.Name = "lblAkt1"
        Me.lblAkt1.Size = New System.Drawing.Size(99, 13)
        Me.lblAkt1.TabIndex = 14
        Me.lblAkt1.Text = "Aktueller Heber HG"
        '
        'cboAktuellerBC
        '
        Me.cboAktuellerBC.BackColor = System.Drawing.Color.Gold
        Me.cboAktuellerBC.FormattingEnabled = True
        Me.cboAktuellerBC.Location = New System.Drawing.Point(133, 166)
        Me.cboAktuellerBC.Name = "cboAktuellerBC"
        Me.cboAktuellerBC.Size = New System.Drawing.Size(82, 21)
        Me.cboAktuellerBC.TabIndex = 11
        '
        'lblAkt2
        '
        Me.lblAkt2.AutoSize = True
        Me.lblAkt2.Location = New System.Drawing.Point(10, 142)
        Me.lblAkt2.Name = "lblAkt2"
        Me.lblAkt2.Size = New System.Drawing.Size(113, 13)
        Me.lblAkt2.TabIndex = 12
        Me.lblAkt2.Text = "Aktueller Heber Schrift"
        '
        'cboSelBackColor
        '
        Me.cboSelBackColor.BackColor = System.Drawing.Color.DodgerBlue
        Me.cboSelBackColor.FormattingEnabled = True
        Me.cboSelBackColor.Location = New System.Drawing.Point(133, 112)
        Me.cboSelBackColor.Name = "cboSelBackColor"
        Me.cboSelBackColor.Size = New System.Drawing.Size(82, 21)
        Me.cboSelBackColor.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Auswahl Schrift"
        '
        'cboSelForeColor
        '
        Me.cboSelForeColor.BackColor = System.Drawing.Color.White
        Me.cboSelForeColor.FormattingEnabled = True
        Me.cboSelForeColor.Location = New System.Drawing.Point(133, 85)
        Me.cboSelForeColor.Name = "cboSelForeColor"
        Me.cboSelForeColor.Size = New System.Drawing.Size(82, 21)
        Me.cboSelForeColor.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 115)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(105, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Auswahl Hintergrund"
        '
        'cboForeColor
        '
        Me.cboForeColor.BackColor = System.Drawing.Color.Black
        Me.cboForeColor.FormattingEnabled = True
        Me.cboForeColor.Location = New System.Drawing.Point(133, 31)
        Me.cboForeColor.Name = "cboForeColor"
        Me.cboForeColor.Size = New System.Drawing.Size(82, 21)
        Me.cboForeColor.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Schrift"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cboPrev)
        Me.GroupBox3.Controls.Add(Me.cboNext)
        Me.GroupBox3.Controls.Add(Me.cboActual)
        Me.GroupBox3.Controls.Add(Me.cboFont)
        Me.GroupBox3.Location = New System.Drawing.Point(252, 255)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(263, 308)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Schrift"
        '
        'cboPrev
        '
        Me.cboPrev.FormattingEnabled = True
        Me.cboPrev.Location = New System.Drawing.Point(16, 247)
        Me.cboPrev.Name = "cboPrev"
        Me.cboPrev.Size = New System.Drawing.Size(229, 21)
        Me.cboPrev.TabIndex = 4
        '
        'cboNext
        '
        Me.cboNext.FormattingEnabled = True
        Me.cboNext.Location = New System.Drawing.Point(16, 193)
        Me.cboNext.Name = "cboNext"
        Me.cboNext.Size = New System.Drawing.Size(229, 21)
        Me.cboNext.TabIndex = 3
        '
        'cboActual
        '
        Me.cboActual.FormattingEnabled = True
        Me.cboActual.Location = New System.Drawing.Point(16, 139)
        Me.cboActual.Name = "cboActual"
        Me.cboActual.Size = New System.Drawing.Size(229, 21)
        Me.cboActual.TabIndex = 2
        '
        'cboFont
        '
        Me.cboFont.FormattingEnabled = True
        Me.cboFont.Location = New System.Drawing.Point(16, 31)
        Me.cboFont.Name = "cboFont"
        Me.cboFont.Size = New System.Drawing.Size(229, 21)
        Me.cboFont.TabIndex = 0
        '
        'btnExit
        '
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(529, 528)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 35)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "Abbrechen"
        Me.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnDefault
        '
        Me.btnDefault.Location = New System.Drawing.Point(529, 394)
        Me.btnDefault.Name = "btnDefault"
        Me.btnDefault.Size = New System.Drawing.Size(75, 35)
        Me.btnDefault.TabIndex = 7
        Me.btnDefault.Text = "Standard"
        Me.btnDefault.UseVisualStyleBackColor = True
        '
        'colName
        '
        Me.colName.HeaderText = "Name"
        Me.colName.Name = "colName"
        Me.colName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.colName.Width = 158
        '
        'colBezeichnung
        '
        Me.colBezeichnung.HeaderText = "Bezeichnung"
        Me.colBezeichnung.Name = "colBezeichnung"
        Me.colBezeichnung.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.colBezeichnung.Width = 158
        '
        'colSichtbar
        '
        Me.colSichtbar.HeaderText = "Sichtbar"
        Me.colSichtbar.Name = "colSichtbar"
        Me.colSichtbar.Width = 70
        '
        'colBreite
        '
        Me.colBreite.HeaderText = "Breite"
        Me.colBreite.Name = "colBreite"
        Me.colBreite.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colBreite.Width = 101
        '
        'frmLayout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(616, 577)
        Me.Controls.Add(Me.btnDefault)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLayout"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tabellen-Layout"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.grdColums, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents grdColums As System.Windows.Forms.DataGridView
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnDown As System.Windows.Forms.Button
    Friend WithEvents btnUp As System.Windows.Forms.Button
    Friend WithEvents chkAll As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cboSelForeColor As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboForeColor As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboAktuellerFC As System.Windows.Forms.ComboBox
    Friend WithEvents lblAkt1 As System.Windows.Forms.Label
    Friend WithEvents cboAktuellerBC As System.Windows.Forms.ComboBox
    Friend WithEvents lblAkt2 As System.Windows.Forms.Label
    Friend WithEvents cboSelBackColor As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cboFont As System.Windows.Forms.ComboBox
    Friend WithEvents cboLetzterFC As System.Windows.Forms.ComboBox
    Friend WithEvents lblPrev1 As System.Windows.Forms.Label
    Friend WithEvents cboLetzterBC As System.Windows.Forms.ComboBox
    Friend WithEvents lblPrev2 As System.Windows.Forms.Label
    Friend WithEvents cboNächsterFC As System.Windows.Forms.ComboBox
    Friend WithEvents lblNext1 As System.Windows.Forms.Label
    Friend WithEvents cboNächsterBC As System.Windows.Forms.ComboBox
    Friend WithEvents lblNext2 As System.Windows.Forms.Label
    Friend WithEvents btnDefault As System.Windows.Forms.Button
    Friend WithEvents cboBackColor As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboPrev As System.Windows.Forms.ComboBox
    Friend WithEvents cboNext As System.Windows.Forms.ComboBox
    Friend WithEvents cboActual As System.Windows.Forms.ComboBox
    Friend WithEvents colName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBezeichnung As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSichtbar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colBreite As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
