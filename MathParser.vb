﻿
Module MathParser
    Public Class Arguments
        Implements IDisposable
        Public HL As Integer
        Public KG As Double
        Public TN As Double
        Public Anzahl As Double
        Public Punkte As Double
        Public Differenz As Double
        Public Zeit As Double
        Public Weite As Double
        Public Value As Double
        Public Sub New()
        End Sub
        Public Sub Dispose() Implements IDisposable.Dispose
        End Sub
    End Class
    Class Parser
        Implements IDisposable
        Dim par As Arguments
        Public Sub New()
        End Sub
        Public Sub Dispose() Implements IDisposable.Dispose
        End Sub
        Public Function Parse_Formel(Formel As String, Parameter As Arguments) As Double?
            par = Parameter
            Try
                ' Format
                Do While Formel.Contains("  ")
                    Formel = Formel.Replace("  ", " ")
                Loop
                Formel = Formel.Replace(".", ",")
                Formel = Formel.Replace("(", " ( ")
                Formel = Formel.Replace(")", " ) ")
                Formel = Formel.Replace("*", " * ")
                Formel = Formel.Replace("/", " / ")
                Formel = Formel.Replace("+", " + ")
                Formel = Formel.Replace("-", " - ")
                Do While Formel.Contains("  ")
                    Formel = Formel.Replace("  ", " ")
                Loop
                Formel = Trim(Formel)

                Dim FormelItems As New List(Of String)
                FormelItems = Split(Formel, " ").ToList

                Dim parL As Integer
                Dim parR As Integer
                Do
                    parL = FormelItems.LastIndexOf("(")
                    If parL = -1 Then Exit Do ' keine weiteren linken Klammern
                    parR = FormelItems.IndexOf(")", parL)
                    Dim SubItems As New List(Of String)
                    For i = parL + 1 To parR - 1
                        SubItems.Add(FormelItems(i))
                    Next
                    For i = parL To parR - 1
                        FormelItems.RemoveAt(parL)
                    Next
                    FormelItems(parL) = Get_Result(SubItems).ToString
                Loop
                Return Get_Result(FormelItems)
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Private Function Get_Result(items As List(Of String)) As Double?
            Dim lpart As String
            Dim rpart As String
            Dim ix As Integer
            Dim lnum As Double?
            Dim rnum As Double?
            Dim op As String() = {"*", "/", "+", "-"}
            Try
                For o = 0 To op.Count - 1
                    Do
                        If Not items.Contains(op(o)) Then Exit Do
                        ix = items.IndexOf(op(o))
                        lpart = items(ix - 1)
                        rpart = items(ix + 1)
                        lnum = Get_Numbers(lpart)
                        rnum = Get_Numbers(rpart)
                        Dim result As Double? = Get_PartResult(op(o), CDbl(lnum), CDbl(rnum))
                        items(ix) = result.ToString
                        ' berechnete Werte entfernen
                        items.RemoveAt(ix - 1)
                        items.RemoveAt(ix)
                    Loop While items.Count > 1
                Next
                Return CDbl(items(0))
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Private Function Get_Numbers(Value As String) As Double?
            Try
                If IsNumeric(Value) Then
                    Return CDbl(Value)
                Else
                    Return Get_Value(Value)
                End If
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Private Function Get_Value(Key As String) As Double?
            If Key = "HL" Then Return par.HL        ' Hantellast
            If Key = "KG" Then Return par.KG        ' Wiegegewicht
            If Key = "TN" Then Return par.TN        ' Technik-Note
            If Key = "Anzahl" Then Return par.Value
            If Key = "Punkte" Then Return par.Punkte
            If Key = "Differenz" Then Return par.Value
            If Key = "Zeit" Then Return par.Value
            If Key = "Weite" Then Return par.Value
            Return Nothing
        End Function
        Private Function Get_PartResult(Op As String, Val_L As Double, Val_R As Double) As Double?
            Try
                If Op = "*" Then Return Val_L * Val_R
                If Op = "/" Then Return Val_L / Val_R
                If Op = "+" Then Return Val_L + Val_R
                If Op = "-" Then Return Val_L - Val_R
                Return Nothing
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

    End Class
End Module
