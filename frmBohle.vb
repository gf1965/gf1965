﻿
Imports System.Timers
'Imports System.Drawing.Text
'Imports AxWMPLib

Public Class frmBohle

    Private dicLampen As New Dictionary(Of Integer, TextBox) 'Container für Lampen

    Private Delegate Sub DelegateSub()
    Private Delegate Sub DelegateSub0(status As Boolean)
    Private Delegate Sub DelegateSub1(Value As String)
    Private Delegate Sub DelegateSub2(Info As String, Target As Integer)
    Private Delegate Sub DelegateSub3(Value As Integer)
    Private Delegate Sub DelegateSub4(status As Integer, value As Double)
    Private Delegate Sub DelegateSub5(Staat As String, Flagge As Byte())
    Private Delegate Sub DelegateSub6(Hantellast As String, Anzeigen As Boolean)
    Private Delegate Sub DelegateSub7(Nachname As String, Vorname As String)
    Private Delegate Sub DelegateSub8(status As Integer, value As String)
    Private Delegate Sub DelegateSub9(status As Integer, value As Color)
    Private Delegate Sub DelegateColor(Value As Color)

    Private Sub Change_Heber(Optional visible As Boolean = True) 'sender As Object, e As EventArgs)
        If InvokeRequired Then
            Dim d As New DelegateSub0(AddressOf Change_Heber)
            Invoke(d, New Object() {visible})
        Else
            'If _pause Then
            '    ' wenn Leader.btnNext geklickt wird ohne Pause mit CLEAR zu beenden
            '    ZN_Input(vbCr)
            '    Set_ZA(Heber.CountDown)
            'End If
            Try
                Dim v As Boolean = Heber.Id > 0
                If Not visible Then v = False
                xTeilnehmer.Visible = v
                xVerein.Visible = v
                xAK.Visible = v
                xGewichtsklasse.Visible = v
                xDurchgang.Visible = v
                xHantelgewicht.Visible = v
                xVersuch.Visible = v
                xStartnummer.Visible = v
                picFlagge.Visible = False
                pnlHantel.Visible = False
                'KR_BlaueTaste()
                lblMsg.Visible = False
                Set_ForeColors(False)
            Catch
            End Try
        End If
    End Sub

    Sub Set_AK(Optional ByVal Value As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_AK)
                Invoke(d, New Object() {Value})
            Else
                xAK.Text = Value & " " & Heber.Sex
                xAK.SendToBack()
            End If
        Catch
        End Try
    End Sub
    Sub Set_Durchgang(value As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_Durchgang)
                Invoke(d, New Object() {value})
            Else
                xDurchgang.Text = value
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub Set_Flagge(Optional ByVal Staat As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_Flagge)
                Invoke(d, New Object() {Staat})
            Else
                With picFlagge
                    .Visible = False
                    xVerein.Left = xTeilnehmer.Left
                    If Wettkampf.Flagge = True AndAlso Staat > "" Then
                        Dim File As String = Path.Combine(Application.StartupPath, "flag", Staat)
                        Try
                            .ImageLocation = File
                            xVerein.Left = .Left + .Width + 20
                            .Visible = True
                        Catch ex As Exception
                        End Try
                    End If
                    ' nachdem Flagge (nicht) angezeigt wird, Set_Verein mit aktuellem Text aufrufen um Width mit neuer Location zu prüfen
                    If xVerein.Text.Length > 0 Then Set_Verein(xVerein.Text)
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub Set_ForeColors(Optional Heber_Present As Boolean = True)
        If InvokeRequired Then
            Dim d As New DelegateSub0(AddressOf Set_ForeColors)
            Invoke(d, New Object() {Heber_Present})
        Else
            Try
                If Heber_Present Then
                    xTeilnehmer.ForeColor = Ansicht_Options.Name_Color
                    xVerein.ForeColor = Ansicht_Options.Verein_Color
                    xAK.ForeColor = Ansicht_Options.AK_Color
                    xGewichtsklasse.ForeColor = Ansicht_Options.AK_Color
                    xDurchgang.ForeColor = Ansicht_Options.Durchgang_Color
                    xHantelgewicht.ForeColor = Ansicht_Options.Durchgang_Color
                    xDurchgang.ForeColor = Ansicht_Options.Durchgang_Color
                    xVersuch.ForeColor = Ansicht_Options.Durchgang_Color
                    xStartnummer.ForeColor = Ansicht_Options.Name_Color
                Else ' Wertungen zurücksetzen
                    For i = 1 To 3
                        dicLampen(i).ForeColor = Ansicht_Options.Shade_Color
                    Next
                    With xTechniknote
                        .ForeColor = Ansicht_Options.Shade_Color
                        .Text = "X,XX"
                        .BringToFront()
                    End With
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Sub Change_ForeColors()
        If InvokeRequired Then
            Dim d As New DelegateSub(AddressOf Change_ForeColors)
            Invoke(d, New Object() {})
        Else
            Try
                xTeilnehmer.ForeColor = Ansicht_Options.Name_Color
                xVerein.ForeColor = Ansicht_Options.Verein_Color
                xAK.ForeColor = Ansicht_Options.AK_Color
                xGewichtsklasse.ForeColor = Ansicht_Options.AK_Color
                xDurchgang.ForeColor = Ansicht_Options.Durchgang_Color
                xHantelgewicht.ForeColor = Ansicht_Options.Durchgang_Color
                xDurchgang.ForeColor = Ansicht_Options.Durchgang_Color
                xVersuch.ForeColor = Ansicht_Options.Durchgang_Color
                xStartnummer.ForeColor = Ansicht_Options.Name_Color
                Change_ShadeColors()
            Catch ex As Exception
            End Try
        End If
    End Sub
    Sub Change_ShadeColors()
        If InvokeRequired Then
            Dim d As New DelegateSub(AddressOf Change_ShadeColors)
            Invoke(d, New Object() {})
        Else
            For i = 1 To 3
                If dicLampen(i).ForeColor <> Anzeige.ScoreColors(5) AndAlso dicLampen(i).ForeColor <> Anzeige.ScoreColors(5) Then dicLampen(i).ForeColor = Anzeige.ScoreColors(0)
            Next
            If xTechniknote.ForeColor <> Anzeige.TechnikColors(1) Then xTechniknote.ForeColor = Anzeige.TechnikColors(0)
        End If
    End Sub
    Sub Set_GK(Optional ByVal value As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_GK)
                Invoke(d, New Object() {value})
            Else
                xGewichtsklasse.Text = value
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub Set_Hantelgewicht()
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub(AddressOf Set_Hantelgewicht)
                Invoke(d, New Object() {})
            Else
                With Heber
                    xTechniknote.Visible = False
                    Hantel.DrawHantel(pnlHantel, .Hantellast,, .Sex, .JG, Wettkampf.BigDisc)
                    pnlHantel.Visible = pnlHantel.Controls.Count > 0
                    pnlHantel.BringToFront()
                End With
                With xHantelgewicht ' angezeigtes Hantelgewicht aktualisieren
                    .Text = Heber.Hantellast.ToString + " kg"
                    .Left = xDurchgang.Left + xDurchgang.Width - .Width
                    .BringToFront()
                End With
            End If
        Catch
        End Try
    End Sub
    Sub Set_Teilnehmer(Optional ByVal Nachname As String = "", Optional ByVal Vorname As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub7(AddressOf Set_Teilnehmer)
                Invoke(d, New Object() {Nachname, Vorname})
            Else
                Dim value As String = String.Empty
                If Vorname.Equals("-1") Then
                    value = Nachname
                    xTeilnehmer.BringToFront()
                    xStartnummer.BringToFront()
                ElseIf Not String.IsNullOrEmpty(Nachname) AndAlso Not String.IsNullOrEmpty(Vorname) Then
                    If Wettkampf.International Then
                        value = UCase(Nachname) + " " + Vorname
                    Else
                        Select Case Ansicht_Options.NameFormat
                            Case NameFormat.Standard                ' 0 'Nachname, Vorname (Standard-Formatierung)
                                value = Nachname + ", " + Vorname
                            Case NameFormat.Standard_UCase          ' 1 'NACHNAME, Vorname
                                value = UCase(Nachname) + ", " + Vorname
                            Case NameFormat.International           ' 2 'NACHNAME Vorname
                                value = UCase(Nachname) + " " + Vorname
                        End Select
                    End If
                End If
                xTeilnehmer.Text = value
                xTeilnehmer.SendToBack()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub NameFormat_Changed()
        Set_Teilnehmer(Heber.Nachname, Heber.Vorname)
    End Sub
    Sub Set_Startnummer(Optional ByVal value As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_Startnummer)
                Invoke(d, New Object() {value})
            Else
                With xStartnummer
                    .Text = value
                    .Left = xDurchgang.Left + xDurchgang.Width - .Width ' Width - xStartnummer.Width - CInt(10 * Width / MinimumSize.Width)
                    .BringToFront()
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub Set_Verein(Optional value As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_Verein)
                Invoke(d, New Object() {value})
            Else
                With xVerein
                    .Text = value
                    Dim LimitLeft = .Left
                    Dim LimitRight = xHantelgewicht.Width - (xHantelgewicht.Left + xHantelgewicht.Width - Width)
                    If .Width > Width - LimitLeft - LimitRight Then ' Bezeichnung zu lang
                        Using GF As New myFunction
                            .Text = GF.String_Shorten(.Text, .Font, Width - LimitLeft - LimitRight)
                        End Using
                    End If
                    .SendToBack()
                End With
            End If
        Catch
        End Try
    End Sub
    Sub Set_Versuch(Optional value As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_Versuch)
                Invoke(d, New Object() {value})
            Else
                xVersuch.Text = value '+ "."
            End If
        Catch ex As Exception
            'Stop
        End Try
    End Sub

    '' Me
    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        RemoveHandler WA_Message.Status_Changed, AddressOf WA_MessageChanged
        RemoveHandler Heber.Id_Changed, AddressOf Change_Heber
        RemoveHandler Heber.Name_Changed, AddressOf Set_Teilnehmer
        RemoveHandler Heber.Startnummer_Changed, AddressOf Set_Startnummer
        RemoveHandler Heber.Verein_Changed, AddressOf Set_Verein
        RemoveHandler Heber.Land_Changed, AddressOf Set_Verein
        RemoveHandler Heber.Flagge_Changed, AddressOf Set_Flagge
        RemoveHandler Heber.AK_Changed, AddressOf Set_AK
        RemoveHandler Heber.GK_Changed, AddressOf Set_GK
        RemoveHandler Heber.Versuch_Changed, AddressOf Set_Versuch
        RemoveHandler Heber.Hantellast_Changed, AddressOf Set_Hantelgewicht
        RemoveHandler Leader.Durchgang_Changed, AddressOf Set_Durchgang
        'RemoveHandler Leader.Edit_Hantellast, AddressOf Edit_Hantelgewicht
        'RemoveHandler Leader.Korrektur, AddressOf Set_Korrektur

        RemoveHandler Anzeige.CountDown_Changed, AddressOf CountDown_Changed

        RemoveHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed
        RemoveHandler Anzeige.Lampe_Changed, AddressOf Change_Lampe
        'RemoveHandler Leader.Wertung_Changed, AddressOf Change_Lampe

        RemoveHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        RemoveHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed
        RemoveHandler Anzeige.Technik_Visible_Changed, AddressOf TechnikVisible_Changed

        RemoveHandler Wettkampf.WKModus_Changed, AddressOf Change_WKModus
        RemoveHandler Progress_Change, AddressOf Set_ProgressBar
        'RemoveHandler Msg_Change, AddressOf Set_Message

        'RemoveHandler JuryMsg_Change, AddressOf Call_TimerMsg
        'RemoveHandler Leader.Jury_Entscheidung, AddressOf InfoMessage_Show
        RemoveHandler Leader.InfoMessage_Changed, AddressOf InfoMessage

        RemoveHandler ForeColors_Change, AddressOf Set_ForeColors
        RemoveHandler Leader.Pause_Changed, AddressOf Change_AdsVisibility
        RemoveHandler Leader.AdsVisible_Changed, AddressOf Change_AdsVisibility

        'RemoveHandler Clear_Hantel, AddressOf Hide_Hantel
        'RemoveHandler Refresh_Bohle, AddressOf Bohle_Refresh

        RemoveHandler Ansicht_Options.Ansicht_Changed, AddressOf Change_ForeColors
        RemoveHandler Ansicht_Options.ShadeChanged, AddressOf Change_ShadeColors

        'RemoveHandler Hantel.Draw_Bar, AddressOf Draw_Hantel

        RemoveHandler Ansicht_Options.NameFormat_Changed, AddressOf NameFormat_Changed

        'PrivateFonts.Dispose()

        Using sc As New ScreenScale
            Dim mon = sc.GetDeviceNumber(Screen.FromControl(Me))
            If dicScreens.ContainsKey(mon) Then dicScreens.Remove(mon)
        End Using

    End Sub
    Private Sub frmBohle_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        'KR_IsInitialized = False
        Cursor = Cursors.Default
        formMain.Cursor = Cursors.Default
    End Sub
    Private Sub frmBohle_Load(sender As Object, e As EventArgs) Handles Me.Load

        Cursor = Cursors.WaitCursor

        pnlWertung.Visible = False
        xTechniknote.Visible = False

        'Wertung
        Try
            dicLampen.Add(1, xGültig1)
            dicLampen.Add(2, xGültig2)
            dicLampen.Add(3, xGültig3)
        Catch ex As Exception
        End Try

        Try
            For i As Integer = 1 To 3
                dicLampen(i).Font = New Font(FontFamilies(Fonts.Wertungsanzeige), dicLampen(i).Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            Next
            xTechniknote.Font = New Font(FontFamilies(Fonts.Techniknote), xTechniknote.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf.Font = New Font(FontFamilies(Fonts.Zeitanzeige), xAufruf.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf.ForeColor = Anzeige.Clock_Colors(Clock_Status.Waiting)
        Catch ex As Exception
        End Try

        If Wettkampf.WK_Modus > Modus.Normal Then Change_WKModus() ' falls EasyMode vor dem Öffnen der Bohle geöffnet wurde

        AddHandler WA_Message.Status_Changed, AddressOf WA_MessageChanged
        AddHandler Heber.Id_Changed, AddressOf Change_Heber
        AddHandler Heber.Name_Changed, AddressOf Set_Teilnehmer
        AddHandler Heber.Startnummer_Changed, AddressOf Set_Startnummer
        AddHandler Heber.Verein_Changed, AddressOf Set_Verein
        AddHandler Heber.Land_Changed, AddressOf Set_Verein
        AddHandler Heber.Flagge_Changed, AddressOf Set_Flagge
        AddHandler Heber.AK_Changed, AddressOf Set_AK
        AddHandler Heber.GK_Changed, AddressOf Set_GK
        AddHandler Heber.Versuch_Changed, AddressOf Set_Versuch
        AddHandler Heber.Hantellast_Changed, AddressOf Set_Hantelgewicht
        AddHandler Leader.Durchgang_Changed, AddressOf Set_Durchgang
        'AddHandler Leader.Edit_Hantellast, AddressOf Edit_Hantelgewicht
        'AddHandler Leader.Korrektur, AddressOf Set_Korrektur

        AddHandler Anzeige.CountDown_Changed, AddressOf CountDown_Changed
        AddHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed

        AddHandler Anzeige.Lampe_Changed, AddressOf Change_Lampe
        'AddHandler Leader.Wertung_Changed, AddressOf Change_Lampe

        AddHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        AddHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed
        AddHandler Anzeige.Technik_Visible_Changed, AddressOf TechnikVisible_Changed

        AddHandler Wettkampf.WKModus_Changed, AddressOf Change_WKModus
        AddHandler Progress_Change, AddressOf Set_ProgressBar
        'AddHandler Msg_Change, AddressOf Set_Message

        'AddHandler JuryMsg_Change, AddressOf Call_TimerMsg
        'AddHandler Leader.Jury_Entscheidung, AddressOf InfoMessage_Show
        AddHandler Leader.InfoMessage_Changed, AddressOf InfoMessage

        AddHandler ForeColors_Change, AddressOf Set_ForeColors

        AddHandler Leader.Pause_Changed, AddressOf Change_AdsVisibility
        AddHandler Leader.AdsVisible_Changed, AddressOf Change_AdsVisibility

        'AddHandler Clear_Hantel, AddressOf Hide_Hantel
        'AddHandler Refresh_Bohle, AddressOf Bohle_Refresh

        AddHandler Ansicht_Options.Ansicht_Changed, AddressOf Change_ForeColors
        AddHandler Ansicht_Options.ShadeChanged, AddressOf Change_ShadeColors

        'AddHandler Hantel.Draw_Bar, AddressOf Draw_Hantel

        AddHandler Ansicht_Options.NameFormat_Changed, AddressOf NameFormat_Changed

    End Sub
    Private Sub frmBohle_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        pnlHantel.Visible = False
        Set_ForeColors()

        pnlWertung.Visible = True
        If Wettkampf.WK_Modus <> Modus.Normal Then Heber.Id = 0
        If Wettkampf.Technikwertung Then
            With xTechniknote
                .ForeColor = Ansicht_Options.Shade_Color
                .Text = "X,XX"
                .Visible = True
                .BringToFront()
            End With
        End If

        With Heber ' für ReLoad
            If Heber.Id > 0 Then
                .Id = .Id
                .Set_Name(.Nachname, .Vorname)
                .Startnummer = .Startnummer
                .Verein = .Verein
                .Flagge = .Flagge
                .Sex = .Sex
                .AK = .AK
                .GK = .GK
                .Versuch = .Versuch
                .Hantellast = .Hantellast
                If .Wertung <> 0 AndAlso Not String.IsNullOrEmpty(.Lampen) Then
                    Try
                        For i = 0 To 2
                            Change_Lampe(i + 1, Anzeige.ScoreColors(CInt(.Lampen.Substring(i, 1))))
                        Next
                    Catch ex As Exception
                    End Try
                End If
                If Wettkampf.Technikwertung AndAlso .Wertung = 1 Then
                    TechnikFarbe_Changed(Anzeige.TechnikColors(1))
                    TechnikVisible_Changed(True)
                    TechnikWert_Changed(Format(.T_Note, "Fixed"))
                End If
            End If
        End With

        Set_Durchgang(Leader.Table)

        Dummy.Focus()
        Cursor = Cursors.Default
        formMain.Cursor = Cursors.Default
    End Sub

    Private Sub Change_WKModus()
        If Wettkampf.WK_Modus > Modus.Normal Then
            ' Anzeige für EasyModus ändern
            'pnlUhr.SetBounds(453, 275, 298, 169)
            'pnlWertung.SetBounds(45, 25, 361, 241)
            'pnlHantel.SetBounds(419, 21, 367, 229)
            'xAufruf_sek.SetBounds(161, 5, 145, 145)
            'xAufruf_sek.Font = New Font(fontFamilies(0), 110)
            'xAufruf_min.SetBounds(0, 5, 137, 145)
            'xAufruf_min.Font = New Font(fontFamilies(0), 110)
            'xAufruf_pkt.SetBounds(125, 17, 31, 106)
            'xAufruf_pkt.Font = New Font(fontFamilies(0), 80)
            'xTechniknote.SetBounds(22, 250, 372, 200)
            'xTechniknote.Font = New Font(fontFamilies(1), 150)
            'For i As Integer = 1 To 3
            '    With dicLampen(i)
            '        .Font = New Font(fontFamilies(2), 100)
            '        .SetBounds((i - 1) * 110, -18, 103, 148)
            '    End With
            'Next
            'xAufruf_p_s.Visible = False
            'xAufruf_std.Visible = False
            '' Zoom-Faktor
            'Dim X = Width / MinimumSize.Width
            'Dim Y = Height / MinimumSize.Height
            '' Anzeige für EasyModus ändern
            'pnlUhr.SetBounds(CInt(453 * X), CInt(275 * Y), CInt(298 * X), CInt(169 * Y))
            'pnlWertung.SetBounds(CInt(45 * X), CInt(25 * Y), CInt(361 * X), CInt(241 * Y))
            'pnlHantel.SetBounds(CInt(419 * X), CInt(21 * Y), CInt(367 * X), CInt(229 * Y))
            'xAufruf_sek.SetBounds(CInt(161 * X), CInt(5 * Y), CInt(145 * X), CInt(145 * Y))
            'xAufruf_sek.Font = New Font(fontFamilies(0), 110)
            'xAufruf_min.SetBounds(CInt(0 * X), CInt(5 * Y), CInt(137 * X), CInt(145 * Y))
            'xAufruf_min.Font = New Font(fontFamilies(0), 110)
            'xAufruf_pkt.SetBounds(CInt(125 * X), CInt(17 * Y), CInt(31 * X), CInt(106 * Y))
            'xAufruf_pkt.Font = New Font(fontFamilies(0), 80)
            'xTechniknote.SetBounds(CInt(22 * X), CInt(250 * Y), CInt(372 * X), CInt(200 * Y))
            'xTechniknote.Font = New Font(fontFamilies(1), 150)
            'For i As Integer = 1 To 3
            '    With xValid(i)
            '        .Font = New Font(fontFamilies(2), 100)
            '        .SetBounds(CInt((i - 1) * 110 * X), CInt(-18 * Y), CInt(103 * X), CInt(148 * Y))
            '    End With
            '    With xInvalid(i)
            '        .Font = New Font(fontFamilies(2), 100)
            '        .SetBounds(CInt((i - 1) * 110 * X), CInt(103 * Y), CInt(103 * X), CInt(148 * Y))
            '    End With
            'Next

            'ElseIf Wettkampf.WK_Modus = Modus.Normal Then
            ' normaler Modus
        End If
    End Sub
    Private Sub Set_ProgressBar(Value As Integer, Visible As Boolean)
        'pgbComInit.Visible = Visible
        'pgbComInit.Value = Value
    End Sub
    Private Sub WA_MessageChanged(Status As Integer, Message As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub8(AddressOf WA_MessageChanged)
                Invoke(d, New Object() {Status, Message})
            Else
                With lblMsg
                    If Status > 0 AndAlso Not xTeilnehmer.Visible Then
                        .Text = Message
                        If (WK_Options.ShowMessages OrElse IsNothing(formLeader) OrElse formLeader.IsDisposed) AndAlso Not String.IsNullOrEmpty(.Text) Then .Visible = True
                    Else
                        .Visible = False
                    End If
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub
    'Private Sub Set_Message(Msg As String)
    '    Try
    '        If InvokeRequired Then
    '            Dim d As New DelegateSub1(AddressOf Set_Message)
    '            Invoke(d, New Object() {Msg})
    '        Else
    '            lblMsg.Text = Msg
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub
    Private Sub lblMsg_TextChanged(sender As Object, e As EventArgs) Handles lblMsg.TextChanged
        lblMsg.Visible = Not String.IsNullOrEmpty(lblMsg.Text)
    End Sub

    Dim _count As Integer
    Private WithEvents timerInfo As New Timer With {.Interval = 375, .AutoReset = True, .SynchronizingObject = Me}
    Private Delegate Sub Clear_Info()
    Private Sub InfoMessage(Info As String, Target As Integer)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub2(AddressOf InfoMessage)
                Invoke(d, New Object() {Info, Target})
            Else
                'If Target = InfoTarget.Moderator Then Return
                With lblInfo
                    .Text = Info
                    .Left = 0
                    .Width = Width
                    .Visible = Not String.IsNullOrEmpty(Info)
                    .BringToFront()
                End With
                _count = 20
                If lblInfo.Visible Then
                    'picFlagge.Visible = False
                    timerInfo.Start()
                Else
                    _count = 0
                    timerInfo.Stop()
                    lblInfo.Invoke(New Clear_Info(AddressOf Info_Clear))
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub TimerMsg_Tick(sender As Object, e As ElapsedEventArgs) Handles timerInfo.Elapsed
        If _count = 0 Then
            timerInfo.Stop()
            lblInfo.Invoke(New Clear_Info(AddressOf Info_Clear))
        Else
            lblInfo.Visible = _count Mod 3 <> 0
            _count -= 1
        End If
    End Sub
    Private Sub Info_Clear()
        lblInfo.Visible = False
        'picFlagge.Visible = Wettkampf.Flagge AndAlso Not String.IsNullOrEmpty(Heber.Flagge)
    End Sub

    '' Events der Anzeige
    Private Sub CountDown_Changed(Value As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf CountDown_Changed)
                Invoke(d, New Object() {Value})
            Else
                xAufruf.Text = Value
            End If
        Catch
        End Try
    End Sub
    Private Sub ClockColor_Changed(Color As Color)
        Try
            If InvokeRequired Then
                Dim d As New DelegateColor(AddressOf ClockColor_Changed)
                Invoke(d, New Object() {Color})
            Else
                xAufruf.ForeColor = Color
                Refresh()
            End If
        Catch
        End Try
    End Sub
    Private Sub Change_Lampe(Index As Integer, color As Color)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub9(AddressOf Change_Lampe)
                Invoke(d, New Object() {Index, color})
            Else
                dicLampen(Index).ForeColor = color
                Refresh()
            End If
        Catch
        End Try
    End Sub
    Private Sub TechnikWert_Changed(Wert As String)
        If InvokeRequired Then
            Dim d As New DelegateSub1(AddressOf TechnikWert_Changed)
            Invoke(d, New Object() {Wert})
        Else
            xTechniknote.Text = Wert
        End If
    End Sub
    Private Sub TechnikFarbe_Changed(Farbe As Color)
        If InvokeRequired Then
            Dim d As New DelegateColor(AddressOf TechnikFarbe_Changed)
            Invoke(d, New Object() {Farbe})
        Else
            xTechniknote.ForeColor = Farbe
        End If
    End Sub
    Private Sub TechnikVisible_Changed(Value As Boolean)
        If InvokeRequired Then
            Dim d As New DelegateSub0(AddressOf TechnikVisible_Changed)
            Invoke(d, New Object() {Value})
        Else
            xTechniknote.Visible = Value
            If Value Then
                pnlHantel.Visible = False
                xTechniknote.BringToFront()
            End If
        End If
    End Sub

    Private Sub Control_GotFocus(sender As Object, e As EventArgs) Handles pnlHantel.GotFocus, pnlWerbung.GotFocus, pnlWertung.GotFocus,
        xGewichtsklasse.GotFocus, xGültig1.GotFocus, xGültig2.GotFocus, xGültig3.GotFocus, xTechniknote.GotFocus, picFlagge.GotFocus

        Dummy.Focus()
    End Sub

    Private Sub wmpWerbung_PlayStateChange() 'sender As Object, e As _WMPOCXEvents_PlayStateChangeEvent)
        'With wmpWerbung
        '    .Visible = True
        '    .SetBounds(0, 0, ClientSize.Width, ClientSize.Height)
        '    .uiMode = "none"
        '    If e.newState = 3 Then .fullScreen = True
        'End With
    End Sub

    Private Sub Change_AdsVisibility(Value As Boolean)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub0(AddressOf Change_AdsVisibility)
                Invoke(d, New Object() {Value})
            Else
                With pnlWerbung
                    .Visible = Value
                    If Not .Visible Then
                        xAufruf.SendToBack()
                        'Change_Heber()
                    Else
                        .BringToFront()
                        .SetBounds(0, 0, ClientSize.Width, ClientSize.Height)
                        'pnlHantel.BringToFront()
                        xAufruf.BringToFront()
                    End If
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub xVerein_SizeChanged(sender As Object, e As EventArgs) Handles xVerein.SizeChanged, xVerein.Move
        xHantelgewicht.BringToFront()
    End Sub

    Private Sub xTeilnehmer_SizeChanged(sender As Object, e As EventArgs) Handles xTeilnehmer.SizeChanged, xTeilnehmer.Move
        xStartnummer.BringToFront()
    End Sub


End Class