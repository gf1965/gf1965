﻿
Public Class frmAufwärmen
    Private sc As ScreenScale = New ScreenScale

    Dim dicLampen As New Dictionary(Of Integer, TextBox) 'Container für Gültig

    Private func As myFunction = New myFunction

    ' Optionen
    Dim Design As String = "Nacht"

    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        RemoveHandler Anzeige.CountDown_Changed, AddressOf CountDown_Changed
        RemoveHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed

        RemoveHandler Anzeige.Lampe_Changed, AddressOf Change_Lampe
        'RemoveHandler Leader.Wertung_Changed, AddressOf Change_Lampe

        RemoveHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        RemoveHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed

        RemoveHandler User.Bohle_Changed, AddressOf Bohle_Changed

        RemoveHandler Wettkampf.Technik_Changed, AddressOf Technik_Changed
        RemoveHandler Wettkampf.MultiBohle_Changed, AddressOf Bohle_Changed

        RemoveHandler Leader.Gruppe_Changed, AddressOf Gruppe_Changed
        RemoveHandler Leader.Durchgang_Changed, AddressOf Durchgang_Changed
        RemoveHandler Leader.Mark_Rows, AddressOf Heber_Highlight
        RemoveHandler Leader.Dim_Rows, AddressOf Heber_Dim
        RemoveHandler Leader.Add_Rows, AddressOf dgv2_Move
        RemoveHandler Leader.Pause_Changed, AddressOf Change_Pause
        RemoveHandler Leader.Blockheben_Changed, AddressOf Change_Blockheben

        RemoveHandler ForeColors_Change, AddressOf Wertung_Clear
        'RemoveHandler RowCount_Changed, AddressOf dgv2_Move
        RemoveHandler Ansicht_Options.NameFormat_Changed, AddressOf NameFormat_Changed

        dicLampen.Clear()

        Using sc As New ScreenScale
            Dim mon = sc.GetDeviceNumber(Screen.FromControl(Me))
            If dicScreens.ContainsKey(mon) Then dicScreens.Remove(mon)
        End Using

    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        'Wertung
        dicLampen.Add(1, xGültig1)
        dicLampen.Add(2, xGültig2)
        dicLampen.Add(3, xGültig3)

        AddHandler Anzeige.CountDown_Changed, AddressOf CountDown_Changed
        AddHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed
        AddHandler Anzeige.Lampe_Changed, AddressOf Change_Lampe
        'AddHandler Leader.Wertung_Changed, AddressOf Change_Lampe

        AddHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        AddHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed

        AddHandler User.Bohle_Changed, AddressOf Bohle_Changed

        AddHandler Wettkampf.Technik_Changed, AddressOf Technik_Changed
        AddHandler Wettkampf.MultiBohle_Changed, AddressOf Bohle_Changed

        AddHandler Leader.Gruppe_Changed, AddressOf Gruppe_Changed
        AddHandler Leader.Durchgang_Changed, AddressOf Durchgang_Changed
        AddHandler Leader.Mark_Rows, AddressOf Heber_Highlight
        AddHandler Leader.Dim_Rows, AddressOf Heber_Dim
        AddHandler Leader.Add_Rows, AddressOf dgv2_Move
        AddHandler Leader.Pause_Changed, AddressOf Change_Pause
        AddHandler Leader.Blockheben_Changed, AddressOf Change_Blockheben

        AddHandler ForeColors_Change, AddressOf Wertung_Clear
        'AddHandler RowCount_Changed, AddressOf dgv2_Move
        AddHandler Ansicht_Options.NameFormat_Changed, AddressOf NameFormat_Changed

        Try
            For i As Integer = 1 To 3
                dicLampen(i).Font = New Font(FontFamilies(Fonts.Wertungsanzeige), dicLampen(i).Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            Next
            xTechniknote.Font = New Font(FontFamilies(Fonts.Techniknote), xTechniknote.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf.Font = New Font(FontFamilies(Fonts.Zeitanzeige), xAufruf.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf.ForeColor = Anzeige.Clock_Colors(Clock_Status.Waiting)
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        If Wettkampf.ID = 0 Then
            'Using New Centered_MessageBox(frmLeader)
            '    MessageBox.Show("Kein Wettkampf ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'End Using
            Close()
            Return
        End If

        lblCaption.Text = String.Empty

        Dim Wertung = CInt(Glob.dtModus.Select("idModus = " & Wettkampf.Modus)(0)("Wertung"))
        Dim NK_Stellen = Get_NK_Stellen(Wertung)
        Dim NK_String = "#,##0." + StrDup(NK_Stellen, "0")

        Build_Grid(dgvJoin)
        Build_Grid(dgv2)

        ' Team-Wertung
        If Wettkampf.Mannschaft Then
            With dgvTeam
                .AutoGenerateColumns = False
                .Columns("Gegner").Visible = Wettkampf.Finale
                .Columns("Punkte2").Visible = Wettkampf.Finale
                .DataSource = bsTeam
                '.Height = pnlWertung.Height
                .Height = .ColumnHeadersHeight + .Rows.Count * .Rows(0).Height
                .Columns("mReissen").DefaultCellStyle.Format = NK_String
                .Columns("mStossen").DefaultCellStyle.Format = NK_String
                .Columns("mHeben").DefaultCellStyle.Format = NK_String
                Dim w = 0
                For Each col As DataGridViewColumn In .Columns
                    If col.Visible Then w += col.Width
                Next
                .Width = w
            End With
        End If

        If Not IsNothing(Leader.Gruppe) AndAlso Leader.Gruppe.Count > 0 Then
            Durchgang_Changed(Leader.Durchgang)
        Else
            lblGruppe.Text = String.Empty
        End If

        If Not String.IsNullOrEmpty(Leader.Durchgang) Then
            Gruppe_Changed(Leader.Gruppe)
        Else
            lblDurchgang.Text = String.Empty
        End If

        'Technik_Changed(Wettkampf.Technikwertung)

        Cursor = Cursors.Default
    End Sub
    Private Sub Me_SizeChanged(sender As Object, e As EventArgs) Handles Me.SizeChanged
        With dgvJoin
            .AutoResizeColumns()
            .AutoResizeRows()
            .SendToBack()
            If .Rows.Count > 0 Then .FirstDisplayedScrollingRowIndex = 0
        End With
        With dgvTeam
            '.Location = New Point(pnlWertung.Left - .Width, ClientSize.Height - .Height)
            .Location = New Point(pnlWertung.Left - .Width, Height - .Height)
            .Visible = Wettkampf.Mannschaft
            .BringToFront()
        End With
    End Sub

    Private Sub lblCaption_SizeChanged(sender As Object, e As EventArgs) Handles lblCaption.SizeChanged
        '     Adjust_Caption()
    End Sub

    Private Sub Adjust_Caption()
        Dim LimitLeft = lblDurchgang.Left + lblDurchgang.Width
        Dim LimitRight = 0
        With lblCaption
            Using GF As New myFunction
                .Text = GF.String_Shorten(If(String.IsNullOrEmpty(Wettkampf.Kurz), Wettkampf.Bezeichnung, Wettkampf.Kurz),
                                          .Font,
                                          Width - LimitLeft - LimitRight,
                                          If(Wettkampf.MultiBohle, " (" & User.Bohle & ")", ""))
            End Using
            .Left = (Width - LimitLeft - LimitRight - .Width) \ 2 + LimitLeft
        End With
    End Sub

    Private Sub Heber_Highlight(Zeile As Integer)
        SuspendLayout()
        Try
            With dgvJoin
                .Rows(Zeile).DefaultCellStyle.ForeColor = Color.FromArgb(0, 240, 0) '(40, 164, 40) 'Lime
                If WK_Options.Markierung > 0 Then
                    For i As Integer = 1 To .Rows.Count - 1
                        Dim SetColor As Boolean
                        Select Case WK_Options.Markierung
                            Case 1 ' nächsten zwei Versuche
                                SetColor = i < 3
                            Case 2 ' Versuche des Hebers 
                                SetColor = CInt(dvLeader(Leader.TableIx)(i)("Teilnehmer")) = CInt(dvLeader(Leader.TableIx)(0)("Teilnehmer"))
                            Case 3 'Versuche mit gleicher Last
                                SetColor = CInt(dvLeader(Leader.TableIx)(i)("HLast")) = CInt(dvLeader(Leader.TableIx)(0)("HLast"))
                        End Select
                        If SetColor = True Then
                            .Rows(i).DefaultCellStyle.ForeColor = Color.FromArgb(180, 255, 180) '(204, 255, 204) 'Color.Yellow
                        Else
                            .Rows(i).DefaultCellStyle.ForeColor = Color.White
                        End If
                    Next
                End If
                .AutoResizeRows()
                .SendToBack()
                .ClearSelection()
            End With

            If Leader.RowNum - Leader.RowNum_Offset = 0 Then
                'dgvTeam.ColumnHeadersDefaultCellStyle.BackColor = Color.LimeGreen
                'dgvTeam.Columns(0).DefaultCellStyle.BackColor = Color.LimeGreen
                'If Not IsNothing(dgv2.DataSource) Then dgv2_Move()
            End If

            'pnlWertung.Visible = False

        Catch ex As Exception
        End Try
        ResumeLayout()
    End Sub
    Private Sub Heber_Dim(Zeile As Integer)
        If Zeile < 0 Then Return
        With dgvJoin
            .Rows(Zeile).DefaultCellStyle.BackColor = Color.DimGray
            .Rows(Zeile).DefaultCellStyle.ForeColor = Color.DarkGray
        End With
        'If Leader.RowNum - Leader.RowNum_Offset = 1 Then
        '    dgvTeam.ColumnHeadersDefaultCellStyle.BackColor = Color.DimGray
        '    dgvTeam.Columns(0).DefaultCellStyle.BackColor = Color.DimGray
        'End If
    End Sub

    Private Sub Change_Blockheben(Blockeheben As Boolean)
        Update_DataSource()
    End Sub

    Private Sub Bohle_Changed()
        Adjust_Caption()
    End Sub
    Private Sub Durchgang_Changed(Durchgang As String)
        lblDurchgang.Left = lblGruppe.Left + lblGruppe.Width
        lblDurchgang.Text = If(Not Wettkampf.Mannschaft, "-  ", "") + Durchgang

        Adjust_Caption()

        If Wettkampf.Mannschaft OrElse (Leader.Gruppe.Count > 0 AndAlso Not String.IsNullOrEmpty(Leader.Durchgang)) Then
            Update_DataSource()
        End If
    End Sub
    Private Sub Gruppe_Changed(Gruppe As List(Of String))
        If Not Wettkampf.Mannschaft Then
            lblGruppe.Text = "Gruppe " + String.Join(", ", Gruppe)
            lblDurchgang.Left = lblGruppe.Left + lblGruppe.Width
        Else
            lblGruppe.Visible = False
            lblDurchgang.Left = lblGruppe.Left
            lblDurchgang.Text = Leader.Durchgang
        End If
        Adjust_Caption()
        If Wettkampf.Mannschaft OrElse (Not IsNothing(Leader.Gruppe) AndAlso Leader.Gruppe.Count > 0 AndAlso Not String.IsNullOrEmpty(Leader.Durchgang)) Then
            Update_DataSource()
        End If
    End Sub
    Private Sub Technik_Changed(Technikwertung As Boolean)
        With pnlWertung
            If Technikwertung AndAlso Not xTechniknote.Visible Then
                .Left -= xTechniknote.Width
                .Width += xTechniknote.Width
                xTechniknote.Visible = True
            ElseIf Not Technikwertung AndAlso xTechniknote.Visible Then
                .Left += xTechniknote.Width
                .Width -= xTechniknote.Width
                xTechniknote.Visible = False
            End If
            .BringToFront()
            'dgvTeam.Location = New Point(.Left - dgvTeam.Width, ClientSize.Height - dgvTeam.Height)
            dgvTeam.Location = New Point(.Left - dgvTeam.Width, Height - dgvTeam.Height)
        End With
        dgvTeam.BringToFront()
    End Sub

    'Private Sub CurrentScore_Changed(Wertung As Object, row As Integer, col As String)
    '    Try
    '        dgvJoin.Rows(dgvJoin.CurrentRow.Index).Cells("Punkte").Value = Wertung
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Private Sub Update_DataSource()
        Refresh()
        SuspendLayout()

        With dgvJoin
            .DataSource = bsLeader
            If .Rows.Count = 0 Then Return ' keine DataSource
            .AutoResizeColumns()
            .AutoResizeRows()
            .Height = Height - .Top
        End With

        With dgv2
            If Leader.TableIx = 0 Then
                .DataSource = dvLeader(1)
                dgv2_Move()
                .AutoResizeColumns()
                .AutoResizeRows()
            Else
                .DataSource = Nothing
                .Visible = False
            End If
        End With
        Technik_Changed(Wettkampf.Technikwertung)

        If Leader.RowNum - Leader.RowNum_Offset = 1 Then Heber_Dim(0)

        ResumeLayout()
    End Sub

    Private Sub Build_Grid(grid As DataGridView, Optional Design As String = "Nacht")
        Try
            With grid
                Select Case Design
                    Case "Standard"
                        .AlternatingRowsDefaultCellStyle.BackColor = Color.OldLace
                        .RowsDefaultCellStyle.BackColor = Color.LightCyan
                        .BackgroundColor = SystemColors.AppWorkspace
                        .GridColor = SystemColors.ControlDark
                        With .DefaultCellStyle
                            .ForeColor = SystemColors.ControlText
                            .SelectionBackColor = Color.LightCyan
                            .SelectionForeColor = SystemColors.ControlText
                        End With
                    Case "Nacht"
                        .AlternatingRowsDefaultCellStyle.BackColor = BackColor
                        .RowsDefaultCellStyle.BackColor = Color.Black
                        .BackgroundColor = Color.Black
                        .GridColor = Color.Black
                        With .DefaultCellStyle
                            .ForeColor = Color.White
                            .SelectionBackColor = Color.Black
                            .SelectionForeColor = Color.White
                        End With
                End Select
                .AllowUserToOrderColumns = False
                .AutoGenerateColumns = False

                Dim _dg = "Hantellast"
                If grid.Name.Equals("dgv2") Then _dg = "  Stoßen  "

                Dim ColBez() As String = {"Teilnehmer", "Nr.", "Nachname", "Vorname", "KG", "Versuch", _dg, "Verein", "Punkte", "      "}
                Dim ColData() As String = {"Teilnehmer", "Reihenfolge", "Nachname", "Vorname", "Wiegen", "Versuch", "HLast", "Vereinsname", "", "a_K"}
                Dim ColAlignment() As Integer = {32, 32, 16, 16, 32, 32, 32, 16, 32, 32}
                Dim ColInVisible() As String = {"Teilnehmer", "Punkte"}
                If .Columns.Count = 0 Then
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Teilnehmer"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Reihenfolge"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname"}) ', .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname"}) ', .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Gewicht"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Versuch"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Last"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Verein", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Punkte"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "a_K", .Visible = func.Show_a_K})
                End If
                For i As Integer = 0 To ColBez.Count - 1
                    .Columns(i).HeaderText = ColBez(i)
                    .Columns(i).DataPropertyName = ColData(i)
                    .Columns(i).DefaultCellStyle.Alignment = CType(ColAlignment(i), DataGridViewContentAlignment)
                    .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                Next
                For Each col In ColInVisible
                    .Columns(col).Visible = False
                Next
                .Columns("Teilnehmer").Visible = False
                .Columns("Last").DefaultCellStyle.ForeColor = Color.Orange
                If .Columns.Contains("Gewicht") Then
                    .Columns("Gewicht").DefaultCellStyle.Format = "0.00"
                    .Columns("Gewicht").Visible = False
                End If
                If .Columns.Contains("Punkte") Then .Columns("Punkte").DefaultCellStyle.Format = "#.#0"
            End With

            Change_NameFormat(grid)

        Catch ex As Exception
            'Stop
        End Try
    End Sub

    Private Sub NameFormat_Changed()
        Change_NameFormat(dgvJoin)
        Change_NameFormat(dgv2)
    End Sub

    Private Sub Change_NameFormat(dgv As DataGridView)
        Try
            With dgv
                Select Case Ansicht_Options.NameFormat
                    Case NameFormat.Standard
                        .Columns("Nachname").DataPropertyName = "Nachname"
                    Case NameFormat.Standard_UCase
                        .Columns("Nachname").DataPropertyName = "Name1"
                    Case NameFormat.International
                        .Columns("Nachname").DataPropertyName = "Name2"
                End Select
                .Columns("Vorname").Visible = Ansicht_Options.NameFormat < NameFormat.International
                .AutoResizeColumns()
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dgvJoin_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvJoin.CellFormatting, dgv2.CellFormatting

        If CType(sender, DataGridView).Columns(e.ColumnIndex).Name.Equals("a_K") Then
            If Equals(e.Value, False) Then
                e.Value = ""
                e.FormattingApplied = True
            ElseIf Equals(e.Value, True) Then
                e.Value = "a.K."
                e.FormattingApplied = True
            End If
        End If
    End Sub
    Private Sub dgvJoin_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvJoin.DataError
        Try
        Catch ex As Exception
        End Try
    End Sub

    ' Mamnnschaftswertung
    Private Sub dgvTeam_SelectionChanged(sender As Object, e As EventArgs) Handles dgvTeam.SelectionChanged
        dgvTeam.ClearSelection()
    End Sub
    Private Sub dgvTeam_SizeChanged(sender As Object, e As EventArgs) Handles dgvTeam.SizeChanged
        With dgvTeam
            '.Location = New Point(pnlWertung.Left - .Width, ClientSize.Height - .Height)
            .Location = New Point(pnlWertung.Left - .Width, Height - .Height)
        End With
    End Sub

    '' 2. DGV
    Private Sub dgv2_Move() 'RowCount As Integer)

        Dim rect As Rectangle
        Dim loc As Point

        Try
            With dgvJoin
                rect = .GetRowDisplayRectangle(.Rows.GetRowCount(DataGridViewElementStates.Displayed) - 1, True)
                loc = New Point(.Left, rect.Bottom + .Top + 10) ' dgv2 wird 5 pt unterhalb der letzten Zeile von dgvLeader positioniert
            End With

            With dgv2
                Dim minHeight = .ColumnHeadersHeight + .RowTemplate.Height + 2
                If .Rows.Count > 0 AndAlso (dgvJoin.Height + dgvJoin.Top) - loc.Y > minHeight Then
                    .Location = loc
                    .Size = New Size(dgvJoin.Width, (dgvJoin.Height + dgvJoin.Top) - loc.Y)
                    .Visible = True
                    .BringToFront()
                Else
                    .Visible = False
                End If
            End With
        Catch ex As Exception
        End Try

        xAufruf.BringToFront()
        pnlWertung.BringToFront()
        dgvTeam.BringToFront()
    End Sub
    Private Sub dgv_SelectionChanged(sender As Object, e As EventArgs) Handles dgvJoin.SelectionChanged, dgv2.SelectionChanged
        CType(sender, DataGridView).ClearSelection()
    End Sub
    Private Sub dgvJoin_RowsRemoved(sender As Object, e As DataGridViewRowsRemovedEventArgs) Handles dgvJoin.RowsRemoved
        If Leader.TableIx = 0 Then
            SuspendLayout()
            dgv2_Move()
            ResumeLayout()
        End If
    End Sub

    Private Delegate Sub Delegate_String(Value As String)
    Private Delegate Sub Delegate_Color(Value As Color)
    Private Delegate Sub Delegate_Bool(Value As Boolean)
    Private Delegate Sub Delegate_IndexColor(Index As Integer, Value As Color)
    Private Sub CountDown_Changed(Value As String)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_String(AddressOf CountDown_Changed)
                Invoke(d, New Object() {Value})
            Else
                xAufruf.Text = Value
            End If
        Catch
        End Try
    End Sub
    Private Sub ClockColor_Changed(Color As Color)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_Color(AddressOf ClockColor_Changed)
                Invoke(d, New Object() {Color})
            Else
                xAufruf.ForeColor = Color
            End If
        Catch
        End Try
    End Sub
    Private Sub Change_Pause(Value As Boolean)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_Bool(AddressOf Change_Pause)
                Invoke(d, New Object() {Value})
            Else
                pnlWertung.Visible = Not Value
                xAufruf.BringToFront()
                If Not Value Then pnlWertung.BringToFront()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Wertung_Clear(HeberPresent As Boolean)
        For i = 1 To 3
            dicLampen(i).ForeColor = Ansicht_Options.Shade_Color
        Next
        xTechniknote.ForeColor = Ansicht_Options.Shade_Color
        xTechniknote.Text = "X,XX"
    End Sub
    Private Sub Change_Lampe(Index As Integer, color As Color)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_IndexColor(AddressOf Change_Lampe)
                Invoke(d, New Object() {Index, color})
            Else
                dicLampen(Index).ForeColor = color
                dicLampen(Index).Refresh()
            End If
        Catch
        End Try
    End Sub
    Private Sub TechnikWert_Changed(Wert As String)
        If InvokeRequired Then
            Dim d As New Delegate_String(AddressOf TechnikWert_Changed)
            Invoke(d, New Object() {Wert})
        Else
            xTechniknote.Text = Wert
        End If
    End Sub
    Private Sub TechnikFarbe_Changed(Farbe As Color)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_Color(AddressOf TechnikFarbe_Changed)
                Invoke(d, New Object() {Farbe})
            Else
                xTechniknote.ForeColor = Farbe
            End If
        Catch
        End Try
    End Sub


End Class