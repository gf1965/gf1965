﻿
Imports System.ComponentModel
Imports MySqlConnector

Public Class Drucken_Quittung

    Property Bereich As String

    Dim PrintDate As String

    Dim Query As String
    Dim cmd As MySqlCommand

    'Dim dvVerein As DataView
    Dim dvVerband As DataView
    Dim dvMeldung As DataView

    WithEvents bsTable As BindingSource

    Dim dtMeldung As New DataTable
    Dim dtVerein As New DataTable
    Dim dtRegion As New DataTable
    Dim dtPrint As New DataTable
    'Dim dtVerband As New DataTable
    Dim dtAthletik As New DataTable
    Dim dtGruppen As New DataTable
    Dim dtLänderwertung As New DataTable

    Dim dtVorlage As New DataTable
    Dim dvVorlage As DataView

    'Dim AK_w As String = Wettkampf.AKs("w").ToString
    'Dim AK_m As String = Wettkampf.AKs("m").ToString
    Dim Vorlage As String
    Dim Driver As String
    Dim Message As New myMsg
    WithEvents report As FastReport.Report
    Dim action As String
    Dim Count As Integer
    Dim Docs As String
    Private Delegate Sub SetMsg(ByVal Value As String)

    Dim CurrList As New CheckedListBox
    Private PreviousTab As TabPage

    Private SelectedIndex As Integer
    Private _SubCount As Integer
    Property SubCount As Integer
        Get
            Return _SubCount
        End Get
        Set(value As Integer)
            _SubCount = value
            Docs = Bereich & If(value < 2, "", "en")
        End Set
    End Property

    Private Sub Me_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If btnCancel.Text = "Abbrechen" Then
            BackgroundWorker1.CancelAsync()
            e.Cancel = True
        Else
            NativeMethods.INI_Write(Bereich, "Vorlage", Vorlage, App_IniFile)
            RemoveHandler Message.Msg_Changed, AddressOf ProgressMsg
        End If
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

#Region "Vorlage"
        Dim x As String
        x = NativeMethods.INI_Read(Bereich, "Vorlage", App_IniFile)
        If Not x.Equals("?") Then Vorlage = x

        dtVorlage.Columns.Add("FileName", GetType(String))
        dtVorlage.Columns.Add("Directory", GetType(String))
        'Dim cols(1) As DataColumn
        'cols(0) = dtVorlage.Columns("FileName")
        'cols(1) = dtVorlage.Columns("Directory")
        'dtVorlage.PrimaryKey = cols

        Dim v = Directory.GetFiles(Path.Combine(Application.StartupPath, "Report"), Bereich & "*.frx", SearchOption.AllDirectories).ToList
        For i = 0 To v.Count - 1
            v(i) = Replace(v(i), Path.Combine(Application.StartupPath, "Report") + "\", "")
            v(i) = Replace(v(i), ".frx", "")
            dtVorlage.Rows.Add(v(i), Path.Combine(Application.StartupPath, "Report"))
        Next
        dvVorlage = New DataView(dtVorlage)
        dvVorlage.Sort = "FileName"

        With cboVorlage
            .DataSource = dvVorlage
            .DisplayMember = "FileName"
            .ValueMember = "Directory"
            .SelectedIndex = .FindStringExact(Vorlage)
        End With
#End Region

        AddHandler Message.Msg_Changed, AddressOf ProgressMsg
    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim dtMeldefrist As New DataTable
        Dim dtStartgeld As New DataTable
        Refresh()

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()

                ' Tabelle Meldung-Startgeld
                Query = "
                    select m.Wettkampf, if(w.Kurz = '', wb.Bez1, w.Kurz) Bez, m.attend, concat(if(a.Titel != '', concat(a.Titel, ' '), ''),  a.Nachname, ', ', a.Vorname) Bezeichnung, a.Verein, IfNull(v.Kurzname, v.Vereinsname) Vereinsname, m.Meldedatum,
	                    if(m.Meldedatum <= wm.Datum1,
		                    if(a.Lizenz = true,
			                    (
				                    select Gebuehr
				                    from wettkampf_startgeld 
				                    where Wettkampf = m.Wettkampf and if( not isnull( (select Gebuehr from wettkampf_startgeld where Wettkampf = m.Wettkampf and AK = 0) ), 0, if(m.idAK > 100, 100, if(m.idAK between 20 and 29, 20, m.idAK) ) ) = AK
			                    ), 
			                    (
				                    select Gebuehr_oL
				                    from wettkampf_startgeld 
				                    where Wettkampf = m.Wettkampf and if( not isnull( (select Gebuehr_oL from wettkampf_startgeld where Wettkampf = m.Wettkampf and AK = 0) ), 0, if(m.idAK > 100, 100, if(m.idAK between 20 and 29, 20, m.idAK) ) ) = AK
			                    )
		                    ), 
                            if(a.Lizenz = true,
			                    (
				                    select NM_Gebuehr 
				                    from wettkampf_startgeld 
				                    where Wettkampf = m.Wettkampf and if( not isnull( (select NM_Gebuehr from wettkampf_startgeld where Wettkampf = m.Wettkampf and AK = 0) ), 0, if(m.idAK > 100, 100, if(m.idAK between 20 and 29, 20, m.idAK) ) ) = AK
			                    ),
			                    (
				                    select NM_Gebuehr_oL 
				                    from wettkampf_startgeld 
				                    where Wettkampf = m.Wettkampf and if( not isnull( (select NM_Gebuehr_oL from wettkampf_startgeld where Wettkampf = m.Wettkampf and AK = 0) ), 0, if(m.idAK > 100, 100, if(m.idAK between 20 and 29, 20, m.idAK) ) ) = AK
			                    )
		                    )
	                    ) Gebuehr 
                    From meldung m 
                    left join wettkampf w on w.Wettkampf = m.Wettkampf
                    left join wettkampf_bezeichnung wb on wb.Wettkampf = m.Wettkampf
                    left join athleten a on a.idTeilnehmer = m.Teilnehmer 
                    left join verein v on v.idVerein = a.Verein 
                    left join wettkampf_meldefrist wm on wm.Wettkampf = m.Wettkampf
                    left join wettkampf_startgeld ws on ws.Wettkampf = m.Wettkampf 
                    where " & Wettkampf.WhereClause("m") & "
                    union
                    select m.Wettkampf, if(wb.Bez2 != '', Bez2, if(w.Kurz != '', w.Kurz, wb.Bez1) ) Bez, m.attend, 
	                    concat('Vereinswertung (', max(m.Vereinswertung) over(partition by Verein), 'x)') Bezeichnung, 
                        a.Verein, IfNull(v.Kurzname, v.Vereinsname) Vereinsname, m.Meldedatum, wm.Verein * max(m.Vereinswertung) over(partition by Verein) Gebuehr
                    from meldung m
                    left join wettkampf w on w.Wettkampf = m.Wettkampf
                    left join wettkampf_bezeichnung wb on wb.Wettkampf = m.Wettkampf
                    left join athleten a on a.idTeilnehmer = m.Teilnehmer 
                    left join verein v on v.idVerein = a.Verein 
                    left join wettkampf_meldefrist wm on wm.Wettkampf = m.Wettkampf
                    where " & Wettkampf.WhereClause("m") & " and not isnull(m.Vereinswertung) and m.Vereinswertung != 0
                    group by a.idTeilnehmer
                    order by Bezeichnung, Bez;"
                cmd = New MySqlCommand(Query, conn)
                dtMeldung.Load(cmd.ExecuteReader)

                ' Tabelle Verbände-Gebühr
                Query = "
                    SELECT m.Wettkampf, if(wb.Bez3 != '', Bez3, if(w.Kurz != '', w.Kurz, wb.Bez1) ) Bez, r.region_id, 
	                    ifnull(r.Kurz, ifnull(r.Bezeichnung, r.region_name)) Landesverband, IfNull(v.Kurzname, v.Vereinsname) Bezeichnung, m.Meldedatum, wm.Land Gebuehr 
                    FROM meldung m 
                    left join wettkampf w on w.Wettkampf = m.Wettkampf
                    left join wettkampf_bezeichnung wb on wb.Wettkampf = m.Wettkampf
                    LEFT JOIN athleten a ON a.idTeilnehmer = m.Teilnehmer
                    LEFT JOIN verein v ON v.idVerein = a.Verein 
                    LEFT JOIN region r ON r.region_id = v.Region 
                    LEFT JOIN wettkampf_meldefrist wm ON wm.Wettkampf = m.Wettkampf 
                    WHERE " & Wettkampf.WhereClause("m") & " and m.Laenderwertung 
                    GROUP BY v.idVerein
                    order by Landesverband, Bezeichnung;"
                cmd = New MySqlCommand(Query, conn)
                dtLänderwertung.Load(cmd.ExecuteReader)

                ' Tabelle Verein
                cmd = New MySqlCommand("SELECT * FROM verein;", conn)
                dtVerein.Load(cmd.ExecuteReader)

                ' Tabelle Region
                cmd = New MySqlCommand("SELECT * FROM region;", conn)
                dtRegion.Load(cmd.ExecuteReader)

            Catch ex As MySqlException
                MySQl_Error(Nothing, "Drucken_Quittung: Me_Shown: " & ex.Message, ex.StackTrace, True)
            End Try
        End Using

        If dtMeldung.Rows.Count = 0 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Athleten gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            Close()
            Return
        End If

        CurrList = lstVerein

        ' Ländermannschaften
        dvVerband = New DataView(dtLänderwertung)

        ' Teilnehmer / Vereinsmannschaften
        dvMeldung = New DataView(dtMeldung)
        bsTable = New BindingSource With {.DataSource = dvMeldung}

        With dgvMeldung
            .AutoGenerateColumns = False
            .DataSource = bsTable
            '.Columns("WK_ID").Visible = Glob.dtWK.Rows.Count > 1
        End With

        ' Liste Vereine
        With lstVerein
            .DataSource = New DataView(dtMeldung.DefaultView.ToTable(True, {"Verein", "Vereinsname"}))
            .DisplayMember = "Vereinsname"
            .ValueMember = "Verein"
        End With

        ' Liste Verbände
        With lstLand
            .DataSource = New DataView(dtLänderwertung.DefaultView.ToTable(True, {"region_id", "Landesverband"}))
            .DisplayMember = "Landesverband"
            .ValueMember = "region_id"
        End With

        tabLänder.Enabled = lstLand.Items.Count > 0

        formMain.Change_MainProgressBarValue(0)

        Cursor = Cursors.Default
    End Sub

    Private Sub cboVorlage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVorlage.SelectedIndexChanged
        If cboVorlage.Focused Then Vorlage = cboVorlage.Text
    End Sub

    Private Sub chkSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkSelectAll.CheckedChanged
        With CurrList
            If chkSelectAll.Focused Then
                For i = 0 To .Items.Count - 1
                    .SetItemChecked(i, chkSelectAll.Checked)
                Next
                btnVorschau.Enabled = .CheckedItems.Count > 0
                btnPrint.Enabled = .CheckedItems.Count > 0
            End If
        End With
    End Sub
    Private Sub chkAnwesend_CheckedChanged(sender As Object, e As EventArgs) Handles chkAnwesend.CheckedChanged
        Dim _Filter = " Verein = " + DirectCast(CurrList.SelectedItem, DataRowView)!Verein.ToString
        If chkAnwesend.Checked Then _Filter += " and attend = true"
        bsTable.Filter = _Filter
    End Sub

    Private Sub TabControl1_Deselected(sender As Object, e As TabControlEventArgs) Handles TabControl1.Deselected
        PreviousTab = e.TabPage
    End Sub
    Private Sub TabControl1_Selected(sender As Object, e As TabControlEventArgs) Handles TabControl1.Selected
        If Not e.TabPage.Enabled Then
            TabControl1.SelectedTab = PreviousTab
        ElseIf e.TabPage.Name.Equals("tabVereine") Then
            CurrList = lstVerein
            bsTable = New BindingSource With {.DataSource = dvMeldung}
            dgvMeldung.Columns("Bezeichnung").HeaderText = "Name, Vorname"
        ElseIf e.TabPage.Name.Equals("tabLänder") Then
            CurrList = lstLand
            bsTable = New BindingSource With {.DataSource = dvVerband}
            dgvMeldung.Columns("Bezeichnung").HeaderText = "Vereinsname"
        End If

        chkAnwesend.Enabled = e.TabPage.Name.Equals("tabVereine")
        dgvMeldung.DataSource = bsTable

        Set_ControlStates()

    End Sub

    Private Sub CheckedList_SelectedValueChanged(sender As Object, e As EventArgs) Handles lstVerein.SelectedValueChanged, lstLand.SelectedValueChanged

        With CurrList
            If IsNothing(.SelectedValue) Then Return
            Try
                If .Equals(lstVerein) Then
                    bsTable.Filter = "Verein = " + DirectCast(.SelectedItem, DataRowView)!Verein.ToString
                Else
                    bsTable.Filter = "region_id = " + DirectCast(.SelectedItem, DataRowView)!region_id.ToString
                End If
            Catch ex As Exception
            End Try
        End With

        Set_ControlStates()

        Dim Summe = (From x In CType(bsTable.DataSource, DataView).ToTable
                     Select x.Field(Of Double?)("Gebuehr")).Sum

        If dgvMeldung.Rows.Count > 0 Then
            Dim rect = dgvMeldung.GetRowDisplayRectangle(0, True)
            lblSumme.Left = dgvMeldung.Left + rect.Width + 1 - lblSumme.Width
        Else
            lblSumme.Left = dgvMeldung.Left + dgvMeldung.Width - lblSumme.Width - 1
        End If

        lblSumme.Text = Format(Summe, "F") & " €"
    End Sub

    Private Sub dgvMeldung_SelectionChanged(sender As Object, e As EventArgs) Handles dgvMeldung.SelectionChanged
        dgvMeldung.ClearSelection()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        BackgroundWorker1.CancelAsync()
        If btnCancel.Text = "Beenden" Then Close()
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        Dim FullFileName = Get_FileName(Me, "Druckvorlage suchen")

        If Not IsNothing(FullFileName) Then
            Try
                Dim PathParts = Split(FullFileName, "\").ToList
                Dim FileName = PathParts.LastOrDefault()
                Vorlage = Split(FileName, ".")(0)
                PathParts.Remove(PathParts.LastOrDefault)
                Dim PathToFile = Path.Combine(PathParts.ToArray)
                dtVorlage.Rows.Add(Vorlage, PathToFile)
                cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage)
            Catch ex As Exception
            End Try
        End If
        cboVorlage.Focus()
    End Sub

    Private Sub btnFastReport_Click(sender As Object, e As EventArgs) Handles btnVorschau.Click, btnPrint.Click, btnEdit.Click

        If Not BackgroundWorker1.IsBusy Then

            Dim msg = "Verein"
            If CurrList.Equals(lstLand) Then
                msg = "Landesverband"
            End If

            User.Check_MySQLDriver(Me)

            If CurrList.CheckedItems.Count = 0 And Not sender.Equals(btnEdit) Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Kein " & msg & " ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                Return
            End If

            If String.IsNullOrEmpty(Vorlage) AndAlso String.IsNullOrEmpty(cboVorlage.Text) Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Keine Druckvorlage ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                Return
            End If

            Cursor = Cursors.WaitCursor

            action = CType(sender, Button).Tag.ToString
            ProgressBar.Maximum = CurrList.CheckedIndices.Count
            Count = 0
            SubCount = 0
            ProgressBar.Value = Count
            lblProgress.Text = "Druck-Fortschritt"
            btnCancel.Text = "Abbrechen"
            btnVorschau.Enabled = False
            btnPrint.Enabled = False
            btnEdit.Enabled = False
            BackgroundWorker1.RunWorkerAsync()
        End If
    End Sub

    Private Sub Set_ControlStates()
        With CurrList
            btnPrint.Enabled = .CheckedItems.Count > 0
            btnVorschau.Enabled = .CheckedItems.Count > 0

            If .CheckedItems.Count > 0 AndAlso .CheckedItems.Count < .Items.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            Else
                chkSelectAll.Checked = .CheckedItems.Count = .Items.Count
            End If
        End With
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As DoWorkEventArgs) Handles BackgroundWorker1.DoWork

        report = New FastReport.Report

        If action = "Edit" Then
            If CurrList.Equals(lstVerein) Then
                Create_Report(New DataView(dtMeldung))
            Else
                Create_Report(New DataView(dtLänderwertung))
            End If
            Return
        End If

        For Each Check As DataRowView In CurrList.CheckedItems
            Threading.Thread.Sleep(25)
            If BackgroundWorker1.CancellationPending Then
                e.Cancel = True
                Exit For
            Else
                BackgroundWorker1.ReportProgress(Count)

                If CurrList.Equals(lstVerein) Then
                    Dim _Filter = "Verein = " & Check(0).ToString
                    If chkAnwesend.Checked Then _Filter += " AND attend = True"
                    Create_Report(New DataView(dtMeldung, _Filter, Nothing, DataViewRowState.CurrentRows))
                Else
                    Dim _Filter = "region_id = " & Check(0).ToString
                    Create_Report(New DataView(dtLänderwertung, _Filter, Nothing, DataViewRowState.CurrentRows))
                End If
            End If
        Next
    End Sub
    Private Sub BackgroundWorker1_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        Try
            ProgressBar.Value = e.ProgressPercentage
        Catch ex As Exception
        End Try
    End Sub
    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        If e.Cancelled Then
            Message.msg = "Vorgang abgebrochen"
        ElseIf e.Cancelled = False Then
            If Not action = "Edit" Then
                ProgressBar.Value = CurrList.CheckedIndices.Count
                Message.msg = String.Concat(SubCount.ToString, " ", Docs, " fertig")
            End If
            Output()
        End If
        btnVorschau.Enabled = True
        btnPrint.Enabled = True
        btnEdit.Enabled = True
        btnCancel.Text = "&Beenden"
        Cursor = Cursors.Default
    End Sub

    Private Delegate Sub CreateReport(dvDocs As DataView)
    Private Sub Create_Report(dvDocs As DataView)
        If InvokeRequired Then
            Dim d As New CreateReport(AddressOf Create_Report)
            Invoke(d, New Object() {dvDocs})
        Else
            With report
                Try
                    .Load(Path.Combine(cboVorlage.SelectedValue.ToString, Vorlage + ".frx"))
                    .Dictionary.Connections(0).ConnectionString = User.FR_ConnString
                Catch ex As Exception
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Vorlage nicht gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                    Return
                End Try

                If CurrList.Equals(lstVerein) Then
                    .RegisterData(dvDocs.ToTable, "Heber")
                    .RegisterData(New DataView(dtVerein, "idVerein = " & dvDocs(0)!Verein.ToString, Nothing, DataViewRowState.CurrentRows).ToTable, "verein")
                ElseIf CurrList.Equals(lstLand) Then
                    .RegisterData(dvDocs.ToTable(True, {"Wettkampf", "Bez", "region_id", "Landesverband", "Bezeichnung", "Meldedatum", "Gebuehr"}), "Länderwertung")
                    .RegisterData(New DataView(dtRegion, "region_id = " & dvDocs(0)("region_id").ToString, Nothing, DataViewRowState.CurrentRows).ToTable, "region")
                End If
                SubCount += 1

                .SetParameterValue("Länderwertung", CurrList.Equals(lstLand))
                .SetParameterValue("IsMultiWettkampf", Glob.dtWK.Rows.Count > 1)
                .SetParameterValue("IsPrintDateToday", Not chkDatum.Checked)
                .RegisterData(Glob.dtWK, "wk")

                Try
                    Dim s = New FastReport.EnvironmentSettings
                    s.ReportSettings.ShowProgress = False
                    Count += 1
                    If Not action = "Edit" Then .Prepare(Count > 0)
                Catch ex As Exception
                    action = "cancelled"
                    LogMessage("Drucken_Quittung (Vorlage: " & Vorlage & "): Create_Report: " & Bereich & ": " & ex.Message, ex.StackTrace)
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Fehler beim Erstellen des Dokuments." & vbNewLine & ex.Message, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Using
                End Try
            End With
        End If
    End Sub
    Private Sub Output(Optional msg As String = "")
        If InvokeRequired Then
            Dim d As New SetMsg(AddressOf Output)
            Invoke(d, New Object() {msg})
        Else
            If action.Equals("cancelled") Then
                Message.msg = "Abbruch - Fehler im Formular"
                Return
            End If
            If Not String.IsNullOrEmpty(msg) Then
                Message.msg = msg
            ElseIf Not action = "Edit" Then
                Message.msg = String.Concat(SubCount.ToString, " ", Docs, " für ", Count.ToString, " ", If(Count > 1, TabControl1.SelectedTab.Text, TabControl1.SelectedTab.Text.Substring(0, TabControl1.SelectedTab.Text.Length - 1)), " gedruckt") 'If(CurrList.Equals(lstVerein), " Verein" & If(Count > 1, "e", "").ToString, If(Count > 1, " Verbände", " Verband")), " gedruckt")
            End If
            Try
                If action.Equals("Show") Then
                    report.ShowPrepared()
                ElseIf action.Equals("Print") Then
                    report.PrintPrepared()
                ElseIf action.Equals("Edit") Then
                    report.Design()
                End If
            Catch ex As Exception
                LogMessage("Drucken_Quittung: Output: " & Bereich & ": " & ex.Message, ex.StackTrace)
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Fehler beim Öffnen des Dokuments." & vbNewLine & ex.Message, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
            End Try
        End If
    End Sub
    Private Sub ProgressMsg(msg As String)
        If InvokeRequired Then
            Dim d As New SetMsg(AddressOf ProgressMsg)
            Invoke(d, New Object() {msg})
        Else
            lblProgress.Text = msg
        End If
    End Sub
    Private Function Get_Date(dvDocs As DataView) As Date
        Dim g As Integer
        Try
            Dim x As List(Of Integer) = dvDocs.ToTable.AsEnumerable().Select(Function(s) CInt(s("Gruppe"))).ToList()
            x.Sort()
            For Each i In x
                If i > 0 Then
                    g = i
                    Exit For
                End If
            Next
        Catch ex As Exception
        End Try
        Dim d = dtPrint.Select("Gruppe = " & g.ToString)

        If Not chkDatum.Checked Then Return Now
        If d.Count = 0 Then Return Wettkampf.Datum
        Return CDate(d(0)("Druckdatum"))

    End Function

End Class