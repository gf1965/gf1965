﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Import_Heber
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtImportFile = New System.Windows.Forms.TextBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.dgvHeber = New System.Windows.Forms.DataGridView()
        Me.Teilnehmer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nachname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vorname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Geschlecht = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jahrgang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lstFields = New System.Windows.Forms.ListBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkCheckOnline = New System.Windows.Forms.CheckBox()
        Me.chkInternational = New System.Windows.Forms.CheckBox()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Title = "Import-Datei öffnen"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Import-Datei"
        '
        'txtImportFile
        '
        Me.txtImportFile.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtImportFile.Location = New System.Drawing.Point(14, 26)
        Me.txtImportFile.Name = "txtImportFile"
        Me.txtImportFile.Size = New System.Drawing.Size(553, 20)
        Me.txtImportFile.TabIndex = 21
        Me.txtImportFile.TabStop = False
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnSearch.Location = New System.Drawing.Point(584, 20)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(152, 27)
        Me.btnSearch.TabIndex = 10
        Me.btnSearch.Text = "Datei auswählen"
        Me.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.Enabled = False
        Me.btnImport.Location = New System.Drawing.Point(584, 53)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(152, 27)
        Me.btnImport.TabIndex = 11
        Me.btnImport.Text = "Athleten importieren"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'dgvHeber
        '
        Me.dgvHeber.AllowUserToAddRows = False
        Me.dgvHeber.AllowUserToDeleteRows = False
        Me.dgvHeber.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro
        Me.dgvHeber.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvHeber.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvHeber.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLight
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvHeber.ColumnHeadersHeight = 24
        Me.dgvHeber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHeber.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Teilnehmer, Me.Nachname, Me.Vorname, Me.Geschlecht, Me.Jahrgang})
        Me.dgvHeber.EnableHeadersVisualStyles = False
        Me.dgvHeber.Location = New System.Drawing.Point(15, 54)
        Me.dgvHeber.MultiSelect = False
        Me.dgvHeber.Name = "dgvHeber"
        Me.dgvHeber.ReadOnly = True
        Me.dgvHeber.RowHeadersVisible = False
        Me.dgvHeber.RowHeadersWidth = 25
        Me.dgvHeber.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHeber.Size = New System.Drawing.Size(551, 463)
        Me.dgvHeber.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.dgvHeber, "Rechtsklick auf Spaltenkopf öffnet Zuordnung")
        '
        'Teilnehmer
        '
        Me.Teilnehmer.DataPropertyName = "idTeilnehmer"
        Me.Teilnehmer.HeaderText = "Nummer"
        Me.Teilnehmer.Name = "Teilnehmer"
        Me.Teilnehmer.ReadOnly = True
        Me.Teilnehmer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Teilnehmer.Width = 52
        '
        'Nachname
        '
        Me.Nachname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Nachname.DataPropertyName = "Nachname"
        Me.Nachname.HeaderText = "Nachname"
        Me.Nachname.Name = "Nachname"
        Me.Nachname.ReadOnly = True
        Me.Nachname.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Vorname
        '
        Me.Vorname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Vorname.DataPropertyName = "Vorname"
        Me.Vorname.HeaderText = "Vorname"
        Me.Vorname.Name = "Vorname"
        Me.Vorname.ReadOnly = True
        Me.Vorname.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Geschlecht
        '
        Me.Geschlecht.DataPropertyName = "Geschlecht"
        Me.Geschlecht.HeaderText = "Geschlecht"
        Me.Geschlecht.Name = "Geschlecht"
        Me.Geschlecht.ReadOnly = True
        Me.Geschlecht.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Geschlecht.Width = 67
        '
        'Jahrgang
        '
        Me.Jahrgang.DataPropertyName = "Jahrgang"
        Me.Jahrgang.HeaderText = "Jahrgang"
        Me.Jahrgang.Name = "Jahrgang"
        Me.Jahrgang.ReadOnly = True
        Me.Jahrgang.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Jahrgang.Width = 57
        '
        'lstFields
        '
        Me.lstFields.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstFields.FormattingEnabled = True
        Me.lstFields.Location = New System.Drawing.Point(584, 109)
        Me.lstFields.Name = "lstFields"
        Me.lstFields.Size = New System.Drawing.Size(97, 106)
        Me.lstFields.TabIndex = 50
        Me.lstFields.TabStop = False
        Me.lstFields.Visible = False
        '
        'chkCheckOnline
        '
        Me.chkCheckOnline.AutoSize = True
        Me.chkCheckOnline.Location = New System.Drawing.Point(584, 477)
        Me.chkCheckOnline.Name = "chkCheckOnline"
        Me.chkCheckOnline.Size = New System.Drawing.Size(90, 17)
        Me.chkCheckOnline.TabIndex = 51
        Me.chkCheckOnline.Text = "Online-Check"
        Me.ToolTip1.SetToolTip(Me.chkCheckOnline, "Überprüfen, ob Athlet im Online-Portal eingetragen ist.")
        Me.chkCheckOnline.UseVisualStyleBackColor = True
        '
        'chkInternational
        '
        Me.chkInternational.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkInternational.AutoSize = True
        Me.chkInternational.Checked = True
        Me.chkInternational.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkInternational.Location = New System.Drawing.Point(584, 500)
        Me.chkInternational.Name = "chkInternational"
        Me.chkInternational.Size = New System.Drawing.Size(147, 17)
        Me.chkInternational.TabIndex = 52
        Me.chkInternational.TabStop = False
        Me.chkInternational.Text = "internationaler Wettkampf"
        Me.ToolTip1.SetToolTip(Me.chkInternational, "keine Erfassung der Vereins-Daten")
        Me.chkInternational.UseVisualStyleBackColor = True
        '
        'Import_Heber
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(748, 531)
        Me.Controls.Add(Me.chkInternational)
        Me.Controls.Add(Me.chkCheckOnline)
        Me.Controls.Add(Me.lstFields)
        Me.Controls.Add(Me.dgvHeber)
        Me.Controls.Add(Me.btnImport)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.txtImportFile)
        Me.Controls.Add(Me.Label1)
        Me.DoubleBuffered = True
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(764, 570)
        Me.Name = "Import_Heber"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Athleten aus Excel importieren"
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents Label1 As Label
    Friend WithEvents txtImportFile As TextBox
    Friend WithEvents btnSearch As Button
    Friend WithEvents btnImport As Button
    Friend WithEvents dgvHeber As DataGridView
    Friend WithEvents Teilnehmer As DataGridViewTextBoxColumn
    Friend WithEvents Nachname As DataGridViewTextBoxColumn
    Friend WithEvents Vorname As DataGridViewTextBoxColumn
    Friend WithEvents Geschlecht As DataGridViewTextBoxColumn
    Friend WithEvents Jahrgang As DataGridViewTextBoxColumn
    Friend WithEvents lstFields As ListBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents chkCheckOnline As CheckBox
    Friend WithEvents chkInternational As CheckBox
End Class
