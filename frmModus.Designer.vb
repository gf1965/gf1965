﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmModus
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim BezeichnungLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmModus))
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboWertung = New System.Windows.Forms.ComboBox()
        Me.chkAthletik = New System.Windows.Forms.CheckBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtSteigerung2 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboEinteilung_Gewicht = New System.Windows.Forms.ComboBox()
        Me.cboEinteilung_Alter = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtSteigerung1 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtBezeichnung = New System.Windows.Forms.TextBox()
        Me.chkTechnikwertung = New System.Windows.Forms.CheckBox()
        Me.dgvModus = New System.Windows.Forms.DataGridView()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuNeu = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLöschen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLayoutModus = New System.Windows.Forms.ToolStripMenuItem()
        Me.bn = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.bnAddNew = New System.Windows.Forms.ToolStripButton()
        Me.bnCount = New System.Windows.Forms.ToolStripLabel()
        Me.bnDelete = New System.Windows.Forms.ToolStripButton()
        Me.bnMoveFirst = New System.Windows.Forms.ToolStripButton()
        Me.bnMovePrevious = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.bnPosition = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bnMoveNext = New System.Windows.Forms.ToolStripButton()
        Me.bnMoveLast = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.bnSave = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.chk5kg = New System.Windows.Forms.CheckBox()
        Me.opt15kg = New System.Windows.Forms.RadioButton()
        Me.chk7kg = New System.Windows.Forms.CheckBox()
        Me.opt20kg = New System.Windows.Forms.RadioButton()
        Me.optSex = New System.Windows.Forms.RadioButton()
        Me.lblMinAge = New System.Windows.Forms.Label()
        Me.nudMinAge = New System.Windows.Forms.NumericUpDown()
        Me.bs = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.chkRegel20 = New System.Windows.Forms.CheckBox()
        Me.tabDetails = New System.Windows.Forms.TabControl()
        Me.tabHantel = New System.Windows.Forms.TabPage()
        Me.optAK = New System.Windows.Forms.RadioButton()
        Me.txtHantelstange = New System.Windows.Forms.TextBox()
        Me.tabFormel = New System.Windows.Forms.TabPage()
        Me.tabGewichtsgruppen = New System.Windows.Forms.TabPage()
        Me.chkEqual_w = New System.Windows.Forms.CheckBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblG_w4 = New System.Windows.Forms.Label()
        Me.nudG_w = New System.Windows.Forms.NumericUpDown()
        Me.lblG_w3 = New System.Windows.Forms.Label()
        Me.nudG_w0 = New System.Windows.Forms.NumericUpDown()
        Me.lblG_w2 = New System.Windows.Forms.Label()
        Me.nudG_w1 = New System.Windows.Forms.NumericUpDown()
        Me.lblG_w0 = New System.Windows.Forms.Label()
        Me.lblG_w1 = New System.Windows.Forms.Label()
        Me.nudG_w2 = New System.Windows.Forms.NumericUpDown()
        Me.nudG_w3 = New System.Windows.Forms.NumericUpDown()
        Me.nudG_w4 = New System.Windows.Forms.NumericUpDown()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.nudEqual_w = New System.Windows.Forms.NumericUpDown()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.nudG_m0 = New System.Windows.Forms.NumericUpDown()
        Me.nudG_m = New System.Windows.Forms.NumericUpDown()
        Me.nudG_m1 = New System.Windows.Forms.NumericUpDown()
        Me.lblG_m4 = New System.Windows.Forms.Label()
        Me.nudG_m2 = New System.Windows.Forms.NumericUpDown()
        Me.lblG_m3 = New System.Windows.Forms.Label()
        Me.nudG_m3 = New System.Windows.Forms.NumericUpDown()
        Me.lblG_m2 = New System.Windows.Forms.Label()
        Me.nudG_m4 = New System.Windows.Forms.NumericUpDown()
        Me.lblG_m0 = New System.Windows.Forms.Label()
        Me.lblG_m1 = New System.Windows.Forms.Label()
        Me.nudEqual_m = New System.Windows.Forms.NumericUpDown()
        Me.chkEqual_m = New System.Windows.Forms.CheckBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.chkLosnummern = New System.Windows.Forms.CheckBox()
        BezeichnungLabel = New System.Windows.Forms.Label()
        CType(Me.dgvModus, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.bn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bn.SuspendLayout()
        CType(Me.nudMinAge, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabDetails.SuspendLayout()
        Me.tabHantel.SuspendLayout()
        Me.tabGewichtsgruppen.SuspendLayout()
        CType(Me.nudG_w, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_w0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_w1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_w2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_w3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_w4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudEqual_w, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudEqual_m, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BezeichnungLabel
        '
        BezeichnungLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        BezeichnungLabel.AutoSize = True
        BezeichnungLabel.Location = New System.Drawing.Point(37, 246)
        BezeichnungLabel.Name = "BezeichnungLabel"
        BezeichnungLabel.Size = New System.Drawing.Size(69, 13)
        BezeichnungLabel.TabIndex = 10
        BezeichnungLabel.Text = "&Bezeichnung"
        BezeichnungLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(58, 354)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 13)
        Me.Label10.TabIndex = 60
        Me.Label10.Text = "&Wertung"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboWertung
        '
        Me.cboWertung.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboWertung.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboWertung.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboWertung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWertung.FormattingEnabled = True
        Me.cboWertung.Location = New System.Drawing.Point(112, 351)
        Me.cboWertung.Name = "cboWertung"
        Me.cboWertung.Size = New System.Drawing.Size(130, 21)
        Me.cboWertung.TabIndex = 61
        '
        'chkAthletik
        '
        Me.chkAthletik.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkAthletik.AutoSize = True
        Me.chkAthletik.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkAthletik.Location = New System.Drawing.Point(182, 272)
        Me.chkAthletik.Name = "chkAthletik"
        Me.chkAthletik.Size = New System.Drawing.Size(61, 17)
        Me.chkAthletik.TabIndex = 31
        Me.chkAthletik.Text = "&Athletik"
        Me.chkAthletik.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(134, 409)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 13)
        Me.Label9.TabIndex = 74
        Me.Label9.Text = "nach 2. &Versuch"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(134, 383)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 13)
        Me.Label8.TabIndex = 72
        Me.Label8.Text = "nach 1. Versuch"
        '
        'txtSteigerung2
        '
        Me.txtSteigerung2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtSteigerung2.Location = New System.Drawing.Point(112, 406)
        Me.txtSteigerung2.Name = "txtSteigerung2"
        Me.txtSteigerung2.Size = New System.Drawing.Size(20, 20)
        Me.txtSteigerung2.TabIndex = 75
        Me.txtSteigerung2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 327)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(95, 13)
        Me.Label4.TabIndex = 50
        Me.Label4.Text = "Einteilung &Gewicht"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboEinteilung_Gewicht
        '
        Me.cboEinteilung_Gewicht.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboEinteilung_Gewicht.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEinteilung_Gewicht.FormattingEnabled = True
        Me.cboEinteilung_Gewicht.Items.AddRange(New Object() {"", "Gewichtsgruppen", "Gewichtsklassen"})
        Me.cboEinteilung_Gewicht.Location = New System.Drawing.Point(112, 324)
        Me.cboEinteilung_Gewicht.Name = "cboEinteilung_Gewicht"
        Me.cboEinteilung_Gewicht.Size = New System.Drawing.Size(130, 21)
        Me.cboEinteilung_Gewicht.TabIndex = 51
        '
        'cboEinteilung_Alter
        '
        Me.cboEinteilung_Alter.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboEinteilung_Alter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEinteilung_Alter.FormattingEnabled = True
        Me.cboEinteilung_Alter.Items.AddRange(New Object() {"", "Jahrgang", "Altersgruppen", "Altersklassen"})
        Me.cboEinteilung_Alter.Location = New System.Drawing.Point(112, 298)
        Me.cboEinteilung_Alter.Name = "cboEinteilung_Alter"
        Me.cboEinteilung_Alter.Size = New System.Drawing.Size(130, 21)
        Me.cboEinteilung_Alter.TabIndex = 41
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(29, 300)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 13)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "&Einteilung Alter"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtSteigerung1
        '
        Me.txtSteigerung1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtSteigerung1.Location = New System.Drawing.Point(112, 380)
        Me.txtSteigerung1.Name = "txtSteigerung1"
        Me.txtSteigerung1.Size = New System.Drawing.Size(20, 20)
        Me.txtSteigerung1.TabIndex = 71
        Me.txtSteigerung1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 383)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 13)
        Me.Label2.TabIndex = 70
        Me.Label2.Text = "&Steigerung in kg"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtBezeichnung
        '
        Me.txtBezeichnung.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtBezeichnung.Location = New System.Drawing.Point(112, 243)
        Me.txtBezeichnung.Name = "txtBezeichnung"
        Me.txtBezeichnung.Size = New System.Drawing.Size(304, 20)
        Me.txtBezeichnung.TabIndex = 11
        '
        'chkTechnikwertung
        '
        Me.chkTechnikwertung.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkTechnikwertung.AutoSize = True
        Me.chkTechnikwertung.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkTechnikwertung.Location = New System.Drawing.Point(24, 272)
        Me.chkTechnikwertung.Name = "chkTechnikwertung"
        Me.chkTechnikwertung.Size = New System.Drawing.Size(103, 17)
        Me.chkTechnikwertung.TabIndex = 21
        Me.chkTechnikwertung.Text = "&Technikwertung"
        Me.chkTechnikwertung.UseVisualStyleBackColor = True
        '
        'dgvModus
        '
        Me.dgvModus.AllowUserToAddRows = False
        Me.dgvModus.AllowUserToDeleteRows = False
        Me.dgvModus.AllowUserToResizeRows = False
        Me.dgvModus.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvModus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvModus.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvModus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvModus.Location = New System.Drawing.Point(0, 36)
        Me.dgvModus.MultiSelect = False
        Me.dgvModus.Name = "dgvModus"
        Me.dgvModus.ReadOnly = True
        Me.dgvModus.RowHeadersVisible = False
        Me.dgvModus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvModus.Size = New System.Drawing.Size(898, 185)
        Me.dgvModus.StandardTab = True
        Me.dgvModus.TabIndex = 0
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuOptionen})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(898, 24)
        Me.MenuStrip1.TabIndex = 158
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNeu, Me.mnuSpeichern, Me.mnuLöschen, Me.ToolStripMenuItem1, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 20)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuNeu
        '
        Me.mnuNeu.Name = "mnuNeu"
        Me.mnuNeu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuNeu.Size = New System.Drawing.Size(175, 22)
        Me.mnuNeu.Text = "&Neu"
        '
        'mnuSpeichern
        '
        Me.mnuSpeichern.Name = "mnuSpeichern"
        Me.mnuSpeichern.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSpeichern.Size = New System.Drawing.Size(175, 22)
        Me.mnuSpeichern.Text = "&Speichern "
        '
        'mnuLöschen
        '
        Me.mnuLöschen.Name = "mnuLöschen"
        Me.mnuLöschen.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Delete), System.Windows.Forms.Keys)
        Me.mnuLöschen.Size = New System.Drawing.Size(175, 22)
        Me.mnuLöschen.Text = "&Löschen"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(172, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(175, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLayoutModus})
        Me.mnuOptionen.Name = "mnuOptionen"
        Me.mnuOptionen.Size = New System.Drawing.Size(69, 20)
        Me.mnuOptionen.Text = "&Optionen"
        '
        'mnuLayoutModus
        '
        Me.mnuLayoutModus.Name = "mnuLayoutModus"
        Me.mnuLayoutModus.Size = New System.Drawing.Size(160, 22)
        Me.mnuLayoutModus.Text = "&Tabellen-Layout"
        '
        'bn
        '
        Me.bn.AddNewItem = Me.bnAddNew
        Me.bn.CountItem = Me.bnCount
        Me.bn.DeleteItem = Me.bnDelete
        Me.bn.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.bn.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bnMoveFirst, Me.bnMovePrevious, Me.BindingNavigatorSeparator, Me.bnPosition, Me.bnCount, Me.BindingNavigatorSeparator1, Me.bnMoveNext, Me.bnMoveLast, Me.BindingNavigatorSeparator2, Me.bnAddNew, Me.bnDelete, Me.bnSave, Me.ToolStripSeparator1})
        Me.bn.Location = New System.Drawing.Point(0, 475)
        Me.bn.MoveFirstItem = Me.bnMoveFirst
        Me.bn.MoveLastItem = Me.bnMoveLast
        Me.bn.MoveNextItem = Me.bnMoveNext
        Me.bn.MovePreviousItem = Me.bnMovePrevious
        Me.bn.Name = "bn"
        Me.bn.PositionItem = Me.bnPosition
        Me.bn.Size = New System.Drawing.Size(898, 25)
        Me.bn.TabIndex = 171
        Me.bn.Text = "BindingNavigator1"
        '
        'bnAddNew
        '
        Me.bnAddNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.bnAddNew.Image = CType(resources.GetObject("bnAddNew.Image"), System.Drawing.Image)
        Me.bnAddNew.Name = "bnAddNew"
        Me.bnAddNew.RightToLeftAutoMirrorImage = True
        Me.bnAddNew.Size = New System.Drawing.Size(23, 22)
        Me.bnAddNew.Text = "Neu hinzufügen"
        '
        'bnCount
        '
        Me.bnCount.Name = "bnCount"
        Me.bnCount.Size = New System.Drawing.Size(44, 22)
        Me.bnCount.Text = "von {0}"
        Me.bnCount.ToolTipText = "Die Gesamtanzahl der Elemente."
        '
        'bnDelete
        '
        Me.bnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.bnDelete.Image = CType(resources.GetObject("bnDelete.Image"), System.Drawing.Image)
        Me.bnDelete.Name = "bnDelete"
        Me.bnDelete.RightToLeftAutoMirrorImage = True
        Me.bnDelete.Size = New System.Drawing.Size(23, 22)
        Me.bnDelete.Text = "Löschen"
        '
        'bnMoveFirst
        '
        Me.bnMoveFirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.bnMoveFirst.Image = CType(resources.GetObject("bnMoveFirst.Image"), System.Drawing.Image)
        Me.bnMoveFirst.Name = "bnMoveFirst"
        Me.bnMoveFirst.RightToLeftAutoMirrorImage = True
        Me.bnMoveFirst.Size = New System.Drawing.Size(23, 22)
        Me.bnMoveFirst.Text = "Erste verschieben"
        '
        'bnMovePrevious
        '
        Me.bnMovePrevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.bnMovePrevious.Image = CType(resources.GetObject("bnMovePrevious.Image"), System.Drawing.Image)
        Me.bnMovePrevious.Name = "bnMovePrevious"
        Me.bnMovePrevious.RightToLeftAutoMirrorImage = True
        Me.bnMovePrevious.Size = New System.Drawing.Size(23, 22)
        Me.bnMovePrevious.Text = "Vorherige verschieben"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'bnPosition
        '
        Me.bnPosition.AccessibleName = "Position"
        Me.bnPosition.AutoSize = False
        Me.bnPosition.Name = "bnPosition"
        Me.bnPosition.Size = New System.Drawing.Size(50, 23)
        Me.bnPosition.Text = "0"
        Me.bnPosition.ToolTipText = "Aktuelle Position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'bnMoveNext
        '
        Me.bnMoveNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.bnMoveNext.Image = CType(resources.GetObject("bnMoveNext.Image"), System.Drawing.Image)
        Me.bnMoveNext.Name = "bnMoveNext"
        Me.bnMoveNext.RightToLeftAutoMirrorImage = True
        Me.bnMoveNext.Size = New System.Drawing.Size(23, 22)
        Me.bnMoveNext.Text = "Nächste verschieben"
        '
        'bnMoveLast
        '
        Me.bnMoveLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.bnMoveLast.Image = CType(resources.GetObject("bnMoveLast.Image"), System.Drawing.Image)
        Me.bnMoveLast.Name = "bnMoveLast"
        Me.bnMoveLast.RightToLeftAutoMirrorImage = True
        Me.bnMoveLast.Size = New System.Drawing.Size(23, 22)
        Me.bnMoveLast.Text = "Letzte verschieben"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'bnSave
        '
        Me.bnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.bnSave.Image = Global.Gewichtheben.My.Resources.Resources.saveHS
        Me.bnSave.Name = "bnSave"
        Me.bnSave.Size = New System.Drawing.Size(23, 22)
        Me.bnSave.Text = "Daten speichern"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'chk5kg
        '
        Me.chk5kg.AutoSize = True
        Me.chk5kg.Location = New System.Drawing.Point(163, 102)
        Me.chk5kg.Name = "chk5kg"
        Me.chk5kg.Size = New System.Drawing.Size(87, 17)
        Me.chk5kg.TabIndex = 54
        Me.chk5kg.Tag = "x"
        Me.chk5kg.Text = "optional 5 kg"
        Me.chk5kg.UseVisualStyleBackColor = True
        '
        'opt15kg
        '
        Me.opt15kg.AutoSize = True
        Me.opt15kg.Location = New System.Drawing.Point(26, 76)
        Me.opt15kg.Name = "opt15kg"
        Me.opt15kg.Size = New System.Drawing.Size(82, 17)
        Me.opt15kg.TabIndex = 52
        Me.opt15kg.Tag = "w"
        Me.opt15kg.Text = "immer 15 kg"
        Me.opt15kg.UseVisualStyleBackColor = True
        '
        'chk7kg
        '
        Me.chk7kg.AutoSize = True
        Me.chk7kg.Location = New System.Drawing.Point(163, 82)
        Me.chk7kg.Name = "chk7kg"
        Me.chk7kg.Size = New System.Drawing.Size(87, 17)
        Me.chk7kg.TabIndex = 53
        Me.chk7kg.Tag = "k"
        Me.chk7kg.Text = "optional 7 kg"
        Me.chk7kg.UseVisualStyleBackColor = True
        '
        'opt20kg
        '
        Me.opt20kg.AutoSize = True
        Me.opt20kg.Location = New System.Drawing.Point(26, 46)
        Me.opt20kg.Name = "opt20kg"
        Me.opt20kg.Size = New System.Drawing.Size(82, 17)
        Me.opt20kg.TabIndex = 51
        Me.opt20kg.Tag = "m"
        Me.opt20kg.Text = "immer 20 kg"
        Me.opt20kg.UseVisualStyleBackColor = True
        '
        'optSex
        '
        Me.optSex.AutoSize = True
        Me.optSex.Checked = True
        Me.optSex.Location = New System.Drawing.Point(26, 16)
        Me.optSex.Name = "optSex"
        Me.optSex.Size = New System.Drawing.Size(106, 17)
        Me.optSex.TabIndex = 50
        Me.optSex.TabStop = True
        Me.optSex.Tag = "s"
        Me.optSex.Text = "nach Geschlecht"
        Me.optSex.UseVisualStyleBackColor = True
        '
        'lblMinAge
        '
        Me.lblMinAge.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMinAge.AutoSize = True
        Me.lblMinAge.Location = New System.Drawing.Point(304, 300)
        Me.lblMinAge.Name = "lblMinAge"
        Me.lblMinAge.Size = New System.Drawing.Size(64, 13)
        Me.lblMinAge.TabIndex = 42
        Me.lblMinAge.Text = "&Mindestalter"
        '
        'nudMinAge
        '
        Me.nudMinAge.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.nudMinAge.Location = New System.Drawing.Point(373, 298)
        Me.nudMinAge.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nudMinAge.Name = "nudMinAge"
        Me.nudMinAge.Size = New System.Drawing.Size(43, 20)
        Me.nudMinAge.TabIndex = 43
        Me.nudMinAge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudMinAge.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(786, 402)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(94, 25)
        Me.btnSave.TabIndex = 172
        Me.btnSave.Text = "Speichern"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(786, 433)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(94, 25)
        Me.btnExit.TabIndex = 173
        Me.btnExit.Text = "Abbrechen"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'chkRegel20
        '
        Me.chkRegel20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkRegel20.AutoSize = True
        Me.chkRegel20.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkRegel20.Location = New System.Drawing.Point(279, 326)
        Me.chkRegel20.Name = "chkRegel20"
        Me.chkRegel20.Size = New System.Drawing.Size(137, 17)
        Me.chkRegel20.TabIndex = 150
        Me.chkRegel20.Text = "20 kg-Regel anwenden"
        Me.chkRegel20.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkRegel20.UseVisualStyleBackColor = True
        '
        'tabDetails
        '
        Me.tabDetails.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tabDetails.Controls.Add(Me.tabHantel)
        Me.tabDetails.Controls.Add(Me.tabFormel)
        Me.tabDetails.Controls.Add(Me.tabGewichtsgruppen)
        Me.tabDetails.HotTrack = True
        Me.tabDetails.Location = New System.Drawing.Point(451, 243)
        Me.tabDetails.MinimumSize = New System.Drawing.Size(298, 183)
        Me.tabDetails.Multiline = True
        Me.tabDetails.Name = "tabDetails"
        Me.tabDetails.SelectedIndex = 0
        Me.tabDetails.ShowToolTips = True
        Me.tabDetails.Size = New System.Drawing.Size(318, 215)
        Me.tabDetails.TabIndex = 174
        '
        'tabHantel
        '
        Me.tabHantel.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.tabHantel.Controls.Add(Me.optAK)
        Me.tabHantel.Controls.Add(Me.txtHantelstange)
        Me.tabHantel.Controls.Add(Me.chk5kg)
        Me.tabHantel.Controls.Add(Me.optSex)
        Me.tabHantel.Controls.Add(Me.opt15kg)
        Me.tabHantel.Controls.Add(Me.opt20kg)
        Me.tabHantel.Controls.Add(Me.chk7kg)
        Me.tabHantel.Location = New System.Drawing.Point(4, 22)
        Me.tabHantel.Name = "tabHantel"
        Me.tabHantel.Padding = New System.Windows.Forms.Padding(3)
        Me.tabHantel.Size = New System.Drawing.Size(310, 189)
        Me.tabHantel.TabIndex = 0
        Me.tabHantel.Text = "Hantelstange"
        '
        'optAK
        '
        Me.optAK.AutoSize = True
        Me.optAK.Location = New System.Drawing.Point(26, 107)
        Me.optAK.Name = "optAK"
        Me.optAK.Size = New System.Drawing.Size(108, 17)
        Me.optAK.TabIndex = 56
        Me.optAK.Tag = "a"
        Me.optAK.Text = "nach Altersklasse"
        Me.optAK.UseVisualStyleBackColor = True
        '
        'txtHantelstange
        '
        Me.txtHantelstange.Location = New System.Drawing.Point(114, 41)
        Me.txtHantelstange.Name = "txtHantelstange"
        Me.txtHantelstange.Size = New System.Drawing.Size(34, 20)
        Me.txtHantelstange.TabIndex = 55
        Me.txtHantelstange.Visible = False
        '
        'tabFormel
        '
        Me.tabFormel.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.tabFormel.Location = New System.Drawing.Point(4, 22)
        Me.tabFormel.Name = "tabFormel"
        Me.tabFormel.Padding = New System.Windows.Forms.Padding(3)
        Me.tabFormel.Size = New System.Drawing.Size(310, 189)
        Me.tabFormel.TabIndex = 1
        Me.tabFormel.Text = "Formeln"
        '
        'tabGewichtsgruppen
        '
        Me.tabGewichtsgruppen.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.tabGewichtsgruppen.Controls.Add(Me.chkEqual_w)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label24)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_w4)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_w3)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w0)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_w2)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w1)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_w0)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_w1)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w2)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w3)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w4)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label31)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudEqual_w)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label35)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label37)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m0)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m1)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_m4)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m2)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_m3)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m3)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_m2)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m4)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_m0)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_m1)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudEqual_m)
        Me.tabGewichtsgruppen.Controls.Add(Me.chkEqual_m)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label40)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label32)
        Me.tabGewichtsgruppen.Location = New System.Drawing.Point(4, 22)
        Me.tabGewichtsgruppen.Name = "tabGewichtsgruppen"
        Me.tabGewichtsgruppen.Padding = New System.Windows.Forms.Padding(3)
        Me.tabGewichtsgruppen.Size = New System.Drawing.Size(310, 189)
        Me.tabGewichtsgruppen.TabIndex = 2
        Me.tabGewichtsgruppen.Text = "Gewichtsgruppen"
        '
        'chkEqual_w
        '
        Me.chkEqual_w.AutoSize = True
        Me.chkEqual_w.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.chkEqual_w.Location = New System.Drawing.Point(174, 164)
        Me.chkEqual_w.Name = "chkEqual_w"
        Me.chkEqual_w.Size = New System.Drawing.Size(73, 17)
        Me.chkEqual_w.TabIndex = 51
        Me.chkEqual_w.Text = "alle gleich"
        Me.chkEqual_w.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label24.Location = New System.Drawing.Point(226, 11)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(33, 13)
        Me.Label24.TabIndex = 38
        Me.Label24.Text = "Teiler"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblG_w4
        '
        Me.lblG_w4.AutoSize = True
        Me.lblG_w4.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_w4.Location = New System.Drawing.Point(269, 133)
        Me.lblG_w4.Name = "lblG_w4"
        Me.lblG_w4.Size = New System.Drawing.Size(30, 13)
        Me.lblG_w4.TabIndex = 50
        Me.lblG_w4.Tag = ""
        Me.lblG_w4.Text = "5 x 7"
        '
        'nudG_w
        '
        Me.nudG_w.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w.Location = New System.Drawing.Point(168, 47)
        Me.nudG_w.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_w.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudG_w.Name = "nudG_w"
        Me.nudG_w.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w.TabIndex = 40
        Me.nudG_w.Tag = "5"
        Me.nudG_w.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'lblG_w3
        '
        Me.lblG_w3.AutoSize = True
        Me.lblG_w3.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_w3.Location = New System.Drawing.Point(269, 108)
        Me.lblG_w3.Name = "lblG_w3"
        Me.lblG_w3.Size = New System.Drawing.Size(30, 13)
        Me.lblG_w3.TabIndex = 48
        Me.lblG_w3.Tag = ""
        Me.lblG_w3.Text = "4 x 7"
        '
        'nudG_w0
        '
        Me.nudG_w0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w0.Location = New System.Drawing.Point(223, 31)
        Me.nudG_w0.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_w0.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudG_w0.Name = "nudG_w0"
        Me.nudG_w0.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w0.TabIndex = 41
        Me.nudG_w0.Tag = "13"
        Me.nudG_w0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w0.Value = New Decimal(New Integer() {13, 0, 0, 0})
        '
        'lblG_w2
        '
        Me.lblG_w2.AutoSize = True
        Me.lblG_w2.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_w2.Location = New System.Drawing.Point(269, 83)
        Me.lblG_w2.Name = "lblG_w2"
        Me.lblG_w2.Size = New System.Drawing.Size(30, 13)
        Me.lblG_w2.TabIndex = 46
        Me.lblG_w2.Tag = ""
        Me.lblG_w2.Text = "3 x 7"
        '
        'nudG_w1
        '
        Me.nudG_w1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w1.Increment = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudG_w1.Location = New System.Drawing.Point(223, 56)
        Me.nudG_w1.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_w1.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudG_w1.Name = "nudG_w1"
        Me.nudG_w1.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w1.TabIndex = 43
        Me.nudG_w1.Tag = "14"
        Me.nudG_w1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w1.Value = New Decimal(New Integer() {14, 0, 0, 0})
        '
        'lblG_w0
        '
        Me.lblG_w0.AutoSize = True
        Me.lblG_w0.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_w0.Location = New System.Drawing.Point(269, 33)
        Me.lblG_w0.Name = "lblG_w0"
        Me.lblG_w0.Size = New System.Drawing.Size(36, 13)
        Me.lblG_w0.TabIndex = 42
        Me.lblG_w0.Tag = ""
        Me.lblG_w0.Text = "1 x 13"
        '
        'lblG_w1
        '
        Me.lblG_w1.AutoSize = True
        Me.lblG_w1.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_w1.Location = New System.Drawing.Point(269, 58)
        Me.lblG_w1.Name = "lblG_w1"
        Me.lblG_w1.Size = New System.Drawing.Size(30, 13)
        Me.lblG_w1.TabIndex = 44
        Me.lblG_w1.Tag = ""
        Me.lblG_w1.Text = "2 x 7"
        '
        'nudG_w2
        '
        Me.nudG_w2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w2.Increment = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudG_w2.Location = New System.Drawing.Point(223, 81)
        Me.nudG_w2.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_w2.Minimum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudG_w2.Name = "nudG_w2"
        Me.nudG_w2.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w2.TabIndex = 45
        Me.nudG_w2.Tag = "21"
        Me.nudG_w2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w2.Value = New Decimal(New Integer() {21, 0, 0, 0})
        '
        'nudG_w3
        '
        Me.nudG_w3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w3.Increment = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nudG_w3.Location = New System.Drawing.Point(223, 106)
        Me.nudG_w3.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_w3.Minimum = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nudG_w3.Name = "nudG_w3"
        Me.nudG_w3.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w3.TabIndex = 47
        Me.nudG_w3.Tag = "28"
        Me.nudG_w3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w3.Value = New Decimal(New Integer() {28, 0, 0, 0})
        '
        'nudG_w4
        '
        Me.nudG_w4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w4.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_w4.Location = New System.Drawing.Point(223, 131)
        Me.nudG_w4.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_w4.Minimum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_w4.Name = "nudG_w4"
        Me.nudG_w4.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w4.TabIndex = 49
        Me.nudG_w4.Tag = "35"
        Me.nudG_w4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w4.Value = New Decimal(New Integer() {35, 0, 0, 0})
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.Color.Transparent
        Me.Label31.ForeColor = System.Drawing.Color.Firebrick
        Me.Label31.Location = New System.Drawing.Point(165, 11)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(45, 13)
        Me.Label31.TabIndex = 37
        Me.Label31.Text = "weiblich"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'nudEqual_w
        '
        Me.nudEqual_w.Enabled = False
        Me.nudEqual_w.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudEqual_w.Location = New System.Drawing.Point(250, 161)
        Me.nudEqual_w.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudEqual_w.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudEqual_w.Name = "nudEqual_w"
        Me.nudEqual_w.Size = New System.Drawing.Size(39, 20)
        Me.nudEqual_w.TabIndex = 52
        Me.nudEqual_w.Tag = "7"
        Me.nudEqual_w.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudEqual_w.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.Transparent
        Me.Label35.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label35.Location = New System.Drawing.Point(168, 30)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(39, 13)
        Me.Label35.TabIndex = 39
        Me.Label35.Text = "Anzahl"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.BackColor = System.Drawing.Color.Transparent
        Me.Label37.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label37.Location = New System.Drawing.Point(64, 11)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(33, 13)
        Me.Label37.TabIndex = 18
        Me.Label37.Text = "Teiler"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'nudG_m0
        '
        Me.nudG_m0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m0.Location = New System.Drawing.Point(64, 31)
        Me.nudG_m0.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_m0.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudG_m0.Name = "nudG_m0"
        Me.nudG_m0.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m0.TabIndex = 21
        Me.nudG_m0.Tag = "15"
        Me.nudG_m0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m0.Value = New Decimal(New Integer() {15, 0, 0, 0})
        '
        'nudG_m
        '
        Me.nudG_m.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m.Location = New System.Drawing.Point(9, 47)
        Me.nudG_m.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_m.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudG_m.Name = "nudG_m"
        Me.nudG_m.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m.TabIndex = 20
        Me.nudG_m.Tag = "5"
        Me.nudG_m.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'nudG_m1
        '
        Me.nudG_m1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m1.Increment = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudG_m1.Location = New System.Drawing.Point(64, 56)
        Me.nudG_m1.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_m1.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudG_m1.Name = "nudG_m1"
        Me.nudG_m1.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m1.TabIndex = 23
        Me.nudG_m1.Tag = "16"
        Me.nudG_m1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m1.Value = New Decimal(New Integer() {16, 0, 0, 0})
        '
        'lblG_m4
        '
        Me.lblG_m4.AutoSize = True
        Me.lblG_m4.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_m4.Location = New System.Drawing.Point(110, 133)
        Me.lblG_m4.Name = "lblG_m4"
        Me.lblG_m4.Size = New System.Drawing.Size(30, 13)
        Me.lblG_m4.TabIndex = 30
        Me.lblG_m4.Tag = ""
        Me.lblG_m4.Text = "5 x 9"
        '
        'nudG_m2
        '
        Me.nudG_m2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m2.Increment = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudG_m2.Location = New System.Drawing.Point(64, 81)
        Me.nudG_m2.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_m2.Minimum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudG_m2.Name = "nudG_m2"
        Me.nudG_m2.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m2.TabIndex = 25
        Me.nudG_m2.Tag = "27"
        Me.nudG_m2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m2.Value = New Decimal(New Integer() {27, 0, 0, 0})
        '
        'lblG_m3
        '
        Me.lblG_m3.AutoSize = True
        Me.lblG_m3.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_m3.Location = New System.Drawing.Point(110, 108)
        Me.lblG_m3.Name = "lblG_m3"
        Me.lblG_m3.Size = New System.Drawing.Size(30, 13)
        Me.lblG_m3.TabIndex = 28
        Me.lblG_m3.Tag = ""
        Me.lblG_m3.Text = "4 x 9"
        '
        'nudG_m3
        '
        Me.nudG_m3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m3.Increment = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nudG_m3.Location = New System.Drawing.Point(64, 106)
        Me.nudG_m3.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_m3.Minimum = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nudG_m3.Name = "nudG_m3"
        Me.nudG_m3.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m3.TabIndex = 27
        Me.nudG_m3.Tag = "36"
        Me.nudG_m3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m3.Value = New Decimal(New Integer() {36, 0, 0, 0})
        '
        'lblG_m2
        '
        Me.lblG_m2.AutoSize = True
        Me.lblG_m2.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_m2.Location = New System.Drawing.Point(110, 83)
        Me.lblG_m2.Name = "lblG_m2"
        Me.lblG_m2.Size = New System.Drawing.Size(30, 13)
        Me.lblG_m2.TabIndex = 26
        Me.lblG_m2.Tag = ""
        Me.lblG_m2.Text = "3 x 9"
        '
        'nudG_m4
        '
        Me.nudG_m4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m4.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_m4.Location = New System.Drawing.Point(64, 131)
        Me.nudG_m4.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_m4.Minimum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_m4.Name = "nudG_m4"
        Me.nudG_m4.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m4.TabIndex = 29
        Me.nudG_m4.Tag = "45"
        Me.nudG_m4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m4.Value = New Decimal(New Integer() {45, 0, 0, 0})
        '
        'lblG_m0
        '
        Me.lblG_m0.AutoSize = True
        Me.lblG_m0.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_m0.Location = New System.Drawing.Point(110, 33)
        Me.lblG_m0.Name = "lblG_m0"
        Me.lblG_m0.Size = New System.Drawing.Size(36, 13)
        Me.lblG_m0.TabIndex = 22
        Me.lblG_m0.Tag = ""
        Me.lblG_m0.Text = "1 x 15"
        '
        'lblG_m1
        '
        Me.lblG_m1.AutoSize = True
        Me.lblG_m1.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_m1.Location = New System.Drawing.Point(110, 58)
        Me.lblG_m1.Name = "lblG_m1"
        Me.lblG_m1.Size = New System.Drawing.Size(30, 13)
        Me.lblG_m1.TabIndex = 24
        Me.lblG_m1.Tag = ""
        Me.lblG_m1.Text = "2 x 8"
        '
        'nudEqual_m
        '
        Me.nudEqual_m.Enabled = False
        Me.nudEqual_m.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudEqual_m.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudEqual_m.Location = New System.Drawing.Point(91, 161)
        Me.nudEqual_m.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudEqual_m.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudEqual_m.Name = "nudEqual_m"
        Me.nudEqual_m.Size = New System.Drawing.Size(39, 20)
        Me.nudEqual_m.TabIndex = 32
        Me.nudEqual_m.Tag = "10"
        Me.nudEqual_m.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudEqual_m.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'chkEqual_m
        '
        Me.chkEqual_m.AutoSize = True
        Me.chkEqual_m.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEqual_m.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.chkEqual_m.Location = New System.Drawing.Point(15, 164)
        Me.chkEqual_m.Name = "chkEqual_m"
        Me.chkEqual_m.Size = New System.Drawing.Size(73, 17)
        Me.chkEqual_m.TabIndex = 31
        Me.chkEqual_m.Text = "alle gleich"
        Me.chkEqual_m.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkEqual_m.UseVisualStyleBackColor = True
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Transparent
        Me.Label40.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label40.Location = New System.Drawing.Point(9, 30)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(39, 13)
        Me.Label40.TabIndex = 19
        Me.Label40.Text = "Anzahl"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.Color.Transparent
        Me.Label32.ForeColor = System.Drawing.Color.Firebrick
        Me.Label32.Location = New System.Drawing.Point(6, 11)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(49, 13)
        Me.Label32.TabIndex = 17
        Me.Label32.Text = "männlich"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkLosnummern
        '
        Me.chkLosnummern.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkLosnummern.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLosnummern.Location = New System.Drawing.Point(279, 349)
        Me.chkLosnummern.Name = "chkLosnummern"
        Me.chkLosnummern.Size = New System.Drawing.Size(137, 35)
        Me.chkLosnummern.TabIndex = 175
        Me.chkLosnummern.Text = "automatische Losnummern-Vergabe"
        Me.chkLosnummern.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLosnummern.UseVisualStyleBackColor = True
        '
        'frmModus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(898, 500)
        Me.Controls.Add(Me.chkLosnummern)
        Me.Controls.Add(Me.tabDetails)
        Me.Controls.Add(Me.chkRegel20)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.nudMinAge)
        Me.Controls.Add(Me.lblMinAge)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.bn)
        Me.Controls.Add(Me.cboWertung)
        Me.Controls.Add(Me.chkAthletik)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.dgvModus)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.cboEinteilung_Gewicht)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.chkTechnikwertung)
        Me.Controls.Add(Me.txtSteigerung2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtBezeichnung)
        Me.Controls.Add(BezeichnungLabel)
        Me.Controls.Add(Me.cboEinteilung_Alter)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtSteigerung1)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(914, 538)
        Me.Name = "frmModus"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Stammdaten: Wettkampf-Modus"
        CType(Me.dgvModus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.bn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bn.ResumeLayout(False)
        Me.bn.PerformLayout()
        CType(Me.nudMinAge, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabDetails.ResumeLayout(False)
        Me.tabHantel.ResumeLayout(False)
        Me.tabHantel.PerformLayout()
        Me.tabGewichtsgruppen.ResumeLayout(False)
        Me.tabGewichtsgruppen.PerformLayout()
        CType(Me.nudG_w, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_w0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_w1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_w2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_w3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_w4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudEqual_w, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudEqual_m, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtSteigerung1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtBezeichnung As System.Windows.Forms.TextBox
    Friend WithEvents chkTechnikwertung As System.Windows.Forms.CheckBox
    Friend WithEvents dgvModus As System.Windows.Forms.DataGridView
    Friend WithEvents cboEinteilung_Gewicht As System.Windows.Forms.ComboBox
    Friend WithEvents cboEinteilung_Alter As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDatei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBeenden As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtSteigerung2 As System.Windows.Forms.TextBox
    Friend WithEvents chkAthletik As System.Windows.Forms.CheckBox
    Friend WithEvents mnuOptionen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLayoutModus As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeichern As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cboWertung As System.Windows.Forms.ComboBox
    Friend WithEvents bn As BindingNavigator
    Friend WithEvents bnAddNew As ToolStripButton
    Friend WithEvents bnCount As ToolStripLabel
    Friend WithEvents bnDelete As ToolStripButton
    Friend WithEvents bnMoveFirst As ToolStripButton
    Friend WithEvents bnMovePrevious As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents bnPosition As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents bnMoveNext As ToolStripButton
    Friend WithEvents bnMoveLast As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents bnSave As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents chk5kg As CheckBox
    Friend WithEvents chk7kg As CheckBox
    Friend WithEvents opt15kg As RadioButton
    Friend WithEvents opt20kg As RadioButton
    Friend WithEvents optSex As RadioButton
    Friend WithEvents mnuNeu As ToolStripMenuItem
    Friend WithEvents mnuLöschen As ToolStripMenuItem
    Friend WithEvents lblMinAge As Label
    Friend WithEvents nudMinAge As NumericUpDown
    Friend WithEvents bs As BindingSource
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents chkRegel20 As CheckBox
    Friend WithEvents tabDetails As TabControl
    Friend WithEvents tabHantel As TabPage
    Friend WithEvents tabFormel As TabPage
    Friend WithEvents txtHantelstange As TextBox
    Friend WithEvents optAK As RadioButton
    Friend WithEvents tabGewichtsgruppen As TabPage
    Friend WithEvents Label37 As Label
    Friend WithEvents nudG_m0 As NumericUpDown
    Friend WithEvents nudG_m As NumericUpDown
    Friend WithEvents nudG_m1 As NumericUpDown
    Friend WithEvents lblG_m4 As Label
    Friend WithEvents nudG_m2 As NumericUpDown
    Friend WithEvents lblG_m3 As Label
    Friend WithEvents nudG_m3 As NumericUpDown
    Friend WithEvents lblG_m2 As Label
    Friend WithEvents nudG_m4 As NumericUpDown
    Friend WithEvents lblG_m0 As Label
    Friend WithEvents lblG_m1 As Label
    Friend WithEvents nudEqual_m As NumericUpDown
    Friend WithEvents chkEqual_m As CheckBox
    Friend WithEvents Label40 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents chkEqual_w As CheckBox
    Friend WithEvents Label24 As Label
    Friend WithEvents lblG_w4 As Label
    Friend WithEvents nudG_w As NumericUpDown
    Friend WithEvents lblG_w3 As Label
    Friend WithEvents nudG_w0 As NumericUpDown
    Friend WithEvents lblG_w2 As Label
    Friend WithEvents nudG_w1 As NumericUpDown
    Friend WithEvents lblG_w0 As Label
    Friend WithEvents lblG_w1 As Label
    Friend WithEvents nudG_w2 As NumericUpDown
    Friend WithEvents nudG_w3 As NumericUpDown
    Friend WithEvents nudG_w4 As NumericUpDown
    Friend WithEvents Label31 As Label
    Friend WithEvents nudEqual_w As NumericUpDown
    Friend WithEvents Label35 As Label
    Friend WithEvents chkLosnummern As CheckBox
End Class
