﻿Imports MySqlConnector

Public Class Import_Meldeliste

    Dim dtImport As New DataTable
    Dim dtVerein As New DataTable
    Dim dtAthlet As New DataTable
    Dim dtMeldung As New DataTable
    Dim dtLand As New DataTable
    Dim dtFehler As New DataTable
    Dim bs As BindingSource
    Dim dvB As DataView
    Dim dv As DataView
    Dim Query As String
    Dim cmd As MySqlCommand
    Dim MySQLReader As MySqlDataReader
    Dim Last_Insert_Verein As Integer
    Dim Last_Insert_Athlet As Integer
    Dim loading As Boolean

    Private Sub Import_Meldeliste_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        Dispose()
    End Sub

    Private Sub Import_Meldeliste_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim col As DataColumn
        col = New DataColumn With {.ColumnName = "ID", .DataType = GetType(Integer)}
        dtFehler.Columns.Add(col)
        col = New DataColumn With {.ColumnName = "Verein", .DataType = GetType(String)}
        dtFehler.Columns.Add(col)
        col = New DataColumn With {.ColumnName = "Bundesland", .DataType = GetType(String)}
        dtFehler.Columns.Add(col)
        col = New DataColumn With {.ColumnName = "region", .DataType = GetType(String)}
        dtFehler.Columns.Add(col)
    End Sub

    Private Sub cboFile_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFile.SelectedIndexChanged

        pnlOptionen.Enabled = cboFile.SelectedIndex < cboFile.Items.Count - 1
        btnStart.Enabled = cboFile.SelectedIndex > -1

        If cboFile.SelectedIndex = cboFile.Items.Count - 1 Then
            Using conn As New MySqlConnection(User.ConnString)
                Try
                    conn.Open()
                    Query = "SELECT * FROM meldeliste_import;"
                    cmd = New MySqlCommand(Query, conn)
                    dtImport.Load(cmd.ExecuteReader)
                    Query = "SELECT * FROM verein ORDER BY Vereinsname;"
                    cmd = New MySqlCommand(Query, conn)
                    dtVerein.Load(cmd.ExecuteReader)
                    Query = "SELECT * FROM athleten ORDER BY Nachname, Vorname;"
                    cmd = New MySqlCommand(Query, conn)
                    dtAthlet.Load(cmd.ExecuteReader)
                    Query = "SELECT * FROM meldung WHERE Wettkampf = " & Wettkampf.ID & ";"
                    cmd = New MySqlCommand(Query, conn)
                    dtMeldung.Load(cmd.ExecuteReader)
                    Query = "SELECT * FROM laender;"
                    cmd = New MySqlCommand(Query, conn)
                    dtLand.Load(cmd.ExecuteReader)
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Meldelisten-Import", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End Try
            End Using
            dvB = New DataView(dtLand)
            bsB = New BindingSource With {.DataSource = dvB}
        Else

        End If
    End Sub

    Private Sub btnSearchFile_Click(sender As Object, e As EventArgs) Handles btnSearchFile.Click
        If dlgOpen.ShowDialog() = DialogResult.Cancel Then Exit Sub
    End Sub

    Private Function Import_Verein() As Boolean
        Dim dv As DataView = New DataView(dtImport)
        Dim dt As New DataTable
        dt = dv.ToTable(True, "Verein", "Bundesland")
        Dim vRow As DataRow() = Nothing ' für Verein
        Dim lRow As DataRow() = Nothing ' für Bundesländer
        Dim fRow As DataRow = Nothing ' für Fehler
        Dim test As String = Nothing
        Dim land As String = String.Empty
        Dim state As String = String.Empty

        prbVerein.Maximum = dt.Rows.Count
        prbVerein.Value = 0
        lblVerein.Text = ""
        grpVerein.Visible = True
        Dim i As Integer = 0
        Dim l As Integer = 0
        Dim ix As Integer = 0

        Try
            For Each row As DataRow In dt.Rows
                prbVerein.Value += 1
                vRow = dtVerein.Select("Vereinsname = '" + row("Verein").ToString + "'")
                'If vRow.Count = 0 Then
                '    'Verein nicht in DB => prüfe Schreibfehler
                '    test = row("Verein").ToString
                '    If test.Contains("  ") Then
                '        ' doppelte Leerzeichen entfernen
                '        test = Strings.Replace(test, "  ", " ")
                '    End If
                '    If test.Contains(".") Then
                '        If InStr(test, ".") <> InStr(test, "e.V.") Then
                '            If Not test.Contains(". ") Then
                '                ' fehlendes Leerzeichen nach "." hinzufügen
                '                test = Strings.Replace(test, ".", ". ")
                '            End If
                '        End If
                '    End If
                '    If test.Contains("-") Then
                '        If test.Contains("- ") Then
                '            ' Leerzeichen nach "_" entfernen
                '            test = Strings.Replace(test, "- ", "-")
                '        End If
                '    End If
                '    If test.Contains("-") Then
                '        If test.Contains(" -") Then
                '            ' Leerzeichen vor "_" entfernen
                '            test = Strings.Replace(test, " -", "-")
                '        End If
                '    End If
                '    ' nach Korrekur Verein erneut suchen
                '    vRow = dtVerein.Select("Vereinsname = '" + test + "'")
                If vRow.Count = 0 Then
                    ' Verein in DB anlegen
                    land = row("Bundesland").ToString
                    If Len(land) = 2 Then
                        state = "state_short"
                    ElseIf Len(land) = 3 Then
                        state = "state_old"
                    Else
                        state = "state_long"
                    End If
                    lRow = dtLand.Select(state + " = '" + land + "'")
                    If lRow.Count > 0 Then
                        land = lRow(0)("region").ToString
                        ' neuen Verein aus Meldung anlegen
                        Save_Verein(row("Verein").ToString, land)
                        i += 1
                        ' in dtMeldung: Verein(Name) mit Verein(idVerein) ersetzen
                        Exchange_Verein(row("Verein").ToString)
                        'ix = Exchange_Verein(row("Verein").ToString)
                        'If Not IsNothing(fRow) Then
                        '    fRow(0) = ix
                        '    dtFehler.Rows.Add(fRow)
                        '    fRow = Nothing
                        'End If
                    Else
                        'kein Bundesland gefunden
                        'Stop
                        land = "NULL"
                        l += 1
                        fRow = dtFehler.NewRow
                        fRow(1) = row("Verein").ToString
                        fRow(2) = row("Bundesland").ToString
                    End If
                    'Else
                    '    ' Verein nach Schreibfehler-Korrektur in DB vorhanden
                    '    row("Verein") = test
                    '    ' in dtMeldung: Verein(Name) mit Verein(idVerein) ersetzen
                    '    Exchange_Verein(row("Verein").ToString)
                    'End If
                Else
                    ' Verein in DB vorhanden
                    ' in dtMeldung: Verein(Name) mit Verein(idVerein) ersetzen
                    'Debug.Write("Vorhanden: " + row("Verein").ToString + ", " + vRow(0)("idVerein").ToString + vbNewLine)
                    Exchange_Verein(row("Verein").ToString, vRow(0)("idVerein").ToString, True)
                End If
            Next
            prbVerein.Value = dt.Rows.Count
            lblVerein.Text = i.ToString + " Vereine angelegt"
            If i = 1 Then lblVerein.Text = Replace(lblVerein.Text, "Vereine", "Verein")
            If l > 0 Then
                lblVerein.Text += " (" + l.ToString + " ohne Bundesland)"
                btnShowVerein.Visible = True
            End If
        Catch ex As Exception
            'Stop
            Return False
        End Try
        Return True
    End Function

    Private Sub Save_Verein(verein As String, land As String)
        'lblVerein.Text = "Verein <" + verein + "> anlegen."
        Query = "INSERT INTO verein (Vereinsname, Bundesland) VALUES('" & Trim(verein) & "', " & land & ");"
        'Debug.Write(Query + vbNewLine)
        Using conn As New MySqlConnection(User.ConnString)
            conn.Open()
            cmd = New MySqlCommand(Query, conn)
            MySQLReader = cmd.ExecuteReader
            MySQLReader.Close()
        End Using
    End Sub

    Private Function Exchange_Verein(verein As String, Optional newID As String = "", Optional exists As Boolean = False) As Integer
        Dim mRow As DataRow() = Nothing ' für Meldung
        'Dim newID As String = String.Empty
        Using conn As New MySqlConnection(User.ConnString)
            conn.Open()
            If Not exists Then
                Query = "SELECT LAST_INSERT_ID()"
                cmd = New MySqlCommand(Query, conn)
                MySQLReader = cmd.ExecuteReader
                MySQLReader.Read()
                newID = MySQLReader(0).ToString
            End If
            If MySQLReader.HasRows OrElse Val(newID) > 0 Then
                ' alle Rows aus dtMeldung mit dem gerade gespeicherten Vereinsnamen
                mRow = dtImport.Select("Verein = '" + verein + "'")
                For Each r As DataRow In mRow
                    'Debug.Write(r("Nachname").ToString + ", " + r("Vorname").ToString + ", " + r("verein").ToString + ", " + newID + vbNewLine)
                    r("Verein") = newID
                    'Debug.Write(r("verein").ToString + vbNewLine)
                Next
            End If
            MySQLReader.Close()
        End Using
        Return CInt(newID)
    End Function

    Private Sub btnShowVerein_Click(sender As Object, e As EventArgs) Handles btnShowVerein.Click

        Dim colB As DataGridViewColumn = Nothing
        Dim cb As DataGridViewComboBoxCell = Nothing

        With dgv
            .AutoGenerateColumns = False
            .AlternatingRowsDefaultCellStyle.BackColor = Color.OldLace
            .RowsDefaultCellStyle.BackColor = Color.LightCyan

            Dim ColWidth As New List(Of Integer)
            ColWidth.AddRange({30, 100, 40, 110, 35, 35})
            Dim ColBez As New List(Of String)
            ColBez.AddRange({"ID", "Verein", "BL", "Bundesland", "alt", "neu"})
            Dim ColRO As New List(Of Boolean)
            ColRO.AddRange({True, True, True, False, True, True})

            cb = New DataGridViewComboBoxCell With {.DataSource = bsB, .ValueMember = "region", .DisplayMember = "state_long", .FlatStyle = FlatStyle.Flat}
            colB = New DataGridViewColumn(cb) With {.Name = "Bundesland", .DataPropertyName = "state_long"}

            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "ID", .DataPropertyName = "ID"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Verein", .DataPropertyName = "Verein"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "BL", .DataPropertyName = "Bundesland"})
            .Columns.Add(colB)
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "BL_old", .DataPropertyName = "state_old"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "BL_new", .DataPropertyName = "state_short"})

            For i As Integer = 0 To ColWidth.Count - 1
                .Columns(i).Width = ColWidth(i)
                .Columns(i).HeaderText = ColBez(i)
                .Columns(i).ReadOnly = ColRO(i)
            Next

            .Columns("ID").Visible = False
            .Columns("Verein").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

            dv = New DataView(dtFehler)
            bs = New BindingSource With {.DataSource = dv}
            .DataSource = bs
        End With

        bsB.Sort = "state_long ASC"

        pnlBL.Left = 323
        pnlBL.Top = 137
        pnlBL.Visible = True

    End Sub

    Private Function Import_Athleten() As Boolean

        ' Geburtstag <--> Jahrgang


        prbHeber.Maximum = dtImport.Rows.Count
        prbHeber.Value = 0
        lblHeber.Text = ""
        grpHeber.Visible = True
        Dim str As String = String.Empty
        Dim dt As DateTime = Nothing
        Dim idTeilnehmer As String = String.Empty
        Dim i As Integer = 0

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                For Each row As DataRow In dtImport.Rows
                    prbHeber.Value += 1
                    Query = "SELECT * FROM athleten WHERE Nachname = '" & row("Nachname").ToString &
                        "' AND Vorname = '" & row("Vorname").ToString & "';"
                    cmd = New MySqlCommand(Query, conn)
                    MySQLReader = cmd.ExecuteReader
                    If MySQLReader.HasRows Then
                        MySQLReader.Read()
                        str = MySQLReader("Jahrgang").ToString
                        ' prüft Geburtstag in athleten 
                        If DateTime.TryParse(str, dt) = False Then
                            ' nimmt Geburtstag aus Import
                            str = row("Geburtstag").ToString
                            If DateTime.TryParse(str, dt) = False Then
                                ' Nur Geburtsjahr angegeben
                                str += "-1-1"
                                If DateTime.TryParse(str, dt) Then
                                    ' Geburtstag in athleten speichern
                                    Query = "UPDATE athleten SET Geburtstag = '" & Format(dt, "yyyy-MM-dd") & "' WHERE idTeilnehmer = " & MySQLReader("idTeilnehmer").ToString & ";"
                                    cmd = New MySqlCommand(Query, conn)
                                    MySQLReader = cmd.ExecuteReader
                                End If
                            End If
                        End If
                        MySQLReader.Close()
                    Else
                        MySQLReader.Close()
                        ' Athlet anlegen
                        Query = "INSERT INTO athleten (Nachname, Vorname, Geschlecht, Verein, Jahrgang) VALUES ('" &
                                Trim(row("Nachname").ToString) & "', '" & Trim(row("Vorname").ToString) & "', '" &
                                Trim(row("Geschlecht").ToString) & "', " & row("Verein").ToString
                        str = row("Geburtsdatum").ToString
                        If DateTime.TryParse(str, dt) Then
                            Query += ", '" & Format(dt, "yyyy-MM-dd") & "');"
                        Else
                            str += "-1-1"
                            If DateTime.TryParse(str, dt) Then
                                Query += ", '" & Format(dt, "yyyy-MM-dd") & "');"
                            Else
                                Query += ");"
                            End If
                        End If
                        cmd = New MySqlCommand(Query, conn)
                        MySQLReader = cmd.ExecuteReader
                        MySQLReader.Close()
                        ' idTeilnehmer in dtMeldung speichern
                        Query = "SELECT LAST_INSERT_ID()"
                        cmd = New MySqlCommand(Query, conn)
                        MySQLReader = cmd.ExecuteReader
                        MySQLReader.Read()
                        row("Teilnehmer") = MySQLReader(0)
                        MySQLReader.Close()
                        i += 1
                    End If
                Next
            Catch ex As Exception
            End Try
        End Using

        prbHeber.Value = dtImport.Rows.Count
        lblHeber.Text = i.ToString + " Heber angelegt"

        Return True

    End Function

    Private Function Import_Meldung() As Boolean
        prbMeldung.Maximum = dtImport.Rows.Count
        prbMeldung.Value = 0
        lblMeldung.Text = ""
        grpMeldung.Visible = True

        Dim str As String = String.Empty
        Dim dt As DateTime = Nothing
        Dim idTeilnehmer As String = String.Empty
        Dim i As Integer = 0

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                For Each row As DataRow In dtImport.Rows
                    prbMeldung.Value += 1
                    ' Abfrage enthält kein GEWICHT, dafür AK und GK / kein Reissen, Stossen, dafür ZKL
                    Query = "INSERT INTO meldung (Teilnehmer, Wettkampf, Datum, GK, Gruppe, Zweikampf) VALUES (" &
                            row("Teilnehmer").ToString & ", " & Wettkampf.ID & ", '" & Format(row("Meldedatum"), "yyyy-MM-dd") & "', '" &
                            row("GK").ToString & "', " & row("Gruppe").ToString & ", " & If(row("ZKL").ToString = "", "NULL", row("ZKL").ToString) & ");"
                    cmd = New MySqlCommand(Query, conn)
                    MySQLReader = cmd.ExecuteReader
                    MySQLReader.Close()
                    i += 1
                Next
            Catch ex As Exception
            End Try
        End Using

        prbMeldung.Value = dtImport.Rows.Count
        lblMeldung.Text = i.ToString + " Meldungen importiert"
        If i = 1 Then Replace(lblMeldung.Text, "Meldungen", "Meldung")
        Return True

    End Function

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        If cboFile.SelectedIndex = cboFile.Items.Count - 1 Then
            If Import_Verein() = False Then Stop
            If Import_Athleten() = False Then Stop
            If Import_Meldung() = False Then Stop
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Import erfolgreich abgeschlossen.", Wettkampf.Bezeichnung, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
            'Using conn As New MySqlConnection(User.ConnString)
            '    Try
            '        conn.Open()
            '        Query = "TRUNCATE meldeliste_import;"
            '        cmd = New MySqlCommand(Query, conn)
            '        MySQLReader = cmd.ExecuteReader
            '        MySQLReader.Close()
            '    Catch ex As Exception
            '    End Try
            'End Using
        Else
            With dlgOpen
                .Title = "Import-Datei öffnen"
                .Filter = "CSV-Datei (*.csv)|*.csv|Excel-Datei (*.xls;*.xlsx;*.xlsm)|*.xls;xlsx;xlsm|MySQL-Datei (*.sql)|*.sql|Alle Dateien (*.*)|*.*"
                .FilterIndex = cboFile.SelectedIndex
                .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            End With
        End If
    End Sub

    Private Sub btnSaveBL_Click(sender As Object, e As EventArgs) Handles btnSaveBL.Click
        Dim del As New List(Of Integer)
        Dim f As Integer = 0
        Dim r As Integer = 0
        loading = True
        For i As Integer = 0 To dv.Count - 1
            If Not String.IsNullOrEmpty(dv(i)("region").ToString) Then
                Save_Verein(dv(i)("Verein").ToString, dv(i)("region").ToString)
                Exchange_Verein(dv(i)("Verein").ToString)
                del.Add(i)
                r += 1
            Else
                f += 1
            End If
        Next
        For i As Integer = 0 To del.Count - 1
            dv.Delete(i)
        Next
        'dv(0)("region") = ""
        'dgv.DataSource = Nothing
        'dgv.DataSource = bs

        lblVerein.Text = r.ToString + " Vereine neu angelegt"
        If r = 1 Then lblVerein.Text = Replace(lblVerein.Text, "Vereine", "Verein")
        If f > 0 Then
            lblVerein.Text += " (" + f.ToString + " ohne Bundesland)"
        Else
            btnShowVerein.Visible = False
            pnlBL.Visible = False
        End If
        loading = False
    End Sub

    Private Sub dgv_CellLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellLeave
        If loading Then Exit Sub
        If dgv.Columns(e.ColumnIndex).Name = "Bundesland" Then
            dgv.Rows(e.RowIndex).Cells("BL_old").Value = dvB(bsB.Position)("state_old")
            dgv.Rows(e.RowIndex).Cells("BL_new").Value = dvB(bsB.Position)("state_short")
        End If
    End Sub

    Private Sub bsB_CurrentChanged(sender As Object, e As EventArgs) Handles bsB.CurrentChanged
        If loading Then Exit Sub
        dv(bs.Position)("region") = dvB(bsB.Position)("region")
    End Sub

    Private Sub btnCancelVerein_Click(sender As Object, e As EventArgs) Handles btnCancelVerein.Click
        btnShowVerein.Visible = False
        pnlBL.Visible = False
    End Sub
End Class