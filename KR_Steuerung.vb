﻿
Imports System.Math
Imports Microsoft.Win32

Module KR_Steuerung

    Public Event Return_Wertung(Wertung As Integer, Zeit As Date, Note As Double, Lampen As String, Korrektur As Boolean)
    'Public Event Werbung_VisibleChange(Value As Boolean)
    Public Event Progress_Change(Value As Integer, Visible As Boolean)
    'Public Event Msg_Change(Msg As String)
    Public Event JuryMsg_Change(Value As Boolean) ' für Form JuryEntscheid
    Public Event ForeColors_Change(Heber_Present As Boolean)
    Public Event Set_formHantelbeladung_btnShow_Enabled(Value As Boolean)
    Public Event Set_formLeader_btnNext_Enabled(Value As Boolean)
    Public Event Set_formEasyMode_btnStart_Enabled(Value As Boolean)
    Public Event Reset_WK(IsHeberPresent As Boolean)
    'Public Event Clear_Hantel()

    Private t_Wertung As Integer
    Private t_Note As Double
    Private t_Lampen As String
    Private t_Korrektur As Boolean

    Private Delegate Sub DelegateSub()
    Private Sub ReturnToWertung()
        ' Event in eigenem Thread aufrufen
        If formLeader.InvokeRequired Then
            Dim d As New DelegateSub(AddressOf ReturnToWertung)
            formLeader.Invoke(d, New Object() {})
        Else
            RaiseEvent Return_Wertung(t_Wertung, Now(), t_Note, t_Lampen, t_Korrektur)
        End If
    End Sub
    Enum LineType
        Empty
        Techniknote
        Bestätigt
    End Enum
    Enum ID
        v0
        KR1
        KR2
        KR3
        ZN
        v5
        v6
        v7
        v8
        v9
        ZA
        JB
    End Enum

    Public Sub Change_formLeader_btnNext_Enabled(Value As Boolean)
        RaiseEvent Set_formLeader_btnNext_Enabled(Value)
    End Sub
    Public Sub WK_Reset(Optional IsHeberPresent As Boolean = True)
        RaiseEvent Reset_WK(IsHeberPresent)
    End Sub
    Public Sub Change_Progress(Value As Integer, Optional Visible As Boolean = True)
        RaiseEvent Progress_Change(Value, Visible)
    End Sub

    'Public Function Change_KR(KR_Count As Integer, Optional ShowMessage As Boolean = True) As Integer
    '    ' prüft, ob die Anzahl der KRs entsprechend des Parameters geändert werden kann

    '    Dim msg = String.Empty

    '    Dim Pad_Connected = (From pad In KR_Pad
    '                         Where pad.Value.Connected).ToList

    '    If Pad_Connected.Count = 0 Then
    '        msg = "Es sind keine KR-Pads angeschlossen."
    '    ElseIf Pad_Connected.Count < KR_Count Then
    '        Dim Pad_NotConnected As New List(Of String)
    '        For i = ID.KR1 To ID.KR3
    '            If Not KR_Pad(i).Connected Then Pad_NotConnected.Add(i.ToString)
    '        Next
    '        msg = If(Pad_Connected.Count = 1, "Das", "Die") & " KR-Pad" & If(Pad_Connected.Count = 1, " ", "s ") & Join(Pad_NotConnected.ToArray, " und ") & If(Pad_Connected.Count = 1, " ist", " sind") & " nicht angeschlossen."
    '    End If

    '    If Not String.IsNullOrEmpty(msg) Then
    '        If ShowMessage Then
    '            Using New Centered_MessageBox(formLeader)
    '                MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '            End Using
    '        End If
    '        Return 0
    '    End If

    '    Return 1

    'End Function

#Region "Declarations"
    Enum Score
        Not_Set
        Incomplete
        Complete
    End Enum

    Const P_Light_ON As Byte = 15
    Const P_Light_OFF As Byte = 14
    Const P_Clear_Display As Byte = 12  ' Display löschen
    Const P_CLEAR_Line As Byte = 4      ' Button CLEAR löscht ganze Zeile
    Const P_CLEAR_Char As Byte = 3      ' Button CLEAR löscht letztes Zeichen
    Const P_SecondLine_PC As Byte = 2   ' 2.Zeile wird vom PC geschrieben
    Const P_SecondLine_Pad As Byte = 1  ' 2.Zeile wird vom Pad geschrieben
    Const P_SelLine_1 As Byte = 13      ' 1.Zeile auswählen und löschen
    Const P_SelLine_2 As Byte = 10      ' 2.Zeile auswählen und löschen
    Const P_Beep As Byte = 7
    Const P_ü As Byte = 245             ' Zeichen "ü"
    Const P_ä As Byte = 225             ' Zeichen "ä"
    Const JB_Off As Byte = 128
    Const JB_Rev As Byte = 1
    Private JB_Key As Byte() = {0, 2, 4, 8}
    Const ZA_Colon As Byte = &H20
    Const ZA_Light As Byte = &H40       ' Ab-Zeichen (akustisch + visuell)
    Const ZA_Alert As Byte = &H80       ' Hupe der Uhr 

    Public KR_Pad As Dictionary(Of Integer, myPad)
    Public ZN_Pad As myTPad
    Public JB_Dev As myJB
    Public ZA_Dev As myZA
    Public Ports As Dictionary(Of Integer, SerialPort)

    Public Steuerung As New mySteuerung

    'Private LockObject As New Object
    Private ScoreComplete As Integer ' KR-Wertung: Not_Set | Incomplete | Complete
    Private KR_Fail As Integer 'Index des Pads, das einen Piep erhält
    Private JB_Enabled As Boolean '   wird bei Durchgang_beendet (Initiate_Bridge) FALSE, bei Wertung_begonnen TRUE

    Private timerKR As Threading.Timer          ' unterbrechendes Signal für KR
    Private timerDelay As Threading.Timer
    Private timerAbzeichen As Threading.Timer
    Private timerAufruf As Threading.Timer
    Private timerHupe As Threading.Timer

    Private timerDelay_Running As Boolean       ' Flag für Delay der Wertungsanzeige
    Private timerAbzeichen_Running As Boolean   ' Flag für Ab-Zeichen
    Public timerAufruf_Running As Boolean       ' Flag für CountDown des Aufrufes
    Private timerHupe_Running As Boolean        ' Flag für Zeit-Hupe
    Class myTN
        Public Result As Double
        Public KR_Note As New Dictionary(Of Integer, Double)
        Public KR_invalid As Integer
        Public ShowThis As Boolean
        Public Sub New()
            Result = 0
            KR_Note.Clear()
            KR_invalid = 0
            ShowThis = False
        End Sub
    End Class
    Public Technikwertung As myTN
#End Region

    Class KR_Pad_Proceed
        Implements IDisposable

        Public Sub KR_Proceed(KR_Ix As Integer, KR_Data As String)
            Dim Korrektur As Boolean = False

            'SyncLock LockObject
            Try
                With KR_Pad(KR_Ix)
                    For Each item As Char In KR_Data
                        Select Case item

                            Case Convert.ToChar(10) ' ENTER
#Region "Techniknote bestätigen (Verify)"
                                If Wettkampf.Technikwertung AndAlso .Wertung = 5 AndAlso Not .Verified Then
                                    .Verified = True
                                    .Techniknote = .Techniknote.Substring(1)
                                    Leader.Change_Techniknote(KR_Ix, .Techniknote)  ' Aufruf des Events für Leader, um TN an Jury zu senden
                                    If JB_Dev.Connected Then Steuerung.JB_Write()
                                    Using wp As New Write_Pad
                                        wp.Technik_Display(KR_Ix, , LineType.Bestätigt)
                                    End Using
                                End If
#End Region
                            Case Convert.ToChar(vbBack), Convert.ToChar(13) ' CLEAR
#Region "Techniknote löschen (Clear)"
                                If Wettkampf.Technikwertung AndAlso .Wertung = 5 Then
                                    .Verified = False
                                    .Techniknote = "0"
                                    If JB_Dev.Connected Then Steuerung.JB_Write()
                                    Leader.Change_Techniknote(KR_Ix, .Techniknote) ' Aufruf des Events für Leader, um TN an Jury zu senden
                                    Technikwertung = New myTN
                                    Using wp As New Write_Pad
                                        wp.Technik_Display(KR_Ix, , LineType.Techniknote)
                                    End Using
                                End If
                                Return
#End Region
                            Case CChar("G") ' Gültig
#Region "Gültig"
                                If .Wertung = -1 Then
                                    ' Wertung von JuryBoard gesetzt 
                                    Return
                                ElseIf .Wertung = 5 Then
                                    ' gleiche Wertung nochmal
                                    If .Connected Then
                                        ' 2.Zeile "Gültig" löschen
                                        With Ports(KR_Ix)
                                            .Write({P_SelLine_2}, 0, 1)
                                        End With
                                    End If
                                    Return
                                ElseIf .Wertung = 7 AndAlso ScoreComplete > Score.Not_Set AndAlso Not timerDelay_Running Then
                                    ' nachträgliche Änderung in GÜLTIG
                                    Korrektur = True
                                    If WK_Options.Overwrite Then
                                        ' nachträglich GÜLTIG möglich
                                        'Anzeige.Change_Lampe(KR_Ix, Anzeige.ScoreColors(5)) ' Event für Anzeige der Lampen 
                                        .Verified = False
                                    ElseIf Not WK_Options.Overwrite Then
                                        ' nachträglich GÜLTIG ausgeschaltet
                                        Return
                                    End If
                                End If

                                .Wertung = 5
                                Anzeige.Change_Lampe_Sofort(KR_Ix, Anzeige.ScoreColors(5)) ' Wertung in Bohle und Moderator sofort anzeigen
                                Using wp As New Write_Pad
                                    wp.Write_Gültig(KR_Ix)
                                End Using

#End Region
                            Case CChar("U") ' Ungültig
#Region "Ungültig"
                                If .Wertung = -1 OrElse .Wertung = 7 Then
                                    ' Wertung von JuryBoard gesetzt ODER gleiche Wertung nochmal
                                    Return
                                ElseIf .Wertung = 5 AndAlso ScoreComplete > Score.Not_Set AndAlso Not timerDelay_Running Then
                                    ' nachträgliche Änderung in UNGÜLTIG
                                    Korrektur = True
                                    'Anzeige.Change_Lampe(KR_Ix, Anzeige.ScoreColors(7)) ' Event für Anzeige der Lampen 
                                End If

                                .Wertung = 7
                                Anzeige.Change_Lampe_Sofort(KR_Ix, Anzeige.ScoreColors(.Wertung)) ' Wertung in Bohle und Moderator sofort anzeigen
                                Using wp As New Write_Pad
                                    wp.Write_Ungültig(KR_Ix)
                                End Using

                                If Wettkampf.Technikwertung Then
                                    .Techniknote = "0"
                                    .Verified = True
                                End If
#End Region
                            Case CChar("P") ' blauer Taster = Reset
#Region "Blaue Taste"
                                If (Wettkampf.WK_Modus <> Modus.Normal OrElse Heber.Id = 0) OrElse Not JB_Enabled Then
                                    Steuerung.KR_BlaueTaste()
                                End If
                                Return
#End Region
                            Case CChar("0"), CChar("1"), CChar("2"), CChar("3"), CChar("4"), CChar("5"), CChar("6"), CChar("7"), CChar("8"), CChar("9"), CChar(",") 'Zahlen
#Region "Ziffern"
                                If Not Wettkampf.Technikwertung OrElse Not .Wertung = 5 Then
                                    If .Connected Then Ports(KR_Ix).Write({P_SelLine_2}, 0, 1)
                                    Return
                                End If

                                ' Techniknote ist ein String mit einer führenden "0", da die Wertung an TN = "0" angehängt wird
                                If Not .Verified AndAlso CDbl(.Techniknote) < 10 AndAlso Len(.Techniknote) < 4 Then
                                    .Techniknote += item
                                    If item.ToString.Equals(",") Then Return
                                    If Len(.Techniknote) > 3 Then
                                        If WK_Options.Decimals = 1 Then
                                            ' nur x,5 und x,0 zulässing
                                            If CDbl(.Techniknote) Mod 0.5 <> 0 Then
                                                ' Techniknote endet nicht auf 0 oder 5 --> Vorkomma-Stelle wird ins Pad geschrieben
                                                .Techniknote = "0" + Int(.Techniknote).ToString
                                                Using wp As New Write_Pad
                                                    wp.Technik_Display(KR_Ix, .Techniknote.ToString.Substring(1))
                                                End Using
                                                Ports(KR_Ix).Write({P_Beep}, 0, 1)
                                            End If
                                        End If
                                    End If
                                ElseIf .Connected Then
                                    Ports(KR_Ix).Write({P_Beep}, 0, 1)
                                    Using wp As New Write_Pad
                                        wp.Technik_Display(KR_Ix, .Techniknote.ToString.Substring(1))
                                    End Using
                                    Return
                                End If
#End Region
                        End Select
                    Next
                End With
            Catch ex As Exception
                LogMessage("KR_Steuerung: KR_Proceed: " & ex.Message, ex.StackTrace)
            End Try
            'End SyncLock

            Wertung_Proceed(KR_Ix, KR_Data = "G" OrElse KR_Data = "U", Korrektur)

        End Sub

        Sub Wertung_Proceed(KR_Ix As Integer, Scoring As Boolean, Korrektur As Boolean)
            ' wird nach jeder Pad-Eingabe aufgerufen
            ' Scoring = True    --> eine Wertung eingegben
            ' Scoring = False   --> eine Techniknote eingegeben/bestätigt/gelöscht
            Try
#Region "Meldung an Jury"
                If JB_Dev.Connected Then
                    JB_Enabled = True
                    Steuerung.JB_Write()
                End If
#End Region
#Region "Wertung von säumigem KR"
                If KR_Fail > 0 AndAlso KR_Pad(KR_Fail).Wertung > 0 Then
                    ' säumiger Kampfrichter hat gewertet
                    If Not IsNothing(timerKR) Then timerKR.Dispose()
                    KR_Fail = 0
                End If
#End Region
                Dim _wertung = Wertung() ' (0) = kumulierte Pad-Wertung, (1) = Heber.Wertung (1 | -1)

                If Scoring Then
                    If Heber.Id = 0 OrElse Heber.Wertung <> _wertung(1) Then Technikwertung = New myTN
#Region "ZN stoppen, falls Countdown noch läuft"
                    If timerAufruf_Running Then Leader.Change_Aufruf(False, "Aufruf gestoppt")
                    ZN_Pad.IsAttemptRated = True
#End Region
                    Select Case _wertung(0)
                        Case 5, 7
#Region "1 Kampfrichter"
                            If Wettkampf.KR(User.Bohle) = 1 Then
                                If ScoreComplete = Score.Not_Set Then
                                    Steuerung.Set_Abzeichen()                               ' Ab-Zeichen auslösen
                                    Set_Scoring_Delay()                                     ' Wertung verzögert anzeigen und an Leader zurückgeben
                                    ScoreComplete = Score.Complete
                                ElseIf ScoreComplete = Score.Complete Then                  ' nachträgliche Wertung
                                    ' Wertung anzeigen und zum Speichern an Leader zurückgeben
                                    If Not timerDelay_Running Then Set_Wertung(_wertung, Not timerDelay_Running, Korrektur)
                                End If
                            End If
#End Region
                        Case 15, 21, 17, 19     ' Wertung vollständig
#Region "3 Kampfrichter"
                            If ScoreComplete = Score.Not_Set Then Steuerung.Set_Abzeichen() ' bei erster vollständiger Wertung --> Ab-Zeichen auslösen
                            If ScoreComplete < Score.Complete Then
                                ScoreComplete = Score.Complete
                                Set_Scoring_Delay()                                         ' Wertung verzögert anzeigen und an Leader zurückgeben
                            ElseIf ScoreComplete = Score.Complete Then                      ' nachträgliche Wertung
                                ' Wertung anzeigen und zum Speichern an Leader zurückgeben
                                If Not timerDelay_Running Then Set_Wertung(_wertung, Not timerDelay_Running, Korrektur)
                            End If

                        Case 10, 14             ' 2x Gültig oder 2x Ungültig
                            ScoreComplete = Score.Incomplete
                            Steuerung.Set_Abzeichen()                                       ' Ab-Zeichen auslösen
                            KR_Fail = Get_KR_Failed(True)                                   ' fehlendes KR-Pad bestimmen und Msg senden
                            'Set_Scoring_Delay()                                             ' Regelwerk: Anzeige bei 2 gleichen Wertungen???????????????????????????

                        Case 12                 ' 1x Gültig und 1x Ungültig 
                            KR_Fail = Get_KR_Failed(False)                                  ' fehlendes KR-Pad bestimmen und Msg senden
#End Region
                    End Select
                Else
#Region "Technik-Note"
                    If Wettkampf.Technikwertung Then
                        With Technikwertung
                            If Wettkampf.KR(User.Bohle) = 1 AndAlso KR_Pad(KR_Ix).Verified Then
                                .Result = Floor(CDbl(KR_Pad(KR_Ix).Techniknote) * 100) / 100
                                If ScoreComplete = Score.Complete AndAlso Not timerDelay_Running Then   ' Wertung wird angezeigt --> Techniknote kann angezeigt werden
                                    Set_Wertung(_wertung, Not timerDelay_Running, Korrektur)            ' Wertung = 0 --> nur TN und Results ändern und speichern
                                End If
                            ElseIf KR_Pad(1).Verified And KR_Pad(2).Verified And KR_Pad(3).Verified Then
                                .Result = Get_Techniknote()
                                Set_OutOfRange()
                                If ScoreComplete = Score.Complete AndAlso Not timerDelay_Running Then   ' Wertung wird angezeigt --> Techniknote kann angezeigt werden
                                    Set_Wertung(_wertung, Not timerDelay_Running, Korrektur)            ' Wertung = 0 --> nur TN und Results ändern und speichern
                                End If
                            End If
                        End With
                    End If
#End Region

                End If
            Catch ex As Exception
                LogMessage("KR_Steuerung: Wertung_Proceed: " & ex.Message, ex.StackTrace)
            End Try
        End Sub

        Sub Set_Wertung(Wertung() As Integer, Optional ShowWertung As Boolean = False, Optional Korrektur As Boolean = False)
            ' Versuch beendet oder nachträglich gewertet
            Dim Lampen As String = String.Empty

            'wenn noch eine Wertung fehlt und kein Jury-Entscheid ansteht, wird die Wertung nicht angezeigt
            If Not JB_Dev.RevPressed AndAlso Wettkampf.KR(User.Bohle) = 3 Then
                If (From pad In KR_Pad Where pad.Value.Wertung = 0).Count > 0 Then Return
            End If

            For i = 1 To 3
                If KR_Pad.ContainsKey(i) Then
                    If ShowWertung Then
                        Anzeige.Change_Lampe(i, Anzeige.ScoreColors(KR_Pad(i).Wertung)) ' Lampe in Bohle, Aufwärmen
                    End If
                    Lampen += KR_Pad(i).Wertung.ToString
                Else
                    Lampen += "0"
                End If
            Next

            ' /* wird bei Technik-Eingabe und Wechsel in der Wertung ausgeführt
            If Wettkampf.Technikwertung AndAlso Technikwertung.Result <> Heber.T_Note Then
                Using f As New myFunction
                    Anzeige.T_Wert = f.Format_Techniknote(Technikwertung.Result.ToString)
                End Using
                Anzeige.T_Farbe = Anzeige.TechnikColors(Heber.Wertung) 'Anzeige.TechnikColors(Abs(CInt(Not Anzeige.T_Wert.Contains("X"))))
                Anzeige.T_Shown = True
            End If
            ' */
            If Heber.Id > 0 AndAlso Korrektur OrElse (Heber.Wertung <> Wertung(1) OrElse (Wettkampf.Technikwertung AndAlso Heber.T_Note <> Technikwertung.Result)) Then

                ' Event direkt aufrufen --> scheint langsamer zu sein
                'RaiseEvent Return_Wertung(Wertung, Now(), Technikwertung.Result, Lampen, False)

                ' Event in eigenem Thread aufrufen
                t_Wertung = Wertung(1)
                t_Note = Technikwertung.Result
                t_Lampen = Lampen
                t_Korrektur = Korrektur
                Dim t As New Threading.Thread(New Threading.ThreadStart(AddressOf ReturnToWertung))
                t.Priority = Threading.ThreadPriority.Normal
                t.Start()
            End If
        End Sub

        Private Sub Set_Scoring_Delay()  ' Verzögerung für => Set_Wertung starten
            timerDelay_Running = True
            timerDelay = New Threading.Timer(AddressOf Steuerung.timerDelay_Tick)
            timerDelay.Change(0, Threading.Timeout.Infinite)
        End Sub
        Private Function Get_KR_Failed(AutoReset As Boolean) As Integer

            Dim Ix As Integer

            If Not IsNothing(timerKR) Then timerKR.Dispose() ' Signal an Pad ausschalten

            For Ix = 1 To 3
                If KR_Pad(Ix).Wertung = 0 Then Exit For
            Next

            Try
                If KR_Pad(Ix).Connected Then
                    Try
                        With Ports(Ix)
                            .Write({P_SelLine_1}, 0, 1)
                            .Write("Wertung ?!")
                        End With
                    Catch ex As Exception
                    End Try

                    timerKR = New Threading.Timer(AddressOf Steuerung.TimerKR_Tick)
                    If AutoReset Then
                        ' einmaliges Signal an Pad
                        timerKR.Change(1000, 1000)
                    Else
                        ' unterbrechendes Signal an Pad
                        timerKR.Change(1000, Threading.Timeout.Infinite)
                    End If
                End If
            Catch ex As Exception
                Return 0
            End Try

            Return Ix

        End Function
        Private Function Get_Techniknote() As Double

            For i = 1 To 3
                JB_Dev.KR_Pressed(i) = False

                If KR_Pad.ContainsKey(i) Then
                    If Not IsNumeric(KR_Pad(i).Techniknote) Then KR_Pad(i).Techniknote = "0"
                    Dim tn = CDbl(KR_Pad(i).Techniknote)
                    If tn > 0 Then
                        Technikwertung.KR_Note(i) = tn
                        Technikwertung.Result += tn
                    Else
                        If Technikwertung.KR_invalid > 0 Then Return 0 ' mehr als eine Wertung ungültig
                        Technikwertung.KR_invalid = i                  ' eine Wertung ungültig
                    End If
                End If
            Next

            Return Floor(Technikwertung.Result / (3 - Abs(CInt(Technikwertung.KR_invalid > 0))) * 100) / 100
        End Function
        Private Sub Set_OutOfRange()
            Try
                Dim Taster = OutOfRange(Technikwertung.KR_invalid, Technikwertung.KR_Note)
                With JB_Dev
                    Select Case Taster
                        Case 0
                            For i = 1 To 3
                                .KR_Pressed(i) = False
                            Next
                        Case 2, 6, 10, 14
                            .KR_Pressed(1) = True
                            If Taster > 2 And Taster < 7 Then .KR_Pressed(2) = True
                            If Taster > 6 Then .KR_Pressed(3) = True
                        Case 4, 12
                            .KR_Pressed(2) = True
                            If Taster = 12 Then .KR_Pressed(3) = True
                        Case 8
                            .KR_Pressed(3) = True
                    End Select
                    Try
                        Leader.Change_OutOfRange(.KR_Pressed(1), .KR_Pressed(2), .KR_Pressed(3)) ' an Raspberry
                        If .Connected Then Steuerung.JB_Write(Taster)                                      ' an ControlUnit   
                    Catch ex As Exception
                    End Try
                End With
            Catch ex As Exception
                LogMessage("KR_Steuerung: Set_OutOfRange: " & ex.Message, ex.StackTrace)
            End Try
        End Sub
        Private Function OutOfRange(ungültig As Integer, value As Dictionary(Of Integer, Double)) As Integer
            Dim check As Integer() = {0, 0, 0}
            If value.Count < 2 Then Return 0 ' keine Wertung
            Try
                Select Case ungültig
                    Case 1
                        If Abs(value(2) - value(3)) > WK_Options.LimitTechniknote Then check = {0, JB_Key(2), JB_Key(3)}
                    Case 2
                        If Abs(value(1) - value(3)) > WK_Options.LimitTechniknote Then check = {JB_Key(1), 0, JB_Key(3)}
                    Case 3
                        If Abs(value(1) - value(2)) > WK_Options.LimitTechniknote Then check = {JB_Key(1), JB_Key(2), 0}
                    Case 0 ' alle gültig gewertet
                        If Abs(value(1) - value(2)) > WK_Options.LimitTechniknote Then
                            ' 1 = False
                            If Abs(value(2) - value(3)) > WK_Options.LimitTechniknote Then
                                ' 2 = False
                                If Abs(value(1) - value(3)) > WK_Options.LimitTechniknote Then
                                    ' 3 = False
                                    check = {JB_Key(1), JB_Key(2), JB_Key(3)}
                                Else
                                    ' 3 = True
                                    check = {0, JB_Key(2), 0}
                                End If
                            Else
                                ' 2 = True
                                If Abs(value(1) - value(3)) > WK_Options.LimitTechniknote Then
                                    ' 3 = False
                                    check = {JB_Key(1), 0, 0}
                                Else
                                    ' 3 = True
                                    check = {JB_Key(1), JB_Key(2), 0}
                                End If
                            End If
                        Else
                            ' 1 = True
                            If Abs(value(2) - value(3)) > WK_Options.LimitTechniknote Then
                                ' 2 = False
                                If Abs(value(1) - value(3)) > WK_Options.LimitTechniknote Then
                                    ' 3 = False
                                    check = {0, 0, JB_Key(3)}
                                Else
                                    ' 3 = True
                                    check = {0, JB_Key(2), JB_Key(3)}
                                End If
                            Else
                                ' 2 = True
                                If Abs(value(1) - value(3)) > WK_Options.LimitTechniknote Then
                                    ' 3 = False
                                    check = {JB_Key(1), 0, JB_Key(3)}
                                Else
                                    ' 3 = True
                                    check = {0, 0, 0}
                                End If
                            End If
                        End If
                End Select
            Catch ex As Exception
                LogMessage("KR_Steuerung: OutOfRange: " & ex.Message, ex.StackTrace)
            End Try

            Return check(0) + check(1) + check(2)

        End Function

        Public Sub Dispose() Implements IDisposable.Dispose
        End Sub
    End Class

    Class Write_Pad
        Implements IDisposable
        Public Sub Write_Gültig(Index As Integer)
            Try
                If Not KR_Pad(Index).Connected Then Return
                With Ports(Index)
                    .Write({P_Clear_Display}, 0, 1)
                    .Write({P_SelLine_1}, 0, 1)
                    If Not Wettkampf.Technikwertung Then
                        .Write("G")
                        '.Write({P_ü}, 0, 1)
                        '.Write("ltig ")
                    Else
                        .Write("Technik-Note:")
                        .Write({P_SecondLine_Pad}, 0, 1)
                        .Write({P_SelLine_2}, 0, 1)
                    End If
                End With
            Catch ex As Exception
            End Try
        End Sub
        Public Sub Write_Ungültig(Index As Integer) ', Direct As Boolean, Wertung As Integer)
            'If Direct Then Leader.Change_Wertung(Index, Anzeige.ScoreColors(Wertung))
            If Not KR_Pad(Index).Connected Then Return
            With Ports(Index)
                .Write({P_Clear_Display}, 0, 1)
                .Write({P_SelLine_1}, 0, 1)
                .Write("Ung")
                '.Write({P_ü}, 0, 1)
                '.Write("ltig ")
                '.Write({P_SecondLine_PC}, 0, 1)
            End With
        End Sub
        Public Sub Technik_Display(Index As Integer, Optional Note As String = "", Optional Line_1_Type As Integer = LineType.Empty)
            If Not KR_Pad(Index).Connected Then Return
            With Ports(Index)
                If Not Line_1_Type = LineType.Empty Then .Write({P_SelLine_1}, 0, 1)
                If Line_1_Type = LineType.Techniknote Then
                    If Note.Equals("0") Then Note = String.Empty
                    .Write("Technik-Note:")
                ElseIf Line_1_Type = LineType.Bestätigt Then
                    .Write({P_SelLine_1}, 0, 1)
                    .Write("best")
                    .Write({P_ä}, 0, 1)
                    .Write("tigt")
                Else
                    .Write({P_SecondLine_PC}, 0, 1)
                    .Write({P_SelLine_2}, 0, 1)
                    .Write(Note)
                End If
                .Write({P_SecondLine_Pad}, 0, 1)
            End With
        End Sub
        Public Sub Dispose() Implements IDisposable.Dispose
        End Sub
    End Class

    Public Sub Change_Technik_Display(Index As Integer, Optional Note As String = "", Optional Line_1_Type As Integer = LineType.Empty)
        Using wp As New Write_Pad
            wp.Technik_Display(Index, Note, Line_1_Type)
        End Using

    End Sub
    Public Sub Change_Gültig(Index As Integer)
        Using wp As New Write_Pad
            wp.Write_Gültig(Index)
        End Using
    End Sub
    Public Sub Change_Ungültig(Index As Integer)
        Using wp As New Write_Pad
            wp.Write_Ungültig(Index)
        End Using
    End Sub
    Public Function Wertung() As Integer()
        Dim _wertung = 0
        For i As Integer = 1 To 3
            If KR_Pad.ContainsKey(i) Then _wertung += KR_Pad(i).Wertung
        Next
        Select Case _wertung
            Case 5, 10, 15, 17
                Return {_wertung, 1} ' Gültig
            Case 7, 14, 19, 21
                Return {_wertung, -1} ' Ungültig
            Case Else
                Return {_wertung, 0} ' keine Wertung
        End Select
    End Function

    Public Function IsPadIndexExisting(Index As Integer) As Boolean
        Dim dlgResult = DialogResult.None
        Do
            For Each pad In KR_Pad
                If pad.Value.Index > 0 AndAlso pad.Value.Index = Index Then
                    Using New Centered_MessageBox(formLeader, {"Prüfen"})
                        dlgResult = MessageBox.Show("Ein KR-Pad mit der ID " & Index & " wurde bereits angesteckt." & vbNewLine &
                                                    "Vor erneutem Prüfen erst Pad austauschen.", msgCaption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error)
                        If dlgResult = DialogResult.Cancel Then
                            Return False
                        End If
                    End Using
                End If
            Next
        Loop While dlgResult = DialogResult.Retry
        Return True
    End Function

    Class mySteuerung
        Implements IDisposable

        'Private Hantel As New myHantel

#Region "Deklarationen"

        'Private WithEvents JB_Port As New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
        '    .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}
        'Private WithEvents Port1 As New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
        '    .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}
        'Private WithEvents Port2 As New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
        '    .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}
        'Private WithEvents Port3 As New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
        '    .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}
        'Private WithEvents ZN_Port As New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
        '    .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}
        'Private WithEvents ZA_Port As New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
        '    .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}

        Private WithEvents JB_Port As SerialPort
        Private WithEvents Port1 As SerialPort
        Private WithEvents Port2 As SerialPort
        Private WithEvents Port3 As SerialPort
        Private WithEvents ZN_Port As SerialPort
        Private WithEvents ZA_Port As SerialPort

        Private ZN_Suspended As Boolean

        Dim JuryCall As Boolean() = {False, False, False, False}  'KR(Index) wurde zur Jury gerufen
        Shared buf(16) As Byte

        Private Delegate Function CheckPort() As Boolean
        Private Delegate Function DelegateJuryEntscheid() As DialogResult

        Dim JB_Data As Integer      ' die von JB_Port empfangenen Daten
        Dim ZN_Data As String
        Dim frmResult As frmJuryEntscheid

        Shared bytes As Dictionary(Of Integer, Byte()) 'Byte-Arrays für JB-Wertung

        Dim Key As RegistryKey
        Private wmi As WMIReceiveEvent

        'Shared _pause As Boolean
        'WithEvents bgwHantel As New BackgroundWorker With {.WorkerReportsProgress = False, .WorkerSupportsCancellation = True}
#End Region

        Public Sub KR_Steuerung_Load()

            bytes = New Dictionary(Of Integer, Byte())
            bytes.Add(5, {2, 8, 32})
            bytes.Add(7, {1, 4, 16})

            'AddHandler WA_Message.Closed, AddressOf WA_ExternalClosing
            AddHandler WA_Message.Initialize_KR, AddressOf KR_Steuerung_Initialize
            AddHandler WA_Events.Set_ZN, AddressOf ZN_Input
            AddHandler WA_Events.Set_KR, AddressOf KR_Input
            'AddHandler Wettkampf.KR_Changed, AddressOf Wettkampf_KR
            AddHandler Leader.Call_Aufruf, AddressOf Set_Aufruf
            'AddHandler Leader.Korrektur, AddressOf Set_Korrektur
            AddHandler Leader.NächsterHeber, AddressOf Change_CountDown
            'AddHandler Anzeige.Gültig_Changed, AddressOf Gültig_Changed
            'AddHandler Anzeige.Ungültig_Changed, AddressOf Ungültig_Changed
            AddHandler Reset_WK, AddressOf KR_BlaueTaste
            'AddHandler WA_Events.Suspend_ZN, AddressOf Set_ZN_Suspended

            WK_Options = WK_Options_Tmp

            KR_Pad = New Dictionary(Of Integer, myPad)
            KR_Pad(1) = New myPad
            KR_Pad(2) = New myPad
            KR_Pad(3) = New myPad

            ZN_Pad = New myTPad
            ZA_Dev = New myZA
            JB_Dev = New myJB

            ' Pads und Ports initialisieren
            Dim t As New Threading.Thread(New Threading.ThreadStart(AddressOf KR_Steuerung_Initialize))
            t.Priority = Threading.ThreadPriority.Normal
            t.Start()

            ' Timer stoppen, Variablen zurücksetzen
            Initiate_Bridge()

        End Sub
        Public Sub KR_Steuerung_Dispose()
            Try
                wmi.Stop_Watching()
            Catch ex As Exception
                ' falls Watcher nicht gestartet wurde
            End Try
            Try
                For i As Integer = 1 To 3
                    If KR_Pad.ContainsKey(i) Then
                        If KR_Pad(i).Connected Then
                            With Ports(i)
                                If .IsOpen Then
                                    Try
                                        Threading.Thread.Sleep(100)
                                        .Write({P_Clear_Display}, 0, 1)
                                        .Write({P_Light_OFF}, 0, 1)
                                    Catch ex As Exception
                                    Finally
                                        .Dispose()
                                    End Try
                                End If
                            End With
                        End If
                    End If
                Next
                If ZN_Pad.Connected Then
                    With Ports(ID.ZN)
                        Try
                            Threading.Thread.Sleep(50)
                            .Write({P_Clear_Display}, 0, 1)
                            .Write({P_Light_OFF}, 0, 1)
                        Catch ex As Exception
                        Finally
                            .Dispose()
                        End Try
                    End With
                End If
                Try
                    With ZN_Pad
                        .Connected = False
                        .IsAttemptRated = False
                        .CountDown = 0
                    End With
                Catch ex As Exception
                End Try
                If JB_Dev.Connected Then
                    Try
                        JB_Write(JB_Off)
                    Catch ex As Exception
                    Finally
                        Ports(ID.JB).Dispose()
                    End Try
                End If
                If ZA_Dev.Connected Then
                    With Ports(ID.ZA)
                        Try
                            If .IsOpen Then ZA_Write()
                        Catch ex As Exception
                        Finally
                            .Dispose()
                        End Try
                    End With
                End If
            Catch ex As Exception
            End Try
            Try
                timerAufruf_Running = False
                timerAbzeichen_Running = False
                timerDelay_Running = False
                timerHupe_Running = False

                WA_Message.Status = -1 ' Update("keine Wertungsanlage", False)

                If Wettkampf.WK_Modus <> Modus.Normal Then RaiseEvent Set_formEasyMode_btnStart_Enabled(True)
            Catch ex As Exception
            End Try

            'RemoveHandler WA_Message.Closed, AddressOf WA_ExternalClosing
            RemoveHandler WA_Message.Initialize_KR, AddressOf KR_Steuerung_Initialize
            RemoveHandler Leader.Call_Aufruf, AddressOf Set_Aufruf
            RemoveHandler Leader.NächsterHeber, AddressOf Change_CountDown
            'RemoveHandler Leader.Korrektur, AddressOf Set_Korrektur
            RemoveHandler WA_Events.Set_ZN, AddressOf ZN_Input
            RemoveHandler WA_Events.Set_KR, AddressOf KR_Input
            'RemoveHandler Wettkampf.KR_Changed, AddressOf Wettkampf_KR
            'RemoveHandler Anzeige.Gültig_Changed, AddressOf Gültig_Changed
            'RemoveHandler Anzeige.Ungültig_Changed, AddressOf Ungültig_Changed
            RemoveHandler Reset_WK, AddressOf KR_BlaueTaste
            'RemoveHandler WA_Events.Suspend_ZN, AddressOf Set_ZN_Suspended

            KR_IsInitialized = False
            Dispose()
        End Sub
        Public Sub KR_Steuerung_Initialize()

            WA_Message.Status = 1000

            If Not IsNothing(Ports) Then
                Try
                    For Each Port In Ports
                        Port.Value.Close()
                        Port.Value.Dispose()
                    Next
                Catch ex As Exception
                End Try
            End If

            Port1 = New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
                        .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}
            Port2 = New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
                        .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}
            Port3 = New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
                        .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}
            ZN_Port = New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
                        .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}
            ZA_Port = New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
                        .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}
            JB_Port = New SerialPort With {.BaudRate = 9600, .DataBits = 8, .Parity = Parity.None, .ParityReplace = 63, .PortName = "COM0",
                        .ReadBufferSize = 4096, .ReadTimeout = 500, .ReceivedBytesThreshold = 1, .StopBits = StopBits.One, .WriteBufferSize = 2048, .WriteTimeout = 500}

            Ports = New Dictionary(Of Integer, SerialPort)
            Ports.Add(ID.KR1, Port1)
            Ports.Add(ID.KR2, Port2)
            Ports.Add(ID.KR3, Port3)
            Ports.Add(ID.ZN, ZN_Port)
            Ports.Add(ID.ZA, ZA_Port)
            Ports.Add(ID.JB, JB_Port)

            If Check_for_Ports() Then
                ' es sind Ports vorhanden
                COM_Init()
                WA_Message.Status = 1
            Else
                ' keine Ports vorhanden
                WA_Message.Status = 0
                'WA_Message.Update("keine Wertungsanlage", False)
            End If

            RaiseEvent Set_formLeader_btnNext_Enabled(True)

            Try
                wmi = New WMIReceiveEvent
                wmi.Start_Watching()
            Catch ex As Exception
                ' falls Watcher nicht gestartet wurde
            End Try

            KR_IsInitialized = WA_Message.Status > 0 ' True
        End Sub

        Class WMIReceiveEvent
            Dim serialPorts As String() = SerialPort.GetPortNames 'Liste der überwachten Ports -- wird in den Handles aktualisiert
            Dim deviceArrivalQuery As New WqlEventQuery("SELECT * FROM Win32_DeviceChangeEvent WHERE EventType = 2")
            Dim deviceRemovalQuery As New WqlEventQuery("SELECT * FROM Win32_DeviceChangeEvent WHERE EventType = 3")
            Dim arrival As New ManagementEventWatcher(deviceArrivalQuery)
            Dim removal As New ManagementEventWatcher(deviceRemovalQuery)
            Private LockObject As New Object
            Public Sub Start_Watching()
                AddHandler arrival.EventArrived, AddressOf HandleArrival
                AddHandler removal.EventArrived, AddressOf HandleRemoval
                arrival.Start()
                removal.Start()
            End Sub
            Public Sub Stop_Watching()
                arrival.Stop()
                removal.Stop()
                RemoveHandler arrival.EventArrived, AddressOf HandleArrival
                RemoveHandler removal.EventArrived, AddressOf HandleRemoval
            End Sub
            Private Sub HandleArrival(sender As Object, e As EventArrivedEventArgs)
                Dim availablePorts = SerialPort.GetPortNames()
                Dim _ID As Byte = 0
                SyncLock LockObject
                    If Not serialPorts.SequenceEqual(availablePorts) Then
                        WA_Message.Status = 1000 ' Init
                        Threading.Thread.Sleep(WK_Options.COM_Sleep)  ' Diese Zeit wird benötigt um die Initialisierung des Pads abzuwarten
                        For i = serialPorts.Length To availablePorts.Length - 1 ' der hinzugekommene Port wird immer hinten angefügt
                            Dim port As New SerialPort
                            Dim NoPad As Boolean
                            With port
                                Try
                                    .PortName = availablePorts(i)
                                    .BaudRate = 9600
                                    .Parity = Parity.None
                                    .DataBits = 8
                                    .StopBits = StopBits.One
                                    .Handshake = Handshake.None
                                    .ReceivedBytesThreshold = 1
                                    .ReadTimeout = -1
                                    .WriteTimeout = -1
                                    .Open()
                                Catch ex As Exception
                                    NoPad = True
                                End Try
                                If Not NoPad Then
                                    Try
                                        .DiscardInBuffer()
                                        .DiscardOutBuffer()
                                        .Write({5}, 0, 1) 'Hier wird überprüft ob an der Schnittstelle ein Pad angeschlossen ist: Es muss seine ID nennen.
                                    Catch ex As Exception
                                        NoPad = True
                                    End Try
                                End If
                                If Not NoPad Then
                                    Try
                                        _ID = CByte(.ReadByte - 97)
                                        Ports(_ID).PortName = .PortName
                                        .Dispose()
                                        Ports(_ID).Open()
                                    Catch ex As Exception
                                        NoPad = True
                                    End Try
                                End If
                            End With
                            If Not NoPad Then
                                With Ports(_ID)
                                    Select Case _ID
                                        Case CByte(ID.KR1) To CByte(ID.KR3)
                                            If Not IsPadIndexExisting(_ID) Then Exit For
                                            KR_Pad(_ID).Connected = True
                                            KR_Pad(_ID).Index = _ID
                                            Try
                                                .Write({P_Clear_Display}, 0, 1)
                                                .Write({P_Light_ON}, 0, 1)
                                                .Write({P_SecondLine_PC}, 0, 1)
                                                .Write({P_SelLine_1}, 0, 1)
                                                If Wettkampf.Technikwertung AndAlso KR_Pad(_ID).Verified Then
                                                    .Write("best")
                                                    .Write({P_ä}, 0, 1)
                                                    .Write("tigt")
                                                ElseIf Wettkampf.Technikwertung AndAlso KR_Pad(_ID).Wertung > 0 AndAlso Not KR_Pad(_ID).Verified Then
                                                    .Write("Technik-Note")
                                                ElseIf KR_Pad(_ID).Wertung > 0 Then
                                                    .Write(If(KR_Pad(_ID).Wertung = 5, "G", "Ung"))
                                                    .Write({P_ü}, 0, 1)
                                                    .Write("ltig ")
                                                Else
                                                    .Write("Wertung")
                                                End If
                                                If Wettkampf.Technikwertung AndAlso KR_Pad(_ID).Techniknote <> "0" Then
                                                    .Write({P_SelLine_2}, 0, 1)
                                                    .Write(Format(KR_Pad(_ID).Techniknote, "Fixed"))
                                                End If
                                                .Write({P_SecondLine_Pad}, 0, 1)
                                            Catch ex As Exception
                                                KR_Pad(_ID).Connected = False
                                            End Try
                                            'WA_Message.Status = 1 'Change_KR(Wettkampf.KR(User.Bohle), False)
                                        Case CByte(ID.ZN)
                                            Try
                                                ZN_Pad.IsAttemptRated = (From pad In KR_Pad
                                                                         Where pad.Value.Connected AndAlso pad.Value.Wertung > 0).Count > 0
                                                ZN_Pad.Connected = True
                                                Dim txt = "Aufruf "
                                                If Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Started) Then
                                                    txt += "gestartet"
                                                ElseIf Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Stopped) Then
                                                    txt += "gestoppt"
                                                Else
                                                    txt += "starten"
                                                End If
                                                With Ports(ID.ZN)
                                                    If Not .IsOpen Then .Open()
                                                    .Write({P_Light_ON}, 0, 1)
                                                    .Write({P_CLEAR_Line}, 0, 1)
                                                    .Write({P_SecondLine_PC}, 0, 1)
                                                    .Write({P_SelLine_1}, 0, 1)
                                                    .Write(txt)
                                                    .Write({P_SelLine_2}, 0, 1)
                                                    If Not IsNothing(Anzeige.CountDown) Then .Write(Anzeige.CountDown)
                                                End With
                                            Catch ex As Exception
                                                ZN_Pad.Connected = False
                                            End Try
                                            'WA_Message.Status = 1
                                        Case CByte(ID.ZA)
                                            ZA_Dev.Connected = True
                                            Try
                                                If Not IsNothing(Anzeige.CountDown) Then
                                                    Steuerung.ZA_Write(, , Anzeige.CountDown.Replace(":", "")) 'Anzeige.Minuten + Anzeige.Sekunden)
                                                Else
                                                    Steuerung.ZA_Write() ', , "") 'Anzeige.Minuten + Anzeige.Sekunden)
                                                End If
                                            Catch ex As Exception
                                                ZA_Dev.Connected = False
                                            End Try
                                            'WA_Message.Status = 1
                                        Case CByte(ID.JB)
                                            With JB_Dev
                                                If .Device = DevType.Raspberry Then
                                                    Using New Centered_MessageBox(formLeader)
                                                        If MessageBox.Show("Ein Jury-Board (Typ: " & .Device & ") ist bereits angesteckt." + vbNewLine +
                                                                           "Deaktivieren und mit Jury-Board (Typ: Control-Unit) ersetzen?",
                                                                            msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.No Then Return
                                                    End Using
                                                End If
                                                Try
                                                    Steuerung.JB_Write()
                                                    .Device = DevType.ControlUnit
                                                    .Connected = True
                                                Catch ex As Exception
                                                    .Connected = .Device = DevType.Raspberry
                                                End Try
                                            End With
                                    End Select
                                End With
                            End If
                        Next
                        serialPorts = availablePorts
                        WA_Message.Status = 1
                    End If
                End SyncLock
            End Sub
            Private Sub HandleRemoval(sender As Object, e As EventArrivedEventArgs)
                Dim availablePorts = SerialPort.GetPortNames
                SyncLock LockObject ' _serialPorts
                    If Not serialPorts.SequenceEqual(availablePorts) Then
                        WA_Message.Status = 3000
                        For i = availablePorts.Length To serialPorts.Length - 1
                            Dim disconnected = Get_Device_by_Port(serialPorts(i))
                            Select Case disconnected
                                Case ID.KR1 To ID.KR3
                                    KR_Pad(disconnected).Connected = False
                                    KR_Pad(disconnected).Index = 0
                                    'WA_Message.Status = 1 'Change_KR(Wettkampf.KR(User.Bohle), False)
                                Case ID.ZN
                                    ZN_Pad.Connected = False
                                Case ID.ZA
                                    ZA_Dev.Connected = False
                                Case ID.JB
                                    With JB_Dev
                                        .Device = DevType.None
                                        .Connected = False
                                    End With
                            End Select
                        Next
                        serialPorts = availablePorts
                        'WA_Message.Update()
                        WA_Message.Status = 1
                    End If
                End SyncLock
            End Sub
            Private Function Get_Device_by_Port(_port As String) As Integer
                If Ports(ID.KR1).PortName = _port Then Return ID.KR1
                If Ports(ID.KR2).PortName = _port Then Return ID.KR2
                If Ports(ID.KR3).PortName = _port Then Return ID.KR3
                If Ports(ID.JB).PortName = _port Then Return ID.JB
                If Ports(ID.ZN).PortName = _port Then Return ID.ZN
                If Ports(ID.ZA).PortName = _port Then Return ID.ZA
                Return 0
            End Function
        End Class

        Private Sub Change_CountDown(value As Integer) ' Zeitanzeige ändern, wenn Heber sich ändert
            ''Try
            ''    If InvokeRequired Then
            ''        Dim d As New DelegateSub3(AddressOf Change_CountDown)
            ''        Invoke(d, New Object() {value})
            ''    Else

            Set_CountDown(value)

            'If value <= 0 Then
            '    Anzeige.Stunden = ""
            '    Anzeige.DplPunkt = ""
            '    Anzeige.Minuten = "0"
            '    Anzeige.Sekunden = "00"
            'Else
            '    Dim Stunden As Integer = value \ 3600
            '    Dim Minuten As Integer = (value - Stunden * 3600) \ 60
            '    Dim Sekunden As Integer = value - Stunden * 3600 - Minuten * 60
            '    If Stunden > 0 Then
            '        Anzeige.Stunden = Stunden.ToString
            '        Anzeige.DplPunkt = ":"
            '        Anzeige.Minuten = Minuten.ToString("00")
            '    Else
            '        Anzeige.Stunden = ""
            '        Anzeige.DplPunkt = ""
            '        Anzeige.Minuten = Minuten.ToString
            '    End If
            '    Anzeige.Sekunden = Sekunden.ToString("00")
            'End If

            ''If Not timerHupe.Enabled Then
            'If Not timerHupe_Running Then
            '    If ZA_Dev.Connected Then ZA_Write(, , Anzeige.Minuten + Anzeige.Sekunden)
            'End If

            ZN_Pad.CountDown = Heber.CountDown

            'If ZN_Pad.Connected AndAlso value >= 0 Then
            '    Try
            '        With Ports(ID.ZN)
            '            .Write({P_SelLine_2}, 0, 1)
            '            .Write(Anzeige.Stunden + Anzeige.DplPunkt + Anzeige.Minuten + ":" + Anzeige.Sekunden)
            '        End With
            '    Catch ex As Exception
            '    End Try
            'End If
            ''    End If
            ''Catch ex As Exception
            ''End Try
        End Sub

        'Private Sub Gültig_Changed(Gültig As Color, Index As Integer)
        '    xValid(Index).ForeColor = Gültig
        '    'xInvalid(Index).ForeColor = Ansicht_Options.Shade_Color
        'End Sub

        'Private Sub Ungültig_Changed(Ungültig As Color, Index As Integer)
        '    xInvalid(Index).ForeColor = Ungültig
        '    xValid(Index).ForeColor = Ansicht_Options.Shade_Color
        'End Sub

        Public Sub Set_Abzeichen()

            If timerDelay_Running Then Return ' weitere Wertung innerhalb der 3 Sekunden

            Try
                If ZA_Dev.Connected Then
                    ' Ab-Zeichen
                    timerAbzeichen_Running = True
                    timerAbzeichen = New Threading.Timer(AddressOf TimerAbzeichen_Tick)
                    timerAbzeichen.Change(100, Threading.Timeout.Infinite)
                End If

                If Wettkampf.Technikwertung Then
                    Anzeige.T_Farbe = Ansicht_Options.Shade_Color
                    Anzeige.T_Wert = "X,XX"
                    Anzeige.T_Shown = True
                End If

                If Not IsNothing(dvLeader(Leader.TableIx)) AndAlso dvLeader(Leader.TableIx).Count > Leader.RowNum Then
                    Dim row As DataRow = dvLeader(Leader.TableIx)(Leader.RowNum + 1).Row
                    Hantel.Change_Hantel(CInt(row!HLast),, row!Sex.ToString, CInt(row!Jahrgang), Wettkampf.BigDisc, False)
                Else
                    Hantel.Clear_Hantel()
                End If

            Catch ex As Exception
                LogMessage("KR_Steuerung: Set_Abzeichen: " & ex.Message, ex.StackTrace)
            End Try

            ' Eingabe der Hantelbeladung bei EasyModus sperren
            If Wettkampf.WK_Modus = Modus.EasyHantel Then RaiseEvent Set_formHantelbeladung_btnShow_Enabled(False)
        End Sub
        'Sub bgwHantel_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwHantel.DoWork
        '    ' Hantelbeladung in Bohle und Hantel löschen --> nur für Bohle notwendig, da Neuzeichnen in frmHantel Clear impliziert
        '    RaiseEvent Clear_Hantel()
        '    ' Hantelbeladung für nächsten Heber in frmHantel anzeigen
        '    Try
        '        Dim row As DataRow = dvLeader(Leader.TableIx)(Leader.RowNum + 1).Row
        '        Leader.Update_Hantel(Leader.Set_Hantelstange(row!sex.ToString, row!Jahrgang.ToString, CInt(row!HLast)), CInt(row!HLast))
        '    Catch ex As Exception
        '    End Try
        'End Sub

        '' TimerAufruf und ClockColor
        Private Sub Set_Aufruf(Optional status As Boolean? = Nothing, Optional msg As String = "", Optional Pause As Boolean = False)
            ' status{Nothing} = Waiting, status{True} = Started, status{False} = Stopped

            If IsNothing(status) Then
                Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Waiting)

            ElseIf status AndAlso timerAufruf_Running Then
                Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Started)
                timerAufruf = New Threading.Timer(AddressOf timerAufruf_Tick)
                timerAufruf.Change(1000, Threading.Timeout.Infinite)

            ElseIf Not status Then
                Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Stopped)
                timerAufruf_Running = False
                If Pause AndAlso Not IsNothing(timerAufruf) Then timerAufruf.Dispose()

                If ZN_Pad.Connected Then
                    With Ports(ID.ZN)
                        .Write({P_SelLine_1}, 0, 1)
                        If msg.Contains("ä") Then
                            .Write("Aufruf l")
                            .Write({P_ä}, 0, 1)
                            .Write("uft")
                        Else
                            .Write(msg)
                        End If
                    End With
                End If

            End If
        End Sub

        Private Sub Set_CountDown(value As Integer)

            ''Try
            ''    If InvokeRequired Then
            ''        Dim d As New DelegateSub3(AddressOf Set_ZA)
            ''        Invoke(d, New Object() {value})
            ''    Else

            Dim Stunden As Integer = value \ 3600
            Dim Minuten As Integer = (value - Stunden * 3600) \ 60
            Dim Sekunden As Integer = value - Stunden * 3600 - Minuten * 60

            If value <= 0 Then
                Anzeige.CountDown = "0:00"
            ElseIf Stunden > 0 Then
                Anzeige.CountDown = Stunden.ToString + ":" + Minuten.ToString("00") + ":" + Sekunden.ToString("00")
            Else
                Anzeige.CountDown = Minuten.ToString + ":" + Sekunden.ToString("00")
            End If

            If Not timerHupe_Running Then
                If ZA_Dev.Connected Then ZA_Write(, , Anzeige.CountDown.Replace(":", ""))
            End If

            If ZN_Pad.Connected Then
                'Threading.Thread.Sleep(50)
                With Ports(ID.ZN)
                    .Write({P_SelLine_2}, 0, 1)
                    .Write(Anzeige.CountDown)
                End With
            End If

            ''    End If
            ''Catch ex As Exception
            ''End Try

        End Sub

        'Private Sub Wettkampf_KR(KR As Integer)
        '    ' Event wird ausgelöst, wenn sich die Anzahl der KRs im WK ändert
        '    ' hier kommt immer 1 oder 3 an
        '    'Change_KR(KR)
        '    WA_Message.Status = 1
        'End Sub

        '' Devices initialisieren
        Private Function Check_for_Ports() As Boolean

            'WA_Message.Update("Wertungsanlage initialisieren", False) 'Prüfe Ports
            'RaiseEvent Refresh_Bohle()

            'Ports = New Dictionary(Of Integer, SerialPort)
            'Ports.Add(ID.KR1, Port1)
            'Ports.Add(ID.KR2, Port2)
            'Ports.Add(ID.KR3, Port3)
            'Ports.Add(ID.ZN, ZN_Port)
            'Ports.Add(ID.ZA, ZA_Port)
            'Ports.Add(ID.JB, JB_Port)

            Dim TestPort As New SerialPort With {.BaudRate = 9600, .DataBits = 8, .ParityReplace = 63, .PortName = "COM0", .ReadBufferSize = 4096, .ReceivedBytesThreshold = 1, .ReadTimeout = -1, .WriteBufferSize = 2048, .WriteTimeout = -1}
            Dim PortNames As New List(Of String)
            Dim NoPort As Boolean
            Dim _ID As Integer

            ' liest Registry
            Try
                Using Key As RegistryKey = Registry.LocalMachine.OpenSubKey("HARDWARE\DEVICEMAP\SERIALCOMM", RegistryKeyPermissionCheck.ReadSubTree)
                    If Not IsNothing(Key) Then
                        Dim ValueNames = Key.GetValueNames.ToList
                        For Each Value In ValueNames
                            If LCase(Value).Contains("serial") OrElse
                               LCase(Value).Contains("silabser") OrElse
                               LCase(Value).Contains("usbser") OrElse
                               LCase(Value).Contains("vcp") Then
                                If Not PortNames.Contains(Key.GetValue(Value).ToString) Then
                                    PortNames.Add(Key.GetValue(Value).ToString)
                                End If
                            End If
                        Next
                    End If
                End Using
            Catch ex As Exception
            End Try

            If PortNames.Count = 0 Then Return False 'keine Ports in Registry

            With TestPort
                For Each PortName In PortNames
                    NoPort = False
                    Try
                        If .IsOpen Then .Close()
                        .PortName = PortName
                        If Not .IsOpen Then .Open()
                    Catch ex As Exception ' IOException
                        NoPort = True
                        LogMessage("KR-Steuerung:Check_for_Ports / " & ex.Message & " / StackTrace " &
                                   If(InStr(ex.StackTrace, "Zeile") > 0, Mid(ex.StackTrace, InStr(ex.StackTrace, "Zeile")), String.Empty),
                                   Path.Combine(Application.StartupPath, "log", "com_" & Format(Now, "yyyy-MM-dd") & ".log"))
                    End Try
                    If Not NoPort Then
                        Try
                            Threading.Thread.Sleep(WK_Options.COM_Sleep)
                            .DiscardInBuffer()
                            Threading.Thread.Sleep(WK_Options.COM_Sleep)
                            .DiscardOutBuffer()
                            Threading.Thread.Sleep(WK_Options.COM_Sleep)
                            .Write({5}, 0, 1) 'Hier wird überprüft ob an der Schnittstelle ein Pad angeschlossen ist: Es muss seine ID nennen.
                        Catch ex As Exception
                            NoPort = True
                        End Try
                    End If
                    If Not NoPort Then
                        Try
                            Threading.Thread.Sleep(WK_Options.COM_Sleep)
                            If .BytesToRead > 0 Then
                                _ID = .ReadByte - 97
                            Else
                                NoPort = True
                            End If
                        Catch ex As Exception
                            NoPort = True
                            If ex.Message.Contains("Anschluss ist geschlossen") Then
                                MessageBox.Show("Das Gerät am Anschluss " + PortName + " wurde nicht erkannt." + vbNewLine +
                                                "Anschluss prüfen und Initialisierung wiederholen.",
                                                msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                        End Try
                    End If
                    If Not NoPort Then
                        Try
                            With Ports(_ID)
                                If .IsOpen Then .Close()
                                If .PortName.Equals("COM0") Then
                                    .PortName = PortName
                                Else
                                    ' bereits ein Device zugeordnet
                                End If
                            End With
                        Catch ex As Exception
                        End Try
                    End If
                Next
                .Close()
                .Dispose()
            End With

            Return True







            Dim tmpPads As New Dictionary(Of Integer, myPad)
            For Each pad In KR_Pad
                'Dim value As myPad = pad.Value
                tmpPads(pad.Key) = New myPad 'value
            Next

            With TestPort
                For c As Integer = 0 To Ports.Count - 1
                    'Try
                    '    Change_Progress(CInt(100 / (Ports.Count + 1) * (c + 1)))
                    'Catch ex As Exception
                    '    Change_Progress(100)
                    'End Try
                    NoPort = False
                    Try
                        If .IsOpen Then .Close()
                        .PortName = PortNames(c)
                        If Not .IsOpen Then .Open()
                    Catch ex As Exception ' IOException
                        NoPort = True
                    End Try
                    If Not NoPort Then
                        Try
                            .DiscardInBuffer()
                            .DiscardOutBuffer()
                            .Write({5}, 0, 1) 'Hier wird überprüft ob an der Schnittstelle ein Pad angeschlossen ist: Es muss seine ID nennen.
                        Catch ex As Exception
                            NoPort = True
                        End Try
                    End If
                    If Not NoPort Then
                        Try
                            Threading.Thread.Sleep(100) 'WK_Options.COM_Sleep) 
                            If .BytesToRead > 0 Then
                                _ID = CByte(.ReadByte - 97)
                            Else
                                NoPort = True
                            End If
                        Catch ex As Exception
                            If ex.Message.Contains("Anschluss ist geschlossen") Then
                                MessageBox.Show("Das Gerät am Anschluss " + PortNames(c) + " wurde nicht erkannt." + vbNewLine +
                                                    "Anschluss prüfen und erneut initialisieren.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                            NoPort = True
                        End Try
                        If Not NoPort Then
                            Try
                                If _ID <= ID.KR3 Then NoPort = Not IsPadIndexExisting(_ID)
                                If Not NoPort Then
                                    Dim pad As New myPad
                                    If tmpPads.ContainsKey(_ID) Then
                                        pad = tmpPads(_ID)
                                        'pad.Index = _ID
                                        KR_Pad(_ID) = pad
                                    End If
                                    'KR_Pad(_ID).Connected = True
                                    'KR_Pad(_ID).Index = _ID
                                    With Ports(_ID)
                                        If .IsOpen Then .Close()
                                        .PortName = PortNames(c)
                                    End With
                                End If
                            Catch ex As Exception
                            End Try
                        End If
                    End If
                Next
                'Change_Progress(100)
                .Dispose()
            End With
            Return True
        End Function
        Private Function Check_PadExists(Index As Integer) As Boolean
            For Each Port In Ports

            Next
            Return False
        End Function

        Private Sub COM_Init()

            Dim Success As Boolean = False

#Region "Jury-Board"
            With JB_Dev
                Try
                    If Ports(ID.JB).PortName <> "COM0" Then
                        If Not Ports(ID.JB).IsOpen Then Ports(ID.JB).Open()
                        If .Device = DevType.Raspberry Then
                            Using New Centered_MessageBox(formLeader)
                                Success = MessageBox.Show("Ein Jury-Board (Typ: " & .Device & ") ist bereits angesteckt." + vbNewLine +
                                                          "Deaktivieren und mit Jury-Board (Typ: Control-Unit) ersetzen?",
                                                           msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes
                            End Using
                        Else
                            Success = True
                        End If
                        If Success Then
                            .Device = DevType.ControlUnit
                            .Connected = True
                        End If
                    End If
                Catch ex As Exception
                    .Connected = .Device = DevType.Raspberry
                End Try
            End With
#End Region

#Region "Zeit-Anzeige"
            Success = False
            If Ports(ID.ZA).PortName <> "COM0" Then
                Try
                    If Not Ports(ID.ZA).IsOpen Then Ports(ID.ZA).Open()
                    ZA_Dev.Connected = True
                    Success = True
                Catch ex As Exception
                End Try
            End If
            If Not Success Then ZA_Dev.Connected = False
#End Region

#Region "Zeitnehmer-Pad"
            Success = False
            If Ports(ID.ZN).PortName <> "COM0" Then
                Try
                    With Ports(ID.ZN)
                        If Not .IsOpen Then .Open()
                        .Write({P_CLEAR_Line}, 0, 1)
                        .Write({P_Light_ON}, 0, 1)
                        .Write({P_Clear_Display}, 0, 1)
                        .Write({P_SecondLine_PC}, 0, 1)
                        .Write({P_SelLine_1}, 0, 1)
                        .Write("ZN-Pad bereit")
                        Change_CountDown(-1)
                    End With
                    With ZN_Pad
                        .Connected = True
                        .CountDown = 0
                    End With
                    Success = True
                Catch ex As Exception
                End Try
            End If
            If Not Success Then ZN_Pad.Connected = False
#End Region

#Region "KR-Pads"
            For i = ID.KR1 To ID.KR3
                Success = False
                If Ports(i).PortName <> "COM0" Then
                    Try
                        With Ports(i)
                            Try
                                If Not .IsOpen Then .Open()
                                .Write({P_CLEAR_Line}, 0, 1)
                                .Write({P_Light_ON}, 0, 1)
                                .Write({P_Clear_Display}, 0, 1)
                                .Write({P_SecondLine_PC}, 0, 1)
                                .Write({P_SelLine_1}, 0, 1)
                                .Write("KR-Pad " & i & " bereit")
                            Catch ex As Exception
                            End Try
                        End With
                        With KR_Pad(i)
                            .Connected = True
                            .Index = i
                        End With
                        Success = True
                    Catch ex As Exception
                    End Try
                End If
                If Not Success Then KR_Pad(i).Connected = False
            Next
#End Region

            Change_Progress(0, False) ' ProgressBar in Bohle

            'WA_Message.Status = Abs(CInt(Success))

            'Return Abs(CInt(Success))

        End Sub
        Private Sub Initiate_Bridge()
            timerAufruf_Running = False
            timerDelay_Running = False
            timerHupe_Running = False

            JB_Enabled = False

            ScoreComplete = Score.Not_Set

            If Not IsNothing(timerAufruf) Then timerAufruf.Dispose()
            If Not IsNothing(timerAbzeichen) Then timerAbzeichen.Dispose()
            If Not IsNothing(timerDelay) Then timerDelay.Dispose()
            If Not IsNothing(timerKR) Then timerKR.Dispose()
        End Sub
        Private Sub Initiate_Devices(Optional Heber_Present As Boolean = True)
            'KR-Pads
            For i = 1 To 3
                If KR_Pad.ContainsKey(i) Then
                    With KR_Pad(i)
                        .AufrufZeit = Nothing
                        .Techniknote = "0"
                        .TechnikZeit = Nothing
                        .Wertung = 0
                        .WertungZeit = Nothing
                        .Verified = False
                    End With
                    If KR_Pad(i).Connected Then
                        Try
                            With Ports(i)
                                .DiscardInBuffer()
                                .DiscardOutBuffer()
                                .Write({P_Clear_Display}, 0, 1)
                                If Heber_Present Then
                                    .Write({P_SelLine_1}, 0, 1)
                                    .Write("Wertung")
                                End If
                            End With
                        Catch ex As Exception
                        End Try
                        Threading.Thread.Sleep(50)
                    End If
                End If
            Next

            'ZN-Port
            With ZN_Pad
                .IsAttemptRated = False
                Try
                    If .Connected Then
                        With Ports(ID.ZN)
                            .DiscardInBuffer()
                            .DiscardOutBuffer()
                            .Write({P_Clear_Display}, 0, 1)
                            If Heber_Present Then
                                .Write({P_SelLine_1}, 0, 1)
                                .Write("Aufruf starten")
                            End If
                        End With
                    End If
                Catch ex As Exception
                End Try
                If Wettkampf.WK_Modus > Modus.Normal Then
                    .CountDown = 60
                    Set_CountDown(.CountDown)
                Else
                    Set_CountDown(0)
                End If
                Set_Aufruf()
            End With

            JB_Write(JB_Off)

            'RaiseEvent Werbung_VisibleChange(False)
            'Leader.IsPause = False
        End Sub

        '' JB
        Sub JuryBoard_ReadData(sender As Object, e As SerialDataReceivedEventArgs) Handles JB_Port.DataReceived

            If Not JB_Enabled Then
                With Ports(ID.JB)
                    .DiscardInBuffer()
                    .DiscardOutBuffer()
                End With
                Return
            End If
            Try
                If Leader.IsPause Then Return
                JB_Data = Ports(ID.JB).ReadByte
                If JB_Data > 0 Then JB_Proceed()
                'BeginInvoke(JB_Proc)
            Catch ex As Exception
            End Try
        End Sub
        Private Sub JB_Proceed()

            'If xAK.ForeColor = Color.Red Then
            '    'Fehlermeldung wird an Bohle angezeigt
            '    If JB_Dev.Connected Then JB_Write(JB_Off)
            '    Exit Sub
            'End If

            Dim x As Integer = 0

            Select Case JB_Data
                Case 70 ' Fehler beim Lesen
                'Stop
                Case 79 ' fehlerfrei empfangen
                Case 80 ' Taster losgelassen
                Case > 80, <= 95 ' ein Taster gedrückt
                    With JB_Dev
                        Select Case JB_Data - 80
                            Case JB_Rev ' Revision gedrückt
                                Try
                                    If frmResult.Visible Then
                                        Ports(ID.JB).DiscardInBuffer()
                                        Ports(ID.JB).DiscardOutBuffer()
                                        Exit Sub
                                    End If
                                Catch ex As Exception
                                End Try
                                Try
                                    If .Connected Then
                                        .RevPressed = True
                                        JB_Write(JB_Rev) 'orangen Taster einschalten
                                        'RaiseEvent ForeColors_Change(True)
                                    End If
                                Catch ex As Exception
                                End Try
                                timerKR.Dispose()
                                KR_Fail = 0
                                ScoreComplete = Score.Complete 'alle Wertungen überschrieben = Wertung vollständig
                                For i As Integer = 1 To 3
                                    If KR_Pad.ContainsKey(i) Then KR_Pad(i).Wertung = -1
                                Next

                                'If (Not IsNothing(formLeader) AndAlso Not formLeader.IsDisposed) Then formLeader.btnNext.Enabled = False
                                RaiseEvent Set_formLeader_btnNext_Enabled(False)

                                Select Case JuryEntscheid()
                                    Case DialogResult.Yes
                                        For i As Integer = 1 To 3
                                            If KR_Pad.ContainsKey(i) Then
                                                KR_Pad(i).Wertung = 5
                                                KR_Pad(i).Verified = False 'damit neue Technikwertung möglich ist
                                                If Not Wettkampf.Technikwertung Then
                                                    Using wp As New Write_Pad
                                                        wp.Write_Gültig(i)
                                                    End Using
                                                Else
                                                    'Technik_Display(i,, LineType.Techniknote)
                                                    Using wp As New Write_Pad
                                                        wp.Technik_Display(i, , LineType.Techniknote)
                                                    End Using
                                                End If
                                            End If
                                        Next
                                    Case DialogResult.No
                                        For i As Integer = 1 To 3
                                            If KR_Pad.ContainsKey(i) Then
                                                KR_Pad(i).Wertung = 7
                                                KR_Pad(i).Techniknote = "0"
                                                KR_Pad(i).Verified = False
                                                'Write_Ungültig(i)
                                                Using wp As New Write_Pad
                                                    wp.Write_Ungültig(i)
                                                End Using
                                            End If
                                        Next
                                End Select
                                Using proc As New KR_Pad_Proceed
                                    proc.Set_Wertung(Wertung(), True)
                                End Using
                                .RevPressed = False 'wenn keine Bestätigung durch die Jury erfolgen soll, ansonsten auskommentieren
                                If JB_Dev.Connected Then Steuerung.JB_Write()
                            Case JB_Key(1) To JB_Key(3) ' blaue Taster (KR-Ruf)
                                Select Case JB_Data - 80
                                    Case 2
                                        x = 1
                                    Case 4
                                        x = 2
                                    Case 8
                                        x = 3
                                End Select
                                If .KR_Pressed(x) Then
                                    ' blauer Taster leuchtet
                                    If .KR_Pressed(x) Then
                                        ' KR wurde zur Jury gerufen und wird jetzt freigegeben ODER Taster von OutofRange aktiviert = Technikwert passt nicht
                                        If KR_Pad.ContainsKey(x) Then
                                            If KR_Pad(x).Connected Then
                                                With Ports(x)
                                                    .Write({P_Clear_Display}, 0, 1)
                                                    .Write({P_SelLine_1}, 0, 1)
                                                    '.Write({P_Beep}, 0, 1)
                                                End With
                                            End If
                                            Dim _wertung = Wertung(1)
                                            Select Case _wertung
                                                Case 7, 14, 19, 21
                                                    If KR_Pad(x).Wertung = 7 Then
                                                        'Write_Ungültig(x)
                                                        Using wp As New Write_Pad
                                                            wp.Write_Ungültig(x)
                                                        End Using
                                                    Else
                                                        Ports(x).Write("Wertung")
                                                    End If
                                                Case 0, 5, 10, 12, 15, 17
                                                    Select Case KR_Pad(x).Wertung
                                                    ' Wertung vom Pad
                                                        Case 0
                                                            If KR_Pad(x).Connected Then Ports(x).Write("Wertung")
                                                        Case 5
                                                            If Wettkampf.Technikwertung Then
                                                                'Technik_Display(x, , LineType.Techniknote)
                                                                Using wp As New Write_Pad
                                                                    wp.Technik_Display(x, , LineType.Techniknote)
                                                                End Using
                                                                KR_Pad(x).Verified = False
                                                                KR_Pad(x).Techniknote = "0"
                                                                KR_Input(x, vbCr)
                                                            End If
                                                        Case 7
                                                            'Write_Ungültig(x)
                                                            Using wp As New Write_Pad
                                                                wp.Write_Ungültig(x)
                                                            End Using
                                                    End Select
                                            End Select
                                        End If
                                        JB_Dev.KR_Pressed(x) = False
                                        If JB_Dev.Connected Then Steuerung.JB_Write()
                                    Else
                                        ' Taster von OutofRange aktiviert = Technikwert passt nicht
                                        If KR_Pad.ContainsKey(x) Then
                                            If KR_Pad(x).Wertung = 5 Then
                                                If KR_Pad(x).Connected Then
                                                    With Ports(x)
                                                        .Write({P_Clear_Display}, 0, 1)
                                                        .Write("Bitte neu werten")
                                                        .Write({P_Beep}, 0, 1)
                                                    End With
                                                End If
                                                JB_Dev.KR_Pressed(x) = False
                                                Try
                                                    If JB_Dev.Connected Then JB_Write(JB_Key(x))
                                                Catch ex As Exception
                                                End Try
                                            End If
                                        End If
                                    End If
                                Else
                                    'KR wird zur Jury gerufen
                                    If KR_Pad.ContainsKey(x) Then
                                        JB_Dev.KR_Pressed(x) = True
                                        If KR_Pad(x).Connected Then
                                            With Ports(x)
                                                .Write({P_Clear_Display}, 0, 1)
                                                .Write({P_SelLine_1}, 0, 1)
                                                .Write("Bitte zur Jury!")
                                                .Write({P_Beep}, 0, 1) '
                                            End With
                                        End If
                                    End If
                                    Try
                                        If JB_Dev.Connected Then JB_Write(JB_Key(x))
                                    Catch ex As Exception
                                    End Try
                                End If
                        End Select
                    End With
            End Select
        End Sub

        '' Jury-Control (Raspi)


        '' ZN-Pad
        Private Sub ZN_Port_ReadData(sender As Object, e As SerialDataReceivedEventArgs) Handles ZN_Port.DataReceived

            If Leader.IsPause Then Return

            Try
                With JB_Dev
                    If .RevPressed Or .KR_Pressed(1) Or .KR_Pressed(2) Or .KR_Pressed(3) Then
                        Ports(ID.ZN).DiscardInBuffer()
                        Exit Sub
                    End If
                End With
                ZN_Data = Ports(ID.ZN).ReadExisting().ToString
                If Not String.IsNullOrEmpty(ZN_Data) Then ZN_Proceed()
            Catch ex As TimeoutException
            End Try
        End Sub
        Private Sub ZN_Input(Data As String)
            ZN_Data = Data
            If Not String.IsNullOrEmpty(ZN_Data) Then
                With Ports(ID.ZN)
                    If Data.Equals("Lock") Then
                        ZN_Suspended = True
                        If ZN_Pad.Connected Then
                            .Write({P_SelLine_1}, 0, 1)
                            .Write("G E S P E R R T")
                        End If
                    ElseIf Data.Equals("Unlock") Then
                        ZN_Suspended = False
                        If ZN_Pad.Connected Then
                            .Write({P_SelLine_1}, 0, 1)
                            .Write("Aufruf gestoppt")
                        End If
                    Else
                        ZN_Proceed()
                    End If
                End With
            End If
        End Sub
        Private Sub ZN_Proceed()
            If ZN_Suspended Then
                If ZN_Pad.Connected Then
                    Ports(ID.ZN).Write({P_Beep}, 0, 1)
                End If
                Return
            End If
            Try
                With Ports(ID.ZN)
                    For Each item As Char In ZN_Data
                        Select Case item
                            Case Convert.ToChar(10)     ' Start / Stop
                                If Not ZN_Pad.IsAttemptRated Then
                                    If timerAufruf_Running Then
                                        Set_Aufruf(False, "Aufruf gestoppt")
                                    ElseIf Not timerAufruf_Running AndAlso ZN_Pad.CountDown > 0 Then
                                        timerAufruf_Running = True
                                        Set_Aufruf(True, "Aufruf läuft")
                                        If ZN_Pad.Connected Then
                                            .Write({P_SelLine_1}, 0, 1)
                                            .Write("Aufruf l")
                                            .Write({P_ä}, 0, 1)
                                            .Write("uft")
                                        End If
                                        Leader.Change_InfoMessage(String.Empty)
                                    End If
                                End If
                            Case Convert.ToChar(vbBack), Convert.ToChar(13)     ' CLEAR
                                If Not ZN_Pad.IsAttemptRated Then
                                    ZN_Pad.CountDown = 0
                                    timerAufruf_Running = False
                                    If ZN_Pad.Connected Then
                                        .Write({P_SelLine_1}, 0, 1)
                                        .Write("Bereit")
                                        .Write({P_SelLine_2}, 0, 1)
                                    End If
                                    Set_Aufruf()
                                    Set_CountDown(0)
                                End If
                            Case CChar("U"), CChar("G") ' Dummy
                            Case CChar("P")             ' Pause --> Vorbereiten zum Pause-Zeit eingeben 
                            Case Else                   ' Zahlen
                                If ZN_Pad.IsAttemptRated Or timerAufruf_Running Then Return
                                ZN_Pad.CountDown = CInt(item.ToString) * 60
                                If ZN_Pad.Connected Then
                                    .Write({P_SelLine_2}, 0, 1)
                                    .Write(item + ":00")
                                End If
                                ' *** Farbe der Zeitanzeige bei Eingabe einer Zahl
                                Dim x As Boolean? = Nothing
                                If timerAufruf_Running Then x = True
                                Set_Aufruf(x)
                                ' ***
                                Set_CountDown(ZN_Pad.CountDown)
                        End Select
                    Next
                End With
            Catch ex As Exception
            End Try
        End Sub

        '' KR-Pads
        Private Sub sPort1_ReadData(sender As Object, e As SerialDataReceivedEventArgs) Handles Port1.DataReceived
            If JB_Dev.RevPressed Then Return
            If Leader.IsPause Then Return
            Try
                Dim KR_Data = Ports(1).ReadExisting().ToString
                If Not String.IsNullOrEmpty(KR_Data) Then
                    Using pad As New KR_Pad_Proceed
                        pad.KR_Proceed(1, KR_Data)
                    End Using
                End If
            Catch ex As TimeoutException
            End Try
        End Sub
        Private Sub sPort2_ReadData(sender As Object, e As SerialDataReceivedEventArgs) Handles Port2.DataReceived
            If JB_Dev.RevPressed Then Return
            If Leader.IsPause Then Return
            Try
                Dim KR_Data = Ports(2).ReadExisting().ToString
                If Not String.IsNullOrEmpty(KR_Data) Then
                    Using pad As New KR_Pad_Proceed
                        pad.KR_Proceed(2, KR_Data)
                    End Using
                End If
            Catch ex As TimeoutException
            End Try
        End Sub
        Private Sub sPort3_ReadData(sender As Object, e As SerialDataReceivedEventArgs) Handles Port3.DataReceived
            If JB_Dev.RevPressed Then Return
            If Leader.IsPause Then Return
            Try
                Dim KR_Data = Ports(3).ReadExisting().ToString
                If Not String.IsNullOrEmpty(KR_Data) Then
                    Using pad As New KR_Pad_Proceed
                        pad.KR_Proceed(3, KR_Data)
                    End Using
                End If
            Catch ex As TimeoutException
            End Try
        End Sub
        Private Sub KR_Input(Index As Integer, Data As String)
            If Not String.IsNullOrEmpty(Data) Then
                Using pad As New KR_Pad_Proceed
                    pad.KR_Proceed(Index, Data)
                End Using
            End If
        End Sub

        Public Sub KR_BlaueTaste(Optional IsHeberPresent As Boolean = True) ' Heber_Present = False kommt von TimerWertungAnzeige wenn Modus=Normal
            'RaiseEvent Msg_Change(String.Empty)
            Initiate_Bridge()
            Initiate_Devices(IsHeberPresent)

            Leader.Change_JuryEntscheid(String.Empty)

            If WK_Options_Tmp.Changed Then
                WK_Options = WK_Options_Tmp
                WK_Options_Tmp.Changed = False
            End If

            ' Eingabe der Hantelbeladung im EasyMode freischalten
            If Wettkampf.WK_Modus = Modus.EasyHantel Then RaiseEvent Set_formHantelbeladung_btnShow_Enabled(True)

            ' Wertungsanzeige zurücksetzen
            RaiseEvent ForeColors_Change(False)

            Technikwertung = New myTN
        End Sub

        '' Timer
        Private Sub TimerAbzeichen_Tick(state As Object)
            Static ii As Integer = -100
            If ii = -100 Then ii = WK_Options.DauerAbzeichen
            If timerAbzeichen_Running = False Then
                timerAbzeichen.Dispose()
                ZA_Write(, , Anzeige.CountDown.Replace(":", ""))
                ii = -100
            Else
                If ii > 0 Then
                    Try
                        If ii = WK_Options.DauerAbzeichen Then ZA_Write(ZA_Light, , Anzeige.CountDown.Replace(":", ""))
                        ii -= 1
                    Catch ex As Exception
                    Finally
                        timerAbzeichen.Change(100, Threading.Timeout.Infinite)
                    End Try
                ElseIf ii <= 0 Then
                    timerAbzeichen_Running = False
                    ii = -100
                    ZA_Write(, , Anzeige.CountDown.Replace(":", ""))
                End If
            End If
        End Sub
        Private Sub timerHupe_Tick(state As Object)
            Static ii As Integer = -100
            If ii = -100 Then ii = WK_Options.DauerHupe
            If timerHupe_Running = False Then
                timerHupe.Dispose()
                ZA_Write(, , Anzeige.CountDown.Replace(":", ""))
                ii = -100
            Else
                If ii > 0 Then
                    Try
                        If ii = WK_Options.DauerHupe Then ZA_Write(, ZA_Alert, Anzeige.CountDown.Replace(":", ""))
                        ii -= 1
                    Catch ex As Exception
                    Finally
                        timerHupe.Change(100, Threading.Timeout.Infinite)
                    End Try
                ElseIf ii <= 0 Then
                    timerHupe_Running = False
                    ii = -100
                    ZA_Write(, , Anzeige.CountDown.Replace(":", ""))
                End If
            End If
        End Sub
        Private Sub timerAufruf_Tick(state As Object)

            If timerAufruf_Running = False Then
                timerAufruf.Dispose()
            Else
                Try
                    ZN_Pad.CountDown -= 1
                    Set_CountDown(ZN_Pad.CountDown)
                    Select Case ZN_Pad.CountDown
                        Case 30, 90 'Signal-Hupe 30 Sekunden nach Aufruf und 30 Sekunden vor Ende
                            If ZA_Dev.Connected Then
                                timerHupe_Running = True
                                timerHupe = New Threading.Timer(AddressOf timerHupe_Tick)
                                timerHupe.Change(0, Threading.Timeout.Infinite)
                            End If
                        Case <= 0   'Zeit abgelaufen / Pause beendet
                            timerAufruf_Running = False
                            If ZA_Dev.Connected Then
                                timerHupe_Running = True
                                timerHupe = New Threading.Timer(AddressOf timerHupe_Tick)
                                timerHupe.Change(0, Threading.Timeout.Infinite)
                            End If
                            Dim msg As String = String.Empty
                            Set_Aufruf(False, "Zeit abgelaufen")
                            For i As Integer = 1 To 3
                                If KR_Pad.ContainsKey(i) AndAlso KR_Pad(i).Connected Then
                                    With Ports(i)
                                        .Write({P_SelLine_1}, 0, 1)
                                        .Write(msg)
                                    End With
                                End If
                            Next
                    End Select
                Catch ex As Exception
                    LogMessage("KR-Steuerung: timerAufruf_Tick: CountDown: " & ex.Message, ex.StackTrace)
                End Try
                Try
                    timerAufruf.Change(1000, Threading.Timeout.Infinite)
                Catch ex As Exception
                    LogMessage("KR-Steuerung: timerAufruf_Tick: Change: " & ex.Message, ex.StackTrace)
                End Try
            End If
        End Sub
        Public Sub timerDelay_Tick(state As Object)
            Static ii As Integer = -100
            If ii = -100 Then ii = WK_Options.WertungDelay ' - 1
            If Not timerDelay_Running Then
                timerDelay.Dispose()
                ii = -100
            Else
                If ii > 0 Then
                    Try
                        ii -= 1
                    Catch ex As Exception
                    Finally
                        timerDelay.Change(1000, Threading.Timeout.Infinite)
                    End Try
                ElseIf ii <= 0 Then
                    ii = -100
                    timerDelay_Running = False
                    Using proc As New KR_Pad_Proceed
                        proc.Set_Wertung(Wertung(), True) 'Wertung anzeigen
                    End Using
                End If
            End If
        End Sub
        Public Sub TimerKR_Tick(state As Object) 'sender As Object, e As ElapsedEventArgs) Handles timerKR.Elapsed
            Try
                If KR_Pad.ContainsKey(KR_Fail) AndAlso KR_Pad(KR_Fail).Connected Then
                    If KR_Fail > 0 Then Ports(KR_Fail).Write({P_Beep}, 0, 1)
                End If
            Catch ex As Exception
            End Try
        End Sub

        Private Function JuryEntscheid() As DialogResult
            Dim r As DialogResult = 0
            If Not IsNothing(frmResult) AndAlso frmResult.InvokeRequired Then
                Dim d As New DelegateJuryEntscheid(AddressOf JuryEntscheid)
                frmResult.Invoke(d, New Object() {})
            Else
                Try
                    frmResult.Close()
                    frmResult.Dispose()
                Catch ex As Exception
                End Try

                If IsNothing(frmResult) OrElse frmResult.IsDisposed Then frmResult = New frmJuryEntscheid

                Dim result As DialogResult = DialogResult.None

                RaiseEvent JuryMsg_Change(True)
                If formLeader.InvokeRequired Then
                    Dim d As New DelegateJuryEntscheid(AddressOf JuryEntscheid)
                    formLeader.Invoke(d, New Object() {})
                Else
                    Try
                        result = frmResult.ShowDialog(formLeader)
                    Catch ex As Exception
                    End Try
                End If

                frmResult.Dispose()
                RaiseEvent JuryMsg_Change(False)

                frmResult.Close()
                frmResult.Dispose()

                Return result
            End If
            Return r
        End Function

        Public Sub JB_Write(Optional value As Integer = -1)

            If Not JB_Dev.Connected OrElse Not JB_Dev.Device = DevType.ControlUnit Then Return
            ReDim buf(16)

            With JB_Dev
                If value = JB_Off Then
                    .RevPressed = False
                    For i As Integer = 1 To 3
                        .KR_Pressed(i) = False
                    Next
                Else
                    ' aktuellen Status der Taster übernehmen
                    If .RevPressed Then buf(13) += JB_Rev
                    For i As Integer = 1 To 3
                        If .KR_Pressed(i) Then buf(13) += JB_Key(i)
                    Next
                End If
            End With

            If value = JB_Off Then
                'alles aus 
                buf(12) = JB_Off
            ElseIf value > -1 Then
                'Taster
                If Wettkampf.Technikwertung Then JB_Technik()
                JB_Wertung()
            Else
                JB_Technik()
                JB_Wertung()
            End If

            Dim CRC As Long = 65535 'Anfangswert für die CRC laden
            For x As Integer = 0 To 13 'CRC geht über alle 12 Datenbytes
                CRC = CRC Xor buf(x) 'Aktuelles Datenbyte in die CRC-einbeziehen
                For y As Integer = 1 To 8 'Acht Runden schieben und XOR-en
                    If (CRC Mod 2) > 0 Then
                        CRC = CLng((CRC And 65534) / 2)
                        CRC = CRC Xor 40961 'Polynom der CRC 0xA001 in dezimal ausgedrückt
                    Else
                        CRC = CLng((CRC And 65534) / 2)
                    End If
                Next
            Next

            'Die beiden CRC-Bytes werden an die Nachricht angehängt 
            buf(14) = CByte((CRC And 65280) / 256)
            buf(15) = CByte(CRC And 255)

            Try
                Threading.Thread.Sleep(50)
                With Ports(ID.JB)
                    .Write(buf, 0, 16) 'Gesamtes Paket absenden
                End With
            Catch ex As Exception
            End Try
        End Sub
        Shared Sub JB_Wertung() 'setzt Byte 12 
            If Not JB_Dev.Connected OrElse Not JB_Dev.Device = DevType.ControlUnit Then Return

            'Wertung
            Try
                Dim buffer As Byte
                For i As Integer = 1 To 3
                    If KR_Pad.ContainsKey(i) Then buffer += bytes(KR_Pad(i).Wertung)(i)
                Next
                buf(12) += buffer
            Catch ex As Exception
            End Try
        End Sub
        Shared Sub JB_Technik() 'setzt Byte 0 bis 11 und Steuerung in 12
            If Not JB_Dev.Connected OrElse Not JB_Dev.Device = DevType.ControlUnit Then Return
            'Techniknote
            Dim vals As String = String.Empty
            For x As Integer = 1 To 3
                If KR_Pad.ContainsKey(x) Then
                    'If JB_Test Then 'schaltet beim Initialisieren alle Digits ein
                    '    If IsNothing(KR_Pad(x).Techniknote) Then
                    '        vals += "8888" '"0000" 
                    '    End If
                    If KR_Pad(x).Techniknote = "0" OrElse Not KR_Pad(x).Verified Then
                        'leere Anzeigen für noch nicht gewertet oder nicht ENTER gedrückt
                        vals += "::::"
                    ElseIf InStr(KR_Pad(x).Techniknote, "0") = 1 Then
                        'alle Zahlen < 1
                        If Len(KR_Pad(x).Techniknote) = 2 Then KR_Pad(x).Techniknote += "0"
                        vals += (CDec(KR_Pad(x).Techniknote) * 1).ToString(":000")
                    ElseIf CDec(KR_Pad(x).Techniknote) < 10 Then
                        'ganze Zahlen
                        vals += (CDec(KR_Pad(x).Techniknote) * 100).ToString(":000")
                    ElseIf CDec(KR_Pad(x).Techniknote) > 10 And CDec(KR_Pad(x).Techniknote) <> 100 Then
                        'gebrochene Zahlen
                        If Len(KR_Pad(x).Techniknote) = 2 Then KR_Pad(x).Techniknote += "0"
                        vals += (CDec(KR_Pad(x).Techniknote) * 1).ToString(":000")
                    Else
                        '10
                        If Len(KR_Pad(x).Techniknote) = 3 Then KR_Pad(x).Techniknote = "10"
                        vals += (CDec(KR_Pad(x).Techniknote) * 100).ToString("0000")
                    End If
                Else
                    vals += "::::"
                End If
            Next
            Encoding.ASCII.GetBytes(vals).CopyTo(buf, 0)
        End Sub
        Dim lo As New Object
        Sub ZA_Write(Optional AbZeichen As Byte = 0, Optional Alarm As Byte = 0, Optional Value As String = "")
            SyncLock lo
                Dim val As String
                Dim buf(4) As Byte

                buf(0) += AbZeichen
                buf(0) += Alarm

                If String.IsNullOrEmpty(Value) Then
                    val = ":::" ' Anzeige aus
                Else
                    val = Right(Value, 3)
                    buf(0) += ZA_Colon
                End If

                Encoding.ASCII.GetBytes(val).CopyTo(buf, 1)

                Try
                    'If String.IsNullOrEmpty(Value) Then
                    '    With Ports(ID.ZA)
                    '        .Write(buf, 0, 4)
                    '    End With
                    '    'Threading.Thread.Sleep(100)
                    'End If
                    If Ports.ContainsKey(ID.ZA) AndAlso ZA_Dev.Connected Then
                        'Debug.WriteLine(CStr(buf(0)) + vbNewLine + CStr(buf(1)) + vbNewLine + CStr(buf(2)) + vbNewLine + CStr(buf(3)) + vbNewLine + CStr(buf(4)) + vbNewLine)
                        Ports(ID.ZA).Write(buf, 0, 4)
                    End If
                Catch ex As Exception
                End Try
            End SyncLock
        End Sub

#Region "IDisposable Support"
        Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
                End If

                ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
                ' TODO: große Felder auf Null setzen.
            End If
            disposedValue = True
        End Sub

        ' TODO: Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
        'Protected Overrides Sub Finalize()
        '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
            Dispose(True)
            ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
            ' GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Module
