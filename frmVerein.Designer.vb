﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVerein
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vereinsname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Adresse = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PLZ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ort = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Land = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Staat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTeams = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMauszeiger = New System.Windows.Forms.ToolStripMenuItem()
        Me.cboBundesland = New System.Windows.Forms.ComboBox()
        Me.pnlVerein = New System.Windows.Forms.Panel()
        Me.btnTeams = New System.Windows.Forms.Button()
        Me.chkTeam = New System.Windows.Forms.CheckBox()
        Me.btnEditID = New System.Windows.Forms.Button()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.txtKurz = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtKurz_LV = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtOrt_LV = New System.Windows.Forms.TextBox()
        Me.txtPLZ_LV = New System.Windows.Forms.TextBox()
        Me.txtAdresse_LV = New System.Windows.Forms.TextBox()
        Me.txtName_LV = New System.Windows.Forms.TextBox()
        Me.btnExpand = New System.Windows.Forms.Button()
        Me.lblRegion_3 = New System.Windows.Forms.Label()
        Me.txtOrt = New System.Windows.Forms.TextBox()
        Me.cboStaat = New System.Windows.Forms.ComboBox()
        Me.picFlagge = New System.Windows.Forms.PictureBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblIDVerein = New System.Windows.Forms.Label()
        Me.txtPlz = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtAdresse = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnAccept = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.pnlButtons = New System.Windows.Forms.Panel()
        Me.pnlFilter = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtLand = New System.Windows.Forms.TextBox()
        Me.txtVerein = New System.Windows.Forms.TextBox()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.bs = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.pnlVerein.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.picFlagge, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlButtons.SuspendLayout()
        Me.pnlFilter.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToOrderColumns = True
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.BackgroundColor = System.Drawing.SystemColors.ButtonFace
        Me.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.Vereinsname, Me.Adresse, Me.PLZ, Me.Ort, Me.Land, Me.Staat})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgv.Location = New System.Drawing.Point(0, 242)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowHeadersWidth = 24
        Me.dgv.RowTemplate.ReadOnly = True
        Me.dgv.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellErrors = False
        Me.dgv.ShowEditingIcon = False
        Me.dgv.ShowRowErrors = False
        Me.dgv.Size = New System.Drawing.Size(929, 262)
        Me.dgv.StandardTab = True
        Me.dgv.TabIndex = 0
        '
        'Id
        '
        Me.Id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Id.DataPropertyName = "idVerein"
        Me.Id.HeaderText = "ID"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Width = 60
        '
        'Vereinsname
        '
        Me.Vereinsname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Vereinsname.DataPropertyName = "Vereinsname"
        Me.Vereinsname.HeaderText = "Vereinsname"
        Me.Vereinsname.Name = "Vereinsname"
        Me.Vereinsname.ReadOnly = True
        '
        'Adresse
        '
        Me.Adresse.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Adresse.DataPropertyName = "Adresse"
        Me.Adresse.HeaderText = "Adresse"
        Me.Adresse.Name = "Adresse"
        Me.Adresse.ReadOnly = True
        Me.Adresse.Width = 200
        '
        'PLZ
        '
        Me.PLZ.DataPropertyName = "PLZ"
        Me.PLZ.HeaderText = "PLZ"
        Me.PLZ.Name = "PLZ"
        Me.PLZ.ReadOnly = True
        Me.PLZ.Width = 52
        '
        'Ort
        '
        Me.Ort.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Ort.DataPropertyName = "Ort"
        Me.Ort.HeaderText = "Ort"
        Me.Ort.MinimumWidth = 150
        Me.Ort.Name = "Ort"
        Me.Ort.ReadOnly = True
        Me.Ort.Width = 150
        '
        'Land
        '
        Me.Land.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Land.DataPropertyName = "region_name"
        Me.Land.HeaderText = "Bundesland"
        Me.Land.MinimumWidth = 100
        Me.Land.Name = "Land"
        Me.Land.ReadOnly = True
        '
        'Staat
        '
        Me.Staat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Staat.DataPropertyName = "state_name"
        Me.Staat.HeaderText = "Staat"
        Me.Staat.MinimumWidth = 100
        Me.Staat.Name = "Staat"
        Me.Staat.ReadOnly = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 143)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Bundesland"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuOptionen, Me.mnuExtras})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(929, 24)
        Me.MenuStrip1.TabIndex = 123
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNew, Me.mnuSave, Me.ToolStripMenuItem1, Me.mnuDelete, Me.ToolStripMenuItem2, Me.mnuClose})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 20)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuNew
        '
        Me.mnuNew.Image = Global.Gewichtheben.My.Resources.Resources.Add
        Me.mnuNew.Name = "mnuNew"
        Me.mnuNew.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuNew.Size = New System.Drawing.Size(168, 22)
        Me.mnuNew.Text = "&Neu"
        '
        'mnuSave
        '
        Me.mnuSave.Enabled = False
        Me.mnuSave.Image = Global.Gewichtheben.My.Resources.Resources.saveHS
        Me.mnuSave.Name = "mnuSave"
        Me.mnuSave.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSave.Size = New System.Drawing.Size(168, 22)
        Me.mnuSave.Text = "&Speichern"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(165, 6)
        '
        'mnuDelete
        '
        Me.mnuDelete.Image = Global.Gewichtheben.My.Resources.Resources.Delete
        Me.mnuDelete.Name = "mnuDelete"
        Me.mnuDelete.Size = New System.Drawing.Size(168, 22)
        Me.mnuDelete.Text = "&Löschen"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(165, 6)
        '
        'mnuClose
        '
        Me.mnuClose.Image = Global.Gewichtheben.My.Resources.Resources.Exit_icon
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.mnuClose.Size = New System.Drawing.Size(168, 22)
        Me.mnuClose.Text = "&Beenden"
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuTeams})
        Me.mnuOptionen.Name = "mnuOptionen"
        Me.mnuOptionen.Size = New System.Drawing.Size(69, 20)
        Me.mnuOptionen.Text = "&Optionen"
        Me.mnuOptionen.Visible = False
        '
        'mnuTeams
        '
        Me.mnuTeams.Image = Global.Gewichtheben.My.Resources.Resources.Schema_16x
        Me.mnuTeams.Name = "mnuTeams"
        Me.mnuTeams.Size = New System.Drawing.Size(107, 22)
        Me.mnuTeams.Text = "&Teams"
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMauszeiger})
        Me.mnuExtras.Name = "mnuExtras"
        Me.mnuExtras.Size = New System.Drawing.Size(50, 20)
        Me.mnuExtras.Text = "&Extras"
        '
        'mnuMauszeiger
        '
        Me.mnuMauszeiger.Name = "mnuMauszeiger"
        Me.mnuMauszeiger.ShortcutKeys = System.Windows.Forms.Keys.F4
        Me.mnuMauszeiger.Size = New System.Drawing.Size(186, 22)
        Me.mnuMauszeiger.Text = "&Mauszeiger holen"
        '
        'cboBundesland
        '
        Me.cboBundesland.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBundesland.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBundesland.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboBundesland.FormattingEnabled = True
        Me.cboBundesland.Location = New System.Drawing.Point(81, 140)
        Me.cboBundesland.Name = "cboBundesland"
        Me.cboBundesland.Size = New System.Drawing.Size(184, 21)
        Me.cboBundesland.TabIndex = 30
        '
        'pnlVerein
        '
        Me.pnlVerein.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pnlVerein.Controls.Add(Me.btnTeams)
        Me.pnlVerein.Controls.Add(Me.chkTeam)
        Me.pnlVerein.Controls.Add(Me.btnEditID)
        Me.pnlVerein.Controls.Add(Me.txtID)
        Me.pnlVerein.Controls.Add(Me.txtKurz)
        Me.pnlVerein.Controls.Add(Me.Label2)
        Me.pnlVerein.Controls.Add(Me.GroupBox1)
        Me.pnlVerein.Controls.Add(Me.btnExpand)
        Me.pnlVerein.Controls.Add(Me.lblRegion_3)
        Me.pnlVerein.Controls.Add(Me.txtOrt)
        Me.pnlVerein.Controls.Add(Me.cboBundesland)
        Me.pnlVerein.Controls.Add(Me.cboStaat)
        Me.pnlVerein.Controls.Add(Me.Label1)
        Me.pnlVerein.Controls.Add(Me.picFlagge)
        Me.pnlVerein.Controls.Add(Me.Label15)
        Me.pnlVerein.Controls.Add(Me.lblIDVerein)
        Me.pnlVerein.Controls.Add(Me.txtPlz)
        Me.pnlVerein.Controls.Add(Me.Label18)
        Me.pnlVerein.Controls.Add(Me.txtAdresse)
        Me.pnlVerein.Controls.Add(Me.Label19)
        Me.pnlVerein.Controls.Add(Me.txtName)
        Me.pnlVerein.Controls.Add(Me.Label20)
        Me.pnlVerein.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlVerein.Location = New System.Drawing.Point(0, 24)
        Me.pnlVerein.Name = "pnlVerein"
        Me.pnlVerein.Size = New System.Drawing.Size(929, 175)
        Me.pnlVerein.TabIndex = 503
        '
        'btnTeams
        '
        Me.btnTeams.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnTeams.Enabled = False
        Me.btnTeams.ForeColor = System.Drawing.Color.Firebrick
        Me.btnTeams.Image = Global.Gewichtheben.My.Resources.Resources.Schema_16x
        Me.btnTeams.Location = New System.Drawing.Point(306, 10)
        Me.btnTeams.Name = "btnTeams"
        Me.btnTeams.Size = New System.Drawing.Size(24, 24)
        Me.btnTeams.TabIndex = 605
        Me.btnTeams.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnTeams, "Kampfgemeinschaft bearbeiten")
        Me.btnTeams.UseVisualStyleBackColor = True
        '
        'chkTeam
        '
        Me.chkTeam.AutoSize = True
        Me.chkTeam.BackColor = System.Drawing.SystemColors.HotTrack
        Me.chkTeam.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTeam.ForeColor = System.Drawing.Color.White
        Me.chkTeam.Location = New System.Drawing.Point(256, 12)
        Me.chkTeam.Name = "chkTeam"
        Me.chkTeam.Padding = New System.Windows.Forms.Padding(4, 2, 0, 0)
        Me.chkTeam.Size = New System.Drawing.Size(45, 19)
        Me.chkTeam.TabIndex = 604
        Me.chkTeam.TabStop = False
        Me.chkTeam.Text = "KG"
        Me.ToolTip1.SetToolTip(Me.chkTeam, "Eintrag ist eine Kampfgemeinschaft.")
        Me.chkTeam.UseVisualStyleBackColor = False
        '
        'btnEditID
        '
        Me.btnEditID.BackgroundImage = Global.Gewichtheben.My.Resources.Resources.edit16
        Me.btnEditID.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnEditID.Location = New System.Drawing.Point(187, 10)
        Me.btnEditID.Name = "btnEditID"
        Me.btnEditID.Size = New System.Drawing.Size(24, 24)
        Me.btnEditID.TabIndex = 603
        Me.btnEditID.TabStop = False
        Me.btnEditID.UseVisualStyleBackColor = True
        '
        'txtID
        '
        Me.txtID.Enabled = False
        Me.txtID.Location = New System.Drawing.Point(81, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(100, 20)
        Me.txtID.TabIndex = 11
        '
        'txtKurz
        '
        Me.txtKurz.Location = New System.Drawing.Point(80, 63)
        Me.txtKurz.Margin = New System.Windows.Forms.Padding(2)
        Me.txtKurz.Name = "txtKurz"
        Me.txtKurz.Size = New System.Drawing.Size(185, 20)
        Me.txtKurz.TabIndex = 23
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 65)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Kurzbez."
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtKurz_LV)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtOrt_LV)
        Me.GroupBox1.Controls.Add(Me.txtPLZ_LV)
        Me.GroupBox1.Controls.Add(Me.txtAdresse_LV)
        Me.GroupBox1.Controls.Add(Me.txtName_LV)
        Me.GroupBox1.Location = New System.Drawing.Point(529, 19)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(389, 116)
        Me.GroupBox1.TabIndex = 60
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Landesverband"
        '
        'txtKurz_LV
        '
        Me.txtKurz_LV.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtKurz_LV.Location = New System.Drawing.Point(320, 26)
        Me.txtKurz_LV.Name = "txtKurz_LV"
        Me.txtKurz_LV.Size = New System.Drawing.Size(50, 20)
        Me.txtKurz_LV.TabIndex = 63
        Me.ToolTip1.SetToolTip(Me.txtKurz_LV, "Kurzbezeichnung")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 13)
        Me.Label3.TabIndex = 66
        Me.Label3.Text = "PLZ / Ort"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 55)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 64
        Me.Label4.Text = "Adresse"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(25, 29)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 61
        Me.Label5.Text = "Name"
        '
        'txtOrt_LV
        '
        Me.txtOrt_LV.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOrt_LV.Location = New System.Drawing.Point(108, 78)
        Me.txtOrt_LV.Name = "txtOrt_LV"
        Me.txtOrt_LV.Size = New System.Drawing.Size(262, 20)
        Me.txtOrt_LV.TabIndex = 68
        '
        'txtPLZ_LV
        '
        Me.txtPLZ_LV.Location = New System.Drawing.Point(64, 78)
        Me.txtPLZ_LV.Name = "txtPLZ_LV"
        Me.txtPLZ_LV.Size = New System.Drawing.Size(38, 20)
        Me.txtPLZ_LV.TabIndex = 67
        '
        'txtAdresse_LV
        '
        Me.txtAdresse_LV.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAdresse_LV.Location = New System.Drawing.Point(64, 52)
        Me.txtAdresse_LV.Name = "txtAdresse_LV"
        Me.txtAdresse_LV.Size = New System.Drawing.Size(306, 20)
        Me.txtAdresse_LV.TabIndex = 65
        '
        'txtName_LV
        '
        Me.txtName_LV.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtName_LV.Location = New System.Drawing.Point(64, 26)
        Me.txtName_LV.Name = "txtName_LV"
        Me.txtName_LV.Size = New System.Drawing.Size(250, 20)
        Me.txtName_LV.TabIndex = 62
        '
        'btnExpand
        '
        Me.btnExpand.Image = Global.Gewichtheben.My.Resources.Resources.arrow_right_16
        Me.btnExpand.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnExpand.Location = New System.Drawing.Point(406, 131)
        Me.btnExpand.Name = "btnExpand"
        Me.btnExpand.Padding = New System.Windows.Forms.Padding(2, 0, 0, 0)
        Me.btnExpand.Size = New System.Drawing.Size(105, 23)
        Me.btnExpand.TabIndex = 50
        Me.btnExpand.TabStop = False
        Me.btnExpand.Text = "Landesverband"
        Me.btnExpand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolTip1.SetToolTip(Me.btnExpand, "Landesverband ein-/ausblenden")
        Me.btnExpand.UseCompatibleTextRendering = True
        Me.btnExpand.UseVisualStyleBackColor = True
        Me.btnExpand.Visible = False
        '
        'lblRegion_3
        '
        Me.lblRegion_3.BackColor = System.Drawing.SystemColors.Window
        Me.lblRegion_3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblRegion_3.Location = New System.Drawing.Point(293, 140)
        Me.lblRegion_3.Name = "lblRegion_3"
        Me.lblRegion_3.Size = New System.Drawing.Size(37, 20)
        Me.lblRegion_3.TabIndex = 31
        Me.lblRegion_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtOrt
        '
        Me.txtOrt.Location = New System.Drawing.Point(124, 114)
        Me.txtOrt.Name = "txtOrt"
        Me.txtOrt.Size = New System.Drawing.Size(206, 20)
        Me.txtOrt.TabIndex = 28
        '
        'cboStaat
        '
        Me.cboStaat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboStaat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboStaat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStaat.DropDownWidth = 170
        Me.cboStaat.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboStaat.FormattingEnabled = True
        Me.cboStaat.Location = New System.Drawing.Point(390, 13)
        Me.cboStaat.Name = "cboStaat"
        Me.cboStaat.Size = New System.Drawing.Size(121, 21)
        Me.cboStaat.TabIndex = 41
        '
        'picFlagge
        '
        Me.picFlagge.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.picFlagge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.picFlagge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picFlagge.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.picFlagge.ImageLocation = ""
        Me.picFlagge.Location = New System.Drawing.Point(391, 43)
        Me.picFlagge.Name = "picFlagge"
        Me.picFlagge.Size = New System.Drawing.Size(100, 70)
        Me.picFlagge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picFlagge.TabIndex = 602
        Me.picFlagge.TabStop = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(350, 17)
        Me.Label15.MinimumSize = New System.Drawing.Size(36, 13)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(36, 17)
        Me.Label15.TabIndex = 40
        Me.Label15.Text = "Staat*"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label15.UseCompatibleTextRendering = True
        '
        'lblIDVerein
        '
        Me.lblIDVerein.AutoSize = True
        Me.lblIDVerein.Location = New System.Drawing.Point(59, 15)
        Me.lblIDVerein.Name = "lblIDVerein"
        Me.lblIDVerein.Size = New System.Drawing.Size(22, 13)
        Me.lblIDVerein.TabIndex = 10
        Me.lblIDVerein.Text = "ID*"
        '
        'txtPlz
        '
        Me.txtPlz.Location = New System.Drawing.Point(80, 114)
        Me.txtPlz.Name = "txtPlz"
        Me.txtPlz.Size = New System.Drawing.Size(38, 20)
        Me.txtPlz.TabIndex = 27
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(24, 117)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(52, 13)
        Me.Label18.TabIndex = 26
        Me.Label18.Text = "PLZ / Ort"
        '
        'txtAdresse
        '
        Me.txtAdresse.Location = New System.Drawing.Point(80, 88)
        Me.txtAdresse.Name = "txtAdresse"
        Me.txtAdresse.Size = New System.Drawing.Size(250, 20)
        Me.txtAdresse.TabIndex = 25
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(31, 91)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(45, 13)
        Me.Label19.TabIndex = 24
        Me.Label19.Text = "Adresse"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(80, 38)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(250, 20)
        Me.txtName.TabIndex = 21
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(41, 41)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(39, 13)
        Me.Label20.TabIndex = 20
        Me.Label20.Text = "Name*"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(839, 513)
        Me.btnClose.MinimumSize = New System.Drawing.Size(77, 25)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 25)
        Me.btnClose.TabIndex = 601
        Me.btnClose.TabStop = False
        Me.btnClose.Text = "Schließen"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Enabled = False
        Me.btnSave.Location = New System.Drawing.Point(748, 513)
        Me.btnSave.MaximumSize = New System.Drawing.Size(160, 25)
        Me.btnSave.MinimumSize = New System.Drawing.Size(77, 25)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(85, 25)
        Me.btnSave.TabIndex = 600
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Speichern"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAccept.Location = New System.Drawing.Point(5, 9)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(85, 25)
        Me.btnAccept.TabIndex = 201
        Me.btnAccept.TabStop = False
        Me.btnAccept.Text = "Übernehmen"
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(96, 9)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 202
        Me.btnCancel.TabStop = False
        Me.btnCancel.Text = "Abbrechen"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'pnlButtons
        '
        Me.pnlButtons.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlButtons.Controls.Add(Me.btnCancel)
        Me.pnlButtons.Controls.Add(Me.btnAccept)
        Me.pnlButtons.Location = New System.Drawing.Point(743, 175)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(194, 44)
        Me.pnlButtons.TabIndex = 504
        Me.pnlButtons.Visible = False
        '
        'pnlFilter
        '
        Me.pnlFilter.Controls.Add(Me.Label7)
        Me.pnlFilter.Controls.Add(Me.Label6)
        Me.pnlFilter.Controls.Add(Me.txtLand)
        Me.pnlFilter.Controls.Add(Me.txtVerein)
        Me.pnlFilter.Location = New System.Drawing.Point(7, 209)
        Me.pnlFilter.Name = "pnlFilter"
        Me.pnlFilter.Size = New System.Drawing.Size(480, 31)
        Me.pnlFilter.TabIndex = 602
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(262, 8)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 13)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Bundesland"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Vereinsname"
        '
        'txtLand
        '
        Me.txtLand.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLand.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLand.Location = New System.Drawing.Point(327, 5)
        Me.txtLand.Name = "txtLand"
        Me.txtLand.Size = New System.Drawing.Size(128, 20)
        Me.txtLand.TabIndex = 7
        Me.txtLand.Tag = "Bundesland"
        Me.txtLand.WordWrap = False
        '
        'txtVerein
        '
        Me.txtVerein.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtVerein.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVerein.Location = New System.Drawing.Point(87, 5)
        Me.txtVerein.Name = "txtVerein"
        Me.txtVerein.Size = New System.Drawing.Size(128, 20)
        Me.txtVerein.TabIndex = 5
        Me.txtVerein.Tag = "Vereinsname"
        Me.txtVerein.WordWrap = False
        '
        'btnNew
        '
        Me.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNew.Location = New System.Drawing.Point(22, 513)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(53, 25)
        Me.btnNew.TabIndex = 603
        Me.btnNew.TabStop = False
        Me.btnNew.Text = "Neu"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'frmVerein
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(933, 544)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.pnlFilter)
        Me.Controls.Add(Me.pnlButtons)
        Me.Controls.Add(Me.pnlVerein)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnClose)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(949, 587)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(949, 258)
        Me.Name = "frmVerein"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Stammdaten: Verein"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.pnlVerein.ResumeLayout(False)
        Me.pnlVerein.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.picFlagge, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlButtons.ResumeLayout(False)
        Me.pnlFilter.ResumeLayout(False)
        Me.pnlFilter.PerformLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDatei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNew As ToolStripMenuItem
    Friend WithEvents mnuSave As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents cboBundesland As ComboBox
    Friend WithEvents pnlVerein As Panel
    Friend WithEvents txtOrt As TextBox
    Friend WithEvents cboStaat As ComboBox
    Friend WithEvents picFlagge As PictureBox
    Friend WithEvents Label15 As Label
    Friend WithEvents lblIDVerein As Label
    Friend WithEvents txtPlz As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents txtAdresse As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents txtName As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents btnClose As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents mnuDelete As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents pnlButtons As Panel
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnAccept As Button
    Friend WithEvents pnlFilter As Panel
    Friend WithEvents txtLand As TextBox
    Friend WithEvents txtVerein As TextBox
    Friend WithEvents btnNew As Button
    Friend WithEvents lblRegion_3 As Label
    Friend WithEvents btnExpand As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtOrt_LV As TextBox
    Friend WithEvents txtPLZ_LV As TextBox
    Friend WithEvents txtAdresse_LV As TextBox
    Friend WithEvents txtName_LV As TextBox
    Friend WithEvents txtKurz_LV As TextBox
    Friend WithEvents mnuExtras As ToolStripMenuItem
    Friend WithEvents mnuMauszeiger As ToolStripMenuItem
    Friend WithEvents txtKurz As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtID As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents btnEditID As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents chkTeam As CheckBox
    Friend WithEvents btnTeams As Button
    Friend WithEvents mnuOptionen As ToolStripMenuItem
    Friend WithEvents mnuTeams As ToolStripMenuItem
    Friend WithEvents bs As BindingSource
    Friend WithEvents Id As DataGridViewTextBoxColumn
    Friend WithEvents Vereinsname As DataGridViewTextBoxColumn
    Friend WithEvents Adresse As DataGridViewTextBoxColumn
    Friend WithEvents PLZ As DataGridViewTextBoxColumn
    Friend WithEvents Ort As DataGridViewTextBoxColumn
    Friend WithEvents Land As DataGridViewTextBoxColumn
    Friend WithEvents Staat As DataGridViewTextBoxColumn
End Class
