﻿
Public Class BVDG

    Private dtWK As DataTable
    Private dvWK As DataView
    Private WithEvents bsWK As BindingSource
    Private dtTN As DataTable
    Private dtA As DataTable
    Private dvA As DataView
    Private dtS As DataTable
    Private dvS As DataView
    Private WithEvents bsS As BindingSource
    Private dtV As DataTable
    Private dvV As DataView
    Private WithEvents bsV As BindingSource
    'Private dtM As DataTable
    Private dtExport As DataTable

    'Private dtVerein As DataTable
    Private dvVerein As DataView
    'Private WithEvents bsVerein As BindingSource

    Private Declare Function InternetGetConnectedState Lib "wininet.dll" (ByRef lpSFlags As Integer, ByVal dwReserved As Integer) As Boolean
    Private ConnString As String
    Private Guid As String
    Private cmd As MySqlCommand
    Private Func As New myFunction
    Private _LoginChanged As Boolean

    Private Event Connect_Database()
    Property Target As KeyValuePair(Of String, String) ' Key = Export / Import, Value = Bundesliga / Meisterschaft
    Property WK_ID As String ' "0" = neuer Wettkampf
    Private Property LoginChanged As Boolean
        Get
            Return _LoginChanged
        End Get
        Set(value As Boolean)
            _LoginChanged = value
            btnSaveLogin.Enabled = value
        End Set
    End Property

    Private Query_Meldung, Query_Athlet, InetState As String
    Private dicVereine As New Dictionary(Of String, Integer())
    Private Roles() As String = {"Heim", "Gast", "Gast2"}
    Private Siegpunkte As Dictionary(Of Integer, String())
    Private VereinsnameBVGD As String

    'Enum InetConnState
    '    modem = &H1
    '    lan = &H2
    '    proxy = &H4
    '    ras = &H10
    '    offline = &H20
    '    configured = &H40
    'End Enum
    'Enum Status
    '    Neu = 0                     ' Neuer Datensatz
    '    Ausgelesen = 1              ' Datensatz ausgelesen
    '    Änderung = 2                ' Datensatz wurde geändert
    '    Ergebnis = 3                ' Ergebnis eingetragen
    '    Abgeschlossen = 4           ' Datensatz ausgelesen und geschlossen
    '    Ergebnis_Nachmeldung = 5    ' Ergebnis einer Nachmeldung eingetragen
    'End Enum

    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        If LoginChanged Then
            Using New Centered_MessageBox(Me)
                If MessageBox.Show("Änderung der Server-Anmeldedaten speichern?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    SaveLogin()
                    grpServer.Enabled = False
                End If
            End Using
        End If

        NativeMethods.INI_Write("Import_BVDG", "Location", Location.X & ";" & Location.Y, App_IniFile)

        RemoveHandler Connect_Database, AddressOf Database_Connect

    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        AddHandler Connect_Database, AddressOf Database_Connect

        Dim Pos() As String = Split(NativeMethods.INI_Read("Import_BVDG", "Location", App_IniFile), ";")
        If Pos(0).ToString <> "?" Then
            Location = New Point(CInt(Pos(0)), CInt(Pos(1)))
        End If

        Query_Meldung = "INSERT INTO meldung (Wettkampf, Teilnehmer, Gewicht, Losnummer, Meldedatum, Vereinswertung, Team, idAK, AK, idGK, GK, Wiegen, Reissen, Stossen, Zweikampf, attend, OnlineStatus) " &
                        "VALUES(@wk, @tn, @kg, @los, @md, @vw, @team, @idAK, @AK, @idGK, @GK, @wiegen, @r, @s, @zk, @attd, @status) " &
                        "On DUPLICATE KEY UPDATE Gewicht = @kg, Losnummer = @los, Meldedatum = @md, Vereinswertung = @vw, Team = @team, idAK = @idAK, AK = @AK, idGK = @idGK, GK = @GK, Wiegen = @wiegen, Reissen = @r, Stossen = @s, Zweikampf = @zk, attend = @attd, OnlineStatus = @status;"

        Query_Athlet = "INSERT INTO athleten (idTeilnehmer, GUID, Nachname, Vorname, Verein, ESR, MSR, Jahrgang, Geburtstag, Geschlecht, Staat) " &
                       "VALUES (@tn, @id, @nn, @vn, @verein, @esr, @msr, @jg, @geb, @sex, @staat) " &
                       "ON DUPLICATE KEY UPDATE GUID = @id, Nachname = @nn, Vorname = @vn, Verein = @verein, ESR = @esr, MSR = @msr, Jahrgang = @jg, Geburtstag = @geb, Geschlecht = @sex, Staat = @staat;"
    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Siegpunkte = New Dictionary(Of Integer, String())
        Siegpunkte(0) = {"Heim_1", "Heim_2"}
        Siegpunkte(1) = {"Gast_1", "Gast_3"}
        Siegpunkte(2) = {"Gast2_2", "Gast2_3"}

        If Target.Key.Equals("Export") Then
            btnWettkampf.Text = "Ergebnisse exportieren"
        ElseIf Target.Key.Equals("Import") Then
            btnWettkampf.Text = "Teilnehmer anzeigen"
        End If

        With btnServer
            Dim loc = grpServer.PointToScreen(New Point(.Left, .Top))
            .Location = FindForm.PointToClient(loc)
            .Parent = Me
            .BringToFront()
        End With

        Timer1.Start()
        Cursor = Cursors.Default
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        InetState = CheckInetConnection()
        If Not String.IsNullOrEmpty(InetState) Then
            Timer1.Stop()
            RaiseEvent Connect_Database()
        End If
    End Sub
    Private Sub Database_Connect()

        If Timer1.Enabled Then Return

        Dim dlgResult = DialogResult.None
        Do
            Select Case InetState
                Case "Offline", "Not Connected", "Failed"
                    Using New Centered_MessageBox(Me)
                        dlgResult = MessageBox.Show("Der Vorgang kann nicht abgeschlossen werden." & vbNewLine &
                                                    "(Status der Internetverbindung: " & InetState.ToUpper & ")",
                                                    msgCaption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation)
                    End Using
                    If dlgResult = DialogResult.Retry Then
                        InetState = String.Empty
                        Timer1.Start()
                    Else
                        Close()
                    End If
                    Return
                Case Else
                    Exit Do
            End Select
        Loop

        ConnString = Get_ConnectionString()

        btnWettkampf.Enabled = Not String.IsNullOrEmpty(ConnString)

        'If String.IsNullOrEmpty(ConnString) Then
        '    Using New Centered_MessageBox(Me)
        '        MessageBox.Show("Keine Daten für die Server-Anmeldung.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    End Using
        '    btnServer.PerformClick()
        'Else
        'If ConnString.Equals(User.Passwort) Then
        '    btnServer.PerformClick()
        'End If

        Using conn As New MySqlConnection(User.ConnString)
            If Not conn.State = ConnectionState.Open Then conn.Open()
            ' Athleten einlesen
            cmd = New MySqlCommand("SELECT * FROM athleten;", conn)
            dtA = New DataTable
            dtA.Load(cmd.ExecuteReader())
            ' Staaten einlesen
            cmd = New MySqlCommand("SELECT * FROM staaten;", conn)
            dtS = New DataTable
            dtS.Load(cmd.ExecuteReader())
            ' Staaten einlesen
            cmd = New MySqlCommand("SELECT * FROM verein;", conn)
            dtV = New DataTable
            dtV.Load(cmd.ExecuteReader())
        End Using

        dvA = New DataView(dtA) With {.Sort = "idTeilnehmer"}
        dvS = New DataView(dtS) 'With {.Sort = "state_name"}
        dvV = New DataView(dtV) 'With {.Sort = "Vereinsname"}
        bsS = New BindingSource With {.DataSource = dvS}
        bsV = New BindingSource With {.DataSource = dvV}

        dvVerein = New DataView(dtV, "idVerein > 0", "Vereinsname", DataViewRowState.CurrentRows)
        'bsVerein = New BindingSource With {.DataSource = dvVerein}

        With cboStaaten
            .DataSource = dtS
            .DisplayMember = "state_name"
            .ValueMember = "state_id"
        End With

        ' load dtWK, dvWK, bsWK, set bsWK.Position, fill dgvWK
        If Load_WKsFromCMS() Then
            If WK_ID.Equals("0") OrElse String.IsNullOrEmpty(Wettkampf.GUID) Then
                ' kein WK geöffnet oder WK hat keine GUID
                'bsWK.Filter = "status < 3 and Datum >= '" & Now() & "'"
                bsWK.Filter = "Datum >= '" & DateAdd("d", -1, Now()).ToString & "'"
            Else
                ' WK mit GUID
                Guid = Wettkampf.GUID
                bsWK.Position = bsWK.Find("cas_gguid", Guid)

                If chkIgnoreDate.Checked Then
                    bsWK.Filter = String.Empty
                ElseIf Not IsNothing(Wettkampf.Datum) AndAlso Wettkampf.Datum < Now() Then
                    chkIgnoreDate.Checked = True
                Else
                    'bsWK.Filter = "status < 3 and Datum >= '" & Now() & "'"
                    bsWK.Filter = "Datum >= '" & DateAdd("d", -1, Now()).ToString & "'"
                End If

                If Target.Key.Equals("Import") Then
                    Perform_Import()
                ElseIf Target.Key.Equals("Export") Then
                    chkIgnoreDate.Visible = False
                    Dim IsComplete = (From x In dvResults.ToTable
                                      Where x.Field(Of Integer)("OnlineStatus") = 4).Count = dvResults.Count
                    chkProceedApi.Visible = IsComplete
                    Perform_Export()
                End If
            End If
        End If
    End Sub

    Private Function Get_ConnectionString(Optional password As String = "") As String

        Dim ConnStr = String.Empty
        If String.IsNullOrEmpty(password) Then password = User.Passwort

        If Not String.IsNullOrEmpty(txtServer.Text) AndAlso
            Not String.IsNullOrEmpty(txtUser.Text) AndAlso
            Not String.IsNullOrEmpty(txtDatenbank.Text) Then
            ' Anmeldedaten eingegeben 
            ConnStr = Get_Connection()
        Else
            ' Anmeldedaten leer --> versuche INI 
            Try
                Dim CipherText = NativeMethods.INI_Read("BVDG", "Connection", App_IniFile)
                If CipherText <> "?" AndAlso CipherText <> "" Then
                    ' decoding
                    Dim _Crypter As New Crypter(password)
                    ConnStr = _Crypter.DecryptData(CipherText)
                ElseIf Not String.IsNullOrEmpty(password) Then
                    ' permit access to edit
                    'ConnStr = password
                    txtServer.Text = txtServer.Tag.ToString
                    txtDatenbank.Text = txtDatenbank.Tag.ToString
                    txtPort.Text = txtPort.Tag.ToString
                    txtUser.Text = txtUser.Tag.ToString
                    txtPasswort.Text = txtPasswort.Tag.ToString
                    ConnStr = Get_Connection()
                    LoginChanged = True
                Else
                    ' keine gespeicherte Verbindung vorhanden
                    Using New Centered_MessageBox(Me)
                        If MessageBox.Show("Keine Anmeldedaten für die Datenbank gespeichert." & vbNewLine & "Anmeldedaten jetzt eingeben?", msgCaption,
                                           MessageBoxButtons.OKCancel, MessageBoxIcon.Information) = DialogResult.Cancel Then
                            'Close()
                        Else
                            If Not grpServer.Enabled Then
                                btnServer.PerformClick()
                            Else
                                txtServer.Focus()
                            End If
                        End If
                    End Using
                End If
            Catch ex As Security.Cryptography.CryptographicException
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Die MySQL-Verbindungszeichenfolge " & vbNewLine & "konnte nicht gelesen werden.",
                                    msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
            End Try
        End If

        Return ConnStr

    End Function

    Private Function Get_Connection() As String
        Return String.Join(";", {"server=" + txtServer.Text,
                                 "database=" + txtDatenbank.Text,
                                 "port=" + txtPort.Text,
                                 "userid=" + txtUser.Text,
                                 "password=" + txtPasswort.Text,
                                 "Convert Zero Datetime=True",
                                 "connect timeout=1"}) + ";"
    End Function

    Private Sub Perform_Import()

        Dim dlgResult = DialogResult.None
        Dim tName = String.Empty
        Dim tDate = String.Empty
        Dim msg = String.Empty
        Dim msgIcon = MessageBoxIcon.Exclamation
        Dim msgButtons = MessageBoxButtons.OK

        Cursor = Cursors.WaitCursor

        ' WK eingelesen --> prüfen, ob WK-GUIDs übereinstimmen
        Using conn As New MySqlConnection(ConnString)
            cmd = New MySqlCommand("SELECT * FROM " & LCase(Target.Value) & " WHERE cas_gguid = '" & Guid & "';", conn)
            Try
                conn.Open()
                Dim reader = cmd.ExecuteReader
                If Not reader.HasRows Then
                    msg = "Der zu importierende Wettkampf existiert nicht."
                    Guid = String.Empty
                End If
                reader.Close()
            Catch ex As Exception
                msg = "Fehler bei der Überprüfung der Wettkampf-Daten"
                msgIcon = MessageBoxIcon.Error
                LogMessage("Import_BVDG: Connect: " & ex.Message, ex.StackTrace)
            End Try
        End Using

        If Not String.IsNullOrEmpty(msg) Then
            Using New Centered_MessageBox(Me)
                dlgResult = MessageBox.Show(msg, msgCaption, msgButtons, msgIcon)
                If dlgResult = DialogResult.Cancel Then Guid = String.Empty
            End Using
            If msgIcon = MessageBoxIcon.Error Or String.IsNullOrEmpty(Guid) Then
                Cursor = Cursors.Default
                Close()
                Return
            End If
        End If

        With btnWettkampf
            .Enabled = True
            .PerformClick()
        End With

        Cursor = Cursors.Default
    End Sub
    Private Sub Perform_Export()
        ' Ergebnisse anzeigen
        Cursor = Cursors.WaitCursor

        If Target.Value.Equals("Bundesliga") Then
            dtExport = dvResults.ToTable(False, "Teilnehmer", "Nachname", "Vorname", "KG", "Relativ", "R_1", "RW_1", "R_2", "RW_2", "R_3", "RW_3", "Reissen", "S_1", "SW_1", "S_2", "SW_2", "S_3", "SW_3", "Stossen", "ZK", "Heben", "a_k", "OnlineStatus")
        ElseIf Target.Value.Equals("Meisterschaft") Then
            MessageBox.Show("Diese Funktion wird vom CMS nicht unterstützt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        With dgvHeber
            .DataSource = Nothing
            .Columns.Clear()
            .AutoGenerateColumns = True
            .DataSource = dtExport
            .ScrollBars = ScrollBars.Both
            .DefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor
            .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
            .AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells)
            .Visible = True
            .BringToFront()
            .Focus()
        End With

        btnWettkampf.Enabled = True
        btnImport.Visible = False
        btnExit.Location = btnImport.Location

        Cursor = Cursors.Default
    End Sub

    Private Function Load_WK_Bundesliga() As String
        Dim msg = String.Empty

        Using conn As New MySqlConnection(User.ConnString)
            If NoCompetition() Then
                ' kein WK geladen --> prüfen, ob WK mit ausgewählter GUID existiert
                conn.Open()
                cmd = New MySqlCommand("SELECT Wettkampf from wettkampf where GUID = '" & Guid & "';", conn)
                Dim reader = cmd.ExecuteReader
                If reader.HasRows Then
                    reader.Read()
                    If Not Create_WK_Bundesliga(reader!wettkampf.ToString) Then
                        msg = "Fehler beim Einlesen des Wettkampfs."
                    End If
                ElseIf WK_ID <> "0" Then
                    ' ein WK ausgewählt, aber kein WK mit dieser GUID
                    Using New Centered_MessageBox(Me, {"Anlegen", "Suchen"})
                        Dim dlgResult = MessageBox.Show("Der Wettkampf konnte nicht gefunden werden.",
                                            msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                        If dlgResult = DialogResult.No Then
                            ' WK suchen
                            Dim WK_ID = Func.Wettkampf_Auswahl(Me, "Wettkampf importieren")
                            If Not WK_ID.Key.Equals("0") Then
                                If Wettkampf.Set_Properties(Me, WK_ID.Key) > 0 Then
                                    If String.IsNullOrEmpty(Wettkampf.GUID) Then
                                        ' Update von BVDG und einlesen des WKs
                                        If Not Create_WK_Bundesliga(WK_ID.Key) Then
                                            msg = "Fehler beim Aktualisieren des Wettkampfs."
                                        End If
                                    ElseIf Wettkampf.GUID <> Guid Then
                                        msg = "Der geladene Wettkampf stimmt nicht mit dem zu importierenden überein."
                                    End If
                                Else
                                    msg = "Fehler beim Aktualisieren des Wettkampfs."
                                End If
                            End If
                        ElseIf dlgResult = DialogResult.Yes Then
                            reader.Close()
                            ' WK anlegen
                            cmd = New MySqlCommand("SELECT max(Wettkampf) Id from wettkampf;", conn)
                            reader = cmd.ExecuteReader
                            If reader.HasRows Then
                                reader.Read()
                                Dim maxId As Integer = 0
                                If Not IsDBNull(reader!Id) Then maxId = (CInt(reader!Id))
                                If Not Create_WK_Bundesliga((maxId + 1).ToString) Then
                                    msg = "Fehler beim Anlegen des Wettkampfs."
                                End If
                            End If
                        Else
                            Guid = String.Empty
                        End If
                    End Using
                Else
                    reader.Close()
                    ' WK anlegen
                    cmd = New MySqlCommand("SELECT max(Wettkampf) Id from wettkampf;", conn)
                    reader = cmd.ExecuteReader
                    If reader.HasRows Then
                        reader.Read()
                        Dim maxId As Integer = 0
                        If Not IsDBNull(reader("Id")) Then maxId = (CInt(reader!Id))
                        If Not Create_WK_Bundesliga((maxId + 1).ToString) Then
                            msg = "Fehler beim Anlegen des Wettkampfs."
                        End If
                    End If
                End If
                reader.Close()
            ElseIf String.IsNullOrEmpty(Wettkampf.GUID) Then
                ' in frmMain ausgewählter WK hat keine GUID (könnte manuell angelegt sein) 
                ' --> Abfrage, ob richtiger WK, erfolgt bereits beim Öffnen des Imports
                If Not Create_WK_Bundesliga(Wettkampf.Identities.AsEnumerable(0).Key.ToString) Then
                    msg = "Fehler beim Aktualisieren des Wettkampfs."
                End If
            End If
        End Using

        Return msg
    End Function
    Private Function Create_WK_Bundesliga(WK As String) As Boolean
        ' BL-WK anlegen
        Cursor = Cursors.WaitCursor
        Dim Query = "INSERT INTO wettkampf (wettkampf, Modus, Datum, Veranstalter, Ort, BigDisc, Kampfrichter, Mannschaft, Flagge, Mindestalter, Platzierung, GUID, Finale) " &
                    "VALUES (@wk, @mode, @date, @heim, @ort, @bigdisc, @kr, @team, @flag, @age, @score, @guid, @final) " &
                    "ON DUPLICATE KEY UPDATE Datum = @date, Veranstalter = @heim, Ort = @ort, Finale = @final;"
        Do
            Using conn As New MySqlConnection(User.ConnString)

                cmd = New MySqlCommand(Query, conn)
                With cmd.Parameters
                    .AddWithValue("@wk", WK)
                    .AddWithValue("@mode", 610) ' Mannschaft (Relativ-Abzug) ---> Auswahl
                    .AddWithValue("@date", dvWK(bsWK.Position)!Datum)
                    .AddWithValue("@heim", dgvWK.Rows(bsWK.Position).Cells("Heim_Id").Value) ' kommt nicht von der BVDG-DB
                    .AddWithValue("@ort", dvWK(bsWK.Position)!Ort)
                    .AddWithValue("@bigdisc", False)
                    .AddWithValue("@kr", 1)
                    .AddWithValue("@team", True)
                    .AddWithValue("@flag", False)
                    .AddWithValue("@age", 15) ' ---> aus Modus auslesen
                    .AddWithValue("@score", 1)
                    .AddWithValue("@guid", Guid)
                    .AddWithValue("@final", CBool(dvWK(bsWK.Position)!Gast2_valid))
                End With

                Try
                    If Not conn.State = ConnectionState.Open Then conn.Open()
                    cmd.ExecuteNonQuery()
                    ConnError = False
                Catch ex As Exception
                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                End Try
            End Using
        Loop While ConnError
        If IsNothing(ConnError) OrElse ConnError Then Return False

        Query = "INSERT INTO wettkampf_bezeichnung (Wettkampf, Bez1) " &
                         "VALUES (@wk, @name) " &
                         "ON DUPLICATE KEY UPDATE Bez1 = @name; " &
                "INSERT INTO wettkampf_details (Wettkampf, ZK_m, ZK_w, R_m, S_m, R_w, S_w, Hantelstange, Steigerung1, Steigerung2, Regel20, nurZK, International) " &
                         "VALUES (@wk, @ZK_m, @ZK_w, @R_m, @S_m, @R_w, @S_w, @Hantelstange, @Steigerung1, @Steigerung2, @Regel20, @nurZK, @International) " &
                         "ON DUPLICATE KEY UPDATE Wettkampf = @wk;" &
                "INSERT INTO wettkampf_teams (Wettkampf, Team, Id) " &
                         "VALUES (@wk, @team1, 0), (@wk, @team2, 1) " &
                         If(CBool(dvWK(bsWK.Position)!Gast2_valid), ", (@wk, @team3, 2) ", "") &
                         "ON DUPLICATE KEY UPDATE Team = VALUES (Team), Id = VALUES (Id); " &
                "INSERT INTO gruppen_kampfrichter (Wettkampf, Gruppe, KR_1) " &
                         "VALUES (@wk, 1, @kr1), (@wk, 2, @kr1) " &
                         If(CBool(dvWK(bsWK.Position)!Gast2_valid), ", (@wk, 3, @kr1) ", "") &
                         "ON DUPLICATE KEY UPDATE Gruppe = VALUES (Gruppe), KR_1 = VALUES (KR_1); " &
                "INSERT INTO gruppen (Wettkampf, Gruppe, Datum,  DatumW,  WiegenBeginn,  Reissen, Blockheben, Bohle) " &
                         "VALUES (@wk, 1, @Datum, @DatumW, @Wiegenbeginn, @Reissen, true, 1), (@wk, 2, @Datum, @DatumW, @Wiegenbeginn, @Reissen, true, 1) " &
                         If(CBool(dvWK(bsWK.Position)!Gast2_valid), ", (@wk, 3, @Datum, @DatumW, @Wiegenbeginn, @Reissen, true, 1) ", "") &
                         "ON DUPLICATE KEY UPDATE Gruppe = VALUES (Gruppe), Datum = VALUES (Datum), DatumW =  VALUES (DatumW), " &
                                 "WiegenBeginn = VALUES (WiegenBeginn),  Reissen = VALUES (Reissen), Blockheben = VALUES (Blockheben), Bohle = VALUES (Bohle);" &
               "INSERT INTO wettkampf_mannschaft (Wettkampf, idMannschaft, min, max, gewertet, m_w, multi, relativ) " &
                         "VALUES (@wk, 2, 3, 6, 6, 0, 0, 0)" &
                         "ON DUPLICATE KEY UPDATE Wettkampf = @wk; " &
               "INSERT INTO wettkampf_meldefrist (Wettkampf) " &
                         "VALUES (@wk) " &
                         "ON DUPLICATE KEY UPDATE Wettkampf = @wk;"
        Do
            Using conn As New MySqlConnection(User.ConnString)

                cmd = New MySqlCommand(Query, conn)
                With cmd.Parameters
                    .AddWithValue("@wk", WK)
                    ' gruppen
                    .AddWithValue("@Datum", dvWK(bsWK.Position)!Datum)
                    .AddWithValue("@DatumW", dvWK(bsWK.Position)!Datum)
                    .AddWithValue("@WiegenBeginn", dvWK(bsWK.Position)!Uhrzeit_Wiegen)
                    .AddWithValue("@Reissen", dvWK(bsWK.Position)!Uhrzeit_Wettkampf)
                    ' gruppen_kampfrichter
                    .AddWithValue("kr1", dvWK(bsWK.Position)!Kampfrichter)
                    ' wettkampf_bezeichnung
                    .AddWithValue("@name", dvWK(bsWK.Position)!Name)
                    ' wettkampf_details
                    .AddWithValue("@ZK_m", 0)
                    .AddWithValue("@ZK_w", 0)
                    .AddWithValue("@R_m", 0)
                    .AddWithValue("@S_m", 0)
                    .AddWithValue("@R_w", 0)
                    .AddWithValue("@S_w", 0)
                    .AddWithValue("@Hantelstange", "s")
                    .AddWithValue("@Steigerung1", 1)
                    .AddWithValue("@Steigerung2", 1)
                    .AddWithValue("@Regel20", False)
                    .AddWithValue("@nurZK", False)
                    .AddWithValue("@International", False)
                    ' wettkampf_teams
                    .AddWithValue("@team1", dvWK(bsWK.Position)!Heim_Id)
                    .AddWithValue("@team2", dvWK(bsWK.Position)!Gast_Id)
                    .AddWithValue("@team3", dvWK(bsWK.Position)!Gast2_Id)
                End With
                Try
                    If Not conn.State = ConnectionState.Open Then conn.Open()
                    cmd.ExecuteNonQuery()
                    ConnError = False
                Catch ex As Exception
                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                End Try
            End Using
        Loop While ConnError

        Cursor = Cursors.Default

        If IsNothing(ConnError) OrElse ConnError Then Return False

        Dim _WK = Wettkampf.Set_Properties(Me, WK)
        If _WK > 0 Then
            WK_ID = _WK.ToString
            Text = Split(Text, "(")(0) & "(" & Wettkampf.Bezeichnung & ")"
            Return Glob.Load_Wettkampf_Tables(Me, {WK_ID})
        Else
            Return False
        End If

    End Function
    Private Function Create_WK_Meisterschaft() As Boolean
        Return True
    End Function
    'Private Function CheckInetConnection() As String

    '    Dim lngFlags As Integer

    '    If InternetGetConnectedState(lngFlags, 0&) Then
    '        ' verbunden
    '        If CBool(lngFlags And InetConnState.lan) Then
    '            Return "LAN"
    '        ElseIf CBool(lngFlags And InetConnState.modem) Then
    '            Return "Modem"
    '        ElseIf CBool(lngFlags And InetConnState.configured) Then
    '            Return "Configured"
    '        ElseIf CBool(lngFlags And InetConnState.proxy) Then
    '            Return "Proxy"
    '        ElseIf CBool(lngFlags And InetConnState.ras) Then
    '            Return "RAS"
    '        ElseIf CBool(lngFlags And InetConnState.offline) Then
    '            Return "Offline"
    '        End If
    '    Else
    '        ' nicht verbunden
    '        Return "Not Connected"
    '    End If

    '    Return "Failed"

    'End Function

    Private Function Write_Tables(conn As MySqlConnection, row As DataRow, Query As String,
                                  Optional Vereinswertung As Object = Nothing, Optional Zweikampf As Object = Nothing) As Boolean

        ' Altersklasse bestimmen
        Dim AK = Glob.Get_AK(row!athlet_gebjahr)
        Dim pos = CInt(row!TN_Intern)

        cmd = New MySqlCommand(Query, conn)
        With cmd.Parameters
            ' ---- Athleten anlegen / aktualisieren
            .AddWithValue("@tn", row!athlet_id)
            .AddWithValue("@id", row!athlet_cas_gguid)
            .AddWithValue("@nn", row!athlet_name)
            .AddWithValue("@vn", row!athlet_vorname)
            If pos > -1 Then
                ' Athlet vorhanden
                .AddWithValue("@verein", dvA(pos)!Verein)
                .AddWithValue("@esr", dvA(pos)!ESR)
                .AddWithValue("@geb", dvA(pos)!Geburtstag)
                .AddWithValue("@staat", dvA(pos)!Staat)
            Else
                .AddWithValue("@verein", row!MSR)
                .AddWithValue("@esr", row!MSR)
                .AddWithValue("@geb", row!geburtstag)
                .AddWithValue("@staat", row!Staat)
            End If
            .AddWithValue("@msr", row!MSR)
            .AddWithValue("@jg", row!athlet_gebjahr)
            .AddWithValue("@sex", row!athlet_geschlecht.ToString(0))

            ' ---- Meldung anlegen / aktualisieren
            .AddWithValue("@wk", WK_ID)
            .AddWithValue("@md", row!updatetimestamp)
            .AddWithValue("@los", row!Los_Nr)
            .AddWithValue("@vw", Vereinswertung)
            .AddWithValue("@team", row!MSR)
            .AddWithValue("@r", Get_HLast(row!reissen1))
            .AddWithValue("@s", Get_HLast(row!stossen1))
            .AddWithValue("@zk", Zweikampf)
            .AddWithValue("@kg", row!athlet_gewicht)
            .AddWithValue("@wiegen", row!athlet_gewicht)
            If AK.Key > 0 Then
                If Not IsDBNull(row!athlet_gewicht) Then
                    Dim GK = Glob.Get_GK(row!athlet_geschlecht.ToString(0), AK.Key, CDbl(row!athlet_gewicht)) ' Gewichtsklasse bestimmen
                    .AddWithValue("@idGK", GK.Key)
                    .AddWithValue("@GK", GK.Value)
                Else
                    .AddWithValue("@idGK", DBNull.Value)
                    .AddWithValue("@GK", DBNull.Value)
                End If
                .AddWithValue("@idAK", AK.Key)
                .AddWithValue("@AK", AK.Value)
            Else
                .AddWithValue("@idAK", DBNull.Value)
                .AddWithValue("@AK", DBNull.Value)
                .AddWithValue("@idGK", DBNull.Value)
                .AddWithValue("@GK", DBNull.Value)
            End If
            .AddWithValue("@attd", row!attend)
            .AddWithValue("@status", row!status)
        End With

        Do
            Try
                If Not conn.State = ConnectionState.Open Then conn.Open()
                cmd.ExecuteNonQuery()
                ConnError = False

                ' Status in BVDG-DB auf gelesen setzen
                If CInt(row!status) = 0 Then
                    Using connExt As New MySqlConnection(ConnString)
                        cmd = New MySqlCommand("UPDATE teilnahme_" & If(Target.Value.Equals("Bundesliga"), "bl", "ms") & " SET status = " & Status.Ausgelesen & " WHERE wettkampf_cas_guid = '" & Guid & "' AND athlet_id = " & row!athlet_id.ToString, connExt)
                        Try
                            connExt.Open()
                            cmd.ExecuteNonQuery()
                        Catch ex As Exception
                        End Try
                    End Using
                End If
            Catch ex As Exception
                If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
            End Try
        Loop While ConnError

        Return CBool(ConnError)
    End Function

    Private Function Get_HLast(Value As Object) As Integer?
        If IsDBNull(Value) OrElse CInt(Value) = 0 Then Return Nothing 'DBNull.Value
        Return Math.Abs(CInt(Value))
    End Function

    '' tables: athleten, meldung --> insert || update
    Private Sub Complete_Bundesliga_Athleten()

        Dim LosNr = 0

        dtTN.Columns.Add(New DataColumn With {.ColumnName = "MSR", .DataType = GetType(Integer), .DefaultValue = 0})
        dtTN.Columns.Add(New DataColumn With {.ColumnName = "Verein", .DataType = GetType(String), .DefaultValue = String.Empty})
        dtTN.Columns.Add(New DataColumn With {.ColumnName = "Staat", .DataType = GetType(Integer), .DefaultValue = 0})
        dtTN.Columns.Add(New DataColumn With {.ColumnName = "TN_Intern", .DataType = GetType(Integer), .DefaultValue = False})
        dtTN.Columns.Add(New DataColumn With {.ColumnName = "Los_Nr", .DataType = GetType(Integer), .DefaultValue = DBNull.Value})

        For Each row As DataRow In dtTN.Rows

            Dim pos = dvA.Find(row!athlet_id)
            row!TN_Intern = pos
            LosNr += 1
            row!Los_Nr = LosNr

            'If pos > -1 Then
            '    row!athlet_geschlecht = dvA(pos)!Geschlecht
            '    row!MSR = dvA(pos)!MSR
            '    row!Staat = dvA(pos)!Staat
            'Else
            row!athlet_geschlecht = row!athlet_geschlecht.ToString(0)
            ' Atheten-Verein bestimmen
            Select Case Trim(row!teilnahmerolle.ToString)
                Case "Heim"
                    row!MSR = dvWK(bsWK.Position)!Heim_Id
                Case "Gast"
                    row!MSR = dvWK(bsWK.Position)!Gast_Id
                Case "Gast2"
                    row!MSR = dvWK(bsWK.Position)!Gast2_Id
            End Select
            row!Staat = Glob.Get_StateID(row!athlet_nation, dvS)
            'End If

            ' Atheten-Nation
            If CInt(row!Staat) = 0 Then
                row!athlet_nation = String.Empty
            Else
                row!athlet_nation = Get_StateName(row!Staat)
            End If

            ' Athlet Vereinsname
            pos = bsV.Find("idVerein", row!MSR)
            If pos > -1 Then row!Verein = dvV(pos)!Vereinsname
        Next
    End Sub

    Private Function Get_StateName(Id As Object) As String
        Dim pos = bsS.Find("state_id", Id)
        If pos > -1 Then Return dvS(pos)!state_name.ToString
        Return String.Empty
    End Function

    Private Function Import_Bundesliga_Athleten(Optional OverwriteResults As Boolean = False) As Integer

        Dim Query = Query_Meldung + Query_Athlet
        Dim _table = {"reissen", "stossen"}
        Dim cnt = 0

        Using conn As New MySqlConnection(User.ConnString)
            conn.Open()
            For Each row As DataRow In dtTN.Rows
                If OverwriteResults Then
                    For i = 0 To 1
                        If Not IsDBNull(row(_table(i) & "1")) Then
                            Dim _query = "INSERT INTO " & _table(i) & " (Wettkampf, Teilnehmer, Versuch, HLast, Diff, Steigerung1, Steigerung2, Steigerung3, Wertung) " &
                                "VALUES (@wk, @tn, 1, @h1, @d1, @s1_1, @s1_2, @s1_3, @w1), (@wk, @tn, 2, @h2, @d2, @s2_1, @s2_2, @s2_3, @w2), (@wk, @tn, 3, @h3, @d3, @s3_1, @s3_2, @s3_3, @w3) " &
                                "ON DUPLICATE KEY UPDATE HLast = VALUES(HLast), Diff = VALUES(Diff), Steigerung1 = VALUES(Steigerung1), Steigerung2 = VALUES(Steigerung2), Steigerung3 = VALUES(Steigerung3), Wertung = VALUES(Wertung); " &
                                "INSERT INTO results (Wettkampf, Teilnehmer, " & _table(i) & ", ZK, Heben) VALUES (@wk, @tn, @pkt, @zk, @res) " &
                                "ON DUPLICATE KEY UPDATE " & _table(i) & " = @pkt, ZK = @zk, Heben = @res;"
                            cmd = New MySqlCommand(_query, conn)
                            With cmd.Parameters
                                .AddWithValue("@wk", WK_ID)
                                .AddWithValue("@tn", row!athlet_id)
                                .AddWithValue("@h1", Math.Abs(CInt(row(_table(i) & "1"))))
                                .AddWithValue("@d1", 0)
                                .AddWithValue("@s1_1", Math.Abs(CInt(row(_table(i) & "1"))))
                                .AddWithValue("@s1_2", 0)
                                .AddWithValue("@s1_3", 0)
                                .AddWithValue("@w1", Get_Wertung(UCase(_table(i)).Substring(0, 1) & 1, CInt(row(_table(i) & "1")), row!verzicht.ToString))
                                .AddWithValue("@h2", Math.Abs(CInt(row(_table(i) & "2"))))
                                .AddWithValue("@d2", 1)
                                .AddWithValue("@s2_1", 0)
                                .AddWithValue("@s2_2", 0)
                                .AddWithValue("@s2_3", 0)
                                .AddWithValue("@w2", Get_Wertung(UCase(_table(i)).Substring(0, 1) & 2, CInt(row(_table(i) & "2")), row!verzicht.ToString))
                                .AddWithValue("@h3", Math.Abs(CInt(row(_table(i) & "3"))))
                                .AddWithValue("@d3", 1)
                                .AddWithValue("@s3_1", 0)
                                .AddWithValue("@s3_2", 0)
                                .AddWithValue("@s3_3", 0)
                                .AddWithValue("@w3", Get_Wertung(UCase(_table(i)).Substring(0, 1) & 3, CInt(row(_table(i) & "3")), row!verzicht.ToString))
                                .AddWithValue("@pkt", row(_table(i) & "_punkte"))
                                .AddWithValue("@zk", row!zweikampf)
                                .AddWithValue("@res", row!zweikampf_punkte)
                            End With
                            Try
                                cmd.ExecuteNonQuery()
                                ConnError = False
                            Catch ex As Exception
                                ConnError = True
                            End Try
                        End If
                    Next
                End If

                Dim IsError = Write_Tables(conn, row, Query)
                If Not IsError Then cnt += 1
            Next

            ' Meldungen importiert --> Gruppen festlegen
            Dim upd = "update meldung set Gruppe = @grp where Wettkampf = @wk and Teilnehmer = @tn;"
            Dim sql = "select Team, Teilnehmer, Reissen, Stossen, Gruppe
                       from meldung
                       where Wettkampf = 44 and not isnull(Reissen) and not isnull(Stossen)
                       order by Team, Reissen desc, Stossen desc;"
            cmd = New MySqlCommand(sql, conn)
            Dim tmp As New DataTable
            tmp.Load(cmd.ExecuteReader)

            cmd = New MySqlCommand(upd, conn)
            cmd.Parameters.AddWithValue("@wk", WK_ID)
            cmd.Parameters.AddWithValue("@grp", 0)
            cmd.Parameters.AddWithValue("@tn", 0)
            Dim team, z, grp As Integer

            For Each row As DataRow In tmp.Rows
                If team <> CInt(row!Team) Then
                    team = CInt(row!Team)
                    grp = tmp.Rows.Count \ 6
                    z = 1
                End If
                If z > 3 AndAlso grp > 1 Then grp -= 1
                z += 1
                cmd.Parameters("@tn").Value = row!Teilnehmer
                cmd.Parameters("@grp").Value = grp
                cmd.ExecuteNonQuery()
            Next

        End Using

        Return cnt
    End Function
    Private Sub Import_Meisterschaft_Athleten()
        Dim err = False
        Dim Query = Query_Meldung + Query_Athlet

        Using conn As New MySqlConnection(User.ConnString)
            For Each row As DataRow In dtTN.Rows

                Dim pos = dvA.Find(row!athlet_id)
                If pos > -1 Then
                    '_staat_id = CInt(dvA(pos)!Staat)
                End If

                ' Atheten-Verein suchen
                Dim IndexOf_MinCount As New KeyValuePair(Of Integer, Integer)(-1, 1000000000)
                Dim _verein = 0
                Dim _staat_id = 0 ' bei jedem nicht gefundenen Staat wird "" eingetragen -- alternativ Adjektive von HHP verwenden (Amerikanisch...)
                Dim found = dvV.Find(row!verein)

                If dicVereine.ContainsKey(row!Verein.ToString) Then
                    _verein = dicVereine(row!Verein.ToString)(0)
                    _staat_id = dicVereine(row!Verein.ToString)(1)
                ElseIf found > -1 Then
                    _verein = CInt(dvV(found)!idVerein)
                    _staat_id = CInt(dvV(found)!Staat)
                Else
                    Dim ii As Integer
                    Dim v = Split(row!verein.ToString, " ")
                    Dim tmp As New DataTable
                    For i = 0 To v.Count - 1
                        ii = i
                        Try
                            tmp = (From x In dtV
                                   Where x.Field(Of String)("Vereinsname").Contains(v(ii))).CopyToDataTable

                            If tmp.Rows.Count = 1 Then
                                _verein = CInt(tmp(0)!idVerein)
                                _staat_id = CInt(tmp(0)!Staat)
                                Exit For
                            ElseIf tmp.Rows.Count > 1 Then
                                ' mehrere Vereine gefunden --> Index des Strings, der die kleinste Anzahl an Treffern hat, speichern
                                If tmp.Rows.Count < IndexOf_MinCount.Value Then IndexOf_MinCount = New KeyValuePair(Of Integer, Integer)(i, tmp.Rows.Count)
                            End If
                        Catch ex As Exception
                        End Try
                    Next
                    If _verein = 0 Then
                        ' Verein nicht gefunden
                        With frmVereinSuchen
                            .dtVerein = (From x In dtV
                                         Where x.Field(Of String)("Vereinsname").Contains(v(IndexOf_MinCount.Key))).CopyToDataTable

                            .lblVereinOnline.Text = row!verein.ToString
                            .ShowDialog(Me)
                            If Not .DialogResult = DialogResult.Cancel Then
                                _verein = .Verein
                                _staat_id = .Staat
                                dicVereine.Add(row!verein.ToString, {_verein, _staat_id})
                            End If
                        End With
                    End If
                End If

                ' Vereinswertung festlegen
                Dim _vereinswwertung As Object = DBNull.Value
                If Not IsDBNull(row!vereinswertung) AndAlso Not String.IsNullOrEmpty(row!vereinswertung.ToString) AndAlso Not UCase(row!vereinswertung.ToString(0)).Equals("K") Then
                    If IsNumeric(row!vereinswertung.ToString.Substring(0, 1)) Then
                        _vereinswwertung = CInt(Trim(row!vereinswertung.ToString).Substring(0, 1))
                    End If
                End If






                'Write_Tables(conn, row, Query, _verein, pos, _staat_id, row!gewicht, _vereinswwertung, row!zweikampf_anmeldung)
                err = Write_Tables(conn, row, Query, _vereinswwertung, row!zweikampf_anmeldung)






            Next
        End Using
    End Sub

    Private Function Export_Bundesliga(ByRef IsError As Boolean) As String

        'Dim tbl_A_K = dtExport.Clone()

        Dim Insert_Query = "INSERT INTO teilnahme_bl (
                                wettkampf_cas_guid,
                                athlet_cas_gguid,
                                athlet_name,
                                athlet_vorname,
                                athlet_id,
                                athlet_geschlecht,
                                athlet_gebjahr,
                                athlet_nation,
                                athlet_gewicht,
                                athlet_relativabzug,
                                teilnahmerolle,
                                reissen1,
                                reissen2,
                                reissen3,
                                reissen_punkte,
                                stossen1,
                                stossen2,
                                stossen3,
                                stossen_punkte,
                                zweikampf,
                                zweikampf_punkte,
                                verzicht,
                                updatetimestamp,
                                status
                            ) 
                            VALUES (
                                @wk_guid,
                                @tn_guid,
                                @nn,
                                @vn,
                                @tn,
                                @sex,
                                @yob,
                                @nation,
                                @kg, 
                                @rel,
                                @role,
                                @r1, 
                                @r2, 
                                @r3, 
                                @r, 
                                @s1, 
                                @s2, 
                                @s3, 
                                @s, 
                                @zk, 
                                @h, 
                                @v, 
                                @d, 
                                @st 
                            );"

        Dim Update_Query = "UPDATE teilnahme_bl SET 
                                athlet_gewicht = @kg, 
                                athlet_relativabzug = @rel,
                                reissen1 = @r1, 
                                reissen2 = @r2, 
                                reissen3 = @r3, 
                                reissen_punkte = @r, 
                                stossen1 = @s1, 
                                stossen2 = @s2, 
                                stossen3 = @s3, 
                                stossen_punkte = @s, 
                                zweikampf = @zk, 
                                zweikampf_punkte = @h, 
                                verzicht = @v, 
                                updatetimestamp = @d, 
                                status = @st 
                            WHERE wettkampf_cas_guid = @wk_guid And athlet_id = @tn_guid;"

        Dim comment As String = String.Empty

        Using DS As New DataService
            comment = DS.Get_Wettkampf_Comments({Wettkampf.ID})
        End Using
        If String.IsNullOrEmpty(comment) Then
            comment = Func.Get_Comment(dtResults.Select("a_k = true"))
        End If

        Using InputBox As New Custom_InputBox("Anmerkungen zum Wettkampf sind optional und werden zusammen mit dem Ergebnis übermittelt." &
                                              vbNewLine & vbNewLine & "Schreiben Sie einen Kommentar:",
                                              msgCaption,
                                              comment,,,, True, True, 1000)
            comment = InputBox.Response
        End Using
        If IsNothing(comment) Then
            ' ****** Export abbrechen oder mit leerem Kommentar beenden ?????????????????
            Using New Centered_MessageBox(Me)
                If MessageBox.Show("Export abbrechen?",
                                   msgCaption,
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Question) = DialogResult.Yes Then
                    Close()
                    Return String.Empty
                End If
            End Using
            comment = String.Empty
        ElseIf comment.Length > 1000 Then
            comment = Strings.Left(comment, 1000)
        End If

        Using DS As New DataService
            DS.Save_Wettkampf_Comments({Wettkampf.ID}, comment, Me)
        End Using

        Using conn As New MySqlConnection(ConnString)
            For Each row As DataRow In dtExport.Rows
                Do
                    Try
                        cmd = New MySqlCommand("SELECT count(*) 
                                                FROM teilnahme_bl 
                                                WHERE athlet_id = @tn AND wettkampf_cas_guid = @wk;", conn)
                        Dim IsOnline As Boolean

                        With cmd.Parameters
                            .AddWithValue("@wk", Guid)
                            .AddWithValue("@tn", row!Teilnehmer)
                        End With

                        If Not conn.State = ConnectionState.Open Then conn.Open()

                        Dim reader = cmd.ExecuteReader()
                        If reader.HasRows Then
                            reader.Read()
                            IsOnline = CInt(reader(0)) > 0
                        End If
                        reader.Close()

                        If Not CBool(row!a_k) Then
                            cmd = New MySqlCommand(If(IsOnline, Update_Query, Insert_Query), conn)
                            With cmd.Parameters
                                .AddWithValue("@wk_guid", Guid)
                                If Not IsOnline Then
                                    Dim pos = dvA.Find(row!Teilnehmer)
                                    .AddWithValue("@tn", dvA(pos)!idTeilnehmer)
                                    .AddWithValue("@tn_guid", dvA(pos)!GUID)
                                    .AddWithValue("@nn", dvA(pos)!Nachname)
                                    .AddWithValue("@vn", dvA(pos)!Vorname)
                                    .AddWithValue("@sex", GetSex(dvA(pos)!Geschlecht.ToString))
                                    .AddWithValue("@yob", dvA(pos)!Jahrgang)
                                    .AddWithValue("@nation", Get_StateName(dvA(pos)!Staat))
                                    .AddWithValue("@role", GetRole(CInt(dvA(pos)!MSR)))
                                Else
                                    .AddWithValue("@tn_guid", row!Teilnehmer)
                                End If
                                .AddWithValue("@kg", row!KG)
                                .AddWithValue("@rel", row!Relativ)
                                .AddWithValue("@r1", Get_Result(row!R_1, row!RW_1))
                                .AddWithValue("@r2", Get_Result(row!R_2, row!RW_2))
                                .AddWithValue("@r3", Get_Result(row!R_3, row!RW_3))
                                .AddWithValue("@r", row!Reissen)
                                .AddWithValue("@s1", Get_Result(row!S_1, row!SW_1))
                                .AddWithValue("@s2", Get_Result(row!S_2, row!SW_2))
                                .AddWithValue("@s3", Get_Result(row!S_3, row!SW_3))
                                .AddWithValue("@s", row!Stossen)
                                .AddWithValue("@zk", IIf(IsDBNull(row!ZK), 0, row!ZK))
                                .AddWithValue("@h", IIf(IsDBNull(row!ZK) OrElse CInt(row!ZK) = 0, DBNull.Value, row!Heben)) ' = Relativpunkte
                                .AddWithValue("@v", Set_Verzicht(row))
                                .AddWithValue("@d", Now())
                                .AddWithValue("@st", row!OnlineStatus)
                            End With
                            cmd.ExecuteNonQuery()
                            ConnError = False
                            'Else
                            '    tbl_A_K.ImportRow(row)
                        End If
                    Catch ex As Exception
                        If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit For
                    End Try
                Loop While ConnError
            Next

            If Not ConnError Then
                ' WK auf fertig setzen
                Dim Update_WK = "UPDATE bundesliga SET 
                                 Heim_Ergebnis =  @Heim_Ergebnis, 
                                 Gast_Ergebnis =  @Gast_Ergebnis, 
                                 Gast2_Ergebnis = @Gast2_Ergebnis, 
                                 Heim_1_Punkte =  @Heim_1_Punkte , 
                                 Gast_1_Punkte =  @Gast_1_Punkte , 
                                 Heim_2_Punkte =  @Heim_2_Punkte,
                                 Gast2_2_Punkte = @Gast2_2_Punkte, 
                                 Gast_3_Punkte =  @Gast_3_Punkte, 
                                 Gast2_3_Punkte = @Gast2_3_Punkte, 
                                 Sieger = @Sieger, 
                                 updatetimestamp = @date, 
                                 Kommentar = @comment, 
                                 status = @status 
                                 WHERE cas_gguid = @guid;"
                Dim role = {"Heim", "Gast", "Gast2"}
                Dim Sieger = "unentschieden"
                Dim rows = Glob.dtTeams.Select("Sieger = True")
                If rows.Count = 1 Then Sieger = dtWK(bsWK.Position)(role(CInt(rows(0)!Id))).ToString

                cmd = New MySqlCommand(Update_WK, conn)
                With cmd.Parameters
                    .AddWithValue("@guid", Guid)
                    .AddWithValue("@Heim_Ergebnis", Glob.dtTeams(0)!Heben)
                    .AddWithValue("@Gast_Ergebnis", Glob.dtTeams(1)!Heben)
                    .AddWithValue("@Heim_1_Punkte", Glob.dtTeams(0)!Punkte)
                    .AddWithValue("@Gast_1_Punkte", Glob.dtTeams(1)!Punkte)
                    If Glob.dtTeams.Rows.Count = 3 Then
                        .AddWithValue("@Gast2_Ergebnis", Glob.dtTeams(2)!Heben)
                        .AddWithValue("@Heim_2_Punkte", Glob.dtTeams(0)!Punkte2)
                        .AddWithValue("@Gast2_2_Punkte", Glob.dtTeams(2)!Punkte)
                        .AddWithValue("@Gast_3_Punkte", Glob.dtTeams(1)!Punkte2)
                        .AddWithValue("@Gast2_3_Punkte", Glob.dtTeams(2)!Punkte2)
                    Else
                        .AddWithValue("@Gast2_Ergebnis", DBNull.Value)
                        .AddWithValue("@Heim_2_Punkte", DBNull.Value)
                        .AddWithValue("@Gast2_2_Punkte", DBNull.Value)
                        .AddWithValue("@Gast_3_Punkte", DBNull.Value)
                        .AddWithValue("@Gast2_3_Punkte", DBNull.Value)
                    End If
                    .AddWithValue("@Sieger", Sieger)
                    .AddWithValue("@Date", Now())
                    .AddWithValue("@comment", Strings.Left(comment, 1000))
                    .AddWithValue("@status", Status.Ergebnis)
                End With
                Do
                    Try
                        If Not conn.State = ConnectionState.Open Then conn.Open()
                        cmd.ExecuteNonQuery()
                        ConnError = False
                    Catch ex As Exception
                        If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                    End Try
                Loop While ConnError
            End If
        End Using

        If Not ConnError Then
            btnWettkampf.Enabled = False
            Return "Export erfolgreich abgeschlossen."
        Else
            IsError = True
            Return "Export fehlgeschlagen. Bitte wiederholen."
        End If

    End Function

    Private Function GetRole(Verein As Integer) As String
        Dim found = Glob.dtTeams.Select("Team = " & Verein)
        If found.Length > 0 Then
            If CInt(found(0)!Id) = 0 Then Return "Heim"
            If CInt(found(0)!Id) = 1 Then Return "Gast"
            If CInt(found(0)!Id) = 2 Then Return "Gast2"
        End If
        Return String.Empty
    End Function

    Private Function GetSex(Sex As String) As String
        If Sex.StartsWith("m") Then
            Return "männlich"
        Else
            Return "weiblich"
        End If
    End Function

    Private Function Export_Meisterschaft(ByRef IsErrorReturned As Boolean) As String
        IsErrorReturned = True
        Return "Diese Funktion wird vom CMS nicht unterstützt."
    End Function

    Private Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click

        Dim ImportedCount As Integer
        Dim overWrite = False

        If Target.Value.Equals("Bundesliga") Then
            Dim wk = dvWK(bsWK.Position).Row

            ' WK anlegen/aktualisieren
            Dim msg = Load_WK_Bundesliga()
            If Not msg.Equals(String.Empty) Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
                Return
            End If

            If dtTN.Rows.Count > 0 Then

                If CInt(wk!status) > Status.Änderung Then
                    Using New Centered_MessageBox(Me)
                        overWrite = MessageBox.Show("Sollen die Ergebnisse des Wettkampfes importiert bzw. überschrieben werden?", msgCaption,
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes
                    End Using
                End If

                ImportedCount = Import_Bundesliga_Athleten(overWrite)
            Else
                DialogResult = DialogResult.OK
                Close()
                Return
            End If

            If overWrite Then
                Dim Query = "update wettkampf_teams set Punkte = @punkte, 
                                                       Punkte2 = @punkte2, 
                                                       Reissen = @reissen, 
                                                       Stossen = @stossen, 
                                                         Heben = @heben, 
                                                        Sieger = @sieger " &
                                 "where Wettkampf = @wk and Id = @team;"
                Using conn As New MySqlConnection(User.ConnString)
                    For team = 0 To 1 + Math.Abs(CInt(wk!Gast2_valid))
                        Dim t = team
                        Dim reissen = (From r In dtTN
                                       Where r.Field(Of String)("teilnahmerolle").Equals(Roles(t))
                                       Select r.Field(Of Decimal?)("reissen_punkte")).Sum()
                        Dim stossen = (From r In dtTN
                                       Where r.Field(Of String)("teilnahmerolle").Equals(Roles(t))
                                       Select r.Field(Of Decimal?)("stossen_punkte")).Sum()
                        cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", WK_ID)
                            .AddWithValue("@team", team)
                            .AddWithValue("@punkte", wk(Siegpunkte(team)(0) & "_Punkte"))
                            .AddWithValue("@punkte2", wk(Siegpunkte(team)(1) & "_Punkte"))
                            .AddWithValue("@reissen", reissen)
                            .AddWithValue("@stossen", stossen)
                            .AddWithValue("@heben", wk(Roles(team) & "_Ergebnis"))
                            .AddWithValue("@sieger", wk(Roles(team)).Equals(wk!Sieger))
                        End With
                        Do
                            Try
                                If Not conn.State = ConnectionState.Open Then conn.Open()
                                cmd.ExecuteNonQuery()
                                ConnError = False
                            Catch ex As Exception
                                If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit For
                            End Try
                        Loop While ConnError
                    Next
                    ' dtTeams einlesen um Änderungen anzuzeigen (Publikum, Protokoll)
                    Using ds As New DataService
                        ds.Set_Teams({WK_ID}, conn)
                    End Using
                End Using
            End If

        ElseIf Target.Value.Equals("Meisterschaft") Then
            Import_Meisterschaft_Athleten()
        End If

        Using New Centered_MessageBox(Me)
            If ImportedCount = 0 Then
                MessageBox.Show("Fehler beim Import der Teilnehmer.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf dtTN.Rows.Count > ImportedCount Then
                MessageBox.Show(ImportedCount & " Meldung(en) von " & dtTN.Rows.Count & " angelegt/aktualisiert. (" & dtTN.Rows.Count - ImportedCount & " Fehler)", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                MessageBox.Show(ImportedCount & " Meldung(en) angelegt/aktualisiert.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End Using
    End Sub
    Private Sub btnWettkampf_Click(sender As Object, e As EventArgs) Handles btnWettkampf.Click

        With dgvWK
            If Not .Visible Then
                .Visible = True
                .BringToFront()
                btnWettkampf.Text = "Teilnehmer anzeigen"
                btnImport.Enabled = False
                Return
            End If
        End With

        Dim msgButtons = MessageBoxButtons.OK
        Dim msgIcon = MessageBoxIcon.Information
        Dim msg = String.Empty
        Dim IsError = False
        Cursor = Cursors.WaitCursor

        If LoginChanged Then ConnString = Get_ConnectionString()

        ' ein BVDG-WK der richtigen Kategorie ist ausgewählt
        If Not IsNothing(dvWK) Then Guid = dvWK(bsWK.Position)!cas_gguid.ToString


        If Target.Key.Equals("Import") Then
#Region "Teilnehmer für WK einlesen"
            ' alle TN mit der WK_GUID aus BVDG in dtTN speichern
            Using conn As New MySqlConnection(ConnString)
                Dim Query = "Select t.teilnahme_id, t.cas_gguid, t.wettkampf_cas_guid, t.athlet_cas_gguid, t.athlet_name, t.athlet_vorname, t.athlet_id, t.athlet_geschlecht, t.athlet_gebjahr, 
	                            IfNull(t.athlet_nation, a.athlet_nation) athlet_nation, t.athlet_de_wert, t.athlet_gewicht, t.athlet_relativabzug, t.teilnahmerolle, 
                                t.reissen1, t.reissen2, t.reissen3, t.reissen_punkte, t.stossen1, t.stossen2, t.stossen3, t.stossen_punkte, t.zweikampf, t.zweikampf_punkte, t.verzicht, t.kommentar, t.updatetimestamp, 
                                if(t.`status` < " & Status.Ergebnis & ", " & Status.Ergebnis & ", t.`status`) `status`, 
	                            case 
		                            when t.athlet_gewicht is null then null 
		                            when (t.reissen1 + t.reissen2 + t.reissen3 + t.stossen1 + t.stossen2 + t.stossen3) <> 0 then true
		                            else false
	                            end attend, a.geburtstag " &
                            "FROM teilnahme_" & If(Target.Value.Equals("Bundesliga"), "bl", "ms") & " t " &
                            "LEFT JOIN athleten a ON a.athleten_id = t.athlet_id " &
                            "WHERE t.wettkampf_cas_guid = '" & Guid & "' AND a.startrecht_typ " & If(Target.Value.Equals("Bundesliga"), "> 1 ", "<> 2 ") &
                            "ORDER BY t.teilnahme_id;"
                cmd = New MySqlCommand(Query, conn)

                dtTN = New DataTable
                Try
                    conn.Open()
                    dtTN.Load(cmd.ExecuteReader())
                    ConnError = False
                Catch ex As MySqlException
                    MySQl_Error(Nothing, "Import_BVDG: btnWettkampf: TN_einlesen: " & ex.Message, ex.StackTrace, True)
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Fehler beim Einlesen der Import-Tabelle.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Using
                    Cursor = Cursors.Default
                    Return
                End Try
            End Using

            If Not ConnError Then

                If Target.Value.Equals("Bundesliga") Then
                    Complete_Bundesliga_Athleten()
                ElseIf Target.Value.Equals("Meisterschaft") Then
                    'Import_Meisterschaft_Athleten()
                End If

                dgvWK.Visible = False

                ' Tabelle mit Hebern anzeigen
                With dgvHeber
                    .DataSource = Nothing
                    .AutoGenerateColumns = False
                    .DataSource = dtTN
                    .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
                    .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor
                    .Visible = True
                    .BringToFront()
                    .Focus()
                End With

                Dim IsMissing = Mark_Missing_State()
                btnImport.Enabled = IsMissing = -1
                If IsMissing > -1 Then dgvHeber.FirstDisplayedScrollingRowIndex = IsMissing
            End If

            btnWettkampf.Text = "Wettkämpfe anzeigen" 'Target.Value & " anzeigen"
#End Region

        ElseIf Target.Key.Equals("Export") Then
            ' Ergebnisse exportieren
            If Target.Value.Equals("Bundesliga") Then
                msg = Export_Bundesliga(IsError)
            ElseIf Target.Value.Equals("Meisterschaft") Then
                msg = Export_Meisterschaft(IsError)
            End If

            If IsError Then
                msgButtons = MessageBoxButtons.RetryCancel
                msgIcon = MessageBoxIcon.Exclamation
            End If
        End If

        Cursor = Cursors.Default
        If String.IsNullOrEmpty(msg) Then Return

        Using New Centered_MessageBox(Me)
            MessageBox.Show(msg, msgCaption, msgButtons, msgIcon)
        End Using
    End Sub
    Private Function Get_Wertung(Durchgang As String, Last As Integer, Verzicht As String) As Integer
        Dim _verzicht = Split(Verzicht, ",").ToList
        If _verzicht.Contains(Durchgang) Then Return -2
        If Last < 0 Then Return -1
        If Last > 0 Then Return 1
        Return Nothing
    End Function

    Private Sub Insert_VereinsId()

        Dim GefundeneVereine As New Dictionary(Of String, Integer)

        For Each Role In Roles
            For Each row As DataRow In dtWK.Rows
                Dim verein = row(Role).ToString()

                If GefundeneVereine.ContainsKey(verein) Then
                    row(Role + "_Id") = GefundeneVereine(verein)
                ElseIf Not String.IsNullOrEmpty(verein) Then
                    Dim r = dvVerein.Find(verein)
                    If r = -1 Then
                        Dim splits = verein.Split(CChar(" ")).ToList
                        Dim splitsCopy = verein.Split(CChar(" ")).ToList
                        For Each item In splits
                            If item.Contains(".") OrElse item.Equals(String.Empty) OrElse item.Length < 4 OrElse IsNumeric(item) Then
                                Dim ix = splitsCopy.IndexOf(item)
                                splitsCopy.RemoveAt(ix)
                            End If
                        Next
                        Dim Filter = "Vereinsname LIKE '%" & String.Join("%' or Vereinsname LIKE '%", splitsCopy) & "%'"
                        Dim rows As DataRow() = dtV.Select(Filter)
                        If rows.Length > 1 Then
                        ElseIf rows.Length = 1 Then
                            GefundeneVereine.Add(verein, CInt(rows(0)!idVerein))
                            row(Role + "_Id") = rows(0)!idVerein
                        End If
                    Else
                        row(Role + "_Id") = dvVerein(r)!idVerein
                    End If
                End If
            Next
        Next

    End Sub

    Private Sub Mark_Missing_Verein()
        For pos = 0 To bsWK.Count - 1
            For Each Role In Roles
                If Not dvWK(pos)(Role).Equals(String.Empty) AndAlso IsDBNull(dvWK(pos)(Role & "_Id")) Then
                    dgvWK(dgvWK.Columns(Role).Index, pos).Style.BackColor = Color.Orange
                End If
            Next
        Next
    End Sub
    Private Function Mark_Missing_State() As Integer
        ' markiert alle fehlenden Staaten orange und gibt die Zeile mit dem ersten Vorkommen zurück
        Dim missing = -1
        For pos = 0 To dgvHeber.Rows.Count - 1
            With dgvHeber.Rows(pos).Cells("Staat")
                If IsDBNull(.Value) OrElse .Value.Equals(String.Empty) Then
                    .Style.BackColor = Color.Orange
                    If missing = -1 Then missing = pos
                End If
            End With
        Next
        Return missing
    End Function

    Private Function Load_WKsFromCMS() As Boolean

        If ConnString.Equals(User.Passwort) Then Return False

        Cursor = Cursors.WaitCursor

        dgvHeber.Visible = False
        dtWK = New DataTable

        Using conn As New MySqlConnection(ConnString)
            cmd = New MySqlCommand("Select * FROM " & LCase(Target.Value) & "  ORDER BY Datum;", conn)
            Try
                conn.Open()
                dtWK.Load(cmd.ExecuteReader())
            Catch ex As MySqlException
                MySQl_Error(Nothing, "Import_BVDG: WK_Import_TN: " & ex.Message, ex.StackTrace, True)
                Dim msg = ex.Message
                If ex.Message.StartsWith("Unable to connect") Then
                    msg = "Verbindung zum BVDG-Server nicht möglich."
                End If
                Using New Centered_MessageBox(Me)
                    MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
                WK_ID = String.Empty
                Cursor = Cursors.Default
                Return False
            End Try
        End Using

        dtWK.Columns.AddRange({New DataColumn With {.AllowDBNull = True, .ColumnName = "Heim_Id", .Caption = "Heim ID", .DataType = GetType(Integer)},
                               New DataColumn With {.AllowDBNull = True, .ColumnName = "Gast_Id", .Caption = "Gast ID", .DataType = GetType(Integer)},
                               New DataColumn With {.AllowDBNull = True, .ColumnName = "Gast2_Id", .Caption = "Gast2 ID", .DataType = GetType(Integer)}})

        Insert_VereinsId()

        dvWK = New DataView(dtWK)
        bsWK = New BindingSource With {.DataSource = dvWK}

        If dtWK.Rows.Count > 0 Then
            With dgvWK
                .AutoGenerateColumns = False
                .Columns("wk_name").DataPropertyName = If(Target.Value.Equals("Bundesliga"), "Name", "wk_name") ' --> verschiedene Feldnamen in der BVDG-DB
                .DataSource = bsWK
                .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
                .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor
                .Focus()
            End With

            Mark_Missing_Verein()
        End If

        Cursor = Cursors.Default
        Return True
    End Function

    Private Sub chkIgnoreDate_CheckedChanged(sender As Object, e As EventArgs) Handles chkIgnoreDate.CheckedChanged

        If chkIgnoreDate.Checked Then
            bsWK.Filter = String.Empty
        Else
            'bsWK.Filter = "status < 3 and Datum >= '" & Now() & "'"
            bsWK.Filter = "Datum >= '" & DateAdd("d", -1, Now()).ToString & "'"
        End If

        Mark_Missing_Verein()
    End Sub
    Private Sub SaveLogin_Click(sender As Object, e As EventArgs) Handles btnSaveLogin.Click
        SaveLogin()
    End Sub
    Private Sub SaveLogin()
        ConnString = Get_ConnectionString()
        Dim _Crypter As New Crypter(User.Passwort)
        NativeMethods.INI_Write("BVDG", "Connection", _Crypter.EncryptData(ConnString), App_IniFile)
        LoginChanged = False
        txtServer.Focus()
    End Sub
    Private Sub btnServer_Click(sender As Object, e As EventArgs) Handles btnServer.Click
        If grpServer.Enabled Then
            If LoginChanged Then
                Using New Centered_MessageBox(Me)
                    If MessageBox.Show("Änderung der Server-Anmeldedaten speichern?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        SaveLogin()
                        grpServer.Enabled = False
                    End If
                End Using
            End If
        Else
            'Using InputBox As New Custom_InputBox("Bitte Benutzer-Passwort eingeben:", msgCaption,,,, True)
            '    Dim pw = InputBox.Response
            '    If Not IsNothing(pw) AndAlso Not String.IsNullOrEmpty(pw) Then
            '        If pw.Equals(ConnString) Then
            '            grpServer.Enabled = True
            '            For Each ctl In grpServer.Controls
            '                If TypeOf ctl Is TextBox Then
            '                    Dim txt = CType(ctl, TextBox)
            '                    txt.Text = txt.Tag.ToString
            '                End If
            '            Next
            '            ConnString = Get_ConnectionString(pw)
            '            txtServer.Focus()
            '        Else
            '            Dim items = Split(Get_ConnectionString(pw), ";")
            '            If items.Count > 1 Then
            '                grpServer.Enabled = True
            '                txtServer.Text = Split(items(0), "=")(1)
            '                txtDatenbank.Text = Split(items(1), "=")(1)
            '                txtPort.Text = Split(items(2), "=")(1)
            '                txtUser.Text = Split(items(3), "=")(1)
            '                txtPasswort.Text = Split(items(4), "=")(1)
            '                btnSaveLogin.Enabled = False
            '                txtServer.Focus()
            '            End If
            '            'grpServer.Enabled = False
            '            'LoginChanged = False
            '        End If
            '    End If
            'End Using
            grpServer.Enabled = True
            For Each ctl In grpServer.Controls
                If TypeOf ctl Is TextBox AndAlso Not IsNothing(CType(ctl, TextBox).Tag) Then
                    CType(ctl, TextBox).Text = CType(ctl, TextBox).Tag.ToString
                End If
            Next
            txtServer.Focus()
        End If
    End Sub
    Private Sub btnTestConn_Click(sender As Object, e As EventArgs) Handles btnTestConn.Click
        Dim ConnStr = String.Join(";", {"server=" + txtServer.Text,
                                        "database=" + txtDatenbank.Text,
                                        "port=" + txtPort.Text,
                                        "userid=" + txtUser.Text,
                                        "password=" + txtPasswort.Text,
                                        "Convert Zero Datetime=True",
                                        "connect timeout=1"}) + ";"
        ConnTest(ConnStr, Not String.IsNullOrEmpty(txtPasswort.Text), Me)
        txtServer.Focus()
    End Sub
    Private Sub chkPasswort_CheckedChanged(sender As Object, e As EventArgs) Handles chkPasswort.CheckedChanged
        txtPasswort.UseSystemPasswordChar = Not chkPasswort.Checked
    End Sub

    Private Function Get_Result(Last As Object, Wert As Object) As Object ' rechnet HLast in Gültig/Ungültig und Verzicht als NULL
        If Not IsDBNull(Wert) AndAlso CInt(Wert) > -2 Then
            If Not IsDBNull(Last) Then Return CInt(Last) * CInt(Wert)
        End If
        Return 0
    End Function
    Private Function Set_Verzicht(row As DataRow) As String
        Dim Verzicht = New List(Of String)
        Dim dg = {"RW_", "SW_"}
        For Each d In dg
            For v = 1 To 3
                If Not IsDBNull(row(d & v)) AndAlso CInt(row(d & v)) = -2 Then Verzicht.Add(d.Substring(0, 1) & v)
            Next
        Next
        Return String.Join(",", Verzicht)
    End Function

    Private Sub dgvWK_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvWK.CellDoubleClick
        If e.RowIndex > -1 Then
            btnWettkampf.PerformClick()
        End If
    End Sub
    Private Sub dgvWK_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvWK.CellMouseDown
        If e.Button = MouseButtons.Right Then
            bsWK.Position = e.RowIndex
            If dgvWK(e.ColumnIndex, e.RowIndex).Style.BackColor.Equals(Color.Orange) Then
                dgvWK.ContextMenuStrip = cmnuVerein
                VereinsnameBVGD = dgvWK(e.ColumnIndex, e.RowIndex).Value.ToString
            Else
                dgvWK.ContextMenuStrip = Nothing
                VereinsnameBVGD = String.Empty
            End If
        End If
    End Sub
    Private Sub dgvWK_SelectionChanged(sender As Object, e As EventArgs) Handles dgvWK.SelectionChanged

        If dgvWK.SelectedRows.Count > 0 AndAlso
                    Not IsDBNull(dvWK(bsWK.Position)!Heim_Id) AndAlso
                    Not IsDBNull(dvWK(bsWK.Position)!Gast_Id) AndAlso
                    (Not CBool(dvWK(bsWK.Position)!Gast2_valid) OrElse Not IsDBNull(dvWK(bsWK.Position)!Gast2_Id)) Then
            btnWettkampf.Enabled = True
            dgvWK.DefaultCellStyle.SelectionBackColor = Color.FromKnownColor(KnownColor.Highlight)
            dgvWK.DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.HighlightText)
        Else
            btnWettkampf.Enabled = False
            dgvWK.DefaultCellStyle.SelectionBackColor = Color.Orange
            dgvWK.DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.ControlText)
        End If
    End Sub
    Private Sub dgvWK_Sorted(sender As Object, e As EventArgs) Handles dgvWK.Sorted
        Mark_Missing_Verein()
    End Sub
    Private Sub dgvWK_VisibleChanged(sender As Object, e As EventArgs) Handles dgvWK.VisibleChanged
        chkIgnoreDate.Visible = dgvWK.Visible
    End Sub
    Private Sub dgvHeber_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvHeber.CellMouseDown

        If dgvHeber(e.ColumnIndex, e.RowIndex).Style.BackColor.Equals(Color.Orange) OrElse
           dgvHeber.Columns(e.ColumnIndex).Name.Equals("Staat") AndAlso e.Button = MouseButtons.Right Then

            Dim rect = dgvHeber.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, True)
            Dim loc = dgvHeber.PointToScreen(New Point(rect.X, rect.Y))

            With cboStaaten
                .Location = .FindForm.PointToClient(loc)
                .Visible = True
                .BringToFront()
                .Focus()
            End With
        End If

        If e.Button = MouseButtons.Right Then
        ElseIf e.Button = MouseButtons.Left Then
            'dgvHeber(e.ColumnIndex, e.RowIndex).ReadOnly = Not dgvHeber(e.ColumnIndex, e.RowIndex).Style.BackColor.Equals(Color.Orange)
        End If
    End Sub
    Private Sub dgvHeber_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvHeber.DataError
        Try
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvHeber_SelectionChanged(sender As Object, e As EventArgs) Handles dgvHeber.SelectionChanged
        If IsNothing(dgvHeber.CurrentRow) OrElse Not dgvHeber.Columns.Contains("Staat") Then Return
        If dgvHeber(dgvHeber.Columns("Staat").Index, dgvHeber.CurrentRow.Index).Value.Equals(String.Empty) Then
            dgvHeber.DefaultCellStyle.SelectionBackColor = Color.Orange
            dgvHeber.DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.ControlText)
        Else
            dgvHeber.DefaultCellStyle.SelectionBackColor = Color.FromKnownColor(KnownColor.Highlight)
            dgvHeber.DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.HighlightText)
        End If
    End Sub
    Private Sub dgvHeber_Sorted(sender As Object, e As EventArgs) Handles dgvHeber.Sorted
        Dim IsMissing = Mark_Missing_State()
        If IsMissing > -1 Then dgvHeber.FirstDisplayedScrollingRowIndex = IsMissing
    End Sub
    'Private Sub dgvHeber_VisibleChanged(sender As Object, e As EventArgs) Handles dgvHeber.VisibleChanged
    '    chkIgnoreDate.Visible = Not dgvHeber.Visible
    'End Sub

    Private Sub ServerAnmeldung_TextChanged(sender As Object, e As EventArgs) Handles txtServer.TextChanged, txtPort.TextChanged, txtUser.TextChanged, txtPasswort.TextChanged, txtDatenbank.TextChanged
        'Dim DataExisting = Not String.IsNullOrEmpty(txtServer.Text) AndAlso
        '                   Not String.IsNullOrEmpty(txtPort.Text) AndAlso
        '                   Not String.IsNullOrEmpty(txtUser.Text) AndAlso
        '                   Not String.IsNullOrEmpty(txtDatenbank.Text)
        'AndAlso Not String.IsNullOrEmpty(txtPasswort.Text) 
        'btnSaveLogin.Enabled = DataExisting
        'btnWettkampf.Enabled = DataExisting
        If CType(sender, TextBox).Focused Then LoginChanged = True
    End Sub

    Private Sub grpServer_EnabledChanged(sender As Object, e As EventArgs) Handles grpServer.EnabledChanged
        'If Not grpServer.Enabled Then
        '    txtServer.Text = String.Empty
        '    txtDatenbank.Text = String.Empty
        '    txtPort.Text = String.Empty
        '    txtUser.Text = String.Empty
        '    txtPasswort.Text = String.Empty
        'End If
    End Sub

    'Contextmenü
    Private Sub mnuTeam_Click(sender As Object, e As EventArgs) Handles mnuTeam.Click
        Using formTeams As New frmTeams
            With formTeams
                .TeamName = VereinsnameBVGD
                .ShowDialog(Me)
                If Not .DialogResult = DialogResult.Cancel AndAlso .TeamId > 0 Then
                    ' alle Vorkommen von oldName mit Id ergänzen
                    For Each Role In Roles
                        Dim rows() As DataRow = dtWK.Select(Role & " = '" & VereinsnameBVGD & "'")
                        For Each row As DataRow In rows
                            row(Role & "_Id") = .TeamId
                        Next
                    Next
                    bsWK.ResetBindings(False)
                    Mark_Missing_Verein()
                End If
            End With
        End Using

    End Sub
    Private Sub mnuVerein_Click(sender As Object, e As EventArgs) Handles mnuVerein.Click
        Using formVerein As New frmVerein
            With formVerein
                .Tag = "BVDG"
                .Verein.Vereinsname = VereinsnameBVGD
                If dgvWK.CurrentRow.Cells("Heim").Value.ToString.Equals(VereinsnameBVGD) Then
                    Dim adresse = dgvWK.CurrentRow.Cells("Ort").Value.ToString.Split(CChar(","))
                    Dim ort = adresse(1).Split(CChar(" ")).ToList
                    If ort(0).Equals(String.Empty) Then ort.RemoveAt(0)
                    .Verein.Adresse = Trim(adresse(0))
                    .Verein.PLZ = Trim(ort(0))
                    .Verein.Ort = Trim(ort(1))
                End If
                .ShowDialog(Me)
                If Not .DialogResult = DialogResult.Cancel AndAlso .Verein.Id > 0 Then
                    ' alle Vorkommen von oldName mit Id ergänzen
                    For Each Role In Roles
                        Dim rows() As DataRow = dtWK.Select(Role & " = '" & VereinsnameBVGD & "'")
                        For Each row As DataRow In rows
                            row(Role & "_Id") = .Verein.Id
                        Next
                    Next
                    bsWK.ResetBindings(False)
                    Mark_Missing_Verein()
                End If
            End With
        End Using
    End Sub

    Private Sub cboStaaten_KeyDown(sender As Object, e As KeyEventArgs) Handles cboStaaten.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                e.SuppressKeyPress = True
                cboStaaten.Visible = False
                dgvHeber.Focus()
            Case Keys.Enter
                If Not IsNothing(cboStaaten.SelectedItem) Then Staat_Übernehmen()
        End Select
    End Sub
    Private Sub cboStaaten_VisibleChanged(sender As Object, e As EventArgs) Handles cboStaaten.VisibleChanged
        If cboStaaten.Visible Then
            CancelButton = Nothing
        Else
            CancelButton = btnExit
        End If
    End Sub

    Private Sub chkProceedApi_CheckedChanged(sender As Object, e As EventArgs) Handles chkProceedApi.CheckedChanged
        Cursor = Cursors.WaitCursor
        For Each row As DataRow In dtExport.Rows
            row!OnlineStatus = 4 - Math.Abs(CInt(chkProceedApi.Checked))
        Next
        dtExport.AcceptChanges()
        Cursor = Cursors.Default
    End Sub

    Private Sub cboStaaten_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboStaaten.SelectionChangeCommitted
        Staat_Übernehmen()
    End Sub

    Private Sub Staat_Übernehmen()
        Dim row = dtTN.Select("athlet_id = " & dgvHeber.CurrentRow.Cells("Id").Value.ToString)
        If row.Count = 1 Then
            row(0)!athlet_nation = DirectCast(cboStaaten.SelectedItem, DataRowView)!state_name
            row(0)!Staat = cboStaaten.SelectedValue
        End If
        cboStaaten.Visible = False
        With dgvHeber
            .CurrentRow.Cells("Staat").Style.BackColor = If(.CurrentRow.Index Mod 2 = 0, RowsDefaultCellStyle_BackColor, AlternatingRowsDefaultCellStyle_BackColor)
            .DefaultCellStyle.SelectionBackColor = Color.FromKnownColor(KnownColor.Highlight)
            .DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.HighlightText)
            .Focus()
        End With
        btnImport.Enabled = (From x In dtTN
                             Where x.Field(Of Integer)("Staat") = 0).Count = 0
    End Sub

End Class