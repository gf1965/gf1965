﻿
Imports MySqlConnector

Public Class frmVerein
    Public Class myVerein
        Property Id As Integer
        Property Vereinsname As String
        Property Kurzname As String
        Property Region As Integer
        Property Staat As Integer
        Property Adresse As String
        Property PLZ As String
        Property Ort As String
        Property region_name As String
        Property region_3 As String
        Property state_name As String
    End Class
    Public Verein As New myVerein

    Dim Bereich As String = "Verein"
    Dim Query As String
    Dim cmd As MySqlCommand
    Dim dtVerein As New DataTable
    Dim dtRegion As New DataTable
    Dim dtStaat As New DataTable
    'Private WithEvents bs As BindingSource
    Dim bsL As BindingSource
    Dim dv As DataView
    Dim dvL As DataView
    Dim nonNumberEntered As Boolean

    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If mnuSave.Enabled AndAlso (IsNothing(Tag) OrElse Not Tag.ToString.ToUpper.Equals("BVDG")) Then
            Using New Centered_MessageBox(Me)
                Dim result As DialogResult = MessageBox.Show("Änderungen an den Stammdaten speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                If result = DialogResult.Yes Then
                    mnuSave.PerformClick()
                ElseIf result = DialogResult.Cancel Then
                    e.Cancel = True
                    Cursor = Cursors.Default
                Else
                    Tag = Nothing
                End If
            End Using
        End If
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load

        Cursor = Cursors.WaitCursor

        If Not IsNothing(Tag) Then
            If Tag.ToString.ToLower.Equals("new") Then
                MenuStrip1.Visible = False
                pnlFilter.Visible = False
                dgv.Visible = False
                pnlButtons.Visible = True
                Height = MinimumSize.Height
                Width = MinimumSize.Width
                btnExpand.Visible = True
                CancelButton = btnCancel
            End If
        End If
    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                Query = "SELECT v.*, r.region_name, r.region_3, s.state_name, " &
                            "(
	                        select distinct count(*) 
	                        from teams 
	                        where idTeam = v.idVerein
                            ) TeamMembers " &
                        "FROM verein v " &
                        "LEFT JOIN region r ON v.Region = r.region_id " &
                        "LEFT JOIN staaten s ON v.Staat = s.state_id " &
                        "WHERE v.idVerein > 0 " &
                        "ORDER BY v.Vereinsname;"
                cmd = New MySqlCommand(Query, conn)
                dtVerein.Load(cmd.ExecuteReader())

                Query = "select * from region -- where region_id > 0;"
                cmd = New MySqlCommand(Query, conn)
                dtRegion.Load(cmd.ExecuteReader())

                Query = "Select state_id, state_name, state_flag From staaten Order by state_name;"
                cmd = New MySqlCommand(Query, conn)
                dtStaat.Load(cmd.ExecuteReader())

            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
                ConnError = True
            End Try
        End Using

        If Not ConnError Then

            'dtVerein.Columns("Kurzname").AllowDBNull = True
            'dtVerein.Columns("Vereinsname").AllowDBNull = True
            'dtVerein.Columns("Staat").AllowDBNull = True

            Build_Grid(Bereich)

            dv = New DataView(dtVerein) ', "Region > 0", Nothing, DataViewRowState.CurrentRows)
            bs = New BindingSource With {.DataSource = dv}
            dgv.DataSource = bs

            dvL = New DataView(dtRegion)
            bsL = New BindingSource With {.DataSource = dvL, .Sort = "region_name"}
            With cboBundesland
                .DataSource = bsL
                .DisplayMember = "region_name"
                .ValueMember = "region_id"
            End With

            With cboStaat
                .DataSource = dtStaat
                .DisplayMember = "state_name"
                .ValueMember = "state_id"
            End With

            txtID.DataBindings.Add(New Binding("Text", bs, "idVerein", False, DataSourceUpdateMode.OnPropertyChanged))
            txtName.DataBindings.Add(New Binding("Text", bs, "Vereinsname", False, DataSourceUpdateMode.OnPropertyChanged))
            txtKurz.DataBindings.Add(New Binding("Text", bs, "Kurzname", False, DataSourceUpdateMode.OnPropertyChanged))
            txtAdresse.DataBindings.Add(New Binding("Text", bs, "Adresse", False, DataSourceUpdateMode.OnPropertyChanged))
            txtPlz.DataBindings.Add(New Binding("Text", bs, "PLZ", False, DataSourceUpdateMode.OnPropertyChanged))
            txtOrt.DataBindings.Add(New Binding("Text", bs, "Ort", False, DataSourceUpdateMode.OnPropertyChanged))
            cboBundesland.DataBindings.Add(New Binding("SelectedValue", bs, "Region", False, DataSourceUpdateMode.OnPropertyChanged))
            lblRegion_3.DataBindings.Add(New Binding("Text", bsL, "region_3", False, DataSourceUpdateMode.OnPropertyChanged))
            cboStaat.DataBindings.Add(New Binding("SelectedValue", bs, "Staat", False, DataSourceUpdateMode.OnPropertyChanged))

            txtName_LV.DataBindings.Add(New Binding("Text", bsL, "Bezeichnung", False, DataSourceUpdateMode.OnPropertyChanged))
            txtKurz_LV.DataBindings.Add(New Binding("Text", bsL, "Kurz", False, DataSourceUpdateMode.OnPropertyChanged))
            txtAdresse_LV.DataBindings.Add(New Binding("Text", bsL, "Adresse", False, DataSourceUpdateMode.OnPropertyChanged))
            txtPLZ_LV.DataBindings.Add(New Binding("Text", bsL, "PLZ", False, DataSourceUpdateMode.OnPropertyChanged))
            txtOrt_LV.DataBindings.Add(New Binding("Text", bsL, "Ort", False, DataSourceUpdateMode.OnPropertyChanged))

            chkTeam.DataBindings.Add(New Binding("Checked", bs, "Team", False, DataSourceUpdateMode.OnPropertyChanged))

            If Not IsNothing(Tag) Then
                ' Aufruf aus anderer Form
                If Tag.ToString.ToLower.Equals("new") Then
                    Text = "Verein anlegen"
                    mnuNew.PerformClick()
                ElseIf Tag.ToString.ToUpper.Equals("BVDG") Then
                    btnClose.Text = "Abbrechen"
                    btnClose.DialogResult = DialogResult.Cancel
                    CancelButton = btnClose
                    mnuSave.Text = "Übernehmen"
                    mnuSave.Enabled = True
                    btnSave.Text = "Übernehmen"
                    ToolTip1.SetToolTip(btnSave, "Auswahl übernehmen und schließen")
                    Text = Verein.Vereinsname
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Tipp:" & vbNewLine & "Überprüfen, ob der angegebene Verein mit anderer Schreibweise bereits existiert.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Using
                    dgv.Focus()
                Else
                    ' ausgewählter Verein
                    Text = "Verein - Stammdaten bearbeiten"
                    bs.Position = bs.Find("idVerein", Tag)
                    mnuSave.Enabled = False
                End If
            End If
        End If

        If bs.Count = 0 Then btnNew.PerformClick()
        txtName.Focus()
        Cursor = Cursors.Default
    End Sub

    Private Sub Build_Grid(Bereich As String)
        With dgv
            .AutoGenerateColumns = False
            .AlternatingRowsDefaultCellStyle.BackColor = Color.GhostWhite
        End With
        'Format_Grid(Bereich, grdVerein)
    End Sub

    Private Sub mnuClose_Click(sender As Object, e As EventArgs) Handles mnuClose.Click, btnClose.Click
        Close()
    End Sub
    Private Sub mnuDelete_Click(sender As Object, e As EventArgs) Handles mnuDelete.Click
        dv(bs.Position).Delete()
        mnuSave.Enabled = True
    End Sub

    '' Aufruf aus anderer Form --> Return
    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        mnuSave.PerformClick()
        If ConnError Then Return

        ' successfully saved, return to calling form
        DialogResult = DialogResult.OK

        With Verein
            .Id = CInt(dv(bs.Position)!idVerein)
            '.ESR = CInt(dv(bs.Position)!ESR)
            '.MSR = CInt(dv(bs.Position)!MSR)
            .Vereinsname = dv(bs.Position)!Vereinsname.ToString
            .Kurzname = dv(bs.Position)!Kurzname.ToString
            .Region = CInt(dv(bs.Position)!Region)
            .Staat = CInt(dv(bs.Position)!Staat)
            .Adresse = dv(bs.Position)!Adresse.ToString
            .PLZ = dv(bs.Position)!PLZ.ToString
            .Ort = dv(bs.Position)!Ort.ToString
            .region_name = dv(bs.Position)!region_name.ToString
            .region_3 = dv(bs.Position)!region_3.ToString
            .state_name = dv(bs.Position)!state_name.ToString
        End With

        Close()
    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Tag = Nothing
    End Sub

    'Private Function Get_LastId() As Integer
    '    Dim id As Integer
    '    Using conn As New MySqlConnection(User.ConnString)
    '        Try
    '            conn.Open()
    '            cmd = New MySqlCommand("select max(idVerein) id from verein;", conn)
    '            Dim reader = cmd.ExecuteReader()
    '            If reader.HasRows Then
    '                reader.Read()
    '                id = CInt(reader!id)
    '            End If
    '            reader.Close()
    '        Catch ex As MySqlException
    '            MessageBox.Show(ex.Message)
    '        End Try
    '    End Using
    '    Return id
    'End Function

    '' Buttons
    Private Sub btnExpand_Click(sender As Object, e As EventArgs) Handles btnExpand.Click
        If Width = MaximumSize.Width Then
            Width = MinimumSize.Width
            btnExpand.Image = arrow_right_16
        Else
            Width = MaximumSize.Width
            btnExpand.Image = arrow_left_16
        End If
        GroupBox1.Enabled = Width = MaximumSize.Width
    End Sub
    Private Sub mnuNew_Click(sender As Object, e As EventArgs) Handles mnuNew.Click, btnNew.Click

        Dim ItemArray = New List(Of Object)

        Using GF As New myFunction
            ItemArray.Add(GF.Get_LastId("verein", "idVerein") + 1)
        End Using

        If Not IsNothing(Tag) AndAlso Tag.ToString.ToUpper.Equals("BVDG") Then
            ItemArray.Add(Verein.Vereinsname)
            ItemArray.Add(String.Empty)
            ItemArray.Add(0)
            ItemArray.Add(0)
            ItemArray.Add(Verein.Adresse)
            ItemArray.Add(Verein.PLZ)
            ItemArray.Add(Verein.Ort)
            ItemArray.Add(False)
            mnuSave.Text = "Speichern"
            btnSave.Text = "Speichern"
            ToolTip1.SetToolTip(btnSave, "Speichern und schließen")
        Else
            ItemArray.Add(String.Empty)
            ItemArray.Add(String.Empty)
            ItemArray.Add(0)
            ItemArray.Add(0)
            ItemArray.Add(DBNull.Value)
            ItemArray.Add(DBNull.Value)
            ItemArray.Add(DBNull.Value)
            ItemArray.Add(False)
        End If

        Dim NewRow = dtVerein.NewRow()
        NewRow.ItemArray = ItemArray.ToArray()
        dtVerein.Rows.Add(NewRow)
        bs.Position = bs.Find("idVerein", ItemArray(0))

        'lblRegion_3.Text = String.Empty
        txtID.Enabled = True
        mnuNew.Enabled = False
        mnuSave.Enabled = True
        txtName.Focus()
    End Sub
    Private Sub mnuNew_EnabledChanged(sender As Object, e As EventArgs) Handles mnuNew.EnabledChanged
        btnNew.Enabled = mnuNew.Enabled
    End Sub
    Private Sub mnuSave_Click(sender As Object, e As EventArgs) Handles mnuSave.Click, btnSave.Click
        Validate()
        bs.EndEdit()
        bsL.EndEdit()

        Dim id = "1"
        If mnuSave.Text.Equals("Übernehmen") Then id = txtID.Text

        Using conn As New MySqlConnection((User.ConnString))
            Dim changes As DataTable = dtVerein.GetChanges()
            If Not IsNothing(changes) Then
                For Each row As DataRow In changes.Rows

                    If String.IsNullOrEmpty(row!Vereinsname.ToString()) Then
                        Using New Centered_MessageBox(Me)
                            MessageBox.Show("Name des Vereins nicht angegeben.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End Using
                        txtName.Focus()
                        Cursor = Cursors.Default
                        Return
                    End If
                    If IsNothing(cboStaat.SelectedValue) OrElse CInt(row!Staat) < 1 Then
                        Using New Centered_MessageBox(Me)
                            MessageBox.Show("Nation (Staat) nicht ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End Using
                        cboStaat.Focus()
                        Cursor = Cursors.Default
                        Return
                    End If
                    If CBool(row!Team) AndAlso Not row!Team.Equals(row!TeamMembers) Then
                        Dim msg = String.Empty
                        Dim btns = {""}
                        If CBool(row!Team) AndAlso Not CBool(row!TeamMembers) Then
                            msg = "Keine Mitglieder für die Kampfgemeinschaft ausgewählt," & vbNewLine & "aber als KG markiert."
                            btns = {"Bearbeiten", "Ist keine KG"}
                        ElseIf Not CBool(row!Team) AndAlso CBool(row!TeamMebers) Then
                            msg = row!TeamMembers.ToString() & " Mitglieder für die Kampfgemeinschaft vorhanden," & vbNewLine & "aber nicht als KG markiert."
                            btns = {"Bearbeiten", "Ist eine KG"}
                        End If
                        Using New Centered_MessageBox(Me, btns)
                            If MessageBox.Show(msg, msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.No Then
                                row!Team = Not (CBool(row!Team))
                                Dim r = dtVerein.Select("idVerein = " & row!idVerein.ToString())
                                r(0)!Team = row!Team
                            Else
                                Cursor = Cursors.Default
                                Return
                            End If
                        End Using
                    End If

                    Cursor = Cursors.WaitCursor

                    If row.RowState = DataRowState.Deleted Then
                        Query = "DELETE FROM verein WHERE idVerein = @id;"
                    ElseIf Not row.RowState = DataRowState.Added AndAlso row("idVerein", DataRowVersion.Original).ToString.Equals(row!idVerein.ToString) Then
                        Query = "UPDATE verein SET idVerein = @id, Vereinsname = @name, Kurzname = @kurz, Region = @region, Staat = @staat, Adresse = @adr, PLZ = @plz, Ort = @ort, Team = @team WHERE idVerein = @idold;"
                    Else
                        Query = "INSERT INTO verein (idVerein, Vereinsname, Kurzname, Region, Staat, Adresse, PLZ, Ort, Team) VALUES (@id, @name, @kurz, @region, @staat, @adr, @plz, @ort, @team) " &
                                "ON DUPLICATE KEY UPDATE Vereinsname = @name, Kurzname = @kurz, Region = @region, Staat = @staat, Adresse = @adr, PLZ = @plz, Ort = @ort, Team = @team;"
                    End If
                    cmd = New MySqlCommand(Query, conn)
                    With cmd.Parameters
                        If row.RowState = DataRowState.Deleted Then
                            .AddWithValue("@id", row("idVerein", DataRowVersion.Original))
                        Else
                            If row.RowState = DataRowState.Added Then
                                .AddWithValue("@idold", row!idVerein)
                            Else
                                .AddWithValue("@idold", row("idVerein", DataRowVersion.Original))
                            End If
                            .AddWithValue("@id", row!idVerein)
                            .AddWithValue("@name", row!Vereinsname)
                            .AddWithValue("@kurz", IIf(IsDBNull(row!Kurzname), String.Empty, row!Kurzname))
                            .AddWithValue("@region", row!Region)
                            .AddWithValue("@staat", row!Staat)
                            .AddWithValue("@adr", row!Adresse)
                            .AddWithValue("@plz", row!PLZ)
                            .AddWithValue("@ort", row!Ort)
                            .AddWithValue("@team", row!Team)
                        End If
                    End With
                    Do
                        Try
                            If conn.State = ConnectionState.Closed Then conn.Open()
                            cmd.ExecuteNonQuery()
                            ConnError = False
                            If Not row.RowState = DataRowState.Deleted Then id = row!idVerein.ToString
                            If Not row.RowState = DataRowState.Added AndAlso row("idVerein", DataRowVersion.Original).ToString.Equals(txtID.Tag) Then txtID.Enabled = False
                        Catch ex As Exception
                            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                        End Try
                    Loop While ConnError
                Next
            End If

            If Not ConnError Then
                changes = dtRegion.GetChanges
                If Not IsNothing(changes) Then
                    Query = "UPDATE region SET Bezeichnung = @name, Kurz = @kurz, Adresse = @adr, PLZ = @plz, Ort = @ort WHERE region_id = @id;"
                    For Each row As DataRow In changes.Rows
                        cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            .AddWithValue("@id", row!region_id)
                            .AddWithValue("@name", row!Bezeichnung)
                            .AddWithValue("@kurz", row!Kurz)
                            .AddWithValue("@adr", row!Adresse)
                            .AddWithValue("@plz", row!PLZ)
                            .AddWithValue("@ort", row!Ort)
                        End With
                        Do
                            Try
                                If conn.State = ConnectionState.Closed Then conn.Open()
                                cmd.ExecuteNonQuery()
                                ConnError = False
                            Catch ex As Exception
                                If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                            End Try
                        Loop While ConnError
                    Next
                End If
            End If
        End Using

        Cursor = Cursors.Default

        If ConnError Then Return

        dtVerein.AcceptChanges()
        dtRegion.AcceptChanges()

        mnuSave.Enabled = False

        If Not IsNothing(Tag) AndAlso Tag.ToString.ToUpper.Equals("BVDG") Then
            Verein.Id = CInt(id)
            DialogResult = DialogResult.OK
            Close()
            Return
        End If

        bs.ResetBindings(False)
        Dim pos = bs.Find("idVerein", id)
        bs.Position = pos

        dgv.Focus()
    End Sub
    Private Sub mnuSave_EnabledChanged(sender As Object, e As EventArgs) Handles mnuSave.EnabledChanged
        btnSave.Enabled = mnuSave.Enabled
        btnAccept.Enabled = mnuSave.Enabled
    End Sub

    '' Filter
    Private Sub txtFilter_GotFocus(sender As Object, e As EventArgs) Handles txtVerein.GotFocus, txtLand.GotFocus
        CancelButton = Nothing
        dgv.ClearSelection()
    End Sub
    Private Sub txtFilter_LostFocus(sender As Object, e As EventArgs) Handles txtVerein.LostFocus, txtLand.LostFocus
        CancelButton = btnCancel
    End Sub
    Private Sub txtFilter_KeyDown(sender As Object, e As KeyEventArgs) Handles txtVerein.KeyDown, txtLand.KeyDown
        If IsNothing(bs) Then Return

        With CType(sender, TextBox)
            Select Case e.KeyCode
                Case Keys.Escape
                    e.Handled = True
                    e.SuppressKeyPress = True
                    .Text = String.Empty
                    bs.Filter = .Text
                    dgv.Focus()
                Case Keys.Enter
                    dgv.Focus()
                Case Else
                    bs.Filter = .Tag.ToString & " Like '*" & .Text & "*'"
            End Select
        End With
    End Sub

    '' Input
    Private Sub cboStaat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboStaat.SelectedIndexChanged
        Dim FileName As String
        Try
            FileName = Path.Combine(Application.StartupPath, "flag", DirectCast(cboStaat.SelectedItem, DataRowView)("state_flag").ToString)
        Catch ex As Exception
            FileName = Path.Combine(Application.StartupPath, "flag", "Empty.png")
        End Try
        picFlagge.ImageLocation = FileName

        If cboStaat.Focused Then mnuSave.Enabled = True
    End Sub
    Private Sub cboBundesland_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBundesland.SelectedIndexChanged
        Try
            'lblRegion_3.Text = DirectCast(cboLand.SelectedItem, DataRowView)!region_3.ToString()
            cboStaat.SelectedValue = 42
            If cboBundesland.Focused Then mnuSave.Enabled = True
        Catch ex As Exception
        End Try
    End Sub
    Private Sub TextBox_TextChanged(sender As Object, e As EventArgs) Handles txtID.TextChanged, txtName.TextChanged, txtAdresse.TextChanged, txtPlz.TextChanged, txtOrt.TextChanged, txtKurz.TextChanged,
                                    txtName_LV.TextChanged, txtKurz_LV.TextChanged, txtAdresse_LV.TextChanged, txtPLZ_LV.TextChanged, txtOrt_LV.TextChanged
        If CType(sender, TextBox).Focused Then mnuSave.Enabled = True
    End Sub
    Private Sub txtID_EnabledChanged(sender As Object, e As EventArgs) Handles txtID.EnabledChanged
        btnEditID.Enabled = Not txtID.Enabled
        If Not txtID.Enabled Then txtID.Tag = Nothing
    End Sub
    Private Sub txtID_GotFocus(sender As Object, e As EventArgs) Handles txtID.GotFocus
        txtID.Tag = txtID.Text
        CancelButton = Nothing
    End Sub
    Private Sub txtID_KeyDown(sender As Object, e As KeyEventArgs) Handles txtID.KeyDown
        nonNumberEntered = False
        Select Case e.KeyCode
            Case Keys.Escape
                e.Handled = True
                e.SuppressKeyPress = True
                Dim row = dv(bs.Position).Row
                txtID.Text = row("idVerein", DataRowVersion.Original).ToString
                txtID.Enabled = False
                txtName.Focus()
            Case Else
                Using GF As New myFunction
                    nonNumberEntered = GF.IsNan(e.KeyCode)
                End Using
        End Select
        If ModifierKeys = Keys.Shift Then
            nonNumberEntered = True
        End If
    End Sub
    Private Sub txtID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtID.KeyPress
        If nonNumberEntered Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtID_LostFocus(sender As Object, e As EventArgs) Handles txtID.LostFocus
        If txtID.Text.Equals(String.Empty) Then
            Using New Centered_MessageBox(Me, {"Auslesen", "Eingeben"})
                If MessageBox.Show("Die ID muss eine Zahl größer 0 sein." & "Sie kann aus der Datenbank ausgelesen oder manuell vergeben werden.", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Error) = DialogResult.Yes Then
                    Dim row = dv(bs.Position).Row
                    If row.RowState = DataRowState.Added Then
                        Using GF As New myFunction
                            txtID.Text = (GF.Get_LastId("verein", "idVerein") + 1).ToString
                        End Using
                    Else
                        txtID.Text = row("idVerein", DataRowVersion.Original).ToString
                    End If
                Else
                    txtID.Focus()
                End If
            End Using
        End If
        CancelButton = btnCancel
    End Sub

    ''Numeric Input
    Private Sub TextboxNum_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPlz.KeyDown, txtPLZ_LV.KeyDown
        Using GF As New myFunction
            nonNumberEntered = GF.IsNan(e.KeyCode)
        End Using
        If ModifierKeys = Keys.Shift Then
            nonNumberEntered = True
        End If
    End Sub
    Private Sub TextboxNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPlz.KeyPress, txtPLZ_LV.KeyPress
        If nonNumberEntered = True Then
            e.Handled = True
        End If
    End Sub

    '' MouseWheel ausschalten
    Private Sub Prohibited_MouseWheel(sender As Object, e As MouseEventArgs) Handles cboBundesland.MouseWheel, cboStaat.MouseWheel
        If Not CType(sender, Control).Capture Then
            Dim HMEA As HandledMouseEventArgs = DirectCast(e, HandledMouseEventArgs)
            HMEA.Handled = True
        End If
    End Sub

    Private Sub mnuMauszeiger_Click(sender As Object, e As EventArgs) Handles mnuMauszeiger.Click
        Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
        Cursor.Show()
    End Sub

    Private Sub dgv_GotFocus(sender As Object, e As EventArgs) Handles dgv.GotFocus
        If IsNothing(bs) OrElse bs.Position = -1 Then Return
        dgv.Rows(bs.Position).Selected = True
    End Sub
    Private Sub dgv_LostFocus(sender As Object, e As EventArgs) Handles dgv.LostFocus
        If txtVerein.Focused() OrElse txtLand.Focused() Then dgv.ClearSelection()
    End Sub

    Private Sub btnEditID_Click(sender As Object, e As EventArgs) Handles btnEditID.Click
        txtID.Enabled = True
    End Sub

    Private Sub btnTeams_Click(sender As Object, e As EventArgs) Handles btnTeams.Click
        Using formTeams As New frmTeams
            With formTeams
                .TeamId = CInt(txtID.Text)
                .TeamNew = chkTeam.BackColor = Color.Red
                .TeamName = txtName.Text
                .ShowDialog(Me)
                If Not .DialogResult = DialogResult.Cancel Then
                    If Not dv(bs.Position)!idVerein.Equals(.TeamId) Then
                        txtID.Text = .TeamId.ToString()
                        mnuSave.Enabled = True
                    End If
                    dv(bs.Position)!TeamMembers = .TeamMembers
                End If
            End With
        End Using
    End Sub

    Private Sub chkTeam_CheckedChanged(sender As Object, e As EventArgs) Handles chkTeam.CheckedChanged
        If chkTeam.Checked = CBool(dv(bs.Position)!TeamMembers) Then
            chkTeam.BackColor = Color.FromKnownColor(KnownColor.HotTrack)
        Else
            chkTeam.BackColor = Color.FromKnownColor(KnownColor.Red)
        End If
        btnTeams.Enabled = chkTeam.Checked
        If chkTeam.Focused Then mnuSave.Enabled = True
    End Sub

End Class