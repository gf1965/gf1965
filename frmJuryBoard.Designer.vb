﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJuryBoard
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Weiß1 = New System.Windows.Forms.CheckBox()
        Me.Rot1 = New System.Windows.Forms.CheckBox()
        Me.Wert1 = New System.Windows.Forms.NumericUpDown()
        Me.Weiß2 = New System.Windows.Forms.CheckBox()
        Me.Rot2 = New System.Windows.Forms.CheckBox()
        Me.Rot3 = New System.Windows.Forms.CheckBox()
        Me.Weiß3 = New System.Windows.Forms.CheckBox()
        Me.Wert2 = New System.Windows.Forms.NumericUpDown()
        Me.Wert3 = New System.Windows.Forms.NumericUpDown()
        Me.NurLeds = New System.Windows.Forms.CheckBox()
        Me.ledKnopf = New System.Windows.Forms.CheckBox()
        Me.KnopfGed = New System.Windows.Forms.CheckBox()
        Me.wertung = New System.Windows.Forms.GroupBox()
        Me.leds = New System.Windows.Forms.GroupBox()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.portList = New System.Windows.Forms.ComboBox()
        Me.Con = New System.Windows.Forms.Button()
        Me.Sync = New System.Windows.Forms.CheckBox()
        Me.KR1LED = New System.Windows.Forms.CheckBox()
        Me.KR2LED = New System.Windows.Forms.CheckBox()
        Me.KR3LED = New System.Windows.Forms.CheckBox()
        Me.KR1ged = New System.Windows.Forms.CheckBox()
        Me.KR2ged = New System.Windows.Forms.CheckBox()
        Me.KR3ged = New System.Windows.Forms.CheckBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.Wert1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Wert2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Wert3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wertung.SuspendLayout()
        Me.leds.SuspendLayout()
        Me.SuspendLayout()
        '
        'Weiß1
        '
        Me.Weiß1.AutoSize = True
        Me.Weiß1.Location = New System.Drawing.Point(11, 19)
        Me.Weiß1.Name = "Weiß1"
        Me.Weiß1.Size = New System.Drawing.Size(51, 17)
        Me.Weiß1.TabIndex = 0
        Me.Weiß1.Text = "Weiß"
        Me.Weiß1.UseVisualStyleBackColor = True
        '
        'Rot1
        '
        Me.Rot1.AutoSize = True
        Me.Rot1.Location = New System.Drawing.Point(11, 42)
        Me.Rot1.Name = "Rot1"
        Me.Rot1.Size = New System.Drawing.Size(43, 17)
        Me.Rot1.TabIndex = 1
        Me.Rot1.Text = "Rot"
        Me.Rot1.UseVisualStyleBackColor = True
        '
        'Wert1
        '
        Me.Wert1.DecimalPlaces = 2
        Me.Wert1.Location = New System.Drawing.Point(15, 16)
        Me.Wert1.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.Wert1.Name = "Wert1"
        Me.Wert1.Size = New System.Drawing.Size(120, 20)
        Me.Wert1.TabIndex = 2
        '
        'Weiß2
        '
        Me.Weiß2.AutoSize = True
        Me.Weiß2.Location = New System.Drawing.Point(11, 78)
        Me.Weiß2.Name = "Weiß2"
        Me.Weiß2.Size = New System.Drawing.Size(51, 17)
        Me.Weiß2.TabIndex = 3
        Me.Weiß2.Text = "Weiß"
        Me.Weiß2.UseVisualStyleBackColor = True
        '
        'Rot2
        '
        Me.Rot2.AutoSize = True
        Me.Rot2.Location = New System.Drawing.Point(10, 101)
        Me.Rot2.Name = "Rot2"
        Me.Rot2.Size = New System.Drawing.Size(43, 17)
        Me.Rot2.TabIndex = 4
        Me.Rot2.Text = "Rot"
        Me.Rot2.UseVisualStyleBackColor = True
        '
        'Rot3
        '
        Me.Rot3.AutoSize = True
        Me.Rot3.Location = New System.Drawing.Point(10, 153)
        Me.Rot3.Name = "Rot3"
        Me.Rot3.Size = New System.Drawing.Size(43, 17)
        Me.Rot3.TabIndex = 5
        Me.Rot3.Text = "Rot"
        Me.Rot3.UseVisualStyleBackColor = True
        '
        'Weiß3
        '
        Me.Weiß3.AutoSize = True
        Me.Weiß3.Location = New System.Drawing.Point(11, 134)
        Me.Weiß3.Name = "Weiß3"
        Me.Weiß3.Size = New System.Drawing.Size(51, 17)
        Me.Weiß3.TabIndex = 6
        Me.Weiß3.Text = "Weiß"
        Me.Weiß3.UseVisualStyleBackColor = True
        '
        'Wert2
        '
        Me.Wert2.DecimalPlaces = 2
        Me.Wert2.Location = New System.Drawing.Point(15, 78)
        Me.Wert2.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.Wert2.Name = "Wert2"
        Me.Wert2.Size = New System.Drawing.Size(120, 20)
        Me.Wert2.TabIndex = 7
        '
        'Wert3
        '
        Me.Wert3.DecimalPlaces = 2
        Me.Wert3.Location = New System.Drawing.Point(15, 134)
        Me.Wert3.Maximum = New Decimal(New Integer() {9999, 0, 0, 131072})
        Me.Wert3.Name = "Wert3"
        Me.Wert3.Size = New System.Drawing.Size(120, 20)
        Me.Wert3.TabIndex = 8
        '
        'NurLeds
        '
        Me.NurLeds.AutoSize = True
        Me.NurLeds.Location = New System.Drawing.Point(14, 252)
        Me.NurLeds.Name = "NurLeds"
        Me.NurLeds.Size = New System.Drawing.Size(72, 17)
        Me.NurLeds.TabIndex = 9
        Me.NurLeds.Text = "Nur LEDs"
        Me.NurLeds.UseVisualStyleBackColor = True
        '
        'ledKnopf
        '
        Me.ledKnopf.AutoSize = True
        Me.ledKnopf.Location = New System.Drawing.Point(46, 278)
        Me.ledKnopf.Name = "ledKnopf"
        Me.ledKnopf.Size = New System.Drawing.Size(54, 17)
        Me.ledKnopf.TabIndex = 10
        Me.ledKnopf.Text = "Knopf"
        Me.ledKnopf.UseVisualStyleBackColor = True
        '
        'KnopfGed
        '
        Me.KnopfGed.AutoCheck = False
        Me.KnopfGed.AutoSize = True
        Me.KnopfGed.Checked = True
        Me.KnopfGed.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.KnopfGed.Location = New System.Drawing.Point(46, 301)
        Me.KnopfGed.Name = "KnopfGed"
        Me.KnopfGed.Size = New System.Drawing.Size(54, 17)
        Me.KnopfGed.TabIndex = 11
        Me.KnopfGed.Text = "Knopf"
        Me.KnopfGed.UseVisualStyleBackColor = True
        '
        'wertung
        '
        Me.wertung.Controls.Add(Me.Wert3)
        Me.wertung.Controls.Add(Me.Wert2)
        Me.wertung.Controls.Add(Me.Wert1)
        Me.wertung.Location = New System.Drawing.Point(92, 47)
        Me.wertung.Name = "wertung"
        Me.wertung.Size = New System.Drawing.Size(146, 185)
        Me.wertung.TabIndex = 12
        Me.wertung.TabStop = False
        Me.wertung.Text = "Wertung"
        '
        'leds
        '
        Me.leds.Controls.Add(Me.Weiß3)
        Me.leds.Controls.Add(Me.Rot3)
        Me.leds.Controls.Add(Me.Rot2)
        Me.leds.Controls.Add(Me.Weiß2)
        Me.leds.Controls.Add(Me.Rot1)
        Me.leds.Controls.Add(Me.Weiß1)
        Me.leds.Location = New System.Drawing.Point(3, 47)
        Me.leds.Name = "leds"
        Me.leds.Size = New System.Drawing.Size(83, 185)
        Me.leds.TabIndex = 13
        Me.leds.TabStop = False
        Me.leds.Text = "LEDs"
        '
        'SerialPort1
        '
        Me.SerialPort1.ReadTimeout = 1
        '
        'portList
        '
        Me.portList.FormattingEnabled = True
        Me.portList.Location = New System.Drawing.Point(13, 13)
        Me.portList.Name = "portList"
        Me.portList.Size = New System.Drawing.Size(121, 21)
        Me.portList.TabIndex = 14
        '
        'Con
        '
        Me.Con.Location = New System.Drawing.Point(141, 13)
        Me.Con.Name = "Con"
        Me.Con.Size = New System.Drawing.Size(75, 23)
        Me.Con.TabIndex = 15
        Me.Con.Text = "Verbinden"
        Me.Con.UseVisualStyleBackColor = True
        '
        'Sync
        '
        Me.Sync.AutoSize = True
        Me.Sync.Enabled = False
        Me.Sync.Location = New System.Drawing.Point(107, 252)
        Me.Sync.Name = "Sync"
        Me.Sync.Size = New System.Drawing.Size(92, 17)
        Me.Sync.TabIndex = 16
        Me.Sync.Text = "Synchronisiert"
        Me.Sync.UseVisualStyleBackColor = True
        '
        'KR1LED
        '
        Me.KR1LED.AutoSize = True
        Me.KR1LED.Location = New System.Drawing.Point(106, 278)
        Me.KR1LED.Name = "KR1LED"
        Me.KR1LED.Size = New System.Drawing.Size(47, 17)
        Me.KR1LED.TabIndex = 17
        Me.KR1LED.Text = "KR1"
        Me.KR1LED.UseVisualStyleBackColor = True
        '
        'KR2LED
        '
        Me.KR2LED.AutoSize = True
        Me.KR2LED.Location = New System.Drawing.Point(152, 278)
        Me.KR2LED.Name = "KR2LED"
        Me.KR2LED.Size = New System.Drawing.Size(47, 17)
        Me.KR2LED.TabIndex = 18
        Me.KR2LED.Text = "KR2"
        Me.KR2LED.UseVisualStyleBackColor = True
        '
        'KR3LED
        '
        Me.KR3LED.AutoSize = True
        Me.KR3LED.Location = New System.Drawing.Point(200, 278)
        Me.KR3LED.Name = "KR3LED"
        Me.KR3LED.Size = New System.Drawing.Size(47, 17)
        Me.KR3LED.TabIndex = 19
        Me.KR3LED.Text = "KR3"
        Me.KR3LED.UseVisualStyleBackColor = True
        '
        'KR1ged
        '
        Me.KR1ged.AutoCheck = False
        Me.KR1ged.AutoSize = True
        Me.KR1ged.Checked = True
        Me.KR1ged.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.KR1ged.Location = New System.Drawing.Point(106, 301)
        Me.KR1ged.Name = "KR1ged"
        Me.KR1ged.Size = New System.Drawing.Size(47, 17)
        Me.KR1ged.TabIndex = 20
        Me.KR1ged.Text = "KR1"
        Me.KR1ged.UseVisualStyleBackColor = True
        '
        'KR2ged
        '
        Me.KR2ged.AutoCheck = False
        Me.KR2ged.AutoSize = True
        Me.KR2ged.Checked = True
        Me.KR2ged.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.KR2ged.Location = New System.Drawing.Point(152, 301)
        Me.KR2ged.Name = "KR2ged"
        Me.KR2ged.Size = New System.Drawing.Size(47, 17)
        Me.KR2ged.TabIndex = 21
        Me.KR2ged.Text = "KR2"
        Me.KR2ged.UseVisualStyleBackColor = True
        '
        'KR3ged
        '
        Me.KR3ged.AutoCheck = False
        Me.KR3ged.AutoSize = True
        Me.KR3ged.Checked = True
        Me.KR3ged.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.KR3ged.Location = New System.Drawing.Point(200, 301)
        Me.KR3ged.Name = "KR3ged"
        Me.KR3ged.Size = New System.Drawing.Size(47, 17)
        Me.KR3ged.TabIndex = 22
        Me.KR3ged.Text = "KR3"
        Me.KR3ged.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'frmJuryBoard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(259, 335)
        Me.Controls.Add(Me.KR3ged)
        Me.Controls.Add(Me.KR2ged)
        Me.Controls.Add(Me.KR1ged)
        Me.Controls.Add(Me.KR3LED)
        Me.Controls.Add(Me.KR2LED)
        Me.Controls.Add(Me.KR1LED)
        Me.Controls.Add(Me.Sync)
        Me.Controls.Add(Me.Con)
        Me.Controls.Add(Me.portList)
        Me.Controls.Add(Me.leds)
        Me.Controls.Add(Me.wertung)
        Me.Controls.Add(Me.KnopfGed)
        Me.Controls.Add(Me.ledKnopf)
        Me.Controls.Add(Me.NurLeds)
        Me.Name = "frmJuryBoard"
        Me.Text = "Jury-Board Test"
        CType(Me.Wert1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Wert2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Wert3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wertung.ResumeLayout(False)
        Me.leds.ResumeLayout(False)
        Me.leds.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Weiß1 As System.Windows.Forms.CheckBox
    Friend WithEvents Rot1 As System.Windows.Forms.CheckBox
    Friend WithEvents Wert1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Weiß2 As System.Windows.Forms.CheckBox
    Friend WithEvents Rot2 As System.Windows.Forms.CheckBox
    Friend WithEvents Rot3 As System.Windows.Forms.CheckBox
    Friend WithEvents Weiß3 As System.Windows.Forms.CheckBox
    Friend WithEvents Wert2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Wert3 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NurLeds As System.Windows.Forms.CheckBox
    Friend WithEvents ledKnopf As System.Windows.Forms.CheckBox
    Friend WithEvents KnopfGed As System.Windows.Forms.CheckBox
    Friend WithEvents wertung As System.Windows.Forms.GroupBox
    Friend WithEvents leds As System.Windows.Forms.GroupBox
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents portList As System.Windows.Forms.ComboBox
    Friend WithEvents Con As System.Windows.Forms.Button
    Friend WithEvents Sync As System.Windows.Forms.CheckBox
    Friend WithEvents KR1LED As System.Windows.Forms.CheckBox
    Friend WithEvents KR2LED As System.Windows.Forms.CheckBox
    Friend WithEvents KR3LED As System.Windows.Forms.CheckBox
    Friend WithEvents KR1ged As System.Windows.Forms.CheckBox
    Friend WithEvents KR2ged As System.Windows.Forms.CheckBox
    Friend WithEvents KR3ged As System.Windows.Forms.CheckBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer

End Class
