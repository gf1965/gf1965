﻿
Imports System.Math

Module Versuch_beendet

    Class AttemptFinished
        Implements IDisposable

        Private Delegate Sub DelegateSub()

        Public Shared Event CallOrderTable()

        Dim Wertung As Integer, Zeit As Date, Note As Double, Lampen As String, Korrektur As Boolean, myForm As Form

        Public Sub New(_Wertung As Integer, _Zeit As Date, _Note As Double, _Lampen As String, _Korrektur As Boolean, _Form As Form)
            Wertung = _Wertung
            Zeit = _Zeit
            Note = _Note
            Lampen = _Lampen
            Korrektur = _Korrektur
            myForm = _Form
        End Sub
        Public Sub Set_Wertung()
            If myForm.InvokeRequired Then
                Dim d As New DelegateSub(AddressOf Set_Wertung)
                myForm.Invoke(d, New Object() {})
            Else
                If Heber.Id = 0 Then
                    ' wenn Bohle-Anzeige gelöscht wird, darf keine Wertung erfolgen
                    Change_formLeader_btnNext_Enabled(True)
                    Return
                End If

                Dim _table = Leader.Table.Replace("ß", "ss")
                Dim Attempts() As DataRow = Nothing
                Dim AttemtpsFinished() As DataRow = Nothing
                Try
                    With Heber
                        Dim Col = UCase(Left(_table, 1)) & "_" & .Versuch
                        Dim Row = bsResults.Find("Teilnehmer", .Id)
                        Try
                            If Wettkampf.Technikwertung Then
                                If .Wertung <> Wertung AndAlso Wertung = 1 AndAlso .T_Note = Note Then
                                    ' nur Wertung geändert --> nur Wertung in Results schreiben
                                    dvResults(Row)(Col.Insert(1, "W")) = Wertung
                                    bsResults.EndEdit()
                                    ' Wertung für aktuellen Heber in Moderator, Publikum
                                    Leader.Write_Scoring(Row, Col, .Hantellast, Wertung, 0, String.Empty) ', True)
                                    .Wertung = Wertung
                                    Return
                                End If
                            End If
                            'alle gespeicherten Versuche des Hebers auslesen --> 0 = letzter gespeicherter Versuch
                            AttemtpsFinished = dtDelete(Leader.TableIx).Select("Teilnehmer = " + .Id.ToString & " AND Versuch < " & .Versuch.ToString & " AND Wertung = 1", "Versuch DESC")
                            'alle nicht gespeicherten Versuche des Hebers auslesen --> 0 = aktueller Versuch
                            Attempts = dtLeader(Leader.TableIx).Select("Teilnehmer = " + .Id.ToString & " AND Versuch >= " & .Versuch.ToString, "Versuch ASC")
                            'Wertung in dv und dgv schreiben
                            Attempts(0)("Lampen") = Lampen
                            Attempts(0)("Wertung") = Wertung
                            .Wertung = Wertung
                            .Lampen = Lampen
                            If Wettkampf.Technikwertung Then
                                Attempts(0)("Note") = Note
                                .T_Note = Note '------------------------------------------------------
                            End If
                            If Wertung = -2 Then .Hantellast = 0
                            ' Zeitpunkt des absolvierten Versuches in nächstem Versuch speichern
                            If Attempts.Count > 1 Then Attempts(1)("Zeit") = Zeit
                        Catch ex As Exception
                            LogMessage("AttemptFinished: Set_Wertung: Data: " & ex.Message, ex.StackTrace)
                        End Try
                        Try
                            If .Versuch < 3 Then
                                If Wertung = -1 Then
                                    ' Steigerung / Differenz bei Ungültig zurücksetzen
                                    If Attempts.Count > 1 Then
                                        'Steigerung für nächsten Versuch zurücksetzen
                                        Attempts(1)("HLast") = .Hantellast
                                        Attempts(1)("Diff") = 0
                                        If Attempts.Count > 2 Then
                                            'Steigerung für dritten Versuch zurücksetzen, falls aktueller der erste ist 
                                            Attempts(2)("HLast") = .Hantellast + CInt(Attempts(2)("Diff"))
                                        End If
                                    Else
#Region "Restriction: Versuch = letzter Versuch"
                                        If Wettkampf.ThirdAttempt Then
                                            Dim r = Glob.dtRestrict.Select("Durchgang = " & Leader.TableIx & " AND Altersklasse <= " & Year(Wettkampf.Datum) - CInt(Heber.JG))
                                            Dim maxAttempts = -1
                                            If r.Count > 0 Then maxAttempts = CInt(r(0)!Versuche)
                                            If .Versuch >= maxAttempts Then
                                                ' prüfen, ob 1.Versuch ungültig
                                                Dim rAttempts = New DataView(dtDelete(Leader.TableIx), "Teilnehmer = " + .Id.ToString, "Versuch ASC", DataViewRowState.CurrentRows)
                                                If rAttempts.Count > 0 AndAlso CInt(rAttempts(0)!Wertung) = -1 Then
                                                    ' 3.Versuch geben
                                                    Dim i = 0
                                                    Do Until CInt(rAttempts(i)!Wertung) = -2
                                                        i += 1
                                                    Loop
                                                    rAttempts(i)!Wertung = DBNull.Value
                                                    rAttempts(i)!HLast = .Hantellast
                                                    rAttempts(i)!Diff = 0
                                                    dtLeader(Leader.TableIx).ImportRow(rAttempts(i).Row)
                                                    rAttempts(i).Delete()
                                                    bsDelete.EndEdit()
                                                    bsLeader.EndEdit()
                                                    ' nächste Hantellast für aktuellen Heber setzen
                                                    Dim nCol = UCase(Left(_table, 1)) & "_" & .Versuch + 1
                                                    Dim rRow = bsResults.Find("Teilnehmer", .Id)
                                                    dvResults(rRow)(nCol) = .Hantellast
                                                    dvResults(rRow)(nCol.Insert(1, "W")) = DBNull.Value
                                                    Leader.Change_Anfangslast(.Hantellast, rRow, nCol, True)
                                                End If
                                            End If
                                        End If
#End Region
                                    End If
                                End If
                                If Attempts.Count > 1 Then
                                    ' alle Versuche nach dem ersten und vor dem letzten
                                    If Wertung = 1 AndAlso .Hantellast <= CInt(Attempts(1)("HLast")) Then
                                        Attempts(1)("HLast") = .Hantellast + Wettkampf.Steigerung(.Versuch - 1)
                                    End If
                                    If Not formLeader.mnuConfirm.Checked Then
                                        ' automatische Steigerung wird nicht bestätigt --> d.h. wird als Steigerung1 gespeichert
                                        Attempts(1)("Steigerung1") = Attempts(1)("HLast")
                                        formLeader.btnBestätigung.Enabled = False
                                        ' Steigerung speichern
                                        If User.UserId = UserID.Wettkampfleiter Then
                                            Using save As New mySave
                                                save.Save_Steigerung(Attempts.CopyToDataTable, Leader.Table, myForm)
                                            End Using
                                        End If
                                    End If
                                End If
                            End If
                        Catch ex As Exception
                            LogMessage("AttemptFinished: Set_Wertung: Ungültig: " & ex.Message, ex.StackTrace)
                        End Try

                        Dim result As Double? = 0
                        Dim result2 As Double? = 0
                        Dim gesamt2 As Double? = 0

#Region "Wertung: Result + Result_2"
                        If Wertung = 1 OrElse Korrektur Then
                            Dim _Last = .Hantellast
                            Using RS As New Results
                                If Wertung = -1 Then
                                    If AttemtpsFinished.Length > 0 Then
                                        Dim i = 0
                                        For i = .Versuch - 1 To 1 Step -1
                                            If CInt(AttemtpsFinished(i - 1)!Wertung) = 1 Then
                                                _Last = CInt(AttemtpsFinished(i - 1)!HLast)
                                                Exit For
                                            End If
                                        Next
                                        If i > 0 Then
                                            result = RS.Get_Result_Heben(.Id, .Auswertung, .Wiegen, .Sex, _Last, .JG, .T_Note, _table, CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
                                            result2 = RS.Get_Result_Heben(.Id, .Auswertung2, .Wiegen, .Sex, _Last, .JG, .T_Note, _table, CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
                                            gesamt2 = RS.Get_Result_2(result, .Auswertung, .Auswertung2, _table, dtDelete, Row, .Id, .Wiegen, .Sex, .JG, _Last, .T_Note, CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
                                        End If
                                    End If
                                Else
                                    result = RS.Get_Result_Heben(.Id, .Auswertung, .Wiegen, .Sex, .Hantellast, .JG, .T_Note, _table, CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
                                    result2 = RS.Get_Result_Heben(.Id, .Auswertung2, .Wiegen, .Sex, .Hantellast, .JG, .T_Note, _table, CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
                                    gesamt2 = RS.Get_Result_2(result, .Auswertung, .Auswertung2, _table, dtDelete, Row, .Id, .Wiegen, .Sex, .JG, .Hantellast, .T_Note, CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
                                End If
                            End Using
                        End If
#End Region

#Region "Jury hat gewertet"
                        ' bei JuryWertung = Ungültig IMMER prüfen, ob es einen gültigen Vorversuch gibt
                        If Korrektur AndAlso Wertung = -1 AndAlso AttemtpsFinished.Count > 0 Then
                            ' --> wenn ja, dessen Resultat berechnen und speichern --------------------------> Warum?
                            Wertung = CInt(AttemtpsFinished(0)!Wertung)
                            Note = If(IsDBNull(AttemtpsFinished(0)!Note), 0, CDbl(AttemtpsFinished(0)!Note))
                            Dim Hantellast = CInt(AttemtpsFinished(0)!HLast)
                            Using RS As New Results
                                result = RS.Get_Result_Heben(.Id, .Auswertung, .Wiegen, .Sex, Hantellast, .JG, Note, _table, CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
                                result2 = RS.Get_Result_Heben(.Id, .Auswertung2, .Wiegen, .Sex, Hantellast, .JG, Note, _table, CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
                                gesamt2 = RS.Get_Result_2(result, .Auswertung, .Auswertung2, _table, dtDelete, Row, .Id, .Wiegen, .Sex, .JG, Hantellast, Note, CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
                            End Using

                            If Wertung = 1 OrElse Korrektur Then
                                'result2 = RS.Get_Result_2(result, _table, Row, Hantellast, Note)
                            End If
                        End If
#End Region

#Region "Speichern & Platzierung"
                        Using RS As New Results
                            'RS.Write_Results_Table(Row, Col, .Sex, .Hantellast, Wertung, Zeit, Note, _table, result, result2, .JG, .Auswertung, .Auswertung2, .Gruppierung2, Korrektur)
                            Dim Result_Old = RS.Write_Results_Table_Ergebnis(Row, Col, .Hantellast, Wertung, Zeit, Note, _table, result, .Auswertung, Korrektur)
                            RS.Write_Results_Table_Wertung(Row, .Sex, Wertung, _table, result, result2, gesamt2, .JG, .Auswertung, .Auswertung2, .Gruppierung2, Result_Old, Korrektur)
                        End Using
                        Row = bsResults.Find("Teilnehmer", .Id) ' falls Reihenfolge in Scoring geändert wurde
#End Region

                        '#Region "ZK-Resultat"
                        '                        If Wertung = 1 OrElse Korrektur Then
                        '                            Using RS As New Results
                        '                                RS.ZK_Resultat(.Id, Row)
                        '                            End Using
                        '                        End If
                        '#End Region

                        If .Versuch < 3 AndAlso Attempts.Count > 1 Then
                            Dim nCol = UCase(Left(_table, 1)) & "_" & .Versuch + 1
                            ' nächste Hantellast für aktuellen Heber setzen
                            If Not formLeader.mnuConfirm.Checked OrElse ((Attempts(0)!AK.ToString = "Schüler" OrElse Attempts(0)!AK.ToString = "Kinder") AndAlso CInt(Attempts(0)!Wertung) = -1) Then
                                ' automatische Steigerung muss nicht bestätigt werden
                                dvResults(Row)(nCol) = Attempts(1)!HLast
                                Leader.Change_Anfangslast(CInt(Attempts(1)!HLast), Row, nCol, True)
                                ' Resultat speichern
                                If User.UserId = UserID.Wettkampfleiter Then
                                    Using save As New mySave
                                        save.Save_Results(Leader.Table, myForm)
                                    End Using
                                End If
                            Else
                                ' Resultat speichern
                                If User.UserId = UserID.Wettkampfleiter Then
                                    Using save As New mySave
                                        save.Save_Results(Leader.Table, myForm)
                                    End Using
                                End If
                                ' nächste HLast unbestätigt in Publikum schreiben
                                Leader.Change_Anfangslast(CInt(Attempts(1)!HLast), Row, nCol)
                                dvResults(Row)(nCol) = Attempts(1)!HLast
                            End If
                        Else
                            ' Resultat speichern
                            If User.UserId = UserID.Wettkampfleiter Then
                                Using save As New mySave
                                    save.Save_Results(Leader.Table, myForm)
                                End Using
                            End If
                        End If

                        If Wettkampf.Mannschaft Then
                            bsResults.EndEdit()
                            Using RS As New Results
                                ' Ergebnisse des Teams in dtTeams schreiben
                                RS.TeamWertung(dvResults.ToTable)
                            End Using
                            bsTeam.ResetBindings(False)
                            If User.UserId = UserID.Wettkampfleiter Then
                                Using save As New mySave
                                    save.Save_Punkte(myForm)
                                End Using
                            End If
                        End If

                        ' Wertung für aktuellen Heber in Moderator, Publikum + alle Platzierungen
                        Leader.Write_Scoring(Row, Col, .Hantellast, Wertung, Note, If(Not IsDBNull(dvResults(Row)!ZK), dvResults(Row)!ZK.ToString, String.Empty)) ', True)

                    End With

                    If User.UserId = UserID.Wettkampfleiter Then
                        Using save As New mySave
                            If save.Save_Versuch(Attempts.CopyToDataTable, Leader.Table, myForm) Then dtLeader(Leader.TableIx).AcceptChanges()
                        End Using
                    End If

                    If Wertung = -1 OrElse Wertung = 1 AndAlso (Not Wettkampf.Technikwertung OrElse Note > 0) Then
                        RaiseEvent CallOrderTable()
                    Else
                        Change_formLeader_btnNext_Enabled(False)
                    End If
                Catch ex As Exception
                    LogMessage("Versuch_beendet: Set_Wertung: " & ex.Message, ex.StackTrace)
                End Try
            End If
        End Sub
        'Private Function Get_Result_2(Result As Double?, Table As String, Row As Integer, Hantellast As Integer, Note As Double) As Double?
        '    Dim Result_2 As Double? = Nothing
        '    If Wettkampf.Platzierung > 99 Then
        '        Dim result_dg(1) As Double?
        '        With Heber
        '            Try
        '                ' 2. Wertung des aktuellen Durchgangs
        '                If .Auswertung2 <> 1 AndAlso .Auswertung2 <> 5 Then ' ############################## alles außer Robi und Zweikampf 
        '                    If .Auswertung <> .Auswertung2 Then
        '                        ' bei unterschiedlichen Wertungen Resultat berechnen
        '                        Using RS As New Results
        '                            result_dg(Leader.TableIx) = RS.Get_Result_Heben(.Id, .Auswertung2, .Wiegen, .Sex, Hantellast, .JG, Note, Table, CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
        '                        End Using
        '                        If IsNothing(result_dg(Leader.TableIx)) Then result_dg(Leader.TableIx) = 0
        '                    Else
        '                        ' bei gleicher Wertung Resultat übernehmen
        '                        result_dg(Leader.TableIx) = Result
        '                    End If
        '                    ' Resultat des anderen Durchgangs in Delete suchen --> wenn nicht in Delete, dann ist der jeweils andere Durchgang noch nicht gelaufen
        '                    Try
        '                        Dim HLast = (From x In dtDelete(Abs(Leader.TableIx - 1))
        '                                     Where x.Field(Of Integer)("Teilnehmer") = .Id AndAlso x.Field(Of Integer?)("Wertung") = 1
        '                                     Select x.Field(Of Integer)("HLast")).Max
        '                        Using RS As New Results
        '                            result_dg(Abs(Leader.TableIx - 1)) = RS.Get_Result_Heben(.Id, .Auswertung2, .Wiegen, .Sex, HLast, .JG, 0, Table, CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
        '                        End Using
        '                    Catch ex As Exception
        '                        result_dg(Abs(Leader.TableIx - 1)) = 0
        '                    End Try
        '                    ' Gesamtresultat 2.Wertung
        '                    Result_2 = result_dg(0) + result_dg(1)
        '                ElseIf Not IsDBNull(dvResults(Row)!ZK) Then ' ############################ Robi und Zweikampf
        '                    If .Auswertung <> .Auswertung2 Then
        '                        Using RS As New Results
        '                            Result_2 = RS.Get_Result_Heben(.Id, .Auswertung2, CDbl(dvResults(Row)!KG), dvResults(Row)!Sex.ToString, CInt(dvResults(Row)!ZK), 0, 0, "Total", CInt(dvResults(Row)!idAK), CInt(dvResults(Row)!idGK))
        '                        End Using
        '                    Else
        '                        Result_2 = Result
        '                    End If
        '                    ' analog zu #Region "ZK-Resultat" in Set_Wertung
        '                    If Not IsNothing(Result_2) Then dvResults(Row)!Wertung2 = Result_2
        '                    End If
        '            Catch ex As Exception
        '                LogMessage("Leader:Wertung2 - " & ex.Message, Path.Combine(Application.StartupPath, "log", "err_" & Format(Now, "yyyy-MM-dd") & ".log"))
        '            End Try
        '        End With
        '    End If
        '    Return Result_2
        'End Function

        Public Sub Dispose() Implements IDisposable.Dispose
        End Sub
    End Class

End Module
