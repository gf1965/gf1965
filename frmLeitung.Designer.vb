﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmLeitung
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeitung))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnAufruf = New System.Windows.Forms.Button()
        Me.Status = New System.Windows.Forms.StatusStrip()
        Me.statusMsg = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Progress1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.statusVersuch = New System.Windows.Forms.ToolStripStatusLabel()
        Me.statusResultat = New System.Windows.Forms.ToolStripStatusLabel()
        Me.statusSteigerung = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ComMsg = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MenuStrip2 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHeberAuswechseln = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPrognose = New System.Windows.Forms.ToolStripMenuItem()
        Me.sep5 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPause = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWertung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBlaueTaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuClearHeber = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuClearHupe = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMeldungen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKonflikte = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuEingabePrüfen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAktuellenVersuch = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuConfirm = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTechniknote = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSplitItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSplit_AG = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHeber = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuVerein = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuMauszeiger = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTableLayout = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuKR_Init = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTCP = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuZNPadSperrzeit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHupe = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuGrid = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuHeberWechsel = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHighLightAttempts = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuResultsLifter = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSteigerung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBestätigung = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuVerzicht = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuKorrektur = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmnuHeber = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmnuVerein = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPanik = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnSteigerung = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnBestätigung = New System.Windows.Forms.Button()
        Me.pnlNotizen = New System.Windows.Forms.Panel()
        Me.btnUndo_Notiz = New System.Windows.Forms.Button()
        Me.lblID = New System.Windows.Forms.Label()
        Me.btnDelete_Notiz = New System.Windows.Forms.Button()
        Me.btnSave_Notiz = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.rtfNotiz = New System.Windows.Forms.RichTextBox()
        Me.chkNotizUnderline = New System.Windows.Forms.CheckBox()
        Me.chkNotizItalic = New System.Windows.Forms.CheckBox()
        Me.chkNotizBold = New System.Windows.Forms.CheckBox()
        Me.btnNotizForeColor = New System.Windows.Forms.Button()
        Me.btnNotizExit = New System.Windows.Forms.Button()
        Me.lblNotizen = New System.Windows.Forms.Label()
        Me.btnVerzicht = New System.Windows.Forms.Button()
        Me.chkBH = New System.Windows.Forms.CheckBox()
        Me.btnKorrektur = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboDurchgang = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.pnlManuell = New System.Windows.Forms.Panel()
        Me.btnFertig = New System.Windows.Forms.Button()
        Me.pnlT = New System.Windows.Forms.Panel()
        Me.txtT3 = New System.Windows.Forms.TextBox()
        Me.txtT2 = New System.Windows.Forms.TextBox()
        Me.txtT1 = New System.Windows.Forms.TextBox()
        Me.pnlW1 = New System.Windows.Forms.Panel()
        Me.btnU1 = New System.Windows.Forms.RadioButton()
        Me.ImageList64 = New System.Windows.Forms.ImageList(Me.components)
        Me.btnG1 = New System.Windows.Forms.RadioButton()
        Me.pnlW2 = New System.Windows.Forms.Panel()
        Me.btnU2 = New System.Windows.Forms.RadioButton()
        Me.btnG2 = New System.Windows.Forms.RadioButton()
        Me.pnlW3 = New System.Windows.Forms.Panel()
        Me.btnU3 = New System.Windows.Forms.RadioButton()
        Me.btnG3 = New System.Windows.Forms.RadioButton()
        Me.btnExitManuell = New System.Windows.Forms.Button()
        Me.lblManuell = New System.Windows.Forms.Label()
        Me.Dummy = New System.Windows.Forms.Button()
        Me.pnlForm = New System.Windows.Forms.Panel()
        Me.btnMinute2 = New System.Windows.Forms.Button()
        Me.btnMinute1 = New System.Windows.Forms.Button()
        Me.dgvTeam = New System.Windows.Forms.DataGridView()
        Me.Rolle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Team = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gegner = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mReissen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mStossen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mHeben = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mPunkte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Punkte2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Platz = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlWertung = New System.Windows.Forms.Panel()
        Me.xGültig1 = New System.Windows.Forms.TextBox()
        Me.xTechniknote = New System.Windows.Forms.TextBox()
        Me.xGültig3 = New System.Windows.Forms.TextBox()
        Me.xGültig2 = New System.Windows.Forms.TextBox()
        Me.xAufruf = New System.Windows.Forms.Label()
        Me.dgvHeber = New System.Windows.Forms.DataGridView()
        Me.RSG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.D1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.T1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.D2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.T2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.D3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.T3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Wert = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvGruppe = New System.Windows.Forms.DataGridView()
        Me.WG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gruppe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgv2 = New System.Windows.Forms.DataGridView()
        Me.pnlSteigerung = New System.Windows.Forms.Panel()
        Me.lblSteigernName = New System.Windows.Forms.Label()
        Me.btnSteigernOK = New System.Windows.Forms.Button()
        Me.nudHantellast = New System.Windows.Forms.NumericUpDown()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnExitSteigerung = New System.Windows.Forms.Button()
        Me.lblSteigerung = New System.Windows.Forms.Label()
        Me.btnPanik = New System.Windows.Forms.Button()
        Me.pnlFilter = New System.Windows.Forms.Panel()
        Me.btnGruppe = New System.Windows.Forms.Button()
        Me.lblGruppe = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.bgwResult = New System.ComponentModel.BackgroundWorker()
        Me.bgwPunkte = New System.ComponentModel.BackgroundWorker()
        Me.bgwSteigerung = New System.ComponentModel.BackgroundWorker()
        Me.bgwVersuch = New System.ComponentModel.BackgroundWorker()
        Me.bgwRechnen = New System.ComponentModel.BackgroundWorker()
        Me.bgwServer = New System.ComponentModel.BackgroundWorker()
        Me.bgwClient = New System.ComponentModel.BackgroundWorker()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.dgvLeader = New System.Windows.Forms.DataGridView()
        Me.Status.SuspendLayout()
        Me.MenuStrip2.SuspendLayout()
        Me.ContextMenuGrid.SuspendLayout()
        Me.pnlNotizen.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pnlManuell.SuspendLayout()
        Me.pnlT.SuspendLayout()
        Me.pnlW1.SuspendLayout()
        Me.pnlW2.SuspendLayout()
        Me.pnlW3.SuspendLayout()
        Me.pnlForm.SuspendLayout()
        CType(Me.dgvTeam, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlWertung.SuspendLayout()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvGruppe, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSteigerung.SuspendLayout()
        CType(Me.nudHantellast, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFilter.SuspendLayout()
        CType(Me.dgvLeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnAufruf
        '
        Me.btnAufruf.BackColor = System.Drawing.SystemColors.ControlLight
        Me.btnAufruf.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLightLight
        resources.ApplyResources(Me.btnAufruf, "btnAufruf")
        Me.btnAufruf.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnAufruf.Name = "btnAufruf"
        Me.btnAufruf.TabStop = False
        Me.btnAufruf.Tag = ""
        Me.ToolTip1.SetToolTip(Me.btnAufruf, resources.GetString("btnAufruf.ToolTip"))
        Me.btnAufruf.UseVisualStyleBackColor = False
        '
        'Status
        '
        resources.ApplyResources(Me.Status, "Status")
        Me.Status.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.Status.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.statusMsg, Me.Progress1, Me.statusVersuch, Me.statusResultat, Me.statusSteigerung, Me.ComMsg})
        Me.Status.Name = "Status"
        '
        'statusMsg
        '
        resources.ApplyResources(Me.statusMsg, "statusMsg")
        Me.statusMsg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.statusMsg.ForeColor = System.Drawing.Color.Red
        Me.statusMsg.Name = "statusMsg"
        '
        'Progress1
        '
        Me.Progress1.Name = "Progress1"
        resources.ApplyResources(Me.Progress1, "Progress1")
        Me.Progress1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        '
        'statusVersuch
        '
        resources.ApplyResources(Me.statusVersuch, "statusVersuch")
        Me.statusVersuch.Name = "statusVersuch"
        '
        'statusResultat
        '
        resources.ApplyResources(Me.statusResultat, "statusResultat")
        Me.statusResultat.Name = "statusResultat"
        '
        'statusSteigerung
        '
        resources.ApplyResources(Me.statusSteigerung, "statusSteigerung")
        Me.statusSteigerung.Name = "statusSteigerung"
        '
        'ComMsg
        '
        Me.ComMsg.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.ComMsg, "ComMsg")
        Me.ComMsg.Name = "ComMsg"
        Me.ComMsg.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.ComMsg.Spring = True
        '
        'MenuStrip2
        '
        resources.ApplyResources(Me.MenuStrip2, "MenuStrip2")
        Me.MenuStrip2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.MenuStrip2.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuPause, Me.mnuWertung, Me.mnuOptionen, Me.mnuExtras, Me.mnuHupe})
        Me.MenuStrip2.Name = "MenuStrip2"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHeberAuswechseln, Me.mnuPrognose, Me.sep5, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        resources.ApplyResources(Me.mnuDatei, "mnuDatei")
        '
        'mnuHeberAuswechseln
        '
        Me.mnuHeberAuswechseln.ForeColor = System.Drawing.Color.Firebrick
        Me.mnuHeberAuswechseln.Name = "mnuHeberAuswechseln"
        resources.ApplyResources(Me.mnuHeberAuswechseln, "mnuHeberAuswechseln")
        '
        'mnuPrognose
        '
        resources.ApplyResources(Me.mnuPrognose, "mnuPrognose")
        Me.mnuPrognose.Name = "mnuPrognose"
        '
        'sep5
        '
        Me.sep5.Name = "sep5"
        resources.ApplyResources(Me.sep5, "sep5")
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        resources.ApplyResources(Me.mnuBeenden, "mnuBeenden")
        '
        'mnuPause
        '
        Me.mnuPause.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuPause.MergeIndex = 2
        Me.mnuPause.Name = "mnuPause"
        resources.ApplyResources(Me.mnuPause, "mnuPause")
        '
        'mnuWertung
        '
        Me.mnuWertung.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.mnuWertung.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuBlaueTaste, Me.mnuClearHeber, Me.ToolStripMenuItem1, Me.mnuClearHupe})
        resources.ApplyResources(Me.mnuWertung, "mnuWertung")
        Me.mnuWertung.ForeColor = System.Drawing.SystemColors.ControlText
        Me.mnuWertung.Name = "mnuWertung"
        '
        'mnuBlaueTaste
        '
        Me.mnuBlaueTaste.Name = "mnuBlaueTaste"
        resources.ApplyResources(Me.mnuBlaueTaste, "mnuBlaueTaste")
        '
        'mnuClearHeber
        '
        Me.mnuClearHeber.Name = "mnuClearHeber"
        resources.ApplyResources(Me.mnuClearHeber, "mnuClearHeber")
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        resources.ApplyResources(Me.ToolStripMenuItem1, "ToolStripMenuItem1")
        '
        'mnuClearHupe
        '
        Me.mnuClearHupe.Name = "mnuClearHupe"
        resources.ApplyResources(Me.mnuClearHupe, "mnuClearHupe")
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMeldungen, Me.mnuKonflikte, Me.ToolStripMenuItem7, Me.mnuEingabePrüfen, Me.mnuAktuellenVersuch, Me.mnuConfirm, Me.mnuTechniknote, Me.mnuSplitItem1, Me.mnuSplit_AG})
        Me.mnuOptionen.MergeIndex = 3
        Me.mnuOptionen.Name = "mnuOptionen"
        resources.ApplyResources(Me.mnuOptionen, "mnuOptionen")
        '
        'mnuMeldungen
        '
        Me.mnuMeldungen.CheckOnClick = True
        Me.mnuMeldungen.MergeIndex = 3
        Me.mnuMeldungen.Name = "mnuMeldungen"
        resources.ApplyResources(Me.mnuMeldungen, "mnuMeldungen")
        '
        'mnuKonflikte
        '
        Me.mnuKonflikte.Checked = True
        Me.mnuKonflikte.CheckOnClick = True
        Me.mnuKonflikte.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuKonflikte.MergeIndex = 3
        Me.mnuKonflikte.Name = "mnuKonflikte"
        resources.ApplyResources(Me.mnuKonflikte, "mnuKonflikte")
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        resources.ApplyResources(Me.ToolStripMenuItem7, "ToolStripMenuItem7")
        '
        'mnuEingabePrüfen
        '
        Me.mnuEingabePrüfen.Checked = True
        Me.mnuEingabePrüfen.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuEingabePrüfen.Name = "mnuEingabePrüfen"
        resources.ApplyResources(Me.mnuEingabePrüfen, "mnuEingabePrüfen")
        '
        'mnuAktuellenVersuch
        '
        Me.mnuAktuellenVersuch.Checked = True
        Me.mnuAktuellenVersuch.CheckOnClick = True
        Me.mnuAktuellenVersuch.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuAktuellenVersuch.Name = "mnuAktuellenVersuch"
        resources.ApplyResources(Me.mnuAktuellenVersuch, "mnuAktuellenVersuch")
        '
        'mnuConfirm
        '
        Me.mnuConfirm.Checked = True
        Me.mnuConfirm.CheckOnClick = True
        Me.mnuConfirm.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuConfirm.Name = "mnuConfirm"
        resources.ApplyResources(Me.mnuConfirm, "mnuConfirm")
        '
        'mnuTechniknote
        '
        Me.mnuTechniknote.CheckOnClick = True
        Me.mnuTechniknote.Name = "mnuTechniknote"
        resources.ApplyResources(Me.mnuTechniknote, "mnuTechniknote")
        Me.mnuTechniknote.Tag = "Leader"
        '
        'mnuSplitItem1
        '
        Me.mnuSplitItem1.Name = "mnuSplitItem1"
        resources.ApplyResources(Me.mnuSplitItem1, "mnuSplitItem1")
        Me.mnuSplitItem1.Tag = "Leader"
        '
        'mnuSplit_AG
        '
        Me.mnuSplit_AG.Name = "mnuSplit_AG"
        resources.ApplyResources(Me.mnuSplit_AG, "mnuSplit_AG")
        Me.mnuSplit_AG.Tag = "Leader"
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHeber, Me.mnuVerein, Me.ToolStripMenuItem2, Me.mnuMauszeiger, Me.mnuTableLayout, Me.ToolStripMenuItem8, Me.mnuKR_Init, Me.mnuTCP, Me.mnuZNPadSperrzeit})
        Me.mnuExtras.MergeIndex = 3
        Me.mnuExtras.Name = "mnuExtras"
        resources.ApplyResources(Me.mnuExtras, "mnuExtras")
        '
        'mnuHeber
        '
        Me.mnuHeber.Name = "mnuHeber"
        resources.ApplyResources(Me.mnuHeber, "mnuHeber")
        Me.mnuHeber.Tag = "Leader"
        '
        'mnuVerein
        '
        Me.mnuVerein.Name = "mnuVerein"
        resources.ApplyResources(Me.mnuVerein, "mnuVerein")
        Me.mnuVerein.Tag = "Leader"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        resources.ApplyResources(Me.ToolStripMenuItem2, "ToolStripMenuItem2")
        Me.ToolStripMenuItem2.Tag = "Leader"
        '
        'mnuMauszeiger
        '
        Me.mnuMauszeiger.Name = "mnuMauszeiger"
        resources.ApplyResources(Me.mnuMauszeiger, "mnuMauszeiger")
        Me.mnuMauszeiger.Tag = "Leader"
        '
        'mnuTableLayout
        '
        Me.mnuTableLayout.CheckOnClick = True
        Me.mnuTableLayout.MergeIndex = 3
        Me.mnuTableLayout.Name = "mnuTableLayout"
        resources.ApplyResources(Me.mnuTableLayout, "mnuTableLayout")
        Me.mnuTableLayout.Tag = "Leader"
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        resources.ApplyResources(Me.ToolStripMenuItem8, "ToolStripMenuItem8")
        Me.ToolStripMenuItem8.Tag = ""
        '
        'mnuKR_Init
        '
        Me.mnuKR_Init.Name = "mnuKR_Init"
        resources.ApplyResources(Me.mnuKR_Init, "mnuKR_Init")
        Me.mnuKR_Init.Tag = "Leader"
        '
        'mnuTCP
        '
        Me.mnuTCP.Name = "mnuTCP"
        resources.ApplyResources(Me.mnuTCP, "mnuTCP")
        '
        'mnuZNPadSperrzeit
        '
        Me.mnuZNPadSperrzeit.Name = "mnuZNPadSperrzeit"
        resources.ApplyResources(Me.mnuZNPadSperrzeit, "mnuZNPadSperrzeit")
        '
        'mnuHupe
        '
        Me.mnuHupe.Image = Global.Gewichtheben.My.Resources.Resources.mute
        Me.mnuHupe.Name = "mnuHupe"
        resources.ApplyResources(Me.mnuHupe, "mnuHupe")
        '
        'ContextMenuGrid
        '
        Me.ContextMenuGrid.BackColor = System.Drawing.SystemColors.Info
        Me.ContextMenuGrid.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuGrid.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHeberWechsel, Me.mnuHighLightAttempts, Me.mnuResultsLifter, Me.ToolStripSeparator1, Me.mnuSteigerung, Me.mnuBestätigung, Me.ToolStripMenuItem6, Me.mnuVerzicht, Me.ToolStripMenuItem3, Me.mnuKorrektur, Me.ToolStripMenuItem4, Me.cmnuHeber, Me.cmnuVerein, Me.mnuPanik})
        Me.ContextMenuGrid.Name = "ContextMenu"
        Me.ContextMenuGrid.ShowImageMargin = False
        resources.ApplyResources(Me.ContextMenuGrid, "ContextMenuGrid")
        Me.ContextMenuGrid.Tag = "9"
        '
        'mnuHeberWechsel
        '
        Me.mnuHeberWechsel.ForeColor = System.Drawing.Color.Red
        Me.mnuHeberWechsel.Name = "mnuHeberWechsel"
        resources.ApplyResources(Me.mnuHeberWechsel, "mnuHeberWechsel")
        '
        'mnuHighLightAttempts
        '
        Me.mnuHighLightAttempts.AutoToolTip = True
        Me.mnuHighLightAttempts.Name = "mnuHighLightAttempts"
        resources.ApplyResources(Me.mnuHighLightAttempts, "mnuHighLightAttempts")
        '
        'mnuResultsLifter
        '
        Me.mnuResultsLifter.Name = "mnuResultsLifter"
        resources.ApplyResources(Me.mnuResultsLifter, "mnuResultsLifter")
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        resources.ApplyResources(Me.ToolStripSeparator1, "ToolStripSeparator1")
        '
        'mnuSteigerung
        '
        Me.mnuSteigerung.Name = "mnuSteigerung"
        resources.ApplyResources(Me.mnuSteigerung, "mnuSteigerung")
        '
        'mnuBestätigung
        '
        Me.mnuBestätigung.Name = "mnuBestätigung"
        resources.ApplyResources(Me.mnuBestätigung, "mnuBestätigung")
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        resources.ApplyResources(Me.ToolStripMenuItem6, "ToolStripMenuItem6")
        '
        'mnuVerzicht
        '
        Me.mnuVerzicht.Name = "mnuVerzicht"
        resources.ApplyResources(Me.mnuVerzicht, "mnuVerzicht")
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        resources.ApplyResources(Me.ToolStripMenuItem3, "ToolStripMenuItem3")
        Me.ToolStripMenuItem3.Tag = "Leader"
        '
        'mnuKorrektur
        '
        Me.mnuKorrektur.Name = "mnuKorrektur"
        resources.ApplyResources(Me.mnuKorrektur, "mnuKorrektur")
        Me.mnuKorrektur.Tag = "Leader"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        resources.ApplyResources(Me.ToolStripMenuItem4, "ToolStripMenuItem4")
        Me.ToolStripMenuItem4.Tag = "Leader"
        '
        'cmnuHeber
        '
        Me.cmnuHeber.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.cmnuHeber.Name = "cmnuHeber"
        resources.ApplyResources(Me.cmnuHeber, "cmnuHeber")
        Me.cmnuHeber.Tag = "Leader"
        '
        'cmnuVerein
        '
        Me.cmnuVerein.Name = "cmnuVerein"
        resources.ApplyResources(Me.cmnuVerein, "cmnuVerein")
        Me.cmnuVerein.Tag = "Leader"
        '
        'mnuPanik
        '
        Me.mnuPanik.AutoToolTip = True
        Me.mnuPanik.BackColor = System.Drawing.Color.FromArgb(CType(CType(175, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(125, Byte), Integer))
        resources.ApplyResources(Me.mnuPanik, "mnuPanik")
        Me.mnuPanik.ForeColor = System.Drawing.Color.White
        Me.mnuPanik.Name = "mnuPanik"
        Me.mnuPanik.Tag = "Leader"
        '
        'btnSteigerung
        '
        Me.btnSteigerung.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.btnSteigerung, "btnSteigerung")
        Me.btnSteigerung.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnSteigerung.Name = "btnSteigerung"
        Me.btnSteigerung.TabStop = False
        Me.btnSteigerung.Tag = ""
        Me.ToolTip1.SetToolTip(Me.btnSteigerung, resources.GetString("btnSteigerung.ToolTip"))
        Me.btnSteigerung.UseVisualStyleBackColor = False
        '
        'btnNext
        '
        resources.ApplyResources(Me.btnNext, "btnNext")
        Me.btnNext.BackColor = System.Drawing.Color.FromArgb(CType(CType(140, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(140, Byte), Integer))
        Me.btnNext.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLight
        Me.btnNext.Name = "btnNext"
        Me.btnNext.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnNext, resources.GetString("btnNext.ToolTip"))
        Me.btnNext.UseVisualStyleBackColor = False
        '
        'btnBestätigung
        '
        Me.btnBestätigung.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.btnBestätigung, "btnBestätigung")
        Me.btnBestätigung.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnBestätigung.Name = "btnBestätigung"
        Me.btnBestätigung.TabStop = False
        Me.btnBestätigung.Tag = ""
        Me.ToolTip1.SetToolTip(Me.btnBestätigung, resources.GetString("btnBestätigung.ToolTip"))
        Me.btnBestätigung.UseVisualStyleBackColor = False
        '
        'pnlNotizen
        '
        Me.pnlNotizen.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.pnlNotizen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlNotizen.Controls.Add(Me.btnUndo_Notiz)
        Me.pnlNotizen.Controls.Add(Me.lblID)
        Me.pnlNotizen.Controls.Add(Me.btnDelete_Notiz)
        Me.pnlNotizen.Controls.Add(Me.btnSave_Notiz)
        Me.pnlNotizen.Controls.Add(Me.Panel1)
        Me.pnlNotizen.Controls.Add(Me.chkNotizUnderline)
        Me.pnlNotizen.Controls.Add(Me.chkNotizItalic)
        Me.pnlNotizen.Controls.Add(Me.chkNotizBold)
        Me.pnlNotizen.Controls.Add(Me.btnNotizForeColor)
        Me.pnlNotizen.Controls.Add(Me.btnNotizExit)
        Me.pnlNotizen.Controls.Add(Me.lblNotizen)
        resources.ApplyResources(Me.pnlNotizen, "pnlNotizen")
        Me.pnlNotizen.Name = "pnlNotizen"
        '
        'btnUndo_Notiz
        '
        Me.btnUndo_Notiz.DialogResult = System.Windows.Forms.DialogResult.Cancel
        resources.ApplyResources(Me.btnUndo_Notiz, "btnUndo_Notiz")
        Me.btnUndo_Notiz.Name = "btnUndo_Notiz"
        Me.btnUndo_Notiz.TabStop = False
        Me.btnUndo_Notiz.UseVisualStyleBackColor = True
        '
        'lblID
        '
        resources.ApplyResources(Me.lblID, "lblID")
        Me.lblID.Name = "lblID"
        '
        'btnDelete_Notiz
        '
        Me.btnDelete_Notiz.DialogResult = System.Windows.Forms.DialogResult.Cancel
        resources.ApplyResources(Me.btnDelete_Notiz, "btnDelete_Notiz")
        Me.btnDelete_Notiz.Name = "btnDelete_Notiz"
        Me.btnDelete_Notiz.TabStop = False
        Me.btnDelete_Notiz.UseVisualStyleBackColor = True
        '
        'btnSave_Notiz
        '
        resources.ApplyResources(Me.btnSave_Notiz, "btnSave_Notiz")
        Me.btnSave_Notiz.Name = "btnSave_Notiz"
        Me.btnSave_Notiz.TabStop = False
        Me.btnSave_Notiz.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(235, Byte), Integer))
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.rtfNotiz)
        Me.Panel1.Name = "Panel1"
        '
        'rtfNotiz
        '
        resources.ApplyResources(Me.rtfNotiz, "rtfNotiz")
        Me.rtfNotiz.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(235, Byte), Integer))
        Me.rtfNotiz.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtfNotiz.DetectUrls = False
        Me.rtfNotiz.HideSelection = False
        Me.rtfNotiz.Name = "rtfNotiz"
        '
        'chkNotizUnderline
        '
        resources.ApplyResources(Me.chkNotizUnderline, "chkNotizUnderline")
        Me.chkNotizUnderline.AutoCheck = False
        Me.chkNotizUnderline.Name = "chkNotizUnderline"
        Me.chkNotizUnderline.TabStop = False
        Me.chkNotizUnderline.UseVisualStyleBackColor = True
        '
        'chkNotizItalic
        '
        resources.ApplyResources(Me.chkNotizItalic, "chkNotizItalic")
        Me.chkNotizItalic.AutoCheck = False
        Me.chkNotizItalic.Name = "chkNotizItalic"
        Me.chkNotizItalic.TabStop = False
        Me.chkNotizItalic.UseVisualStyleBackColor = True
        '
        'chkNotizBold
        '
        resources.ApplyResources(Me.chkNotizBold, "chkNotizBold")
        Me.chkNotizBold.AutoCheck = False
        Me.chkNotizBold.Name = "chkNotizBold"
        Me.chkNotizBold.TabStop = False
        Me.chkNotizBold.UseVisualStyleBackColor = True
        '
        'btnNotizForeColor
        '
        Me.btnNotizForeColor.BackColor = System.Drawing.Color.Black
        Me.btnNotizForeColor.FlatAppearance.BorderSize = 2
        resources.ApplyResources(Me.btnNotizForeColor, "btnNotizForeColor")
        Me.btnNotizForeColor.Name = "btnNotizForeColor"
        Me.btnNotizForeColor.TabStop = False
        Me.btnNotizForeColor.UseVisualStyleBackColor = False
        '
        'btnNotizExit
        '
        Me.btnNotizExit.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnNotizExit.FlatAppearance.BorderColor = System.Drawing.Color.White
        resources.ApplyResources(Me.btnNotizExit, "btnNotizExit")
        Me.btnNotizExit.ForeColor = System.Drawing.Color.White
        Me.btnNotizExit.Name = "btnNotizExit"
        Me.btnNotizExit.TabStop = False
        Me.btnNotizExit.UseVisualStyleBackColor = False
        '
        'lblNotizen
        '
        Me.lblNotizen.AutoEllipsis = True
        Me.lblNotizen.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblNotizen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.lblNotizen, "lblNotizen")
        Me.lblNotizen.ForeColor = System.Drawing.Color.White
        Me.lblNotizen.Name = "lblNotizen"
        '
        'btnVerzicht
        '
        Me.btnVerzicht.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.btnVerzicht, "btnVerzicht")
        Me.btnVerzicht.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnVerzicht.Name = "btnVerzicht"
        Me.btnVerzicht.TabStop = False
        Me.btnVerzicht.Tag = ""
        Me.ToolTip1.SetToolTip(Me.btnVerzicht, resources.GetString("btnVerzicht.ToolTip"))
        Me.btnVerzicht.UseVisualStyleBackColor = False
        '
        'chkBH
        '
        Me.chkBH.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        resources.ApplyResources(Me.chkBH, "chkBH")
        Me.chkBH.Checked = True
        Me.chkBH.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkBH.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.chkBH.Name = "chkBH"
        Me.chkBH.UseCompatibleTextRendering = True
        Me.chkBH.UseVisualStyleBackColor = False
        '
        'btnKorrektur
        '
        Me.btnKorrektur.BackColor = System.Drawing.SystemColors.ControlLight
        resources.ApplyResources(Me.btnKorrektur, "btnKorrektur")
        Me.btnKorrektur.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnKorrektur.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnKorrektur.Image = Global.Gewichtheben.My.Resources.Resources.Exclamation24red1
        Me.btnKorrektur.Name = "btnKorrektur"
        Me.btnKorrektur.TabStop = False
        Me.btnKorrektur.Tag = ""
        Me.ToolTip1.SetToolTip(Me.btnKorrektur, resources.GetString("btnKorrektur.ToolTip"))
        Me.btnKorrektur.UseCompatibleTextRendering = True
        Me.btnKorrektur.UseVisualStyleBackColor = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "btnGray.png")
        Me.ImageList1.Images.SetKeyName(1, "btnGreen.png")
        Me.ImageList1.Images.SetKeyName(2, "btnRed.png")
        Me.ImageList1.Images.SetKeyName(3, "client_up.png")
        Me.ImageList1.Images.SetKeyName(4, "client_down.png")
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label13.Name = "Label13"
        Me.Label13.UseCompatibleTextRendering = True
        '
        'cboDurchgang
        '
        Me.cboDurchgang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cboDurchgang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboDurchgang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.cboDurchgang, "cboDurchgang")
        Me.cboDurchgang.FormattingEnabled = True
        Me.cboDurchgang.Items.AddRange(New Object() {resources.GetString("cboDurchgang.Items"), resources.GetString("cboDurchgang.Items1")})
        Me.cboDurchgang.Name = "cboDurchgang"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label11.Name = "Label11"
        Me.Label11.UseCompatibleTextRendering = True
        '
        'pnlManuell
        '
        Me.pnlManuell.BackColor = System.Drawing.Color.Gray
        Me.pnlManuell.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlManuell.Controls.Add(Me.btnFertig)
        Me.pnlManuell.Controls.Add(Me.pnlT)
        Me.pnlManuell.Controls.Add(Me.pnlW1)
        Me.pnlManuell.Controls.Add(Me.pnlW2)
        Me.pnlManuell.Controls.Add(Me.pnlW3)
        Me.pnlManuell.Controls.Add(Me.btnExitManuell)
        Me.pnlManuell.Controls.Add(Me.lblManuell)
        Me.pnlManuell.Controls.Add(Me.Dummy)
        resources.ApplyResources(Me.pnlManuell, "pnlManuell")
        Me.pnlManuell.Name = "pnlManuell"
        '
        'btnFertig
        '
        resources.ApplyResources(Me.btnFertig, "btnFertig")
        Me.btnFertig.Name = "btnFertig"
        Me.btnFertig.UseVisualStyleBackColor = True
        '
        'pnlT
        '
        Me.pnlT.Controls.Add(Me.txtT3)
        Me.pnlT.Controls.Add(Me.txtT2)
        Me.pnlT.Controls.Add(Me.txtT1)
        resources.ApplyResources(Me.pnlT, "pnlT")
        Me.pnlT.Name = "pnlT"
        '
        'txtT3
        '
        resources.ApplyResources(Me.txtT3, "txtT3")
        Me.txtT3.Name = "txtT3"
        '
        'txtT2
        '
        resources.ApplyResources(Me.txtT2, "txtT2")
        Me.txtT2.Name = "txtT2"
        '
        'txtT1
        '
        resources.ApplyResources(Me.txtT1, "txtT1")
        Me.txtT1.Name = "txtT1"
        '
        'pnlW1
        '
        Me.pnlW1.Controls.Add(Me.btnU1)
        Me.pnlW1.Controls.Add(Me.btnG1)
        resources.ApplyResources(Me.pnlW1, "pnlW1")
        Me.pnlW1.Name = "pnlW1"
        '
        'btnU1
        '
        resources.ApplyResources(Me.btnU1, "btnU1")
        Me.btnU1.AutoCheck = False
        Me.btnU1.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnU1.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.btnU1.FlatAppearance.BorderSize = 2
        Me.btnU1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red
        Me.btnU1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me.btnU1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(65, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.btnU1.ImageList = Me.ImageList64
        Me.btnU1.Name = "btnU1"
        Me.btnU1.UseVisualStyleBackColor = False
        '
        'ImageList64
        '
        Me.ImageList64.ImageStream = CType(resources.GetObject("ImageList64.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList64.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList64.Images.SetKeyName(0, "Btn-Red-64.png")
        Me.ImageList64.Images.SetKeyName(1, "Btn-Red-64-Hover.png")
        Me.ImageList64.Images.SetKeyName(2, "Btn-Red-64-down.png")
        Me.ImageList64.Images.SetKeyName(3, "Btn-White-64.png")
        Me.ImageList64.Images.SetKeyName(4, "Btn-White-64-Down.png")
        Me.ImageList64.Images.SetKeyName(5, "Btn-White-64-Hover.png")
        '
        'btnG1
        '
        resources.ApplyResources(Me.btnG1, "btnG1")
        Me.btnG1.AutoCheck = False
        Me.btnG1.BackColor = System.Drawing.Color.LightGray
        Me.btnG1.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.btnG1.FlatAppearance.BorderSize = 2
        Me.btnG1.FlatAppearance.CheckedBackColor = System.Drawing.Color.White
        Me.btnG1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gainsboro
        Me.btnG1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.btnG1.ImageList = Me.ImageList64
        Me.btnG1.Name = "btnG1"
        Me.btnG1.UseVisualStyleBackColor = False
        '
        'pnlW2
        '
        Me.pnlW2.Controls.Add(Me.btnU2)
        Me.pnlW2.Controls.Add(Me.btnG2)
        resources.ApplyResources(Me.pnlW2, "pnlW2")
        Me.pnlW2.Name = "pnlW2"
        '
        'btnU2
        '
        resources.ApplyResources(Me.btnU2, "btnU2")
        Me.btnU2.AutoCheck = False
        Me.btnU2.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnU2.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.btnU2.FlatAppearance.BorderSize = 2
        Me.btnU2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red
        Me.btnU2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me.btnU2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(65, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.btnU2.ImageList = Me.ImageList64
        Me.btnU2.Name = "btnU2"
        Me.btnU2.UseVisualStyleBackColor = False
        '
        'btnG2
        '
        resources.ApplyResources(Me.btnG2, "btnG2")
        Me.btnG2.AutoCheck = False
        Me.btnG2.BackColor = System.Drawing.Color.LightGray
        Me.btnG2.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.btnG2.FlatAppearance.BorderSize = 2
        Me.btnG2.FlatAppearance.CheckedBackColor = System.Drawing.Color.White
        Me.btnG2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gainsboro
        Me.btnG2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.btnG2.ImageList = Me.ImageList64
        Me.btnG2.Name = "btnG2"
        Me.btnG2.UseVisualStyleBackColor = False
        '
        'pnlW3
        '
        Me.pnlW3.Controls.Add(Me.btnU3)
        Me.pnlW3.Controls.Add(Me.btnG3)
        resources.ApplyResources(Me.pnlW3, "pnlW3")
        Me.pnlW3.Name = "pnlW3"
        '
        'btnU3
        '
        resources.ApplyResources(Me.btnU3, "btnU3")
        Me.btnU3.AutoCheck = False
        Me.btnU3.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.btnU3.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.btnU3.FlatAppearance.BorderSize = 2
        Me.btnU3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red
        Me.btnU3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(20, Byte), Integer), CType(CType(20, Byte), Integer))
        Me.btnU3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(65, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.btnU3.ImageList = Me.ImageList64
        Me.btnU3.Name = "btnU3"
        Me.btnU3.UseVisualStyleBackColor = False
        '
        'btnG3
        '
        resources.ApplyResources(Me.btnG3, "btnG3")
        Me.btnG3.AutoCheck = False
        Me.btnG3.BackColor = System.Drawing.Color.LightGray
        Me.btnG3.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.btnG3.FlatAppearance.BorderSize = 2
        Me.btnG3.FlatAppearance.CheckedBackColor = System.Drawing.Color.White
        Me.btnG3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gainsboro
        Me.btnG3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.btnG3.ImageList = Me.ImageList64
        Me.btnG3.Name = "btnG3"
        Me.btnG3.UseVisualStyleBackColor = False
        '
        'btnExitManuell
        '
        Me.btnExitManuell.BackColor = System.Drawing.Color.Black
        Me.btnExitManuell.FlatAppearance.BorderColor = System.Drawing.Color.White
        resources.ApplyResources(Me.btnExitManuell, "btnExitManuell")
        Me.btnExitManuell.ForeColor = System.Drawing.Color.White
        Me.btnExitManuell.Name = "btnExitManuell"
        Me.btnExitManuell.TabStop = False
        Me.btnExitManuell.UseVisualStyleBackColor = False
        '
        'lblManuell
        '
        Me.lblManuell.BackColor = System.Drawing.Color.Black
        Me.lblManuell.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblManuell.Cursor = System.Windows.Forms.Cursors.Default
        resources.ApplyResources(Me.lblManuell, "lblManuell")
        Me.lblManuell.ForeColor = System.Drawing.Color.White
        Me.lblManuell.Name = "lblManuell"
        '
        'Dummy
        '
        resources.ApplyResources(Me.Dummy, "Dummy")
        Me.Dummy.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.Dummy.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.Dummy.FlatAppearance.BorderSize = 0
        Me.Dummy.ForeColor = System.Drawing.Color.Gray
        Me.Dummy.Name = "Dummy"
        Me.Dummy.UseVisualStyleBackColor = True
        '
        'pnlForm
        '
        Me.pnlForm.Controls.Add(Me.btnMinute2)
        Me.pnlForm.Controls.Add(Me.btnMinute1)
        Me.pnlForm.Controls.Add(Me.btnNext)
        Me.pnlForm.Controls.Add(Me.btnKorrektur)
        Me.pnlForm.Controls.Add(Me.btnAufruf)
        Me.pnlForm.Controls.Add(Me.btnSteigerung)
        Me.pnlForm.Controls.Add(Me.btnVerzicht)
        Me.pnlForm.Controls.Add(Me.btnBestätigung)
        Me.pnlForm.Controls.Add(Me.dgvTeam)
        Me.pnlForm.Controls.Add(Me.pnlWertung)
        Me.pnlForm.Controls.Add(Me.xAufruf)
        resources.ApplyResources(Me.pnlForm, "pnlForm")
        Me.pnlForm.Name = "pnlForm"
        '
        'btnMinute2
        '
        Me.btnMinute2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.btnMinute2.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLightLight
        resources.ApplyResources(Me.btnMinute2, "btnMinute2")
        Me.btnMinute2.ForeColor = System.Drawing.Color.MediumBlue
        Me.btnMinute2.Name = "btnMinute2"
        Me.btnMinute2.TabStop = False
        Me.btnMinute2.Tag = "120"
        Me.ToolTip1.SetToolTip(Me.btnMinute2, resources.GetString("btnMinute2.ToolTip"))
        Me.btnMinute2.UseVisualStyleBackColor = False
        '
        'btnMinute1
        '
        Me.btnMinute1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.btnMinute1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLightLight
        resources.ApplyResources(Me.btnMinute1, "btnMinute1")
        Me.btnMinute1.ForeColor = System.Drawing.Color.MediumBlue
        Me.btnMinute1.Name = "btnMinute1"
        Me.btnMinute1.TabStop = False
        Me.btnMinute1.Tag = "60"
        Me.ToolTip1.SetToolTip(Me.btnMinute1, resources.GetString("btnMinute1.ToolTip"))
        Me.btnMinute1.UseVisualStyleBackColor = False
        '
        'dgvTeam
        '
        Me.dgvTeam.AllowUserToAddRows = False
        Me.dgvTeam.AllowUserToDeleteRows = False
        Me.dgvTeam.AllowUserToResizeColumns = False
        Me.dgvTeam.AllowUserToResizeRows = False
        Me.dgvTeam.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvTeam.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvTeam.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.dgvTeam.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTeam.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvTeam.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle1.Padding = New System.Windows.Forms.Padding(0, 2, 0, 2)
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTeam.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        resources.ApplyResources(Me.dgvTeam, "dgvTeam")
        Me.dgvTeam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvTeam.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Rolle, Me.Team, Me.Gegner, Me.mReissen, Me.mStossen, Me.mHeben, Me.mPunkte, Me.Punkte2, Me.Platz})
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.Padding = New System.Windows.Forms.Padding(2, 0, 2, 0)
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTeam.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgvTeam.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvTeam.EnableHeadersVisualStyles = False
        Me.dgvTeam.Name = "dgvTeam"
        Me.dgvTeam.ReadOnly = True
        Me.dgvTeam.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvTeam.RowHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvTeam.RowHeadersVisible = False
        Me.dgvTeam.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvTeam.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTeam.ShowCellToolTips = False
        Me.dgvTeam.ShowEditingIcon = False
        Me.dgvTeam.ShowRowErrors = False
        Me.dgvTeam.TabStop = False
        '
        'Rolle
        '
        Me.Rolle.DataPropertyName = "Rolle"
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Gold
        Me.Rolle.DefaultCellStyle = DataGridViewCellStyle2
        resources.ApplyResources(Me.Rolle, "Rolle")
        Me.Rolle.Name = "Rolle"
        Me.Rolle.ReadOnly = True
        Me.Rolle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Team
        '
        Me.Team.DataPropertyName = "Kurzname"
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Gold
        Me.Team.DefaultCellStyle = DataGridViewCellStyle3
        resources.ApplyResources(Me.Team, "Team")
        Me.Team.Name = "Team"
        Me.Team.ReadOnly = True
        Me.Team.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Gegner
        '
        Me.Gegner.DataPropertyName = "Gegner"
        resources.ApplyResources(Me.Gegner, "Gegner")
        Me.Gegner.Name = "Gegner"
        Me.Gegner.ReadOnly = True
        '
        'mReissen
        '
        Me.mReissen.DataPropertyName = "Reissen"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mReissen.DefaultCellStyle = DataGridViewCellStyle4
        resources.ApplyResources(Me.mReissen, "mReissen")
        Me.mReissen.Name = "mReissen"
        Me.mReissen.ReadOnly = True
        Me.mReissen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'mStossen
        '
        Me.mStossen.DataPropertyName = "Stossen"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mStossen.DefaultCellStyle = DataGridViewCellStyle5
        resources.ApplyResources(Me.mStossen, "mStossen")
        Me.mStossen.Name = "mStossen"
        Me.mStossen.ReadOnly = True
        Me.mStossen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'mHeben
        '
        Me.mHeben.DataPropertyName = "Heben"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mHeben.DefaultCellStyle = DataGridViewCellStyle6
        resources.ApplyResources(Me.mHeben, "mHeben")
        Me.mHeben.Name = "mHeben"
        Me.mHeben.ReadOnly = True
        Me.mHeben.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'mPunkte
        '
        Me.mPunkte.DataPropertyName = "Punkte"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.Format = "N0"
        Me.mPunkte.DefaultCellStyle = DataGridViewCellStyle7
        resources.ApplyResources(Me.mPunkte, "mPunkte")
        Me.mPunkte.Name = "mPunkte"
        Me.mPunkte.ReadOnly = True
        Me.mPunkte.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Punkte2
        '
        Me.Punkte2.DataPropertyName = "Punkte2"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.Format = "N0"
        Me.Punkte2.DefaultCellStyle = DataGridViewCellStyle8
        resources.ApplyResources(Me.Punkte2, "Punkte2")
        Me.Punkte2.Name = "Punkte2"
        Me.Punkte2.ReadOnly = True
        Me.Punkte2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Platz
        '
        Me.Platz.DataPropertyName = "Platz"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Format = "N0"
        Me.Platz.DefaultCellStyle = DataGridViewCellStyle9
        resources.ApplyResources(Me.Platz, "Platz")
        Me.Platz.Name = "Platz"
        Me.Platz.ReadOnly = True
        Me.Platz.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'pnlWertung
        '
        resources.ApplyResources(Me.pnlWertung, "pnlWertung")
        Me.pnlWertung.BackColor = System.Drawing.Color.Black
        Me.pnlWertung.Controls.Add(Me.xGültig1)
        Me.pnlWertung.Controls.Add(Me.xTechniknote)
        Me.pnlWertung.Controls.Add(Me.xGültig3)
        Me.pnlWertung.Controls.Add(Me.xGültig2)
        Me.pnlWertung.Name = "pnlWertung"
        '
        'xGültig1
        '
        Me.xGültig1.BackColor = System.Drawing.Color.Black
        Me.xGültig1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig1.Cursor = System.Windows.Forms.Cursors.Arrow
        resources.ApplyResources(Me.xGültig1, "xGültig1")
        Me.xGültig1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig1.Name = "xGültig1"
        Me.xGültig1.ReadOnly = True
        Me.xGültig1.TabStop = False
        '
        'xTechniknote
        '
        Me.xTechniknote.BackColor = System.Drawing.Color.Black
        Me.xTechniknote.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xTechniknote.Cursor = System.Windows.Forms.Cursors.Arrow
        resources.ApplyResources(Me.xTechniknote, "xTechniknote")
        Me.xTechniknote.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xTechniknote.Name = "xTechniknote"
        Me.xTechniknote.ReadOnly = True
        Me.xTechniknote.TabStop = False
        '
        'xGültig3
        '
        Me.xGültig3.BackColor = System.Drawing.Color.Black
        Me.xGültig3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig3.Cursor = System.Windows.Forms.Cursors.Arrow
        resources.ApplyResources(Me.xGültig3, "xGültig3")
        Me.xGültig3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig3.Name = "xGültig3"
        Me.xGültig3.ReadOnly = True
        Me.xGültig3.TabStop = False
        '
        'xGültig2
        '
        Me.xGültig2.BackColor = System.Drawing.Color.Black
        Me.xGültig2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig2.Cursor = System.Windows.Forms.Cursors.Arrow
        resources.ApplyResources(Me.xGültig2, "xGültig2")
        Me.xGültig2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig2.Name = "xGültig2"
        Me.xGültig2.ReadOnly = True
        Me.xGültig2.TabStop = False
        '
        'xAufruf
        '
        resources.ApplyResources(Me.xAufruf, "xAufruf")
        Me.xAufruf.BackColor = System.Drawing.Color.Black
        Me.xAufruf.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf.Name = "xAufruf"
        '
        'dgvHeber
        '
        Me.dgvHeber.AllowUserToAddRows = False
        Me.dgvHeber.AllowUserToDeleteRows = False
        Me.dgvHeber.AllowUserToResizeColumns = False
        Me.dgvHeber.AllowUserToResizeRows = False
        Me.dgvHeber.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader
        Me.dgvHeber.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle12
        resources.ApplyResources(Me.dgvHeber, "dgvHeber")
        Me.dgvHeber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHeber.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.RSG, Me.D1, Me.T1, Me.D2, Me.T2, Me.D3, Me.T3, Me.Wert})
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvHeber.DefaultCellStyle = DataGridViewCellStyle17
        Me.dgvHeber.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvHeber.EnableHeadersVisualStyles = False
        Me.dgvHeber.MultiSelect = False
        Me.dgvHeber.Name = "dgvHeber"
        Me.dgvHeber.ReadOnly = True
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.RowHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.dgvHeber.RowHeadersVisible = False
        Me.dgvHeber.RowTemplate.Height = 26
        Me.dgvHeber.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvHeber.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHeber.TabStop = False
        '
        'RSG
        '
        Me.RSG.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.LemonChiffon
        Me.RSG.DefaultCellStyle = DataGridViewCellStyle13
        resources.ApplyResources(Me.RSG, "RSG")
        Me.RSG.Name = "RSG"
        Me.RSG.ReadOnly = True
        Me.RSG.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RSG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'D1
        '
        resources.ApplyResources(Me.D1, "D1")
        Me.D1.Name = "D1"
        Me.D1.ReadOnly = True
        Me.D1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'T1
        '
        Me.T1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle14.Format = "N2"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.T1.DefaultCellStyle = DataGridViewCellStyle14
        Me.T1.DividerWidth = 1
        resources.ApplyResources(Me.T1, "T1")
        Me.T1.Name = "T1"
        Me.T1.ReadOnly = True
        Me.T1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.T1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'D2
        '
        resources.ApplyResources(Me.D2, "D2")
        Me.D2.Name = "D2"
        Me.D2.ReadOnly = True
        Me.D2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'T2
        '
        Me.T2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle15.Format = "N2"
        Me.T2.DefaultCellStyle = DataGridViewCellStyle15
        Me.T2.DividerWidth = 1
        resources.ApplyResources(Me.T2, "T2")
        Me.T2.Name = "T2"
        Me.T2.ReadOnly = True
        Me.T2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'D3
        '
        resources.ApplyResources(Me.D3, "D3")
        Me.D3.Name = "D3"
        Me.D3.ReadOnly = True
        Me.D3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'T3
        '
        Me.T3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle16.Format = "N2"
        Me.T3.DefaultCellStyle = DataGridViewCellStyle16
        Me.T3.DividerWidth = 1
        resources.ApplyResources(Me.T3, "T3")
        Me.T3.Name = "T3"
        Me.T3.ReadOnly = True
        Me.T3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Wert
        '
        resources.ApplyResources(Me.Wert, "Wert")
        Me.Wert.Name = "Wert"
        Me.Wert.ReadOnly = True
        Me.Wert.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgvGruppe
        '
        Me.dgvGruppe.AllowUserToAddRows = False
        Me.dgvGruppe.AllowUserToDeleteRows = False
        Me.dgvGruppe.AllowUserToResizeColumns = False
        Me.dgvGruppe.AllowUserToResizeRows = False
        Me.dgvGruppe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvGruppe.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.ControlLight
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle19.Padding = New System.Windows.Forms.Padding(2, 0, 2, 0)
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGruppe.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle19
        resources.ApplyResources(Me.dgvGruppe, "dgvGruppe")
        Me.dgvGruppe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvGruppe.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WG, Me.Gruppe, Me.Bezeichnung})
        Me.dgvGruppe.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvGruppe.EnableHeadersVisualStyles = False
        Me.dgvGruppe.Name = "dgvGruppe"
        Me.dgvGruppe.ReadOnly = True
        Me.dgvGruppe.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvGruppe.RowHeadersVisible = False
        Me.dgvGruppe.RowTemplate.Height = 20
        Me.dgvGruppe.RowTemplate.ReadOnly = True
        Me.dgvGruppe.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvGruppe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGruppe.TabStop = False
        '
        'WG
        '
        Me.WG.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.WG.DataPropertyName = "WG"
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.WG.DefaultCellStyle = DataGridViewCellStyle20
        Me.WG.FillWeight = 30.0!
        resources.ApplyResources(Me.WG, "WG")
        Me.WG.Name = "WG"
        Me.WG.ReadOnly = True
        Me.WG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Gruppe
        '
        Me.Gruppe.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gruppe.DataPropertyName = "Gruppe"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Gruppe.DefaultCellStyle = DataGridViewCellStyle21
        Me.Gruppe.FillWeight = 50.0!
        resources.ApplyResources(Me.Gruppe, "Gruppe")
        Me.Gruppe.Name = "Gruppe"
        Me.Gruppe.ReadOnly = True
        Me.Gruppe.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Gruppe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Bezeichnung
        '
        Me.Bezeichnung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Bezeichnung.DataPropertyName = "Bezeichnung"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.Bezeichnung.DefaultCellStyle = DataGridViewCellStyle22
        Me.Bezeichnung.FillWeight = 127.1574!
        resources.ApplyResources(Me.Bezeichnung, "Bezeichnung")
        Me.Bezeichnung.Name = "Bezeichnung"
        Me.Bezeichnung.ReadOnly = True
        Me.Bezeichnung.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgv2
        '
        Me.dgv2.AllowUserToAddRows = False
        Me.dgv2.AllowUserToDeleteRows = False
        Me.dgv2.AllowUserToResizeRows = False
        resources.ApplyResources(Me.dgv2, "dgv2")
        Me.dgv2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgv2.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.dgv2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.AppWorkspace
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle23.Padding = New System.Windows.Forms.Padding(0, 2, 0, 2)
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle23
        Me.dgv2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle24.NullValue = Nothing
        DataGridViewCellStyle24.Padding = New System.Windows.Forms.Padding(0, 2, 0, 2)
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv2.DefaultCellStyle = DataGridViewCellStyle24
        Me.dgv2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2
        Me.dgv2.EnableHeadersVisualStyles = False
        Me.dgv2.MultiSelect = False
        Me.dgv2.Name = "dgv2"
        Me.dgv2.ReadOnly = True
        Me.dgv2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Red
        DataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv2.RowHeadersDefaultCellStyle = DataGridViewCellStyle25
        Me.dgv2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle26.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle26.Padding = New System.Windows.Forms.Padding(8, 0, 8, 0)
        Me.dgv2.RowsDefaultCellStyle = DataGridViewCellStyle26
        Me.dgv2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv2.StandardTab = True
        Me.dgv2.Tag = "8,25"
        '
        'pnlSteigerung
        '
        Me.pnlSteigerung.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pnlSteigerung.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSteigerung.Controls.Add(Me.lblSteigernName)
        Me.pnlSteigerung.Controls.Add(Me.btnSteigernOK)
        Me.pnlSteigerung.Controls.Add(Me.nudHantellast)
        Me.pnlSteigerung.Controls.Add(Me.Label6)
        Me.pnlSteigerung.Controls.Add(Me.btnExitSteigerung)
        Me.pnlSteigerung.Controls.Add(Me.lblSteigerung)
        resources.ApplyResources(Me.pnlSteigerung, "pnlSteigerung")
        Me.pnlSteigerung.Name = "pnlSteigerung"
        '
        'lblSteigernName
        '
        resources.ApplyResources(Me.lblSteigernName, "lblSteigernName")
        Me.lblSteigernName.ForeColor = System.Drawing.Color.MediumBlue
        Me.lblSteigernName.Name = "lblSteigernName"
        Me.lblSteigernName.UseCompatibleTextRendering = True
        '
        'btnSteigernOK
        '
        resources.ApplyResources(Me.btnSteigernOK, "btnSteigernOK")
        Me.btnSteigernOK.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnSteigernOK.Name = "btnSteigernOK"
        Me.btnSteigernOK.UseCompatibleTextRendering = True
        Me.btnSteigernOK.UseVisualStyleBackColor = True
        '
        'nudHantellast
        '
        resources.ApplyResources(Me.nudHantellast, "nudHantellast")
        Me.nudHantellast.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nudHantellast.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudHantellast.Name = "nudHantellast"
        Me.nudHantellast.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        Me.Label6.UseCompatibleTextRendering = True
        '
        'btnExitSteigerung
        '
        resources.ApplyResources(Me.btnExitSteigerung, "btnExitSteigerung")
        Me.btnExitSteigerung.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnExitSteigerung.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnExitSteigerung.ForeColor = System.Drawing.Color.White
        Me.btnExitSteigerung.Name = "btnExitSteigerung"
        Me.btnExitSteigerung.TabStop = False
        Me.btnExitSteigerung.UseVisualStyleBackColor = False
        '
        'lblSteigerung
        '
        Me.lblSteigerung.BackColor = System.Drawing.Color.RoyalBlue
        Me.lblSteigerung.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSteigerung.Cursor = System.Windows.Forms.Cursors.Default
        resources.ApplyResources(Me.lblSteigerung, "lblSteigerung")
        Me.lblSteigerung.ForeColor = System.Drawing.Color.White
        Me.lblSteigerung.Name = "lblSteigerung"
        Me.lblSteigerung.UseCompatibleTextRendering = True
        '
        'btnPanik
        '
        Me.btnPanik.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        resources.ApplyResources(Me.btnPanik, "btnPanik")
        Me.btnPanik.FlatAppearance.BorderSize = 0
        Me.btnPanik.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red
        Me.btnPanik.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red
        Me.btnPanik.ForeColor = System.Drawing.Color.White
        Me.btnPanik.Name = "btnPanik"
        Me.ToolTip1.SetToolTip(Me.btnPanik, resources.GetString("btnPanik.ToolTip"))
        Me.btnPanik.UseCompatibleTextRendering = True
        Me.btnPanik.UseVisualStyleBackColor = False
        '
        'pnlFilter
        '
        Me.pnlFilter.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.pnlFilter.Controls.Add(Me.btnGruppe)
        Me.pnlFilter.Controls.Add(Me.lblGruppe)
        Me.pnlFilter.Controls.Add(Me.Label10)
        Me.pnlFilter.Controls.Add(Me.cboDurchgang)
        Me.pnlFilter.Controls.Add(Me.Label11)
        Me.pnlFilter.Controls.Add(Me.Label13)
        Me.pnlFilter.Controls.Add(Me.chkBH)
        resources.ApplyResources(Me.pnlFilter, "pnlFilter")
        Me.pnlFilter.Name = "pnlFilter"
        '
        'btnGruppe
        '
        resources.ApplyResources(Me.btnGruppe, "btnGruppe")
        Me.btnGruppe.ForeColor = System.Drawing.SystemColors.WindowText
        Me.btnGruppe.Name = "btnGruppe"
        Me.btnGruppe.UseVisualStyleBackColor = True
        '
        'lblGruppe
        '
        Me.lblGruppe.BackColor = System.Drawing.SystemColors.Window
        Me.lblGruppe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblGruppe.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        resources.ApplyResources(Me.lblGruppe, "lblGruppe")
        Me.lblGruppe.Name = "lblGruppe"
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.Window
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'bgwRechnen
        '
        Me.bgwRechnen.WorkerReportsProgress = True
        '
        'bgwServer
        '
        Me.bgwServer.WorkerSupportsCancellation = True
        '
        'bgwClient
        '
        Me.bgwClient.WorkerSupportsCancellation = True
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "SquareBlue.jpg")
        Me.ImageList2.Images.SetKeyName(1, "SquareRed.jpg")
        '
        'Timer1
        '
        Me.Timer1.Interval = 250
        '
        'dgvLeader
        '
        Me.dgvLeader.AllowUserToAddRows = False
        Me.dgvLeader.AllowUserToDeleteRows = False
        Me.dgvLeader.AllowUserToResizeRows = False
        resources.ApplyResources(Me.dgvLeader, "dgvLeader")
        Me.dgvLeader.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvLeader.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvLeader.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvLeader.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvLeader.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle27.Padding = New System.Windows.Forms.Padding(0, 2, 0, 2)
        DataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLeader.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle27
        Me.dgvLeader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle28.NullValue = Nothing
        DataGridViewCellStyle28.Padding = New System.Windows.Forms.Padding(0, 2, 0, 2)
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLeader.DefaultCellStyle = DataGridViewCellStyle28
        Me.dgvLeader.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2
        Me.dgvLeader.MultiSelect = False
        Me.dgvLeader.Name = "dgvLeader"
        Me.dgvLeader.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Red
        DataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLeader.RowHeadersDefaultCellStyle = DataGridViewCellStyle29
        Me.dgvLeader.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle30.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle30.Padding = New System.Windows.Forms.Padding(8, 0, 8, 0)
        Me.dgvLeader.RowsDefaultCellStyle = DataGridViewCellStyle30
        Me.dgvLeader.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLeader.StandardTab = True
        Me.dgvLeader.TabStop = False
        Me.dgvLeader.Tag = "8,25"
        '
        'frmLeitung
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        resources.ApplyResources(Me, "$this")
        Me.AutoValidate = System.Windows.Forms.AutoValidate.Disable
        Me.Controls.Add(Me.dgv2)
        Me.Controls.Add(Me.dgvHeber)
        Me.Controls.Add(Me.pnlManuell)
        Me.Controls.Add(Me.pnlSteigerung)
        Me.Controls.Add(Me.dgvGruppe)
        Me.Controls.Add(Me.pnlNotizen)
        Me.Controls.Add(Me.btnPanik)
        Me.Controls.Add(Me.pnlFilter)
        Me.Controls.Add(Me.pnlForm)
        Me.Controls.Add(Me.Status)
        Me.Controls.Add(Me.MenuStrip2)
        Me.Controls.Add(Me.dgvLeader)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip2
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeitung"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Status.ResumeLayout(False)
        Me.Status.PerformLayout()
        Me.MenuStrip2.ResumeLayout(False)
        Me.MenuStrip2.PerformLayout()
        Me.ContextMenuGrid.ResumeLayout(False)
        Me.pnlNotizen.ResumeLayout(False)
        Me.pnlNotizen.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.pnlManuell.ResumeLayout(False)
        Me.pnlT.ResumeLayout(False)
        Me.pnlT.PerformLayout()
        Me.pnlW1.ResumeLayout(False)
        Me.pnlW2.ResumeLayout(False)
        Me.pnlW3.ResumeLayout(False)
        Me.pnlForm.ResumeLayout(False)
        CType(Me.dgvTeam, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlWertung.ResumeLayout(False)
        Me.pnlWertung.PerformLayout()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvGruppe, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSteigerung.ResumeLayout(False)
        CType(Me.nudHantellast, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFilter.ResumeLayout(False)
        Me.pnlFilter.PerformLayout()
        CType(Me.dgvLeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAufruf As System.Windows.Forms.Button
    Friend WithEvents Status As System.Windows.Forms.StatusStrip
    Friend WithEvents statusMsg As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents MenuStrip2 As System.Windows.Forms.MenuStrip
    Friend WithEvents btnSteigerung As System.Windows.Forms.Button
    Friend WithEvents mnuOptionen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTableLayout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnBestätigung As System.Windows.Forms.Button
    Friend WithEvents pnlNotizen As System.Windows.Forms.Panel
    Friend WithEvents lblNotizen As System.Windows.Forms.Label
    Friend WithEvents btnVerzicht As System.Windows.Forms.Button
    Friend WithEvents chkBH As System.Windows.Forms.CheckBox
    Friend WithEvents mnuMeldungen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPause As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuKonflikte As System.Windows.Forms.ToolStripMenuItem
    'Friend WithEvents bs As BindingSource
    Friend WithEvents mnuDatei As ToolStripMenuItem
    Friend WithEvents mnuBeenden As ToolStripMenuItem
    Friend WithEvents btnKorrektur As Button
    Friend WithEvents btnNotizExit As Button
    Friend WithEvents mnuHeber As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents ContextMenuGrid As ContextMenuStrip
    Friend WithEvents cmnuHeber As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripSeparator
    Friend WithEvents mnuSteigerung As ToolStripMenuItem
    Friend WithEvents mnuVerzicht As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As ToolStripSeparator
    Friend WithEvents mnuKorrektur As ToolStripMenuItem
    Friend WithEvents Label13 As Label
    Friend WithEvents cboDurchgang As ComboBox
    Friend WithEvents Label11 As Label
    Friend WithEvents rtfNotiz As RichTextBox
    Friend WithEvents ColorDialog1 As ColorDialog
    Friend WithEvents btnNotizForeColor As Button
    Friend WithEvents chkNotizBold As CheckBox
    Friend WithEvents chkNotizUnderline As CheckBox
    Friend WithEvents chkNotizItalic As CheckBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents mnuWertung As ToolStripMenuItem
    Friend WithEvents mnuBestätigung As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As ToolStripSeparator
    Friend WithEvents pnlManuell As Panel
    Friend WithEvents lblManuell As Label
    Friend WithEvents txtT1 As TextBox
    Friend WithEvents Dummy As Button
    Friend WithEvents txtT3 As TextBox
    Friend WithEvents txtT2 As TextBox
    Friend WithEvents mnuTechniknote As ToolStripMenuItem
    Friend WithEvents ImageList64 As ImageList
    Friend WithEvents pnlW1 As Panel
    Friend WithEvents btnU1 As RadioButton
    Friend WithEvents btnG1 As RadioButton
    Friend WithEvents pnlW3 As Panel
    Friend WithEvents btnU3 As RadioButton
    Friend WithEvents btnG3 As RadioButton
    Friend WithEvents pnlW2 As Panel
    Friend WithEvents btnU2 As RadioButton
    Friend WithEvents btnG2 As RadioButton
    Friend WithEvents pnlT As Panel
    Friend WithEvents btnSave_Notiz As Button
    Friend WithEvents btnDelete_Notiz As Button
    Friend WithEvents pnlForm As Panel
    Friend WithEvents pnlWertung As Panel
    Friend WithEvents xTechniknote As TextBox
    Friend WithEvents xGültig3 As TextBox
    Friend WithEvents xGültig2 As TextBox
    Friend WithEvents xGültig1 As TextBox
    Friend WithEvents statusSteigerung As ToolStripStatusLabel
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents statusVersuch As ToolStripStatusLabel
    Friend WithEvents statusResultat As ToolStripStatusLabel
    Friend WithEvents ToolStripMenuItem7 As ToolStripSeparator
    Friend WithEvents mnuConfirm As ToolStripMenuItem
    Friend WithEvents mnuKR_Init As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem8 As ToolStripSeparator
    Friend WithEvents mnuAktuellenVersuch As ToolStripMenuItem
    Friend WithEvents pnlSteigerung As Panel
    Friend WithEvents lblSteigernName As Label
    Friend WithEvents btnExitSteigerung As Button
    Friend WithEvents lblSteigerung As Label
    Friend WithEvents btnSteigernOK As Button
    Friend WithEvents nudHantellast As NumericUpDown
    Friend WithEvents Label6 As Label
    Friend WithEvents mnuHighLightAttempts As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents mnuPanik As ToolStripMenuItem
    Friend WithEvents mnuSplit_AG As ToolStripMenuItem
    Friend WithEvents btnPanik As Button
    Friend WithEvents btnFertig As Button
    Friend WithEvents btnExitManuell As Button
    Friend WithEvents mnuSplitItem1 As ToolStripSeparator
    Friend WithEvents pnlFilter As Panel
    Friend WithEvents dgv2 As DataGridView
    Friend WithEvents btnGruppe As Button
    Friend WithEvents dgvGruppe As DataGridView
    Friend WithEvents Label10 As Label
    Friend WithEvents lblGruppe As Label
    Friend WithEvents mnuEingabePrüfen As ToolStripMenuItem
    Friend WithEvents mnuMauszeiger As ToolStripMenuItem
    Friend WithEvents cmnuVerein As ToolStripMenuItem
    Friend WithEvents mnuVerein As ToolStripMenuItem
    Friend WithEvents mnuTCP As ToolStripMenuItem
    Friend WithEvents ComMsg As ToolStripStatusLabel
    Friend WithEvents bgwResult As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwPunkte As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwSteigerung As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwVersuch As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwRechnen As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwServer As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwClient As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblID As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents WG As DataGridViewTextBoxColumn
    Friend WithEvents Gruppe As DataGridViewTextBoxColumn
    Friend WithEvents Bezeichnung As DataGridViewTextBoxColumn
    Friend WithEvents mnuPrognose As ToolStripMenuItem
    Friend WithEvents sep5 As ToolStripSeparator
    Friend WithEvents dgvHeber As DataGridView
    Friend WithEvents mnuResultsLifter As ToolStripMenuItem
    Friend WithEvents ImageList2 As ImageList
    Friend WithEvents dgvTeam As DataGridView
    Friend WithEvents RSG As DataGridViewTextBoxColumn
    Friend WithEvents D1 As DataGridViewTextBoxColumn
    Friend WithEvents T1 As DataGridViewTextBoxColumn
    Friend WithEvents D2 As DataGridViewTextBoxColumn
    Friend WithEvents T2 As DataGridViewTextBoxColumn
    Friend WithEvents D3 As DataGridViewTextBoxColumn
    Friend WithEvents T3 As DataGridViewTextBoxColumn
    Friend WithEvents Wert As DataGridViewTextBoxColumn
    Friend WithEvents mnuZNPadSperrzeit As ToolStripMenuItem
    Friend WithEvents btnUndo_Notiz As Button
    Friend WithEvents mnuHeberWechsel As ToolStripMenuItem
    Friend WithEvents xAufruf As Label
    Friend WithEvents Progress1 As ToolStripProgressBar
    Friend WithEvents Rolle As DataGridViewTextBoxColumn
    Friend WithEvents Team As DataGridViewTextBoxColumn
    Friend WithEvents Gegner As DataGridViewTextBoxColumn
    Friend WithEvents mReissen As DataGridViewTextBoxColumn
    Friend WithEvents mStossen As DataGridViewTextBoxColumn
    Friend WithEvents mHeben As DataGridViewTextBoxColumn
    Friend WithEvents mPunkte As DataGridViewTextBoxColumn
    Friend WithEvents Punkte2 As DataGridViewTextBoxColumn
    Friend WithEvents Platz As DataGridViewTextBoxColumn
    Friend WithEvents Timer1 As Timer
    Friend WithEvents mnuHeberAuswechseln As ToolStripMenuItem
    Friend WithEvents mnuBlaueTaste As ToolStripMenuItem
    Friend WithEvents mnuClearHeber As ToolStripMenuItem
    Friend WithEvents mnuExtras As ToolStripMenuItem
    Friend WithEvents btnMinute2 As Button
    Friend WithEvents btnMinute1 As Button
    Friend WithEvents dgvLeader As DataGridView
    Friend WithEvents mnuHupe As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents mnuClearHupe As ToolStripMenuItem
End Class
