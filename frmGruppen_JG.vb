﻿
Imports System.ComponentModel
Imports System.Math

Public Class frmGruppen_JG
    Dim Bereich As String = "Gruppen_JG"
    Dim Query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim func As New myFunction

    Dim da As MySqlDataAdapter
    Dim ds As New DataSet
    'Dim dtGruppen As New DataTable

    Dim dtMeldung As New DataTable
    Dim dtWK_Gruppen As New DataTable
    Dim dtModus_GG As New DataTable

    Dim dv As DataView

    'Dim AK_w As Integer = Wettkampf.AKs("w")
    'Dim AK_m As Integer = Wettkampf.AKs("m")
    Dim dicJahrgang As New Dictionary(Of String, List(Of String))
    'Dim dvGruppe(5) As DataView
    Dim bs(5) As BindingSource
    Dim loading As Boolean
    Dim draging As Boolean

    Private _datachanged As Boolean
    Private Property DataChanged As Boolean ' Daten-Änderung in DataTables
        Get
            Return _datachanged
        End Get
        Set(value As Boolean)
            _datachanged = value
            If value = True Then mnuSpeichern.Enabled = True
        End Set
    End Property

    Dim nonNumberEntered As Boolean = False

    Dim tblGruppe As New Dictionary(Of Integer, DataTable)

    Dim pnlGruppe As New Dictionary(Of Integer, Panel)
    Dim dgvGruppe As New Dictionary(Of Integer, DataGridView)
    Dim lblGruppe As New Dictionary(Of Integer, Label)
    Dim cboGruppe As New Dictionary(Of Integer, ComboBox)
    Dim chkBH As New Dictionary(Of Integer, CheckBox)
    Dim pkDate As New Dictionary(Of Integer, DateTimePicker)
    Dim pkDateA As New Dictionary(Of Integer, DateTimePicker)
    Dim pkDateW As New Dictionary(Of Integer, DateTimePicker)
    Dim pkTimeR As New Dictionary(Of Integer, DateTimePicker)
    Dim pkTimeS As New Dictionary(Of Integer, DateTimePicker)
    Dim pkTimeA As New Dictionary(Of Integer, DateTimePicker)
    Dim pkTimeWB As New Dictionary(Of Integer, DateTimePicker)
    Dim pkTimeWE As New Dictionary(Of Integer, DateTimePicker)
    Dim cboBohle As New Dictionary(Of Integer, ComboBox)
    Dim picGruppe As New Dictionary(Of Integer, PictureBox)


    'Dim GruppenIndex As New Dictionary(Of String, Integer)
    Dim SourceTable As Integer
    Dim TargetTable As Integer
    Dim dgvSelection As New List(Of Integer) ' die zu verschiebenden Rows
    Dim OldValue(5) As Integer

    Dim GruppenBezeichnung As New Dictionary(Of String, List(Of String))
    Dim dicMaxGruppen As New Dictionary(Of String, Integer)
    Dim dicGruppenTeiler As New Dictionary(Of String, List(Of Integer))
    Dim sx() As String = {"männlich", "weiblich"}

    Private _dw As Date
    Private _wb As Date
    Private _we As Date
    Private Property DatumWiegen As Date
        Get
            Return _dw
        End Get
        Set(value As Date)
            _dw = value
            If chkWiegenGleich.Checked Then
                For i = 1 To 5
                    pkDateW(i).Value = value
                Next
            End If
        End Set
    End Property
    Private Property WiegeBeginn As Date
        Get
            Return _wb
        End Get
        Set(value As Date)
            _wb = value
            If chkWiegenGleich.Checked Then
                For i = 1 To 5
                    pkTimeWB(i).Value = value
                Next
            End If
        End Set
    End Property
    Private Property WiegeEnde As Date
        Get
            Return _we
        End Get
        Set(value As Date)
            _we = value
            If chkWiegenGleich.Checked Then
                For i = 1 To 5
                    pkTimeWE(i).Value = value
                Next
            End If
        End Set
    End Property

    'Optionen
    Dim AlternatingRowsDefaultCellStyle_BackColor As Color = Color.OldLace
    Dim RowsDefaultCellStyle_BackColor As Color = Color.LightCyan
    Dim Zeit_pro_Hebung As Integer = 6
    Dim Zeit_Rundung As Integer = 5

    Private Sub frmGruppen_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If mnuSpeichern.Enabled Then
            Using New Centered_MessageBox(Me)
                Select Case MessageBox.Show("Änderung an der Gruppen-Einteilung speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                    Case DialogResult.Yes
                        Save()
                    Case DialogResult.Cancel
                        e.Cancel = True
                End Select
            End Using
        End If
        NativeMethods.INI_Write(Bereich, "Konflikte", CStr(mnuKonflikte.Checked), INI_File)
        NativeMethods.INI_Write(Bereich, "ShowMessages", CStr(mnuMeldungen.Checked), INI_File)
        NativeMethods.INI_Write(Bereich, "KeepOpen", CStr(mnuJGListeOffen.Checked), INI_File)
        NativeMethods.INI_Write(Bereich, "KeepGroups", CStr(mnuZusammenhalten.Checked), INI_File)
        NativeMethods.INI_Write(Bereich, "CutGroups", CStr(mnuCutGroups.Checked), INI_File)
    End Sub
    Private Sub frmGruppen_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        dgvGruppe.Add(1, dgvGK1)
        dgvGruppe.Add(2, dgvGK2)
        dgvGruppe.Add(3, dgvGK3)
        dgvGruppe.Add(4, dgvGK4)
        dgvGruppe.Add(5, dgvGK5)
        lblGruppe.Add(1, lblG1)
        lblGruppe.Add(2, lblG2)
        lblGruppe.Add(3, lblG3)
        lblGruppe.Add(4, lblG4)
        lblGruppe.Add(5, lblG5)
        cboGruppe.Add(1, cbo1)
        cboGruppe.Add(2, cbo2)
        cboGruppe.Add(3, cbo3)
        cboGruppe.Add(4, cbo4)
        cboGruppe.Add(5, cbo5)
        chkBH.Add(1, chkBH1)
        chkBH.Add(2, chkBH2)
        chkBH.Add(3, chkBH3)
        chkBH.Add(4, chkBH4)
        chkBH.Add(5, chkBH5)
        pkDate.Add(1, date1)
        pkDate.Add(2, date2)
        pkDate.Add(3, date3)
        pkDate.Add(4, date4)
        pkDate.Add(5, date5)
        pkDateA.Add(1, dateA1)
        pkDateA.Add(2, dateA2)
        pkDateA.Add(3, dateA3)
        pkDateA.Add(4, dateA4)
        pkDateA.Add(5, dateA5)
        pkDateW.Add(1, dateW1)
        pkDateW.Add(2, dateW2)
        pkDateW.Add(3, dateW3)
        pkDateW.Add(4, dateW4)
        pkDateW.Add(5, dateW5)
        pkTimeR.Add(1, timeR1)
        pkTimeR.Add(2, timeR2)
        pkTimeR.Add(3, timeR3)
        pkTimeR.Add(4, timeR4)
        pkTimeR.Add(5, timeR5)
        pkTimeS.Add(1, timeS1)
        pkTimeS.Add(2, timeS2)
        pkTimeS.Add(3, timeS3)
        pkTimeS.Add(4, timeS4)
        pkTimeS.Add(5, timeS5)
        pkTimeA.Add(1, timeA1)
        pkTimeA.Add(2, timeA2)
        pkTimeA.Add(3, timeA3)
        pkTimeA.Add(4, timeA4)
        pkTimeA.Add(5, timeA5)
        pkTimeWB.Add(1, timeWb1)
        pkTimeWB.Add(2, timeWb2)
        pkTimeWB.Add(3, timeWb3)
        pkTimeWB.Add(4, timeWb4)
        pkTimeWB.Add(5, timeWb5)
        pkTimeWE.Add(1, timeWe1)
        pkTimeWE.Add(2, timeWe2)
        pkTimeWE.Add(3, timeWe3)
        pkTimeWE.Add(4, timeWe4)
        pkTimeWE.Add(5, timeWe5)
        cboBohle.Add(1, cboB1)
        cboBohle.Add(2, cboB2)
        cboBohle.Add(3, cboB3)
        cboBohle.Add(4, cboB4)
        cboBohle.Add(5, cboB5)
        picGruppe.Add(1, picGruppe1)
        picGruppe.Add(2, picGruppe2)
        picGruppe.Add(3, picGruppe3)
        picGruppe.Add(4, picGruppe4)
        picGruppe.Add(5, picGruppe5)
        pnlGruppe.Add(1, pnlGruppe1)
        pnlGruppe.Add(2, pnlGruppe2)
        pnlGruppe.Add(3, pnlGruppe3)
        pnlGruppe.Add(4, pnlGruppe4)
        pnlGruppe.Add(5, pnlGruppe5)
    End Sub
    Private Sub frmGruppen_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        loading = True
        Dim x As String
        x = NativeMethods.INI_Read(Bereich, "Konflikte", INI_File)
        If x <> "?" Then mnuKonflikte.Checked = CBool(x)
        x = NativeMethods.INI_Read(Bereich, "ShowMessages", INI_File)
        If x <> "?" Then mnuMeldungen.Checked = CBool(x)
        x = NativeMethods.INI_Read(Bereich, "KeepOpen", INI_File)
        If x <> "?" Then mnuJGListeOffen.Checked = CBool(x)
        x = NativeMethods.INI_Read(Bereich, "KeepGroups", INI_File)
        If x <> "?" Then
            mnuZusammenhalten.Checked = CBool(x)
            mnuAufteilen.Checked = Not CBool(x)
        End If
        x = NativeMethods.INI_Read(Bereich, "CutGroups", INI_File)
        If x <> "?" Then mnuCutGroups.Checked = CBool(x)

        Using conn As New MySqlConnection(MySqlConnString)
            Try
                conn.Open()
                ' Datenquelle für lstJahrgang
                Query = "Select DISTINCT a.Jahrgang FROM meldung m, athleten a " &
                        "WHERE m.Wettkampf = " & Wettkampf.ID & " And m.Teilnehmer = a.idTeilnehmer AND (m.attend = True OR (IsNull(m.attend) AND IsNull(m.Wiegen))) " &
                        "ORDER BY a.Jahrgang DESC;"
                cmd = New MySqlCommand(Query, conn)
                reader = cmd.ExecuteReader()
                While reader.Read
                    lstJahrgang.Items.Add(Space(2) & reader(0).ToString)
                End While
                reader.Close()
                'Datenquelle für cboGruppe
                Query = "Select * FROM gruppen WHERE Wettkampf = " & Wettkampf.ID & " ORDER BY Gruppe ASC;"
                'cmd = New MySqlCommand(Query, conn)
                'ds.tables("dtGruppen").Load(cmd.ExecuteReader)
                da = New MySqlDataAdapter(Query, conn)
                da.Fill(ds, "dtGruppen")
                'Datenquelle für Wettkampf_Gruppen
                Query = "Select * FROM wettkampf_gruppen WHERE Wettkampf = " & Wettkampf.ID & " ORDER BY Geschlecht, Gruppe;"
                cmd = New MySqlCommand(Query, conn)
                dtWK_Gruppen.Load(cmd.ExecuteReader)
                'Datenquelle für Meldung
                Query = "SELECT m.Teilnehmer, m.Gruppe, a.Nachname, a.Vorname, v.Vereinsname, a.Geschlecht, IfNull(m.Wiegen, m.Gewicht) KG, a.Jahrgang, CONCAT(Altersklasse,'_',LEFT(Geschlecht,1)) AK, m.GK " &
                "FROM meldung m " &
                "LEFT JOIN athleten a On m.Teilnehmer = a.idTeilnehmer " &
                "LEFT JOIN wettkampf w ON m.Wettkampf = w.idWettkampf " &
                "LEFT JOIN altersklassen ON YEAR(w.Datum)-a.Jahrgang BETWEEN von AND bis " &
                "LEFT JOIN verein v ON a.Verein = v.idVerein " &
                "WHERE m.Wettkampf = " & Wettkampf.ID & " AND (m.attend = True OR (IsNull(m.attend) AND IsNull(m.Wiegen))) " &
                "ORDER BY m.Gruppe, IfNull(m.Wiegen, m.Gewicht), a.Nachname, a.Vorname;"
                cmd = New MySqlCommand(Query, conn)
                dtMeldung.Load(cmd.ExecuteReader())
                ' modus_gewichtsgruppen (Source für Gruppenbezeichnung & MaxGruppen
                'Query = "SELECT * FROM modus_gewichtsgruppen WHERE Modus = " & Wettkampf.Modus & " ORDER BY Geschlecht, Gruppe;"
                Query = "SELECT * FROM modus_gewichtsgruppen ORDER BY Geschlecht, Gruppe;"
                cmd = New MySqlCommand(Query, conn)
                dtModus_GG.Load(cmd.ExecuteReader())
            Catch ex As MySqlException
                Stop
            End Try
        End Using

        '' Primärschlüssel
        'Dim col(1) As DataColumn
        'col(0) = dtMeldung.Columns("Teilnehmer")
        'dtMeldung.PrimaryKey = col
        'col(0) = ds.Tables("dtGruppen").Columns("Gruppe")
        'ds.Tables("dtGruppen").PrimaryKey = col

        '' gespeicherte Gruppen in cboGruppenSaved eintragen
        'Dim m = New DataView(dtMeldung).ToTable(True, {"Gruppe"})
        'For Each r As DataRow In m.Rows
        '    If Not IsDBNull(r!Gruppe) Then cboGruppenSaved.Items.Add(r!Gruppe)
        'Next

        Dim GroupsCount As Integer = 0 ' errechnete Gesamtanzahl der Gruppen
        For i = 0 To 1
            Dim sex = Strings.Left(sx(i), 1)
            ' Listen für lstJahrgang
            dv = New DataView(dtMeldung.DefaultView.ToTable(True, {"Geschlecht", "Jahrgang"}), "Geschlecht = '" & sx(i) & "'", "Jahrgang DESC", DataViewRowState.CurrentRows)
            Dim slst As New List(Of String)
            For Each r As DataRowView In dv
                slst.Add(" AK " & Year(CDate(Glob.dtWK(0)("Datum"))) - CInt(r!Jahrgang) & "  -  " & r!Jahrgang.ToString)
            Next
            dicJahrgang.Add(sx(i), slst)
            ' Gruppenbezeichnung
            Dim dvG As DataView = New DataView(dtModus_GG)
            dvG.RowFilter = "Geschlecht = '" & sex & "'"
            slst = New List(Of String)
            For Each r As DataRowView In dvG
                slst.Add(r!Bezeichnung.ToString)
            Next
            GruppenBezeichnung.Add(sx(i), slst)
            ' maxGruppen
            dv.RowFilter = "Geschlecht = '" & sex & "'"
            If dv.Count = 0 Then
                ' keine Gruppeneinteilung im WK
                dv = New DataView(dtModus_GG)
                dv.RowFilter = "Geschlecht = '" & sex & "'"
            End If
            dicMaxGruppen.Add(sx(i), dv.Count)
            If dv.Count = 1 AndAlso CInt(dv(0)("Gruppe")) = 0 Then dicMaxGruppen(sx(i)) = WK_Modus.MaxGruppen  ' alle Gruppen gleich groß
            ' GruppenTeiler
            dv = New DataView(dtWK_Gruppen)
            dv.RowFilter = "Geschlecht = '" & sex & "'"
            Dim ilst As New List(Of Integer)
            For Each r As DataRowView In dv
                If dv.Count = 1 AndAlso CInt(r!Gruppe) = 0 Then
                    ' alle Gruppen gleich groß
                    For c = 1 To dicMaxGruppen(sx(i))
                        ilst.Add(CInt(r!Heber) * c)
                    Next
                Else
                    ilst.Add(CInt(r!Heber))
                End If
            Next
            dicGruppenTeiler.Add(sx(i), ilst)
            GroupsCount += Get_GroupsCount(sx(i))
        Next

        ' errechnete Gesamtanzahl der Gruppen in cboGruppe eintragen
        For i = 1 To GroupsCount
            cboGruppe(1).Items.Add(i)
        Next
        Dim range(cboGruppe(1).Items.Count - 1) As Object
        cboGruppe(1).Items.CopyTo(range, 0)
        For i = 2 To 5
            cboGruppe(i).Items.AddRange(range)
        Next

        ' Gewichtsklassen einlesen
        'bgwGK.RunWorkerAsync()

        ' Grids erstellen
        For i As Integer = 1 To 5
            tblGruppe.Add(i, dtMeldung.Clone)
            dgvGruppen_Build(dgvGruppe(i))
        Next

        cboGeschlecht.SelectedIndex = 0

        loading = False
        mnuGruppenLöschen.Enabled = ds.Tables("dtGruppen").Rows.Count > 0
        mnuSpeichern.Enabled = False
        Cursor = Cursors.Default
    End Sub

    Private Sub bgwGK_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwGK.DoWork
        'bei Einteilung vor dem Wiegen GK ermitteln
        'Dim found() As DataRow
        For Each dr As DataRow In dtMeldung.Rows
            If IsDBNull(dr!GK) Then
                'found = Glob.dtGK.Select(dr!AK.ToString.Replace("ü", "ue") + " > " + dr!Gewicht.ToString.Replace(",", "."), "id ASC")
                'If found.Count = 0 Then
                '    '+Klasse
                '    found = Glob.dtGK.Select(dr!AK.ToString.Replace("ü", "ue") + " > 0", "id DESC")
                '    dr!GK = "+" + found(0)(dr!AK.ToString.Replace("ü", "ue")).ToString
                'Else
                '    '-Klasse
                '    dr!GK = "-" + found(0)(dr!AK.ToString.Replace("ü", "ue")).ToString
                'End If
                Try
                    dr!GK = func.Get_GK(dr!AK.ToString, Strings.Left(dr!Geschlecht.ToString, 1), CDbl(If(IsDBNull(dr!Wiegen), dr!Gewicht, dr!Wiegen)))
                Catch ex As Exception
                    Stop
                End Try
            End If
        Next
    End Sub

    Private Sub mnuBeenden_Click(sender As Object, e As EventArgs) Handles mnuBeenden.Click, btnClose.Click
        Close()
    End Sub
    Private Sub mnuDrucken_Click(sender As Object, e As EventArgs) Handles mnuDrucken.Click
        Dim WK_Datum As String = String.Empty
        Dim WK_Datum_E As String = String.Empty
        Dim WK_Ort As String = String.Empty
        Dim WK_Beginn As String = String.Empty
        Dim Wiegen_B As String = String.Empty
        Dim Wiegen_E As String = String.Empty
        Dim R_Datum As String = String.Empty
        Dim G_Bez As String = String.Empty

        Using conn As New MySqlConnection(MySqlConnString)
            Try
                conn.Open()
                Query = "select Ort, Datum, DatumBis from wettkampf where idWettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                reader = (cmd.ExecuteReader())
                reader.Read()
                WK_Ort = reader(0).ToString
                WK_Datum = Format(reader(1), "dd.MM.yyyy")
                If Not IsDBNull(reader(2)) Then WK_Datum_E = Format(reader(2), "dd.MM.yyyy")
                reader.Close()
                Query = "select Bezeichnung, Datum, WiegenBeginn, WiegenEnde, Reissen from gruppen where Wettkampf = " & Wettkampf.ID & " AND Gruppe = " & cboGruppenSaved.Text & ";"
                cmd = New MySqlCommand(Query, conn)
                reader = (cmd.ExecuteReader())
                reader.Read()
                G_Bez = reader(0).ToString
                If Not IsDBNull(reader(1)) Then R_Datum = Format(reader(1), "dd.MM.yyyy")
                If Not IsDBNull(reader(2)) Then Wiegen_B = Strings.Left(Format("hh:mm", reader(2).ToString), 5)
                If Not IsDBNull(reader(3)) Then Wiegen_E = Strings.Left(Format("hh:mm", reader(3).ToString), 5)
                If Not IsDBNull(reader(4)) Then WK_Beginn = Strings.Left(Format("hh:mm", reader(4).ToString), 5)
            Catch ex As Exception
                If cboGruppenSaved.Text = "" Then
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Keine Gruppe für den Druck ausgewählt.", "Wiegeliste drucken", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Using
                End If
                Exit Sub
            Finally
                reader.Close()
            End Try
        End Using

        Using report As New FastReport.Report
            Try
                With report
                    .Load(Path.Combine(Application.StartupPath, "Report", "Gruppenliste.frx"))
                    .RegisterData(tblGruppe(1), "Wiegeliste")
                    '.SetParameterValue("pAK_w", AK_w)
                    '.SetParameterValue("pAK_m", AK_m)
                    .SetParameterValue("pWK", Wettkampf.ID)
                    .SetParameterValue("pWK_Name", Wettkampf.Bezeichnung)
                    .SetParameterValue("pWK_Ort", WK_Ort)
                    .SetParameterValue("pWK_Datum", If(String.IsNullOrEmpty(WK_Datum_E), WK_Datum, Strings.Left(WK_Datum, 5)))
                    .SetParameterValue("pWK_Datum_E", If(String.IsNullOrEmpty(WK_Datum_E), "", "- " + WK_Datum_E + " "))
                    .SetParameterValue("pGruppe", cboGruppenSaved.Text)
                    .SetParameterValue("pWK_Beginn", WK_Beginn)
                    .SetParameterValue("pWiegen_B", Wiegen_B)
                    .SetParameterValue("pWiegen_E", Wiegen_E)
                    .SetParameterValue("pR_Datum", R_Datum)
                    .SetParameterValue("pG_Bez", G_Bez)
                    .Show()
                End With
            Catch ex As Exception
                Stop
                'Using New Centered_MessageBox(Me)
                '    MessageBox.Show("Keine Kriterien für den Druck ausgewählt.", "Wiegeliste drucken", MessageBoxButtons.OK, MessageBoxIcon.Information)
                'End Using
                'Stop
            End Try
        End Using
    End Sub
    Private Sub mnuGruppenLöschen_Click(sender As Object, e As EventArgs) Handles mnuGruppenLöschen.Click
        Cursor = Cursors.WaitCursor
        ' alle Gruppen auf NULL setzen
        For Each row As DataRow In dtMeldung.Rows
            row!Gruppe = DBNull.Value
        Next
        For i = 1 To 5
            ' alle Zeilen in angezeigten Grids auf Default-ForeColor setzen (falls nicht zugewiesene Gruppen rot sind)
            If dgvGruppe(i).Rows.Count > 0 Then
                For Each row As DataGridViewRow In dgvGruppe(i).Rows
                    row.DefaultCellStyle.ForeColor = dgvGruppe(i).ForeColor
                Next
            End If
            cboGruppe(i).SelectedIndex = -1
            OldValue(i) = 0
        Next
        cboGruppenSaved.Items.Clear()
        Dim msg = "Sollen die gespeicherten Gruppen "
        If ds.Tables("dtGruppen").Rows.Count > 0 Then msg += "und vorhandenen Daten "
        msg += "auch gelöscht werden?"
        Using New Centered_MessageBox(Me)
            If MessageBox.Show(msg, msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                ds.Tables("dtGruppen").Clear()
                cboGruppenSaved.Items.Clear()
            End If
        End Using
        mnuGruppenLöschen.Enabled = False
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuSpeichern_Click(sender As Object, e As EventArgs) Handles mnuSpeichern.Click, btnSave.Click
        Save()
    End Sub
    Private Sub mnuSpeichern_EnabledChanged(sender As Object, e As EventArgs) Handles mnuSpeichern.EnabledChanged
        btnSave.Enabled = mnuSpeichern.Enabled
    End Sub
    Private Sub mnuZusammenhalten_Click(sender As Object, e As EventArgs) Handles mnuZusammenhalten.Click
        mnuZusammenhalten.Checked = Not mnuZusammenhalten.Checked
        mnuAufteilen.Checked = Not mnuZusammenhalten.Checked
        'Filter_Changed()
    End Sub
    Private Sub mnuAufteilen_Click(sender As Object, e As EventArgs) Handles mnuAufteilen.Click
        mnuAufteilen.Checked = Not mnuAufteilen.Checked
        mnuZusammenhalten.Checked = Not mnuAufteilen.Checked
        'Filter_Changed()
    End Sub

    Private Function Save() As Boolean
        ' alle Änderungen in dtMeldung und ds.tables("dtGruppen") speichern
        If Data_Update() = False Then Return False


        'Dim msg As String = String.Empty

        'dv = New DataView(dtMeldung, Nothing, "Gruppe ASC", DataViewRowState.CurrentRows)
        'Dim dt As DataTable = dv.ToTable(True, "Gruppe")
        'For i As Integer = 1 To dt.Rows.Count
        '    If IsDBNull(dt.Rows(i - 1)("Gruppe")) Then
        '        msg = "Keine Gruppe für [" & lblGruppe(i).Text & "] ausgewählt."
        '        Using New Centered_MessageBox(Me) ', {"Abbrechen"})
        '            'If mnuKonflikte.Checked Then
        '            MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            Return False
        '            'End If
        '        End Using
        '    ElseIf i <> CInt(dt.Rows(i - 1)("Gruppe")) Then
        '        msg = "Die Gruppen müssen in aufsteigender Reihenfolge zugewiesen werden." & vbNewLine & "Fehlerhafte Reihenfolge vor Gruppe " & i & "."
        '        Using New Centered_MessageBox(Me, {"Weiter"})
        '            If mnuKonflikte.Checked Then
        '                If MessageBox.Show(msg, msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then Return False
        '            End If
        '        End Using
        '    End If
        '    If Not String.IsNullOrEmpty(msg) Then
        '    End If
        'Next

        Cursor = Cursors.WaitCursor
        Validate()
        conn = New MySqlConnection(MySqlConnString)
        BindingContext(ds.Tables("dtGruppen")).EndCurrentEdit()
        Dim changes = ds.Tables("dtGruppen").GetChanges()
        If Not IsNothing(changes) Then
            Dim cb = New MySqlCommandBuilder(da)
            Do
                Try
                    da.Update(changes)
                    ConnError = False
                Catch ex As MySqlException
                    If MySQl_Error(Me, ex.Message) = DialogResult.Cancel Then Exit Do
                End Try
            Loop While ConnError
            If Not ConnError Then
                ds.Tables("dtGruppen").AcceptChanges()

                changes = dtMeldung.GetChanges()
                If Not IsNothing(changes) Then
                    Using conn
                        conn.Open()
                        For Each row As DataRow In changes.Rows
                            Query = "UPDATE meldung SET Gruppe = @group WHERE Wettkampf = @wk AND Teilnehmer = @tn;"
                            cmd = New MySqlCommand(Query, conn)
                            With cmd.Parameters
                                .AddWithValue("@wk", Wettkampf.ID)
                                .AddWithValue("@tn", row!Teilnehmer)
                                .AddWithValue("@group", row!Gruppe)
                            End With
                            Do
                                Try
                                    cmd.ExecuteNonQuery()
                                    ConnError = False
                                Catch ex As MySqlException
                                    If MySQl_Error(Me, ex.Message) = DialogResult.Cancel Then Exit Do
                                End Try
                            Loop While ConnError
                            If ConnError Then Exit For
                        Next
                        If Not ConnError Then
                            dtMeldung.AcceptChanges()
                        End If
                    End Using
                End If
            End If
        End If

        Cursor = Cursors.Default
        If Not ConnError Then
            mnuSpeichern.Enabled = False
            DataChanged = False
            cboGruppenSaved.Enabled = True
            Return True
        End If
        Return False
    End Function
    Private Function Get_JG_String(ByVal JG As String) As String ' formatiert Jahrgänge als Bedingung
        Dim s As String() = Split(JG, ", ")
        If s.Count = 1 AndAlso s(0) = String.Empty Then Return String.Empty
        Dim f = "("
        For i As Integer = 0 To s.Count - 1
            If i > 0 Then f += " OR "
            f += "Jahrgang = " + s(i)
        Next
        f += ")"
        Return " AND " + f
    End Function
    'Private Function Get_GroupsCount(sex As String, heber As Integer) As Integer()
    '    ' maximale Anzahl der Gruppen ermitteln
    '    Dim groups As Integer
    '    Dim d = dtWK_Gruppen.Select("Geschlecht = '" & Strings.Left(sex, 1) & "' AND Heber <= " & heber, "Gruppe DESC")
    '    Try
    '        groups = CInt(d(0)("Gruppe"))
    '    Catch ex As Exception
    '        If sex = "gemischt" Then
    '            groups = CInt(Floor(heber / 9))
    '            'If groups > maxGruppen(2) Then groups = maxGruppen(2)
    '        Else
    '            groups = 1
    '        End If
    '    End Try
    '    Dim divider As Integer
    '    If groups = 0 Then
    '        ' alle Gruppen gleich groß
    '        divider = CInt(d(0)("Heber"))
    '        If mnuZusammenhalten.Checked Then
    '            ' Gruppengröße überschreiben
    '            dv = New DataView(dtMeldung)
    '            dv.RowFilter = "Geschlecht = '" & sex & "'"
    '            dv.Sort = "Jahrgang,Nachname,Vorname"
    '            Dim gr As DataTable = dv.ToTable(True, "Gruppe")
    '            dv.RowFilter = "Geschlecht = '" & sex & "' AND Gruppe = NULL"
    '            groups = gr.Rows.Count + dv.Count \ divider
    '        Else
    '            If mnuCutGroups.Checked Then
    '                ' Gruppen werden nur bis Gruppenteiler gefüllt, dann wird eine neue Gruppe angelegt
    '                groups = CInt(Ceiling(heber / divider))
    '            Else
    '                ' Gruppen werden bis Teiler-1 überfüllt: bei Teiler=7 => bis TN=27 --> 3 Gruppen, ab TN=28 --> 4 Gruppen
    '                groups = heber \ divider
    '            End If
    '        End If
    '        'If groups > maxGruppen(cboGeschlecht.Items.IndexOf(sex)) Then
    '        '    groups = maxGruppen(cboGeschlecht.Items.IndexOf(sex))
    '        '    divider = CInt(Ceiling(heber / groups))
    '        'End If
    '    Else
    '        divider = CInt(Ceiling(heber / groups))
    '    End If
    '    If heber Mod groups = 1 Then divider -= 1
    '    Return {groups, divider}
    'End Function
    Private Function Get_GroupsCount(Sex As String) As Integer
        ' gefilteres dv aus dtMeldung = alle ausgewählten Heber
        dv = New DataView(dtMeldung)
        Dim _filter = "Geschlecht = '" & Sex & "'"
        _filter += Get_JG_String(cboJahrgang.Text)
        dv.RowFilter = _filter
        dv.Sort = "KG, Nachname, Vorname"
        ' Anzahl Gruppen berechnen
        Dim _cnt = dicGruppenTeiler(Sex).FindIndex(Function(value As Integer)
                                                       Return value >= dv.Count
                                                   End Function)
        If _cnt = -1 Then _cnt = dicMaxGruppen(Sex)
        Return _cnt
    End Function
    Private Function Select_Tables(groups As Integer) As Integer()
        Select Case groups
            Case 1
                Return {3}
            Case 2
                Return {2, 5}
            Case 3
                Return {2, 3, 5}
            Case 4
                Return {2, 3, 4, 5}
            Case Else
                Return {1, 2, 3, 4, 5}
        End Select
    End Function
    Private Function Get_Gewicht() As String
        Dim tmp As DataTable = dv.ToTable
        Dim sum As Object
        sum = tmp.Compute("Sum(Wiegen)", Nothing)
        If Not IsDBNull(sum) Then
            Return "Wiegen"
        Else
            sum = tmp.Compute("Sum(Gewicht)", Nothing)
            If Not IsDBNull(sum) Then
                Return "Gewicht"
            End If
        End If
        Return String.Empty
    End Function
    Private Function Get_GroupIndex(GroupOld As Integer) As Integer
        Dim Index As Integer
        For Index = 1 To 5
            If cboGruppe(Index).Text.Equals(GroupOld.ToString) Then Return Index
        Next
        Return Index
    End Function
    Private Function Data_Update() As Boolean
        Cursor = Cursors.WaitCursor
        For i = 1 To 5
            If picGruppe(i).Visible Then
                Using New Centered_MessageBox(Me)
                    'Teilnehmer ohne Grupppenzuweisung
                    If mnuKonflikte.Checked Then MessageBox.Show("Gruppe [" & GruppenBezeichnung(cboGeschlecht.Text)(i) & "] hat keine Gruppen-Zuweisung " & vbNewLine & "und kann daher nicht gespeichert werden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
                Cursor = Cursors.Default
                Return False
            End If
            If tblGruppe(i).Rows.Count > 0 Then
                'Dim changes = tblGruppe(i).GetChanges
                'If Not IsNothing(changes) Then
                dtMeldung.Merge(tblGruppe(i), False)
                'End If
                If DataChanged Then
                    If Not String.IsNullOrEmpty(cboGruppe(i).Text) Then
                        Dim row As DataRow = ds.Tables("dtGruppen").Rows.Find(cboGruppe(i).Text)
                        If IsNothing(row) Then
                            ' neue Zeile in ds.tables("dtGruppen") anlegen
                            row = ds.Tables("dtGruppen").NewRow
                            row!Wettkampf = Wettkampf.ID
                            row!Gruppe = cboGruppe(i).Text
                            ds.Tables("dtGruppen").Rows.Add(row)
                        End If
                        row!Bezeichnung = GruppenBezeichnung(Strings.Left(cboGeschlecht.Text, 1) & i)
                        If pkDate(i).Format = DateTimePickerFormat.Custom Then
                            row!Datum = DBNull.Value
                        Else
                            row!Datum = pkDate(i).Value
                        End If
                        If pkDateW(i).Format = DateTimePickerFormat.Custom Then
                            row!DatumW = DBNull.Value
                        Else
                            row!DatumW = pkDateW(i).Value
                        End If
                        If pkTimeWB(i).CustomFormat = " " Then
                            row!WiegenBeginn = DBNull.Value
                        Else
                            row!WiegenBeginn = Format(pkTimeWB(i).Value, "HH:mm")
                        End If
                        If pkTimeWE(i).CustomFormat = " " Then
                            row!WiegenEnde = DBNull.Value
                        Else
                            row!WiegenEnde = Format(pkTimeWE(i).Value, "HH:mm")
                        End If
                        If pkTimeR(i).CustomFormat = " " Then
                            row!Reissen = DBNull.Value
                        Else
                            row!Reissen = Format(pkTimeR(i).Value, "HH:mm")
                        End If
                        If pkTimeS(i).CustomFormat = " " Then
                            row!Stossen = DBNull.Value
                        Else
                            row!Stossen = Format(pkTimeS(i).Value, "HH:mm")
                        End If
                        If pkDateA(i).Format = DateTimePickerFormat.Custom Then
                            row!DatumA = DBNull.Value
                        Else
                            row!DatumA = pkDateA(i).Value
                        End If
                        If pkTimeA(i).CustomFormat = " " Then
                            row!Athletik = DBNull.Value
                        Else
                            row!Athletik = Format(pkTimeA(i).Value, "HH:mm")
                        End If
                        row!Blockheben = chkBH(i).Checked
                        row!Bohle = cboBohle(i).Text
                    End If
                End If
            End If
        Next
        DataChanged = False
        Cursor = Cursors.Default
        Return True
    End Function


    '' Zuweisung der Gruppen in dtMeldung
    Private Sub cboGruppen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbo1.SelectedIndexChanged, cbo2.SelectedIndexChanged, cbo3.SelectedIndexChanged, cbo4.SelectedIndexChanged, cbo5.SelectedIndexChanged
        If loading Then Exit Sub
        Dim Ix = CInt(Strings.Right(CType(sender, ComboBox).Name, 1))
        Cursor = Cursors.WaitCursor
        loading = True
        Dim NewValue = CInt(CType(sender, ComboBox).Text)
        Dim ExistingGroup As DataView = New DataView(dtMeldung)
        ExistingGroup.RowFilter = "Gruppe = " & cboGruppe(Ix).Text
        Dim ExistingValue As Integer
        If ExistingGroup.Count > 0 Then ExistingValue = CInt(cboGruppe(Ix).Text)

        dv = New DataView(dtMeldung, Nothing, "Teilnehmer ASC", DataViewRowState.CurrentRows)
        If ExistingValue > 0 Then
            If NewValue.Equals(OldValue(Ix)) Then
                ' keine Änderung
                Cursor = Cursors.Default
                loading = False
                Exit Sub
            End If
            ' Gruppen-Nummer einer bestehenden Gruppen-Zuweisung an eine Gruppe mit bestehender Zuweisung vergeben
            Using New Centered_MessageBox(Me, {"Tauschen", "Entfernen"})
                Select Case MessageBox.Show("Die zu vergebende Gruppe existiert bereits." + vbNewLine + "Gruppen tauschen oder bestehende Zuweisung entfernen?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                    Case DialogResult.Yes ' Tauschen
                        ' existierende Zuweisung zurücksetzen
                        For Each row As DataRowView In ExistingGroup
                            row!Gruppe = OldValue(Ix)
                        Next
                        ' prüfen, ob ExistingGroup momentan angezeigt wird (Index suchen)
                        Dim Index = Get_GroupIndex(ExistingValue)
                        If Index < 6 Then
                            cboGruppe(Index).Text = ExistingValue.ToString
                            OldValue(Index) = ExistingValue
                            Fill_Controls(ExistingValue,, Index)
                            lblGruppe(Index).BackColor = Color.Firebrick
                            lblGruppe(Index).Text = GruppenBezeichnung(cboGeschlecht.Text)(Index) & " (" & tblGruppe(Index).Rows.Count.ToString & ")"
                        End If
                    Case DialogResult.No ' Löschen
                        ' existierende Zuweisung löschen
                        For Each row As DataRowView In ExistingGroup
                            row!Gruppe = DBNull.Value
                        Next
                        ' prüfen, ob ExistingGroup momentan angezeigt wird (Index suchen)
                        Dim Index = Get_GroupIndex(ExistingValue)
                        If Index < 6 Then
                            cboGruppe(Index).SelectedIndex = -1
                            OldValue(Index) = 0
                            lblGruppe(Index).BackColor = Color.Firebrick
                            lblGruppe(Index).Text = GruppenBezeichnung(cboGeschlecht.Text)(Index) & " (" & tblGruppe(Index).Rows.Count.ToString & ")"
                            picGruppe(Index).Visible = True
                        End If
                        ' Gruppe in ds.tables("dtGruppen") löschen
                        Fill_Controls(ExistingValue, True)
                    Case DialogResult.Cancel
                        ' cboGruppe zurücksetzen
                        cboGruppe(Ix).Text = OldValue(Ix).ToString
                        loading = False
                        Cursor = Cursors.Default
                        Exit Sub
                End Select
            End Using
        ElseIf OldValue(Ix) = 0 Then
            For Each row As DataGridViewRow In dgvGruppe(Ix).Rows
                row.DefaultCellStyle.ForeColor = dgvGruppe(Ix).ForeColor
            Next
        End If
        ' neue Gruppen-Zuweisung
        For Each row As DataRow In tblGruppe(Ix).Rows
            Dim tn = dv.Find(row!Teilnehmer)
            dv(tn)("Gruppe") = NewValue
            row!Gruppe = NewValue
        Next
        OldValue(Ix) = NewValue
        lblGruppe(Ix).BackColor = Color.Firebrick
        picGruppe(Ix).Visible = False
        If cboGruppe(Ix).Text > "" AndAlso Not cboGruppenSaved.Items.Contains(cboGruppe(Ix).Text) Then cboGruppenSaved.Items.Add(cboGruppe(Ix).Text)
        mnuGruppenLöschen.Enabled = cboGruppenSaved.Items.Count > 0
        If CType(sender, ComboBox).Focused Then DataChanged = True
        loading = False
        Cursor = Cursors.Default
    End Sub
    Private Sub cboGruppeSaved_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGruppenSaved.SelectedIndexChanged
        If loading Then Exit Sub
        loading = True
        ' Änderungen in dtMeldung und dtGruppe speichern
        If DataChanged = True Then Data_Update()
        ' DataView für die Gruppe erstellen
        dv = New DataView(dtMeldung)
        ' prüfen, ob WiegeGewicht vorhanden -> sonst MeldeGewicht verwenden
        Cursor = Cursors.WaitCursor
        ' alle Panels zurücksetzen
        For i As Integer = 1 To 5
            Disable_Controls(i)
        Next
        ' Geschlecht in cboGeschlecht anzeigen
        dv.RowFilter = "Gruppe = " & cboGruppenSaved.Text
        Dim sex = dv.ToTable(True, "Geschlecht")
        If sex.Rows.Count > 1 Then
            cboGeschlecht.Text = "gemischt"
        Else
            cboGeschlecht.Text = sex.Rows(0)("Geschlecht").ToString
        End If
        ' Jahrgänge in lstJahrgang selektieren
        lstJahrgang.ClearSelected()
        Dim _ak = dv.ToTable(True, {"Jahrgang"})
        For i = 0 To _ak.Rows.Count - 1
            lstJahrgang.SetSelected(lstJahrgang.FindString(Space(2) & _ak(i)("Jahrgang").ToString), True)
        Next
        ' richtige Gewichtsgruppe suchen
        Dim Ix As Integer
        Dim gr() As DataRow = ds.Tables("dtGruppen").Select("Gruppe = " + dv(0)("Gruppe").ToString)


        'Ix = GruppenIndex(gr(0)("Bezeichnung").ToString)

        'Gruppe einlesen
        dgvGruppe(Ix).Columns("KG").DataPropertyName = Get_Gewicht() '"Gruppe = " & cboGruppenSaved.Text)
        tblGruppe(Ix) = dv.ToTable()
        bs(Ix).DataSource = tblGruppe(Ix)
        dgvGruppe(Ix).DataSource = bs(Ix)
        Enable_Controls(Ix)
        cboGruppe(Ix).Text = cboGruppenSaved.Text
        OldValue(Ix) = CInt(cboGruppe(Ix).Text)
        Fill_Controls(CInt(cboGruppenSaved.Text), , Ix)
        Cursor = Cursors.Default
        loading = False
    End Sub

    Private Sub cbo_Filter_SelectionChanged(sender As Object, e As EventArgs) Handles cboGeschlecht.SelectedIndexChanged, cboJahrgang.TextChanged

        If sender.Equals(cboJahrgang) AndAlso Not IsNothing(CType(sender, ComboBox).Tag) Then Exit Sub

        If sender.Equals(cboGeschlecht) Then
            With lstJahrgang
                .DataSource = dicJahrgang(cboGeschlecht.Text)
                .DisplayMember = "Value"
            End With
            dv = New DataView(dtModus_GG)
            dv.RowFilter = "Geschlecht = '" & Strings.Left(cboGeschlecht.Text, 1) & "'"
            For i = 0 To dv.Count - 1
                lblGruppe(CInt(dv(i)("Gruppe"))).Text = dv(i)("Bezeichnung").ToString
            Next
        End If
        ' Anzahl Gruppen berechnen
        Dim _cnt = Get_GroupsCount(cboGeschlecht.Text)
        ' anzuzeigende Tabellen
        Dim _tables = Select_Tables(_cnt)
        ' TN-Anzahl der einzelnen Gruppen
        Dim _grps(4) As Integer
        For i = 0 To _grps.Count - 1
            If _tables.Contains(i + 1) Then _grps(i) = dv.Count \ _cnt
        Next
        ' restliche TN verteilen
        Dim c As Integer = 1
        For i = 1 To dv.Count Mod _cnt
            If _tables.Contains(c) Then
                _grps(c - 1) += 1
                c += 1
                If c = dicMaxGruppen(cboGeschlecht.Text) Then c = 1
            End If
        Next
        ' tblGruppen füllen
        c = 0
        For i As Integer = 1 To 5
            If _tables.Contains(i) Then
                tblGruppe(i).Clear()
                For ii = 0 To _grps(i - 1) - 1
                    tblGruppe(i).ImportRow(dv(c).Row)
                    c += 1
                Next
                Enable_Controls(i)
            Else
                Disable_Controls(i)
            End If
            bs(i) = New BindingSource With {.DataSource = tblGruppe(i)}
            dgvGruppe(i).DataSource = bs(i)
        Next

        '    If chkWiegenGleich.Checked Then
        '        DatumWiegen = DatumWiegen
        '        WiegeBeginn = WiegeBeginn
        '        WiegeEnde = WiegeEnde
        '    End If
        'Filter_Changed()
    End Sub

    'Private Sub Filter_Changed()
    '    If loading Then Exit Sub
    '    loading = True

    '    ' alle Panels zurücksetzen
    '    For i As Integer = 1 To 5
    '        Disable_Controls(i)
    '    Next
    '    cboGruppenSaved.SelectedIndex = -1

    '    If cboJahrgang.Text = "" Then
    '        If Not mnuJGListeOffen.Checked Then lstJahrgang.Visible = False
    '        loading = False
    '        Exit Sub
    '    End If

    '    Cursor = Cursors.WaitCursor

    '    Dim groups As Integer
    '    Dim divider As Integer
    '    Dim _tables() As Integer

    '    ' DataView für die Gruppe erstellen
    '    dv = New DataView(dtMeldung)
    '    Dim _Filter As String = String.Empty
    '    If cboGeschlecht.SelectedIndex < 2 Then _Filter = "Geschlecht = '" + cboGeschlecht.Text + "'" + " AND "
    '    _Filter += Get_JG_String(cboJahrgang.Text)
    '    dv.RowFilter = _Filter
    '    dv.Sort = "Jahrgang,Nachname,Vorname"

    '    'nicht gespeicherte Gruppen
    '    Dim c() = Get_GroupsCount(cboGeschlecht.Text, dv.Count)
    '    groups = c(0)
    '    divider = c(1)
    '    'benutzte Tabellen
    '    _tables = Select_Tables(groups)
    '    For i = 0 To _tables.Count - 1
    '        ' prüfen, ob Wiege- MeldeGewicht vorhanden -> sonst Sortierung beibehalten 
    '        Dim _Gewicht = Get_Gewicht()
    '        If Not String.IsNullOrEmpty(_Gewicht) Then dv.Sort = _Gewicht
    '        dgvGruppe(_tables(i)).Columns("KG").DataPropertyName = _Gewicht
    '        ' DataSource festlegen
    '        bs(_tables(i)).DataSource = tblGruppe(_tables(i))
    '        dgvGruppe(_tables(i)).DataSource = bs(_tables(i))
    '    Next
    '    ' Gruppen einlesen
    '    Dim ExistingGroup As Object = Nothing
    '    Dim _drop As Integer = dv.Count Mod groups + 1
    '    Dim _dropped As Integer
    '    Dim counter = -1
    '    For i = 0 To dv.Count - 1
    '        If cboGeschlecht.SelectedIndex < 2 AndAlso mnuZusammenhalten.Checked Then
    '            If counter = -1 Then counter += 1
    '            If Not dv(i)("Gruppe").Equals(ExistingGroup) Then
    '                If Not IsNothing(ExistingGroup) Then counter += 1
    '                ExistingGroup = dv(i)("Gruppe")
    '            End If
    '        Else
    '            If i Mod divider = divider - 1 - _dropped AndAlso _drop > 2 Then
    '                If _drop <= counter + 1 Then
    '                    counter += 1
    '                    _dropped += 1
    '                End If
    '            ElseIf i Mod divider = 0 And _dropped = 0 Then
    '                If counter < groups - 1 Then counter += 1
    '            End If
    '        End If
    '        With tblGruppe(_tables(counter))
    '            .ImportRow(dv(i).Row)
    '            If Not IsDBNull(dv(i)("Gruppe")) AndAlso String.IsNullOrEmpty(cboGruppe(counter + 1).Text) Then
    '                cboGruppe(_tables(counter)).Text = dv(i)("Gruppe").ToString
    '                OldValue(_tables(counter)) = CInt(dv(i)("Gruppe"))
    '            End If
    '            If Not dv(i)("Gruppe").ToString.Equals(cboGruppe(counter + 1).Text) Then
    '                dgvGruppe(_tables(counter)).Rows(tblGruppe(_tables(counter)).Rows.Count - 1).DefaultCellStyle.ForeColor = Color.Firebrick
    '            End If
    '        End With
    '    Next

    '    _tables = Select_Tables(counter + 1)
    '    For i = 0 To _tables.Count - 1
    '        Enable_Controls(_tables(i))
    '        ' nicht übereinstimmende Gruppen rot kennzeichnen
    '        If Not String.IsNullOrEmpty(cboGruppe(_tables(i)).Text) Then
    '            Dim gr() As DataRow = ds.Tables("dtGruppen").Select("Gruppe = " & cboGruppe(_tables(i)).Text) 'GruppenIndex(GruppenBezeichnung(_tables(i))))
    '            If Not gr(0)("Bezeichnung").ToString.Equals(GruppenBezeichnung(Strings.Left(cboGeschlecht.Text, 1) & _tables(i))) Then
    '                lblGruppe(_tables(i)).BackColor = Color.Firebrick
    '            End If
    '        End If
    '        Fill_Controls(If(Not IsNumeric(cboGruppe(_tables(i)).Text), 0, CInt(cboGruppe(_tables(i)).Text)), , _tables(i))
    '    Next
    '    For i = 1 To 5
    '        If lblGruppe(i).BackColor = Color.Firebrick Then
    '            DataChanged = True
    '            Exit For
    '        End If
    '    Next
    '    If Not mnuJGListeOffen.Checked Then lstJahrgang.Visible = False
    '    Cursor = Cursors.Default
    '    loading = False
    'End Sub

    Private Sub Disable_Controls(i As Integer)
        Try
            pnlGruppe(i).Enabled = False
            chkBH(i).Enabled = False
            pkDate(i).Format = DateTimePickerFormat.Custom
            pkDate(i).CustomFormat = " "
            pkDate(i).Value = CDate("1.1.1800")
            pkDateA(i).Format = DateTimePickerFormat.Custom
            pkDateA(i).CustomFormat = " "
            pkDateA(i).Value = CDate("1.1.1800")
            pkDateW(i).Format = DateTimePickerFormat.Custom
            pkDateW(i).CustomFormat = " "
            pkDateW(i).Value = CDate("1.1.1800")
            pkTimeR(i).CustomFormat = " "
            pkTimeS(i).CustomFormat = " "
            pkTimeA(i).CustomFormat = " "
            pkTimeWB(i).CustomFormat = " "
            pkTimeWE(i).CustomFormat = " "
            tblGruppe(i).Clear()
            lblGruppe(i).Text = GruppenBezeichnung(cboGeschlecht.Text)(i - 1)
            lblGruppe(i).BackColor = Color.SteelBlue
            cboBohle(i).SelectedIndex = 0
            chkBH(i).Checked = False
            cboGruppe(i).Enabled = False
            cboGruppe(i).SelectedIndex = -1
            OldValue(i) = 0
            picGruppe(i).Visible = False
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Enable_Controls(i As Integer)
        If i > 0 Then
            Try
                pnlGruppe(i).Enabled = True
                pkDateA(i).Enabled = Wettkampf.Technikwertung
                pkTimeA(i).Enabled = Wettkampf.Technikwertung
                chkBH(i).Enabled = True
                cboGruppe(i).Enabled = True
                dgvGruppe(i).ClearSelection()
                pkDate(i).Format = DateTimePickerFormat.Short
                If pkDateA(i).Enabled Then pkDateA(i).Format = DateTimePickerFormat.Short
                pkDateW(i).Format = DateTimePickerFormat.Short
                If cboBohle(i).SelectedIndex = -1 Then cboBohle(i).SelectedIndex = 0
            Catch ex As Exception
            End Try
        End If
    End Sub
    Private Sub Fill_Controls(ByVal group As Integer, Optional ByVal delete As Boolean = False, Optional ByVal i As Integer = 0)
        Try
            ' Labels beschriften
            If i > 0 Then
                lblGruppe(i).Text = GruppenBezeichnung(cboGeschlecht.Text)(group) & " (" & tblGruppe(i).Rows.Count.ToString & ")"
            End If
            ' prüfen, ob Gruppe in ds.tables("dtGruppen") vorhanden
            Dim found() As DataRow = ds.Tables("dtGruppen").Select("Gruppe = " & group)
            If group = 0 Then
                ' Gruppe ohne Zuweisung
                pkDate(i).Value = CDate(Glob.dtWK(0)("Datum"))
                pkDateA(i).Value = CDate(Glob.dtWK(0)("Datum"))
                pkDateW(i).Value = CDate(Glob.dtWK(0)("Datum"))
            ElseIf found.Count = 1 And Not delete Then
                'vorhandene Gruppe aktualisieren
                If Not IsDBNull(found(0)("Datum")) Then
                    pkDate(i).Value = CDate(found(0)("Datum"))
                Else
                    pkDate(i).Value = CDate(Glob.dtWK(0)("Datum"))
                    DataChanged = True
                End If
                If pkDateA(i).Enabled Then
                    If Not IsDBNull(found(0)("DatumA")) Then
                        pkDateA(i).Value = CDate(found(0)("DatumA"))
                    Else
                        pkDateA(i).Value = CDate(Glob.dtWK(0)("Datum"))
                        DataChanged = True
                    End If
                End If
                If Not IsDBNull(found(0)("DatumW")) Then
                    pkDateW(i).Value = CDate(found(0)("DatumW"))
                Else
                    pkDateW(i).Value = CDate(Glob.dtWK(0)("Datum"))
                    DataChanged = True
                End If
                If Not IsDBNull(found(0)("Reissen")) Then
                    pkTimeR(i).CustomFormat = "HH:mm"
                    pkTimeR(i).Text = found(0)("Reissen").ToString
                End If
                If Not IsDBNull(found(0)("Stossen")) Then
                    pkTimeS(i).CustomFormat = "HH:mm"
                    pkTimeS(i).Text = found(0)("Stossen").ToString
                End If
                If Not IsDBNull(found(0)("Athletik")) Then
                    pkTimeA(i).CustomFormat = "HH:mm"
                    pkTimeA(i).Text = found(0)("Athletik").ToString
                End If
                If Not IsDBNull(found(0)("WiegenBeginn")) Then
                    pkTimeWB(i).CustomFormat = "HH:mm"
                    pkTimeWB(i).Text = found(0)("WiegenBeginn").ToString
                End If
                If Not IsDBNull(found(0)("WiegenEnde")) Then
                    pkTimeWE(i).CustomFormat = "HH:mm"
                    pkTimeWE(i).Text = found(0)("WiegenEnde").ToString
                End If
                chkBH(i).Checked = CBool(found(0)("Blockheben"))
                cboBohle(i).Text = found(0)("Bohle").ToString
            ElseIf found.Count = 1 And delete Then
                'vorhandene Gruppe löschen
                found(0).Delete()
            End If
        Catch ex As Exception
            Stop
        End Try
    End Sub

    Private Sub cboBlockheben_CheckedChanged(sender As Object, e As EventArgs) Handles chkBH1.CheckedChanged, chkBH2.CheckedChanged, chkBH3.CheckedChanged, chkBH4.CheckedChanged, chkBH5.CheckedChanged
        If loading Then Exit Sub
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, CheckBox).Name, 1))
        If chkBH(Ix).Focused AndAlso String.IsNullOrEmpty(cboGruppe(Ix).Text) Then
            Using New Centered_MessageBox(Me)
                If mnuMeldungen.Checked Then MessageBox.Show("Zum Festlegen des Blockhebens muss eine Gruppe zugewiesen sein.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                picGruppe(Ix).Visible = True
                Exit Sub
            End Using
        End If
        If CType(sender, CheckBox).Focused Then DataChanged = True
        pnlForm.Focus()
    End Sub
    Private Sub cboBohle_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboB1.SelectedValueChanged, cboB2.SelectedValueChanged, cboB3.SelectedValueChanged, cboB4.SelectedValueChanged, cboB5.SelectedValueChanged
        If loading Then Exit Sub
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, ComboBox).Name, 1))
        If cboBohle(Ix).Focused AndAlso String.IsNullOrEmpty(cboGruppe(Ix).Text) Then
            Using New Centered_MessageBox(Me)
                If mnuMeldungen.Checked Then MessageBox.Show("Zum Festlegen der Bohle muss eine Gruppe zugewiesen sein.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                picGruppe(Ix).Visible = True
                Exit Sub
            End Using
        End If
        If CType(sender, ComboBox).Focused Then DataChanged = True
    End Sub

    Private Sub pkDatum_GotFocus(sender As Object, e As EventArgs) Handles date1.GotFocus, date2.GotFocus, date3.GotFocus, date4.GotFocus, date5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkDate(Ix).Format = DateTimePickerFormat.Short Then Exit Sub
        pkDate(Ix).Format = DateTimePickerFormat.Short
        pkDate(Ix).Value = CDate(Glob.dtWK(0)("Datum"))
    End Sub
    Private Sub pkDatum_ValueChanged(sender As Object, e As EventArgs) Handles date1.ValueChanged, date2.ValueChanged, date3.ValueChanged, date4.ValueChanged, date5.ValueChanged
        If loading Then Exit Sub
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkDate(Ix).Focused AndAlso String.IsNullOrEmpty(cboGruppe(Ix).Text) Then
            Using New Centered_MessageBox(Me)
                If mnuMeldungen.Checked Then MessageBox.Show("Zum Festlegen des Datums muss eine Gruppe zugewiesen sein.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                picGruppe(Ix).Visible = True
                Exit Sub
            End Using
        End If
        If CType(sender, DateTimePicker).Focused Then DataChanged = True
    End Sub
    Private Sub pkDatumA_GotFocus(sender As Object, e As EventArgs) Handles dateA1.GotFocus, dateA2.GotFocus, dateA3.GotFocus, dateA4.GotFocus, dateA5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkDateA(Ix).Format = DateTimePickerFormat.Short Then Exit Sub
        pkDateA(Ix).Format = DateTimePickerFormat.Short
        pkDateA(Ix).Value = CDate(Glob.dtWK(0)("Datum"))
    End Sub
    Private Sub pkDateA_ValueChanged(sender As Object, e As EventArgs) Handles dateA1.ValueChanged, dateA2.ValueChanged, dateA3.ValueChanged, dateA4.ValueChanged, dateA5.ValueChanged
        If loading Then Exit Sub
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkDateA(Ix).Focused AndAlso String.IsNullOrEmpty(cboGruppe(Ix).Text) Then
            Using New Centered_MessageBox(Me)
                If mnuMeldungen.Checked Then MessageBox.Show("Zum Festlegen des Datums muss eine Gruppe zugewiesen sein.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                picGruppe(Ix).Visible = True
                Exit Sub
            End Using
        End If
        If CType(sender, DateTimePicker).Focused Then DataChanged = True
    End Sub
    Private Sub pkDatumW_GotFocus(sender As Object, e As EventArgs) Handles dateW1.GotFocus, dateW2.GotFocus, dateW3.GotFocus, dateW4.GotFocus, dateW5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkDateW(Ix).Format = DateTimePickerFormat.Short Then Exit Sub
        pkDateW(Ix).Format = DateTimePickerFormat.Short
        pkDateW(Ix).Value = CDate(Glob.dtWK(0)("Datum"))
    End Sub
    Private Sub pkDateW_ValueChanged(sender As Object, e As EventArgs) Handles dateW1.ValueChanged, dateW2.ValueChanged, dateW3.ValueChanged, dateW4.ValueChanged, dateW5.ValueChanged
        If loading Then Exit Sub
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkDateW(Ix).Focused AndAlso String.IsNullOrEmpty(cboGruppe(Ix).Text) Then
            Using New Centered_MessageBox(Me)
                If mnuMeldungen.Checked Then MessageBox.Show("Zum Festlegen des Wiege-Datums muss eine Gruppe zugewiesen sein.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                picGruppe(Ix).Visible = True
                Exit Sub
            End Using
        End If
        DatumWiegen = pkDate(Ix).Value
        If CType(sender, DateTimePicker).Focused Then DataChanged = True
    End Sub
    Private Sub pkTimeA_GotFocus(sender As Object, e As EventArgs) Handles timeA1.GotFocus, timeA2.GotFocus, timeA3.GotFocus, timeA4.GotFocus, timeA5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkTimeA(Ix).CustomFormat = "HH:mm" Then Exit Sub
        pkTimeA(Ix).CustomFormat = "HH:mm"
        pkTimeA(Ix).Text = Format(Now, "HH:mm")
    End Sub
    Private Sub pkTimeA_ValueChanged(sender As Object, e As EventArgs) Handles timeA1.ValueChanged, timeA2.ValueChanged, timeA3.ValueChanged, timeA4.ValueChanged, timeA5.ValueChanged
        If loading Then Exit Sub
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkTimeA(Ix).Focused AndAlso String.IsNullOrEmpty(cboGruppe(Ix).Text) Then
            Using New Centered_MessageBox(Me)
                If mnuMeldungen.Checked Then MessageBox.Show("Zum Festlegen der Anfangszeit muss eine Gruppe zugewiesen sein.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                picGruppe(Ix).Visible = True
                Exit Sub
            End Using
        End If
        If pkTimeA(Ix).Text = " " Then pkTimeA(Ix).CustomFormat = "HH:mm"
        If CType(sender, DateTimePicker).Focused Then DataChanged = True
    End Sub
    Private Sub pkTimeR_GotFocus(sender As Object, e As EventArgs) Handles timeR1.GotFocus, timeR2.GotFocus, timeR3.GotFocus, timeR4.GotFocus, timeR5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkTimeR(Ix).CustomFormat = "HH:mm" Then Exit Sub
        pkTimeR(Ix).CustomFormat = "HH:mm"
        pkTimeR(Ix).Text = Format(Now, "HH:mm")
    End Sub
    Private Sub pkTimeR_ValueChanged(sender As Object, e As EventArgs) Handles timeR1.ValueChanged, timeR2.ValueChanged, timeR3.ValueChanged, timeR4.ValueChanged, timeR5.ValueChanged
        If loading Then Exit Sub
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkTimeR(Ix).Focused AndAlso String.IsNullOrEmpty(cboGruppe(Ix).Text) Then
            Using New Centered_MessageBox(Me)
                If mnuMeldungen.Checked Then MessageBox.Show("Zum Festlegen der Anfangszeit muss eine Gruppe zugewiesen sein.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                picGruppe(Ix).Visible = True
                Exit Sub
            End Using
        End If
        'wenn die erste Gruppe geändet wird, Zeiten für die nächsten kalkulieren
        Dim minutes As Integer = tblGruppe(Ix).Rows.Count * Zeit_pro_Hebung
        minutes = ((minutes \ Zeit_Rundung) + CInt((minutes Mod Zeit_Rundung) / Zeit_Rundung)) * Zeit_Rundung
        Dim _zeit1 As TimeSpan = TimeSpan.Parse(pkTimeR(Ix).Text)
        Dim _zeit2 As New TimeSpan(minutes \ 60, minutes Mod 60, 0)
        Dim _zeit3 As TimeSpan = _zeit1 + _zeit2
        pkTimeS(Ix).Text = _zeit3.ToString
        _zeit3 += _zeit2
        pkTimeA(Ix).Text = _zeit3.ToString
        If CType(sender, DateTimePicker).Focused Then DataChanged = True
    End Sub
    Private Sub pkTimeS_GotFocus(sender As Object, e As EventArgs) Handles timeS1.GotFocus, timeS2.GotFocus, timeS3.GotFocus, timeS4.GotFocus, timeS5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkTimeS(Ix).CustomFormat = "HH:mm" Then Exit Sub
        pkTimeS(Ix).CustomFormat = "HH:mm"
        pkTimeS(Ix).Text = Format(Now, "HH:mm")
    End Sub
    Private Sub pkTimeS_ValueChanged(sender As Object, e As EventArgs) Handles timeS1.ValueChanged, timeS2.ValueChanged, timeS3.ValueChanged, timeS4.ValueChanged, timeS5.ValueChanged
        If loading Then Exit Sub
        Dim Ix = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkTimeS(Ix).Focused AndAlso String.IsNullOrEmpty(cboGruppe(Ix).Text) Then
            Using New Centered_MessageBox(Me)
                If mnuMeldungen.Checked Then MessageBox.Show("Zum Festlegen der Anfangszeit muss eine Gruppe zugewiesen sein.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                picGruppe(Ix).Visible = True
                Exit Sub
            End Using
        End If
        If pkTimeS(Ix).Text = " " Then pkTimeS(Ix).CustomFormat = "HH:mm"
        If CType(sender, DateTimePicker).Focused Then DataChanged = True
    End Sub
    Private Sub pkTimeWB_GotFocus(sender As Object, e As EventArgs) Handles timeWb1.GotFocus, timeWb2.GotFocus, timeWb3.GotFocus, timeWb4.GotFocus, timeWb5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkTimeWB(Ix).CustomFormat = "HH:mm" Then Exit Sub
        pkTimeWB(Ix).CustomFormat = "HH:mm"
        pkTimeWB(Ix).Text = Format(Now, "HH:mm")
    End Sub
    Private Sub pkTimeWB_ValueChanged(sender As Object, e As EventArgs) Handles timeWb1.ValueChanged, timeWb2.ValueChanged, timeWb3.ValueChanged, timeWb4.ValueChanged, timeWb5.ValueChanged
        If loading Then Exit Sub
        Dim Ix = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkTimeWB(Ix).Focused AndAlso String.IsNullOrEmpty(cboGruppe(Ix).Text) Then
            Using New Centered_MessageBox(Me)
                If mnuMeldungen.Checked Then MessageBox.Show("Zum Festlegen der Wiegezeit muss eine Gruppe zugewiesen sein.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                picGruppe(Ix).Visible = True
                Exit Sub
            End Using
        End If
        If pkTimeWB(Ix).Text = " " Then pkTimeWB(Ix).CustomFormat = "HH:mm"
        WiegeBeginn = pkTimeWB(Ix).Value
        If CType(sender, DateTimePicker).Focused Then DataChanged = True
    End Sub
    Private Sub pkTimeWE_GotFocus(sender As Object, e As EventArgs) Handles timeWe1.GotFocus, timeWe2.GotFocus, timeWe3.GotFocus, timeWe4.GotFocus, timeWe5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkTimeWE(Ix).CustomFormat = "HH:mm" Then Exit Sub
        pkTimeWE(Ix).CustomFormat = "HH:mm"
        pkTimeWE(Ix).Text = Format(Now, "HH:mm")
    End Sub
    Private Sub pkTimeWe_ValueChanged(sender As Object, e As EventArgs) Handles timeWe1.ValueChanged, timeWe2.ValueChanged, timeWe3.ValueChanged, timeWe4.ValueChanged, timeWe5.ValueChanged
        If loading Then Exit Sub
        Dim Ix = CInt(Strings.Right(CType(sender, DateTimePicker).Name, 1))
        If pkTimeWE(Ix).Focused AndAlso String.IsNullOrEmpty(cboGruppe(Ix).Text) Then
            Using New Centered_MessageBox(Me)
                If mnuMeldungen.Checked Then MessageBox.Show("Zum Festlegen der Anfangszeit muss eine Gruppe zugewiesen sein.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                picGruppe(Ix).Visible = True
                Exit Sub
            End Using
        End If
        If pkTimeWE(Ix).Text = " " Then pkTimeWE(Ix).CustomFormat = "HH:mm"
        WiegeEnde = pkTimeWE(Ix).Value
        If CType(sender, DateTimePicker).Focused Then DataChanged = True
    End Sub

    Private Sub lstJahrgang_KeyDown(sender As Object, e As KeyEventArgs) Handles lstJahrgang.KeyDown
        With lstJahrgang
            Select Case e.KeyCode
                Case Keys.Escape
                    .Tag = "escaped"
                    .Visible = False
                    With cboJahrgang
                        .Text = .Tag.ToString
                        .Tag = Nothing
                    End With
                Case Keys.Enter
                    .Tag = Nothing
                    lstJahrgang_Save()
                Case Keys.A
                    If ModifierKeys = Keys.Control Then
                        For i = 0 To .Items.Count - 1
                            .SetSelected(i, True)
                        Next
                        .Tag = Nothing
                        lstJahrgang_Save()
                    End If
                Case Keys.ControlKey
                    If IsNothing(.Tag) Then
                        .Tag = "ctrl"
                    ElseIf .Tag.ToString.Contains("shift") Then
                        .Tag = "ctrl+shift"
                    End If
                Case Keys.ShiftKey
                    If IsNothing(.Tag) Then
                        .Tag = "shift"
                    ElseIf .Tag.ToString.Contains("ctrl") Then
                        .Tag = "ctrl+shift"
                    End If
            End Select
        End With
    End Sub
    Private Sub lstJahrgang_KeyUp(sender As Object, e As KeyEventArgs) Handles lstJahrgang.KeyUp
        With lstJahrgang
            If Not IsNothing(.Tag) Then
                Select Case e.KeyCode
                    Case Keys.ControlKey
                        If .Tag.ToString.Contains("shift") Then
                            .Tag = "shift"
                        Else
                            .Tag = Nothing
                        End If
                    Case Keys.ShiftKey
                        If .Tag.ToString.Contains("ctrl") Then
                            .Tag = "ctrl"
                        Else
                            .Tag = Nothing
                        End If
                End Select
            End If
        End With
    End Sub
    Private Sub lstJahrgang_MouseClick(sender As Object, e As MouseEventArgs) Handles lstJahrgang.MouseClick
        With lstJahrgang
            If Not .IndexFromPoint(e.Location) = ListBox.NoMatches Then
                If Not IsNothing(.Tag) Then .SetSelected(.IndexFromPoint(e.Location), True)
                lstJahrgang_Save()
            End If
        End With
    End Sub
    Private Sub lstJahrgang_MouseMove(sender As Object, e As MouseEventArgs) Handles lstJahrgang.MouseMove
        With lstJahrgang
            If Not .IndexFromPoint(e.Location) = ListBox.NoMatches Then
                If IsNothing(.Tag) Then .ClearSelected()
                .SetSelected(.IndexFromPoint(e.Location), True)
            End If
        End With
    End Sub
    Private Sub lstJahrgang_Save()
        Dim tx As String = String.Empty
        With lstJahrgang
            For i = .SelectedItems.Count - 1 To 0 Step -1
                If tx > "" Then tx += ", "
                tx += Strings.Right(.SelectedItems(i).ToString, 4)
            Next
            .Visible = False
            .Tag = Nothing
        End With
        With cboJahrgang
            .Tag = "updating"
            .Items.Clear()
            .Items.Add(tx)
            .Tag = Nothing
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub cboJahrgang_Click(sender As Object, e As EventArgs) Handles cboJahrgang.Click
        cboJahrgang.DroppedDown = False
        With lstJahrgang
            If .Visible Then
                .Visible = False
                'pnlForm.Focus()
            Else
                .Height = 17 + (.Items.Count - 1) * 13
                .Visible = True
                .Focus()
                cboJahrgang.Tag = cboJahrgang.Text
            End If
        End With
    End Sub

    Private Sub dgvGruppen_Build(grid As DataGridView)
        Try
            With grid
                .AutoGenerateColumns = False
                .RowHeadersVisible = False
                .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor 'Color.OldLace
                .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor 'Color.LightCyan
                .DefaultCellStyle.SelectionBackColor = RowsDefaultCellStyle_BackColor
                .DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.ControlText)
                Dim ColWidth() As Integer = {10, 50, 50, 26, 35, 16}
                Dim ColBez() As String = {"TN", "Nachname", "Vorname", "JG", "KG", ""}
                Dim ColData() As String = {"Teilnehmer", "Nachname", "Vorname", "Jahrgang", "KG", "Gruppe"}
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "TN", .Visible = False})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "JG"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "KG"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Gruppe", .Visible = False})
                For i As Integer = 0 To ColWidth.Count - 1
                    .Columns(i).FillWeight = ColWidth(i)
                    .Columns(i).HeaderText = ColBez(i)
                    .Columns(i).DataPropertyName = ColData(i)
                    .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                Next
                .Columns("JG").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("KG").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns("KG").DefaultCellStyle.Format = "0.00"
                .Sort(.Columns("KG"), ListSortDirection.Ascending)
                .Columns("Gruppe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
        Catch ex As Exception
            Stop
        End Try
    End Sub
    Private Sub dgvGruppe_DragDrop(sender As Object, e As DragEventArgs) Handles dgvGK1.DragDrop, dgvGK2.DragDrop, dgvGK3.DragDrop, dgvGK4.DragDrop, dgvGK5.DragDrop
        ' prüfen ob Quelle und Ziel identisch sind
        For TargetTable = 1 To dgvGruppe.Count
            If dgvGruppe.ContainsKey(TargetTable) Then
                If sender.Equals(dgvGruppe(TargetTable)) Then Exit For
            End If
        Next
        If SourceTable = TargetTable Then Exit Sub ' identisch
        Cursor = Cursors.WaitCursor
        Dim found As DataRow()
        With dgvGruppe(TargetTable)
            If .Rows.Count = 0 Then
                ' Gruppe in den cbo's anhängen
                For i As Integer = 1 To 5
                    cboGruppe(i).Items.Add(cboGruppe(i).Items.Count + 1)
                Next
            End If
            .Columns("KG").DataPropertyName = dgvGruppe(SourceTable).Columns("KG").DataPropertyName
            For i = 0 To dgvSelection.Count - 1
                found = tblGruppe(SourceTable).Select("Teilnehmer = " + dgvSelection(i).ToString)
                tblGruppe(TargetTable).ImportRow(found(0))
                tblGruppe(SourceTable).Rows.Remove(found(0))
                If Not String.IsNullOrEmpty(cboGruppe(TargetTable).Text) Then
                    found(0)("Gruppe") = cboGruppe(TargetTable).Text
                Else
                    found(0)("Gruppe") = DBNull.Value
                End If
            Next
            Try
                .Sort(.Columns("KG"), ListSortDirection.Ascending)
                For i = 0 To dgvSelection.Count - 1
                    For Each row As DataGridViewRow In .Rows
                        If row.Cells("TN").Value.Equals(dgvSelection(i)) Then
                            row.DefaultCellStyle.ForeColor = Color.Firebrick
                        End If
                    Next
                Next
            Catch ex As Exception
            End Try
        End With
        Enable_Controls(TargetTable)
        If dgvGruppe(SourceTable).Rows.Count = 0 Then 'And cboGruppe(SourceTable).Text > "" Then
            'alle Heber der Gruppe entfernt
            If Not String.IsNullOrEmpty(cboGruppe(SourceTable).Text) Then
                cboGruppenSaved.Items.Remove(cboGruppe(SourceTable).Text)
                For i = 1 To 5
                    cboGruppe(i).Items.Remove(cboGruppe(SourceTable).Text)
                Next
                cboGruppe(SourceTable).SelectedIndex = -1
            End If
            Disable_Controls(SourceTable)
            OldValue(SourceTable) = 0
            lblGruppe(SourceTable).Text = GruppenBezeichnung(cboGeschlecht.Text)(SourceTable)
        Else
            lblGruppe(SourceTable).Text = GruppenBezeichnung(cboGeschlecht.Text)(SourceTable) + " (" + tblGruppe(SourceTable).Rows.Count.ToString + ")"
            draging = dgvGruppe(TargetTable).Rows.Count = 0 'True
        End If
        lblGruppe(TargetTable).Text = GruppenBezeichnung(cboGeschlecht.Text)(TargetTable) + " (" + tblGruppe(TargetTable).Rows.Count.ToString + ")"
        DataChanged = True
        Cursor = Cursors.Default
    End Sub
    Private Sub dgvGruppe_DragEnter(sender As Object, e As DragEventArgs) Handles dgvGK1.DragEnter, dgvGK2.DragEnter, dgvGK3.DragEnter, dgvGK4.DragEnter, dgvGK5.DragEnter
        Dim t As Integer
        For t = 1 To dgvGruppe.Count
            If sender.Equals(dgvGruppe(t)) Then Exit For
        Next
        If t = SourceTable Then
            e.Effect = DragDropEffects.None
        Else
            e.Effect = DragDropEffects.Move
        End If
    End Sub
    Private Sub dgvGruppe_GotFocus(sender As Object, e As EventArgs) Handles dgvGK1.GotFocus, dgvGK2.GotFocus, dgvGK3.GotFocus, dgvGK4.GotFocus, dgvGK5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DataGridView).Name, 1))
        With dgvGruppe(Ix)
            If .Rows.Count = 0 Then Exit Sub
            With .DefaultCellStyle
                .SelectionBackColor = Color.FromKnownColor(KnownColor.Highlight)
                .SelectionForeColor = Color.FromKnownColor(KnownColor.HighlightText)
            End With
        End With
    End Sub
    Private Sub dgvGruppe_LostFocus(sender As Object, e As EventArgs) Handles dgvGK1.LostFocus, dgvGK2.LostFocus, dgvGK3.LostFocus, dgvGK4.LostFocus, dgvGK5.LostFocus
        Dim Ix As Integer = CInt(Strings.Right(CType(sender, DataGridView).Name, 1))
        With dgvGruppe(Ix)
            If .Rows.Count = 0 Then Exit Sub
            If .CurrentRow.Index Mod 2 = 0 Then
                .DefaultCellStyle.SelectionBackColor = RowsDefaultCellStyle_BackColor
            ElseIf .CurrentRow.Index Mod 2 = 1 Then
                .DefaultCellStyle.SelectionBackColor = AlternatingRowsDefaultCellStyle_BackColor
            End If
            .DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.ControlText)
        End With
    End Sub
    Private Sub dgvGruppe_MouseDown(sender As Object, e As MouseEventArgs) Handles dgvGK1.MouseDown, dgvGK2.MouseDown, dgvGK3.MouseDown, dgvGK4.MouseDown, dgvGK5.MouseDown
        SourceTable = CInt(Strings.Right(CType(sender, DataGridView).Name, 1))
        With dgvGruppe(SourceTable)
            Dim hit As DataGridView.HitTestInfo = .HitTest(e.X, e.Y)
            dgvSelection.Clear()
            If hit.RowIndex > -1 Then
                If .SelectedRows.Contains(.Rows(hit.RowIndex)) Then
                    For i As Integer = 0 To .SelectedRows.Count - 1
                        dgvSelection.Add(CInt(.SelectedRows(i).Cells("TN").Value))
                    Next
                Else
                    dgvSelection.Add(CInt(.Rows(hit.RowIndex).Cells("TN").Value))
                End If
                .DoDragDrop(dgvSelection, DragDropEffects.Move)
            End If
        End With
    End Sub


End Class