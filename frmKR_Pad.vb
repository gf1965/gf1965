﻿
Public Class frmKR_Pad

    Shared mySerialPort As SerialPort

    Private Sub frmKR_Pad_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            With mySerialPort
                .Write({12}, 0, 1)
                .Write({14}, 0, 1)
                '.Close()
                .Dispose()
            End With
        Catch ex As Exception

        End Try
    End Sub
    'Shared SP_Open As Boolean = False

    'Private WithEvents myDriveWatcher As New DriveChangeWatcher(Me)

    'Private Sub myDriveWatcher_DriveCoutChanged(ByVal sender As Object, ByVal e As System.IO.DriveInfo) Handles myDriveWatcher.DriveArrived
    '    MsgBox("Arrived -> " & e.Name & " - " & e.DriveType.ToString)
    'End Sub

    'Private Sub myDriveWatcher_DriveRemoved(ByVal sender As Object, ByVal e As System.IO.DriveInfo) Handles myDriveWatcher.DriveRemoved
    '    MsgBox("Removed -> " & e.Name & " - " & e.DriveType.ToString)
    'End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Beim Laden der Anwendung werden die seriellen Ports in die ComboBox gelistet
        mySerialPort = New SerialPort

        '***
        'AddHandler mySerialPort.DataReceived, AddressOf SP_ReadData
        '***

        btnRefresh.PerformClick()

    End Sub

    'Private Sub Button1_Click(sender As Object, e As EventArgs) Handles VerbindenKnopf.Click
    Private Sub ComPort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComPort.SelectedIndexChanged
        Me.Cursor = Cursors.WaitCursor

        If mySerialPort.IsOpen = True Then 'Wenn die Schnittstelle schon aktiv war wird sie zuerst geschlossen
            mySerialPort.Close()
        End If

        'Die Schnittstelle wird konfiguriert und geöffnet. Ein eventueller Fehler wird gemeldet
        Try
            mySerialPort.PortName = CStr(ComPort.SelectedItem)
            mySerialPort.BaudRate = 9600
            mySerialPort.Parity = Parity.None
            mySerialPort.DataBits = 8
            mySerialPort.stopbits = CType(1, Stopbits)
            mySerialPort.Handshake = Handshake.None

            mySerialPort.ReadTimeout = 500
            mySerialPort.WriteTimeout = 500

            mySerialPort.Open()
        Catch ex As Exception
            MsgBox("Fehler beim Öffnen des Ports" & Chr(13) & ex.ToString, MsgBoxStyle.Critical, "Fehler")
            Me.Cursor = Cursors.Default
            Exit Sub
        End Try

        mySerialPort.DiscardInBuffer()
        'Hier wird überprüft ob an der Schnittstelle ein Pad angeschlossen ist (Es muss auf Nachfrage seine ID nennen)
        Try
            mySerialPort.Write({5}, 0, 1)
        Catch ex As Exception
            MsgBox("Ausgewähltes Gerät ist kein Kampfrichter-Pad.", MsgBoxStyle.Information, "Fehler")
            UI_Grouper.Enabled = False
            Me.Cursor = Cursors.Default
            Exit Sub
        End Try
        System.Threading.Thread.Sleep(50)
        Try
            Dim ID As Byte = CByte(mySerialPort.ReadByte - 97)
            If ID < 16 And ID > 0 Then
                ID_Label.Text = "ID:" + (ID).ToString
                UI_Grouper.Enabled = True       'Hier wird der restliche Teil des Interfaces (Form) freigeschalten
                Timer1.Start()                  'Timer1 veranlasst dreimal pro Sekunde ein Auslesen der Schnittstelle
                '***
                'SP_Open = True                  'Port offen => SP_ReadData::ReadExisting()
                '***
                mySerialPort.Write({12}, 0, 1)  'Beim anfänglichen Verbinden wird das Display in den leeren Ausgangszustand zurückgesetzt
            Else
                MsgBox("Kein Kampfrichter-Pad an diesem Port", MsgBoxStyle.Critical, "Fehler")
                ID_Label.Text = "ID: ?"
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("Ausgewähltes Gerät ist kein Kampfrichter-Pad.", MsgBoxStyle.Information, "Fehler")
            ID_Label.Text = "ID: ?"
            UI_Grouper.Enabled = False
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles LichtAn.CheckedChanged, LichtAus.CheckedChanged
        '0x0f und 0x0e schalten die Beleuchtung an und aus
        If LichtAn.Checked = True Then
            mySerialPort.Write({15}, 0, 1)
        Else
            mySerialPort.Write({14}, 0, 1)
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles Clear_Buchstaben.CheckedChanged, Clear_Zeile.CheckedChanged
        'Via 0x03 und 0x04 wird das Verhalten des "Clear"-Knopfes auf dem Pad festgelegt
        If Clear_Buchstaben.Checked = True Then
            mySerialPort.Write({3}, 0, 1)
        Else
            mySerialPort.Write({4}, 0, 1)
        End If
    End Sub

    Private Sub RadioButton5_CheckedChanged(sender As Object, e As EventArgs) Handles Zeile2_Tasten.CheckedChanged, Zeile2_PC.CheckedChanged
        '0x01 und 0x02 bestimmen die Kontrolle über die zweite Zeile
        If Zeile2_Tasten.Checked = True Then
            mySerialPort.Write({1}, 0, 1)
            Zeile2_Text.Enabled = False
        Else
            mySerialPort.Write({2}, 0, 1)
            Zeile2_Text.Enabled = True
        End If
    End Sub

    Private Sub RadioButton7_CheckedChanged(sender As Object, e As EventArgs) Handles Erg_Note.CheckedChanged
        'Hier wird zwischen der Eingabe von Wertung und Gültigkeit umgeschalten (hauptsächlich UI-Anpassungen, keine Kommunikation mit dem Display)
        Erg_Text.Text = ""
        If Zeile2_PC.Checked = False Then
            mySerialPort.Write({10}, 0, 1) 'Falls die zweite Zeile vom Nutzer schon beschrieben worden sein könnte wird sie hier geleert
        End If
        Erg_Text.BackColor = Color.White
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Piep_Knopf.Click
        '0x07 lässt den Summer kurz piepen
        mySerialPort.Write({7}, 0, 1)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Zeile1_Loe.Click
        '0x0D löscht die erste Zeile
        mySerialPort.Write({13}, 0, 1)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Zeile2_Loe.Click
        '0x0A löscht die zweite Zeile
        mySerialPort.Write({10}, 0, 1)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Disp_Loe.Click
        '0x0C löscht beide Zeilen
        mySerialPort.Write({12}, 0, 1)
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Senden_Knopf.Click
        'Hier wird der Inhalt der beiden Textboxen auf das Display geschrieben
        mySerialPort.Write({13}, 0, 1) 'Zeile 1 löschen und auswählen
        mySerialPort.Write(Zeile1_Text.Text) 'Text für Zeile1 abschicken
        If Zeile2_Text.Enabled = True Then 'Falls die zweite Zeile vom PC beschrieben wird dann:
            mySerialPort.Write({10}, 0, 1) 'Zeile2 löschen und auswählen
            mySerialPort.Write(Zeile2_Text.Text) 'Text abschicken
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        'In dem Timer-Tick werden die empfangenen Daten abgeholt und verarbeitet
        Dim InData As String = String.Empty
        Try
            InData = mySerialPort.ReadExisting() 'Vorhandene Daten werden von der Schnittstelle abgeholt
        Catch ex As Exception
        End Try
        If String.IsNullOrEmpty(InData) = False Then 'Nur wenn Daten vorhanden sind wird die Verarbeitung gestartet
            For Each item As Char In InData 'Die Zeichen werden einzeln, der Reihenfolge nach verarbeitet
                Select Case item
                    Case Convert.ToChar(10) '0x0A bedeuetet: "Enter" gedrückt -> Feld grün hinterlegen
                        Erg_Text.BackColor = Color.LightGreen
                    Case Convert.ToChar(13) '0x0D bedeutet: "Ganze Eingabe löschen"
                        Erg_Text.Text = ""
                        Erg_Text.BackColor = Color.White
                    Case Convert.ToChar(8)  '0x08 bedeuetet: "Letztes Zeichen löschen
                        If Erg_Guelt.Checked Then 'Falls Gültig/Ungültig -> Immer ganze Zeile löschen
                            Erg_Text.Text = ""
                            If Zeile2_PC.Checked = False Then
                                mySerialPort.Write({10}, 0, 1)
                            End If
                        End If
                        If CBool(Erg_Text.Text.Length) Then 'Falls die Textbox nicht leer ist wird ein Zeichen vom Ende entfernt
                            Erg_Text.Text = Erg_Text.Text.Remove(Erg_Text.Text.Length - 1)
                        End If
                        Erg_Text.BackColor = Color.White
                    Case CChar("U") '"Ungültig" gedrückt
                        If Erg_Guelt.Checked Then 'Falls nach Gültigkeit gefragt war Antwort anzeigen
                            Erg_Text.Text = "Ungültig"
                        Else                            'Ansonsten sinnlose Eingabe komplett löschen
                            Erg_Text.Text = ""
                            If Zeile2_PC.Checked = False Then 'Ansonsten sinnlose Eingabe komplett löschen
                                mySerialPort.Write({10}, 0, 1)
                            End If
                        End If
                        Erg_Text.BackColor = Color.White
                    Case CChar("G") 'Selbes Verfahren nur mit "Gültig"
                        If Erg_Guelt.Checked Then
                            Erg_Text.Text = "Gültig"
                        Else
                            Erg_Text.Text = ""
                            If Zeile2_PC.Checked = False Then
                                mySerialPort.Write({10}, 0, 1)
                            End If
                        End If
                        Erg_Text.BackColor = Color.White
                    Case Else
                        If Erg_Note.Checked Then
                            Erg_Text.Text += item 'Normale Zahlen -> einfach durchreichen
                        Else
                            Erg_Text.Text = "" 'Zahlen in Gültigkeitsfrage getippt -> löschen
                            If Zeile2_PC.Checked = False Then
                                mySerialPort.Write({10}, 0, 1)
                            End If
                        End If
                        Erg_Text.BackColor = Color.White
                End Select
            Next
        End If
        ' End If

    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Dim ports As String() = SerialPort.GetPortNames()
        ComPort.Items.Clear()
        ComPort.Items.AddRange(ports)
        ComPort.SelectedIndex = -1
    End Sub

End Class
