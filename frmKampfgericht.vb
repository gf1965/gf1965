﻿
Imports MySqlConnector

Public Class frmKampfgericht
    Dim Bereich As String = "Kampfrichter"
    Dim conn As MySqlConnection
    Dim Query As String
    Dim cmd As MySqlCommand
    Dim dtKampfrichter As New DataTable
    Dim dtGruppen As New DataTable
    Dim dv As DataView
    Dim bs As BindingSource

    Private Sub frmKampfgericht_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If btnSave.Enabled Then
            Dim result As DialogResult
            Using New Centered_MessageBox(Me)
                result = MessageBox.Show("Änderungen vor dem Schließen speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
            End Using
            If result = DialogResult.Cancel Then
                e.Cancel = True
            ElseIf result = DialogResult.Yes Then
                btnSave.PerformClick()
                Close()
            End If
        End If
    End Sub
    Private Sub frmKampfgericht_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Cursor = Cursors.WaitCursor
        If String.IsNullOrEmpty(User.ConnString) Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Anmeldung an MySQL-Server nicht nöglich." & vbNewLine & "(Keine Anmeldedaten vorhanden.)", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            Close()
            Return
        End If

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                cmd = New MySqlCommand("SELECT Wettkampf, Gruppe FROM gruppen WHERE Wettkampf = " & Wettkampf.ID & " ORDER BY Gruppe;", conn)
                dtGruppen.Load(cmd.ExecuteReader())
                cmd = New MySqlCommand("SELECT Wettkampf, Gruppe, KR_1, KR_2, KR_3, ZN, TC, CM, Sec FROM gruppen_kampfrichter WHERE Wettkampf = " & Wettkampf.ID & ";", conn)
                dtKampfrichter.Load(cmd.ExecuteReader())
            Catch ex As Exception
            End Try
        End Using

        dv = New DataView(dtKampfrichter)
        dv.Sort = "Gruppe"

        For Each row As DataRow In dtGruppen.Rows
            Dim x = dv.Find(row!Gruppe)
            If x = -1 Then
                Dim nr = dtKampfrichter.NewRow
                nr!Wettkampf = row!Wettkampf
                nr!Gruppe = row!Gruppe
                nr!KR_1 = String.Empty
                nr!KR_2 = String.Empty
                nr!KR_3 = String.Empty
                nr!ZN = String.Empty
                nr!TC = String.Empty
                nr!CM = String.Empty
                nr!Sec = String.Empty
                dtKampfrichter.Rows.Add(nr)
            End If
        Next

        bs = New BindingSource With {.DataSource = dv}
        With dgvKR
            .AutoGenerateColumns = False
            .DataSource = bs
        End With

        btnSave.Enabled = False
        formMain.Change_MainProgressBarValue(0)
        Cursor = Cursors.Default
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Validate()
        bs.EndEdit()
        Dim changes = dtKampfrichter.GetChanges
        Dim UpdateCmd = "UPDATE gruppen_kampfrichter SET KR_1 = @kr1, KR_2 = @kr2, KR_3 = @kr3, ZN = @zn, TC = @tc, CM = @cm, Sec = @sec WHERE Wettkampf = @wk AND Gruppe = @gr;"
        Dim InsertCmd = "INSERT INTO gruppen_kampfrichter (Wettkampf, Gruppe, KR_1, KR_2, KR_3, ZN, TC, CM, Sec) VALUES (@wk, @gr, @kr1, @kr2, @kr3, @zn, @tc, @cm, @sec);"

        Query = "INSERT INTO gruppen_kampfrichter 
                    (Wettkampf, Gruppe, KR_1, KR_2, KR_3, ZN, TC, CM, Sec) 
                    VALUES (@wk, @gr, @kr1, @kr2, @kr3, @zn, @tc, @cm, @sec) 
                ON DUPLICATE KEY UPDATE KR_1 = @kr1, KR_2 = @kr2, KR_3 = @kr3, ZN = @zn, TC = @tc, CM = @cm, Sec = @sec;"

        If Not IsNothing(changes) Then
            Using conn As New MySqlConnection(User.ConnString)
                conn.Open()
                For Each WK In Wettkampf.Identities.Keys
                    For Each row As DataRow In changes.Rows
                        'If row.RowState = DataRowState.Added Then
                        '    cmd = New MySqlCommand(InsertCmd, conn)
                        'ElseIf row.RowState = DataRowState.Modified Then
                        '    cmd = New MySqlCommand(UpdateCmd, conn)
                        'End If
                        cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", WK)
                            .AddWithValue("@gr", row!Gruppe)
                            .AddWithValue("@kr1", row!KR_1)
                            .AddWithValue("@kr2", row!KR_2)
                            .AddWithValue("@kr3", row!KR_3)
                            .AddWithValue("@zn", row!ZN)
                            .AddWithValue("@tc", row!TC)
                            .AddWithValue("@cm", row!CM)
                            .AddWithValue("@sec", row!Sec)
                        End With
                        Try
                            cmd.ExecuteNonQuery()
                        Catch ex As Exception
                            ConnError = True
                            Exit For
                        End Try
                    Next
                Next
            End Using
        End If
        If Not ConnError Then dtKampfrichter.AcceptChanges()
        btnSave.Enabled = CBool(ConnError)
    End Sub
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Close()
    End Sub

    Private Sub dgvKR_CurrentCellDirtyStateChanged(sender As Object, e As EventArgs) Handles dgvKR.CurrentCellDirtyStateChanged
        btnSave.Enabled = True
        Tag = "changed"
    End Sub

End Class