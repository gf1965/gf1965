﻿Public Class frmHeberwechsel

    Public dvEmergency As New DataView
    Public dtGruppen As New DataTable
    Public NeuerHeber As Integer
    Public Gruppe As String

    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        With cboGruppe
            .Parent = pnlAustausch
            .DataSource = dtGruppen
            .DisplayMember = "Gruppe"
            .ValueMember = "Gruppe"
            .SelectedIndex = .FindStringExact(Gruppe)
        End With

        lblAustauschMessage.Text = "für " & dvLeader(Leader.TableIx)(bsLeader.Position)!Nachname.ToString & ", " & dvLeader(Leader.TableIx)(bsLeader.Position)!Vorname.ToString

        With pnlAustausch
            .Visible = True
            .BringToFront()
        End With

        With EmergencyLifters
            .AutoGenerateColumns = False
            .DataSource = dvEmergency
            .ClearSelection()
            .Focus()
            btnAustauchOK.Enabled = .Rows.Count > 0 AndAlso .SelectedRows.Count > 0
        End With

    End Sub

    Private Sub btnAustauchOK_Click(sender As Object, e As EventArgs) Handles btnAustauchOK.Click
        Gruppe = cboGruppe.Text
        NeuerHeber = CInt(dvEmergency(EmergencyLifters.SelectedRows(0).Index)!Teilnehmer)
        Close()
    End Sub

    Private Sub EmergencyLifters_SelectionChanged(sender As Object, e As EventArgs) Handles EmergencyLifters.SelectionChanged
        btnAustauchOK.Enabled = EmergencyLifters.SelectedRows.Count = 1
    End Sub

End Class