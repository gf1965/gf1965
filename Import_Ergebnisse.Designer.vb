﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Import_Ergebnisse
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.btnFile = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.dgvErgebnisse = New System.Windows.Forms.DataGridView()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.lblMsg = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.dgvGruppe = New System.Windows.Forms.DataGridView()
        Me.GruppeSelected = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.GruppenNummer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GruppenBezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Expand = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Checked = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Nummer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gruppe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Datum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvErgebnisse, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvGruppe, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.Location = New System.Drawing.Point(435, 25)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(95, 25)
        Me.btnImport.TabIndex = 0
        Me.btnImport.TabStop = False
        Me.btnImport.Text = "Importieren"
        Me.ToolTip1.SetToolTip(Me.btnImport, "ausgewählten Wettkampf importieren")
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(435, 56)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(95, 25)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.TabStop = False
        Me.btnCancel.Text = "Schließen"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'FolderBrowserDialog1
        '
        Me.FolderBrowserDialog1.Description = """Verzeichnis der Export-Datei auswählen"""
        Me.FolderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyComputer
        '
        'btnFile
        '
        Me.btnFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFile.Location = New System.Drawing.Point(435, 223)
        Me.btnFile.Name = "btnFile"
        Me.btnFile.Size = New System.Drawing.Size(95, 25)
        Me.btnFile.TabIndex = 2
        Me.btnFile.TabStop = False
        Me.btnFile.Text = "Import-Datei"
        Me.ToolTip1.SetToolTip(Me.btnFile, "zu importierende Datei suchen")
        Me.btnFile.UseVisualStyleBackColor = True
        '
        'dgvErgebnisse
        '
        Me.dgvErgebnisse.AllowUserToAddRows = False
        Me.dgvErgebnisse.AllowUserToDeleteRows = False
        Me.dgvErgebnisse.AllowUserToOrderColumns = True
        Me.dgvErgebnisse.AllowUserToResizeColumns = False
        Me.dgvErgebnisse.AllowUserToResizeRows = False
        Me.dgvErgebnisse.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvErgebnisse.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvErgebnisse.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvErgebnisse.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvErgebnisse.ColumnHeadersHeight = 22
        Me.dgvErgebnisse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvErgebnisse.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Expand, Me.Checked, Me.Nummer, Me.Gruppe, Me.Datum})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvErgebnisse.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvErgebnisse.Location = New System.Drawing.Point(22, 25)
        Me.dgvErgebnisse.Name = "dgvErgebnisse"
        Me.dgvErgebnisse.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvErgebnisse.RowHeadersVisible = False
        Me.dgvErgebnisse.RowHeadersWidth = 23
        Me.dgvErgebnisse.RowTemplate.ErrorText = "+"
        Me.dgvErgebnisse.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.dgvErgebnisse.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvErgebnisse.ShowEditingIcon = False
        Me.dgvErgebnisse.Size = New System.Drawing.Size(392, 223)
        Me.dgvErgebnisse.TabIndex = 3
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.DefaultExt = "xml"
        Me.OpenFileDialog1.Filter = "XML-Datei (*.xml)|*.xml|alle Dateien (*.*)|*.*"
        Me.OpenFileDialog1.Title = "Import-Datei auswählen"
        '
        'lblMsg
        '
        Me.lblMsg.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMsg.AutoSize = True
        Me.lblMsg.Location = New System.Drawing.Point(20, 262)
        Me.lblMsg.Name = "lblMsg"
        Me.lblMsg.Size = New System.Drawing.Size(39, 13)
        Me.lblMsg.TabIndex = 4
        Me.lblMsg.Text = "Label1"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar1.Location = New System.Drawing.Point(22, 281)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(508, 19)
        Me.ProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.ProgressBar1.TabIndex = 5
        '
        'dgvGruppe
        '
        Me.dgvGruppe.AllowUserToAddRows = False
        Me.dgvGruppe.AllowUserToDeleteRows = False
        Me.dgvGruppe.AllowUserToOrderColumns = True
        Me.dgvGruppe.AllowUserToResizeColumns = False
        Me.dgvGruppe.AllowUserToResizeRows = False
        Me.dgvGruppe.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGruppe.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvGruppe.ColumnHeadersHeight = 21
        Me.dgvGruppe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvGruppe.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.GruppeSelected, Me.GruppenNummer, Me.GruppenBezeichnung})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.LightGoldenrodYellow
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvGruppe.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgvGruppe.EnableHeadersVisualStyles = False
        Me.dgvGruppe.Location = New System.Drawing.Point(44, 56)
        Me.dgvGruppe.MultiSelect = False
        Me.dgvGruppe.Name = "dgvGruppe"
        Me.dgvGruppe.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvGruppe.RowHeadersVisible = False
        Me.dgvGruppe.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGruppe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGruppe.Size = New System.Drawing.Size(366, 100)
        Me.dgvGruppe.TabIndex = 6
        Me.dgvGruppe.Visible = False
        '
        'GruppeSelected
        '
        Me.GruppeSelected.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.GruppeSelected.HeaderText = ""
        Me.GruppeSelected.Name = "GruppeSelected"
        Me.GruppeSelected.Width = 24
        '
        'GruppenNummer
        '
        Me.GruppenNummer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.GruppenNummer.DefaultCellStyle = DataGridViewCellStyle6
        Me.GruppenNummer.HeaderText = " Gruppe "
        Me.GruppenNummer.Name = "GruppenNummer"
        Me.GruppenNummer.ReadOnly = True
        Me.GruppenNummer.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GruppenNummer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.GruppenNummer.Width = 52
        '
        'GruppenBezeichnung
        '
        Me.GruppenBezeichnung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.GruppenBezeichnung.HeaderText = "Bezeichnung"
        Me.GruppenBezeichnung.Name = "GruppenBezeichnung"
        Me.GruppenBezeichnung.ReadOnly = True
        Me.GruppenBezeichnung.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GruppenBezeichnung.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Expand
        '
        Me.Expand.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        Me.Expand.DefaultCellStyle = DataGridViewCellStyle2
        Me.Expand.HeaderText = ""
        Me.Expand.Name = "Expand"
        Me.Expand.ReadOnly = True
        Me.Expand.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Expand.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Expand.ToolTipText = "Gruppen anzeigen"
        Me.Expand.Width = 23
        '
        'Checked
        '
        Me.Checked.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Checked.HeaderText = ""
        Me.Checked.Name = "Checked"
        Me.Checked.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Checked.ThreeState = True
        Me.Checked.ToolTipText = "zum Import markieren"
        Me.Checked.Width = 24
        '
        'Nummer
        '
        Me.Nummer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Nummer.DefaultCellStyle = DataGridViewCellStyle3
        Me.Nummer.HeaderText = "Nr"
        Me.Nummer.Name = "Nummer"
        Me.Nummer.ReadOnly = True
        Me.Nummer.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Nummer.Width = 35
        '
        'Gruppe
        '
        Me.Gruppe.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Gruppe.HeaderText = "Wettkampf"
        Me.Gruppe.Name = "Gruppe"
        Me.Gruppe.ReadOnly = True
        Me.Gruppe.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Gruppe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Datum
        '
        Me.Datum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Datum.HeaderText = "Datum"
        Me.Datum.Name = "Datum"
        Me.Datum.ReadOnly = True
        Me.Datum.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Datum.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Datum.Width = 70
        '
        'Import_Ergebnisse
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(550, 315)
        Me.Controls.Add(Me.dgvGruppe)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.lblMsg)
        Me.Controls.Add(Me.dgvErgebnisse)
        Me.Controls.Add(Me.btnFile)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnImport)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Import_Ergebnisse"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Wettkampf-Ergebnisse importieren"
        CType(Me.dgvErgebnisse, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvGruppe, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnImport As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents btnFile As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents dgvErgebnisse As DataGridView
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents lblMsg As Label
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents dgvGruppe As DataGridView
    Friend WithEvents GruppeSelected As DataGridViewCheckBoxColumn
    Friend WithEvents GruppenNummer As DataGridViewTextBoxColumn
    Friend WithEvents GruppenBezeichnung As DataGridViewTextBoxColumn
    Friend WithEvents Expand As DataGridViewTextBoxColumn
    Friend WithEvents Checked As DataGridViewCheckBoxColumn
    Friend WithEvents Nummer As DataGridViewTextBoxColumn
    Friend WithEvents Gruppe As DataGridViewTextBoxColumn
    Friend WithEvents Datum As DataGridViewTextBoxColumn
End Class
