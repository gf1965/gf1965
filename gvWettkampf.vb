﻿Imports MySqlConnector

Public Class gvWettkampf

    Dim Query As String
    Dim cmd As MySqlCommand

    Dim dv As DataView
    Dim Bereich As String = "Wettkampf_Auswahl"
    Dim _Onlyid As Boolean
    Dim CancelClosing As Boolean = False
    Dim loading As Boolean = True

    Property Get_Id_Only As Boolean ' True = geöffneter WK wird zur Bearbeitung geöffnet --> kein erneutes Einlesen der Tabellen
        Get
            Return _Onlyid
        End Get
        Set(value As Boolean)
            _Onlyid = value
            'btnNew.Visible = Not _Onlyid
            'btnBundesliga_Import.Visible = Not _Onlyid
        End Set
    End Property
    Property SelectedWk As KeyValuePair(Of String, String) ' Wettkampf-Id, Wettkampf-Bezeichnung

    Private Sub gvWettkampf_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = CancelClosing
        CancelClosing = False
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Cursor = Cursors.WaitCursor

        Dim dtWk As New DataTable

        Using conn As New MySqlConnection(User.ConnString)
            Do
                Try
                    If Not conn.State.Equals(ConnectionState.Open) Then conn.Open()
                    Query = "Select w.Wettkampf, w.Datum, w.Ort, b.Bez1, w.Modus, ifnull(v.Kurzname, v.Vereinsname) Vereinsname, m.Bezeichnung, u.Multi, u.Primaer   
                            From wettkampf w 
                            LEFT Join verein v ON w.Veranstalter = v.idVerein 
                            LEFT Join modus m ON w.Modus = m.idModus 
                            Left Join wettkampf_bezeichnung b ON b.wettkampf = w.Wettkampf 
                            left join wettkampf_multi u on u.Wettkampf = w.Wettkampf
                            ORDER BY w.Datum DESC, b.Bez1, Vereinsname ASC;"
                    cmd = New MySqlCommand(Query, conn)
                    dtWk.Load(cmd.ExecuteReader)
                    ConnError = False
                Catch ex As MySqlException
                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                End Try
            Loop While ConnError
        End Using

        If Not ConnError Then
            dv = New DataView(dtWk)
            bs = New BindingSource With {.DataSource = dv}
            With dgvWK
                .AutoGenerateColumns = False
                .DataSource = bs
                .RowHeadersVisible = .MultiSelect
            End With
        Else
            Wettkampf.ID = 0
            Close()
            Return
        End If

        dgvWK.Focus()
        Cursor = Cursors.Default
        loading = False
    End Sub

    Private Sub dgvWK_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles dgvWK.MouseDoubleClick
        If e.Button.ToString = "Left" AndAlso dgvWK.SelectedRows.Count = 1 Then btnÜbernehmen.PerformClick()
    End Sub
    Private Sub dgvWK_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvWK.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                btnÜbernehmen.PerformClick()
                e.Handled = True
        End Select
    End Sub
    Private Sub dgvWK_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvWK.RowEnter
        If loading Then Return
        'dgvWK.ClearSelection()
        'dgvWK.Rows(e.RowIndex).Selected = True
        If dgvWK.MultiSelect Then
            Dim WKs = Split(dv(e.RowIndex)!Multi.ToString, ",").ToList
            WKs.Remove(String.Empty)
            If WKs.Count < 2 Then Return
            For Each WK As String In WKs
                Dim pos = bs.Find("Wettkampf", WK)
                If Not pos = e.RowIndex Then
                    dgvWK.Rows(pos).Selected = True
                End If
            Next
        End If
    End Sub

    Private Sub mnuBeenden_Click(sender As Object, e As EventArgs) Handles mnuBeenden.Click
        Close()
    End Sub

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        formMain.mnuWK_New.PerformClick()
        Close()
    End Sub
    Private Sub btnÜbernehmen_Click(sender As Object, e As EventArgs) Handles btnÜbernehmen.Click, mnuÜbernehmen.Click

        Dim Multis As New Dictionary(Of String, String)

        If dgvWK.SelectedRows.Count > 1 Then
            For Each row As DataGridViewRow In dgvWK.SelectedRows
                Multis(dv(row.Index)!Wettkampf.ToString) = dv(row.Index)!Multi.ToString
            Next
            For i = 0 To Multis.Count - 1
                If Not Multis.Values(i).Contains(Multis.Keys(i)) Then
                    Dim keys = Multis.Keys.ToList
                    keys.Sort()
                    Dim pos = bs.Find("Wettkampf", keys(0))
                    Using New Centered_MessageBox(Me, {"Verknüpfen"})
                        Select Case MessageBox.Show("Die ausgewählten Wettkämpfe sind nicht verknüpft.",
                                        msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                            Case DialogResult.OK
                                Dim SrcTable = New DataTable
                                SrcTable.Columns.Add(New DataColumn With {.ColumnName = "Wettkampf", .DataType = GetType(Integer)})
                                SrcTable.Columns.Add(New DataColumn With {.ColumnName = "Joined", .DataType = GetType(Integer)})
                                SrcTable.Columns.Add(New DataColumn With {.ColumnName = "Primaer", .DataType = GetType(Boolean), .DefaultValue = False})
                                For Each key In keys
                                    SrcTable.Rows.Add({CInt(key), 1})
                                Next
                                Using ds As New DataService
                                    Dim response = ds.ValidateEquality(Me, keys(0), SrcTable)
                                    If Not response.Key Then
                                        If IsNothing(response.Value) Then
                                            CancelClosing = True
                                            Return
                                        Else
                                            Dim msg = "Folgende Eigenschaften der Wettkämpfe stimmmen nicht überein:" & vbNewLine & vbNewLine
                                            For Each table In response.Value
                                                If table.Value.Count > 0 Then
                                                    msg += "- " & table.Key & ": " & Join(table.Value.ToArray, ", ") & vbNewLine
                                                End If
                                            Next
                                            msg += vbNewLine & "Alle relevanten Eigenschaften werden an:" & vbNewLine &
                                                dv(pos)!Bez1.ToString & " [" & keys(0) & "]" & vbNewLine & "angeglichen."
                                            Using New Centered_MessageBox(Me, {"Anpassen"})
                                                If MessageBox.Show(msg, msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) = DialogResult.Cancel Then
                                                    CancelClosing = True
                                                    Return
                                                End If
                                            End Using
                                        End If
                                    End If
                                    ds.Save_Wettkampf_Multi(SrcTable.Select(), Nothing, SrcTable, keys(0))
                                    ds.EqualizeMulti(Me, SrcTable, keys(0))
                                    SelectedWk = New KeyValuePair(Of String, String)(dv(pos)!Wettkampf.ToString, dv(pos)!Bez1.ToString)
                                End Using
                            Case DialogResult.Cancel
                                CancelClosing = True
                        End Select
                    End Using
                    Exit For
                Else
                    SelectedWk = New KeyValuePair(Of String, String)(Multis.Keys(i), Multis.Values(i))
                    Exit For
                End If
            Next
        Else
            SelectedWk = New KeyValuePair(Of String, String)(dv(bs.Position)!Wettkampf.ToString, dv(bs.Position)!Bez1.ToString)
        End If

    End Sub
    Private Sub BtnBundesliga_Import_Click(sender As Object, e As EventArgs) Handles btnBundesliga_Import.Click
        formMain.mnuImport_Bundesliga.PerformClick()
        Close()
    End Sub

    Private Sub mnuMauszeiger_Click_1(sender As Object, e As EventArgs) Handles mnuMauszeiger.Click
        Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
        Cursor.Show()
    End Sub


End Class