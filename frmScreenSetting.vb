﻿Public Class frmScreenSetting

    Property FormText As String

    Private IsDirty As Boolean

    Private Sub frmScreenSetting_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        RemoveHandler Ansicht_Options.ShadeChanged, AddressOf Change_ShadeColors
    End Sub

    Private Sub frmScreenSetting_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim x As String = String.Empty

        AddHandler Ansicht_Options.ShadeChanged, AddressOf Change_ShadeColors

        If File.Exists(App_IniFile) Then
            For Each ctl As Control In TabControl1.TabPages("TabBohle").Controls
                If Not (IsNothing(ctl.Tag) OrElse String.IsNullOrEmpty(ctl.Tag.ToString)) Then
                    Dim keys() As String = Split(CStr(ctl.Tag), ":")
                    If TypeOf (ctl) Is Label Then
                        If x <> "?" Then ctl.ForeColor = Color.FromArgb(CInt(x))
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub frmScreenSetting_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Select Case Split(FormText)(0)
            Case "Wertung"
                TabControl1.SelectedTab = TabWertung
            Case "Bohle"
                TabControl1.SelectedTab = TabBohle
        End Select
    End Sub

    Private Sub btnFontSizeLarger_Click(sender As Object, e As EventArgs) Handles btnFontSizeLarger.Click
        Ansicht_Options.Change_FontSize(0.5)
    End Sub

    Private Sub btnFontSizeSmaller_Click(sender As Object, e As EventArgs) Handles btnFontSizeSmaller.Click
        Ansicht_Options.Change_FontSize(-0.5)
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dim DlgResult As DialogResult = DialogResult.Yes

        If Not HideMessages AndAlso IsDirty Then
            Using New Centered_MessageBox(Me)
                DlgResult = MessageBox.Show("Änderungen an der Bohlen-Ansicht speichern?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            End Using
        End If

        If IsDirty AndAlso DlgResult = DialogResult.Yes Then Save_Colors()
        Close()
    End Sub

    Private Sub btnResetFontsize_Click(sender As Object, e As EventArgs) Handles btnResetFontsize.Click
        Ansicht_Options.Change_FontSize(0, True)
    End Sub

    Private Sub lblName_Click(sender As Object, e As EventArgs) Handles lblName.Click,
                                                                        lblStartnummer.Click,
                                                                        lblVerein.Click,
                                                                        lblAK.Click,
                                                                        lblDurchgang.Click,
                                                                        lblHantellast.Click,
                                                                        lblVersuch.Click
        Dim ctl = CType(sender, Label)
        With ColorDialog1
            .Color = ctl.ForeColor
            .FullOpen = True
            If .ShowDialog() = DialogResult.OK Then
                ctl.ForeColor = .Color
                IsDirty = True
            End If
        End With
    End Sub
    Private Sub lblWertung_Click(sender As Object, e As EventArgs) Handles lblWertung.Click
        Dim Color_OldValue = Ansicht_Options.Shade_Color
        With frmKontrast
            .Left = Left + 140
            .Top = Top + 190
            .ShowDialog(Me)
            If .DialogResult = DialogResult.Cancel Then
                Ansicht_Options.Shade_Color = Color_OldValue
            End If
        End With
    End Sub

    Private Sub btnDefault_Bohle_Click(sender As Object, e As EventArgs) Handles btnDefault_Bohle.Click

        IsDirty = lblName.ForeColor <> Color.FromArgb(-10496) OrElse
                  lblStartnummer.ForeColor <> Color.FromArgb(-10496) OrElse
                  lblVerein.ForeColor <> Color.FromArgb(-10185235) OrElse
                  lblAK.ForeColor <> Color.FromArgb(-10185235) OrElse
                  lblDurchgang.ForeColor <> Color.FromArgb(-32944) OrElse
                  lblWertung.ForeColor <> Color.FromArgb(-13092808)

        lblName.ForeColor = Color.FromArgb(-10496)
        lblStartnummer.ForeColor = Color.FromArgb(-10496)
        lblVerein.ForeColor = Color.FromArgb(-10185235)
        lblAK.ForeColor = Color.FromArgb(-10185235)
        lblDurchgang.ForeColor = Color.FromArgb(-32944)
        lblWertung.ForeColor = Color.FromArgb(-13092808)
    End Sub

    Private Sub Save_Colors()
        For Each ctl As Control In TabControl1.TabPages("TabBohle").Controls
            If Not (IsNothing(ctl.Tag) OrElse String.IsNullOrEmpty(ctl.Tag.ToString)) Then
                Dim keys() As String = Split(CStr(ctl.Tag), ":")
                If TypeOf (ctl) Is Label Then
                    NativeMethods.INI_Write(keys(0), keys(1), ctl.ForeColor.ToArgb.ToString, App_IniFile)
                End If
            End If
        Next
    End Sub

    Private Sub Change_ShadeColors()
        lblWertung.ForeColor = Ansicht_Options.Shade_Color
    End Sub

End Class