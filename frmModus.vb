﻿
Imports MySqlConnector

Public Class frmModus
    Dim Bereich As String = "Modus"

    Dim da As MySqlDataAdapter
    Dim dw As New DataTable
    Dim ds As New DataSet
    Dim dv As DataView
    Dim loading As Boolean

    Private Sub Me_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Dispose()
    End Sub
    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Validate()
        bs.EndEdit()
        Dim changes As DataTable = ds.Tables("modus").GetChanges()
        If Not IsNothing(changes) Then
            Using New Centered_MessageBox(Me)
                Select Case MessageBox.Show("Änderungen in Wettkampf-Modus speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                    Case DialogResult.Yes
                        Dim cb As MySqlCommandBuilder = New MySqlCommandBuilder(da)
                        da.Update(changes)
                        ds.Tables("modus").AcceptChanges()
                    Case DialogResult.Cancel
                        e.Cancel = True
                End Select
            End Using
        End If
    End Sub
    Private Sub frmModus_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim tTip As New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        tTip.SetToolTip(nudMinAge, "Mindestalter für Teilnahme an Wettkämpfen mit diesem Modus")
        tTip.SetToolTip(lblMinAge, "Mindestalter für Teilnahme an Wettkämpfen mit diesem Modus")
        tTip.SetToolTip(optSex, "männlich 20-kg-Hantel / weiblich 15-kg-Hantel")
        tTip.SetToolTip(opt20kg, "ausschließlich 20-kg-Hantel")
        tTip.SetToolTip(opt15kg, "15-kg-Hantel mit Option 7- oder 5-kg-Hantel")
        tTip.SetToolTip(optAK, "- weiblich und männlich bis AK 14 immer 15-kg-Hantel mit Option 7- oder 5-kg-Hantel" & vbNewLine & "- ab AK 15 männlich 20-kg-Hantel")

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Cursor = Cursors.WaitCursor

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                da = New MySqlDataAdapter("SELECT * FROM modus;", conn)
                da.Fill(ds, "modus")
                Dim cmd As New MySqlCommand("SELECT * FROM wertung ORDER BY idWertung;", conn)
                dw.Load(cmd.ExecuteReader)
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            End Try
        End Using

        With cboWertung
            .DataSource = dw
            .ValueMember = "idWertung"
            .DisplayMember = "Bezeichnung"
        End With

        dv = New DataView(ds.Tables("modus"))
        dv.Sort = "Bezeichnung ASC"
        bs = New BindingSource With {.DataSource = dv}
        bn.BindingSource = bs
        dgvModus.DataSource = bs
        Build_Grid(dgvModus, Bereich)

        txtBezeichnung.DataBindings.Add(New Binding("Text", bs, "Bezeichnung"))
        chkTechnikwertung.DataBindings.Add(New Binding("Checked", bs, "Technikwertung"))
        chkAthletik.DataBindings.Add(New Binding("Checked", bs, "Athletik"))
        txtSteigerung1.DataBindings.Add(New Binding("Text", bs, "Steigerung1"))
        txtSteigerung2.DataBindings.Add(New Binding("Text", bs, "Steigerung2"))
        cboEinteilung_Alter.DataBindings.Add(New Binding("Text", bs, "Alterseinteilung"))
        cboEinteilung_Gewicht.DataBindings.Add(New Binding("Text", bs, "Gewichtseinteilung"))
        cboWertung.DataBindings.Add(New Binding("SelectedValue", bs, "Wertung"))
        txtHantelstange.DataBindings.Add(New Binding("Text", bs, "Hantelstange"))
        chkRegel20.DataBindings.Add(New Binding("Checked", bs, "Regel20"))
        nudMinAge.DataBindings.Add(New Binding("Value", bs, "MinAlter"))
        chkLosnummern.DataBindings.Add(New Binding("Checked", bs, "AutoLosnummer"))
        nudMinAge.Tag = nudMinAge.Value ' wofür auch immer das gedacht ist

        Cursor = Cursors.Default
    End Sub

    Private Sub mnuBeenden_Click(sender As Object, e As EventArgs) Handles mnuBeenden.Click, btnExit.Click
        Close()
    End Sub
    Private Sub mnuLayoutModus_Click(sender As Object, e As EventArgs) Handles mnuLayoutModus.Click
        frmTableLayout.Tag = Bereich
        If Not frmTableLayout.ShowDialog(Me) = DialogResult.Cancel Then
            Build_Grid(dgvModus, Bereich)
        End If
        frmTableLayout.Dispose()
    End Sub
    Private Sub mnuLöschen_Click(sender As Object, e As EventArgs) Handles mnuLöschen.Click
        dv.Delete(bs.Position)
    End Sub
    Private Sub mnuNeu_Click(sender As Object, e As EventArgs) Handles mnuNeu.Click
        'ds.Tables("modus").Rows.Add(ds.Tables("modus").NewRow)
        'dv.AddNew()
        bs.AddNew()
    End Sub
    Private Sub mnuSpeichern_Click(sender As Object, e As EventArgs) Handles mnuSpeichern.Click, bnSave.Click, btnSave.Click
        Validate()
        bs.EndEdit()
        Dim changes As DataTable = ds.Tables("modus").GetChanges()
        If Not IsNothing(changes) Then
            Dim cb As MySqlCommandBuilder = New MySqlCommandBuilder(da)
            da.Update(changes)
            ds.Tables("modus").AcceptChanges()
        End If
    End Sub

    Private Sub pnlHantel_Click(sender As Object, e As EventArgs) Handles optSex.Click, opt20kg.Click, opt15kg.Click, optAK.Click
        chk7kg.Enabled = opt15kg.Checked OrElse optAK.Checked
        chk5kg.Enabled = opt15kg.Checked OrElse optAK.Checked
        txtHantelstange.Text = CType(sender, RadioButton).Tag.ToString
        If chk7kg.Enabled AndAlso chk7kg.Checked Then txtHantelstange.Text += "_" & chk7kg.Tag.ToString
        If chk5kg.Enabled AndAlso chk5kg.Checked Then txtHantelstange.Text += "_" & chk5kg.Tag.ToString
    End Sub
    Private Sub pnlHantelSub_Click(sender As Object, e As EventArgs) Handles chk7kg.Click, chk5kg.Click
        loading = True
        txtHantelstange.Text = Strings.Left(txtHantelstange.Text, 1)
        If chk7kg.Checked Then txtHantelstange.Text += "_" & chk7kg.Tag.ToString
        If chk5kg.Checked Then txtHantelstange.Text += "_" & chk5kg.Tag.ToString
        loading = False
    End Sub
    Private Sub txtHantel_TextChanged(sender As Object, e As EventArgs) Handles txtHantelstange.TextChanged
        If loading Then Exit Sub
        With txtHantelstange.Text
            optSex.Checked = .Contains("s")
            opt20kg.Checked = .Contains("m")
            opt15kg.Checked = .Contains("w")
            optAK.Checked = .Contains("a")
            chk7kg.Checked = .Contains("k")
            chk5kg.Checked = .Contains("x")
        End With
        chk7kg.Enabled = opt15kg.Checked OrElse optAK.Checked
        chk5kg.Enabled = opt15kg.Checked OrElse optAK.Checked
    End Sub

    Private Sub Build_Grid(grid As DataGridView, Bereich As String)
        'Dim ColWidth() As Integer = {30, 100, 70, 70, 70, 70, 70, 70, 70}
        Dim ColBez() As String = {"ID", "Bezeichnung", "Technik- Wertung", "Athletik", "1. Steigerung", "2. Steigerung", "Einteilung Alter", "Einteilung Gewicht", "Wertung", "Hantel- Stange", "Mindest- Alter"}
        Dim ColData() As String = {"idModus", "Bezeichnung", "Technikwertung", "Athletik", "Steigerung1", "Steigerung2", "Alterseinteilung", "Gewichtseinteilung", "Wertung", "Hantelstange", "MinAlter"}
        With grid
            .AlternatingRowsDefaultCellStyle.BackColor = Color.OldLace
            .RowsDefaultCellStyle.BackColor = Color.LightCyan
            .AllowUserToOrderColumns = True
            For i As Integer = 0 To ColBez.Count - 1
                '.Columns(i).Width = ColWidth(i)
                .Columns(i).HeaderText = ColBez(i)
                .Columns(i).DataPropertyName = ColData(i)
            Next
            '.Columns("idModus").Visible = False
        End With
        Format_Grid(Bereich, grid)
    End Sub


End Class