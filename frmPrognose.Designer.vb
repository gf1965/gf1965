﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrognose
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.dgvHeber = New System.Windows.Forms.DataGridView()
        Me.Nachname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vorname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Team = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Reißen = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Stoßen = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvResult = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Rolle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Mannschaft = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gegner = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Reissen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Stossen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gesamt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Punkte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Punkte2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Platz = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvResult, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(634, 394)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 0
        Me.btnExit.Text = "Schließen"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.Location = New System.Drawing.Point(553, 394)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(75, 23)
        Me.btnRefresh.TabIndex = 5
        Me.btnRefresh.Text = "&Aktualisieren"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.dgvHeber)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgvResult)
        Me.SplitContainer1.Size = New System.Drawing.Size(721, 387)
        Me.SplitContainer1.SplitterDistance = 248
        Me.SplitContainer1.TabIndex = 6
        '
        'dgvHeber
        '
        Me.dgvHeber.AllowUserToAddRows = False
        Me.dgvHeber.AllowUserToDeleteRows = False
        Me.dgvHeber.AllowUserToOrderColumns = True
        Me.dgvHeber.AllowUserToResizeColumns = False
        Me.dgvHeber.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        Me.dgvHeber.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvHeber.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvHeber.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvHeber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHeber.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Nachname, Me.Vorname, Me.Team, Me.Reißen, Me.Stoßen})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Beige
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvHeber.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvHeber.Location = New System.Drawing.Point(13, 27)
        Me.dgvHeber.Name = "dgvHeber"
        Me.dgvHeber.RowHeadersVisible = False
        Me.dgvHeber.RowHeadersWidth = 62
        Me.dgvHeber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvHeber.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHeber.Size = New System.Drawing.Size(695, 220)
        Me.dgvHeber.TabIndex = 3
        '
        'Nachname
        '
        Me.Nachname.DataPropertyName = "Nachname"
        Me.Nachname.FillWeight = 80.0!
        Me.Nachname.HeaderText = "Nachname"
        Me.Nachname.MinimumWidth = 8
        Me.Nachname.Name = "Nachname"
        Me.Nachname.ReadOnly = True
        '
        'Vorname
        '
        Me.Vorname.DataPropertyName = "Vorname"
        Me.Vorname.FillWeight = 80.0!
        Me.Vorname.HeaderText = "Vorname"
        Me.Vorname.MinimumWidth = 8
        Me.Vorname.Name = "Vorname"
        Me.Vorname.ReadOnly = True
        '
        'Team
        '
        Me.Team.DataPropertyName = "Verein"
        Me.Team.HeaderText = "Mannschaft"
        Me.Team.MinimumWidth = 8
        Me.Team.Name = "Team"
        Me.Team.ReadOnly = True
        '
        'Reißen
        '
        Me.Reißen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Reißen.DataPropertyName = "Reißen"
        Me.Reißen.HeaderText = "Reißen"
        Me.Reißen.MinimumWidth = 8
        Me.Reißen.Name = "Reißen"
        Me.Reißen.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Reißen.Width = 45
        '
        'Stoßen
        '
        Me.Stoßen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Stoßen.DataPropertyName = "Stoßen"
        Me.Stoßen.HeaderText = "Stoßen"
        Me.Stoßen.MinimumWidth = 8
        Me.Stoßen.Name = "Stoßen"
        Me.Stoßen.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Stoßen.Width = 45
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "&Prognose"
        '
        'dgvResult
        '
        Me.dgvResult.AllowUserToAddRows = False
        Me.dgvResult.AllowUserToResizeColumns = False
        Me.dgvResult.AllowUserToResizeRows = False
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dgvResult.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvResult.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvResult.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvResult.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Rolle, Me.Mannschaft, Me.Gegner, Me.Reissen, Me.Stossen, Me.Gesamt, Me.Punkte, Me.Punkte2, Me.Platz})
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvResult.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgvResult.Location = New System.Drawing.Point(13, 22)
        Me.dgvResult.Name = "dgvResult"
        Me.dgvResult.RowHeadersVisible = False
        Me.dgvResult.RowHeadersWidth = 62
        Me.dgvResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResult.Size = New System.Drawing.Size(695, 101)
        Me.dgvResult.TabIndex = 2
        Me.dgvResult.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "&Teilnehmer"
        '
        'Rolle
        '
        Me.Rolle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Rolle.DataPropertyName = "Rolle"
        Me.Rolle.HeaderText = ""
        Me.Rolle.MinimumWidth = 8
        Me.Rolle.Name = "Rolle"
        Me.Rolle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Rolle.Width = 50
        '
        'Mannschaft
        '
        Me.Mannschaft.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Mannschaft.DataPropertyName = "Kurzname"
        Me.Mannschaft.HeaderText = "Mannschaft"
        Me.Mannschaft.Name = "Mannschaft"
        Me.Mannschaft.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Gegner
        '
        Me.Gegner.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Gegner.DataPropertyName = "Gegner"
        Me.Gegner.HeaderText = "Gegner"
        Me.Gegner.Name = "Gegner"
        Me.Gegner.Visible = False
        '
        'Reissen
        '
        Me.Reissen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Reissen.DataPropertyName = "Reissen"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.Reissen.DefaultCellStyle = DataGridViewCellStyle6
        Me.Reissen.HeaderText = "Reißen"
        Me.Reissen.MinimumWidth = 8
        Me.Reissen.Name = "Reissen"
        Me.Reissen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Reissen.Width = 75
        '
        'Stossen
        '
        Me.Stossen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Stossen.DataPropertyName = "Stossen"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.Format = "N2"
        Me.Stossen.DefaultCellStyle = DataGridViewCellStyle7
        Me.Stossen.HeaderText = "Stoßen"
        Me.Stossen.MinimumWidth = 8
        Me.Stossen.Name = "Stossen"
        Me.Stossen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Stossen.Width = 75
        '
        'Gesamt
        '
        Me.Gesamt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gesamt.DataPropertyName = "Heben"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.Format = "N2"
        Me.Gesamt.DefaultCellStyle = DataGridViewCellStyle8
        Me.Gesamt.HeaderText = "Gesamt"
        Me.Gesamt.MinimumWidth = 8
        Me.Gesamt.Name = "Gesamt"
        Me.Gesamt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Gesamt.Width = 75
        '
        'Punkte
        '
        Me.Punkte.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Punkte.DataPropertyName = "Punkte"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle9.Format = "N0"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.Punkte.DefaultCellStyle = DataGridViewCellStyle9
        Me.Punkte.HeaderText = "Pkt"
        Me.Punkte.MinimumWidth = 8
        Me.Punkte.Name = "Punkte"
        Me.Punkte.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Punkte.Width = 35
        '
        'Punkte2
        '
        Me.Punkte2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Punkte2.DataPropertyName = "Punkte2"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle10.Format = "N0"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.Punkte2.DefaultCellStyle = DataGridViewCellStyle10
        Me.Punkte2.HeaderText = "Pkt"
        Me.Punkte2.MinimumWidth = 8
        Me.Punkte2.Name = "Punkte2"
        Me.Punkte2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Punkte2.Visible = False
        Me.Punkte2.Width = 35
        '
        'Platz
        '
        Me.Platz.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Platz.DataPropertyName = "Platz"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.Firebrick
        DataGridViewCellStyle11.Format = "N0"
        Me.Platz.DefaultCellStyle = DataGridViewCellStyle11
        Me.Platz.HeaderText = "Platz"
        Me.Platz.MinimumWidth = 8
        Me.Platz.Name = "Platz"
        Me.Platz.Width = 35
        '
        'frmPrognose
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(722, 429)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.SplitContainer1)
        Me.DoubleBuffered = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(736, 462)
        Me.Name = "frmPrognose"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Prognose Wettkampf-Ergebnis"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvResult, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnExit As Button
    Friend WithEvents btnRefresh As Button
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents dgvHeber As DataGridView
    Friend WithEvents Nachname As DataGridViewTextBoxColumn
    Friend WithEvents Vorname As DataGridViewTextBoxColumn
    Friend WithEvents Team As DataGridViewTextBoxColumn
    Friend WithEvents Reißen As DataGridViewCheckBoxColumn
    Friend WithEvents Stoßen As DataGridViewCheckBoxColumn
    Friend WithEvents dgvResult As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Rolle As DataGridViewTextBoxColumn
    Friend WithEvents Mannschaft As DataGridViewTextBoxColumn
    Friend WithEvents Gegner As DataGridViewTextBoxColumn
    Friend WithEvents Reissen As DataGridViewTextBoxColumn
    Friend WithEvents Stossen As DataGridViewTextBoxColumn
    Friend WithEvents Gesamt As DataGridViewTextBoxColumn
    Friend WithEvents Punkte As DataGridViewTextBoxColumn
    Friend WithEvents Punkte2 As DataGridViewTextBoxColumn
    Friend WithEvents Platz As DataGridViewTextBoxColumn
End Class
