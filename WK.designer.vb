﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWettkampf
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Bezeichnung_3Label As System.Windows.Forms.Label
        Dim Bezeichnung_2Label As System.Windows.Forms.Label
        Dim Bezeichnung_1Label As System.Windows.Forms.Label
        Dim OrtLabel As System.Windows.Forms.Label
        Dim VeranstalterLabel As System.Windows.Forms.Label
        Dim DatumLabel As System.Windows.Forms.Label
        Dim ModusLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboOrt = New System.Windows.Forms.ComboBox()
        Me.cboBezeichnung3 = New System.Windows.Forms.ComboBox()
        Me.cboBezeichnung2 = New System.Windows.Forms.ComboBox()
        Me.cboBezeichnung1 = New System.Windows.Forms.ComboBox()
        Me.cboVeranstalter = New System.Windows.Forms.ComboBox()
        Me.cboDatum = New System.Windows.Forms.DateTimePicker()
        Me.cboModus = New System.Windows.Forms.ComboBox()
        Me.txtStartgeld = New System.Windows.Forms.TextBox()
        Me.lstAthletik = New System.Windows.Forms.CheckedListBox()
        Me.cboMeldeschluss = New System.Windows.Forms.DateTimePicker()
        Me.cboMeldeschlussNM = New System.Windows.Forms.DateTimePicker()
        Me.txtStartgeld_NM = New System.Windows.Forms.TextBox()
        Me.chkMannschaft = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboKR = New System.Windows.Forms.ComboBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMeldungen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKonflikte = New System.Windows.Forms.ToolStripMenuItem()
        Me.cbo2 = New System.Windows.Forms.ComboBox()
        Me.cbo1 = New System.Windows.Forms.ComboBox()
        Me.btnModus = New System.Windows.Forms.Button()
        Me.btnVerein = New System.Windows.Forms.Button()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.lbl2 = New System.Windows.Forms.Label()
        Me.pnlMannschaft = New System.Windows.Forms.Panel()
        Me.chkBigdisc = New System.Windows.Forms.CheckBox()
        Me.txtStartgeld_M = New System.Windows.Forms.TextBox()
        Me.pnlMasters = New System.Windows.Forms.GroupBox()
        Me.txtLeistung_m = New System.Windows.Forms.TextBox()
        Me.txtLeistung_w = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.updTeam_min = New System.Windows.Forms.NumericUpDown()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.updTeam = New System.Windows.Forms.NumericUpDown()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.updAK_m = New System.Windows.Forms.NumericUpDown()
        Me.updAK_w = New System.Windows.Forms.NumericUpDown()
        Me.lblDetails2 = New System.Windows.Forms.Label()
        Me.lblDetails1 = New System.Windows.Forms.Label()
        Me.pnlAthletik = New System.Windows.Forms.GroupBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboDatumBis = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtSteigerung2 = New System.Windows.Forms.TextBox()
        Me.txtSteigerung1 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.chkFlagge = New System.Windows.Forms.CheckBox()
        Bezeichnung_3Label = New System.Windows.Forms.Label()
        Bezeichnung_2Label = New System.Windows.Forms.Label()
        Bezeichnung_1Label = New System.Windows.Forms.Label()
        OrtLabel = New System.Windows.Forms.Label()
        VeranstalterLabel = New System.Windows.Forms.Label()
        DatumLabel = New System.Windows.Forms.Label()
        ModusLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.pnlMannschaft.SuspendLayout()
        Me.pnlMasters.SuspendLayout()
        CType(Me.updTeam_min, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.updTeam, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.updAK_m, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.updAK_w, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAthletik.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Bezeichnung_3Label
        '
        Bezeichnung_3Label.AutoSize = True
        Bezeichnung_3Label.Location = New System.Drawing.Point(27, 227)
        Bezeichnung_3Label.Name = "Bezeichnung_3Label"
        Bezeichnung_3Label.Size = New System.Drawing.Size(78, 13)
        Bezeichnung_3Label.TabIndex = 24
        Bezeichnung_3Label.Text = "Bezeichnung &3"
        '
        'Bezeichnung_2Label
        '
        Bezeichnung_2Label.AutoSize = True
        Bezeichnung_2Label.Location = New System.Drawing.Point(27, 196)
        Bezeichnung_2Label.Name = "Bezeichnung_2Label"
        Bezeichnung_2Label.Size = New System.Drawing.Size(78, 13)
        Bezeichnung_2Label.TabIndex = 22
        Bezeichnung_2Label.Text = "Bezeichnung &2"
        '
        'Bezeichnung_1Label
        '
        Bezeichnung_1Label.AutoSize = True
        Bezeichnung_1Label.Location = New System.Drawing.Point(27, 165)
        Bezeichnung_1Label.Name = "Bezeichnung_1Label"
        Bezeichnung_1Label.Size = New System.Drawing.Size(78, 13)
        Bezeichnung_1Label.TabIndex = 20
        Bezeichnung_1Label.Text = "Bezeichnung &1"
        '
        'OrtLabel
        '
        OrtLabel.AutoSize = True
        OrtLabel.Location = New System.Drawing.Point(16, 134)
        OrtLabel.Name = "OrtLabel"
        OrtLabel.Size = New System.Drawing.Size(89, 13)
        OrtLabel.TabIndex = 18
        OrtLabel.Text = "Veranstaltungs&ort"
        '
        'VeranstalterLabel
        '
        VeranstalterLabel.AutoSize = True
        VeranstalterLabel.Location = New System.Drawing.Point(42, 42)
        VeranstalterLabel.Name = "VeranstalterLabel"
        VeranstalterLabel.Size = New System.Drawing.Size(63, 13)
        VeranstalterLabel.TabIndex = 10
        VeranstalterLabel.Text = "&Veranstalter"
        '
        'DatumLabel
        '
        DatumLabel.AutoSize = True
        DatumLabel.Location = New System.Drawing.Point(67, 104)
        DatumLabel.Name = "DatumLabel"
        DatumLabel.Size = New System.Drawing.Size(38, 13)
        DatumLabel.TabIndex = 16
        DatumLabel.Text = "&Datum"
        '
        'ModusLabel
        '
        ModusLabel.AutoSize = True
        ModusLabel.Location = New System.Drawing.Point(66, 73)
        ModusLabel.Name = "ModusLabel"
        ModusLabel.Size = New System.Drawing.Size(39, 13)
        ModusLabel.TabIndex = 13
        ModusLabel.Text = "&Modus"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(225, 258)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(49, 13)
        Label2.TabIndex = 37
        Label2.Text = "&Startgeld"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Location = New System.Drawing.Point(34, 258)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(71, 13)
        Label8.TabIndex = 35
        Label8.Text = "&Meldeschluss"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Location = New System.Drawing.Point(12, 288)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(96, 13)
        Label9.TabIndex = 39
        Label9.Text = "&Nachmeldeschluss"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Location = New System.Drawing.Point(225, 288)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(49, 13)
        Label10.TabIndex = 41
        Label10.Text = "St&artgeld"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Location = New System.Drawing.Point(225, 318)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(49, 13)
        Label4.TabIndex = 119
        Label4.Text = "Sta&rtgeld"
        '
        'txtID
        '
        Me.txtID.Enabled = False
        Me.txtID.Location = New System.Drawing.Point(432, 101)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(62, 20)
        Me.txtID.TabIndex = 111
        Me.txtID.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(408, 104)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 13)
        Me.Label1.TabIndex = 110
        Me.Label1.Text = "ID"
        Me.Label1.Visible = False
        '
        'cboOrt
        '
        Me.cboOrt.DisplayMember = "Ort"
        Me.cboOrt.FormattingEnabled = True
        Me.cboOrt.Location = New System.Drawing.Point(111, 131)
        Me.cboOrt.Name = "cboOrt"
        Me.cboOrt.Size = New System.Drawing.Size(383, 21)
        Me.cboOrt.TabIndex = 19
        '
        'cboBezeichnung3
        '
        Me.cboBezeichnung3.DisplayMember = "Bezeichnung3"
        Me.cboBezeichnung3.FormattingEnabled = True
        Me.cboBezeichnung3.Location = New System.Drawing.Point(111, 224)
        Me.cboBezeichnung3.Name = "cboBezeichnung3"
        Me.cboBezeichnung3.Size = New System.Drawing.Size(383, 21)
        Me.cboBezeichnung3.TabIndex = 25
        '
        'cboBezeichnung2
        '
        Me.cboBezeichnung2.DisplayMember = "Bezeichnung2"
        Me.cboBezeichnung2.FormattingEnabled = True
        Me.cboBezeichnung2.Location = New System.Drawing.Point(111, 193)
        Me.cboBezeichnung2.Name = "cboBezeichnung2"
        Me.cboBezeichnung2.Size = New System.Drawing.Size(383, 21)
        Me.cboBezeichnung2.TabIndex = 23
        '
        'cboBezeichnung1
        '
        Me.cboBezeichnung1.DisplayMember = "Bezeichnung1"
        Me.cboBezeichnung1.FormattingEnabled = True
        Me.cboBezeichnung1.Location = New System.Drawing.Point(111, 162)
        Me.cboBezeichnung1.Name = "cboBezeichnung1"
        Me.cboBezeichnung1.Size = New System.Drawing.Size(383, 21)
        Me.cboBezeichnung1.TabIndex = 21
        '
        'cboVeranstalter
        '
        Me.cboVeranstalter.DisplayMember = "Vereinsname"
        Me.cboVeranstalter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVeranstalter.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboVeranstalter.FormattingEnabled = True
        Me.cboVeranstalter.Location = New System.Drawing.Point(111, 39)
        Me.cboVeranstalter.Name = "cboVeranstalter"
        Me.cboVeranstalter.Size = New System.Drawing.Size(343, 21)
        Me.cboVeranstalter.TabIndex = 11
        '
        'cboDatum
        '
        Me.cboDatum.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.cboDatum.Location = New System.Drawing.Point(111, 101)
        Me.cboDatum.Name = "cboDatum"
        Me.cboDatum.Size = New System.Drawing.Size(97, 20)
        Me.cboDatum.TabIndex = 17
        '
        'cboModus
        '
        Me.cboModus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboModus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboModus.DisplayMember = "Bezeichnung"
        Me.cboModus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboModus.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboModus.Location = New System.Drawing.Point(111, 70)
        Me.cboModus.Name = "cboModus"
        Me.cboModus.Size = New System.Drawing.Size(343, 21)
        Me.cboModus.TabIndex = 14
        Me.cboModus.ValueMember = "ID"
        '
        'txtStartgeld
        '
        Me.txtStartgeld.Location = New System.Drawing.Point(280, 255)
        Me.txtStartgeld.Name = "txtStartgeld"
        Me.txtStartgeld.Size = New System.Drawing.Size(55, 20)
        Me.txtStartgeld.TabIndex = 38
        Me.txtStartgeld.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lstAthletik
        '
        Me.lstAthletik.CheckOnClick = True
        Me.lstAthletik.FormattingEnabled = True
        Me.lstAthletik.Location = New System.Drawing.Point(13, 23)
        Me.lstAthletik.Name = "lstAthletik"
        Me.lstAthletik.Size = New System.Drawing.Size(141, 154)
        Me.lstAthletik.Sorted = True
        Me.lstAthletik.TabIndex = 76
        '
        'cboMeldeschluss
        '
        Me.cboMeldeschluss.CustomFormat = " "
        Me.cboMeldeschluss.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.cboMeldeschluss.Location = New System.Drawing.Point(111, 255)
        Me.cboMeldeschluss.Name = "cboMeldeschluss"
        Me.cboMeldeschluss.Size = New System.Drawing.Size(97, 20)
        Me.cboMeldeschluss.TabIndex = 36
        '
        'cboMeldeschlussNM
        '
        Me.cboMeldeschlussNM.CustomFormat = " "
        Me.cboMeldeschlussNM.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.cboMeldeschlussNM.Location = New System.Drawing.Point(111, 285)
        Me.cboMeldeschlussNM.Name = "cboMeldeschlussNM"
        Me.cboMeldeschlussNM.Size = New System.Drawing.Size(97, 20)
        Me.cboMeldeschlussNM.TabIndex = 40
        '
        'txtStartgeld_NM
        '
        Me.txtStartgeld_NM.Location = New System.Drawing.Point(280, 285)
        Me.txtStartgeld_NM.Name = "txtStartgeld_NM"
        Me.txtStartgeld_NM.Size = New System.Drawing.Size(55, 20)
        Me.txtStartgeld_NM.TabIndex = 42
        Me.txtStartgeld_NM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkMannschaft
        '
        Me.chkMannschaft.AutoSize = True
        Me.chkMannschaft.Location = New System.Drawing.Point(111, 439)
        Me.chkMannschaft.Name = "chkMannschaft"
        Me.chkMannschaft.Size = New System.Drawing.Size(142, 17)
        Me.chkMannschaft.TabIndex = 70
        Me.chkMannschaft.Text = "Mannschafts-&Wettkampf"
        Me.chkMannschaft.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(39, 352)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 13)
        Me.Label3.TabIndex = 45
        Me.Label3.Text = "&Kampfrichter"
        '
        'cboKR
        '
        Me.cboKR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboKR.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboKR.FormattingEnabled = True
        Me.cboKR.Items.AddRange(New Object() {"1", "3"})
        Me.cboKR.Location = New System.Drawing.Point(111, 345)
        Me.cboKR.Name = "cboKR"
        Me.cboKR.Size = New System.Drawing.Size(31, 21)
        Me.cboKR.TabIndex = 46
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuOptionen})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(706, 24)
        Me.MenuStrip1.TabIndex = 114
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSpeichern, Me.ToolStripMenuItem1, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 20)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuSpeichern
        '
        Me.mnuSpeichern.Name = "mnuSpeichern"
        Me.mnuSpeichern.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSpeichern.Size = New System.Drawing.Size(168, 22)
        Me.mnuSpeichern.Text = "&Speichern"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(165, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(168, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMeldungen, Me.mnuKonflikte})
        Me.mnuOptionen.Name = "mnuOptionen"
        Me.mnuOptionen.Size = New System.Drawing.Size(69, 20)
        Me.mnuOptionen.Text = "&Optionen"
        '
        'mnuMeldungen
        '
        Me.mnuMeldungen.CheckOnClick = True
        Me.mnuMeldungen.Name = "mnuMeldungen"
        Me.mnuMeldungen.Size = New System.Drawing.Size(210, 22)
        Me.mnuMeldungen.Text = "&Meldungen ausblenden"
        '
        'mnuKonflikte
        '
        Me.mnuKonflikte.Checked = True
        Me.mnuKonflikte.CheckOnClick = True
        Me.mnuKonflikte.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuKonflikte.Name = "mnuKonflikte"
        Me.mnuKonflikte.Size = New System.Drawing.Size(210, 22)
        Me.mnuKonflikte.Text = "bei &Konflikten nachfragen"
        '
        'cbo2
        '
        Me.cbo2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbo2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbo2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbo2.Location = New System.Drawing.Point(50, 35)
        Me.cbo2.Name = "cbo2"
        Me.cbo2.Size = New System.Drawing.Size(180, 21)
        Me.cbo2.TabIndex = 102
        '
        'cbo1
        '
        Me.cbo1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbo1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbo1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbo1.Location = New System.Drawing.Point(50, 8)
        Me.cbo1.Name = "cbo1"
        Me.cbo1.Size = New System.Drawing.Size(180, 21)
        Me.cbo1.TabIndex = 100
        '
        'btnModus
        '
        Me.btnModus.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnModus.Image = Global.Gewichtheben.My.Resources.Resources.Zoom16
        Me.btnModus.Location = New System.Drawing.Point(459, 68)
        Me.btnModus.Name = "btnModus"
        Me.btnModus.Size = New System.Drawing.Size(35, 24)
        Me.btnModus.TabIndex = 15
        Me.btnModus.UseVisualStyleBackColor = True
        '
        'btnVerein
        '
        Me.btnVerein.Image = Global.Gewichtheben.My.Resources.Resources.Zoom16
        Me.btnVerein.Location = New System.Drawing.Point(459, 37)
        Me.btnVerein.Name = "btnVerein"
        Me.btnVerein.Size = New System.Drawing.Size(35, 24)
        Me.btnVerein.TabIndex = 12
        Me.btnVerein.UseVisualStyleBackColor = True
        '
        'lbl1
        '
        Me.lbl1.Location = New System.Drawing.Point(2, 2)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(44, 33)
        Me.lbl1.TabIndex = 101
        Me.lbl1.Text = "Heim"
        Me.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl2
        '
        Me.lbl2.Location = New System.Drawing.Point(2, 29)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(44, 33)
        Me.lbl2.TabIndex = 102
        Me.lbl2.Text = "Gast 1"
        Me.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnlMannschaft
        '
        Me.pnlMannschaft.AutoScroll = True
        Me.pnlMannschaft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMannschaft.Controls.Add(Me.cbo2)
        Me.pnlMannschaft.Controls.Add(Me.lbl2)
        Me.pnlMannschaft.Controls.Add(Me.lbl1)
        Me.pnlMannschaft.Controls.Add(Me.cbo1)
        Me.pnlMannschaft.Location = New System.Drawing.Point(111, 462)
        Me.pnlMannschaft.Name = "pnlMannschaft"
        Me.pnlMannschaft.Size = New System.Drawing.Size(255, 64)
        Me.pnlMannschaft.TabIndex = 71
        '
        'chkBigdisc
        '
        Me.chkBigdisc.AutoSize = True
        Me.chkBigdisc.Checked = True
        Me.chkBigdisc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBigdisc.Location = New System.Drawing.Point(111, 386)
        Me.chkBigdisc.Name = "chkBigdisc"
        Me.chkBigdisc.Size = New System.Drawing.Size(170, 17)
        Me.chkBigdisc.TabIndex = 48
        Me.chkBigdisc.Text = "große Scheiben (2,5 und 5 kg)"
        Me.chkBigdisc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkBigdisc.UseVisualStyleBackColor = True
        '
        'txtStartgeld_M
        '
        Me.txtStartgeld_M.Location = New System.Drawing.Point(280, 315)
        Me.txtStartgeld_M.Name = "txtStartgeld_M"
        Me.txtStartgeld_M.Size = New System.Drawing.Size(55, 20)
        Me.txtStartgeld_M.TabIndex = 120
        Me.txtStartgeld_M.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'pnlMasters
        '
        Me.pnlMasters.Controls.Add(Me.txtLeistung_m)
        Me.pnlMasters.Controls.Add(Me.txtLeistung_w)
        Me.pnlMasters.Controls.Add(Me.Label14)
        Me.pnlMasters.Controls.Add(Me.Label13)
        Me.pnlMasters.Controls.Add(Me.updTeam_min)
        Me.pnlMasters.Controls.Add(Me.Label7)
        Me.pnlMasters.Controls.Add(Me.updTeam)
        Me.pnlMasters.Controls.Add(Me.Label11)
        Me.pnlMasters.Controls.Add(Me.updAK_m)
        Me.pnlMasters.Controls.Add(Me.updAK_w)
        Me.pnlMasters.Controls.Add(Me.lblDetails2)
        Me.pnlMasters.Controls.Add(Me.lblDetails1)
        Me.pnlMasters.Location = New System.Drawing.Point(521, 39)
        Me.pnlMasters.Name = "pnlMasters"
        Me.pnlMasters.Size = New System.Drawing.Size(170, 190)
        Me.pnlMasters.TabIndex = 121
        Me.pnlMasters.TabStop = False
        Me.pnlMasters.Text = "De&tails"
        '
        'txtLeistung_m
        '
        Me.txtLeistung_m.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLeistung_m.Location = New System.Drawing.Point(115, 155)
        Me.txtLeistung_m.Name = "txtLeistung_m"
        Me.txtLeistung_m.Size = New System.Drawing.Size(39, 20)
        Me.txtLeistung_m.TabIndex = 12
        Me.txtLeistung_m.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtLeistung_w
        '
        Me.txtLeistung_w.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLeistung_w.Location = New System.Drawing.Point(115, 133)
        Me.txtLeistung_w.Name = "txtLeistung_w"
        Me.txtLeistung_w.Size = New System.Drawing.Size(39, 20)
        Me.txtLeistung_w.TabIndex = 11
        Me.txtLeistung_w.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(10, 158)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(87, 13)
        Me.Label14.TabIndex = 10
        Me.Label14.Text = "ZK - Leistung - m"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(10, 136)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(87, 13)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "ZK - Leistung - w"
        '
        'updTeam_min
        '
        Me.updTeam_min.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.updTeam_min.Location = New System.Drawing.Point(115, 99)
        Me.updTeam_min.Name = "updTeam_min"
        Me.updTeam_min.Size = New System.Drawing.Size(39, 20)
        Me.updTeam_min.TabIndex = 8
        Me.updTeam_min.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.updTeam_min.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(81, 101)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(26, 13)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "min."
        '
        'updTeam
        '
        Me.updTeam.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.updTeam.Location = New System.Drawing.Point(115, 77)
        Me.updTeam.Name = "updTeam"
        Me.updTeam.Size = New System.Drawing.Size(39, 20)
        Me.updTeam.TabIndex = 5
        Me.updTeam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.updTeam.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(10, 79)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(97, 13)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Mannschaftsstärke"
        '
        'updAK_m
        '
        Me.updAK_m.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.updAK_m.Location = New System.Drawing.Point(115, 44)
        Me.updAK_m.Name = "updAK_m"
        Me.updAK_m.Size = New System.Drawing.Size(39, 20)
        Me.updAK_m.TabIndex = 3
        Me.updAK_m.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.updAK_m.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'updAK_w
        '
        Me.updAK_w.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.updAK_w.Location = New System.Drawing.Point(115, 22)
        Me.updAK_w.Name = "updAK_w"
        Me.updAK_w.Size = New System.Drawing.Size(39, 20)
        Me.updAK_w.TabIndex = 2
        Me.updAK_w.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.updAK_w.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'lblDetails2
        '
        Me.lblDetails2.AutoSize = True
        Me.lblDetails2.Location = New System.Drawing.Point(10, 46)
        Me.lblDetails2.Name = "lblDetails2"
        Me.lblDetails2.Size = New System.Drawing.Size(86, 13)
        Me.lblDetails2.TabIndex = 1
        Me.lblDetails2.Text = "Altersklassen - m"
        '
        'lblDetails1
        '
        Me.lblDetails1.AutoSize = True
        Me.lblDetails1.Location = New System.Drawing.Point(10, 24)
        Me.lblDetails1.Name = "lblDetails1"
        Me.lblDetails1.Size = New System.Drawing.Size(86, 13)
        Me.lblDetails1.TabIndex = 0
        Me.lblDetails1.Text = "Altersklassen - w"
        '
        'pnlAthletik
        '
        Me.pnlAthletik.Controls.Add(Me.lstAthletik)
        Me.pnlAthletik.Enabled = False
        Me.pnlAthletik.Location = New System.Drawing.Point(521, 349)
        Me.pnlAthletik.Name = "pnlAthletik"
        Me.pnlAthletik.Size = New System.Drawing.Size(170, 190)
        Me.pnlAthletik.TabIndex = 122
        Me.pnlAthletik.TabStop = False
        Me.pnlAthletik.Text = "&Athletik"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(42, 318)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(63, 13)
        Me.Label12.TabIndex = 123
        Me.Label12.Text = "Mannschaft"
        '
        'cboDatumBis
        '
        Me.cboDatumBis.CustomFormat = " "
        Me.cboDatumBis.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.cboDatumBis.Location = New System.Drawing.Point(237, 101)
        Me.cboDatumBis.Name = "cboDatumBis"
        Me.cboDatumBis.Size = New System.Drawing.Size(97, 20)
        Me.cboDatumBis.TabIndex = 124
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(214, 104)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(20, 13)
        Me.Label5.TabIndex = 125
        Me.Label5.Text = "bis"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnAdd
        '
        Me.btnAdd.Image = Global.Gewichtheben.My.Resources.Resources.Add
        Me.btnAdd.Location = New System.Drawing.Point(372, 496)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(25, 25)
        Me.btnAdd.TabIndex = 2
        Me.btnAdd.TabStop = False
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.Image = Global.Gewichtheben.My.Resources.Resources.Delete21
        Me.btnRemove.Location = New System.Drawing.Point(399, 496)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(25, 25)
        Me.btnRemove.TabIndex = 117
        Me.btnRemove.TabStop = False
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtSteigerung2)
        Me.GroupBox1.Controls.Add(Me.txtSteigerung1)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(521, 247)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(170, 84)
        Me.GroupBox1.TabIndex = 126
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Steigerung"
        '
        'txtSteigerung2
        '
        Me.txtSteigerung2.Location = New System.Drawing.Point(101, 52)
        Me.txtSteigerung2.Name = "txtSteigerung2"
        Me.txtSteigerung2.Size = New System.Drawing.Size(20, 20)
        Me.txtSteigerung2.TabIndex = 79
        Me.txtSteigerung2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSteigerung1
        '
        Me.txtSteigerung1.Location = New System.Drawing.Point(101, 23)
        Me.txtSteigerung1.Name = "txtSteigerung1"
        Me.txtSteigerung1.Size = New System.Drawing.Size(20, 20)
        Me.txtSteigerung1.TabIndex = 76
        Me.txtSteigerung1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(10, 26)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(133, 13)
        Me.Label15.TabIndex = 77
        Me.Label15.Text = "nach 1. Versuch            kg"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 55)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(133, 13)
        Me.Label6.TabIndex = 78
        Me.Label6.Text = "nach 2. Versuch            kg"
        '
        'chkFlagge
        '
        Me.chkFlagge.AutoSize = True
        Me.chkFlagge.Location = New System.Drawing.Point(347, 380)
        Me.chkFlagge.Name = "chkFlagge"
        Me.chkFlagge.Size = New System.Drawing.Size(104, 17)
        Me.chkFlagge.TabIndex = 127
        Me.chkFlagge.Text = "Flagge anzeigen"
        Me.chkFlagge.UseVisualStyleBackColor = True
        '
        'frmWettkampf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 554)
        Me.Controls.Add(Me.chkFlagge)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cboDatumBis)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.pnlMasters)
        Me.Controls.Add(Me.pnlAthletik)
        Me.Controls.Add(Me.txtStartgeld_M)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Me.chkBigdisc)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.pnlMannschaft)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.chkMannschaft)
        Me.Controls.Add(Me.cboKR)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Label9)
        Me.Controls.Add(Me.cboMeldeschlussNM)
        Me.Controls.Add(Me.txtStartgeld_NM)
        Me.Controls.Add(Label10)
        Me.Controls.Add(Label8)
        Me.Controls.Add(Me.cboMeldeschluss)
        Me.Controls.Add(Me.txtStartgeld)
        Me.Controls.Add(Me.btnModus)
        Me.Controls.Add(Me.btnVerein)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboOrt)
        Me.Controls.Add(Bezeichnung_3Label)
        Me.Controls.Add(Me.cboBezeichnung3)
        Me.Controls.Add(Bezeichnung_2Label)
        Me.Controls.Add(Me.cboBezeichnung2)
        Me.Controls.Add(Bezeichnung_1Label)
        Me.Controls.Add(Me.cboBezeichnung1)
        Me.Controls.Add(OrtLabel)
        Me.Controls.Add(VeranstalterLabel)
        Me.Controls.Add(Me.cboVeranstalter)
        Me.Controls.Add(DatumLabel)
        Me.Controls.Add(Me.cboDatum)
        Me.Controls.Add(ModusLabel)
        Me.Controls.Add(Me.cboModus)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWettkampf"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Neuen Wettkampf anlegen"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.pnlMannschaft.ResumeLayout(False)
        Me.pnlMasters.ResumeLayout(False)
        Me.pnlMasters.PerformLayout()
        CType(Me.updTeam_min, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.updTeam, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.updAK_m, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.updAK_w, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAthletik.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboOrt As System.Windows.Forms.ComboBox
    Friend WithEvents cboBezeichnung3 As System.Windows.Forms.ComboBox
    Friend WithEvents cboBezeichnung2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboBezeichnung1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboVeranstalter As System.Windows.Forms.ComboBox
    Friend WithEvents cboDatum As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboModus As System.Windows.Forms.ComboBox
    Friend WithEvents btnVerein As System.Windows.Forms.Button
    Friend WithEvents btnModus As System.Windows.Forms.Button
    Friend WithEvents txtStartgeld As System.Windows.Forms.TextBox
    Friend WithEvents lstAthletik As System.Windows.Forms.CheckedListBox
    Friend WithEvents cboMeldeschluss As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboMeldeschlussNM As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtStartgeld_NM As System.Windows.Forms.TextBox
    Friend WithEvents chkMannschaft As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboKR As System.Windows.Forms.ComboBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDatei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeichern As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuBeenden As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cbo2 As System.Windows.Forms.ComboBox
    Friend WithEvents cbo1 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl2 As System.Windows.Forms.Label
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents pnlMannschaft As System.Windows.Forms.Panel
    Friend WithEvents mnuOptionen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMeldungen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuKonflikte As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkBigdisc As System.Windows.Forms.CheckBox
    Friend WithEvents txtStartgeld_M As TextBox
    Friend WithEvents pnlMasters As GroupBox
    Friend WithEvents txtLeistung_m As TextBox
    Friend WithEvents txtLeistung_w As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents updTeam_min As NumericUpDown
    Friend WithEvents Label7 As Label
    Friend WithEvents updTeam As NumericUpDown
    Friend WithEvents Label11 As Label
    Friend WithEvents updAK_m As NumericUpDown
    Friend WithEvents updAK_w As NumericUpDown
    Friend WithEvents lblDetails2 As Label
    Friend WithEvents lblDetails1 As Label
    Friend WithEvents pnlAthletik As GroupBox
    Friend WithEvents Label12 As Label
    Friend WithEvents cboDatumBis As DateTimePicker
    Friend WithEvents Label5 As Label
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnRemove As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtSteigerung2 As TextBox
    Friend WithEvents txtSteigerung1 As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents chkFlagge As CheckBox
End Class
