﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAthletik
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAuswertung = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuLaufkarten = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuImport = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLayout = New System.Windows.Forms.ToolStripMenuItem()
        Me.cboGruppe = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dgvJoin = New System.Windows.Forms.DataGridView()
        Me.cboDisziplinen = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblDG1 = New System.Windows.Forms.Label()
        Me.txtDG1 = New System.Windows.Forms.TextBox()
        Me.lblEinheit = New System.Windows.Forms.Label()
        Me.txtDG2 = New System.Windows.Forms.TextBox()
        Me.lblDG2 = New System.Windows.Forms.Label()
        Me.txtDG3 = New System.Windows.Forms.TextBox()
        Me.lblDG3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnWeiter = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkMaxOnly = New System.Windows.Forms.CheckBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.bs = New System.Windows.Forms.BindingSource(Me.components)
        Me.MenuStrip1.SuspendLayout()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuOptionen})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(818, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSpeichern, Me.mnuAuswertung, Me.ToolStripMenuItem1, Me.mnuLaufkarten, Me.ToolStripMenuItem2, Me.mnuExport, Me.mnuImport, Me.ToolStripMenuItem3, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 20)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuSpeichern
        '
        Me.mnuSpeichern.Name = "mnuSpeichern"
        Me.mnuSpeichern.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSpeichern.Size = New System.Drawing.Size(182, 22)
        Me.mnuSpeichern.Text = "&Speichern"
        '
        'mnuAuswertung
        '
        Me.mnuAuswertung.Name = "mnuAuswertung"
        Me.mnuAuswertung.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.mnuAuswertung.Size = New System.Drawing.Size(182, 22)
        Me.mnuAuswertung.Text = "&Auswertung"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(179, 6)
        '
        'mnuLaufkarten
        '
        Me.mnuLaufkarten.Name = "mnuLaufkarten"
        Me.mnuLaufkarten.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.K), System.Windows.Forms.Keys)
        Me.mnuLaufkarten.Size = New System.Drawing.Size(182, 22)
        Me.mnuLaufkarten.Text = "Lauf&karten"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(179, 6)
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        Me.mnuExport.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.mnuExport.Size = New System.Drawing.Size(182, 22)
        Me.mnuExport.Text = "&Export"
        '
        'mnuImport
        '
        Me.mnuImport.Name = "mnuImport"
        Me.mnuImport.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.mnuImport.Size = New System.Drawing.Size(182, 22)
        Me.mnuImport.Text = "&Import"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(179, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(182, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLayout})
        Me.mnuOptionen.Name = "mnuOptionen"
        Me.mnuOptionen.Size = New System.Drawing.Size(69, 20)
        Me.mnuOptionen.Text = "&Optionen"
        '
        'mnuLayout
        '
        Me.mnuLayout.Name = "mnuLayout"
        Me.mnuLayout.Size = New System.Drawing.Size(160, 22)
        Me.mnuLayout.Text = "Tabellen-&Layout"
        '
        'cboGruppe
        '
        Me.cboGruppe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGruppe.FormattingEnabled = True
        Me.cboGruppe.Location = New System.Drawing.Point(62, 41)
        Me.cboGruppe.Name = "cboGruppe"
        Me.cboGruppe.Size = New System.Drawing.Size(51, 21)
        Me.cboGruppe.TabIndex = 2
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(17, 44)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(42, 17)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "&Gruppe"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label13.UseCompatibleTextRendering = True
        '
        'dgvJoin
        '
        Me.dgvJoin.AllowUserToAddRows = False
        Me.dgvJoin.AllowUserToDeleteRows = False
        Me.dgvJoin.AllowUserToResizeRows = False
        Me.dgvJoin.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvJoin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvJoin.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvJoin.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvJoin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvJoin.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvJoin.Location = New System.Drawing.Point(14, 75)
        Me.dgvJoin.MultiSelect = False
        Me.dgvJoin.Name = "dgvJoin"
        Me.dgvJoin.ReadOnly = True
        Me.dgvJoin.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvJoin.RowHeadersVisible = False
        Me.dgvJoin.RowHeadersWidth = 18
        Me.dgvJoin.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvJoin.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvJoin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvJoin.ShowCellErrors = False
        Me.dgvJoin.ShowEditingIcon = False
        Me.dgvJoin.ShowRowErrors = False
        Me.dgvJoin.Size = New System.Drawing.Size(599, 374)
        Me.dgvJoin.StandardTab = True
        Me.dgvJoin.TabIndex = 10
        '
        'cboDisziplinen
        '
        Me.cboDisziplinen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisziplinen.FormattingEnabled = True
        Me.cboDisziplinen.Location = New System.Drawing.Point(259, 41)
        Me.cboDisziplinen.Name = "cboDisziplinen"
        Me.cboDisziplinen.Size = New System.Drawing.Size(131, 21)
        Me.cboDisziplinen.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(209, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 17)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "&Disziplin"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label1.UseCompatibleTextRendering = True
        '
        'lblDG1
        '
        Me.lblDG1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblDG1.AutoSize = True
        Me.lblDG1.Location = New System.Drawing.Point(23, 67)
        Me.lblDG1.Name = "lblDG1"
        Me.lblDG1.Size = New System.Drawing.Size(34, 17)
        Me.lblDG1.TabIndex = 3
        Me.lblDG1.Text = "&1. DG"
        Me.lblDG1.UseCompatibleTextRendering = True
        '
        'txtDG1
        '
        Me.txtDG1.Location = New System.Drawing.Point(60, 64)
        Me.txtDG1.Name = "txtDG1"
        Me.txtDG1.Size = New System.Drawing.Size(46, 20)
        Me.txtDG1.TabIndex = 4
        Me.txtDG1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblEinheit
        '
        Me.lblEinheit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblEinheit.AutoSize = True
        Me.lblEinheit.BackColor = System.Drawing.SystemColors.Window
        Me.lblEinheit.Location = New System.Drawing.Point(69, 28)
        Me.lblEinheit.MinimumSize = New System.Drawing.Size(36, 20)
        Me.lblEinheit.Name = "lblEinheit"
        Me.lblEinheit.Padding = New System.Windows.Forms.Padding(3, 1, 4, 2)
        Me.lblEinheit.Size = New System.Drawing.Size(36, 20)
        Me.lblEinheit.TabIndex = 2
        Me.lblEinheit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtDG2
        '
        Me.txtDG2.Location = New System.Drawing.Point(60, 97)
        Me.txtDG2.Name = "txtDG2"
        Me.txtDG2.Size = New System.Drawing.Size(46, 20)
        Me.txtDG2.TabIndex = 6
        Me.txtDG2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblDG2
        '
        Me.lblDG2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblDG2.AutoSize = True
        Me.lblDG2.Location = New System.Drawing.Point(23, 100)
        Me.lblDG2.Name = "lblDG2"
        Me.lblDG2.Size = New System.Drawing.Size(34, 17)
        Me.lblDG2.TabIndex = 5
        Me.lblDG2.Text = "&2. DG"
        Me.lblDG2.UseCompatibleTextRendering = True
        '
        'txtDG3
        '
        Me.txtDG3.Location = New System.Drawing.Point(60, 130)
        Me.txtDG3.Name = "txtDG3"
        Me.txtDG3.Size = New System.Drawing.Size(46, 20)
        Me.txtDG3.TabIndex = 8
        Me.txtDG3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblDG3
        '
        Me.lblDG3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblDG3.AutoSize = True
        Me.lblDG3.Location = New System.Drawing.Point(23, 133)
        Me.lblDG3.Name = "lblDG3"
        Me.lblDG3.Size = New System.Drawing.Size(34, 17)
        Me.lblDG3.TabIndex = 7
        Me.lblDG3.Text = "&3. DG"
        Me.lblDG3.UseCompatibleTextRendering = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btnWeiter)
        Me.GroupBox1.Controls.Add(Me.lblDG1)
        Me.GroupBox1.Controls.Add(Me.txtDG1)
        Me.GroupBox1.Controls.Add(Me.txtDG3)
        Me.GroupBox1.Controls.Add(Me.lblDG3)
        Me.GroupBox1.Controls.Add(Me.lblEinheit)
        Me.GroupBox1.Controls.Add(Me.txtDG2)
        Me.GroupBox1.Controls.Add(Me.lblDG2)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(638, 68)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(159, 215)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        '
        'btnWeiter
        '
        Me.btnWeiter.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnWeiter.Enabled = False
        Me.btnWeiter.Location = New System.Drawing.Point(44, 171)
        Me.btnWeiter.Name = "btnWeiter"
        Me.btnWeiter.Size = New System.Drawing.Size(75, 25)
        Me.btnWeiter.TabIndex = 9
        Me.btnWeiter.Text = "&Weiter"
        Me.btnWeiter.UseCompatibleTextRendering = True
        Me.btnWeiter.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Einheit"
        Me.Label2.UseCompatibleTextRendering = True
        '
        'chkMaxOnly
        '
        Me.chkMaxOnly.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkMaxOnly.AutoSize = True
        Me.chkMaxOnly.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkMaxOnly.ForeColor = System.Drawing.Color.Firebrick
        Me.chkMaxOnly.Location = New System.Drawing.Point(642, 42)
        Me.chkMaxOnly.Name = "chkMaxOnly"
        Me.chkMaxOnly.Size = New System.Drawing.Size(155, 17)
        Me.chkMaxOnly.TabIndex = 208
        Me.chkMaxOnly.TabStop = False
        Me.chkMaxOnly.Text = "nur Maximal-Wert eingeben"
        Me.chkMaxOnly.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Enabled = False
        Me.btnSave.Location = New System.Drawing.Point(695, 390)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(107, 28)
        Me.btnSave.TabIndex = 100
        Me.btnSave.Text = "&Speichern"
        Me.btnSave.UseCompatibleTextRendering = True
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(695, 424)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(107, 28)
        Me.btnExit.TabIndex = 101
        Me.btnExit.Text = "&Beenden"
        Me.btnExit.UseCompatibleTextRendering = True
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'bs
        '
        Me.bs.AllowNew = False
        Me.bs.Sort = "Nachname, Vorname"
        '
        'frmAthletik
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(818, 464)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkMaxOnly)
        Me.Controls.Add(Me.cboDisziplinen)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvJoin)
        Me.Controls.Add(Me.cboGruppe)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(834, 502)
        Me.Name = "frmAthletik"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Athletik"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents mnuDatei As ToolStripMenuItem
    Friend WithEvents mnuSpeichern As ToolStripMenuItem
    Friend WithEvents mnuOptionen As ToolStripMenuItem
    Friend WithEvents mnuLayout As ToolStripMenuItem
    Friend WithEvents mnuAuswertung As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents mnuLaufkarten As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents mnuExport As ToolStripMenuItem
    Friend WithEvents mnuImport As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripSeparator
    Friend WithEvents mnuBeenden As ToolStripMenuItem
    Friend WithEvents cboGruppe As ComboBox
    Friend WithEvents Label13 As Label
    Friend WithEvents dgvJoin As DataGridView
    Friend WithEvents cboDisziplinen As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lblDG1 As Label
    Friend WithEvents txtDG1 As TextBox
    Friend WithEvents lblEinheit As Label
    Friend WithEvents txtDG2 As TextBox
    Friend WithEvents lblDG2 As Label
    Friend WithEvents txtDG3 As TextBox
    Friend WithEvents lblDG3 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnWeiter As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents chkMaxOnly As CheckBox
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents bs As BindingSource
End Class
