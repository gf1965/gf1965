﻿
Imports System.Math
Imports System.Runtime.InteropServices
Imports MySqlConnector

Module Globale

    Public dtResults As DataTable
    Public dvResults As DataView ' DataSource für Publikumsanzeige
    Public WithEvents bsResults As BindingSource
    Public dvLeader(1) As DataView ' DataSource für die Anzeigen
    Public dtLeader(1) As DataTable
    Public dtDelete(1) As DataTable
    Public dvDelete(1) As DataView

    Public dvNotizen As DataView
    'Public dvTeam As DataView

    Public WithEvents bsLeader As BindingSource
    Public WithEvents bsDelete As BindingSource
    Public WithEvents bsTeam As BindingSource
    Public WithEvents bsNotizen As BindingSource

    Public SexColor As Dictionary(Of String, Color)

    'Public multiResults As Dictionary(Of String, DataTable) ' Result-Tables bei Identities
    Public TeamCombinations As List(Of Object) ' Kombinationen der Mannschaften bei Finale

    'Public dicGKs_BySex_ByAK As Dictionary(Of String, Dictionary(Of Integer, SortedList(Of Integer, String)))

    Public Event LeaderGruppe_Changed()
    Public Event ReturnFromKorrektur(Args As Correction)

    'Private _rowcount As Integer
    'Public Event RowCount_Changed(RowCount As Integer)

    Public Event RowIndex_Changed(RowIndex As Integer)
    Public Event DataSource_Changed()

    Public Sub bsLeader_PositionChanged(sender As Object, e As EventArgs) Handles bsLeader.PositionChanged
        RaiseEvent RowIndex_Changed(bsLeader.Position)
    End Sub
    Public Sub Change_LeaderGruppe()
        RaiseEvent LeaderGruppe_Changed()
    End Sub
    Public Sub Update_From_Korrektur(Args As Correction)
        RaiseEvent ReturnFromKorrektur(Args)
    End Sub

    Public IsMySQL_Started As Boolean

    Enum ConnState
        Disconnected = 0 ' Error
        Connected = 1
        Available = 2
        UserClosed = 3
        NotAvailable = 4
    End Enum
    Enum Status ' aus BVDG-API
        Neu = 0                     ' Neuer Datensatz
        Ausgelesen = 1              ' Datensatz ausgelesen
        Änderung = 2                ' Datensatz wurde geändert
        Ergebnis = 3                ' Ergebnis eingetragen
        Abgeschlossen = 4           ' Datensatz ausgelesen und geschlossen
        Ergebnis_Nachmeldung = 5    ' Ergebnis einer Nachmeldung eingetragen
    End Enum

    Public Event AppConnectionChanged(Status As Integer)
    Private _AppConnState As Integer
    Public Property AppConnState As Integer
        Get
            Return _AppConnState
        End Get
        Set(value As Integer)
            _AppConnState = value
            RaiseEvent AppConnectionChanged(value)
        End Set
    End Property

    Public Class Combination
        Public a As Integer
        Public b As Integer
    End Class

    Public Glob As New myGlobal
    Class myGlobal
        Implements IDisposable
#Region "IDisposable Support"
        Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
                End If

                ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
                ' TODO: große Felder auf Null setzen.
            End If
            disposedValue = True
        End Sub

        ' TODO: Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
        'Protected Overrides Sub Finalize()
        '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
            Dispose(True)
            ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
            ' GC.SuppressFinalize(Me)
        End Sub
#End Region
        Public Sub New()
        End Sub

        Public SinclairFaktor As Dictionary(Of String, Double) 'der Faktor A in der Sinclair-Formel
        Public minKG As Dictionary(Of String, Double) 'für Sinclair-Formel
        Public maxKG As Dictionary(Of String, Double) 'für Sinclair-Formel
        Public maxSC As Dictionary(Of String, Double) 'maximaler Sinclair-Koeffizient

        Dim dtDisziplinen As DataTable      ' Formeln zur Berechnung der Athletik-Wertung            (wird durch `wettkampf_athletik_defaults` überschrieben)
        Dim dtFaktoren As DataTable
        Dim dtFormeln As DataTable
        Dim dtMannschaft As DataTable       ' Mannschaftswertung: Verein, Länder
        Dim dtMulti As DataTable
        Dim dtRelativ As DataTable

        Public dtRekorde As DataTable

        Public dtAG As DataTable
        Public dtAK As DataTable
        Public dtAlter As DataTable
        Public dtDefaults As DataTable      ' Defaults für die einzelnen AK's der Athletik-Wertung   (wird durch `wettkampf_athletik_defaults` überschrieben)
        Public dtGewicht As DataTable
        Public dtGK As DataTable
        Public dtModus As DataTable
        Public dtNotizen As DataTable
        Public dtPrognose As DataTable
        Public dtRestrict As DataTable
        Public dtTeams As DataTable         ' Mannschaften bei (Bundes)Liga-Wettkämpfen, speichert Punktestand
        Public dtWertung As DataTable
        Public dtWertung_Bezeichnung As DataTable
        Public dtWK As DataTable

        Public dvAG As DataView
        Public dvAK As DataView
        Public dvDefaults As DataView       ' merged athletik_defaults
        Public dvDisziplinen As DataView
        Public dvFormeln As DataView
        Public dvGewicht As DataView
        Public dvGK As DataView
        Public dvMannschaft As DataView
        Public dvRelativ As DataView
        Public dvRekorde As DataView
        'Public dvWertung As DataView
        'Public dvW2 As DataView

        Public Function Load_Global_Tables(CurrentForm As Form) As Boolean

            Dim query As String
            Dim cmd As MySqlCommand
            'Dim dtWK_Defaults As New DataTable
            'Do
            Using ds As New DataService
                Using conn As New MySqlConnection(User.ConnString)
                    Try
                        If Not conn.State = ConnectionState.Open Then conn.Open()

                        ' Tabelle relativabzug
                        dtRelativ = New DataTable
                        query = "Select * FROM relativabzug ORDER BY Gewicht;"
                        cmd = New MySqlCommand(query, conn)
                        dtRelativ.Load(cmd.ExecuteReader)

                        ' Tabelle alterskoeffizient
                        dtAlter = New DataTable
                        query = "Select * FROM alterskoeffizient ORDER BY idAlter;"
                        cmd = New MySqlCommand(query, conn)
                        dtAlter.Load(cmd.ExecuteReader)

                        ' Tabelle faktoren für Formeln Sinclair & S-M
                        dtFaktoren = New DataTable
                        query = "Select * FROM faktoren ORDER BY sex ASC;"
                        cmd = New MySqlCommand(query, conn)
                        dtFaktoren.Load(cmd.ExecuteReader)

                        ' Tabelle wertung
                        dtWertung = New DataTable
                        query = "Select * FROM wertung ORDER BY idWertung ASC;"
                        cmd = New MySqlCommand(query, conn)
                        dtWertung.Load(cmd.ExecuteReader)

                        ' Tabelle wertung_bezeichnung
                        dtWertung_Bezeichnung = New DataTable
                        query = "Select * FROM wertung_bezeichnung;"
                        cmd = New MySqlCommand(query, conn)
                        dtWertung_Bezeichnung.Load(cmd.ExecuteReader)

                        ' Tabelle Formeln
                        dtFormeln = New DataTable
                        query = "Select * FROM modus_formeln ORDER BY Modus, AK, Sex;"
                        cmd = New MySqlCommand(query, conn)
                        dtFormeln.Load(cmd.ExecuteReader)

                        ' Athletik-Disziplinen
                        dtDisziplinen = New DataTable
                        query = "Select * FROM athletik_disziplinen ORDER BY idDisziplin ASC;"
                        cmd = New MySqlCommand(query, conn)
                        dtDisziplinen.Load(cmd.ExecuteReader)

                        ' Athletik-Berechnung
                        dtDefaults = New DataTable
                        query = "SELECT * FROM athletik_defaults;"
                        cmd = New MySqlCommand(query, conn)
                        dtDefaults.Load(cmd.ExecuteReader)

                        ' Modus
                        dtModus = New DataTable
                        query = "SELECT * FROM modus ORDER BY idModus;"
                        cmd = New MySqlCommand(query, conn)
                        dtModus.Load(cmd.ExecuteReader)

                        ' Weltrekorde
                        'dtRobi = ds.Get_DataTable("records", conn)
                        ds.Set_Global_Data("Rekorde", conn)

                        ConnError = False
                    Catch ex As MySqlException
                        'If MySQl_Error(CurrentForm, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                        'If MySQl_Error(Nothing, "Global: Load_Global_Tables: DataService" & vbNewLine & ex.Message, ex.StackTrace, True) = DialogResult.Cancel Then Exit Do
                        MySQl_Error(Nothing, "Global: Load_Global_Tables: DataService: " & ex.Message, ex.StackTrace)
                    End Try
                End Using
            End Using
            'Loop While ConnError
            If IsNothing(ConnError) OrElse ConnError Then Return False

            dvDisziplinen = New DataView(dtDisziplinen)
            dvDisziplinen.Sort = "idDisziplin"

            dvFormeln = New DataView(dtFormeln)
            dvFormeln.Sort = "Modus, AK, Sex"

            dvRelativ = New DataView(dtRelativ)
            dvRelativ.Sort = "Gewicht"

            'dvWertung = New DataView(dtWertung)
            'dvWertung.Sort = "idWertung"

            'dvRobi = New DataView(dtRobi)
            'dvRobi.Sort = "sex, age_id, bw_id, lift"

            Try
                'Werte für Relativ-/Sinclair-Berechnung
                SinclairFaktor = New Dictionary(Of String, Double)
                minKG = New Dictionary(Of String, Double)
                maxKG = New Dictionary(Of String, Double)
                maxSC = New Dictionary(Of String, Double)
                SinclairFaktor.Add("m", CDbl(dtFaktoren(0)!Faktor))
                SinclairFaktor.Add("w", CDbl(dtFaktoren(1)!Faktor))
                minKG.Add("m", CDbl(dtFaktoren(0)!minKG))
                minKG.Add("w", CDbl(dtFaktoren(1)!minKG))
                maxKG.Add("m", CDbl(dtFaktoren(0)!maxKG))
                maxKG.Add("w", CDbl(dtFaktoren(1)!maxKG))
                maxSC.Add("m", CDbl(dtFaktoren(0)!maxSC))
                maxSC.Add("w", CDbl(dtFaktoren(1)!maxSC))

                SexColor = New Dictionary(Of String, Color)
                SexColor.Add("m", Color.FromArgb(0, 204, 255)) '219, 238, 255))
                SexColor.Add("w", Color.FromArgb(255, 102, 255)) '255, 230, 242))
            Catch ex As Exception
                MySQl_Error(Nothing, "Global: Load_Global_Tables: Dictionaries" & vbNewLine & ex.Message, ex.StackTrace, True)
                Return False
            End Try

            Return True
        End Function

        Public Function Load_Wettkampf_Tables(Form As Form, WK_Keys As Object()) As Boolean

            ' alle WK-spezifischen Tabellen einlesen / löschen

            If NoCompetition() Then
                If IsNothing(WK_Keys) Then WK_Keys = {"0"}
            Else
                WK_Keys = Wettkampf.Identities.Keys.ToArray
            End If

            Dim showProgress = Form.Text.Contains("bearbeiten")
            If showProgress Then formMain.Change_MainProgressBarValue(20)

            Dim query As String
            Dim cmd As MySqlCommand
            Dim dt_Athletik_Defaults As New DataTable
            'Dim WKs As New List(Of String)
            'If Not WK_ID = 0 Then WKs.Add(WK_ID.ToString)

            Do
                Using ds As New DataService
                    Using conn As New MySqlConnection(User.ConnString)
                        Try
                            If Not conn.State = ConnectionState.Open Then conn.Open()
#Region "Liga"
                            ds.Set_Teams(WK_Keys, conn)
#End Region
#Region "DataTables erstellen"
                            ' Wettkampf
                            dtWK = New DataTable
                            dtWK = ds.Get_Wettkampf(WK_Keys, conn)
                            'query = "Select w.Wettkampf, w.Datum, w.DatumBis, w.Kurz, b.Bez1, b.Bez2, b.Bez3, w.Ort, w.GUID " &
                            '"FROM wettkampf w LEFT JOIN wettkampf_bezeichnung b On w.Wettkampf = b.Wettkampf " &
                            '"WHERE " & Wettkampf.WhereClause("w", WK_Keys) & ";"
                            'cmd = New MySqlCommand(query, conn)
                            'dtWK.Load(cmd.ExecuteReader())

                            ' Altersklassen --> für WK laden, da International im WK festgelegt ist
                            dtAK = New DataTable
                            'query = "SELECT idAltersklasse idAK, Altersklasse AK, AK MastersAK, von, bis FROM altersklassen" & If(Wettkampf.International, "_international;", ";")
                            query = "SELECT idAltersklasse idAK, Altersklasse AK, AK MastersAK, von, bis FROM altersklassen WHERE International = " & Wettkampf.International & ";"
                            cmd = New MySqlCommand(query, conn)
                            dtAK.Load(cmd.ExecuteReader)

                            ' Gewichtsklassen
                            dtGK = New DataTable
                            query = "SELECT * FROM gewichtsklassen where International = " & Wettkampf.International & ";"
                            cmd = New MySqlCommand(query, conn)
                            dtGK.Load(cmd.ExecuteReader)

                            ' Athletik-Defaults
                            dt_Athletik_Defaults = New DataTable
                            query = "SELECT * FROM wettkampf_athletik_defaults WHERE " & Wettkampf.WhereClause(, WK_Keys) & ";"
                            cmd = New MySqlCommand(query, conn)
                            dt_Athletik_Defaults.Load(cmd.ExecuteReader)

#Region "Resultate --> alle TN mit attend = True oder attend = Null"
                            ' ==> alle Results aller Heber aus allen Wettkämpfen
                            ' ==> dvResults filtern, falls nur attend = True in Tabelle sein darf
                            dtResults = New DataTable
                            query = Get_QueryResults("Null", Wettkampf.Athletik)
                            cmd = New MySqlCommand(query, conn)
                            dtResults.Load(cmd.ExecuteReader())
#End Region
                            ' Altersgruppen-Zusammenfassung
                            dtAG = New DataTable
                            query = "SELECT Reihenfolge, Sex, Jahrgang FROM wettkampf_altersgruppen WHERE " & Wettkampf.WhereClause(, WK_Keys) & " ORDER BY Reihenfolge;"
                            cmd = New MySqlCommand(query, conn)
                            dtAG.Load(cmd.ExecuteReader)

                            ' Exceptions
                            dtRestrict = New DataTable
                            query = "SELECT Durchgang, Altersklasse, Versuche FROM wettkampf_restriction WHERE " & Wettkampf.WhereClause(, WK_Keys) & " ORDER BY Durchgang;"
                            cmd = New MySqlCommand(query, conn)
                            dtRestrict.Load(cmd.ExecuteReader)

                            ' Gewichtsgruppen-Wertung in Gewicht aufteilen
                            dtGewicht = New DataTable
                            query = "SELECT Wettkampf, Gruppe, GG, KG_von, KG_bis FROM wettkampf_gewichtsteiler WHERE " & Wettkampf.WhereClause(, WK_Keys) & ";"
                            cmd = New MySqlCommand(query, conn)
                            dtGewicht.Load(cmd.ExecuteReader)

                            ' Mannschafts-Wertungen für Auswertung (frmMain)
                            dtMannschaft = New DataTable
                            query = "SELECT idMannschaft, min, max, gewertet, m_w, multi, relativ FROM wettkampf_mannschaft WHERE " & Wettkampf.WhereClause(, WK_Keys) & ";"
                            cmd = New MySqlCommand(query, conn)
                            dtMannschaft.Load(cmd.ExecuteReader)

                            ' Notizen
                            dtNotizen = New DataTable
                            query = "SELECT m.Teilnehmer, n.Notiz FROM " &
                                "meldung m " &
                                "LEFT JOIN notizen n On n.Heber = m.Teilnehmer " &
                                "WHERE " & Wettkampf.WhereClause("m", WK_Keys) & ";"
                            cmd = New MySqlCommand(query, conn)
                            dtNotizen.Load(cmd.ExecuteReader)

                            '' AKs 2.Wertung
                            'dtW2 = New DataTable
                            'query = "select ak.idAltersklasse, ak.Altersklasse, wa.Wertung, w.Kurz " &
                            '"from altersklassen ak " &
                            '"left join (select * from wettkampf_altersklassen where " & Wettkampf.WhereClause(, WKs) & " And Geschlecht = '') wa on wa.idAK = ak.idAltersklasse " &
                            '"left join wertung w on w.idWertung = wa.Wertung " &
                            '"group by ak.Altersklasse " &
                            '"order by ak.idAltersklasse;"
                            'cmd = New MySqlCommand(query, conn)
                            'dtW2.Load(cmd.ExecuteReader)

                            ConnError = False
                        Catch ex As MySqlException
                            If MySQl_Error(Form, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                        End Try
                    End Using
                End Using
            Loop While ConnError

            If ConnError Then
                If showProgress Then formMain.Change_MainProgressBarValue(0)
                Return False
            End If
#End Region

            If showProgress Then formMain.Change_MainProgressBarValue(45)

            'dvTeam = New DataView(dtTeams)
            bsTeam = New BindingSource With {.DataSource = dtTeams}

            dvNotizen = New DataView(dtNotizen)
            bsNotizen = New BindingSource With {.DataSource = dvNotizen}

            dvGK = New DataView(dtGK)
            dvGK.Sort = "idAK, Sex, idGK"

            If showProgress Then formMain.Change_MainProgressBarValue(60)

            ' WK_ATHLETIK_DEF integrieren
            With dt_Athletik_Defaults
                .PrimaryKey = Nothing
                Dim cols2(2) As DataColumn
                cols2(0) = .Columns("Disziplin")
                cols2(1) = .Columns("Bezeichnung")
                cols2(2) = .Columns("Geschlecht")
                .PrimaryKey = cols2
                .Columns.Remove("Wettkampf")
            End With
            dtDefaults.Merge(dt_Athletik_Defaults, False)
            dvDefaults = New DataView(dtDefaults)
            dvDefaults.Sort = "Disziplin, Bezeichnung, Geschlecht"

            'dtAG.PrimaryKey = Nothing
            'dtAG.Columns("Reihenfolge").AllowDBNull = True
            dvAG = New DataView(dtAG)
            dvAG.Sort = "Reihenfolge"

            dvGewicht = New DataView(dtGewicht)
            dvGewicht.Sort = "Gruppe" ', GG"

            dvMannschaft = New DataView(dtMannschaft)
            dvMannschaft.Sort = "idMannschaft"

            'dvW2 = New DataView(dtW2)
            'dvW2.Sort = "idAltersklasse"

            formMain.mnuLänderwertung.Enabled = dvMannschaft.Count > 0 AndAlso Not dvMannschaft.Find(1) = -1
            formMain.mnuVereinswertung.Enabled = dvMannschaft.Count > 0 AndAlso Not dvMannschaft.Find(0) = -1
            'formMain.div1.Visible = formMain.mnuLänderwertung.Visible OrElse formMain.mnuVereinswertung.Visible

            If showProgress Then formMain.Change_MainProgressBarValue(80)

            Create_Results()

            If showProgress Then formMain.Change_MainProgressBarValue(100)
            Return True
        End Function

        Public Sub Create_Results()
            'Dim cols(0) As DataColumn
            'cols(0) = dtResults.Columns("Teilnehmer")
            'dtResults.PrimaryKey = cols

            ' Create Score Columns in Results
            Dim cols As New List(Of DataColumn)
            For Each col As DataColumn In dtResults.Columns
                If col.ColumnName.Contains("_Platz") Then
                    For Each Key In Wettkampf.Identities.Keys
                        cols.Add(New DataColumn With {
                                 .ColumnName = col.ColumnName & "_" & Key,
                                 .DataType = GetType(Integer)})
                    Next
                End If
            Next
            dtResults.Columns.AddRange(cols.ToArray)

            For Each row As DataRow In dtResults.Rows
                For Each col As DataColumn In dtResults.Columns
                    If col.ColumnName.Contains("_Platz_") Then Exit For
                    If col.ColumnName.Contains("_Platz") Then
                        Dim Scorings = Split(row(col.ColumnName).ToString(), ",").ToList()
                        Scorings.Remove(String.Empty)
                        For Each Key In Wettkampf.Identities.Keys
                            Dim ix = Split(row!Wettkampf.ToString(), ",").ToList().IndexOf(Key)
                            If ix > -1 AndAlso Scorings.Count > ix Then
                                row(col.ColumnName & "_" & Key) = Scorings(ix)
                            End If
                        Next
                    End If
                Next
            Next

            dvResults = New DataView(dtResults)

            ' Sortierung überprüfen: 
            ' --> Jahrgänge zusammenfassen, wenn gemeinsam gewertet
            ' --> Jahrgänge in AKs zusammenfassen bei Masters
            Dim Sort = String.Empty
            Try
                If Wettkampf.Modus > 0 AndAlso Wettkampf.Modus < 400 Then
                    If Wettkampf.Alterseinteilung.Equals("Jahrgang") Then
                        Sort = "Gruppe, Jahrgang, Startnummer"
                    ElseIf Wettkampf.Alterseinteilung.Equals("Altersgruppen") Then
                        Sort = "Gruppe, Startnummer"
                    End If
                ElseIf Wettkampf.Mannschaft Then
                    'Sort = "TeamId, Gruppe, Startnummer"
                    'Sort = "TeamID, Verein, Gruppe, Startnummer, Nachname, Vorname" ' anstatt nach Name müsste die Sortierung nach Startnummern erfolgen 
                    Sort = "TeamID, Gruppe, Startnummer, Nachname, Vorname"
                End If
                bsResults = New BindingSource With {
                    .DataSource = dvResults,
                    .Sort = Sort}
            Catch ex As Exception
            End Try
        End Sub

        Public Function Get_QueryResults(PlaceHolder As String, Optional Athletik As Boolean = False, Optional WK As String = "") As String

            ' ==>  Technik wird immer geladen
            ' ==>  Athletik wird nur bei Wettkampf.Athletik = True geladen

            Dim WKs As New List(Of String)
            If Not String.IsNullOrEmpty(WK) Then WKs.Add(WK)

            '"If(If(h.Geschlecht = 'm', rel.m, rel.w) = 0, round(m.Wiegen, 1), If(h.Geschlecht = 'm', rel.m, rel.w)) Relativ, " &

            Dim x = PlaceHolder
            Dim Query As String = String.Empty
            Dim dtA As New DataTable
            Dim Sel_Heben As String
            Dim Sel_A As String
            Dim Feldliste = "Select group_concat(m.Wettkampf ORDER BY m.Wettkampf)  Wettkampf, IfNull(wt.Id, 1000) TeamID, m.Teilnehmer, " & If(Wettkampf.Mannschaft, "IfNull(m.Gruppe, 1000)", "m.Gruppe") & " Gruppe, m.Losnummer, m.Startnummer, m.attend, m.OnlineStatus," &
                            "h.Nachname, h.Vorname, UCase(h.Nachname) Name1, concat(UCase(h.Nachname), ' ', h.Vorname) Name2, " &
                            "h.Jahrgang, h.Geschlecht Sex, v.idVerein, v.Vereinsname Verein, v.Kurzname VereinKurz, " &
                            "IfNull(reg.Kurz,reg.region_3) Verband, reg.region_flag, sta.state_iso3, sta.state_flag, m.Wiegen KG, m.idAK, m.AK, m.idGK, m.GK, m.a_k, wg.GG, " &
                            "
                            (select 
	                            case 
		                            when h.Geschlecht = 'm' then (
			                            select
				                            case 
					                            when m.Wiegen between 79.1 and 95.5 then m.Wiegen
                                                when m.Wiegen between 95.5 and 95.999 then 95.0 
					                            else (select m from relativabzug where Gewicht = ceil(round(m.Wiegen, 1)))
				                            end    
                                            from relativabzug Limit 1)
		                            else (select w from relativabzug where Gewicht = ceil(round(m.Wiegen, 1)))
	                            end) Relativ,
                            " &
                            "Round(If(m.Wiegen > f.minKG, If(m.Wiegen < f.maxKG, Round(Pow(10, (f.Faktor * Pow(Log10(m.Wiegen / f.maxKG), 2))), 4), 1), f.maxSC) * If(h.Geschlecht = 'm', wd.Faktor_m, wd.Faktor_w), 4) Sinclair, " &
                            "R_1, RW_1, RT_1, R_2, RW_2, RT_2, R_3, RW_3, RT_3, res.Reissen, res.Reissen_Zeit, group_concat(res.Reissen_Platz ORDER BY m.Wettkampf) Reissen_Platz, res.Reissen2, group_concat(res.Reissen2_Platz ORDER BY m.Wettkampf) Reissen2_Platz, " &
                            "S_1, SW_1, ST_1, S_2, SW_2, ST_2, S_3, SW_3, ST_3, res.Stossen, res.Stossen_Zeit, group_concat(res.Stossen_Platz ORDER BY m.Wettkampf) Stossen_Platz, res.Stossen2, group_concat(res.Stossen2_Platz ORDER BY m.Wettkampf) Stossen2_Platz, " &
                            "maxR, maxS, IfNull(res.ZK, " & x & ") ZK, res.Heben, group_concat(res.Heben_Platz ORDER BY m.Wettkampf) Heben_Platz, " &
                            "res.Wertung2, group_concat(res.Wertung2_Platz ORDER BY m.Wettkampf) Wertung2_Platz, " &
                            "(
			                    Select Gruppierung
                                From wettkampf_wertung
                                Where Wettkampf = m.Wettkampf And idAK = m.idAK
                                limit 1
                            ) Gruppierung"
            Sel_Heben = "LEFT JOIN (Select Teilnehmer, " &
                        "max(If(Versuch = 1, If(Wertung > -2, HLast, " & x & "), NULL)) R_1, " &
                        "max(If(Versuch = 2, If(Wertung > -2, HLast, " & x & "), NULL)) R_2, " &
                        "max(If(Versuch = 3, If(Wertung > -2, HLast, " & x & "), NULL)) R_3, " &
                        "max(If(Versuch = 1, Wertung, NULL)) RW_1, " &
                        "max(If(Versuch = 2, Wertung, NULL)) RW_2, " &
                        "max(If(Versuch = 3, Wertung, NULL)) RW_3, " &
                        "max(If(Versuch = 1, Note, NULL)) RT_1, " &
                        "max(If(Versuch = 2, Note, NULL)) RT_2, " &
                        "max(If(Versuch = 3, Note, NULL)) RT_3, " &
                        "max(HLast * if(Wertung < 0, 0, Wertung)) maxR " &
                        "FROM reissen " &
                        "WHERE " & Wettkampf.WhereClause(, WKs.ToArray) &
                        " GROUP BY Teilnehmer) r " &
                        "On m.Teilnehmer = r.Teilnehmer " &
                        "LEFT JOIN (Select Teilnehmer, " &
                        "max(If(Versuch = 1, If(Wertung > -2, HLast, " & x & "), NULL)) S_1, " &
                        "max(If(Versuch = 2, If(Wertung > -2, HLast, " & x & "), NULL)) S_2, " &
                        "max(If(Versuch = 3, If(Wertung > -2, HLast, " & x & "), NULL)) S_3, " &
                        "max(If(Versuch = 1, Wertung, NULL)) SW_1, " &
                        "max(If(Versuch = 2, Wertung, NULL)) SW_2, " &
                        "max(If(Versuch = 3, Wertung, NULL)) SW_3, " &
                        "max(If(Versuch = 1, Note, NULL)) ST_1, " &
                        "max(If(Versuch = 2, Note, NULL)) ST_2, " &
                        "max(If(Versuch = 3, Note, NULL)) ST_3, " &
                        "max(HLast * if(Wertung < 0, 0, Wertung)) maxS " &
                        "FROM stossen " &
                        "WHERE " & Wettkampf.WhereClause(, WKs.ToArray) &
                        " GROUP BY Teilnehmer) s " &
                        "On m.Teilnehmer = s.Teilnehmer "
            If Athletik Then
                Using conn As New MySqlConnection(User.ConnString)
                    Try
                        conn.Open()
                        Query = "Select d.idDisziplin, d.Bezeichnung, a.Disziplin " &
                        "FROM athletik_disziplinen d " &
                        "LEFT JOIN wettkampf_athletik a On d.idDisziplin = a.Disziplin And a.Wettkampf = " & Wettkampf.ID &
                        " WHERE Not IsNull(a.Disziplin) " &
                        "ORDER BY d.idDisziplin;"
                        Dim cmd = New MySqlCommand(Query, conn)
                        dtA.Load(cmd.ExecuteReader())
                        If dtA.Rows.Count > 0 Then
                            ' Athletik zur Feldliste hinzufügen --> nach wettkampf_athletik.Disziplin ASC sortiert
                            For i = 1 To dtA.Rows.Count
                                Feldliste += ", A_" & i.ToString & ", AW_" & i.ToString
                            Next
                        End If
                        Feldliste += ", res.Athletik"
                        Feldliste += ", res.Gesamt, res.Gesamt_Platz"
                        Query = Feldliste + " FROM meldung m " + Sel_Heben
                        If dtA.Rows.Count > 0 Then
                            Sel_A = "LEFT JOIN (Select Teilnehmer"
                            For i = 1 To dtA.Rows.Count
                                Sel_A += String.Concat(", MAX(Case When Disziplin = ", dtA.Rows(i - 1)("Disziplin").ToString, " Then Wert End) A_", i.ToString)
                            Next
                            Query += Sel_A + " FROM athletik " &
                                     "WHERE Wettkampf = " & Wettkampf.ID &
                                     " GROUP BY Teilnehmer) a " &
                                     "On m.Teilnehmer = a.Teilnehmer "
                            Sel_A = "LEFT JOIN (Select Teilnehmer"
                            For i = 1 To dtA.Rows.Count
                                Sel_A += String.Concat(", MAX(Case When Disziplin = ", dtA.Rows(i - 1)("Disziplin").ToString, " Then Resultat End) AW_", i.ToString)
                            Next
                            Query += Sel_A + " FROM athletik_results " &
                                     "WHERE Wettkampf = " & Wettkampf.ID &
                                     " GROUP BY Teilnehmer) ar " &
                                     "On m.Teilnehmer = ar.Teilnehmer "
                        End If
                    Catch ex As Exception
                        Return String.Empty
                    End Try
                End Using
            ElseIf Not Athletik Then
                Query = Feldliste + " FROM meldung m " + Sel_Heben
            End If
            Query += "LEFT JOIN athleten h ON m.Teilnehmer = h.idTeilnehmer " &
                     "LEFT JOIN faktoren f ON f.sex = h.Geschlecht " &
                     "LEFT JOIN wettkampf_details wd ON wd.Wettkampf = m.Wettkampf " &
                     "LEFT JOIN results res On m.Teilnehmer = res.Teilnehmer And res.Wettkampf = m.Wettkampf " &
                     If(Wettkampf.Mannschaft, "LEFT JOIN teams t On t.idVerein = IfNull(h.MSR, h.Verein) " &
                                              "LEFT JOIN verein v On v.idVerein = IfNull(t.idTeam, IfNull(h.MSR, h.Verein)) ",
                                              "LEFT JOIN verein v On v.idVerein = IfNull(h.ESR, h.Verein) ") &
                     "LEFT JOIN region reg On reg.region_id = v.Region " &
                     "LEFT JOIN staaten sta On h.Staat = sta.state_id " &
                     "LEFT JOIN wettkampf_gewichtsteiler wg ON wg.Wettkampf = m.Wettkampf AND wg.Gruppe = m.Gruppe AND wg.GG = m.GG " &
                     "LEFT JOIN wettkampf_teams wt ON wt.Wettkampf = m.Wettkampf AND wt.Team = h.MSR " &
                     "WHERE " & Wettkampf.WhereClause("m", WKs.ToArray) & " And (m.attend = True OR IsNull(m.attend)) " &
                     "GROUP BY m.Teilnehmer " &
                     "ORDER BY TeamID, -m.Gruppe DESC, Startnummer, Sex, res.Gesamt_Platz, h.Nachname, h.Vorname;"
            Return Query
        End Function

        Public Sub Refresh_ResultsTable()
            ' dtResults neu erstellen
            Using conn As New MySqlConnection(User.ConnString)
                conn.Open()
                Dim cmd = New MySqlCommand(Get_QueryResults("Null", Wettkampf.Athletik), conn)
                dtResults = New DataTable
                dtResults.Load(cmd.ExecuteReader())
            End Using
            ' dvResults und bsResults neu erstellen
            Create_Results()
        End Sub

        Public Function Get_GK(Sex As String, idAK As Integer, Gewicht As Double) As KeyValuePair(Of Integer, String)

            Gewicht = -Ceiling(Gewicht)
            If idAK > 100 Then idAK = 100

            Dim GKs = (From x In dtGK
                       Where x.Field(Of Integer)("idAK") = idAK AndAlso x.Field(Of String)("Sex") = Sex AndAlso CInt(x.Field(Of String)("GK")) <= Gewicht
                       Select key = x.Field(Of Integer)("idGK"), val = CInt(x.Field(Of String)("GK"))).ToDictionary(Function(p) p.key, Function(p) p.val)
            If GKs.Count = 0 Then
                GKs = (From x In dtGK
                       Where x.Field(Of Integer)("idAK") = idAK AndAlso x.Field(Of String)("Sex") = Sex AndAlso CInt(x.Field(Of String)("GK")) > 0
                       Select key = x.Field(Of Integer)("idGK"), val = CInt(x.Field(Of String)("GK"))).ToDictionary(Function(p) p.key, Function(p) p.val)
            End If

            If GKs.Count = 0 Then
                Return Nothing
            ElseIf GKs.ToList(0).Value > 0 Then
                Return New KeyValuePair(Of Integer, String)(GKs.ToList(0).Key, "+" + GKs.ToList(0).Value.ToString)
            Else
                Return New KeyValuePair(Of Integer, String)(GKs.ToList(0).Key, GKs.ToList(0).Value.ToString)
            End If
        End Function

        Public Function Get_StateID(Nation As Object, dvStaaten As DataView, Optional DataField As String = "state_name") As Integer

            If IsDBNull(Nation) OrElse IsNothing(Nation) Then Return 0

            Dim state_id = 0 ' bei jedem nicht gefundenen Staat wird "" eingetragen
            Dim staat_name = Trim(Nation.ToString)

            For i = 1 To staat_name.Length
                dvStaaten.RowFilter = DataField & " Like '" & staat_name.Substring(0, i) & "*'"
                If dvStaaten.Count = 1 Then
                    state_id = CInt(dvStaaten(0)!state_id)
                    Exit For
                ElseIf dvStaaten.Count = 0 Then
                    If DataField.Equals("state_name") Then state_id = Get_StateID(Nation, dvStaaten, "state_long")
                    Exit For
                End If
            Next

            dvStaaten.RowFilter = String.Empty
            Return state_id
        End Function

        Public Function Get_AK(JG As Object) As KeyValuePair(Of Integer, String)

            If IsNothing(JG) OrElse IsDBNull(JG) Then Return Nothing

            Dim _jg = CInt(JG)
            Dim _alter As Integer = Year(Wettkampf.Datum) - _jg
            Dim _ak = Glob.dtAK.Select(_alter & ">= von AND " & _alter & " <= bis")

            If _ak.Count = 0 Then Return Nothing

            Dim s = _ak(0)!AK.ToString

            If CInt(_ak(0)!idAK) > 99 Then s += " (" & _ak(0)("MastersAK").ToString & ")"

            Return New KeyValuePair(Of Integer, String)(CInt(_ak(0)!idAK), s)

        End Function

        '' Mannschaftswertung - Länder/Vereine
        Public Function Get_DGJ_Mannschaftsliste(Id As Integer, Source As DataView) As DataView()
            ' Vereins-, Länderwertung für DGJ

            ' Parameter: Id = 0 (Verein), 1 (Länder)

            Try
                Dim Mannschaft As New DataTable
                With Mannschaft.Columns
                    .Add(New DataColumn With {.ColumnName = "Verein", .DataType = GetType(Integer)})
                    .Add(New DataColumn With {.ColumnName = "Vereinsname", .DataType = GetType(String)})
                    .Add(New DataColumn With {.ColumnName = "Region", .DataType = GetType(Integer)})
                    .Add(New DataColumn With {.ColumnName = "region_name", .DataType = GetType(String)})
                    .Add(New DataColumn With {.ColumnName = "region_3", .DataType = GetType(String)})
                    .Add(New DataColumn With {.ColumnName = "team", .DataType = GetType(Integer)})
                    .Add(New DataColumn With {.ColumnName = "Punkte", .DataType = GetType(Double)})
                    .Add(New DataColumn With {.ColumnName = "Platz", .DataType = GetType(Integer), .DefaultValue = DBNull.Value})
                End With

                Dim Heber As New DataTable
                With Heber.Columns
                    .Add(New DataColumn With {.ColumnName = "Verein", .DataType = GetType(Integer)})
                    .Add(New DataColumn With {.ColumnName = "Region", .DataType = GetType(Integer)})
                    .Add(New DataColumn With {.ColumnName = "Nachname", .DataType = GetType(String)})
                    .Add(New DataColumn With {.ColumnName = "Vorname", .DataType = GetType(String)})
                    .Add(New DataColumn With {.ColumnName = "Geschlecht", .DataType = GetType(String)})
                    .Add(New DataColumn With {.ColumnName = "Vereinsname", .DataType = GetType(String)})
                    .Add(New DataColumn With {.ColumnName = "team", .DataType = GetType(Integer)})
                    .Add(New DataColumn With {.ColumnName = "Punkte", .DataType = GetType(Double)})
                End With

                Dim kvp = Get_Wertung()
                Dim Auswertung = kvp.Key
                Dim Spalte = kvp.Value
                Dim Wertung() = {"Vereinswertung", "Laenderwertung"}
                Dim Bereich = {"Verein", "Region"}
                Dim min = CInt(dvMannschaft(Id)("min"))
                Dim max = CInt(dvMannschaft(Id)("max"))
                Dim gewertet = CInt(dvMannschaft(Id)("gewertet"))
                Dim m_w = CBool(dvMannschaft(Id)("m_w"))           ' mindestens 1 weiblich in Mannschaft
                Dim multi = CBool(dvMannschaft(Id)("multi"))       ' mehrere Mannschaften möglich
                Dim relativ = dvMannschaft(Id)("relativ")          ' maxHeber(w), weitere w = relativ(m), Default = Null

                Dim tmp = New DataView(Source.ToTable)

                tmp.RowFilter = "Convert(IsNull(" & Wertung(Id) & ",''), System.String) <> '' AND " & Wertung(Id) & " > 0"
                tmp.Sort = Bereich(Id) & " ASC, " & Spalte & " DESC"

                Dim TeamCount = Get_Teams(tmp, Bereich(Id), Wertung(Id))

                Dim row = 0
                Dim _bereich = 0    ' Vereine / Länder
                Dim _team = 0       ' Mannschaften
                Dim cnt = 0
                Dim Punkte As Double = 0
                Dim SexChanged = False
                Dim Females = 0

                Do While row < tmp.Count
                    If Not CInt(tmp(row)(Bereich(Id))) = _bereich OrElse multi AndAlso _team < TeamCount(CInt(tmp(row)(Bereich(Id)))) Then '(multi AndAlso Not CInt(tmp(row)(Wertung(Id))) = _team) Then
                        ' nächster Verein / Land
                        If multi AndAlso _bereich = CInt(tmp(row)(Bereich(Id))) Then
                            _team += 1
                        Else
                            _bereich = CInt(tmp(row)(Bereich(Id)))
                            _team = 1
                        End If
                        Punkte = 0
                        SexChanged = False
                        Females = 0
                        cnt = 0
                        Do While cnt < gewertet AndAlso CInt(tmp(row)(Bereich(Id))) = _bereich
                            If Not IsDBNull(tmp(row)(Spalte)) Then
                                If tmp(row)!Geschlecht.ToString.Equals("w") Then SexChanged = True
                                If Not m_w OrElse m_w AndAlso (SexChanged OrElse cnt < gewertet - 1) Then
                                    If Not IsDBNull(relativ) AndAlso Females > CInt(relativ) Then
                                        ' alle weiteren weiblichen Heber mit RelativWertung(männlich) hinzufügen
                                        If Not IsDBNull(tmp(row)!R_Last) AndAlso Not IsDBNull(tmp(row)!S_Last) Then
                                            Using RS As New Results
                                                Dim Reissen = RS.Get_Result_Heben(CInt(tmp(row)!Teilnehmer), Auswertung, CDbl(tmp(row)!Wiegen), "m", CInt(tmp(row)!R_Last), 0, 0, "", 0, 0)
                                                If Not IsNothing(Reissen) Then Punkte += CDbl(Reissen)
                                                Dim Stossen = RS.Get_Result_Heben(CInt(tmp(row)!Teilnehmer), Auswertung, CDbl(tmp(row)!Wiegen), "m", CInt(tmp(row)!S_Last), 0, 0, "", 0, 0)
                                                If Not IsNothing(Stossen) Then Punkte += CDbl(Stossen)
                                            End Using
                                        End If
                                    Else
                                        Punkte += CDbl(tmp(row)(Spalte))
                                    End If
                                    If tmp(row)!Geschlecht.ToString.Equals("w") Then Females += 1
                                    Heber.Rows.Add(tmp(row)!Verein, tmp(row)!Region, tmp(row)!Nachname, tmp(row)!Vorname, tmp(row)!Geschlecht, tmp(row)!Vereinsname, _team, tmp(row)(Spalte))
                                    cnt += 1
                                    If cnt < gewertet Then row += 1
                                    If row = tmp.Count OrElse Not CInt(tmp(row)(Bereich(Id))) = _bereich Then
                                        row -= 1
                                        Exit Do
                                    End If
                                Else
                                    row += 1
                                End If
                            Else
                                row += 1
                            End If
                        Loop
                        'If cnt >= min Then
                        Mannschaft.Rows.Add(tmp(row)!Verein, tmp(row)!Vereinsname, tmp(row)!Region, tmp(row)!region_name, tmp(row)!region_3, _team, Punkte)
                        'Else
                        '    Dim del = Heber.Select(Bereich(Id) & " = " & tmp(row)(Bereich(Id)).ToString)
                        '    For Each r As DataRow In del
                        '        Heber.Rows.Remove(r)
                        '    Next
                        'End If
                    End If
                    row += 1
                Loop

                tmp = New DataView(Mannschaft)
                tmp.Sort = "Punkte DESC"
                Dim Platz = 1
                Punkte = CDbl(tmp(0)!Punkte)
                For i = 0 To tmp.Count - 1
                    If CDbl(tmp(i)!Punkte) < Punkte Then Platz += 1
                    Punkte = CDbl(tmp(i)!Punkte)
                    tmp(i)!Platz = Platz
                Next

                Return {tmp, New DataView(Heber)}

            Catch ex As Exception
                Return Nothing
            End Try

        End Function

        Private Function Get_Teams(tmp As DataView, Bereich As String, Wertung As String) As Dictionary(Of Integer, Integer)
            Dim tc = (From x In tmp.ToTable
                      Group By b = x.Field(Of Integer)(Bereich) Into Group
                      Select New With {Key b, .Value = Group.Max(Function(r) r.Field(Of Integer)(Wertung))}).AsEnumerable
            Dim TeamCount As New Dictionary(Of Integer, Integer)
            For Each item In tc
                TeamCount.Add(item.b, item.Value)
            Next
            Return TeamCount
        End Function

        Public Function Get_Wertung() As KeyValuePair(Of Integer, String)

            ' Returnwert:   1. oder 2. Wertung + entsprechende Spalte

            Dim Score = CInt(Glob.dtModus.Select("idModus = " & Wettkampf.Modus)(0)!Wertung)        ' 1.Wertung des WKs
            Dim IsEqualScoring = Wettkampf.Platzierung > 99 AndAlso                                 ' 1. und 2. Platzierung nicht gleich
                CInt(Wettkampf.Platzierung.ToString.Substring(0, 1)) = CInt(Wettkampf.Platzierung.ToString.Substring(1, 1))
            If Not IsEqualScoring Then Score = CInt(Wettkampf.Platzierung.ToString.Substring(2, 1)) ' 2. Wertung nehmen

            Dim Spalte = String.Empty
            If Not Wettkampf.Athletik AndAlso IsEqualScoring Then
                Spalte = "Heben"
            ElseIf IsEqualScoring Then
                Spalte = "Gesamt"
            ElseIf Not IsEqualScoring Then
                Spalte = "Wertung2"
            End If

            Return New KeyValuePair(Of Integer, String)(Score, Spalte)
        End Function

    End Class

    Class Results
        '' Gewichtheben
        Implements IDisposable
        Public Sub New()
        End Sub

        Public Function Get_Result_Heben(TN As Integer, Auswertung As Integer, KG As Double, Sex As String, HLast As Integer,
                                         JG As Integer, T_Note As Double, Durchgang As String, idAK As Integer, idGK As Integer,
                                         Optional Lasten As Integer?() = Nothing) As Double?

            If KG = 0 Then KG = Heber.Wiegen
            If String.IsNullOrEmpty(Sex) Then Sex = Heber.Sex
            If JG = 0 Then JG = Heber.JG
            If TN = 0 Then TN = Heber.Id
            Dim result As Double?
            Dim ZK As Integer?

            Try
                Dim NK_Stellen As Integer = Get_NK_Stellen(Auswertung)

                Select Case Auswertung
                    Case 1 ' *************** Robi-Punkte
                        If Not IsNothing(Lasten) Then
                            If Not IsNothing(Lasten(0)) AndAlso Not IsNothing(Lasten(1)) Then
                                ZK = CInt(Lasten(0)) + CInt(Lasten(1))
                            Else
                                ZK = Nothing
                            End If
                        Else
                            ZK = ZK_Resultat(TN, {dvDelete(0).ToTable, dvLeader(1).ToTable})
                        End If
                        If IsNothing(ZK) Then
                            result = Nothing
                        Else
                            If Wettkampf.SameAK > 0 Then idAK = Wettkampf.SameAK
                            Dim b = 3.32192809488736
                            Dim pos = Glob.dvRekorde.Find({"Welt", Sex, idAK, idGK, 3}) ' 3 = Total
                            Dim World_Record = CInt(Glob.dvRekorde(pos)!result)
                            result = Round(CDbl(1000 / World_Record ^ b * ZK ^ b), NK_Stellen)
                        End If
                    Case 2 ' *************** Relativpunkte
                        Dim Abzug As Double
                        If Sex.Equals("m") AndAlso KG >= 79.1 AndAlso KG <= 95.5 Then
                            Abzug = KG
                        ElseIf Sex.Equals("m") AndAlso KG > 95.5 AndAlso KG < 96.0 Then
                            Abzug = 95.0
                        Else
                            Dim pos = Glob.dvRelativ.Find(Ceiling(KG))
                            If pos > -1 Then Abzug = CDbl(Glob.dvRelativ(pos)(Sex))
                            'Dim pos = bsResults.Find("Teilnehmer", TN)
                            'If pos > -1 Then Abzug = CDbl(dvResults(pos)!Relativ)
                        End If
                        result = HLast - Abzug
                    Case 3, 4 ' **************** Sinclairpunkte, S-M Punkte
                        'Sinclair-Faktor berechnen
                        NK_Stellen = Get_NK_Stellen(3)
                        Dim SinclairC As Double = Round(If(KG > Glob.minKG(Sex), If(KG < Glob.maxKG(Sex), Round(10 ^ (Glob.SinclairFaktor(Sex) * Log10(KG / Glob.maxKG(Sex)) ^ 2), NK_Stellen), 1), Glob.maxSC(Sex)), NK_Stellen)
                        If Auswertung = 3 Then
                            ' Sinclair
                            result = Round(HLast * SinclairC * Wettkampf.Wertungsfaktor(Sex), NK_Stellen)
                        ElseIf Auswertung = 4 Then
                            ' S-M Punkte
                            Dim AgeFactor As Double = 1
                            NK_Stellen = Get_NK_Stellen(4)
                            Dim age() As DataRow = Glob.dtAlter.Select("idAlter = " & Year(Wettkampf.Datum) - JG)
                            If age.Count > 0 Then AgeFactor = CDbl(age(0)("Faktor"))
                            result = Round(HLast * SinclairC * AgeFactor, NK_Stellen)
                        End If
                    Case 5 ' **************** Zweikampf
                        result = HLast
                    Case 6 ' **************** Mehrkampf
                        Dim Modus = Wettkampf.Modus
                        If Modus < 110 Then Modus = 110 ' es gibt keine Formeln für Modus < 110
                        Dim p = Glob.dvFormeln.Find({Modus, Year(Wettkampf.Datum) - CInt(JG), Sex})
                        Dim Formel As String = Glob.dvFormeln(p)(Durchgang).ToString
                        If Not String.IsNullOrEmpty(Formel) Then
                            If T_Note = 0 Then
                                result = 0
                            Else
                                Dim args As New Arguments
                                With args
                                    .HL = HLast
                                    .KG = KG
                                    .TN = T_Note
                                End With
                                Using Parser As New Parser
                                    result = Parser.Parse_Formel(Formel, args)
                                End Using
                            End If
                        Else ' sollte es gar nicht geben
                            result = HLast
                        End If
                        result = Round(CDbl(result), NK_Stellen)
                End Select
            Catch ex As Exception
                Return Nothing
            End Try

            If result < 0 Then Return 0
            Return result

        End Function

        Public Function Get_Result_2(Result As Double?,
                                     Auswertung As Integer,
                                     Auswertung2 As Integer,
                                     Table As String,
                                     Complements() As DataTable,
                                     Row As Integer,
                                     TN As Integer,
                                     KG As Double,
                                     Sex As String,
                                     JG As Integer,
                                     Hantellast As Integer,
                                     Note As Double,
                                     idAK As Integer,
                                     idGK As Integer,
                                     Optional Lasten As Integer?() = Nothing) As Double?

            If KG = 0 Then KG = Heber.Wiegen
            If String.IsNullOrEmpty(Sex) Then Sex = Heber.Sex
            If JG = 0 Then JG = Heber.JG
            If TN = 0 Then TN = Heber.Id

            Dim Durchgang = If(Table.Equals("Reissen"), 0, 1)
            Dim Result_2 As Double? = Nothing

            If Wettkampf.Platzierung > 99 Then
                Dim Result_Durchgang(1) As Double?

                Try
                    ' 2. Wertung des aktuellen Durchgangs
                    If Auswertung2 <> 1 AndAlso Auswertung2 <> 5 Then ' ############################## alles außer Robi und Zweikampf 

                        If Auswertung <> Auswertung2 Then
                            ' bei unterschiedlichen Wertungen Resultat berechnen
                            Using RS As New Results
                                Result_Durchgang(Durchgang) = RS.Get_Result_Heben(TN, Auswertung2, KG, Sex, Hantellast, JG, Note, Table, idAK, idGK)
                            End Using
                            If IsNothing(Result_Durchgang(Durchgang)) Then Result_Durchgang(Durchgang) = 0
                        Else
                            ' bei gleicher Wertung Resultat übernehmen
                            Result_Durchgang(Durchgang) = Result
                        End If
                        ' Resultat des anderen Durchgangs in Delete suchen --> wenn nicht in Delete, dann ist der jeweils andere Durchgang noch nicht gelaufen
                        Try
                            Dim HLast = (From x In Complements(Abs(Durchgang - 1))
                                         Where x.Field(Of Integer)("Teilnehmer") = TN AndAlso x.Field(Of Integer?)("Wertung") = 1
                                         Select x.Field(Of Integer)("HLast")).Max
                            Using RS As New Results
                                Result_Durchgang(Abs(Durchgang - 1)) = RS.Get_Result_Heben(TN, Auswertung2, KG, Sex, HLast, JG, 0, Table, idAK, idGK)
                            End Using
                        Catch ex As Exception
                            Result_Durchgang(Abs(Durchgang - 1)) = 0
                        End Try
                        ' Gesamtresultat 2.Wertung
                        Result_2 = Result_Durchgang(0) + Result_Durchgang(1)

                    ElseIf Row > -1 And Not IsDBNull(dvResults(Row)!ZK) Or Not IsNothing(Lasten) Then ' ############################ Robi und Zweikampf

                        If Auswertung <> Auswertung2 Then
                            Using RS As New Results
                                Result_2 = RS.Get_Result_Heben(TN, Auswertung2, KG, Sex, CInt(dvResults(Row)!ZK), 0, 0, "Total", idAK, idGK, Lasten)
                            End Using
                        Else
                            Result_2 = Result
                        End If

                    End If

                Catch ex As Exception
                    LogMessage("Global: Get_Result_2: " & ex.Message, ex.StackTrace)
                End Try
            End If

            Return Result_2
        End Function

        Public Function Get_Wertung1_Filter(Sex As String,
                                            JG As Integer,
                                            Gruppe As String,
                                            KG As Double,
                                            idAK As Integer,
                                            idGK As Integer,
                                            Optional LeaderGruppe As List(Of String) = Nothing) As String

            Dim Filter = String.Empty
            If IsNothing(LeaderGruppe) Then LeaderGruppe = Leader.Gruppe

            If Wettkampf.Modus < 400 AndAlso Glob.dtAG.Rows.Count > 0 Then
                ' Wertung in Altersgruppen (nur bei Modus < 400)
                Try
                    Dim JGs = From j In Glob.dtAG
                              Where j.Field(Of String)("Sex") = Sex AndAlso CInt(j.Field(Of String)("Jahrgang")) = JG
                              Select j.Field(Of String)("Jahrgang")
                    If JGs.Count = 0 Then
                        Filter = " Sex = '" + Sex + "' And Jahrgang = " + JG.ToString
                    Else
                        Dim sp = Split(JGs(0), ", ")
                        Filter = " Sex = '" + Sex + "' AND (Jahrgang = " + Join(sp, " Or Jahrgang = ") + ")"
                    End If
                Catch ex As Exception
                End Try
            ElseIf Wettkampf.Modus < 400 AndAlso Wettkampf.Gewichtseinteilung.Equals("Gewichtsgruppen") Then
                Dim pos = Glob.dvGewicht.Find(Gruppe) ' -------------------- mehrere Gewichtsteiler pro Gruppe möglich !!!!!!!!!!!!!!!!!
                If pos > -1 Then
                    ' Wertung in Gewichtsgruppen (nur bei Modus < 400) --------------------------------------------------------------------------------------- das stimmt gar nicht --------------------------
                    Dim Gewichtsteiler = CDbl(Glob.dvGewicht(pos)("Gewicht")) '************************* GG anstatt Gewicht
                    If KG < Gewichtsteiler Then
                        Filter = " Gruppe = " & Gruppe & " And KG < " & Gewichtsteiler.ToString
                    Else
                        Filter = " Gruppe = " & Gruppe & " And KG >= " & Gewichtsteiler.ToString
                    End If
                ElseIf Wettkampf.Alterseinteilung.Equals("Jahrgang") Then
                    Filter = " (Gruppe = " & String.Join(" OR Gruppe = ", LeaderGruppe) & ") AND Jahrgang = " & JG
                Else
                    ' Wertung in Gruppen
                    Filter = " Gruppe = " & String.Join(" OR Gruppe = ", LeaderGruppe)
                End If
            Else
                Select Case Wettkampf.Platzierung.ToString.Substring(0, 1)
                    Case "1" ' Wertung über gesamten Wettkampf
                    Case "2" ' Wertung in Gruppen
                        Filter = " Gruppe = " & Gruppe
                    Case "3" ' Wertung in Altersklassen 
                        Filter = " Sex = '" & Sex & "' AND idAK = " & idAK & " AND idGK = " & idGK
                    Case "4" ' Wertung in Gewichtsklassen
                        Filter = " Sex = '" & Sex & "' AND idGK = " & idGK
                End Select
            End If

            Return Filter
        End Function

        Public Function Write_Results_Table(row As Integer,
                                            col As String,
                                            Sex As String,
                                            HLast As Integer,
                                            Wertung As Integer?,
                                            Zeit As Date?,
                                            Note As Double?,
                                            Durchgang As String,
                                            Result As Double?,
                                            Result2 As Double?,
                                            JG As Integer,
                                            Auswertung As Integer,
                                            Auswertung2 As Integer,
                                            Gruppierung2 As Integer,
                                            Optional Korrektur As Boolean = False,
                                            Optional LeaderGruppe As List(Of String) = Nothing) As Boolean

            Dim Result_Old As Double
            If Not IsDBNull(dvResults(row)(Durchgang)) Then Result_Old = CDbl(dvResults(row)(Durchgang))

#Region "Ergebnis"
            Try
                dvResults(row)(col) = HLast
                If Not IsNothing(Result) OrElse Auswertung = 1 Then
                    'dvResults(row)(col) = HLast
                    If IsNothing(Wertung) Then
                        ' wieder eingestellt
                        dvResults(row)(col.Insert(1, "W")) = DBNull.Value
                        dvResults(row)("max" & col.Substring(0, 1)) = DBNull.Value
                        dvResults(row)(Durchgang & "_Zeit") = DBNull.Value
                        If Wettkampf.Technikwertung Then dvResults(row)(col.Insert(1, "T")) = DBNull.Value
                    Else
                        dvResults(row)(col.Insert(1, "W")) = Wertung
                        If IsNothing(Zeit) OrElse Year(CDate(Zeit)) = 1 Then
                            dvResults(row)(Durchgang & "_Zeit") = DBNull.Value
                        Else
                            dvResults(row)(Durchgang & "_Zeit") = Zeit ' Zeit immer speichern, da auch ungültige Versuche in Sortierung einfließen
                        End If
                        If Wertung = 1 Then
                            dvResults(row)("max" & col.Substring(0, 1)) = HLast
                            If Wettkampf.Technikwertung Then dvResults(row)(col.Insert(1, "T")) = Note
                        ElseIf Korrektur AndAlso Wertung <> 0 Then
                            ' Ungültig und Verzicht von Korrektur
                            Dim apx = col.Substring(0, 1)
                            Dim max = 0
                            For i = 1 To 3
                                If Not IsDBNull(dvResults(row)(apx & "W_" & i)) Then
                                    max = CInt(dvResults(row)(apx & "W_" & i)) * CInt(dvResults(row)(apx & "_" & i))
                                End If
                            Next
                            dvResults(row)("max" & col.Substring(0, 1)) = IIf(max < 0, 0, max)
                        End If
                    End If

                    If Wettkampf.Athletik Then
                        dvResults(row)!Gesamt = dvResults(row)!Heben
                        If dtResults.Columns.Contains("Athletik") AndAlso Not IsDBNull(dvResults(row)!Athletik) Then
                            ' Athletik-Wertung einrechnen
                            dvResults(row)!Gesamt = CDbl(If(IsDBNull(dvResults(row)!Gesamt), 0, dvResults(row)!Gesamt)) + CDbl(dvResults(row)!Athletik)
                        End If
                    End If
                Else
                    'dvResults(row)(col) = HLast
                    dvResults(row)(col.Insert(1, "W")) = DBNull.Value
                    dvResults(row)("max" & col.Substring(0, 1)) = DBNull.Value
                    dvResults(row)(Durchgang & "_Zeit") = DBNull.Value
                    If Wettkampf.Technikwertung Then dvResults(row)(col.Insert(1, "T")) = DBNull.Value

                    'dvResults(row)(Durchgang) = IIf(IsNothing(Result), DBNull.Value, Result)
                    'dvResults(row)!Wertung2 = IIf(IsNothing(Result2), DBNull.Value, Result2)
                End If
#End Region

#Region "Wertungen NICHT bei Korrektur"
                If Not Korrektur Then
                    If IsDBNull(dvResults(row)(Durchgang)) OrElse (Not IsDBNull(dvResults(row)(Durchgang)) AndAlso Result > CDbl(dvResults(row)(Durchgang))) Then
                        If Auswertung = 1 Then
                            'ROBI Points für Durchgang berechnen ODER auf DBNull setzen
                            dvResults(row)(Durchgang) = DBNull.Value
                        ElseIf Auswertung = 5 Then
                            ' bei ZK keine Results für Durchgang
                            dvResults(row)(Durchgang) = DBNull.Value
                        Else
                            dvResults(row)(Durchgang) = IIf(IsNothing(Result), DBNull.Value, Result)
                        End If
                        dvResults(row)!Wertung2 = IIf(IsNothing(Result2), DBNull.Value, Result2)
                    End If

                    Dim ZK = ZK_Resultat(CInt(dvResults(row)!Teilnehmer))
                    dvResults(row)!ZK = IIf(IsNothing(ZK), DBNull.Value, ZK)

                    If Auswertung = 1 OrElse Auswertung = 5 Then
                        If IsNothing(ZK) Then
                            dvResults(row)!Heben = DBNull.Value
                        ElseIf Auswertung = 1 Then
                            dvResults(row)!Heben = Result
                        ElseIf Auswertung = 5 Then
                            dvResults(row)!Heben = ZK
                        End If
                    Else
                        dvResults(row)!Heben = CDbl(If(IsDBNull(dvResults(row)!Reissen), 0, dvResults(row)!Reissen)) + CDbl(If(IsDBNull(dvResults(row)!Stossen), 0, dvResults(row)!Stossen))
                    End If
                End If
                bsResults.EndEdit()
#End Region

#Region "Platzierung"
                If Not Wettkampf.Mannschaft AndAlso (Not IsNothing(Wertung) AndAlso Wertung = 1 AndAlso Result > Result_Old) OrElse Korrektur Then
                    For Each WK In Wettkampf.Identities.Keys
                        Dim Kategorie As New List(Of String)
                        Kategorie.AddRange({Durchgang, "Heben"})
                        If Wettkampf.Athletik Then Kategorie.Add("Gesamt")
                        Set_Rank(Sex,
                                 JG,
                                 CDbl(dvResults(row)!KG),
                                 dvResults(row)!Gruppe.ToString,
                                 Kategorie,
                                 Auswertung,
                                 Auswertung2,
                                 Gruppierung2,
                                 CInt(dvResults(row)!idAK),
                                 CInt(dvResults(row)!idGK),
                                 WK,
                                 LeaderGruppe)
                    Next
                End If
#End Region

            Catch ex As Exception
                LogMessage("Results: Write_Results_Table: " & ex.Message, ex.StackTrace)
                Return False
            Finally
                bsResults.EndEdit()
            End Try
            Return True
        End Function

        Public Function Write_Results_Table_Ergebnis(row As Integer,
                                                     Col As String,
                                                     HLast As Integer,
                                                     Wertung As Integer?,
                                                     Zeit As Date?,
                                                     Note As Double?,
                                                     Durchgang As String,
                                                     Result As Double?,
                                                     Auswertung As Integer,
                                                     Optional Korrektur As Boolean = False) As Double

            Dim Result_Old As Double
            If Not Korrektur AndAlso Not IsDBNull(dvResults(row)(Durchgang)) Then
                Result_Old = CDbl(dvResults(row)(Durchgang))
            End If

            Try
                dvResults(row)(Col) = HLast
                If Not IsNothing(Result) OrElse Auswertung = 1 Then
                    'dvResults(row)(col) = HLast
                    If IsNothing(Wertung) Then
                        ' wieder eingestellt
                        dvResults(row)(Col.Insert(1, "W")) = DBNull.Value
                        dvResults(row)("max" & Col.Substring(0, 1)) = DBNull.Value
                        dvResults(row)(Durchgang & "_Zeit") = DBNull.Value
                        If Wettkampf.Technikwertung Then dvResults(row)(Col.Insert(1, "T")) = DBNull.Value
                    Else
                        dvResults(row)(Col.Insert(1, "W")) = Wertung
                        If IsNothing(Zeit) OrElse Year(CDate(Zeit)) = 1 OrElse Wertung = 0 Then
                            dvResults(row)(Durchgang & "_Zeit") = DBNull.Value
                        Else
                            dvResults(row)(Durchgang & "_Zeit") = Zeit ' Zeit immer speichern, da auch ungültige Versuche in Sortierung einfließen
                        End If
                        If Wertung = 1 Then
                            dvResults(row)("max" & Col.Substring(0, 1)) = HLast
                            If Wettkampf.Technikwertung Then dvResults(row)(Col.Insert(1, "T")) = Note
                        ElseIf Korrektur AndAlso Wertung <> 0 Then
                            ' Ungültig oder Verzicht von Korrektur
                            Dim dg = Col.Substring(0, 1) 'Durchgang.Substring(0, 1)
                            dvResults(row)("max" & dg) = Get_DG_Max(dg, dvResults(row))
                        End If
                    End If

                    If Wettkampf.Athletik Then
                        dvResults(row)!Gesamt = dvResults(row)!Heben
                        If dtResults.Columns.Contains("Athletik") AndAlso Not IsDBNull(dvResults(row)!Athletik) Then
                            ' Athletik-Wertung einrechnen
                            dvResults(row)!Gesamt = CDbl(If(IsDBNull(dvResults(row)!Gesamt), 0, dvResults(row)!Gesamt)) + CDbl(dvResults(row)!Athletik)
                        End If
                    End If
                Else
                    dvResults(row)(Col) = DBNull.Value
                    dvResults(row)(Col.Insert(1, "W")) = DBNull.Value
                    If Wettkampf.Technikwertung Then dvResults(row)(Col.Insert(1, "T")) = DBNull.Value
                    If Not Korrektur OrElse CInt(Col.Substring(2, 1)) = 1 Then
                        dvResults(row)("max" & Durchgang.Substring(0, 1)) = DBNull.Value
                        dvResults(row)(Durchgang & "_Zeit") = DBNull.Value
                    End If
                    'dvResults(row)(Durchgang) = IIf(IsNothing(Result), DBNull.Value, Result)
                    'dvResults(row)!Wertung2 = IIf(IsNothing(Result2), DBNull.Value, Result2)
                End If
            Catch ex As Exception
                LogMessage("Results: Write_Results_Table_Ergebnis: " & ex.Message, ex.StackTrace)
            Finally
                bsResults.EndEdit()
            End Try

            Return Result_Old
        End Function
        Public Function Get_DG_Max(dg As String, row As DataRowView) As Integer
            Dim max = 0
            For i = 1 To 3
                If Not IsDBNull(row(dg & "W_" & i)) AndAlso CInt(row(dg & "W_" & i)) = 1 Then
                    max = CInt(row(dg & "_" & i))
                End If
            Next
            Return max
        End Function
        Public Function Write_Results_Table_Wertung(row As Integer,
                                                    Sex As String,
                                                    Wertung As Integer?,
                                                    Durchgang As String,
                                                    Result As Double?,
                                                    Result2 As Double?,
                                                    Gesamt2 As Double?,
                                                    JG As Integer,
                                                    Auswertung As Integer,
                                                    Auswertung2 As Integer,
                                                    Gruppierung2 As Integer,
                                                    Result_Old As Double,
                                                    Optional Korrektur As Boolean = False,
                                                    Optional LeaderGruppe As List(Of String) = Nothing) As Boolean

            ' Wertung --> Gültig/Ungültig

            Try
#Region "Wertung"
                If Korrektur OrElse IsDBNull(dvResults(row)(Durchgang)) OrElse (Not IsDBNull(dvResults(row)(Durchgang)) AndAlso Result > CDbl(dvResults(row)(Durchgang))) Then
                    If Auswertung = 1 Then
                        'ROBI Points für Durchgang berechnen ODER auf DBNull setzen
                        dvResults(row)(Durchgang) = DBNull.Value
                        'ElseIf Auswertung = 5 Then
                        '    ' bei ZK keine Results für Durchgang
                        '    dvResults(row)(Durchgang) = DBNull.Value
                    Else
                        dvResults(row)(Durchgang) = IIf(IsNothing(Result), DBNull.Value, Result)
                    End If
                    If Auswertung2 = 1 Then
                        'ROBI Points für Durchgang berechnen ODER auf DBNull setzen
                        dvResults(row)(Durchgang & "2") = DBNull.Value
                        'ElseIf Auswertung2 = 5 Then
                        '    ' bei ZK keine Results für Durchgang
                        '    dvResults(row)(Durchgang & "2") = DBNull.Value
                    Else
                        dvResults(row)(Durchgang & "2") = IIf(IsNothing(Result2), DBNull.Value, Result2)
                    End If

                    dvResults(row)!Wertung2 = IIf(IsNothing(Gesamt2), DBNull.Value, Gesamt2)
                End If

                Dim ZK = ZK_Resultat(CInt(dvResults(row)!Teilnehmer))
                dvResults(row)!ZK = IIf(IsNothing(ZK), DBNull.Value, ZK)

                If Auswertung = 1 OrElse Auswertung = 5 Then
                    If IsNothing(ZK) Then
                        dvResults(row)!Heben = DBNull.Value
                    ElseIf Auswertung = 1 Then
                        dvResults(row)!Heben = Result
                    ElseIf Auswertung = 5 Then
                        dvResults(row)!Heben = ZK
                    End If
                Else
                    dvResults(row)!Heben = CDbl(If(IsDBNull(dvResults(row)!Reissen), 0, dvResults(row)!Reissen)) + CDbl(If(IsDBNull(dvResults(row)!Stossen), 0, dvResults(row)!Stossen))
                    If CInt(dvResults(row)!Heben) = 0 Then dvResults(row)!Heben = DBNull.Value
                End If
#End Region
#Region "Platzierung"
                If Not Wettkampf.Mannschaft AndAlso (Not IsNothing(Wertung) AndAlso Wertung = 1 AndAlso Result > Result_Old) OrElse Korrektur Then
                    For Each WK In Wettkampf.Identities.Keys
                        Dim Kategorie As New List(Of String)
                        Kategorie.AddRange({Durchgang, "Heben"})
                        If Wettkampf.Athletik Then Kategorie.Add("Gesamt")
                        Set_Rank(Sex,
                                 JG,
                                 CDbl(dvResults(row)!KG),
                                 dvResults(row)!Gruppe.ToString,
                                 Kategorie,
                                 Auswertung,
                                 Auswertung2,
                                 Gruppierung2,
                                 CInt(dvResults(row)!idAK),
                                 CInt(dvResults(row)!idGK),
                                 WK,
                                 LeaderGruppe)
                    Next
                End If
#End Region
            Catch ex As Exception
                LogMessage("Results: Write_Results_Table_Wertung: " & ex.Message, ex.StackTrace)
                Return False
            Finally
                bsResults.EndEdit()
            End Try

            Return True
        End Function

        Public Sub Set_Rank(Sex As String,
                            JG As Integer,
                            KG As Double,
                            Gruppe As String,
                            Kategorie As List(Of String),
                            Auswertung As Integer,
                            Auswertung2 As Integer,
                            Gruppierung2 As Integer,
                            idAK As Integer,
                            idGK As Integer,
                            WK As String,
                            Optional LeaderGruppe As List(Of String) = Nothing)

            ' Platzierung der einzelnen Athletik-Disziplinen erfolgt in frmAthletik
            ' Gruppierung ist die Zusammenfassung von Platzierung2 in Altersgruppen

            Dim tmpSource = New DataView(dtResults)
            If Wettkampf.SameAK > 0 Then idAK = Wettkampf.SameAK

            ' 1. Platzierung
            Dim Filter = Get_Wertung1_Filter(Sex, JG, Gruppe, KG, idAK, idGK, LeaderGruppe)
            If Not String.IsNullOrEmpty(Filter) Then Filter = " And " & Filter
            tmpSource.RowFilter = "Wettkampf Like '%" & WK & "%'" & Filter
            Do_Ranking(tmpSource, Kategorie, Auswertung, WK)

            ' 2. Wertung + Platzierung
            If Wettkampf.Platzierung > 99 AndAlso Auswertung2 > 0 Then
                Dim dg = Kategorie(0) & "2"
                Kategorie.Clear()
                Kategorie.AddRange({dg, "Wertung2"})
                Filter = Get_Wertung2_Filter(, WK, Sex, idAK, idGK, Gruppierung2, LeaderGruppe)
                tmpSource.RowFilter = Filter
                Do_Ranking(tmpSource, Kategorie, Auswertung2, WK)
            End If

        End Sub

        Private Sub Do_Ranking(tmpSource As DataView, Kategorie As List(Of String), Auswertung As Integer, WK As String)
            ' tmpSource = alle Heber, für die das Ranking durchgeführt werden soll
            ' Kategorie = Reißen, Stoßen, Heben, Gesamt, Athletik, Wertung2

            ' bei Zweikampf-Wertung (alle außer 5 (Mehrkampf)) gibt es kein Gesamt-Resultat,
            ' wenn eine Disziplin geplatzt ist (Disziplin = 0) --> Heben = 0, Gesamt = 0 
            ' --> aber eine Platzierung (nach Zeit sortiert???)

            For k = 0 To Kategorie.Count - 1

                Dim sort = Kategorie(k) & " DESC"
                Dim Platz As Integer = 0
                Dim Platz_Add = 1
                Dim tmpResult As Double = 0

                If Not Wettkampf.Athletik AndAlso Kategorie(k).Contains("ssen") Then sort += ", " + Replace(Kategorie(k), "2", "") + "_Zeit ASC"
                tmpSource.Sort = sort

                For Each row As DataRowView In tmpSource
                    Try
                        'Dim ix = Split(row!Wettkampf.ToString, ",").ToList().IndexOf(WK)

                        If Not IsDBNull(row(Kategorie(k))) AndAlso CInt((row(Kategorie(k)))) > 0 AndAlso  ' Wertung vorhanden
                            (
                                (
                                    Auswertung <> 5 Or Kategorie(k).Contains("ssen")
                                ) OrElse
                                Not IsDBNull(row!maxR) AndAlso CInt(row!maxR) > 0 AndAlso
                                Not IsDBNull(row!maxS) AndAlso CInt(row!maxS) > 0
                            ) Then
                            ' ==> Wertung == 0 bekommt keine Platzierung

                            If Not CBool(row!a_K) Then
                                If Wettkampf.Athletik Then
                                    ' Platzierung ohne Zeit = gleiche Plätze möglich
                                    If Not tmpResult = CDbl(row(Kategorie(k))) Then
                                        tmpResult = CDbl(row(Kategorie(k)))
                                        ' neue Platzierung
                                        Platz += Platz_Add
                                        Platz_Add = 1
                                    Else
                                        Platz_Add += 1
                                    End If
                                Else
                                    ' Platzierung mit Sortierung nach Zeit = es gibt keine gleichen Plätze
                                    Platz += 1
                                End If
                                ' Resultat speichern
                                row(Kategorie(k) + "_Platz_" & WK) = Platz
                                row(Kategorie(k) + "_Platz") = Platz

                            Else
                                ' DBNull.Value für a.K. speichern ==> nicht platziert
                                row(Kategorie(k) + "_Platz_" & WK) = DBNull.Value
                                row(Kategorie(k) + "_Platz") = DBNull.Value
                            End If
                        Else
                            ' Platzierung löschen
                            row(Kategorie(k) + "_Platz_" & WK) = DBNull.Value
                            row(Kategorie(k) + "_Platz") = DBNull.Value
                        End If
                    Catch ex As Exception
                        LogMessage("Global: Do_Ranking: Write_Rank: " & ex.Message, ex.StackTrace)
                    End Try

                    bsResults.EndEdit()
                Next
            Next
        End Sub

        Function Get_Wertung2_Filter(
                                    Optional Row As DataRowView = Nothing,
                                    Optional WK As String = "",
                                    Optional Sex As String = "",
                                    Optional idAK As Integer = 0,
                                    Optional idGK As Integer = 0,
                                    Optional Gruppierung As Integer = 0,
                                    Optional LeaderGruppe As List(Of String) = Nothing
                                    ) As String

            If Not IsNothing(Row) Then
                WK = Wettkampf.Identities.Keys(0)  'Row!Wettkampf.ToString
                Sex = Row!Sex.ToString
                idAK = CInt(Row!idAK)
                idGK = CInt(Row!idGK)
                Gruppierung = CInt(Row!Gruppierung)
            End If

            If IsNothing(LeaderGruppe) Then LeaderGruppe = Leader.Gruppe

            Select Case Wettkampf.Platzierung.ToString.Substring(1, 1)
                Case "2"    ' Gruppen
                    Return "Wettkampf Like '%" & WK & "%' AND (Gruppe = " & String.Join(" OR Gruppe = ", LeaderGruppe) & ")"
                Case "3"    ' Altersklassen
                    Dim Filter = "Wettkampf LIKE '%" & WK & "%'"
                    If Not Wettkampf.IgnoreSex Then
                        Filter += " AND Sex = '" & Sex & "'"
                    End If
                    If Wettkampf.Platzierung < 100 Then
                        Return Filter & " AND AK = " & idAK & " AND GK = " & idGK
                    Else    ' AK Gruppierung
                        Return Filter & " AND Gruppierung = " & Gruppierung
                    End If
                Case "4"    ' Gewichtsklassen
                    Return "Wettkampf LIKE '%" & WK & "%' AND Sex = '" & Sex & "' AND GK = " & idGK
                Case "5"    ' weiblich
                    Return "Wettkampf LIKE '%" & WK & "%' AND Sex = 'w'"
                Case Else    ' Wettkampf
                    Return "Wettkampf LIKE '%" & WK & "%'"
            End Select
        End Function

        Public Function TeamWertung(Results As DataTable, Optional Prognose As Boolean = False) As DataTable
            ' Rückgabe: DataTable --> die Orignaltabelle kann nicht editiert werden,
            '                         da die Function auch von Prognose verwendet wird

            Dim dtTeams As DataTable
            If Prognose Then
                dtTeams = Glob.dtTeams.Copy
            Else
                dtTeams = Glob.dtTeams
            End If
            Dim Summe(dtTeams.Rows.Count) As Double?
            Dim WertungsDG = {"Reissen", "Stossen", "Heben"}
            Dim Teams As New Dictionary(Of Integer, Integer()) ' (Id, Siegpunkte)

#Region "fertige Durchgänge bestimmen"
            Dim Wertung = {"RW_3", "SW_3"}
            Dim complete = {True, True, True}
            If Not Prognose Then
                For i = 0 To 1
                    Dim ii = i
                    complete(i) = (From x In Results
                                   Where Not x.Field(Of Integer?)("Gruppe") = 1000 AndAlso IsNothing(x.Field(Of Object)(Wertung(ii)))).Count = 0
                Next
                complete(2) = complete(0) AndAlso complete(1)
            End If
#End Region

#Region "Summen aller Resultate"
            For Each row As DataRow In dtTeams.Rows
                For i = 0 To WertungsDG.Count - 1
                    Dim ii = i
                    Try
                        Summe(ii) = (From x In Results
                                     Where x.Field(Of Integer)("idVerein") = CInt(row!Team) AndAlso Not x.Field(Of Boolean)("a_K")
                                     Select x.Field(Of Double?)(WertungsDG(ii))).Sum()
                        row(WertungsDG(ii)) = Summe(ii)
                    Catch ex As Exception
                    End Try
                Next
                'range.Add(CInt(row!Team))
                Teams.Add(CInt(row!Team), {0, 0})
            Next
            dtTeams.AcceptChanges()
#End Region

#Region "Siegpunkte vergeben"
            Dim dg = -1
            For Each d In WertungsDG
                dg += 1
                If complete(dg) Then
                    Dim c = 0
                    For Each t As Combination In TeamCombinations
                        Try
                            If Not IsNothing(dvLeader(0)) AndAlso CDbl(dtTeams(bsTeam.Find("Team", t.a))(d)) = CDbl(dtTeams(bsTeam.Find("Team", t.b))(d)) Then
                                ' unentschieden --> das Team, dass die Punkte zuerst erreicht hat, gewinnt den WertungsDG
                                ' --> MaxZeiten der Teams auslesen
                                Dim Zeiten = From x In Results
                                             Where Not x.Field(Of Boolean)("a_K") AndAlso (x.Field(Of Integer)("idVerein") = t.a Or x.Field(Of Integer)("idVerein") = t.b)
                                             Group By Team = x.Field(Of Integer)("idVerein") Into Group
                                             Select Team, Reissen = (From r In Group Select r.Field(Of Date?)("Reissen_Zeit")).Max,
                                                      Stossen = (From s In Group Select s.Field(Of Date?)("Stossen_Zeit")).Max
                                If Zeiten.Count > 0 Then
                                    If d.Equals("Reissen") AndAlso IsNothing(Zeiten(0).Reissen) AndAlso IsNothing(Zeiten(1).Reissen) OrElse
                                             Not d.Equals("Reissen") AndAlso IsNothing(Zeiten(0).Stossen) AndAlso IsNothing(Zeiten(1).Stossen) Then
                                        ' bei NULL-Zeiten beider Teams im Reißen oder Stoßen/Heben (kommt nur bei Prognose vor) 
                                        ' --> MaxSortierreihenfolge auslesen
                                        Dim SortOrder = From x In dvLeader(If(d.Equals("Reissen"), 0, 1)).ToTable
                                                        Group By Team = x.Field(Of Integer)("idVerein") Into Group
                                                        Select Team, (From r In Group Select r.Field(Of Integer)("Reihenfolge")).Max
                                        ' kleinere Nummer in der Reihenfolge gewinnt
                                        Teams(SortOrder(0).Team)(c) += Abs(CInt(SortOrder(0).Max < SortOrder(1).Max))
                                        Teams(SortOrder(1).Team)(c) += Abs(CInt(SortOrder(0).Max > SortOrder(1).Max))
                                    ElseIf d.Equals("Reissen") Then
                                        ' Reißen: vorhanden gegen Nothing --> vorhanden gewinnt (kommt nur bei Prognose vor), Zeiten vergleichen --> niedrigere Zeit gewinnt
                                        Teams(Zeiten(0).Team)(c) += Abs(CInt(IsNothing(Zeiten(1).Reissen) OrElse Zeiten(0).Reissen < Zeiten(1).Reissen))
                                        Teams(Zeiten(1).Team)(c) += Abs(CInt(IsNothing(Zeiten(0).Reissen) OrElse Zeiten(0).Reissen > Zeiten(1).Reissen))
                                    Else
                                        ' Stoßen: vorhanden gegen Nothing --> vorhanden gewinnt (kommt nur bei Prognose vor), Zeiten vergleichen --> niedrigere Zeit gewinnt
                                        Teams(Zeiten(0).Team)(c) += Abs(CInt(IsNothing(Zeiten(1).Stossen) OrElse Zeiten(0).Stossen < Zeiten(1).Stossen))
                                        Teams(Zeiten(1).Team)(c) += Abs(CInt(IsNothing(Zeiten(0).Stossen) OrElse Zeiten(0).Stossen > Zeiten(1).Stossen))
                                    End If
                                End If
                            Else
                                Teams(t.a)(c) += Abs(CInt(CDbl(dtTeams(bsTeam.Find("Team", t.a))(d)) > CDbl(dtTeams(bsTeam.Find("Team", t.b))(d))))
                                Teams(t.b)(c) += Abs(CInt(CDbl(dtTeams(bsTeam.Find("Team", t.a))(d)) < CDbl(dtTeams(bsTeam.Find("Team", t.b))(d))))
                            End If
                        Catch ex As Exception
                        End Try
                        c = 1
                    Next
                End If
            Next
            For Each key As Integer In Teams.Keys
                dtTeams(bsTeam.Find("Team", key))!Punkte = Teams(key)(0)
                dtTeams(bsTeam.Find("Team", key))!Punkte2 = IIf(Wettkampf.Finale, Teams(key)(1), DBNull.Value)
            Next
            dtTeams.AcceptChanges()
#End Region

#Region "Platzierung"

            If complete(0) And complete(1) And complete(2) Then
                Dim TeamOrder = (From x In dtTeams
                                 Order By x.Field(Of Double)("Heben") Descending
                                 Select x.Field(Of Integer)("Team")).ToList()
                For Platz = 1 To TeamOrder.Count
                    dtTeams(bsTeam.Find("Team", TeamOrder(Platz - 1)))!Sieger = Platz = 1
                    dtTeams(bsTeam.Find("Team", TeamOrder(Platz - 1)))!Platz = Platz
                Next
                dtTeams.AcceptChanges()
                ' RaiseEvent Sieger_anzeigen ############################################################################################################################
            End If
#End Region

            Return dtTeams

        End Function

        Public Function ZK_Resultat(TN As Integer, Optional table As DataTable() = Nothing) As Integer?

            'If TN = 0 OrElse pos = -1 Then Return Nothing
            If TN = 0 Then Return Nothing

            Dim pos = bsResults.Find("Teilnehmer", TN)
            Dim ZK(1) As Integer

            If IsNothing(table) Then
                Try
                    ZK(0) = If(IsDBNull(dvResults(pos)!maxR), 0, CInt(dvResults(pos)!maxR))
                    ZK(1) = If(IsDBNull(dvResults(pos)!maxS), 0, CInt(dvResults(pos)!maxS))
                Catch ex As Exception
                End Try
            Else
                For i = 0 To 1
                    Try
                        ZK(i) = (From x In table(i)
                                 Where x.Field(Of Integer)("Teilnehmer") = TN And x.Field(Of Integer?)("Wertung") = 1
                                 Select x.Field(Of Integer)("HLast")).Max
                    Catch ex As Exception
                        ZK(i) = 0
                    End Try
                Next
            End If

            If ZK(0) > 0 AndAlso ZK(1) > 0 Then
                'dvResults(pos)!ZK = ZK(0) + ZK(1)
                Return ZK(0) + ZK(1)
            Else
                'dvResults(pos)!ZK = DBNull.Value
                Return Nothing
            End If

            'bsResults.EndEdit()
        End Function

#Region "IDisposable Support"
        Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
                End If

                ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
                ' TODO: große Felder auf Null setzen.
            End If
            disposedValue = True
        End Sub

        ' TODO: Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
        'Protected Overrides Sub Finalize()
        '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
            Dispose(True)
            ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
            ' GC.SuppressFinalize(Me)
        End Sub
#End Region
    End Class

    Public Enum Position
        TopLeft = 1
        TopRight = 2
        CenterMiddle = 4
        BottomLeft = 8
        BottomRight = 16
    End Enum

    Class myFunction
        '' Globale Funktionen
        Implements IDisposable

        Public Sub New()
        End Sub

        Public Function Format_Techniknote(Value As String) As String
            Select Case Value
                Case "10,00"
                    Return "10,0"
                Case "0", "0,00"
                    Return "X,XX"
                Case Else
                    Return Strings.Format(Value, "Fixed")
            End Select
        End Function

        Public Function Format_AK(AK As String, sex As String) As String
            ' bei Masters die AK entfernen
            If AK.Contains("(") Then AK = Trim(Split(AK, "(")(0))
            'Mid(AK, 1, InStr(AK, " (") - 1)
            ' Geschlecht anhängen
            AK += "_" + sex
            '' Umlaute entfernen (Schüler)
            'AK = AK.Replace("ü", "ue")
            Return AK
        End Function

        Public Function Wettkampf_Auswahl(myForm As Form, Optional Caption As String = "Wettkampf auswählen", Optional MultiSelect As Boolean = False, Optional Get_Id_Only As Boolean = False) As KeyValuePair(Of String, String)
            ' OnlyID    True  = Wettkampf ist aktiv und wird zum Bearbeiten erneut geladen  --> neuen WK anlegen nicht möglich
            '           False = Wettkampf wird geöffnet                                     --> neuer WK kann angelegt werden
            Using WettkampfLaden As New gvWettkampf
                With WettkampfLaden
                    .Get_Id_Only = Get_Id_Only
                    .Text = Caption
                    .dgvWK.MultiSelect = MultiSelect
                    .ShowDialog(myForm)
                    If Not IsNothing(.SelectedWk) Then Return .SelectedWk
                End With
            End Using
            Return Nothing
        End Function

        Public Function Get_Schedule() As DataTable

            Dim cmd As MySqlCommand
            Dim dtTmp As New DataTable
            Dim dtTbl As New DataTable

            Dim Query = "select m.Gruppe, g.Bezeichnung, g.DatumW WiegenDatum, g.WiegenBeginn, g.WiegenEnde, g.Datum, g.Reissen, g.Stossen, g.DatumA, g.Athletik, " &
                    "(SELECT group_concat(concat(a.Geschlecht, ' ', m.GK,' ',m.idGK))) GKs, count(m.Teilnehmer) TN, w.DatumBis " &
                    "from meldung m " &
                    "left join athleten a on a.idTeilnehmer = m.Teilnehmer " &
                    "Left join gruppen g on g.Wettkampf = m.Wettkampf and g.Gruppe = m.Gruppe " &
                    "left join wettkampf w on w.Wettkampf = m.Wettkampf " &
                    "where m.Wettkampf = " & Wettkampf.ID & " and not isnull(m.GK) " &
                    "group by m.Gruppe " &
                    "order by m.Gruppe, a.Geschlecht DESC, m.idGK;"
            Using conn As New MySqlConnection(User.ConnString)
                Try
                    If Not conn.State = ConnectionState.Open Then conn.Open()
                    cmd = New MySqlCommand(Query, conn)
                    dtTmp.Load(cmd.ExecuteReader)
                    cmd = New MySqlCommand("select * from wettkampf_zeitplan where Wettkampf = " & Wettkampf.ID & ";", conn)
                    dtTbl.Load(cmd.ExecuteReader)
                    ConnError = False
                Catch ex As MySqlException
                    LogMessage("Zeitplan: Daten: " & ex.Message, ex.StackTrace)
                End Try
            End Using

            If dtTmp.Rows.Count = 0 Then Return Nothing

            Dim dtPlan = New DataTable
            With dtPlan.Columns
                .Add("Gruppe", GetType(Integer))
                .Add("Datum", GetType(Date))
                .Add("Beginn", GetType(Date))
                .Add("Ende", GetType(Date))
                .Add("Bezeichnung", GetType(String))
                .Add("TN", GetType(Integer))
                .Add("Gewichtsklassen", GetType(String))
                .Add("Drucken", GetType(Boolean))
                .Add("GruppeOld", GetType(Integer))
            End With

            Dim dicGK As New Dictionary(Of String, SortedList(Of Integer, String))
            dicGK.Add("w", New SortedList(Of Integer, String))
            dicGK.Add("m", New SortedList(Of Integer, String))

            For Each row As DataRow In dtTmp.Rows
                dicGK("w").Clear()
                dicGK("m").Clear()
                Dim GKs = Split(row!GKs.ToString, ",")
                For Each item In GKs
                    Dim GK = Split(item, " ")
                    If Not dicGK(GK(0)).ContainsKey(CInt(GK(2))) Then dicGK(GK(0)).Add(CInt(GK(2)), GK(1))
                Next
                Dim gk_w = String.Empty
                Dim gk_m = String.Empty
                If dicGK("w").Count > 0 Then gk_w = If(dicGK("m").Count > 0, "weibl.: ", "") + String.Join(", ", dicGK("w").Values)
                If dicGK("m").Count > 0 Then gk_m = If(dicGK("w").Count > 0, "männl.: ", "") + String.Join(", ", dicGK("m").Values)
                ' Wiegen
                Dim gk_ges = String.Concat(gk_w, If(Not String.IsNullOrEmpty(gk_w) AndAlso Not String.IsNullOrEmpty(gk_m), "; ", ""), gk_m)
                dtPlan.Rows.Add({row!Gruppe, row!WiegenDatum, row!WiegenBeginn, row!WiegenEnde, "Wiegen Gr. " & row!Gruppe.ToString & ": " + row!Bezeichnung.ToString, DBNull.Value, gk_ges, True, row!Gruppe})
                ' WK-Beginn
                dtPlan.Rows.Add({row!Gruppe, row!Datum, row!Reissen, DBNull.Value, "Wettkampf-Beginn Gr. " & row!Gruppe.ToString & ": " + row!Bezeichnung.ToString, row!TN, DBNull.Value, True, row!Gruppe})
                ' Athletik
                If Wettkampf.Athletik Then dtPlan.Rows.Add({row!Gruppe, row!DatumA, row!Athletik, DBNull.Value, "Athletik Gr. " & row!Gruppe.ToString & ": " + row!Bezeichnung.ToString, row!TN, DBNull.Value, True, row!Gruppe})
            Next

            For Each row As DataRow In dtTbl.Rows
                dtPlan.Rows.Add({row!Gruppe, row!Datum, row!Beginn, row!Ende, row!Bezeichnung, DBNull.Value, DBNull.Value, True, DBNull.Value})
            Next

            Return dtPlan

        End Function

        'Public Function Generate_Startnumbers(ByRef dt As DataTable, Optional SortCol As String = "") As Boolean

        '    ' Startnummern werden beim Wiegen automatisch vergeben
        '    ' wenn Losnummern neu vergeben werden, werden die Startnummern auf NULL gesetzt

        '    ' dv mit allen indizierbaren Athleten (attend <> False und nicht aus Meldung gelöscht (RowState = CurrentRows))
        '    Dim tmp = New DataView(dt, "Convert(IsNull(attend,''), System.String) = '' OR attend = True", "Losnummer, Meldedatum", DataViewRowState.CurrentRows)

        '    Dim LotNumbersMissing = (From x In tmp.ToTable
        '                             Where IsNothing(x.Field(Of Integer)("Losnummer"))).Count > 0

        '    If LotNumbersMissing Then
        '        If IsNothing(Generate_Lotnumbers(dt)) Then Return False
        '    End If

        '    If Wettkampf.Mannschaft Then
        '        Dim num As New Dictionary(Of Integer, Integer)
        '        Dim start = 1
        '        Try
        '            Dim rec = (From x In tmp.ToTable
        '                       Group By team = x.Field(Of Long)("Team") Into Group
        '                       Select key = team, value = Group.Count).ToDictionary(Function(p) p.key, Function(p) p.value)
        '            For Each row As DataRow In Glob.dtTeams.Rows
        '                num(CInt(row!Team)) = start
        '                start += rec(CInt(row!Team))
        '            Next
        '            For Each row As DataRowView In tmp
        '                Dim team = CInt(row!Team)
        '                row!Startnummer = num(team)
        '                num(team) += 1
        '            Next
        '        Catch ex As Exception ' könnte "x.Field(Of Long)("Team")" sein
        '            LogMessage("Global:Generate_Startnumbers / " & ex.Message & " / StackTrace " &
        '                If(InStr(ex.StackTrace, "Zeile") > 0, Mid(ex.StackTrace, InStr(ex.StackTrace, "Zeile")), String.Empty),
        '                Path.Combine(Application.StartupPath, "log", "err_" & Format(Now, "yyyy-MM-dd") & ".log"))
        '            Return False
        '        End Try
        '        Return True
        '    Else

        '    End If

        '    Return True
        'End Function

        Public Function Generate_Lotnumbers(Count As Integer) As List(Of Integer)

            '6.3. Lot Numbers
            '6.3.1. On completion of the Start List, a randomly generated lot number Is drawn for each verified Lifter. The
            'Lifters retain the lot number throughout the Event. The lot number defines the order of the weigh-in And the
            'order of lifting during the competition in the Lifter's relevant group.

            'Dim LotNumbers As New List(Of Integer)
            Dim TN_Lot As New List(Of Integer)
            Dim Generator As New Random()
            Dim LotNumbers As List(Of Integer) = Enumerable.Range(1, Count).ToList

            Dim t As Integer
            Try
                Do While LotNumbers.Count > 0
                    t = Generator.Next(1, LotNumbers.Count + 1)
                    TN_Lot.Add(LotNumbers(t - 1))
                    LotNumbers.RemoveAt(t - 1)
                Loop
                Return TN_Lot
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Function String_Shorten(oString As String, StringFont As Font, MaxWidth As Integer, Optional KeepAlways As String = "") As String

            Dim Parts = Split(oString, " ").ToList
            Dim Arr As String()

            For i = 1 To Parts.Count
                ReDim Arr(i - 1)
                Parts.CopyTo(0, Arr, 0, i)
                Dim NewString = Join(Arr, " ") + KeepAlways
                Dim TextWidth = TextRenderer.MeasureText(NewString, StringFont).Width
                If TextWidth > MaxWidth Then
                    ReDim Arr(i - 2)
                    Parts.CopyTo(0, Arr, 0, i - 1)
                    Return Join(Arr, " ") + KeepAlways
                End If
            Next

            Return oString + KeepAlways
        End Function

        Public Function Get_VereinInfo(TN As String) As DataTable
            Using conn As New MySqlConnection(User.ConnString)
                ' bei Ausländern Land, ansonsten Verein selektieren
                Dim Query = "Select v.idVerein, If(v.idVerein > 0, IfNull(v.Kurzname, v.Vereinsname), s.state_name) Vereinsname, IfNull(r.Kurz, r.region_3) Verband, r.region_3, r.region_name, r.region_flag, s.state_iso3, s.state_name, s.state_flag " &
                            "FROM athleten a " &
                            "LEFT JOIN verein v ON v.idVerein = If(0, IfNull(a.ESR, a.Verein), IfNull(a.MSR, a.Verein)) " &
                            "Left Join region r ON r. region_id = v.Region " &
                            "Left Join staaten s On s.state_id = a.Staat " &
                            "WHERE a.idTeilnehmer = " & TN & ";"
                conn.Open()
                Dim cmd = New MySqlCommand(Query, conn)
                Dim tmp As New DataTable
                Try
                    tmp.Load(cmd.ExecuteReader)
                    Return tmp
                Catch ex As Exception
                    Return Nothing
                End Try
            End Using
        End Function

        Public Function Get_Location(ParentGrid As DataGridView, ColumnIndex As Integer, RowIndex As Integer,
                                     Optional Position As Position = Position.BottomLeft,
                                     Optional OffsetLeft As Integer = 0,
                                     Optional OffsetTop As Integer = 0,
                                     Optional ChildBounds As Rectangle = Nothing) As Point
            ' Usage: ctl.FindForm.PointToClient(Get_Location(ParentGrid, ...))

            With ParentGrid
                Dim rect = .GetCellDisplayRectangle(ColumnIndex, RowIndex, True)
                Select Case Position
                    Case Position.TopLeft
                        Return .PointToScreen(New Point(rect.X, rect.Y))
                    Case Position.TopRight
                        Return .PointToScreen(New Point(rect.X + rect.Width, rect.Y))
                    Case Position.CenterMiddle
                        If IsNothing(ChildBounds) Then
                            Return Get_Location(ParentGrid, ColumnIndex, RowIndex, Position.TopLeft)
                        Else

                        End If
                    Case Position.BottomLeft
                        Return .PointToScreen(New Point(rect.X, rect.Y + rect.Height))
                    Case Position.BottomRight
                        Return .PointToScreen(New Point(rect.X + rect.Width, rect.Y + rect.Height))
                End Select
            End With
        End Function

        Public Function Show_a_K() As Boolean
            Return (From x In dvResults.ToTable
                    Group x By g = x.Field(Of Boolean)("a_K") Into Group).Count > 1
        End Function

        Public Function Get_Comment(A_K_Lifters As DataRow()) As String
            If A_K_Lifters.Count = 0 Then Return String.Empty
            Dim comment = "Resultate a.K.:" & vbNewLine
            Dim dg = {"R", "S"}
            Dim R_present As Boolean
            For Each row As DataRow In A_K_Lifters
                comment += row!Nachname.ToString + ", " + row!Vorname.ToString + ": "
                R_present = False
                For d = 0 To 1
                    Dim lst = New List(Of String)
                    If Not IsDBNull(row(dg(d) & "_1")) AndAlso Not CInt(row(dg(d) & "_1")) = 0 Then
                        If R_present Then comment += "; "
                        R_present = d = 0
                        comment += dg(d) + ": "
                        For i = 1 To 3
                            If IsDBNull(row(dg(d) & "_" & i)) Then
                                lst.Add("---")
                            Else
                                lst.Add((CInt(row(dg(d) & "_" & i)) * CInt(row(dg(d) & "W_" & i))).ToString)
                            End If
                        Next
                        comment += Join(lst.ToArray, ", ")
                    End If
                Next
                comment += vbNewLine
            Next
            Return comment
        End Function

        Public Function Get_ServerIP(myForm As Form) As String
            Using conn As New MySqlConnection(User.ConnString)
                Do
                    Try
                        Dim cmd As MySqlCommand
                        cmd = New MySqlCommand("SELECT * FROM users;", conn)
                        If Not conn.State = ConnectionState.Open Then conn.Open()
                        dtUser = New DataTable
                        dtUser.Load(cmd.ExecuteReader())
                        ConnError = False
                    Catch ex As MySqlException
                        If MySQl_Error(myForm, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                    End Try
                Loop While ConnError
            End Using
            If ConnError Then Return String.Empty
            Return Set_ServerIP()
        End Function
        Public Function Set_ServerIP() As String
            If IsNothing(dtUser) Then Return String.Empty
            Dim row As DataRow() = dtUser.Select("UserId = " & UserID.Wettkampfleiter & " AND Bohle = " & User.Bohle)
            If Not row.Count = 0 Then Return row(0)!Server.ToString
            Return String.Empty
        End Function

        Public Function Get_LastId(table As String, field As String) As Integer?
            Dim id As Integer?
            Using conn As New MySqlConnection(User.ConnString)
                Try
                    Dim cmd = New MySqlCommand("select max(" & field & ") id from " & table & ";", conn)
                    If Not conn.State = ConnectionState.Open Then conn.Open()
                    Dim reader = cmd.ExecuteReader()
                    If reader.HasRows Then
                        reader.Read()
                        If IsDBNull(reader!id) Then
                            id = 0
                        Else
                            id = CInt(reader!id)
                        End If
                    End If
                    reader.Close()
                Catch ex As MySqlException
                    id = Nothing
                    'MessageBox.Show(ex.Message)
                End Try
            End Using
            Return id
        End Function

        Public Function IsNan(KeyCode As Keys, Optional AllowComma As Boolean = False, Optional AllowDot As Boolean = False) As Boolean
            Select Case KeyCode
                Case Keys.D0 To Keys.D9, Keys.NumPad0 To Keys.NumPad9, Keys.Back
                    Return False
                Case Keys.Oemcomma, Keys.Decimal
                    Return Not AllowComma
                Case Keys.OemPeriod
                    Return Not AllowDot
            End Select
            Return True
        End Function


#Region "IDisposable Support"
        Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
                End If

                ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
                ' TODO: große Felder auf Null setzen.
            End If
            disposedValue = True
        End Sub

        ' TODO: Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
        'Protected Overrides Sub Finalize()
        '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
            Dispose(True)
            ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
            ' GC.SuppressFinalize(Me)
        End Sub
#End Region
    End Class

    Public Class IPcalc

        Implements IDisposable

        Public Function Mask2CIDR(octets As Byte()) As Integer

            Dim maskL As Long 'accumulator for address

            For x As Integer = 0 To 3
                maskL = maskL Or (CLng(octets(x)) << (3 - x) * 8)
            Next

            Dim CIDR = 0
            Dim oneBit As Long = &H80000000L

            For x As Integer = 31 To 0 Step -1
                If (maskL And oneBit) = oneBit Then CIDR += 1 Else Exit For
                oneBit >>= 1
            Next

            Return CIDR

        End Function

        Public Function Get_NA_BC_NP(IP As String, CIDR As Integer) As String()
            ' NA = NetAddress
            ' BC = BroadCast
            ' NP = NetPart of IP (alle Oktete der SM, die 255 sind)

            Dim address = IPAddress.Parse(IP)
            Dim netMaskInt As Integer = 0
            Dim suffix As Integer = CIDR

            For i = 31 To 32 - suffix Step -1
                netMaskInt = netMaskInt Or (1 << i)
            Next

            Dim NetMask() As Byte = BitConverter.GetBytes(netMaskInt).Reverse.ToArray()
            Dim NetWork(3) As Byte
            Dim BroadCast(3) As Byte

            For i = 0 To 3
                NetWork(i) = address.GetAddressBytes()(i) And NetMask(i)
                BroadCast(i) = address.GetAddressBytes()(i) Or Not NetMask(i)
            Next

            Dim NA As New IPAddress(NetWork)
            Dim BC As New IPAddress(BroadCast)
            Dim maxHosts = Abs(netMaskInt) - 2

            Dim s = Split(IP, ".").ToArray
            ReDim Preserve s(CIDR \ 8 - 1)
            Dim NP = Join(s, ".") + "."

            Return {NA.ToString, BC.ToString, NP}

        End Function

#Region "IDisposable Support"
        Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
                End If

                ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
                ' TODO: große Felder auf Null setzen.
            End If
            disposedValue = True
        End Sub

        ' TODO: Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
        'Protected Overrides Sub Finalize()
        '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
            Dispose(True)
            ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
            ' GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

    Public Class NetWork
        ' https://www.experts-exchange.com/articles/11178/A-Peer-To-Peer-LAN-Chat-Application-in-Visual-Basic-Net-using-TcpClient-and-TcpListener.html

        Public Const SV_TYPE_ALL As Integer = &HFFFFFFFF
        Public Clients As List(Of String)

        Public Structure SERVER_INFO_101
            Public Platform_ID As Integer
            <MarshalAs(UnmanagedType.LPWStr)> Public Name As String
            Public Version_Major As Integer
            Public Version_Minor As Integer
            Public Type As Integer
            <MarshalAs(UnmanagedType.LPWStr)> Public Comment As String
        End Structure

        Public Declare Unicode Function NetServerEnum Lib "Netapi32.dll" (
        ByVal Servername As Integer, ByVal Level As Integer, ByRef Buffer As IntPtr, ByVal PrefMaxLen As Integer,
        ByRef EntriesRead As Integer, ByRef TotalEntries As Integer, ByVal ServerType As Integer,
        ByVal DomainName As String, ByRef ResumeHandle As Integer) As Integer

        Public Declare Function NetApiBufferFree Lib "Netapi32.dll" (ByVal lpBuffer As IntPtr) As Integer

        Public Shared Function GetNetworkComputers(Optional ByVal DomainName As String = Nothing) As DataTable
            Dim ServerInfo As New SERVER_INFO_101
            Dim MaxLenPref As Integer = -1
            Dim level As Integer = 101
            Dim ResumeHandle As Integer = 0
            Dim ret, EntriesRead, TotalEntries As Integer
            Dim BufPtr As IntPtr

            Dim dt As New DataTable
            dt.Columns.Add("IPs")
            dt.Columns.Add("PCs")
            Try
                ret = NetServerEnum(0, level, BufPtr, MaxLenPref, EntriesRead, TotalEntries, SV_TYPE_ALL, DomainName, ResumeHandle)
                If ret = 0 Then
                    For i As Integer = 0 To EntriesRead - 1
                        ServerInfo = CType(Marshal.PtrToStructure(IncrementPointer(BufPtr, i * Marshal.SizeOf(ServerInfo)), GetType(SERVER_INFO_101)), SERVER_INFO_101)
                        Dim Rw As DataRow = dt.NewRow

                        Dim IPstring As String = ""
                        For Each ip As Net.IPAddress In Net.Dns.GetHostAddresses(ServerInfo.Name)
                            'Debug.WriteLine(ip.ToString)
                            If Not ip.IsIPv6LinkLocal Then
                                IPstring = IPstring & ip.ToString
                            End If
                        Next
                        Rw(0) = IPstring.ToString
                        Rw(1) = ServerInfo.Name.ToString
                        dt.Rows.Add(Rw)
                    Next
                End If
                NetApiBufferFree(BufPtr)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            Return dt
        End Function

        Private Shared Function IncrementPointer(ByVal ptr As IntPtr, ByVal i As Integer) As IntPtr
            If IntPtr.Size = 4 Then
                Return New IntPtr(ptr.ToInt32 + i)
            End If
            If IntPtr.Size = 8 Then
                Return New IntPtr(ptr.ToInt64 + i)
            End If
        End Function

        Public Function Get_Computers(Optional ByVal DomainName As String = Nothing) As List(Of String) ' Handles btnGet.Click
            'LstBx.DataSource = GetNetworkComputers()
            Clients = New List(Of String)
            Dim dtt As DataTable = GetNetworkComputers(DomainName)
            If dtt.Rows.Count > 0 Then
                For i As Integer = 0 To dtt.Rows.Count - 1
                    Clients.Add(dtt.Rows(i)(0).ToString & ":" & dtt.Rows(i)(1).ToString)
                Next
            End If
            dtt.Dispose()
            Return Clients
        End Function

    End Class


End Module
