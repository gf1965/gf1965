﻿
Imports MySqlConnector

Public Class Export_Ergebnisse

    Dim Query As String
    Dim cmd As MySqlCommand
    Dim _path As String

    Dim dtR As DataTable
    Dim dtS As DataTable
    Dim dtRes As DataTable
    Dim dtA As DataTable
    Dim dtResA As DataTable

    Private _checkerHeber As Integer
    Private _checkerAthletik As Integer
    Private Property checkerHeben As Integer
        Get
            Return _checkerHeber
        End Get
        Set(value As Integer)
            _checkerHeber = value
            If value = 0 Then
                chkHeben.CheckState = CheckState.Unchecked
            ElseIf value = dgvErgebnisse.Rows.Count Then
                chkHeben.CheckState = CheckState.Checked
            Else
                chkHeben.CheckState = CheckState.Indeterminate
            End If
        End Set
    End Property
    Private Property checkerAthletik As Integer
        Get
            Return _checkerAthletik
        End Get
        Set(value As Integer)
            _checkerAthletik = value
            If value = 0 Then
                chkAthletik.CheckState = CheckState.Unchecked
            ElseIf value = dgvErgebnisse.Rows.Count Then
                chkAthletik.CheckState = CheckState.Checked
            Else
                chkAthletik.CheckState = CheckState.Indeterminate
            End If
        End Set
    End Property

    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        dgvErgebnisse.Columns("Athletik").Visible = Wettkampf.Athletik
        chkAthletik.Visible = Wettkampf.Athletik
    End Sub

    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        dgvErgebnisse_Fill()
    End Sub

    Private Sub btnWK_Click(sender As Object, e As EventArgs) Handles btnWK.Click

        Using f As New myFunction
            Dim WK_ID = f.Wettkampf_Auswahl(Me)
            If WK_ID.Equals(0) Then Return
            If Wettkampf.Set_Properties(Me, WK_ID.Key.ToString) = 0 Then
                'Error 
                'else
                'WK_Load_Tables
            End If
        End Using

        dgvErgebnisse.Columns("Athletik").Visible = Wettkampf.Athletik
        chkAthletik.Visible = Wettkampf.Athletik
        dgvErgebnisse_Fill()

    End Sub

    Private Sub dgvErgebnisse_Fill()
        Cursor = Cursors.WaitCursor

        Dim Durchgang = {"reissen", "stossen"}

#Region "Data"
        Using conn As New MySqlConnection(User.ConnString)
            conn.Open()
            For Each dg In Durchgang
                Dim d = dg.Substring(0, 1)
                Query = "SELECT m.Wettkampf, m.Gruppe, m.Teilnehmer, x.Versuch, x.HLast, x.Diff, x.Steigerung1, x.Steigerung2, x.Steigerung3, x.Wertung, x.Zeit, x.Note, x.Lampen, x.Updated " &
                        "FROM " & dg & " " & d & " " &
                        "LEFT JOIN meldung m ON m.Wettkampf = x.Wettkampf AND m.Teilnehmer = x.Teilnehmer  " &
                        "WHERE x.Wettkampf = " & Wettkampf.ID & " AND m.attend = True " &
                        "ORDER BY m.Gruppe, m.Teilnehmer, x.Versuch;"
                Query = Query.Replace("x.", d & ".")
                cmd = New MySqlCommand(Query, conn)
                If d.Equals("r") Then
                    dtR = New DataTable
                    dtR.Load(cmd.ExecuteReader())
                ElseIf d.Equals("s") Then
                    dtS = New DataTable
                    dtS.Load(cmd.ExecuteReader())
                End If
            Next
            Query = "SELECT m.Wettkampf, m.Gruppe, g.Bezeichnung, m.Teilnehmer, r.Reissen, r.Reissen_Zeit, r.Reissen_Platz , r.Stossen, r.Stossen_Zeit, r.Stossen_Platz, r.ZK, r.Heben, r.Heben_Platz, r.Wertung2, r.Wertung2_Platz, r.Athletik, r.Gesamt, r.Gesamt_Platz, r.Updated " &
                    "FROM results r " &
                    "LEFT JOIN meldung m On m.Wettkampf = r.Wettkampf And m.Teilnehmer = r.Teilnehmer " &
                    "LEFT JOIN gruppen g ON g.Wettkampf = m.Wettkampf AND g.Gruppe = m.Gruppe " &
                    "WHERE r.Wettkampf = " & Wettkampf.ID & " AND m.attend = True " &
                    "ORDER BY m.Gruppe, m.Teilnehmer;"
            cmd = New MySqlCommand(Query, conn)
            dtRes = New DataTable
            dtRes.Load(cmd.ExecuteReader())

            If Wettkampf.Athletik Then
                Query = "SELECT m.Wettkampf, m.Gruppe, m.Teilnehmer, a.Disziplin, a.Durchgang, a.Wert, a.Updated " &
                        "FROM athletik a " &
                        "LEFT JOIN meldung m On m.Wettkampf = a.Wettkampf And m.Teilnehmer = a.Teilnehmer " &
                        "WHERE a.Wettkampf = " & Wettkampf.ID & " AND m.attend = True " &
                        "ORDER BY m.Gruppe, m.Teilnehmer, a.Disziplin, a.Durchgang;"
                cmd = New MySqlCommand(Query, conn)
                dtA = New DataTable
                dtA.Load(cmd.ExecuteReader())
                Query = "SELECT m.Wettkampf, m.Gruppe, m.Teilnehmer, a.Disziplin, a.Resultat, a.Platz, a.Updated " &
                        "FROM athletik_results a " &
                        "LEFT JOIN meldung m On m.Wettkampf = a.Wettkampf And m.Teilnehmer = a.Teilnehmer " &
                        "WHERE a.Wettkampf = " & Wettkampf.ID & " AND m.attend = True " &
                        "ORDER BY m.Gruppe, m.Teilnehmer, a.Disziplin;"
                cmd = New MySqlCommand(Query, conn)
                dtResA = New DataTable
                dtResA.Load(cmd.ExecuteReader())
            End If
        End Using
#End Region

        Dim dic = (From x In dtRes
                   Where Not IsDBNull(x.Field(Of Double?)("Reissen")) Or Not IsDBNull(x.Field(Of Double?)("Athletik"))
                   Group By Gruppe = x.Field(Of Integer)("Gruppe") Into Group).ToDictionary(Function(t) t.Gruppe, Function(t) t.Group)

        With dgvErgebnisse
            .Rows.Clear()

            For Each item In dic
                .Rows.Add({item.Key, item.Value(0)!Bezeichnung, False, False})
                With .Rows(.Rows.Count - 1)
                    .Cells("Heben").ReadOnly = True
                    .Cells("Athletik").ReadOnly = True
                    chkHeben.Enabled = False
                    chkAthletik.Enabled = False

                    For Each row As DataRow In item.Value
                        If Not IsDBNull(row!Reissen) Then
                            .Cells("Heben").ReadOnly = False
                            '.Cells("Heben").Value = True
                            'chkHeben.Checked = True
                            chkHeben.Enabled = True
                            Exit For
                        End If
                    Next
                    If Wettkampf.Athletik Then
                        For Each row As DataRow In item.Value
                            If Not IsDBNull(row!Athletik) Then
                                .Cells("Athletik").ReadOnly = False
                                '.Cells("Athletik").Value = True
                                'chkAthletik.Checked=true
                                chkAthletik.Enabled = True
                                Exit For
                            End If
                        Next
                    End If
                End With
            Next
        End With

        Cursor = Cursors.Default
    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click

#Region "Path"

        With FolderBrowserDialog1
            .Description = "Laufwerk/Verzeichnis für Export-Datei"
            .RootFolder = Environment.SpecialFolder.MyComputer
            .SelectedPath = If(String.IsNullOrEmpty(_path), .RootFolder.ToString, _path)
            If .ShowDialog() = DialogResult.Cancel Then Return
            _path = .SelectedPath
        End With

        Cursor = Cursors.WaitCursor

        If _path.Contains("GFHsoft Gewichtheben") Then
            If Not _path.Contains(Wettkampf.Bezeichnung) Then
                Dim p = Split(_path, "\").ToList
                Dim x = p.IndexOf("GFHsoft Gewichtheben")
                If x < p.Count - 1 Then p.RemoveRange(x + 1, p.Count - 1 - x)
                p.Add(Wettkampf.Bezeichnung)
                _path = Path.Combine(p.ToArray)
            End If
        Else
            Dim subdirs = Computer.FileSystem.GetDirectories(_path, FileIO.SearchOption.SearchTopLevelOnly).ToList
            For Each d In subdirs
                If d.Contains("GFHsoft Gewichtheben") Then
                    _path = Path.Combine(_path, "GFHsoft Gewichtheben")
                    Dim subsubdirs = Computer.FileSystem.GetDirectories(_path, FileIO.SearchOption.SearchTopLevelOnly).ToList
                    For Each sd In subsubdirs
                        If sd.Contains(Wettkampf.Bezeichnung) Then
                            _path = Path.Combine(_path, Wettkampf.Bezeichnung)
                            Exit For
                        End If
                    Next
                    Exit For
                End If
            Next
            If Not _path.Contains("GFHsoft Gewichtheben") Then _path = Path.Combine(_path, "GFHsoft Gewichtheben")
            If Not _path.Contains(Wettkampf.Bezeichnung) Then _path = Path.Combine(_path, Wettkampf.Bezeichnung)
        End If

        If Not Directory.Exists(_path) Then Directory.CreateDirectory(_path)
#End Region

#Region "Filter"
        Dim lstH As New List(Of String)
        Dim lstA As New List(Of String)
        Dim lstR As New List(Of String)

        For i = 0 To dgvErgebnisse.Rows.Count - 1
            With dgvErgebnisse("Heben", i)
                If Not .ReadOnly AndAlso CBool(.Value) Then
                    lstH.Add(dgvErgebnisse("Nummer", i).Value.ToString)
                    If Not lstR.Contains(dgvErgebnisse("Nummer", i).Value.ToString) Then lstR.Add(dgvErgebnisse("Nummer", i).Value.ToString)
                End If
            End With
            If Wettkampf.Athletik Then
                With dgvErgebnisse("Athletik", i)
                    If Not .ReadOnly AndAlso CBool(.Value) Then
                        lstA.Add(dgvErgebnisse("Nummer", i).Value.ToString)
                        If Not lstR.Contains(dgvErgebnisse("Nummer", i).Value.ToString) Then lstR.Add(dgvErgebnisse("Nummer", i).Value.ToString)
                    End If
                End With
            End If
        Next
        Dim FilterH = "Gruppe = -1"
        Dim FilterA = "Gruppe = -1"
        Dim FilterR = "Gruppe = -1"
        If lstH.Count > 0 Then FilterH = "Gruppe = " & Join(lstR.ToArray, " OR Gruppe = ")
        If lstA.Count > 0 Then FilterA = "Gruppe = " & Join(lstA.ToArray, " OR Gruppe = ")
        If lstR.Count > 0 Then FilterR = "Gruppe = " & Join(lstR.ToArray, " OR Gruppe = ")
#End Region

#Region "Data"
        Dim wk As New DataTable With {.TableName = "wettkampf"}
        Dim cols(2) As DataColumn
        cols(0) = New DataColumn("Id", GetType(Integer))
        cols(1) = New DataColumn("Bezeichnung", GetType(String))
        cols(2) = New DataColumn("Datum", GetType(Date))
        wk.Columns.AddRange(cols)
        wk.Rows.Add({Wettkampf.ID, Wettkampf.Bezeichnung, Wettkampf.Datum})

        Dim dvR = New DataView(dtR)
        Dim dvS = New DataView(dtS)
        Dim dvRes = New DataView(dtRes)
        Dim dvA = New DataView(dtA)
        Dim dvResA = New DataView(dtResA)

        dvR.RowFilter = FilterH
        dvS.RowFilter = FilterH
        dvA.RowFilter = FilterA
        dvResA.RowFilter = FilterA
        dvRes.RowFilter = FilterR

        Dim reissen = dvR.ToTable
        Dim stossen = dvS.ToTable
        Dim results = dvRes.ToTable
        Dim athletik = dvA.ToTable
        Dim athletik_results = dvResA.ToTable

        reissen.TableName = "reissen"
        stossen.TableName = "stossen"
        results.TableName = "results"
        athletik.TableName = "athletik"
        athletik_results.TableName = "athletik_results"

        Dim ds As DataSet = New DataSet
        ds.Tables.AddRange({wk, reissen, stossen, results, athletik, athletik_results})
#End Region

        ds.WriteXml(Path.Combine(_path, "Ergebnisse.xml"))

        Cursor = Cursors.Default
        Using New Centered_MessageBox(Me)
            Dim msg = "Export erfolgreich abgeschlossen." & vbNewLine & vbNewLine & "Verzeichnis der Export-Datei: " & vbNewLine & _path
            MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Using
    End Sub

    Private Sub chk_CheckedChanged(sender As Object, e As EventArgs) Handles chkHeben.CheckedChanged, chkAthletik.CheckedChanged
        Dim ctl = CType(sender, CheckBox)
        If ctl.Focused Then
            Dim col = ctl.Name.Substring(3)
            For Each row As DataGridViewRow In dgvErgebnisse.Rows
                row.Cells(col).Value = ctl.Checked
            Next
            If sender.Equals(chkHeben) Then
                checkerHeben = If(ctl.Checked, dgvErgebnisse.Rows.Count, 0)
            ElseIf sender.Equals(chkAthletik) Then
                checkerAthletik = If(ctl.Checked, dgvErgebnisse.Rows.Count, 0)
            End If
        End If
    End Sub

    Private Sub dgvErgebnisse_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvErgebnisse.CellContentClick
        If e.RowIndex < 0 Then Return
        dgvErgebnisse.EndEdit()
        If dgvErgebnisse(e.ColumnIndex, e.RowIndex).ReadOnly Then Return
        If dgvErgebnisse.Columns(e.ColumnIndex).Name.Equals("Heben") Then
            checkerHeben += Math.Abs(CInt(dgvErgebnisse(e.ColumnIndex, e.RowIndex).Value)) * 2 - 1
        ElseIf dgvErgebnisse.Columns(e.ColumnIndex).Name.Equals("Athletik") Then
            checkerAthletik += Math.Abs(CInt(dgvErgebnisse(e.ColumnIndex, e.RowIndex).Value)) * 2 - 1
        End If
    End Sub

End Class