﻿Imports System.ComponentModel
Imports MySqlConnector

Structure Correction
    Dim gruppe As Integer
    Dim durchgang As Integer
    Dim teilnehmer As Integer
    Dim angezeigt As Boolean
    Dim gewertet As Boolean
    Dim versuch As Dictionary(Of Integer, Attempt)
End Structure
Structure Attempt
    Dim versuch As Integer
    Dim hlast As Integer
    Dim diff As Integer
    Dim wertung As String
    Dim note As String
    Dim zeit As String
    Dim lampen As String
End Structure
Public Class frmKorrektur

    Dim dvHeben(1) As DataView
    Dim dvDurchgang As DataView ' Für Anzeige cboHeber
    Private dtGruppen As DataTable
    Private bsGruppen As BindingSource

    Dim Table() As String = {"Reissen", "Stossen"}
    Dim Versuch As Integer

    Dim pnlPos As Point
    Dim cmd As MySqlCommand
    Dim Query As String
    Private LockObject As New Object

    Dim Gültig As String = Chr(252)
    Dim Ungültig As String = Chr(251)
    Dim Verzicht As String = "l"

    Dim loading As Boolean = True
    Dim nonNumberEntered As Boolean

    Dim Versuche As New Dictionary(Of Integer, List(Of Control))

    'Private ReturnForClient As Correction  ' bei Return wird diese Liste an den Client gesendet

    Property CalledFromLeader As Boolean
    Property Teilnehmer As Integer ' aktueller Teilnehmer (wenn gewertet (RowNum = 1) --> negativer Wert)
    Property ScoredLifter As Integer
    Property Wiederholen As Boolean

    Property TechnikwertungEnabled As Boolean

    'Private Gruppe As Integer
    Private Durchgang As Integer
    Private dtHeben(1) As DataTable

    Private Gruppen As New List(Of String)

    Enum Ctls
        Last
        Wertung
        Gültig
        Ungültig
        Verzicht
        Technik
    End Enum

    '' Me
    Private Sub frmKorrektur_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If btnSave.Enabled Then
            Using New Centered_MessageBox(Me)
                Dim dlgResult = MessageBox.Show("Änderungen für " & cboHeber.Text & " übernehmen?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                Select Case dlgResult
                    Case DialogResult.Yes
                        Save()
                    Case DialogResult.Cancel
                        e.Cancel = True
                    Case DialogResult.No
                        dtHeben(Durchgang).RejectChanges()
                        btnSave.Enabled = False
                End Select
            End Using
        End If
        If Not e.Cancel AndAlso CalledFromLeader Then NativeMethods.INI_Write("Korrektur", "Location", Location.X & ";" & Location.Y, App_IniFile)
    End Sub
    Private Sub frmKorrektur_Load(sender As Object, e As EventArgs) Handles Me.Load

        ' Position
        Dim loc As Point
        If File.Exists(App_IniFile) AndAlso CalledFromLeader Then
            Dim Pos() As String = Split(NativeMethods.INI_Read("Korrektur", "Location", App_IniFile), ";")
            If Pos(0).ToString <> "?" Then loc = New Point(CInt(Pos(0)), CInt(Pos(1)))
        Else
            loc = New Point(120, 60)
        End If
        Location = FindForm.PointToClient(PointToScreen(loc))

        ' Korrektur-Controls
        Dim x As New List(Of Control)
        x.AddRange({txtLast1, txtWertung1, chkG1, chkU1, chkV1, txtTechnik1})
        Versuche.Add(1, x)
        x = New List(Of Control)
        x.AddRange({txtLast2, txtWertung2, chkG2, chkU2, chkV2, txtTechnik2})
        Versuche.Add(2, x)
        x = New List(Of Control)
        x.AddRange({txtLast3, txtWertung3, chkG3, chkU3, chkV3, txtTechnik3})
        Versuche.Add(3, x)

        ' alle Wertungen ausblenden
        For i = 1 To 3
            Versuche(i)(Ctls.Wertung).Text = String.Empty
        Next

    End Sub
    Private Sub frmKorrektur_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Cursor = Cursors.WaitCursor
        Refresh()

#Region "Daten"
        'If Not CalledFromLeader Then
        Using conn As New MySqlConnection(User.ConnString)
            For i = 0 To 1
                Query = "SELECT m.Teilnehmer, m.idAK, m.idGK, h.Versuch, h.HLast, h.Diff, h.Steigerung1, h.Steigerung2, h.Steigerung3, h.Wertung, h.Zeit, h.Note, h.Lampen, " &
                        "Concat(a.Nachname,', ', a.Vorname) Heber, Year(a.Geburtstag) Jahrgang, a.Geschlecht Sex, m.Gruppe, m.Wiegen, " &
                        "IfNull((Select Wertung FROM wettkampf_wertung WHERE Wettkampf = m.Wettkampf And idAK = m.idAK Limit 1), " &
                        "       (select Wertung from wettkampf left join modus on idModus = Modus where Wettkampf = m.Wettkampf Limit 1)) Auswertung, " &
                        "IfNull((Select Wertung2 FROM wettkampf_wertung WHERE Wettkampf = m.Wettkampf And idAK = m.idAK Limit 1), " &
                        "        IfNull((nullif(substr(w.Platzierung, 3, 1), '')), 0)) Auswertung2, " &
                        "IfNull((Select Gruppierung FROM wettkampf_wertung WHERE Wettkampf = m.Wettkampf And idAK = m.idAK Limit 1), 0) Gruppierung2 " &
                        "FROM meldung m " &
                        "LEFT JOIN " & LCase(Table(i)) & " h ON h.Wettkampf = m.Wettkampf AND h.Teilnehmer = m.Teilnehmer " &
                        "LEFT JOIN athleten a ON a.idTeilnehmer = m.Teilnehmer " &
                        "LEFT JOIN modus mo ON idModus = " & Wettkampf.Modus & " " &
                        "LEFT JOIN wettkampf w ON w.Wettkampf = m.Wettkampf " &
                        "WHERE m.Wettkampf = " & Wettkampf.ID & " AND not IsNull(h.Versuch) AND m.attend = True " &
                        "ORDER BY a.Nachname, a.Vorname, h.Versuch;"
                cmd = New MySqlCommand(Query, conn)
                Try
                    dtHeben(i) = New DataTable
                    If Not conn.State = ConnectionState.Open Then conn.Open()
                    dtHeben(i).Load(cmd.ExecuteReader())
                    ConnError = False
                Catch ex As MySqlException
                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then
                        Close()
                        Return
                    End If
                End Try
            Next

            Query = "SELECT Gruppe, Teilgruppe, WG, Bezeichnung 
                     FROM gruppen 
                     WHERE Wettkampf = " & Wettkampf.ID & ";"
            dtGruppen = New DataTable
            cmd = New MySqlCommand(Query, conn)
            Try
                If Not conn.State = ConnectionState.Open Then conn.Open()
                dtGruppen.Load(cmd.ExecuteReader())
                ConnError = False
            Catch ex As MySqlException
            End Try
        End Using
        'End If
#End Region

        If dtHeben(0).Rows.Count = 0 AndAlso dtHeben(1).Rows.Count = 0 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Heber mit bestätigter Teilnahme gefunden" & vbNewLine & "oder Tabelle mit Versuchsreihenfolge nicht angelegt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
            Close()
            Return
        End If

        'If Teilnehmer < 0 Then ScoredLifter = Math.Abs(Teilnehmer) ' Heber ist in der Anzeige und gewertet
        'Teilnehmer = Math.Abs(Teilnehmer)

        'If Not TechnikwertungEnabled Then
        '    Dim AK_Schüler = (From x In dtHeben(0)
        '                      Where x.Field(Of Integer)("idAK") < 30).Count > 0

        '    If AK_Schüler AndAlso Not Wettkampf.Technikwertung Then
        '        Using New Centered_MessageBox(Me)
        '            Dim DlgResult = MessageBox.Show("Unter den TN sind Heber der AK Schüler." + vbNewLine + "Technik-Wertung für diese TN aktivieren?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        '            TechnikwertungEnabled = DlgResult = DialogResult.Yes
        '        End Using
        '    End If
        'End If

        Durchgang = Leader.TableIx

        For i = 0 To 1
            dvHeben(i) = New DataView(dtHeben(i))
        Next

        dvDurchgang = New DataView(dtHeben(Durchgang).DefaultView.ToTable(True, {"Gruppe", "Heber", "Teilnehmer"}))
        With cboHeber
            .DataSource = dvDurchgang
            .DisplayMember = "Heber"
            .ValueMember = "Teilnehmer"
        End With

        'With dtGruppen
        '    Dim nr = .NewRow
        '    nr.ItemArray = {0, String.Empty, DBNull.Value, "[alle]"}
        '    .Rows.InsertAt(nr, 0)
        'End With
        bsGruppen = New BindingSource With {.DataSource = dtGruppen}

        With dgvGruppe
            .MultiSelect = True 'Not CalledFromLeader OrElse Not IsNothing(Leader.Gruppe) AndAlso Leader.Gruppe.Count > 1
            .AutoGenerateColumns = False
            .DataSource = bsGruppen
            Dim _rowcount = 8
            If bsGruppen.Count < 8 Then _rowcount = bsGruppen.Count
            .Height = _rowcount * .RowTemplate.Height + 3
        End With

        If Not Wettkampf.Mannschaft Then
            If Not CalledFromLeader OrElse IsNothing(Leader.Gruppe) OrElse Leader.Gruppe.Count = 0 Then
                Gruppen.Add(dtGruppen(0)!Gruppe.ToString)
            Else
                Gruppen.AddRange(Leader.Gruppe)
            End If

            Set_Gruppen()
        End If

        cboDurchgang.SelectedIndex = Durchgang

        cboHeber.SelectedIndex = -1

        loading = False

        ' Wertungen anzeigen
        'If Not CalledFromLeader Then
        '    Teilnehmer = CInt(cboHeber.SelectedValue)
        'End If

        ' Heber anzeigen
        If Not Teilnehmer = 0 Then cboHeber.SelectedValue = Teilnehmer

        'pnlTechnik.Enabled = Wettkampf.Technikwertung

        formMain.Change_MainProgressBarValue(0)

        Cursor = Cursors.Default
    End Sub

    '' NumericInput & ENTER
    Private Sub TextboxNumDecimal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtTechnik1.KeyDown, txtTechnik2.KeyDown, txtTechnik3.KeyDown
        nonNumberEntered = False

        Select Case e.KeyCode

            Case Keys.Enter, Keys.Down, Keys.Up
                Versuch = CInt(Strings.Right(CType(sender, TextBox).Name, 1))
                Dim increase = 1
                If e.KeyCode = Keys.Up Then increase = -1
                Do
                    Versuch += increase
                    If Versuch > 3 Then
                        Versuch = 1
                    ElseIf Versuch < 1 Then
                        Versuch = 3
                    End If
                    If Versuche(Versuch)(Ctls.Technik).Enabled Then
                        Versuche(Versuch)(Ctls.Technik).Focus()
                        Exit Do
                    End If
                Loop
                e.Handled = True
                e.SuppressKeyPress = True

            Case Keys.Escape
                Dim ctl = CType(sender, TextBox)
                ctl.Text = If(IsNothing(ctl.Tag), String.Empty, ctl.Tag.ToString)
                e.Handled = True
                e.SuppressKeyPress = True
                CancelButton = btnCancel

            Case Keys.Back
                CType(sender, TextBox).Text = String.Empty
                e.Handled = True
                e.SuppressKeyPress = True

            Case < Keys.D0, > Keys.D9
                If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                    If e.KeyCode <> Keys.Oemcomma AndAlso e.KeyCode <> Keys.Decimal Then
                        nonNumberEntered = True
                    End If
                End If
        End Select

        If ModifierKeys = Keys.Shift Then
            nonNumberEntered = True
        End If
    End Sub
    Private Sub TextboxNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTechnik1.KeyPress, txtTechnik2.KeyPress, txtTechnik3.KeyPress
        If nonNumberEntered = True Then
            e.Handled = True
        End If
    End Sub

    '' SAVE
    Private Sub Save()
        Cursor = Cursors.WaitCursor

        Dim pos = bsResults.Find("Teilnehmer", Teilnehmer)
        Dim col As String
        Dim col_ohneWertung = String.Empty
        Dim v = 1
        Dim corr As New Correction With {.gruppe = If(String.IsNullOrEmpty(lblGruppe.Text), 0, CInt(lblGruppe.Text)),
                                         .durchgang = Durchgang,
                                         .teilnehmer = Teilnehmer,
                                         .gewertet = False, 'Leader.RowNum > 0 AndAlso Durchgang = Leader.TableIx,
                                         .angezeigt = Teilnehmer = ScoredLifter AndAlso Durchgang = Leader.TableIx,
                                         .versuch = New Dictionary(Of Integer, Attempt)}

        Dim ResultRow As DataRow = Nothing
        If pos = -1 Then
            Dim found As DataRow() = dtResults.Select("Teilnehmer = " & Teilnehmer)
            If found.Count = 1 Then ResultRow = found(0)
        End If

        For Each row As DataRowView In dvHeben(Durchgang)
            ' jeder Versuch wird anhand der Daten in den Controls einzeln gespeichert

            col = Table(Durchgang).Substring(0, 1) & "_" & row!Versuch.ToString

#Region "Daten aktualisieren"

            ' Wertung ************************************************************************
            Select Case Versuche(v)(Ctls.Wertung).Text
                Case Gültig
                    row!Wertung = 1
                Case Ungültig
                    row!Wertung = -1
                Case Verzicht
                    row!Wertung = -2
                Case Else
                    row!Wertung = DBNull.Value
                    If String.IsNullOrEmpty(col_ohneWertung) Then
                        col_ohneWertung = Table(Durchgang).Substring(0, 1) & "_" & v
                    Else
                        'col_ohneWertung = String.Empty
                    End If
            End Select

            ' HLast ************************************************************************
            If String.IsNullOrEmpty(Versuche(v)(Ctls.Last).Text) Then
                Versuche(v)(Ctls.Last).Text = "0"
            End If
            row!HLast = Versuche(v)(Ctls.Last).Text

            ' Diff ************************************************************************
            If v = 1 Then
                row!Diff = 0
            Else
                Dim diff = CInt(Versuche(v)(Ctls.Last).Text) - CInt(Versuche(v - 1)(Ctls.Last).Text)
                row!Diff = diff
            End If

            ' Technik ************************************************************************
            If Wettkampf.Technikwertung AndAlso pnlTechnik.Enabled AndAlso Versuche(v)(Ctls.Technik).Enabled Then
                row!Note = CDbl(Versuche(v)(Ctls.Technik).Text)
            Else
                row!Note = DBNull.Value
            End If

            ' Steigerung ************************************************************************
            If Not IsDBNull(row!Wertung) Then
                ' gewerteter Versuch, s = letzte gespeicherte Steigerung
                ' --> letzte Steigerung des Versuches suchen
                Dim s As Integer
                For s = 3 To 1 Step -1
                    If CInt(row("Steigerung" & s)) > 0 Then Exit For
                Next
                If CInt(row!Wertung) > -2 AndAlso s > 0 Then
                    ' gültig/ungültig und Steigerung eingetragen
                    ' (bei Verzicht bleibt die Steigerung erhalten, um sie ggf. wieder einlesen zu können)
                    row("Steigerung" & s) = row!HLast
                End If
            ElseIf Not String.IsNullOrEmpty(col_ohneWertung) AndAlso CInt(Strings.Right(col_ohneWertung, 1)) = CInt(row!Versuch) Then
                ' nächster nach gewertetem Versuch
                If Not IsNothing(formLeader) AndAlso Not formLeader.IsDisposed AndAlso formLeader.mnuConfirm.Checked Then
                    row!Steigerung1 = 0
                Else
                    row!Steigerung1 = row!HLast
                End If
                row!Steigerung2 = 0
                row!Steigerung3 = 0
            Else
                ' nicht gewerteter Versuch
                row!Steigerung1 = 0
                row!Steigerung2 = 0
                row!Steigerung3 = 0
            End If

            ' Lampen ************************************************************************
            Dim _lamp = String.Empty
            If Not IsDBNull(row!Wertung) AndAlso Not IsNothing(KR_Pad) Then
                For c = 1 To 3
                    If KR_Pad.ContainsKey(c) AndAlso KR_Pad(c).Wertung <> 0 Then
                        Select Case CInt(row!Wertung)
                            Case 1 : _lamp += "5"
                            Case -1 : _lamp += "7"
                            Case -2 : _lamp += "0"
                        End Select
                    Else
                        _lamp += "0"
                    End If
                Next
                row!Lampen = _lamp
                If Teilnehmer = ScoredLifter AndAlso v = Heber.Versuch Then Heber.Lampen = _lamp
            Else
                row!Lampen = DBNull.Value
                If Teilnehmer = ScoredLifter AndAlso v = Heber.Versuch Then Heber.Lampen = "000"
            End If

            ' Zeit ************************************************************************
            If IsDBNull(row!Wertung) Then row!Zeit = DBNull.Value
#End Region

            '#Region "aktueller Heber"
            '            If Teilnehmer = ScoredLifter AndAlso Durchgang = Leader.TableIx AndAlso v = Heber.Versuch Then
            '                Heber.Wertung = If(IsDBNull(row!wertung), 0, CInt(row!Wertung))
            '                Heber.Hantellast = CInt(row!HLast)
            '                Heber.T_Note = If(IsDBNull(row!Note), 0, CDbl(row!Note))
            '                Heber.Lampen = If(IsDBNull(row!Lampen), "000", _lamp)
            '            End If
            '#End Region

#Region "alle gespeicherten Rows in eine Liste schreiben --> wird bei Return an Client gesendet"
            Dim att As New Attempt With {.versuch = v,
                                         .hlast = CInt(row!HLast),
                                         .diff = CInt(row!Diff),
                                         .wertung = If(IsDBNull(row!Wertung), String.Empty, row!Wertung.ToString),
                                         .note = row!Note.ToString,
                                         .zeit = If(IsDBNull(row!Zeit), String.Empty, row!Zeit.ToString),
                                         .lampen = _lamp}
            corr.versuch(v) = att
            If Teilnehmer = ScoredLifter AndAlso Durchgang = Leader.TableIx AndAlso v = Heber.Versuch AndAlso Not String.IsNullOrEmpty(att.wertung) Then
                corr.gewertet = True
            End If
#End Region

#Region "dvResults aktualisieren"
            If pos > -1 Then
                ' Heber ist in aktueller Gruppe
                If Not IsDBNull(row!Wertung) AndAlso CInt(row!Wertung) <> 0 Then
                    dvResults(pos)(col) = If(CInt(row!HLast) = 0, DBNull.Value, row!HLast)
                Else
                    dvResults(pos)(col) = DBNull.Value
                End If
                dvResults(pos)(col.Insert(1, "W")) = row!Wertung
                If pnlTechnik.Enabled Then
                    dvResults(pos)(col.Insert(1, "T")) = row!Note
                Else
                    dvResults(pos)(col.Insert(1, "T")) = DBNull.Value
                End If
                bsResults.EndEdit()
            ElseIf pos = -1 And Not IsNothing(ResultRow) Then
                If Not IsDBNull(row!Wertung) AndAlso CInt(row!Wertung) <> 0 Then
                    ResultRow(col) = If(CInt(row!HLast) = 0, DBNull.Value, row!HLast)
                Else
                    ResultRow(col) = DBNull.Value
                End If
                ResultRow(col.Insert(1, "W")) = row!Wertung
                If pnlTechnik.Enabled Then
                    ResultRow(col.Insert(1, "T")) = row!Note
                Else
                    ResultRow(col.Insert(1, "T")) = DBNull.Value
                End If
            End If
#End Region

#Region "Leader/Delete aktualisieren"
            If Not IsNothing(dvLeader(Durchgang)) AndAlso Not IsNothing(dvDelete(Durchgang)) Then
                Dim r As DataRow() = dtLeader(Durchgang).Select("Teilnehmer = " & Teilnehmer.ToString & " And Versuch = " & v.ToString)
                If String.IsNullOrEmpty(Versuche(v)(Ctls.Wertung).Text) Then
                    ' keine Wertung
                    If r.Count > 0 Then
                        ' Versuch in Leader aktualisieren
                        Write_Record(r(0), row)
                    Else
                        r = dtDelete(Durchgang).Select("Teilnehmer = " & Teilnehmer.ToString & " And Versuch = " & v.ToString)
                        If r.Count > 0 Then
                            ' Versuch in Delete --> in Leader importieren / in Delete löschen
                            Write_Record(r(0), row)
                            dtLeader(Durchgang).ImportRow(r(0))
                            dtDelete(Durchgang).Rows.Remove(r(0))
                        End If
                    End If
                Else
                    ' Wertung
                    If r.Count > 0 Then
                        ' steht in Leader --> in Delete importieren / in Leader löschen
                        Write_Record(r(0), row)
                        If Not Teilnehmer = ScoredLifter Then
                            dtDelete(Durchgang).ImportRow(r(0))
                            dtLeader(Durchgang).Rows.Remove(r(0))
                        End If
                    Else
                        r = dtDelete(Durchgang).Select("Teilnehmer = " & Teilnehmer.ToString & " And Versuch = " & v.ToString)
                        If r.Count > 0 Then
                            ' steht in Delete --> aktualisieren
                            Write_Record(r(0), row)
                        End If
                    End If
                End If
                bsLeader.EndEdit()
                bsDelete.EndEdit()
            End If
#End Region


#Region "Publikum aktualisieren"
            If pos > -1 Then
                Leader.Write_Scoring(pos, col, If(IsDBNull(dvResults(pos)(col)), -1, CInt(dvResults(pos)(col))),
                                     If(IsDBNull(row!Wertung), 0, CInt(row!Wertung)),
                                     If(IsDBNull(row!Note), 0, CDbl(row!Note)),, corr.angezeigt)


                If Not String.IsNullOrEmpty(col_ohneWertung) AndAlso CInt(Strings.Right(col_ohneWertung, 1)) = CInt(row!Versuch) Then
                    ' Wertung für geänderten Heber in Publikum
                    'Leader.Write_Scoring(pos, col, If(IsDBNull(dvResults(pos)(col)), -1, CInt(dvResults(pos)(col))),
                    '                               If(IsDBNull(row!Wertung), 0, CInt(row!Wertung)),
                    '                               If(IsDBNull(row!Note), 0, CDbl(row!Note))) ', , True)

                    ' neue Anfangslast
                    Leader.Change_Anfangslast(CInt(row!HLast), pos, col_ohneWertung, True)

                    'ElseIf String.IsNullOrEmpty(nCol) OrElse CInt(Strings.Right(nCol, 1)) <> CInt(row!Versuch) Then
                    ' Anfangslast löschen
                    'Leader.Change_Anfangslast(0, pos, col)

                    ' Wertung für aktuellen Heber in Publikum
                    'Leader.Write_Scoring(pos, col, If(IsDBNull(dvResults(pos)(col)), -1, CInt(dvResults(pos)(col))),
                    '                               If(IsDBNull(row!Wertung), 0, CInt(row!Wertung)),
                    '                               If(IsDBNull(row!Note), 0, CDbl(row!Note))) ', , True)
                    'If CInt(row!HLast) > 0 Then
                    '    Leader.Change_Anfangslast(CInt(row!HLast), pos, nCol, True)
                    'End If
                    'If IsDBNull(row!Wertung) OrElse CInt(row!Wertung) = 0 Then
                    '    'Leader.Change_Anfangslast(CInt(row!HLast), pos, nCol, True)
                    'Else
                    '    Leader.Change_Anfangslast(CInt(row!HLast), pos, col, True)
                    'End If     
                End If
            End If
            v += 1 ' Versuche hochzählen
#End Region
        Next

#Region "Resultate & Platzierung berechnen und in dvResults schreiben"
        Get_Results(pos, ResultRow)

        Using save As New mySave
            ' Versuche speichern
            If save.Save_Versuch(dvHeben(Durchgang).ToTable, Table(Durchgang), Me) Then dtHeben(Durchgang).AcceptChanges()
            ' dvResults speichern
            save.Save_Results(Table(Durchgang), Me)
        End Using
#End Region

#Region "Zweikampf in Publikum aktualisieren"
        If pos > -1 Then
            Leader.Write_Scoring(pos, String.Empty,,,, If(IsDBNull(dvResults(pos)!ZK), String.Empty, dvResults(pos)!ZK.ToString), corr.angezeigt)
        End If
#End Region

#Region "bei Mannschafts-WK in Publikum die Gesamtwertung für das Team des akt. Hebers aktualisieren"
        If Wettkampf.Mannschaft Then
            bsResults.EndEdit()
            Using RS As New Results
                ' aktualisiert bsTeams
                RS.TeamWertung(dvResults.ToTable)
            End Using
            bsTeam.ResetBindings(False)
        End If
#End Region

        If CalledFromLeader Then Update_From_Korrektur(corr)

        btnSave.Enabled = False

        Cursor = Cursors.Default
    End Sub
    Private Sub Write_Record(r As DataRow, row As DataRowView)
        r!HLast = row!HLast
        r!Wertung = row!Wertung
        r!Steigerung1 = row!Steigerung1
        r!Steigerung2 = row!Steigerung2
        r!Steigerung3 = row!Steigerung3
        r!Lampen = row!Lampen
        r!Zeit = row!Zeit
    End Sub
    Private Sub Get_Results(pos As Integer, Optional ByRef ResultRow As DataRow = Nothing)
        ' Resultate & Platzierung berechnen

        Dim Cols As String() = {"R", "S"}
        Dim Auswertung As Integer
        Dim Auswertung2 As Integer
        Dim zeit As Date?
        Dim result As Double?
        Dim result2 As Double?
        Dim gesamt2 As Double?
        Dim lstResults As New SortedList(Of Double, Integer)
        Dim lstResults2 As New SortedList(Of Double, Integer)
        Dim lstGesamt2 As New SortedList(Of Double, Integer)
        'Dim Result_Old As Double = -1

        Dim Lasten(1) As Integer?

        Auswertung = CInt(dvHeben(Durchgang)(0)!Auswertung)
        If Not IsDBNull(dvHeben(Durchgang)(0)!Auswertung2) Then Auswertung2 = CInt(dvHeben(Durchgang)(0)!Auswertung2)

        Dim TN = CInt(dvHeben(Durchgang)(0)!Teilnehmer)

        Dim Attempts As New Dictionary(Of Integer, DataRow())
        For d = 0 To 1
            Attempts(d) = dtHeben(d).Select("Teilnehmer = " & TN)
        Next

        Dim i = 0

        Try
            For Each row As DataRowView In dvHeben(Durchgang)
                If Not IsDBNull(row!Wertung) AndAlso CInt(row!Wertung) > -2 Then
                    If CInt(row!Wertung) = 1 Then
                        ' Versuch gültig
                        Lasten(0) = If(Not IsDBNull(Attempts(0)(i)!HLast), CInt(Attempts(0)(i)!HLast), Nothing)
                        Lasten(1) = If(Not IsDBNull(Attempts(1)(i)!HLast), CInt(Attempts(1)(i)!HLast), Nothing)
                        Using RS As New Results
                            result = RS.Get_Result_Heben(CInt(row!Teilnehmer),
                                                     Auswertung,
                                                     CDbl(row!Wiegen),
                                                     row!Sex.ToString,
                                                     CInt(row!HLast),
                                                     CInt(row!Jahrgang),
                                                     If(IsDBNull(row!Note), 0, CDbl(row!Note)),
                                                     Table(Durchgang),
                                                     CInt(row!idAK),
                                                     CInt(row!idGK),
                                                     Lasten)
                            result2 = RS.Get_Result_Heben(CInt(row!Teilnehmer),
                                                     Auswertung2,
                                                     CDbl(row!Wiegen),
                                                     row!Sex.ToString,
                                                     CInt(row!HLast),
                                                     CInt(row!Jahrgang),
                                                     If(IsDBNull(row!Note), 0, CDbl(row!Note)),
                                                     Table(Durchgang),
                                                     CInt(row!idAK),
                                                     CInt(row!idGK),
                                                     Lasten)
                            gesamt2 = RS.Get_Result_2(result,
                                                 Auswertung,
                                                 Auswertung2,
                                                 Table(Durchgang),
                                                 dtHeben,
                                                 pos,
                                                 CInt(row!Teilnehmer),
                                                 CDbl(row!Wiegen),
                                                 row!Sex.ToString,
                                                 CInt(row!Jahrgang),
                                                 CInt(row!HLast),
                                                 If(IsDBNull(row!Note), 0, CDbl(row!Note)),
                                                 CInt(row!idAK),
                                                 CInt(row!idGK),
                                                 Lasten)
                        End Using

                        If Not IsNothing(result) Then
                            lstResults.Add(-CDbl(result), CInt(row!Versuch) - 1)
                            i = CInt(row!Versuch) - 1
                        End If
                        If Not IsNothing(result2) Then
                            lstResults2.Add(-CDbl(result2), CInt(row!Versuch) - 1)
                            i = CInt(row!Versuch) - 1
                        End If
                        If Not IsNothing(gesamt2) Then
                            lstGesamt2.Add(-CDbl(gesamt2), CInt(row!Versuch) - 1)
                        End If

                        'i += 1

                    ElseIf CInt(row!Wertung) = -1 Then
                        ' Versuch ungültig
                        lstResults.Add(0, CInt(row!Versuch) - 1)
                        lstResults2.Add(0, CInt(row!Versuch) - 1)
                        lstGesamt2.Add(0, CInt(row!Versuch) - 1)
                    End If
                Else
                    ' Versuch nicht gewertet oder verzichtet
                    result = Nothing
                    result2 = Nothing
                    gesamt2 = Nothing
                End If
            Next

            If lstResults.Count > 0 Then
                result = Math.Abs(lstResults.Keys(0)) ' höchstes Resultat 
                zeit = If(IsDBNull(dvHeben(Durchgang)(lstResults.Values(0))!Zeit), Nothing, CDate(dvHeben(Durchgang)(lstResults.Values(0))!Zeit))
            Else
                result = Nothing
                zeit = Nothing
            End If

            If lstResults2.Count > 0 Then
                result2 = Math.Abs(lstResults2.Keys(0))
            Else
                result2 = Nothing
            End If

            If lstGesamt2.Count > 0 Then
                gesamt2 = Math.Abs(lstGesamt2.Keys(0))
            Else
                gesamt2 = Nothing
            End If

            Using RS As New Results
                If pos > -1 Then
                    dvResults(pos)("max" & Cols(Durchgang)) = RS.Get_DG_Max(Cols(Durchgang), dvResults(pos))

                    If Wettkampf.Athletik Then
                        dvResults(pos)!Gesamt = dvResults(pos)!Heben
                        If dtResults.Columns.Contains("Athletik") AndAlso Not IsDBNull(dvResults(pos)!Athletik) Then
                            ' Athletik-Wertung einrechnen
                            dvResults(pos)!Gesamt = CDbl(If(IsDBNull(dvResults(pos)!Gesamt), 0, dvResults(pos)!Gesamt)) + CDbl(dvResults(pos)!Athletik)
                        End If
                    End If

                    'If IsDBNull(dvResults(pos)(Table(Durchgang))) OrElse (Not IsDBNull(dvResults(pos)(Table(Durchgang))) AndAlso result > CDbl(dvResults(pos)(Table(Durchgang)))) Then
                    If Auswertung = 1 Then
                        'ROBI Points für Durchgang berechnen ODER auf DBNull setzen
                        dvResults(pos)(Table(Durchgang)) = DBNull.Value
                        'ElseIf Auswertung = 5 Then
                        '    ' bei ZK keine Results für Durchgang
                        '    dvResults(pos)(Table(Durchgang)) = DBNull.Value
                    Else
                        dvResults(pos)(Table(Durchgang)) = IIf(IsNothing(result), DBNull.Value, result)
                    End If

                    If Auswertung2 = 1 Then
                        'ROBI Points für Durchgang berechnen ODER auf DBNull setzen
                        dvResults(pos)(Table(Durchgang) & "2") = DBNull.Value
                        'ElseIf Auswertung2 = 5 Then
                        '    ' bei ZK keine Results für Durchgang ---------> doch
                        '    dvResults(pos)(Table(Durchgang) & "2") = result2 'DBNull.Value
                    Else
                        dvResults(pos)(Table(Durchgang) & "2") = IIf(IsNothing(result2), DBNull.Value, result2)
                    End If

                    dvResults(pos)!Wertung2 = IIf(IsNothing(gesamt2), DBNull.Value, gesamt2)
                    'End If

                    Dim ZK = RS.ZK_Resultat(CInt(dvResults(pos)!Teilnehmer))
                    dvResults(pos)!ZK = IIf(IsNothing(ZK), DBNull.Value, ZK)

                    If Auswertung = 1 OrElse Auswertung = 5 Then
                        If IsNothing(ZK) Then
                            dvResults(pos)!Heben = DBNull.Value
                        ElseIf Auswertung = 1 Then
                            dvResults(pos)!Heben = result
                        ElseIf Auswertung = 5 Then
                            dvResults(pos)!Heben = ZK
                        End If
                    Else
                        dvResults(pos)!Heben = CDbl(If(IsDBNull(dvResults(pos)!Reissen), 0, dvResults(pos)!Reissen)) + CDbl(If(IsDBNull(dvResults(pos)!Stossen), 0, dvResults(pos)!Stossen))
                        If CInt(dvResults(pos)!Heben) = 0 Then dvResults(pos)!Heben = DBNull.Value
                    End If

                ElseIf pos = -1 And Not IsNothing(ResultRow) Then

                    ' Heber ist NICHT in aktueller Gruppe --> Änderung direkt in Tabelle
                    ResultRow("max" & Cols(Durchgang)) = RS.Get_DG_Max(Cols(Durchgang), ResultRow.Table.DefaultView(0))

                    If Wettkampf.Athletik Then
                        ResultRow!Gesamt = ResultRow!Heben
                        If dtResults.Columns.Contains("Athletik") AndAlso Not IsDBNull(ResultRow!Athletik) Then
                            ' Athletik-Wertung einrechnen
                            ResultRow!Gesamt = CDbl(If(IsDBNull(ResultRow!Gesamt), 0, ResultRow!Gesamt)) + CDbl(ResultRow!Athletik)
                        End If
                    End If

                    If Auswertung = 1 Then
                        'ROBI Points für Durchgang berechnen ODER auf DBNull setzen
                        ResultRow(Table(Durchgang)) = DBNull.Value
                        'ElseIf Auswertung = 5 Then
                        '    ' bei ZK keine Results für Durchgang
                        '    ResultRow(Table(Durchgang)) = DBNull.Value
                    Else
                        ResultRow(Table(Durchgang)) = IIf(IsNothing(result), DBNull.Value, result)
                    End If

                    If Auswertung = 1 Then
                        'ROBI Points für Durchgang berechnen ODER auf DBNull setzen
                        ResultRow(Table(Durchgang) & "2") = DBNull.Value
                    Else
                        ResultRow(Table(Durchgang) & "2") = IIf(IsNothing(result2), DBNull.Value, result2)
                    End If

                    ResultRow!Wertung2 = IIf(IsNothing(gesamt2), DBNull.Value, gesamt2)

                    Dim ZK = RS.ZK_Resultat(CInt(ResultRow!Teilnehmer))
                    ResultRow!ZK = IIf(IsNothing(ZK), DBNull.Value, ZK)

                    If Auswertung = 1 OrElse Auswertung = 5 Then
                        If IsNothing(ZK) Then
                            ResultRow!Heben = DBNull.Value
                        ElseIf Auswertung = 1 Then
                            ResultRow!Heben = result
                        ElseIf Auswertung = 5 Then
                            ResultRow!Heben = ZK
                        End If
                    Else
                        ResultRow!Heben = CDbl(If(IsDBNull(ResultRow!Reissen), 0, ResultRow!Reissen)) + CDbl(If(IsDBNull(ResultRow!Stossen), 0, ResultRow!Stossen))
                        If CInt(ResultRow!Heben) = 0 Then ResultRow!Heben = DBNull.Value
                    End If
                End If

#Region "Platzierung"
                If Not Wettkampf.Mannschaft Then ' AndAlso Not IsDBNull(dvHeben(Durchgang)(i)!Wertung) AndAlso CInt(dvHeben(Durchgang)(i)!Wertung) = 1 Then
                    For Each WK In Wettkampf.Identities.Keys
                        Dim Kategorie As New List(Of String)
                        Kategorie.AddRange({Table(Durchgang), "Heben"})
                        If Wettkampf.Athletik Then Kategorie.Add("Gesamt")

                        RS.Set_Rank(dvHeben(Durchgang)(0)!Sex.ToString,
                                    CInt(dvHeben(Durchgang)(0)!Jahrgang),
                                    CDbl(dvHeben(Durchgang)(0)!Wiegen),
                                    dvHeben(Durchgang)(0)!Gruppe.ToString,
                                    Kategorie,
                                    Auswertung,
                                    Auswertung2,
                                    CInt(dvHeben(Durchgang)(0)!Gruppierung2),
                                    CInt(dvHeben(Durchgang)(0)!idAK),
                                    CInt(dvHeben(Durchgang)(0)!idGK),
                                    WK,
                                    Gruppen)
                    Next
                End If
#End Region
            End Using
        Catch ex As Exception
            LogMessage("Korrektur: Get_Results: " & ex.Message, ex.StackTrace)
        End Try
    End Sub

    '' MOVE
    Private Sub Panel_Headers_MouseDown(sender As Object, e As MouseEventArgs) Handles lblTitleBar.MouseDown
        If e.Button = MouseButtons.Left Then
            pnlForm.BorderStyle = BorderStyle.None
            pnlPos = New Point(e.X, e.Y)
            lblTitleBar.Cursor = Cursors.SizeAll
        End If
    End Sub
    Private Sub Panel_Headers_MouseMove(sender As Object, e As MouseEventArgs) Handles lblTitleBar.MouseMove

        If (e.Button And MouseButtons.Left) = MouseButtons.Left And pnlForm.BorderStyle = BorderStyle.None Then

            Dim loc = New Point(Location.X + (e.X - pnlPos.X), Location.Y + (e.Y - pnlPos.Y))
            Location = loc
        End If
    End Sub
    Private Sub Panel_Headers_MouseUp(sender As Object, e As MouseEventArgs) Handles lblTitleBar.MouseUp
        If e.Button = MouseButtons.Left And pnlForm.BorderStyle = BorderStyle.None Then
            pnlForm.BorderStyle = BorderStyle.FixedSingle
            pnlPos = New Point(pnlForm.Left, pnlForm.Top + pnlForm.Height)
            lblTitleBar.Cursor = Cursors.Default
        End If
    End Sub

    '' BUTTONS
    Private Sub btnEscape_Click(sender As Object, e As EventArgs) Handles btnEscape.Click
        Close()
    End Sub
    Private Sub btnEscape_MouseEnter(sender As Object, e As EventArgs) Handles btnEscape.MouseEnter
        btnEscape.ForeColor = Color.White
    End Sub
    Private Sub btnEscape_MouseLeave(sender As Object, e As EventArgs) Handles btnEscape.MouseLeave
        btnEscape.ForeColor = Color.DarkGray
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Save()
    End Sub
    Private Sub btnWiederholen_Click(sender As Object, e As EventArgs) Handles btnWiederholen.Click

        Wiederholen = True
        Close()



        'Dim LastAttempt As Integer ' Index des zu wiederholenden Versuches

        'For LastAttempt = restricted() To 0 Step -1
        '    Try
        '        If Not IsDBNull(dvHeben(Durchgang)(LastAttempt)!Wertung) Then Exit For ' gewerteter versuch
        '    Catch ex As Exception
        '    End Try
        'Next
        'If LastAttempt = -1 Then Return ' es gibt keine gewerteten Versuche

        'Cursor = Cursors.WaitCursor

        'Leader.RowNum = 0

        'Versuch(LastAttempt)(1).Text = String.Empty ' Wertung des letzten Versuchs zurücksetzen

        'dgvLeader.Rows(Leader.RowNum + Leader.RowNum_Offset).ReadOnly = False

        ''Leader.Change_Wertung(0) ' dgvWertung & dgvTechnik in Moderator ausblenden

        'SaveKorrektur()

        'Sortierung()

        'cboRS.Enabled = True
        'pnlKorrektur.Visible = False

        ''SendtoBohle()

        'dgvLeader.ClearSelection()

        'Heber.Id = 0
        'Cursor = Cursors.Default
    End Sub
    Private Function restricted() As Integer
        Dim row() = Glob.dtRestrict.Select("Durchgang = " & Durchgang)
        If row.Count = 0 Then Return 2
        If Year(Wettkampf.Datum) - CInt(dvHeben(Durchgang)(0)!Jahrgang) = CInt(row(0)!Altersklasse) Then Return CInt(row(0)!Versuche) - 1
        Return 2
    End Function

    '' FILTER
    Private Sub cboDurchgang_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDurchgang.SelectedIndexChanged

        If loading Then Return

        If btnSave.Enabled Then
            Using New Centered_MessageBox(Me)
                If MessageBox.Show("Änderungen für " & cboHeber.Text & " übernehmen?", msgCaption,
                                   MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    Save()
                Else
                    btnSave.Enabled = False
                End If
            End Using
        End If

        Durchgang = cboDurchgang.SelectedIndex

        Try
            Get_New_Lifter()
        Catch ex As Exception
            loading = False
        End Try

    End Sub
    Private Sub cboHeber_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboHeber.SelectedValueChanged

        If loading Then Return
        'If Teilnehmer = 0 AndAlso cboHeber.SelectedIndex = -1 Then Return
        If cboHeber.SelectedIndex = -1 Then Return

        If btnSave.Enabled Then
            Using New Centered_MessageBox(Me)
                If MessageBox.Show("Änderungen für " & cboHeber.Text & " übernehmen?", msgCaption,
                                   MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    Save()
                Else
                    btnSave.Enabled = False
                End If
            End Using
        End If

        Try
            Get_New_Lifter()
        Catch ex As Exception
            loading = False
        End Try

    End Sub
    Private Sub dgvGruppe_SelectionChanged()

        If Not loading AndAlso btnSave.Enabled Then
            Using New Centered_MessageBox(Me)
                If MessageBox.Show("Änderungen für " & cboHeber.Text & " übernehmen?", msgCaption,
                                   MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    Save()
                Else
                    btnSave.Enabled = False
                End If
            End Using
        End If

        Gruppen.Clear()
        For Each row As DataGridViewRow In dgvGruppe.SelectedRows
            Gruppen.Add(row.Cells("Groups").Value.ToString)
            'If Gruppen(0) = "0" Then Exit For ' [alle] ausgewählt
        Next

        Gruppen.Sort()

        Set_Gruppen()

        Try
            Get_New_Lifter()
        Catch ex As Exception
            loading = False
        End Try

        cboDurchgang.Focus()
    End Sub

    Private Sub Set_Gruppen()
        'If Gruppen(0) = "0" Then
        If Gruppen.Count = 0 Then
            dvHeben(Durchgang).RowFilter = Nothing
            dvDurchgang.RowFilter = Nothing
            lblGruppe.Text = "[alle]"
        Else
            Dim filter = "Gruppe = " & Join(Gruppen.ToArray, " OR Gruppe = ")
            dvHeben(Durchgang).RowFilter = filter
            dvDurchgang.RowFilter = filter
            lblGruppe.Text = Join(Gruppen.ToArray, ", ")
            'bsGruppen.Position = bsGruppen.Find("Gruppe", Gruppen(0))
        End If
    End Sub

    Private Sub Get_New_Lifter(Optional TN As Integer = 0)

        loading = True
        Cursor = Cursors.WaitCursor

        Try
            ' vorhandene Rows für den Heber
            'If Not Teilnehmer = 0 AndAlso Not IsNothing(cboHeber.SelectedValue) Then Teilnehmer = CInt(cboHeber.SelectedValue)
            If Not IsNothing(cboHeber.SelectedValue) Then Teilnehmer = CInt(cboHeber.SelectedValue)
            dvHeben(Durchgang).RowFilter = "Teilnehmer = " + Teilnehmer.ToString()
            dvHeben(Durchgang).Sort = "Versuch ASC"

            'If Teilnehmer = ScoredLifter Then
            '    ' gewerteter TN in Anzeige
            '    '    With pnlError
            '    '        .Width = Width
            '    '        .Visible = True
            '    '        .BringToFront()
            '    '    End With
            '    '    btnWiederholen.Left = btnSave.Left
            '    '    btnWiederholen.Visible = True
            '    '    btnSave.Visible = False
            '    '    loading = False
            '    '    Cursor = Cursors.Default
            '    '    Return
            '    If Heber.Versuch < 3 Then
            '        For i = Heber.Versuch + 1 To 3
            '            For Each ctl As Control In Versuche(i)
            '                ctl.Enabled = False
            '            Next
            '        Next
            '    End If
            'Else

            pnlTechnik.Enabled = Wettkampf.Technikwertung OrElse TechnikwertungEnabled AndAlso CInt(dvHeben(Durchgang)(0)!idAK) < 30

            For i = 1 To 3
                For Each ctl As Control In Versuche(i)
                    If ctl.Name.Contains("txtTechnik") Then
                        ctl.Enabled = pnlTechnik.Enabled 'Wettkampf.Technikwertung
                    Else
                        ctl.Enabled = True
                    End If
                Next
            Next
            pnlError.Visible = False
            btnWiederholen.Visible = False
            btnSave.Visible = True
            'End If

            For Versuch = 1 To 3
                ' Wertung
                With Versuche(Versuch)(Ctls.Wertung)
                    Select Case dvHeben(Durchgang)(Versuch - 1)!Wertung.ToString
                        Case "1"
                            .Text = Gültig
                            .ForeColor = Color.LimeGreen
                            CType(Versuche(Versuch)(Ctls.Gültig), CheckBox).Checked = True
                            CType(Versuche(Versuch)(Ctls.Ungültig), CheckBox).Checked = False
                            CType(Versuche(Versuch)(Ctls.Verzicht), CheckBox).Checked = False
                        Case "-1"
                            .Text = Ungültig
                            .ForeColor = Color.Red
                            CType(Versuche(Versuch)(Ctls.Ungültig), CheckBox).Checked = True
                            CType(Versuche(Versuch)(Ctls.Gültig), CheckBox).Checked = False
                            CType(Versuche(Versuch)(Ctls.Verzicht), CheckBox).Checked = False
                        Case "-2"
                            .Text = Verzicht
                            .ForeColor = Color.Black
                            CType(Versuche(Versuch)(Ctls.Verzicht), CheckBox).Checked = True
                            CType(Versuche(Versuch)(Ctls.Gültig), CheckBox).Checked = False
                            CType(Versuche(Versuch)(Ctls.Ungültig), CheckBox).Checked = False
                        Case Else
                            .Text = String.Empty
                            CType(Versuche(Versuch)(Ctls.Gültig), CheckBox).Checked = False
                            CType(Versuche(Versuch)(Ctls.Ungültig), CheckBox).Checked = False
                            CType(Versuche(Versuch)(Ctls.Verzicht), CheckBox).Checked = False
                    End Select
                End With
                ' Hantellast
                With CType(Versuche(Versuch)(Ctls.Last), NumericUpDown)
                    .Value = CInt(dvHeben(Durchgang)(Versuch - 1)!HLast)
                    '.Enabled = (Not Teilnehmer = ScoredLifter OrElse Versuch <= Heber.Versuch) AndAlso Not Versuche(Versuch)(Ctls.Wertung).Text.Equals(Verzicht)
                End With
                ' Technik
                With Versuche(Versuch)(Ctls.Technik)
                    If IsDBNull(dvHeben(Durchgang)(Versuch - 1)!Note) OrElse CDbl(dvHeben(Durchgang)(Versuch - 1)!Note) = 0 Then
                        .Text = String.Empty
                    Else
                        .Text = Format(dvHeben(Durchgang)(Versuch - 1)!Note.ToString, "Fixed")
                    End If
                    '.Enabled = Wettkampf.Technikwertung AndAlso Versuche(Versuch)(Ctls.Wertung).Text = Gültig
                    .Enabled = pnlTechnik.Enabled AndAlso Versuche(Versuch)(Ctls.Wertung).Text = Gültig
                End With
            Next
            Versuch = 3
        Catch ex As Exception
        End Try

        loading = False
        Cursor = Cursors.Default

        btnSave.Enabled = False
    End Sub

    '' WERTUNG
    Private Sub txtLast_GotFocus(sender As Object, e As EventArgs) Handles txtLast1.GotFocus, txtLast2.GotFocus, txtLast3.GotFocus
        With CType(sender, NumericUpDown)
            .Tag = .Text
            .Select(0, .Text.Length)
            Versuch = CInt(Strings.Right(.Name, 1))
        End With
        CancelButton = Nothing
    End Sub
    Private Sub txtLast_LostFocus(sender As Object, e As EventArgs) Handles txtLast1.LostFocus, txtLast2.LostFocus, txtLast3.LostFocus
        CancelButton = btnCancel
    End Sub
    Private Sub txtLast_ValueChanged(sender As Object, e As EventArgs) Handles txtLast1.ValueChanged, txtLast2.ValueChanged, txtLast3.ValueChanged
        With CType(sender, NumericUpDown)
            If Versuche(Versuch)(Ctls.Wertung).Text.Equals(Gültig) OrElse Versuche(Versuch)(Ctls.Wertung).Text.Equals(Ungültig) Then
                .BackColor = If(.Value = 0, Color.Red, Color.White)
                .ForeColor = If(.Value = 0, Color.White, Color.Black)
            Else
                .BackColor = Color.White
                .ForeColor = Color.Black
            End If
            For i = Versuch + 1 To 3
                If CType(Versuche(i)(Ctls.Last), NumericUpDown).Value < .Value Then
                    CType(Versuche(i)(Ctls.Last), NumericUpDown).Value = .Value
                End If
            Next
        End With
        btnSave.Enabled = True
    End Sub

    Private Sub chkG_Click(sender As Object, e As EventArgs) Handles chkG1.Click, chkG2.Click, chkG3.Click

        Versuch = CInt(CType(sender, CheckBox).Name.Substring(4, 1))

        CType(Versuche(Versuch)(Ctls.Gültig), CheckBox).Checked = Not CType(Versuche(Versuch)(Ctls.Gültig), CheckBox).Checked
        CType(Versuche(Versuch)(Ctls.Ungültig), CheckBox).Checked = False
        CType(Versuche(Versuch)(Ctls.Verzicht), CheckBox).Checked = False

        If CType(Versuche(Versuch)(Ctls.Gültig), CheckBox).Checked Then Versuche(Versuch)(Ctls.Wertung).Text = Gültig

    End Sub
    Private Sub chkU_Click(sender As Object, e As EventArgs) Handles chkU1.Click, chkU2.Click, chkU3.Click

        Versuch = CInt(CType(sender, CheckBox).Name.Substring(4, 1))

        CType(Versuche(Versuch)(Ctls.Ungültig), CheckBox).Checked = Not CType(Versuche(Versuch)(Ctls.Ungültig), CheckBox).Checked
        CType(Versuche(Versuch)(Ctls.Gültig), CheckBox).Checked = False
        CType(Versuche(Versuch)(Ctls.Verzicht), CheckBox).Checked = False

        If CType(Versuche(Versuch)(Ctls.Ungültig), CheckBox).Checked Then Versuche(Versuch)(Ctls.Wertung).Text = Ungültig

    End Sub
    Private Sub chkV_Click(sender As Object, e As EventArgs) Handles chkV1.Click, chkV2.Click, chkV3.Click

        Versuch = CInt(CType(sender, CheckBox).Name.Substring(4, 1))

        CType(Versuche(Versuch)(Ctls.Verzicht), CheckBox).Checked = Not CType(Versuche(Versuch)(Ctls.Verzicht), CheckBox).Checked
        CType(Versuche(Versuch)(Ctls.Gültig), CheckBox).Checked = False
        CType(Versuche(Versuch)(Ctls.Ungültig), CheckBox).Checked = False

        If CType(Versuche(Versuch)(Ctls.Verzicht), CheckBox).Checked Then Versuche(Versuch)(Ctls.Wertung).Text = Verzicht

    End Sub
    Private Sub chkWertung_CheckedChanged(sender As Object, e As EventArgs) Handles chkG1.CheckedChanged, chkG2.CheckedChanged, chkG3.CheckedChanged,
                                                                                    chkU1.CheckedChanged, chkU2.CheckedChanged, chkU3.CheckedChanged,
                                                                                    chkV1.CheckedChanged, chkV2.CheckedChanged, chkV3.CheckedChanged

        If loading Then Return
        If Not CType(sender, CheckBox).Checked Then Versuche(Versuch)(Ctls.Wertung).Text = String.Empty
    End Sub

    Private Sub txtWertung_GotFocus(sender As Object, e As EventArgs) Handles txtWertung1.GotFocus, txtWertung2.GotFocus, txtWertung3.GotFocus
        Versuche(Versuch)(Ctls.Last).Focus()
    End Sub
    Private Sub txtWertung_TextChanged(sender As Object, e As EventArgs) Handles txtWertung1.TextChanged, txtWertung2.TextChanged, txtWertung3.TextChanged

        If loading OrElse Teilnehmer = 0 Then Return
        Try
            Select Case Versuche(Versuch)(Ctls.Wertung).Text
                Case Gültig
                    Versuche(Versuch)(Ctls.Wertung).ForeColor = Color.LimeGreen
                    With Versuche(Versuch)(Ctls.Technik)
                        .Enabled = pnlTechnik.Enabled  'Wettkampf.Technikwertung
                        .Text = Get_Technik()
                        .Focus()
                        'If Not Wettkampf.Technikwertung OrElse Not String.IsNullOrEmpty(.Text) Then btnSave.Enabled = True
                        If Not pnlTechnik.Enabled OrElse Not String.IsNullOrEmpty(.Text) Then btnSave.Enabled = True
                    End With
                    Return
                Case Ungültig
                    Versuche(Versuch)(Ctls.Wertung).ForeColor = Color.Red
                    Versuche(Versuch)(Ctls.Technik).Enabled = False
                    Versuche(Versuch)(Ctls.Technik).Text = String.Empty
                Case Verzicht
                    'Versuche(Versuch)(Ctls.Last).Enabled = False
                    CType(Versuche(Versuch)(Ctls.Last), NumericUpDown).Value = 0
                    Versuche(Versuch)(Ctls.Wertung).ForeColor = Color.Black
                    Versuche(Versuch)(Ctls.Technik).Enabled = False
                    Versuche(Versuch)(Ctls.Technik).Text = String.Empty
                    Return
                Case String.Empty
                    Versuche(Versuch)(Ctls.Technik).Enabled = False
                    Versuche(Versuch)(Ctls.Technik).Text = String.Empty
            End Select
            'Versuche(Versuch)(Ctls.Last).Enabled = True
            'CType(Versuche(Versuch)(Ctls.Last), NumericUpDown).Value = Get_HLast()
            btnSave.Enabled = True
        Catch ex As Exception
        End Try
    End Sub

    Private Sub txtTechnik_EnabledChanged(sender As Object, e As EventArgs) Handles txtTechnik1.EnabledChanged, txtTechnik2.EnabledChanged, txtTechnik3.EnabledChanged
        With CType(sender, TextBox)
            If Not .Enabled Then .BackColor = Color.White
        End With
    End Sub
    Private Sub txtTechnik_GotFocus(sender As Object, e As EventArgs) Handles txtTechnik1.GotFocus, txtTechnik2.GotFocus, txtTechnik3.GotFocus
        CancelButton = Nothing
        With CType(sender, TextBox)
            .Tag = .Text
            Versuch = CInt(Strings.Right(.Name, 1))
        End With
    End Sub
    Private Sub txtTechnik_LostFocus(sender As Object, e As EventArgs) Handles txtTechnik1.LostFocus, txtTechnik2.LostFocus, txtTechnik3.LostFocus
        CancelButton = btnCancel
    End Sub
    Private Sub txtTechnik_TextChanged(sender As Object, e As EventArgs) Handles txtTechnik1.TextChanged, txtTechnik2.TextChanged, txtTechnik3.TextChanged
        If Not loading Then btnSave.Enabled = True
    End Sub

    Private Function Get_HLast() As Integer
        Try
            Dim row() = dtHeben(Durchgang).Select("Teilnehmer = " + Teilnehmer.ToString + " AND Versuch = " + Versuch.ToString)
            If row.Length = 0 Then Return 0
            For i = 3 To 1 Step -1
                If CInt(row(0)("Steigerung" & i)) > 0 Then Return CInt(row(0)("Steigerung" & i))
            Next
            'Dim HLast = CInt(row(0)!Steigerung3)
            'If HLast = 0 Then HLast = CInt(row(0)!Steigerung2)
            'If HLast = 0 Then HLast = CInt(row(0)!Steigerung1)
            'If HLast = 0 Then HLast = CInt(row(0)!HLast)
            'Return HLast
            Return CInt(row(0)!HLast)
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Private Function Get_Technik() As String
        'If Not Wettkampf.Technikwertung Then Return String.Empty
        If Not pnlTechnik.Enabled Then Return String.Empty
        Try
            Dim row() = dtHeben(Durchgang).Select("Teilnehmer = " + Teilnehmer.ToString + " AND Versuch = " + Versuch.ToString)
            Return Format(row(0)!Note.ToString, "Fixed")
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    '' MouseWheel ausschalten
    Dim CtlCaptured As Boolean
    Private Sub Prohibited_MouseWheel(sender As Object, e As MouseEventArgs) Handles cboDurchgang.MouseWheel, txtLast1.MouseWheel, txtLast2.MouseWheel, txtLast3.MouseWheel
        If Not CType(sender, Control).Capture Then
            Dim HMEA As HandledMouseEventArgs = DirectCast(e, HandledMouseEventArgs)
            HMEA.Handled = True
        End If
    End Sub
    Private Sub Allowed_MouseWheel(sender As Object, e As MouseEventArgs) Handles cboHeber.MouseWheel
        If Not CtlCaptured Then
            Dim HMEA As HandledMouseEventArgs = DirectCast(e, HandledMouseEventArgs)
            HMEA.Handled = True
        End If
    End Sub
    Private Sub Allowed_MouseEnter(sender As Object, e As EventArgs) Handles cboHeber.MouseEnter
        CtlCaptured = True
    End Sub
    Private Sub Allowed_MouseLeave(sender As Object, e As EventArgs) Handles cboHeber.MouseLeave
        CtlCaptured = False
    End Sub

    '' Contextmenüs
    Private Sub ContextMenuHeber_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuHeber.Opening
        mnuHeberId.Text = "Heber-Id: " & cboHeber.SelectedValue.ToString
    End Sub

    Private Sub ContextMenuLast_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuLast.Opening
        Versuch = CInt(Strings.Right(ContextMenuLast.SourceControl.Name, 1))
        If Versuch = 1 Then
            mnuLast.Text = "gespeicherte Anfangslast: " & Get_HLast() & " kg"
        Else
            mnuLast.Text = "gespeicherte Hantel-Last: " & Get_HLast() & " kg"
        End If
    End Sub
    Private Sub ContextMenuLast_Click(sender As Object, e As EventArgs) Handles ContextMenuLast.Click
        With Versuche(Versuch)(Ctls.Last)
            .Text = Split(mnuLast.Text, " ")(2)
            .Focus()
        End With
    End Sub

    Private Sub ContextMenuTechnik_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuTechnik.Opening
        Versuch = CInt(Strings.Right(ContextMenuLast.SourceControl.Name, 1))
        mnuNote.Text = "Note: " & Get_Technik()
    End Sub
    Private Sub ContextMenuTechnik_Click(sender As Object, e As EventArgs) Handles ContextMenuTechnik.Click
        With Versuche(Versuch)(Ctls.Technik)
            .Text = Split(mnuNote.Text, " ")(1)
            .Focus()
        End With
    End Sub

    '' ComboBox Fake für Gruppe
    Dim ButtonStyle As String = "Normal"
    Private Sub pnlGruppe_Paint(sender As Object, e As PaintEventArgs) Handles pnlGruppe.Paint
        Dim renderer As VisualStyles.VisualStyleRenderer = Nothing
        Select Case ButtonStyle
            Case "Normal"
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Normal)
            Case "Hot"
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Hot)
            Case "Pressed"
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Pressed)
        End Select
        Dim rectangle1 As New Rectangle(0, 0, 17, 28)
        renderer.DrawBackground(e.Graphics, rectangle1)
    End Sub
    Private Sub pnlGruppe_MouseEnter(sender As Object, e As EventArgs) Handles pnlGruppe.MouseEnter
        If ButtonStyle = "Pressed" Then Return
        ButtonStyle = "Hot"
        pnlGruppe.Refresh()
    End Sub
    Private Sub pnlGruppe_MouseLeave(sender As Object, e As EventArgs) Handles pnlGruppe.MouseLeave
        If ButtonStyle = "Pressed" Then Return
        ButtonStyle = "Normal"
        pnlGruppe.Refresh()
    End Sub
    Private Sub pnlGruppe_MouseClick(sender As Object, e As MouseEventArgs) Handles pnlGruppe.MouseClick
        If ButtonStyle = "Pressed" Then
            ButtonStyle = "Hot"
            dgvGruppe.Visible = False
        Else
            ButtonStyle = "Pressed"
            Dim loc = PointToScreen(New Point(lblGruppe.Left, lblGruppe.Top + lblGruppe.Height - 1))
            With dgvGruppe
                .Location = lblGruppe.FindForm.PointToClient(loc)
                .Visible = True
                .BringToFront()
                .ClearSelection()
                .Focus()
                'If Gruppen.Count = 0 Then 'OrElse Gruppen(0) = "0" Then
                '    bsGruppen.Position = 0
                'Else
                'bsGruppen.Position = bsGruppen.Find("Gruppe", Gruppen(0))
                For Each g In Gruppen
                    .Rows(bsGruppen.Find("Gruppe", g)).Selected = True
                Next
                'End If
            End With
        End If
        pnlGruppe.Refresh()
    End Sub

    Private Sub dgvGruppe_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGruppe.CellClick  'CellDoubleClick
        dgvGruppe_SelectionChanged()
    End Sub
    Private Sub dgvGruppe_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvGruppe.CellFormatting
        If Equals(e.Value, 0) Then
            e.Value = ""
            e.FormattingApplied = True
        End If
    End Sub
    Private Sub dgvGruppe_GotFocus(sender As Object, e As EventArgs) Handles dgvGruppe.GotFocus
        CancelButton = Nothing
    End Sub
    Private Sub dgvGruppe_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvGruppe.KeyDown
        If e.KeyCode = Keys.Escape Then
            cboDurchgang.Focus()
        ElseIf e.KeyCode = Keys.Enter Then
            dgvGruppe_SelectionChanged()
        End If
    End Sub
    Private Sub dgvGruppe_LostFocus(sender As Object, e As EventArgs) Handles dgvGruppe.LostFocus
        ButtonStyle = "Normal"
        pnlGruppe.Refresh()
        CancelButton = btnCancel
        dgvGruppe.Visible = False
    End Sub

End Class