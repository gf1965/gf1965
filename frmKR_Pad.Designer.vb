﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmKR_Pad
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ComPort = New System.Windows.Forms.ComboBox()
        Me.VerbindenKnopf = New System.Windows.Forms.Button()
        Me.ID_Label = New System.Windows.Forms.Label()
        Me.UI_Grouper = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Erg_Note = New System.Windows.Forms.RadioButton()
        Me.Erg_Guelt = New System.Windows.Forms.RadioButton()
        Me.Erg_Text = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Senden_Knopf = New System.Windows.Forms.Button()
        Me.Zeile1_Text = New System.Windows.Forms.TextBox()
        Me.Zeile2_Text = New System.Windows.Forms.TextBox()
        Me.Disp_Loe = New System.Windows.Forms.Button()
        Me.Zeile2_Loe = New System.Windows.Forms.Button()
        Me.Zeile1_Loe = New System.Windows.Forms.Button()
        Me.Piep_Knopf = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Zeile2_PC = New System.Windows.Forms.RadioButton()
        Me.Zeile2_Tasten = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Clear_Zeile = New System.Windows.Forms.RadioButton()
        Me.Clear_Buchstaben = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.LichtAus = New System.Windows.Forms.RadioButton()
        Me.LichtAn = New System.Windows.Forms.RadioButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.UI_Grouper.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ComPort
        '
        Me.ComPort.FormattingEnabled = True
        Me.ComPort.Location = New System.Drawing.Point(16, 12)
        Me.ComPort.Name = "ComPort"
        Me.ComPort.Size = New System.Drawing.Size(208, 21)
        Me.ComPort.TabIndex = 1
        '
        'VerbindenKnopf
        '
        Me.VerbindenKnopf.Location = New System.Drawing.Point(258, 10)
        Me.VerbindenKnopf.Name = "VerbindenKnopf"
        Me.VerbindenKnopf.Size = New System.Drawing.Size(75, 23)
        Me.VerbindenKnopf.TabIndex = 2
        Me.VerbindenKnopf.Text = "Verbinden"
        Me.VerbindenKnopf.UseVisualStyleBackColor = True
        Me.VerbindenKnopf.Visible = False
        '
        'ID_Label
        '
        Me.ID_Label.AutoSize = True
        Me.ID_Label.Location = New System.Drawing.Point(345, 15)
        Me.ID_Label.Name = "ID_Label"
        Me.ID_Label.Size = New System.Drawing.Size(30, 13)
        Me.ID_Label.TabIndex = 3
        Me.ID_Label.Text = "ID: ?"
        '
        'UI_Grouper
        '
        Me.UI_Grouper.Controls.Add(Me.GroupBox6)
        Me.UI_Grouper.Controls.Add(Me.GroupBox5)
        Me.UI_Grouper.Controls.Add(Me.Disp_Loe)
        Me.UI_Grouper.Controls.Add(Me.Zeile2_Loe)
        Me.UI_Grouper.Controls.Add(Me.Zeile1_Loe)
        Me.UI_Grouper.Controls.Add(Me.Piep_Knopf)
        Me.UI_Grouper.Controls.Add(Me.GroupBox4)
        Me.UI_Grouper.Controls.Add(Me.GroupBox3)
        Me.UI_Grouper.Controls.Add(Me.GroupBox2)
        Me.UI_Grouper.Enabled = False
        Me.UI_Grouper.Location = New System.Drawing.Point(16, 39)
        Me.UI_Grouper.Name = "UI_Grouper"
        Me.UI_Grouper.Size = New System.Drawing.Size(363, 273)
        Me.UI_Grouper.TabIndex = 4
        Me.UI_Grouper.tabstop = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Erg_Note)
        Me.GroupBox6.Controls.Add(Me.Erg_Guelt)
        Me.GroupBox6.Controls.Add(Me.Erg_Text)
        Me.GroupBox6.Location = New System.Drawing.Point(7, 195)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(352, 73)
        Me.GroupBox6.TabIndex = 11
        Me.GroupBox6.tabstop = False
        Me.GroupBox6.Text = "Ergebnis"
        '
        'Erg_Note
        '
        Me.Erg_Note.AutoSize = True
        Me.Erg_Note.Location = New System.Drawing.Point(6, 42)
        Me.Erg_Note.Name = "Erg_Note"
        Me.Erg_Note.Size = New System.Drawing.Size(48, 17)
        Me.Erg_Note.TabIndex = 2
        Me.Erg_Note.Text = "Note"
        Me.Erg_Note.UseVisualStyleBackColor = True
        '
        'Erg_Guelt
        '
        Me.Erg_Guelt.AutoSize = True
        Me.Erg_Guelt.Checked = True
        Me.Erg_Guelt.Location = New System.Drawing.Point(6, 19)
        Me.Erg_Guelt.Name = "Erg_Guelt"
        Me.Erg_Guelt.Size = New System.Drawing.Size(75, 17)
        Me.Erg_Guelt.TabIndex = 1
        Me.Erg_Guelt.tabstop = True
        Me.Erg_Guelt.Text = "Gueltigkeit"
        Me.Erg_Guelt.UseVisualStyleBackColor = True
        '
        'Erg_Text
        '
        Me.Erg_Text.BackColor = System.Drawing.SystemColors.Window
        Me.Erg_Text.Location = New System.Drawing.Point(131, 29)
        Me.Erg_Text.Name = "Erg_Text"
        Me.Erg_Text.ReadOnly = True
        Me.Erg_Text.Size = New System.Drawing.Size(205, 20)
        Me.Erg_Text.TabIndex = 0
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Senden_Knopf)
        Me.GroupBox5.Controls.Add(Me.Zeile1_Text)
        Me.GroupBox5.Controls.Add(Me.Zeile2_Text)
        Me.GroupBox5.Location = New System.Drawing.Point(6, 111)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(354, 78)
        Me.GroupBox5.TabIndex = 10
        Me.GroupBox5.tabstop = False
        Me.GroupBox5.Text = "DisplayText"
        '
        'Senden_Knopf
        '
        Me.Senden_Knopf.Location = New System.Drawing.Point(262, 30)
        Me.Senden_Knopf.Name = "Senden_Knopf"
        Me.Senden_Knopf.Size = New System.Drawing.Size(75, 23)
        Me.Senden_Knopf.TabIndex = 10
        Me.Senden_Knopf.Text = "Senden"
        Me.Senden_Knopf.UseVisualStyleBackColor = True
        '
        'Zeile1_Text
        '
        Me.Zeile1_Text.Location = New System.Drawing.Point(6, 19)
        Me.Zeile1_Text.Name = "Zeile1_Text"
        Me.Zeile1_Text.Size = New System.Drawing.Size(238, 20)
        Me.Zeile1_Text.TabIndex = 2
        '
        'Zeile2_Text
        '
        Me.Zeile2_Text.Enabled = False
        Me.Zeile2_Text.Location = New System.Drawing.Point(6, 45)
        Me.Zeile2_Text.Name = "Zeile2_Text"
        Me.Zeile2_Text.Size = New System.Drawing.Size(237, 20)
        Me.Zeile2_Text.TabIndex = 9
        '
        'Disp_Loe
        '
        Me.Disp_Loe.Location = New System.Drawing.Point(253, 82)
        Me.Disp_Loe.Name = "Disp_Loe"
        Me.Disp_Loe.Size = New System.Drawing.Size(106, 23)
        Me.Disp_Loe.TabIndex = 8
        Me.Disp_Loe.Text = "Display löschen"
        Me.Disp_Loe.UseVisualStyleBackColor = True
        '
        'Zeile2_Loe
        '
        Me.Zeile2_Loe.Location = New System.Drawing.Point(169, 82)
        Me.Zeile2_Loe.Name = "Zeile2_Loe"
        Me.Zeile2_Loe.Size = New System.Drawing.Size(75, 23)
        Me.Zeile2_Loe.TabIndex = 7
        Me.Zeile2_Loe.Text = "Z2 löschen"
        Me.Zeile2_Loe.UseVisualStyleBackColor = True
        '
        'Zeile1_Loe
        '
        Me.Zeile1_Loe.Location = New System.Drawing.Point(87, 82)
        Me.Zeile1_Loe.Name = "Zeile1_Loe"
        Me.Zeile1_Loe.Size = New System.Drawing.Size(75, 23)
        Me.Zeile1_Loe.TabIndex = 6
        Me.Zeile1_Loe.Text = "Z1 löschen"
        Me.Zeile1_Loe.UseVisualStyleBackColor = True
        '
        'Piep_Knopf
        '
        Me.Piep_Knopf.Location = New System.Drawing.Point(6, 82)
        Me.Piep_Knopf.Name = "Piep_Knopf"
        Me.Piep_Knopf.Size = New System.Drawing.Size(75, 23)
        Me.Piep_Knopf.TabIndex = 5
        Me.Piep_Knopf.Text = "Piepen"
        Me.Piep_Knopf.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Zeile2_PC)
        Me.GroupBox4.Controls.Add(Me.Zeile2_Tasten)
        Me.GroupBox4.Location = New System.Drawing.Point(247, 19)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(112, 57)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.tabstop = False
        Me.GroupBox4.Text = "Zeile2 via:"
        '
        'Zeile2_PC
        '
        Me.Zeile2_PC.AutoSize = True
        Me.Zeile2_PC.Location = New System.Drawing.Point(6, 33)
        Me.Zeile2_PC.Name = "Zeile2_PC"
        Me.Zeile2_PC.Size = New System.Drawing.Size(70, 17)
        Me.Zeile2_PC.TabIndex = 1
        Me.Zeile2_PC.Text = "Computer"
        Me.Zeile2_PC.UseVisualStyleBackColor = True
        '
        'Zeile2_Tasten
        '
        Me.Zeile2_Tasten.AutoSize = True
        Me.Zeile2_Tasten.Location = New System.Drawing.Point(6, 14)
        Me.Zeile2_Tasten.Name = "Zeile2_Tasten"
        Me.Zeile2_Tasten.Size = New System.Drawing.Size(62, 17)
        Me.Zeile2_Tasten.TabIndex = 0
        Me.Zeile2_Tasten.Text = "KR-Pad"
        Me.Zeile2_Tasten.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Clear_Zeile)
        Me.GroupBox3.Controls.Add(Me.Clear_Buchstaben)
        Me.GroupBox3.Location = New System.Drawing.Point(115, 19)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(126, 56)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.tabstop = False
        Me.GroupBox3.Text = "Clear löscht:"
        '
        'Clear_Zeile
        '
        Me.Clear_Zeile.AutoSize = True
        Me.Clear_Zeile.Location = New System.Drawing.Point(7, 33)
        Me.Clear_Zeile.Name = "Clear_Zeile"
        Me.Clear_Zeile.Size = New System.Drawing.Size(67, 17)
        Me.Clear_Zeile.TabIndex = 1
        Me.Clear_Zeile.Text = "Die Zeile"
        Me.Clear_Zeile.UseVisualStyleBackColor = True
        '
        'Clear_Buchstaben
        '
        Me.Clear_Buchstaben.AutoSize = True
        Me.Clear_Buchstaben.Location = New System.Drawing.Point(7, 14)
        Me.Clear_Buchstaben.Name = "Clear_Buchstaben"
        Me.Clear_Buchstaben.Size = New System.Drawing.Size(82, 17)
        Me.Clear_Buchstaben.TabIndex = 0
        Me.Clear_Buchstaben.Text = "Ein Zeichen"
        Me.Clear_Buchstaben.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.LichtAus)
        Me.GroupBox2.Controls.Add(Me.LichtAn)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 19)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(103, 57)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.tabstop = False
        Me.GroupBox2.Text = "Licht"
        '
        'LichtAus
        '
        Me.LichtAus.AutoSize = True
        Me.LichtAus.Location = New System.Drawing.Point(7, 34)
        Me.LichtAus.Name = "LichtAus"
        Me.LichtAus.Size = New System.Drawing.Size(43, 17)
        Me.LichtAus.TabIndex = 1
        Me.LichtAus.Text = "Aus"
        Me.LichtAus.UseVisualStyleBackColor = True
        '
        'LichtAn
        '
        Me.LichtAn.AutoSize = True
        Me.LichtAn.Location = New System.Drawing.Point(7, 15)
        Me.LichtAn.Name = "LichtAn"
        Me.LichtAn.Size = New System.Drawing.Size(38, 17)
        Me.LichtAn.TabIndex = 0
        Me.LichtAn.Text = "An"
        Me.LichtAn.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Interval = 333
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(230, 10)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(93, 23)
        Me.btnRefresh.TabIndex = 5
        Me.btnRefresh.Text = "Liste Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'frmKR_Pad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(396, 326)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.UI_Grouper)
        Me.Controls.Add(Me.ID_Label)
        Me.Controls.Add(Me.VerbindenKnopf)
        Me.Controls.Add(Me.ComPort)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmKR_Pad"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Kampfrichter-Pad"
        Me.UI_Grouper.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ComPort As System.Windows.Forms.ComboBox
    Friend WithEvents VerbindenKnopf As System.Windows.Forms.Button
    Friend WithEvents ID_Label As System.Windows.Forms.Label
    Friend WithEvents UI_Grouper As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents LichtAus As System.Windows.Forms.RadioButton
    Friend WithEvents LichtAn As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Clear_Zeile As System.Windows.Forms.RadioButton
    Friend WithEvents Clear_Buchstaben As System.Windows.Forms.RadioButton
    Friend WithEvents Disp_Loe As System.Windows.Forms.Button
    Friend WithEvents Zeile2_Loe As System.Windows.Forms.Button
    Friend WithEvents Zeile1_Loe As System.Windows.Forms.Button
    Friend WithEvents Piep_Knopf As System.Windows.Forms.Button
    Friend WithEvents Zeile2_PC As System.Windows.Forms.RadioButton
    Friend WithEvents Zeile2_Tasten As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Senden_Knopf As System.Windows.Forms.Button
    Friend WithEvents Zeile1_Text As System.Windows.Forms.TextBox
    Friend WithEvents Zeile2_Text As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Erg_Text As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Erg_Note As System.Windows.Forms.RadioButton
    Friend WithEvents Erg_Guelt As System.Windows.Forms.RadioButton
    Friend WithEvents btnRefresh As System.Windows.Forms.Button

End Class
