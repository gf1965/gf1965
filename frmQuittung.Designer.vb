﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQuittung
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvJoin = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnVorschau = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblMeldeschluss = New System.Windows.Forms.Label()
        Me.lblNachmeldeschluss = New System.Windows.Forms.Label()
        Me.chkAlleHeber = New System.Windows.Forms.CheckBox()
        Me.txtSumme = New System.Windows.Forms.TextBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.chkDatum = New System.Windows.Forms.CheckBox()
        Me.lstVerein = New System.Windows.Forms.CheckedListBox()
        Me.chkAll = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkAutoBook = New System.Windows.Forms.CheckBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboVorlage = New System.Windows.Forms.ComboBox()
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvJoin
        '
        Me.dgvJoin.AllowUserToAddRows = False
        Me.dgvJoin.AllowUserToDeleteRows = False
        Me.dgvJoin.AllowUserToResizeColumns = False
        Me.dgvJoin.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvJoin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvJoin.Location = New System.Drawing.Point(208, 30)
        Me.dgvJoin.MultiSelect = False
        Me.dgvJoin.Name = "dgvJoin"
        Me.dgvJoin.RowHeadersVisible = False
        Me.dgvJoin.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvJoin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvJoin.Size = New System.Drawing.Size(495, 199)
        Me.dgvJoin.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "&Verein"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(590, 242)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Summe"
        '
        'btnVorschau
        '
        Me.btnVorschau.Location = New System.Drawing.Point(723, 30)
        Me.btnVorschau.Name = "btnVorschau"
        Me.btnVorschau.Size = New System.Drawing.Size(100, 25)
        Me.btnVorschau.TabIndex = 6
        Me.btnVorschau.Text = "Druck&vorschau"
        Me.btnVorschau.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(363, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Meldeschluss"
        Me.Label3.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(533, 12)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Nachmeldeschluss"
        Me.Label4.Visible = False
        '
        'lblMeldeschluss
        '
        Me.lblMeldeschluss.BackColor = System.Drawing.SystemColors.Window
        Me.lblMeldeschluss.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMeldeschluss.Location = New System.Drawing.Point(440, 9)
        Me.lblMeldeschluss.Name = "lblMeldeschluss"
        Me.lblMeldeschluss.Size = New System.Drawing.Size(68, 19)
        Me.lblMeldeschluss.TabIndex = 11
        Me.lblMeldeschluss.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblMeldeschluss.Visible = False
        '
        'lblNachmeldeschluss
        '
        Me.lblNachmeldeschluss.BackColor = System.Drawing.SystemColors.Window
        Me.lblNachmeldeschluss.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNachmeldeschluss.Location = New System.Drawing.Point(635, 9)
        Me.lblNachmeldeschluss.Name = "lblNachmeldeschluss"
        Me.lblNachmeldeschluss.Size = New System.Drawing.Size(68, 19)
        Me.lblNachmeldeschluss.TabIndex = 12
        Me.lblNachmeldeschluss.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblNachmeldeschluss.Visible = False
        '
        'chkAlleHeber
        '
        Me.chkAlleHeber.AutoSize = True
        Me.chkAlleHeber.BackColor = System.Drawing.SystemColors.Control
        Me.chkAlleHeber.Location = New System.Drawing.Point(222, 239)
        Me.chkAlleHeber.Name = "chkAlleHeber"
        Me.chkAlleHeber.Size = New System.Drawing.Size(195, 17)
        Me.chkAlleHeber.TabIndex = 13
        Me.chkAlleHeber.Text = "für alle gemeldeten Heber kassieren"
        Me.chkAlleHeber.UseVisualStyleBackColor = False
        '
        'txtSumme
        '
        Me.txtSumme.Location = New System.Drawing.Point(638, 239)
        Me.txtSumme.Name = "txtSumme"
        Me.txtSumme.Size = New System.Drawing.Size(65, 20)
        Me.txtSumme.TabIndex = 14
        Me.txtSumme.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(723, 61)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(100, 25)
        Me.btnPrint.TabIndex = 16
        Me.btnPrint.Text = "&Drucken"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'chkDatum
        '
        Me.chkDatum.AutoSize = True
        Me.chkDatum.Checked = True
        Me.chkDatum.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDatum.Location = New System.Drawing.Point(222, 262)
        Me.chkDatum.Name = "chkDatum"
        Me.chkDatum.Size = New System.Drawing.Size(201, 17)
        Me.chkDatum.TabIndex = 17
        Me.chkDatum.Text = "Wettkampftag ist Datum der Quittung"
        Me.chkDatum.UseVisualStyleBackColor = True
        '
        'lstVerein
        '
        Me.lstVerein.FormattingEnabled = True
        Me.lstVerein.Location = New System.Drawing.Point(12, 30)
        Me.lstVerein.Name = "lstVerein"
        Me.lstVerein.Size = New System.Drawing.Size(192, 199)
        Me.lstVerein.TabIndex = 32
        '
        'chkAll
        '
        Me.chkAll.AutoSize = True
        Me.chkAll.BackColor = System.Drawing.SystemColors.Control
        Me.chkAll.Location = New System.Drawing.Point(11, 239)
        Me.chkAll.Name = "chkAll"
        Me.chkAll.Padding = New System.Windows.Forms.Padding(4, 3, 0, 1)
        Me.chkAll.Size = New System.Drawing.Size(100, 21)
        Me.chkAll.TabIndex = 31
        Me.chkAll.Text = "alle aus&wählen"
        Me.chkAll.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(211, 13)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 13)
        Me.Label5.TabIndex = 33
        Me.Label5.Text = "&Meldung"
        '
        'chkAutoBook
        '
        Me.chkAutoBook.AutoSize = True
        Me.chkAutoBook.Checked = True
        Me.chkAutoBook.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAutoBook.Location = New System.Drawing.Point(222, 285)
        Me.chkAutoBook.Name = "chkAutoBook"
        Me.chkAutoBook.Size = New System.Drawing.Size(191, 17)
        Me.chkAutoBook.TabIndex = 34
        Me.chkAutoBook.Text = "beim Drucken automatisch buchen"
        Me.chkAutoBook.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Enabled = False
        Me.btnCancel.Location = New System.Drawing.Point(723, 92)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(100, 25)
        Me.btnCancel.TabIndex = 35
        Me.btnCancel.Text = "&Abbrechen"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'lblProgress
        '
        Me.lblProgress.AutoSize = True
        Me.lblProgress.Location = New System.Drawing.Point(14, 302)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(85, 13)
        Me.lblProgress.TabIndex = 37
        Me.lblProgress.Text = "Druck-Fortschritt"
        '
        'ProgressBar
        '
        Me.ProgressBar.Location = New System.Drawing.Point(11, 319)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(405, 22)
        Me.ProgressBar.TabIndex = 36
        '
        'btnSearch
        '
        Me.btnSearch.Image = Global.Gewichtheben.My.Resources.Resources.Zoom16
        Me.btnSearch.Location = New System.Drawing.Point(709, 318)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(34, 25)
        Me.btnSearch.TabIndex = 40
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(462, 302)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 13)
        Me.Label6.TabIndex = 39
        Me.Label6.Text = "Druck-Vorlage"
        '
        'cboVorlage
        '
        Me.cboVorlage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVorlage.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboVorlage.FormattingEnabled = True
        Me.cboVorlage.Location = New System.Drawing.Point(459, 320)
        Me.cboVorlage.Name = "cboVorlage"
        Me.cboVorlage.Size = New System.Drawing.Size(244, 21)
        Me.cboVorlage.Sorted = True
        Me.cboVorlage.TabIndex = 38
        '
        'OpenFileDialog
        '
        Me.OpenFileDialog.Title = "Druckvorlagen suchen"
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'frmQuittung
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(840, 352)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cboVorlage)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.chkAutoBook)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lstVerein)
        Me.Controls.Add(Me.chkAll)
        Me.Controls.Add(Me.chkDatum)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.txtSumme)
        Me.Controls.Add(Me.chkAlleHeber)
        Me.Controls.Add(Me.lblNachmeldeschluss)
        Me.Controls.Add(Me.lblMeldeschluss)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnVorschau)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvJoin)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmQuittung"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Quittung drucken"
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvJoin As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnVorschau As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblMeldeschluss As Label
    Friend WithEvents lblNachmeldeschluss As Label
    Friend WithEvents chkAlleHeber As CheckBox
    Friend WithEvents txtSumme As TextBox
    Friend WithEvents btnPrint As Button
    Friend WithEvents chkDatum As CheckBox
    Friend WithEvents lstVerein As CheckedListBox
    Friend WithEvents chkAll As CheckBox
    Friend WithEvents Label5 As Label
    Friend WithEvents chkAutoBook As CheckBox
    Friend WithEvents btnCancel As Button
    Friend WithEvents lblProgress As Label
    Friend WithEvents ProgressBar As ProgressBar
    Friend WithEvents btnSearch As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents cboVorlage As ComboBox
    Friend WithEvents OpenFileDialog As OpenFileDialog
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
End Class
