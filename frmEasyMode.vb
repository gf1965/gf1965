﻿
Imports System.ComponentModel

Public Class frmEasyMode

    Private Delegate Sub DelegateIntegerString(ByVal Status As Integer, ByVal Value As String)
    Private Delegate Sub DelegateString(ByVal Value As String)
    Private Delegate Sub DelegateBoolean(ByVal Value As Boolean)
    Private Delegate Sub DelegateColor(ByVal Value As Color)
    ' Private sc As New ScreenScale

    'Dim PrivateFonts As New PrivateFontCollection
    'Dim fontFamilies() As FontFamily
    Dim dicLampen As New Dictionary(Of Integer, TextBox) 'Container für Gültig
    Dim xInvalid As New Dictionary(Of Integer, TextBox) 'Container für Ungültig
    Dim tmpBohle As Integer

    Private Sub btnAufruf_Click(sender As Object, e As EventArgs) Handles btnAufruf.Click
        If btnAufruf.Text = "Aufruf starten" Then
            btnAufruf.Text = "Aufruf stoppen"
        ElseIf btnAufruf.Text = "Aufruf stoppen" Then
            btnAufruf.Text = "Aufruf starten"
        End If
        WA_Events.Push_ZN(vbLf)
    End Sub
    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        If cboKR.Text = "" Then
            Using New Centered_MessageBox(Me, {"Automatisch"})
                Select Case MessageBox.Show("Die Anzahl der Kampfrichter wurde nicht ausgewählt.", msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation)
                    Case DialogResult.OK
                        Wettkampf.KR(User.Bohle) = 0
                    Case DialogResult.Cancel
                        Exit Sub
                End Select
            End Using
        Else
            Wettkampf.KR(User.Bohle) = CInt(cboKR.Text)
        End If

        If cboBohle.SelectedIndex = -1 Then cboBohle.SelectedIndex = 0
        'If formMain.mnuBohle.Checked = False Then formMain.mnuBohle.PerformClick()

        If chkHantel.Checked Then
            If Not formMain.MdiChildren.Contains(formHantelbeladung) Then
                Wettkampf.WK_Modus = Modus.EasyHantel
                Try
                    formHantelbeladung.Dispose()
                Catch ex As Exception
                End Try
                If IsNothing(formHantelbeladung) OrElse formHantelbeladung.IsDisposed Then formHantelbeladung = New frmHantelBeladung With {.MdiParent = formMain}
                formHantelbeladung.Show()
            End If
            Stop
            'If IsNothing(formHantel) OrElse formHantel.IsDisposed Then formHantel = New frmHantel
            'With formHantel
            '    .Controls.Clear()
            '    .Visible = True
            'End With
        End If

        WK_Reset(True)

        btnStart.Enabled = False

    End Sub
    Private Sub btnStart_EnabledChanged(sender As Object, e As EventArgs) Handles btnStart.EnabledChanged
        btnAufruf.Enabled = Not btnStart.Enabled
        cboBohle.Enabled = btnStart.Enabled
        cboKR.Enabled = btnStart.Enabled
        chkTechnik.Enabled = btnStart.Enabled
        chkHantel.Enabled = btnStart.Enabled
    End Sub
    Private Sub btnStart_Enabled(Value As Boolean)
        btnStart.Enabled = Value
    End Sub

    Private Sub Me_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Cursor = Cursors.WaitCursor

        User.Bohle = tmpBohle

        RemoveHandler WA_Message.Status_Changed, AddressOf statusMsg_Update
        'RemoveHandler Anzeige.Minuten_Changed, AddressOf Minuten_Changed
        'RemoveHandler Anzeige.Sekunden_Changed, AddressOf Sekunden_Changed
        RemoveHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed
        RemoveHandler Anzeige.Lampe_Changed, AddressOf Change_Lampe
        RemoveHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        RemoveHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed
        'RemoveHandler Wettkampf.KR_Changed, AddressOf KR_Changed
        RemoveHandler Set_formEasyMode_btnStart_Enabled, AddressOf btnStart_Enabled

        RemoveHandler ForeColors_Change, AddressOf Wertung_Clear

        NativeMethods.INI_Write("EasyModus", "Location", Location.X & ";" & Location.Y, App_IniFile)
        NativeMethods.INI_Write("EasyModus", "ShowHantel", chkHantel.Checked.ToString, App_IniFile)
        NativeMethods.INI_Write("EasyModus", "Technik", chkTechnik.Checked.ToString, App_IniFile)

        With formMain
            .mnuWK_Select.Enabled = True
            .mnuWK_New.Enabled = True
            .mnuWK_Delete.Enabled = True
            .mnuGW.Enabled = True
            .mnuAthletik.Enabled = True
            .mnuClearHeben.Enabled = True
            .mnuClearMeldung.Enabled = True
            .mnuKorrektur.Enabled = True
            .mnuMeldung.Enabled = True
            .PictureBox1.Show()
        End With

        Steuerung.KR_Steuerung_Dispose()
        Cursor = Cursors.Default
    End Sub
    Private Sub Me_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed

        For Each key In IsFormPresent({"Bohle", "Hantelbeladung"})
            dicScreens(key.Key).Form.Close()
        Next

        Wettkampf.WK_Modus = Modus.Indeterminate

    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        tmpBohle = User.Bohle

        With formMain
            .mnuWK_Select.Enabled = False
            .mnuWK_New.Enabled = False
            .mnuWK_Delete.Enabled = False
            .mnuGW.Enabled = False
            .mnuAthletik.Enabled = False
            .mnuClearHeben.Enabled = False
            .mnuClearMeldung.Enabled = False
            .mnuKorrektur.Enabled = False
            .mnuMeldung.Enabled = False
            .PictureBox1.Hide()
        End With

        'Wertung
        dicLampen.Add(1, xGültig1)
        dicLampen.Add(2, xGültig2)
        dicLampen.Add(3, xGültig3)

        Dim Pos() As String = Split(NativeMethods.INI_Read("EasyModus", "Location", App_IniFile), ";")
        If Pos(0).ToString <> "?" Then
            Location = New Point(CInt(Pos(0)), CInt(Pos(1)))
        Else
            Location = New Point(45, 50)
        End If
        Dim x As String
        x = NativeMethods.INI_Read("EasyModus", "ShowHantel", App_IniFile)
        If x <> "?" Then chkHantel.Checked = CBool(x)
        x = NativeMethods.INI_Read("EasyModus", "Technik", App_IniFile)
        If x <> "?" Then chkTechnik.Checked = CBool(x)

        Try
            For i As Integer = 1 To 3
                dicLampen(i).Font = New Font(FontFamilies(2), dicLampen(i).Font.Size, FontStyle.Regular, GraphicsUnit.Point)
                xInvalid(i).Font = New Font(FontFamilies(2), xInvalid(i).Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            Next
            xTechniknote.Font = New Font(fontFamilies(1), xTechniknote.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            'xAufruf_std.Font = New Font(fontFamilies(0), xAufruf_min.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf_min.Font = New Font(fontFamilies(0), xAufruf_min.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf_sek.Font = New Font(fontFamilies(0), xAufruf_sek.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf_pkt.Font = New Font(fontFamilies(0), xAufruf_pkt.Font.Size, FontStyle.Bold, GraphicsUnit.Point)
            'xAufruf_p_s.Font = New Font(fontFamilies(0), xAufruf_sek.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
        Catch ex As Exception
        End Try

        AddHandler WA_Message.Status_Changed, AddressOf statusMsg_Update
        'AddHandler Anzeige.Minuten_Changed, AddressOf Minuten_Changed
        'AddHandler Anzeige.Sekunden_Changed, AddressOf Sekunden_Changed
        AddHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed
        AddHandler Anzeige.Lampe_Changed, AddressOf Change_Lampe
        AddHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        AddHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed
        AddHandler Anzeige.Technik_Visible_Changed, AddressOf TechnikVisible_Changed

        'AddHandler Wettkampf.KR_Changed, AddressOf KR_Changed
        AddHandler Set_formEasyMode_btnStart_Enabled, AddressOf btnStart_Enabled

        AddHandler ForeColors_Change, AddressOf Wertung_Clear

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        ' falls Bohle im Normal-Modus geöffnet war, muss sie geschlossen und erneut im Easy-Modus geöffnet werden
        Dim KeyList = IsFormPresent({"Bohle"})
        For Each item In KeyList
            dicScreens(item.Key).Form.Close()
        Next

        Wettkampf.WK_Modus = Modus.Easy

        ' falls Bohle offen war, erneut öffnen
        For Each item In KeyList
            'dicScreens(item.Key) = New KeyValuePair(Of Form, Integer)(New frmBohle, item.Value)
            dicScreens(item.Key) = New ValueList With {.Form = New frmBohle, .Display = item.Value, .Fontsize = -1}
            dicScreens(item.Key).Form.Show()
            Using sc As New ScreenScale
                sc.SetWindow(item.Value - 1, 1, dicScreens(item.Key).Form)
            End Using
        Next

        Cursor = Cursors.Default
    End Sub

    Private Sub cboBohle_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBohle.SelectedIndexChanged
        If Not cboBohle.SelectedIndex < 0 Then User.Bohle = CInt(cboBohle.Text)
    End Sub
    Private Sub cboKR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboKR.SelectedIndexChanged
        Wettkampf.KR(User.Bohle) = CInt(cboKR.Text)
    End Sub

    Private Sub chkHantel_CheckedChanged(sender As Object, e As EventArgs) Handles chkHantel.CheckedChanged
        If chkHantel.Checked Then
            Wettkampf.WK_Modus = Modus.EasyHantel
        Else
            Wettkampf.WK_Modus = Modus.Easy
        End If
        Wettkampf.ShowHantel = chkHantel.Checked
    End Sub
    Private Sub chkTechnik_CheckedChanged(sender As Object, e As EventArgs) Handles chkTechnik.CheckedChanged
        Try
            Wettkampf.Technikwertung = chkTechnik.Checked
            xTechniknote.Visible = chkTechnik.Checked
        Catch ex As Exception
        End Try
    End Sub

    '' Events der Anzeige
    Private Sub Stunden_Changed(Stunden As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateString(AddressOf Stunden_Changed)
                Invoke(d, New Object() {Stunden})
            Else
                'xAufruf_std.Text = Stunden
            End If
        Catch
        End Try
    End Sub
    Private Sub DplPunkt_Changed(DplPunkt As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateString(AddressOf DplPunkt_Changed)
                Invoke(d, New Object() {DplPunkt})
            Else
                'xAufruf_p_s.Text = DplPunkt
            End If
        Catch
        End Try
    End Sub
    Private Sub Minuten_Changed(Minuten As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateString(AddressOf Minuten_Changed)
                Invoke(d, New Object() {Minuten})
            Else
                xAufruf_min.Text = Minuten
            End If
        Catch
        End Try
    End Sub
    Private Sub Sekunden_Changed(Sekunden As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateString(AddressOf Sekunden_Changed)
                Invoke(d, New Object() {Sekunden})
            Else
                xAufruf_sek.Text = Sekunden
            End If
        Catch
        End Try
    End Sub
    Private Sub ClockColor_Changed(Color As Color)
        'xAufruf_std.ForeColor = Color
        'xAufruf_p_s.ForeColor = Color
        xAufruf_min.ForeColor = Color
        xAufruf_sek.ForeColor = Color
        xAufruf_pkt.ForeColor = Color
        If Color = Anzeige.Clock_Colors(Clock_Status.Stopped) AndAlso btnAufruf.Text.Contains("Pause") Then btnAufruf.PerformClick()
    End Sub
    Private Sub Change_Lampe(Index As Integer, Color As Color)
        dicLampen(Index).ForeColor = Color
    End Sub
    Private Sub TechnikWert_Changed(Wert As String)
        If InvokeRequired Then
            Dim d As New DelegateString(AddressOf TechnikWert_Changed)
            Invoke(d, New Object() {Wert})
        Else
            xTechniknote.Text = Wert
        End If
    End Sub
    Private Sub TechnikFarbe_Changed(Farbe As Color)
        If InvokeRequired Then
            Dim d As New DelegateColor(AddressOf TechnikFarbe_Changed)
            Invoke(d, New Object() {Farbe})
        Else
            xTechniknote.ForeColor = Farbe
        End If
    End Sub
    Private Sub TechnikVisible_Changed(Value As Boolean)
        If InvokeRequired Then
            Dim d As New DelegateBoolean(AddressOf TechnikVisible_Changed)
            Invoke(d, New Object() {Value})
        Else
            xTechniknote.Visible = Value
        End If
    End Sub

    Private Sub Wertung_Clear(HeberPresent As Boolean)
        If Not HeberPresent Then ' Wertungen zurücksetzen
            For i = 1 To 3
                dicLampen(i).ForeColor = Ansicht_Options.Shade_Color
                xInvalid(i).ForeColor = Ansicht_Options.Shade_Color
            Next
            xTechniknote.ForeColor = Ansicht_Options.Shade_Color
            xTechniknote.Text = "X,XX"
        End If
    End Sub

    '' an frmLeader angleichen
    Private Sub statusMsg_Update(Status As Integer, Message As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateIntegerString(AddressOf statusMsg_Update)
                Invoke(d, New Object() {Status, Message})
            Else
                If Not Visible Then Return

                'If Not IsNumeric(cboKR.Text) OrElse CInt(cboKR.Text) <> KR Then
                cboKR.Text = Wettkampf.KR(User.Bohle).ToString
                'End If

                statusMsg.Text = Message
                If Status = 0 Then
                    statusMsg.ForeColor = Color.Green
                Else
                    statusMsg.ForeColor = Color.Red
                    Select Case Status
                        Case > 199
                            Using New Centered_MessageBox(Me)
                                MessageBox.Show("Falsches KR-Pad angeschlossen." + vbNewLine + "Bei 1 KR muss KR-Pad 1 angeschlossen werden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End Using
                            For Each key In IsFormPresent({"Bohle"})
                                dicScreens(key.Key).Form.Close()
                            Next
                        Case > 99
                            Dim msg As String = "Keine KR-Anlage angeschlossen." & vbNewLine & "Anlage prüfen und neu starten."
                            Using New Centered_MessageBox(Me)
                                MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End Using
                            'WA_Message.Update("keine Wertungsanlage")
                    End Select
                    btnAufruf.Visible = Not ZN_Pad.Connected

                    Select Case WA_Message.Status
                        Case 7, 15, 23, 31, 39, 47, 55, 63, 70, 163 ' manuelle Wertung
                            If IsFormPresent({"Bohle"}).Count = 0 Then
                                Using New Centered_MessageBox(Me)
                                    If MessageBox.Show("Wertungsanlage nicht bereit. Anzeige für Bohle " & User.Bohle & " initialisieren?",
                                                   msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then
                                        If frmScreens.ShowDialog(Me) = DialogResult.Cancel Then
                                            btnAufruf.Enabled = False
                                            btnStart.Enabled = False
                                            Return
                                        End If
                                    Else
                                        btnAufruf.Enabled = False
                                        btnStart.Enabled = False
                                        Exit Sub
                                    End If
                                End Using
                            End If
                        Case Else
                            btnStart.Enabled = True
                    End Select
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Controls_Gotfocus(sender As Object, e As EventArgs) Handles xAufruf_min.GotFocus, xAufruf_pkt.GotFocus, xAufruf_sek.GotFocus,
        xGültig1.GotFocus, xGültig2.GotFocus, xGültig3.GotFocus, xTechniknote.GotFocus
        btnStart.Focus()
    End Sub
End Class