﻿
Imports System.ComponentModel

Public Class frmQuittung

    Dim Query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim dtVerein As New DataTable
    Dim dtFrist As New DataTable
    Dim dtJoin As New DataTable
    Dim dtTeam As New DataTable
    Dim dtDatum As DataTable
    Dim bs As BindingSource
    'Dim attend As New Dictionary(Of Integer, Boolean)
    Dim loading As Boolean
    Dim Count As Integer
    Dim report As FastReport.Report
    Dim Message As New myMsg
    Dim Druckvorlage As String
    Dim Action As String
    Private Delegate Sub SetMsg(ByVal Value As String)
    Private Delegate Sub SetRow(ByVal Value As Object)
    Dim Verein As DataRow

    Class myMsg
        Private _msg As String
        Event Msg_Changed(ByVal Value As String)
        Property msg As String
            Get
                Return _msg
            End Get
            Set(value As String)
                _msg = value
                RaiseEvent Msg_Changed(_msg)
            End Set
        End Property
    End Class

    Private Sub frmQuittung_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        BackgroundWorker1.CancelAsync()
        Save()
        RemoveHandler Message.Msg_Changed, AddressOf ProgressMsg
        If Druckvorlage <> String.Empty Then NativeMethods.INI_Write("Quittung", "Druckvorlage", Druckvorlage, INI_File)
        NativeMethods.INI_Write("Quittung", "GlobAll", chkAlleHeber.Checked.ToString, INI_File)
        NativeMethods.INI_Write("Quittung", "EqualDate", chkDatum.Checked.ToString, INI_File)
        NativeMethods.INI_Write("Quittung", "GlobAutoBook", chkAutoBook.Checked.ToString, INI_File)
    End Sub

    Private Sub frmQuittung_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        Dim vorlagen As String() = Directory.GetFiles(Path.Combine(Application.StartupPath, "Report"), "*.frx", SearchOption.AllDirectories)
        Dim _v As String()
        For Each v In vorlagen
            _v = Split(v, "\")
            v = _v(_v.Count - 1)
            v = Replace(v, ".frx", "")
            cboVorlage.Items.Add(v)
        Next

        Dim inp As String
        inp = NativeMethods.INI_Read("Quittung", "GlobAll", INI_File)
        If inp <> "?" Then chkAlleHeber.Checked = CBool(inp)
        inp = NativeMethods.INI_Read("Quittung", "EqualDate", INI_File)
        If inp <> "?" Then chkDatum.Checked = CBool(inp)
        inp = NativeMethods.INI_Read("Quittung", "AutoBook", INI_File)
        If inp <> "?" Then chkAutoBook.Checked = CBool(inp)

        Druckvorlage = NativeMethods.INI_Read("Quittung", "Druckvorlage", INI_File)
        If Druckvorlage = "?" Or cboVorlage.Items.Contains(Druckvorlage) = False Then
            Druckvorlage = String.Empty
        Else
            cboVorlage.Text = Druckvorlage
        End If

        AddHandler Message.Msg_Changed, AddressOf ProgressMsg

    End Sub

    Private Sub frmQuittung_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        loading = True

        Using conn As New MySqlConnection(MySqlConnString)
            Try
                conn.Open()
                Query = "select distinct v.idVerein, v.Vereinsname from athleten a inner join meldung m on m.Teilnehmer = a.idTeilnehmer " &
                    "inner join verein v on v.idVerein = a.Verein where m.Wettkampf = " & Wettkampf.ID & " order by Vereinsname;"
                cmd = New MySqlCommand(Query, conn)
                dtVerein.Load(cmd.ExecuteReader())
                Query = "SELECT * FROM wettkampf_meldefrist WHERE Wettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtFrist.Load(cmd.ExecuteReader())
                Query = "select idVerein, Vereinsname from verein inner join wettkampf_teams on Team = idVerein where Wettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtTeam.Load(cmd.ExecuteReader())
            Catch ex As Exception
                Stop
            End Try
        End Using

        With lstVerein
            .DataSource = dtVerein
            .DisplayMember = "Vereinsname"
            .ValueMember = "idVerein"
            .SelectedIndex = -1
        End With

        If dtFrist.Rows.Count > 0 Then
            If Not IsDBNull(dtFrist.Rows(0)("Datum1")) Then lblMeldeschluss.Text = Format(CDate(dtFrist.Rows(0)("Datum1")), "dd.MM.yyyy")
            If Not IsDBNull(dtFrist.Rows(0)("Datum2")) Then lblNachmeldeschluss.Text = Format(dtFrist.Rows(0)("Datum2"), "dd.MM.yyyy")
        End If

        Build_Grid()
        bs = New BindingSource With {.DataSource = dtJoin}
        dgvJoin.DataSource = bs
        loading = False
        With lstVerein
            .Focus()
            .SelectedIndex = 0
        End With
        Cursor = Cursors.Default
    End Sub

    Private Sub lstVerein_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstVerein.SelectedIndexChanged
        If loading Then Exit Sub
        loading = True
        Save()
        GetHeber(lstVerein.SelectedValue.ToString)
        With dgvJoin
            .DataSource = Nothing
            bs = New BindingSource With {.DataSource = dtJoin}
            .DataSource = bs
            For i As Integer = 0 To .Rows.Count - 1
                With .Rows(i)
                    .Cells("Gebühr").Tag = .Cells("Gebühr").Value
                End With
            Next
        End With
        Globulate()
    End Sub

    Private Sub GetHeber(idVerein As String)
        Query = "Select m.Teilnehmer, m.attend, m.bezahlt, a.Titel, a.Nachname, a.Vorname, m.Meldedatum Meldung, wm.Datum2 Frist, wm.Gebuehr1 Startgeld " &
                "From meldung m inner Join athleten a on m.Teilnehmer = a.idTeilnehmer, " &
                "(select * from wettkampf_meldefrist wm where Wettkampf = " & Wettkampf.ID & ") wm " &
                "where m.Wettkampf = " & Wettkampf.ID &
                " and a.Verein = " & idVerein &
                " group by m.Teilnehmer " &
                "order by abs(wm.Datum2 - m.Meldedatum), a.Nachname, a.Vorname;"

        Using conn As New MySqlConnection(MySqlConnString)
            Try
                conn.Open()
                cmd = New MySqlCommand(Query, conn)
                dtJoin = New DataTable
                dtJoin.Load(cmd.ExecuteReader())
            Catch ex As Exception
                Stop
            End Try
        End Using

        Dim team As DataRow() = dtTeam.Select("idVerein = " + idVerein)
        If team.Count = 1 And dtFrist.Rows.Count = 3 Then
            'Mannschaft
            Dim row As DataRow = dtJoin.NewRow
            row(0) = lstVerein.SelectedValue
            row(1) = True
            row(2) = ""
            row(3) = "Mannschaft"
            row(4) = lstVerein.Text
            row(5) = DBNull.Value
            row(6) = DBNull.Value
            row(7) = dtFrist(2)("Gebuehr")
            dtJoin.Rows.Add(row)
        End If

    End Sub

    Private Sub Build_Grid()

        Dim ColWidth() As Integer = {35, 70, 70, 90, 70}
        Dim ColBez() As String = {"WK", "Nachname", "Vorname", "Meldedatum", "Startgeld"}
        Dim ColData() As String = {"attend", "Nachname", "Vorname", "Meldung", "Startgeld"}
        With dgvJoin
            .AutoGenerateColumns = False
            .AlternatingRowsDefaultCellStyle.BackColor = Color.OldLace
            .RowsDefaultCellStyle.BackColor = Color.LightCyan
            .AllowUserToOrderColumns = False
            With .Columns
                .Add(New DataGridViewCheckBoxColumn With {.Name = "WK", .ThreeState = False, .ValueType = GetType(Boolean)})
                .Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Add(New DataGridViewTextBoxColumn With {.Name = "Datum"})
                .Add(New DataGridViewTextBoxColumn With {.Name = "Gebühr"})
            End With
            For i As Integer = 0 To ColWidth.Count - 1
                With .Columns(i)
                    .Width = ColWidth(i)
                    .HeaderText = ColBez(i)
                    .DataPropertyName = ColData(i)
                    .SortMode = DataGridViewColumnSortMode.NotSortable
                End With
            Next
            .Columns("Gebühr").DefaultCellStyle.Format = "0.00"
            .Columns("Gebühr").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        'Format_Grid(Bereich, grid)
        'LayoutChanged = False

    End Sub

    Private Sub Globulate()

        Dim summe As Double

        Cursor = Cursors.WaitCursor
        With dgvJoin
            For i As Integer = 0 To .Rows.Count - 1
                Try
                    With .Rows(i)
                        If chkAlleHeber.Checked = False Then
                            'nur für anwesende Heber kassieren
                            If CBool(.Cells("WK").Value) = True Then summe += CDbl(.Cells("Gebühr").Value)
                        Else
                            summe += CDbl(.Cells("Gebühr").Value)
                        End If
                    End With
                Catch ex As Exception
                End Try
            Next
        End With

        txtSumme.Text = Format(summe, "#,##0.00")

        loading = False
        Cursor = Cursors.Default

    End Sub

    Private Sub Save()
        Validate()
        bs.EndEdit()
        Dim changes As DataTable
        changes = dtJoin.GetChanges()
        If Not IsNothing(changes) Then
            For Each row As DataRow In changes.Rows
                Query = "UPDATE `meldung` SET `attend` = @attend, `bezahlt` = @bezahlt WHERE `idMeldung` = @id"
                Dim conn As New MySqlConnection((MySqlConnString))
                Dim cmd As New MySqlCommand(Query, conn)
                With cmd.Parameters
                    .AddWithValue("@id", row("idMeldung"))
                    .AddWithValue("@attend", row("attend"))
                    .AddWithValue("@bezahlt", row("bezahlt"))
                End With
                Try
                    Using conn
                        conn.Open()
                        cmd.ExecuteNonQuery()
                    End Using
                Catch ex As Exception
                    MessageBox.Show(ex.ToString)
                End Try
            Next
        End If
    End Sub

    Private Sub dgvJoin_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvJoin.CellValueChanged
        If loading Then Exit Sub
        If dgvJoin.Columns(e.ColumnIndex).Name = "WK" Then
            With dgvJoin.Rows(e.RowIndex)
                If CBool(.Cells("WK").Value) = False Then
                    'Ausschluss
                    If chkAlleHeber.Checked = False Then
                        .Cells("Gebühr").Value = 0
                    Else
                        .Cells("Gebühr").Value = .Cells("Gebühr").Tag
                    End If
                Else
                    .Cells("Gebühr").Value = .Cells("Gebühr").Tag
                End If
            End With
            Globulate()
        End If
    End Sub

    Private Sub dgvJoin_CellMouseUp(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvJoin.CellMouseUp
        If dgvJoin.Columns(e.ColumnIndex).Name = "WK" Then
            With dgvJoin.Rows(e.RowIndex)
                Dim msg As String = .Cells("Nachname").Value.ToString + ", " + .Cells("Vorname").Value.ToString
                If CBool(.Cells("WK").Value) = True Then
                    msg += " vom Wettkampf ausschließen?"
                Else
                    msg += " zum Wettkampf zulassen?"
                End If
                Using New Centered_MessageBox(Me)
                    Select Case MessageBox.Show(msg, Wettkampf.Bezeichnung, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        Case DialogResult.Yes
                            dgvJoin.EndEdit()
                        Case DialogResult.No
                            dgvJoin.RefreshEdit()
                    End Select
                End Using
            End With
        End If
    End Sub

    Private Sub dgvJoin_SelectionChanged(sender As Object, e As EventArgs) Handles dgvJoin.SelectionChanged
        dgvJoin.ClearSelection()
    End Sub

    Private Sub txtSumme_LostFocus(sender As Object, e As EventArgs) Handles txtSumme.LostFocus
        Dim summe As Double = CDbl(txtSumme.Text)
        txtSumme.Text = Format(summe, "#,##0.00")
    End Sub

    Private Sub chkAlleHeber_CheckedChanged(sender As Object, e As EventArgs) Handles chkAlleHeber.CheckedChanged
        With dgvJoin
            For i As Integer = 0 To .Rows.Count - 1
                With .Rows(i)
                    If CBool(.Cells("WK").Value) = False And chkAlleHeber.Checked = False Then
                        .Cells("Gebühr").Value = 0
                    Else
                        .Cells("Gebühr").Value = .Cells("Gebühr").Tag
                    End If
                End With
            Next
        End With
        Globulate()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        BackgroundWorker1.CancelAsync()
    End Sub

    Private Sub btnDrucken_Click(sender As Object, e As EventArgs) Handles btnVorschau.Click, btnPrint.Click

        If lstVerein.CheckedIndices.Count = 0 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Kein Verein ausgewählt.", "Quittungen drucken", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
            Exit Sub
        End If

        If BackgroundWorker1.IsBusy <> True Then
            If Druckvorlage = String.Empty Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Keine Druckvorlage ausgewählt.", "Quittungen drucken", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                Exit Sub
            End If
            Cursor = Cursors.WaitCursor
            If sender.Equals(btnVorschau) Then
                Action = "Show"
            Else
                Action = "Print"
            End If
            dtDatum = New DataTable
            If chkDatum.Checked Then
                Query = "select m.Gruppe, g.Datum, a.Verein " &
                    "From athleten a inner join meldung m " &
                    "on m.Teilnehmer = a.idTeilnehmer " &
                    "inner join gruppen g on m.Gruppe = g.Gruppe " &
                    "where g.Wettkampf = " & Wettkampf.ID &
                    " and m.wettkampf= " & Wettkampf.ID &
                    " group by a.verein;"
            Else
                Query = "Select Datum from wettkampf Where idWettkampf = " & Wettkampf.ID & ";"
            End If
            Using conn As New MySqlConnection(MySqlConnString)
                Try
                    conn.Open()
                    cmd = New MySqlCommand(Query, conn)
                    dtDatum.Load(cmd.ExecuteReader())
                Catch ex As Exception
                    Stop
                End Try
            End Using

            ProgressBar.Maximum = lstVerein.CheckedIndices.Count
            Count = 0
            ProgressBar.Value = Count
            lblProgress.Text = "Druck-Fortschritt"
            btnCancel.Enabled = True

            BackgroundWorker1.RunWorkerAsync()
        End If
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As DoWorkEventArgs) Handles BackgroundWorker1.DoWork

        Using report = New FastReport.Report

            For Each index In lstVerein.CheckedIndices
                If BackgroundWorker1.CancellationPending Then
                    e.Cancel = True
                    Exit For
                Else
                    If chkAutoBook.Checked And dtJoin.Rows.Count > 0 Then
                        If CBool(dtJoin.Rows(0)("bezahlt")) = False Then
                            For Each row As DataRow In dtJoin.Rows
                                row("bezahlt") = True
                            Next
                        End If
                    End If
                    Save()
                    GetVerein(index)
                    GetHeber(Verein("idVerein").ToString) ' Tabelle Heber für aktuellen Verein
                    With report
                        .Load(Path.Combine(Application.StartupPath, "Report", Druckvorlage + ".frx"))
                        .RegisterData(dtJoin, "Heber")
                        .SetParameterValue("parWK", Wettkampf.ID.ToString)
                        .SetParameterValue("parVerein", Verein("idVerein"))
                        .SetParameterValue("pVerein", Verein("Vereinsname"))
                        .SetParameterValue("pWK", Wettkampf.Bezeichnung)
                        '.SetParameterValue("pAdresse", Verein("Adresse"))
                        '.SetParameterValue("pOrt", Verein("Ort"))
                        '.SetParameterValue("pPlz", Verein("PLZ"))
                        .SetParameterValue("pGlobAll", chkAlleHeber.Checked)
                        Try
                            If chkDatum.Checked Then
                                Dim d As DataRow() = dtDatum.Select("Verein = " + Verein("idVerein").ToString)
                                .SetParameterValue("pDatum", Format(d(0)("Datum"), "dd.MM.yyyy"))
                            Else
                                .SetParameterValue("pDatum", Format(dtDatum.Rows(0)("Datum"), "dd.MM.yyyy"))
                            End If
                        Catch ex As Exception
                            'Stop
                        End Try
                        .ConvertNulls = False
                        .Prepare(Count > 0)
                    End With
                    Count += 1
                    BackgroundWorker1.ReportProgress(Count)
                End If
            Next

            If e.Cancel = False Then
                Dim msg As String
                If lstVerein.CheckedIndices.Count = 1 Then
                    msg = "Quittung gedruckt"
                Else
                    msg = lstVerein.CheckedIndices.Count.ToString + " Quittungen gedruckt"
                End If
                Message.msg = msg

                ' --- Vorschau oder Ausdruck
                If Action = "Show" Then
                    report.ShowPrepared()
                ElseIf Action = "Print" Then
                    report.PrintPrepared()
                End If
                '--------------------------
            End If
        End Using
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        ProgressBar.Value = e.ProgressPercentage
        Message.msg = e.ProgressPercentage.ToString + " von " + lstVerein.CheckedIndices.Count.ToString + " fertig"
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        ProgressBar.Value = lstVerein.CheckedIndices.Count
        If e.Cancelled = True Then
            Message.msg = "Vorgang abgebrochen"
        ElseIf e.Cancelled = False Then
            Message.msg = "Fertig"
        End If
        btnCancel.Enabled = False
        Cursor = Cursors.Default
    End Sub

    Private Sub GetVerein(index As Object)
        If InvokeRequired Then
            Dim d As New SetRow(AddressOf GetVerein)
            Invoke(d, New Object() {index})
        Else
            Dim id = CType(lstVerein.Items.Item(CInt(index)), DataRowView)("idVerein")
            Dim row() As DataRow = dtVerein.Select("idVerein = " & id.ToString)
            Verein = row(0)
        End If
    End Sub

    Private Sub ProgressMsg(msg As String)
        If InvokeRequired Then
            Dim d As New SetMsg(AddressOf ProgressMsg)
            Invoke(d, New Object() {msg})
        Else
            lblProgress.Text = msg
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        With OpenFileDialog
            .Filter = "Druck-Vorlagen (*.frx)|*.frx|Alle Dateien (*.*)|*.*"
            .InitialDirectory = Path.Combine(Application.StartupPath, "Report")
            If .ShowDialog() = DialogResult.Cancel Then Exit Sub
            If Not .FileName.Contains(".frx") Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Die ausgewählte Datei ist keine Druckvorlage.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End Using
            Else
                cboVorlage.Items.Add(.FileName)
            End If
        End With
    End Sub

    Private Sub chkAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkAll.CheckedChanged
        For i As Integer = 0 To lstVerein.Items.Count - 1
            lstVerein.SetItemCheckState(i, chkAll.CheckState)
        Next
    End Sub

    Private Sub lstVerein_Click(sender As Object, e As EventArgs) Handles lstVerein.Click
        With lstVerein
            .SetItemChecked(.SelectedIndex, Not .GetItemChecked(.SelectedIndex))
        End With
    End Sub

    Private Sub cboVorlage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVorlage.SelectedIndexChanged
        Druckvorlage = cboVorlage.Text
    End Sub


End Class