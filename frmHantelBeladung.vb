﻿
Public Class frmHantelBeladung

    'Dim Hantel As New myHantel
    Dim Stange As String
    Dim loading As Boolean

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Hantel.Clear_Hantel()
        With txtGewicht
            .Focus()
            .Select(0, 3)
        End With
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Close()
    End Sub
    Private Sub btnShow_Click(sender As Object, e As EventArgs) Handles btnShow.Click
        If Not optM.Checked And Not optW.Checked And Not optK.Checked And Not optX.Checked Then
            Using New Centered_MessageBox(Me)
                MsgBox("Keine Hantelstange ausgewählt.", MsgBoxStyle.Exclamation, "Hantel-Beladung")
                Exit Sub
            End Using
        End If
        Drawing()
    End Sub

    Private Sub Drawing()
        If loading Then Return
        Cursor = Cursors.WaitCursor

        For Each item In IsFormPresent({"Hantelbeladung"})
            With dicScreens(item.Key).Form
                .Controls.Clear()
                '.Controls.Add(Hantel.DrawHantel(.ClientSize.Width, .ClientSize.Height, CInt(txtGewicht.Value), Stange, If(chkBigDisc.Enabled, chkBigDisc.Checked, False)))

                ' damit probieren:                DrawHantel(CType(dicScreens(key.Key).Key.Controls("pnlHantel"), Panel), _HLast, _Stange, _Sex, _JG, _BigDisc)

                .Visible = True
            End With
        Next

        'formBohle.Draw_Hantel(CInt(txtGewicht.Value), Stange)
        'Hantel.Draw_Hantel_In_Bohle(CInt(txtGewicht.Value), Stange)

        Cursor = Cursors.Default
    End Sub

    Private Sub optM_Click(sender As Object, e As EventArgs) Handles optM.Click
        Hantelstange_Changed("m")
    End Sub
    Private Sub optW_Click(sender As Object, e As EventArgs) Handles optW.Click
        Hantelstange_Changed("w")
    End Sub
    Private Sub optK_Click(sender As Object, e As EventArgs) Handles optK.Click
        Hantelstange_Changed("k")
    End Sub
    Private Sub optX_Click(sender As Object, e As EventArgs) Handles optX.Click
        Hantelstange_Changed("x")
    End Sub

    Private Sub txtGewicht_ValueChanged(sender As Object, e As EventArgs) Handles txtGewicht.ValueChanged

        Select Case txtGewicht.Value
            Case < 5
                Stange = String.Empty
            Case < 7
                If Not optX.Checked Then
                    Stange = String.Empty
                Else
                    Hantelstange_Changed("x")
                End If
            Case < 17
                If optX.Enabled AndAlso Not optK.Checked Then
                    Hantelstange_Changed("x")
                ElseIf optK.Enabled AndAlso Not optX.Checked Then
                    Hantelstange_Changed("k")
                End If
            Case < 21
                ' keine 5-kg-Stange möglich
                If optK.Enabled AndAlso Not optK.Checked Then
                    Hantelstange_Changed("k")
                ElseIf Not optK.Enabled AndAlso Not optW.Checked Then
                    Hantelstange_Changed("w")
                End If
            Case Else
                If Not optW.Checked AndAlso Not optM.Checked Then
                    Hantelstange_Changed("w")
                End If
        End Select

        If txtGewicht.Focused And Wettkampf.WK_Modus <> Modus.EasyHantel Then Drawing()
    End Sub

    Private Sub Hantelstange_Changed(s As String)
        Stange = s
        Select Case Stange
            Case "m"
                optM.Checked = True 'Not optM.Checked
                optW.Checked = Not optM.Checked
                optK.Checked = Not optM.Checked
                optX.Checked = Not optM.Checked
                'chkBigDisc.Enabled = Not optM.Checked
                'txtGewicht.Minimum = 20
            Case "w"
                optW.Checked = True 'Not optW.Checked
                optM.Checked = Not optW.Checked
                optK.Checked = Not optW.Checked
                optX.Checked = Not optW.Checked
                'chkBigDisc.Enabled = optW.Checked
                'txtGewicht.Minimum = 15
            Case "k"
                optK.Checked = True 'Not optK.Checked
                optM.Checked = Not optK.Checked
                optW.Checked = Not optK.Checked
                optX.Checked = Not optK.Checked
                'chkBigDisc.Enabled = optK.Checked
                'txtGewicht.Minimum = 7
            Case "x"
                optX.Checked = True 'Not optX.Checked
                optM.Checked = Not optX.Checked
                optW.Checked = Not optX.Checked
                optK.Checked = Not optX.Checked
                'chkBigDisc.Enabled = optX.Checked
                'txtGewicht.Minimum = 5
        End Select
        If Wettkampf.WK_Modus <> Modus.EasyHantel Then Drawing()
    End Sub

    Private Sub frmHantelBeladung_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        RemoveHandler Set_formHantelbeladung_btnShow_Enabled, AddressOf btnShow_EnabledChange
    End Sub
    Private Sub frmHantelBeladung_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Cursor = Cursors.WaitCursor

        NativeMethods.INI_Write("EasyModus", "Stange", Stange, App_IniFile)
        NativeMethods.INI_Write("EasyModus", "Gewicht", txtGewicht.Value.ToString, App_IniFile)
        NativeMethods.INI_Write("EasyModus", "TopMost", chkTopMost.Checked.ToString, App_IniFile)
        NativeMethods.INI_Write("EasyModus", "BigDisc", chkBigDisc.Checked.ToString, App_IniFile)

        'If Not IsNothing(formHantel) AndAlso Not formHantel.IsDisposed Then formHantel.Close()
        For Each key In IsFormPresent({"Hanteleladung"})
            dicScreens(key.Key).Form.Close()
        Next
        Cursor = Cursors.Default
    End Sub
    Private Sub frmHantelBeladung_Load(sender As Object, e As EventArgs) Handles Me.Load
        loading = True

        AddHandler Set_formHantelbeladung_btnShow_Enabled, AddressOf btnShow_EnabledChange

        Dim toolTip1 As New ToolTip()
        With toolTip1
            .AutoPopDelay = 5000
            .InitialDelay = 1000
            .ReshowDelay = 500
            .ShowAlways = True
            .SetToolTip(chkTopMost, "Eingabefenster ist immer sichtbar")
            .SetToolTip(chkBigDisc, "große Scheiben für 2,5 und 5 kg")
        End With
        Dim inp As String
        If File.Exists(App_IniFile) Then
            inp = NativeMethods.INI_Read("EasyModus", "Gewicht", App_IniFile)
            If inp <> "?" Then txtGewicht.Value = CInt(inp)
            inp = NativeMethods.INI_Read("EasyModus", "Stange", App_IniFile)
            Select Case inp
                Case "m"
                    optM.PerformClick()
                    If txtGewicht.Value < 20 Then txtGewicht.Value = 20
                Case "w"
                    optW.PerformClick()
                    If txtGewicht.Value < 15 Then txtGewicht.Value = 15
                Case "k"
                    optK.PerformClick()
                    If txtGewicht.Value < 7 Then txtGewicht.Value = 7
            End Select
            inp = NativeMethods.INI_Read("EasyModus", "TopMost", App_IniFile)
            If inp <> "?" Then chkTopMost.Checked = CBool(inp)
            inp = NativeMethods.INI_Read("EasyModus", "BigDisc", App_IniFile)
            If inp <> "?" Then chkBigDisc.Checked = CBool(inp)
        End If
        If Text.Contains("Test") Then
            Location = New Point(460, 145)
        Else
            Location = New Point(formEasyMode.Left + formEasyMode.Width, formEasyMode.Top)
        End If
    End Sub
    Private Sub frmHantelBeladung_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        loading = False
        Cursor = Cursors.Default
    End Sub

    Private Sub chkBigDisc_CheckedChanged(sender As Object, e As EventArgs) Handles chkBigDisc.CheckedChanged
        Wettkampf.BigDisc = chkBigDisc.Checked
        If txtGewicht.Value < 40 And Stange <> "m" Then Drawing()
    End Sub

    Private Sub chkTopMost_CheckedChanged(sender As Object, e As EventArgs) Handles chkTopMost.CheckedChanged
        TopMost = chkTopMost.Checked
    End Sub

    Private Sub btnShow_EnabledChange(Value As Boolean)
        btnShow.Enabled = Value
    End Sub

    Private Sub chkEnableK_Click(sender As Object, e As EventArgs) Handles chkEnableK.Click
        If chkEnableK.Checked And Not chkEnableX.Checked Then Return
        optK.Enabled = chkEnableK.Checked
    End Sub

    Private Sub chkEableX_Click(sender As Object, e As EventArgs) Handles chkEnableX.Click
        If chkEnableX.Checked And Not chkEnableK.Checked Then Return
        optX.Enabled = chkEnableX.Checked
    End Sub


End Class