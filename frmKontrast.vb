﻿Public Class frmKontrast

    Dim loading As Boolean = True
    Dim Color_Value_Old As Color

    Private Sub Slider_Scroll(sender As Object, e As EventArgs) Handles Slider.Scroll
        With Value
            .Text = Slider.Value.ToString
            .Location = New Point(25 - .Width \ 2 + CInt(Slider.Value * 1.11), 74)
        End With
        If chkVorschau.Checked Then
            With Slider
                'frmOptionen.lblWertung.ForeColor = Color.FromArgb(.Value, .Value, .Value)
                Ansicht_Options.Shade_Color = Color.FromArgb(.Value, .Value, .Value)
            End With
        End If
    End Sub

    Private Sub frmKontrast_Load(sender As Object, e As EventArgs) Handles Me.Load
        Color_Value_Old = Ansicht_Options.Shade_Color
        Slider.Value = Color_Value_Old.R
        With Value
            .Text = Slider.Value.ToString
            .Location = New Point(25 - .Width \ 2 + CInt(Slider.Value * 1.11), 74)
        End With
        loading = False
    End Sub

    Private Sub chkVorschau_CheckedChanged(sender As Object, e As EventArgs) Handles chkVorschau.CheckedChanged
        If loading Then Exit Sub
        If chkVorschau.Checked Then
            With Slider
                Ansicht_Options.Shade_Color = Color.FromArgb(.Value, .Value, .Value)
            End With
        Else
            With Color_Value_Old
                Ansicht_Options.Shade_Color = Color.FromArgb(.R, .G, .B)
            End With
        End If
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        With Slider
            Ansicht_Options.Shade_Color = Color.FromArgb(.Value, .Value, .Value)
        End With
    End Sub

End Class