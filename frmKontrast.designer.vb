﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmKontrast
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmKontrast))
        Me.Slider = New System.Windows.Forms.TrackBar()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Value = New System.Windows.Forms.Label()
        Me.chkVorschau = New System.Windows.Forms.CheckBox()
        CType(Me.Slider, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Slider
        '
        Me.Slider.BackColor = System.Drawing.SystemColors.Control
        Me.Slider.Location = New System.Drawing.Point(12, 42)
        Me.Slider.Maximum = 255
        Me.Slider.Name = "Slider"
        Me.Slider.Size = New System.Drawing.Size(312, 45)
        Me.Slider.TabIndex = 0
        Me.Slider.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        '
        'btnOK
        '
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(168, 98)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(249, 98)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Abbrechen"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(25, 15)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(286, 25)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'Value
        '
        Me.Value.AutoSize = True
        Me.Value.Location = New System.Drawing.Point(19, 74)
        Me.Value.Name = "Value"
        Me.Value.Size = New System.Drawing.Size(13, 13)
        Me.Value.TabIndex = 4
        Me.Value.Text = "0"
        Me.Value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkVorschau
        '
        Me.chkVorschau.AutoSize = True
        Me.chkVorschau.Checked = True
        Me.chkVorschau.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkVorschau.Location = New System.Drawing.Point(22, 102)
        Me.chkVorschau.Name = "chkVorschau"
        Me.chkVorschau.Size = New System.Drawing.Size(71, 17)
        Me.chkVorschau.TabIndex = 5
        Me.chkVorschau.Text = "Vorschau"
        Me.chkVorschau.UseVisualStyleBackColor = True
        '
        'frmKontrast
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(336, 131)
        Me.Controls.Add(Me.chkVorschau)
        Me.Controls.Add(Me.Value)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.Slider)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmKontrast"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Kontrast einstellen"
        CType(Me.Slider, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Slider As TrackBar
    Friend WithEvents btnOK As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Value As Label
    Friend WithEvents chkVorschau As CheckBox
End Class
