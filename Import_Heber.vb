﻿Imports System
Imports System.Data
Imports System.Data.OleDb

Public Class Import_Heber

    Dim dtAthleten As DataTable
    Dim dtImport As DataTable
    Dim dtFail As DataTable
    Dim dvA As DataView
    Dim dvF As DataView
    Dim bs As BindingSource
    'Dim query As String
    Dim cmd As MySqlCommand
    Dim dicID As Dictionary(Of Integer, Integer)

    Dim ExcelContents As New DataTable
    Dim Fields As New Dictionary(Of String, String())
    Dim Ttips As New Dictionary(Of String, String)
    Dim ColIndex As Integer

    Dim Staaten As DataTable

    Private Sub Import_Heber_Load(sender As Object, e As EventArgs) Handles Me.Load
        Fields("Nachname") = {"name", "last name"}
        Fields("Vorname") = {"first name", "given name"}
        Fields("Geburtstag") = {"birthday", "dob", "day of birth"}
        Fields("Geschlecht") = {"m/f", "sex"}
        Fields("GK") = {"category"}
        Fields("Iso3") = {}
        Fields("Zweikampf") = {"total"}
        Fields("Notiz") = {"sporting success"}

        Ttips("Nachname") = ""
        Ttips("Vorname") = ""
        Ttips("Geburtstag") = ""
        Ttips("Geschlecht") = ""
        Ttips("GK") = "Gewichtsklasse"
        Ttips("Iso3") = "ISO-3-Code des Landes"
        Ttips("Zweikampf") = "gemeldete Zweikampf-Leistung"
        Ttips("Notiz") = "Infos für Moderator"
        Ttips("") = ""

        lstFields.Items.Add(String.Empty)
        lstFields.Items.AddRange(Fields.Keys.ToArray)
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        With OpenFileDialog1
            .Filter = "Excel-Tabelle (*.xls;*.xlsx)|*.xls;*.xlsx" '|CSV-Datei (*.csv)|*.csv|MySQL-Datei (*.sql)|*.sql|Alle Dateien (*.*)|*.*"
            If .ShowDialog() = DialogResult.Cancel Then Return
            txtImportFile.Text = .FileName
        End With
    End Sub

    Private Sub btnDB_Click(sender As Object, e As EventArgs)

        dtAthleten = New DataTable
        dtImport = New DataTable
        dtFail = New DataTable
        dicID = New Dictionary(Of Integer, Integer)

        Using conn As New MySqlConnection(User.ConnString)
            conn.Open()
            cmd = New MySqlCommand("SELECT * FROM athleten;", conn)
            dtAthleten.Load(cmd.ExecuteReader)
            cmd = New MySqlCommand("SELECT * FROM athleten_import;", conn)
            dtImport.Load(cmd.ExecuteReader)
        End Using

        dtFail = dtImport.Clone
        dvA = New DataView(dtAthleten)
        dvA.Sort = "Nachname, Vorname"
        dvF = New DataView(dtFail)
        bs = New BindingSource With {.DataSource = dvF}
        dgvHeber.AutoGenerateColumns = False
        dgvHeber.DataSource = bs

        ' wie wär's mit dtAthleten.Merge(dtImport)

        For Each row As DataRow In dtImport.Rows
            Dim pos = dvA.Find({row!nachname, row!Vorname})
            If pos > -1 Then
                If Not CInt(dvA(pos)("idTeilnehmer")).Equals(CInt(row!idTeilnehmer)) Then
                    dicID.Add(CInt(row!idTeilnehmer), CInt(dvA(pos)("idTeilnehmer")))
                End If
                dvA(pos)("idTeilnehmer") = row!idTeilnehmer
                dvA(pos)("Verein") = row!Verein
                dvA(pos)("ESR") = row!ESR
                dvA(pos)("MSR") = row!MSR
                dvA(pos)("Jahrgang") = row!Jahrgang ' vorsichtshalber
            Else
                dtFail.ImportRow(row)
                Dim newrow = dtAthleten.NewRow
                newrow.ItemArray = row.ItemArray
                dtAthleten.Rows.Add(newrow)
            End If
        Next

    End Sub

    Private Function Insert_Athlet(Conn As MySqlConnection, Athlet As DataRow, Teilnehmer As Object) As Integer
        Dim Query = "INSERT INTO athleten (
                            idTeilnehmer,
                            Nachname,
                            Vorname,
                            ESR,
                            MSR,
                            Jahrgang,
                            Geburtstag,
                            Geschlecht,
                            Staat
                     ) VALUES (
                            @tn,
                            @nn,
                            @vn,
                            @esr,
                            @msr,
                            @jg,
                            @dob,
                            @sex,
                            @state
                     )
                    ON DUPLICATE KEY UPDATE 
                            Nachname = @nn,
                            Jahrgang = @jg,
                            Geburtstag = @dob,
                            Geschlecht = @sex,
                            Staat = @state
                    ;"
        Dim ParamErr As Boolean
        Dim kvp As New KeyValuePair(Of String, Object)
        Do
            ParamErr = False

            Dim NN = Athlet!Nachname.ToString.Trim
            Dim Nachname_Arr = Split(NN, " ")
            Dim Nachname_Lst = New List(Of String)
            For Each n In Nachname_Arr
                Nachname_Lst.Add(UCase(n.Substring(0, 1)) & LCase(n.Substring(1)))
            Next
            NN = Join(Nachname_Lst.ToArray, " ")

            Try
                Dim cmd = New MySqlCommand(Query, Conn)
                With cmd.Parameters
                    .AddWithValue("@tn", Teilnehmer)
                    .AddWithValue("@nn", NN)
                    .AddWithValue("@vn", Athlet!Vorname.ToString.Trim)
                    .AddWithValue("@esr", 0)
                    .AddWithValue("@msr", 0)
                    kvp = New KeyValuePair(Of String, Object)("Jahrgang", Athlet!Geburtstag)
                    .AddWithValue("@jg", CDate(Athlet!Geburtstag).Year)
                    .AddWithValue("@dob", Athlet!Geburtstag)
                    .AddWithValue("@sex", IIf(Athlet!Geschlecht.ToString.Equals("f"), "w", Athlet!Geschlecht))
                    kvp = New KeyValuePair(Of String, Object)("Nationalität", Athlet!Iso3)
                    .AddWithValue("@state", Staaten.Select("state_iso3 = '" & Athlet!Iso3.ToString & "'")(0)!state_id)
                End With
            Catch ex As Exception
                Dim Msg = String.Empty
                Using InputBox As New Custom_InputBox("Der Wert von < " & kvp.Key & " > für " & Athlet!Nachname.ToString & ", " & Athlet!Vorname.ToString & " ist <" & If(IsDBNull(kvp.Value), " {} ", kvp.Value.ToString) & ">.")
                    If IsNothing(InputBox.Response) OrElse InputBox.Response.Length = 0 Then
                        Return -1
                    End If
                    Select Case kvp.Key
                        Case "Jahrgang"
                            Try
                                Athlet!Geburtstag = CDate(InputBox.Response)
                            Catch
                                Msg = InputBox.Response & " ist kein Datum."
                            End Try
                    End Select
                End Using
                If Not String.IsNullOrEmpty(Msg) Then
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show(msgCaption, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End Using
                End If
                ParamErr = True

            End Try
        Loop While ParamErr
        Try
            cmd.ExecuteNonQuery()
            Return CInt(cmd.LastInsertedId)
        Catch ex As Exception
            Return -1
        End Try
    End Function

    Private Function Insert_Meldung(Conn As MySqlConnection, Athlet As DataRow, Teilnehmer As Integer) As Boolean
        Dim Query = "INSERT INTO meldung (
                            Wettkampf, 
                            Teilnehmer, 
                            Meldedatum, 
                            Gewicht,
                            idAK, 
                            AK, 
                            idGK, 
                            GK, 
                            Zweikampf
                    ) VALUES (
                            @wk, 
                            @tn, 
                            @md, 
                            @kg,
                            @idAK, 
                            @AK, 
                            @idGK, 
                            @GK, 
                            @zk
                    ) ON DUPLICATE KEY UPDATE 
                            Gewicht = @kg, 
                            Meldedatum = @md, 
                            idAK = @idAK, 
                            AK = @AK, 
                            idGK = @idGK, 
                            GK = @GK, 
                            Zweikampf = @zk
                    ;"

        ' Altersklasse bestimmen
        Dim AK = Glob.Get_AK(CDate(Athlet!Geburtstag).Year)
        Dim Gewicht = IIf(Athlet!GK.ToString.StartsWith("+"), CInt(Athlet!GK) + 1, Athlet!GK)

        cmd = New MySqlCommand(Query, Conn)
        With cmd.Parameters
            .AddWithValue("@wk", Wettkampf.ID)
            .AddWithValue("@tn", Teilnehmer)
            .AddWithValue("@md", Now)
            .AddWithValue("@kg", Gewicht)
            .AddWithValue("@zk", Athlet!Zweikampf)
            If AK.Key > 0 Then
                If Not IsDBNull(Athlet!GK) Then
                    Dim GK = Glob.Get_GK(IIf(Athlet!Geschlecht.ToString.Equals("f"), "w", Athlet!Geschlecht).ToString, AK.Key, CDbl(Gewicht))
                    .AddWithValue("@idGK", GK.Key)
                    .AddWithValue("@GK", GK.Value)
                Else
                    .AddWithValue("@idGK", DBNull.Value)
                    .AddWithValue("@GK", DBNull.Value)
                End If
                .AddWithValue("@idAK", AK.Key)
                .AddWithValue("@AK", AK.Value)
            Else
                .AddWithValue("@idAK", DBNull.Value)
                .AddWithValue("@AK", DBNull.Value)
                .AddWithValue("@idGK", DBNull.Value)
                .AddWithValue("@GK", DBNull.Value)
            End If
        End With
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function Insert_Notiz(Conn As MySqlConnection, Athlet As DataRow, Teilnehmer As Integer) As Boolean
        Dim Query = "INSERT INTO notizen (
                        Heber,
                        Notiz 
                    ) VALUES (
                        @tn,
                        @note
                    ) ON DUPLICATE KEY UPDATE 
                        Notiz = @note
                    ;"
        cmd = New MySqlCommand(Query, Conn)
        With cmd.Parameters
            .AddWithValue("@tn", Teilnehmer)
            .AddWithValue("@note", Athlet!Notiz)
        End With
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click

        ' Suche Nachname, Vorname, Geburtstag in lokalen athleten
        ' wenn nicht gefunden
        ' --> suche in BVDG-Portal ???????
        ' --> Insert in lokale athleten
        ' Insert in meldung, notizen

        Cursor = Cursors.WaitCursor

        For i = 0 To ExcelContents.Columns.Count - 1
            ExcelContents.Columns(i).ColumnName = dgvHeber.Columns(i).HeaderText
        Next

        Dim Iso3 As String = String.Empty
        Dim LiftersAdded As Integer = 0
        Dim RowsCount As Integer = 0

        Using conn As New MySqlConnection(User.ConnString)
            conn.Open()

            Staaten = New DataTable
            cmd = New MySqlCommand("SELECT * FROM staaten;", conn)
            Staaten.Load(cmd.ExecuteReader)

            For Each row As DataRow In ExcelContents.Rows

                If Not IsDBNull(row!Nachname) Then
                    RowsCount += 1

                    If Not IsDBNull(row!Iso3) AndAlso Not row!Iso3.ToString.Equals(Iso3) Then Iso3 = row!Iso3.ToString
                    row!Iso3 = Iso3

                    Dim Athlet As New DataTable
                    'cmd = New MySqlCommand("SELECT * FROM athleten WHERE Nachname LIKE '%" & row!Nachname.ToString.Trim & "%' AND Vorname LIKE '%" & row!Vorname.ToString.Trim & "%';", conn)
                    cmd = New MySqlCommand("SELECT * FROM athleten WHERE Nachname LIKE concat(@nn, '%') AND Vorname LIKE concat(@vn, '%');", conn)
                    With cmd.Parameters
                        .AddWithValue("@nn", row!Nachname.ToString.Trim)
                        .AddWithValue("@vn", row!Vorname.ToString.Trim)
                    End With
                    Athlet.Load(cmd.ExecuteReader)

                    If Athlet.Rows.Count > 1 Then
                        Using New Centered_MessageBox(Me)
                            MessageBox.Show("Mehrere Athleten mit demselben Namen gefunden." & vbNewLine &
                                            "(" & Trim(row!Nachname.ToString) & ", " & Trim(row!Vorname.ToString) & ")",
                                            msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        End Using
                    Else
                        Try
                            Dim TN = If(Athlet.Rows.Count = 1, Athlet(0)!idTeilnehmer, DBNull.Value)
                            Dim Response = Insert_Athlet(conn, row, TN)
                            If Response > -1 Then
                                Dim Teilnehmer = If(Response > 0, Response, CInt(Athlet(0)!idTeilnehmer))
                                If Insert_Meldung(conn, row, Teilnehmer) Then
                                    If Insert_Notiz(conn, row, Teilnehmer) Then
                                        LiftersAdded += 1
                                    End If
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If
            Next
        End Using

        Using New Centered_MessageBox(Me)
            MessageBox.Show(LiftersAdded & " von " & RowsCount & " Teilnehmern hinzugefügt/aktualisiert.", Wettkampf.Identities.Values(0), MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Using

        Cursor = Cursors.Default

        Close()

        'If chkCheckOnline.Checked Then
        '    Using b_conn As New MySqlConnection(User.BVDG_ConnString.Result)
        '        Dim b_cmd = New MySqlCommand("SELECT * FROM athleten WHERE athlet_name = @nn AND athlet_vorname = @vn;", b_conn)
        '        With b_cmd.Parameters
        '            .AddWithValue("@nn", row!Nachname)
        '            .AddWithValue("@vn", row!Vorname)
        '        End With
        '        Dim b_Athlet = New DataTable
        '        b_Athlet.Load(cmd.ExecuteReader)
        '        Stop
        '    End Using
        'End If

    End Sub

    Private Sub txtImportFile_TextChanged(sender As Object, e As EventArgs) Handles txtImportFile.TextChanged
        Cursor = Cursors.WaitCursor

        Using ES As New ExcelService
            ExcelContents = ES.Get_Excel_Contents(txtImportFile.Text)
            With dgvHeber
                .Columns.Clear()
                If ExcelContents.Rows.Count > 0 Then
                    bs = New BindingSource With {.DataSource = ExcelContents}
                    .DataSource = bs
                    For Each Col As DataGridViewColumn In .Columns
                        Col.SortMode = DataGridViewColumnSortMode.NotSortable
                        Set_ColumnProps(Col)
                    Next
                End If
            End With
        End Using

        Cursor = Cursors.Default
    End Sub

    Private Sub Set_ColumnProps(Col As DataGridViewColumn, Optional Field As String() = Nothing)

        If IsNothing(Field) Then Field = Get_Field(Col.Name)

        If IsNothing(Field) OrElse String.IsNullOrEmpty(Field(0)) Then
            Col.HeaderCell.Style.BackColor = Color.FromArgb(245, 212, 212)
            Col.HeaderText = Col.Name
            Col.ToolTipText = Nothing
        Else
            Col.HeaderCell.Style.BackColor = Color.FromArgb(198, 255, 192)
            Col.HeaderText = Field(0)
            Col.ToolTipText = If(Field.Length = 2, Field(1), String.Empty)
        End If

        Dim RequiredFields = {"Nachname", "Vorname", "Geburtstag", "Geschlecht", "Iso3"}.ToList
        For Each Column As DataGridViewColumn In dgvHeber.Columns
            If RequiredFields.Contains(Column.HeaderText) Then RequiredFields.Remove(Column.HeaderText)
            If RequiredFields.Count = 0 Then Exit For
        Next
        btnImport.Enabled = RequiredFields.Count = 0

    End Sub

    Private Function Get_Field(ColName As String) As String()

        For Each Entry In Fields
            If Entry.Value.Contains(LCase(ColName)) Then
                Return {Entry.Key, Ttips(Entry.Key)}
            End If
        Next

        Return Nothing
    End Function

    Private Sub dgvHeber_CellContentClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvHeber.ColumnHeaderMouseClick
        'If e.Button = MouseButtons.Right Then
        With dgvHeber
            Dim Rect = .GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, True)
            Dim Loc = .PointToScreen(New Point(Rect.X, Rect.Bottom))
            lstFields.Location = .FindForm.PointToClient(Loc)
            lstFields.Visible = True
            lstFields.Focus()
            lstFields.BringToFront()
        End With
        ColIndex = e.ColumnIndex
        'End If
    End Sub

    Private Sub lstFields_DoubleClick(sender As Object, e As EventArgs) Handles lstFields.DoubleClick
        Set_ColumnProps(dgvHeber.Columns(ColIndex), {lstFields.SelectedItem.ToString, Ttips(lstFields.SelectedItem.ToString)})
        lstFields.Visible = False
    End Sub

    Private Sub lstFields_KeyDown(sender As Object, e As KeyEventArgs) Handles lstFields.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                lstFields.Visible = False
            Case Keys.Enter
                Set_ColumnProps(dgvHeber.Columns(ColIndex), {lstFields.SelectedItem.ToString, Ttips(lstFields.SelectedItem.ToString)})
                lstFields.Visible = False
        End Select
    End Sub

    Private Sub lstFields_VisibleChanged(sender As Object, e As EventArgs) Handles lstFields.VisibleChanged
        dgvHeber.Enabled = Not lstFields.Visible
    End Sub

    Private Sub chkInternational_CheckedChanged(sender As Object, e As EventArgs) Handles chkInternational.CheckedChanged

    End Sub

End Class

Class ExcelService
    Implements IDisposable

    Function Get_Excel_Contents(FileName As String) As DataTable

        '"HDR=Yes;" indicates that the first row contains columnnames, not data. "HDR=No;" indicates the opposite.
        Dim XLSX_File = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & ";Extended Properties='Excel 12.0 Xml;HDR=YES'"
        Dim XLS_File = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & ";Extended Properties='Excel 8.0;HDR=YES'"
        Dim ConnString = If(LCase(FileName).EndsWith("xlsx"), XLSX_File, XLS_File)

        Dim SheetToImport As String = "Tabelle1"
        Dim Statement As String = "SELECT * FROM [" + SheetToImport + "$]"

        Dim dt As New DataTable

        dt.TableName = "ExcelData"

        Dim Conn As OleDbConnection = New OleDbConnection(ConnString)
        Dim adapter As OleDbDataAdapter = New OleDbDataAdapter(Statement, Conn)

        Try
            adapter.Fill(dt)
        Catch ex As Exception
            MessageBox.Show("Fehler: " + ex.Message)
            Return Nothing
        End Try

        Return dt
    End Function

    Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
            End If
            ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
            ' TODO: große Felder auf Null setzen.
        End If
        disposedValue = True
    End Sub
    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
    End Sub

End Class