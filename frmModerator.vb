﻿
Imports System.Timers

Public Class frmModerator
    'Dim fontFamilies() As FontFamily
    Dim dicLampen As New Dictionary(Of Integer, TextBox) 'Container für Lampen-Anzeige
    'Private ZoomFaktor As Single 'für RTF
    'Dim bsR As BindingSource

    ' Optionen
    Dim Design As String = "Nacht"

    Private func As myFunction = New myFunction

    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Integer, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As Object) As Integer
    Private Const EM_GETLINECOUNT As Integer = &HBA

    Dim NK_String As String
    'Private loading As Boolean
    Private LockObject As New Object

    Private Delegate Sub Delegate_String(Value As String)
    Private Delegate Sub Delegate_StringInteger(value1 As String, value2 As Integer)
    Private Delegate Sub Delegate_Bool(Value As Boolean)
    Private Delegate Sub Delegate_IntegerInteger(value1 As Integer, value2 As Integer)
    Private Delegate Sub Delegate_Integer(value As Integer)
    Private Delegate Sub Delegate_Color(Color As Color)
    Private Delegate Sub Delegate_IntegerColor(Index As Integer, Color As Color)

    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        RemoveHandler Anzeige.CountDown_Changed, AddressOf CountDown_Changed
        RemoveHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed

        RemoveHandler Anzeige.Lampe_Changed_Sofort, AddressOf Change_Lampe

        RemoveHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        RemoveHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed

        RemoveHandler User.Bohle_Changed, AddressOf Bohle_Changed

        RemoveHandler Wettkampf.Technik_Changed, AddressOf Technik_Changed
        RemoveHandler Wettkampf.MultiBohle_Changed, AddressOf Bohle_Changed

        RemoveHandler Leader.Gruppe_Changed, AddressOf Gruppe_Changed
        RemoveHandler Leader.Durchgang_Changed, AddressOf Durchgang_Changed
        RemoveHandler Leader.Mark_Rows, AddressOf Heber_Highlight
        RemoveHandler Leader.Dim_Rows, AddressOf Heber_Dim
        RemoveHandler Leader.Change_AV, AddressOf Change_Anfangslast

        RemoveHandler Leader.Pause_Changed, AddressOf Change_Pause
        RemoveHandler Leader.Scoring_Changed, AddressOf Wertung_Changed
        RemoveHandler Leader.Add_Rows, AddressOf dgv2_Move
        RemoveHandler Leader.Notiz_Changed, AddressOf Get_Notiz
        RemoveHandler Leader.InfoMessage_Changed, AddressOf SpeakerInfo
        RemoveHandler Leader.Blockheben_Changed, AddressOf Change_Blockheben

        RemoveHandler ForeColors_Change, AddressOf Wertung_Clear
        'RemoveHandler RowCount_Changed, AddressOf dgv2_Move
        RemoveHandler Ansicht_Options.NameFormat_Changed, AddressOf NameFormat_Changed

        dicLampen.Clear()

        Using sc As New ScreenScale
            Dim mon = sc.GetDeviceNumber(Screen.FromControl(Me))
            If dicScreens.ContainsKey(mon) Then dicScreens.Remove(mon)
        End Using

    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        'Wertung
        dicLampen.Add(1, xGültig1)
        dicLampen.Add(2, xGültig2)
        dicLampen.Add(3, xGültig3)

        AddHandler Anzeige.CountDown_Changed, AddressOf CountDown_Changed
        AddHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed

        AddHandler Anzeige.Lampe_Changed_Sofort, AddressOf Change_Lampe

        AddHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        AddHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed

        AddHandler User.Bohle_Changed, AddressOf Bohle_Changed

        AddHandler Wettkampf.Technik_Changed, AddressOf Technik_Changed
        AddHandler Wettkampf.MultiBohle_Changed, AddressOf Bohle_Changed

        AddHandler Leader.Gruppe_Changed, AddressOf Gruppe_Changed
        AddHandler Leader.Durchgang_Changed, AddressOf Durchgang_Changed
        AddHandler Leader.Mark_Rows, AddressOf Heber_Highlight
        AddHandler Leader.Dim_Rows, AddressOf Heber_Dim
        AddHandler Leader.Change_AV, AddressOf Change_Anfangslast

        AddHandler Leader.Pause_Changed, AddressOf Change_Pause
        AddHandler Leader.Scoring_Changed, AddressOf Wertung_Changed
        AddHandler Leader.Add_Rows, AddressOf dgv2_Move
        AddHandler Leader.Notiz_Changed, AddressOf Get_Notiz
        AddHandler Leader.InfoMessage_Changed, AddressOf SpeakerInfo
        AddHandler Leader.Blockheben_Changed, AddressOf Change_Blockheben

        AddHandler ForeColors_Change, AddressOf Wertung_Clear
        'AddHandler RowCount_Changed, AddressOf dgv2_Move
        AddHandler Ansicht_Options.NameFormat_Changed, AddressOf NameFormat_Changed

        Try
            For i As Integer = 1 To 3
                dicLampen(i).Font = New Font(FontFamilies(Fonts.Wertungsanzeige), dicLampen(i).Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            Next
            xTechniknote.Font = New Font(FontFamilies(Fonts.Techniknote), xTechniknote.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf.Font = New Font(FontFamilies(Fonts.Zeitanzeige), xAufruf.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf.ForeColor = Anzeige.Clock_Colors(Clock_Status.Waiting)
        Catch ex As Exception
        End Try

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        If Wettkampf.ID = 0 Then
            'Using New Centered_MessageBox(frmLeader)
            '    MessageBox.Show("Kein Wettkampf ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'End Using
            Close()
            Return
        End If

        Dim Wertung = CInt(Glob.dtModus.Select("idModus = " & Wettkampf.Modus)(0)("Wertung"))
        Dim NK_Stellen = Get_NK_Stellen(Wertung)
        NK_String = "#,##0." + StrDup(NK_Stellen, "0")

        Build_Grid(dgvJoin)
        Build_Grid(dgv2)

        ' Wertungsanzeige 
        With dgvHeber
            .Columns("T1").Visible = Wettkampf.Technikwertung
            .Columns("T2").Visible = Wettkampf.Technikwertung
            .Columns("T3").Visible = Wettkampf.Technikwertung
            .Columns("Wert").DefaultCellStyle.Format = NK_String
            .Rows.Add({"Reißen", DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value})
            .Rows.Add({"Stoßen", DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value})
            .Rows.Add({"Total", DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value})
            Dim w = 0
            For Each col As DataGridViewColumn In .Columns
                If col.Visible Then w += col.Width
            Next
            .SetBounds(If(dgvTeam.Visible, dgvTeam.Left, pnlWertung.Left) - dgvHeber.Width, pnlWertung.Top, w, pnlWertung.Height)
            .AutoResizeRows()
        End With

        ' Team-Wertung
        If Wettkampf.Mannschaft Then
            With dgvTeam
                .AutoGenerateColumns = False
                .Columns("Gegner").Visible = Wettkampf.Finale
                .Columns("Punkte2").Visible = Wettkampf.Finale
                .DataSource = bsTeam
                .Height = .ColumnHeadersHeight + .Rows.Count * .Rows(0).Height
                .Columns("mReissen").DefaultCellStyle.Format = NK_String
                .Columns("mStossen").DefaultCellStyle.Format = NK_String
                .Columns("mHeben").DefaultCellStyle.Format = NK_String
                Dim w = 0
                For Each col As DataGridViewColumn In .Columns
                    If col.Visible Then w += col.Width
                Next
                .Width = w
                .Location = New Point(pnlWertung.Left - .Width, Height - .Height)
                .Visible = True
            End With
        End If

        If Not IsNothing(Leader.Gruppe) AndAlso Leader.Gruppe.Count > 0 OrElse Wettkampf.Mannschaft Then
            Gruppe_Changed(Leader.Gruppe)
        Else
            lblGruppe.Text = String.Empty
        End If

        If Not String.IsNullOrEmpty(Leader.Durchgang) Then
            Durchgang_Changed(Leader.Durchgang)
        Else
            lblDurchgang.Text = String.Empty
            Technik_Changed(Wettkampf.Technikwertung)
        End If

        Cursor = Cursors.Default
    End Sub
    Private Sub Me_SizeChanged(sender As Object, e As EventArgs) Handles Me.SizeChanged
        With dgvJoin
            .AutoResizeColumns()
            .AutoResizeRows()
            .SendToBack()
            If .Rows.Count > 0 Then .FirstDisplayedScrollingRowIndex = 0
        End With
        With dgvTeam
            .DefaultCellStyle.Font = New Font(.Font.FontFamily, 8, FontStyle.Regular)
            .ColumnHeadersDefaultCellStyle.Font = New Font(.Font.FontFamily, 8, FontStyle.Regular)
            .Location = New Point(pnlWertung.Left - .Width, Height - .Height)
            .Visible = Wettkampf.Mannschaft
            .BringToFront()
        End With
        With dgvHeber
            .DefaultCellStyle.Font = New Font(.Font.FontFamily, 6, FontStyle.Regular)
            .ColumnHeadersDefaultCellStyle.Font = New Font(.Font.FontFamily, 6, FontStyle.Regular)
        End With
    End Sub

    Private Sub lblCaption_SizeChanged(sender As Object, e As EventArgs) Handles lblCaption.SizeChanged

    End Sub

    Private Sub Adjust_Caption()
        Dim LimitLeft = lblDurchgang.Left + lblDurchgang.Width
        Dim LimitRight = 0
        With lblCaption
            Using GF As New myFunction
                .Text = GF.String_Shorten(If(String.IsNullOrEmpty(Wettkampf.Kurz), Wettkampf.Bezeichnung, Wettkampf.Kurz),
                                          .Font,
                                          Width - LimitLeft - LimitRight,
                                          If(Wettkampf.MultiBohle, " (" & User.Bohle & ")", ""))
            End Using
            .Left = (Width - LimitLeft - LimitRight - .Width) \ 2 + LimitLeft
        End With
    End Sub

    Private Sub Heber_Highlight(Zeile As Integer)
        If IsNothing(dvLeader(Leader.TableIx)) Then Return
        Try
            Dim tn = CInt(dvLeader(Leader.TableIx)(Leader.RowNum + Leader.RowNum_Offset)("Teilnehmer"))

            With dgvJoin
                .Rows(Zeile).DefaultCellStyle.ForeColor = Color.FromArgb(0, 240, 0) '(40, 164, 40) 'Lime
                If WK_Options.Markierung > 0 Then
                    For i = Zeile + 1 To .Rows.Count - 1
                        Dim SetColor As Boolean
                        Select Case WK_Options.Markierung
                            Case 1 ' nächsten zwei Versuche
                                SetColor = i < 3 + Leader.RowNum
                            Case 2 ' Versuche des Hebers 
                                SetColor = CInt(dvLeader(Leader.TableIx)(i)("Teilnehmer")) = tn
                            Case 3 'Versuche mit gleicher Last
                                SetColor = CInt(dvLeader(Leader.TableIx)(i)("HLast")) = CInt(dvLeader(Leader.TableIx)(Leader.RowNum + Leader.RowNum_Offset)("HLast"))
                        End Select
                        If SetColor = True Then
                            .Rows(i).DefaultCellStyle.ForeColor = Color.FromArgb(180, 255, 180) '(204, 255, 204) 'Color.Yellow
                        Else
                            .Rows(i).DefaultCellStyle.ForeColor = Color.White
                        End If
                    Next
                End If
            End With

            If Leader.RowNum = 0 AndAlso lblName.BackColor <> Color.FromArgb(45, 185, 45) Then lblName.BackColor = Color.FromArgb(45, 185, 45)

        Catch ex As Exception
        End Try
    End Sub
    Private Sub Heber_Dim(Zeile As Integer)
        If Zeile < 0 Then Return
        With dgvJoin
            .Rows(Zeile).DefaultCellStyle.BackColor = Color.DimGray
            .Rows(Zeile).DefaultCellStyle.ForeColor = Color.DarkGray
        End With
        If Leader.RowNum = 1 AndAlso lblName.BackColor <> Color.DimGray Then lblName.BackColor = Color.DimGray
    End Sub

    Private Sub Change_Blockheben(Blockeheben As Boolean)
        Update_DataSource()
    End Sub

    Private Sub Bohle_Changed()
        Adjust_Caption()
    End Sub
    Private Sub Durchgang_Changed(Durchgang As String)
        lblDurchgang.Left = lblGruppe.Left + lblGruppe.Width
        lblDurchgang.Text = If(Not Wettkampf.Mannschaft, "-  ", "") + Durchgang

        Adjust_Caption()

        If Wettkampf.Mannschaft OrElse (Leader.Gruppe.Count > 0 AndAlso Not String.IsNullOrEmpty(Leader.Durchgang)) Then
            Update_DataSource()
        End If
    End Sub
    Private Sub Gruppe_Changed(Gruppe As List(Of String))
        If Not Wettkampf.Mannschaft Then
            lblGruppe.Text = "Gruppe " + String.Join(", ", Gruppe)
            lblDurchgang.Left = lblGruppe.Left + lblGruppe.Width
        Else
            lblGruppe.Visible = False
            lblDurchgang.Left = lblGruppe.Left
            lblDurchgang.Text = Leader.Durchgang
        End If

        Adjust_Caption()

        If Wettkampf.Mannschaft OrElse (Leader.Gruppe.Count > 0 AndAlso Not String.IsNullOrEmpty(Leader.Durchgang)) Then
            Update_DataSource()
        End If
    End Sub
    Private Sub Technik_Changed(Technikwertung As Boolean)
        With pnlWertung
            .Visible = True
            If Technikwertung AndAlso Not xTechniknote.Visible Then
                .Left -= xTechniknote.Width
                .Width += xTechniknote.Width
                xTechniknote.Visible = True
            ElseIf Not Technikwertung AndAlso xTechniknote.Visible Then
                .Left += xTechniknote.Width
                .Width -= xTechniknote.Width
                xTechniknote.Visible = False
            End If
            .BringToFront()
            dgvTeam.Location = New Point(.Left - dgvTeam.Width, Height - dgvTeam.Height)
        End With

        dgvTeam.BringToFront()

        With dgvHeber
            .Columns("T1").Visible = Wettkampf.Technikwertung
            .Columns("T2").Visible = Wettkampf.Technikwertung
            .Columns("T3").Visible = Wettkampf.Technikwertung
            .Columns("Wert").DefaultCellStyle.Format = NK_String
            Dim w = 0
            For Each col As DataGridViewColumn In .Columns
                If col.Visible Then w += col.Width
            Next
            .SetBounds(If(dgvTeam.Visible, dgvTeam.Left, pnlWertung.Left) - dgvHeber.Width, pnlWertung.Top - 1, w, pnlWertung.Height + 1)
        End With

        rtfNotiz.SetBounds(0, pnlWertung.Top, dgvHeber.Left, pnlWertung.Top)

        With lblName
            '    .SetBounds(0, pnlWertung.Top - .Height, dgvHeber.Left + dgvHeber.Width - 1, .Height)
        End With

    End Sub
    Private Sub CurrentScore_Changed(Wertung As Object, row As Integer, col As String)
        Try
            dgvJoin.Rows(dgvJoin.CurrentRow.Index).Cells("Punkte").Value = Wertung
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Change_Anfangslast(HLast As Integer, Row As Integer, Col As String, Optional Declared? As Boolean = False, Optional Panik As Boolean = False)

        If dgvJoin.Rows.Count = 0 OrElse
            Leader.RowNum > 0 OrElse
            CInt(dgvJoin.Rows(0).Cells("Teilnehmer").Value) <> CInt(dvResults(Row)!Teilnehmer) Then Return

        With dgvHeber.Rows(Leader.TableIx).Cells("D" & Col.Substring(2))
            .Value = HLast
            'If HLast = 0 Then
            '    ' Verzicht
            '    '.Value = DBNull.Value
            '    CellFormatting(Row, Col)
            'End If
        End With
        'Refresh()
    End Sub

    Private Sub Update_DataSource()

        SyncLock LockObject

            'If loading Then Return
            'loading = True

            Refresh()
            'SuspendLayout()

            With dgvJoin
                .DataSource = bsLeader
                If .Rows.Count = 0 Then Return ' keine DataSource

                'Me_Shown()

                .AutoResizeColumns()
                .AutoResizeRows()
                .Height = Height - .Top
            End With

            With dgv2
                If Leader.TableIx = 0 Then
                    .DataSource = dvLeader(1)
                    .AutoResizeColumns()
                    .AutoResizeRows()
                    dgv2_Move()
                Else
                    .DataSource = Nothing
                    .Visible = False
                End If
            End With

            Technik_Changed(Wettkampf.Technikwertung)

            If Leader.RowNum = 1 Then
                Heber_Dim(0)
            Else
                Heber_Highlight(0)
            End If

            If bsLeader.Count > 0 Then Get_Notiz(CInt(dvLeader(Leader.TableIx)(0)!Teilnehmer))

            'ResumeLayout()
            'loading = False
        End SyncLock

    End Sub

    Private Sub Build_Grid(grid As DataGridView, Optional Design As String = "Nacht")
        Try
            With grid
                Select Case Design
                    Case "Standard"
                        .AlternatingRowsDefaultCellStyle.BackColor = Color.OldLace
                        .RowsDefaultCellStyle.BackColor = Color.LightCyan
                        .BackgroundColor = SystemColors.AppWorkspace
                        .GridColor = SystemColors.ControlDark
                        With .DefaultCellStyle
                            .ForeColor = SystemColors.ControlText
                            .SelectionBackColor = Color.LightCyan
                            .SelectionForeColor = SystemColors.ControlText
                        End With
                    Case "Nacht"
                        .AlternatingRowsDefaultCellStyle.BackColor = BackColor
                        .RowsDefaultCellStyle.BackColor = Color.Black
                        .BackgroundColor = Color.Black
                        .GridColor = Color.Black
                        With .DefaultCellStyle
                            .ForeColor = Color.White
                            .SelectionBackColor = Color.Black
                            .SelectionForeColor = Color.White
                        End With
                End Select
                .AutoGenerateColumns = False

                Dim _dg = "Hantellast"
                If grid.Name.Equals("dgv2") Then _dg = "  Stoßen  "

                Dim ColBez() As String = {"            ", "TN", "Nachname", "Vorname", "", "Start-Nr", "Nation", "Verein", "Versuch", _dg, "GK", "Jahrgang", "Gewicht", "AK", "      "}
                Dim Coldata() As String = {"Reihenfolge", "Teilnehmer", "Name1", "Vorname", "sex", "Startnummer", "state_name", "Vereinsname", "Versuch", "HLast", "GK", "Jahrgang", "Wiegen", "AK", "a_K"}
                Dim ColAlignCenter() As String = {"Reihenfolge", "Startnummer", "Jahrgang", "Wiegen", "Versuch", "HLast", "GK"}

                If .Columns.Count = 0 Then
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Reihenfolge"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Teilnehmer", .Visible = False})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Sex"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Startnummer"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nation", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill, .Visible = Wettkampf.International})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vereinsname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill, .Visible = Not Wettkampf.International})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Versuch"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "HLast"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "GK"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Jahrgang"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Wiegen"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "AK"})
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "a_K", .Visible = func.Show_a_K})
                End If

                ' Format
                .Columns("Wiegen").DefaultCellStyle.Format = "0.00"
                .Columns("HLast").DefaultCellStyle.ForeColor = Color.GreenYellow    'Orange
                .Columns("Sex").DefaultCellStyle.ForeColor = Color.Orange
                '.Font = New Font(.Font.FontFamily, .Font.Size, FontStyle.Bold)

                ' Headertext
                For i = 0 To ColBez.Count - 1
                    .Columns(i).HeaderText = ColBez(i) ' + vbNewLine + vbNewLine
                    .Columns(i).DataPropertyName = Coldata(i)
                    .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                Next

                'Spalten zentriert
                For i As Integer = 0 To ColAlignCenter.Count - 1
                    .Columns(ColAlignCenter(i)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                Next
            End With

            'Change_NameFormat(grid)

        Catch ex As Exception
            'Stop
        End Try
    End Sub

    Private Sub NameFormat_Changed()
        Change_NameFormat(dgvJoin)
        Change_NameFormat(dgv2)
    End Sub
    Private Sub Change_NameFormat(dgv As DataGridView)
        Try
            With dgv
                Select Case Ansicht_Options.NameFormat
                    Case NameFormat.Standard
                        .Columns("Nachname").DataPropertyName = "Nachname"
                    Case NameFormat.Standard_UCase
                        .Columns("Nachname").DataPropertyName = "Name1"
                    Case NameFormat.International
                        .Columns("Nachname").DataPropertyName = "Name2"
                End Select
                .Columns("Vorname").Visible = Ansicht_Options.NameFormat < NameFormat.International
                .AutoResizeColumns()
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dgv2_Move() 'RowCount As Integer)

        Dim rect As Rectangle
        Dim loc As Point

        Try
            With dgvJoin
                rect = .GetRowDisplayRectangle(.DisplayedRowCount(False), True)
                rect = .GetRowDisplayRectangle(.Rows.GetRowCount(DataGridViewElementStates.Displayed) - 1, True)
                loc = New Point(.Left, rect.Bottom + .Top + 5) ' dgv2 wird 5 pt unterhalb der letzten Zeile von dgvLeader positioniert
            End With

            With dgv2
                Dim minHeight = .ColumnHeadersHeight + .RowTemplate.Height + 2
                If .Rows.Count > 0 AndAlso (dgvJoin.Height + dgvJoin.Top) - loc.Y > minHeight Then
                    .Location = loc
                    .Size = New Size(dgvJoin.Width, (dgvJoin.Height + dgvJoin.Top) - loc.Y)
                    .Visible = True
                    .BringToFront()
                Else
                    .Visible = False
                End If
            End With
        Catch ex As Exception
        End Try

        lblName.BringToFront()
        xAufruf.BringToFront()
        pnlWertung.BringToFront()
        dgvHeber.BringToFront()
        dgvTeam.BringToFront()
        rtfNotiz.BringToFront()
    End Sub
    Private Sub dgv_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvJoin.CellFormatting, dgv2.CellFormatting

        If CType(sender, DataGridView).Columns(e.ColumnIndex).Name.Equals("a_K") Then
            If Equals(e.Value, False) Then
                e.Value = ""
                e.FormattingApplied = True
            ElseIf Equals(e.Value, True) Then
                e.Value = "a.K."
                e.FormattingApplied = True
            End If
        End If
    End Sub
    Private Sub dgv_SelectionChanged(sender As Object, e As EventArgs) Handles dgvJoin.SelectionChanged, dgv2.SelectionChanged ', dgvWertung.SelectionChanged, dgvTechnik.SelectionChanged
        CType(sender, DataGridView).ClearSelection()
    End Sub
    Private Sub dgvJoin_RowsRemoved(sender As Object, e As DataGridViewRowsRemovedEventArgs) Handles dgvJoin.RowsRemoved
        If Leader.TableIx = 0 Then
            SuspendLayout()
            dgv2_Move()
            ResumeLayout()
        End If
    End Sub
    Private Sub dgvJoin_UserDeletedRow(sender As Object, e As DataGridViewRowEventArgs) Handles dgvJoin.UserDeletedRow
        dgvJoin.Rows(0).Cells("Sex").Style.ForeColor = Color.Orange
    End Sub
    Private Sub Wertung_Changed(Row As Integer, Col As String, Optional HLast As Integer = -1, Optional Wertung As Integer = 0, Optional Note As Double = 0, Optional ZK As String = "", Optional IsCurrent As Boolean = True)
        dgvHeber_Fill(CInt(dvResults(Row)!Teilnehmer))
    End Sub

    Private Sub CountDown_Changed(Value As String)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_String(AddressOf CountDown_Changed)
                Invoke(d, New Object() {Value})
            Else
                xAufruf.Text = Value
            End If
        Catch
        End Try
    End Sub
    Private Sub ClockColor_Changed(Color As Color)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_Color(AddressOf ClockColor_Changed)
                Invoke(d, New Object() {Color})
            Else
                xAufruf.ForeColor = Color
            End If
        Catch
        End Try
    End Sub
    Private Sub Change_Pause(Value As Boolean)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_Bool(AddressOf Change_Pause)
                Invoke(d, New Object() {Value})
            Else
                pnlWertung.Visible = Not Value
                xAufruf.BringToFront()
                If Not Value Then pnlWertung.BringToFront()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Wertung_Clear(HeberPresent As Boolean)
        For i = 1 To 3
            dicLampen(i).ForeColor = Ansicht_Options.Shade_Color
        Next
        xTechniknote.ForeColor = Ansicht_Options.Shade_Color
        xTechniknote.Text = "X,XX"

        lblInfo.Invoke(New Color_Info(AddressOf Info_Clear))
    End Sub
    Private Sub Change_Lampe(Index As Integer, Color As Color)
        If InvokeRequired Then
            Dim d As New Delegate_IntegerColor(AddressOf Change_Lampe)
            Invoke(d, New Object() {Index, Color})
        Else
            Try
                dicLampen(Index).ForeColor = Color
                dicLampen(Index).Refresh()
                'lblInfo.Invoke(New Color_Info(AddressOf Info_Clear))
            Catch ex As Exception
            End Try
        End If
    End Sub

    'Private Sub Set_Wertung(Index As Integer, Wertung As Integer)
    '    If InvokeRequired Then
    '        Dim d As New Delegate_IntegerInteger(AddressOf Set_Wertung)
    '        Invoke(d, New Object() {Index, Wertung})
    '    ElseIf Wertung > 0 Then
    '        lblInfo.Invoke(New Color_Info(AddressOf Info_Clear))
    '        dicLampen(Index).ForeColor = Anzeige.ScoreColors(Wertung)
    '    End If
    'End Sub
    Private Sub TechnikWert_Changed(Wert As String)
        If InvokeRequired Then
            Dim d As New Delegate_String(AddressOf TechnikWert_Changed)
            Invoke(d, New Object() {Wert})
        Else
            xTechniknote.Text = Wert
        End If
    End Sub
    Private Sub TechnikFarbe_Changed(Farbe As Color)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_Color(AddressOf TechnikFarbe_Changed)
                Invoke(d, New Object() {Farbe})
            Else
                xTechniknote.ForeColor = Farbe
            End If
        Catch
        End Try
    End Sub

    '' Notizen & Wertung des Hebers
    Private Sub rtfNotiz_LocationChanged(sender As Object, e As EventArgs) Handles rtfNotiz.LocationChanged
        'If MinimumSize.Height = 0 Then Return
        'With rtfNotiz
        '    lblName.SetBounds(.Left, .Top - CInt(19 * Height \ MinimumSize.Height), .Width + dgvHeber.Width - 1, pnlWertung.Top - .Top - lblName.Height)
        'End With
    End Sub
    Private Sub rtfNotiz_VisibleChanged(sender As Object, e As EventArgs) Handles rtfNotiz.VisibleChanged
        lblName.Visible = rtfNotiz.Visible
    End Sub
    Private Sub Get_Notiz(Id As Integer)
        If InvokeRequired Then
            Dim d As New Delegate_Integer(AddressOf Get_Notiz)
            Invoke(d, New Object() {Id})
        Else
            Dim pos = bsNotizen.Find("Teilnehmer", Id)
            Dim _factor = Height \ MinimumSize.Height
            Dim _height = pnlWertung.Height - 3
            Dim _width = If(dgvTeam.Visible, dgvTeam.Left, pnlWertung.Left) - dgvHeber.Width
            With rtfNotiz
                .ZoomFactor = 0.1
                .SetBounds(0, Height - _height, _width, _height)
                .Clear()
                If pos > -1 AndAlso Not IsDBNull(dvNotizen(pos)!Notiz) Then
                    'Dim note = CType(dvNotizen(pos)!Notiz, Byte())
                    'Using ms = New MemoryStream(note)
                    '    Try
                    '        .LoadFile(ms, RichTextBoxStreamType.RichText)
                    '    Catch ex As Exception
                    '        .LoadFile(ms, RichTextBoxStreamType.PlainText)
                    '    End Try
                    'End Using

                    .Rtf = ConvertTextToRTF(dvNotizen(pos)!Notiz.ToString)

                    If .TextLength > 0 Then
                        Dim TextHeight = .Lines.Count * TextRenderer.MeasureText(.Lines(0), .Font).Height
                        Dim MaxLineWidth = 0
                        For i = 0 To .Lines.Count - 1
                            Dim lw = TextRenderer.MeasureText(.Lines(i), .Font).Width
                            If lw > MaxLineWidth Then MaxLineWidth = lw
                        Next
                        Dim FaktorList As New List(Of Double)
                        FaktorList.AddRange({_width / MaxLineWidth, _height / TextHeight})
                        FaktorList.Sort()
                        If FaktorList(0) > _factor Then
                            .ZoomFactor = _factor
                        Else
                            .ZoomFactor = CInt(FaktorList(0))
                        End If
                    End If
                End If
                .BringToFront()
            End With

            lblName.SetBounds(0, (Height - _height) - 19 * _factor, _width + dgvHeber.Width, (Height - _height) - 19 * _factor) ' 19 ist Höhe im Entwurf

            pos = bsLeader.Find("Teilnehmer", Id)
            lblName.Text = dvLeader(Leader.TableIx)(pos)!Nachname.ToString & ", " & dvLeader(Leader.TableIx)(pos)!Vorname.ToString

            ' Wertung des Hebers einlesen
            dgvHeber_Fill(Id)

        End If
    End Sub
    Private Function GetLinesCount(ByVal txtCtl As TextBoxBase) As Integer
        Return SendMessage(txtCtl.Handle.ToInt32, EM_GETLINECOUNT, -1, Nothing)
    End Function

    Private Sub dgvHeber_Fill(TN As Integer)

        Dim ix = bsResults.Find("Teilnehmer", TN)
        Dim row = dvResults(ix).Row
        Dim dg = {"R", "S"}

        With dgvHeber
            For z = 0 To 1
                For s = 1 To 3
                    .Rows(z).Cells("D" & s).Value = row(dg(z) & "_" & s)
                    .Rows(z).Cells("T" & s).Value = row(dg(z) & "T_" & s)
                    ' nächste HLast
                    If s < 3 AndAlso Not IsDBNull(row(dg(z) & "_" & s + 1)) Then .Rows(z).Cells("D" & s + 1).Value = row(dg(z) & "_" & s)
                Next
                .Rows(z).Cells("Wert").Value = row(If(dg(z).Equals("R"), "Reissen", "Stossen"))
            Next
            .Rows(2).Cells("Wert").Value = row!Heben
            '.ColumnHeadersHeight = .Rows(0).Height
            .ClearSelection()
            .Visible = True
            .Refresh()
            .BringToFront()
        End With
    End Sub

    '' Info Heber/Hantel
    Dim _count As Integer
    Private WithEvents timerInfo As New Timer With {.Interval = 500, .AutoReset = True, .SynchronizingObject = Me}
    Private Delegate Sub Dim_Info()
    Private Delegate Sub Color_Info()
    Private Delegate Sub Clear_Info()
    Private Sub SpeakerInfo(Info As String, Target As Integer)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_StringInteger(AddressOf SpeakerInfo)
                Invoke(d, New Object() {Info, Target})
            Else
                With lblInfo
                    .Text = Info
                    .Location = New Point(ClientSize.Width \ 2 - .Width \ 2, ClientSize.Height \ 2 - .Height \ 2)
                    .Visible = Not String.IsNullOrEmpty(Info)
                    .BringToFront()
                End With
                _count = 14 ' Anzahl der Farbwechsel, Anzeigedauer = 10 x 500 ms
                If lblInfo.Visible Then
                    timerInfo.Start()
                Else
                    _count = 0
                    timerInfo.Stop()
                    lblInfo.Invoke(New Clear_Info(AddressOf Info_Clear))
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub timerInfo_Tick(sender As Object, e As ElapsedEventArgs) Handles timerInfo.Elapsed
        If _count = 0 Then
            timerInfo.Stop()
            lblInfo.Invoke(New Clear_Info(AddressOf Info_Clear))
        Else
            lblInfo.Invoke(New Color_Info(AddressOf Info_Colors))
            _count -= 1
        End If
    End Sub

    Private Sub Info_Colors()
        With lblInfo
            .BackColor = If(_count Mod 2 = 0, Color.Red, Color.Yellow)
            .ForeColor = If(_count Mod 2 = 1, Color.Red, Color.Yellow)
        End With
    End Sub
    Private Sub Info_Dim()
        With lblInfo
            .BackColor = BackColor
            .ForeColor = Color.CornflowerBlue
        End With
    End Sub
    Private Sub Info_Clear()
        lblInfo.Visible = False
    End Sub

    ' Wertung des Hebers
    Private Sub dgvHeber_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgvHeber.CellPainting
        Try
            Dim d = {"RW_", "SW_"}
            Dim ix = bsResults.Find("Teilnehmer", dvLeader(Leader.TableIx)(0)!Teilnehmer)
            Dim row = dvResults(ix).Row
            If e.RowIndex > -1 AndAlso e.RowIndex < 2 Then
                If dgvHeber.Columns(e.ColumnIndex).Name.Contains("D") Then
                    e.Handled = True
                    Dim img As Image = Nothing
                    Dim col = Val(dgvHeber.Columns(e.ColumnIndex).Name(1))
                    'Debug.WriteLine(e.RowIndex & vbTab & col)
                    If Not IsDBNull(row(d(e.RowIndex) & col)) Then
                        e.CellStyle.ForeColor = Color.White
                        Select Case CInt(row(d(e.RowIndex) & col))
                            Case 1
                                img = ImageList2.Images("SquareBlue.jpg")
                                'e.CellStyle.Font = New Font(e.CellStyle.Font.FontFamily, e.CellStyle.Font.Size, FontStyle.Bold)
                            Case -1
                                img = ImageList2.Images("SquareRed.jpg")
                                'e.CellStyle.Font = New Font(e.CellStyle.Font.FontFamily, e.CellStyle.Font.Size, FontStyle.Bold)
                            Case -2
                                dgvHeber.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = "---"
                                e.CellStyle.ForeColor = dgvHeber.DefaultCellStyle.ForeColor
                        End Select
                    End If
                    e.PaintBackground(e.CellBounds, False)
                    If Not IsNothing(img) Then
                        Dim pX = 1
                        Dim pY = 1
                        e.Graphics.DrawImage(img, e.CellBounds.Left + 1, e.CellBounds.Top + 1, e.CellBounds.Width - 3, e.CellBounds.Height - 3)
                    End If
                    e.PaintContent(e.CellBounds)
                ElseIf dgvHeber.Columns(e.ColumnIndex).Name.Contains("T") Then
                    e.CellStyle.Font = New Font(e.CellStyle.Font.FontFamily, e.CellStyle.Font.Size, FontStyle.Bold)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvHeber_SelectionChanged(sender As Object, e As EventArgs) Handles dgvHeber.SelectionChanged
        dgvHeber.ClearSelection()
    End Sub

    'Mannschaftswertung
    Private Sub dgvTeam_SelectionChanged(sender As Object, e As EventArgs) Handles dgvTeam.SelectionChanged
        dgvTeam.ClearSelection()
    End Sub

    ' Focus Textboxen
    Private Sub Textbox_GotFocus(sender As Object, e As EventArgs) Handles xGültig1.GotFocus, xGültig2.GotFocus, xGültig3.GotFocus,
                        xTechniknote.GotFocus, rtfNotiz.GotFocus
        dgvJoin.Focus()
    End Sub

    Private Sub frmModerator_Click(sender As Object, e As EventArgs) Handles Me.Click

    End Sub


End Class