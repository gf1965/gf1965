﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Drucken_Startkarten
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.chkSelectAll = New System.Windows.Forms.CheckBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.lstVerein = New System.Windows.Forms.CheckedListBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dgvMeldung = New System.Windows.Forms.DataGridView()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnVorschau = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboVorlage = New System.Windows.Forms.ComboBox()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.btnTemplate = New System.Windows.Forms.Button()
        Me.chkDatum = New System.Windows.Forms.CheckBox()
        Me.chkAnwesend = New System.Windows.Forms.CheckBox()
        Me.chkTeam = New System.Windows.Forms.CheckBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabVereine = New System.Windows.Forms.TabPage()
        Me.tabLänder = New System.Windows.Forms.TabPage()
        Me.lstLand = New System.Windows.Forms.CheckedListBox()
        Me.tabGruppen = New System.Windows.Forms.TabPage()
        Me.lstGruppen = New System.Windows.Forms.CheckedListBox()
        Me.btnEdit = New System.Windows.Forms.Button()
        CType(Me.dgvMeldung, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.tabVereine.SuspendLayout()
        Me.tabLänder.SuspendLayout()
        Me.tabGruppen.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.BackColor = System.Drawing.SystemColors.Control
        Me.chkSelectAll.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkSelectAll.Location = New System.Drawing.Point(21, 321)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(102, 18)
        Me.chkSelectAll.TabIndex = 19
        Me.chkSelectAll.Text = "alle auswählen"
        Me.chkSelectAll.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkSelectAll.UseVisualStyleBackColor = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Title = "Druckvorlagen suchen"
        '
        'lstVerein
        '
        Me.lstVerein.Location = New System.Drawing.Point(0, 0)
        Me.lstVerein.Name = "lstVerein"
        Me.lstVerein.Size = New System.Drawing.Size(230, 274)
        Me.lstVerein.TabIndex = 39
        Me.lstVerein.Tag = "idVerein"
        Me.lstVerein.ThreeDCheckBoxes = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label5.Location = New System.Drawing.Point(257, 18)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 13)
        Me.Label5.TabIndex = 41
        Me.Label5.Text = "&Meldungen"
        '
        'dgvMeldung
        '
        Me.dgvMeldung.AllowUserToAddRows = False
        Me.dgvMeldung.AllowUserToDeleteRows = False
        Me.dgvMeldung.AllowUserToResizeColumns = False
        Me.dgvMeldung.AllowUserToResizeRows = False
        Me.dgvMeldung.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvMeldung.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvMeldung.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMeldung.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvMeldung.ColumnHeadersHeight = 25
        Me.dgvMeldung.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvMeldung.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvMeldung.Location = New System.Drawing.Point(254, 35)
        Me.dgvMeldung.Name = "dgvMeldung"
        Me.dgvMeldung.ReadOnly = True
        Me.dgvMeldung.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvMeldung.RowHeadersVisible = False
        Me.dgvMeldung.RowTemplate.Height = 21
        Me.dgvMeldung.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvMeldung.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMeldung.ShowEditingIcon = False
        Me.dgvMeldung.ShowRowErrors = False
        Me.dgvMeldung.Size = New System.Drawing.Size(342, 279)
        Me.dgvMeldung.TabIndex = 40
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Location = New System.Drawing.Point(617, 102)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(100, 28)
        Me.btnCancel.TabIndex = 44
        Me.btnCancel.Text = "&Beenden"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Enabled = False
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnPrint.Location = New System.Drawing.Point(617, 68)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(100, 28)
        Me.btnPrint.TabIndex = 43
        Me.btnPrint.Tag = "Print"
        Me.btnPrint.Text = "&Drucken"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnVorschau
        '
        Me.btnVorschau.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVorschau.Enabled = False
        Me.btnVorschau.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnVorschau.Location = New System.Drawing.Point(617, 34)
        Me.btnVorschau.Name = "btnVorschau"
        Me.btnVorschau.Size = New System.Drawing.Size(100, 28)
        Me.btnVorschau.TabIndex = 42
        Me.btnVorschau.Tag = "Show"
        Me.btnVorschau.Text = "&Vorschau"
        Me.btnVorschau.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnSearch.Location = New System.Drawing.Point(602, 409)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(24, 24)
        Me.btnSearch.TabIndex = 49
        Me.btnSearch.Text = "..."
        Me.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label6.Location = New System.Drawing.Point(281, 393)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 13)
        Me.Label6.TabIndex = 48
        Me.Label6.Text = "Druck-Vorlage"
        '
        'cboVorlage
        '
        Me.cboVorlage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboVorlage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVorlage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboVorlage.FormattingEnabled = True
        Me.cboVorlage.Location = New System.Drawing.Point(278, 411)
        Me.cboVorlage.Name = "cboVorlage"
        Me.cboVorlage.Size = New System.Drawing.Size(318, 21)
        Me.cboVorlage.Sorted = True
        Me.cboVorlage.TabIndex = 47
        '
        'lblProgress
        '
        Me.lblProgress.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblProgress.AutoSize = True
        Me.lblProgress.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.lblProgress.Location = New System.Drawing.Point(22, 393)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(85, 13)
        Me.lblProgress.TabIndex = 46
        Me.lblProgress.Text = "Druck-Fortschritt"
        '
        'ProgressBar
        '
        Me.ProgressBar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar.Location = New System.Drawing.Point(19, 410)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(229, 22)
        Me.ProgressBar.TabIndex = 45
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'btnTemplate
        '
        Me.btnTemplate.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnTemplate.Location = New System.Drawing.Point(617, 287)
        Me.btnTemplate.Name = "btnTemplate"
        Me.btnTemplate.Size = New System.Drawing.Size(100, 28)
        Me.btnTemplate.TabIndex = 50
        Me.btnTemplate.Text = "leere Vorlage"
        Me.btnTemplate.UseVisualStyleBackColor = True
        Me.btnTemplate.Visible = False
        '
        'chkDatum
        '
        Me.chkDatum.AutoSize = True
        Me.chkDatum.Checked = True
        Me.chkDatum.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDatum.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkDatum.Location = New System.Drawing.Point(257, 345)
        Me.chkDatum.Name = "chkDatum"
        Me.chkDatum.Size = New System.Drawing.Size(218, 18)
        Me.chkDatum.TabIndex = 52
        Me.chkDatum.Text = "Rechnungsdatum ist Wettkampf-Datum"
        Me.chkDatum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkDatum.UseVisualStyleBackColor = True
        Me.chkDatum.Visible = False
        '
        'chkAnwesend
        '
        Me.chkAnwesend.AutoSize = True
        Me.chkAnwesend.BackColor = System.Drawing.SystemColors.Control
        Me.chkAnwesend.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkAnwesend.Location = New System.Drawing.Point(257, 321)
        Me.chkAnwesend.Name = "chkAnwesend"
        Me.chkAnwesend.Size = New System.Drawing.Size(183, 18)
        Me.chkAnwesend.TabIndex = 51
        Me.chkAnwesend.Text = "nur gemeldete Athleten drucken"
        Me.chkAnwesend.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkAnwesend.UseVisualStyleBackColor = False
        '
        'chkTeam
        '
        Me.chkTeam.AutoSize = True
        Me.chkTeam.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkTeam.Location = New System.Drawing.Point(21, 345)
        Me.chkTeam.Name = "chkTeam"
        Me.chkTeam.Size = New System.Drawing.Size(110, 18)
        Me.chkTeam.TabIndex = 53
        Me.chkTeam.Text = "Teams anzeigen"
        Me.chkTeam.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkTeam.UseVisualStyleBackColor = True
        Me.chkTeam.Visible = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabVereine)
        Me.TabControl1.Controls.Add(Me.tabLänder)
        Me.TabControl1.Controls.Add(Me.tabGruppen)
        Me.TabControl1.HotTrack = True
        Me.TabControl1.ItemSize = New System.Drawing.Size(86, 18)
        Me.TabControl1.Location = New System.Drawing.Point(14, 15)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(238, 300)
        Me.TabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.TabControl1.TabIndex = 54
        '
        'tabVereine
        '
        Me.tabVereine.Controls.Add(Me.lstVerein)
        Me.tabVereine.Location = New System.Drawing.Point(4, 22)
        Me.tabVereine.Name = "tabVereine"
        Me.tabVereine.Padding = New System.Windows.Forms.Padding(3)
        Me.tabVereine.Size = New System.Drawing.Size(230, 274)
        Me.tabVereine.TabIndex = 0
        Me.tabVereine.Tag = "0"
        Me.tabVereine.Text = "Vereine"
        Me.tabVereine.UseVisualStyleBackColor = True
        '
        'tabLänder
        '
        Me.tabLänder.Controls.Add(Me.lstLand)
        Me.tabLänder.Location = New System.Drawing.Point(4, 22)
        Me.tabLänder.Name = "tabLänder"
        Me.tabLänder.Padding = New System.Windows.Forms.Padding(3)
        Me.tabLänder.Size = New System.Drawing.Size(230, 274)
        Me.tabLänder.TabIndex = 1
        Me.tabLänder.Tag = "1"
        Me.tabLänder.Text = "Länder"
        Me.tabLänder.UseVisualStyleBackColor = True
        '
        'lstLand
        '
        Me.lstLand.Location = New System.Drawing.Point(0, 0)
        Me.lstLand.Name = "lstLand"
        Me.lstLand.Size = New System.Drawing.Size(230, 274)
        Me.lstLand.TabIndex = 40
        Me.lstLand.Tag = "region_id"
        Me.lstLand.ThreeDCheckBoxes = True
        '
        'tabGruppen
        '
        Me.tabGruppen.Controls.Add(Me.lstGruppen)
        Me.tabGruppen.Location = New System.Drawing.Point(4, 22)
        Me.tabGruppen.Name = "tabGruppen"
        Me.tabGruppen.Size = New System.Drawing.Size(230, 274)
        Me.tabGruppen.TabIndex = 2
        Me.tabGruppen.Tag = "2"
        Me.tabGruppen.Text = "Gruppen"
        Me.tabGruppen.UseVisualStyleBackColor = True
        '
        'lstGruppen
        '
        Me.lstGruppen.Location = New System.Drawing.Point(0, 0)
        Me.lstGruppen.Name = "lstGruppen"
        Me.lstGruppen.Size = New System.Drawing.Size(230, 274)
        Me.lstGruppen.TabIndex = 41
        Me.lstGruppen.Tag = "Gruppe"
        Me.lstGruppen.ThreeDCheckBoxes = True
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(632, 408)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(85, 24)
        Me.btnEdit.TabIndex = 55
        Me.btnEdit.Tag = "Edit"
        Me.btnEdit.Text = "Bearbeiten"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'Drucken_Vorbereitung
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(735, 448)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.chkTeam)
        Me.Controls.Add(Me.chkDatum)
        Me.Controls.Add(Me.chkAnwesend)
        Me.Controls.Add(Me.btnTemplate)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cboVorlage)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnVorschau)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dgvMeldung)
        Me.Controls.Add(Me.chkSelectAll)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Drucken_Startkarten"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Startkarten drucken"
        CType(Me.dgvMeldung, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.tabVereine.ResumeLayout(False)
        Me.tabLänder.ResumeLayout(False)
        Me.tabGruppen.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkSelectAll As CheckBox
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents lstVerein As CheckedListBox
    Friend WithEvents Label5 As Label
    Friend WithEvents dgvMeldung As DataGridView
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnPrint As Button
    Friend WithEvents btnVorschau As Button
    Friend WithEvents btnSearch As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents cboVorlage As ComboBox
    Friend WithEvents lblProgress As Label
    Friend WithEvents ProgressBar As ProgressBar
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnTemplate As Button
    Friend WithEvents chkDatum As CheckBox
    Friend WithEvents chkAnwesend As CheckBox
    Friend WithEvents chkTeam As CheckBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents tabVereine As TabPage
    Friend WithEvents tabLänder As TabPage
    Friend WithEvents lstLand As CheckedListBox
    Friend WithEvents tabGruppen As TabPage
    Friend WithEvents lstGruppen As CheckedListBox
    Friend WithEvents btnEdit As Button
End Class
