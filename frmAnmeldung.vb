﻿
Imports System.Net.NetworkInformation
Imports System.Management.Automation
Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports MySqlConnector

Public Class frmAnmeldung

    Property Lock As Boolean? = False
    Property OwnerForm As Form
    Overloads Property DialogResult As DialogResult

    Private tmpBohle As Integer = -1
    Private cmd As MySqlCommand
    Private Users As List(Of String)
    Private DatabaseSync As Boolean
    'Private InProcess As Boolean

    Private IsNoConnectionSaved As Boolean

    Private dtHosts As DataTable
    Private dvHosts As DataView
    Private dtDatabases As New DataTable

    Private WithEvents bsHosts As BindingSource
    Private WithEvents bsIPs As BindingSource

    Private Sub chkUserPW_CheckedChanged(sender As Object, e As EventArgs) Handles chkUserPW.CheckedChanged
        txtUserPassword.UseSystemPasswordChar = Not chkUserPW.Checked
    End Sub
    Private Sub chkPasswort_CheckedChanged(sender As Object, e As EventArgs) Handles chkPasswort.CheckedChanged
        txtPassword.UseSystemPasswordChar = Not chkPasswort.Checked
    End Sub

    Private Sub frmAnmeldung_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            If Not Lock And DialogResult <> DialogResult.OK Then
                btnCancel.PerformClick()
                e.Cancel = DialogResult = DialogResult.Retry
            End If
        ElseIf DialogResult = DialogResult.Retry Then
            e.Cancel = True
            DialogResult = DialogResult.None
        End If
        Cursor = Cursors.Default
    End Sub
    Private Sub frmAnmeldung_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            If pnlIPs.Visible Then btnExittt.PerformClick()
        ElseIf e.KeyCode = Keys.Enter Then
            If pnlIPs.Visible Then Set_NetworkID(dgvNetwork.SelectedRows(0).Index)
        ElseIf ModifierKeys = Keys.Control AndAlso e.KeyCode = Keys.M OrElse e.KeyCode = Keys.F4 Then
            Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
            Cursor.Show()
            e.SuppressKeyPress = True
        End If
    End Sub
    Private Sub frmAnmeldung_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        Dim cols(4) As DataColumn
        cols(0) = New DataColumn With {.ColumnName = "IP", .DataType = GetType(String)}
        cols(1) = New DataColumn With {.ColumnName = "Device", .DataType = GetType(String)}                      ' Computername
        cols(2) = New DataColumn With {.ColumnName = "Name", .DataType = GetType(String)}                        ' Network-Name
        cols(3) = New DataColumn With {.ColumnName = "Typ", .DataType = GetType(String)}                         ' Network-Typ
        cols(4) = New DataColumn With {.ColumnName = "Index", .DataType = GetType(Integer), .AllowDBNull = True} ' Network-Index

        dtHosts = New DataTable
        dtHosts.Columns.AddRange(cols)
        dvHosts = New DataView(dtHosts) With {.Sort = "Device, Name"}

    End Sub
    Private Sub frmAnmeldung_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Refresh()
        SuspendLayout()

        Enabled = False

        Users = New List(Of String)
        Dim usr = [Enum].GetValues(GetType(UserID))
        For Each item In usr
            Users.Add(item.ToString)
        Next
        With cboUser
            .DataSource = Users
            .DisplayMember = "value"
            .SelectedIndex = -1
        End With

#Region "Programm-Start"
        If String.IsNullOrEmpty(User.Guid) Then
            Using IP_Addresses As New IP_Addresses
                AddHandler IP_Addresses.IPsCompleted, AddressOf Update_UserIPs
                IP_Addresses.Get_HostIPs(Me)
            End Using
        End If
#End Region

        bsHosts = New BindingSource With {.DataSource = dvHosts}
        With dgvServer
            .AutoGenerateColumns = False
            .DataSource = bsHosts
            If bsHosts.Count > 0 Then HostsTable_Setbounds()
        End With

        Enabled = True

        If User.Bohle = -1 Then
            cboUser.SelectedIndex = 0
            cboBohle.SelectedIndex = 0 ' Bohle 1 (bei Neustart = -1)
        Else
            cboUser.SelectedIndex = User.UserId
            cboBohle.SelectedIndex = User.Bohle - 1
        End If

        ' prüfe Anmeldung
        IsNoConnectionSaved = String.IsNullOrEmpty(Get_ConnFromIni(, True))

        ' suche vorhandene Datenbanken
        dtDatabases = Get_DataBases("server=" + txtServer.Tag.ToString + ";port=" + txtPort.Tag.ToString + ";uid=root;")
        With cboDatabase
            .DataSource = dtDatabases
            .DisplayMember = "schema_name"
        End With

        If IsNoConnectionSaved AndAlso dtDatabases.Rows.Count = 0 Then
            Using New Centered_MessageBox(Me, {"Erstellen", "Verbinden"})
                If MessageBox.Show("Keine lokale Datenbank und keine Verbindungsdaten" & vbNewLine &
                                   "für externe Datenbank gefunden." & vbNewLine &
                                   "Möchten sie eine lokale Datenbank erstellen oder" & vbNewLine &
                                   "Verbindungsdaten zu einer externen Datenbank eingeben?",
                                   msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    Fill_Connection()
                    btnAnmelden.Text = "Datenbank anlegen"
                    txtUserPassword.Focus()
                Else
                    Height = MaximumSize.Height
                    btnServer.Visible = False
                End If
            End Using
        End If

        If Height = MaximumSize.Height AndAlso Not btnServer.Visible Then
            ' Edit Database Connection from formMain > Datei > Anmeldung
            txtTCP_Ip.Text = User.ServerIP
            txtTCP_Port.Text = User.ServerPort
            txtUserPassword.Text = ""
            If Not IsNoConnectionSaved AndAlso dtDatabases.Rows.Count > 0 Then
                Write_Connection(User.ConnString, True)
                txtServer.Focus()
            End If
        Else
            txtUserPassword.Text = ""
            txtUserPassword.Focus()
            ' alle Felder in Serveranmeldung leeren, damit die Verbindung erst angezeigt wird,
            ' wenn das Passwort eingegeben wurde
            For Each ctl As Control In grpServer.Controls
                If TypeOf ctl Is TextBox OrElse TypeOf ctl Is ComboBox Then ctl.Text = String.Empty
            Next
        End If

        ResumeLayout()
        Cursor = Cursors.Default
    End Sub
    Private Sub frmAnmeldung_SizeChanged(sender As Object, e As EventArgs) Handles Me.SizeChanged
        Try
            If Height = MaximumSize.Height Then Top = (OwnerForm.Height - Height) \ 2
            txtPort.Text = txtPort.Tag.ToString
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnAnmelden_Click(sender As Object, e As EventArgs) Handles btnAnmelden.Click
        Dim msg = String.Empty
        Dim InsertValues As Boolean = False

#Region "Datenbank anlegen"
        If btnAnmelden.Text.Equals("Datenbank anlegen") Then
            Using New Centered_MessageBox(Me)
                'InsertValues = MessageBox.Show("Datenbank '" & cboDatabase.Text & "' mit Benutzer '" & txtUser.Text & "' wird angelegt." & vbNewLine &
                '              "Die Verbindungsdaten werden mit dem eingegebenen Passwort gespeichert." & vbNewLine &
                '              "ACHTUNG: Das Passwort wird nicht gespeichert!" & vbNewLine & vbNewLine &
                '              "Nach dem Erstellen Athleten und Vereine importieren?",
                '               msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes
                If MessageBox.Show("Datenbank '" & cboDatabase.Text & "' mit Benutzer '" & txtUser.Text & "' wird angelegt." & vbNewLine &
                                   "Die Verbindungsdaten werden mit dem eingegebenen Passwort gespeichert." & vbNewLine &
                                   "ACHTUNG: Das Passwort wird nicht gespeichert!",
                                    msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = DialogResult.Cancel Then
                    DialogResult = DialogResult.Cancel
                    Return
                End If
            End Using

            Cursor = Cursors.WaitCursor

            WriteConnToIni()

            Dim Keys = {"FastReport", "MySQLDriver"}
            Dim x = NativeMethods.INI_Read(Keys(0), Keys(1), App_IniFile)
            If x.Equals("?") Then
                If User.Get_Driver.Contains(FR_Driver) Then
                    NativeMethods.INI_Write(Keys(0), Keys(1), FR_Driver, App_IniFile)
                    User.MySQL_Driver = FR_Driver
                End If
            End If

            Dim DB = New MySQL_Database
            Dim databaseResult = DB.CreateDatabase(txtServer.Text, txtUser.Text, txtPassword.Text, txtPort.Text, cboDatabase.Text)
            If databaseResult.Equals(String.Empty) Then
                Dim userResult = DB.CreateUser(txtServer.Text, txtUser.Text, txtPassword.Text, txtPort.Text, cboDatabase.Text)
                If String.IsNullOrEmpty(userResult) Then
                    Dim tableResult = DB.CreateTables(txtServer.Text, txtUser.Text, txtPassword.Text, txtPort.Text, cboDatabase.Text, InsertValues)
                    If tableResult.Equals(String.Empty) Then
                        btnSaveLogin.PerformClick()
                        txtUserPassword.Focus()
                    Else
                        msg = "Fehler beim Anlegen der Tabellen" & vbNewLine & "(" & tableResult & ")"
                    End If
                Else
                    msg = "Fehler beim Anlegen des Benutzers" & vbNewLine & "(" & userResult & ")"
                End If
            Else
                msg = "Fehler beim Anlegen der Datenbank" & vbNewLine
                'If LCase(databaseResult).StartsWith("Access denied for user") OrElse
                '   LCase(databaseResult).Contains("not allowed to connect") Then
                'Else
                msg += "(" & databaseResult & ")"
                'End If
                msg += StrDup(2, vbNewLine) & "Bitte als IP-Adresse 'localhost' auswählen."
                txtServer.Text = "localhost"
            End If
            Cursor = Cursors.Default

            If Not String.IsNullOrEmpty(msg) Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
                DialogResult = DialogResult.Retry
                Return
            End If
            btnAnmelden.Text = "Anmelden"
        End If
#End Region

#Region "Guid"
        If (IsNothing(User.Guid) OrElse String.IsNullOrEmpty(User.Guid)) AndAlso Not Lock Then
            User.Guid = Guid.NewGuid.ToString
        ElseIf Not String.IsNullOrEmpty(User.Guid) AndAlso Lock Then
            ' Anmeldung nach Lock
            If User.Passwort.Equals(txtUserPassword.Text) Then Close()
            Return
        End If
#End Region

#Region "changing stage for user during contest"
        If Not IsNothing(formLeader) AndAlso Not formLeader.IsDisposed Then
            If tmpBohle > -1 AndAlso tmpBohle <> User.Bohle Then
                'Using New Centered_MessageBox(Me)
                '    If MessageBox.Show("Beim Wechsel der Bohle wird der laufende Wettkampf" & vbNewLine & "automatisch geschlossen.", msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then
                '        User.Bohle = tmpBohle
                '        Return
                '    End If
                'End Using
                'formLeader.Dispose()
                Change_LeaderGruppe()
                Close()
                Return
            End If
        End If
#End Region

        DialogResult = DialogResult.OK
        User.Passwort = txtUserPassword.Text
        If Not DatabaseSync Then
            ConnError = Nothing
            Connect_Database()
        End If
    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

        Dim DlgResult = DialogResult.None

        If IsNothing(Lock) Then Return

        Using New Centered_MessageBox(Me, {"OK", "Zurück"})
            DlgResult = MessageBox.Show("Die Anwendung wird geschlossen.", msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation)
        End Using

        If DlgResult = DialogResult.OK Then
            DialogResult = DialogResult.Abort
        Else
            DialogResult = DialogResult.Retry
            txtUserPassword.Focus()
        End If

    End Sub
    Private Sub btnConnTest_Click(sender As Object, e As EventArgs) Handles btnConnTest.Click

        Dim test = "server=" + txtServer.Text + ";database=" + cboDatabase.Text + ";port=" + txtPort.Text + ";userid=" + txtUser.Text + ";password=" + txtPassword.Text + ";Convert Zero Datetime=True;connect timeout=1;"

        ConnTest(test, Not String.IsNullOrEmpty(txtPassword.Text), Me)



        'Dim msg = String.Empty
        'Dim icon = MessageBoxIcon.Information

        'Using conn As New MySqlConnection(test)
        '    Try
        '        conn.Open()
        '        msg = "Verbindung zur Datenbank erfolgreich hergestellt"
        '    Catch ex As MySqlException
        '        msg = "Fehler beim Verbinden mit der Datenbank"
        '        icon = MessageBoxIcon.Error
        '    End Try
        'End Using

        'msg += vbNewLine + "Benutze Passwort: " & If(String.IsNullOrEmpty(txtPassword.Text), "Nein", "Ja")

        'Using New Centered_MessageBox(Me)
        '    MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, icon)
        'End Using

    End Sub
    Private Sub btnServer_Click(sender As Object, e As EventArgs) Handles btnServer.Click
        If Height = MinimumSize.Height Then
            Height = MaximumSize.Height
            Dim ConnStr = Get_ConnString(txtUserPassword.Text)
            If Not String.IsNullOrEmpty(ConnStr) AndAlso Not ConnStr.Equals("Manuell") Then
                Write_Connection(ConnStr)
            End If
            txtServer.Focus()
        ElseIf Height = MaximumSize.Height Then
            Height = MinimumSize.Height
            txtUserPassword.Focus()
        End If
    End Sub
    Private Sub btnDevices_Click(sender As Object, e As EventArgs) Handles btnDevices.Click

        Using IP_Addresses As New IP_Addresses
            AddHandler IP_Addresses.DevicesStarted, AddressOf Set_Status
            AddHandler IP_Addresses.DevicesCompleted, AddressOf HostsTable_Update
            IP_Addresses.Get_Devices()
        End Using

    End Sub
    Private Sub btnIP_Click(sender As Object, e As EventArgs) Handles btnIP.Click

        Cursor = Cursors.WaitCursor

        Using IP_Addresses As New IP_Addresses
            AddHandler IP_Addresses.IPsCompleted, AddressOf Update_UserIPs
            IP_Addresses.Get_HostIPs(btnIP)
        End Using

        Cursor = Cursors.Default
    End Sub
    Private Sub btnSaveLogin_Click(sender As Object, e As EventArgs) Handles btnSaveLogin.Click

        'If String.IsNullOrEmpty(txtUserPassword.Text) Then
        '    Using New Centered_MessageBox(Me)
        '        MessageBox.Show("Benutzer-Passwort fehlt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    End Using
        '    txtUserPassword.Focus()
        '    Return
        'End If

        If String.IsNullOrEmpty(txtServer.Text) OrElse String.IsNullOrEmpty(txtUser.Text) OrElse String.IsNullOrEmpty(cboDatabase.Text) Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Die Daten für die Anmeldung am Server sind nicht korrekt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            If String.IsNullOrEmpty(txtServer.Text) Then
                txtServer.Focus()
            ElseIf String.IsNullOrEmpty(txtUser.Text) Then
                txtUser.Focus()
            Else
                cboDatabase.Focus()
            End If
        Else
            WriteConnToIni()
            txtServer.Focus()
        End If

    End Sub

    Private Sub WriteConnToIni()
        Dim ConnStr As String

        If String.IsNullOrEmpty(txtPort.Text) Then txtPort.Text = txtPort.Tag.ToString
        ConnStr = "server=" + txtServer.Text + ";database=" + cboDatabase.Text + ";port=" + txtPort.Text + ";userid=" + txtUser.Text + ";password=" + txtPassword.Text + ";Convert Zero Datetime=True;connect timeout=1;"

        ' Encoding
        Dim _Crypter As New Crypter(txtUserPassword.Text)
        Dim StringEncoded = _Crypter.EncryptData(ConnStr)
        NativeMethods.INI_Write("Anmeldung", "Connection", StringEncoded, App_IniFile)
    End Sub

    Private Sub Set_Status()
        btnDevices.Enabled = False
        With lblStatus
            .ForeColor = Color.Firebrick
            .Text = "Host-Einträge lesen"
            ' .Visible = True
        End With
    End Sub
    Private Sub Update_UserIPs(sender As Object, result As List(Of HostIP))

        'If Not IsNothing(result) Then
        '    User.IPs.Clear()
        '    For Each r In result
        '        User.IPs.Add(New HostIP With {.IP = r.IP, .Index = r.Index, .SSID = r.SSID, .Typ = r.Typ})
        '    Next
        'End If

        If sender.Equals(Me) Then
            If User.IPs.Count = 0 Then
                With lblStatus
                    .Text = "keine IP-Adressen"
                    .ForeColor = Color.Gray
                End With
                Return
            ElseIf Not User.IPs.Count > 1 Then
                User.NetworkID = 0
                Using IP_Addresses As New IP_Addresses With {.NetIndex = User.IPs(User.NetworkID).Index}
                    AddHandler IP_Addresses.DevicesStarted, AddressOf Set_Status
                    AddHandler IP_Addresses.DevicesCompleted, AddressOf HostsTable_Update
                    IP_Addresses.Get_Devices()
                End Using
                Return
            End If
        End If

        'lblMessage.Visible = sender.Equals(Me)
        'btnExitPanel.Enabled = Not sender.Equals(Me)

        bsIPs = New BindingSource With {.DataSource = User.IPs}
        With dgvNetwork
            .DataSource = Nothing
            .AutoGenerateColumns = False
            .DataSource = bsIPs
            If Not sender.Equals(Me) Then
                .Rows(User.NetworkID).Selected = True 'Cells("Selected").Value = True
                If .FirstDisplayedScrollingRowIndex < User.NetworkID Then .FirstDisplayedScrollingRowIndex = User.NetworkID
            End If
        End With
        With pnlIPs
            Label6.Text = If(sender.Equals(Me), "Netzwerk-Adapter", "Eigene IP-Adresse")
            .Height = If(sender.Equals(Me), .MaximumSize.Height, .MinimumSize.Height)
            .Location = New Point((ClientSize.Width - .Width) \ 2, (ClientSize.Height - .Height) \ 2)
            .Visible = True
        End With

        dgvNetwork.Focus()
    End Sub
    'Private Function Get_DataBases(ConnString As String) As DataTable
    '    Dim tmp = New DataTable
    '    Using conn As New MySqlConnection(ConnString)
    '        Try
    '            conn.Open()
    '            cmd = New MySqlCommand("select schema_name from information_schema.schemata where schema_name like 'weightlift%';", conn)
    '            tmp.Load(cmd.ExecuteReader())
    '            Return tmp
    '        Catch ex As MySqlException
    '            'Databases = Get_DataBases("server=" + txtServer.Tag.ToString + ";port=" + txtPort.Tag.ToString + ";uid=root;")
    '        End Try
    '    End Using
    '    Return Nothing
    'End Function

    Private Function Get_ConnString(Optional Password As String = "") As String

        If Not String.IsNullOrEmpty(txtServer.Text) AndAlso Not String.IsNullOrEmpty(txtPort.Text) AndAlso Not String.IsNullOrEmpty(txtUser.Text) AndAlso Not String.IsNullOrEmpty(cboDatabase.Text) Then
            ' Anmeldedaten vorhanden
            Return "server=" + txtServer.Text + ";database=" + cboDatabase.Text + ";port=" + txtPort.Text + ";userid=" + txtUser.Text + ";password=" + txtPassword.Text + ";Convert Zero Datetime=True;connect timeout=1;"
        End If

        If String.IsNullOrEmpty(txtServer.Text) OrElse String.IsNullOrEmpty(txtUser.Text) OrElse String.IsNullOrEmpty(cboDatabase.Text) Then
            ' keine Anmeldedaten vorhanden
            Return Get_ConnFromIni(Password)
        End If

        Return String.Empty
    End Function
    Private Function Get_ConnFromIni(Optional Password As String = "", Optional Test As Boolean = False) As String
        Try
            Dim CipherText = NativeMethods.INI_Read("Anmeldung", "Connection", App_IniFile)
            If CipherText <> "?" AndAlso CipherText <> "" Then
                If Test Then Return CipherText
                ' Decoding
                Dim _Crypter As New Crypter(Password)
                Return _Crypter.DecryptData(CipherText)
            ElseIf Not Test Then
                ' keine gespeicherte Verbindung vorhanden
                'Using New Centered_MessageBox(Me)
                '    If MessageBox.Show("Keine Anmeldedaten für die Datenbank gespeichert." & vbNewLine & "Anmeldedaten jetzt eingeben?", msgCaption,
                '                           MessageBoxButtons.OKCancel, MessageBoxIcon.Information) = DialogResult.OK Then
                Return "Manuell"
                '    End If
                'End Using
            End If
        Catch ex As System.Security.Cryptography.CryptographicException
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Die MySQL-Verbindungszeichenfolge " & vbNewLine & "konnte nicht gelesen werden." &
                                vbNewLine & "(" & If(String.IsNullOrEmpty(txtUserPassword.Text), "kein", "falsches") & " Passwort)",
                                msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        End Try
        Return String.Empty
    End Function

    Private Function Fill_Connection() As String
        txtServer.Text = txtServer.Tag.ToString
        cboDatabase.Text = cboDatabase.Tag.ToString
        txtUser.Text = txtUser.Tag.ToString
        txtPassword.Text = txtPassword.Tag.ToString
        txtPort.Text = txtPort.Tag.ToString
        Return cboDatabase.Tag.ToString
    End Function

    Private Function Write_Connection(ConnStr As String, Optional Sync As Boolean = False) As String
        ' Anmeldedaten in Textfelder schreiben oder Textfelder leeren, wenn ConnStr=""

        Dim items = Split(ConnStr, ";")

        If items.Count >= 4 Then
            txtServer.Text = Split(items(0), "=")(1)
            cboDatabase.Text = Split(items(1), "=")(1)
            txtPort.Text = Split(items(2), "=")(1)
            txtUser.Text = Split(items(3), "=")(1)
            txtPassword.Text = Split(items(4), "=")(1)
            If Sync Then DatabaseSync = True
            Return Split(items(1), "=")(1)
        End If

        For Each ctl As Control In grpServer.Controls
            If TypeOf ctl Is TextBox OrElse TypeOf ctl Is ComboBox Then ctl.Text = String.Empty
        Next

        Return String.Empty

    End Function
    Private Sub Connect_Database()

        Cursor = Cursors.WaitCursor

#Region "Connection"
        User.ConnString = Get_ConnString(User.Passwort)

        If User.ConnString.Equals("Manuell") Then
            User.ConnString = String.Empty
            Height = MaximumSize.Height
            txtPort.Text = txtPort.Tag.ToString
            cboDatabase.SelectedIndex = -1            'DataSource = Nothing
            txtServer.Focus()
            DialogResult = DialogResult.Retry
            Cursor = Cursors.Default
            Return
        ElseIf String.IsNullOrEmpty(User.ConnString) Then
            User.Passwort = String.Empty
            txtUserPassword.Text = String.Empty
            txtUserPassword.Focus()
            DialogResult = DialogResult.Retry
            Cursor = Cursors.Default
            Return
        Else
            User.Database = Write_Connection(User.ConnString)
        End If
#End Region

#Region "MySQL-Server"
        DialogResult = Check_MySQL_Connection() ' Users aus DB einlesen
        If DialogResult = DialogResult.Cancel Then
            btnCancel.PerformClick()
        ElseIf DialogResult = DialogResult.Abort Then
            Close()
        ElseIf DialogResult = DialogResult.Retry Then
            btnServer.PerformClick()
        End If
#End Region

        Cursor = Cursors.Default
    End Sub
    Private Function Check_MySQL_Connection() As DialogResult

        Dim dResult As DialogResult

#Region "mysqld auf localhost"
        If LCase(txtServer.Text).Equals("localhost") OrElse txtServer.Text.Equals("127.0.0.0") Then
            Dim MyProcesses() As Process = Process.GetProcessesByName("mysqld")
            If MyProcesses.Length = 0 Then
                ' versuche, MySQL aus XAMPP zu starten
                Try
                    Dim proc As Process = New Process()
                    With proc
                        .StartInfo = New ProcessStartInfo() With {.CreateNoWindow = True, .FileName = "C:\xampp\mysql_start.bat", .UseShellExecute = False}
                        .Start()
                    End With
                    Threading.Thread.Sleep(300)
                Catch ex As Exception
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Der MySQL-Server konnte nicht gestartet werden." & vbNewLine & "Die Anwendung wird geschlossen.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                    IsMySQL_Started = False
                    Return DialogResult.Abort
                End Try
                IsMySQL_Started = True
            End If
        End If
#End Region

#Region "User einlesen"
        Using conn As New MySqlConnection(User.ConnString)
            Try
                If Not conn.State = ConnectionState.Open Then conn.Open()
                cmd = New MySqlCommand("SELECT * FROM users;", conn)
                ' alle bereits angelegten User für diese Session auslesen 
                dtUser = New DataTable
                dtUser.Load(cmd.ExecuteReader())
            Catch ex As MySqlException
                If btnServer.Focused Then
                    Return DialogResult.Cancel
                End If
                Using New Centered_MessageBox(Me, {"Prüfen", "Beenden"})
                    Select Case ex.Number
                        Case 0
                            If ex.Message.Contains("Accesss denied for user") Then
                                dResult = MessageBox.Show("Zugriff auf die Datenbank verweigert (falsches Passwort).", msgCaption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning)
                            ElseIf ex.Message.Contains("Authentication to host") Then
                                dResult = MessageBox.Show("Datenbank-Benutzer nicht angelegt oder Passwort falsch.", msgCaption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning)
                            Else
                                dResult = MessageBox.Show(ex.Message, msgCaption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error)
                            End If
                            LogMessage("Anmeldung: Check_MySQL_Connection: User einlesen: Case 0: " & ex.Message, ex.StackTrace)
                        Case 1130 ' user not allowed to connect
                            dResult = MessageBox.Show("Dem Computer '" & Computer.Name & "' wurde der Zugriff auf den Server verweigert.", msgCaption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning)
                        Case 1042 ' unable to connect
                            dResult = MessageBox.Show("Verbindung zum angegebenen Server nicht möglich.", msgCaption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning)
                        Case Else
                            dResult = MessageBox.Show(ex.Message, msgCaption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error)
                            LogMessage("Anmeldung: Check_MySQL_Connection: User einlesen: Case Else: " & ex.Message, ex.StackTrace)
                    End Select
                End Using
                If dResult = DialogResult.Cancel Then Return DialogResult.Abort
                Return DialogResult.Retry
            End Try
        End Using
#End Region

        If dResult = DialogResult.None AndAlso User.UserId > UserID.Versuchsermittler Then
            ' Admin = immer verfügbar --> keine Überprüfung von User
            Return DialogResult.None
        End If

#Region "User"
        ' prüfen, ob WK-Leiter vorhanden ist
        Dim Leader As DataRow() = dtUser.Select("UserId = 0 And Bohle = " & User.Bohle, "UserId ASC")
        If User.UserId = UserID.Versuchsermittler AndAlso Leader.Count = 0 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Für Bohle " & User.Bohle.ToString & " ist kein Wettkampfleiter angemeldet." & vbNewLine & "Die Anwendung wird beendet.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            Return DialogResult.Abort
        End If
        ' Server-IP und -Port bestimmen
        If User.UserId = UserID.Wettkampfleiter Then
            If String.IsNullOrEmpty(txtTCP_Ip.Text) AndAlso User.NetworkID > -1 Then txtTCP_Ip.Text = User.IPs(User.NetworkID).IP
            If String.IsNullOrEmpty(txtTCP_Port.Text) Then txtTCP_Port.Text = "8000"
        ElseIf User.UserId = UserID.Versuchsermittler Then
            If String.IsNullOrEmpty(txtTCP_Ip.Text) AndAlso User.NetworkID > -1 Then txtTCP_Ip.Text = Leader(0)!Server.ToString
            If String.IsNullOrEmpty(txtTCP_Port.Text) Then txtTCP_Port.Text = Leader(0)!Port.ToString
        End If
        User.ServerIP = txtTCP_Ip.Text
        User.ServerPort = txtTCP_Port.Text
        ' prüfen, ob ausgewählte Bohle für User verfügbar ist 
        Dim CurrentUsers As DataRow() = dtUser.Select("UserId = " & User.UserId & " And Bohle = " & User.Bohle, "UserId ASC")
        If CurrentUsers.Count > 0 AndAlso Not HideMessages Then
            Using New Centered_MessageBox(Me, {"Aktualisieren", "Beenden"})
                If MessageBox.Show("Für Bohle " & User.Bohle.ToString & " ist bereits ein " & Users(User.UserId) & " angemeldet.", msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then
                    Return DialogResult.Abort
                End If
            End Using
        End If
        ' User für diese Session speichern
        Using conn As New MySqlConnection(User.ConnString)
            cmd = New MySqlCommand("INSERT INTO users (UserId, Bohle, Server, Port) VALUES(@id, @bohle, @ip, @port) " &
                                   "ON DUPLICATE KEY UPDATE Server = @ip, Port = @port;", conn)
            With cmd.Parameters
                .AddWithValue("@id", User.UserId)
                .AddWithValue("@bohle", User.Bohle)
                .AddWithValue("@ip", User.ServerIP)
                .AddWithValue("@port", User.ServerPort)
            End With
            Do
                Try
                    If Not conn.State = ConnectionState.Open Then conn.Open()
                    cmd.ExecuteNonQuery()
                    ' alle User dieser Session auslesen 
                    dtUser = New DataTable
                    cmd = New MySqlCommand("SELECT * FROM users;", conn)
                    dtUser.Load(cmd.ExecuteReader())
                    ConnError = False
                Catch ex As MySqlException
                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                End Try
            Loop While ConnError
            If ConnError Then Return DialogResult.Cancel
        End Using
#End Region

        DatabaseSync = True
        Return DialogResult.None
    End Function

    Private Sub cboBohle_EnabledChanged(sender As Object, e As EventArgs) Handles cboBohle.EnabledChanged
        lblBohle.Enabled = cboBohle.Enabled
    End Sub
    Private Sub cboBohle_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBohle.SelectedIndexChanged
        If User.Bohle > -1 Then tmpBohle = User.Bohle
        User.Bohle = cboBohle.SelectedIndex + 1
        With txtUserPassword
            .Enabled = User.UserId > -1 AndAlso User.Bohle > -1
            .Focus()
        End With
    End Sub
    Private Sub cboUser_EnabledChanged(sender As Object, e As EventArgs) Handles cboUser.EnabledChanged
        lblUser.Enabled = cboUser.Enabled
    End Sub
    Private Sub cboUser_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboUser.SelectedIndexChanged
        If Not Enabled Then Return

        User.UserId = cboUser.SelectedIndex
        cboBohle.Enabled = cboUser.Enabled AndAlso User.UserId < UserID.Administrator
        With txtUserPassword
            .Enabled = User.UserId > -1 AndAlso User.Bohle > -1
            .Focus()
        End With
        With txtTCP_Ip
            .Enabled = cboUser.Enabled AndAlso User.UserId > UserID.Wettkampfleiter
            If .Enabled OrElse User.UserId = UserID.Wettkampfleiter Then
                .Text = User.ServerIP
                txtTCP_Port.Text = User.ServerPort
            Else
                .Text = String.Empty
                txtTCP_Port.Text = String.Empty
            End If
        End With
    End Sub

    Private Sub txtUserPassword_TextChanged(sender As Object, e As EventArgs) Handles txtUserPassword.TextChanged
        Dim enable = Not String.IsNullOrEmpty(txtUserPassword.Text) AndAlso (String.IsNullOrEmpty(User.Passwort) OrElse txtUserPassword.Text.Equals(User.Passwort))
        btnServer.Enabled = enable
        btnAnmelden.Enabled = enable
        btnSaveLogin.Enabled = enable
        txtUserPassword.BackColor = If(enable, Color.FromName("Window"), Color.FromArgb(245, 212, 212))
    End Sub
    Private Sub txtUserPassword_KeyDown(sender As Object, e As KeyEventArgs) Handles txtUserPassword.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                btnAnmelden.PerformClick()
        End Select
    End Sub
    Private Sub txtUserPassword_LostFocus(sender As Object, e As EventArgs) Handles txtUserPassword.LostFocus
        'If InProcess OrElse
        '            DialogResult = DialogResult.Cancel OrElse
        '            cboUser.Focused OrElse
        '            cboBohle.Focused OrElse
        '            chkUserPW.Focused OrElse
        '            txtTCP_Ip.Focused OrElse
        '            txtTCP_Port.Focused OrElse
        '            Not ButtonStyle_TCP.Equals("Normal") OrElse
        '            txtServer.Focused OrElse
        '            Not ButtonStyle_DB.Equals("Normal") OrElse
        '            txtPort.Focused OrElse
        '            cboDatabase.Focused OrElse
        '            txtUser.Focused OrElse
        '            txtPassword.Focused OrElse
        '            chkPasswort.Focused OrElse
        '            btnSaveLogin.Focused OrElse
        '            btnCancel.Focused OrElse
        '            btnIP.Focused OrElse
        '            btnServer.Focused OrElse
        '            btnConnTest.Focused OrElse
        '            btnDevices.Focused OrElse
        '            pnlTCP.Focused OrElse
        '            pnlServer.Focused Then Return

        'btnAnmelden.PerformClick()
    End Sub

    Private Sub txtAnmeldung_LostFocus(sender As Object, e As EventArgs) Handles txtPort.LostFocus, txtUser.LostFocus, txtPassword.LostFocus
        Dim ctl = CType(sender, TextBox)
        ctl.Text = Trim(ctl.Text)
    End Sub

    '' pnlIPs
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExittt.Click
        pnlIPs.Visible = False
    End Sub
    Private Sub btnExit_MouseEnter(sender As Object, e As EventArgs) Handles btnExittt.MouseEnter
        btnExittt.ForeColor = Color.White
    End Sub
    Private Sub btnExit_MouseLeave(sender As Object, e As EventArgs) Handles btnExittt.MouseLeave
        btnExittt.ForeColor = Color.FromKnownColor(KnownColor.ControlText)
    End Sub
    Private Sub pnlIPs_VisibleChanged(sender As Object, e As EventArgs) Handles pnlIPs.VisibleChanged

        grpUser.Enabled = Not pnlIPs.Visible
        grpServer.Enabled = Not pnlIPs.Visible
        pnlButtons.Enabled = Not pnlIPs.Visible

        If pnlIPs.Visible Then
            CancelButton = Nothing
            dgvNetwork.Select()
            dgvNetwork.Focus()
        Else
            CancelButton = btnCancel
            If Height = MinimumSize.Height Then
                txtUserPassword.Focus()
            Else
                txtServer.Focus()
            End If
            Refresh()
        End If
    End Sub
    Private Sub dgvNetwork_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvNetwork.CellMouseDoubleClick
        If e.RowIndex > -1 AndAlso e.ColumnIndex > -1 Then
            If e.Button = MouseButtons.Left Then
                Set_NetworkID(e.RowIndex)
            End If
        End If
    End Sub
    Private Sub Set_NetworkID(RowIndex As Integer)
        pnlIPs.Visible = False
        If Not User.NetworkID = RowIndex Then
            User.NetworkID = RowIndex
            Using IP_Addresses As New IP_Addresses With {.NetIndex = User.IPs(User.NetworkID).Index}
                AddHandler IP_Addresses.DevicesStarted, AddressOf Set_Status
                AddHandler IP_Addresses.DevicesCompleted, AddressOf HostsTable_Update
                IP_Addresses.Get_Devices()
            End Using
        End If
    End Sub

    '' dgvServer
    Private Sub dgvServer_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvServer.KeyDown
        Dim box = CType(dgvServer.Tag, TextBox)
        If e.KeyCode = Keys.Escape Then
            dgvServer.Visible = False
        ElseIf e.KeyCode = Keys.Enter Then
            'txtServer.Text = dvHosts(dgvServer.SelectedRows(0).Index)!IP.ToString
            box.Text = dvHosts(dgvServer.SelectedRows(0).Index)!IP.ToString
            dgvServer.Visible = False
        End If
        box.Focus()
        box.SelectAll()
    End Sub
    Private Sub dgvServer_MouseClick(sender As Object, e As MouseEventArgs) Handles dgvServer.MouseClick, dgvServer.MouseDown
        If Not e.Button = MouseButtons.Left Then Return
        'txtServer.Text = dvHosts(dgvServer.HitTest(e.X, e.Y).RowIndex)!IP.ToString
        Dim box = CType(dgvServer.Tag, TextBox)
        box.Text = dvHosts(dgvServer.HitTest(e.X, e.Y).RowIndex)!IP.ToString
        dgvServer.Visible = False
        box.Focus()
        box.SelectAll()
    End Sub
    Private Sub dgvServer_MouseMove(sender As Object, e As MouseEventArgs) Handles dgvServer.MouseMove
        Dim Ix = dgvServer.HitTest(e.X, e.Y).RowIndex
        If Ix < 0 Then Return
        dgvServer.Rows(Ix).Selected = True
    End Sub
    Private Sub dgvServer_VisibleChanged(sender As Object, e As EventArgs) Handles dgvServer.VisibleChanged

        If dgvServer.Visible Then
            CancelButton = Nothing
        Else
            CancelButton = btnCancel
            txtServer.Focus()
        End If
    End Sub

    Private Sub HostsTable_Setbounds()
        With dgvServer
            .Height = .RowTemplate.Height * If(.Rows.Count > 6, 6, .Rows.Count) + 2 ' 20 mit horizontaler Scrollbar 
            If Not IsNothing(.Tag) AndAlso CType(.Tag, TextBox).Name.Equals("txtTCP_Ip") AndAlso Not Height = MaximumSize.Height Then
                Dim loc = grpUser.PointToScreen(New Point(txtTCP_Ip.Left, txtTCP_Ip.Top - .Height))
                .Location = txtTCP_Ip.FindForm.PointToClient(loc)
            End If
        End With
        btnDevices.Enabled = True

        With lblStatus
            .Text = "Liste der IP-Adressen aktualisiert"
            .ForeColor = Color.Green
        End With
    End Sub
    Private Sub HostsTable_Update(result As List(Of HostIP))

        If IsNothing(result) Then Return

        dtHosts.Clear()
        dtHosts.Rows.Add({"localhost", "", "", "", DBNull.Value})
        For Each r In result
            dtHosts.Rows.Add({r.IP, r.Device, r.SSID, r.Typ, r.Index})
        Next

        HostsTable_Setbounds()
    End Sub

    Dim ButtonStyle_DB As String = "Normal"
    Dim ButtonStyle_TCP As String = "Normal"
    Private Sub pnlServer_Paint(sender As Object, e As PaintEventArgs) Handles pnlServer.Paint
        Dim renderer As VisualStyles.VisualStyleRenderer = Nothing
        Select Case ButtonStyle_DB
            Case "Normal"
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Normal)
            Case "Hot"
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Hot)
            Case "Pressed"
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Pressed)
        End Select
        Dim rectangle1 As New Rectangle(0, 0, 17, 20)
        renderer.DrawBackground(e.Graphics, rectangle1)
    End Sub
    Private Sub pnlServer_MouseEnter(sender As Object, e As EventArgs) Handles pnlServer.MouseEnter
        If ButtonStyle_DB = "Pressed" Then Return
        ButtonStyle_DB = "Hot"
        pnlServer.Refresh()
    End Sub
    Private Sub pnlServer_MouseLeave(sender As Object, e As EventArgs) Handles pnlServer.MouseLeave
        If ButtonStyle_DB = "Pressed" Then Return
        ButtonStyle_DB = "Normal"
        pnlServer.Refresh()
    End Sub
    Private Sub pnlServer_MouseClick(sender As Object, e As MouseEventArgs) Handles pnlServer.MouseClick
        If ButtonStyle_DB = "Pressed" Then
            ButtonStyle_DB = "Hot"
            dgvServer.Visible = False
        Else
            ButtonStyle_DB = "Pressed"
            Dim loc = grpServer.PointToScreen(New Point(txtServer.Left, txtServer.Top + txtServer.Height))
            With dgvServer
                .Location = txtServer.FindForm.PointToClient(loc)
                .Tag = txtServer
                Dim row = bsHosts.Find("IP", txtServer.Text)
                If Not row = -1 Then .Rows(row).Selected = True
                .Visible = True
                .BringToFront()
                .Focus()
            End With
        End If
        pnlServer.Refresh()
    End Sub
    Private Sub pnlTCP_Paint(sender As Object, e As PaintEventArgs) Handles pnlTCP.Paint
        Dim renderer As VisualStyles.VisualStyleRenderer = Nothing
        Select Case ButtonStyle_TCP
            Case "Normal"
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Normal)
            Case "Hot"
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Hot)
            Case "Pressed"
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Pressed)
        End Select
        Dim rectangle1 As New Rectangle(0, 0, 17, 20)
        renderer.DrawBackground(e.Graphics, rectangle1)
    End Sub
    Private Sub pnlTCP_MouseEnter(sender As Object, e As EventArgs) Handles pnlTCP.MouseEnter
        If ButtonStyle_TCP = "Pressed" Then Return
        ButtonStyle_TCP = "Hot"
        pnlTCP.Refresh()
    End Sub
    Private Sub pnlTCP_MouseLeave(sender As Object, e As EventArgs) Handles pnlTCP.MouseLeave
        If ButtonStyle_TCP = "Pressed" Then Return
        ButtonStyle_TCP = "Normal"
        pnlTCP.Refresh()
    End Sub
    Private Sub pnlTCP_MouseClick(sender As Object, e As MouseEventArgs) Handles pnlTCP.MouseClick
        If ButtonStyle_TCP = "Pressed" Then
            ButtonStyle_TCP = "Hot"
            dgvServer.Visible = False
        Else
            ButtonStyle_TCP = "Pressed"
            Dim loc = grpUser.PointToScreen(New Point(txtTCP_Ip.Left, txtTCP_Ip.Top + If(Height = MaximumSize.Height, txtTCP_Ip.Height, -dgvServer.Height)))
            With dgvServer
                .Location = txtTCP_Ip.FindForm.PointToClient(loc)
                .Tag = txtTCP_Ip
                Dim row = bsHosts.Find("IP", txtTCP_Ip.Text)
                If Not row = -1 Then .Rows(row).Selected = True
                .Visible = True
                .BringToFront()
                .Focus()
            End With
        End If
        pnlTCP.Refresh()
    End Sub

    Private Sub txtServer_GotFocus(sender As Object, e As EventArgs) Handles txtServer.GotFocus
        ButtonStyle_DB = "Normal"
        pnlServer.Refresh()
    End Sub
    Private Sub txtTCP_Ip_GotFocus(sender As Object, e As EventArgs) Handles txtTCP_Ip.GotFocus
        ButtonStyle_TCP = "Normal"
        pnlTCP.Refresh()
    End Sub

    Private Sub frmAnmeldung_GotFocus(sender As Object, e As EventArgs) Handles Me.Click, cboUser.GotFocus, cboBohle.GotFocus, txtPassword.GotFocus, txtPort.GotFocus, txtTCP_Port.GotFocus, txtUser.GotFocus, txtUserPassword.GotFocus,
            btnAnmelden.GotFocus, btnCancel.GotFocus, btnConnTest.GotFocus, btnIP.GotFocus, btnSaveLogin.GotFocus, btnServer.GotFocus,
            chkPasswort.GotFocus, chkUserPW.GotFocus, cboBohle.DropDown, cboUser.DropDown

        dgvServer.Visible = False
    End Sub

    Private Sub txtTCP_Ip_EnabledChanged(sender As Object, e As EventArgs) Handles txtTCP_Ip.EnabledChanged
        Label7.Enabled = txtTCP_Ip.Enabled
        txtTCP_Port.Enabled = txtTCP_Ip.Enabled

        Label8.Enabled = txtTCP_Ip.Enabled
    End Sub

    Private Sub Credentials_TextChanged(sender As Object, e As EventArgs) Handles txtServer.TextChanged, txtPort.TextChanged, txtUser.TextChanged, txtPassword.TextChanged
        Credentials_Changed()
    End Sub

    'Private Sub cboDatabase_LostFocus(sender As Object, e As EventArgs) Handles cboDatabase.LostFocus
    '    Timer1.Stop()
    'End Sub
    Private Sub cboDatabase_TextChanged(sender As Object, e As EventArgs) Handles cboDatabase.TextChanged
        If Not cboDatabase.Focused Then Return
        Timer1.Stop()
        Timer1.Start()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Stop()
        If Not (String.IsNullOrEmpty(txtServer.Text) OrElse String.IsNullOrEmpty(txtUser.Text)) Then
            Dim Database = cboDatabase.Text
            Dim conn = "server=" + txtServer.Text + ";userid=" & txtUser.Text + ";"
            If Not String.IsNullOrEmpty(txtPassword.Text) Then conn += "password=" + txtPassword.Text + ";"
            dtDatabases = Get_DataBases(conn)
            If IsNothing(dtDatabases) Then
                btnAnmelden.Text = "Datenbank anlegen"
            Else
                Dim found As DataRow() = dtDatabases.Select("schema_name = '" & Database & "'")
                If found.Length = 0 Then
                    btnAnmelden.Text = "Datenbank anlegen"
                Else
                    btnAnmelden.Text = "Anmelden"
                End If
            End If
        End If
        Credentials_Changed()
    End Sub

    Private Sub Credentials_Changed()
        If String.IsNullOrEmpty(User.ConnString) Then Return

        Dim items = Split(User.ConnString, ";").ToList
        Dim inputs = New List(Of String)
        inputs.AddRange({txtServer.Text, cboDatabase.Text, txtPort.Text, txtUser.Text, txtPassword.Text})

        For i = 0 To inputs.Count - 1
            If Not inputs(i).Equals(Split(items(i), "=")(1)) Then
                DatabaseSync = False
                Exit For
            End If
        Next
    End Sub

End Class

Public Class HostIP
    Property IP As String
    Property CIDR As Integer
    Property NetAddress As String ' Netz-Adresse des Hosts
    Property Broadcast As String ' Broadcast-Adresse des Hosts
    Property NetPart As String ' Netzanteil der IP 
    Property Gateway As String ' Default-Gateway der IP
    Property Device As String
    Property SSID As String
    Property Typ As String
    Property Index As Integer
End Class

Public Class IP_Addresses
    Implements IDisposable

    Property NetIndex As Integer = -1

    WithEvents bgwDevices As New BackgroundWorker

    Public Event IPsCompleted(sender As Object, result As List(Of HostIP))
    Public Event DevicesCompleted(result As List(Of HostIP))
    Public Event DevicesStarted()

    Dim dicProfiles As New Dictionary(Of Integer, String())

    Private Function Get_Profiles() As Dictionary(Of Integer, String())

        Dim dic = New Dictionary(Of Integer, String())

        Dim profiles = PS_Run("get-netconnectionprofile")

        For Each profil In profiles
            Dim prop = profil.Properties
            Dim Net_Name = CStr(prop.Single(Function(p) p.Name = "Name").Value)
            Dim Net_Index = CInt(prop.Single(Function(p) p.Name = "InterfaceIndex").Value)
            Dim Net_Typ = CStr(prop.Single(Function(p) p.Name = "InterfaceAlias").Value)
            'Dim Net_Server = prop.Single(Function(p) p.Name = "CimSystemProperties").Value
            dic(Net_Index) = {Net_Name, Net_Typ}
        Next
        Return dic
    End Function

    Public Function Get_HostIPs(sender As Object) As String

        dicProfiles = Get_Profiles()

        Dim result = Read_Host(Dns.GetHostName)

        If Not IsNothing(result) Then
            User.IPs.Clear()
            'For Each r In result
            User.IPs.AddRange(result)
            'Next
        End If

        If IsNothing(sender) Then
            Return User.IPs(User.NetworkID).IP
        End If

        RaiseEvent IPsCompleted(sender, result)

        Return Nothing
    End Function

    Public Sub Get_Devices()

        RaiseEvent DevicesStarted()

        With User.IPs(User.NetworkID)
            Dim StartIP As Integer = CInt(.NetAddress.Substring(.NetPart.Length)) + 1
            Dim EndIP As Integer = CInt(.Broadcast.Substring(.NetPart.Length)) - 1
            Dim checker As New myPing
            checker.NetString = .NetPart
            checker.GateWay = .Gateway
            AddHandler checker.CheckCompleted, AddressOf bgwDevices_Start
            Try
                checker.DoPing(StartIP, EndIP)
            Catch ex As Exception
                LogMessage("Anmeldung: Get_Debvices: DoPing: " & ex.Message, ex.StackTrace)
            End Try
        End With
    End Sub

    Private Sub bgwDevices_Start(Results As List(Of String))
        If bgwDevices.IsBusy Then Return
        bgwDevices.RunWorkerAsync(Results)
    End Sub
    Private Sub bgwDevices_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwDevices.DoWork

        Dim IPsList = CType(e.Argument, List(Of String))
        Dim results As New List(Of HostIP)

        dicProfiles = Get_Profiles()

        'Dim devices = PS_Run("net view")

        'If Not IsNothing(devices) Then
        '    For Each d In devices
        '        Dim item = CStr(d.BaseObject)
        '        If item.Contains("\\") Then
        '            Dim Device_Name = RTrim(item.Replace("\\", ""))
        '            If Device_Name.Contains(" ") Then Device_Name = Split(Device_Name, " ")(0)
        '            lstDevices.Add(Device_Name)
        '        End If
        '    Next
        'End If

        Do
            Try
                For Each IP In IPsList
                    Dim result = Read_Host(IP)
                    If Not IsNothing(result) Then
                        results.AddRange(result.ToArray)
                    End If
                Next
                Exit Do
            Catch ex As Exception
                results.Clear()
            End Try
        Loop While results.Count = 0

        e.Result = results

    End Sub
    Private Sub bgwDevices_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwDevices.RunWorkerCompleted

        Dim r = CType(e.Result, List(Of HostIP))
        RaiseEvent DevicesCompleted(r)

    End Sub

    Private Function Read_Host(ByVal NameOrIP As String) As List(Of HostIP)

        Dim result As New List(Of HostIP)
        Dim devIPs As IPAddress()
        Dim DeviceName = String.Empty

        If Not IPAddress.TryParse(NameOrIP, IPAddress.Loopback) Then
            devIPs = Dns.GetHostAddresses(NameOrIP)
            DeviceName = Split(NameOrIP, ".")(0)
        Else
            devIPs = {IPAddress.Parse(NameOrIP)}
            Try
                DeviceName = Split(Dns.GetHostEntry(NameOrIP).HostName, ".")(0)
            Catch ex As Exception
            End Try
        End If

        For Each devIP In devIPs

            If devIP.AddressFamily = AddressFamily.InterNetwork Then
                Dim Device_IP As String = devIP.ToString
                Dim adapterIP As IPAddress
                Dim adapters = NetworkInterface.GetAllNetworkInterfaces()

                For Each adapter In adapters
                    If adapter.OperationalStatus = OperationalStatus.Up And Not adapter.NetworkInterfaceType = NetworkInterfaceType.Loopback Then

                        Dim adapterIPProperties As IPv4InterfaceProperties = adapter.GetIPProperties().GetIPv4Properties
                        Dim Device_Index = adapterIPProperties.Index ' Network-Index der IP-Adresse

                        If NetIndex = Device_Index OrElse NetIndex = -1 Then

                            Dim _addr(2) As String
                            Dim _cidr = 0
                            Dim _gateway = String.Empty
                            Dim unicastinfos = adapter.GetIPProperties().UnicastAddresses
                            For Each unicastinfo In unicastinfos
                                If unicastinfo.Address.AddressFamily = AddressFamily.InterNetwork Then
                                    adapterIP = unicastinfo.Address
                                    Using IPcalc As New IPcalc
                                        _cidr = IPcalc.Mask2CIDR(unicastinfo.IPv4Mask.GetAddressBytes)
                                        _addr = IPcalc.Get_NA_BC_NP(adapterIP.ToString, _cidr)
                                    End Using
                                    'Default Gateway
                                    Dim adapterProperties As IPInterfaceProperties = adapter.GetIPProperties()
                                    For Each gateway As GatewayIPAddressInformation In adapterProperties.GatewayAddresses
                                        _gateway = gateway.Address.ToString()
                                    Next
                                End If
                            Next

                            If Device_IP.Contains(_addr(2)) Then
                                Dim Profil_Name = String.Empty
                                Dim Profil_Typ = String.Empty
                                If dicProfiles.ContainsKey(Device_Index) Then
                                    Profil_Name = dicProfiles(Device_Index)(0)
                                    Profil_Typ = dicProfiles(Device_Index)(1)
                                End If
                                result.Add(New HostIP With {.IP = Device_IP,
                                                            .CIDR = _cidr,
                                                            .NetAddress = _addr(0),
                                                            .Broadcast = _addr(1),
                                                            .Gateway = _gateway,
                                                            .NetPart = _addr(2),
                                                            .Device = DeviceName,
                                                            .SSID = Profil_Name,
                                                            .Typ = Profil_Typ,
                                                            .Index = Device_Index})
                            End If
                        End If
                    End If
                Next
            End If
        Next
        Return result
    End Function

    Private Function PS_Run(ByVal Command As String) As Collection(Of PSObject)
        Dim ps As PowerShell = PowerShell.Create()
        ps.AddScript(Command)
        Dim ErgebnisMenge As Collection(Of PSObject) = ps.Invoke()

        If ps.Streams.[Error].Count = 0 Then
            Return ErgebnisMenge
        Else
            Return Nothing
        End If
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
            End If

            ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
            ' TODO: große Felder auf Null setzen.
        End If
        disposedValue = True
    End Sub
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        Dispose(True)
        ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
        ' GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class

Public Class myPing
    Property NetString As String ' = User.IPs(User.SelectedNetwork).NP ' Net-Part of IP
    Property GateWay As String
    Private LastIP As Integer ' =  = User.IPs(User.SelectedNetwork).BC ' Broadcast of IP 
    Public Event CheckCompleted(Results As List(Of String))

    Private ResultList As New List(Of String)

    Private Class DoPingAsync
        Private WithEvents m_Ping As New Ping
        Public Event PingCompleted(sender As Object, e As PingCompletedEventArgs)

        Public Sub New(IP As String)
            m_Ping.SendAsync(IP, IP)
        End Sub

        Private Sub m_Ping_PingCompleted(sender As Object, e As PingCompletedEventArgs) Handles m_Ping.PingCompleted
            RaiseEvent PingCompleted(sender, e)
        End Sub

    End Class

    Public Sub DoPing(StartIP As Integer, EndIP As Integer)
        LastIP = EndIP
        For x = StartIP To EndIP
            'Dim p As DoPingAsync
            Dim p = New DoPingAsync(NetString & x.ToString)
            AddHandler p.PingCompleted, AddressOf PingCompleted
        Next
    End Sub

    Private Sub PingCompleted(sender As Object, e As PingCompletedEventArgs)
        If e.Reply.Status.ToString = "Success" Then
            Dim IP = e.UserState.ToString
            If Not IP.Equals(GateWay) Then ResultList.Add(IP)
        End If
        Dim mask = Split(e.UserState.ToString, ".")
        'If CInt(Split(e.UserState.ToString, ".")(3)) = LastIP Then
        If mask.Length = 4 AndAlso CInt(mask(3)) = LastIP Then
            RaiseEvent CheckCompleted(ResultList)
            'Else
            '    RaiseEvent CheckCompleted(New List(Of String))
        End If
    End Sub

End Class
