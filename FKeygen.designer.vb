﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FKeygen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LHardCode = New System.Windows.Forms.Label()
        Me.txtHardCode = New System.Windows.Forms.TextBox()
        Me.txtSerial = New System.Windows.Forms.TextBox()
        Me.LSerial = New System.Windows.Forms.Label()
        Me.btnRegister = New System.Windows.Forms.Button()
        Me.btnTestversion = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LHardCode
        '
        Me.LHardCode.AutoSize = True
        Me.LHardCode.Location = New System.Drawing.Point(12, 22)
        Me.LHardCode.Name = "LHardCode"
        Me.LHardCode.Size = New System.Drawing.Size(81, 13)
        Me.LHardCode.TabIndex = 0
        Me.LHardCode.Text = "Hardware-Code"
        Me.LHardCode.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtHardCode
        '
        Me.txtHardCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtHardCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtHardCode.Location = New System.Drawing.Point(98, 19)
        Me.txtHardCode.Name = "txtHardCode"
        Me.txtHardCode.ReadOnly = True
        Me.txtHardCode.Size = New System.Drawing.Size(237, 20)
        Me.txtHardCode.TabIndex = 1
        Me.txtHardCode.TabStop = False
        '
        'txtSerial
        '
        Me.txtSerial.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSerial.Location = New System.Drawing.Point(98, 45)
        Me.txtSerial.Name = "txtSerial"
        Me.txtSerial.Size = New System.Drawing.Size(237, 20)
        Me.txtSerial.TabIndex = 0
        Me.txtSerial.Text = "00000-00000-00000-00000-00000"
        '
        'LSerial
        '
        Me.LSerial.AutoSize = True
        Me.LSerial.Location = New System.Drawing.Point(19, 48)
        Me.LSerial.Name = "LSerial"
        Me.LSerial.Size = New System.Drawing.Size(74, 13)
        Me.LSerial.TabIndex = 3
        Me.LSerial.Text = "Seriennummer"
        Me.LSerial.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnRegister
        '
        Me.btnRegister.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRegister.Enabled = False
        Me.btnRegister.Location = New System.Drawing.Point(260, 88)
        Me.btnRegister.Name = "btnRegister"
        Me.btnRegister.Size = New System.Drawing.Size(75, 23)
        Me.btnRegister.TabIndex = 7
        Me.btnRegister.TabStop = False
        Me.btnRegister.Text = "Registrieren"
        Me.btnRegister.UseVisualStyleBackColor = True
        '
        'btnTestversion
        '
        Me.btnTestversion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTestversion.Location = New System.Drawing.Point(12, 88)
        Me.btnTestversion.Name = "btnTestversion"
        Me.btnTestversion.Size = New System.Drawing.Size(115, 23)
        Me.btnTestversion.TabIndex = 8
        Me.btnTestversion.TabStop = False
        Me.btnTestversion.Text = "30-Tage-Testversion"
        Me.btnTestversion.UseVisualStyleBackColor = True
        '
        'FKeygen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(347, 122)
        Me.Controls.Add(Me.btnTestversion)
        Me.Controls.Add(Me.btnRegister)
        Me.Controls.Add(Me.LSerial)
        Me.Controls.Add(Me.txtSerial)
        Me.Controls.Add(Me.txtHardCode)
        Me.Controls.Add(Me.LHardCode)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FKeygen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GFHsoft Lizenzierung"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LHardCode As System.Windows.Forms.Label
    Friend WithEvents txtHardCode As System.Windows.Forms.TextBox
    Friend WithEvents txtSerial As System.Windows.Forms.TextBox
    Friend WithEvents LSerial As System.Windows.Forms.Label
    Friend WithEvents btnRegister As System.Windows.Forms.Button
    Friend WithEvents btnTestversion As Button
End Class
