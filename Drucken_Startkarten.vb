﻿
Imports System.ComponentModel
Imports MySqlConnector

Public Class Drucken_Startkarten

    Property Bereich As String

    Dim PrintDate As String

    Dim Query As String
    Dim cmd As MySqlCommand
    Dim dvVerein As DataView
    Dim dvMeldung As DataView
    Dim dvGruppen As DataView
    Dim dvMannschaft As DataView

    Dim dtMeldung As New DataTable
    Dim dtVerein As New DataTable
    Dim dtRegion As New DataTable
    Dim dtPrint As New DataTable
    'Dim dtVerband As New DataTable
    Dim dtAthletik As New DataTable
    Dim dtGruppen As New DataTable
    Dim dtLänderwertung As New DataTable

    Dim dtVorlage As New DataTable
    Dim dvVorlage As DataView

    'Dim AK_w As String = Wettkampf.AKs("w").ToString
    'Dim AK_m As String = Wettkampf.AKs("m").ToString
    Dim Vorlage As String
    Dim Message As New myMsg
    Dim report As FastReport.Report
    Dim action As String
    Dim Count As Integer
    Dim Docs As String
    Private Delegate Sub SetMsg(ByVal Value As String)

    Dim CurrList As New CheckedListBox
    Private SelectedIndex As Integer
    Private _SubCount As Integer
    Property SubCount As Integer
        Get
            Return _SubCount
        End Get
        Set(value As Integer)
            _SubCount = value
            If value < 2 Then
                Docs = Bereich
            Else
                Docs = If(Bereich = "Startkarte", "Startkarten", "Quittungen")
            End If
        End Set
    End Property

    Private Sub Me_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If btnCancel.Text = "Abbrechen" Then
            BackgroundWorker1.CancelAsync()
            e.Cancel = True
        Else
            NativeMethods.INI_Write(Bereich, "Vorlage", Vorlage, App_IniFile)
            RemoveHandler Message.Msg_Changed, AddressOf ProgressMsg
        End If
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        Dim tTip As New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        tTip.SetToolTip(chkDatum, "wenn nicht aktiviert, ist das Rechnungsdatum der " & Format(Now, "d"))

        ''Bereich = Tag.ToString
        'Select Case Bereich
        '    Case "Startkarte"
        btnTemplate.Visible = True
        '    Case "Quittung"
        '        chkDatum.Visible = True
        '        'chkAnwesend.Visible = True
        'End Select

#Region "Vorlage"
        Vorlage = NativeMethods.INI_Read(Bereich, "Vorlage", App_IniFile)
        If Vorlage.Equals("?") Then Vorlage = String.Empty

        dtVorlage.Columns.Add("FileName", GetType(String))
        dtVorlage.Columns.Add("Directory", GetType(String))
        Dim cols(1) As DataColumn
        cols(0) = dtVorlage.Columns("FileName")
        cols(1) = dtVorlage.Columns("Directory")
        dtVorlage.PrimaryKey = cols

        Dim v = Directory.GetFiles(Path.Combine(Application.StartupPath, "Report"), Bereich & "*.frx", SearchOption.AllDirectories).ToList
        For i = 0 To v.Count - 1
            v(i) = Replace(v(i), Path.Combine(Application.StartupPath, "Report") + "\", "")
            v(i) = Replace(v(i), ".frx", "")
            dtVorlage.Rows.Add(v(i), Path.Combine(Application.StartupPath, "Report"))
        Next
        dvVorlage = New DataView(dtVorlage)
        dvVorlage.Sort = "FileName"

        With cboVorlage
            .DataSource = dvVorlage
            .DisplayMember = "FileName"
            .ValueMember = "Directory"
            .SelectedIndex = .FindStringExact(Vorlage)
        End With
#End Region

        AddHandler Message.Msg_Changed, AddressOf ProgressMsg
    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim dtMeldefrist As New DataTable
        Dim dtStartgeld As New DataTable
        Refresh()

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                ' Tabelle Athletik
                Query = "Select m.Wettkampf, m.Teilnehmer, wa.Disziplin, d.Bezeichnung, d.Durchgaenge, " &
                            "Case When IfNull(wd.Bezeichnung, ad.Bezeichnung) = 'Faktor' then " &
                                "If(year(w.Datum) - Year(a.Geburtstag) = 5, IfNull(wd.ak5, ad.ak5) * IfNull(m.Wiegen, m.Gewicht), " &
                                "If(year(w.Datum) - Year(a.Geburtstag) = 6, IfNull(wd.ak6, ad.ak6) * IfNull(m.Wiegen, m.Gewicht), " &
                                "if (year(w.Datum) - Year(a.Geburtstag) = 7, IfNull(wd.ak7, ad.ak7) * IfNull(m.Wiegen, m.Gewicht), " &
                                "if (year(w.Datum) - Year(a.Geburtstag) = 8, IfNull(wd.ak8, ad.ak8) * IfNull(m.Wiegen, m.Gewicht), " &
                                "if (year(w.Datum) - Year(a.Geburtstag) = 9, IfNull(wd.ak9, ad.ak9) * IfNull(m.Wiegen, m.Gewicht), " &
                                "if (year(w.Datum) - Year(a.Geburtstag) = 10, IfNull(wd.ak10, ad.ak10) * IfNull(m.Wiegen, m.Gewicht), " &
                                "if (year(w.Datum) - Year(a.Geburtstag) = 11, IfNull(wd.ak11, ad.ak11) * IfNull(m.Wiegen, m.Gewicht), " &
                                "if (year(w.Datum) - Year(a.Geburtstag) = 12, IfNull(wd.ak12, ad.ak12) * IfNull(m.Wiegen, m.Gewicht), " &
                                "if (year(w.Datum) - Year(a.Geburtstag) = 13, IfNull(wd.ak13, ad.ak13) * IfNull(m.Wiegen, m.Gewicht), " &
                                "if (year(w.Datum) - Year(a.Geburtstag) = 14, IfNull(wd.ak14, ad.ak14) * IfNull(m.Wiegen, m.Gewicht), " &
                                "if (year(w.Datum) - Year(a.Geburtstag) = 15, IfNull(wd.ak15, ad.ak15) * IfNull(m.Wiegen, m.Gewicht), " &
                                "if (year(w.Datum) - Year(a.Geburtstag) = 16, IfNull(wd.ak16, ad.ak16) * IfNull(m.Wiegen, m.Gewicht), " &
                                "if (year(w.Datum) - Year(a.Geburtstag) = 17, IfNull(wd.ak17, ad.ak17) * IfNull(m.Wiegen, m.Gewicht), Null))))))))))))) " &
                            "when IfNull(wd.Bezeichnung, ad.Bezeichnung) = 'Kugel' then " &
                                "If(year(w.Datum) - Year(a.Geburtstag) = 5, IfNull(wd.ak5, ad.ak5), " &
                                "If(year(w.Datum) - Year(a.Geburtstag) = 6, IfNull(wd.ak6, ad.ak6), " &
                                "If (year(w.Datum) - Year(a.Geburtstag) = 7, IfNull(wd.ak7, ad.ak7), " &
                                "If (year(w.Datum) - Year(a.Geburtstag) = 8, IfNull(wd.ak8, ad.ak8), " &
                                "If (year(w.Datum) - Year(a.Geburtstag) = 9, IfNull(wd.ak9, ad.ak9), " &
                                "If (year(w.Datum) - Year(a.Geburtstag) = 10, IfNull(wd.ak10, ad.ak10), " &
                                "If (year(w.Datum) - Year(a.Geburtstag) = 11, IfNull(wd.ak11, ad.ak11), " &
                                "If (year(w.Datum) - Year(a.Geburtstag) = 12, IfNull(wd.ak12, ad.ak12), " &
                                "If (year(w.Datum) - Year(a.Geburtstag) = 13, IfNull(wd.ak13, ad.ak13), " &
                                "If (year(w.Datum) - Year(a.Geburtstag) = 14, IfNull(wd.ak14, ad.ak14), " &
                                "If (year(w.Datum) - Year(a.Geburtstag) = 15, IfNull(wd.ak15, ad.ak15), " &
                                "If (year(w.Datum) - Year(a.Geburtstag) = 16, IfNull(wd.ak16, ad.ak16), " &
                                "If (year(w.Datum) - Year(a.Geburtstag) = 17, IfNull(wd.ak17, ad.ak17), Null))))))))))))) " &
                            "End Addition " &
                        "From meldung m " &
                        "Left Join athleten a On a.idTeilnehmer = m.Teilnehmer " &
                        "Left Join wettkampf_athletik wa On wa.Wettkampf = m.Wettkampf " &
                        "Left Join athletik_disziplinen d On wa.Disziplin = d.idDisziplin " &
                        "Left Join wettkampf w On w.Wettkampf = m.Wettkampf " &
                        "Left Join wettkampf_athletik_defaults wd On wd.Wettkampf = m.Wettkampf And wd.Disziplin = wa.Disziplin And wd.Geschlecht = a.Geschlecht " &
                        "Left Join athletik_defaults ad On ad.Disziplin = wa.Disziplin And ad.Geschlecht = a.Geschlecht " &
                        "WHERE m.Wettkampf = " & Wettkampf.ID & '" AND " &
                        " GROUP BY Teilnehmer, Disziplin " & '"(IfNull(wd.Bezeichnung, ad.Bezeichnung) = 'Faktor' Or IfNull(wd.Bezeichnung, ad.Bezeichnung) = 'Kugel' Or Isnull(IfNull(wd.Bezeichnung, ad.Bezeichnung))) " &
                        "ORDER BY m.Teilnehmer;"
                cmd = New MySqlCommand(Query, conn)
                dtAthletik.Load(cmd.ExecuteReader)
                ' Tabelle Gruppen
                Query = "SELECT Gruppe, Bezeichnung FROM gruppen WHERE Wettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtGruppen.Load(cmd.ExecuteReader)
                ' Druckdatum
                cmd = New MySqlCommand("SELECT g.Gruppe, Ifnull(g.DatumW,Ifnull(g.Datum,w.Datum)) Druckdatum FROM gruppen g, wettkampf w WHERE g.Wettkampf = " & Wettkampf.ID & " AND w.Wettkampf = " & Wettkampf.ID & ";", conn)
                dtPrint.Load(cmd.ExecuteReader)

                ' Tabelle Heber
                Query = "
                    select m.*, a.Titel, a.Nachname, a.Vorname, a.Geschlecht Sex, a.Jahrgang, 
                    IfNull(v.Kurzname, v.Vereinsname) Vereinsname, r.region_3 Verband, s.state_iso3 Staat, v.Region, v.idVerein 
                    from meldung m
                    left join athleten a on a.idTeilnehmer = m.Teilnehmer
                    Left join verein v On v.idVerein = a.Verein 
                    Left Join region r On r.region_id = v.Region 
                    Left Join staaten s On s.state_id = a.Staat 
                    where " & Wettkampf.WhereClause("m") &
                    "group by m.Teilnehmer
                    order by Vereinsname, a.Nachname, a.Vorname;"
                cmd = New MySqlCommand(Query, conn)
                dtMeldung.Load(cmd.ExecuteReader)
                ' Tabelle Verein
                cmd = New MySqlCommand("SELECT * FROM verein;", conn)
                dtVerein.Load(cmd.ExecuteReader)
                ' Tabelle Region
                cmd = New MySqlCommand("SELECT * FROM region;", conn)
                dtRegion.Load(cmd.ExecuteReader)
                ' Tabelle Startgeld
                cmd = New MySqlCommand("SELECT * FROM wettkampf_startgeld WHERE Wettkampf = " & Wettkampf.ID & ";", conn)
                dtStartgeld.Load(cmd.ExecuteReader)

            Catch ex As MySqlException
                MySQl_Error(Nothing, "Drucken_Verein: Me_Shown: " & ex.Message, ex.StackTrace, True)
            End Try
        End Using

        If dtMeldung.Rows.Count = 0 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Athleten gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            Close()
            Return
        End If

        If Wettkampf.International Then
            CurrList = lstGruppen
        Else
            CurrList = lstVerein
        End If

        With dgvMeldung
            Select Case Bereich

                Case "Startkarte"
                    dvMeldung = New DataView(dtMeldung.DefaultView.ToTable(False, {"idVerein", "Nachname", "Vorname", "Sex", "AK", "Gruppe"}), Nothing, "Nachname, Vorname", DataViewRowState.CurrentRows)
                    .DataSource = dvMeldung
                    .Columns("idVerein").Visible = False
                    .Columns("Gruppe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns("Sex").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns("Gruppe").SortMode = DataGridViewColumnSortMode.NotSortable
                    .Columns("Sex").SortMode = DataGridViewColumnSortMode.NotSortable
                    .Columns("AK").SortMode = DataGridViewColumnSortMode.NotSortable
                    TabControl1.TabPages.Remove(tabLänder)
                    If Wettkampf.International Then TabControl1.TabPages.Remove(tabVereine)

                Case "Quittung"
                    ' Startgelder nach AK
                    If dtStartgeld.Rows.Count > 0 Then
                        Dim dvStartgeld = New DataView(dtStartgeld, Nothing, "AK", DataViewRowState.CurrentRows)
                        For Each row As DataRow In dtMeldung.Rows
                            Dim pos = dvStartgeld.Find(If(CInt(row!idAK) > 100, 100, row!idAK))
                            If pos > -1 Then
                                row!Gebuehr = dvStartgeld(pos)!Gebuehr
                            End If
                        Next
                        dtMeldung.AcceptChanges()
                    End If
                    ' Vereinsmannschaft-Gebühr zum Verein hinzufügen
                    Dim tmp As DataView = New DataView(dtMeldung.DefaultView.ToTable(True, {"idVerein", "Vereinswertung", "Region"}), "Vereinswertung = True", Nothing, DataViewRowState.CurrentRows)
                    For Each row As DataRowView In tmp
                        dtMeldung.Rows.Add({Wettkampf.ID, 0, DBNull.Value, "", "Mannschaftswertung", "1. Mannschaft", "", 0, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value,
                                            DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, False, row!Vereinswertung,
                                            DBNull.Value, True, DBNull.Value, dtMeldefrist(0)("Gebuehr3"), row!Region, row!idVerein, "", "", ""})
                    Next
                    dvMeldung = New DataView(dtMeldung.DefaultView.ToTable(False, {"Nachname", "Vorname", "Meldedatum", "Gebuehr", "idVerein"}), Nothing, "Meldedatum DESC, Nachname, Vorname", DataViewRowState.CurrentRows)
                    .DataSource = dvMeldung
                    .Columns("Meldedatum").SortMode = DataGridViewColumnSortMode.NotSortable
                    .Columns("Gebuehr").SortMode = DataGridViewColumnSortMode.NotSortable
                    .Columns("Meldedatum").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns("Gebuehr").HeaderText = "Gebühr"
                    .Columns("Gebuehr").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .Columns("Gebuehr").DefaultCellStyle.Format = "#,##0.00 €"
                    .Columns("idVerein").Visible = False

                    ' Liste der Vereine im Verband
                    dvMannschaft = New DataView(dtLänderwertung.DefaultView.ToTable(True, {"Landesverband", "Vereinsname", "Land", "Gebuehr", "region_id"}), Nothing, "Landesverband, Vereinsname", DataViewRowState.CurrentRows)

                    ' Liste Länder
                    With lstLand
                        .DataSource = New DataView(dtLänderwertung.DefaultView.ToTable(True, {"region_id", "Landesverband"})) 'dvMannschaft
                        .DisplayMember = "Landesverband"
                        .ValueMember = "region_id"
                    End With
                    If dtLänderwertung.Rows.Count = 0 Then TabControl1.TabPages.Remove(tabLänder)
                    TabControl1.TabPages.Remove(tabGruppen)
            End Select

            .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
            .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor
            .Columns("Nachname").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("Vorname").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("Nachname").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("Vorname").SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        If Bereich.Equals("Startkarte") Then
            ' Liste Gruppen
            dvGruppen = New DataView(dtGruppen)
            With lstGruppen
                .DataSource = dvGruppen
                .DisplayMember = "Bezeichnung"
                .ValueMember = "Gruppe"
            End With
        End If
        If Bereich.Equals("Quittung") OrElse Not Wettkampf.International Then
            ' Liste Vereine
            dvVerein = New DataView(dtMeldung.DefaultView.ToTable(True, {"idVerein", "Vereinsname"}), "Vereinsname <> ''", "Vereinsname", DataViewRowState.CurrentRows)
            With lstVerein
                .SuspendLayout()
                .DataSource = dvVerein
                .DisplayMember = "Vereinsname"
                .ValueMember = "idVerein"
                dvMeldung.RowFilter = "idVerein = " + .SelectedValue.ToString
                .ResumeLayout()
            End With
        End If

        If String.IsNullOrEmpty(dvMeldung.RowFilter) Then dvMeldung.RowFilter = CurrList.Tag.ToString + " = " + CurrList.SelectedValue.ToString

        formMain.Change_MainProgressBarValue(0)

        Cursor = Cursors.Default
    End Sub

    Private Sub cboVorlage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVorlage.SelectedIndexChanged
        If cboVorlage.Focused Then Vorlage = cboVorlage.Text
    End Sub

    Private Sub chkSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkSelectAll.CheckedChanged
        With CurrList
            If chkSelectAll.Focused Then
                For i = 0 To .Items.Count - 1
                    .SetItemChecked(i, chkSelectAll.Checked)
                Next
                btnVorschau.Enabled = .CheckedItems.Count > 0
                btnPrint.Enabled = .CheckedItems.Count > 0
            End If
        End With
    End Sub
    Private Sub chkAnwesend_EnabledChanged(sender As Object, e As EventArgs) Handles chkAnwesend.EnabledChanged
        With chkAnwesend
            If .Enabled AndAlso Not IsNothing(.Tag) Then
                .Checked = CBool(.Tag)
            Else
                .Tag = .Checked
            End If
        End With
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged
        Dim Index = CInt(TabControl1.SelectedTab.Tag)

        If String.IsNullOrEmpty(Vorlage) Then
            cboVorlage.SelectedIndex = -1
        Else
            cboVorlage.Text = Vorlage
        End If

        If Index = 0 Then
            CurrList = lstVerein
            With dgvMeldung
                .DataSource = dvMeldung
                .Columns("Nachname").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns("Vorname").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns("Nachname").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("Vorname").SortMode = DataGridViewColumnSortMode.NotSortable
                If Bereich.Equals("Quittung") Then
                    .Columns("idVerein").Visible = False
                    .Columns("Meldedatum").SortMode = DataGridViewColumnSortMode.NotSortable
                    .Columns("Gebuehr").SortMode = DataGridViewColumnSortMode.NotSortable
                End If
            End With
            chkAnwesend.Enabled = True
        ElseIf Index = 1 Then
            CurrList = lstLand
            Try
                dvMannschaft.RowFilter = CurrList.Tag.ToString + " = " + CurrList.SelectedValue.ToString
            Catch ex As Exception
            End Try
            With dgvMeldung
                .DataSource = dvMannschaft
                .Columns("Land").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("Vereinsname").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("Gebuehr").Visible = False
                .Columns("Landesverband").Visible = False
                .Columns("Vereinsname").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns("region_id").Visible = False
            End With
            chkAnwesend.Enabled = False
            chkAnwesend.Checked = False
        ElseIf Index = 2 Then
            CurrList = lstGruppen
        End If

        Try
            dvMeldung.RowFilter = CurrList.Tag.ToString + " = " + CurrList.SelectedValue.ToString
        Catch ex As Exception
        End Try

        With CurrList
            btnVorschau.Enabled = .CheckedItems.Count > 0
            btnPrint.Enabled = .CheckedItems.Count > 0
            If .CheckedItems.Count > 0 AndAlso .CheckedItems.Count < .Items.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            Else
                chkSelectAll.CheckState = CType(Math.Abs(CInt(.CheckedItems.Count > 0)), CheckState)
            End If
        End With

    End Sub

    Private Sub CheckedList_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles lstVerein.ItemCheck, lstLand.ItemCheck, lstGruppen.ItemCheck
        With CurrList
            If .Focused Then

                btnVorschau.Enabled = .CheckedItems.Count - e.CurrentValue + e.NewValue > 0
                btnPrint.Enabled = .CheckedItems.Count - e.CurrentValue + e.NewValue > 0

                If .CheckedItems.Count - e.CurrentValue + e.NewValue > 0 AndAlso .CheckedItems.Count - e.CurrentValue + e.NewValue < .Items.Count Then
                    chkSelectAll.CheckState = CheckState.Indeterminate
                Else
                    chkSelectAll.CheckState = CType(Math.Abs(CInt(.CheckedItems.Count - e.CurrentValue + e.NewValue > 0)), CheckState)
                End If

            End If

            Try
                Dim lstCheck As New List(Of String)
                For Each Check As DataRowView In .CheckedItems
                    lstCheck.Add(Check(0).ToString)
                Next

                Dim r As DataRowView = CType(.Items(e.Index), DataRowView)
                If lstCheck.Contains(r(0).ToString) Then
                    lstCheck.Remove(r(0).ToString)
                Else
                    lstCheck.Add(r(0).ToString)
                End If

                dvMeldung.RowFilter = .Tag.ToString + " = " + .SelectedValue.ToString

                If .Equals(lstLand) Then dvMannschaft.RowFilter = .Tag.ToString + " = " + .SelectedValue.ToString

            Catch ex As Exception
            End Try
        End With
    End Sub
    Private Sub CheckedList_MouseDown(sender As Object, e As MouseEventArgs) Handles lstVerein.MouseClick, lstLand.MouseClick, lstGruppen.MouseClick
        If e.Button = MouseButtons.Left AndAlso e.X < 15 Then
            With CurrList
                Dim index = .IndexFromPoint(e.X, e.Y)
                If Not SelectedIndex = index Then
                    SelectedIndex = index
                    Dim checked = .GetItemChecked(index)
                    .SetItemChecked(index, Not checked)
                End If
            End With
        End If
    End Sub
    Private Sub CheckedList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstVerein.SelectedIndexChanged, lstLand.SelectedIndexChanged, lstGruppen.SelectedIndexChanged

        With CurrList
            If IsNothing(.SelectedValue) Then Return
            SelectedIndex = .SelectedIndex
            Try
                If .Equals(lstLand) Then
                    dvMannschaft.RowFilter = .Tag.ToString + " = " + .SelectedValue.ToString
                Else
                    dvMeldung.RowFilter = .Tag.ToString + " = " + .SelectedValue.ToString
                End If
            Catch ex As Exception
            End Try
        End With
    End Sub

    Private Sub dgvMeldung_SelectionChanged(sender As Object, e As EventArgs) Handles dgvMeldung.SelectionChanged
        dgvMeldung.ClearSelection()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        BackgroundWorker1.CancelAsync()
        If btnCancel.Text = "Beenden" Then Close()
    End Sub
    'Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
    '    Cursor = Cursors.WaitCursor
    '    Using report = New FastReport.Report
    '        With report
    '            Try
    '                .Load(Path.Combine(CType(cboVorlage.SelectedItem, DataRowView)(1).ToString, CType(cboVorlage.SelectedItem, DataRowView)(0).ToString + ".frx"))
    '                .Design()
    '            Catch ex As Exception
    '                Using New Centered_MessageBox(Me)
    '                    MessageBox.Show("Vorlage nicht gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '                End Using
    '                Return
    '            End Try
    '        End With
    '    End Using
    '    TabControl1.SelectedTab.Focus()
    '    Cursor = Cursors.Default
    'End Sub
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        With OpenFileDialog1
            .Filter = "Druck-Vorlagen (*.frx)|*.frx|Alle Dateien (*.*)|*.*"
            .InitialDirectory = Path.Combine(Application.StartupPath, "Report")
            If .ShowDialog() = DialogResult.Cancel Then Exit Sub
            If Not .FileName.Contains(".frx") Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Die ausgewählte Datei ist keine Druckvorlage.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
            Else
                Try
                    Dim directory = .FileName.Replace(.SafeFileName, "")
                    Vorlage = Split(.SafeFileName, ".")(0)
                    directory = Strings.Left(directory, directory.Length - 1)
                    dtVorlage.Rows.Add(Vorlage, directory)
                    cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage)
                Catch ex As Exception
                End Try
            End If
        End With
        cboVorlage.Focus()
    End Sub
    Private Sub btnTemplate_Click(sender As Object, e As EventArgs) Handles btnTemplate.Click
        If Vorlage = String.Empty AndAlso cboVorlage.Text = String.Empty Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Druckvorlage ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
            Return
        End If
        Dim dtLeer As DataTable = dtMeldung.Clone
        Dim leer As DataRow = dtLeer.NewRow
        leer.ItemArray = {" ", " ", " ", 0, 0, " ", " ", DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, False, False, DBNull.Value, DBNull.Value, DBNull.Value, " ", " ", Wettkampf.Datum}


        dtLeer.Rows.Add(leer)
        report = New FastReport.Report
        Create_Report(New DataView(dtLeer))
        action = "Show"
        Output("1 Vorlage gedruckt")
        ProgressBar.Value = 0
    End Sub
    Private Sub btnVorschau_Click(sender As Object, e As EventArgs) Handles btnVorschau.Click, btnPrint.Click, btnEdit.Click
        If Not BackgroundWorker1.IsBusy Then
            Dim msg = "Verein"
            If CurrList.Equals(lstLand) Then
                msg = "Landesverband"
            ElseIf CurrList.Equals(lstGruppen) Then
                msg = "e Gruppe"
            End If

            If CurrList.CheckedItems.Count = 0 Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Kein " & msg & " ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                Return
            End If

            If String.IsNullOrEmpty(Vorlage) AndAlso String.IsNullOrEmpty(cboVorlage.Text) Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Keine Druckvorlage ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                Return
            End If

            Cursor = Cursors.WaitCursor

            action = CType(sender, Button).Tag.ToString
            ProgressBar.Maximum = CurrList.CheckedIndices.Count
            Count = 0
            SubCount = 0
            ProgressBar.Value = Count
            lblProgress.Text = "Druck-Fortschritt"
            btnCancel.Text = "Abbrechen"
            btnVorschau.Enabled = False
            btnPrint.Enabled = False

            BackgroundWorker1.RunWorkerAsync() 'CurrList)
        End If
    End Sub

    'Private Delegate Sub BGW_DoWork(sender As Object, e As DoWorkEventArgs)
    Private Sub BackgroundWorker1_DoWork(sender As Object, e As DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        'If InvokeRequired Then
        '    Dim d As New BGW_DoWork(AddressOf BackgroundWorker1_DoWork)
        '    Invoke(d, New Object() {sender, e})
        'Else

        'Dim chkList = DirectCast(e.Argument, CheckedListBox)

        report = New FastReport.Report
        For Each Check As DataRowView In CurrList.CheckedItems
            Threading.Thread.Sleep(25)
            If BackgroundWorker1.CancellationPending Then
                e.Cancel = True
                Exit For
            Else
                Dim value = Check(0).ToString
                Dim dvDocs As DataView = Nothing
                Dim Filter = CurrList.Tag.ToString & " = " & value
                If chkAnwesend.Checked Then Filter += " AND attend = True"
                Select Case Bereich
                    Case "Startkarte"
                        dvDocs = New DataView(dtMeldung, Filter, "Nachname, Vorname", DataViewRowState.CurrentRows)
                    Case "Quittung"
                        If CurrList.Equals(lstLand) Then
                            dvDocs = New DataView(dtLänderwertung, Filter, Nothing, DataViewRowState.CurrentRows)
                        Else
                            dvDocs = New DataView(dtMeldung, Filter, Nothing, DataViewRowState.CurrentRows)
                            'If CurrList.Equals(lstVerein) Then dvDocs.Sort = "Meldedatum DESC, Nachname, Vorname"
                        End If
                End Select
                BackgroundWorker1.ReportProgress(Count)
                Create_Report(dvDocs)
            End If
        Next
        'End If
    End Sub
    Private Sub BackgroundWorker1_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        Try
            ProgressBar.Value = e.ProgressPercentage
        Catch ex As Exception
        End Try
    End Sub
    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        If e.Cancelled Then
            Message.msg = "Vorgang abgebrochen"
        ElseIf e.Cancelled = False Then
            ProgressBar.Value = CurrList.CheckedIndices.Count
            Message.msg = String.Concat(SubCount.ToString, " ", Docs, " fertig")
            Output()
        End If
        btnVorschau.Enabled = True
        btnPrint.Enabled = True
        btnCancel.Text = "&Beenden"
        Cursor = Cursors.Default
    End Sub

    Private Delegate Sub CreateReport(dvDocs As DataView)
    Private Sub Create_Report(dvDocs As DataView)
        If InvokeRequired Then
            Dim d As New CreateReport(AddressOf Create_Report)
            Invoke(d, New Object() {dvDocs})
        Else
            User.Check_MySQLDriver(Me)

            With report
                Try
                    .Load(Path.Combine(cboVorlage.SelectedValue.ToString, Vorlage + ".frx"))
                    .Dictionary.Connections(0).ConnectionString = User.FR_ConnString
                Catch ex As Exception
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Vorlage nicht gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                    Return
                End Try

                Select Case Bereich
                    Case "Startkarte"
                        If Wettkampf.Athletik Then .RegisterData(dtAthletik, "Athletik")
                        .RegisterData(dvDocs.ToTable, "Startkarte")
                        .RegisterData(Glob.dtWK, "wk")
                        SubCount += dvDocs.Count
                    Case "Quittung"
                        If CurrList.Equals(lstVerein) Then

                            .SetParameterValue("Länderwertung", False)

                            .RegisterData(dvDocs.ToTable, "Heber")
                            .RegisterData(New DataView(dtVerein, "idVerein = " & dvDocs(0)("idVerein").ToString, Nothing, DataViewRowState.CurrentRows).ToTable, "verein")

                        ElseIf CurrList.Equals(lstLand) Then

                            .SetParameterValue("Länderwertung", True)

                            .RegisterData(dvDocs.ToTable(True, {"region_id", "Landesverband", "Land", "Gebuehr"}), "Länderwertung")
                            .RegisterData(New DataView(dtRegion, "region_id = " & dvDocs(0)("region_id").ToString, Nothing, DataViewRowState.CurrentRows).ToTable, "region")

                        End If
                        SubCount += 1
                End Select

                .SetParameterValue("IsPrintDateToday", Not chkDatum.Checked)
                .RegisterData(Glob.dtWK, "wk")

                Try
                    Dim s = New FastReport.EnvironmentSettings
                    s.ReportSettings.ShowProgress = False
                    Count += 1
                    .Prepare(Count > 0)
                Catch ex As Exception
                    action = "cancelled"
                    LogMessage("Drucken_Startkarten (Vorlage: " & Vorlage & "): Create_Report: " & Bereich & ": " & ex.Message, ex.StackTrace)
                End Try
            End With
        End If
    End Sub
    Private Sub Output(Optional msg As String = "")
        If InvokeRequired Then
            Dim d As New SetMsg(AddressOf Output)
            Invoke(d, New Object() {msg})
        Else
            If action.Equals("cancelled") Then
                Message.msg = "Abbruch - Fehler im Formular"
                Return
            End If
            If Not String.IsNullOrEmpty(msg) Then
                Message.msg = msg
            Else
                Message.msg = String.Concat(SubCount.ToString, " ", Docs, " für ", Count.ToString, " ", If(Count > 1, TabControl1.SelectedTab.Text, TabControl1.SelectedTab.Text.Substring(0, TabControl1.SelectedTab.Text.Length - 1)), " gedruckt") 'If(CurrList.Equals(lstVerein), " Verein" & If(Count > 1, "e", "").ToString, If(Count > 1, " Verbände", " Verband")), " gedruckt")
            End If
            Try
                If action.Equals("Show") Then
                    report.ShowPrepared()
                ElseIf action.Equals("Print") Then
                    report.PrintPrepared()
                ElseIf action.Equals("Edit") Then
                    report.Design()
                End If
            Catch ex As Exception
                LogMessage("Drucken_Verein: Output: " & Bereich & ": " & ex.Message, ex.StackTrace)
            End Try
        End If
    End Sub
    Private Sub ProgressMsg(msg As String)
        If InvokeRequired Then
            Dim d As New SetMsg(AddressOf ProgressMsg)
            Invoke(d, New Object() {msg})
        Else
            lblProgress.Text = msg
        End If
    End Sub
    Private Function Get_Date(dvDocs As DataView) As Date
        Dim g As Integer
        Try
            Dim x As List(Of Integer) = dvDocs.ToTable.AsEnumerable().Select(Function(s) CInt(s("Gruppe"))).ToList()
            x.Sort()
            For Each i In x
                If i > 0 Then
                    g = i
                    Exit For
                End If
            Next
        Catch ex As Exception
        End Try
        Dim d = dtPrint.Select("Gruppe = " & g.ToString)

        If Not chkDatum.Checked Then Return Now
        If d.Count = 0 Then Return Wettkampf.Datum
        Return CDate(d(0)("Druckdatum"))

    End Function


End Class