﻿Module ScreenScaling

    'Public Event FontSize_Changed(Grid As DataGridView, FontSize As Single)

    'Public Screens As New SortedList(Of Integer, KeyValuePair(Of Form, Integer))

    Class ScreenScale
        Implements IDisposable

        Private X, Y As Double      ' Vergrößerungsfaktor der Screens
        Private Faktor As Double    ' Vergrößerungsfaktor der Fonts in dgv's

        'Public Sub Form_Size(f As Form, n As Integer)
        '    Dim mon As Integer

        '    If Screens.ContainsKey(n) Then
        '        mon = n
        '    Else
        '        mon = GetDeviceNumber(Screen.FromControl(f)) - 1
        '    End If

        '    If f.FormBorderStyle = FormBorderStyle.Fixed3D Then
        '        f.FormBorderStyle = FormBorderStyle.None
        '        SetWindow(mon, 1, f)
        '    Else
        '        f.FormBorderStyle = FormBorderStyle.Fixed3D
        '        SetWindow(mon, 1, f, f.MinimumSize.Width, f.MinimumSize.Height, 2)
        '    End If
        'End Sub

        Public Function GetDeviceNumber(MyScreen As Screen) As Integer
            'Return CInt(Int(StrReverse(CStr(Val(StrReverse(MyScreen.DeviceName))))))
            'Return CInt(StrReverse(Val(StrReverse(MyScreen.DeviceName)).ToString))
            Return CInt(Split(MyScreen.DeviceName, "DISPLAY")(1))
        End Function

        Public Sub SetWindow(Monitor As Integer, myFaktor As Double, myForm As Form,
                             Optional myFormLeft As Integer = 0, Optional myFormTop As Integer = 0, Optional myFormWidth As Integer = 0, Optional myFormHeight As Integer = 0,
                             Optional myPos As Integer = 2, Optional Zoom As Single = 1)
            'MyPos: 1 = links / 2 = zentriert / 3 = rechts
            Faktor = myFaktor

            Dim sourceX As Integer
            Dim sourceY As Integer
            Dim sourceWidth As Integer
            Dim sourceHeight As Integer

            Try
                sourceX = If(myFormLeft > 0, myFormLeft, Screen.AllScreens(Monitor).Bounds.Left)
                sourceY = If(myFormTop > 0, myFormTop, Screen.AllScreens(Monitor).Bounds.Top)
                sourceWidth = If(myFormWidth > 0, myFormWidth, Screen.AllScreens(Monitor).Bounds.Width)
                sourceHeight = If(myFormHeight > 0, myFormHeight, Screen.AllScreens(Monitor).Bounds.Height)
            Catch ex As Exception
                sourceX = myFormLeft
                sourceY = myFormTop
                sourceWidth = myFormWidth
                sourceHeight = myFormHeight
            End Try

            With myForm
                Dim clientWidth As Integer = .MinimumSize.Width
                Dim clientHeight As Integer = .MinimumSize.Height
                'Form skalieren
                .SetBounds(sourceX, sourceY, sourceWidth, sourceHeight)
                'Skalierung der Controls berechnen
                X = sourceWidth / clientWidth * Zoom
                Y = sourceHeight / clientHeight * Zoom
                'Controls skalieren 
                For Each ctl As Control In .Controls
                    CtlLoop(ctl)
                Next ctl
                'Form auf Screen positionieren
                Select Case myPos
                    Case 1 'links oben
                        .Left = sourceX
                        .Top = sourceY
                    Case 2 'zentriert
                        .Left = sourceX + (CInt((sourceWidth - .Width) / 2))
                        .Top = sourceY + (CInt((sourceHeight - .Height) / 2))
                End Select
            End With

        End Sub

        Public Sub CtlLoop(ByVal Ctl As Control)
            ControlResize(Ctl)
            If Ctl.HasChildren = True Then
                ' rekursiv vorhandene UnterControls durchlaufen
                For Each c As Control In Ctl.Controls
                    ControlResize(c)
                Next
            End If
        End Sub

        Public Sub ControlResize(ByVal Ctl As Control)
            With Ctl
                If TypeOf Ctl Is DataGridView Then
                    If Not TypeOf Ctl.Parent Is Panel Then
                        With CType(Ctl, DataGridView)
                            .DefaultCellStyle.Font = New Font(.Font.FontFamily, .DefaultCellStyle.Font.Size * CSng(X * Faktor), FontStyle.Regular, GraphicsUnit.Point)
                            'Debug.WriteLine(.DefaultCellStyle.Font)
                            .ColumnHeadersDefaultCellStyle.Font = New Font(.Font.FontFamily, .ColumnHeadersDefaultCellStyle.Font.Size * CSng(X), FontStyle.Regular, GraphicsUnit.Point)
                            'If Not IsNothing(.Tag) AndAlso IsNumeric(.Tag) Then RaiseEvent FontSize_Changed(CType(Ctl, DataGridView), CSng(.Tag) * CSng(X * Faktor))
                            'For Each col As DataGridViewColumn In .Columns
                            '    col.MinimumWidth = CInt(col.MinimumWidth * X)
                            'Next
                        End With
                    End If
                ElseIf Ctl.Name <> "rtfNotiz" Then
                    Try
                        .Font = New Font(.Font.FontFamily, .Font.Size * CSng(X), .Font.Style, GraphicsUnit.Point)
                    Catch ex As Exception
                    End Try
                End If
                Try
                    .SetBounds(CInt(.Left * X), CInt(.Top * Y), CInt(.Width * X), CInt(.Height * Y))
                Catch ex As Exception
                End Try
            End With
        End Sub

#Region "IDisposable Support"
        Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
                End If

                ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
                ' TODO: große Felder auf Null setzen.
            End If
            disposedValue = True
        End Sub

        ' TODO: Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
        'Protected Overrides Sub Finalize()
        '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
            Dispose(True)
            ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
            ' GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Module
