﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmWettkampf
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label9 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Bezeichnung_3Label As System.Windows.Forms.Label
        Dim Bezeichnung_2Label As System.Windows.Forms.Label
        Dim Bezeichnung_1Label As System.Windows.Forms.Label
        Dim OrtLabel As System.Windows.Forms.Label
        Dim VeranstalterLabel As System.Windows.Forms.Label
        Dim DatumLabel As System.Windows.Forms.Label
        Dim ModusLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWettkampf))
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle43 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle44 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle45 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle46 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle47 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle48 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle49 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle50 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle51 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle52 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle53 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle59 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle60 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle54 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle55 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle56 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle57 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle58 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle61 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle65 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle62 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle63 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle64 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle66 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle67 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle68 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle69 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle70 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle71 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle72 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboKR = New System.Windows.Forms.ComboBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuNeu = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLöschen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMeldungen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKonflikte = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMauszeiger = New System.Windows.Forms.ToolStripMenuItem()
        Me.chkBigdisc = New System.Windows.Forms.CheckBox()
        Me.txtZK_m = New System.Windows.Forms.TextBox()
        Me.txtZK_w = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.nudSteigerung2 = New System.Windows.Forms.NumericUpDown()
        Me.nudSteigerung1 = New System.Windows.Forms.NumericUpDown()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.chkFlagge = New System.Windows.Forms.CheckBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chk5kg = New System.Windows.Forms.CheckBox()
        Me.chk7kg = New System.Windows.Forms.CheckBox()
        Me.optAK = New System.Windows.Forms.RadioButton()
        Me.optSex = New System.Windows.Forms.RadioButton()
        Me.txtHantelstange = New System.Windows.Forms.TextBox()
        Me.nudVmin = New System.Windows.Forms.NumericUpDown()
        Me.nudMmin = New System.Windows.Forms.NumericUpDown()
        Me.nudLmin = New System.Windows.Forms.NumericUpDown()
        Me.nudMmax = New System.Windows.Forms.NumericUpDown()
        Me.nudVmax = New System.Windows.Forms.NumericUpDown()
        Me.nudLmax = New System.Windows.Forms.NumericUpDown()
        Me.nudMwert = New System.Windows.Forms.NumericUpDown()
        Me.nudVwert = New System.Windows.Forms.NumericUpDown()
        Me.nudLwert = New System.Windows.Forms.NumericUpDown()
        Me.chkMadd = New System.Windows.Forms.CheckBox()
        Me.chkMsex = New System.Windows.Forms.CheckBox()
        Me.chkLiga = New System.Windows.Forms.CheckBox()
        Me.chkLadd = New System.Windows.Forms.CheckBox()
        Me.chkVadd = New System.Windows.Forms.CheckBox()
        Me.chkLsex = New System.Windows.Forms.CheckBox()
        Me.chkVsex = New System.Windows.Forms.CheckBox()
        Me.chkLand = New System.Windows.Forms.CheckBox()
        Me.chkVerein = New System.Windows.Forms.CheckBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtR_w = New System.Windows.Forms.TextBox()
        Me.txtR_m = New System.Windows.Forms.TextBox()
        Me.txtS_w = New System.Windows.Forms.TextBox()
        Me.txtS_m = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.chkEqual_w = New System.Windows.Forms.CheckBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblG_w4 = New System.Windows.Forms.Label()
        Me.lblG_w3 = New System.Windows.Forms.Label()
        Me.lblG_m4 = New System.Windows.Forms.Label()
        Me.lblG_w2 = New System.Windows.Forms.Label()
        Me.lblG_m3 = New System.Windows.Forms.Label()
        Me.lblG_w0 = New System.Windows.Forms.Label()
        Me.lblG_w1 = New System.Windows.Forms.Label()
        Me.lblG_m2 = New System.Windows.Forms.Label()
        Me.lblG_m0 = New System.Windows.Forms.Label()
        Me.lblG_m1 = New System.Windows.Forms.Label()
        Me.chkEqual_m = New System.Windows.Forms.CheckBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.nudEqual_w = New System.Windows.Forms.NumericUpDown()
        Me.nudG_w4 = New System.Windows.Forms.NumericUpDown()
        Me.nudEqual_m = New System.Windows.Forms.NumericUpDown()
        Me.nudG_w3 = New System.Windows.Forms.NumericUpDown()
        Me.nudG_m4 = New System.Windows.Forms.NumericUpDown()
        Me.nudG_w2 = New System.Windows.Forms.NumericUpDown()
        Me.nudG_m3 = New System.Windows.Forms.NumericUpDown()
        Me.nudG_w1 = New System.Windows.Forms.NumericUpDown()
        Me.nudG_m2 = New System.Windows.Forms.NumericUpDown()
        Me.nudG_w0 = New System.Windows.Forms.NumericUpDown()
        Me.nudG_m1 = New System.Windows.Forms.NumericUpDown()
        Me.nudG_w = New System.Windows.Forms.NumericUpDown()
        Me.nudG_m = New System.Windows.Forms.NumericUpDown()
        Me.nudG_m0 = New System.Windows.Forms.NumericUpDown()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.chkLosnummer = New System.Windows.Forms.CheckBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.nudMinAge = New System.Windows.Forms.NumericUpDown()
        Me.chkInternational = New System.Windows.Forms.CheckBox()
        Me.btnDiscColor = New System.Windows.Forms.Button()
        Me.chkRegel20 = New System.Windows.Forms.CheckBox()
        Me.tabCollection = New System.Windows.Forms.TabControl()
        Me.tabAthletik = New System.Windows.Forms.TabPage()
        Me.btnDefault = New System.Windows.Forms.Button()
        Me.dgvAthletik = New System.Windows.Forms.DataGridView()
        Me.Checked = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Bezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AKs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblBank = New System.Windows.Forms.Label()
        Me.dgvDefaults = New System.Windows.Forms.DataGridView()
        Me.D_Sex = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ak17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tabAusnahme = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkVersucheSortieren = New System.Windows.Forms.CheckBox()
        Me.chkWeitererVersuch = New System.Windows.Forms.CheckBox()
        Me.grpStossen = New System.Windows.Forms.GroupBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.nudRestrict_S_AK = New System.Windows.Forms.NumericUpDown()
        Me.chkRestrict_S = New System.Windows.Forms.CheckBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.nudRestrict_S_V = New System.Windows.Forms.NumericUpDown()
        Me.grpReissen = New System.Windows.Forms.GroupBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.nudRestrict_R_V = New System.Windows.Forms.NumericUpDown()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.nudRestrict_R_AK = New System.Windows.Forms.NumericUpDown()
        Me.chkRestrict_R = New System.Windows.Forms.CheckBox()
        Me.tabGewichtsgruppen = New System.Windows.Forms.TabPage()
        Me.tabAltersgruppen = New System.Windows.Forms.TabPage()
        Me.optAG_w = New System.Windows.Forms.RadioButton()
        Me.optAG_m = New System.Windows.Forms.RadioButton()
        Me.dgvAKs = New System.Windows.Forms.DataGridView()
        Me.Check = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.AK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvAGs = New System.Windows.Forms.DataGridView()
        Me.Sex = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jahrgang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnAG_Remove = New System.Windows.Forms.Button()
        Me.btnAG_Add = New System.Windows.Forms.Button()
        Me.tabMannschaft = New System.Windows.Forms.TabPage()
        Me.pnlLiga = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.pnlLand = New System.Windows.Forms.Panel()
        Me.lblLadd = New System.Windows.Forms.Label()
        Me.lblLsex = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.pnlVerein = New System.Windows.Forms.Panel()
        Me.lblVrel = New System.Windows.Forms.Label()
        Me.nudVrel = New System.Windows.Forms.NumericUpDown()
        Me.lblVadd = New System.Windows.Forms.Label()
        Me.lblVsex = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.tabPlatzierung = New System.Windows.Forms.TabPage()
        Me.chkUseAgeGroups = New System.Windows.Forms.CheckBox()
        Me.chkPlatzierung2 = New System.Windows.Forms.CheckBox()
        Me.grpPlatzierung2 = New System.Windows.Forms.GroupBox()
        Me.grpWertung2 = New System.Windows.Forms.GroupBox()
        Me.cboWertung2 = New System.Windows.Forms.ComboBox()
        Me.optWettkampf2 = New System.Windows.Forms.RadioButton()
        Me.optGewichtsklassen2 = New System.Windows.Forms.RadioButton()
        Me.optAltersklassen2 = New System.Windows.Forms.RadioButton()
        Me.optWeiblich2 = New System.Windows.Forms.RadioButton()
        Me.optGruppen2 = New System.Windows.Forms.RadioButton()
        Me.dgvWertung2 = New System.Windows.Forms.DataGridView()
        Me.Altersklasse2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Wertung2 = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.Gruppe2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grpPlatzierung = New System.Windows.Forms.GroupBox()
        Me.optAltersklassen = New System.Windows.Forms.RadioButton()
        Me.optGruppen = New System.Windows.Forms.RadioButton()
        Me.optWettkampf = New System.Windows.Forms.RadioButton()
        Me.optGewichtsklassen = New System.Windows.Forms.RadioButton()
        Me.tabWertung = New System.Windows.Forms.TabPage()
        Me.grpFaktor = New System.Windows.Forms.GroupBox()
        Me.nudFaktor_w = New System.Windows.Forms.NumericUpDown()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.nudFaktor_m = New System.Windows.Forms.NumericUpDown()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.chkSameAK = New System.Windows.Forms.CheckBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.cboSameAK = New System.Windows.Forms.ComboBox()
        Me.tabMulti = New System.Windows.Forms.TabPage()
        Me.dgvMulti = New System.Windows.Forms.DataGridView()
        Me.Multi = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Primär = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bez1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Datum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.dgvBigDisc = New System.Windows.Forms.DataGridView()
        Me.Available = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Gewicht = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Farbe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Dicke = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.lstColors = New System.Windows.Forms.ListBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkFinale = New System.Windows.Forms.CheckBox()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.txtKurz = New System.Windows.Forms.TextBox()
        Me.chkGebührNachAk = New System.Windows.Forms.CheckBox()
        Me.pnlVereine = New System.Windows.Forms.Panel()
        Me.txtVeranstalter = New System.Windows.Forms.TextBox()
        Me.lstVereine = New System.Windows.Forms.ListBox()
        Me.dgvGebühr = New System.Windows.Forms.DataGridView()
        Me.Altersklasse = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gebuehr = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nachmeldung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GebuehrOL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NachmeldungOL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.grpMannschaft = New System.Windows.Forms.GroupBox()
        Me.pnlMannschaft = New System.Windows.Forms.Panel()
        Me.pnl0 = New System.Windows.Forms.Panel()
        Me.pnl2 = New System.Windows.Forms.Panel()
        Me.pnl1 = New System.Windows.Forms.Panel()
        Me.txt2 = New System.Windows.Forms.TextBox()
        Me.txt1 = New System.Windows.Forms.TextBox()
        Me.txt0 = New System.Windows.Forms.TextBox()
        Me.lbl2 = New System.Windows.Forms.Label()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.lbl0 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dpkDatumBis = New System.Windows.Forms.DateTimePicker()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtStartgeld_M_Land = New System.Windows.Forms.TextBox()
        Me.txtStartgeld_M_Verein = New System.Windows.Forms.TextBox()
        Me.txtStartgeld_M_Liga = New System.Windows.Forms.TextBox()
        Me.dpkMeldeschlussNachmeldung = New System.Windows.Forms.DateTimePicker()
        Me.dpkMeldeschluss = New System.Windows.Forms.DateTimePicker()
        Me.btnModus = New System.Windows.Forms.Button()
        Me.cboOrt = New System.Windows.Forms.ComboBox()
        Me.cboBezeichnung3 = New System.Windows.Forms.ComboBox()
        Me.cboBezeichnung2 = New System.Windows.Forms.ComboBox()
        Me.cboBezeichnung1 = New System.Windows.Forms.ComboBox()
        Me.dpkDatum = New System.Windows.Forms.DateTimePicker()
        Me.cboModus = New System.Windows.Forms.ComboBox()
        Me.chkIgnoreSex = New System.Windows.Forms.CheckBox()
        Label9 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Bezeichnung_3Label = New System.Windows.Forms.Label()
        Bezeichnung_2Label = New System.Windows.Forms.Label()
        Bezeichnung_1Label = New System.Windows.Forms.Label()
        OrtLabel = New System.Windows.Forms.Label()
        VeranstalterLabel = New System.Windows.Forms.Label()
        DatumLabel = New System.Windows.Forms.Label()
        ModusLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.nudSteigerung2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudSteigerung1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.nudVmin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMmin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLmin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMmax, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudVmax, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLmax, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMwert, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudVwert, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLwert, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.nudEqual_w, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_w4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudEqual_m, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_w3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_w2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_w1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_w0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_w, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudG_m0, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        CType(Me.nudMinAge, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabCollection.SuspendLayout()
        Me.tabAthletik.SuspendLayout()
        CType(Me.dgvAthletik, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDefaults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabAusnahme.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.grpStossen.SuspendLayout()
        CType(Me.nudRestrict_S_AK, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudRestrict_S_V, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpReissen.SuspendLayout()
        CType(Me.nudRestrict_R_V, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudRestrict_R_AK, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGewichtsgruppen.SuspendLayout()
        Me.tabAltersgruppen.SuspendLayout()
        CType(Me.dgvAKs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAGs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMannschaft.SuspendLayout()
        Me.pnlLiga.SuspendLayout()
        Me.pnlLand.SuspendLayout()
        Me.pnlVerein.SuspendLayout()
        CType(Me.nudVrel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabPlatzierung.SuspendLayout()
        Me.grpPlatzierung2.SuspendLayout()
        Me.grpWertung2.SuspendLayout()
        CType(Me.dgvWertung2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPlatzierung.SuspendLayout()
        Me.tabWertung.SuspendLayout()
        Me.grpFaktor.SuspendLayout()
        CType(Me.nudFaktor_w, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFaktor_m, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.tabMulti.SuspendLayout()
        CType(Me.dgvMulti, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBigDisc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvGebühr, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpMannschaft.SuspendLayout()
        Me.pnlMannschaft.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Location = New System.Drawing.Point(6, 286)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(99, 17)
        Label9.TabIndex = 54
        Label9.Text = "Nachmeldeschluss"
        Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        Label9.UseCompatibleTextRendering = True
        '
        'Label8
        '
        Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Label8.AutoSize = True
        Label8.Location = New System.Drawing.Point(32, 257)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(73, 17)
        Label8.TabIndex = 2024
        Label8.Text = "Meldeschluss"
        Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        Label8.UseCompatibleTextRendering = True
        '
        'Bezeichnung_3Label
        '
        Bezeichnung_3Label.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Bezeichnung_3Label.AutoSize = True
        Bezeichnung_3Label.Location = New System.Drawing.Point(42, 225)
        Bezeichnung_3Label.Name = "Bezeichnung_3Label"
        Bezeichnung_3Label.Size = New System.Drawing.Size(61, 17)
        Bezeichnung_3Label.TabIndex = 2022
        Bezeichnung_3Label.Text = "Länder-WK"
        Bezeichnung_3Label.TextAlign = System.Drawing.ContentAlignment.TopRight
        Bezeichnung_3Label.UseCompatibleTextRendering = True
        '
        'Bezeichnung_2Label
        '
        Bezeichnung_2Label.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Bezeichnung_2Label.AutoSize = True
        Bezeichnung_2Label.Location = New System.Drawing.Point(39, 194)
        Bezeichnung_2Label.Name = "Bezeichnung_2Label"
        Bezeichnung_2Label.Size = New System.Drawing.Size(65, 17)
        Bezeichnung_2Label.TabIndex = 2020
        Bezeichnung_2Label.Text = "Vereins-WK"
        Bezeichnung_2Label.TextAlign = System.Drawing.ContentAlignment.TopRight
        Bezeichnung_2Label.UseCompatibleTextRendering = True
        '
        'Bezeichnung_1Label
        '
        Bezeichnung_1Label.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Bezeichnung_1Label.AutoSize = True
        Bezeichnung_1Label.Location = New System.Drawing.Point(46, 163)
        Bezeichnung_1Label.Name = "Bezeichnung_1Label"
        Bezeichnung_1Label.Size = New System.Drawing.Size(56, 17)
        Bezeichnung_1Label.TabIndex = 2018
        Bezeichnung_1Label.Text = "WK-Name"
        Bezeichnung_1Label.TextAlign = System.Drawing.ContentAlignment.TopRight
        Bezeichnung_1Label.UseCompatibleTextRendering = True
        '
        'OrtLabel
        '
        OrtLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        OrtLabel.AutoSize = True
        OrtLabel.Location = New System.Drawing.Point(9, 133)
        OrtLabel.Name = "OrtLabel"
        OrtLabel.Size = New System.Drawing.Size(93, 17)
        OrtLabel.TabIndex = 2016
        OrtLabel.Text = "Veranstaltungsort"
        OrtLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        OrtLabel.UseCompatibleTextRendering = True
        '
        'VeranstalterLabel
        '
        VeranstalterLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        VeranstalterLabel.AutoSize = True
        VeranstalterLabel.Location = New System.Drawing.Point(38, 40)
        VeranstalterLabel.Name = "VeranstalterLabel"
        VeranstalterLabel.Size = New System.Drawing.Size(65, 17)
        VeranstalterLabel.TabIndex = 2004
        VeranstalterLabel.Text = "Veranstalter"
        VeranstalterLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        VeranstalterLabel.UseCompatibleTextRendering = True
        '
        'DatumLabel
        '
        DatumLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DatumLabel.AutoSize = True
        DatumLabel.Location = New System.Drawing.Point(63, 102)
        DatumLabel.Name = "DatumLabel"
        DatumLabel.Size = New System.Drawing.Size(38, 17)
        DatumLabel.TabIndex = 2010
        DatumLabel.Text = "Datum"
        DatumLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        DatumLabel.UseCompatibleTextRendering = True
        '
        'ModusLabel
        '
        ModusLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        ModusLabel.AutoSize = True
        ModusLabel.Location = New System.Drawing.Point(62, 70)
        ModusLabel.Name = "ModusLabel"
        ModusLabel.Size = New System.Drawing.Size(38, 17)
        ModusLabel.TabIndex = 2007
        ModusLabel.Text = "Modus"
        ModusLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        ModusLabel.UseCompatibleTextRendering = True
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(55, 314)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(50, 17)
        Label2.TabIndex = 2039
        Label2.Text = "Startgeld"
        Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        Label2.UseCompatibleTextRendering = True
        '
        'txtID
        '
        Me.txtID.Enabled = False
        Me.txtID.Location = New System.Drawing.Point(996, 1)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(62, 20)
        Me.txtID.TabIndex = 21
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(17, 187)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 13)
        Me.Label3.TabIndex = 252
        Me.Label3.Text = "Kampfrichter"
        '
        'cboKR
        '
        Me.cboKR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboKR.DropDownWidth = 40
        Me.cboKR.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboKR.Items.AddRange(New Object() {"1", "3", "1, 3", "3, 1"})
        Me.cboKR.Location = New System.Drawing.Point(89, 183)
        Me.cboKR.Name = "cboKR"
        Me.cboKR.Size = New System.Drawing.Size(52, 21)
        Me.cboKR.TabIndex = 253
        Me.ToolTip1.SetToolTip(Me.cboKR, "Bohle 1 [, Bohle 2]" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "KR-Anlage prüft angesteckte Pads")
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuOptionen, Me.mnuExtras})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(4, 1, 0, 1)
        Me.MenuStrip1.Size = New System.Drawing.Size(1060, 24)
        Me.MenuStrip1.TabIndex = 114
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNeu, Me.mnuSpeichern, Me.mnuLöschen, Me.ToolStripMenuItem1, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 22)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuNeu
        '
        Me.mnuNeu.Name = "mnuNeu"
        Me.mnuNeu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuNeu.Size = New System.Drawing.Size(175, 22)
        Me.mnuNeu.Text = "&Neu"
        Me.mnuNeu.Visible = False
        '
        'mnuSpeichern
        '
        Me.mnuSpeichern.Name = "mnuSpeichern"
        Me.mnuSpeichern.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSpeichern.Size = New System.Drawing.Size(175, 22)
        Me.mnuSpeichern.Text = "&Speichern"
        '
        'mnuLöschen
        '
        Me.mnuLöschen.Name = "mnuLöschen"
        Me.mnuLöschen.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Delete), System.Windows.Forms.Keys)
        Me.mnuLöschen.Size = New System.Drawing.Size(175, 22)
        Me.mnuLöschen.Text = "&Löschen"
        Me.mnuLöschen.Visible = False
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(172, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(175, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMeldungen, Me.mnuKonflikte})
        Me.mnuOptionen.Name = "mnuOptionen"
        Me.mnuOptionen.Size = New System.Drawing.Size(69, 22)
        Me.mnuOptionen.Text = "&Optionen"
        '
        'mnuMeldungen
        '
        Me.mnuMeldungen.CheckOnClick = True
        Me.mnuMeldungen.Name = "mnuMeldungen"
        Me.mnuMeldungen.Size = New System.Drawing.Size(210, 22)
        Me.mnuMeldungen.Text = "&Meldungen ausblenden"
        '
        'mnuKonflikte
        '
        Me.mnuKonflikte.Checked = True
        Me.mnuKonflikte.CheckOnClick = True
        Me.mnuKonflikte.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuKonflikte.Name = "mnuKonflikte"
        Me.mnuKonflikte.Size = New System.Drawing.Size(210, 22)
        Me.mnuKonflikte.Text = "bei &Konflikten nachfragen"
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMauszeiger})
        Me.mnuExtras.Name = "mnuExtras"
        Me.mnuExtras.Size = New System.Drawing.Size(50, 22)
        Me.mnuExtras.Text = "&Extras"
        '
        'mnuMauszeiger
        '
        Me.mnuMauszeiger.Name = "mnuMauszeiger"
        Me.mnuMauszeiger.ShortcutKeys = System.Windows.Forms.Keys.F4
        Me.mnuMauszeiger.Size = New System.Drawing.Size(186, 22)
        Me.mnuMauszeiger.Text = "&Mauszeiger holen"
        '
        'chkBigdisc
        '
        Me.chkBigdisc.AutoSize = True
        Me.chkBigdisc.Checked = True
        Me.chkBigdisc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBigdisc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkBigdisc.Location = New System.Drawing.Point(17, 107)
        Me.chkBigdisc.Name = "chkBigdisc"
        Me.chkBigdisc.Size = New System.Drawing.Size(101, 17)
        Me.chkBigdisc.TabIndex = 206
        Me.chkBigdisc.Text = "große Scheiben"
        Me.chkBigdisc.UseVisualStyleBackColor = True
        '
        'txtZK_m
        '
        Me.txtZK_m.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtZK_m.Location = New System.Drawing.Point(65, 47)
        Me.txtZK_m.Name = "txtZK_m"
        Me.txtZK_m.Size = New System.Drawing.Size(39, 20)
        Me.txtZK_m.TabIndex = 304
        Me.txtZK_m.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtZK_w
        '
        Me.txtZK_w.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtZK_w.Location = New System.Drawing.Point(118, 47)
        Me.txtZK_w.Name = "txtZK_w"
        Me.txtZK_w.Size = New System.Drawing.Size(39, 20)
        Me.txtZK_w.TabIndex = 305
        Me.txtZK_w.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label14.Location = New System.Drawing.Point(12, 79)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(41, 13)
        Me.Label14.TabIndex = 306
        Me.Label14.Text = "Reißen"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label13.Location = New System.Drawing.Point(12, 51)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(21, 13)
        Me.Label13.TabIndex = 303
        Me.Label13.Text = "ZK"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.nudSteigerung2)
        Me.GroupBox1.Controls.Add(Me.nudSteigerung1)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Firebrick
        Me.GroupBox1.Location = New System.Drawing.Point(709, 196)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(330, 53)
        Me.GroupBox1.TabIndex = 200
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Steigerung"
        '
        'nudSteigerung2
        '
        Me.nudSteigerung2.BackColor = System.Drawing.SystemColors.Window
        Me.nudSteigerung2.Location = New System.Drawing.Point(265, 23)
        Me.nudSteigerung2.Maximum = New Decimal(New Integer() {9, 0, 0, 0})
        Me.nudSteigerung2.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudSteigerung2.Name = "nudSteigerung2"
        Me.nudSteigerung2.ReadOnly = True
        Me.nudSteigerung2.Size = New System.Drawing.Size(32, 20)
        Me.nudSteigerung2.TabIndex = 4
        Me.nudSteigerung2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudSteigerung2.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'nudSteigerung1
        '
        Me.nudSteigerung1.BackColor = System.Drawing.SystemColors.Window
        Me.nudSteigerung1.Location = New System.Drawing.Point(101, 23)
        Me.nudSteigerung1.Maximum = New Decimal(New Integer() {9, 0, 0, 0})
        Me.nudSteigerung1.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudSteigerung1.Name = "nudSteigerung1"
        Me.nudSteigerung1.ReadOnly = True
        Me.nudSteigerung1.Size = New System.Drawing.Size(32, 20)
        Me.nudSteigerung1.TabIndex = 2
        Me.nudSteigerung1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudSteigerung1.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label15.Location = New System.Drawing.Point(14, 26)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(142, 13)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "nach 1. Versuch               kg"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label6.Location = New System.Drawing.Point(178, 25)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(142, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "nach 2. Versuch               kg"
        '
        'chkFlagge
        '
        Me.chkFlagge.AutoSize = True
        Me.chkFlagge.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkFlagge.Location = New System.Drawing.Point(17, 85)
        Me.chkFlagge.Name = "chkFlagge"
        Me.chkFlagge.Size = New System.Drawing.Size(104, 17)
        Me.chkFlagge.TabIndex = 205
        Me.chkFlagge.Text = "Flagge anzeigen"
        Me.chkFlagge.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Enabled = False
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSave.Location = New System.Drawing.Point(843, 535)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(93, 25)
        Me.btnSave.TabIndex = 2000
        Me.btnSave.Text = "Speichern"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.optAK)
        Me.GroupBox2.Controls.Add(Me.optSex)
        Me.GroupBox2.Controls.Add(Me.txtHantelstange)
        Me.GroupBox2.ForeColor = System.Drawing.Color.Firebrick
        Me.GroupBox2.Location = New System.Drawing.Point(903, 38)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(136, 144)
        Me.GroupBox2.TabIndex = 400
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Hantelstange"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chk5kg)
        Me.GroupBox3.Controls.Add(Me.chk7kg)
        Me.GroupBox3.ForeColor = System.Drawing.Color.MediumBlue
        Me.GroupBox3.Location = New System.Drawing.Point(0, 73)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(136, 72)
        Me.GroupBox3.TabIndex = 450
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "  optional  "
        '
        'chk5kg
        '
        Me.chk5kg.AutoSize = True
        Me.chk5kg.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chk5kg.Location = New System.Drawing.Point(18, 44)
        Me.chk5kg.Name = "chk5kg"
        Me.chk5kg.Size = New System.Drawing.Size(93, 17)
        Me.chk5kg.TabIndex = 452
        Me.chk5kg.Tag = "x"
        Me.chk5kg.Text = " 5 kg - Stange"
        Me.chk5kg.UseVisualStyleBackColor = True
        '
        'chk7kg
        '
        Me.chk7kg.AutoSize = True
        Me.chk7kg.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chk7kg.Location = New System.Drawing.Point(18, 22)
        Me.chk7kg.Name = "chk7kg"
        Me.chk7kg.Size = New System.Drawing.Size(93, 17)
        Me.chk7kg.TabIndex = 451
        Me.chk7kg.Tag = "k"
        Me.chk7kg.Text = " 7 kg - Stange"
        Me.chk7kg.UseVisualStyleBackColor = True
        '
        'optAK
        '
        Me.optAK.AutoSize = True
        Me.optAK.ForeColor = System.Drawing.SystemColors.WindowText
        Me.optAK.Location = New System.Drawing.Point(17, 46)
        Me.optAK.Name = "optAK"
        Me.optAK.Size = New System.Drawing.Size(108, 17)
        Me.optAK.TabIndex = 402
        Me.optAK.Tag = "a"
        Me.optAK.Text = "nach Altersklasse"
        Me.optAK.UseVisualStyleBackColor = True
        '
        'optSex
        '
        Me.optSex.AutoSize = True
        Me.optSex.Checked = True
        Me.optSex.ForeColor = System.Drawing.SystemColors.WindowText
        Me.optSex.Location = New System.Drawing.Point(17, 25)
        Me.optSex.Name = "optSex"
        Me.optSex.Size = New System.Drawing.Size(106, 17)
        Me.optSex.TabIndex = 401
        Me.optSex.TabStop = True
        Me.optSex.Tag = "s"
        Me.optSex.Text = "nach Geschlecht"
        Me.optSex.UseVisualStyleBackColor = True
        '
        'txtHantelstange
        '
        Me.txtHantelstange.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtHantelstange.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtHantelstange.ForeColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.txtHantelstange.Location = New System.Drawing.Point(91, 9)
        Me.txtHantelstange.Name = "txtHantelstange"
        Me.txtHantelstange.Size = New System.Drawing.Size(34, 13)
        Me.txtHantelstange.TabIndex = 7
        '
        'nudVmin
        '
        Me.nudVmin.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.nudVmin.BackColor = System.Drawing.SystemColors.Window
        Me.nudVmin.Location = New System.Drawing.Point(112, 18)
        Me.nudVmin.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudVmin.Name = "nudVmin"
        Me.nudVmin.ReadOnly = True
        Me.nudVmin.Size = New System.Drawing.Size(39, 20)
        Me.nudVmin.TabIndex = 8
        Me.nudVmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudVmin.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'nudMmin
        '
        Me.nudMmin.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.nudMmin.BackColor = System.Drawing.SystemColors.Window
        Me.nudMmin.Location = New System.Drawing.Point(112, 18)
        Me.nudMmin.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudMmin.Name = "nudMmin"
        Me.nudMmin.ReadOnly = True
        Me.nudMmin.Size = New System.Drawing.Size(39, 20)
        Me.nudMmin.TabIndex = 2
        Me.nudMmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudMmin.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'nudLmin
        '
        Me.nudLmin.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.nudLmin.BackColor = System.Drawing.SystemColors.Window
        Me.nudLmin.Location = New System.Drawing.Point(112, 18)
        Me.nudLmin.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudLmin.Name = "nudLmin"
        Me.nudLmin.ReadOnly = True
        Me.nudLmin.Size = New System.Drawing.Size(39, 20)
        Me.nudLmin.TabIndex = 14
        Me.nudLmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudLmin.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'nudMmax
        '
        Me.nudMmax.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.nudMmax.BackColor = System.Drawing.SystemColors.Window
        Me.nudMmax.Location = New System.Drawing.Point(112, 42)
        Me.nudMmax.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudMmax.Name = "nudMmax"
        Me.nudMmax.ReadOnly = True
        Me.nudMmax.Size = New System.Drawing.Size(39, 20)
        Me.nudMmax.TabIndex = 3
        Me.nudMmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudMmax.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'nudVmax
        '
        Me.nudVmax.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.nudVmax.BackColor = System.Drawing.SystemColors.Window
        Me.nudVmax.Location = New System.Drawing.Point(112, 42)
        Me.nudVmax.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudVmax.Name = "nudVmax"
        Me.nudVmax.ReadOnly = True
        Me.nudVmax.Size = New System.Drawing.Size(39, 20)
        Me.nudVmax.TabIndex = 9
        Me.nudVmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudVmax.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'nudLmax
        '
        Me.nudLmax.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.nudLmax.BackColor = System.Drawing.SystemColors.Window
        Me.nudLmax.Location = New System.Drawing.Point(112, 42)
        Me.nudLmax.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudLmax.Name = "nudLmax"
        Me.nudLmax.ReadOnly = True
        Me.nudLmax.Size = New System.Drawing.Size(39, 20)
        Me.nudLmax.TabIndex = 15
        Me.nudLmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudLmax.Value = New Decimal(New Integer() {8, 0, 0, 0})
        '
        'nudMwert
        '
        Me.nudMwert.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.nudMwert.BackColor = System.Drawing.SystemColors.Window
        Me.nudMwert.Location = New System.Drawing.Point(112, 66)
        Me.nudMwert.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudMwert.Name = "nudMwert"
        Me.nudMwert.ReadOnly = True
        Me.nudMwert.Size = New System.Drawing.Size(39, 20)
        Me.nudMwert.TabIndex = 4
        Me.nudMwert.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudMwert.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'nudVwert
        '
        Me.nudVwert.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.nudVwert.BackColor = System.Drawing.SystemColors.Window
        Me.nudVwert.Location = New System.Drawing.Point(112, 66)
        Me.nudVwert.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudVwert.Name = "nudVwert"
        Me.nudVwert.ReadOnly = True
        Me.nudVwert.Size = New System.Drawing.Size(39, 20)
        Me.nudVwert.TabIndex = 10
        Me.nudVwert.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudVwert.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'nudLwert
        '
        Me.nudLwert.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.nudLwert.BackColor = System.Drawing.SystemColors.Window
        Me.nudLwert.Location = New System.Drawing.Point(112, 66)
        Me.nudLwert.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudLwert.Name = "nudLwert"
        Me.nudLwert.ReadOnly = True
        Me.nudLwert.Size = New System.Drawing.Size(39, 20)
        Me.nudLwert.TabIndex = 16
        Me.nudLwert.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudLwert.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'chkMadd
        '
        Me.chkMadd.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.chkMadd.AutoSize = True
        Me.chkMadd.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkMadd.Location = New System.Drawing.Point(136, 115)
        Me.chkMadd.Name = "chkMadd"
        Me.chkMadd.Size = New System.Drawing.Size(15, 14)
        Me.chkMadd.TabIndex = 6
        Me.chkMadd.UseVisualStyleBackColor = True
        Me.chkMadd.Visible = False
        '
        'chkMsex
        '
        Me.chkMsex.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.chkMsex.AutoSize = True
        Me.chkMsex.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkMsex.Location = New System.Drawing.Point(136, 92)
        Me.chkMsex.Name = "chkMsex"
        Me.chkMsex.Size = New System.Drawing.Size(15, 14)
        Me.chkMsex.TabIndex = 5
        Me.chkMsex.UseVisualStyleBackColor = True
        Me.chkMsex.Visible = False
        '
        'chkLiga
        '
        Me.chkLiga.AutoSize = True
        Me.chkLiga.ForeColor = System.Drawing.Color.MediumBlue
        Me.chkLiga.Location = New System.Drawing.Point(356, 10)
        Me.chkLiga.Name = "chkLiga"
        Me.chkLiga.Size = New System.Drawing.Size(46, 17)
        Me.chkLiga.TabIndex = 1
        Me.chkLiga.Tag = "0"
        Me.chkLiga.Text = "Liga"
        Me.chkLiga.UseVisualStyleBackColor = True
        '
        'chkLadd
        '
        Me.chkLadd.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.chkLadd.AutoSize = True
        Me.chkLadd.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkLadd.Location = New System.Drawing.Point(136, 115)
        Me.chkLadd.Name = "chkLadd"
        Me.chkLadd.Size = New System.Drawing.Size(15, 14)
        Me.chkLadd.TabIndex = 18
        Me.chkLadd.UseVisualStyleBackColor = True
        '
        'chkVadd
        '
        Me.chkVadd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkVadd.AutoSize = True
        Me.chkVadd.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkVadd.Checked = True
        Me.chkVadd.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkVadd.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkVadd.Location = New System.Drawing.Point(136, 116)
        Me.chkVadd.Name = "chkVadd"
        Me.chkVadd.Size = New System.Drawing.Size(15, 14)
        Me.chkVadd.TabIndex = 12
        Me.chkVadd.UseVisualStyleBackColor = True
        '
        'chkLsex
        '
        Me.chkLsex.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.chkLsex.AutoSize = True
        Me.chkLsex.Checked = True
        Me.chkLsex.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLsex.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkLsex.Location = New System.Drawing.Point(136, 92)
        Me.chkLsex.Name = "chkLsex"
        Me.chkLsex.Size = New System.Drawing.Size(15, 14)
        Me.chkLsex.TabIndex = 17
        Me.chkLsex.UseVisualStyleBackColor = True
        '
        'chkVsex
        '
        Me.chkVsex.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkVsex.AutoSize = True
        Me.chkVsex.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkVsex.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkVsex.Location = New System.Drawing.Point(136, 92)
        Me.chkVsex.Name = "chkVsex"
        Me.chkVsex.Size = New System.Drawing.Size(15, 14)
        Me.chkVsex.TabIndex = 11
        Me.chkVsex.UseVisualStyleBackColor = True
        '
        'chkLand
        '
        Me.chkLand.AutoSize = True
        Me.chkLand.ForeColor = System.Drawing.Color.MediumBlue
        Me.chkLand.Location = New System.Drawing.Point(185, 10)
        Me.chkLand.Name = "chkLand"
        Me.chkLand.Size = New System.Drawing.Size(97, 17)
        Me.chkLand.TabIndex = 13
        Me.chkLand.Tag = "2"
        Me.chkLand.Text = "Länderwertung"
        Me.chkLand.UseVisualStyleBackColor = True
        '
        'chkVerein
        '
        Me.chkVerein.AutoSize = True
        Me.chkVerein.ForeColor = System.Drawing.Color.MediumBlue
        Me.chkVerein.Location = New System.Drawing.Point(14, 10)
        Me.chkVerein.Name = "chkVerein"
        Me.chkVerein.Size = New System.Drawing.Size(99, 17)
        Me.chkVerein.TabIndex = 7
        Me.chkVerein.Tag = "1"
        Me.chkVerein.Text = "Vereinswertung"
        Me.chkVerein.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label7.Location = New System.Drawing.Point(60, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 13)
        Me.Label7.TabIndex = 301
        Me.Label7.Text = "männlich"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label11.Location = New System.Drawing.Point(115, 25)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(45, 13)
        Me.Label11.TabIndex = 302
        Me.Label11.Text = "weiblich"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label23.Location = New System.Drawing.Point(12, 110)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(41, 13)
        Me.Label23.TabIndex = 309
        Me.Label23.Text = "Stoßen"
        '
        'txtR_w
        '
        Me.txtR_w.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtR_w.Location = New System.Drawing.Point(118, 77)
        Me.txtR_w.Name = "txtR_w"
        Me.txtR_w.Size = New System.Drawing.Size(39, 20)
        Me.txtR_w.TabIndex = 308
        Me.txtR_w.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtR_m
        '
        Me.txtR_m.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtR_m.Location = New System.Drawing.Point(65, 77)
        Me.txtR_m.Name = "txtR_m"
        Me.txtR_m.Size = New System.Drawing.Size(39, 20)
        Me.txtR_m.TabIndex = 307
        Me.txtR_m.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtS_w
        '
        Me.txtS_w.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtS_w.Location = New System.Drawing.Point(118, 107)
        Me.txtS_w.Name = "txtS_w"
        Me.txtS_w.Size = New System.Drawing.Size(39, 20)
        Me.txtS_w.TabIndex = 311
        Me.txtS_w.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtS_m
        '
        Me.txtS_m.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtS_m.Location = New System.Drawing.Point(65, 107)
        Me.txtS_m.Name = "txtS_m"
        Me.txtS_m.Size = New System.Drawing.Size(39, 20)
        Me.txtS_m.TabIndex = 310
        Me.txtS_m.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label23)
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Controls.Add(Me.Label13)
        Me.GroupBox5.Controls.Add(Me.txtS_m)
        Me.GroupBox5.Controls.Add(Me.txtR_m)
        Me.GroupBox5.Controls.Add(Me.txtZK_m)
        Me.GroupBox5.Controls.Add(Me.Label11)
        Me.GroupBox5.Controls.Add(Me.txtS_w)
        Me.GroupBox5.Controls.Add(Me.txtR_w)
        Me.GroupBox5.Controls.Add(Me.txtZK_w)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.ForeColor = System.Drawing.Color.Firebrick
        Me.GroupBox5.Location = New System.Drawing.Point(709, 38)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(179, 144)
        Me.GroupBox5.TabIndex = 300
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Mindestlasten"
        '
        'chkEqual_w
        '
        Me.chkEqual_w.AutoSize = True
        Me.chkEqual_w.ForeColor = System.Drawing.Color.MediumBlue
        Me.chkEqual_w.Location = New System.Drawing.Point(315, 162)
        Me.chkEqual_w.Name = "chkEqual_w"
        Me.chkEqual_w.Size = New System.Drawing.Size(73, 17)
        Me.chkEqual_w.TabIndex = 1235
        Me.chkEqual_w.Text = "alle gleich"
        Me.chkEqual_w.UseVisualStyleBackColor = True
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.BackColor = System.Drawing.Color.Transparent
        Me.Label37.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label37.Location = New System.Drawing.Point(125, 9)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(33, 13)
        Me.Label37.TabIndex = 1202
        Me.Label37.Text = "Teiler"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label24.Location = New System.Drawing.Point(367, 9)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(33, 13)
        Me.Label24.TabIndex = 1222
        Me.Label24.Text = "Teiler"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblG_w4
        '
        Me.lblG_w4.AutoSize = True
        Me.lblG_w4.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_w4.Location = New System.Drawing.Point(410, 131)
        Me.lblG_w4.Name = "lblG_w4"
        Me.lblG_w4.Size = New System.Drawing.Size(30, 13)
        Me.lblG_w4.TabIndex = 1234
        Me.lblG_w4.Tag = ""
        Me.lblG_w4.Text = "5 x 7"
        '
        'lblG_w3
        '
        Me.lblG_w3.AutoSize = True
        Me.lblG_w3.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_w3.Location = New System.Drawing.Point(410, 105)
        Me.lblG_w3.Name = "lblG_w3"
        Me.lblG_w3.Size = New System.Drawing.Size(30, 13)
        Me.lblG_w3.TabIndex = 1232
        Me.lblG_w3.Tag = ""
        Me.lblG_w3.Text = "4 x 7"
        '
        'lblG_m4
        '
        Me.lblG_m4.AutoSize = True
        Me.lblG_m4.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_m4.Location = New System.Drawing.Point(171, 131)
        Me.lblG_m4.Name = "lblG_m4"
        Me.lblG_m4.Size = New System.Drawing.Size(30, 13)
        Me.lblG_m4.TabIndex = 1214
        Me.lblG_m4.Tag = ""
        Me.lblG_m4.Text = "5 x 9"
        '
        'lblG_w2
        '
        Me.lblG_w2.AutoSize = True
        Me.lblG_w2.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_w2.Location = New System.Drawing.Point(410, 81)
        Me.lblG_w2.Name = "lblG_w2"
        Me.lblG_w2.Size = New System.Drawing.Size(30, 13)
        Me.lblG_w2.TabIndex = 1230
        Me.lblG_w2.Tag = ""
        Me.lblG_w2.Text = "3 x 7"
        '
        'lblG_m3
        '
        Me.lblG_m3.AutoSize = True
        Me.lblG_m3.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_m3.Location = New System.Drawing.Point(171, 105)
        Me.lblG_m3.Name = "lblG_m3"
        Me.lblG_m3.Size = New System.Drawing.Size(30, 13)
        Me.lblG_m3.TabIndex = 1212
        Me.lblG_m3.Tag = ""
        Me.lblG_m3.Text = "4 x 9"
        '
        'lblG_w0
        '
        Me.lblG_w0.AutoSize = True
        Me.lblG_w0.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_w0.Location = New System.Drawing.Point(410, 31)
        Me.lblG_w0.Name = "lblG_w0"
        Me.lblG_w0.Size = New System.Drawing.Size(36, 13)
        Me.lblG_w0.TabIndex = 1226
        Me.lblG_w0.Tag = ""
        Me.lblG_w0.Text = "1 x 13"
        '
        'lblG_w1
        '
        Me.lblG_w1.AutoSize = True
        Me.lblG_w1.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_w1.Location = New System.Drawing.Point(410, 56)
        Me.lblG_w1.Name = "lblG_w1"
        Me.lblG_w1.Size = New System.Drawing.Size(30, 13)
        Me.lblG_w1.TabIndex = 1228
        Me.lblG_w1.Tag = ""
        Me.lblG_w1.Text = "2 x 7"
        '
        'lblG_m2
        '
        Me.lblG_m2.AutoSize = True
        Me.lblG_m2.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_m2.Location = New System.Drawing.Point(171, 81)
        Me.lblG_m2.Name = "lblG_m2"
        Me.lblG_m2.Size = New System.Drawing.Size(30, 13)
        Me.lblG_m2.TabIndex = 1210
        Me.lblG_m2.Tag = ""
        Me.lblG_m2.Text = "3 x 9"
        '
        'lblG_m0
        '
        Me.lblG_m0.AutoSize = True
        Me.lblG_m0.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_m0.Location = New System.Drawing.Point(171, 31)
        Me.lblG_m0.Name = "lblG_m0"
        Me.lblG_m0.Size = New System.Drawing.Size(36, 13)
        Me.lblG_m0.TabIndex = 1206
        Me.lblG_m0.Tag = ""
        Me.lblG_m0.Text = "1 x 15"
        '
        'lblG_m1
        '
        Me.lblG_m1.AutoSize = True
        Me.lblG_m1.ForeColor = System.Drawing.Color.DimGray
        Me.lblG_m1.Location = New System.Drawing.Point(171, 56)
        Me.lblG_m1.Name = "lblG_m1"
        Me.lblG_m1.Size = New System.Drawing.Size(30, 13)
        Me.lblG_m1.TabIndex = 1208
        Me.lblG_m1.Tag = ""
        Me.lblG_m1.Text = "2 x 8"
        '
        'chkEqual_m
        '
        Me.chkEqual_m.AutoSize = True
        Me.chkEqual_m.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEqual_m.ForeColor = System.Drawing.Color.MediumBlue
        Me.chkEqual_m.Location = New System.Drawing.Point(76, 162)
        Me.chkEqual_m.Name = "chkEqual_m"
        Me.chkEqual_m.Size = New System.Drawing.Size(73, 17)
        Me.chkEqual_m.TabIndex = 1215
        Me.chkEqual_m.Text = "alle gleich"
        Me.chkEqual_m.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkEqual_m.UseVisualStyleBackColor = True
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.Color.Transparent
        Me.Label31.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label31.Location = New System.Drawing.Point(306, 9)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(45, 13)
        Me.Label31.TabIndex = 1221
        Me.Label31.Text = "weiblich"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Transparent
        Me.Label40.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label40.Location = New System.Drawing.Point(70, 27)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(39, 13)
        Me.Label40.TabIndex = 1203
        Me.Label40.Text = "Anzahl"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.Transparent
        Me.Label35.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label35.Location = New System.Drawing.Point(309, 27)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(39, 13)
        Me.Label35.TabIndex = 1223
        Me.Label35.Text = "Anzahl"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.Color.Transparent
        Me.Label32.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label32.Location = New System.Drawing.Point(67, 9)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(49, 13)
        Me.Label32.TabIndex = 1201
        Me.Label32.Text = "männlich"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'nudEqual_w
        '
        Me.nudEqual_w.BackColor = System.Drawing.SystemColors.Window
        Me.nudEqual_w.Enabled = False
        Me.nudEqual_w.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudEqual_w.Location = New System.Drawing.Point(391, 159)
        Me.nudEqual_w.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudEqual_w.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudEqual_w.Name = "nudEqual_w"
        Me.nudEqual_w.ReadOnly = True
        Me.nudEqual_w.Size = New System.Drawing.Size(39, 20)
        Me.nudEqual_w.TabIndex = 1236
        Me.nudEqual_w.Tag = "7"
        Me.nudEqual_w.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudEqual_w.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'nudG_w4
        '
        Me.nudG_w4.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_w4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w4.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_w4.Location = New System.Drawing.Point(364, 129)
        Me.nudG_w4.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_w4.Minimum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_w4.Name = "nudG_w4"
        Me.nudG_w4.ReadOnly = True
        Me.nudG_w4.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w4.TabIndex = 1233
        Me.nudG_w4.Tag = "35"
        Me.nudG_w4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w4.Value = New Decimal(New Integer() {35, 0, 0, 0})
        '
        'nudEqual_m
        '
        Me.nudEqual_m.Enabled = False
        Me.nudEqual_m.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudEqual_m.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudEqual_m.Location = New System.Drawing.Point(152, 159)
        Me.nudEqual_m.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudEqual_m.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudEqual_m.Name = "nudEqual_m"
        Me.nudEqual_m.Size = New System.Drawing.Size(39, 20)
        Me.nudEqual_m.TabIndex = 1216
        Me.nudEqual_m.Tag = "10"
        Me.nudEqual_m.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudEqual_m.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'nudG_w3
        '
        Me.nudG_w3.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_w3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w3.Increment = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nudG_w3.Location = New System.Drawing.Point(364, 104)
        Me.nudG_w3.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_w3.Minimum = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nudG_w3.Name = "nudG_w3"
        Me.nudG_w3.ReadOnly = True
        Me.nudG_w3.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w3.TabIndex = 1231
        Me.nudG_w3.Tag = "28"
        Me.nudG_w3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w3.Value = New Decimal(New Integer() {28, 0, 0, 0})
        '
        'nudG_m4
        '
        Me.nudG_m4.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_m4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m4.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_m4.Location = New System.Drawing.Point(125, 129)
        Me.nudG_m4.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_m4.Minimum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_m4.Name = "nudG_m4"
        Me.nudG_m4.ReadOnly = True
        Me.nudG_m4.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m4.TabIndex = 1213
        Me.nudG_m4.Tag = "45"
        Me.nudG_m4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m4.Value = New Decimal(New Integer() {45, 0, 0, 0})
        '
        'nudG_w2
        '
        Me.nudG_w2.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_w2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w2.Increment = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudG_w2.Location = New System.Drawing.Point(364, 79)
        Me.nudG_w2.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_w2.Minimum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudG_w2.Name = "nudG_w2"
        Me.nudG_w2.ReadOnly = True
        Me.nudG_w2.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w2.TabIndex = 1229
        Me.nudG_w2.Tag = "21"
        Me.nudG_w2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w2.Value = New Decimal(New Integer() {21, 0, 0, 0})
        '
        'nudG_m3
        '
        Me.nudG_m3.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_m3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m3.Increment = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nudG_m3.Location = New System.Drawing.Point(125, 104)
        Me.nudG_m3.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_m3.Minimum = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nudG_m3.Name = "nudG_m3"
        Me.nudG_m3.ReadOnly = True
        Me.nudG_m3.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m3.TabIndex = 1211
        Me.nudG_m3.Tag = "36"
        Me.nudG_m3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m3.Value = New Decimal(New Integer() {36, 0, 0, 0})
        '
        'nudG_w1
        '
        Me.nudG_w1.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_w1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w1.Increment = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudG_w1.Location = New System.Drawing.Point(364, 53)
        Me.nudG_w1.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_w1.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudG_w1.Name = "nudG_w1"
        Me.nudG_w1.ReadOnly = True
        Me.nudG_w1.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w1.TabIndex = 1227
        Me.nudG_w1.Tag = "14"
        Me.nudG_w1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w1.Value = New Decimal(New Integer() {14, 0, 0, 0})
        '
        'nudG_m2
        '
        Me.nudG_m2.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_m2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m2.Increment = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudG_m2.Location = New System.Drawing.Point(125, 79)
        Me.nudG_m2.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_m2.Minimum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudG_m2.Name = "nudG_m2"
        Me.nudG_m2.ReadOnly = True
        Me.nudG_m2.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m2.TabIndex = 1209
        Me.nudG_m2.Tag = "27"
        Me.nudG_m2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m2.Value = New Decimal(New Integer() {27, 0, 0, 0})
        '
        'nudG_w0
        '
        Me.nudG_w0.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_w0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w0.Location = New System.Drawing.Point(364, 29)
        Me.nudG_w0.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_w0.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudG_w0.Name = "nudG_w0"
        Me.nudG_w0.ReadOnly = True
        Me.nudG_w0.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w0.TabIndex = 1225
        Me.nudG_w0.Tag = "13"
        Me.nudG_w0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w0.Value = New Decimal(New Integer() {13, 0, 0, 0})
        '
        'nudG_m1
        '
        Me.nudG_m1.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_m1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m1.Increment = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudG_m1.Location = New System.Drawing.Point(125, 53)
        Me.nudG_m1.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_m1.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudG_m1.Name = "nudG_m1"
        Me.nudG_m1.ReadOnly = True
        Me.nudG_m1.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m1.TabIndex = 1207
        Me.nudG_m1.Tag = "16"
        Me.nudG_m1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m1.Value = New Decimal(New Integer() {16, 0, 0, 0})
        '
        'nudG_w
        '
        Me.nudG_w.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_w.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_w.Location = New System.Drawing.Point(309, 45)
        Me.nudG_w.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_w.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudG_w.Name = "nudG_w"
        Me.nudG_w.ReadOnly = True
        Me.nudG_w.Size = New System.Drawing.Size(39, 20)
        Me.nudG_w.TabIndex = 1224
        Me.nudG_w.Tag = "5"
        Me.nudG_w.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_w.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'nudG_m
        '
        Me.nudG_m.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_m.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m.Location = New System.Drawing.Point(70, 45)
        Me.nudG_m.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudG_m.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudG_m.Name = "nudG_m"
        Me.nudG_m.ReadOnly = True
        Me.nudG_m.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m.TabIndex = 1204
        Me.nudG_m.Tag = "5"
        Me.nudG_m.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'nudG_m0
        '
        Me.nudG_m0.BackColor = System.Drawing.SystemColors.Window
        Me.nudG_m0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.nudG_m0.Location = New System.Drawing.Point(125, 29)
        Me.nudG_m0.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudG_m0.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudG_m0.Name = "nudG_m0"
        Me.nudG_m0.ReadOnly = True
        Me.nudG_m0.Size = New System.Drawing.Size(39, 20)
        Me.nudG_m0.TabIndex = 1205
        Me.nudG_m0.Tag = "15"
        Me.nudG_m0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudG_m0.Value = New Decimal(New Integer() {15, 0, 0, 0})
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnExit.Location = New System.Drawing.Point(946, 535)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(93, 25)
        Me.btnExit.TabIndex = 2001
        Me.btnExit.Text = "Abbrechen"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.chkLosnummer)
        Me.GroupBox7.Controls.Add(Me.Label21)
        Me.GroupBox7.Controls.Add(Me.nudMinAge)
        Me.GroupBox7.Controls.Add(Me.chkInternational)
        Me.GroupBox7.Controls.Add(Me.btnDiscColor)
        Me.GroupBox7.Controls.Add(Me.chkRegel20)
        Me.GroupBox7.Controls.Add(Me.chkFlagge)
        Me.GroupBox7.Controls.Add(Me.chkBigdisc)
        Me.GroupBox7.Controls.Add(Me.cboKR)
        Me.GroupBox7.Controls.Add(Me.Label3)
        Me.GroupBox7.ForeColor = System.Drawing.Color.Firebrick
        Me.GroupBox7.Location = New System.Drawing.Point(516, 38)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(170, 214)
        Me.GroupBox7.TabIndex = 200
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Details"
        '
        'chkLosnummer
        '
        Me.chkLosnummer.AutoSize = True
        Me.chkLosnummer.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkLosnummer.Location = New System.Drawing.Point(17, 19)
        Me.chkLosnummer.Name = "chkLosnummer"
        Me.chkLosnummer.Size = New System.Drawing.Size(121, 17)
        Me.chkLosnummer.TabIndex = 201
        Me.chkLosnummer.Text = "autom. Losnummern"
        Me.chkLosnummer.ThreeState = True
        Me.ToolTip1.SetToolTip(Me.chkLosnummer, "Losnummern nach IWF-Regeln automatisch vergeben")
        Me.chkLosnummer.UseVisualStyleBackColor = True
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label21.Location = New System.Drawing.Point(17, 160)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(64, 13)
        Me.Label21.TabIndex = 250
        Me.Label21.Text = "&Mindestalter"
        '
        'nudMinAge
        '
        Me.nudMinAge.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.nudMinAge.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMinAge.ForeColor = System.Drawing.SystemColors.ControlText
        Me.nudMinAge.Location = New System.Drawing.Point(89, 157)
        Me.nudMinAge.Maximum = New Decimal(New Integer() {120, 0, 0, 0})
        Me.nudMinAge.Minimum = New Decimal(New Integer() {7, 0, 0, 0})
        Me.nudMinAge.Name = "nudMinAge"
        Me.nudMinAge.ReadOnly = True
        Me.nudMinAge.Size = New System.Drawing.Size(52, 20)
        Me.nudMinAge.TabIndex = 251
        Me.nudMinAge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip1.SetToolTip(Me.nudMinAge, "Mindestalter der Teilnehmer")
        Me.nudMinAge.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'chkInternational
        '
        Me.chkInternational.AutoSize = True
        Me.chkInternational.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkInternational.Location = New System.Drawing.Point(17, 41)
        Me.chkInternational.Name = "chkInternational"
        Me.chkInternational.Size = New System.Drawing.Size(113, 17)
        Me.chkInternational.TabIndex = 203
        Me.chkInternational.Text = "internationaler WK"
        Me.chkInternational.UseVisualStyleBackColor = True
        '
        'btnDiscColor
        '
        Me.btnDiscColor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnDiscColor.Image = CType(resources.GetObject("btnDiscColor.Image"), System.Drawing.Image)
        Me.btnDiscColor.Location = New System.Drawing.Point(126, 102)
        Me.btnDiscColor.Name = "btnDiscColor"
        Me.btnDiscColor.Size = New System.Drawing.Size(25, 25)
        Me.btnDiscColor.TabIndex = 207
        Me.btnDiscColor.TabStop = False
        Me.btnDiscColor.Tag = ""
        Me.ToolTip1.SetToolTip(Me.btnDiscColor, "Farben & Verfügbarkeit")
        Me.btnDiscColor.UseVisualStyleBackColor = True
        '
        'chkRegel20
        '
        Me.chkRegel20.AutoSize = True
        Me.chkRegel20.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkRegel20.Location = New System.Drawing.Point(17, 63)
        Me.chkRegel20.Name = "chkRegel20"
        Me.chkRegel20.Size = New System.Drawing.Size(87, 17)
        Me.chkRegel20.TabIndex = 204
        Me.chkRegel20.Text = "20-KG-Regel"
        Me.chkRegel20.UseVisualStyleBackColor = True
        '
        'tabCollection
        '
        Me.tabCollection.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tabCollection.Controls.Add(Me.tabAthletik)
        Me.tabCollection.Controls.Add(Me.tabAusnahme)
        Me.tabCollection.Controls.Add(Me.tabGewichtsgruppen)
        Me.tabCollection.Controls.Add(Me.tabAltersgruppen)
        Me.tabCollection.Controls.Add(Me.tabMannschaft)
        Me.tabCollection.Controls.Add(Me.tabPlatzierung)
        Me.tabCollection.Controls.Add(Me.tabWertung)
        Me.tabCollection.Controls.Add(Me.tabMulti)
        Me.tabCollection.HotTrack = True
        Me.tabCollection.Location = New System.Drawing.Point(516, 276)
        Me.tabCollection.Name = "tabCollection"
        Me.tabCollection.SelectedIndex = 0
        Me.tabCollection.ShowToolTips = True
        Me.tabCollection.Size = New System.Drawing.Size(523, 248)
        Me.tabCollection.TabIndex = 1000
        Me.tabCollection.Visible = False
        '
        'tabAthletik
        '
        Me.tabAthletik.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tabAthletik.Controls.Add(Me.btnDefault)
        Me.tabAthletik.Controls.Add(Me.dgvAthletik)
        Me.tabAthletik.Controls.Add(Me.lblBank)
        Me.tabAthletik.Controls.Add(Me.dgvDefaults)
        Me.tabAthletik.Location = New System.Drawing.Point(4, 22)
        Me.tabAthletik.Name = "tabAthletik"
        Me.tabAthletik.Size = New System.Drawing.Size(515, 222)
        Me.tabAthletik.TabIndex = 2
        Me.tabAthletik.Text = "Athletik"
        Me.tabAthletik.ToolTipText = "Athletik-Disziplinen bearbeiten"
        '
        'btnDefault
        '
        Me.btnDefault.Enabled = False
        Me.btnDefault.ForeColor = System.Drawing.Color.MediumBlue
        Me.btnDefault.Location = New System.Drawing.Point(426, 124)
        Me.btnDefault.Name = "btnDefault"
        Me.btnDefault.Size = New System.Drawing.Size(75, 23)
        Me.btnDefault.TabIndex = 1104
        Me.btnDefault.Text = "Standard"
        Me.btnDefault.UseVisualStyleBackColor = True
        '
        'dgvAthletik
        '
        Me.dgvAthletik.AllowUserToAddRows = False
        Me.dgvAthletik.AllowUserToDeleteRows = False
        Me.dgvAthletik.AllowUserToResizeColumns = False
        Me.dgvAthletik.AllowUserToResizeRows = False
        Me.dgvAthletik.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvAthletik.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle37.BackColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(227, Byte), Integer))
        DataGridViewCellStyle37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAthletik.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle37
        Me.dgvAthletik.ColumnHeadersHeight = 21
        Me.dgvAthletik.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAthletik.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Checked, Me.Bezeichnung, Me.AKs})
        Me.dgvAthletik.EnableHeadersVisualStyles = False
        Me.dgvAthletik.Location = New System.Drawing.Point(9, 10)
        Me.dgvAthletik.MultiSelect = False
        Me.dgvAthletik.Name = "dgvAthletik"
        Me.dgvAthletik.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvAthletik.RowHeadersVisible = False
        Me.dgvAthletik.RowHeadersWidth = 51
        Me.dgvAthletik.RowTemplate.Height = 21
        Me.dgvAthletik.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvAthletik.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAthletik.Size = New System.Drawing.Size(217, 170)
        Me.dgvAthletik.TabIndex = 1101
        '
        'Checked
        '
        Me.Checked.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Checked.DataPropertyName = "Checked"
        Me.Checked.HeaderText = ""
        Me.Checked.MinimumWidth = 6
        Me.Checked.Name = "Checked"
        Me.Checked.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Checked.ToolTipText = "Auswahl"
        Me.Checked.Width = 23
        '
        'Bezeichnung
        '
        Me.Bezeichnung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Bezeichnung.DataPropertyName = "Bezeichnung"
        Me.Bezeichnung.FillWeight = 115.0!
        Me.Bezeichnung.HeaderText = "Disziplin"
        Me.Bezeichnung.MinimumWidth = 6
        Me.Bezeichnung.Name = "Bezeichnung"
        Me.Bezeichnung.ReadOnly = True
        Me.Bezeichnung.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Bezeichnung.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Bezeichnung.Width = 115
        '
        'AKs
        '
        Me.AKs.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.AKs.DataPropertyName = "AKs"
        Me.AKs.FillWeight = 80.0!
        Me.AKs.HeaderText = "AKs"
        Me.AKs.MinimumWidth = 6
        Me.AKs.Name = "AKs"
        Me.AKs.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.AKs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AKs.ToolTipText = "teilnehmende AKs (Komma getrennt, leer = alle)"
        '
        'lblBank
        '
        Me.lblBank.AutoSize = True
        Me.lblBank.ForeColor = System.Drawing.Color.MediumBlue
        Me.lblBank.Location = New System.Drawing.Point(243, 14)
        Me.lblBank.Name = "lblBank"
        Me.lblBank.Size = New System.Drawing.Size(188, 13)
        Me.lblBank.TabIndex = 1102
        Me.lblBank.Text = "Bankdrücken (Wiegegewicht * Faktor)"
        '
        'dgvDefaults
        '
        Me.dgvDefaults.AllowUserToAddRows = False
        Me.dgvDefaults.AllowUserToDeleteRows = False
        Me.dgvDefaults.AllowUserToResizeColumns = False
        Me.dgvDefaults.AllowUserToResizeRows = False
        Me.dgvDefaults.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvDefaults.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle38.BackColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(227, Byte), Integer))
        DataGridViewCellStyle38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDefaults.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle38
        Me.dgvDefaults.ColumnHeadersHeight = 22
        Me.dgvDefaults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvDefaults.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.D_Sex, Me.ak5, Me.ak6, Me.ak7, Me.ak8, Me.ak9, Me.ak10, Me.ak11, Me.ak12, Me.ak13, Me.ak14, Me.ak15, Me.ak16, Me.ak17})
        DataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle40.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle40.Format = "0.00"
        DataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDefaults.DefaultCellStyle = DataGridViewCellStyle40
        Me.dgvDefaults.Enabled = False
        Me.dgvDefaults.EnableHeadersVisualStyles = False
        Me.dgvDefaults.Location = New System.Drawing.Point(243, 33)
        Me.dgvDefaults.MultiSelect = False
        Me.dgvDefaults.Name = "dgvDefaults"
        Me.dgvDefaults.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvDefaults.RowHeadersVisible = False
        Me.dgvDefaults.RowHeadersWidth = 51
        Me.dgvDefaults.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvDefaults.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.dgvDefaults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvDefaults.ShowEditingIcon = False
        Me.dgvDefaults.Size = New System.Drawing.Size(257, 85)
        Me.dgvDefaults.TabIndex = 1103
        '
        'D_Sex
        '
        Me.D_Sex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.D_Sex.DataPropertyName = "Geschlecht"
        DataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle39.BackColor = System.Drawing.Color.LightGray
        DataGridViewCellStyle39.ForeColor = System.Drawing.Color.DarkRed
        DataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.LightGray
        DataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.DarkRed
        Me.D_Sex.DefaultCellStyle = DataGridViewCellStyle39
        Me.D_Sex.Frozen = True
        Me.D_Sex.HeaderText = "AK"
        Me.D_Sex.MinimumWidth = 6
        Me.D_Sex.Name = "D_Sex"
        Me.D_Sex.ReadOnly = True
        Me.D_Sex.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.D_Sex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.D_Sex.Width = 25
        '
        'ak5
        '
        Me.ak5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak5.DataPropertyName = "ak5"
        Me.ak5.HeaderText = "5"
        Me.ak5.MinimumWidth = 6
        Me.ak5.Name = "ak5"
        Me.ak5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak5.Width = 33
        '
        'ak6
        '
        Me.ak6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak6.DataPropertyName = "ak6"
        Me.ak6.HeaderText = "6"
        Me.ak6.MinimumWidth = 6
        Me.ak6.Name = "ak6"
        Me.ak6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak6.Width = 33
        '
        'ak7
        '
        Me.ak7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak7.DataPropertyName = "ak7"
        Me.ak7.HeaderText = "7"
        Me.ak7.MinimumWidth = 6
        Me.ak7.Name = "ak7"
        Me.ak7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak7.Width = 33
        '
        'ak8
        '
        Me.ak8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak8.DataPropertyName = "ak8"
        Me.ak8.HeaderText = "8"
        Me.ak8.MinimumWidth = 6
        Me.ak8.Name = "ak8"
        Me.ak8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak8.Width = 33
        '
        'ak9
        '
        Me.ak9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak9.DataPropertyName = "ak9"
        Me.ak9.HeaderText = "9"
        Me.ak9.MinimumWidth = 6
        Me.ak9.Name = "ak9"
        Me.ak9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak9.Width = 33
        '
        'ak10
        '
        Me.ak10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak10.DataPropertyName = "ak10"
        Me.ak10.HeaderText = "10"
        Me.ak10.MinimumWidth = 6
        Me.ak10.Name = "ak10"
        Me.ak10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak10.Width = 33
        '
        'ak11
        '
        Me.ak11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak11.DataPropertyName = "ak11"
        Me.ak11.HeaderText = "11"
        Me.ak11.MinimumWidth = 6
        Me.ak11.Name = "ak11"
        Me.ak11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak11.Width = 33
        '
        'ak12
        '
        Me.ak12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak12.DataPropertyName = "ak12"
        Me.ak12.HeaderText = "12"
        Me.ak12.MinimumWidth = 6
        Me.ak12.Name = "ak12"
        Me.ak12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak12.Width = 33
        '
        'ak13
        '
        Me.ak13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak13.DataPropertyName = "ak13"
        Me.ak13.HeaderText = "13"
        Me.ak13.MinimumWidth = 6
        Me.ak13.Name = "ak13"
        Me.ak13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak13.Width = 33
        '
        'ak14
        '
        Me.ak14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak14.DataPropertyName = "ak14"
        Me.ak14.HeaderText = "14"
        Me.ak14.MinimumWidth = 6
        Me.ak14.Name = "ak14"
        Me.ak14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak14.Width = 33
        '
        'ak15
        '
        Me.ak15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak15.DataPropertyName = "ak15"
        Me.ak15.HeaderText = "15"
        Me.ak15.MinimumWidth = 6
        Me.ak15.Name = "ak15"
        Me.ak15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak15.Width = 33
        '
        'ak16
        '
        Me.ak16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak16.DataPropertyName = "ak16"
        Me.ak16.HeaderText = "16"
        Me.ak16.MinimumWidth = 6
        Me.ak16.Name = "ak16"
        Me.ak16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak16.Width = 33
        '
        'ak17
        '
        Me.ak17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ak17.DataPropertyName = "ak17"
        Me.ak17.HeaderText = "17"
        Me.ak17.MinimumWidth = 6
        Me.ak17.Name = "ak17"
        Me.ak17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ak17.Width = 33
        '
        'tabAusnahme
        '
        Me.tabAusnahme.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tabAusnahme.Controls.Add(Me.GroupBox4)
        Me.tabAusnahme.Controls.Add(Me.grpStossen)
        Me.tabAusnahme.Controls.Add(Me.grpReissen)
        Me.tabAusnahme.Location = New System.Drawing.Point(4, 22)
        Me.tabAusnahme.Name = "tabAusnahme"
        Me.tabAusnahme.Padding = New System.Windows.Forms.Padding(3)
        Me.tabAusnahme.Size = New System.Drawing.Size(515, 222)
        Me.tabAusnahme.TabIndex = 6
        Me.tabAusnahme.Text = "Beschränkung"
        Me.tabAusnahme.ToolTipText = "Disziplinen und Anzahl der Versuche festlegen"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.chkVersucheSortieren)
        Me.GroupBox4.Controls.Add(Me.chkWeitererVersuch)
        Me.GroupBox4.Enabled = False
        Me.GroupBox4.ForeColor = System.Drawing.Color.Firebrick
        Me.GroupBox4.Location = New System.Drawing.Point(362, 20)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(134, 147)
        Me.GroupBox4.TabIndex = 24
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Optionen"
        Me.GroupBox4.UseCompatibleTextRendering = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "bei Beschränkung"
        Me.Label1.UseCompatibleTextRendering = True
        '
        'chkVersucheSortieren
        '
        Me.chkVersucheSortieren.AutoCheck = False
        Me.chkVersucheSortieren.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkVersucheSortieren.Location = New System.Drawing.Point(12, 83)
        Me.chkVersucheSortieren.Name = "chkVersucheSortieren"
        Me.chkVersucheSortieren.Size = New System.Drawing.Size(105, 30)
        Me.chkVersucheSortieren.TabIndex = 1
        Me.chkVersucheSortieren.TabStop = False
        Me.chkVersucheSortieren.Text = "alle Versuche hintereinander"
        Me.chkVersucheSortieren.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkVersucheSortieren.UseCompatibleTextRendering = True
        Me.chkVersucheSortieren.UseVisualStyleBackColor = True
        '
        'chkWeitererVersuch
        '
        Me.chkWeitererVersuch.AutoCheck = False
        Me.chkWeitererVersuch.ForeColor = System.Drawing.SystemColors.WindowText
        Me.chkWeitererVersuch.Location = New System.Drawing.Point(12, 40)
        Me.chkWeitererVersuch.Name = "chkWeitererVersuch"
        Me.chkWeitererVersuch.Size = New System.Drawing.Size(120, 30)
        Me.chkWeitererVersuch.TabIndex = 0
        Me.chkWeitererVersuch.TabStop = False
        Me.chkWeitererVersuch.Text = "weiterer Versuch, wenn alle ungültig"
        Me.chkWeitererVersuch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkWeitererVersuch.UseCompatibleTextRendering = True
        Me.chkWeitererVersuch.UseVisualStyleBackColor = True
        '
        'grpStossen
        '
        Me.grpStossen.Controls.Add(Me.Label25)
        Me.grpStossen.Controls.Add(Me.nudRestrict_S_AK)
        Me.grpStossen.Controls.Add(Me.chkRestrict_S)
        Me.grpStossen.Controls.Add(Me.Label39)
        Me.grpStossen.Controls.Add(Me.nudRestrict_S_V)
        Me.grpStossen.ForeColor = System.Drawing.Color.Firebrick
        Me.grpStossen.Location = New System.Drawing.Point(21, 103)
        Me.grpStossen.Name = "grpStossen"
        Me.grpStossen.Size = New System.Drawing.Size(325, 64)
        Me.grpStossen.TabIndex = 23
        Me.grpStossen.TabStop = False
        Me.grpStossen.Text = "Beschränkung für Stoßen"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label25.Location = New System.Drawing.Point(118, 29)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(96, 17)
        Me.Label25.TabIndex = 17
        Me.Label25.Text = "und jünger hat nur"
        Me.Label25.UseCompatibleTextRendering = True
        '
        'nudRestrict_S_AK
        '
        Me.nudRestrict_S_AK.BackColor = System.Drawing.SystemColors.Window
        Me.nudRestrict_S_AK.Enabled = False
        Me.nudRestrict_S_AK.Location = New System.Drawing.Point(71, 27)
        Me.nudRestrict_S_AK.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudRestrict_S_AK.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudRestrict_S_AK.Name = "nudRestrict_S_AK"
        Me.nudRestrict_S_AK.ReadOnly = True
        Me.nudRestrict_S_AK.Size = New System.Drawing.Size(40, 20)
        Me.nudRestrict_S_AK.TabIndex = 20
        Me.nudRestrict_S_AK.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudRestrict_S_AK.Value = New Decimal(New Integer() {11, 0, 0, 0})
        '
        'chkRestrict_S
        '
        Me.chkRestrict_S.AutoSize = True
        Me.chkRestrict_S.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(228, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.chkRestrict_S.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(228, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.chkRestrict_S.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(228, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.chkRestrict_S.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRestrict_S.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRestrict_S.ImageIndex = 0
        Me.chkRestrict_S.Location = New System.Drawing.Point(27, 26)
        Me.chkRestrict_S.Name = "chkRestrict_S"
        Me.chkRestrict_S.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.chkRestrict_S.Size = New System.Drawing.Size(38, 21)
        Me.chkRestrict_S.TabIndex = 10
        Me.chkRestrict_S.Text = "AK"
        Me.chkRestrict_S.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.chkRestrict_S.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.chkRestrict_S.UseCompatibleTextRendering = True
        Me.chkRestrict_S.UseVisualStyleBackColor = False
        '
        'Label39
        '
        Me.Label39.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label39.AutoSize = True
        Me.Label39.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label39.Location = New System.Drawing.Point(263, 29)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(52, 17)
        Me.Label39.TabIndex = 14
        Me.Label39.Text = "Versuche"
        Me.Label39.UseCompatibleTextRendering = True
        '
        'nudRestrict_S_V
        '
        Me.nudRestrict_S_V.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.nudRestrict_S_V.BackColor = System.Drawing.SystemColors.Window
        Me.nudRestrict_S_V.Enabled = False
        Me.nudRestrict_S_V.Location = New System.Drawing.Point(218, 27)
        Me.nudRestrict_S_V.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudRestrict_S_V.Name = "nudRestrict_S_V"
        Me.nudRestrict_S_V.ReadOnly = True
        Me.nudRestrict_S_V.Size = New System.Drawing.Size(40, 20)
        Me.nudRestrict_S_V.TabIndex = 21
        Me.nudRestrict_S_V.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'grpReissen
        '
        Me.grpReissen.Controls.Add(Me.Label46)
        Me.grpReissen.Controls.Add(Me.nudRestrict_R_V)
        Me.grpReissen.Controls.Add(Me.Label42)
        Me.grpReissen.Controls.Add(Me.nudRestrict_R_AK)
        Me.grpReissen.Controls.Add(Me.chkRestrict_R)
        Me.grpReissen.ForeColor = System.Drawing.Color.Firebrick
        Me.grpReissen.Location = New System.Drawing.Point(21, 20)
        Me.grpReissen.Name = "grpReissen"
        Me.grpReissen.Size = New System.Drawing.Size(325, 64)
        Me.grpReissen.TabIndex = 22
        Me.grpReissen.TabStop = False
        Me.grpReissen.Text = "Beschränkung für Reißen"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label46.Location = New System.Drawing.Point(118, 29)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(96, 17)
        Me.Label46.TabIndex = 26
        Me.Label46.Text = "und jünger hat nur"
        Me.Label46.UseCompatibleTextRendering = True
        '
        'nudRestrict_R_V
        '
        Me.nudRestrict_R_V.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.nudRestrict_R_V.BackColor = System.Drawing.SystemColors.Window
        Me.nudRestrict_R_V.Enabled = False
        Me.nudRestrict_R_V.ForeColor = System.Drawing.SystemColors.ControlText
        Me.nudRestrict_R_V.Location = New System.Drawing.Point(218, 27)
        Me.nudRestrict_R_V.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudRestrict_R_V.Name = "nudRestrict_R_V"
        Me.nudRestrict_R_V.ReadOnly = True
        Me.nudRestrict_R_V.Size = New System.Drawing.Size(40, 20)
        Me.nudRestrict_R_V.TabIndex = 25
        Me.nudRestrict_R_V.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudRestrict_R_V.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'Label42
        '
        Me.Label42.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label42.AutoSize = True
        Me.Label42.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label42.Location = New System.Drawing.Point(263, 29)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(52, 17)
        Me.Label42.TabIndex = 24
        Me.Label42.Text = "Versuche"
        Me.Label42.UseCompatibleTextRendering = True
        '
        'nudRestrict_R_AK
        '
        Me.nudRestrict_R_AK.BackColor = System.Drawing.SystemColors.Window
        Me.nudRestrict_R_AK.Enabled = False
        Me.nudRestrict_R_AK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.nudRestrict_R_AK.Location = New System.Drawing.Point(71, 27)
        Me.nudRestrict_R_AK.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudRestrict_R_AK.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudRestrict_R_AK.Name = "nudRestrict_R_AK"
        Me.nudRestrict_R_AK.ReadOnly = True
        Me.nudRestrict_R_AK.Size = New System.Drawing.Size(40, 20)
        Me.nudRestrict_R_AK.TabIndex = 23
        Me.nudRestrict_R_AK.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudRestrict_R_AK.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'chkRestrict_R
        '
        Me.chkRestrict_R.AutoSize = True
        Me.chkRestrict_R.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(228, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.chkRestrict_R.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(228, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.chkRestrict_R.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(228, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.chkRestrict_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRestrict_R.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRestrict_R.ImageIndex = 0
        Me.chkRestrict_R.Location = New System.Drawing.Point(27, 26)
        Me.chkRestrict_R.Name = "chkRestrict_R"
        Me.chkRestrict_R.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.chkRestrict_R.Size = New System.Drawing.Size(38, 21)
        Me.chkRestrict_R.TabIndex = 21
        Me.chkRestrict_R.Text = "AK"
        Me.chkRestrict_R.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.chkRestrict_R.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.chkRestrict_R.UseCompatibleTextRendering = True
        Me.chkRestrict_R.UseVisualStyleBackColor = False
        '
        'tabGewichtsgruppen
        '
        Me.tabGewichtsgruppen.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tabGewichtsgruppen.Controls.Add(Me.chkEqual_w)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label37)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m0)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label24)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_w4)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_w3)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m1)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_m4)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w0)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_w2)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m2)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_m3)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w1)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_w0)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m3)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_w1)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w2)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_m2)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_m4)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_m0)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w3)
        Me.tabGewichtsgruppen.Controls.Add(Me.lblG_m1)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudEqual_m)
        Me.tabGewichtsgruppen.Controls.Add(Me.chkEqual_m)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudG_w4)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label31)
        Me.tabGewichtsgruppen.Controls.Add(Me.nudEqual_w)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label40)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label32)
        Me.tabGewichtsgruppen.Controls.Add(Me.Label35)
        Me.tabGewichtsgruppen.Location = New System.Drawing.Point(4, 22)
        Me.tabGewichtsgruppen.Name = "tabGewichtsgruppen"
        Me.tabGewichtsgruppen.Padding = New System.Windows.Forms.Padding(3)
        Me.tabGewichtsgruppen.Size = New System.Drawing.Size(515, 222)
        Me.tabGewichtsgruppen.TabIndex = 0
        Me.tabGewichtsgruppen.Text = "Gewichtsgruppen"
        Me.tabGewichtsgruppen.ToolTipText = "Verteilerschlüssel der Gewichtsgruppen"
        '
        'tabAltersgruppen
        '
        Me.tabAltersgruppen.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tabAltersgruppen.Controls.Add(Me.optAG_w)
        Me.tabAltersgruppen.Controls.Add(Me.optAG_m)
        Me.tabAltersgruppen.Controls.Add(Me.dgvAKs)
        Me.tabAltersgruppen.Controls.Add(Me.dgvAGs)
        Me.tabAltersgruppen.Controls.Add(Me.btnAG_Remove)
        Me.tabAltersgruppen.Controls.Add(Me.btnAG_Add)
        Me.tabAltersgruppen.Location = New System.Drawing.Point(4, 22)
        Me.tabAltersgruppen.Name = "tabAltersgruppen"
        Me.tabAltersgruppen.Size = New System.Drawing.Size(515, 222)
        Me.tabAltersgruppen.TabIndex = 3
        Me.tabAltersgruppen.Text = "Altersgruppen"
        Me.tabAltersgruppen.ToolTipText = "Atersklassen zusammenfassen"
        '
        'optAG_w
        '
        Me.optAG_w.AutoSize = True
        Me.optAG_w.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optAG_w.ForeColor = System.Drawing.Color.Firebrick
        Me.optAG_w.Location = New System.Drawing.Point(89, 10)
        Me.optAG_w.Name = "optAG_w"
        Me.optAG_w.Size = New System.Drawing.Size(33, 17)
        Me.optAG_w.TabIndex = 2302
        Me.optAG_w.Text = "w"
        Me.optAG_w.UseVisualStyleBackColor = True
        '
        'optAG_m
        '
        Me.optAG_m.AutoSize = True
        Me.optAG_m.Checked = True
        Me.optAG_m.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optAG_m.ForeColor = System.Drawing.Color.MediumBlue
        Me.optAG_m.Location = New System.Drawing.Point(37, 10)
        Me.optAG_m.Name = "optAG_m"
        Me.optAG_m.Size = New System.Drawing.Size(33, 17)
        Me.optAG_m.TabIndex = 2301
        Me.optAG_m.TabStop = True
        Me.optAG_m.Text = "m"
        Me.optAG_m.UseVisualStyleBackColor = True
        '
        'dgvAKs
        '
        Me.dgvAKs.AllowUserToAddRows = False
        Me.dgvAKs.AllowUserToDeleteRows = False
        Me.dgvAKs.AllowUserToResizeColumns = False
        Me.dgvAKs.AllowUserToResizeRows = False
        DataGridViewCellStyle41.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAKs.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle41
        Me.dgvAKs.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvAKs.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle42.BackColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(227, Byte), Integer))
        DataGridViewCellStyle42.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle42.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAKs.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle42
        Me.dgvAKs.ColumnHeadersHeight = 21
        Me.dgvAKs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAKs.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Check, Me.AK, Me.JG})
        Me.dgvAKs.EnableHeadersVisualStyles = False
        Me.dgvAKs.Location = New System.Drawing.Point(13, 33)
        Me.dgvAKs.MultiSelect = False
        Me.dgvAKs.Name = "dgvAKs"
        Me.dgvAKs.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvAKs.RowHeadersVisible = False
        Me.dgvAKs.RowHeadersWidth = 51
        Me.dgvAKs.RowTemplate.Height = 20
        Me.dgvAKs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvAKs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvAKs.Size = New System.Drawing.Size(127, 143)
        Me.dgvAKs.TabIndex = 2304
        '
        'Check
        '
        Me.Check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Check.DataPropertyName = "Check"
        Me.Check.HeaderText = ""
        Me.Check.MinimumWidth = 6
        Me.Check.Name = "Check"
        Me.Check.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Check.ToolTipText = "Auswahl"
        Me.Check.Width = 20
        '
        'AK
        '
        Me.AK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.AK.DataPropertyName = "AK"
        DataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.AK.DefaultCellStyle = DataGridViewCellStyle43
        Me.AK.FillWeight = 30.0!
        Me.AK.HeaderText = "AK"
        Me.AK.MinimumWidth = 6
        Me.AK.Name = "AK"
        Me.AK.ReadOnly = True
        Me.AK.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.AK.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AK.ToolTipText = "Altersklasse"
        Me.AK.Width = 35
        '
        'JG
        '
        Me.JG.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.JG.DataPropertyName = "JG"
        DataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.JG.DefaultCellStyle = DataGridViewCellStyle44
        Me.JG.FillWeight = 74.0!
        Me.JG.HeaderText = "Jahrgang"
        Me.JG.MinimumWidth = 6
        Me.JG.Name = "JG"
        Me.JG.ReadOnly = True
        Me.JG.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.JG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.JG.ToolTipText = "verfügbare Jahrgänge"
        '
        'dgvAGs
        '
        Me.dgvAGs.AllowUserToAddRows = False
        Me.dgvAGs.AllowUserToDeleteRows = False
        Me.dgvAGs.AllowUserToResizeColumns = False
        Me.dgvAGs.AllowUserToResizeRows = False
        DataGridViewCellStyle45.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAGs.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle45
        Me.dgvAGs.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvAGs.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle46.BackColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(227, Byte), Integer))
        DataGridViewCellStyle46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle46.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle46.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle46.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAGs.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle46
        Me.dgvAGs.ColumnHeadersHeight = 21
        Me.dgvAGs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAGs.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Sex, Me.Jahrgang})
        Me.dgvAGs.EnableHeadersVisualStyles = False
        Me.dgvAGs.Location = New System.Drawing.Point(146, 13)
        Me.dgvAGs.MultiSelect = False
        Me.dgvAGs.Name = "dgvAGs"
        Me.dgvAGs.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvAGs.RowHeadersVisible = False
        Me.dgvAGs.RowHeadersWidth = 51
        Me.dgvAGs.RowTemplate.Height = 20
        Me.dgvAGs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvAGs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAGs.Size = New System.Drawing.Size(328, 163)
        Me.dgvAGs.TabIndex = 2305
        '
        'Sex
        '
        Me.Sex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Sex.DataPropertyName = "Sex"
        DataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Sex.DefaultCellStyle = DataGridViewCellStyle47
        Me.Sex.HeaderText = "m/w"
        Me.Sex.MinimumWidth = 6
        Me.Sex.Name = "Sex"
        Me.Sex.ReadOnly = True
        Me.Sex.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Sex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Sex.ToolTipText = "Geschlecht"
        Me.Sex.Width = 35
        '
        'Jahrgang
        '
        Me.Jahrgang.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Jahrgang.DataPropertyName = "Jahrgang"
        DataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.Jahrgang.DefaultCellStyle = DataGridViewCellStyle48
        Me.Jahrgang.HeaderText = "Jahrgänge"
        Me.Jahrgang.MinimumWidth = 6
        Me.Jahrgang.Name = "Jahrgang"
        Me.Jahrgang.ReadOnly = True
        Me.Jahrgang.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Jahrgang.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Jahrgang.ToolTipText = "gemeinsam gewertete Jahrgänge"
        '
        'btnAG_Remove
        '
        Me.btnAG_Remove.Enabled = False
        Me.btnAG_Remove.Image = Global.Gewichtheben.My.Resources.Resources.Delete21
        Me.btnAG_Remove.Location = New System.Drawing.Point(480, 53)
        Me.btnAG_Remove.Name = "btnAG_Remove"
        Me.btnAG_Remove.Size = New System.Drawing.Size(24, 23)
        Me.btnAG_Remove.TabIndex = 2307
        Me.btnAG_Remove.TabStop = False
        Me.btnAG_Remove.UseVisualStyleBackColor = True
        '
        'btnAG_Add
        '
        Me.btnAG_Add.Image = Global.Gewichtheben.My.Resources.Resources.Add
        Me.btnAG_Add.Location = New System.Drawing.Point(480, 25)
        Me.btnAG_Add.Name = "btnAG_Add"
        Me.btnAG_Add.Size = New System.Drawing.Size(24, 23)
        Me.btnAG_Add.TabIndex = 2306
        Me.btnAG_Add.TabStop = False
        Me.btnAG_Add.UseVisualStyleBackColor = True
        '
        'tabMannschaft
        '
        Me.tabMannschaft.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tabMannschaft.Controls.Add(Me.chkLiga)
        Me.tabMannschaft.Controls.Add(Me.chkVerein)
        Me.tabMannschaft.Controls.Add(Me.chkLand)
        Me.tabMannschaft.Controls.Add(Me.pnlLiga)
        Me.tabMannschaft.Controls.Add(Me.pnlLand)
        Me.tabMannschaft.Controls.Add(Me.pnlVerein)
        Me.tabMannschaft.Location = New System.Drawing.Point(4, 22)
        Me.tabMannschaft.Name = "tabMannschaft"
        Me.tabMannschaft.Padding = New System.Windows.Forms.Padding(3)
        Me.tabMannschaft.Size = New System.Drawing.Size(515, 222)
        Me.tabMannschaft.TabIndex = 4
        Me.tabMannschaft.Text = "Mannschaften"
        Me.tabMannschaft.ToolTipText = "Details für Mannschaftswertung"
        '
        'pnlLiga
        '
        Me.pnlLiga.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLiga.Controls.Add(Me.chkMadd)
        Me.pnlLiga.Controls.Add(Me.Label4)
        Me.pnlLiga.Controls.Add(Me.chkMsex)
        Me.pnlLiga.Controls.Add(Me.Label12)
        Me.pnlLiga.Controls.Add(Me.Label16)
        Me.pnlLiga.Controls.Add(Me.Label18)
        Me.pnlLiga.Controls.Add(Me.Label19)
        Me.pnlLiga.Controls.Add(Me.nudMwert)
        Me.pnlLiga.Controls.Add(Me.nudMmax)
        Me.pnlLiga.Controls.Add(Me.nudMmin)
        Me.pnlLiga.Enabled = False
        Me.pnlLiga.Location = New System.Drawing.Point(346, 17)
        Me.pnlLiga.Name = "pnlLiga"
        Me.pnlLiga.Size = New System.Drawing.Size(165, 167)
        Me.pnlLiga.TabIndex = 24
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 116)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(116, 13)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "mehrere Mannschaften"
        Me.Label4.Visible = False
        '
        'Label12
        '
        Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(7, 94)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(101, 13)
        Me.Label12.TabIndex = 20
        Me.Label12.Text = "ausländische Heber"
        Me.ToolTip1.SetToolTip(Me.Label12, "erlaubte Anzahl ausländischer Heber in der Mannschaft")
        '
        'Label16
        '
        Me.Label16.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(7, 68)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(66, 13)
        Me.Label16.TabIndex = 21
        Me.Label16.Text = "TN gewertet"
        '
        'Label18
        '
        Me.Label18.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(7, 44)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(44, 13)
        Me.Label18.TabIndex = 22
        Me.Label18.Text = "TN max"
        '
        'Label19
        '
        Me.Label19.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(7, 20)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(41, 13)
        Me.Label19.TabIndex = 23
        Me.Label19.Text = "TN min"
        '
        'pnlLand
        '
        Me.pnlLand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLand.Controls.Add(Me.lblLadd)
        Me.pnlLand.Controls.Add(Me.lblLsex)
        Me.pnlLand.Controls.Add(Me.Label47)
        Me.pnlLand.Controls.Add(Me.Label48)
        Me.pnlLand.Controls.Add(Me.Label49)
        Me.pnlLand.Controls.Add(Me.chkLadd)
        Me.pnlLand.Controls.Add(Me.nudLmin)
        Me.pnlLand.Controls.Add(Me.chkLsex)
        Me.pnlLand.Controls.Add(Me.nudLmax)
        Me.pnlLand.Controls.Add(Me.nudLwert)
        Me.pnlLand.Enabled = False
        Me.pnlLand.Location = New System.Drawing.Point(175, 17)
        Me.pnlLand.Name = "pnlLand"
        Me.pnlLand.Size = New System.Drawing.Size(165, 167)
        Me.pnlLand.TabIndex = 1
        '
        'lblLadd
        '
        Me.lblLadd.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblLadd.AutoSize = True
        Me.lblLadd.Location = New System.Drawing.Point(7, 116)
        Me.lblLadd.Name = "lblLadd"
        Me.lblLadd.Size = New System.Drawing.Size(116, 13)
        Me.lblLadd.TabIndex = 19
        Me.lblLadd.Text = "mehrere Mannschaften"
        '
        'lblLsex
        '
        Me.lblLsex.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblLsex.AutoSize = True
        Me.lblLsex.Location = New System.Drawing.Point(7, 94)
        Me.lblLsex.Name = "lblLsex"
        Me.lblLsex.Size = New System.Drawing.Size(99, 13)
        Me.lblLsex.TabIndex = 20
        Me.lblLsex.Text = "beide Geschlechter"
        '
        'Label47
        '
        Me.Label47.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(7, 68)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(66, 13)
        Me.Label47.TabIndex = 21
        Me.Label47.Text = "TN gewertet"
        '
        'Label48
        '
        Me.Label48.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(7, 44)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(44, 13)
        Me.Label48.TabIndex = 22
        Me.Label48.Text = "TN max"
        '
        'Label49
        '
        Me.Label49.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(7, 20)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(41, 13)
        Me.Label49.TabIndex = 23
        Me.Label49.Text = "TN min"
        '
        'pnlVerein
        '
        Me.pnlVerein.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pnlVerein.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlVerein.Controls.Add(Me.lblVrel)
        Me.pnlVerein.Controls.Add(Me.nudVrel)
        Me.pnlVerein.Controls.Add(Me.lblVadd)
        Me.pnlVerein.Controls.Add(Me.lblVsex)
        Me.pnlVerein.Controls.Add(Me.Label29)
        Me.pnlVerein.Controls.Add(Me.Label28)
        Me.pnlVerein.Controls.Add(Me.Label27)
        Me.pnlVerein.Controls.Add(Me.nudVmin)
        Me.pnlVerein.Controls.Add(Me.chkVadd)
        Me.pnlVerein.Controls.Add(Me.nudVmax)
        Me.pnlVerein.Controls.Add(Me.nudVwert)
        Me.pnlVerein.Controls.Add(Me.chkVsex)
        Me.pnlVerein.Enabled = False
        Me.pnlVerein.Location = New System.Drawing.Point(4, 17)
        Me.pnlVerein.Name = "pnlVerein"
        Me.pnlVerein.Size = New System.Drawing.Size(165, 167)
        Me.pnlVerein.TabIndex = 0
        '
        'lblVrel
        '
        Me.lblVrel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblVrel.AutoSize = True
        Me.lblVrel.Location = New System.Drawing.Point(7, 140)
        Me.lblVrel.Name = "lblVrel"
        Me.lblVrel.Size = New System.Drawing.Size(81, 13)
        Me.lblVrel.TabIndex = 15
        Me.lblVrel.Text = "Relativ weiblich"
        '
        'nudVrel
        '
        Me.nudVrel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.nudVrel.BackColor = System.Drawing.SystemColors.Window
        Me.nudVrel.Location = New System.Drawing.Point(112, 138)
        Me.nudVrel.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudVrel.Name = "nudVrel"
        Me.nudVrel.ReadOnly = True
        Me.nudVrel.Size = New System.Drawing.Size(39, 20)
        Me.nudVrel.TabIndex = 14
        Me.nudVrel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudVrel.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'lblVadd
        '
        Me.lblVadd.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblVadd.AutoSize = True
        Me.lblVadd.Location = New System.Drawing.Point(7, 116)
        Me.lblVadd.Name = "lblVadd"
        Me.lblVadd.Size = New System.Drawing.Size(116, 13)
        Me.lblVadd.TabIndex = 13
        Me.lblVadd.Text = "mehrere Mannschaften"
        '
        'lblVsex
        '
        Me.lblVsex.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblVsex.AutoSize = True
        Me.lblVsex.Location = New System.Drawing.Point(7, 94)
        Me.lblVsex.Name = "lblVsex"
        Me.lblVsex.Size = New System.Drawing.Size(99, 13)
        Me.lblVsex.TabIndex = 13
        Me.lblVsex.Text = "beide Geschlechter"
        '
        'Label29
        '
        Me.Label29.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(7, 68)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(66, 13)
        Me.Label29.TabIndex = 13
        Me.Label29.Text = "TN gewertet"
        '
        'Label28
        '
        Me.Label28.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(7, 44)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(44, 13)
        Me.Label28.TabIndex = 13
        Me.Label28.Text = "TN max"
        '
        'Label27
        '
        Me.Label27.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(7, 20)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(41, 13)
        Me.Label27.TabIndex = 13
        Me.Label27.Text = "TN min"
        '
        'tabPlatzierung
        '
        Me.tabPlatzierung.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tabPlatzierung.Controls.Add(Me.chkUseAgeGroups)
        Me.tabPlatzierung.Controls.Add(Me.chkPlatzierung2)
        Me.tabPlatzierung.Controls.Add(Me.grpPlatzierung2)
        Me.tabPlatzierung.Controls.Add(Me.grpPlatzierung)
        Me.tabPlatzierung.Location = New System.Drawing.Point(4, 22)
        Me.tabPlatzierung.Name = "tabPlatzierung"
        Me.tabPlatzierung.Size = New System.Drawing.Size(515, 222)
        Me.tabPlatzierung.TabIndex = 7
        Me.tabPlatzierung.Text = "Platzierung"
        '
        'chkUseAgeGroups
        '
        Me.chkUseAgeGroups.AutoSize = True
        Me.chkUseAgeGroups.ForeColor = System.Drawing.Color.Firebrick
        Me.chkUseAgeGroups.Location = New System.Drawing.Point(165, 183)
        Me.chkUseAgeGroups.Name = "chkUseAgeGroups"
        Me.chkUseAgeGroups.Size = New System.Drawing.Size(160, 17)
        Me.chkUseAgeGroups.TabIndex = 5
        Me.chkUseAgeGroups.Text = "Jahrgänge zusammenfassen"
        Me.ToolTip1.SetToolTip(Me.chkUseAgeGroups, "jeweils eine Altersgruppe für alle Jahrgänge der AK Schüler bzw. Jugend")
        Me.chkUseAgeGroups.UseVisualStyleBackColor = True
        '
        'chkPlatzierung2
        '
        Me.chkPlatzierung2.AutoSize = True
        Me.chkPlatzierung2.ForeColor = System.Drawing.Color.Firebrick
        Me.chkPlatzierung2.Location = New System.Drawing.Point(161, 18)
        Me.chkPlatzierung2.Name = "chkPlatzierung2"
        Me.chkPlatzierung2.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.chkPlatzierung2.Size = New System.Drawing.Size(93, 17)
        Me.chkPlatzierung2.TabIndex = 0
        Me.chkPlatzierung2.Text = "2. Platzierung"
        Me.ToolTip1.SetToolTip(Me.chkPlatzierung2, "zusätzliche Wertung konfigurieren")
        Me.chkPlatzierung2.UseVisualStyleBackColor = True
        '
        'grpPlatzierung2
        '
        Me.grpPlatzierung2.Controls.Add(Me.grpWertung2)
        Me.grpPlatzierung2.Controls.Add(Me.optWettkampf2)
        Me.grpPlatzierung2.Controls.Add(Me.optGewichtsklassen2)
        Me.grpPlatzierung2.Controls.Add(Me.optAltersklassen2)
        Me.grpPlatzierung2.Controls.Add(Me.optWeiblich2)
        Me.grpPlatzierung2.Controls.Add(Me.optGruppen2)
        Me.grpPlatzierung2.Controls.Add(Me.dgvWertung2)
        Me.grpPlatzierung2.Enabled = False
        Me.grpPlatzierung2.Location = New System.Drawing.Point(153, 19)
        Me.grpPlatzierung2.Name = "grpPlatzierung2"
        Me.grpPlatzierung2.Size = New System.Drawing.Size(346, 155)
        Me.grpPlatzierung2.TabIndex = 2
        Me.grpPlatzierung2.TabStop = False
        '
        'grpWertung2
        '
        Me.grpWertung2.Controls.Add(Me.cboWertung2)
        Me.grpWertung2.Enabled = False
        Me.grpWertung2.ForeColor = System.Drawing.Color.MediumBlue
        Me.grpWertung2.Location = New System.Drawing.Point(159, 33)
        Me.grpWertung2.Name = "grpWertung2"
        Me.grpWertung2.Size = New System.Drawing.Size(138, 56)
        Me.grpWertung2.TabIndex = 39
        Me.grpWertung2.TabStop = False
        Me.grpWertung2.Text = "Wertung"
        Me.grpWertung2.Visible = False
        '
        'cboWertung2
        '
        Me.cboWertung2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWertung2.FormattingEnabled = True
        Me.cboWertung2.ItemHeight = 13
        Me.cboWertung2.Location = New System.Drawing.Point(11, 22)
        Me.cboWertung2.Name = "cboWertung2"
        Me.cboWertung2.Size = New System.Drawing.Size(115, 21)
        Me.cboWertung2.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.cboWertung2, "Wertung für die 2. Platzierung")
        '
        'optWettkampf2
        '
        Me.optWettkampf2.AutoSize = True
        Me.optWettkampf2.ForeColor = System.Drawing.Color.MediumBlue
        Me.optWettkampf2.Location = New System.Drawing.Point(12, 31)
        Me.optWettkampf2.Name = "optWettkampf2"
        Me.optWettkampf2.Size = New System.Drawing.Size(77, 17)
        Me.optWettkampf2.TabIndex = 1
        Me.optWettkampf2.TabStop = True
        Me.optWettkampf2.Tag = "1"
        Me.optWettkampf2.Text = "Wettkampf"
        Me.optWettkampf2.UseVisualStyleBackColor = True
        '
        'optGewichtsklassen2
        '
        Me.optGewichtsklassen2.AutoSize = True
        Me.optGewichtsklassen2.ForeColor = System.Drawing.Color.MediumBlue
        Me.optGewichtsklassen2.Location = New System.Drawing.Point(12, 100)
        Me.optGewichtsklassen2.Name = "optGewichtsklassen2"
        Me.optGewichtsklassen2.Size = New System.Drawing.Size(105, 17)
        Me.optGewichtsklassen2.TabIndex = 4
        Me.optGewichtsklassen2.TabStop = True
        Me.optGewichtsklassen2.Tag = "4"
        Me.optGewichtsklassen2.Text = "Gewichtsklassen"
        Me.optGewichtsklassen2.UseVisualStyleBackColor = True
        '
        'optAltersklassen2
        '
        Me.optAltersklassen2.AutoSize = True
        Me.optAltersklassen2.ForeColor = System.Drawing.Color.MediumBlue
        Me.optAltersklassen2.Location = New System.Drawing.Point(12, 77)
        Me.optAltersklassen2.Name = "optAltersklassen2"
        Me.optAltersklassen2.Size = New System.Drawing.Size(87, 17)
        Me.optAltersklassen2.TabIndex = 3
        Me.optAltersklassen2.TabStop = True
        Me.optAltersklassen2.Tag = "3"
        Me.optAltersklassen2.Text = "Altersklassen"
        Me.optAltersklassen2.UseVisualStyleBackColor = True
        '
        'optWeiblich2
        '
        Me.optWeiblich2.AutoSize = True
        Me.optWeiblich2.ForeColor = System.Drawing.Color.MediumBlue
        Me.optWeiblich2.Location = New System.Drawing.Point(12, 123)
        Me.optWeiblich2.Name = "optWeiblich2"
        Me.optWeiblich2.Size = New System.Drawing.Size(63, 17)
        Me.optWeiblich2.TabIndex = 5
        Me.optWeiblich2.TabStop = True
        Me.optWeiblich2.Tag = "5"
        Me.optWeiblich2.Text = "weiblich"
        Me.optWeiblich2.UseVisualStyleBackColor = True
        '
        'optGruppen2
        '
        Me.optGruppen2.AutoSize = True
        Me.optGruppen2.ForeColor = System.Drawing.Color.MediumBlue
        Me.optGruppen2.Location = New System.Drawing.Point(12, 54)
        Me.optGruppen2.Name = "optGruppen2"
        Me.optGruppen2.Size = New System.Drawing.Size(66, 17)
        Me.optGruppen2.TabIndex = 2
        Me.optGruppen2.TabStop = True
        Me.optGruppen2.Tag = "2"
        Me.optGruppen2.Text = "Gruppen"
        Me.optGruppen2.UseVisualStyleBackColor = True
        '
        'dgvWertung2
        '
        Me.dgvWertung2.AllowUserToAddRows = False
        Me.dgvWertung2.AllowUserToDeleteRows = False
        Me.dgvWertung2.AllowUserToResizeColumns = False
        Me.dgvWertung2.AllowUserToResizeRows = False
        DataGridViewCellStyle49.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dgvWertung2.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle49
        Me.dgvWertung2.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvWertung2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle50.BackColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(227, Byte), Integer))
        DataGridViewCellStyle50.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle50.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle50.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle50.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle50.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvWertung2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle50
        Me.dgvWertung2.ColumnHeadersHeight = 21
        Me.dgvWertung2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvWertung2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Altersklasse2, Me.Wertung2, Me.Gruppe2})
        DataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle51.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle51.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle51.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle51.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle51.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle51.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvWertung2.DefaultCellStyle = DataGridViewCellStyle51
        Me.dgvWertung2.EnableHeadersVisualStyles = False
        Me.dgvWertung2.Location = New System.Drawing.Point(124, 19)
        Me.dgvWertung2.MultiSelect = False
        Me.dgvWertung2.Name = "dgvWertung2"
        Me.dgvWertung2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvWertung2.RowHeadersVisible = False
        Me.dgvWertung2.RowHeadersWidth = 51
        Me.dgvWertung2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvWertung2.RowTemplate.Height = 20
        Me.dgvWertung2.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvWertung2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvWertung2.Size = New System.Drawing.Size(210, 123)
        Me.dgvWertung2.TabIndex = 2402
        Me.dgvWertung2.Visible = False
        '
        'Altersklasse2
        '
        Me.Altersklasse2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Altersklasse2.DataPropertyName = "Altersklasse"
        Me.Altersklasse2.HeaderText = "Altersklasse"
        Me.Altersklasse2.MinimumWidth = 6
        Me.Altersklasse2.Name = "Altersklasse2"
        Me.Altersklasse2.ReadOnly = True
        Me.Altersklasse2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Altersklasse2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Altersklasse2.Width = 87
        '
        'Wertung2
        '
        Me.Wertung2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Wertung2.DataPropertyName = "idWertung2"
        Me.Wertung2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.Wertung2.DisplayStyleForCurrentCellOnly = True
        Me.Wertung2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Wertung2.HeaderText = "Wertung"
        Me.Wertung2.Items.AddRange(New Object() {"", "Robi", "Relativ", "Sinclair", "S-M", "ZK"})
        Me.Wertung2.MinimumWidth = 6
        Me.Wertung2.Name = "Wertung2"
        Me.Wertung2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Wertung2.ToolTipText = "Wertung für die 2. Platzierung"
        '
        'Gruppe2
        '
        Me.Gruppe2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gruppe2.DataPropertyName = "Gruppierung"
        Me.Gruppe2.HeaderText = "WG"
        Me.Gruppe2.Name = "Gruppe2"
        Me.Gruppe2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Gruppe2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Gruppe2.ToolTipText = "Wertungs-Gruppierung der AKs für 2. Wertung"
        Me.Gruppe2.Width = 35
        '
        'grpPlatzierung
        '
        Me.grpPlatzierung.Controls.Add(Me.optAltersklassen)
        Me.grpPlatzierung.Controls.Add(Me.optGruppen)
        Me.grpPlatzierung.Controls.Add(Me.optWettkampf)
        Me.grpPlatzierung.Controls.Add(Me.optGewichtsklassen)
        Me.grpPlatzierung.ForeColor = System.Drawing.Color.Firebrick
        Me.grpPlatzierung.Location = New System.Drawing.Point(14, 19)
        Me.grpPlatzierung.Name = "grpPlatzierung"
        Me.grpPlatzierung.Size = New System.Drawing.Size(125, 155)
        Me.grpPlatzierung.TabIndex = 1
        Me.grpPlatzierung.TabStop = False
        Me.grpPlatzierung.Text = "Platzierung "
        '
        'optAltersklassen
        '
        Me.optAltersklassen.AutoSize = True
        Me.optAltersklassen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optAltersklassen.Location = New System.Drawing.Point(11, 77)
        Me.optAltersklassen.Name = "optAltersklassen"
        Me.optAltersklassen.Size = New System.Drawing.Size(87, 17)
        Me.optAltersklassen.TabIndex = 4
        Me.optAltersklassen.Tag = "3"
        Me.optAltersklassen.Text = "Altersklassen"
        Me.ToolTip1.SetToolTip(Me.optAltersklassen, "Platzierung über die gesamte Altersklasse")
        Me.optAltersklassen.UseVisualStyleBackColor = True
        '
        'optGruppen
        '
        Me.optGruppen.AutoSize = True
        Me.optGruppen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optGruppen.Location = New System.Drawing.Point(11, 54)
        Me.optGruppen.Name = "optGruppen"
        Me.optGruppen.Size = New System.Drawing.Size(66, 17)
        Me.optGruppen.TabIndex = 3
        Me.optGruppen.Tag = "2"
        Me.optGruppen.Text = "Gruppen"
        Me.ToolTip1.SetToolTip(Me.optGruppen, "Platzierung für jede Gruppe")
        Me.optGruppen.UseVisualStyleBackColor = True
        '
        'optWettkampf
        '
        Me.optWettkampf.AutoSize = True
        Me.optWettkampf.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optWettkampf.Location = New System.Drawing.Point(11, 31)
        Me.optWettkampf.Name = "optWettkampf"
        Me.optWettkampf.Size = New System.Drawing.Size(77, 17)
        Me.optWettkampf.TabIndex = 2
        Me.optWettkampf.Tag = "1"
        Me.optWettkampf.Text = "Wettkampf"
        Me.ToolTip1.SetToolTip(Me.optWettkampf, "Platzierung über den gesamten Wettkampf")
        Me.optWettkampf.UseVisualStyleBackColor = True
        '
        'optGewichtsklassen
        '
        Me.optGewichtsklassen.AutoSize = True
        Me.optGewichtsklassen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optGewichtsklassen.Location = New System.Drawing.Point(11, 100)
        Me.optGewichtsklassen.Name = "optGewichtsklassen"
        Me.optGewichtsklassen.Size = New System.Drawing.Size(105, 17)
        Me.optGewichtsklassen.TabIndex = 5
        Me.optGewichtsklassen.Tag = "4"
        Me.optGewichtsklassen.Text = "Gewichtsklassen"
        Me.ToolTip1.SetToolTip(Me.optGewichtsklassen, "Platzierung nach Gewichtsklassen in den Altersklassen")
        Me.optGewichtsklassen.UseVisualStyleBackColor = True
        '
        'tabWertung
        '
        Me.tabWertung.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tabWertung.Controls.Add(Me.grpFaktor)
        Me.tabWertung.Controls.Add(Me.chkSameAK)
        Me.tabWertung.Controls.Add(Me.GroupBox6)
        Me.tabWertung.Location = New System.Drawing.Point(4, 22)
        Me.tabWertung.Name = "tabWertung"
        Me.tabWertung.Size = New System.Drawing.Size(515, 222)
        Me.tabWertung.TabIndex = 9
        Me.tabWertung.Text = "Wertung"
        Me.tabWertung.ToolTipText = "Details der Wertungsberechnung"
        '
        'grpFaktor
        '
        Me.grpFaktor.Controls.Add(Me.chkIgnoreSex)
        Me.grpFaktor.Controls.Add(Me.nudFaktor_w)
        Me.grpFaktor.Controls.Add(Me.Label10)
        Me.grpFaktor.Controls.Add(Me.nudFaktor_m)
        Me.grpFaktor.Controls.Add(Me.Label26)
        Me.grpFaktor.ForeColor = System.Drawing.Color.Firebrick
        Me.grpFaktor.Location = New System.Drawing.Point(216, 18)
        Me.grpFaktor.Name = "grpFaktor"
        Me.grpFaktor.Size = New System.Drawing.Size(147, 116)
        Me.grpFaktor.TabIndex = 7
        Me.grpFaktor.TabStop = False
        Me.grpFaktor.Text = "Wertungsfaktor"
        Me.ToolTip1.SetToolTip(Me.grpFaktor, "Faktor, mit dem die Wertung multipliziert wird")
        '
        'nudFaktor_w
        '
        Me.nudFaktor_w.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nudFaktor_w.DecimalPlaces = 4
        Me.nudFaktor_w.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.nudFaktor_w.InterceptArrowKeys = False
        Me.nudFaktor_w.Location = New System.Drawing.Point(65, 56)
        Me.nudFaktor_w.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.nudFaktor_w.Name = "nudFaktor_w"
        Me.nudFaktor_w.Size = New System.Drawing.Size(68, 20)
        Me.nudFaktor_w.TabIndex = 9
        Me.nudFaktor_w.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudFaktor_w.ThousandsSeparator = True
        Me.ToolTip1.SetToolTip(Me.nudFaktor_w, "Faktor, mit dem die Wertung multipliziert wird")
        Me.nudFaktor_w.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
        Me.nudFaktor_w.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label10
        '
        Me.Label10.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label10.Location = New System.Drawing.Point(15, 58)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(45, 13)
        Me.Label10.TabIndex = 307
        Me.Label10.Text = "weiblich"
        '
        'nudFaktor_m
        '
        Me.nudFaktor_m.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nudFaktor_m.DecimalPlaces = 4
        Me.nudFaktor_m.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.nudFaktor_m.InterceptArrowKeys = False
        Me.nudFaktor_m.Location = New System.Drawing.Point(65, 26)
        Me.nudFaktor_m.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.nudFaktor_m.Name = "nudFaktor_m"
        Me.nudFaktor_m.Size = New System.Drawing.Size(68, 20)
        Me.nudFaktor_m.TabIndex = 8
        Me.nudFaktor_m.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudFaktor_m.ThousandsSeparator = True
        Me.ToolTip1.SetToolTip(Me.nudFaktor_m, "Faktor, mit dem die Wertung multipliziert wird")
        Me.nudFaktor_m.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
        Me.nudFaktor_m.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label26
        '
        Me.Label26.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label26.Location = New System.Drawing.Point(15, 28)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(49, 13)
        Me.Label26.TabIndex = 306
        Me.Label26.Text = "männlich"
        '
        'chkSameAK
        '
        Me.chkSameAK.AutoSize = True
        Me.chkSameAK.ForeColor = System.Drawing.Color.Firebrick
        Me.chkSameAK.Location = New System.Drawing.Point(25, 18)
        Me.chkSameAK.Name = "chkSameAK"
        Me.chkSameAK.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.chkSameAK.Size = New System.Drawing.Size(80, 17)
        Me.chkSameAK.TabIndex = 5
        Me.chkSameAK.Text = "gleiche AK"
        Me.ToolTip1.SetToolTip(Me.chkSameAK, "alle TN werden in derselben AK  gewertet")
        Me.chkSameAK.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.cboSameAK)
        Me.GroupBox6.Location = New System.Drawing.Point(16, 19)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(178, 58)
        Me.GroupBox6.TabIndex = 6
        Me.GroupBox6.TabStop = False
        '
        'cboSameAK
        '
        Me.cboSameAK.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboSameAK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSameAK.Enabled = False
        Me.cboSameAK.FormattingEnabled = True
        Me.cboSameAK.ItemHeight = 13
        Me.cboSameAK.Location = New System.Drawing.Point(11, 24)
        Me.cboSameAK.Name = "cboSameAK"
        Me.cboSameAK.Size = New System.Drawing.Size(156, 21)
        Me.cboSameAK.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.cboSameAK, "alle TN werden in der gewählten AK  gewertet")
        '
        'tabMulti
        '
        Me.tabMulti.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tabMulti.Controls.Add(Me.dgvMulti)
        Me.tabMulti.Location = New System.Drawing.Point(4, 22)
        Me.tabMulti.Name = "tabMulti"
        Me.tabMulti.Size = New System.Drawing.Size(515, 222)
        Me.tabMulti.TabIndex = 8
        Me.tabMulti.Text = "Multi-WK"
        Me.tabMulti.ToolTipText = "Wettkämpfe zusammen durchführen"
        '
        'dgvMulti
        '
        Me.dgvMulti.AllowUserToAddRows = False
        Me.dgvMulti.AllowUserToDeleteRows = False
        Me.dgvMulti.AllowUserToResizeColumns = False
        Me.dgvMulti.AllowUserToResizeRows = False
        DataGridViewCellStyle52.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dgvMulti.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle52
        Me.dgvMulti.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvMulti.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle53.BackColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(227, Byte), Integer))
        DataGridViewCellStyle53.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle53.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMulti.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle53
        Me.dgvMulti.ColumnHeadersHeight = 21
        Me.dgvMulti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvMulti.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Multi, Me.Primär, Me.Id, Me.Bez1, Me.Datum})
        DataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle59.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle59.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle59.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle59.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(197, Byte), Integer))
        DataGridViewCellStyle59.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle59.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvMulti.DefaultCellStyle = DataGridViewCellStyle59
        Me.dgvMulti.EnableHeadersVisualStyles = False
        Me.dgvMulti.Location = New System.Drawing.Point(14, 13)
        Me.dgvMulti.Margin = New System.Windows.Forms.Padding(2)
        Me.dgvMulti.Name = "dgvMulti"
        Me.dgvMulti.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle60.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle60.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle60.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle60.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMulti.RowHeadersDefaultCellStyle = DataGridViewCellStyle60
        Me.dgvMulti.RowHeadersVisible = False
        Me.dgvMulti.RowHeadersWidth = 62
        Me.dgvMulti.RowTemplate.Height = 20
        Me.dgvMulti.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvMulti.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMulti.Size = New System.Drawing.Size(487, 163)
        Me.dgvMulti.TabIndex = 2002
        Me.dgvMulti.TabStop = False
        '
        'Multi
        '
        Me.Multi.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Multi.DataPropertyName = "Joined"
        DataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle54.NullValue = "0"
        Me.Multi.DefaultCellStyle = DataGridViewCellStyle54
        Me.Multi.FalseValue = "0"
        Me.Multi.HeaderText = "..."
        Me.Multi.IndeterminateValue = "0"
        Me.Multi.MinimumWidth = 8
        Me.Multi.Name = "Multi"
        Me.Multi.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Multi.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Multi.ToolTipText = "Verknüpfung"
        Me.Multi.TrueValue = "1"
        Me.Multi.Width = 35
        '
        'Primär
        '
        Me.Primär.DataPropertyName = "Primaer"
        DataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle55.NullValue = "0"
        Me.Primär.DefaultCellStyle = DataGridViewCellStyle55
        Me.Primär.FalseValue = "0"
        Me.Primär.HeaderText = "H"
        Me.Primär.IndeterminateValue = "0"
        Me.Primär.MinimumWidth = 8
        Me.Primär.Name = "Primär"
        Me.Primär.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Primär.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Primär.ToolTipText = "Hauptwettkampf"
        Me.Primär.TrueValue = "1"
        Me.Primär.Width = 35
        '
        'Id
        '
        Me.Id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Id.DataPropertyName = "Wettkampf"
        DataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Id.DefaultCellStyle = DataGridViewCellStyle56
        Me.Id.HeaderText = "WK"
        Me.Id.MinimumWidth = 8
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Width = 45
        '
        'Bez1
        '
        Me.Bez1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Bez1.DataPropertyName = "Bez1"
        DataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.Bez1.DefaultCellStyle = DataGridViewCellStyle57
        Me.Bez1.HeaderText = "Wettkampf"
        Me.Bez1.MinimumWidth = 8
        Me.Bez1.Name = "Bez1"
        Me.Bez1.ReadOnly = True
        Me.Bez1.ToolTipText = "zu verknüpfender Wettkampf"
        '
        'Datum
        '
        Me.Datum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Datum.DataPropertyName = "Datum"
        DataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle58.Format = "d"
        DataGridViewCellStyle58.NullValue = Nothing
        Me.Datum.DefaultCellStyle = DataGridViewCellStyle58
        Me.Datum.HeaderText = "Datum"
        Me.Datum.MinimumWidth = 8
        Me.Datum.Name = "Datum"
        Me.Datum.ReadOnly = True
        Me.Datum.Width = 85
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "Delete.png")
        Me.ImageList1.Images.SetKeyName(1, "accept-icon.png")
        Me.ImageList1.Images.SetKeyName(2, "clipart1277685.png")
        '
        'dgvBigDisc
        '
        Me.dgvBigDisc.AllowUserToAddRows = False
        Me.dgvBigDisc.AllowUserToOrderColumns = True
        Me.dgvBigDisc.AllowUserToResizeColumns = False
        Me.dgvBigDisc.AllowUserToResizeRows = False
        Me.dgvBigDisc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle61.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle61.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle61.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle61.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle61.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle61.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBigDisc.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle61
        Me.dgvBigDisc.ColumnHeadersHeight = 22
        Me.dgvBigDisc.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Available, Me.Gewicht, Me.Farbe, Me.Dicke})
        DataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle65.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle65.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle65.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle65.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle65.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle65.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBigDisc.DefaultCellStyle = DataGridViewCellStyle65
        Me.dgvBigDisc.Location = New System.Drawing.Point(533, 169)
        Me.dgvBigDisc.MultiSelect = False
        Me.dgvBigDisc.Name = "dgvBigDisc"
        Me.dgvBigDisc.RowHeadersVisible = False
        Me.dgvBigDisc.RowHeadersWidth = 10
        Me.dgvBigDisc.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.AliceBlue
        Me.dgvBigDisc.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvBigDisc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvBigDisc.Size = New System.Drawing.Size(188, 10)
        Me.dgvBigDisc.TabIndex = 280
        Me.dgvBigDisc.TabStop = False
        Me.dgvBigDisc.Visible = False
        '
        'Available
        '
        Me.Available.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Available.DataPropertyName = "available"
        Me.Available.HeaderText = ""
        Me.Available.MinimumWidth = 6
        Me.Available.Name = "Available"
        Me.Available.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Available.ToolTipText = "Scheibe vorhanden"
        Me.Available.Width = 24
        '
        'Gewicht
        '
        Me.Gewicht.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gewicht.DataPropertyName = "Gewicht"
        DataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle62.Format = "##.# kg"
        DataGridViewCellStyle62.NullValue = Nothing
        DataGridViewCellStyle62.Padding = New System.Windows.Forms.Padding(0, 0, 5, 0)
        Me.Gewicht.DefaultCellStyle = DataGridViewCellStyle62
        Me.Gewicht.HeaderText = "Gewicht"
        Me.Gewicht.MinimumWidth = 6
        Me.Gewicht.Name = "Gewicht"
        Me.Gewicht.ReadOnly = True
        Me.Gewicht.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Gewicht.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Gewicht.Width = 60
        '
        'Farbe
        '
        Me.Farbe.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Farbe.DataPropertyName = "Farbe"
        DataGridViewCellStyle63.ForeColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle63.Padding = New System.Windows.Forms.Padding(7, 2, 7, 2)
        DataGridViewCellStyle63.SelectionForeColor = System.Drawing.Color.Transparent
        Me.Farbe.DefaultCellStyle = DataGridViewCellStyle63
        Me.Farbe.HeaderText = "Farbe"
        Me.Farbe.MinimumWidth = 6
        Me.Farbe.Name = "Farbe"
        Me.Farbe.ReadOnly = True
        Me.Farbe.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Farbe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Farbe.ToolTipText = "Scheiben-Farbe"
        Me.Farbe.Width = 50
        '
        'Dicke
        '
        Me.Dicke.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Dicke.DataPropertyName = "Dicke"
        DataGridViewCellStyle64.Format = "N0"
        DataGridViewCellStyle64.NullValue = Nothing
        Me.Dicke.DefaultCellStyle = DataGridViewCellStyle64
        Me.Dicke.HeaderText = "Dicke"
        Me.Dicke.MinimumWidth = 6
        Me.Dicke.Name = "Dicke"
        Me.Dicke.ReadOnly = True
        Me.Dicke.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Dicke.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Dicke.ToolTipText = "Anzeige anpassen (mm)"
        Me.Dicke.Width = 50
        '
        'ColorDialog1
        '
        Me.ColorDialog1.AllowFullOpen = False
        '
        'lstColors
        '
        Me.lstColors.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.lstColors.FormattingEnabled = True
        Me.lstColors.ItemHeight = 17
        Me.lstColors.Location = New System.Drawing.Point(619, 180)
        Me.lstColors.Name = "lstColors"
        Me.lstColors.Size = New System.Drawing.Size(153, 4)
        Me.lstColors.TabIndex = 1113
        Me.lstColors.Visible = False
        '
        'chkFinale
        '
        Me.chkFinale.AutoSize = True
        Me.chkFinale.Enabled = False
        Me.chkFinale.ForeColor = System.Drawing.Color.MediumBlue
        Me.chkFinale.Location = New System.Drawing.Point(53, 123)
        Me.chkFinale.Name = "chkFinale"
        Me.chkFinale.Size = New System.Drawing.Size(104, 17)
        Me.chkFinale.TabIndex = 106
        Me.chkFinale.Text = "Finale-Aufteilung"
        Me.ToolTip1.SetToolTip(Me.chkFinale, "Gruppe besteht aus 2 Teilnehmern jedes Teams")
        Me.chkFinale.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.Enabled = False
        Me.btnRemove.Image = Global.Gewichtheben.My.Resources.Resources.Delete21
        Me.btnRemove.Location = New System.Drawing.Point(423, 88)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(25, 25)
        Me.btnRemove.TabIndex = 104
        Me.btnRemove.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnRemove, "letztes Team entfernen")
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Enabled = False
        Me.btnAdd.Image = Global.Gewichtheben.My.Resources.Resources.Add
        Me.btnAdd.Location = New System.Drawing.Point(423, 58)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(25, 25)
        Me.btnAdd.TabIndex = 103
        Me.btnAdd.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnAdd, "weiteres Team hinzufügen")
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'txtKurz
        '
        Me.txtKurz.Location = New System.Drawing.Point(330, 99)
        Me.txtKurz.MaxLength = 25
        Me.txtKurz.Name = "txtKurz"
        Me.txtKurz.Size = New System.Drawing.Size(163, 20)
        Me.txtKurz.TabIndex = 2015
        Me.ToolTip1.SetToolTip(Me.txtKurz, "optionale Kurzbezeichnung für den Wettkampf" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(wird in den Anzeigen anstatt WK-Nam" &
        "e benutzt)")
        '
        'chkGebührNachAk
        '
        Me.chkGebührNachAk.AutoSize = True
        Me.chkGebührNachAk.Checked = True
        Me.chkGebührNachAk.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkGebührNachAk.Location = New System.Drawing.Point(110, 314)
        Me.chkGebührNachAk.Name = "chkGebührNachAk"
        Me.chkGebührNachAk.Size = New System.Drawing.Size(67, 17)
        Me.chkGebührNachAk.TabIndex = 2028
        Me.chkGebührNachAk.Text = "nach AK"
        Me.ToolTip1.SetToolTip(Me.chkGebührNachAk, "wenn ausgewählt, kann das Startgeld für jede AK separat festgelegt werden")
        Me.chkGebührNachAk.UseVisualStyleBackColor = True
        '
        'pnlVereine
        '
        Me.pnlVereine.Location = New System.Drawing.Point(476, 38)
        Me.pnlVereine.Margin = New System.Windows.Forms.Padding(2)
        Me.pnlVereine.Name = "pnlVereine"
        Me.pnlVereine.Size = New System.Drawing.Size(17, 20)
        Me.pnlVereine.TabIndex = 2006
        '
        'txtVeranstalter
        '
        Me.txtVeranstalter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtVeranstalter.Location = New System.Drawing.Point(110, 38)
        Me.txtVeranstalter.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtVeranstalter.Name = "txtVeranstalter"
        Me.txtVeranstalter.Size = New System.Drawing.Size(367, 20)
        Me.txtVeranstalter.TabIndex = 2005
        '
        'lstVereine
        '
        Me.lstVereine.FormattingEnabled = True
        Me.lstVereine.Location = New System.Drawing.Point(278, 38)
        Me.lstVereine.Margin = New System.Windows.Forms.Padding(2)
        Me.lstVereine.Name = "lstVereine"
        Me.lstVereine.Size = New System.Drawing.Size(87, 17)
        Me.lstVereine.TabIndex = 2038
        Me.lstVereine.Visible = False
        '
        'dgvGebühr
        '
        Me.dgvGebühr.AllowUserToAddRows = False
        Me.dgvGebühr.AllowUserToDeleteRows = False
        Me.dgvGebühr.AllowUserToResizeColumns = False
        Me.dgvGebühr.AllowUserToResizeRows = False
        DataGridViewCellStyle66.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dgvGebühr.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle66
        Me.dgvGebühr.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvGebühr.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle67.BackColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(227, Byte), Integer))
        DataGridViewCellStyle67.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle67.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle67.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle67.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle67.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGebühr.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle67
        Me.dgvGebühr.ColumnHeadersHeight = 21
        Me.dgvGebühr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvGebühr.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Altersklasse, Me.Gebuehr, Me.Nachmeldung, Me.GebuehrOL, Me.NachmeldungOL})
        Me.dgvGebühr.EnableHeadersVisualStyles = False
        Me.dgvGebühr.Location = New System.Drawing.Point(191, 254)
        Me.dgvGebühr.MultiSelect = False
        Me.dgvGebühr.Name = "dgvGebühr"
        Me.dgvGebühr.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvGebühr.RowHeadersVisible = False
        Me.dgvGebühr.RowHeadersWidth = 24
        Me.dgvGebühr.RowTemplate.Height = 18
        Me.dgvGebühr.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvGebühr.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGebühr.Size = New System.Drawing.Size(301, 77)
        Me.dgvGebühr.TabIndex = 2029
        '
        'Altersklasse
        '
        Me.Altersklasse.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Altersklasse.DataPropertyName = "Altersklasse"
        DataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.Altersklasse.DefaultCellStyle = DataGridViewCellStyle68
        Me.Altersklasse.FillWeight = 72.31128!
        Me.Altersklasse.HeaderText = "AK"
        Me.Altersklasse.MinimumWidth = 6
        Me.Altersklasse.Name = "Altersklasse"
        Me.Altersklasse.ReadOnly = True
        Me.Altersklasse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Altersklasse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Altersklasse.ToolTipText = "Altersklasse"
        '
        'Gebuehr
        '
        Me.Gebuehr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gebuehr.DataPropertyName = "Gebuehr"
        DataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle69.Format = "N2"
        DataGridViewCellStyle69.NullValue = Nothing
        Me.Gebuehr.DefaultCellStyle = DataGridViewCellStyle69
        Me.Gebuehr.FillWeight = 40.0!
        Me.Gebuehr.HeaderText = "MG"
        Me.Gebuehr.MinimumWidth = 6
        Me.Gebuehr.Name = "Gebuehr"
        Me.Gebuehr.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Gebuehr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Gebuehr.ToolTipText = "Meldegebühr"
        Me.Gebuehr.Width = 40
        '
        'Nachmeldung
        '
        Me.Nachmeldung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Nachmeldung.DataPropertyName = "NM_Gebuehr"
        DataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle70.Format = "N2"
        Me.Nachmeldung.DefaultCellStyle = DataGridViewCellStyle70
        Me.Nachmeldung.FillWeight = 40.0!
        Me.Nachmeldung.HeaderText = "NMG"
        Me.Nachmeldung.MinimumWidth = 6
        Me.Nachmeldung.Name = "Nachmeldung"
        Me.Nachmeldung.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Nachmeldung.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Nachmeldung.ToolTipText = "Nachmeldegebühr"
        Me.Nachmeldung.Width = 40
        '
        'GebuehrOL
        '
        Me.GebuehrOL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.GebuehrOL.DataPropertyName = "Gebuehr_oL"
        DataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle71.Format = "N2"
        Me.GebuehrOL.DefaultCellStyle = DataGridViewCellStyle71
        Me.GebuehrOL.FillWeight = 40.0!
        Me.GebuehrOL.HeaderText = "MoL"
        Me.GebuehrOL.MinimumWidth = 6
        Me.GebuehrOL.Name = "GebuehrOL"
        Me.GebuehrOL.ToolTipText = "Meldegebühr ohne BVDG-Lizenz"
        Me.GebuehrOL.Width = 40
        '
        'NachmeldungOL
        '
        Me.NachmeldungOL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.NachmeldungOL.DataPropertyName = "NM_Gebuehr_oL"
        DataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle72.Format = "N2"
        Me.NachmeldungOL.DefaultCellStyle = DataGridViewCellStyle72
        Me.NachmeldungOL.FillWeight = 40.0!
        Me.NachmeldungOL.HeaderText = "NoL"
        Me.NachmeldungOL.MinimumWidth = 6
        Me.NachmeldungOL.Name = "NachmeldungOL"
        Me.NachmeldungOL.ToolTipText = "Nachmeldegebühr ohne BVDG-Lizenz"
        Me.NachmeldungOL.Width = 40
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(300, 102)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(28, 13)
        Me.Label38.TabIndex = 2014
        Me.Label38.Text = "Kurz"
        '
        'grpMannschaft
        '
        Me.grpMannschaft.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.grpMannschaft.Controls.Add(Me.chkFinale)
        Me.grpMannschaft.Controls.Add(Me.pnlMannschaft)
        Me.grpMannschaft.Controls.Add(Me.btnRemove)
        Me.grpMannschaft.Controls.Add(Me.btnAdd)
        Me.grpMannschaft.ForeColor = System.Drawing.Color.Firebrick
        Me.grpMannschaft.Location = New System.Drawing.Point(34, 420)
        Me.grpMannschaft.Name = "grpMannschaft"
        Me.grpMannschaft.Size = New System.Drawing.Size(459, 142)
        Me.grpMannschaft.TabIndex = 2037
        Me.grpMannschaft.TabStop = False
        Me.grpMannschaft.Text = "   Mannschaften"
        '
        'pnlMannschaft
        '
        Me.pnlMannschaft.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMannschaft.AutoScroll = True
        Me.pnlMannschaft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMannschaft.Controls.Add(Me.pnl0)
        Me.pnlMannschaft.Controls.Add(Me.pnl2)
        Me.pnlMannschaft.Controls.Add(Me.pnl1)
        Me.pnlMannschaft.Controls.Add(Me.txt2)
        Me.pnlMannschaft.Controls.Add(Me.txt1)
        Me.pnlMannschaft.Controls.Add(Me.txt0)
        Me.pnlMannschaft.Controls.Add(Me.lbl2)
        Me.pnlMannschaft.Controls.Add(Me.lbl1)
        Me.pnlMannschaft.Controls.Add(Me.lbl0)
        Me.pnlMannschaft.Enabled = False
        Me.pnlMannschaft.Location = New System.Drawing.Point(5, 19)
        Me.pnlMannschaft.Name = "pnlMannschaft"
        Me.pnlMannschaft.Size = New System.Drawing.Size(412, 99)
        Me.pnlMannschaft.TabIndex = 102
        '
        'pnl0
        '
        Me.pnl0.Location = New System.Drawing.Point(371, 10)
        Me.pnl0.Margin = New System.Windows.Forms.Padding(2)
        Me.pnl0.Name = "pnl0"
        Me.pnl0.Size = New System.Drawing.Size(17, 20)
        Me.pnl0.TabIndex = 117
        '
        'pnl2
        '
        Me.pnl2.Location = New System.Drawing.Point(371, 68)
        Me.pnl2.Margin = New System.Windows.Forms.Padding(2)
        Me.pnl2.Name = "pnl2"
        Me.pnl2.Size = New System.Drawing.Size(17, 20)
        Me.pnl2.TabIndex = 121
        '
        'pnl1
        '
        Me.pnl1.Location = New System.Drawing.Point(371, 38)
        Me.pnl1.Margin = New System.Windows.Forms.Padding(2)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(17, 20)
        Me.pnl1.TabIndex = 119
        '
        'txt2
        '
        Me.txt2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt2.Location = New System.Drawing.Point(47, 68)
        Me.txt2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txt2.Name = "txt2"
        Me.txt2.Size = New System.Drawing.Size(325, 20)
        Me.txt2.TabIndex = 120
        '
        'txt1
        '
        Me.txt1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt1.Location = New System.Drawing.Point(47, 38)
        Me.txt1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txt1.Name = "txt1"
        Me.txt1.Size = New System.Drawing.Size(325, 20)
        Me.txt1.TabIndex = 118
        '
        'txt0
        '
        Me.txt0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt0.Location = New System.Drawing.Point(47, 10)
        Me.txt0.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txt0.Name = "txt0"
        Me.txt0.Size = New System.Drawing.Size(325, 20)
        Me.txt0.TabIndex = 116
        '
        'lbl2
        '
        Me.lbl2.ForeColor = System.Drawing.Color.MediumBlue
        Me.lbl2.Location = New System.Drawing.Point(3, 70)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(40, 25)
        Me.lbl2.TabIndex = 114
        Me.lbl2.Text = "Gast 2"
        Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lbl1
        '
        Me.lbl1.ForeColor = System.Drawing.Color.MediumBlue
        Me.lbl1.Location = New System.Drawing.Point(3, 40)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(40, 25)
        Me.lbl1.TabIndex = 112
        Me.lbl1.Text = "Gast 1"
        Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lbl0
        '
        Me.lbl0.ForeColor = System.Drawing.Color.MediumBlue
        Me.lbl0.Location = New System.Drawing.Point(3, 1)
        Me.lbl0.Name = "lbl0"
        Me.lbl0.Size = New System.Drawing.Size(40, 34)
        Me.lbl0.TabIndex = 110
        Me.lbl0.Text = "Heim"
        Me.lbl0.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(189, 102)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(20, 13)
        Me.Label5.TabIndex = 2012
        Me.Label5.Text = "bis"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'dpkDatumBis
        '
        Me.dpkDatumBis.Checked = False
        Me.dpkDatumBis.CustomFormat = ""
        Me.dpkDatumBis.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dpkDatumBis.Location = New System.Drawing.Point(212, 100)
        Me.dpkDatumBis.Name = "dpkDatumBis"
        Me.dpkDatumBis.Size = New System.Drawing.Size(75, 20)
        Me.dpkDatumBis.TabIndex = 2013
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label22.Location = New System.Drawing.Point(194, 338)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(31, 13)
        Me.Label22.TabIndex = 2033
        Me.Label22.Text = "Land"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label30.Location = New System.Drawing.Point(275, 338)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(27, 13)
        Me.Label30.TabIndex = 2035
        Me.Label30.Text = "Liga"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label20.Location = New System.Drawing.Point(113, 338)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(37, 13)
        Me.Label20.TabIndex = 2031
        Me.Label20.Text = "Verein"
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(41, 344)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(63, 30)
        Me.Label17.TabIndex = 2030
        Me.Label17.Text = "Startgeld" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Mannschaft"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label17.UseCompatibleTextRendering = True
        '
        'txtStartgeld_M_Land
        '
        Me.txtStartgeld_M_Land.Location = New System.Drawing.Point(191, 355)
        Me.txtStartgeld_M_Land.Name = "txtStartgeld_M_Land"
        Me.txtStartgeld_M_Land.Size = New System.Drawing.Size(60, 20)
        Me.txtStartgeld_M_Land.TabIndex = 2034
        Me.txtStartgeld_M_Land.Tag = "Land"
        Me.txtStartgeld_M_Land.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtStartgeld_M_Verein
        '
        Me.txtStartgeld_M_Verein.Location = New System.Drawing.Point(110, 355)
        Me.txtStartgeld_M_Verein.Name = "txtStartgeld_M_Verein"
        Me.txtStartgeld_M_Verein.Size = New System.Drawing.Size(60, 20)
        Me.txtStartgeld_M_Verein.TabIndex = 2032
        Me.txtStartgeld_M_Verein.Tag = "Verein"
        Me.txtStartgeld_M_Verein.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtStartgeld_M_Liga
        '
        Me.txtStartgeld_M_Liga.Location = New System.Drawing.Point(272, 355)
        Me.txtStartgeld_M_Liga.Name = "txtStartgeld_M_Liga"
        Me.txtStartgeld_M_Liga.Size = New System.Drawing.Size(60, 20)
        Me.txtStartgeld_M_Liga.TabIndex = 2036
        Me.txtStartgeld_M_Liga.Tag = "Liga"
        Me.txtStartgeld_M_Liga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dpkMeldeschlussNachmeldung
        '
        Me.dpkMeldeschlussNachmeldung.CustomFormat = ""
        Me.dpkMeldeschlussNachmeldung.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dpkMeldeschlussNachmeldung.Location = New System.Drawing.Point(110, 283)
        Me.dpkMeldeschlussNachmeldung.Name = "dpkMeldeschlussNachmeldung"
        Me.dpkMeldeschlussNachmeldung.Size = New System.Drawing.Size(75, 20)
        Me.dpkMeldeschlussNachmeldung.TabIndex = 2027
        '
        'dpkMeldeschluss
        '
        Me.dpkMeldeschluss.CustomFormat = ""
        Me.dpkMeldeschluss.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dpkMeldeschluss.Location = New System.Drawing.Point(110, 254)
        Me.dpkMeldeschluss.Name = "dpkMeldeschluss"
        Me.dpkMeldeschluss.Size = New System.Drawing.Size(75, 20)
        Me.dpkMeldeschluss.TabIndex = 2025
        '
        'btnModus
        '
        Me.btnModus.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnModus.Image = Global.Gewichtheben.My.Resources.Resources.Add
        Me.btnModus.Location = New System.Drawing.Point(469, 66)
        Me.btnModus.Name = "btnModus"
        Me.btnModus.Size = New System.Drawing.Size(24, 25)
        Me.btnModus.TabIndex = 2009
        Me.btnModus.TabStop = False
        Me.btnModus.UseVisualStyleBackColor = True
        '
        'cboOrt
        '
        Me.cboOrt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cboOrt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboOrt.DisplayMember = "Ort"
        Me.cboOrt.FormattingEnabled = True
        Me.cboOrt.Location = New System.Drawing.Point(110, 129)
        Me.cboOrt.Name = "cboOrt"
        Me.cboOrt.Size = New System.Drawing.Size(383, 21)
        Me.cboOrt.TabIndex = 2017
        '
        'cboBezeichnung3
        '
        Me.cboBezeichnung3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cboBezeichnung3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBezeichnung3.DisplayMember = "Bezeichnung3"
        Me.cboBezeichnung3.FormattingEnabled = True
        Me.cboBezeichnung3.Location = New System.Drawing.Point(110, 222)
        Me.cboBezeichnung3.Name = "cboBezeichnung3"
        Me.cboBezeichnung3.Size = New System.Drawing.Size(383, 21)
        Me.cboBezeichnung3.TabIndex = 2023
        '
        'cboBezeichnung2
        '
        Me.cboBezeichnung2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cboBezeichnung2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBezeichnung2.DisplayMember = "Bezeichnung2"
        Me.cboBezeichnung2.FormattingEnabled = True
        Me.cboBezeichnung2.Location = New System.Drawing.Point(110, 191)
        Me.cboBezeichnung2.Name = "cboBezeichnung2"
        Me.cboBezeichnung2.Size = New System.Drawing.Size(383, 21)
        Me.cboBezeichnung2.TabIndex = 2021
        '
        'cboBezeichnung1
        '
        Me.cboBezeichnung1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cboBezeichnung1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBezeichnung1.DisplayMember = "Bezeichnung1"
        Me.cboBezeichnung1.FormattingEnabled = True
        Me.cboBezeichnung1.Location = New System.Drawing.Point(110, 160)
        Me.cboBezeichnung1.Name = "cboBezeichnung1"
        Me.cboBezeichnung1.Size = New System.Drawing.Size(383, 21)
        Me.cboBezeichnung1.TabIndex = 2019
        '
        'dpkDatum
        '
        Me.dpkDatum.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dpkDatum.Location = New System.Drawing.Point(110, 100)
        Me.dpkDatum.Name = "dpkDatum"
        Me.dpkDatum.Size = New System.Drawing.Size(75, 20)
        Me.dpkDatum.TabIndex = 2011
        '
        'cboModus
        '
        Me.cboModus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cboModus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboModus.DisplayMember = "Bezeichnung"
        Me.cboModus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboModus.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboModus.Location = New System.Drawing.Point(110, 68)
        Me.cboModus.Name = "cboModus"
        Me.cboModus.Size = New System.Drawing.Size(354, 21)
        Me.cboModus.TabIndex = 2008
        Me.cboModus.ValueMember = "ID"
        '
        'chkIgnoreSex
        '
        Me.chkIgnoreSex.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkIgnoreSex.ForeColor = System.Drawing.Color.MediumBlue
        Me.chkIgnoreSex.Location = New System.Drawing.Point(14, 82)
        Me.chkIgnoreSex.Name = "chkIgnoreSex"
        Me.chkIgnoreSex.Size = New System.Drawing.Size(120, 24)
        Me.chkIgnoreSex.TabIndex = 308
        Me.chkIgnoreSex.Text = "gemeinsam werten"
        Me.chkIgnoreSex.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.chkIgnoreSex, "keine Unterteilung in Geschlechter")
        Me.chkIgnoreSex.UseVisualStyleBackColor = True
        '
        'frmWettkampf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1060, 572)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.lstVereine)
        Me.Controls.Add(Me.pnlVereine)
        Me.Controls.Add(Me.txtVeranstalter)
        Me.Controls.Add(Me.chkGebührNachAk)
        Me.Controls.Add(Me.dgvGebühr)
        Me.Controls.Add(Me.txtKurz)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.grpMannschaft)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dpkDatumBis)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtStartgeld_M_Land)
        Me.Controls.Add(Me.txtStartgeld_M_Verein)
        Me.Controls.Add(Me.txtStartgeld_M_Liga)
        Me.Controls.Add(Me.dpkMeldeschlussNachmeldung)
        Me.Controls.Add(Label8)
        Me.Controls.Add(Me.dpkMeldeschluss)
        Me.Controls.Add(Me.btnModus)
        Me.Controls.Add(Me.cboOrt)
        Me.Controls.Add(Bezeichnung_3Label)
        Me.Controls.Add(Me.cboBezeichnung3)
        Me.Controls.Add(Bezeichnung_2Label)
        Me.Controls.Add(Me.cboBezeichnung2)
        Me.Controls.Add(Bezeichnung_1Label)
        Me.Controls.Add(Me.cboBezeichnung1)
        Me.Controls.Add(OrtLabel)
        Me.Controls.Add(VeranstalterLabel)
        Me.Controls.Add(DatumLabel)
        Me.Controls.Add(Me.dpkDatum)
        Me.Controls.Add(ModusLabel)
        Me.Controls.Add(Me.cboModus)
        Me.Controls.Add(Me.lstColors)
        Me.Controls.Add(Me.dgvBigDisc)
        Me.Controls.Add(Me.tabCollection)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Label9)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWettkampf"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Neuen Wettkampf anlegen"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.nudSteigerung2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudSteigerung1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.nudVmin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMmin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLmin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMmax, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudVmax, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLmax, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMwert, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudVwert, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLwert, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.nudEqual_w, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_w4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudEqual_m, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_w3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_w2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_w1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_w0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_w, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudG_m0, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        CType(Me.nudMinAge, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabCollection.ResumeLayout(False)
        Me.tabAthletik.ResumeLayout(False)
        Me.tabAthletik.PerformLayout()
        CType(Me.dgvAthletik, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDefaults, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabAusnahme.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.grpStossen.ResumeLayout(False)
        Me.grpStossen.PerformLayout()
        CType(Me.nudRestrict_S_AK, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudRestrict_S_V, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpReissen.ResumeLayout(False)
        Me.grpReissen.PerformLayout()
        CType(Me.nudRestrict_R_V, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudRestrict_R_AK, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGewichtsgruppen.ResumeLayout(False)
        Me.tabGewichtsgruppen.PerformLayout()
        Me.tabAltersgruppen.ResumeLayout(False)
        Me.tabAltersgruppen.PerformLayout()
        CType(Me.dgvAKs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAGs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMannschaft.ResumeLayout(False)
        Me.tabMannschaft.PerformLayout()
        Me.pnlLiga.ResumeLayout(False)
        Me.pnlLiga.PerformLayout()
        Me.pnlLand.ResumeLayout(False)
        Me.pnlLand.PerformLayout()
        Me.pnlVerein.ResumeLayout(False)
        Me.pnlVerein.PerformLayout()
        CType(Me.nudVrel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabPlatzierung.ResumeLayout(False)
        Me.tabPlatzierung.PerformLayout()
        Me.grpPlatzierung2.ResumeLayout(False)
        Me.grpPlatzierung2.PerformLayout()
        Me.grpWertung2.ResumeLayout(False)
        CType(Me.dgvWertung2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPlatzierung.ResumeLayout(False)
        Me.grpPlatzierung.PerformLayout()
        Me.tabWertung.ResumeLayout(False)
        Me.tabWertung.PerformLayout()
        Me.grpFaktor.ResumeLayout(False)
        CType(Me.nudFaktor_w, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFaktor_m, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.tabMulti.ResumeLayout(False)
        CType(Me.dgvMulti, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBigDisc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvGebühr, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpMannschaft.ResumeLayout(False)
        Me.grpMannschaft.PerformLayout()
        Me.pnlMannschaft.ResumeLayout(False)
        Me.pnlMannschaft.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboKR As System.Windows.Forms.ComboBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDatei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeichern As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuBeenden As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOptionen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMeldungen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuKonflikte As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkBigdisc As System.Windows.Forms.CheckBox
    Friend WithEvents txtZK_m As TextBox
    Friend WithEvents txtZK_w As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents chkFlagge As CheckBox
    Friend WithEvents btnSave As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents optSex As RadioButton
    Friend WithEvents chk5kg As CheckBox
    Friend WithEvents chk7kg As CheckBox
    Friend WithEvents nudVmin As NumericUpDown
    Friend WithEvents nudMmin As NumericUpDown
    Friend WithEvents nudLmin As NumericUpDown
    Friend WithEvents nudMmax As NumericUpDown
    Friend WithEvents nudVmax As NumericUpDown
    Friend WithEvents nudLmax As NumericUpDown
    Friend WithEvents nudMwert As NumericUpDown
    Friend WithEvents nudVwert As NumericUpDown
    Friend WithEvents nudLwert As NumericUpDown
    Friend WithEvents chkLand As CheckBox
    Friend WithEvents chkVerein As CheckBox
    Friend WithEvents chkLiga As CheckBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents txtR_w As TextBox
    Friend WithEvents txtR_m As TextBox
    Friend WithEvents txtS_w As TextBox
    Friend WithEvents txtS_m As TextBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents chkEqual_m As CheckBox
    Friend WithEvents Label31 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents nudEqual_w As NumericUpDown
    Friend WithEvents nudG_w4 As NumericUpDown
    Friend WithEvents nudEqual_m As NumericUpDown
    Friend WithEvents nudG_w3 As NumericUpDown
    Friend WithEvents nudG_m4 As NumericUpDown
    Friend WithEvents nudG_w2 As NumericUpDown
    Friend WithEvents nudG_m3 As NumericUpDown
    Friend WithEvents nudG_w1 As NumericUpDown
    Friend WithEvents nudG_m2 As NumericUpDown
    Friend WithEvents nudG_w0 As NumericUpDown
    Friend WithEvents nudG_m1 As NumericUpDown
    Friend WithEvents nudG_m0 As NumericUpDown
    Friend WithEvents lblG_w4 As Label
    Friend WithEvents lblG_w3 As Label
    Friend WithEvents lblG_m4 As Label
    Friend WithEvents lblG_w2 As Label
    Friend WithEvents lblG_m3 As Label
    Friend WithEvents lblG_w1 As Label
    Friend WithEvents lblG_m2 As Label
    Friend WithEvents lblG_m1 As Label
    Friend WithEvents btnExit As Button
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents chkLadd As CheckBox
    Friend WithEvents chkVadd As CheckBox
    Friend WithEvents chkLsex As CheckBox
    Friend WithEvents chkMadd As CheckBox
    Friend WithEvents chkVsex As CheckBox
    Friend WithEvents chkMsex As CheckBox
    Friend WithEvents chkEqual_w As CheckBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents mnuNeu As ToolStripMenuItem
    Friend WithEvents mnuLöschen As ToolStripMenuItem
    Friend WithEvents txtHantelstange As TextBox
    Friend WithEvents nudG_m As NumericUpDown
    Friend WithEvents Label37 As Label
    Friend WithEvents lblG_w0 As Label
    Friend WithEvents lblG_m0 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents nudG_w As NumericUpDown
    Friend WithEvents chkRegel20 As CheckBox
    Friend WithEvents nudSteigerung2 As NumericUpDown
    Friend WithEvents nudSteigerung1 As NumericUpDown
    Friend WithEvents btnDiscColor As Button
    Friend WithEvents chkInternational As CheckBox
    Friend WithEvents tabCollection As TabControl
    Friend WithEvents tabGewichtsgruppen As TabPage
    Friend WithEvents tabAthletik As TabPage
    Friend WithEvents dgvDefaults As DataGridView
    Friend WithEvents lblBank As Label
    Friend WithEvents tabAltersgruppen As TabPage
    Friend WithEvents btnAG_Add As Button
    Friend WithEvents tabMannschaft As TabPage
    Friend WithEvents pnlLand As Panel
    Friend WithEvents pnlVerein As Panel
    Friend WithEvents lblLadd As Label
    Friend WithEvents lblLsex As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents lblVadd As Label
    Friend WithEvents lblVsex As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents lblVrel As Label
    Friend WithEvents nudVrel As NumericUpDown
    Friend WithEvents optAK As RadioButton
    Friend WithEvents pnlLiga As Panel
    Friend WithEvents Label4 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents btnAG_Remove As Button
    Friend WithEvents tabAusnahme As TabPage
    Friend WithEvents dgvAGs As DataGridView
    Friend WithEvents Label21 As Label
    Friend WithEvents nudMinAge As NumericUpDown
    Friend WithEvents dgvAKs As DataGridView
    Friend WithEvents Label25 As Label
    Friend WithEvents nudRestrict_S_V As NumericUpDown
    Friend WithEvents Label39 As Label
    Friend WithEvents nudRestrict_S_AK As NumericUpDown
    Friend WithEvents chkRestrict_R As CheckBox
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents chkRestrict_S As CheckBox
    Friend WithEvents grpReissen As GroupBox
    Friend WithEvents Label46 As Label
    Friend WithEvents nudRestrict_R_V As NumericUpDown
    Friend WithEvents Label42 As Label
    Friend WithEvents nudRestrict_R_AK As NumericUpDown
    Friend WithEvents grpStossen As GroupBox
    Friend WithEvents tabPlatzierung As TabPage
    Friend WithEvents grpPlatzierung As GroupBox
    Friend WithEvents optGruppen As RadioButton
    Friend WithEvents optWettkampf As RadioButton
    Friend WithEvents optGewichtsklassen As RadioButton
    Friend WithEvents optAltersklassen As RadioButton
    Friend WithEvents chkPlatzierung2 As CheckBox
    Friend WithEvents grpPlatzierung2 As GroupBox
    Friend WithEvents optGewichtsklassen2 As RadioButton
    Friend WithEvents optAltersklassen2 As RadioButton
    Friend WithEvents optGruppen2 As RadioButton
    Friend WithEvents optWeiblich2 As RadioButton
    Friend WithEvents optWettkampf2 As RadioButton
    Friend WithEvents grpWertung2 As GroupBox
    Friend WithEvents chkLosnummer As CheckBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Check As DataGridViewCheckBoxColumn
    Friend WithEvents AK As DataGridViewTextBoxColumn
    Friend WithEvents JG As DataGridViewTextBoxColumn
    Friend WithEvents optAG_w As RadioButton
    Friend WithEvents optAG_m As RadioButton
    Friend WithEvents Sex As DataGridViewTextBoxColumn
    Friend WithEvents Jahrgang As DataGridViewTextBoxColumn
    Friend WithEvents dgvAthletik As DataGridView
    Friend WithEvents dgvBigDisc As DataGridView
    Friend WithEvents ColorDialog1 As ColorDialog
    Friend WithEvents lstColors As ListBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents chkVersucheSortieren As CheckBox
    Friend WithEvents chkWeitererVersuch As CheckBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents mnuExtras As ToolStripMenuItem
    Friend WithEvents mnuMauszeiger As ToolStripMenuItem
    Friend WithEvents dgvMulti As DataGridView
    Friend WithEvents pnlVereine As Panel
    Friend WithEvents txtVeranstalter As TextBox
    Friend WithEvents lstVereine As ListBox
    Friend WithEvents chkGebührNachAk As CheckBox
    Friend WithEvents dgvGebühr As DataGridView
    Friend WithEvents Altersklasse As DataGridViewTextBoxColumn
    Friend WithEvents Gebuehr As DataGridViewTextBoxColumn
    Friend WithEvents Nachmeldung As DataGridViewTextBoxColumn
    Friend WithEvents GebuehrOL As DataGridViewTextBoxColumn
    Friend WithEvents NachmeldungOL As DataGridViewTextBoxColumn
    Friend WithEvents txtKurz As TextBox
    Friend WithEvents Label38 As Label
    Friend WithEvents grpMannschaft As GroupBox
    Friend WithEvents chkFinale As CheckBox
    Friend WithEvents pnlMannschaft As Panel
    Friend WithEvents pnl0 As Panel
    Friend WithEvents pnl2 As Panel
    Friend WithEvents pnl1 As Panel
    Friend WithEvents txt2 As TextBox
    Friend WithEvents txt1 As TextBox
    Friend WithEvents txt0 As TextBox
    Friend WithEvents lbl2 As Label
    Friend WithEvents lbl1 As Label
    Friend WithEvents lbl0 As Label
    Friend WithEvents btnRemove As Button
    Friend WithEvents btnAdd As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents dpkDatumBis As DateTimePicker
    Friend WithEvents Label22 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents txtStartgeld_M_Land As TextBox
    Friend WithEvents txtStartgeld_M_Verein As TextBox
    Friend WithEvents txtStartgeld_M_Liga As TextBox
    Friend WithEvents dpkMeldeschlussNachmeldung As DateTimePicker
    Friend WithEvents dpkMeldeschluss As DateTimePicker
    Friend WithEvents btnModus As Button
    Friend WithEvents cboOrt As ComboBox
    Friend WithEvents cboBezeichnung3 As ComboBox
    Friend WithEvents cboBezeichnung2 As ComboBox
    Friend WithEvents cboBezeichnung1 As ComboBox
    Friend WithEvents dpkDatum As DateTimePicker
    Friend WithEvents cboModus As ComboBox
    Friend WithEvents Available As DataGridViewCheckBoxColumn
    Friend WithEvents Gewicht As DataGridViewTextBoxColumn
    Friend WithEvents Farbe As DataGridViewTextBoxColumn
    Friend WithEvents Dicke As DataGridViewTextBoxColumn
    Friend WithEvents cboWertung2 As ComboBox
    Friend WithEvents dgvWertung2 As DataGridView
    Friend WithEvents Checked As DataGridViewCheckBoxColumn
    Friend WithEvents Bezeichnung As DataGridViewTextBoxColumn
    Friend WithEvents AKs As DataGridViewTextBoxColumn
    Friend WithEvents D_Sex As DataGridViewTextBoxColumn
    Friend WithEvents ak5 As DataGridViewTextBoxColumn
    Friend WithEvents ak6 As DataGridViewTextBoxColumn
    Friend WithEvents ak7 As DataGridViewTextBoxColumn
    Friend WithEvents ak8 As DataGridViewTextBoxColumn
    Friend WithEvents ak9 As DataGridViewTextBoxColumn
    Friend WithEvents ak10 As DataGridViewTextBoxColumn
    Friend WithEvents ak11 As DataGridViewTextBoxColumn
    Friend WithEvents ak12 As DataGridViewTextBoxColumn
    Friend WithEvents ak13 As DataGridViewTextBoxColumn
    Friend WithEvents ak14 As DataGridViewTextBoxColumn
    Friend WithEvents ak15 As DataGridViewTextBoxColumn
    Friend WithEvents ak16 As DataGridViewTextBoxColumn
    Friend WithEvents ak17 As DataGridViewTextBoxColumn
    Friend WithEvents btnDefault As Button
    Friend WithEvents tabMulti As TabPage
    Friend WithEvents Multi As DataGridViewCheckBoxColumn
    Friend WithEvents Primär As DataGridViewCheckBoxColumn
    Friend WithEvents Id As DataGridViewTextBoxColumn
    Friend WithEvents Bez1 As DataGridViewTextBoxColumn
    Friend WithEvents Datum As DataGridViewTextBoxColumn
    Friend WithEvents Altersklasse2 As DataGridViewTextBoxColumn
    Friend WithEvents Wertung2 As DataGridViewComboBoxColumn
    Friend WithEvents Gruppe2 As DataGridViewTextBoxColumn
    Friend WithEvents chkUseAgeGroups As CheckBox
    Friend WithEvents tabWertung As TabPage
    Friend WithEvents chkSameAK As CheckBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents cboSameAK As ComboBox
    Friend WithEvents nudFaktor_m As NumericUpDown
    Friend WithEvents grpFaktor As GroupBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents nudFaktor_w As NumericUpDown
    Friend WithEvents chkIgnoreSex As CheckBox
End Class
