﻿
Imports System.ComponentModel
Imports MySqlConnector

Public Class Auswertung_Länder
    Dim Bereich As String = "Protokoll"

    Dim Query As String
    Dim cmd As MySqlCommand

    Dim dtHeber As New DataTable
    Dim dtGroups As New DataTable

    Dim dvHeber As DataView
    Dim dvGroups As DataView

    WithEvents bsGroups As BindingSource
    WithEvents bsHeber As BindingSource

    Dim dtVorlage As New DataTable
    Dim dvVorlage As DataView
    Private Vorlage As String

    Dim report As FastReport.Report

    Dim W As New Dictionary(Of String, Integer)

    Private Sub Me_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        NativeMethods.INI_Write(Bereich, "Vorlage", Vorlage, App_IniFile)
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load

        Bereich = Bereich & "_" & Tag.ToString

        formMain.mnuLänderwertung.Enabled = Glob.dvMannschaft.Count > 0 AndAlso Not Glob.dvMannschaft.Find(1) = -1
        formMain.mnuVereinswertung.Enabled = Glob.dvMannschaft.Count > 0 AndAlso Not Glob.dvMannschaft.Find(0) = -1

        Dim msg = String.Empty
        If Tag.ToString = "Länderwertung" AndAlso Not formMain.mnuLänderwertung.Enabled OrElse Tag.ToString = "Vereinswertung" AndAlso Not formMain.mnuVereinswertung.Enabled Then
            msg = "Keine " & Tag.ToString & " gefunden"
            Using New Centered_MessageBox(Me)
                MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
            Close()
            Return
        End If

        Cursor = Cursors.WaitCursor

        W.Add("Vereinswertung", 0)
        W.Add("Länderwertung", 1)

#Region "Vorlage"
        Vorlage = NativeMethods.INI_Read(Bereich, "Vorlage", App_IniFile)
        If Vorlage.Equals("?") Then Vorlage = String.Empty

        dtVorlage.Columns.Add("FileName", GetType(String))
        dtVorlage.Columns.Add("Directory", GetType(String))
        'Dim cols(1) As DataColumn
        'cols(0) = dtVorlage.Columns("FileName")
        'cols(1) = dtVorlage.Columns("Directory")
        'dtVorlage.PrimaryKey = cols

        Dim v = Directory.GetFiles(Path.Combine(Application.StartupPath, "Report"), Bereich + "*.frx", SearchOption.AllDirectories).ToList
        For i = 0 To v.Count - 1
            v(i) = Replace(v(i), Path.Combine(Application.StartupPath, "Report") + "\", "")
            v(i) = Replace(v(i), ".frx", "")
            dtVorlage.Rows.Add(v(i), Path.Combine(Application.StartupPath, "Report"))
        Next
        dvVorlage = New DataView(dtVorlage)
        dvVorlage.Sort = "FileName"

        With cboVorlage
            .DataSource = dvVorlage
            .DisplayMember = "FileName"
            .ValueMember = "Directory"
            '.SelectedIndex = .FindStringExact(Vorlage)
        End With
#End Region

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        ' Defaults der Wertung
        'Dim min = CInt(Glob.dvMannschaft(W(Bereich))!min)
        'Dim max = CInt(Glob.dvMannschaft(W(Bereich))!max)
        'Dim gewertet = CInt(Glob.dvMannschaft(W(Bereich))!gewertet)
        'Dim m_w = CBool(Glob.dvMannschaft(W(Bereich))!m_w)
        'Dim multi = CBool(Glob.dvMannschaft(W(Bereich))!multi)
        'Dim relativ = 0
        'If Not IsDBNull(Glob.dvMannschaft(W(Bereich))!Relativ) Then relativ = CInt(Glob.dvMannschaft(W(Bereich))!relativ)
        'Dim sex = String.Empty

        Dim HeberSort = String.Empty

#Region "Daten"
        Dim tmp As New DataTable
        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                If Tag.ToString.Equals("Länderwertung") Then
                    Query = "Select m.Losnummer, a.Nachname, a.Vorname, a.Geschlecht Sex, m.Laenderwertung Mannschaft, v.Region Gruppe, r.region_name Gruppierung, r.region_3 LV, " &
                            "rs." & If(Wettkampf.Technikwertung, "Gesamt", "Wertung2") & " Wertung, IfNull(v.Kurzname, v.Vereinsname) Vereinsname " &
                            "From meldung m " &
                            "Left Join athleten a On a.idTeilnehmer = m.Teilnehmer " &
                            "Left Join verein v On v.idVerein = a.Verein " &
                            "Left Join results rs On rs.Teilnehmer = m.Teilnehmer And rs.Wettkampf = m.Wettkampf " &
                            "Left Join region r On r.region_id = v.Region " &
                            "Where m.Wettkampf = " & Wettkampf.ID & " And m.Laenderwertung > 0 And m.attend = true And m.a_K = False " &
                            "ORDER BY v.Region, rs." & If(Wettkampf.Technikwertung, "Gesamt", "Wertung2") & " DESC;"

                ElseIf Tag.ToString.Equals("Vereinswertung") Then
                    If Wettkampf.Modus < 120 Then ' alle Schüler-WKs
                        Query = "Select m.Losnummer, a.Nachname, a.Vorname, a.Geschlecht Sex, m.Vereinswertung Mannschaft, m.Relativ_W, v.idVerein Gruppe, IfNull(v.Kurzname, v.Vereinsname) Gruppierung, r.region_3 LV, " &
                                "rs.Gesamt Wertung " &
                                "From meldung m " &
                                "Left Join athleten a On a.idTeilnehmer = m.Teilnehmer " &
                                "Left Join verein v On v.idVerein = a.Verein " &
                                "Left Join results rs On rs.Teilnehmer = m.Teilnehmer And rs.Wettkampf = m.Wettkampf " &
                                "Left Join region r On r.region_id = v.Region " &
                                "Where m.Wettkampf = " & Wettkampf.ID & " And m.Vereinswertung <> 0 And m.attend = true And m.a_K = False " &
                                "ORDER BY v.idVerein, rs.Gesamt DESC;"
                    Else
                        ' IDJM --> Relativwertung
                        'Query = "Select m.Losnummer, a.Nachname, a.Vorname, a.Geschlecht Sex, m.Vereinswertung Mannschaft, m.Relativ_W, v.idVerein Gruppe, IfNull(v.Kurzname, v.Vereinsname) Gruppierung, reg.region_3 LV, " &
                        '        "rs.Wertung2 Wertung, If(If(a.Geschlecht = 'm', rel.m, rel.w) = 0, round(m.Wiegen, 1), If(a.Geschlecht = 'm', rel.m, rel.w)) Relativ, " &
                        '        "If(rel.m = 0, round(m.Wiegen, 1), rel.m) Relativ_M, If(r.R_Max < 0, 0, r.R_Max) maxR, If(s.S_Max < 0, 0, s.S_Max) maxS " &
                        '        "From meldung m " &
                        '        "Left Join (Select Teilnehmer, Max(HLast*Wertung) R_Max " &
                        '        "FROM reissen WHERE Wettkampf = " & Wettkampf.ID & " GROUP BY Teilnehmer) r On m.Teilnehmer = r.Teilnehmer " &
                        '        "Left Join (Select Teilnehmer, Max(HLast*Wertung) S_Max " &
                        '        "FROM stossen WHERE Wettkampf = " & Wettkampf.ID & " GROUP BY Teilnehmer) s On m.Teilnehmer = s.Teilnehmer " &
                        '        "Left Join athleten a On a.idTeilnehmer = m.Teilnehmer " &
                        '        "Left Join verein v On v.idVerein = a.Verein " &
                        '        "Left Join results rs On rs.Teilnehmer = m.Teilnehmer And rs.Wettkampf = m.Wettkampf " &
                        '        "Left Join region reg On reg.region_id = v.Region " &
                        '        "LEFT JOIN relativabzug rel ON rel.Gewicht = Ceiling(round(m.Wiegen, 1)) " &
                        '        "Where m.Wettkampf = " & Wettkampf.ID & " And m.Vereinswertung <> 0 And m.attend = true And m.a_K = False " &
                        '        "ORDER BY v.idVerein, rs.Wertung2 DESC;"





                        ' Heben_Platz in Punkte umrechnen --> Gesamt = Summe aller Punkte = Grundlage für Platzierung
                        Query = "select m.Teilnehmer, a.Nachname, a.Vorname, a.Geschlecht, m.AK, m.GK, r.ZK, 0.0 Abzug, r.Heben_Platz Wertung, a.Verein, IfNull(v.Kurzname, v.Vereinsname) Vereinsname,
                                     case 
	                                    when r.Heben_Platz = 1 then 16
	                                    when r.Heben_Platz = 2 then 14
	                                    when r.Heben_Platz = 3 then 13
	                                    when r.Heben_Platz = 4 then 12
	                                    when r.Heben_Platz = 5 then 11
	                                    when r.Heben_Platz = 6 then 10
	                                    when r.Heben_Platz = 7 then 9
	                                    when r.Heben_Platz = 8 then 8
	                                    when r.Heben_Platz = 9 then 7
	                                    when r.Heben_Platz = 10 then 6
	                                    when r.Heben_Platz = 11 then 5
	                                    when r.Heben_Platz = 12 then 4
	                                    when r.Heben_Platz = 13 then 3
	                                    when r.Heben_Platz = 14 then 2
	                                    when r.Heben_Platz = 15 then 1
                                        else 0
                                    end Punkte,
                                    sum(
	                                    case 
		                                    when r.Heben_Platz = 1 then 16
		                                    when r.Heben_Platz = 2 then 14
		                                    when r.Heben_Platz = 3 then 13
		                                    when r.Heben_Platz = 4 then 12
		                                    when r.Heben_Platz = 5 then 11
		                                    when r.Heben_Platz = 6 then 10
		                                    when r.Heben_Platz = 7 then 9
		                                    when r.Heben_Platz = 8 then 8
		                                    when r.Heben_Platz = 9 then 7
		                                    when r.Heben_Platz = 10 then 6
		                                    when r.Heben_Platz = 11 then 5
		                                    when r.Heben_Platz = 12 then 4
		                                    when r.Heben_Platz = 13 then 3
		                                    when r.Heben_Platz = 14 then 2
		                                    when r.Heben_Platz = 15 then 1
		                                    else 0
	                                    end
                                    ) over (partition by a.Verein) Gesamt, 0 Platz
                                from meldung m
                                left join athleten a on a.idTeilnehmer = m.Teilnehmer
                                left join results r on r.Wettkampf = m.Wettkampf and r.Teilnehmer = m.Teilnehmer
                                left join verein v on v.idVerein = a.Verein
                                where m.Wettkampf = 30 And m.attend = true And m.a_K = False
                                order by Gesamt desc;"
                    End If
                ElseIf Tag.ToString.Equals("Mannschaftswertung") Then
                    ' Relativ --> Gesamt = Grundlage für Platzierung
                    Query = "select m.Teilnehmer, a.Nachname, a.Vorname, a.Geschlecht, m.AK, m.GK, r.ZK, 
                                (select 
	                                case 
		                                when a.Geschlecht = 'm' then (
			                                select
				                                case 
					                                when m.Wiegen between 79.1 and 95.5 then m.Wiegen
                                                    when m.Wiegen between 95.6 and 95.9 then 95.0 
					                                else (select m from relativabzug where Gewicht = ceil(round(m.Wiegen, 1)))
				                                end    
                                                from relativabzug Limit 1)
		                                else (select w from relativabzug where Gewicht = ceil(round(m.Wiegen, 1)))
	                                end) Abzug,
    	                            r.Wertung2 Wertung, a.Verein, IfNull(v.Kurzname, v.Vereinsname) Vereinsname, Null Punkte, sum(r.Wertung2) over (partition by a.Verein) Gesamt, 0 Platz
                            from meldung m
                            left join athleten a on a.idTeilnehmer = m.Teilnehmer
                            left join results r on r.Wettkampf = m.Wettkampf and r.Teilnehmer = m.Teilnehmer
                            left join verein v on v.idVerein = a.Verein
                            where m.Wettkampf = " & Wettkampf.ID & " and m.Laenderwertung = true And m.attend = true And m.a_K = False and not isnull(r.ZK) 
                            order by Gesamt desc;"
                End If
                cmd = New MySqlCommand(Query, conn)
                tmp.Load(cmd.ExecuteReader)
            Catch ex As Exception
            End Try
        End Using

        If tmp.Rows.Count = 0 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Auswertung für <" & Wettkampf.Bezeichnung & "> gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Close()
                Return
            End Using
        End If
#End Region

#Region "CreateTables"
        ' linkle Tabelle 
        dtGroups = New DataView(tmp).ToTable(True, {"Verein", "Vereinsname", "Gesamt", "Platz"})
        Dim Platz = 0
        Dim Gesamt As Double = 1000000

        For Each row As DataRow In dtGroups.Rows
            If Not IsDBNull(row!Gesamt) Then
                If CDbl(row!Gesamt) < Gesamt Then
                    Gesamt = CDbl(row!Gesamt)
                    Platz += 1
                End If
                row!Platz = Platz
            End If
        Next
        dtGroups.AcceptChanges()

        'dtGroups = New DataTable
        'dtGroups.Columns.Add(New DataColumn With {.ColumnName = "Gruppe", .Caption = "IdGruppe", .DataType = GetType(String)})
        'dtGroups.Columns.Add(New DataColumn With {.ColumnName = "Gruppierung", .Caption = If(Bereich = "Länderwertung", "Landesverband", "Verein"), .DataType = GetType(String)})
        'dtGroups.Columns.Add(New DataColumn With {.ColumnName = "LV", .DataType = GetType(String)})
        'dtGroups.Columns.Add(New DataColumn With {.ColumnName = "Mannschaft", .DataType = GetType(Integer)})
        'dtGroups.Columns.Add(New DataColumn With {.ColumnName = "Punkte", .DataType = GetType(Double), .DefaultValue = DBNull.Value})
        'dtGroups.Columns.Add(New DataColumn With {.ColumnName = "Platz", .DataType = GetType(Integer), .DefaultValue = DBNull.Value})

        ' Tabelle der Heber

        dtHeber = New DataView(tmp).ToTable(True, {"Verein", "Vereinsname", "Nachname", "Vorname", "Geschlecht", "AK", "GK", "ZK", "Abzug", "Wertung", "Punkte"})
        dtHeber.AcceptChanges()


        'dtHeber = New DataTable
        'dtHeber.Columns.Add(New DataColumn With {.ColumnName = "Nachname", .DataType = GetType(String)})
        'dtHeber.Columns.Add(New DataColumn With {.ColumnName = "Vorname", .DataType = GetType(String)})
        'dtHeber.Columns.Add(New DataColumn With {.ColumnName = "Sex", .DataType = GetType(String)})
        'dtHeber.Columns.Add(New DataColumn With {.ColumnName = "Wertung", .DataType = GetType(Double)})
        'dtHeber.Columns.Add(New DataColumn With {.ColumnName = "LV", .DataType = GetType(String)})
        'dtHeber.Columns.Add(New DataColumn With {.ColumnName = "Verein", .DataType = GetType(Integer)})
        'dtHeber.Columns.Add(New DataColumn With {.ColumnName = "Mannschaft", .DataType = GetType(Integer)})
        'dtHeber.Columns.Add(New DataColumn With {.ColumnName = "Gruppierung", .Caption = If(Bereich = "Länderwertung", "Landesverband", "Verein"), .DataType = GetType(String)})
        'dtHeber.Columns.Add(New DataColumn With {.ColumnName = "Vereinsname", .DataType = GetType(String), .DefaultValue = DBNull.Value})
#End Region



        ' Gruppierungen einlesen
        If Tag.ToString.Equals("Vereinswertung") Then ' Heben_Platz in Punkte umrechnen --> Gesamt = Summe aller Punkte = Grundlage für Platzierung

            HeberSort = "Punkte DESC, Nachname ASC, Vorname ASC"


            '' Groups = Vereine: Heber nach Wertung DESC
            'Dim Groups = From x In tmp
            '             Group x By Team = x.Field(Of Integer)("Gruppe") Into TN = Group
            'For i = 0 To Groups.Count - 1
            '    Dim c = 0
            '    ' Anzahl Mannschaften in Gruppe
            '    Dim grp = (From x In Groups(i).TN
            '               Group x By ms = x.Field(Of Object)("Mannschaft") Into Group).Count
            '    If Not multi Then grp = 1

            '    For m = 1 To grp
            '        Dim nr = dtGroups.NewRow
            '        Dim sum As Double = 0
            '        Do
            '            Try
            '                Dim hr = dtHeber.NewRow
            '                If Wettkampf.Modus >= 120 Then
            '                    ' Jugend-Vereinsmeisterschaften
            '                    ' - max 2x weiblich, Rest Relativ(m)
            '                    If Groups(i).TN(c)!Sex.ToString = "w" AndAlso Not IsDBNull(Groups(i).TN(c)!Relativ_W) AndAlso Not CBool(Groups(i).TN(c)!Relativ_W) Then
            '                        ' 2.Wertung in realtiv männlich umrechnen
            '                        Dim resR = CInt(Groups(i).TN(c)!maxR) - CDbl(CInt(Groups(i).TN(c)!Relativ_M))
            '                        Dim resS = CInt(Groups(i).TN(c)!maxS) - CDbl(CInt(Groups(i).TN(c)!Relativ_M))
            '                        If resR < 0 Then resR = 0
            '                        If resS < 0 Then resS = 0
            '                        Groups(i).TN(c)!Wertung = resR + resS
            '                    End If
            '                End If
            '                hr.ItemArray = ({Groups(i).TN(c)!Nachname, Groups(i).TN(c)!Vorname, Groups(i).TN(c)!Sex, Groups(i).TN(c)!Wertung, Groups(i).TN(c)!LV, Groups(i).TN(c)!Gruppe, m, Groups(i).TN(c)!Gruppierung})
            '                dtHeber.Rows.Add(hr)
            '                ' If c < gewertet Then sum += CDbl(Groups(i).TN(c)!Wertung)
            '                c += 1
            '                If grp > 1 AndAlso c = gewertet Then Exit Do
            '            Catch ex As Exception
            '                Exit Do
            '            End Try
            '        Loop

            '        If c < min Then
            '            ' Mindestmannschaftsstärke unterschritten
            '            Stop
            '        End If

            '        Dim ii = i
            '        Dim mm = m
            '        sum = (From x In dtHeber
            '               Where x.Field(Of Integer)("Verein") = Groups(ii).Team AndAlso x.Field(Of Integer)("Mannschaft") = mm
            '               Order By x.Field(Of Double)("Wertung") Descending
            '               Take gewertet).Sum(Function(u) u.Field(Of Double)("Wertung"))

            '        nr.ItemArray = ({Groups(i).Team, Groups(i).TN(0)("Gruppierung"), Groups(i).TN(0)("LV"), m, sum})
            '        dtGroups.Rows.Add(nr)
            '    Next
            'Next




        ElseIf Tag.ToString.Equals("Länderwertung") Then
            'Dim Groups = From x In tmp
            '             Group x By Team = x.Field(Of Integer)("Gruppe") Into TN = Group
            'For i = 0 To Groups.Count - 1
            '    sex = Groups(i).TN(0)!Sex.ToString
            '    Dim Sex_Changed = False
            '    Dim nr = dtGroups.NewRow
            '    Dim sum As Double = 0
            '    Dim c = 0
            '    Dim g = 0
            '    Do
            '        Try
            '            Dim hr = dtHeber.NewRow
            '            hr.ItemArray = ({Groups(i).TN(c)!Nachname, Groups(i).TN(c)!Vorname, Groups(i).TN(c)!Sex, Groups(i).TN(c)!Wertung, Groups(i).TN(c)!LV, Groups(i).TN(c)!Gruppe, 1, Groups(i).TN(c)!Gruppierung, Groups(i).TN(c)!Vereinsname})
            '            dtHeber.Rows.Add(hr)
            '            If Not Groups(i).TN(c)!Sex.ToString.Equals(sex) Then Sex_Changed = True
            '            If c < gewertet - 1 OrElse g < gewertet AndAlso Sex_Changed Then
            '                sum += CDbl(Groups(i).TN(c)!Wertung)
            '                g += 1
            '            End If
            '            If c = max - 1 Then Exit Do
            '            c += 1
            '        Catch ex As Exception
            '            Exit Do
            '        End Try
            '    Loop
            '    nr.ItemArray = ({Groups(i).Team, Groups(i).TN(0)("Gruppierung"), Groups(i).TN(0)("LV"), 1, sum})
            '    dtGroups.Rows.Add(nr)
            'Next


        ElseIf Bereich.Equals("Mannschaftswertung") Then

            HeberSort = "Wertung DESC, Nachname ASC, Vorname ASC"

        End If

        dvGroups = New DataView(dtGroups, Nothing, "Platz ASC", DataViewRowState.CurrentRows)
        dvHeber = New DataView(dtHeber, Nothing, HeberSort, DataViewRowState.CurrentRows)
        bsGroups = New BindingSource With {.DataSource = dvGroups}
        bsHeber = New BindingSource With {.DataSource = dvHeber}

        ' Platzierung
        'Dim tmpResult As Double = 0
        'Dim Platz As Integer = 0
        'Dim Platz_Add = 1

        'For Each row As DataRowView In dvGroups
        '    Try
        '        ' Platzierung ohne Zeit = gleiche Plätze möglich
        '        If Not tmpResult = CDbl(row!Punkte) Then
        '            tmpResult = CDbl(row!Punkte)
        '            ' neue Platzierung
        '            Platz += Platz_Add
        '            Platz_Add = 1
        '        Else
        '            Platz_Add += 1
        '        End If
        '        ' Resultat speichern
        '        row!Platz = Platz
        '    Catch ex As Exception
        '        LogMessage("Auswertung_Länder.Me_Shown.Platzierung / " & ex.Message & " / StackTrace " &
        '                       If(InStr(ex.StackTrace, "Zeile") > 0, Mid(ex.StackTrace, InStr(ex.StackTrace, "Zeile")), String.Empty),
        '                       Path.Combine(Application.StartupPath, "log", "err_" & Format(Now, "yyyy-MM-dd") & ".log"))
        '    End Try
        'Next
        'dvGroups.Sort = "Platz ASC"

        '' Platzierung in Heber eintragen
        'dtHeber.Columns.Add(New DataColumn With {.ColumnName = "Platz", .DataType = GetType(Integer), .DefaultValue = DBNull.Value})
        'For Each row As DataRow In dtGroups.Rows
        '    Dim found() = dtHeber.Select("Verein = " & row!Gruppe.ToString & " And Mannschaft = " & row!Mannschaft.ToString)
        '    For Each item In found
        '        item!Platz = row!Platz
        '    Next
        'Next

        With dgvGruppe
            .AutoGenerateColumns = False
            .DataSource = bsGroups
        End With

        With dgvHeber
            .AutoGenerateColumns = False
            .DataSource = dvHeber
        End With
        bsHeber.Filter = "Verein = " & dvGroups(bsGroups.Position)!Verein.ToString

        Cursor = Cursors.Default
    End Sub

    Private Sub cboVorlage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVorlage.SelectedIndexChanged
        If cboVorlage.Focused Then Vorlage = cboVorlage.Text
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        With OpenFileDialog1
            .Filter = "Druck-Vorlagen (*.frx)|*.frx|Alle Dateien (*.*)|*.*"
            .InitialDirectory = Path.Combine(Application.StartupPath, "Report")
            If .ShowDialog() = DialogResult.Cancel Then Exit Sub
            If Not .FileName.Contains(".frx") Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Die ausgewählte Datei ist keine Druckvorlage.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
            Else
                Try
                    Dim directory = .FileName.Replace(.SafeFileName, "")
                    Vorlage = Split(.SafeFileName, ".")(0)
                    directory = Strings.Left(directory, directory.Length - 1)
                    dtVorlage.Rows.Add(Vorlage, directory)
                    cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage)
                Catch ex As Exception
                End Try
            End If
        End With
        cboVorlage.Focus()
    End Sub
    Private Sub btnVorschau_Click(sender As Object, e As EventArgs) Handles btnVorschau.Click, btnPrint.Click, btnEdit.Click

        User.Check_MySQLDriver(Me)

        Using report As New FastReport.Report
            With report

#Region "Vorlage"
                If Vorlage = String.Empty AndAlso cboVorlage.Text = String.Empty Then
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Keine Druckvorlage ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Using
                    Cursor = Cursors.Default
                    Return
                End If
                Try
                    .Load(Path.Combine(cboVorlage.SelectedValue.ToString, If(Not String.IsNullOrEmpty(Vorlage), Vorlage, cboVorlage.Text) + ".frx"))
                    .Dictionary.Connections(0).ConnectionString = User.FR_ConnString
                    btnVorschau.Enabled = False
                    Cursor = Cursors.WaitCursor
                Catch ex As Exception
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Vorlage nicht gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                    Return
                End Try
#End Region

                '.SetParameterValue("WK_Name", Wettkampf.Bezeichnung)
                '.SetParameterValue("WK_Datum", Year(Wettkampf.Datum))
                '.SetParameterValue("WK_Titel", Bereich)
                '.SetParameterValue("WK_Wertung", "")
                '.SetParameterValue("WK_Group", IIf(Bereich = "Länderwertung", "Land", "Verein"))

                .RegisterData(Glob.dtWK, "wk")
                .RegisterData(dtHeber, "heber")
                .RegisterData(dvGroups.ToTable, "team")

                Try
                    Dim s = New FastReport.EnvironmentSettings
                    s.ReportSettings.ShowProgress = False
                    .Prepare()
                    If sender.Equals(btnVorschau) Then
                        .ShowPrepared()
                    ElseIf sender.Equals(btnPrint) Then
                        .PrintPrepared()
                    ElseIf sender.Equals(btnEdit) Then
                        .Design()
                    End If
                Catch ex As Exception
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Fehler beim Drucken", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Using
                    LogMessage("Auswertung_Länder: btnVorschau: Prepare: " & Bereich & " / " & ex.Message, ex.StackTrace)
                End Try
            End With
        End Using

        btnVorschau.Enabled = True
        Cursor = Cursors.Default
    End Sub
    Private Sub btnVorschau_EnabledChanged(sender As Object, e As EventArgs) Handles btnVorschau.EnabledChanged
        btnPrint.Enabled = btnVorschau.Enabled
    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub bsGroups_PositionChanged(sender As Object, e As EventArgs) Handles bsGroups.PositionChanged
        bsHeber.Filter = "Verein = " & dvGroups(bsGroups.Position)!Verein.ToString
    End Sub

End Class