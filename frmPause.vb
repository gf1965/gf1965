﻿
Public Class frmPause

    Private pnlPos As Point
    Private timerPause As Threading.Timer
    Private btnState As New Dictionary(Of String, Boolean)

    Private Sub frmPause_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Leader.IsPause = False
        If Heber.Id > 0 Then
            AnzeigeCountDown_Change(ZN_Pad.Countdown)
            Anzeige.ClockColor = Anzeige.TmpClockColor
        Else
            AnzeigeCountDown_Change(0)
        End If
        If Not IsNothing(timerPause) Then timerPause.Dispose()
        Leader.Set_ButtonState()
    End Sub
    Private Sub frmPause_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode.Equals(Keys.F4) Then
            Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
            Cursor.Show()
        End If
    End Sub
    Private Sub frmPause_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        Location = New Point(200, 200)
    End Sub
    Private Sub frmPause_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        dtpZeit.Value = Now().AddMinutes(15)
        Leader.Get_ButtonState()
        Leader.Set_ButtonState(True)
        Cursor = Cursors.Default
    End Sub

    '' BUTTONS
    Private Sub btnEscape_Click(sender As Object, e As EventArgs) Handles btnEscape.Click
        Close()
    End Sub
    Private Sub btnEscape_MouseEnter(sender As Object, e As EventArgs) Handles btnEscape.MouseEnter
        btnEscape.ForeColor = Color.White
    End Sub
    Private Sub btnEscape_MouseLeave(sender As Object, e As EventArgs) Handles btnEscape.MouseLeave
        btnEscape.ForeColor = Color.FromKnownColor(KnownColor.DarkGray)
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click

        If btnStart.Text.Equals("Starten") Then
            btnStart.Text = "Stoppen"
            Leader.IsPause = True

            If TabControl1.SelectedTab.Equals(tabUhrzeit) Then
                Leader.Pause_Countdown = CInt((dtpZeit.Value - Now()).TotalSeconds)
            ElseIf optCustom.Checked Then
                Leader.Pause_Countdown = CInt(nudStunden.Value * 3600 + nudMinuten.Value * 60)
            Else 'If TabControl1.SelectedTab.Equals(tabIntervall) Then
                For Each ctl As Control In tabIntervall.Controls
                    If TypeOf ctl Is RadioButton Then
                        Dim c As RadioButton = CType(ctl, RadioButton)
                        If c.Checked Then
                            Leader.Pause_Countdown = CInt(c.Name.Substring(3)) * 60
                            Exit For
                        End If
                    End If
                Next
            End If

            If timerAufruf_Running Then Leader.Change_Aufruf(False, "Aufruf gestoppt", True)

            Anzeige.TmpClockColor = Anzeige.ClockColor
            Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Waiting)

            AnzeigeCountDown_Change(Leader.Pause_Countdown)
            timerPause = New Threading.Timer(AddressOf timerPause_Tick)
            timerPause.Change(0, Threading.Timeout.Infinite)
        Else
            Close()
        End If

    End Sub

    '' MOVE
    Private Sub Panel_Headers_MouseDown(sender As Object, e As MouseEventArgs) Handles lblTitleBar.MouseDown
        If e.Button = MouseButtons.Left Then
            pnlForm.BorderStyle = BorderStyle.None
            pnlPos = New Point(e.X, e.Y)
            lblTitleBar.Cursor = Cursors.SizeAll
        End If
    End Sub
    Private Sub Panel_Headers_MouseMove(sender As Object, e As MouseEventArgs) Handles lblTitleBar.MouseMove
        If (e.Button And MouseButtons.Left) = MouseButtons.Left And pnlForm.BorderStyle = BorderStyle.None Then
            Dim loc = New Point(Location.X + (e.X - pnlPos.X), Location.Y + (e.Y - pnlPos.Y))
            With Screen.FromControl(Me).WorkingArea
                If loc.X + Width < .Width AndAlso loc.Y + Height < .Height AndAlso loc.X > .Left AndAlso loc.Y > .Top Then
                    Location = loc
                End If
            End With
        End If
    End Sub
    Private Sub Panel_Headers_MouseUp(sender As Object, e As MouseEventArgs) Handles lblTitleBar.MouseUp
        If e.Button = MouseButtons.Left And pnlForm.BorderStyle = BorderStyle.None Then
            pnlForm.BorderStyle = BorderStyle.FixedSingle
            pnlPos = New Point(pnlForm.Left, pnlForm.Top + pnlForm.Height)
            lblTitleBar.Cursor = Cursors.Default
        End If
    End Sub

    '' Pause-Timer
    Private Delegate Sub Delegate_Object(state As Object)
    Private Sub timerPause_Tick(state As Object)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_Object(AddressOf timerPause_Tick)
                Invoke(d, New Object() {state})
            Else
                If Leader.Pause_Countdown > 0 Then
                    Try
                        AnzeigeCountDown_Change(Leader.Pause_Countdown)
                        timerPause.Change(1000, Threading.Timeout.Infinite)
                        Leader.Pause_Countdown -= 1
                    Catch ex As Exception
                    End Try
                Else
                    Close()
                End If
                'If Leader.Pause_Countdown = 0 Then
                '    With btnStart
                '        .BackColor = Color.Red
                '        .ForeColor = Color.Yellow
                '        .Text = "Schließen"
                '    End With
                'End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub AnzeigeCountDown_Change(value As Integer)

        Dim Stunden As Integer = value \ 3600
        Dim Minuten As Integer = (value - Stunden * 3600) \ 60
        Dim Sekunden As Integer = value - Stunden * 3600 - Minuten * 60

        If value <= 0 Then
            Anzeige.CountDown = "0:00"
        ElseIf Stunden > 0 Then
            Anzeige.CountDown = Stunden.ToString + ":" + Minuten.ToString("00") + ":" + Sekunden.ToString("00")
        Else
            Anzeige.CountDown = Minuten.ToString + ":" + Sekunden.ToString("00")
        End If

    End Sub

    Private Sub nudMinuten_GotFocus(sender As Object, e As EventArgs) Handles nudMinuten.GotFocus
        nudMinuten.Select(0, nudMinuten.Value.ToString.Length)
    End Sub
    Private Sub nudMinuten_ValueChanged(sender As Object, e As EventArgs) Handles nudMinuten.ValueChanged
        If nudMinuten.Value = 60 Then
            nudMinuten.Value = 0
            nudStunden.Value += 1
        End If
    End Sub

    Private Sub nudStunden_GotFocus(sender As Object, e As EventArgs) Handles nudStunden.GotFocus
        nudStunden.Select(0, nudStunden.Value.ToString.Length)
    End Sub

    Private Sub optCustom_CheckedChanged(sender As Object, e As EventArgs) Handles optCustom.CheckedChanged
        pnlCustom.Enabled = optCustom.Checked
    End Sub

    Private Sub chkHideAds_CheckedChanged(sender As Object, e As EventArgs) Handles chkHideAds.CheckedChanged
        Leader.AdsVisible = Not chkHideAds.Checked
    End Sub

End Class