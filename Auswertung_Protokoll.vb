﻿
Imports MySqlConnector

Public Class Auswertung_Protokoll

    Dim Bereich As String
    Dim Design As String = "Standard"


    Dim dt As New DataTable
    Dim dv As DataView
    Dim dtA As New DataTable
    Dim dtGruppen As New DataTable

    Dim bs As BindingSource
    Dim Query As String
    Dim cmd As MySqlCommand

    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        lblCaption.Text = Wettkampf.Bezeichnung
    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        If Wettkampf.Technikwertung AndAlso Wettkampf.Athletik Then
            Bereich = "Protokoll_Technik_Athletik"
        ElseIf Wettkampf.Athletik Then
            Bereich = "Protokoll_Athletik"
        ElseIf Wettkampf.Mannschaft Then
            Bereich = "Protokoll_Mannschaft"
        Else
            Bereich = "Protokoll"
        End If

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                Query = "Select Disziplin, Bezeichnung " &
                        "FROM wettkampf_athletik, athletik_disziplinen " &
                        "WHERE Wettkampf = " & Wettkampf.ID & " And idDisziplin = Disziplin ORDER BY Disziplin;"
                cmd = New MySqlCommand(Query, conn)
                dtA.Load(cmd.ExecuteReader())

                ' Protokoll-Tabelle
                cmd = New MySqlCommand(Glob.Get_QueryResults("'---'", Wettkampf.Athletik), conn)
                dt.Load(cmd.ExecuteReader())

                ' Gruppen
                cmd = New MySqlCommand("SELECT * FROM gruppen WHERE Wettkampf = " & Wettkampf.ID & ";", conn)
                dtGruppen.Load(cmd.ExecuteReader())
            Catch ex As Exception
                'Stop
            End Try
        End Using

        dv = New DataView(dt)
        bs = New BindingSource With {.DataSource = dv}
        With dgvJoin
            .DataSource = bs
            Try
                .Columns("A_1").HeaderText = If(dtA.Rows.Count > 0, dtA.Rows(0)("Bezeichnung").ToString, String.Empty)
                .Columns("A_2").HeaderText = If(dtA.Rows.Count > 1, dtA.Rows(1)("Bezeichnung").ToString, String.Empty)
                .Columns("A_3").HeaderText = If(dtA.Rows.Count > 2, dtA.Rows(2)("Bezeichnung").ToString, String.Empty)
                .Columns("A_4").HeaderText = If(dtA.Rows.Count > 3, dtA.Rows(3)("Bezeichnung").ToString, String.Empty)
            Catch ex As Exception
            End Try
        End With

        With cboGruppe
            .DataSource = dtGruppen
            .DisplayMember = "Bezeichnung"
            .ValueMember = "Gruppe"
            .SelectedIndex = -1
        End With

        'Build_Grid()

        Cursor = Cursors.Default
    End Sub

    Private Sub Build_Grid()

        Dim ColWidth As New List(Of Integer)
        ColWidth.AddRange({20, 30, 35, 35, 50, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30})
        Dim ColBez As New List(Of String)
        ColBez.AddRange({"Nr.", "   AK   ", "Nachname", "Vorname", "Verein", "    BL    ", " Jahrgang ", "  Gewicht  ", "   GK   ", "   Koeffizient   ", " 1.Reißen ", " 2.Reißen ", " 3.Reißen ", " 1.Stoßen ", " 2.Stoßen ", " 3.Stoßen ", " Zweikampf ", "    Wertung    ", " Platz ", " a.K. "})
        Dim ColData As New List(Of String)
        ColData.AddRange({"Teilnehmer", "AK", "Nachname", "Vorname", "Vereinsname", "BL", "JG", "Wiegen", "GK", "Koeffizient", "Reissen1", "Reissen2", "Reissen3", "Stossen1", "Stossen2", "Stossen3", "Zweikampf", "Wertung", "Gesamt_Platz", "a_K"})
        'Dim ColCenter() As String = {"JG", "AK", "Gesamt_Platz", "Stoßen1", "Stoßen2", "Stoßen3", "W_Reißen", "W_Stoßen"}

        If Wettkampf.Technikwertung Then
            ColWidth.InsertRange(5, {30, 30, 30, 30, 30, 30})
            ColBez.RemoveRange(5, 6)
            ColBez.InsertRange(5, {"1.", "", "2.", "", "3.", "", "1.", "", "2.", "", "3.", ""})
            ColData.RemoveRange(5, 6)
            ColData.InsertRange(5, {"Reißen1", "ReißenT1", "Reißen2", "ReißenT2", "Reißen3", "ReißenT3", "Stoßen1", "StoßenT1", "Stoßen2", "StoßenT2", "Stoßen3", "StoßenT3"})
            'ColCenter = {"Reißen1", "ReißenT1", "Reißen2", "ReißenT2", "Reißen3", "ReißenT3", "Stoßen1", "StoßenT1", "Stoßen2", "StoßenT2", "Stoßen3", "StoßenT3", "Wertung"}
        End If

        With dgvJoin
            Select Case Design
                Case "Standard"
                    .AlternatingRowsDefaultCellStyle.BackColor = Color.OldLace
                    .RowsDefaultCellStyle.BackColor = Color.LightCyan
                    .BackgroundColor = SystemColors.AppWorkspace
                    .GridColor = SystemColors.ControlDark
                    With .DefaultCellStyle
                        .ForeColor = SystemColors.ControlText
                        .SelectionBackColor = Color.LightCyan
                        .SelectionForeColor = SystemColors.ControlText
                    End With
                Case "Nacht"
                    .AlternatingRowsDefaultCellStyle.BackColor = BackColor
                    .RowsDefaultCellStyle.BackColor = Color.Black
                    .BackgroundColor = Color.Black
                    .GridColor = Color.Black
                    With .DefaultCellStyle
                        .ForeColor = Color.White
                        .SelectionBackColor = Color.White
                        .SelectionForeColor = Color.Black
                    End With
            End Select
            .AllowUserToOrderColumns = False
            If .Columns.Count = 0 Then
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nr"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "AK"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Verein", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "BL"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "JG"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Gewicht"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "GK"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Faktor"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Reißen1"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Reißen2"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Reißen3"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Stoßen1"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Stoßen2"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Stoßen3"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Zweikampf"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Wertung"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Gesamt_Platz"})
                .Columns.Add(New DataGridViewCheckBoxColumn With {.Name = "a_K", .ValueType = GetType(Boolean)})
                If Wettkampf.Technikwertung Then
                    .Columns.Insert(6, New DataGridViewTextBoxColumn With {.Name = "ReißenT1", .ValueType = GetType(Double), .Resizable = DataGridViewTriState.False})
                    .Columns.Insert(8, New DataGridViewTextBoxColumn With {.Name = "ReißenT2", .ValueType = GetType(Double), .Resizable = DataGridViewTriState.False})
                    .Columns.Insert(10, New DataGridViewTextBoxColumn With {.Name = "ReißenT3", .ValueType = GetType(Double), .Resizable = DataGridViewTriState.False})
                    .Columns.Insert(12, New DataGridViewTextBoxColumn With {.Name = "StoßenT1", .ValueType = GetType(Double), .Resizable = DataGridViewTriState.False})
                    .Columns.Insert(14, New DataGridViewTextBoxColumn With {.Name = "StoßenT2", .ValueType = GetType(Double), .Resizable = DataGridViewTriState.False})
                    .Columns.Insert(16, New DataGridViewTextBoxColumn With {.Name = "StoßenT3", .ValueType = GetType(Double), .Resizable = DataGridViewTriState.False})
                End If
                .Columns("Nr").Visible = False
                .Columns("Wertung").DefaultCellStyle.Format = "0.0000"
            End If
            Try
                For i As Integer = 0 To ColWidth.Count - 1
                    .Columns(i).Width = ColWidth(i)
                    .Columns(i).HeaderText = ColBez(i)
                    .Columns(i).DataPropertyName = ColData(i)
                    .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                Next
                For i As Integer = 5 To 19
                    .Columns(i).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                Next
                .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            Catch ex As Exception
                'Stop
            End Try
        End With
    End Sub

    Private Sub cboGruppe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGruppe.SelectedIndexChanged
        Try
            bs.Filter = "Gruppe = " & cboGruppe.SelectedValue.ToString
            lblGruppe.Text = "Gruppe " & cboGruppe.SelectedValue.ToString
        Catch ex As Exception
        End Try
    End Sub

    Private Sub mnuWKProtokoll_Click(sender As Object, e As EventArgs) Handles mnuProtokoll.Click, btnProtokoll.Click
        Cursor = Cursors.WaitCursor
        With Drucken_Protokoll
            .Text = "Wettkampf-Protokoll drucken"
            .Tag = cboGruppe.SelectedValue
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub

End Class