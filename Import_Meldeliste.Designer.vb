﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Import_Meldeliste
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.pnlOptionen = New System.Windows.Forms.Panel()
        Me.btnSearchFile = New System.Windows.Forms.Button()
        Me.txtRowDivider = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtColEnclosed = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtColDivider = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboFile = New System.Windows.Forms.ComboBox()
        Me.dlgOpen = New System.Windows.Forms.OpenFileDialog()
        Me.grpVerein = New System.Windows.Forms.GroupBox()
        Me.btnShowVerein = New System.Windows.Forms.Button()
        Me.lblVerein = New System.Windows.Forms.Label()
        Me.prbVerein = New System.Windows.Forms.ProgressBar()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.bsB = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnSaveBL = New System.Windows.Forms.Button()
        Me.pnlBL = New System.Windows.Forms.Panel()
        Me.btnCancelVerein = New System.Windows.Forms.Button()
        Me.grpHeber = New System.Windows.Forms.GroupBox()
        Me.lblHeber = New System.Windows.Forms.Label()
        Me.prbHeber = New System.Windows.Forms.ProgressBar()
        Me.grpMeldung = New System.Windows.Forms.GroupBox()
        Me.lblMeldung = New System.Windows.Forms.Label()
        Me.prbMeldung = New System.Windows.Forms.ProgressBar()
        Me.GroupBox1.SuspendLayout()
        Me.pnlOptionen.SuspendLayout()
        Me.grpVerein.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsB, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBL.SuspendLayout()
        Me.grpHeber.SuspendLayout()
        Me.grpMeldung.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnStart)
        Me.GroupBox1.Controls.Add(Me.pnlOptionen)
        Me.GroupBox1.Controls.Add(Me.cboFile)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 21)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(288, 281)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Import-Datei"
        '
        'btnStart
        '
        Me.btnStart.Enabled = False
        Me.btnStart.Location = New System.Drawing.Point(156, 64)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(109, 23)
        Me.btnStart.TabIndex = 2
        Me.btnStart.Text = "Import starten"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'pnlOptionen
        '
        Me.pnlOptionen.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlOptionen.Controls.Add(Me.btnSearchFile)
        Me.pnlOptionen.Controls.Add(Me.txtRowDivider)
        Me.pnlOptionen.Controls.Add(Me.Label3)
        Me.pnlOptionen.Controls.Add(Me.txtColEnclosed)
        Me.pnlOptionen.Controls.Add(Me.Label2)
        Me.pnlOptionen.Controls.Add(Me.txtColDivider)
        Me.pnlOptionen.Controls.Add(Me.Label1)
        Me.pnlOptionen.Enabled = False
        Me.pnlOptionen.Location = New System.Drawing.Point(23, 110)
        Me.pnlOptionen.Name = "pnlOptionen"
        Me.pnlOptionen.Size = New System.Drawing.Size(242, 148)
        Me.pnlOptionen.TabIndex = 1
        '
        'btnSearchFile
        '
        Me.btnSearchFile.Location = New System.Drawing.Point(124, 108)
        Me.btnSearchFile.Name = "btnSearchFile"
        Me.btnSearchFile.Size = New System.Drawing.Size(99, 23)
        Me.btnSearchFile.TabIndex = 6
        Me.btnSearchFile.Text = "Durchsuchen"
        Me.btnSearchFile.UseVisualStyleBackColor = True
        '
        'txtRowDivider
        '
        Me.txtRowDivider.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRowDivider.Location = New System.Drawing.Point(156, 69)
        Me.txtRowDivider.Name = "txtRowDivider"
        Me.txtRowDivider.Size = New System.Drawing.Size(63, 20)
        Me.txtRowDivider.TabIndex = 5
        Me.txtRowDivider.Text = "automatisch"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Zeilen getrennt mit"
        '
        'txtColEnclosed
        '
        Me.txtColEnclosed.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtColEnclosed.Location = New System.Drawing.Point(156, 44)
        Me.txtColEnclosed.Name = "txtColEnclosed"
        Me.txtColEnclosed.Size = New System.Drawing.Size(63, 20)
        Me.txtColEnclosed.TabIndex = 3
        Me.txtColEnclosed.Text = """"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 47)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(140, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Spalten eingeschlossen von"
        '
        'txtColDivider
        '
        Me.txtColDivider.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtColDivider.Location = New System.Drawing.Point(156, 19)
        Me.txtColDivider.Name = "txtColDivider"
        Me.txtColDivider.Size = New System.Drawing.Size(63, 20)
        Me.txtColDivider.TabIndex = 1
        Me.txtColDivider.Text = ","
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Spalten getrennt mit"
        '
        'cboFile
        '
        Me.cboFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFile.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboFile.FormattingEnabled = True
        Me.cboFile.Items.AddRange(New Object() {"CSV-Datei", "Excel-Datei", "MySQL-Tabelle", "Tabelle ""Meldeliste"""})
        Me.cboFile.Location = New System.Drawing.Point(23, 33)
        Me.cboFile.Name = "cboFile"
        Me.cboFile.Size = New System.Drawing.Size(242, 21)
        Me.cboFile.TabIndex = 0
        '
        'dlgOpen
        '
        Me.dlgOpen.FileName = "OpenFileDialog1"
        '
        'grpVerein
        '
        Me.grpVerein.Controls.Add(Me.btnShowVerein)
        Me.grpVerein.Controls.Add(Me.lblVerein)
        Me.grpVerein.Controls.Add(Me.prbVerein)
        Me.grpVerein.Location = New System.Drawing.Point(323, 21)
        Me.grpVerein.Name = "grpVerein"
        Me.grpVerein.Size = New System.Drawing.Size(383, 102)
        Me.grpVerein.TabIndex = 1
        Me.grpVerein.TabStop = False
        Me.grpVerein.Text = "Vereine importieren"
        '
        'btnShowVerein
        '
        Me.btnShowVerein.Location = New System.Drawing.Point(304, 64)
        Me.btnShowVerein.Name = "btnShowVerein"
        Me.btnShowVerein.Size = New System.Drawing.Size(54, 23)
        Me.btnShowVerein.TabIndex = 2
        Me.btnShowVerein.Text = "Fehler"
        Me.btnShowVerein.UseVisualStyleBackColor = True
        Me.btnShowVerein.Visible = False
        '
        'lblVerein
        '
        Me.lblVerein.AutoSize = True
        Me.lblVerein.Location = New System.Drawing.Point(21, 69)
        Me.lblVerein.Name = "lblVerein"
        Me.lblVerein.Size = New System.Drawing.Size(105, 13)
        Me.lblVerein.TabIndex = 1
        Me.lblVerein.Text = "Vereine aktualisieren"
        '
        'prbVerein
        '
        Me.prbVerein.ForeColor = System.Drawing.Color.YellowGreen
        Me.prbVerein.Location = New System.Drawing.Point(24, 33)
        Me.prbVerein.Name = "prbVerein"
        Me.prbVerein.Size = New System.Drawing.Size(334, 23)
        Me.prbVerein.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.prbVerein.TabIndex = 0
        '
        'dgv
        '
        Me.dgv.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(0, 0)
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(382, 133)
        Me.dgv.TabIndex = 2
        '
        'bsB
        '
        '
        'btnSaveBL
        '
        Me.btnSaveBL.Location = New System.Drawing.Point(226, 139)
        Me.btnSaveBL.Name = "btnSaveBL"
        Me.btnSaveBL.Size = New System.Drawing.Size(75, 23)
        Me.btnSaveBL.TabIndex = 3
        Me.btnSaveBL.Text = "Speichern"
        Me.btnSaveBL.UseVisualStyleBackColor = True
        '
        'pnlBL
        '
        Me.pnlBL.Controls.Add(Me.btnCancelVerein)
        Me.pnlBL.Controls.Add(Me.dgv)
        Me.pnlBL.Controls.Add(Me.btnSaveBL)
        Me.pnlBL.Location = New System.Drawing.Point(448, 132)
        Me.pnlBL.Name = "pnlBL"
        Me.pnlBL.Size = New System.Drawing.Size(391, 170)
        Me.pnlBL.TabIndex = 4
        Me.pnlBL.Visible = False
        '
        'btnCancelVerein
        '
        Me.btnCancelVerein.Location = New System.Drawing.Point(307, 139)
        Me.btnCancelVerein.Name = "btnCancelVerein"
        Me.btnCancelVerein.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelVerein.TabIndex = 4
        Me.btnCancelVerein.Text = "Abbrechen"
        Me.btnCancelVerein.UseVisualStyleBackColor = True
        '
        'grpHeber
        '
        Me.grpHeber.Controls.Add(Me.lblHeber)
        Me.grpHeber.Controls.Add(Me.prbHeber)
        Me.grpHeber.Location = New System.Drawing.Point(323, 141)
        Me.grpHeber.Name = "grpHeber"
        Me.grpHeber.Size = New System.Drawing.Size(383, 102)
        Me.grpHeber.TabIndex = 3
        Me.grpHeber.TabStop = False
        Me.grpHeber.Text = "Heber importieren"
        '
        'lblHeber
        '
        Me.lblHeber.AutoSize = True
        Me.lblHeber.Location = New System.Drawing.Point(21, 69)
        Me.lblHeber.Name = "lblHeber"
        Me.lblHeber.Size = New System.Drawing.Size(98, 13)
        Me.lblHeber.TabIndex = 1
        Me.lblHeber.Text = "Heber aktualisieren"
        '
        'prbHeber
        '
        Me.prbHeber.ForeColor = System.Drawing.Color.YellowGreen
        Me.prbHeber.Location = New System.Drawing.Point(24, 33)
        Me.prbHeber.Name = "prbHeber"
        Me.prbHeber.Size = New System.Drawing.Size(334, 23)
        Me.prbHeber.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.prbHeber.TabIndex = 0
        '
        'grpMeldung
        '
        Me.grpMeldung.Controls.Add(Me.lblMeldung)
        Me.grpMeldung.Controls.Add(Me.prbMeldung)
        Me.grpMeldung.Location = New System.Drawing.Point(323, 261)
        Me.grpMeldung.Name = "grpMeldung"
        Me.grpMeldung.Size = New System.Drawing.Size(383, 102)
        Me.grpMeldung.TabIndex = 4
        Me.grpMeldung.TabStop = False
        Me.grpMeldung.Text = "Meldungen importieren"
        '
        'lblMeldung
        '
        Me.lblMeldung.AutoSize = True
        Me.lblMeldung.Location = New System.Drawing.Point(21, 69)
        Me.lblMeldung.Name = "lblMeldung"
        Me.lblMeldung.Size = New System.Drawing.Size(122, 13)
        Me.lblMeldung.TabIndex = 1
        Me.lblMeldung.Text = "Meldungen aktualisieren"
        '
        'prbMeldung
        '
        Me.prbMeldung.ForeColor = System.Drawing.Color.YellowGreen
        Me.prbMeldung.Location = New System.Drawing.Point(24, 33)
        Me.prbMeldung.Name = "prbMeldung"
        Me.prbMeldung.Size = New System.Drawing.Size(334, 23)
        Me.prbMeldung.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.prbMeldung.TabIndex = 0
        '
        'Import_Meldeliste
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(725, 377)
        Me.Controls.Add(Me.grpMeldung)
        Me.Controls.Add(Me.grpHeber)
        Me.Controls.Add(Me.pnlBL)
        Me.Controls.Add(Me.grpVerein)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Import_Meldeliste"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Import_Meldeliste"
        Me.GroupBox1.ResumeLayout(False)
        Me.pnlOptionen.ResumeLayout(False)
        Me.pnlOptionen.PerformLayout()
        Me.grpVerein.ResumeLayout(False)
        Me.grpVerein.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsB, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBL.ResumeLayout(False)
        Me.grpHeber.ResumeLayout(False)
        Me.grpHeber.PerformLayout()
        Me.grpMeldung.ResumeLayout(False)
        Me.grpMeldung.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents pnlOptionen As Panel
    Friend WithEvents txtRowDivider As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtColEnclosed As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtColDivider As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cboFile As ComboBox
    Friend WithEvents btnSearchFile As Button
    Friend WithEvents dlgOpen As OpenFileDialog
    Friend WithEvents grpVerein As GroupBox
    Friend WithEvents lblVerein As Label
    Friend WithEvents prbVerein As ProgressBar
    Friend WithEvents btnShowVerein As Button
    Friend WithEvents dgv As DataGridView
    Friend WithEvents bsB As BindingSource
    Friend WithEvents btnStart As Button
    Friend WithEvents btnSaveBL As Button
    Friend WithEvents pnlBL As Panel
    Friend WithEvents btnCancelVerein As Button
    Friend WithEvents grpHeber As GroupBox
    Friend WithEvents lblHeber As Label
    Friend WithEvents prbHeber As ProgressBar
    Friend WithEvents grpMeldung As GroupBox
    Friend WithEvents lblMeldung As Label
    Friend WithEvents prbMeldung As ProgressBar
End Class
