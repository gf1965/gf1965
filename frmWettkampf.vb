﻿
Imports System.ComponentModel
Imports System.Text.RegularExpressions
Imports MySqlConnector

Public Class frmWettkampf
    Class DD_Button
        Public Button As Panel
        Public Style As Integer
    End Class
#Region "Definitions"
    Dim Bereich As String = "Wettkampf"

    Dim nonNumberEntered As Boolean = False
    Dim Query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader

    Private WithEvents bsA As BindingSource
    Private WithEvents bsAGs As BindingSource
    Private WithEvents bsAK As BindingSource
    Private WithEvents bsSameAK As BindingSource
    Private WithEvents bsAKs As BindingSource
    Private WithEvents bsBezeichnung As New BindingSource
    Private WithEvents bsD As BindingSource
    Private WithEvents bsDetails As New BindingSource
    Private WithEvents bsGebühr As BindingSource
    Private WithEvents bsMeldefrist As New BindingSource
    Private WithEvents bsMulti As BindingSource
    Private WithEvents bsTeam As BindingSource
    Private WithEvents bsVerein As BindingSource
    Private WithEvents bsWertung As BindingSource
    Private WithEvents bsWK As New BindingSource

    Dim dvA As DataView
    Dim dvAK As DataView
    Dim dvAKs As DataView
    Dim dvBezeichnung As DataView
    Dim dvD As DataView
    Dim dvDisc As DataView
    Dim dvGebühr As DataView
    Dim dvModus As DataView
    Dim dvMulti As DataView
    Dim dvTeam As DataView
    Dim dvVeranstalter As DataView
    Dim dvVerein As DataView
    Dim dvWettkampf As DataView

    Dim dtAthletik As New DataTable     ' Athletik-Disziplinen und Formeln für den WK
    Dim dtAK As New DataTable
    Dim dtAKs As New DataTable
    Dim dtBezeichnung As New DataTable  ' wettkampf_bezeichnung
    Dim dtDefaults As New DataTable     ' wettkampf_athletik_defaults
    Dim dtDetails As New DataTable
    Dim dtDisc As New DataTable
    Dim dtGebühr As New DataTable
    Dim dtGruppen As New DataTable
    Dim dtGruppenDef As New DataTable
    Dim dtMann_Wertung As New DataTable ' wettkampf_mannschaft
    Dim dtMeldefrist As New DataTable
    Dim dtMulti As New DataTable
    Dim dtOrt As New DataTable
    Dim dtVerein As New DataTable
    Dim dtWettkampf As New DataTable
    Dim dtSameAK As New DataTable

    Dim tbGewichtsgruppen As New TabPage
    Dim tbWertung As New TabPage
    Dim tbAltersgruppen As New TabPage
    Dim tbAthletik As New TabPage
    Dim tbMannschaft As New TabPage
    Dim tbAusnahme As New TabPage
    Dim tbPlatzierung As New TabPage

    Dim lblTeam As New List(Of Label)
    Dim txtTeam As New List(Of TextBox)
    Dim pnlTeam As New List(Of DD_Button)
    Dim TeamIndex As Integer = -1

    Dim dicMannschaft As New Dictionary(Of Integer, List(Of Control))
    Dim dicAltersklassen As New Dictionary(Of Integer, List(Of Control))

    Dim nudGG_m As New List(Of NumericUpDown)
    Dim lblGG_m As New List(Of Label)
    Dim nudGG_w As New List(Of NumericUpDown)
    Dim lblGG_w As New List(Of Label)

    'Dim Identities As List(Of String)

    Dim loading As Boolean
    Dim Settings_Changed As Boolean ' Steigerung, Restriction geändert --> Event in Save auslösen, um neue autom. Steigerung und/oder Restriction in Leader zu übernehmen (KR und mehr???)
    Dim AK_Tables As String() = {"altersklassen ", "altersklassen_international "} ' Leerzeichen am Ende für Query


    Private Rating2 As Integer
    Private IsEqualRating As Boolean
    Private _IsMehrkampf As Boolean
    Private Property IsMehrkampf As Boolean
        Get
            Return _IsMehrkampf
        End Get
        Set(value As Boolean)
            If Not value.Equals(_IsMehrkampf) Then
                _IsMehrkampf = value
                If value Then
                    Wertung_Clear()
                    Platzierung_Clear()
                End If
            End If
        End Set
    End Property
    Property WK_ID As String
    Enum ButtonState
        Disabled
        Normal
        Pressed
        Hot
    End Enum
#End Region

    '' Me
    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        If mnuSpeichern.Enabled Then
            Dim DlgResult As DialogResult = DialogResult.None
            With New Centered_MessageBox(Me)
                DlgResult = MessageBox.Show("Änderungen am Wettkampf speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                Select Case DlgResult
                    Case DialogResult.Yes
                        mnuSpeichern.PerformClick()
                    Case DialogResult.No
                        DialogResult = DialogResult.Cancel
                    Case DialogResult.Cancel
                        e.Cancel = True
                End Select
            End With
        End If

        ConnError = False
        formMain.Change_MainProgressBarValue(0)
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        ' Listen für Team-WK (z.B. Liga)
        lblTeam.Add(lbl0)
        lblTeam.Add(lbl1)
        lblTeam.Add(lbl2)
        txtTeam.Add(txt0)
        txtTeam.Add(txt1)
        txtTeam.Add(txt2)
        Dim btn As New DD_Button
        btn.Button = pnl0
        btn.Style = ButtonState.Disabled
        pnlTeam.Add(btn)
        btn = New DD_Button
        btn.Button = pnl1
        btn.Style = ButtonState.Disabled
        pnlTeam.Add(btn)
        btn = New DD_Button
        btn.Button = pnl2
        btn.Style = ButtonState.Disabled
        pnlTeam.Add(btn)

        ' Mannschaftstärke
        Dim ctl As New List(Of Control)
        ctl.AddRange({chkVerein, nudVmin, nudVmax, nudVwert, chkVsex, chkVadd, nudVrel})
        dicMannschaft.Add(0, ctl)
        ctl = New List(Of Control)
        ctl.AddRange({chkLand, nudLmin, nudLmax, nudLwert, chkLsex, chkLadd})
        dicMannschaft.Add(1, ctl)
        ctl = New List(Of Control)
        ctl.AddRange({chkLiga, nudMmin, nudMmax, nudMwert, chkMsex, chkMadd})
        dicMannschaft.Add(2, ctl)

        ' Gewichtsgruppen
        nudGG_m.AddRange({nudG_m0, nudG_m1, nudG_m2, nudG_m3, nudG_m4})
        nudGG_w.AddRange({nudG_w0, nudG_w1, nudG_w2, nudG_w3, nudG_w4})
        lblGG_m.AddRange({lblG_m0, lblG_m1, lblG_m2, lblG_m3, lblG_m4})
        lblGG_w.AddRange({lblG_w0, lblG_w1, lblG_w2, lblG_w3, lblG_w4})

        ' Tooltips
        Dim tTip As New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        'tTip.SetToolTip(btnAdd, "Fügt ein Element am Ende der Liste hinzu.")
        'tTip.SetToolTip(btnRemove, "Entfernt das ausgewählte Element aus der Liste.")
        'tTip.SetToolTip(lblMsex, "Haken setzen, wenn beide Geschlechter vertreten sein müssen")
        'tTip.SetToolTip(chkMsex, "Haken setzen, wenn beide Geschlechter vertreten sein müssen")
        'tTip.SetToolTip(chkMadd, "Haken setzen, wenn mehrere Mannschaften pro Verein erlaubt sind")
        'tTip.SetToolTip(lblMadd, "wenn aktiviert, sind mehrere Mannschaften pro Verein erlaubt")
        tTip.SetToolTip(chkVsex, "wenn aktiviert, müssen beide Geschlechter vertreten sein")
        tTip.SetToolTip(lblVsex, "wenn aktiviert, müssen beide Geschlechter vertreten sein")
        tTip.SetToolTip(chkVadd, "wenn aktiviert, sind mehrere Mannschaften pro Verein erlaubt")
        tTip.SetToolTip(lblVadd, "wenn aktiviert, sind mehrere Mannschaften pro Verein erlaubt")
        tTip.SetToolTip(nudVmin, "Mindestanzahl der Teilnehmer pro Mannschaften")
        tTip.SetToolTip(nudVmax, "Anzahl Teilnehmer pro Mannschaften")
        tTip.SetToolTip(nudVwert, "Anzahl Teilnehmer pro Mannschaften, die in die Wertung eingehen")
        tTip.SetToolTip(nudVwert, "Anzahl weiblicher Teilnehmer pro Mannschaften, die nach Relativ-Wertung weiblich gewertet werden")

        tTip.SetToolTip(chkLsex, "wenn aktiviert, müssen beide Geschlechter vertreten sein")
        tTip.SetToolTip(lblLsex, "wenn aktiviert, müssen beide Geschlechter vertreten sein")
        tTip.SetToolTip(chkLadd, "wenn aktiviert, sind mehrere Mannschaften pro Verein erlaubt")
        tTip.SetToolTip(lblLadd, "wenn aktiviert, sind mehrere Mannschaften pro Verein erlaubt")
        tTip.SetToolTip(nudLmin, "Mindestanzahl der Teilnehmer pro Mannschaften")
        tTip.SetToolTip(nudLmax, "Anzahl Teilnehmer pro Mannschaften")
        tTip.SetToolTip(nudLwert, "Anzahl Teilnehmer pro Mannschaften, die in die Wertung eingehen")

        'tTip.SetToolTip(chkLosnummer, "Losnummern nach IWF-Regel automatisch vergeben")
        tTip.SetToolTip(nudG_m, "maximale Anzahl der Gruppen")
        tTip.SetToolTip(nudG_w, "maximale Anzahl der Gruppen")

        tTip.SetToolTip(optSex, "männlich 20-kg-Hantel / weiblich 15-kg-Hantel")
        tTip.SetToolTip(optAK, "- weiblich und männlich bis AK 14 immer 15-kg-Hantel mit Option 7- oder 5-kg-Hantel" & vbNewLine & "- ab AK 15 männlich 20-kg-Hantel")

        'tTip.SetToolTip(nudMinAge, "Mindestalter der Teilnehmer")
        tTip.SetToolTip(btnAG_Add, "neue Altersgruppe hinzufügen")

        'tTip.SetToolTip(txtKurz, "optionale Kurzbezeichnung für den Wettkampf" + vbNewLine + "(wird in Anzeigen bei überlangen Namen benutzt)")

        tTip.SetToolTip(optSex, "Auswahl ausschließlich nach Geschlecht ohne Altersbeschränkungen")
        tTip.SetToolTip(optAK, "Altersbeschränkungen werden bei der Auswahl berücksichtigt")

        tTip.SetToolTip(chkBigdisc, "große Scheiben bis 5 kg verwenden")
        'tTip.SetToolTip(btnDiscColor, "Verfügbarkeit, Farbe und Dicke der großen Scheiben")

        dpkDatumBis.CustomFormat = " "
        dpkMeldeschluss.CustomFormat = " "
        dpkMeldeschlussNachmeldung.CustomFormat = " "

        nudFaktor_m.Controls(0).Enabled = False
        nudFaktor_w.Controls(0).Enabled = False

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Refresh()
        Dim sx = {"m", "w"}
        Dim maxId As Integer = 0

#Region "Daten"
        Dim conn = New MySqlConnection(User.ConnString)
        Using conn
            Try
                conn.Open()
                cmd = New MySqlCommand("SELECT max(Wettkampf) maxId FROM wettkampf;", conn)
                Dim reader = cmd.ExecuteReader()
                If reader.HasRows Then
                    reader.Read()
                    If Not IsDBNull(reader!maxId) Then maxId = CInt(reader!maxId)
                End If
                reader.Close()
                'Liste der Orte
                Query = "SELECT DISTINCT Ort FROM wettkampf;"
                cmd = New MySqlCommand(Query, conn)
                dtOrt.Load(cmd.ExecuteReader)
                'Liste der Vereine
                Query = "SELECT * FROM verein ORDER BY Vereinsname ASC;"
                cmd = New MySqlCommand(Query, conn)
                dtVerein.Load(cmd.ExecuteReader)
                ' BigDisc
                Query = "SELECT * FROM hantel WHERE Bezeichnung = 'Dummy' ORDER BY ID;"
                cmd = New MySqlCommand(Query, conn)
                dtDisc.Load(cmd.ExecuteReader)
                ' Wettkampf
                Query = "Select * FROM wettkampf WHERE Wettkampf = " & WK_ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtWettkampf.Load(cmd.ExecuteReader)
                ' Athletik
                Query = "Select ad.idDisziplin, ad.Bezeichnung, wa.Disziplin = ad.idDisziplin Checked, wa.AKs " &
                        "FROM athletik_disziplinen ad " &
                        "left join wettkampf_athletik wa On wa.Disziplin = ad.idDisziplin And wa.Wettkampf = " & WK_ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtAthletik.Load(cmd.ExecuteReader)
                ' Bezeichnung
                Query = "Select * FROM wettkampf_bezeichnung;"
                cmd = New MySqlCommand(Query, conn)
                dtBezeichnung.Load(cmd.ExecuteReader)
                ' Details
                Query = "SELECT * FROM wettkampf_details WHERE Wettkampf = " & WK_ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtDetails.Load(cmd.ExecuteReader)
                ' Gewichtsgruppen-Einteilung (Schüler/Jugend)
                Query = "SELECT * FROM wettkampf_gewichtsgruppen WHERE Wettkampf = " & WK_ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtGruppen.Load(cmd.ExecuteReader)
                ' Mannschaftsstärke, -wertung
                Query = "SELECT * FROM wettkampf_mannschaft WHERE Wettkampf = " & WK_ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtMann_Wertung.Load(cmd.ExecuteReader)
                ' Meldefristen, Startgeld
                Query = "SELECT * FROM wettkampf_meldefrist WHERE Wettkampf = " & WK_ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtMeldefrist.Load(cmd.ExecuteReader)
                Query = "select distinct ak.International, 
                            (select group_concat(idAltersklasse order by idAltersklasse) 
                            from altersklassen 
                            where Altersklasse = ak.Altersklasse 
                            group by Altersklasse) AK, 
                            ak.Altersklasse, 
                            min(ak.von) over(partition by ak.Altersklasse) minAge, 
                            max(ak.bis) over(partition by ak.Altersklasse) maxAge, 
                            concat(min(ak.von) over(partition by ak.Altersklasse), ' - ', max(ak.bis) over(partition by ak.Altersklasse)) JG, 
                            wa.Wertung idWertung, wa.Wertung2 idWertung2, wa.Gruppierung 
                            from altersklassen ak 
                            left join (Select * from wettkampf_wertung where Wettkampf = " & WK_ID & ") wa On wa.idAK = ak.idAltersklasse 
                            left join wertung w on w.idWertung = wa.Wertung 
                            order by ak.idAltersklasse;"
                cmd = New MySqlCommand(Query, conn)
                dtAK.Load(cmd.ExecuteReader)
                ' Startgebühr
                Query = "select ak.International, ak.idAltersklasse, ak.Altersklasse, ws.Gebuehr, ws.NM_Gebuehr, ws.Gebuehr_oL, ws.NM_Gebuehr_oL, max(ak.bis) maxAge 
                        from altersklassen ak
                        left join wettkampf_startgeld ws on ws.AK = ak.idAltersklasse and ws.Wettkampf = " & WK_ID & "
                        group by ak.Altersklasse
                        union
                        select false International, 0 idAltersklasse, 'alle' Altersklasse, Gebuehr, NM_Gebuehr, Gebuehr_oL, NM_Gebuehr_oL, 120
                        from wettkampf_startgeld
                        where Wettkampf = " & WK_ID & " and AK = 0
                        order by idAltersklasse;"
                cmd = New MySqlCommand(Query, conn)
                dtGebühr = New DataTable
                dtGebühr.Load(cmd.ExecuteReader)
                Dim alle = dtGebühr.Select("idAltersklasse = 0 and International = false")
                If alle.Count = 0 Then
                    dtGebühr.Rows.Add({False, 0, "alle", DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, 120})
                End If
                alle = dtGebühr.Select("idAltersklasse = 0 and International = true")
                If alle.Count = 0 Then
                    dtGebühr.Rows.Add({True, 0, "alle", DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, 120})
                End If
                ' Multi-WK (Verknüpfung)
                Query = "
                    select b.Wettkampf, b.Bez1, abs((not ifnull(m.Multi, w.Wettkampf = " & WK_ID & ")) - 1) Joined, m.Multi, ifnull(m.Primaer, 0) Primaer, w.Datum 
                    from wettkampf_bezeichnung b 
                    left join wettkampf_multi m on m.Wettkampf = b.Wettkampf 
                    left join wettkampf w on w.Wettkampf = b.Wettkampf 
                    where isnull(m.Multi) or find_in_set('" & WK_ID & "', m.Multi) != 0 
                    order by w.Datum desc, b.Wettkampf desc;"
                cmd = New MySqlCommand(Query, conn)
                dtMulti.Load(cmd.ExecuteReader)
                ' SameAK
                Query = "select distinct wr.age_id, ak.Altersklasse, ak.International 
                        from world_records wr 
                        left join altersklassen ak on ak.idAltersklasse = wr.age_id 
                        order by wr.age_id;"
                cmd = New MySqlCommand(Query, conn)
                dtSameAK.Load(cmd.ExecuteReader)

                Using s As New DataService
                    dtDefaults = s.Get_Athletik_Defaults({WK_ID},, conn) ' Athletik-Defaults (Wettkampf=0 für athletik_defaults, Wettkampf=WK_ID für wk_athletik_defaults)
                    dtGruppenDef = s.Get_Modus_Gewichtsgruppen(conn)
                End Using

            Catch ex As MySqlException
                LogMessage("Wettkampf: Shown: " & ex.Message, ex.StackTrace)
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Datenbank-Fehler (Wettkampf einlesen)" & vbNewLine & "Weiterführende Hinweise im Log-Verzeichnis", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
                ConnError = True
                Close()
                formMain.Change_MainProgressBarValue(0)
                Exit Sub
            End Try
        End Using
#End Region

        formMain.Change_MainProgressBarValue(70)

        dvWettkampf = New DataView(dtWettkampf)

#Region "Grid für Multi-WK"
        dvMulti = New DataView(dtMulti)
        bsMulti = New BindingSource With {.DataSource = dvMulti}
        Dim pos = bsMulti.Find("Wettkampf", WK_ID)
        If pos > -1 Then
            dvMulti(pos)!Joined = True
        End If
        bsMulti.EndEdit()
        With dgvMulti
            .AutoGenerateColumns = False
            .DataSource = bsMulti
        End With
        tabMulti.Enabled = dvMulti.Count > 1
#End Region

        dvBezeichnung = New DataView(dtBezeichnung, "Wettkampf = " & WK_ID, Nothing, DataViewRowState.CurrentRows)

        dvModus = New DataView(Glob.dtModus) With {.Sort = "idModus"}

        dvDisc = New DataView(dtDisc)
        With dgvBigDisc
            .AutoGenerateColumns = False
            .Size = New Size(188, 134)
            .DataSource = dvDisc
        End With
        Color_Disc()

        Dim ColorList = New List(Of String)
        Dim colorType As Type = GetType(Color)
        Dim propInfoList As Reflection.PropertyInfo() = colorType.GetProperties(Reflection.BindingFlags.[Static] Or Reflection.BindingFlags.DeclaredOnly Or Reflection.BindingFlags.[Public])

        For Each c As Reflection.PropertyInfo In propInfoList
            ColorList.Add(c.Name)
        Next
        With lstColors
            .Items.AddRange(ColorList.ToArray)
            .Height = 157
        End With

        loading = True
        formMain.Change_MainProgressBarValue(80)

#Region "Tabelle Athletik"
        dvA = New DataView(dtAthletik)
        bsA = New BindingSource With {.DataSource = dvA}
        With dgvAthletik
            .AutoGenerateColumns = False
            .DataSource = bsA
        End With
#End Region


        loading = False

        '#Region "Liste Veranstalter"
        '        dvVeranstalter = New DataView(dtVerein)
        '        With cboVeranstalter
        '            .DataSource = dvVeranstalter
        '            .DisplayMember = "Vereinsname"
        '            .ValueMember = "idVerein"
        '            .SelectedIndex = -1
        '        End With
        '#End Region

#Region "Liste Vereine"
        dvVerein = New DataView(dtVerein)
        bsVerein = New BindingSource With {.DataSource = dvVerein}
        With lstVereine
            .DataSource = bsVerein
            .DisplayMember = "Vereinsname"
            .ValueMember = "idVerein"
            .SelectedIndex = -1
        End With
#End Region

#Region "Teams für Mannschafts-WK (z.B. Liga)"
        dvTeam = New DataView(Glob.dtTeams) With {.Sort = "Id"}
        bsTeam = New BindingSource With {.DataSource = dvTeam}

        For i = 0 To dvTeam.Count - 1
            If i > 2 Then btnAdd.PerformClick()
            With txtTeam(i)
                If i >= dvTeam.Count Then
                    btnAdd.PerformClick()
                End If
                .Tag = dvTeam(i)!Team
                .Text = dvVerein(bsVerein.Find("idVerein", .Tag))!Vereinsname.ToString
            End With
        Next
#End Region

#Region "Liste Modi"
        With cboModus
            .DataSource = dvModus
            .DisplayMember = "Bezeichnung"
            .ValueMember = "idModus"
            .SelectedValue = -1
        End With
#End Region

#Region "Liste Orte"
        Dim _ort = dtOrt.AsEnumerable().Select(Function(d) d("Ort")).ToArray
        cboOrt.Items.AddRange(_ort)
#End Region

#Region "Listen WK-Bezeichnung"
        Dim dv As DataView
        dv = New DataView(dtBezeichnung)
        Dim dt = dv.ToTable(True, "Bez1")
        Dim val = dt.AsEnumerable().Where(Function(d) Not d("Bez1").Equals("")).ToArray.AsEnumerable().Select(Function(d) d("Bez1")).ToArray
        cboBezeichnung1.Items.AddRange(val)
        dt = dv.ToTable(True, "Bez2")
        val = dt.AsEnumerable().Where(Function(d) Not d("Bez2").Equals("")).ToArray.AsEnumerable().Select(Function(d) d("Bez2")).ToArray
        cboBezeichnung2.Items.AddRange(val)
        dt = dv.ToTable(True, "Bez3")
        val = dt.AsEnumerable().Where(Function(d) Not d("Bez3").Equals("")).ToArray.AsEnumerable().Select(Function(d) d("Bez3")).ToArray
        cboBezeichnung3.Items.AddRange(val)
#End Region

#Region "Mannschaften"
        With dtMann_Wertung
            If .Rows.Count > 0 AndAlso CInt(.Rows(0)("Wettkampf")) > 0 Then
                For i = 0 To .Rows.Count - 1
                    Dim ix = CInt(.Rows(i)("idMannschaft"))
                    CType(dicMannschaft(ix)(0), CheckBox).Checked = True
                    CType(dicMannschaft(ix)(1), NumericUpDown).Value = CInt(.Rows(i)("min"))
                    CType(dicMannschaft(ix)(2), NumericUpDown).Value = CInt(.Rows(i)("max"))
                    CType(dicMannschaft(ix)(3), NumericUpDown).Value = CInt(.Rows(i)("gewertet"))
                    CType(dicMannschaft(ix)(4), CheckBox).Checked = CBool(.Rows(i)("m_w"))
                    CType(dicMannschaft(ix)(5), CheckBox).Checked = CBool(.Rows(i)("multi"))
                    If dicMannschaft(ix).Contains(nudVrel) Then CType(dicMannschaft(ix)(6), NumericUpDown).Value = CInt(.Rows(i)("relativ"))
                Next
            End If
        End With
#End Region

#Region "Gewichtsgruppen"
        dv = New DataView(dtGruppen) ' gespeicherte GGs des WKs
        If dv.Count = 0 Then dv = New DataView(dtGruppenDef) ' Default-Werte 
        Dim tst = From g In dv.ToTable
                  Group g By sex = g.Field(Of String)("Geschlecht"), anz = g.Field(Of Integer)("Heber") Into Group
        Dim grp = tst.GroupBy(Function(x) x.sex).ToDictionary(Function(t) t.Key, Function(a) a.ToList())
        Dim chk = {chkEqual_m, chkEqual_w}    ' alle Gruppen gleich
        Dim nud = {nudG_m, nudG_w}            ' Anzahl GGs
        Dim nudGG = {nudGG_m, nudGG_w}        ' Anzahl Heber/Gruppe
        Dim cnt = 0
        For i = 0 To 1
            chk(i).Checked = grp(sx(i)).Count = 1
            If chk(i).Checked Then
                cnt = grp(sx(i))(0).Group.Count
            Else
                cnt = grp(sx(i)).Count
                For c = 0 To cnt - 1
                    nudGG(i)(c).Value = grp(sx(i))(c).anz
                Next
            End If
            nud(i).Value = cnt
        Next
#End Region

#Region "Restrictions"
        For Each row As DataRow In Glob.dtRestrict.Rows
            If CInt(row!Durchgang) = 0 Then
                chkRestrict_R.Checked = True
                nudRestrict_R_AK.Value = CInt(row!Altersklasse)
                nudRestrict_R_V.Value = CInt(row!Versuche)
            End If
            If CInt(row!Durchgang) = 1 Then
                chkRestrict_S.Checked = True
                nudRestrict_S_AK.Value = CInt(row!Altersklasse)
                nudRestrict_S_V.Value = CInt(row!Versuche)
            End If
        Next
#End Region

#Region "Altersgruppen"
        ' Tabelle Altersklassen (links)
        dtAKs.Columns.Add(New DataColumn With {.ColumnName = "Check", .DataType = GetType(Boolean)})
        dtAKs.Columns.Add(New DataColumn With {.ColumnName = "Sex", .DataType = GetType(String)})
        dtAKs.Columns.Add(New DataColumn With {.ColumnName = "AK", .DataType = GetType(Integer)})
        dtAKs.Columns.Add(New DataColumn With {.ColumnName = "JG", .DataType = GetType(Integer)})
        For s = 0 To 1
            For i = 7 To 17
                dtAKs.Rows.Add({False, sx(s), i, Year(dpkDatum.Value) - i})
            Next
        Next
        dvAKs = New DataView(dtAKs)
        bsAKs = New BindingSource With {.DataSource = dvAKs}
        If cboModus.SelectedIndex > -1 Then
            Dim ix = dvModus.Find(cboModus.SelectedValue)
            bsAKs.Filter = "Sex = 'm' AND (AK >= " & dvModus(ix)!MinAlter.ToString & " AND AK <= " & dvModus(ix)!MaxAlter.ToString & ")"
        End If
        bsAKs.Sort = "Sex, AK"
        dgvAKs.AutoGenerateColumns = False
        dgvAKs.DataSource = bsAKs
        ' Tabelle Altersgruppen (rechts)
        bsAGs = New BindingSource With {.DataSource = Glob.dvAG}
        bsAGs.Sort = "Reihenfolge"
        dgvAGs.AutoGenerateColumns = False
        dgvAGs.DataSource = bsAGs
        If bsAGs.Count = 0 Then
            bsAGs.AddNew()
            Glob.dvAG(0)!Reihenfolge = 0
        Else
            For i = 0 To 1
                AKs_Set_CheckValue(Glob.dtAG.Select("Sex = '" & sx(i) & "'"), True)
            Next
        End If
#End Region

#Region "Wertung"
        'bsSameAK = New BindingSource With {.DataSource = dvAK, .Filter = "NOT AK LIKE '%,%'"}
        bsSameAK = New BindingSource With {.DataSource = dtSameAK}
        With cboSameAK
            .DataSource = bsSameAK
            .ValueMember = "age_id"
            .DisplayMember = "Altersklasse"
        End With

        dvAK = New DataView(dtAK)
        bsAK = New BindingSource With {.DataSource = dvAK}

        Set_bsAK_Filter()

        With dgvWertung2
            .AutoGenerateColumns = False
            .DataSource = bsAK
        End With

        bsWertung = New BindingSource With {
            .DataSource = Glob.dtWertung,
            .Filter = "idWertung < 6"}

        WertungCombo_Fill(dgvWertung2, "Wertung2", "Kurz")

        With cboWertung2
            .DataSource = bsWertung
            .ValueMember = "idWertung"
            .DisplayMember = "Bezeichnung"
            .SelectedIndex = -1
        End With
#End Region

#Region "Gebühr"

        dvGebühr = New DataView(dtGebühr)
        bsGebühr = New BindingSource With {.DataSource = dvGebühr}
        Set_bsGebühr_Filter()

        With dgvGebühr
            .AutoGenerateColumns = False
            .DataSource = bsGebühr
        End With

#End Region

        '------------------------------------------------------------------------------------
        If dvWettkampf.Count = 0 Then WK_New((maxId + 1).ToString)
        loading = True
        WK_Binding()
        loading = False
        Refresh()
        formMain.Change_MainProgressBarValue(90)
        '------------------------------------------------------------------------------------

#Region "Tabelle Athletik-Defaults"
        dvD = New DataView(dtDefaults)
        bsD = New BindingSource With {.DataSource = dvD}
        With dgvDefaults
            .AutoGenerateColumns = False
            .DataSource = bsD
        End With
        Set_dgvDefaults_ColumnsVisible()
        bsA_PositionChanged(bsA, Nothing)
#End Region

#Region "Platzierung"
        'Dim Score = New List(Of Double)
        'For i = 1 To dvWettkampf(0)!Platzierung.ToString.Length
        '    Score.Add(Conversion.Val(GetChar(dvWettkampf(0)!Platzierung.ToString, i)))
        'Next
        Dim Score = New List(Of String)
        For i = 0 To dvWettkampf(0)!Platzierung.ToString.Length - 1
            Score.Add(dvWettkampf(0)!Platzierung.ToString.Substring(i, 1))
        Next

        If Score.Count > 0 Then
            Select Case Score(0)
                Case "1"
                    optWettkampf.Checked = True
                Case "2"
                    optGruppen.Checked = True
                Case "3"
                    optAltersklassen.Checked = True
                Case "4"
                    optGewichtsklassen.Checked = True
            End Select
        End If

        If Score.Count > 1 Then
            chkPlatzierung2.Checked = True
            Select Case Score(1)
                Case "1"
                    optWettkampf2.Checked = True
                Case "2"
                    optGruppen2.Checked = True
                Case "3"
                    optAltersklassen2.Checked = True
                Case "4"
                    optGewichtsklassen2.Checked = True
                Case "5"
                    optWeiblich2.Checked = True
            End Select
        End If

        If Score.Count > 2 Then cboWertung2.SelectedValue = Score(2)
#End Region


        mnuSpeichern.Enabled = Not IsNothing(mnuSpeichern.Tag)
        mnuSpeichern.Tag = Nothing
        Settings_Changed = False

        With tabCollection
            .Visible = True
            If .Contains(tabMulti) Then .SelectTab(tabMulti)
            If .Contains(tabPlatzierung) Then .SelectTab(tabPlatzierung)
            If .Contains(tabMannschaft) Then .SelectTab(tabMannschaft)
            If .Contains(tabAthletik) Then .SelectTab(tabAthletik)
        End With

        '' Tooltip ändern, wenn kein internationaler WK
        'If Not Wettkampf.International Then ToolTip1.SetToolTip(chkLosnummer, "Losnummern automatisch vergeben")

        If tabCollection.Contains(tabPlatzierung) AndAlso Not IsDBNull(dtWettkampf(0)!SameAK) Then
            cboSameAK.SelectedValue = CInt(dtWettkampf(0)!SameAK)
        Else
            cboSameAK.SelectedIndex = -1
        End If
        chkSameAK.Checked = cboSameAK.SelectedIndex > -1

        For i = 0 To txtTeam.Count - 1
            pnlTeam(i).Button.Refresh()
        Next

        formMain.Change_MainProgressBarValue(0)
        Cursor = Cursors.Default
    End Sub

    Private Sub Set_dgvDefaults_ColumnsVisible()
        For Each col As DataGridViewColumn In dgvDefaults.Columns
            If col.Name.Contains("ak") Then
                col.Visible = CInt(col.Name.Substring(2)) >= nudMinAge.Value
            End If
        Next
    End Sub

    Private Sub Set_bsGebühr_Filter(Optional SkipCheck As Boolean = False)
        If IsNothing(bsGebühr) Then Return

        bsGebühr.Filter = "International = " & Math.Abs(CInt(chkInternational.Checked)) & " And maxAge >= " & nudMinAge.Value & " And idAltersklasse " & If(chkGebührNachAk.Checked, " > 0", " = 0")

        If SkipCheck Then Return

        chkGebührNachAk.Checked = (From x In dvGebühr.ToTable
                                   Where x.Field(Of Integer)("idAltersklasse") > 0 AndAlso x.Field(Of Double?)("Gebuehr") > 0
                                   Select x.Field(Of Integer)("idAltersklasse")).Count > 0


    End Sub

    Private Sub WertungCombo_Fill(Sender As DataGridView, ColumnName As String, Optional DisplayMember As String = "Bezeichnung")
        Dim combo As DataGridViewComboBoxColumn = CType(Sender.Columns(ColumnName), DataGridViewComboBoxColumn)
        combo.DataSource = bsWertung
        combo.ValueMember = "idWertung"
        combo.DisplayMember = DisplayMember
    End Sub

    Private Sub Color_Disc()
        With dgvBigDisc
            For i = 0 To dvDisc.Count - 1
                .Rows(i).Cells("Farbe").Style.BackColor = Color.FromName(.Rows(i).Cells("Farbe").Value.ToString)
            Next
        End With
    End Sub
    Private Sub WK_Binding()
        bsWK.DataSource = dvWettkampf
        bsBezeichnung.DataSource = dvBezeichnung
        bsMeldefrist.DataSource = dtMeldefrist
        bsDetails.DataSource = dtDetails

        'cboVeranstalter.DataBindings.Add(New Binding("SelectedValue", bsWK, "Veranstalter"))

        txtVeranstalter.DataBindings.Add(New Binding("Tag", bsWK, "Veranstalter"))
        If Not IsDBNull(txtVeranstalter.Tag) AndAlso Not txtVeranstalter.Tag.ToString.Equals("0") Then
            txtVeranstalter.Text = dvVerein(bsVerein.Find("idVerein", txtVeranstalter.Tag))!Vereinsname.ToString
        End If

        dpkDatum.DataBindings.Add(New Binding("Text", bsWK, "Datum"))
        dpkDatumBis.DataBindings.Add(New Binding("Text", bsWK, "DatumBis"))
        dpkMeldeschluss.DataBindings.Add(New Binding("Text", bsMeldefrist, "Datum1"))
        dpkMeldeschlussNachmeldung.DataBindings.Add(New Binding("Text", bsMeldefrist, "Datum2"))

        txtID.DataBindings.Add(New Binding("Text", bsWK, "Wettkampf", False, DataSourceUpdateMode.OnPropertyChanged))
        cboOrt.DataBindings.Add(New Binding("Text", bsWK, "Ort"))
        'cboKR.DataBindings.Add(New Binding("Text", bsWK, "Kampfrichter", False, DataSourceUpdateMode.Never))

        If Not IsDBNull(dvWettkampf(0)!Kampfrichter) Then
            cboKR.Text = dvWettkampf(0)!Kampfrichter.ToString
        End If

        'chkMannschaft.DataBindings.Add(New Binding("Checked", bsWK, "Mannschaft"))
        'chkMannschaft.Checked = CBool(dtWettkampf(0)!Mannschaft)

        chkBigdisc.DataBindings.Add(New Binding("Checked", bsWK, "BigDisc"))
        chkFlagge.DataBindings.Add(New Binding("Checked", bsWK, "Flagge"))
        cboModus.DataBindings.Add(New Binding("SelectedValue", bsWK, "Modus"))

        cboBezeichnung1.DataBindings.Add(New Binding("Text", bsBezeichnung, "Bez1"))
        cboBezeichnung2.DataBindings.Add(New Binding("Text", bsBezeichnung, "Bez2"))
        cboBezeichnung3.DataBindings.Add(New Binding("Text", bsBezeichnung, "Bez3"))
        txtKurz.DataBindings.Add(New Binding("Text", bsWK, "Kurz"))
        'txtStartgeld.DataBindings.Add(New Binding("Text", bsMeldefrist, "Gebuehr1"))
        'txtStartgeld_Nachmeldung.DataBindings.Add(New Binding("Text", bsMeldefrist, "Gebuehr2"))
        txtStartgeld_M_Verein.DataBindings.Add(New Binding("Text", bsMeldefrist, "Verein"))
        txtStartgeld_M_Land.DataBindings.Add(New Binding("Text", bsMeldefrist, "Land"))
        txtStartgeld_M_Liga.DataBindings.Add(New Binding("Text", bsMeldefrist, "Liga"))
        nudSteigerung1.DataBindings.Add(New Binding("Value", bsDetails, "Steigerung1"))
        nudSteigerung2.DataBindings.Add(New Binding("Value", bsDetails, "Steigerung2"))
        chkRegel20.DataBindings.Add(New Binding("Checked", bsDetails, "Regel20"))
        chkInternational.DataBindings.Add(New Binding("Checked", bsDetails, "International"))
        chkLosnummer.DataBindings.Add(New Binding("Checked", bsDetails, "AutoLosnummer"))
        'chkAuswertung.DataBindings.Add(New Binding("Checked", bsWK, "ZK_2017"))
        txtZK_m.DataBindings.Add(New Binding("Text", bsDetails, "ZK_m"))
        txtZK_w.DataBindings.Add(New Binding("Text", bsDetails, "ZK_w"))
        txtR_m.DataBindings.Add(New Binding("Text", bsDetails, "R_m"))
        txtR_w.DataBindings.Add(New Binding("Text", bsDetails, "R_w"))
        txtS_m.DataBindings.Add(New Binding("Text", bsDetails, "S_m"))
        txtS_w.DataBindings.Add(New Binding("Text", bsDetails, "S_w"))
        txtHantelstange.DataBindings.Add(New Binding("Text", bsDetails, "Hantelstange")) ', False, DataSourceUpdateMode.OnPropertyChanged))
        chkWeitererVersuch.DataBindings.Add(New Binding("Checked", bsDetails, "ThirdAttempt"))
        chkVersucheSortieren.DataBindings.Add(New Binding("Checked", bsDetails, "OrderByLifter"))

        nudFaktor_m.DataBindings.Add(New Binding("Text", bsDetails, "Faktor_m"))
        nudFaktor_w.DataBindings.Add(New Binding("Text", bsDetails, "Faktor_w"))

        chkFinale.DataBindings.Add(New Binding("Checked", bsWK, "Finale"))
        'cboSameAK.DataBindings.Add(New Binding("SelectedValue", bsWK, "SameAK"))
        chkUseAgeGroups.DataBindings.Add(New Binding("Checked", bsWK, "UseAgeGroups"))
        chkIgnoreSex.DataBindings.Add(New Binding("Checked", bsWK, "IgnoreSex"))

        nudMinAge.DataBindings.Add(New Binding("Value", bsWK, "Mindestalter"))

        'nudRestrict_AK1.DataBindings.Add(New Binding("Value", Glob.dtRestrict, "Altersklasse"))
        'chkRestrict_R1.DataBindings.Add(New Binding("Checked", Glob.dtRestrict, "Reissen"))
        'chkRestrict_S1.DataBindings.Add(New Binding("Checked", Glob.dtRestrict, "Stossen"))
        'nudRestrict_R1.DataBindings.Add(New Binding("Value", Glob.dtRestrict, "ReissenVersuche"))
        'nudRestrict_S1.DataBindings.Add(New Binding("Value", Glob.dtRestrict, "StossenVersuche"))

        'loading = True
        'With cboEinteilung
        '    If Not IsDBNull(dtWettkampf(0)("Alterseinteilung")) AndAlso .Text = "" Then
        '        .SelectedIndex = .Items.IndexOf(dtWettkampf(0)("Alterseinteilung").ToString) ' prüft ob GewichtsGruppen gespeichert sind
        '        If .SelectedIndex = -1 Then                                                  ' Gewichtsgruppen-String gespeichert
        '            Dim count = Set_CheckState(dtWettkampf(0)("Alterseinteilung").ToString)  ' lstAlterklassen aktualisieren, Gruppen füllen und Anzahl der Gruppen zurückgeben
        '            .Text = "Altersgruppen"
        '        End If
        '    End If
        'End With
        'loading = False

        'loading = True
        'For i = 0 To Glob.dtTeams.Rows.Count - 1
        '    Team(i).SelectedValue = Glob.dtTeams.Rows(i)("Team")
        'Next
        'loading = False

        'Try
        '    If dvWettkampf.Count = 1 Then
        '        If CInt(dvWettkampf(0)!Mindestalter) > 0 Then
        '            nudMinAge.Value = CInt(dvWettkampf(0)!Mindestalter)
        '        ElseIf cboModus.SelectedIndex > -1 Then
        '            nudMinAge.Value = CInt(CType(cboModus.SelectedItem, DataRowView)("MinAlter"))
        '        End If
        '    End If
        'Catch ex As Exception
        'End Try

        'txtStartgeld.Text = Format(txtStartgeld.Text, "Fixed")
        'txtStartgeld_Nachmeldung.Text = Format(txtStartgeld_Nachmeldung.Text, "Fixed")
        txtStartgeld_M_Land.Text = Format(txtStartgeld_M_Land.Text, "Fixed")
        txtStartgeld_M_Liga.Text = Format(txtStartgeld_M_Liga.Text, "Fixed")
        txtStartgeld_M_Verein.Text = Format(txtStartgeld_M_Verein.Text, "Fixed")

        If Not IsDBNull(dvWettkampf(0)!DatumBis) Then dpkDatumBis.Format = DateTimePickerFormat.Short
        If Not IsDBNull(dtMeldefrist(0)!Datum1) Then dpkMeldeschluss.Format = DateTimePickerFormat.Short
        If Not IsDBNull(dtMeldefrist(0)!Datum2) Then dpkMeldeschlussNachmeldung.Format = DateTimePickerFormat.Short

    End Sub
    Private Sub WK_New(NewId As String) ' neuen WK anlegen
        WK_ID = NewId

        With dtWettkampf
            Dim row As DataRow = .NewRow
            row!Wettkampf = WK_ID
            row!Modus = 0 'cmd.Parameters("@mode").Value
            row!Datum = Now 'cmd.Parameters("@Date").Value
            'row!DatumBis = cmd.Parameters("@bis").Value
            row!Veranstalter = 0 'cmd.Parameters("@verein").Value
            row!Ort = "" 'cmd.Parameters("@ort").Value
            row!BigDisc = True ' cmd.Parameters("@bigdisc").Value
            'row!Kampfrichter = cmd.Parameters("@kr").Value
            row!Mannschaft = False 'cmd.Parameters("@team").Value
            row!Flagge = False 'cmd.Parameters("@flag").Value
            row!MindestAlter = nudMinAge.Minimum 'cmd.Parameters("@age").Value
            'row!Alterseinteilung = cmd.Parameters("@group").Value
            'row!ZK_2017 = False
            row!Platzierung = 0
            row!Kurz = String.Empty
            row!GUID = String.Empty
            row!Finale = 0
            row!SameAK = 0
            row!UseAgeGroups = False
            row!IgnoreSex = False
            .Rows.Add(row)
        End With
        With dtBezeichnung
            Dim row As DataRow = .NewRow
            row!Wettkampf = WK_ID
            row!Bez1 = String.Empty
            .Rows.Add(row)
        End With
        With dtDetails
            Dim row As DataRow = .NewRow
            row!Wettkampf = WK_ID
            row!Hantelstange = "s"
            row!Steigerung1 = 1
            row!Steigerung2 = 1
            row!ZK_m = 0
            row!ZK_w = 0
            row!R_m = 0
            row!S_m = 0
            row!R_w = 0
            row!s_w = 0
            row!Aks_m = -1
            row!AKs_w = -1
            row!Regel20 = 0
            row!nurZK = 0
            row!International = 0
            row!AutoLosnummer = False
            row!ThirdAttempt = False
            row!OrderByLifter = False
            row!Faktor_m = 1
            row!Faktor_w = 1
            .Rows.Add(row)
        End With
        With dtGruppen
            Dim row As DataRow = .NewRow
            row!Wettkampf = WK_ID
            row!Geschlecht = ""
            row!Gruppe = 0
            row!Heber = 0
            .Rows.Add(row)
        End With
        With dtMeldefrist
            Dim row As DataRow = .NewRow
            row!Wettkampf = WK_ID
            .Rows.Add(row)
        End With

        txtVeranstalter.Text = String.Empty
        txtVeranstalter.Tag = Nothing

        For i = 0 To 2
            txtTeam(i).Text = String.Empty
            txtTeam(i).Tag = Nothing
        Next
        ' alle übrigen Teams entfernen
        For i = txtTeam.Count To 3 Step -1
            btnRemove.PerformClick()
        Next

        chkSameAK.Checked = False
    End Sub

    '' Panel Mannschaften
    Private Sub pnlMannschaft_EnabledChanged(sender As Object, e As EventArgs) Handles pnlMannschaft.EnabledChanged
        btnAdd.Enabled = pnlMannschaft.Enabled
        btnRemove.Enabled = pnlMannschaft.Enabled
        For Each item In pnlTeam
            item.Style = If(pnlMannschaft.Enabled, ButtonState.Normal, ButtonState.Disabled)
            item.Button.Refresh()
        Next
    End Sub
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        ' ComboBox für weitere Mannschaft erstellen
        Dim txt As New TextBox
        With txt
            .Name = "txt" & txtTeam.Count
            .Parent = pnlMannschaft
            .Parent.Controls.Add(txt)
            .MaximumSize = txtTeam(0).MaximumSize
            .MinimumSize = txtTeam(0).MinimumSize
            .Size = txtTeam(0).Size
            .Location = New Point(txtTeam(0).Left, txtTeam(0).Top + txtTeam.Count * 30)
            .Anchor = txtTeam(0).Anchor
            .Visible = True
            .Enabled = True
            .TabStop = False
            AddHandler .TextChanged, AddressOf txtVereine_TextChanged
            AddHandler .GotFocus, AddressOf txtVereine_GotFocus
            AddHandler .KeyDown, AddressOf txtVereine_KeyDown
        End With
        txtTeam.Add(txt)
        ' Label für weitere Mannschaft erstellen
        Dim lbl As New Label
        With lbl
            .Name = "lbl" & lblTeam.Count
            .Parent = pnlMannschaft
            .Parent.Controls.Add(lbl)
            .AutoSize = False
            .Size = lblTeam(0).Size
            .Location = New Point(lblTeam(0).Left, lblTeam(0).Top + lblTeam.Count * 30)
            .Anchor = lblTeam(0).Anchor
            .ForeColor = lblTeam(0).ForeColor
            .Visible = True
            .Enabled = True
            .Text = "Gast " + (lblTeam.Count + 1).ToString ' Rolle aus dvTeam auslesen???
            .TextAlign = lblTeam(0).TextAlign
        End With
        lblTeam.Add(lbl)
        ' DropDown-Fake erstellen
        Dim pnl As New Panel
        With pnl
            .Name = "pnl" & pnlTeam.Count
            .Parent = pnlMannschaft
            .Parent.Controls.Add(pnl)
            .AutoSize = False
            .Size = pnlTeam(0).Button.Size
            .Location = New Point(pnlTeam(0).Button.Left, pnlTeam(0).Button.Top + pnlTeam.Count * 30)
            .Anchor = pnlTeam(0).Button.Anchor
            .Visible = True
            .Enabled = True
            .BringToFront()
            AddHandler .Paint, AddressOf pnlVereine_Paint
            AddHandler .MouseEnter, AddressOf pnlVereine_MouseEnter
            AddHandler .MouseLeave, AddressOf pnlVereine_MouseLeave
            AddHandler .MouseClick, AddressOf pnlVereine_MouseClick
        End With
        Dim btn = New DD_Button
        btn.Button = pnl
        btn.Style = ButtonState.Normal
        pnlTeam.Add(btn)
        pnlTeam(pnlTeam.Count - 1).Button.Refresh()

        If Not loading Then pnlMannschaft.ScrollControlIntoView(lbl)
        mnuSpeichern.Enabled = True
    End Sub
    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        If mnuMeldungen.Checked AndAlso Not String.IsNullOrEmpty(txtTeam(txtTeam.Count - 1).Text) Then
            Using New Centered_MessageBox(Me)
                If MessageBox.Show(txtTeam(txtTeam.Count - 1).Text & " wird aus der Mannschaftsliste gelöscht.",
                                   msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then Return
            End Using
        End If

        If txtTeam.Count > 3 Then
            ' Textbox entfernen
            With txtTeam(txtTeam.Count - 1)
                Dim found = bsTeam.Find("Team", .Tag)
                If found > -1 Then dvTeam.Delete(found)
                RemoveHandler .TextChanged, AddressOf txtVereine_TextChanged
                RemoveHandler .GotFocus, AddressOf txtVereine_GotFocus
                RemoveHandler .KeyDown, AddressOf txtVereine_KeyDown
                .Dispose()
            End With
            txtTeam.RemoveAt(txtTeam.Count - 1)
            ' Label entfernen
            lblTeam(lblTeam.Count - 1).Dispose()
            lblTeam.RemoveAt(lblTeam.Count - 1)
            ' Panel entfernen
            With pnlTeam(pnlTeam.Count - 1).Button
                RemoveHandler .Paint, AddressOf pnlVereine_Paint
                RemoveHandler .MouseEnter, AddressOf pnlVereine_MouseEnter
                RemoveHandler .MouseLeave, AddressOf pnlVereine_MouseLeave
                RemoveHandler .MouseClick, AddressOf pnlVereine_MouseClick
                .Dispose()
            End With
            pnlTeam.RemoveAt(pnlTeam.Count - 1)

            pnlMannschaft.ScrollControlIntoView(lblTeam(lblTeam.Count - 1))
            mnuSpeichern.Enabled = True
        End If
    End Sub

    '' TextBox Format #0.00
    Private Sub txtStartgeld_LostFocus(sender As Object, e As EventArgs) Handles txtStartgeld_M_Verein.LostFocus, txtStartgeld_M_Liga.LostFocus, txtStartgeld_M_Land.LostFocus
        Try
            With CType(sender, TextBox)
                .Text = Format(.Text, "Fixed")
            End With
        Catch ex As Exception
        End Try
    End Sub

    '' NumericInput
    Private Sub TextboxDecimal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtStartgeld_M_Liga.KeyDown,
                                                                                    txtStartgeld_M_Verein.KeyDown,
                                                                                    txtStartgeld_M_Land.KeyDown
        nonNumberEntered = False
        If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
            If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                If e.KeyCode <> Keys.Back AndAlso e.KeyCode <> Keys.Oemcomma AndAlso e.KeyCode <> Keys.Decimal Then
                    nonNumberEntered = True
                End If
            End If
        End If
        If ModifierKeys = Keys.Shift Then
            nonNumberEntered = True
        End If
    End Sub
    Private Sub TextboxInteger_KeyDown(sender As Object, e As KeyEventArgs) Handles txtS_m.KeyDown, txtS_w.KeyDown, txtZK_m.KeyDown, txtZK_w.KeyDown, txtR_m.KeyDown, txtR_w.KeyDown

        nonNumberEntered = False
        If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
            If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                If e.KeyCode <> Keys.Back Then
                    nonNumberEntered = True
                End If
            End If
        End If
        If ModifierKeys = Keys.Shift Then
            nonNumberEntered = True
        End If
    End Sub
    Private Sub TextboxNumber_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtStartgeld_M_Liga.KeyPress, txtStartgeld_M_Verein.KeyPress, txtStartgeld_M_Land.KeyPress,
                                                                                         txtS_m.KeyPress, txtS_w.KeyPress, txtZK_m.KeyPress, txtZK_w.KeyPress, txtR_m.KeyPress, txtR_w.KeyPress
        If nonNumberEntered = True Then
            e.Handled = True
        End If
    End Sub

    '' Hantelstange
    Private Sub pnlHantel_Click(sender As Object, e As EventArgs) Handles optSex.Click, optAK.Click
        txtHantelstange.Text = CType(sender, RadioButton).Tag.ToString
        If chk7kg.Enabled AndAlso chk7kg.Checked Then txtHantelstange.Text += "_" & chk7kg.Tag.ToString
        If chk5kg.Enabled AndAlso chk5kg.Checked Then txtHantelstange.Text += "_" & chk5kg.Tag.ToString
        If CType(sender, RadioButton).Focused Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub pnlHantelSub_Click(sender As Object, e As EventArgs) Handles chk7kg.Click, chk5kg.Click
        loading = True
        txtHantelstange.Text = txtHantelstange.Text.Substring(0, 1)
        If chk7kg.Checked Then txtHantelstange.Text += "_" & chk7kg.Tag.ToString
        If chk5kg.Checked Then txtHantelstange.Text += "_" & chk5kg.Tag.ToString
        loading = False
        If CType(sender, CheckBox).Focused Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub txtHantel_Changed(sender As Object, e As EventArgs) Handles txtHantelstange.TextChanged
        ' txtHantelstange ist an wettkampf_details.Hantelstange gebunden
        If Not loading Then
            With txtHantelstange.Text
                optSex.Checked = .Contains("s")
                optAK.Checked = .Contains("a")
                chk7kg.Checked = .Contains("k")
                chk5kg.Checked = .Contains("x")
            End With
            mnuSpeichern.Enabled = True
        End If
    End Sub

    '' Modus geändert
    Private Sub cboModus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboModus.SelectedIndexChanged
        Try
            If IsNothing(cboModus.SelectedValue) Then Return
            'Dim Modus = DirectCast(cboModus.SelectedItem, DataRowView)
            Dim ix = dvModus.Find(cboModus.SelectedValue)
            IsMehrkampf = dvModus(ix)!Wertung.Equals(6)

#Region "nudMinAge Minimum und Maximum setzen"
            If ix > -1 Then
                With nudMinAge
                    Dim change = .Maximum <> CInt(dvModus(ix)!MaxAlter) OrElse .Minimum <> CInt(dvModus(ix)!MinAlter)
                    .Minimum = CInt(dvModus(ix)!MinAlter)
                    .Maximum = CInt(dvModus(ix)!MaxAlter)
                    If change Then bsAKs.Filter = "Sex = '" & If(optAG_m.Checked, "m", "w") & "' AND (AK >= " & .Minimum.ToString & " AND AK <= " & .Maximum.ToString & ")" 'dgvAK_Fill()
                End With
                chkLosnummer.Checked = CBool(dvModus(ix)!AutoLosnummer)
                chkRegel20.Checked = CBool(dvModus(ix)!Regel20)
            End If
#End Region

#Region "Wertung"
            'If ix = -1 OrElse Not dvModus(ix)!Alterseinteilung.ToString = "Altersklassen" Then
            '    If tabCollection.Contains(tabWertung) Then
            '        tbWertung = tabCollection.TabPages("tabWertung")
            '        tabCollection.TabPages.Remove(tabWertung)
            '    End If
            'ElseIf dvModus(ix)!Alterseinteilung.ToString.Equals("Altersklassen") Then
            '    If Not tabCollection.Contains(tabWertung) Then tabCollection.TabPages.Insert(tabCollection.TabPages.Count - 1, tbWertung)
            '    tabCollection.SelectTab(tabWertung)
            '    If CInt(dvModus(ix)!Wertung) = 6 Then
            '        ' bei Mehrkampf 2.Platzierung deaktivieren (wird beim Speichern auf "Relativ" gesetzt)
            '        For Each ctl As Control In grpWertung2.Controls
            '            If TypeOf ctl Is RadioButton Then
            '                ctl.Enabled = Not CInt(dvModus(ix)!Wertung) = 6
            '                CType(ctl, RadioButton).Checked = Not CInt(dvModus(ix)!Wertung) = 6
            '            End If
            '        Next
            '    End If
            'End If
#End Region

#Region "AK-Beschränkung"
            If ix = -1 OrElse CInt(dvModus(ix)!Wertung) <> 6 Then
                If tabCollection.Contains(tabAusnahme) Then
                    tbAusnahme = tabCollection.TabPages("tabAusnahme")
                    tabCollection.TabPages.Remove(tabAusnahme)
                End If
            Else 'If CInt(dvModus(ix)!idModus) <= 110 Then
                If Not tabCollection.Contains(tabAusnahme) Then tabCollection.TabPages.Insert(tabCollection.TabPages.Count - 1, tbAusnahme)
                nudRestrict_R_AK.Maximum = CInt(dvModus(ix)!MaxAlter)
                nudRestrict_S_AK.Maximum = CInt(dvModus(ix)!MaxAlter)
            End If
#End Region

#Region "Altersgruppen"
            If ix = -1 OrElse Not dvModus(ix)!Alterseinteilung.ToString = "Altersgruppen" Then ' AndAlso Not CInt(dvModus(ix)!Wertung) = 6) Then
                If tabCollection.Contains(tabAltersgruppen) Then
                    tbAltersgruppen = tabCollection.TabPages("tabAltersgruppen")
                    tabCollection.TabPages.Remove(tabAltersgruppen)
                End If
            ElseIf dvModus(ix)!Alterseinteilung.ToString.Equals("Altersgruppen") OrElse CInt(dvModus(ix)!Wertung) = 6 Then
                If Not tabCollection.Contains(tabAltersgruppen) Then tabCollection.TabPages.Insert(tabCollection.TabPages.Count - 1, tbAltersgruppen)
                ' Controls aktualisieren
                'With nudMinAge
                '    .Maximum = CInt(dvModus(ix)("MaxAlter"))
                '    .Minimum = CInt(dvModus(ix)("MinAlter"))
                '    If WK_ID = 0 OrElse .Value < CInt(dvModus(ix)("MinAlter")) Then .Value = CInt(dvModus(ix)("MinAlter"))
                'End With
                'dgvAK_Fill()
            End If
#End Region

#Region "Gewichtsgruppen"
            If ix = -1 OrElse Not dvModus(ix)!Gewichtseinteilung.ToString = "Gewichtsgruppen" Then
                If tabCollection.Contains(tabGewichtsgruppen) Then
                    tbGewichtsgruppen = tabCollection.TabPages("tabGewichtsgruppen")
                    tabCollection.TabPages.Remove(tabGewichtsgruppen)
                End If
            ElseIf dvModus(ix)!Gewichtseinteilung.ToString = "Gewichtsgruppen" Then
                If Not tabCollection.Contains(tabGewichtsgruppen) Then tabCollection.TabPages.Insert(tabCollection.TabPages.Count - 1, tbGewichtsgruppen)
            End If
#End Region

#Region "Platzierung"
            ' cboModus.Tag = ix
            'If ix = -1 OrElse chkLiga.Checked Then
            '    If tabCollection.Contains(tabPlatzierung) Then
            '        tbPlatzierung = tabCollection.TabPages("tabPlatzierung")
            '        tabCollection.TabPages.Remove(tabPlatzierung)
            '    End If
            'ElseIf Not chkLiga.Checked Then
            '    ' angezeigt, wenn keine Mannschaftswertung und keine Mehrkampfwertung
            With tabCollection
                If Not .TabPages.Contains(tabPlatzierung) Then .TabPages.Insert(.TabPages.Count, tbPlatzierung)
                Select Case dvModus(ix)!Platzierung.ToString.Substring(0, 1)
                    Case "0"
                        optWettkampf.Checked = False
                        optGruppen.Checked = False
                        optAltersklassen.Checked = False
                        optGewichtsklassen.Checked = False
                    Case "1"
                        optWettkampf.Checked = True
                    Case "2"
                        optGruppen.Checked = True
                    Case "3"
                        optAltersklassen.Checked = True
                    Case "4"
                        optGewichtsklassen.Checked = True
                End Select
                chkPlatzierung2.Checked = False
                'chkAlleAksGleich.Checked = False
                .SelectTab(tabPlatzierung)
            End With
            'End If
#End Region

#Region "Athletik"
            With tabCollection
                If ix = -1 OrElse Not CBool(dvModus(ix)("Athletik")) Then
                    If .Contains(tabAthletik) Then
                        tbAthletik = .TabPages("tabAthletik")
                        .TabPages.Remove(tabAthletik)
                    End If
                ElseIf CBool(dvModus(ix)("Athletik")) = True Then
                    If Not .Contains(tabAthletik) Then .TabPages.Insert(0, tbAthletik)
                    'Dim combo As DataGridViewComboBoxColumn = CType(dgvAthletik.Columns("AKs"), DataGridViewComboBoxColumn)
                    'combo.Items.Clear()
                    'For i = nudMinAge.Minimum To nudMinAge.Maximum
                    '    combo.Items.Add(i.ToString)
                    'Next
                    .SelectTab(tabAthletik)
                End If
            End With
#End Region

#Region "Mannschaftswettkämpfe"
            With tabCollection
                If ix = -1 OrElse Not bsVerein.Count > 0 Then
                    If .Contains(tabMannschaft) Then
                        tbMannschaft = .TabPages("tabMannschaft")
                        .TabPages.Remove(tabMannschaft)
                    End If
                ElseIf CBool(dvModus(ix)("Athletik")) = True Then
                    If Not .Contains(tabMannschaft) Then .TabPages.Insert(0, tbMannschaft)
                    .SelectTab(tabMannschaft)
                End If
                chkLiga.Checked = CInt(dvModus(ix)!idModus) >= 600
            End With
#End Region

            cboModus.Tag = dvModus(ix)("idModus")

            If WK_ID.Equals("0") AndAlso ix > -1 Then
                nudVrel.Enabled = CInt(dvModus(ix)("Wertung")) = 2 ' tabMannschaft: Anzahl weiblicher TN, die Relativ W gewertet werden, der Rest der Mannschaft wird Relativ M gewertet
                With dtDetails
                    .Rows(0)("Hantelstange") = dvModus(ix)("Hantelstange")
                    .Rows(0)("Steigerung1") = dvModus(ix)("Steigerung1")
                    .Rows(0)("Steigerung2") = dvModus(ix)("Steigerung2")
                    .Rows(0)("Regel20") = dvModus(ix)("Regel20")
                End With
            End If

            mnuSpeichern.Enabled = True
        Catch ex As Exception
        End Try
    End Sub

    '' neue WK-Bezeichnungen zu Combos hinzufügen
    Private Sub cboAuswahl_LostFocus(sender As Object, e As EventArgs) Handles cboOrt.LostFocus, cboBezeichnung1.LostFocus, cboBezeichnung2.LostFocus, cboBezeichnung3.LostFocus
        With CType(sender, ComboBox)
            If .FindStringExact(.Text) = -1 Then .Items.Add(.Text)
        End With
    End Sub

    '' tabCollection
    Private PreviousTab As TabPage
    Private Sub tabCollection_Deselected(sender As Object, e As TabControlEventArgs) Handles tabCollection.Deselected
        PreviousTab = e.TabPage
    End Sub
    Private Sub tabCollection_Selected(sender As Object, e As TabControlEventArgs) Handles tabCollection.Selected
        If Not e.TabPage.Enabled Then
            tabCollection.SelectedTab = PreviousTab
            'ElseIf e.TabPage.Name.Equals("tabAthletik") Then
            '    dgvAthletik.Focus()
            'ElseIf e.TabPage.Name.Equals("tabAltersgruppen") Then
            '    dgvAKs.Focus()
        ElseIf e.TabPage.Name.Equals("tabMulti") Then
            Dim pos = bsMulti.Find("Wettkampf", WK_ID)
            If pos > -1 Then
                Dim posJ = bsMulti.Find("Joined", 1)
                With dgvMulti
                    .FirstDisplayedCell = dgvMulti(0, posJ)
                    .ClearSelection()
                    .Rows(pos).DefaultCellStyle.ForeColor = Color.MediumBlue
                    .Rows(pos).Cells("Multi").ReadOnly = True
                    If .SelectedRows.Count > 0 Then
                        Stop
                    End If
                End With
            End If
        End If
    End Sub

    '' tabMannschaft
    Private Sub chkMannschaft_CheckedChanged(sender As Object, e As EventArgs) Handles chkLiga.CheckedChanged, chkVerein.CheckedChanged, chkLand.CheckedChanged
        pnlVerein.Enabled = chkVerein.Checked
        pnlLand.Enabled = chkLand.Checked

        pnlLiga.Enabled = chkLiga.Checked
        chkFinale.Enabled = chkLiga.Checked
        pnlMannschaft.Enabled = chkLiga.Checked
        grpMannschaft.Enabled = chkLiga.Checked

        mnuSpeichern.Enabled = True
    End Sub

    '' tabGewichtsgruppen
    Private Sub chkEqual_m_CheckedChanged(sender As Object, e As EventArgs) Handles chkEqual_m.CheckedChanged
        'nudG_m.Enabled = Not chkEqual_m.Checked
        nudEqual_m.Enabled = chkEqual_m.Checked
        If nudEqual_m.Enabled Then
            For i = 0 To 4
                nudGG_m(i).Enabled = False
            Next
        Else
            For i = 0 To 4
                nudGG_m(i).Enabled = i < nudG_m.Value
            Next
        End If
        mnuSpeichern.Enabled = True
    End Sub
    Private Sub chkEqual_w_CheckedChanged(sender As Object, e As EventArgs) Handles chkEqual_w.CheckedChanged
        'nudG_w.Enabled = Not chkEqual_w.Checked
        nudEqual_w.Enabled = chkEqual_w.Checked
        If nudEqual_w.Enabled Then
            For i = 0 To 4
                nudGG_w(i).Enabled = False
            Next
        Else
            For i = 0 To 4
                nudGG_w(i).Enabled = i < nudG_w.Value
            Next
        End If
        mnuSpeichern.Enabled = True
    End Sub
    Private Sub nudGG_m_ValueChanged(sender As Object, e As EventArgs) Handles nudG_m0.ValueChanged, nudG_m1.ValueChanged, nudG_m2.ValueChanged, nudG_m3.ValueChanged, nudG_m4.ValueChanged
        Try
            With CType(sender, NumericUpDown)
                lblGG_m(CInt(.Increment) - 1).Text = .Increment & " x " & .Value / .Increment
            End With
            mnuSpeichern.Enabled = True
        Catch ex As Exception
        End Try
    End Sub
    Private Sub nudGG_w_ValueChanged(sender As Object, e As EventArgs) Handles nudG_w0.ValueChanged, nudG_w1.ValueChanged, nudG_w2.ValueChanged, nudG_w3.ValueChanged, nudG_w4.ValueChanged
        Try
            With CType(sender, NumericUpDown)
                lblGG_w(CInt(.Increment) - 1).Text = .Increment & " x " & .Value / .Increment
            End With
            mnuSpeichern.Enabled = True
        Catch ex As Exception
        End Try
    End Sub
    Private Sub nudG_m_ValueChanged(sender As Object, e As EventArgs) Handles nudG_m.ValueChanged
        Try
            If Not chkEqual_w.Checked Then
                For i = 0 To 4
                    nudGG_m(i).Enabled = i < nudG_m.Value
                Next
            End If
            mnuSpeichern.Enabled = True
        Catch ex As Exception
        End Try
    End Sub
    Private Sub nudG_w_ValueChanged(sender As Object, e As EventArgs) Handles nudG_w.ValueChanged
        Try
            If Not chkEqual_w.Checked Then
                For i = 0 To 4
                    nudGG_w(i).Enabled = i < nudG_w.Value
                Next
            End If
            mnuSpeichern.Enabled = True
        Catch ex As Exception
        End Try
    End Sub

    '' tabAusnahme
    Private Sub chkRestrict_R_CheckedChanged(sender As Object, e As EventArgs) Handles chkRestrict_R.CheckedChanged

        nudRestrict_R_AK.Enabled = chkRestrict_R.Checked
        nudRestrict_R_V.Enabled = chkRestrict_R.Checked
        GroupBox4.Enabled = chkRestrict_R.Checked OrElse chkRestrict_S.Checked

        If Not chkRestrict_R.Focused Then Return

        mnuSpeichern.Enabled = True
        Settings_Changed = True
    End Sub
    Private Sub chkRestrict_S_CheckedChanged(sender As Object, e As EventArgs) Handles chkRestrict_S.CheckedChanged

        nudRestrict_S_AK.Enabled = chkRestrict_S.Checked
        nudRestrict_S_V.Enabled = chkRestrict_S.Checked
        GroupBox4.Enabled = chkRestrict_R.Checked OrElse chkRestrict_S.Checked

        If Not chkRestrict_S.Focused Then Return

        mnuSpeichern.Enabled = True
        Settings_Changed = True
    End Sub
    Private Sub chkWeitererVersuch_Click(sender As Object, e As EventArgs) Handles chkWeitererVersuch.Click
        chkWeitererVersuch.Checked = Not chkWeitererVersuch.Checked
        mnuSpeichern.Enabled = True
    End Sub
    Private Sub chkVersucheSortieren_Click(sender As Object, e As EventArgs) Handles chkVersucheSortieren.Click
        chkVersucheSortieren.Checked = Not chkVersucheSortieren.Checked
        mnuSpeichern.Enabled = True
    End Sub
    Private Sub GroupBox4_EnabledChanged(sender As Object, e As EventArgs) Handles GroupBox4.EnabledChanged
        chkWeitererVersuch.Checked = GroupBox4.Enabled
        chkVersucheSortieren.Checked = GroupBox4.Enabled
    End Sub

    '' tabAthletik
    Private Sub dgvDefaults_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDefaults.CellEndEdit
        dvD(bsD.Position)!Wettkampf = WK_ID
        If dgvDefaults.Focused Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub dgvDefaults_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvDefaults.DataError
        With dgvDefaults
            .CurrentCell.Value = dvD(bsD.Position)(.Columns(.CurrentCell.ColumnIndex).Name)
        End With
        e.Cancel = False
    End Sub
    Private Sub dgvDefaults_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvDefaults.KeyDown
        'If dgvDefaults.CurrentCell.ColumnIndex > 0 AndAlso e.KeyCode.Equals(Keys.Delete) Then
        '    dgvDefaults.CurrentCell.Value = DBNull.Value
        '    dvD(bsD.Position)!Wettkampf = WK_ID
        '    mnuSpeichern.Enabled = True
        'End If
    End Sub

    Private Sub bsA_PositionChanged(sender As Object, e As EventArgs) Handles bsA.PositionChanged

        'Dim IsCheckboxColumn = dtDefaults.Select("Disziplin = " & dvA(bsA.Position)!idDisziplin.ToString &
        '                                         " and Bezeichnung = 'Teilnahme'").Length > 0
        Dim IsCheckboxColumn = (From x In dtDefaults
                                Where x.Field(Of Integer)("Disziplin") = CInt(dvA(bsA.Position)!idDisziplin) AndAlso
                                   x.Field(Of String)("Bezeichnung").Equals("Teilnahme")).Count > 0

        Dim Columns As New List(Of DataGridViewColumn)

        With dgvDefaults
            For Each col As DataGridViewColumn In .Columns
                If col.Name.Contains("ak") Then
                    If IsCheckboxColumn Then
                        Dim CheckCol As New DataGridViewCheckBoxColumn With {
                            .Name = col.Name,
                            .DataPropertyName = col.DataPropertyName,
                            .HeaderText = col.HeaderText,
                            .AutoSizeMode = col.AutoSizeMode,
                            .Width = col.Width,
                            .Resizable = col.Resizable,
                            .SortMode = col.SortMode,
                            .Visible = col.Visible}
                        Columns.Add(CheckCol)
                    Else
                        Dim TextCol As New DataGridViewTextBoxColumn With {
                            .Name = col.Name,
                            .DataPropertyName = col.DataPropertyName,
                            .HeaderText = col.HeaderText,
                            .AutoSizeMode = col.AutoSizeMode,
                            .Width = col.Width,
                            .Resizable = col.Resizable,
                            .SortMode = col.SortMode,
                            .Visible = col.Visible}
                        Columns.Add(TextCol)
                    End If
                End If
            Next
            For i = 1 To .Columns.Count - 1
                .Columns.RemoveAt(1)
            Next
            .Columns.AddRange(Columns.ToArray)
        End With

        bsD.Filter = "Disziplin = " & dvA(bsA.Position)!idDisziplin.ToString

        Dim Formatting = String.Empty

        If CInt(dvA(bsA.Position)!idDisziplin) = 20 Then
            Formatting = "0.00"
            lblBank.Text = "Bankdrücken (Faktor für 

)"
        ElseIf CInt(dvA(bsA.Position)!idDisziplin) = 130 Then
            Formatting = "0.00"
            lblBank.Text = "Zug liegend (Faktor für KG)"
        ElseIf CInt(dvA(bsA.Position)!idDisziplin) = 110 Then
            Formatting = "0"
            lblBank.Text = "Schockwurf (Kugelgewicht)"
        ElseIf CInt(dvA(bsA.Position)!idDisziplin) = 10 Then
            Formatting = "0"
            lblBank.Text = "Anristen (Anzahl)"
        ElseIf CInt(dvA(bsA.Position)!idDisziplin) = 60 Then
            Formatting = "0"
            lblBank.Text = "Klimmzug (Anzahl)"
        Else
            lblBank.Text = String.Empty
        End If

        With dgvDefaults
            .DefaultCellStyle.Format = Formatting
            .Enabled = Not lblBank.Text.Equals(String.Empty)
        End With

        btnDefault.Enabled = Not lblBank.Text.Equals(String.Empty)
    End Sub

    Private Sub dgvAthletik_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAthletik.CellClick
        If e.ColumnIndex = 0 Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub dgvAthletik_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAthletik.CellEndEdit
        If dgvAthletik.Focused AndAlso e.ColumnIndex = 2 Then
            If Not IsDBNull(dgvAthletik.CurrentCell.Value) Then
                Dim Entries = Split(dgvAthletik.CurrentCell.Value.ToString, ",").ToList
                For Each Entry In Entries
                    If Not IsNumeric(Entry) Then
                        dgvAthletik.CancelEdit()
                        Exit Sub
                    End If
                Next
            End If
            mnuSpeichern.Enabled = True
        End If
    End Sub
    Private Sub dgvAthletik_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvAthletik.KeyDown
        If dgvAthletik.CurrentCell.ColumnIndex = 2 AndAlso e.KeyCode = Keys.Delete Then
            dgvAthletik.CurrentCell.Value = DBNull.Value
            mnuSpeichern.Enabled = True
        End If
    End Sub

    '' Mindext-Alter ******************* von AK-Einteilung, AG-Einteilung und Wertung2 gemeinsam genutztes MinAge
    Private Sub Set_bsAK_Filter()
        If IsNothing(bsAK) OrElse IsNothing(bsSameAK) Then Return
        bsAK.Filter = "International = " & Math.Abs(CInt(chkInternational.Checked)) & " and maxAge >= " & nudMinAge.Value
        bsSameAK.Filter = "International = " & Math.Abs(CInt(chkInternational.Checked)) ' & " and maxAge >= " & nudMinAge.Value & " and maxAge < 120"
    End Sub
    Private Sub nudMinAge_ValueChanged(sender As Object, e As EventArgs) Handles nudMinAge.ValueChanged

        Set_bsAK_Filter()
        Set_bsGebühr_Filter()

        If IsNothing(dvModus) OrElse IsNothing(cboModus.SelectedValue) Then Return

        Dim ix = dvModus.Find(cboModus.SelectedValue)
        Dim minAge = CInt(dvModus(ix)!minAlter)
        Dim maxAge = CInt(dvModus(ix)!maxAlter)

        'bsAK.Filter = "idWertung < 6 and International = " & Math.Abs(CInt(chkInternational.Checked)) & " and maxAge >= " & nudMinAge.Value
        '        bsGebühr.Filter = "International = " & Math.Abs(CInt(chkInternational.Checked)) & " and maxAge >= " & nudMinAge.Value & " And idAltersklasse " & If(chkGebührNachAk.Checked, " > 0", " = 0")
        bsAKs.Filter = "Sex = 'm' AND (AK >= " & nudMinAge.Value & " AND AK <= " & maxAge & ")"

        Set_dgvDefaults_ColumnsVisible()

        If nudMinAge.Focused() Then mnuSpeichern.Enabled = True
    End Sub

    '' tabAltersgruppen
    Private Sub AKs_Set_CheckValue(Rows As DataRow(), Value As Boolean)
        For Each row In Rows
            Dim JGs = Split(row!Jahrgang.ToString, ",").ToList
            Dim x As Integer
            For Each item In JGs
                Try
                    x = bsAKs.Find("JG", item)
                    dvAKs(x)!Check = Value
                Catch ex As Exception
                End Try
            Next
        Next
        bsAKs.EndEdit()
        dgvAKs.ClearSelection()
    End Sub
    Private Sub btnAG_Add_Click(sender As Object, e As EventArgs) Handles btnAG_Add.Click
        Glob.dvAG.AddNew()
        bsAGs.Position = Glob.dvAG.Count - 1
        Glob.dvAG(bsAGs.Position)!Reihenfolge = Glob.dvAG.Count - 1
        bsAGs.EndEdit()
    End Sub
    Private Sub btnAG_Remove_Click(sender As Object, e As EventArgs) Handles btnAG_Remove.Click

        AKs_Set_CheckValue({Glob.dvAG(bsAGs.Position).Row}, False)

        Glob.dvAG.Delete(bsAGs.Position)
        For i = bsAGs.Position To bsAGs.Count - 1
            If i > 0 Then
                Glob.dvAG(i)!Reihenfolge = CInt(Glob.dvAG(i)!Reihenfolge) - 1
            ElseIf i = 0 Then
                Glob.dvAG(i)!Reihenfolge = 0
            End If
        Next
        bsAGs.EndEdit()
        mnuSpeichern.Enabled = True
    End Sub

    Private Sub optAG_CheckedChanged(sender As Object, e As EventArgs) Handles optAG_m.CheckedChanged, optAG_w.CheckedChanged
        If IsNothing(bsAKs) Then Return
        Dim sex As String
        With CType(sender, RadioButton)
            If Not .Checked Then Return
            sex = Split(.Name, "_")(1)
        End With
        bsAKs.Filter = "Sex = '" & sex & "' AND (AK >= " & nudMinAge.Minimum.ToString & " AND AK <= " & nudMinAge.Maximum.ToString & ")"
        AKs_Set_CheckValue(Glob.dtAG.Select("Sex = '" & sex & "'"), True)
    End Sub

    Private Sub dgvAKs_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAKs.CellContentClick

        If e.RowIndex > -1 AndAlso dgvAKs.Columns(e.ColumnIndex).Name = "Check" Then
            bsAKs.EndEdit()
            dgvAKs.EndEdit()
            bsAGs.EndEdit()

            Dim JGs As New List(Of String)

            If (IsDBNull(Glob.dvAG(bsAGs.Position)!Sex) OrElse String.IsNullOrEmpty(Glob.dvAG(bsAGs.Position)!Sex.ToString)) OrElse
                         Glob.dvAG(bsAGs.Position)!Sex.ToString.Equals(If(optAG_m.Checked, "m", "w")) Then

                If CBool(dvAKs(e.RowIndex)!Check) Then

                    Dim sex = Glob.dvAG(bsAGs.Position)!Sex.ToString
                    If String.IsNullOrEmpty(sex) Then sex = If(optAG_m.Checked, "m", "w")

                    If IsDBNull(Glob.dvAG(bsAGs.Position)!Sex) OrElse String.IsNullOrEmpty(Glob.dvAG(bsAGs.Position)!Sex.ToString) Then
                        ' Sex in rechter Tabelle ist leer
                        Glob.dvAG(bsAGs.Position)!Sex = sex
                        bsAGs.EndEdit()
                        dgvAGs.Refresh()
                    End If
                    JGs = Split(Glob.dvAG(bsAGs.Position)!Jahrgang.ToString, ",").ToList
                    JGs.Remove(String.Empty)
                    JGs.Add(dvAKs(e.RowIndex)!JG.ToString)
                Else
                    ' suche AK.Jahrgang in allen AG.Sex
                    Dim Ix = From j In Glob.dtAG
                             Where j.Field(Of String)("Sex").Equals(dvAKs(e.RowIndex)!Sex.ToString) AndAlso j.Field(Of String)("Jahrgang").Contains(dvAKs(e.RowIndex)!JG.ToString)
                             Select j.Field(Of Integer)("Reihenfolge")
                    Try
                        JGs = Split(Glob.dvAG(Ix(0))!Jahrgang.ToString, ",").ToList
                        JGs.Remove(dvAKs(e.RowIndex)!JG.ToString)
                    Catch ex As Exception
                        Return
                    End Try
                End If
            Else
                Using New Centered_MessageBox(Me)
                    If MessageBox.Show("Neue Altersgruppe [" & If(optAG_m.Checked, "männlich", "weiblich") & "] anlegen?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                        dvAKs(e.RowIndex)!Check = Not CBool(dvAKs(e.RowIndex)!Check)
                        Return
                    Else
                        btnAG_Add.PerformClick()
                        Glob.dvAG(bsAGs.Position)!Sex = If(optAG_m.Checked, "m", "w")
                        JGs = Split(Glob.dvAG(bsAGs.Position)!Jahrgang.ToString, ",").ToList
                        JGs.Remove(String.Empty)
                        JGs.Add(dvAKs(e.RowIndex)!JG.ToString)
                    End If
                End Using
            End If

            JGs.Sort()
            Glob.dvAG(bsAGs.Position)!Jahrgang = Join(JGs.ToArray, ",")
            bsAGs.EndEdit()

            mnuSpeichern.Enabled = True
        End If
    End Sub

    Private Sub dgvAGs_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAGs.RowEnter

        If Not dgvAGs.Focused Then Return

        If Glob.dvAG(e.RowIndex)!Sex.ToString.Equals("m") Then
            optAG_m.Checked = True
        Else
            optAG_w.Checked = True
        End If
    End Sub
    Private Sub dgvAGs_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles dgvAGs.RowsAdded
        btnAG_Remove.Enabled = dgvAGs.Rows.Count > 0
    End Sub
    Private Sub dgvAGs_RowsRemoved(sender As Object, e As DataGridViewRowsRemovedEventArgs) Handles dgvAGs.RowsRemoved
        btnAG_Remove.Enabled = dgvAGs.Rows.Count > 0
    End Sub
    Private Sub dgvAGs_SelectionChanged(sender As Object, e As EventArgs) Handles dgvAGs.SelectionChanged
        dgvAKs.Enabled = dgvAGs.SelectedRows.Count > 0
        optAG_m.Enabled = dgvAGs.SelectedRows.Count > 0
        optAG_w.Enabled = dgvAGs.SelectedRows.Count > 0
        btnAG_Remove.Enabled = dgvAGs.SelectedRows.Count > 0
    End Sub

    '' tabWertung
    Private Sub dgvWertung_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvWertung2.CellValueChanged ', dgvWertung.CellValueChanged
        Dim ctl = CType(sender, DataGridView)
        If ctl.Columns(e.ColumnIndex).Name.Contains("Wertung2") Then
            'Dim Score1 = DirectCast(cboModus.SelectedItem, DataRowView)!Wertung.ToString
            'Dim Score2 = ctl.CurrentCell.Value.ToString
        End If
        If ctl.Focused Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub dgvWertung_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvWertung2.DataError ', dgvWertung.DataError
        Dim ctl = CType(sender, DataGridView)
        With ctl
            Dim colName = .Columns(.CurrentCell.ColumnIndex).DataPropertyName
            .CurrentCell.Value = dvAK(bsAK.Position)(colName)
        End With
        e.Cancel = False
    End Sub
    'Private Sub dgvWertung_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvWertung.KeyDown
    '    If dgvWertung.CurrentCell.ColumnIndex > 1 AndAlso e.KeyCode.Equals(Keys.Delete) Then
    '        If chkAlleAksGleich.Checked Then
    '            Wertung_Clear()
    '        Else
    '            dgvWertung.CurrentCell.Value = DBNull.Value
    '        End If
    '        mnuSpeichern.Enabled = True
    '    End If
    'End Sub
    Private Sub dgvWertung2_EnabledChanged(sender As Object, e As EventArgs) Handles dgvWertung2.EnabledChanged
        With dgvWertung2
            .DefaultCellStyle.ForeColor = If(.Enabled, Color.FromKnownColor(KnownColor.ControlText), Color.FromKnownColor(KnownColor.GrayText))
            .ColumnHeadersDefaultCellStyle.ForeColor = If(.Enabled, Color.FromKnownColor(KnownColor.ControlText), Color.FromKnownColor(KnownColor.GrayText))
        End With
    End Sub
    Private Sub dgvWertung2_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvWertung2.KeyDown
        If dgvWertung2.CurrentCell.ColumnIndex > 0 AndAlso e.KeyCode.Equals(Keys.Delete) Then
            dgvWertung2.CurrentCell.Value = DBNull.Value
            mnuSpeichern.Enabled = True
        End If
    End Sub

    Private Sub chkPlatzierung2_CheckedChanged(sender As Object, e As EventArgs) Handles chkPlatzierung2.CheckedChanged
        grpPlatzierung2.Enabled = chkPlatzierung2.Checked
        grpWertung2.Enabled = chkPlatzierung2.Checked

        'chkAlleAksGleich.Enabled = chkPlatzierung2.Checked

        'Dim cols = {"Wertung_Wertung2", "Gruppierung"}
        'For Each col In cols
        '    With dgvWertung.Columns(col)
        '        .ReadOnly = Not chkPlatzierung2.Checked
        '        .DefaultCellStyle.ForeColor = If(.ReadOnly, Color.FromKnownColor(KnownColor.GrayText), Color.FromKnownColor(KnownColor.ControlText))
        '        '.HeaderCell.Style.ForeColor = If(.ReadOnly, Color.FromKnownColor(KnownColor.GrayText), Color.FromKnownColor(KnownColor.ControlText))
        '    End With
        'Next

        If chkPlatzierung2.Focused Then mnuSpeichern.Enabled = True
    End Sub
    'Private Sub chkchkAlleAksGleich_CheckedChanged(sender As Object, e As EventArgs) Handles chkAlleAksGleich.CheckedChanged
    '    If chkAlleAksGleich.Checked Then
    '        'For Each row As DataRowView In dvAK
    '        '    If IsNothing(cboWertung2.SelectedValue) Then
    '        '        row!idWertung2 = DBNull.Value
    '        '    Else
    '        '        row!idWertung2 = cboWertung2.SelectedValue
    '        '    End If
    '        'Next
    '        'bsAK.EndEdit()
    '    End If
    'End Sub

    Private Sub optPlatzierung_CheckedChanged(sender As Object, e As EventArgs) Handles optWettkampf.CheckedChanged, optGruppen.CheckedChanged, optAltersklassen.CheckedChanged, optGewichtsklassen.CheckedChanged
        Dim ctl = CType(sender, RadioButton)
        If ctl.Focused Then mnuSpeichern.Enabled = True

        chkSameAK.Enabled = Not ctl.Name.Equals(optAltersklassen)

        Dim ctl2 = Get_Checked_Radio(grpPlatzierung2)
        If IsNothing(ctl2) Then Return
        ctl2.Enabled = Not IsMehrkampf AndAlso Not IsEqualRating OrElse (Not ctl2.Name.Contains(ctl.Name) OrElse Not ctl.Checked)
    End Sub
    Private Sub optPlatzierung2_CheckedChanged(sender As Object, e As EventArgs) Handles optWeiblich2.CheckedChanged, optWettkampf2.CheckedChanged, optGruppen2.CheckedChanged, optAltersklassen2.CheckedChanged, optGewichtsklassen2.CheckedChanged

        Dim ctl2 = CType(sender, RadioButton)

        'tabWertung.Enabled = ctl2.Name.Equals("optAltersklassen2")

        If ctl2.Name.Equals("optAltersklassen2") Then
            'chkAlleAksGleich.Checked = Not optAltersklassen2.Checked AndAlso cboWertung2.SelectedIndex > -1
            If Not IsNothing(cboModus.SelectedItem) Then
                bsWertung.Filter = "idWertung < 6 and idWertung <> " & CType(cboModus.SelectedItem, DataRowView)!Wertung.ToString
                'Dim pos = bsWertung.Find("idWertung", Rating2)
                'cboWertung2.SelectedIndex = pos
                'Rating2 = pos
            End If
        Else
            'chkAlleAksGleich.Checked = cboWertung2.SelectedIndex > -1
            bsWertung.Filter = "idWertung < 6"
            'Dim pos = bsWertung.Find("idWertung", Rating2)
            'cboWertung2.SelectedIndex = pos
        End If

        grpWertung2.Visible = Not ctl2.Name.Equals("optAltersklassen2") OrElse Not optAltersklassen2.Checked

        dgvWertung2.Visible = ctl2.Name.Equals("optAltersklassen2") AndAlso optAltersklassen2.Checked

        'dgvWertung.Columns("Gruppierung").Visible = ctl2.Name.Equals("optAltersklassen2") AndAlso optAltersklassen2.Checked

        If ctl2.Focused Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub optPlatzierung2_EnabledChanged(sender As Object, e As EventArgs) Handles optWeiblich2.EnabledChanged, optWettkampf2.EnabledChanged, optGruppen2.EnabledChanged, optAltersklassen2.EnabledChanged, optGewichtsklassen2.EnabledChanged
        With CType(sender, RadioButton)
            If Not .Enabled Then .Checked = False
        End With
    End Sub

    Private Sub cboWertung2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboWertung2.SelectedIndexChanged
        If IsNothing(cboModus.SelectedItem) Then Return

        IsEqualRating = Not IsNothing(cboWertung2.SelectedValue) AndAlso CType(cboModus.SelectedItem, DataRowView)!Wertung.Equals(cboWertung2.SelectedValue)

        Dim ctl2 = Get_Checked_Radio(grpPlatzierung2)
        If Not IsNothing(ctl2) AndAlso Not ctl2.Name.Equals("optAltersklassen2") Then
            Dim ctl = Get_Checked_Radio(grpPlatzierung)
            ctl2.Enabled = Not IsMehrkampf AndAlso Not IsEqualRating OrElse (Not ctl2.Name.Contains(ctl.Name) OrElse Not ctl.Checked)
        ElseIf Not IsMehrkampf Then
            ctl2 = Get_Checked_Radio(grpPlatzierung2, True)
            If Not IsNothing(ctl2) Then ctl2.Enabled = True
        End If

        'If Not cboWertung2.Focused Then Return

        'If IsNothing(cboWertung2.SelectedItem) Then
        '    Rating2 = -1
        'Else
        '    Rating2 = CInt(CType(cboWertung2.SelectedItem, DataRowView)!idWertung)
        'End If
    End Sub

    Private Function Get_Checked_Radio(Group As GroupBox, Optional CheckDisabled As Boolean = False) As RadioButton
        For Each Ctl As Control In Group.Controls
            If TypeOf Ctl Is RadioButton Then
                Dim _ctl = CType(Ctl, RadioButton)
                If _ctl.Checked Then Return _ctl
                If CheckDisabled AndAlso Not _ctl.Enabled Then Return _ctl
            End If
        Next
        Return Nothing
    End Function
    Private Sub Wertung_Clear()
        For Each row As DataRowView In dvAK
            'row!idWertung = DBNull.Value
            row!idWertung2 = DBNull.Value
        Next
        bsAK.EndEdit()
    End Sub
    Private Sub Platzierung_Clear()
        For Each Ctl As Control In grpPlatzierung.Controls
            If TypeOf Ctl Is RadioButton Then
                Dim _ctl = CType(Ctl, RadioButton)
                _ctl.Enabled = True
                _ctl.Checked = False
            ElseIf TypeOf Ctl Is GroupBox Then
                Platzierung_Clear()
            End If
        Next
    End Sub

    '' Anzeige der DateTimePicker
    Private Sub Date_DropDown(sender As Object, e As EventArgs) Handles dpkDatum.DropDown, dpkDatumBis.DropDown, dpkMeldeschluss.DropDown, dpkMeldeschlussNachmeldung.DropDown
        CType(sender, DateTimePicker).Format = DateTimePickerFormat.Short
    End Sub
    Private Sub Date_KeyDown(sender As Object, e As KeyEventArgs) Handles dpkDatum.KeyDown, dpkDatumBis.KeyDown, dpkMeldeschluss.KeyDown, dpkMeldeschlussNachmeldung.KeyDown
        If e.KeyCode = Keys.Back Or e.KeyCode = Keys.Delete Then
            CType(sender, DateTimePicker).Format = DateTimePickerFormat.Custom
        End If
    End Sub
    Private Sub Date_ValueChanged(sender As Object, e As EventArgs) Handles dpkDatum.ValueChanged, dpkDatumBis.ValueChanged, dpkMeldeschluss.ValueChanged, dpkMeldeschlussNachmeldung.ValueChanged
        If loading Then Exit Sub
        With CType(sender, DateTimePicker)
            If .Text = "" Then
                .Format = DateTimePickerFormat.Custom
                .CustomFormat = " "
                .Value = Now
            ElseIf .Text <> " " Then
                .Format = DateTimePickerFormat.Short
            End If
        End With
        mnuSpeichern.Enabled = True
    End Sub

    '' Save
    Private Sub Speichern_Click(sender As Object, e As EventArgs) Handles mnuSpeichern.Click, btnSave.Click

        Dim msg As String = String.Empty
        Dim changes As DataTable
        Dim cmd As MySqlCommand
        Dim _focus = -1
        Dim dlgResult = DialogResult.OK
        Dim InsertQuery = String.Empty
        Dim UpdateQuery = String.Empty
        Dim DeleteQuery = String.Empty

#Region "Validating"
        Cursor = Cursors.WaitCursor
        loading = True
        Validate()
        bsAK.EndEdit()
        bsAKs.EndEdit()
        bsAGs.EndEdit()
        bsWK.EndEdit()
        bsBezeichnung.EndEdit()
        bsMeldefrist.EndEdit()
        bsDetails.EndEdit()
        bsA.EndEdit()
        bsD.EndEdit()
        bsGebühr.EndEdit()
        bsMulti.EndEdit()
        'dgvMulti.EndEdit()
#End Region

#Region "Prüfung Vollständigkeit"

        If cboModus.Text = "" Then
            msg = Space(5) + "- Modus"
            _focus = 0
        End If
        'If cboVeranstalter.Text = "" Then
        If IsNothing(txtVeranstalter.Tag) OrElse IsDBNull(txtVeranstalter.Tag) Then
            msg += If(String.IsNullOrEmpty(msg), "", vbNewLine) + Space(5) + "- Veranstalter"
            If Not _focus = -1 Then _focus = 1
        End If
        If dpkDatum.Text = " " Then
            msg += If(String.IsNullOrEmpty(msg), "", vbNewLine) + Space(5) + "- Datum"
            If Not _focus = -1 Then _focus = 2
        End If
        If cboOrt.Text = "" Then
            msg += If(String.IsNullOrEmpty(msg), "", vbNewLine) + Space(5) + "- Veranstaltungsort"
            If Not _focus = -1 Then _focus = 3
        End If
        If cboBezeichnung1.Text = "" Then
            msg += If(String.IsNullOrEmpty(msg), "", vbNewLine) + Space(5) + "- Bezeichnung"
            If Not _focus = -1 Then _focus = 4
        End If
        'If cboKR.Text = "" Then
        '    msg += If(String.IsNullOrEmpty(msg), "", vbNewLine) + Space(5) + "- Anzahl Kampfrichter"
        '    If Not _focus = -1 Then _focus = 5
        'End If

        If pnlMannschaft.Enabled Then
            'If Not (chkLand.Checked OrElse chkLiga.Checked OrElse chkVerein.Checked) Then
            '    msg += If(String.IsNullOrWhiteSpace(msg), "", vbNewLine) + Space(5) + "- Mannschafts-Modus"
            'End If
            'Dim i As Integer
            'Dim ii As Integer
            'For i = 0 To txtTeam.Count - 1
            '    If txtTeam(i).Text.Length > 0 Then
            '        For ii = i To txtTeam.Count - 1
            '            If txtTeam(ii).Text.Length > 0 Then Exit For
            '        Next
            '        If ii > 0 Then Exit For
            '    End If
            'Next
            'If i = txtTeam.Count OrElse ii = txtTeam.Count Then
            '    msg += If(String.IsNullOrEmpty(msg), "", vbNewLine) + Space(5) + "- teilnehmende Mannschaft(en)"
            '    If Not _focus = -1 Then _focus = 7
            'End If
        End If

        If chkLiga.Checked Then
            If String.IsNullOrEmpty(txtTeam(0).Text) Then
                If Not String.IsNullOrEmpty(txtVeranstalter.Text) Then
                    With txtTeam(0)
                        .Text = txtVeranstalter.Text
                        .Tag = txtVeranstalter.Tag
                    End With
                Else
                    msg += If(String.IsNullOrEmpty(msg), "", vbNewLine) + Space(5) + "- teilnehmende Mannschaft(en)"
                    If Not _focus = -1 Then _focus = 7
                End If
            End If
            If String.IsNullOrEmpty(txtTeam(1).Text) Then
                msg += If(String.IsNullOrEmpty(msg), "", vbNewLine) + Space(5) + "- teilnehmende Mannschaft(en)"
                If Not _focus = -1 Then _focus = 8
            End If
        End If

        ' RETURN if check failed
        If Not String.IsNullOrEmpty(msg) Then
            msg = "Folgende Angaben für den Wettkampf fehlen:" + vbNewLine + msg
            Using New Centered_MessageBox(Me, {"Zurück"})
                MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
            Select Case _focus
                Case 1
                    txtVeranstalter.Focus()
                Case 2
                    dpkDatum.Focus()
                Case 3
                    cboOrt.Focus()
                Case 4
                    cboBezeichnung1.Focus()
                Case 5
                    cboKR.Focus()
                Case 7
                    txt0.Focus()
                Case 8
                    txt1.Focus()
            End Select
            Cursor = Cursors.Default
            Return
        End If
#End Region

#Region "Prüfung Altersgruppen"
        For Each row As DataRowView In Glob.dvAG
            If IsDBNull(row!Jahrgang) OrElse String.IsNullOrEmpty(row!Jahrgang.ToString) Then
                row.Delete()
            End If
        Next
        bsAGs.EndEdit()
        Glob.dtAG.AcceptChanges()

        If tabCollection.TabPages.Contains(tabAltersgruppen) AndAlso
            CType(cboModus.SelectedItem, DataRowView)!Alterseinteilung.ToString = "Altersgruppen" AndAlso
            Glob.dvAG.Count = 0 Then

            Using New Centered_MessageBox(Me, {"OK", "Ändern"})
                If MessageBox.Show("Keine Altersgruppen-Einteilung gefunden." & vbNewLine & "Alle Altersklassen werden in einer Altersgruppe gewertet.",
                                   msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then
                    tabCollection.SelectedTab = tabAltersgruppen
                    Cursor = Cursors.Default
                    Return
                End If
            End Using

            ' alle AKs nach Sex getrennt in eine AG schreiben
            Dim sx = {"m", "w"}
            For i = 0 To 1
                Dim lst As New List(Of String)
                Dim rows = dtAKs.Select("Sex = '" & sx(i) & "' And AK >= " & nudMinAge.Minimum & " And AK <= " & nudMinAge.Maximum)
                For Each row As DataRow In rows
                    lst.Add(row!JG.ToString)
                Next
                lst.Sort()
                Glob.dtAG.Rows.Add(i, sx(i), Join(lst.ToArray, ","))
            Next
            For Each row As DataRowView In dvAKs
                row!Check = True
            Next

            'Dim jg = String.Empty
            'For i = dgvAKs.Rows.Count - 1 To 0 Step -1 'nudMinAge.Minimum To nudMinAge.Maximum
            '    jg += "," + dgvAKs.Rows(i).Cells("JG").Value.ToString  'CStr(Year(dpkDatum.Value) - i)
            'Next
            'jg = jg.Substring(1)
            'Glob.dtAG.Rows.Add(1, "m", jg)
            'Glob.dtAG.Rows.Add(2, "w", jg)
        End If
#End Region

        If chkSameAK.Checked AndAlso IsNothing(cboSameAK.SelectedValue) Then
            Using New Centered_MessageBox(Me, {"OK", "Ändern"})
                If MessageBox.Show("Keine Auswahl bei Wertung nach gleicher AK.",
                                   msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then
                    cboSameAK.Focus()
                    Cursor = Cursors.Default
                    Return
                End If
            End Using
        End If

#Region "Platzierung"
        Dim Scoring = String.Empty
        Dim SameAK As Object = DBNull.Value

        If tabCollection.Contains(tabPlatzierung) Then
            ' Platzierung:
            '   1 = Wettkampf
            '   2 = Gruppen
            '   3 = Altersklassen
            '   4 = Gewichtsklassen
            ' Platzierung 2:
            '   1 = Wettkampf
            '   2 = Gruppen
            '   4 = Gewichtsklassen
            '   5 = weiblich
            '   6 = Mehrkampf
            '   9 = Altersklassen

            For Each ctl As RadioButton In grpPlatzierung.Controls
                If ctl.Checked Then

                    ' 1.Stelle = Platzierung
                    Scoring = ctl.Tag.ToString

                    If chkPlatzierung2.Checked Then
                        For Each ctl2 As Control In grpPlatzierung2.Controls
                            If TypeOf ctl2 Is RadioButton AndAlso CType(ctl2, RadioButton).Checked Then

                                ' 2.Stelle = Platzierung2
                                Scoring = Scoring & ctl2.Tag.ToString

                                ' 2.Wertung (1.Wertung als Vorgabe einlesen)
                                Dim wertung = CInt(CType(cboModus.SelectedItem, DataRowView)("Wertung"))
                                If wertung = 6 Then
                                    ' bei Mehrkampfwertung und nicht ausgewählter 2.Wertung auf Relativ-Wertung setzen
                                    wertung = 2
                                ElseIf CInt(ctl2.Tag) = 3 Then
                                    ' Wertung nach Altersklassen (andere Wertung pro AK möglich)
                                    wertung = 9
                                ElseIf cboWertung2.SelectedIndex = -1 Then
                                    ' 2.Wertung wie 1.Wertung (nichts ausgewählt)
                                    Exit For
                                Else
                                    ' Wertung wie ausgewählt
                                    wertung = CInt(cboWertung2.SelectedValue)
                                End If

                                ' 3.Stelle = Wertung für 2.Platzierung
                                Scoring = Scoring & wertung

                                Exit For
                            End If
                        Next
                    End If
                    Exit For
                End If
            Next

            If chkSameAK.Checked AndAlso cboSameAK.SelectedIndex > -1 Then SameAK = cboSameAK.SelectedValue

        End If

        dtWettkampf(0)!SameAK = SameAK

        If String.IsNullOrEmpty(Scoring) Then Scoring = "0"
        dtWettkampf(0)!Platzierung = Scoring
#End Region

        Using conn As New MySqlConnection((User.ConnString))
            Do
                Try
                    If Not conn.State = ConnectionState.Open Then conn.Open()
#Region "Wettkampf"
                    changes = dtWettkampf.GetChanges()
                    If Not IsNothing(changes) Then
                        Query = "INSERT INTO wettkampf (Wettkampf, Modus, Datum, DatumBis, Veranstalter, Kurz, Ort, BigDisc, Kampfrichter, Mannschaft, Flagge, Mindestalter, Platzierung, GUID, Finale, SameAK, UseAgeGroups, IgnoreSex) " &
                                "VALUES (@wk, @mode, @date, @bis, @verein, @kurz, @ort, @bigdisc, @kr, @team, @flag, @age, @score, @guid, @final, @same, @useAG, @ignore) " &
                                "ON DUPLICATE KEY UPDATE Modus = @mode, Datum = @date, DatumBis = @bis, Veranstalter = @verein, Kurz = @kurz, Ort = @ort, BigDisc = @bigdisc, " &
                                "Kampfrichter = @kr, Mannschaft = @team, Flagge = @flag, Mindestalter = @age, Platzierung = @score, GUID = @guid, Finale = @final, SameAK = @same, UseAgeGroups = @useAG, IgnoreSex = @ignore;"
                        cmd = New MySqlCommand(Query, conn)

                        With cmd.Parameters
                            .AddWithValue("@wk", WK_ID)
                            .AddWithValue("@mode", cboModus.SelectedValue)
                            .AddWithValue("@date", changes(0)!Datum) 'dpkDatum.Value.ToShortDateString)
                            .AddWithValue("@bis", changes(0)!DatumBis) 'IIf(dpkDatumBis.Format.Equals(DateTimePickerFormat.Short), dpkDatumBis.Value.ToShortDateString, DBNull.Value))
                            .AddWithValue("@kurz", txtKurz.Text)
                            .AddWithValue("@verein", txtVeranstalter.Tag) 'cboVeranstalter.SelectedValue)
                            .AddWithValue("@ort", cboOrt.Text)
                            .AddWithValue("@bigdisc", chkBigdisc.Checked)
                            .AddWithValue("@kr", cboKR.Text)
                            .AddWithValue("@team", chkLiga.Checked) 'CInt(cboModus.SelectedValue) >= 600
                            .AddWithValue("@age", nudMinAge.Value)
                            .AddWithValue("@flag", chkFlagge.Checked)
                            .AddWithValue("@guid", changes(0)!GUID)
                            .AddWithValue("@score", Scoring)
                            .AddWithValue("@final", chkFinale.Checked)
                            .AddWithValue("@same", SameAK)
                            .AddWithValue("@useAG", chkUseAgeGroups.Checked)
                            .AddWithValue("@ignore", chkIgnoreSex.Checked)
                        End With
                        cmd.ExecuteNonQuery()
                        dtWettkampf.AcceptChanges()
                    End If
#End Region

#Region "WK-Bezeichnungen"
                    changes = dtBezeichnung.GetChanges()
                    If Not IsNothing(changes) Then
                        Query = "INSERT INTO wettkampf_bezeichnung (Wettkampf, Bez1, Bez2, Bez3) VALUES (@wk, @b1, @b2, @b3) " &
                                "ON DUPLICATE KEY UPDATE Bez1 = @b1, Bez2 = @b2, Bez3 = @b3;"
                        cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", WK_ID)
                            '.AddWithValue("@b1", changes(0)!Bez1)
                            '.AddWithValue("@b2", changes(0)!Bez2)
                            '.AddWithValue("@b3", changes(0)!Bez3)
                            .AddWithValue("@b1", cboBezeichnung1.Text)
                            .AddWithValue("@b2", IIf(String.IsNullOrEmpty(cboBezeichnung2.Text), DBNull.Value, cboBezeichnung2.Text))
                            .AddWithValue("@b3", IIf(String.IsNullOrEmpty(cboBezeichnung3.Text), DBNull.Value, cboBezeichnung3.Text))
                        End With
                        cmd.ExecuteNonQuery()
                        dtBezeichnung.AcceptChanges()
                    End If
#End Region

#Region "Meldefristen und Mannschaft-Startgeld (Mannschaft hat keine AKs, daher in Meldefrist)"
                    changes = dtMeldefrist.GetChanges()
                    If Not IsNothing(changes) Then
                        Query = "INSERT INTO wettkampf_meldefrist (Wettkampf, Datum1, Datum2, Verein, land, Liga) VALUES (@wk, @date1, @date2, @verein, @land, @liga) " &
                                "ON DUPLICATE KEY UPDATE Datum1 = @date1, Datum2 = @date2, Verein = @verein, Land = @land, Liga = @liga;"
                        cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", WK_ID)
                            .AddWithValue("@date1", changes(0)!Datum1)
                            .AddWithValue("@date2", changes(0)!Datum2)
                            .AddWithValue("@verein", changes(0)!Verein)
                            .AddWithValue("@land", changes(0)!Land)
                            .AddWithValue("@liga", changes(0)!Liga)
                        End With
                        cmd.ExecuteNonQuery()
                        dtMeldefrist.AcceptChanges()
                    End If
#End Region

#Region "Mannschaftsstärke"
                    DeleteQuery = "DELETE FROM wettkampf_mannschaft WHERE Wettkampf = @wk AND idMannschaft = @id;"
                    InsertQuery = "INSERT INTO wettkampf_mannschaft (Wettkampf, idMannschaft, min, max, gewertet, m_w, multi, relativ) VALUES(@wk, @id, @min, @max, @gewertet, @m_w, @multi, @relativ) " &
                                  "ON DUPLICATE KEY UPDATE min = @min, max = @max, gewertet =  @gewertet, m_w = @m_w, multi = @multi, relativ = @relativ;"
                    For i = 0 To 2 ' Verein, Länder, Liga
                        cmd = New MySqlCommand(InsertQuery, conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", WK_ID)
                            .AddWithValue("@id", i)
                            If CType(dicMannschaft(i)(0), CheckBox).Checked Then
                                .AddWithValue("@min", CType(dicMannschaft(i)(1), NumericUpDown).Value)
                                .AddWithValue("@max", CType(dicMannschaft(i)(2), NumericUpDown).Value)
                                .AddWithValue("@gewertet", CType(dicMannschaft(i)(3), NumericUpDown).Value)
                                .AddWithValue("@m_w", CType(dicMannschaft(i)(4), CheckBox).Checked)
                                .AddWithValue("@multi", CType(dicMannschaft(i)(5), CheckBox).Checked)
                                Try
                                    If CType(dicMannschaft(i)(6), NumericUpDown).Enabled = True Then
                                        .AddWithValue("@relativ", CType(dicMannschaft(i)(6), NumericUpDown).Value)
                                    Else
                                        .AddWithValue("@relativ", 0)
                                    End If
                                Catch ex As Exception
                                    .AddWithValue("@relativ", 0)
                                End Try
                            Else
                                cmd.CommandText = DeleteQuery
                            End If
                        End With
                        cmd.ExecuteNonQuery()
                    Next
#End Region

                    If tabCollection.Contains(tabAthletik) Then
#Region "Athletik"
                        DeleteQuery = "DELETE FROM wettkampf_athletik WHERE Wettkampf = @wk AND Disziplin = @dis;"
                        InsertQuery = "INSERT INTO wettkampf_athletik (Wettkampf, Disziplin, AKs) VALUES (@wk, @dis, @aks) ON DUPLICATE KEY UPDATE AKs = @aks;"

                        changes = dtAthletik.GetChanges
                        If Not IsNothing(changes) Then
                            For Each row As DataRow In changes.Rows
                                If IsDBNull(row!Checked) OrElse Not CBool(row!Checked) Then
                                    cmd = New MySqlCommand(DeleteQuery, conn)
                                Else
                                    cmd = New MySqlCommand(InsertQuery, conn)
                                End If
                                With cmd.Parameters
                                    .AddWithValue("@wk", WK_ID)
                                    .AddWithValue("@dis", row!idDisziplin)
                                    .AddWithValue("@aks", row!AKs)
                                End With
                                cmd.ExecuteNonQuery()
                            Next
                            dtAthletik.AcceptChanges()
                        End If
#End Region

#Region "Athletik_Defaults"
                        DeleteQuery = "DELETE FROM wettkampf_athletik_defaults WHERE Wettkampf = @wk AND Disziplin = @dis;"
                        InsertQuery = "INSERT INTO wettkampf_athletik_defaults (Wettkampf, Disziplin, Bezeichnung, Geschlecht, AK5, AK6, AK7, AK8, AK9, AK10, AK11, AK12, AK13, AK14, AK15, AK16, AK17) " &
                                      "VALUES (@wk, @dis, @bez, @sex, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16, @p17) " &
                                      "ON DUPLICATE KEY UPDATE AK5 = @p5, AK6 = @p6, AK7 = @p7, AK8 = @p8, AK9 = @p9, AK10 = @p10, AK11 = @p11, AK12 = @p12, AK13 = @p13, AK14 = @p14, AK15 = @p15, AK16 = @p16, AK17 = @p17;"

                        changes = dtDefaults.GetChanges()
                        If Not IsNothing(changes) Then
                            For Each row As DataRow In changes.Rows
                                If CInt(row!Wettkampf) = CInt(WK_ID) Then
                                    cmd = New MySqlCommand(InsertQuery, conn)
                                Else
                                    cmd = New MySqlCommand(DeleteQuery, conn)
                                End If
                                With cmd.Parameters
                                    .AddWithValue("@wk", WK_ID)
                                    .AddWithValue("@dis", row!Disziplin)
                                    .AddWithValue("@bez", row!Bezeichnung)
                                    .AddWithValue("@sex", row!Geschlecht)
                                    For i = 5 To 17
                                        .AddWithValue("@p" & i, row("AK" & i))
                                    Next
                                End With
                                cmd.ExecuteNonQuery()
                            Next
                            dtDefaults.AcceptChanges()
                        End If
#End Region
                    End If

#Region "Gewichtsgruppen"
                    If tabCollection.Contains(tabGewichtsgruppen) Then
                        Query = "DELETE FROM wettkampf_gewichtsgruppen WHERE Wettkampf = @wk;"
                        cmd = New MySqlCommand(Query, conn)
                        cmd.Parameters.AddWithValue("@wk", WK_ID)
                        cmd.ExecuteNonQuery()
                        Query = "INSERT INTO wettkampf_gewichtsgruppen (Wettkampf, Geschlecht, Gruppe, Heber) VALUES(@wk, @sex, @group, @lifter);"
                        If chkEqual_m.Checked Then
                            For i = 1 To nudG_m.Value
                                cmd = New MySqlCommand(Query, conn)
                                With cmd.Parameters
                                    .AddWithValue("@wk", WK_ID)
                                    .AddWithValue("@sex", "m")
                                    .AddWithValue("@group", i)
                                    .AddWithValue("@lifter", nudEqual_m.Value * i)
                                End With
                                cmd.ExecuteNonQuery()
                            Next
                        Else
                            For i = 0 To nudGG_m.Count - 1
                                If nudGG_m(i).Enabled Then
                                    cmd = New MySqlCommand(Query, conn)
                                    With cmd.Parameters
                                        .AddWithValue("@wk", WK_ID)
                                        .AddWithValue("@sex", "m")
                                        .AddWithValue("@group", i + 1)
                                        .AddWithValue("@lifter", nudGG_m(i).Value)
                                    End With
                                    cmd.ExecuteNonQuery()
                                End If
                            Next
                        End If
                        If chkEqual_w.Checked Then
                            For i = 1 To nudG_w.Value
                                cmd = New MySqlCommand(Query, conn)
                                With cmd.Parameters
                                    .AddWithValue("@wk", WK_ID)
                                    .AddWithValue("@sex", "w")
                                    .AddWithValue("@group", i)
                                    .AddWithValue("@lifter", nudEqual_w.Value * i)
                                End With
                                cmd.ExecuteNonQuery()
                            Next
                        Else
                            For i = 0 To nudGG_w.Count - 1
                                If nudGG_w(i).Enabled Then
                                    cmd = New MySqlCommand(Query, conn)
                                    With cmd.Parameters
                                        .AddWithValue("@wk", WK_ID)
                                        .AddWithValue("@sex", "w")
                                        .AddWithValue("@group", i + 1)
                                        .AddWithValue("@lifter", nudGG_w(i).Value)
                                    End With
                                    cmd.ExecuteNonQuery()
                                End If
                            Next
                        End If
                    End If
#End Region

#Region "Wertung"
                    DeleteQuery = "DELETE FROM wettkampf_wertung WHERE Wettkampf = @wk;"
                    InsertQuery = "INSERT INTO wettkampf_wertung (Wettkampf, idAK, Wertung, Wertung2, Gruppierung) 
                                   VALUES (@wk, @ak, @w1, @w2, @gr) 
                                   ON DUPLICATE KEY UPDATE Wertung = @w1, Wertung2 = @w2, Gruppierung = @gr;"

                    If tabCollection.Contains(tabPlatzierung) Then
                        ' alle Einträge des WKs löschen
                        cmd = New MySqlCommand(DeleteQuery, conn)
                        cmd.Parameters.AddWithValue("@wk", WK_ID)
                        cmd.ExecuteNonQuery()

                        If Scoring.ToString.Length = 3 AndAlso Scoring.ToString.Substring(2, 1).Equals("9") Then
                            ' Änderungen speichern
                            For Each row As DataRowView In dvAK  ' so werden alle AKs unter MinAge ausgeschlossen
                                If Not IsDBNull(row!Gruppierung) OrElse
                                      (Not IsDBNull(row!idWertung) AndAlso Not CInt(row!idWertung) = CInt(CType(cboModus.SelectedItem, DataRowView)!Wertung)) OrElse
                                      (Not IsDBNull(row!idWertung2) AndAlso Not CInt(row!idWertung2) = CInt(CType(cboModus.SelectedItem, DataRowView)!Wertung)) Then

                                    Dim AKs = Split(row!AK.ToString, ",").ToList
                                    AKs.Remove(String.Empty)
                                    For Each AK As String In AKs
                                        cmd = New MySqlCommand(InsertQuery, conn)
                                        With cmd.Parameters
                                            .AddWithValue("@wk", WK_ID)
                                            .AddWithValue("@ak", AK)
                                            .AddWithValue("@w1", row!idWertung)
                                            .AddWithValue("@w2", row!idWertung2)
                                            .AddWithValue("@gr", row!Gruppierung)
                                        End With
                                        cmd.ExecuteNonQuery()
                                    Next
                                End If
                            Next
                        End If
                        dtAK(Math.Abs(CInt(chkInternational.Checked))).AcceptChanges()
                    End If
#End Region

#Region "Altersgruppen"
                    If tabCollection.Contains(tabAltersgruppen) Then
                        DeleteQuery = "DELETE FROM wettkampf_altersgruppen WHERE Wettkampf = @wk;"
                        InsertQuery = "INSERT INTO wettkampf_altersgruppen (Wettkampf, Reihenfolge, Sex, Jahrgang) 
                                       VALUES(@wk, @sort, @sex, @jg);"
                        cmd = New MySqlCommand(DeleteQuery, conn)
                        cmd.Parameters.AddWithValue("@wk", WK_ID)
                        cmd.ExecuteNonQuery()
                        For Each row As DataRowView In Glob.dvAG
                            cmd = New MySqlCommand(InsertQuery, conn)
                            With cmd.Parameters
                                .AddWithValue("@wk", WK_ID)
                                .AddWithValue("@sort", row!Reihenfolge)
                                .AddWithValue("@sex", row!Sex)
                                .AddWithValue("@jg", row!Jahrgang)
                            End With
                            cmd.ExecuteNonQuery()
                        Next
                    End If
                    Glob.dtAG.AcceptChanges()
#End Region

#Region "Restrictions"
                    Glob.dtRestrict.Clear()
                    DeleteQuery = "DELETE FROM wettkampf_restriction WHERE Wettkampf = @wk;"
                    InsertQuery = "INSERT INTO wettkampf_restriction (Wettkampf, Durchgang, Altersklasse, Versuche) 
                                   VALUES(@wk, @dg, @ak, @v);"
                    cmd = New MySqlCommand(DeleteQuery, conn)
                    cmd.Parameters.AddWithValue("@wk", WK_ID)
                    cmd.ExecuteNonQuery()
                    If chkRestrict_R.Checked Then
                        cmd = New MySqlCommand(InsertQuery, conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", WK_ID)
                            .AddWithValue("@dg", 0)
                            .AddWithValue("@ak", nudRestrict_R_AK.Value)
                            .AddWithValue("@v", nudRestrict_R_V.Value)
                        End With
                        cmd.ExecuteNonQuery()
                        Glob.dtRestrict.Rows.Add({0, nudRestrict_R_AK.Value, nudRestrict_R_V.Value})
                    End If
                    If chkRestrict_S.Checked Then
                        cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", WK_ID)
                            .AddWithValue("@dg", 1)
                            .AddWithValue("@ak", nudRestrict_S_AK.Value)
                            .AddWithValue("@v", nudRestrict_S_V.Value)
                        End With
                        cmd.ExecuteNonQuery()
                        Glob.dtRestrict.Rows.Add({1, nudRestrict_S_AK.Value, nudRestrict_S_V.Value})
                    End If
                    Glob.dtRestrict.AcceptChanges()
#End Region

#Region "WK-Details"
                    changes = dtDetails.GetChanges()
                    If Not IsNothing(changes) Then
                        'If changes(0).RowState = DataRowState.Added Then
                        '    Query = "INSERT INTO wettkampf_details (Wettkampf, ZK_m, ZK_w, R_m, S_m, R_w, S_w, Hantelstange, Steigerung1, Steigerung2, AKs_m, AKs_w, " &
                        '            "Regel20, NurZK, International, AutoLosnummer, ThirdAttempt, OrderByLifter) " &
                        '            "VALUES (@wk, @h1, @h2, @h3, @h4, @h5, @h6, @stange, @s1, @s2, @a1, @a2, @20, @zk, @int, @lot, @third, @order);"
                        'ElseIf changes(0).RowState = DataRowState.Modified Then
                        '    Query = "UPDATE wettkampf_details SET ZK_m = @h1, ZK_w = @h2, R_m = @h3, S_m = @h4, R_w = @h5, S_w = @h6, Hantelstange = @stange, " &
                        '            "Steigerung1 = @s1, Steigerung2 = @s2, AKs_m = @a1, AKs_w = @a2, Regel20 = @20, NurZK = @zk, International = @int, AutoLosnummer = @lot, " &
                        '            "ThirdAttempt = @third, OrderByLifter = @order WHERE Wettkampf = @wk;"
                        'End If
                        Query = "INSERT INTO wettkampf_details (Wettkampf, ZK_m, ZK_w, R_m, S_m, R_w, S_w, Hantelstange, Steigerung1, Steigerung2, " &
                                "Regel20, NurZK, International, AutoLosnummer, ThirdAttempt, OrderByLifter, Faktor_m, Faktor_w) " &
                                "VALUES (@wk, @h1, @h2, @h3, @h4, @h5, @h6, @stange, @s1, @s2, @20, @zk, @int, @lot, @third, @order, @f_m, @f_w) " &
                                "ON DUPLICATE KEY UPDATE ZK_m = @h1, ZK_w = @h2, R_m = @h3, S_m = @h4, R_w = @h5, S_w = @h6, Hantelstange = @stange, " &
                                "Steigerung1 = @s1, Steigerung2 = @s2, Regel20 = @20, NurZK = @zk, International = @int, AutoLosnummer = @lot, " &
                                "ThirdAttempt = @third, OrderByLifter = @order, Faktor_m = @f_m, Faktor_w = @f_w;"
                        cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", WK_ID)
                            .AddWithValue("@h1", changes(0)!ZK_m) ' txtZK_m.Text)
                            .AddWithValue("@h2", changes(0)!ZK_w) 'txtZK_w.Text)
                            .AddWithValue("@h3", changes(0)!R_m) ' txtR_m.Text)
                            .AddWithValue("@h4", changes(0)!S_m) ' txtS_m.Text)
                            .AddWithValue("@h5", changes(0)!R_w) ' txtR_w.Text)
                            .AddWithValue("@h6", changes(0)!S_w) 'txtS_w.Text)
                            .AddWithValue("@stange", changes(0)!Hantelstange) 'txtHantelstange.Text)
                            .AddWithValue("@s1", changes(0)!Steigerung1) 'nudSteigerung1.Value)
                            .AddWithValue("@s2", changes(0)!Steigerung2) 'nudSteigerung2.Value)
                            '.AddWithValue("@a1", AKs("m"))
                            '.AddWithValue("@a2", AKs("w"))
                            .AddWithValue("@20", changes(0)!Regel20) 'chkRegel20.Checked)
                            .AddWithValue("@zk", False) 'changes(0)!nurZK) 'chkNurZK.Checked)
                            .AddWithValue("@int", changes(0)!International) 'chkInternational.Checked)
                            .AddWithValue("@lot", changes(0)!AutoLosnummer)
                            .AddWithValue("@third", changes(0)!ThirdAttempt)
                            .AddWithValue("@order", changes(0)!OrderByLifter)
                            .AddWithValue("@f_m", changes(0)!Faktor_m)
                            .AddWithValue("@f_w", changes(0)!Faktor_w)
                        End With
                        cmd.ExecuteNonQuery()
                        dtDetails.AcceptChanges()
                    End If
#End Region

#Region "Meldegebühren"
                    Query = "INSERT INTO wettkampf_startgeld (Wettkampf, AK, Gebuehr, NM_Gebuehr, Gebuehr_oL, NM_Gebuehr_oL) VALUES (@wk, @ak, @fee, @fee_nm, @fee_oL, @fee_oL_nm) " &
                            "ON DUPLICATE KEY UPDATE Gebuehr = @fee, NM_Gebuehr = @fee_nm, Gebuehr_oL = @fee_oL, NM_Gebuehr_oL = @fee_oL_nm;"
                    cmd = New MySqlCommand("delete from wettkampf_startgeld where Wettkampf = @wk;", conn)
                    cmd.Parameters.AddWithValue("@wk", WK_ID)
                    cmd.ExecuteNonQuery()
                    For Each row As DataRowView In dvGebühr
                        cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", WK_ID)
                            .AddWithValue("@ak", row!idAltersklasse)
                            .AddWithValue("@fee", row!Gebuehr)
                            .AddWithValue("@fee_nm", row!NM_Gebuehr)
                            .AddWithValue("@fee_oL", row!Gebuehr_oL)
                            .AddWithValue("@fee_oL_nm", row!NM_Gebuehr_oL)
                            'End If
                        End With
                        cmd.ExecuteNonQuery()
                    Next
                    If chkGebührNachAk.Checked Then
                        cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", WK_ID)
                            .AddWithValue("@ak", 0)
                            .AddWithValue("@fee", DBNull.Value)
                            .AddWithValue("@fee_nm", DBNull.Value)
                            .AddWithValue("@fee_oL", DBNull.Value)
                            .AddWithValue("@fee_oL_nm", DBNull.Value)
                        End With
                        cmd.ExecuteNonQuery()
                    End If
                    dtGebühr(Math.Abs(CInt(chkInternational.Checked))).AcceptChanges()
#End Region

#Region "DiscColor"
                    changes = dtDisc.GetChanges
                    If Not IsNothing(changes) Then
                        Query = "UPDATE hantel SET Farbe = @f, Dicke = @d, available = @a WHERE ID = @i;"
                        For Each row As DataRow In changes.Rows
                            cmd = New MySqlCommand(Query, conn)
                            With cmd.Parameters
                                .AddWithValue("@i", row!ID)
                                .AddWithValue("@f", row!Farbe)
                                .AddWithValue("@d", row!Dicke)
                                .AddWithValue("@a", row!available)
                            End With
                            cmd.ExecuteNonQuery()
                        Next
                        dtDisc.AcceptChanges()
                        Color_Disc()
                        Hantel.Change_HantelDefaults()
                    End If
#End Region

#Region "Teams (z.B. Liga)"
                    If pnlMannschaft.Enabled Then
                        InsertQuery = "INSERT INTO wettkampf_teams (Wettkampf, Id, Team) VALUES(@wk, @id, @team) ON DUPLICATE KEY UPDATE Id = @id;" &
                                      "INSERT INTO gruppen (Wettkampf, Gruppe, Datum, DatumW) VALUES(@wk, @grp, @d, @d) ON DUPLICATE KEY UPDATE Datum = @d, DatumW = @d;"
                        DeleteQuery = "DELETE FROM wettkampf_teams WHERE Wettkampf = @wk AND Id = @id;" &
                                      "DELETE FROM gruppen WHERE Wettkampf = @wk AND Gruppe = @id;"
                        Dim id = 0
                        For Each item In txtTeam
                            If Not String.IsNullOrEmpty(item.Text) Then
                                cmd = New MySqlCommand(InsertQuery, conn)
                                With cmd.Parameters
                                    .AddWithValue("@wk", WK_ID)
                                    .AddWithValue("@id", id)
                                    .AddWithValue("@team", item.Tag)
                                    .AddWithValue("@d", dpkDatum.Value)
                                    .AddWithValue("@grp", id + 1)
                                End With
                                cmd.ExecuteNonQuery()
                                id += 1
                            End If
                        Next
                        Dim RowsToDelete As DataRow() = Glob.dtTeams.Select("Id >= " & id)
                        For Each row As DataRow In RowsToDelete
                            dlgResult = Clear_Tables(conn, row) ' ClearTables ohne Abfrage --> dlgResult fällt weg (stattdessen Success)
                            If dlgResult = DialogResult.OK Then
                                cmd = New MySqlCommand(DeleteQuery, conn)
                                With cmd.Parameters
                                    .AddWithValue("@wk", WK_ID)
                                    .AddWithValue("@id", row!Id)
                                End With
                                cmd.ExecuteNonQuery()
                            End If
                        Next
                    End If
#End Region

#Region "Multi-WK"
                    changes = dtMulti.GetChanges
                    If Not IsNothing(changes) Then
                        Dim Insert = dtMulti.Select("Joined = 1")
                        Dim Remove = dtMulti.Select(If(Insert.Count > 1, "Joined = 0 And ", "") & "Multi Is not Null")


                        'DeleteQuery = "delete from wettkampf_multi where Wettkampf = @wk;"
                        'InsertQuery = "insert into wettkampf_multi (Wettkampf, Multi, Primaer) values (@wk, @multi, @prim) 
                        '           on duplicate key update Multi = @multi, Primaer = @prim;"
                        'Dim Idents = (From m In dvMulti.ToTable
                        '              Where m.Field(Of Integer)("Joined") > 0
                        '              Select Key = m.Field(Of Integer)("Wettkampf").ToString).ToList
                        'Idents.Sort()

                        'If Insert.Count > 1 Then
                        '    For Each row As DataRow In Insert
                        '        cmd = New MySqlCommand(InsertQuery, conn)
                        '        With cmd.Parameters
                        '            .AddWithValue("@wk", row!Wettkampf)
                        '            .AddWithValue("@multi", Join(Idents.ToArray, ","))
                        '            .AddWithValue("@prim", row!Primaer)
                        '        End With
                        '        cmd.ExecuteNonQuery()
                        '    Next
                        'End If

                        'For Each row As DataRow In Remove
                        '    cmd = New MySqlCommand(DeleteQuery, conn)
                        '    With cmd.Parameters
                        '        .AddWithValue("@wk", row!Wettkampf)
                        '    End With
                        '    cmd.ExecuteNonQuery()
                        'Next
                        Using ds As New DataService
                            ds.Save_Wettkampf_Multi(Insert, Remove, dvMulti.ToTable, WK_ID, conn)
                            If Insert.Count > 1 Then
                                ds.EqualizeMulti(Me, dvMulti.ToTable, WK_ID, conn)
                            End If
                        End Using
                    End If
#End Region
                    ConnError = False
                Catch ex As Exception
                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                End Try
            Loop While ConnError
        End Using

        'txtStartgeld.Text = Format(txtStartgeld.Text, "Fixed")
        'txtStartgeld_Nachmeldung.Text = Format(txtStartgeld_Nachmeldung.Text, "Fixed")
        txtStartgeld_M_Land.Text = Format(txtStartgeld_M_Land.Text, "Fixed")
        txtStartgeld_M_Liga.Text = Format(txtStartgeld_M_Liga.Text, "Fixed")
        txtStartgeld_M_Verein.Text = Format(txtStartgeld_M_Verein.Text, "Fixed")

        If Not ConnError Then
            mnuSpeichern.Enabled = False
            ' Wettkampf-Properties aktualisieren 
            If Wettkampf.Set_Properties(Me, WK_ID.ToString) > 0 Then
                If Not IsNothing(formLeader) AndAlso Not formLeader.IsDisposed Then
                    If Settings_Changed Then
                        ' Tabellen für Wk einlesen
                        Glob.Load_Wettkampf_Tables(Me, Wettkampf.Identities.Keys.ToArray)
                    End If
                    Change_LeaderGruppe()  ' löst formLeader.Gruppe_Load aus, wenn Steigerung/Restriction geändert wurde
                End If
            End If

            Using OS As New OnlineService
                If Not AppConnState = ConnState.Connected Then
                    AppConnState = OS.Check_Wettkampf()
                End If
                If AppConnState = ConnState.Connected Then
                    OS.UpdateWettkampf()
                    OS.UpdateWertung()
                End If
            End Using
        End If

        loading = False
        Cursor = Cursors.Default
        formMain.Change_MainProgressBarValue(0)
        If Not IsNothing(formLeader) AndAlso Not formLeader.IsDisposed Then Close() ' CallFromLeader könnte als Property von Leader an Wettkampf übergeben werden; könnte auch als Returnwert für Change_LeaderGruppe genutzt werden

    End Sub
    Private Function Clear_Tables(conn As MySqlConnection, row As DataRow) As DialogResult
        Dim dlgResult = DialogResult.OK

        ' Athleten des gelöschten/geänderten Teams suchen
        Dim dtA As New DataTable
        Dim msg = String.Empty
        Dim q = "select Teilnehmer from meldung " &
                "left join athleten on idTeilnehmer = Teilnehmer " &
                "where Wettkampf = " & WK_ID & " and MSR = " & row("Team", DataRowVersion.Original).ToString & ";"
        dtA.Load(New MySqlCommand(q, conn).ExecuteReader())

        If dtA.Rows.Count = 0 Then Return dlgResult

        ' Verein der zu löschenden Athleten
        Dim _verein = dtVerein.Select("idVerein = " & row("Team", DataRowVersion.Original).ToString)

        ' Lösch-Message erstellen
        If row.RowState = DataRowState.Deleted Then
            msg = "Das Team von " & Chr(34) & _verein(0)!Vereinsname.ToString & Chr(34) & " wird "
            If dtA.Rows.Count > 0 Then msg += "mit allen gemeldeten Athleten "
        ElseIf dtA.Rows.Count > 0 Then
            msg = "Alle gemeldeten Athleten von " & Chr(34) & _verein(0)!Vereinsname.ToString & Chr(34) & " werden "
        End If
        If Not String.IsNullOrEmpty(msg) Then
            msg += "aus dem Wettkampf entfernt." & vbNewLine & "(Mannschaft geändert/gelöscht)"
            msg += vbNewLine + vbNewLine + "Abbrechen beendet die Bearbeitung."
        End If

        If Not String.IsNullOrEmpty(msg) Then
            Using New Centered_MessageBox(Me)
                dlgResult = MessageBox.Show(msg, msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation)
            End Using
        End If

        If dlgResult = DialogResult.Yes Then
            Query = "DELETE FROM reissen WHERE Wettkampf = @wk AND Teilnehmer = @tn;" &
                    "DELETE FROM stossen WHERE Wettkampf = @wk AND Teilnehmer = @tn;" &
                    "DELETE FROM results WHERE Wettkampf = @wk AND Teilnehmer = @tn;" &
                    "DELETE FROM meldung WHERE Wettkampf = @wk AND Teilnehmer = @tn;"
            For a = 0 To dtA.Rows.Count - 1
                Dim a_cmd = New MySqlCommand(Query, conn)
                With a_cmd.Parameters
                    .AddWithValue("@wk", WK_ID)
                    .AddWithValue("@tn", dtA.Rows(a)("idTeilnehmer"))
                End With
                cmd.ExecuteNonQuery()
            Next
        End If

        Return dlgResult
    End Function

    '' Menu
    Private Sub mnuSpeichern_EnabledChanged(sender As Object, e As EventArgs) Handles mnuSpeichern.EnabledChanged
        btnSave.Enabled = mnuSpeichern.Enabled
        If mnuSpeichern.Enabled Then
            btnExit.Text = "Schließen"
        End If
    End Sub
    Private Sub mnuBeenden_Click(sender As Object, e As EventArgs) Handles mnuBeenden.Click, btnExit.Click
        If btnExit.Text = "Schließen" Then btnExit.DialogResult = DialogResult.None
        Close()
    End Sub
    Private Sub mnuMauszeiger_Click(sender As Object, e As EventArgs) Handles mnuMauszeiger.Click
        Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
        Cursor.Show()
    End Sub

    '' Enable_Save
    Private Sub TextBox_Changed(sender As Object, e As EventArgs) Handles txtStartgeld_M_Land.TextChanged, txtStartgeld_M_Verein.TextChanged, txtStartgeld_M_Liga.TextChanged,
        txtZK_m.TextChanged, txtZK_w.TextChanged, txtS_m.TextChanged, txtS_w.TextChanged, txtR_m.TextChanged, txtR_w.TextChanged, txtKurz.TextChanged

        If CType(sender, TextBox).Focused Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub NumericUpDwon_Changed(sender As Object, e As EventArgs) Handles nudSteigerung1.ValueChanged, nudSteigerung2.ValueChanged,
                                                                                nudFaktor_m.TextChanged, nudFaktor_w.TextChanged,
                                                                                nudVmax.ValueChanged, nudVmin.ValueChanged, nudVrel.ValueChanged, nudVwert.ValueChanged,
                                                                                nudLmax.ValueChanged, nudLmin.ValueChanged, nudLwert.ValueChanged,
                                                                                nudMmax.ValueChanged, nudMmin.ValueChanged, nudMwert.ValueChanged,
                                                                                nudRestrict_R_AK.ValueChanged, nudRestrict_S_AK.ValueChanged,
                                                                                nudRestrict_R_V.ValueChanged, nudRestrict_S_V.ValueChanged
        If CType(sender, NumericUpDown).Focused Then
            Settings_Changed = True
            mnuSpeichern.Enabled = True
        End If
    End Sub
    Private Sub CheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles chkFlagge.CheckedChanged,
                                                                                  chkRegel20.CheckedChanged,
                                                                                  chkLosnummer.CheckedChanged,
                                                                                  chkFinale.CheckedChanged,
                                                                                  chkUseAgeGroups.CheckedChanged,
                                                                                  chkSameAK.CheckedChanged,
                                                                                  chkIgnoreSex.CheckedChanged
        If CType(sender, CheckBox).Focused Then
            'Settings_Changed = True
            mnuSpeichern.Enabled = True
        End If
    End Sub
    Private Sub ComboBox_TextChanged(sender As Object, e As EventArgs) Handles cboOrt.TextChanged,
                                                                               cboBezeichnung1.TextChanged, cboBezeichnung2.TextChanged, cboBezeichnung3.TextChanged
        If CType(sender, ComboBox).Focused Then mnuSpeichern.Enabled = True
    End Sub

    Private Sub chkBigdisc_CheckedChanged(sender As Object, e As EventArgs) Handles chkBigdisc.CheckedChanged
        btnDiscColor.Enabled = chkBigdisc.Checked
        If chkBigdisc.Focused Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub ComboBox_Changed(sender As Object, e As EventArgs) Handles cboKR.SelectedIndexChanged, cboSameAK.SelectedIndexChanged
        If CType(sender, ComboBox).Focused Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub txtStartgeld_Validating(sender As Object, e As CancelEventArgs) Handles txtStartgeld_M_Land.Validating, txtStartgeld_M_Liga.Validating, txtStartgeld_M_Verein.Validating
        With CType(sender, TextBox)
            If .Text.Length = 0 Then dtMeldefrist(0)(.Tag.ToString) = DBNull.Value
        End With
    End Sub

    ' MouseWheel ausschalten
    Private Sub ComboBox_MouseWheel(sender As Object, e As MouseEventArgs) Handles cboBezeichnung1.MouseWheel, cboBezeichnung2.MouseWheel, cboBezeichnung3.MouseWheel, cboKR.MouseWheel, cboModus.MouseWheel, cboOrt.MouseWheel
        If Not CType(sender, ComboBox).DroppedDown Then
            Dim HMEA As HandledMouseEventArgs = DirectCast(e, HandledMouseEventArgs)
            HMEA.Handled = True
        End If
    End Sub
    Private Sub NumericUpDown_MouseWheel(sender As Object, e As MouseEventArgs) Handles nudEqual_m.MouseWheel, nudEqual_w.MouseWheel,
            nudG_m.MouseWheel, nudG_m0.MouseWheel, nudG_m1.MouseWheel, nudG_m2.MouseWheel, nudG_m3.MouseWheel, nudG_m4.MouseWheel, nudG_w.MouseWheel,
            nudG_w0.MouseWheel, nudG_w1.MouseWheel, nudG_w2.MouseWheel, nudG_w3.MouseWheel, nudG_w4.MouseWheel, nudLmax.MouseWheel, nudLmin.MouseWheel, nudLwert.MouseWheel, nudMmax.MouseWheel, nudMmin.MouseWheel, nudMwert.MouseWheel, nudSteigerung1.MouseWheel, nudSteigerung2.MouseWheel,
            nudVmax.MouseWheel, nudVmin.MouseWheel, nudVrel.MouseWheel, nudVwert.MouseWheel, nudRestrict_S_AK.MouseWheel, nudRestrict_S_V.MouseWheel

        Dim ctl = CType(sender, NumericUpDown)
        Dim HMEA As HandledMouseEventArgs = DirectCast(e, HandledMouseEventArgs)
        If e.X < 0 OrElse e.X > ctl.Width AndAlso e.Y < 0 OrElse e.Y > ctl.Height Then HMEA.Handled = True
    End Sub
    Private Sub DataGridView_MouseWheel(sender As Object, e As MouseEventArgs) Handles dgvAthletik.MouseWheel
        Dim HMEA As HandledMouseEventArgs = DirectCast(e, HandledMouseEventArgs)
        Dim ctl = CType(sender, DataGridView)
        If e.X < 0 OrElse e.X > ctl.Width AndAlso e.Y < 0 OrElse e.Y > ctl.Height Then HMEA.Handled = True
    End Sub

    Private Sub chkInternational_CheckedChanged(sender As Object, e As EventArgs) Handles chkInternational.CheckedChanged

        Set_bsAK_Filter()
        Set_bsGebühr_Filter()

        If chkInternational.Focused Then mnuSpeichern.Enabled = True
    End Sub

    ' Startgeld nach AK
    Private Sub chkGebührNachAk_CheckedChanged(sender As Object, e As EventArgs) Handles chkGebührNachAk.CheckedChanged
        'If IsNothing(bsGebühr) Then Return

        'Dim filter = Split(LCase(bsGebühr.Filter), " and ").ToList
        'filter.Remove(String.Empty)

        'Dim ak_part = filter.AsEnumerable().Where(Function(f) f.Contains("idaltersklasse"))
        'If ak_part.Count = 1 Then filter.Remove(ak_part(0))

        'filter.Add("idAltersklasse " & If(chkGebührNachAk.Checked, " > 0", " = 0"))
        'bsGebühr.Filter = Join(filter.ToArray, " and ")

        Set_bsGebühr_Filter(True)

        If chkGebührNachAk.Focused Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub dgvGebühr_CellValidating(sender As Object, e As DataGridViewCellValidatingEventArgs) Handles dgvGebühr.CellValidating
        If e.ColumnIndex > 0 Then
            If String.IsNullOrEmpty(e.FormattedValue.ToString) Then
                dvGebühr(e.RowIndex)(dgvGebühr.Columns(e.ColumnIndex).DataPropertyName) = DBNull.Value
                bsGebühr.EndEdit()
            End If
            'Dim oldValue = dvGebühr(e.RowIndex)(e.ColumnIndex + 1)
            'mnuSpeichern.Enabled = (Not IsDBNull(oldValue) And Not e.FormattedValue.Equals(String.Empty)) AndAlso CType(oldValue, Double) <> CType(e.FormattedValue, Double) OrElse IsDBNull(oldValue) And Not e.FormattedValue.Equals(String.Empty) OrElse Not IsDBNull(oldValue) And e.FormattedValue.Equals(String.Empty)
            mnuSpeichern.Enabled = True
        End If
    End Sub
    Private Sub dgvGebühr_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvGebühr.DataError
        With dgvGebühr
            .CurrentCell.Value = dvGebühr(bsGebühr.Position)(.Columns(.CurrentCell.ColumnIndex).Name)
        End With
        e.Cancel = False
    End Sub

    Private Sub dgvGebühr_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvGebühr.KeyDown
        'If Not dgvGebühr.Columns(dgvGebühr.CurrentCell.ColumnIndex).Name.Equals("Altersklasse") AndAlso
        '    e.KeyCode.Equals(Keys.Delete) Then
        '    dgvGebühr.CurrentCell.Value = DBNull.Value
        '    mnuSpeichern.Enabled = True
        'End If
    End Sub

#Region "BigDisc Colors"
    Private btnState As Integer = ButtonState.Normal
    Private cellState As Integer = ButtonState.Normal
    Private Sub btnDiscColor_Click(sender As Object, e As EventArgs) Handles btnDiscColor.Click
        If btnState = ButtonState.Pressed Then
            btnState = ButtonState.Hot
            dgvBigDisc.Visible = False
        ElseIf Not dgvBigDisc.Visible Then
            If btnState = ButtonState.Normal Then
                btnState = ButtonState.Hot
            Else
                btnState = ButtonState.Pressed
                dgvBigDisc.Visible = True
                dgvBigDisc.Focus()
            End If
        End If
    End Sub
    Private Sub btnDiscColor_EnabledChanged(sender As Object, e As EventArgs) Handles btnDiscColor.EnabledChanged
        If Not btnDiscColor.Enabled Then
            lstColors.Visible = False
            dgvBigDisc.Visible = False
            chkBigdisc.Focus()
        End If
    End Sub
    Private Sub btnDiscColor_MouseEnter(sender As Object, e As EventArgs) Handles btnDiscColor.MouseEnter
        If Not btnState = ButtonState.Pressed Then btnState = ButtonState.Hot
    End Sub
    Private Sub btnDiscColor_MouseLeave(sender As Object, e As EventArgs) Handles btnDiscColor.MouseLeave
        If Not btnState = ButtonState.Pressed Then btnState = ButtonState.Normal
    End Sub

    Private Sub dgvBigDisc_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBigDisc.CellClick
        If e.RowIndex > -1 AndAlso dgvBigDisc.Columns(e.ColumnIndex).Name.Equals("Farbe") Then
            If cellState = ButtonState.Pressed Then
                cellState = ButtonState.Hot
                lstColors.Visible = False
            ElseIf Not lstColors.Visible Then
                If cellState = ButtonState.Normal Then
                    cellState = ButtonState.Hot
                Else
                    cellState = ButtonState.Pressed
                    Dim loc As Point
                    With dgvBigDisc
                        Dim rect = .GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, True)
                        loc = .PointToScreen(New Point(rect.X, rect.Y + rect.Height))
                        lstColors.Location = .FindForm.PointToClient(loc)
                        With lstColors
                            .SelectedItem = dvDisc(e.RowIndex)!Farbe
                            .Visible = True
                            .BringToFront()
                            .Focus()
                        End With
                    End With
                End If
            End If
        End If
    End Sub
    Private Sub dgvBigDisc_CellMouseEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBigDisc.CellMouseEnter
        If e.RowIndex > -1 AndAlso dgvBigDisc.Columns(e.ColumnIndex).Name.Equals("Farbe") Then
            If Not cellState = ButtonState.Pressed Then cellState = ButtonState.Hot
        End If
    End Sub
    Private Sub dgvBigDisc_CellMouseLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBigDisc.CellMouseLeave
        If e.RowIndex > -1 AndAlso dgvBigDisc.Columns(e.ColumnIndex).Name.Equals("Farbe") Then
            If Not cellState = ButtonState.Pressed Then cellState = ButtonState.Normal
        End If
    End Sub
    Private Sub dgvBigDisc_CellValidating(sender As Object, e As DataGridViewCellValidatingEventArgs) Handles dgvBigDisc.CellValidating
        If dgvBigDisc.IsCurrentRowDirty Then
            mnuSpeichern.Enabled = True
            dgvBigDisc.EndEdit()
        End If
    End Sub
    Private Sub dgvBigDisc_LostFocus(sender As Object, e As EventArgs) Handles dgvBigDisc.LostFocus
        If Not lstColors.Visible Then
            dgvBigDisc.Visible = False
        End If
    End Sub
    Private Sub dgvBigDisc_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvBigDisc.KeyDown
        If e.KeyCode = Keys.Escape Then
            chkBigdisc.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub dgvBigDisc_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBigDisc.RowEnter
        If Not IsNothing(dgvBigDisc.CurrentRow) Then
            dgvBigDisc("Farbe", dgvBigDisc.CurrentRow.Index).Style.SelectionBackColor = Color.FromName(dgvBigDisc("Farbe", dgvBigDisc.CurrentRow.Index).Value.ToString)
        End If
    End Sub
    Private Sub dgvBigDisc_VisibleChanged(sender As Object, e As EventArgs) Handles dgvBigDisc.VisibleChanged
        If IsNothing(ActiveControl) Then Return
        If Not ActiveControl.Name.Equals("btnDiscColor") Then
            btnState = ButtonState.Normal
        End If
    End Sub

    Private Sub lstColors_Click(sender As Object, e As EventArgs) Handles lstColors.Click
        dvDisc(dgvBigDisc.CurrentRow.Index)!Farbe = lstColors.SelectedItem
        dgvBigDisc.CurrentCell.Style.BackColor = Color.FromName(lstColors.SelectedItem.ToString)
        mnuSpeichern.Enabled = True
        dgvBigDisc.Focus()
    End Sub
    Private Sub lstColors_DrawItem(sender As Object, e As DrawItemEventArgs) Handles lstColors.DrawItem
        Dim g As Graphics = e.Graphics
        Dim rect As Rectangle = e.Bounds
        If e.Index >= 0 Then
            Dim n = lstColors.Items(e.Index).ToString()
            Dim f = lstColors.Font
            Dim c = Color.FromName(n)
            Dim b As Brush = New SolidBrush(c)
            g.FillRectangle(b, rect.X + 1, rect.Y + 1, 15, rect.Height - 2)
            g.DrawString(n, f, Brushes.Black, rect.X + 17, rect.Top + 2)
        End If
    End Sub
    Private Sub lstColors_KeyDown(sender As Object, e As KeyEventArgs) Handles lstColors.KeyDown
        If e.KeyCode = Keys.Escape Then
            dgvBigDisc.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub lstColors_LostFocus(sender As Object, e As EventArgs) Handles lstColors.LostFocus
        lstColors.Visible = False
        dgvBigDisc.Focus()
    End Sub
    Private Sub lstColors_VisibleChanged(sender As Object, e As EventArgs) Handles lstColors.VisibleChanged
        If IsNothing(ActiveControl) Then Return
        If Not ActiveControl.Name.Equals("dgvBigDisc") Then
            cellState = ButtonState.Normal
        End If
    End Sub
#End Region

#Region "ComboBox Vereine"
    Private ButtonStyle As Integer = ButtonState.Normal
    Private Sub txtVereine_TextChanged(sender As Object, e As EventArgs) Handles txtVeranstalter.TextChanged, txt0.TextChanged, txt1.TextChanged, txt2.TextChanged
        Dim ctl = CType(sender, TextBox)
        If IsNothing(dvVerein) OrElse Not ctl.Focused() Then Return
        bsVerein.Filter = "Vereinsname LIKE '*" + ctl.Text + "*'"

        If TeamIndex = -1 Then
            ButtonStyle = ButtonState.Pressed
            pnlVereine.Refresh()
        ElseIf TeamIndex > -1 Then
            pnlTeam(TeamIndex).Style = ButtonState.Pressed
            pnlTeam(TeamIndex).Button.Refresh()
        End If

        ListVereine(ctl, True)
    End Sub
    Private Sub txtVereine_GotFocus(sender As Object, e As EventArgs) Handles txtVeranstalter.GotFocus, txt0.GotFocus, txt1.GotFocus, txt2.GotFocus
        Dim ctl = CType(sender, TextBox)
        ctl.SelectAll()
        TeamIndex = -1
        If Regex.IsMatch(ctl.Name, "[0-9]") Then
            TeamIndex = CInt(ctl.Name.Substring(3))
        End If
    End Sub
    Private Sub txtVereine_KeyDown(sender As Object, e As KeyEventArgs) Handles txtVeranstalter.KeyDown, txt0.KeyDown, txt1.KeyDown, txt2.KeyDown
        If e.KeyCode.Equals(Keys.Escape) Then
            e.SuppressKeyPress = True
            If bsVerein.Count = 0 Then Return
            Dim ctl = CType(sender, TextBox)
            ctl.Text = dvVerein(bsVerein.Find("idVerein", ctl.Tag))!Vereinsname.ToString
            lstVereine.Visible = False
            ctl.SelectAll()
        End If
    End Sub

    Private Sub lstVereine_DoubleClick(sender As Object, e As EventArgs) Handles lstVereine.DoubleClick
        If bsVerein.Count = 0 Then Return
        Dim ctl As TextBox
        If TeamIndex = -1 Then
            ctl = txtVeranstalter
        Else
            ctl = txtTeam(TeamIndex)
        End If
        With ctl
            .Tag = lstVereine.SelectedValue
            .Text = DirectCast(lstVereine.SelectedItem, DataRowView)("Vereinsname").ToString
            If TeamIndex = 0 AndAlso CInt(txtVeranstalter.Tag) <> CInt(.Tag) Then
                With txtVeranstalter
                    .Tag = lstVereine.SelectedValue
                    .Text = DirectCast(lstVereine.SelectedItem, DataRowView)("Vereinsname").ToString
                End With
            End If
        End With
        mnuSpeichern.Enabled = True
        lstVereine.Visible = False
        ctl.Focus()
    End Sub
    Private Sub lstVereine_LostFocus(sender As Object, e As EventArgs) Handles lstVereine.LostFocus
        Dim txt As TextBox
        Dim pnl As Panel
        If TeamIndex = -1 Then
            txt = txtVeranstalter
            pnl = pnlVereine
            ButtonStyle = ButtonState.Normal
        Else
            txt = txtTeam(TeamIndex)
            pnl = pnlTeam(TeamIndex).Button
            pnlTeam(TeamIndex).Style = ButtonState.Normal
        End If
        lstVereine.Visible = False
        pnl.Refresh()
        txt.Focus()
    End Sub
    Private Sub lstVereine_KeyDown(sender As Object, e As KeyEventArgs) Handles lstVereine.KeyDown
        Dim ctl As TextBox
        If TeamIndex = -1 Then
            ctl = txtVeranstalter
        Else
            ctl = txtTeam(TeamIndex)
        End If
        Select Case e.KeyCode
            Case Keys.Escape, Keys.Enter
                e.SuppressKeyPress = True
                If e.KeyCode.Equals(Keys.Enter) Then
                    With ctl
                        .Tag = lstVereine.SelectedValue
                        .Text = DirectCast(lstVereine.SelectedItem, DataRowView)("Vereinsname").ToString
                        If TeamIndex = 0 AndAlso CInt(txtVeranstalter.Tag) <> CInt(.Tag) Then
                            With txtVeranstalter
                                .Tag = lstVereine.SelectedValue
                                .Text = DirectCast(lstVereine.SelectedItem, DataRowView)("Vereinsname").ToString
                            End With
                        End If
                    End With
                    mnuSpeichern.Enabled = True
                End If
                lstVereine.Visible = False
                ctl.Focus()
        End Select
    End Sub
    Private Sub lstVereine_VisibleChanged(sender As Object, e As EventArgs) Handles lstVereine.VisibleChanged

        If Not IsNothing(bsVerein) Then bsVerein.Filter = String.Empty

        If TeamIndex > -1 Then pnlMannschaft.AutoScroll = Not lstVereine.Visible

        If Not lstVereine.Visible Then
            Dim ctl As Panel
            If TeamIndex = -1 Then
                ctl = pnlVereine
                ButtonStyle = ButtonState.Normal
            Else
                ctl = pnlTeam(TeamIndex).Button
                pnlTeam(TeamIndex).Style = ButtonState.Normal
            End If
            ctl.Refresh()
        End If
    End Sub

    Private Sub pnlVereine_Paint(sender As Object, e As PaintEventArgs) Handles pnlVereine.Paint, pnl0.Paint, pnl1.Paint, pnl2.Paint
        Dim ctl = CType(sender, Panel)
        Dim style = GetVisualStyle(ctl)
        If IsNothing(style.Value) Then Return
        Dim rect As New Rectangle(0, 0, ctl.Width, ctl.Height)
        Dim renderer As VisualStyles.VisualStyleRenderer = Nothing
        Select Case style.Value
            Case ButtonState.Disabled
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Disabled)
            Case ButtonState.Normal
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Normal)
            Case ButtonState.Hot
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Hot)
            Case ButtonState.Pressed
                renderer = New VisualStyles.VisualStyleRenderer(VisualStyles.VisualStyleElement.ComboBox.DropDownButton.Pressed)
        End Select
        renderer.DrawBackground(e.Graphics, rect)
    End Sub
    Private Sub pnlVereine_MouseEnter(sender As Object, e As EventArgs) Handles pnlVereine.MouseEnter, pnl0.MouseEnter, pnl1.MouseEnter, pnl2.MouseEnter
        Dim ctl = CType(sender, Panel)
        Dim style = GetVisualStyle(ctl)
        If style.Value = ButtonState.Pressed Then Return
        SetVisualStyle(style.Key, ButtonState.Hot)
        ctl.Refresh()
    End Sub
    Private Sub pnlVereine_MouseLeave(sender As Object, e As EventArgs) Handles pnlVereine.MouseLeave, pnl0.MouseLeave, pnl1.MouseLeave, pnl2.MouseLeave
        Dim ctl = CType(sender, Panel)
        Dim style = GetVisualStyle(ctl)
        If style.Value = ButtonState.Pressed Then Return
        SetVisualStyle(style.Key, ButtonState.Normal)
        ctl.Refresh()
    End Sub
    Private Sub pnlVereine_MouseClick(sender As Object, e As MouseEventArgs) Handles pnlVereine.MouseClick, pnl0.MouseClick, pnl1.MouseClick, pnl2.MouseClick
        TeamIndex = -1
        If sender.Equals(pnlVereine) Then
            If ButtonStyle = ButtonState.Pressed Then
                ButtonStyle = ButtonState.Hot
                lstVereine.Visible = False
            Else
                ButtonStyle = ButtonState.Pressed
                ListVereine(txtVeranstalter)
            End If
        Else
            TeamIndex = CInt(CType(sender, Panel).Name.Substring(3))
            If pnlTeam(TeamIndex).Style = ButtonState.Pressed Then
                pnlTeam(TeamIndex).Style = ButtonState.Hot
                lstVereine.Visible = False
            Else
                pnlTeam(TeamIndex).Style = ButtonState.Pressed
                ListVereine(txtTeam(TeamIndex))
            End If
        End If
        CType(sender, Panel).Refresh()
    End Sub

    Private Sub ListVereine(ParentControl As TextBox, Optional HideSelection As Boolean = False)

        Dim loc As Point
        Dim height As Integer

        Try
            If TeamIndex = -1 Then
                loc = PointToScreen(New Point(ParentControl.Left, ParentControl.Top + ParentControl.Height - 1))
                height = ClientSize.Height - loc.Y
            Else
                loc = PointToScreen(New Point(grpMannschaft.Left + pnlMannschaft.Left + ParentControl.Left + 1, grpMannschaft.Top + pnlMannschaft.Top + ParentControl.Top + ParentControl.Height))
                height = ClientSize.Height - PointToClient(loc).Y
            End If

            With lstVereine
                .Height = height
                .Location = PointToClient(loc)
                'If .Height \ .ItemHeight < 7 AndAlso bsVerein.Count > 6 Then
                '    .Top = .Top - (.Height + ParentControl.Height)
                '    .Height = .ItemHeight * 8
                'End If
                .Width = ParentControl.Width
                .Visible = True
                .BringToFront()
                'If Not HideSelection Then
                If Not IsNothing(ParentControl.Tag) AndAlso Not ParentControl.Tag.ToString.Equals("0") Then
                    .SelectedItem = dvVerein(bsVerein.Find("idVerein", ParentControl.Tag))
                End If
                If Not HideSelection Then .Focus()
                'End If
            End With
        Catch ex As Exception
            LogMessage("frmWettkampf: ListVereine: " & ex.Message, ex.StackTrace)
        End Try
    End Sub

    Private Function GetVisualStyle(Ctl As Panel) As KeyValuePair(Of Integer, Integer)
        If pnlTeam.Count = 0 Then Return Nothing
        Dim style = ButtonStyle
        Dim index = -1
        If Regex.IsMatch(Ctl.Name, "[0-9]") Then
            index = CInt(Ctl.Name.Substring(3))
            style = pnlTeam(index).Style
        End If
        Return New KeyValuePair(Of Integer, Integer)(index, style)
    End Function
    Private Sub SetVisualStyle(Index As Integer, Style As Integer)
        If Index = -1 Then
            ButtonStyle = Style
        Else
            pnlTeam(Index).Style = Style
        End If
    End Sub

#End Region

#Region "WK-Verknüpfung"
    Private Sub dgvMulti_CellMouseUp(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvMulti.CellMouseUp

        If e.Button.Equals(MouseButtons.Left) AndAlso e.RowIndex > -1 AndAlso e.ColumnIndex > -1 AndAlso e.ColumnIndex < 2 Then

            Dim check0 As DataGridViewCheckBoxCell = CType(dgvMulti(0, e.RowIndex), DataGridViewCheckBoxCell)
            Dim check1 As DataGridViewCheckBoxCell = CType(dgvMulti(1, e.RowIndex), DataGridViewCheckBoxCell)

            If e.ColumnIndex = 0 Then
                ' Multi-WK hinzufügen
                If check0.EditingCellValueChanged AndAlso Not CBool(check0.EditingCellFormattedValue) Then
                    dvMulti(e.RowIndex)!Primaer = False
                    dgvMulti.Refresh()
                ElseIf Not dgvMulti.CurrentCell.ReadOnly Then
                    Using ds As New DataService
                        Dim response = ds.ValidateEquality(Me, dvMulti(e.RowIndex)!Wettkampf.ToString, dvMulti.ToTable, Me)
                        If Not response.Key Then
                            If IsNothing(response.Value) Then
                                Return ' MySqlError
                            Else
                                Dim msg = "Folgende Eigenschaften des hinzugefügten Wettkampfes stimmmen nicht mit der bestehenden Auswahl überein (angepasst wird der hinzugefügte WK):" & vbNewLine & vbNewLine
                                For Each table In response.Value
                                    If table.Value.Count > 0 Then
                                        msg += "- " & table.Key & ": " & Join(table.Value.ToArray, ", ") & vbNewLine
                                    End If
                                Next
                                Using New Centered_MessageBox(Me, {"Anpassen"})
                                    If MessageBox.Show(msg, msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) = DialogResult.Cancel Then
                                        dgvMulti.CancelEdit()
                                    End If
                                End Using
                            End If
                        End If
                    End Using
                End If

                check1.ReadOnly = CInt(dvMulti(e.RowIndex)!Joined) = 0 'Not CBool(check0.EditingCellFormattedValue)

            ElseIf e.ColumnIndex = 1 AndAlso Not check1.ReadOnly Then
                ' nur 1 Primär möglich
                Dim pos = bsMulti.Find("Primaer", True)
                If pos > -1 AndAlso pos <> e.RowIndex Then
                    Using New Centered_MessageBox(Me)
                        If MessageBox.Show(dvMulti(e.RowIndex)!Bez1.ToString & " zum Hauptwettkampf machen?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                            dvMulti(pos)!Primaer = False
                            dvMulti(e.RowIndex)!Primaer = True
                            dgvMulti.Refresh()
                        Else
                            dgvMulti.CancelEdit()
                        End If
                    End Using
                End If
            End If

            If check0.EditingCellValueChanged OrElse check1.EditingCellValueChanged Then mnuSpeichern.Enabled = True
        End If
    End Sub
    Private Sub dgvMulti_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvMulti.RowEnter
        If e.RowIndex > -1 Then
            Dim check1 As DataGridViewCheckBoxCell = CType(dgvMulti(1, e.RowIndex), DataGridViewCheckBoxCell)
            check1.ReadOnly = CInt(dvMulti(e.RowIndex)!Joined) = 0
        End If
    End Sub

#End Region

#Region "Wertungsdetails"
    Private Sub chkSameAK_CheckedChanged(sender As Object, e As EventArgs) Handles chkSameAK.CheckedChanged
        cboSameAK.Enabled = chkSameAK.Checked
        If Not chkSameAK.Checked Then
            cboSameAK.SelectedIndex = -1
        End If
        cboSameAK.Focus()
    End Sub
    Private Sub nudFaktor_TextChanged(sender As Object, e As EventArgs) Handles nudFaktor_m.TextChanged, nudFaktor_w.TextChanged
        With CType(sender, NumericUpDown)
            If .Focused Then
                btnSave.Enabled = True
            End If
        End With
    End Sub
#End Region

    '' Athletik -- Standardwerte setzen (Änderungen überschreiben)
    Private Sub btnDefault_Click(sender As Object, e As EventArgs) Handles btnDefault.Click
        Dim defaults As New DataTable
        Using s As New DataService
            Try
                defaults = s.Get_Athletik_Defaults(, True)
            Catch ex As MySqlException
                MySQl_Error(Nothing, ex.Message, ex.StackTrace)
            End Try
        End Using
        Dim replace As DataRow() = defaults.Select("Disziplin = " & dvA(bsA.Position)!idDisziplin.ToString)
        For Each row As DataRow In replace
            Dim old = dtDefaults.Select("Disziplin = " & row!Disziplin.ToString & " And Bezeichnung = '" & row!Bezeichnung.ToString & "' and Geschlecht = '" & row!Geschlecht.ToString & "'")
            If old.Length = 1 Then
                old(0).ItemArray = row.ItemArray
                mnuSpeichern.Enabled = True
            End If
        Next
    End Sub

End Class