﻿
Public Class frmLogin

    Private nonNumberEntered As Boolean = False

    Private Sub TextboxNum_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBohle.KeyDown
        nonNumberEntered = False
        If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
            If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                If e.KeyCode <> Keys.Back Then
                    If e.KeyCode <> Keys.Decimal Then
                        nonNumberEntered = True
                    End If
                End If
            End If
        End If
        If Control.ModifierKeys = Keys.Shift Then
            nonNumberEntered = True
        End If
    End Sub

    Private Sub TextboxNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBohle.KeyPress
        If nonNumberEntered = True Then
            e.Handled = True
        End If
    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Me.Close()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub txtUsername_TextChanged(sender As Object, e As EventArgs) Handles txtUsername.TextChanged
        btnOK.Enabled = txtBohle.Text > "" 'and txtUsername.Text > "" 
    End Sub

    Private Sub frmLogin_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        formMain.mnuGW.Enabled = True
    End Sub

    Private Sub frmLogin_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        formMain.mnuGW.Enabled = False
        btnOK.Enabled = False
    End Sub

    Private Sub txtBohle_TextChanged(sender As Object, e As EventArgs) Handles txtBohle.TextChanged
        If CInt(txtBohle.Text) < 1 Or CInt(txtBohle.Text) > 2 Then
            MsgBox("Bohle nicht vorhanden.", MsgBoxStyle.Information, "Eingabe-Fehler")
            txtBohle.Text = ""
        End If
        btnOK.Enabled = txtBohle.Text > "" 'and txtUsername.Text > "" 
    End Sub

    Private Sub chkPasswort_CheckedChanged(sender As Object, e As EventArgs) Handles chkPasswort.CheckedChanged
        txtPassword.PasswordChar = CChar(IIf(chkPasswort.Checked, "", "*"))
    End Sub
End Class
