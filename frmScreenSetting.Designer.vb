﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmScreenSetting
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmScreenSetting))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabWertung = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnFontSizeSmaller = New System.Windows.Forms.Button()
        Me.btnResetFontsize = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btnFontSizeLarger = New System.Windows.Forms.Button()
        Me.TabBohle = New System.Windows.Forms.TabPage()
        Me.btnDefault_Bohle = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblWertung = New System.Windows.Forms.Label()
        Me.lblStartnummer = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblVersuch = New System.Windows.Forms.Label()
        Me.lblHantellast = New System.Windows.Forms.Label()
        Me.lblDurchgang = New System.Windows.Forms.Label()
        Me.lblAK = New System.Windows.Forms.Label()
        Me.lblVerein = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.TabControl1.SuspendLayout()
        Me.TabWertung.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabBohle.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabWertung)
        Me.TabControl1.Controls.Add(Me.TabBohle)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(364, 279)
        Me.TabControl1.TabIndex = 0
        '
        'TabWertung
        '
        Me.TabWertung.Controls.Add(Me.GroupBox2)
        Me.TabWertung.Controls.Add(Me.GroupBox1)
        Me.TabWertung.Location = New System.Drawing.Point(4, 22)
        Me.TabWertung.Name = "TabWertung"
        Me.TabWertung.Size = New System.Drawing.Size(356, 253)
        Me.TabWertung.TabIndex = 3
        Me.TabWertung.Text = "Wertung"
        Me.TabWertung.ToolTipText = "Wertungsanzeige mit Athletik"
        Me.TabWertung.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RadioButton2)
        Me.GroupBox2.Controls.Add(Me.RadioButton1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 90)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(330, 47)
        Me.GroupBox2.TabIndex = 67
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Ansicht"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(94, 19)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(41, 17)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.Text = "hell"
        Me.ToolTip1.SetToolTip(Me.RadioButton2, "Hintergrund hell, Schrift dunkel")
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(15, 19)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(57, 17)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "dunkel"
        Me.ToolTip1.SetToolTip(Me.RadioButton1, "Hintergrund dunkel, Schrift hell")
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnFontSizeSmaller)
        Me.GroupBox1.Controls.Add(Me.btnResetFontsize)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.btnFontSizeLarger)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 14)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(331, 58)
        Me.GroupBox1.TabIndex = 66
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Schriftgröße"
        '
        'btnFontSizeSmaller
        '
        Me.btnFontSizeSmaller.Image = Global.Gewichtheben.My.Resources.Resources.arrow_down_16
        Me.btnFontSizeSmaller.Location = New System.Drawing.Point(145, 19)
        Me.btnFontSizeSmaller.Name = "btnFontSizeSmaller"
        Me.btnFontSizeSmaller.Size = New System.Drawing.Size(28, 25)
        Me.btnFontSizeSmaller.TabIndex = 65
        Me.btnFontSizeSmaller.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.btnFontSizeSmaller, "Schrift kleiner")
        Me.btnFontSizeSmaller.UseVisualStyleBackColor = True
        '
        'btnResetFontsize
        '
        Me.btnResetFontsize.Location = New System.Drawing.Point(192, 19)
        Me.btnResetFontsize.Name = "btnResetFontsize"
        Me.btnResetFontsize.Size = New System.Drawing.Size(90, 25)
        Me.btnResetFontsize.TabIndex = 1
        Me.btnResetFontsize.Text = "Zurücksetzen"
        Me.ToolTip1.SetToolTip(Me.btnResetFontsize, "auf Standard setzen")
        Me.btnResetFontsize.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(15, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(90, 13)
        Me.Label17.TabIndex = 63
        Me.Label17.Text = "Wertungsanzeige"
        '
        'btnFontSizeLarger
        '
        Me.btnFontSizeLarger.Image = Global.Gewichtheben.My.Resources.Resources.arrow_up_16
        Me.btnFontSizeLarger.Location = New System.Drawing.Point(111, 19)
        Me.btnFontSizeLarger.Name = "btnFontSizeLarger"
        Me.btnFontSizeLarger.Size = New System.Drawing.Size(28, 25)
        Me.btnFontSizeLarger.TabIndex = 64
        Me.btnFontSizeLarger.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.btnFontSizeLarger, "Schrift größer")
        Me.btnFontSizeLarger.UseVisualStyleBackColor = True
        '
        'TabBohle
        '
        Me.TabBohle.Controls.Add(Me.btnDefault_Bohle)
        Me.TabBohle.Controls.Add(Me.Panel1)
        Me.TabBohle.Location = New System.Drawing.Point(4, 22)
        Me.TabBohle.Name = "TabBohle"
        Me.TabBohle.Padding = New System.Windows.Forms.Padding(3)
        Me.TabBohle.Size = New System.Drawing.Size(356, 253)
        Me.TabBohle.TabIndex = 1
        Me.TabBohle.Text = "Bohle"
        Me.TabBohle.UseVisualStyleBackColor = True
        '
        'btnDefault_Bohle
        '
        Me.btnDefault_Bohle.Location = New System.Drawing.Point(10, 216)
        Me.btnDefault_Bohle.Name = "btnDefault_Bohle"
        Me.btnDefault_Bohle.Size = New System.Drawing.Size(96, 27)
        Me.btnDefault_Bohle.TabIndex = 1
        Me.btnDefault_Bohle.Text = "Voreinstellung"
        Me.btnDefault_Bohle.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Black
        Me.Panel1.Controls.Add(Me.lblWertung)
        Me.Panel1.Controls.Add(Me.lblStartnummer)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.lblVersuch)
        Me.Panel1.Controls.Add(Me.lblHantellast)
        Me.Panel1.Controls.Add(Me.lblDurchgang)
        Me.Panel1.Controls.Add(Me.lblAK)
        Me.Panel1.Controls.Add(Me.lblVerein)
        Me.Panel1.Controls.Add(Me.lblName)
        Me.Panel1.Location = New System.Drawing.Point(10, 10)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(335, 200)
        Me.Panel1.TabIndex = 29
        '
        'lblWertung
        '
        Me.lblWertung.AutoSize = True
        Me.lblWertung.BackColor = System.Drawing.Color.Black
        Me.lblWertung.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblWertung.Font = New System.Drawing.Font("Wingdings", 32.0!)
        Me.lblWertung.ForeColor = System.Drawing.Color.FromArgb(CType(CType(56, Byte), Integer), CType(CType(56, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.lblWertung.Location = New System.Drawing.Point(11, 145)
        Me.lblWertung.Name = "lblWertung"
        Me.lblWertung.Size = New System.Drawing.Size(116, 47)
        Me.lblWertung.TabIndex = 26
        Me.lblWertung.Tag = "Format:ShadeColor"
        Me.lblWertung.Text = "lll"
        '
        'lblStartnummer
        '
        Me.lblStartnummer.AutoSize = True
        Me.lblStartnummer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblStartnummer.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartnummer.ForeColor = System.Drawing.Color.Gold
        Me.lblStartnummer.Location = New System.Drawing.Point(289, 16)
        Me.lblStartnummer.Name = "lblStartnummer"
        Me.lblStartnummer.Size = New System.Drawing.Size(35, 26)
        Me.lblStartnummer.TabIndex = 7
        Me.lblStartnummer.Tag = "Format:NameColor"
        Me.lblStartnummer.Text = "Nr"
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label16.Image = CType(resources.GetObject("Label16.Image"), System.Drawing.Image)
        Me.Label16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label16.Location = New System.Drawing.Point(139, 145)
        Me.Label16.Name = "Label16"
        Me.Label16.Padding = New System.Windows.Forms.Padding(5, 5, 0, 5)
        Me.Label16.Size = New System.Drawing.Size(195, 54)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "                   Zum Ändern der Anzeige " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "                   auf das Element kl" &
    "icken."
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVersuch
        '
        Me.lblVersuch.AutoSize = True
        Me.lblVersuch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblVersuch.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.lblVersuch.ForeColor = System.Drawing.Color.Coral
        Me.lblVersuch.Location = New System.Drawing.Point(208, 92)
        Me.lblVersuch.Name = "lblVersuch"
        Me.lblVersuch.Size = New System.Drawing.Size(33, 26)
        Me.lblVersuch.TabIndex = 5
        Me.lblVersuch.Text = "X."
        '
        'lblHantellast
        '
        Me.lblHantellast.AutoSize = True
        Me.lblHantellast.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblHantellast.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHantellast.ForeColor = System.Drawing.Color.Coral
        Me.lblHantellast.Location = New System.Drawing.Point(207, 55)
        Me.lblHantellast.Name = "lblHantellast"
        Me.lblHantellast.Size = New System.Drawing.Size(86, 26)
        Me.lblHantellast.TabIndex = 4
        Me.lblHantellast.Text = "XXX kg"
        '
        'lblDurchgang
        '
        Me.lblDurchgang.AutoSize = True
        Me.lblDurchgang.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblDurchgang.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblDurchgang.ForeColor = System.Drawing.Color.Coral
        Me.lblDurchgang.Location = New System.Drawing.Point(236, 98)
        Me.lblDurchgang.Name = "lblDurchgang"
        Me.lblDurchgang.Size = New System.Drawing.Size(88, 20)
        Me.lblDurchgang.TabIndex = 3
        Me.lblDurchgang.Tag = "Format:DurchgangColor"
        Me.lblDurchgang.Text = "Durchgang"
        '
        'lblAK
        '
        Me.lblAK.AutoSize = True
        Me.lblAK.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblAK.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAK.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblAK.Location = New System.Drawing.Point(14, 92)
        Me.lblAK.Name = "lblAK"
        Me.lblAK.Size = New System.Drawing.Size(186, 26)
        Me.lblAK.TabIndex = 2
        Me.lblAK.Tag = "Format:AkColor"
        Me.lblAK.Text = "Altersklasse    GK"
        '
        'lblVerein
        '
        Me.lblVerein.AutoSize = True
        Me.lblVerein.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblVerein.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVerein.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblVerein.Location = New System.Drawing.Point(14, 54)
        Me.lblVerein.Name = "lblVerein"
        Me.lblVerein.Size = New System.Drawing.Size(141, 26)
        Me.lblVerein.TabIndex = 1
        Me.lblVerein.Tag = "Format:VereinColor"
        Me.lblVerein.Text = "Verein / Land"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.ForeColor = System.Drawing.Color.Gold
        Me.lblName.Location = New System.Drawing.Point(14, 16)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(172, 26)
        Me.lblName.TabIndex = 0
        Me.lblName.Tag = "Format:NameColor"
        Me.lblName.Text = "Name, Vorname"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(286, 297)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(90, 27)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Schließen"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmScreenSetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(388, 336)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.TabControl1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmScreenSetting"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Anzeigen anpassen"
        Me.TabControl1.ResumeLayout(False)
        Me.TabWertung.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabBohle.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabBohle As TabPage
    Friend WithEvents btnClose As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblStartnummer As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents lblVersuch As Label
    Friend WithEvents lblHantellast As Label
    Friend WithEvents lblDurchgang As Label
    Friend WithEvents lblAK As Label
    Friend WithEvents lblVerein As Label
    Friend WithEvents lblName As Label
    Friend WithEvents lblWertung As Label
    Friend WithEvents TabWertung As TabPage
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ColorDialog1 As ColorDialog
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnFontSizeSmaller As Button
    Friend WithEvents btnResetFontsize As Button
    Friend WithEvents Label17 As Label
    Friend WithEvents btnFontSizeLarger As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents btnDefault_Bohle As Button
End Class
