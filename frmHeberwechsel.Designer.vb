﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHeberwechsel
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnlAustausch = New System.Windows.Forms.Panel()
        Me.btnAustauchOK = New System.Windows.Forms.Button()
        Me.cboGruppe = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.EmergencyLifters = New System.Windows.Forms.DataGridView()
        Me.Nachname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vorname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblAustauschMessage = New System.Windows.Forms.Label()
        Me.btnAustauschExit = New System.Windows.Forms.Button()
        Me.lblAustausch = New System.Windows.Forms.Label()
        Me.pnlAustausch.SuspendLayout()
        CType(Me.EmergencyLifters, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlAustausch
        '
        Me.pnlAustausch.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(239, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pnlAustausch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlAustausch.Controls.Add(Me.btnAustauchOK)
        Me.pnlAustausch.Controls.Add(Me.cboGruppe)
        Me.pnlAustausch.Controls.Add(Me.Label1)
        Me.pnlAustausch.Controls.Add(Me.EmergencyLifters)
        Me.pnlAustausch.Controls.Add(Me.lblAustauschMessage)
        Me.pnlAustausch.Controls.Add(Me.btnAustauschExit)
        Me.pnlAustausch.Controls.Add(Me.lblAustausch)
        Me.pnlAustausch.Location = New System.Drawing.Point(0, 0)
        Me.pnlAustausch.Name = "pnlAustausch"
        Me.pnlAustausch.Size = New System.Drawing.Size(347, 204)
        Me.pnlAustausch.TabIndex = 510
        Me.pnlAustausch.Visible = False
        '
        'btnAustauchOK
        '
        Me.btnAustauchOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAustauchOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnAustauchOK.Font = New System.Drawing.Font("Wingdings", 18.0!)
        Me.btnAustauchOK.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(172, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnAustauchOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnAustauchOK.Location = New System.Drawing.Point(306, 165)
        Me.btnAustauchOK.Name = "btnAustauchOK"
        Me.btnAustauchOK.Size = New System.Drawing.Size(31, 29)
        Me.btnAustauchOK.TabIndex = 509
        Me.btnAustauchOK.Text = ""
        Me.btnAustauchOK.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnAustauchOK.UseCompatibleTextRendering = True
        Me.btnAustauchOK.UseVisualStyleBackColor = True
        '
        'cboGruppe
        '
        Me.cboGruppe.Enabled = False
        Me.cboGruppe.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.cboGruppe.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.cboGruppe.FormattingEnabled = True
        Me.cboGruppe.Location = New System.Drawing.Point(183, 166)
        Me.cboGruppe.Name = "cboGruppe"
        Me.cboGruppe.Size = New System.Drawing.Size(44, 28)
        Me.cboGruppe.TabIndex = 511
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(162, Byte), Integer), CType(CType(6, Byte), Integer))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(0, 158)
        Me.Label1.Name = "Label1"
        Me.Label1.Padding = New System.Windows.Forms.Padding(7, 0, 0, 0)
        Me.Label1.Size = New System.Drawing.Size(188, 42)
        Me.Label1.TabIndex = 510
        Me.Label1.Text = "einwechseln in Gruppe"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label1.UseCompatibleTextRendering = True
        '
        'EmergencyLifters
        '
        Me.EmergencyLifters.AllowUserToAddRows = False
        Me.EmergencyLifters.AllowUserToDeleteRows = False
        Me.EmergencyLifters.AllowUserToResizeRows = False
        Me.EmergencyLifters.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EmergencyLifters.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.EmergencyLifters.BackgroundColor = System.Drawing.SystemColors.Window
        Me.EmergencyLifters.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.EmergencyLifters.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.EmergencyLifters.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable
        Me.EmergencyLifters.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.EmergencyLifters.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.EmergencyLifters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.EmergencyLifters.ColumnHeadersVisible = False
        Me.EmergencyLifters.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Nachname, Me.Vorname})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.Padding = New System.Windows.Forms.Padding(0, 4, 0, 4)
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.EmergencyLifters.DefaultCellStyle = DataGridViewCellStyle4
        Me.EmergencyLifters.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.EmergencyLifters.EnableHeadersVisualStyles = False
        Me.EmergencyLifters.Location = New System.Drawing.Point(0, 70)
        Me.EmergencyLifters.MultiSelect = False
        Me.EmergencyLifters.Name = "EmergencyLifters"
        Me.EmergencyLifters.ReadOnly = True
        Me.EmergencyLifters.RowHeadersVisible = False
        Me.EmergencyLifters.RowHeadersWidth = 51
        Me.EmergencyLifters.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.EmergencyLifters.RowTemplate.Height = 33
        Me.EmergencyLifters.RowTemplate.ReadOnly = True
        Me.EmergencyLifters.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.EmergencyLifters.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.EmergencyLifters.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.EmergencyLifters.ShowCellErrors = False
        Me.EmergencyLifters.ShowCellToolTips = False
        Me.EmergencyLifters.ShowEditingIcon = False
        Me.EmergencyLifters.ShowRowErrors = False
        Me.EmergencyLifters.Size = New System.Drawing.Size(345, 85)
        Me.EmergencyLifters.TabIndex = 506
        '
        'Nachname
        '
        Me.Nachname.DataPropertyName = "Nachname"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Nachname.DefaultCellStyle = DataGridViewCellStyle2
        Me.Nachname.HeaderText = "Nachname"
        Me.Nachname.MinimumWidth = 8
        Me.Nachname.Name = "Nachname"
        Me.Nachname.ReadOnly = True
        '
        'Vorname
        '
        Me.Vorname.DataPropertyName = "Vorname"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Vorname.DefaultCellStyle = DataGridViewCellStyle3
        Me.Vorname.HeaderText = "Vorname"
        Me.Vorname.MinimumWidth = 8
        Me.Vorname.Name = "Vorname"
        Me.Vorname.ReadOnly = True
        '
        'lblAustauschMessage
        '
        Me.lblAustauschMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblAustauschMessage.ForeColor = System.Drawing.Color.MediumBlue
        Me.lblAustauschMessage.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblAustauschMessage.Location = New System.Drawing.Point(0, 35)
        Me.lblAustauschMessage.Name = "lblAustauschMessage"
        Me.lblAustauschMessage.Padding = New System.Windows.Forms.Padding(7, 0, 0, 0)
        Me.lblAustauschMessage.Size = New System.Drawing.Size(345, 35)
        Me.lblAustauschMessage.TabIndex = 505
        Me.lblAustauschMessage.Text = "für Friedrich, Raphael"
        Me.lblAustauschMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblAustauschMessage.UseCompatibleTextRendering = True
        '
        'btnAustauschExit
        '
        Me.btnAustauschExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAustauschExit.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnAustauschExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnAustauschExit.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnAustauschExit.FlatAppearance.BorderSize = 0
        Me.btnAustauschExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnAustauschExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnAustauschExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAustauschExit.Font = New System.Drawing.Font("Wingdings 2", 14.0!, System.Drawing.FontStyle.Bold)
        Me.btnAustauschExit.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.btnAustauschExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnAustauschExit.Location = New System.Drawing.Point(308, -1)
        Me.btnAustauschExit.Name = "btnAustauschExit"
        Me.btnAustauschExit.Size = New System.Drawing.Size(38, 36)
        Me.btnAustauschExit.TabIndex = 504
        Me.btnAustauschExit.TabStop = False
        Me.btnAustauschExit.Text = ""
        Me.btnAustauschExit.UseVisualStyleBackColor = False
        '
        'lblAustausch
        '
        Me.lblAustausch.BackColor = System.Drawing.Color.RoyalBlue
        Me.lblAustausch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAustausch.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAustausch.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblAustausch.ForeColor = System.Drawing.Color.White
        Me.lblAustausch.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblAustausch.Location = New System.Drawing.Point(0, 0)
        Me.lblAustausch.Name = "lblAustausch"
        Me.lblAustausch.Padding = New System.Windows.Forms.Padding(7, 0, 0, 0)
        Me.lblAustausch.Size = New System.Drawing.Size(345, 35)
        Me.lblAustausch.TabIndex = 503
        Me.lblAustausch.Text = "Ersatzheber"
        Me.lblAustausch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblAustausch.UseCompatibleTextRendering = True
        '
        'frmHeberwechsel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnAustauschExit
        Me.ClientSize = New System.Drawing.Size(347, 204)
        Me.Controls.Add(Me.pnlAustausch)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHeberwechsel"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Heberwechsel"
        Me.pnlAustausch.ResumeLayout(False)
        CType(Me.EmergencyLifters, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnlAustausch As Panel
    Friend WithEvents btnAustauchOK As Button
    Friend WithEvents cboGruppe As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents EmergencyLifters As DataGridView
    Friend WithEvents lblAustauschMessage As Label
    Friend WithEvents btnAustauschExit As Button
    Friend WithEvents lblAustausch As Label
    Friend WithEvents Nachname As DataGridViewTextBoxColumn
    Friend WithEvents Vorname As DataGridViewTextBoxColumn
End Class
