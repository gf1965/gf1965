﻿
Imports System.ComponentModel
Imports System.Drawing.Text
Imports System.Math
Imports MySqlConnector

Public Class frmMeldung
    Dim Bereich As String = "Meldung"
    Dim Query As String
    Dim cmd As MySqlCommand

    Dim dtAthleten As New DataTable
    Dim dtMeldung As New DataTable
    Dim dtVerein As New DataTable
    Dim dtAuswahl As DataTable
    Dim dtAKs As New DataTable
    Dim dtStaaten As New DataTable
    Dim dtTeams As New DataTable

    Dim dv As DataView      ' obere Tabelle
    Dim dvA As DataView     ' Athleten

    Dim LayoutChanged_M As Boolean
    Dim LayoutChanged_A As Boolean
    Dim nonNumberEntered As Boolean
    Private CellInEdit As Boolean = True
    Private AthletenFilter As String

    Private loading As Boolean = True

    Private _CheckedRows As Integer
    Property CheckedRows As Integer
        Get
            Return _CheckedRows
        End Get
        Set(value As Integer)
            _CheckedRows = value
            btnAthlet_Übernehmen.Enabled = value > 0
            If value = 0 Then
                chkAlle_Athlet.CheckState = CheckState.Unchecked
            ElseIf value = bsA.Count Then
                chkAlle_Athlet.CheckState = CheckState.Checked
            Else
                chkAlle_Athlet.CheckState = CheckState.Indeterminate
            End If
        End Set
    End Property

    '' Me
    Private Sub Me_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        If LayoutChanged_M AndAlso MsgBox("Änderungen an der Tabelle " & Chr(34) & "Meldung" & Chr(34) & " speichern?", MsgBoxStyle.YesNo, "Tabellen-Layout geändert") = MsgBoxResult.Yes Then
            Write_ColumnLayout_ToINI(dgvJoin, "Meldung")
        End If
        If LayoutChanged_A AndAlso MsgBox("Änderungen an der Tabelle " & Chr(34) & "Athleten" & Chr(34) & " speichern?", MsgBoxStyle.YesNo, "Tabellen-Layout geändert") = MsgBoxResult.Yes Then
            Write_ColumnLayout_ToINI(dgvAuswahl, "Meldung_Auswahl")
        End If
        Dispose()
    End Sub
    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        NativeMethods.INI_Write(Bereich, "Konflikte", mnuKonflikte.Checked.ToString, App_IniFile)
        NativeMethods.INI_Write(Bereich, "ShowMessages", mnuNoMessage.Checked.ToString, App_IniFile)
        NativeMethods.INI_Write(Bereich, "Lokal", mnuLokaleDatenbank.Checked.ToString, App_IniFile)

        If mnuSpeichern.Enabled Then
            Using New Centered_MessageBox(Me)
                Select Case MessageBox.Show("Änderungen an der Meldeliste speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                    Case DialogResult.Cancel
                        e.Cancel = True
                    Case DialogResult.Yes
                        mnuSpeichern.PerformClick()
                End Select
            End Using
        End If
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        Try
            Dim x As String
            x = NativeMethods.INI_Read(Bereich, "Konflikte", App_IniFile)
            If x <> "?" Then mnuKonflikte.Checked = CBool(x)
            x = NativeMethods.INI_Read(Bereich, "ShowMessages", App_IniFile)
            If x <> "?" Then mnuNoMessage.Checked = CBool(x)
        Catch ex As Exception
        End Try

        Dim tTip As New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        tTip.SetToolTip(chkDatumMerken, "wenn aktiviert, wird angezeigtes Datum verwendet")
        tTip.SetToolTip(btnWeiter, "Speichern und nächsten Datensatz aufrufen (Strg+Enter)")
        tTip.SetToolTip(btnZurück, "Speichern und vorhergehenden Datensatz aufrufen")
        tTip.SetToolTip(txtReissen, "Anfangslast Reißen")
        tTip.SetToolTip(txtStossen, "Anfangslast Stoßen")
        tTip.SetToolTip(cboAK, "Altersklasse ändern")

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Refresh()

        Dim AK_Source As New DataTable

        Using conn As New MySqlConnection(User.ConnString)
            Try
                If conn.State = ConnectionState.Closed Then conn.Open()

                Query = "SELECT * FROM athleten;"
                cmd = New MySqlCommand(Query, conn)
                dtAthleten.Load(cmd.ExecuteReader())

                Query = "SELECT * FROM staaten;"
                cmd = New MySqlCommand(Query, conn)
                dtStaaten.Load(cmd.ExecuteReader())

                Query = "SELECT w.Team, ifnull(t.idVerein, w.Team) Verein, v.Vereinsname 
                         FROM wettkampf_teams w
                         LEFT JOIN teams t ON t.idTeam = w.Team
                         LEFT JOIN verein v ON v.idVerein = w.Team
                         WHERE" & Wettkampf.WhereClause() & ";"
                cmd = New MySqlCommand(Query, conn)
                dtTeams.Load(cmd.ExecuteReader())

                Query = "SELECT * FROM verein ORDER BY Vereinsname;"
                cmd = New MySqlCommand(Query, conn)
                dtVerein.Load(cmd.ExecuteReader())

                Query = "select distinct Altersklasse, min(von) over (partition by Altersklasse) von, max(bis) over (partition by Altersklasse) maxAge
                        from altersklassen 
                        where International = " & Wettkampf.International & "
                        order by idAltersklasse;"
                cmd = New MySqlCommand(Query, conn)
                AK_Source.Load(cmd.ExecuteReader())

                Query = "select * from altersklassen where International = " & Wettkampf.International & " order by idAltersklasse;"
                cmd = New MySqlCommand(Query, conn)
                dtAKs.Load(cmd.ExecuteReader())

                'Datenquelle für obere Tabelle
                ' MSR  = Mannschafts-Startrecht aus athleten
                ' TEAM = Mannschafts-Startrecht aus teams (Kampfgemeinschaft)
                ' ------------ Reihenfolge obere und untere Tabelle muss bis AK übereinstimmen ------------
                ' IsDirty deaktiviert mnuHeber und mnuVerein, wenn in [btnAthlet_Übernehmen] ein Athlet hinzugefügt oder geändert wird
                '   wird beim Speichern der Meldung zurückgesetzt --> ab da kann Athlet über das Menü bearbeitet werden

                Query = "Select m.Teilnehmer, a.Geschlecht Sex, a.Nachname, a.Vorname, a.Geburtstag, a.Jahrgang, a.Staat, s.state_name, v.idVerein ESR, IfNull(v.Kurzname, v.Vereinsname) Verein, 
                    	IFNULL(a.MSR, a.Verein) MSR, IfNull(vv.Kurzname, vv.Vereinsname) Mannschaft, 
                        group_concat(distinct m.Wettkampf order by m.Wettkampf) Wettkampf,
                        m.Sortierung, m.Losnummer, m.Laenderwertung, m.Vereinswertung, m.Relativ_W, m.a_K, m.idAK, m.AK, 
                        -- m.Gewicht, m.Wiegen, m.Startnummer, m.idGK, m.GK, m.Meldedatum, ifnull(m.Zweikampf, m.Reissen + m.Stossen) Zweikampf, m.Reissen, m.Stossen, m.attend, 
                        m.Gewicht, m.Wiegen, m.Startnummer, m.idGK, m.GK, m.Meldedatum, m.Zweikampf, m.Reissen, m.Stossen, m.attend, 
                        IFNULL(m.Team, IFNULL(t.idTeam, IFNULL(a.MSR, a.Verein))) Team, false IsDirty, m.OnlineStatus
                        FROM meldung m 
                        LEFT JOIN athleten a On m.Teilnehmer = a.idTeilnehmer 
                        Left Join verein v ON IFNULL(a.ESR, a.Verein) = v.idVerein 
                        left join teams t on t.idVerein = IFNULL(a.MSR, a.Verein) 
                        Left Join verein vv On IFNULL(m.Team, IFNULL(t.idTeam, IFNULL(a.MSR, a.Verein))) = vv.idVerein 
                        left join staaten s on s.state_id = a.Staat
                        WHERE " & Wettkampf.WhereClause("m") &
                        " group by m.Teilnehmer
                        ORDER BY a.Geschlecht DESC, a.Nachname, a.Vorname;"
                cmd = New MySqlCommand(Query, conn)
                dtMeldung.Load(cmd.ExecuteReader())

                ''Datenquelle für untere Tabelle
                'Query = "select a.idTeilnehmer Teilnehmer, a.Geschlecht Sex, a.Nachname, a.Vorname, a.Geburtstag, a.Jahrgang, a.Staat, s.state_name, ifnull(a.ESR, a.Verein) ESR, v.Vereinsname Verein, " &
                '        "IfNull(a.MSR, a.Verein) MSR, vv.Vereinsname Mannschaft, ak.idAltersklasse idAK, " &
                '        "If(ak.Altersklasse = 'Masters', concat(ak.Altersklasse, ' (', If(a.Geschlecht = 'w' and ak.AK > 8, 8, ak.AK), ')'), ak.Altersklasse) AK " &
                '        "from athleten a " &
                '        "Left Join verein v ON v.idVerein = ifnull(a.ESR, a.Verein) " &
                '        "left join teams t on t.idVerein = IFNULL(a.MSR, a.Verein) " &
                '        "Left Join verein vv On IFNULL(t.idTeam, IFNULL(a.MSR, a.Verein)) = vv.idVerein " &
                '        "LEFT JOIN altersklassen ak On Year(now()) - a.Jahrgang between ak.von And ak.bis and ak.International = " & Wettkampf.International &
                '        " left join staaten s on s.state_id = a.Staat " &
                '        "where Not exists(select * from meldung m where " & Wettkampf.WhereClause("m") & " And m.Teilnehmer = a.idTeilnehmer) " &
                '        "group by a.idTeilnehmer;"
                'cmd = New MySqlCommand(Query, conn)
                'dtAuswahl = New DataTable
                'dtAuswahl.Load(cmd.ExecuteReader())

            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            End Try
        End Using

        ' Primärschlüssel für dtMeldung ändern, damit TN nicht 2x existieren kann
        dtMeldung.PrimaryKey = Nothing
        Dim cols(1) As DataColumn
        cols(0) = dtMeldung.Columns("Teilnehmer")
        dtMeldung.PrimaryKey = cols

        If Not IsNothing(Wettkampf.Identities) AndAlso Wettkampf.Identities.Count > 0 Then
            For Each key In Wettkampf.Identities.Keys
                dtMeldung.Columns.Add("WK_" & key, GetType(Boolean))
            Next
            For Each row As DataRow In dtMeldung.Rows
                Dim lst = row!Wettkampf.ToString.Split(CChar(","))
                For Each item In lst
                    row("WK_" & item) = True
                Next
            Next
        End If
        dtMeldung.AcceptChanges()

        'Build_Grid("Meldung_Auswahl", dgvAuswahl)
        'Fill_dgvAuswahl()

        'dtAuswahl.Columns.Add(New DataColumn With {.ColumnName = "Übernehmen", .DataType = GetType(Boolean), .DefaultValue = False})
        'dvA = New DataView(dtAuswahl)
        'bsA = New BindingSource With {.DataSource = dvA}
        'dgvAuswahl.DataSource = bsA

        Build_Grid(Bereich, dgvJoin)
        dv = New DataView(dtMeldung)
        bs = New BindingSource With {.DataSource = dv}
        dgvJoin.DataSource = bs

        lblAnzahl.Text = "TN:  " & dv.Count

        For Each row As DataRowView In Glob.dvMannschaft
            If CInt(row!idMannschaft) = 0 Then
                ' Vereinsmannschaft 
                ' (pro Verein können max. 3 Vereinsmannschaften zur Vereinsmeisterschaft gemeldet werden)
                ' ==> stimmt das?
                Dim dic As New Dictionary(Of String, Integer?)
                dic("") = Nothing
                dic("1") = 1
                dic("2") = 2
                dic("3") = 3
                With cboVereinsmannschaft
                    .DataSource = New BindingSource(dic, Nothing)
                    .DisplayMember = "Key"
                    .ValueMember = "Value"
                    .Visible = True
                End With
                dgvJoin.Columns("Vereinswertung").Visible = True
            ElseIf CInt(row!idMannschaft) = 1 Then
                ' Ländermannschaft
                chkLändermannschaft.Visible = True
                dgvJoin.Columns("L_W").Visible = True
            End If
        Next

#Region "DataBinding"
        txtGewicht.DataBindings.Add(New Binding("Text", bs, "Gewicht", False, DataSourceUpdateMode.OnPropertyChanged))
        cboGK.DataBindings.Add(New Binding("SelectedValue", bs, "idGK", False, DataSourceUpdateMode.OnPropertyChanged))
        txtZweikampf.DataBindings.Add(New Binding("Text", bs, "Zweikampf", False, DataSourceUpdateMode.OnPropertyChanged))
        txtReissen.DataBindings.Add(New Binding("Text", bs, "Reissen", False, DataSourceUpdateMode.OnPropertyChanged))
        txtStossen.DataBindings.Add(New Binding("Text", bs, "Stossen", False, DataSourceUpdateMode.OnPropertyChanged))
        chkLändermannschaft.DataBindings.Add(New Binding("Checked", bs, "Laenderwertung", False, DataSourceUpdateMode.OnPropertyChanged))
        cboVereinsmannschaft.DataBindings.Add(New Binding("SelectedValue", bs, "Vereinswertung", False, DataSourceUpdateMode.OnPropertyChanged))
        chkA_K.DataBindings.Add(New Binding("Checked", bs, "a_K", False, DataSourceUpdateMode.OnPropertyChanged))
        dpkDatum.DataBindings.Add(New Binding("Text", bs, "Meldedatum", False, DataSourceUpdateMode.OnPropertyChanged))
        txtLosnummer.DataBindings.Add(New Binding("Text", bs, "Losnummer", False, DataSourceUpdateMode.OnPropertyChanged))
#End Region

        'If Not IsNothing(dgvJoin.CurrentRow) Then bs_PositionChanged(bs, Nothing)

        CheckedRows = 0
        mnuSpeichern.Enabled = False
        LayoutChanged_M = False
        LayoutChanged_A = False

        formMain.Change_MainProgressBarValue(0)

        pnlMeldedatum.Location = dpkDatum.Location

        'loading = True
        AK_Source.Rows.Add({"[automatisch]", DBNull.Value, DBNull.Value})
        With cboAK
            .DataSource = AK_Source
            .DisplayMember = "Altersklasse"
            .SelectedIndex = AK_Source.Rows.Count - 1
        End With
        loading = False

        If Not IsNothing(dgvJoin.CurrentRow) Then
            bs_PositionChanged(bs, Nothing)
            dgvJoin.Rows(dgvJoin.CurrentRow.Index).Selected = True
        End If

        If Wettkampf.Mannschaft Then
            txtAthlet_Verein.Text = "Mannschaft"
            txtAthlet_Verein.Tag = "Mannschaft"
            dgvAuswahl.Columns("Verein").HeaderText = "Mannschaft"
            dgvAuswahl.Columns("Verein").DataPropertyName = "Mannschaft"
        End If

        Dim x = NativeMethods.INI_Read(Bereich, "Lokal", App_IniFile)
        Dim c = mnuLokaleDatenbank.Checked
        If x.Equals("?") OrElse x.Equals("False") Then
            mnuLokaleDatenbank.Checked = False
        Else
            mnuLokaleDatenbank.Checked = True
        End If
        If c = mnuLokaleDatenbank.Checked Then Fill_dgvAuswahl(c)

        If dgvJoin.Rows.Count > 0 Then
            dgvJoin.Focus()
            dgvJoin.Rows(0).Selected = True
        Else
            dgvAuswahl.Focus()
        End If

        'SplitContainer1.IsSplitterFixed = True

        Cursor = Cursors.Default
    End Sub

    Private Sub Fill_dgvAuswahl(Optional Local As Boolean = True)
        Dim sql As String
        Dim con As String
        If Local Then
            sql = "select   a.idTeilnehmer Teilnehmer, 
                            a.Geschlecht Sex, 
                            a.Nachname, 
                            a.Vorname, 
                            a.Geburtstag, 
                            a.Jahrgang, 
                            a.Staat, 
                            s.state_name, 
                            ifnull(a.ESR, a.Verein) ESR, IfNull(v.Kurzname, v.Vereinsname) Verein, 
                            IfNull(a.MSR, a.Verein) MSR, IfNull(vv.Kurzname, vv.Vereinsname) Mannschaft, 
                            ak.idAltersklasse idAK, 
                            If(ak.Altersklasse = 'Masters', concat(ak.Altersklasse, ' (', If(a.Geschlecht = 'w' and ak.AK > 8, 8, ak.AK), ')'), ak.Altersklasse) AK 
                from athleten a 
                Left Join verein v ON v.idVerein = ifnull(a.ESR, a.Verein) 
                left join teams t on t.idVerein = IFNULL(a.MSR, a.Verein) 
                Left Join verein vv On IFNULL(t.idTeam, IFNULL(a.MSR, a.Verein)) = vv.idVerein 
                LEFT JOIN altersklassen ak On Year(now()) - a.Jahrgang between ak.von And 
                                              ak.bis and ak.International = " & Wettkampf.International &
              " left join staaten s on s.state_id = a.Staat 
                -- where Not exists(select * from meldung m where " & Wettkampf.WhereClause("m") & " And m.Teilnehmer = a.idTeilnehmer) 
                group by a.idTeilnehmer;"
            'order by a.Nachname, a.Vorname, a.Geburtstag desc;"
            con = User.ConnString
        Else
            sql = "select	athleten_id                     Teilnehmer, 
                            substr(athlet_geschlecht,1,1)   Sex,
		                    athlet_name                     Nachname, 
                            athlet_vorname                  Vorname, 
                            geburtstag                      Geburtstag,
                            athlet_gebjahr                  Jahrgang,
                            0                               Staat,
                            athlet_nation                   state_name,
                            verein Verein,
                            verein_cas_id,
                            athleten_cas_id                 GUID,
                            0                               idAK,
                            0 ESR,
                            0 MSR,
                    from athleten;"
            sql = "select distinct
                        a.athleten_id                   Teilnehmer, 
                        a.athleten_cas_id, 
                        a.athlet_name                   Nachname, 
                        a.athlet_vorname                Vorname, 
                        a.geburtstag                    Geburtstag, 
                        a.athlet_gebjahr                Jahrgang, 
                        substr(a.athlet_geschlecht,1,1) Sex,
                        a.athlet_nation                 state_name,
                        0                               Staat,
                        0                               idAK,
	                    (select verein_cas_id from athleten where athleten_id = a.athleten_id and startrecht_typ <> 2 limit 1) 
                                                        ESR,
	                    (select verein from athleten where athleten_id = a.athleten_id and startrecht_typ <> 2 limit 1) 
                                                        Verein,
	                    (select verein_cas_id from athleten where athleten_id = a.athleten_id and startrecht_typ > 1 limit 1) 
                                                        MSR,
	                    (select verein from athleten where athleten_id = a.athleten_id and startrecht_typ > 1 limit 1) 
                                                        Mannschaft
                    from athleten a
                    where a.startrecht_typ " & If(Wettkampf.Mannschaft, "> 1", "<> 2") &
                    ";"
            con = User.BVDG_ConnString.Result
        End If

        Cursor = Cursors.WaitCursor

        Try
            Using conn As New MySqlConnection(con)
                conn.Open()
                cmd = New MySqlCommand(sql, conn)
                dtAuswahl = New DataTable
                dtAuswahl.Load(cmd.ExecuteReader())
            End Using

            dtAuswahl.Columns.Add(New DataColumn With {.ColumnName = "Übernehmen", .DataType = GetType(Boolean), .DefaultValue = False})

            If Not Local Then
                'dtAuswahl.Columns.Add(New DataColumn With {.ColumnName = "Mannschaft", .DataType = GetType(String)})
                dtAuswahl.Columns.Add(New DataColumn With {.ColumnName = "AK", .DataType = GetType(String)})
                For Each row As DataRow In dtAuswahl.Rows
                    ' AK, idVerein, ESR, MSR, Staat aktualisieren
                    If Not IsDBNull(row!Jahrgang) Then
                        Dim ak = Glob.Get_AK(row!Jahrgang)
                        row!idAK = ak.Key
                        row!AK = ak.Value
                    End If
                Next
            End If

            AthletenFilter = Get_AthletenFilter()
            dvA = New DataView(dtAuswahl, AthletenFilter, "Nachname ASC, Vorname ASC, Geburtstag DESC", DataViewRowState.CurrentRows)
            bsA = New BindingSource With {.DataSource = dvA}

            With dgvAuswahl
                .AutoGenerateColumns = False
                .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
                .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor
                .DataSource = bsA
            End With
        Catch ex As MySqlException
            Using New Centered_MessageBox(Me)
                MessageBox.Show(ex.Message, msgCaption)
            End Using
        End Try
        Cursor = Cursors.Default
    End Sub

    Private Function Get_AthletenFilter() As String
        'Dim Ids = (From x In dtMeldung Select CStr(x.Field(Of Integer)("Teilnehmer"))).ToArray
        Dim Ids = (From x In dv.ToTable Select CStr(x.Field(Of Integer)("Teilnehmer"))).ToArray
        If Ids.Length = 0 Then Return String.Empty
        Return "Teilnehmer NOT IN (" & Join(Ids, ",") & ")"
    End Function

    Private Sub Write_GK()
        If Not IsNothing(dgvJoin.CurrentRow) Then
            Cursor = Cursors.WaitCursor
            Try
                If IsNumeric(txtGewicht.Text) Then
                    Dim GK = Glob.Get_GK(dv(bs.Position)!sex.ToString, CInt(dv(bs.Position)!idAK), CDbl(txtGewicht.Text))
                    dv(bs.Position)!GK = GK.Value
                    dv(bs.Position)!idGK = GK.Key
                    bs.EndEdit()
                    cboGK.Text = GK.Value.ToString
                ElseIf Not IsDBNull(dv(bs.Position)!GK) Then
                    cboGK.Text = dv(bs.Position)!GK.ToString
                    dv(bs.Position)!idGK = cboGK.SelectedValue
                Else
                    cboGK.SelectedIndex = -1
                End If
            Catch ex As Exception
            End Try
            Cursor = Cursors.Default
        End If
    End Sub
    Private Sub Fill_GK()
        Try
            ' wenn TN WK beendet hat --> keine Auswahl mehr möglich --> cboGK.Enabled = False -------------> Status in Meldung ???

            Dim sex = dv(bs.Position)!sex.ToString
            Dim idAK = CInt(dv(bs.Position)!idAK)
            If idAK > 100 Then idAK = 100
            Glob.dvGK.RowFilter = "idAK = " & idAK & " AND Sex = '" & sex & "'"
            With cboGK
                .DataSource = Glob.dvGK
                .ValueMember = "idGK"
                .DisplayMember = "GK"
                If Not IsDBNull(dv(bs.Position)!idGK) Then
                    .Text = dv(bs.Position)!GK.ToString
                Else
                    .SelectedIndex = -1
                End If
            End With
        Catch ex As Exception
        End Try
    End Sub
    Private Sub bs_PositionChanged(sender As Object, e As EventArgs) Handles bs.PositionChanged

        If loading Then Return
        'Set_Team()

        Cursor = Cursors.WaitCursor

        If pnlMeldedatum.Visible Then
            chkMeldedatum.Checked = Not IsDBNull(dv(bs.Position)!Meldedatum)
        End If

        'Try
        '    With dpkDatum
        '        If IsDBNull(dv(bs.Position)!Meldedatum) Then
        '            .Format = DateTimePickerFormat.Custom
        '            .CustomFormat = " "
        '            'chkDatumMerken.Checked = False
        '        Else
        '            .Format = DateTimePickerFormat.Short
        '            .Value = CDate(dv(bs.Position)!Meldedatum)
        '        End If
        '    End With
        'Catch ex As Exception
        'End Try

        Try
            'If IsDBNull(dv(bs.Position)!idAK) AndAlso Not IsDBNull(dv(bs.Position)!Jahrgang) Then
            '    Dim ak = Glob.Get_AK(dv(bs.Position)!Jahrgang)
            '    dv(bs.Position)!idAK = ak.Key
            '    dv(bs.Position)!AK = ak.Value
            '    bs.EndEdit()
            'End If
            loading = True
            Dim ak = Split(dv(bs.Position)!AK.ToString, " ")(0)
            cboAK.Text = ak
            loading = False


            If IsNumeric(txtGewicht.Text) Then
                If IsDBNull(dv(bs.Position)!idGK) Then
                    Dim gk = Glob.Get_GK(dv(bs.Position)!sex.ToString, CInt(dv(bs.Position)!idAK), CDbl(txtGewicht.Text))
                    dv(bs.Position)!idGK = gk.Key
                    dv(bs.Position)!GK = gk.Value
                    bs.EndEdit()
                End If
            End If

            Fill_GK()

            mnuHeber.Enabled = Not CBool(dv(bs.Position)!IsDirty)
            mnuVerein.Enabled = Not CBool(dv(bs.Position)!IsDirty)

        Catch ex As Exception
        End Try

        Cursor = Cursors.Default
    End Sub

    '' Build_Grid
    Private Sub Build_Grid(Bereich As String, grid As DataGridView)
        With grid
            .AutoGenerateColumns = False
            .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
            .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor

            If Bereich = "Meldung" Then

                Dim ColWidth As New List(Of Integer)
                Dim ColBez As New List(Of String)

                If Not IsNothing(Wettkampf.Identities) AndAlso Wettkampf.Identities.Count > 0 Then
                    Dim WKs As New List(Of Integer)
                    For Each key As String In Wettkampf.Identities.Keys
                        WKs.Add(CInt(key))
                    Next
                    WKs.Sort()
                    For Each WK In WKs
                        ColWidth.Add(35)
                        ColBez.Add(WK.ToString)
                        .Columns.Add(New DataGridViewCheckBoxColumn With {.Name = "WK_" & WK, .DataPropertyName = "WK_" & WK, .SortMode = DataGridViewColumnSortMode.Automatic, .ToolTipText = Wettkampf.Identities(WK.ToString)})
                    Next
                End If

                ColWidth.AddRange({30, 35, 35, 100, 100, 35, 55, 100, 100, 100, 80, 70, 55, 55, 55, 55, 55, 35, 35, 35, 35})
                ColBez.AddRange({"Nr.", "...", "Los", "Nachname", "Vorname", "m/w", "JG", "Verein", "Nation", "Mannschaft", "Meldung", "AK", "Gewicht", "GK", "Reißen", "Stoßen", "Zweik.", "WK", "L.W.", "V.M.", "a.K."})

                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Teilnehmer", .DataPropertyName = "Teilnehmer", .Visible = False})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Sortierung", .DataPropertyName = "Sortierung", .ToolTipText = "Sortierung nach Meldung"}) ', .Visible = False
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Losnummer", .DataPropertyName = "Losnummer"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .DataPropertyName = "Nachname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .DataPropertyName = "Vorname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Sex", .DataPropertyName = "Sex"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Jahrgang", .DataPropertyName = "Jahrgang"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "ESR", .DataPropertyName = "Verein", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill, .Visible = Not (Wettkampf.Mannschaft Or Wettkampf.International)})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Staat", .DataPropertyName = "state_name", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill, .Visible = Wettkampf.International})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "MSR", .ToolTipText = "Mannschaft", .DataPropertyName = "Mannschaft", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill, .Visible = Wettkampf.Mannschaft})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Datum", .ToolTipText = "Datum der Meldung", .DataPropertyName = "Meldedatum"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "AK", .ToolTipText = "Altersklasse", .DataPropertyName = "AK"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Gewicht", .ToolTipText = "gemeldetes Gewicht", .DataPropertyName = "Gewicht"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "GK", .ToolTipText = "Gewichtsklasse", .DataPropertyName = "GK"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Reißen", .DataPropertyName = "Reissen"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Stoßen", .DataPropertyName = "Stossen"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Zweikampf", .ToolTipText = "Zweikampf-Leistung", .DataPropertyName = "Zweikampf"})
                .Columns.Add(New DataGridViewCheckBoxColumn With {.Name = "W_K", .ToolTipText = "Teilnahme am Wettkampf", .DataPropertyName = "attend", .Visible = False})
                .Columns.Add(New DataGridViewCheckBoxColumn With {.Name = "L_W", .ToolTipText = "Länder-Wertung", .DataPropertyName = "Laenderwertung", .SortMode = DataGridViewColumnSortMode.Automatic, .Visible = False})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vereinswertung", .ToolTipText = "Vereins-Mannschaft", .DataPropertyName = "Vereinswertung", .Visible = False})
                .Columns.Add(New DataGridViewCheckBoxColumn With {.Name = "A_K", .ToolTipText = "außer Konkurrenz", .DataPropertyName = "a_K", .SortMode = DataGridViewColumnSortMode.Automatic})

                For i As Integer = 0 To ColBez.Count - 1
                    .Columns(i).FillWeight = ColWidth(i)
                    .Columns(i).HeaderText = ColBez(i)
                    .Columns(i).ReadOnly = Not .Columns(i).Name.Contains("WK_")
                Next

                .Columns("Losnummer").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Sortierung").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Sex").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Jahrgang").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Datum").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Datum").DefaultCellStyle.Format = "dd.MM.yyyy"
                .Columns("Gewicht").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Gewicht").DefaultCellStyle.Format = "##0.00"
                .Columns("GK").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Zweikampf").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Reißen").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Stoßen").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Vereinswertung").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            ElseIf Bereich = "Meldung_Auswahl" Then

                Dim ColWidth() As Integer = {24, 40, 100, 100, 35, 50, 100, 100, 60}
                Dim ColBez() As String = {"", "Nr.", "Nachname", "Vorname", "m/w", "JG", "Verein", "Nation", "AK"}
                .Columns.Add(New DataGridViewCheckBoxColumn With {.Name = "Übernehmen", .DataPropertyName = "Übernehmen"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Teilnehmer", .DataPropertyName = "Teilnehmer", .Visible = False})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .DataPropertyName = "Nachname"}) ', .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .DataPropertyName = "Vorname"}) ', .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Sex", .DataPropertyName = "Sex"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Jahrgang", .DataPropertyName = "Jahrgang"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Verein", .DataPropertyName = "Verein", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill, .Visible = Not Wettkampf.International})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nation", .DataPropertyName = "state_name", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill, .Visible = Wettkampf.International})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "AK", .DataPropertyName = "AK"})

                For i As Integer = 0 To ColWidth.Count - 1
                    '.Columns(i).FillWeight = ColWidth(i)
                    .Columns(i).HeaderText = ColBez(i)
                    If .Columns(i).Name <> "Übernehmen" Then .Columns(i).ReadOnly = True
                Next

                .Columns("Sex").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Jahrgang").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            ElseIf Bereich = "Vereinsmeisterschaft" Then

                Dim ColWidth() As Integer = {24, 40, 120, 100, 100}
                Dim ColBez() As String = {"", "Nr.", "Verein", "Bundesland", "Landesverband"}
                .Columns.Add(New DataGridViewCheckBoxColumn With {.Name = "Übernehmen", .DataPropertyName = "Übernehmen", .Width = 24})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Verein", .DataPropertyName = "idVerein", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vereinsname", .DataPropertyName = "Vereinsname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Land", .DataPropertyName = "region_Name", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Verband", .DataPropertyName = "bezeichnung", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})

                For i As Integer = 0 To ColWidth.Count - 1
                    .Columns(i).FillWeight = ColWidth(i)
                    .Columns(i).HeaderText = ColBez(i)
                    If .Columns(i).Name <> "Übernehmen" Then .Columns(i).ReadOnly = True
                Next

                .Columns("Verein").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            End If

            Format_Grid(Bereich, grid)
            If Bereich = "Meldung" Then LayoutChanged_M = False
            If Bereich = "Meldung_Auswahl" Then LayoutChanged_A = False
        End With
    End Sub

    '' dgvAuswahl
    Private Sub dgvAuswahl_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAuswahl.CellClick
        If e.RowIndex = -1 OrElse Not dgvAuswahl.Columns(e.ColumnIndex).Name.Equals("Übernehmen") Then Return
        With dgvAuswahl.CurrentRow.Cells("Übernehmen")
            .Value = Not CBool(.Value)
        End With
        dgvAuswahl.Refresh()
    End Sub
    Private Sub dgvAuswahl_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAuswahl.CellEndEdit
        'If e.ColumnIndex = 0 AndAlso IsDBNull(dgvAuswahl.CurrentCell.Value) Then
        '    dgvAuswahl.CurrentCell.Value = False
        'End If
    End Sub
    Private Sub dgvAuswahl_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAuswahl.CellValueChanged
        If e.RowIndex = -1 OrElse IsNothing(dgvAuswahl.CurrentRow) Then Return

        If dgvAuswahl.Columns(e.ColumnIndex).Name = "Übernehmen" AndAlso CellInEdit Then
            CellInEdit = False
            dgvAuswahl.EndEdit()
            If IsDBNull(dgvAuswahl.Rows(e.RowIndex).Cells("Übernehmen").Value) Then
                dgvAuswahl.Rows(e.RowIndex).Cells("Übernehmen").Value = False
            End If
            If CBool(dgvAuswahl.Rows(e.RowIndex).Cells("Übernehmen").Value) = False Then
                If CheckedRows > 0 Then CheckedRows -= 1
            Else
                CheckedRows += 1

                'If CBool(dgvAuswahl.Rows(e.RowIndex).Cells("Übernehmen").Value) = True Then
                '    CheckedRows += 1
                'ElseIf CheckedRows > 0 Then
                '    CheckedRows -= 1
                'End If
            End If
        End If
        CellInEdit = True
    End Sub
    Private Sub dgvAuswahl_CurrentCellDirtyStateChanged(sender As Object, e As EventArgs) Handles dgvAuswahl.CurrentCellDirtyStateChanged
        'With dgvAuswahl
        '    If .CurrentCell.ColumnIndex = 0 AndAlso CellInEdit Then
        '        CellInEdit = False
        '        '.EndEdit()
        '        If Not IsDBNull(.CurrentCell.Value) AndAlso CBool(.CurrentCell.Value) = True Then
        '            CheckedRows += 1
        '        ElseIf CheckedRows > 0 Then
        '            CheckedRows -= 1
        '        End If
        '    End If
        'End With
        'CellInEdit = True
    End Sub
    Private Sub dgvAuswahl_DoubleClick(sender As Object, e As EventArgs) Handles dgvAuswahl.DoubleClick
        With dgvAuswahl.CurrentRow.Cells("Übernehmen")
            .Value = Not CBool(.Value)
        End With
        dgvAuswahl.Refresh()
    End Sub
    Private Sub dgvAuswahl_GotFocus(sender As Object, e As EventArgs) Handles dgvAuswahl.GotFocus
        'Try
        '    dpkDatum.DataBindings.RemoveAt(0)
        'Catch ex As Exception
        'End Try
    End Sub
    Private Sub dgvAuswahl_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvAuswahl.KeyDown
        Select Case e.KeyCode
            Case Keys.Space
                dgvAuswahl(2, dgvAuswahl.CurrentRow.Index).Selected = True
                With dgvAuswahl.CurrentRow.Cells("Übernehmen")
                    .Value = Not CBool(.Value)
                End With
                dgvAuswahl.Refresh()
            Case Keys.Escape
                txtAthlet_Name.Text = String.Empty
                txtAthlet_Verein.Text = String.Empty
        End Select
    End Sub

    '' dgvJoin
    Private Sub dgvJoin_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvJoin.CellMouseDown
        If e.Button.Equals(MouseButtons.Right) Then
            dgvJoin.ClearSelection()
            bs.Position = e.RowIndex
            dgvJoin.Rows(bs.Position).Selected = True
        ElseIf e.Button.Equals(MouseButtons.Left) AndAlso e.RowIndex > -1 Then
            If dgvJoin.Columns(e.ColumnIndex).Name.StartsWith("WK_") Then
                '        Dim checked = False
                '        For Each key As String In Wettkampf.Identities.Keys
                '            If Not IsDBNull(dgvJoin.Rows(e.RowIndex).Cells("WK_" & key).Value) AndAlso CBool(dgvJoin.Rows(e.RowIndex).Cells("WK_" & key).Value) = True Then
                '                checked = True
                '                Exit For
                '            End If
                '        Next
                '        With dgvJoin.Rows(e.RowIndex)
                '            .DefaultCellStyle.ForeColor = If(checked, dgvJoin.DefaultCellStyle.ForeColor, Color.Red)
                '            For Each key As String In Wettkampf.Identities.Keys
                '                .Cells("WK_" & key).Style.BackColor = If(checked, If(e.RowIndex Mod 2 = 0, RowsDefaultCellStyle_BackColor, AlternatingRowsDefaultCellStyle_BackColor), Color.Red)
                '            Next
                '        End With
                mnuSpeichern.Enabled = True
            ElseIf dgvJoin.Columns(e.ColumnIndex).Name.Equals("MSR") Then

            End If
        End If
    End Sub
    Private Sub dgvJoin_GotFocus(sender As Object, e As EventArgs) Handles dgvJoin.GotFocus
        'dgvJoin.MultiSelect = True
    End Sub
    Private Sub dgvJoin_LostFocus(sender As Object, e As EventArgs) Handles dgvJoin.LostFocus
        'dgvJoin.MultiSelect = False
        'If bs.Position > -1 Then dgvJoin.Rows(bs.Position).Selected = True
    End Sub
    Private Sub dgvJoin_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvJoin.RowLeave
        If loading Then Return
        Try
            ' Datum zuweisen
            'If chkDatumMerken.Checked Then
            '    dv(e.RowIndex)!Meldedatum = chkDatumMerken.Tag
            'ElseIf dpkDatum.Format = DateTimePickerFormat.Short Then
            '    dv(e.RowIndex)!Meldedatum = dpkDatum.Value
            'Else
            '    dv(e.RowIndex)!Meldedatum = DBNull.Value
            'End If

            ' idAK
            If IsDBNull(dv(e.RowIndex)!idAK) AndAlso Not IsDBNull(dv(e.RowIndex)!Jahrgang) Then
                Dim ak = Glob.Get_AK(dv(e.RowIndex)!Jahrgang)
                dv(e.RowIndex)!idAK = ak.Key
                dv(e.RowIndex)!AK = ak.Value
            End If

            ' idGK
            Write_GK()

            ' ZKL
            If IsDBNull(dv(e.RowIndex)!Zweikampf) AndAlso Not IsDBNull(dv(e.RowIndex)!Reissen) AndAlso Not IsDBNull(dv(e.RowIndex)!Stossen) Then
                mnuSpeichern.Enabled = True
            End If
            If Not IsDBNull(dv(e.RowIndex)!Reissen) AndAlso Not IsDBNull(dv(e.RowIndex)!Stossen) Then
                dv(e.RowIndex)!Zweikampf = CInt(dv(e.RowIndex)!Reissen) + CInt(dv(e.RowIndex)!Stossen)
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvJoin_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles dgvJoin.RowsAdded
        mnuLosnummer.Enabled = dgvJoin.Rows.Count > 0
    End Sub
    Private Sub dgvJoin_RowsRemoved(sender As Object, e As DataGridViewRowsRemovedEventArgs) Handles dgvJoin.RowsRemoved
        mnuLosnummer.Enabled = dgvJoin.Rows.Count > 0
    End Sub

    '' Menü
    Private Sub mnuNeu_Click(sender As Object, e As EventArgs) Handles mnuNeu.Click, cmnuNeu.Click
        Try
            Using formAthleten As New frmAthleten
                With formAthleten
                    .Tag = "New"
                    .ShowDialog(Me)
                    If Not .DialogResult = DialogResult.Cancel Then
                        Cursor = Cursors.WaitCursor
                        'Dim pos = dgvJoin.Rows.Count

                        Dim row = dtMeldung.NewRow
                        With .Athlet
                            Dim verein = dtVerein.Select("idVerein = " & .idVerein)
                            Dim msr = dtVerein.Select("idVerein = " & .MSR)
                            Dim ak = Glob.Get_AK(.Jahrgang)

                            row.ItemArray = { .Id,
                                             .Geschlecht,
                                             .Nachname,
                                             .Vorname,
                                             .Geburtstag,
                                             .Jahrgang,
                                             .state_id,
                                             .state_name,
                                             .ESR,
                                             If(verein.Length > 0, verein(0)!Vereinsname, DBNull.Value),
                                             .MSR,
                                             If(msr.Length > 0, msr(0)!Vereinsname, DBNull.Value),
                                             Wettkampf.ID,
                                             Get_LastIndex() + 1,   ' Sortierung
                                             DBNull.Value,          ' Losnummer
                                             False,                 ' Laenderwertung
                                             DBNull.Value,          ' Vereinswertung --> sonst wird eine 0 im dgv angezeigt
                                             DBNull.Value,          ' Relativ_W
                                             False,                 ' a_K
                                             ak.Key,                ' idAK
                                             ak.Value,              ' AK
                                             DBNull.Value,          ' Gewicht
                                             DBNull.Value,          ' Wiegen
                                             DBNull.Value,          ' Startnummer
                                             DBNull.Value,          ' idGK
                                             DBNull.Value,          ' GK
                                             IIf(chkDatumMerken.Checked, CDate(txtMeldedatum.Text), DBNull.Value),
                                             DBNull.Value,          ' Zweikampf
                                             DBNull.Value,          ' Reissen
                                             DBNull.Value,          ' Stossen
                                             DBNull.Value,          ' attend
                                             .MSR,                  ' Team
                                             0,                     ' IsDirty
                                             True}                  ' WK#
                        End With

                        dtMeldung.Rows.Add(row)

                        If Not IsNothing(Wettkampf.Identities) Then
                            Dim pos = bs.Find("Teilnehmer", row!Teilnehmer)
                            'If Wettkampf.Identities.Count = 1 Then
                            dv(pos)("WK_" & Wettkampf.Identities.Keys(0)) = True
                            'ElseIf Wettkampf.Identities.Count > 1 Then
                            '    With dgvJoin.Rows(pos)
                            '        .DefaultCellStyle.ForeColor = Color.Red
                            '        For Each key As String In Wettkampf.Identities.Keys
                            '            .Cells("WK_" & key).Style.BackColor = Color.Red
                            '        Next
                            '    End With
                            'End If
                        End If

                        .Dispose()
                    End If
                End With
            End Using

            mnuSpeichern.Enabled = True

            Try
                loading = True
                bs.MoveLast()
                loading = False
                If bs.Count > 0 Then
                    With dgvJoin
                        .FirstDisplayedScrollingRowIndex = bs.Position
                        .Rows(bs.Position).Selected = True
                        .Focus()
                    End With
                    Fill_GK()
                End If
            Catch ex As Exception
            End Try

            'txtAthlet_Name.Text = String.Empty
            'txtAthlet_Verein.Text = String.Empty

            AthletenFilter = Get_AthletenFilter()
            Set_Filter(txtAthlet_Name)

            lblAnzahl.Text = "TN:  " & dv.Count
            Cursor = Cursors.Default
        Catch ex As Exception
            LogMessage("Meldung: mnuNeu: " & ex.Message, ex.StackTrace)
        End Try
    End Sub
    Private Sub mnuHeber_Click(sender As Object, e As EventArgs) Handles mnuHeber.Click, cmnuHeber.Click
        Try
            Using formAthleten As New frmAthleten
                With formAthleten
                    Dim pos = bs.Position
                    .Tag = dv(pos)!Teilnehmer
                    .ShowDialog(Me)
                    If Not .DialogResult = DialogResult.Cancel Then
                        Cursor = Cursors.WaitCursor

                        With .Athlet
                            Dim VereinRow = dtVerein.Select("idVerein = " & .idVerein)
                            Dim msr = dtVerein.Select("idVerein = " & .MSR)
                            Dim ak = Glob.Get_AK(.Jahrgang)

                            dv(pos)!Sex = .Geschlecht
                            dv(pos)!Nachname = .Nachname
                            dv(pos)!Vorname = .Vorname
                            dv(pos)!Geburtstag = .Geburtstag
                            dv(pos)!Jahrgang = .Jahrgang
                            dv(pos)!Staat = .state_id
                            dv(pos)!state_name = .state_name
                            dv(pos)!ESR = .ESR
                            dv(pos)!MSR = .MSR
                            dv(pos)!Team = .MSR
                            dv(pos)!Verein = VereinRow(0)!Vereinsname
                            dv(pos)!Mannschaft = msr(0)!Vereinsname
                            dv(pos)!idAK = ak.Key
                            dv(pos)!AK = ak.Value
                        End With

                        bs.EndEdit()
                    End If
                    .Dispose()
                End With
            End Using
            mnuSpeichern.Enabled = True
        Catch ex As Exception
            LogMessage("Meldung: mnuHeber: " & ex.Message, ex.StackTrace)
        End Try
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuVerein_Click(sender As Object, e As EventArgs) Handles mnuVerein.Click, cmnuVerein.Click

        If IsDBNull(dv(bs.Position)!ESR) OrElse CInt(dv(bs.Position)!ESR) = 0 Then Return
        Dim idVerein = CInt(dv(bs.Position)!ESR)

        Using formVerein As New frmVerein
            With formVerein
                .Tag = idVerein
                .ShowDialog(Me)
                If Not .DialogResult = DialogResult.Cancel Then
                    Dim rows = dtMeldung.Select("ESR = " & idVerein)
                    For Each r As DataRow In rows
                        r!Verein = .Verein.Vereinsname
                        'r!MSR = .Verein.MSR
                        'r!Mannschaft = .Verein.Vereinsname
                    Next
                    rows = dtAuswahl.Select("MSR = " & idVerein)
                    For Each r As DataRow In rows
                        'r!Verein = .Verein.Vereinsname
                        'r!MSR = .Verein.MSR
                        r!Mannschaft = .Verein.Vereinsname
                    Next
                    dtAuswahl.AcceptChanges()
                    rows = dtVerein.Select("idVerein = " & idVerein)
                    For Each r As DataRow In rows
                        r!Vereinsname = .Verein.Vereinsname
                        r!Region = .Verein.Region
                        r!Staat = .Verein.Staat
                        r!Adresse = .Verein.Adresse
                        r!PLZ = .Verein.PLZ
                        r!Ort = .Verein.Ort
                    Next
                    dtVerein.AcceptChanges()
                    dv(bs.Position)!Verein = .Verein.Vereinsname
                    bs.EndEdit()
                End If
            End With
        End Using
    End Sub
    Private Sub mnuLöschen_Click(sender As Object, e As EventArgs) Handles mnuLöschen.Click, cmnuLöschen.Click
        loading = True
        Dim row = dv(bs.Position)
        Dim athlet = dtAthleten.Select("idTeilnehmer = " & row!Teilnehmer.ToString)
        If athlet.Length > 0 AndAlso athlet(0).RowState = DataRowState.Added Then
            dtAthleten.Rows.Remove(athlet(0))
        End If
        row.Delete()
        loading = False
        bs.EndEdit()

        If bs.Position > -1 Then dgvJoin.Rows(bs.Position).Selected = True

        AthletenFilter = Get_AthletenFilter()
        Set_Filter(txtAthlet_Name)

        lblAnzahl.Text = "TN: " & dv.Count
        mnuSpeichern.Enabled = True
    End Sub
    Private Sub mnuSpeichern_Click(sender As Object, e As EventArgs) Handles mnuSpeichern.Click, btnSpeichern.Click
        Cursor = Cursors.WaitCursor
        dgvJoin.EndEdit()
        Validate()
        bs.EndEdit()

        If Not IsNothing(Wettkampf.Identities) AndAlso Wettkampf.Identities.Count > 0 Then
            For i = 0 To dgvJoin.Rows.Count - 1
                Dim checked = False
                For Each key As String In Wettkampf.Identities.Keys
                    If Not IsDBNull(dgvJoin.Rows(i).Cells("WK_" & key).Value) AndAlso CBool(dgvJoin.Rows(i).Cells("WK_" & key).Value) = True Then
                        checked = True
                        Exit For
                    End If
                Next
                If Not checked Then
                    Using New Centered_MessageBox(Me)
                        If MessageBox.Show("Alle Teilnehmer ohne ausgewählte Wettkämpfe" & vbNewLine & "werden gelöscht.", msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) = DialogResult.Cancel Then
                            Cursor = Cursors.Default
                            Return
                        End If
                    End Using
                End If
            Next
        End If

        Dim changes As DataTable = dtAthleten.GetChanges()
        If Not IsNothing(changes) Then
            Query = "INSERT INTO athleten (idTeilnehmer, GUID, Nachname, Vorname, Verein, ESR, MSR, Jahrgang, Geburtstag, Geschlecht, Staat) 
                        VALUES (@tn, @id, @nn, @vn, @esr, @esr, @msr, @jg, @geb, @sex, @staat)
                     ON DUPLICATE KEY UPDATE GUID = @id, Nachname = @nn, Vorname = @vn, Verein = @esr, ESR = @ESR, MSR = @msr, Jahrgang = @jg, Geburtstag = @geb, Geschlecht = @sex, Staat = @staat;"
            Using conn As New MySqlConnection(User.ConnString)
                For Each row As DataRow In changes.Rows
                    'If row.RowState = DataRowState.Added Then
                    cmd = New MySqlCommand(Query, conn)
                    With cmd.Parameters
                        .AddWithValue("@tn", row!idTeilnehmer)
                        .AddWithValue("@id", row!GUID)
                        .AddWithValue("@nn", row!Nachname)
                        .AddWithValue("@vn", row!Vorname)
                        '.AddWithValue("@verein", row!ESR)
                        .AddWithValue("@esr", row!ESR)
                        .AddWithValue("@msr", row!MSR)
                        .AddWithValue("@jg", row!Jahrgang)
                        .AddWithValue("@geb", row!Geburtstag)
                        .AddWithValue("@sex", row!Geschlecht)
                        .AddWithValue("@staat", row!Staat)
                    End With
                    Do
                        Try
                            If conn.State = ConnectionState.Closed Then conn.Open()
                            cmd.ExecuteNonQuery()
                            ConnError = False
                        Catch ex As MySqlException
                            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                        End Try
                    Loop While ConnError
                    'End If
                Next
            End Using
            If Not ConnError Then dtAthleten.AcceptChanges()
        End If

        changes = dtMeldung.GetChanges()
        If Not IsNothing(changes) Then
            Dim DeleteQuery = "DELETE FROM meldung WHERE Teilnehmer = @heber AND Wettkampf = @wk;" &
                              "DELETE FROM reissen WHERE Teilnehmer = @heber AND Wettkampf = @wk;" &
                              "DELETE FROM stossen WHERE Teilnehmer = @heber AND Wettkampf = @wk;" &
                              "DELETE FROM athletik WHERE Teilnehmer = @heber AND Wettkampf = @wk;" &
                              "DELETE FROM athletik_results WHERE Teilnehmer = @heber AND Wettkampf = @wk;" &
                              "DELETE FROM results WHERE Teilnehmer = @heber AND Wettkampf = @wk;"

            Dim InsertQuery = "INSERT INTO meldung (Teilnehmer, Wettkampf, Sortierung, Losnummer, Startnummer, Gewicht, Meldedatum, Laenderwertung, Vereinswertung, Team, a_K, idAK, AK, idGK, GK, Zweikampf, Reissen, Stossen) " &
                              "VALUES (@heber, @wk, @sort, @los, @start, @gewicht, @datum, @l_w, @v_w, @team, @a_k, @idak, @ak, @idgk, @gk, @zk, @reissen, @stossen) " &
                              "ON DUPLICATE KEY UPDATE Sortierung = @sort, Losnummer = @los, Startnummer = @start, Gewicht = @gewicht, Meldedatum = @datum, Laenderwertung = @l_w, Vereinswertung = @v_w, " &
                              "Team = @team, a_K = @a_k, idAK = @idak, AK = @ak, idGK = @idgk, GK = @gk, Zweikampf = @zk, Reissen = @reissen, Stossen = @stossen;"

            Dim WKs_Row As New List(Of String)

            Try
                Using conn As New MySqlConnection(User.ConnString)
                    For Each row As DataRow In changes.Rows

                        WKs_Row.Clear()

                        For Each key As String In Wettkampf.Identities.Keys
                            If Not row.RowState = DataRowState.Deleted AndAlso Not IsDBNull(row("WK_" & key)) AndAlso CBool(row("WK_" & key)) = True Then
                                Query = InsertQuery
                                WKs_Row.Add(key)
                            Else
                                Query = DeleteQuery
                            End If
                            cmd = New MySqlCommand(Query, conn)
                            With cmd.Parameters
                                .AddWithValue("@wk", key)
                                If row.RowState = DataRowState.Deleted Then
                                    .AddWithValue("@heber", row("Teilnehmer", DataRowVersion.Original))
                                Else
                                    .AddWithValue("@heber", row!Teilnehmer)
                                    .AddWithValue("@sort", row!Sortierung)
                                    .AddWithValue("@los", row!Losnummer)
                                    .AddWithValue("@start", row!Startnummer)
                                    .AddWithValue("@gewicht", row!Gewicht)
                                    .AddWithValue("@datum", row!Meldedatum)
                                    .AddWithValue("@l_w", row!Laenderwertung)
                                    .AddWithValue("@v_w", row!Vereinswertung)
                                    .AddWithValue("@team", row!Team)
                                    .AddWithValue("@a_k", row!a_K)
                                    .AddWithValue("@idak", row!idAK)
                                    .AddWithValue("@ak", row!AK)
                                    .AddWithValue("@idgk", row!idGK)
                                    .AddWithValue("@gk", row!GK)
                                    .AddWithValue("@zk", row!Zweikampf)
                                    .AddWithValue("@reissen", row!Reissen)
                                    .AddWithValue("@stossen", row!Stossen)
                                End If
                            End With
                            Do
                                Try
                                    If conn.State = ConnectionState.Closed Then conn.Open()
                                    cmd.ExecuteNonQuery()
                                    ConnError = False
                                Catch ex As MySqlException
                                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                                End Try
                            Loop While ConnError
                        Next

                        If Not row.RowState = DataRowState.Deleted AndAlso Not ConnError Then
                            If WKs_Row.Count = 0 Then
                                Dim pos = bs.Find("Teilnehmer", row!Teilnehmer)
                                If pos > -1 Then
                                    loading = True
                                    bs.Position = pos
                                    mnuLöschen.PerformClick()
                                End If
                            Else
                                row!IsDirty = False ' mnuHeber and mnuVerein is available at RowEnter
                            End If
                            bs.EndEdit()
                        End If



                        ' ALT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                        'Query = String.Empty
                        'Dim WKsAdd As New List(Of String)
                        'Dim WKsDel As New List(Of String)

                        'If row.RowState = DataRowState.Deleted Then
                        '    ' aus allen WKs entfernen
                        '    Query = CompleteQuery(DeleteQuery)
                        'ElseIf Not IsNothing(Wettkampf.Identities) AndAlso Wettkampf.Identities.Count > 0 Then
                        '    For Each key As String In Wettkampf.Identities.Keys
                        '        If (IsDBNull(row("WK_" & key)) OrElse CBool(row("WK_" & key)) = False) Then
                        '            ' aus WK entfernen
                        '            If row.RowState = DataRowState.Added Then
                        '                ' hinzugefügt und noch nicht gespeichert
                        '                mnuLöschen.PerformClick()
                        '            Else
                        '                WKsDel.Add(key)
                        '            End If
                        '        ElseIf CBool(row("WK_" & key)) = True Then
                        '            ' zu WK hinzufügen oder aktualisieren
                        '            WKsAdd.Add(key)
                        '        End If
                        '    Next
                        'Else
                        '    WKsAdd.Add(Wettkampf.ID.ToString)
                        'End If

                        'For Each wk As String In WKsAdd
                        '    cmd = New MySqlCommand(InsertQuery, conn)
                        '    With cmd.Parameters
                        '        .AddWithValue("@wk", wk)
                        '        .AddWithValue("@heber", row!Teilnehmer)
                        '        .AddWithValue("@sort", row!Sortierung)
                        '        .AddWithValue("@los", row!Losnummer)
                        '        .AddWithValue("@start", row!Startnummer)
                        '        .AddWithValue("@gewicht", row!Gewicht)
                        '        .AddWithValue("@datum", row!Meldedatum)
                        '        .AddWithValue("@l_w", row!Laenderwertung)
                        '        .AddWithValue("@v_w", row!Vereinswertung)
                        '        .AddWithValue("@team", row!Team)
                        '        .AddWithValue("@a_k", row!a_K)
                        '        .AddWithValue("@idak", row!idAK)
                        '        .AddWithValue("@ak", row!AK)
                        '        .AddWithValue("@idgk", row!idGK)
                        '        .AddWithValue("@gk", row!GK)
                        '        .AddWithValue("@zk", row!Zweikampf)
                        '        .AddWithValue("@reissen", row!Reissen)
                        '        .AddWithValue("@stossen", row!Stossen)
                        '    End With
                        '    Do
                        '        Try
                        '            If conn.State = ConnectionState.Closed Then conn.Open()
                        '            cmd.ExecuteNonQuery()
                        '            ConnError = False
                        '        Catch ex As MySqlException
                        '            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                        '        End Try
                        '    Loop While ConnError
                        'Next

                        'row!IsDirty = False ' mnuHeber und mnuVerein freischalten 
                        'bs.EndEdit()

                        'If WKsDel.Count > 0 Then
                        '    Query = CompleteQuery(DeleteQuery, WKsDel.ToArray)
                        'End If
                        'If Not String.IsNullOrEmpty(Query) Then
                        '    cmd = New MySqlCommand(Query, conn)
                        '    cmd.Parameters.AddWithValue("@heber", row("Teilnehmer", DataRowVersion.Original))
                        '    Do
                        '        Try
                        '            If conn.State = ConnectionState.Closed Then conn.Open()
                        '            cmd.ExecuteNonQuery()
                        '            ConnError = False
                        '            If WKsDel.Count = Wettkampf.Identities.Count Then
                        '                Dim pos = bs.Find("Teilnehmer", row!Teilnehmer)
                        '                dv(pos).Delete()
                        '                bs.EndEdit()
                        '            End If
                        '        Catch ex As MySqlException
                        '            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                        '        End Try
                        '    Loop While ConnError
                        'End If

                        'If WKsAdd.Count > 0 Then
                        '    Dim pos = bs.Find("Teilnehmer", row!Teilnehmer)
                        '    If pos > -1 Then
                        '        dv(pos)!Wettkampf = String.Join(",", WKsAdd.ToArray)
                        '    Else
                        '        Stop
                        '    End If
                        '    bs.EndEdit()
                        'End If

                    Next
                End Using
            Catch ex As Exception
                ConnError = True
            End Try

            If Not ConnError Then
                dtMeldung.AcceptChanges()
                mnuSpeichern.Enabled = False
                If bs.Count > 0 Then
                    bs.MoveFirst()
                    With dgvJoin
                        .FirstDisplayedScrollingRowIndex = 0
                        .Rows(0).Selected = True
                        .Focus()
                    End With
                End If

                Using OS As New OnlineService
                    If Not AppConnState = ConnState.Connected Then
                        AppConnState = OS.Check_Wettkampf()
                    End If
                    If AppConnState = ConnState.Connected Then
                        Using conn As New MySqlConnection(User.APP_ConnString)
                            conn.Open()
                            OS.UpdateMeldung(conn)
                            OS.UpdateAthlet(, conn)
                        End Using
                    End If
                End Using

            End If
        End If
        dgvJoin.Focus()
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuSperren_Click(sender As Object, e As EventArgs) Handles mnuSperren.Click
        Lock_Application(Me)
    End Sub
    Private Sub mnuBeenden_Click(sender As Object, e As EventArgs) Handles mnuBeenden.Click, btnBeenden.Click
        Close()
    End Sub
    Private Sub mnuLayout_Click(sender As Object, e As EventArgs) Handles mnuLayout.Click
        Read_TableToLayout(dgvJoin, Bereich)
        frmTableLayout.Tag = Bereich
        If Not frmTableLayout.ShowDialog() = DialogResult.Cancel Then
            Format_Grid(Bereich, dgvJoin)
            LayoutChanged_M = False
        End If
        frmTableLayout.Dispose()
    End Sub
    Private Sub mnuLayoutA_Click(sender As Object, e As EventArgs) Handles mnuLayoutA.Click
        Read_TableToLayout(dgvAuswahl, "Meldung_Auswahl")
        frmTableLayout.Tag = "Meldung_Auswahl"
        If Not frmTableLayout.ShowDialog() = DialogResult.Cancel Then
            Format_Grid("Meldung_Auswahl", dgvAuswahl)
            LayoutChanged_A = False
        End If
        frmTableLayout.Dispose()
    End Sub
    Private Sub mnuMauszeiger_Click(sender As Object, e As EventArgs) Handles mnuMauszeiger.Click
        Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
        Cursor.Show()
    End Sub
    Private Sub mnuLosnummer_Click(sender As Object, e As EventArgs) Handles mnuLosnummer.Click

        If mnuSpeichern.Enabled Then
            Using New Centered_MessageBox(Me)
                If MessageBox.Show("Vor der Vergabe der Losnummern müssen die Daten gespeichert werden.", msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then Return
            End Using
            mnuSpeichern.PerformClick()
        End If

        Dim ReturnValue As New List(Of Integer)
        Dim msg As String
        Dim Exception = False

        Cursor = Cursors.WaitCursor

        Using F As New myFunction
            ReturnValue = F.Generate_Lotnumbers(dv.Count)
        End Using

        Cursor = Cursors.Default

        If IsNothing(ReturnValue) OrElse ReturnValue.Count = 0 Then
            msg = "Fehler bei der Erstellung der Losnummern"
        Else
            Try
                bs.MoveNext()
                For Each row As DataRowView In dv
                    Try
                        row!Losnummer = ReturnValue(0)
                        ReturnValue.RemoveAt(0)
                        row!Startnummer = DBNull.Value
                    Catch ex As Exception
                        Exception = True
                        Throw New Exception(ex.Message)
                    End Try
                Next
                bs.EndEdit()
                msg = "Losnummern erfolgreich aktualisiert"
                mnuSpeichern.Enabled = True
            Catch ex As Exception
                msg = "Fehler bei der Vergabe der Losnummern" & vbNewLine & "(Sortier-Spalte wechseln, aktuelle ist 'Los')"
            End Try
        End If

        If mnuSpeichern.Enabled Then mnuSpeichern.PerformClick()

        'If mnuNoMessage.Checked AndAlso Not Exception Then Return

        Using New Centered_MessageBox(Me)
            MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Using

    End Sub
    Private Sub mnuÜbernehmen_Click(sender As Object, e As EventArgs) Handles mnuÜbernehmen.Click
        'Dim wk As KeyValuePair(Of String, String)
        'Dim selectedRows As DataGridViewSelectedRowCollection = dgvJoin.SelectedRows

        'Using f As New myFunction
        '    wk = f.Wettkampf_Auswahl(Me, "Athleten übernehmen",, True)
        'End Using

        'If CInt(wk.Key) > 0 Then
        '    Dim result = Save(CInt(wk.Key), selectedRows)
        '    Using New Centered_MessageBox(Me)
        '        Dim icon As MessageBoxIcon
        '        Dim msg As String
        '        If IsNothing(result) OrElse result = False Then
        '            icon = MessageBoxIcon.Information
        '            msg = "Altleten in " & wk.Value & " übernommen."
        '        Else
        '            icon = MessageBoxIcon.Error
        '            msg = "Übernahme in " & wk.Value & " fehlgeschlagen."
        '        End If
        '        MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, icon)
        '    End Using
        'End If
    End Sub
    Private Sub mnuIDs_Click(sender As Object, e As EventArgs) Handles mnuIDs.Click
        mnuIDs.Checked = Not mnuIDs.Checked
        dgvJoin.Columns("Teilnehmer").Visible = mnuIDs.Checked
        dgvAuswahl.Columns("Teilnehmer").Visible = mnuIDs.Checked
    End Sub

    Private Sub mnu_EnabledChanged(sender As Object, e As EventArgs) Handles mnuNeu.EnabledChanged, mnuHeber.EnabledChanged, mnuVerein.EnabledChanged,
                                                                              mnuSpeichern.EnabledChanged, mnuLöschen.EnabledChanged
        cmnuNeu.Enabled = mnuNeu.Enabled
        cmnuHeber.Enabled = mnuHeber.Enabled
        cmnuVerein.Enabled = mnuVerein.Enabled
        cmnuLöschen.Enabled = mnuLöschen.Enabled
        btnSpeichern.Enabled = mnuSpeichern.Enabled
        mnuÜbernehmen.Enabled = Not mnuSpeichern.Enabled
    End Sub

    Private Function CompleteQuery(query As String, Optional WKs As Object() = Nothing) As String
        'Private Function CompleteQuery(query As String, Optional WKs As List(Of String) = Nothing) As String
        Dim strings = query.Split(CChar(";"))
        Dim where = Wettkampf.WhereClause(, WKs)
        Dim result As New List(Of String)
        For i = 0 To strings.Count - 1
            result.Add(strings(i).Replace("@wk", where))
        Next
        Return String.Join("; ", result)
    End Function

    Private Function Save(WK_ID As Integer, data As DataGridViewSelectedRowCollection) As Boolean?
        Cursor = Cursors.WaitCursor
        'Validate()
        'bs.EndEdit()
        'dgvJoin.EndEdit()
        Dim Query = "INSERT INTO meldung (Teilnehmer, Wettkampf, Losnummer, Startnummer, Gewicht, Meldedatum, Laenderwertung, Vereinswertung, Team, a_K, idAK, AK, idGK, GK, Zweikampf, Reissen, Stossen) " &
                    "VALUES (@heber, @wk, @los, @start, @gewicht, @datum, @l_w, @v_w, @team, @a_k, @idak, @ak, @idgk, @gk, @zk, @reissen, @stossen) " &
                    "ON DUPLICATE KEY UPDATE Losnummer = @los, Startnummer = @start, Gewicht = @gewicht, Meldedatum = @datum, Laenderwertung = @l_w, Vereinswertung = @v_w, " &
                    "Team = @team, a_K = @a_k, idAK = @idak, AK = @ak, idGK = @idgk, GK = @gk, Zweikampf = @zk, Reissen = @reissen, Stossen = @stossen;"
        Try
            Using conn As New MySqlConnection(User.ConnString)
                For Each selrow As DataGridViewRow In data
                    Dim row = dv(selrow.Index).Row
                    If conn.State = ConnectionState.Closed Then conn.Open()
                    cmd = New MySqlCommand(Query, conn)
                    With cmd.Parameters
                        .AddWithValue("@wk", WK_ID)
                        .AddWithValue("@heber", row!Teilnehmer)
                        .AddWithValue("@los", row!Losnummer)
                        .AddWithValue("@start", row!Startnummer)
                        .AddWithValue("@gewicht", row!Gewicht)
                        .AddWithValue("@datum", row!Meldedatum)
                        .AddWithValue("@l_w", row!Laenderwertung)
                        .AddWithValue("@v_w", row!Vereinswertung)
                        .AddWithValue("@team", row!TEAM)
                        .AddWithValue("@a_k", row!a_K)
                        .AddWithValue("@idak", row!idAK)
                        .AddWithValue("@ak", row!AK)
                        .AddWithValue("@idgk", row!idGK)
                        .AddWithValue("@gk", row!GK)
                        .AddWithValue("@zk", row!Zweikampf)
                        .AddWithValue("@reissen", row!Reissen)
                        .AddWithValue("@stossen", row!Stossen)
                    End With
                    Do
                        Try
                            cmd.ExecuteNonQuery()
                            ConnError = False
                        Catch ex As MySqlException
                            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                        End Try
                    Loop While ConnError
                Next
            End Using
        Catch ex As Exception
        End Try
        dgvJoin.Focus()
        Cursor = Cursors.Default
        Return ConnError
    End Function

    '' Filter
    Private Sub btnFilter_Click(sender As Object, e As EventArgs) Handles btnMeldung_Name.Click, btnMeldung_Verein.Click, btnAthlet_Name.Click, btnAthlet_Verein.Click
        With CType(sender, Button)
            Select Case .Name
                Case "btnMeldung_Name"
                    With txtMeldung_Name
                        If .ForeColor <> Color.DarkGray Then .Text = ""
                        .Focus()
                    End With
                Case "btnMeldung_Verein"
                    With txtMeldung_Verein
                        If .ForeColor <> Color.DarkGray Then .Text = ""
                        .Focus()
                    End With
                Case "btnAthlet_Name"
                    With txtAthlet_Name
                        If .ForeColor <> Color.DarkGray Then .Text = ""
                        .Focus()
                    End With
                Case "btnAthlet_Verein"
                    With txtAthlet_Verein
                        If .ForeColor <> Color.DarkGray Then .Text = ""
                        .Focus()
                    End With
            End Select
            'If .Name.Contains("btnAthlet") Then chkAlle_Athlet.Checked = False
            'If .Name.Contains("btnVerein") Then chkAlle_Verein.Checked = False
        End With
    End Sub
    Private Sub txtFilter_GotFocus(sender As Object, e As EventArgs) Handles txtMeldung_Name.GotFocus, txtMeldung_Verein.GotFocus, txtAthlet_Name.GotFocus, txtAthlet_Verein.GotFocus,
                                                                             txtMeldung_Name.Click, txtMeldung_Verein.Click, txtAthlet_Name.Click, txtAthlet_Verein.Click
        With CType(sender, TextBox)
            If .ForeColor = Color.DarkGray Then
                .SelectionLength = 0
                .SelectionStart = 0
            End If
        End With
    End Sub
    Private Sub txtFilter_KeyDown(sender As Object, e As KeyEventArgs) Handles txtMeldung_Name.KeyDown, txtMeldung_Verein.KeyDown, txtAthlet_Name.KeyDown, txtAthlet_Verein.KeyDown
        With CType(sender, TextBox)
            Select Case e.KeyCode
                Case Keys.Enter, Keys.Escape
                    e.Handled = True
                    If e.KeyCode.Equals(Keys.Escape) AndAlso .ForeColor <> Color.DarkGray Then .Text = ""
                    If .Name.Contains("Meldung") Then
                        dgvJoin.Focus()
                    ElseIf .Name.Contains("Athlet") Then
                        dgvAuswahl.Focus()
                    End If
            End Select
        End With
    End Sub
    Private Sub txtFilter_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMeldung_Name.KeyPress, txtMeldung_Verein.KeyPress, txtAthlet_Name.KeyPress, txtAthlet_Verein.KeyPress
        With CType(sender, TextBox)
            Select Case e.KeyChar
                'Case CChar(vbBack)
                Case <> CChar(vbBack) 'Else
                    If .ForeColor = Color.DarkGray Then
                        e.Handled = True
                        If Asc(e.KeyChar) <> 27 Then
                            .Text = e.KeyChar
                            .ForeColor = Color.FromKnownColor(KnownColor.WindowText)
                        End If
                        .SelectionStart = .Text.Length
                    End If
            End Select
        End With
    End Sub
    Private Sub txtFilter_TextChanged(sender As Object, e As EventArgs) Handles txtMeldung_Name.TextChanged, txtMeldung_Verein.TextChanged, txtAthlet_Name.TextChanged, txtAthlet_Verein.TextChanged

        Dim filters As New List(Of String)

        With CType(sender, TextBox)

            If .Text = String.Empty Then
                .ForeColor = Color.DarkGray
                .Text = .Tag.ToString ' Vorgabe bei inaktiver Box
            End If

            Set_Filter(sender)
            'Select Case .Name
            '    Case "txtMeldung_Name", "txtMeldung_Verein"

            '        If txtMeldung_Name.Text.Length > 0 And txtMeldung_Name.ForeColor <> Color.DarkGray Then
            '            filters.Add("Nachname LIKE '*" & .Text & "*'")
            '        End If
            '        If txtMeldung_Verein.Text.Length > 0 And txtMeldung_Verein.ForeColor <> Color.DarkGray Then
            '            filters.Add("Verein LIKE '*" & .Text & "*'")
            '        End If

            '        bs.Filter = Join(filters.ToArray, " And ")

            '    Case "txtAthlet_Name", "txtAthlet_Verein"

            '        If Not String.IsNullOrEmpty(AthletenFilter) Then filters.Add(AthletenFilter)

            '        If txtAthlet_Name.Text.Length > 0 And txtAthlet_Name.ForeColor <> Color.DarkGray Then
            '            filters.Add("Nachname LIKE '*" & .Text & "*'")
            '        End If
            '        If txtAthlet_Verein.Text.Length > 0 And txtAthlet_Verein.ForeColor <> Color.DarkGray Then
            '            filters.Add("Verein LIKE '*" & .Text & "*'")
            '        End If

            '        bsA.Filter = Join(filters.ToArray, " And ")
            'End Select
        End With
    End Sub
    Private Sub Set_Filter(sender As Object)
        Dim filters As New List(Of String)

        With CType(sender, TextBox)
            Select Case .Name
                Case "txtMeldung_Name", "txtMeldung_Verein"

                    If txtMeldung_Name.Text.Length > 0 And txtMeldung_Name.ForeColor <> Color.DarkGray Then
                        filters.Add("Nachname LIKE '*" & txtMeldung_Name.Text & "*'")
                    End If
                    If txtMeldung_Verein.Text.Length > 0 And txtMeldung_Verein.ForeColor <> Color.DarkGray Then
                        filters.Add("Verein LIKE '*" & txtMeldung_Verein.Text & "*'")
                    End If

                    bs.Filter = Join(filters.ToArray, " And ")

                Case "txtAthlet_Name", "txtAthlet_Verein"

                    If Not String.IsNullOrEmpty(AthletenFilter) Then filters.Add(AthletenFilter)

                    If txtAthlet_Name.Text.Length > 0 And txtAthlet_Name.ForeColor <> Color.DarkGray Then
                        filters.Add("Nachname LIKE '*" & txtAthlet_Name.Text & "*'")
                    End If
                    If txtAthlet_Verein.Text.Length > 0 And txtAthlet_Verein.ForeColor <> Color.DarkGray Then
                        filters.Add(txtAthlet_Verein.Tag.ToString & " LIKE '*" & txtAthlet_Verein.Text & "*'")
                    End If

                    bsA.Filter = Join(filters.ToArray, " And ")
            End Select
        End With
    End Sub

    '' Eingabe
    Private Sub ComboBox_Enter(sender As Object, e As EventArgs) Handles cboGK.Enter, cboVereinsmannschaft.Enter
        With CType(sender, ComboBox)
            .Tag = .SelectedValue
        End With
    End Sub
    Private Sub ComboBox_KeyDown(sender As Object, e As KeyEventArgs) Handles cboGK.KeyDown, cboVereinsmannschaft.KeyDown
        If e.KeyCode = Keys.Escape Then
            With CType(sender, ComboBox)
                If IsNothing(.Tag) Then
                    .SelectedValue = -1
                Else
                    .SelectedValue = .Tag
                End If
            End With
        ElseIf e.KeyCode = Keys.Enter Then
            If ModifierKeys = Keys.Control OrElse sender.Equals(cboVereinsmannschaft) Then
                btnWeiter.PerformClick()
            ElseIf sender.Equals(cboGK) Then
                txtReissen.Focus()
            End If
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub CheckBox_Enter(sender As Object, e As EventArgs) Handles chkA_K.Enter, chkLändermannschaft.Enter, chkMeldedatum.Enter
        With CType(sender, CheckBox)
            .Tag = .Checked
        End With
    End Sub
    Private Sub CheckBox_KeyDown(sender As Object, e As KeyEventArgs) Handles chkA_K.KeyDown, chkLändermannschaft.KeyDown, chkMeldedatum.KeyDown
        If e.KeyCode = Keys.Escape Then
            With CType(sender, CheckBox)
                If Not IsNothing(.Tag) Then .Checked = CBool(.Tag)
            End With
        ElseIf e.KeyCode = Keys.Enter Then
            If ModifierKeys = Keys.Control Then
                btnWeiter.PerformClick()
            ElseIf sender.Equals(chkMeldedatum) Then
                txtGewicht.Focus()
            End If
            e.SuppressKeyPress = True
        End If
    End Sub
    Private Sub CheckBox_Click(sender As Object, e As EventArgs) Handles chkA_K.Click, chkLändermannschaft.Click
        With CType(sender, CheckBox)
            .Checked = Not .Checked
        End With
        bs.EndEdit()
        mnuSpeichern.Enabled = True
    End Sub

    Private Sub cboGK_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGK.SelectedIndexChanged
        Try
            If cboGK.Focused Then
                dv(bs.Position)("GK") = cboGK.Text
                dv(bs.Position)("idGK") = cboGK.SelectedValue
                bs.EndEdit()
                mnuSpeichern.Enabled = True
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dpkDatum_KeyDown(sender As Object, e As KeyEventArgs) Handles dpkDatum.KeyDown
        Select Case e.KeyCode
            Case Keys.Back, Keys.Delete
                'dpkDatum.Format = DateTimePickerFormat.Custom
                'dpkDatum.CustomFormat = " "
                'chkDatumMerken.Checked = False
            Case Keys.Enter
                If ModifierKeys = Keys.Control Then
                    btnWeiter.PerformClick()
                Else
                    txtGewicht.Focus()
                End If
                e.SuppressKeyPress = True
        End Select
        mnuSpeichern.Enabled = True
    End Sub
    Private Sub dpkDatum_ValueChanged(sender As Object, e As EventArgs) Handles dpkDatum.ValueChanged, dpkDatum.TextChanged
        'If dpkDatum.Focused AndAlso chkDatumMerken.Checked Then
        '    chkDatumMerken.Tag = dpkDatum.Value
        'End If
        If dpkDatum.Focused Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub TextBox_Enter(sender As Object, e As EventArgs) Handles txtGewicht.Enter, txtReissen.Enter, txtStossen.Enter, txtZweikampf.Enter

        Dim ctl = CType(sender, TextBox)
        With ctl
            .Tag = .Text
            .SelectAll()
        End With
    End Sub
    Private Sub TextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles txtGewicht.KeyDown,
                                                                             txtReissen.KeyDown,
                                                                             txtStossen.KeyDown,
                                                                             txtZweikampf.KeyDown,
                                                                             txtLosnummer.KeyDown
        If e.KeyCode = Keys.Escape Then
            With CType(sender, TextBox)
                .Text = .Tag.ToString
                .SelectAll()
            End With
        ElseIf e.KeyCode = Keys.Enter Then
            If ModifierKeys = Keys.Control Then
                btnWeiter.PerformClick()
            ElseIf ModifierKeys = Nothing Then
                If txtGewicht.Focused Then
                    txtReissen.Focus()
                ElseIf txtReissen.Focused Then
                    txtStossen.Focus()
                ElseIf txtStossen.Focused Then
                    If Not String.IsNullOrEmpty(txtReissen.Text) AndAlso Not String.IsNullOrEmpty(txtStossen.Text) Then
                        'btnWeiter.PerformClick()
                        txtLosnummer.Focus()
                    Else
                        txtZweikampf.Focus()
                    End If
                ElseIf txtZweikampf.Focused Then
                    txtLosnummer.Focus()
                ElseIf txtLosnummer.Focused Then
                    btnWeiter.PerformClick()
                End If
            End If
            e.SuppressKeyPress = True
            e.Handled = True
        Else
            nonNumberEntered = False
            If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
                If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                    If Not sender.Equals(txtGewicht) AndAlso e.KeyCode <> Keys.Back And e.KeyCode <> Keys.OemPeriod Then
                        nonNumberEntered = True
                    End If
                End If
            End If
            If ModifierKeys = Keys.Shift Then
                nonNumberEntered = True
            End If
        End If
        If Not nonNumberEntered Then mnuSpeichern.Enabled = True
    End Sub
    Private Sub TextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtGewicht.KeyPress,
                                                                                   txtReissen.KeyPress,
                                                                                   txtStossen.KeyPress,
                                                                                   txtZweikampf.KeyPress,
                                                                                   txtLosnummer.KeyPress
        If nonNumberEntered Then e.Handled = True
    End Sub
    Private Sub textBox_TextChanged(sender As Object, e As EventArgs) Handles txtGewicht.TextChanged,
                                                                              txtStossen.TextChanged,
                                                                              txtReissen.TextChanged,
                                                                              txtZweikampf.TextChanged,
                                                                              txtLosnummer.TextChanged
        Dim ctl = CType(sender, TextBox)
        If ctl.Focused Then
            Try
                If String.IsNullOrEmpty(ctl.Text) Then
                    dv(bs.Position)(ctl.Name.Substring(3)) = DBNull.Value
                End If
            Catch ex As Exception
            End Try
            mnuSpeichern.Enabled = True
        End If
    End Sub
    Private Sub txtGewicht_LostFocus(sender As Object, e As EventArgs) Handles txtGewicht.LostFocus
        Write_GK()
        txtGewicht.Text = Format(txtGewicht.Text, "Fixed") 'If IsNumeric(txtGewicht.Text) Then 
    End Sub
    Private Sub txtReissenStossen_LostFocus(sender As Object, e As EventArgs) Handles txtReissen.LostFocus, txtStossen.LostFocus
        If IsDBNull(dv(bs.Position)!Zweikampf) AndAlso (Not IsDBNull(dv(bs.Position)!Reissen) AndAlso Not IsDBNull(dv(bs.Position)!Stossen)) Then
            dv(bs.Position)!Zweikampf = CInt(dv(bs.Position)!Reissen) + CInt(dv(bs.Position)!Stossen)
        ElseIf IsDBNull(dv(bs.Position)!Reissen) OrElse IsDBNull(dv(bs.Position)!Stossen) Then
            dv(bs.Position)!Zweikampf = DBNull.Value
        End If
        bs.EndEdit()
    End Sub
    Private Sub cboVereinsmannschaft_VisibleChanged(sender As Object, e As EventArgs) Handles cboVereinsmannschaft.VisibleChanged
        lblVereinsmannschaft.Visible = cboVereinsmannschaft.Visible
    End Sub
    Private Sub cboVereinsmannschaft_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVereinsmannschaft.SelectedIndexChanged
        If cboVereinsmannschaft.Focused Then
            mnuSpeichern.Enabled = True
            bs.EndEdit()
        End If
    End Sub

    '' Proceed
    Private Sub chkAlle_Athlet_Click(sender As Object, e As EventArgs) Handles chkAlle_Athlet.Click
        Dim CheckValue As Boolean
        With chkAlle_Athlet
            If .CheckState = CheckState.Indeterminate OrElse .CheckState = CheckState.Checked Then
                CheckValue = False
            Else
                CheckValue = True
            End If
            .Checked = CheckValue
        End With
        If Not BackgroundWorker1.IsBusy Then BackgroundWorker1.RunWorkerAsync(CheckValue)
    End Sub
    Private Sub chkDatumMerken_CheckedChanged(sender As Object, e As EventArgs) Handles chkDatumMerken.CheckedChanged

        pnlMeldedatum.Visible = chkDatumMerken.Checked
        If pnlMeldedatum.Visible Then
            txtMeldedatum.Text = dpkDatum.Text
            If bs.Position > 0.1 Then
                chkMeldedatum.Checked = Not IsDBNull(dv(bs.Position)!Meldedatum)
            End If
        End If

        dpkDatum.Visible = Not chkDatumMerken.Checked
        dpkDatum.Enabled = Not chkDatumMerken.Checked

        'If chkDatumMerken.Checked Then
        '    'dpkDatum.Format = DateTimePickerFormat.Short
        '    chkDatumMerken.Tag = dpkDatum.Value
        'Else
        '    chkDatumMerken.Tag = Nothing
        'End If
    End Sub
    Private Sub chkMeldedatum_CheckedChanged(sender As Object, e As EventArgs) Handles chkMeldedatum.CheckedChanged
        If chkMeldedatum.Checked Then
            dv(bs.Position)!Meldedatum = CDate(txtMeldedatum.Text)
        Else
            dv(bs.Position)!Meldedatum = DBNull.Value
        End If
        bs.EndEdit()
        If chkMeldedatum.Focused Then mnuSpeichern.Enabled = True
    End Sub

    Private Sub btnAthletÜbernehmen_Click(sender As Object, e As EventArgs) Handles btnAthlet_Übernehmen.Click

        cboGK.SelectedIndex = -1

        Dim lstStateNotFound As New List(Of Integer)

        Cursor = Cursors.WaitCursor

        'Dim dvSelect As New DataView(dtAuswahl.Copy(), "Übernehmen = True", Nothing, DataViewRowState.CurrentRows)
        Dim dvSelect As New DataView(dtAuswahl, "Übernehmen = True", Nothing, DataViewRowState.CurrentRows)

        With pgbCheckAll
            .Minimum = 0
            .Maximum = dvSelect.Count
            .Visible = True
            .Refresh()
        End With

        Dim i = 1
        SetProgressBarText(pgbCheckAll, "TN importieren", ProgressBarTextLocation.Centered, Color.Black, New Font("Segoe UI", 9.5)) '"Microsoft Sans Serif", 8.25))

        For Each row As DataRowView In dvSelect

            pgbCheckAll.Value = i

            Dim ak = Glob.Get_AK(row!Jahrgang)
            Dim IsDirty = False

            If Not mnuLokaleDatenbank.Checked Then
                Dim Athlet = From x In dtAthleten Where x.Field(Of Integer)("idTeilnehmer") = CInt(row!Teilnehmer)
                Dim VereinRows As DataRow() = Nothing
                Dim MannschaftRows As DataRow() = Nothing
                If Not IsDBNull(row!ESR) Then VereinRows = dtVerein.Select("GUID = '" & row!ESR.ToString & "'")
                If Not IsDBNull(row!MSR) Then MannschaftRows = dtVerein.Select("GUID = '" & row!MSR.ToString & "'")

                If Not IsNothing(VereinRows) AndAlso VereinRows.Length = 1 Then
                    row!ESR = VereinRows(0)!idVerein
                    row!Verein = VereinRows(0)!Vereinsname
                Else
                    row!ESR = 0
                    If IsDBNull(row!Verein) Then row!Verein = String.Empty
                    row!Verein = "nicht gefunden: " & row!Verein.ToString
                End If
                If Not IsNothing(MannschaftRows) AndAlso MannschaftRows.Length = 1 Then
                    Dim TeamFound = dtTeams.Select("Verein = " & MannschaftRows(0)!idVerein.ToString)
                    If TeamFound.Length > 0 Then
                        row!MSR = TeamFound(0)!Team
                        row!Mannschaft = TeamFound(0)!Vereinsname
                    Else
                        row!MSR = MannschaftRows(0)!idVerein
                        row!Mannschaft = MannschaftRows(0)!Vereinsname
                    End If
                Else
                    row!MSR = 0
                    If IsDBNull(row!Mannschaft) Then row!Mannschaft = String.Empty
                    row!Mannschaft = "nicht gefunden: " & row!Mannschaft.ToString
                End If

                row!Staat = Glob.Get_StateID(row!state_name, dtStaaten.DefaultView)
                ' TN ohne Staatenzuordnung in Liste schreiben --> markieren | Meldung | ???
                If CInt(row!Staat) = 0 Then lstStateNotFound.Add(CInt(row!Teilnehmer))

                If Athlet.Count = 0 Then
                        ' add Athlet to dtAthleten --> add to DB at Save
                        Try
                            Dim newAthlet = dtAthleten.NewRow
                            'newAthlet.ItemArray = {
                            '    row!Teilnehmer,
                            '    row!athleten_cas_id,
                            '    DBNull.Value,   ' Startbuch_Nummer
                            '    "",             ' Titel
                            '    row!Nachname,
                            '    row!Vorname,
                            '    row!ESR,
                            '    row!ESR,
                            '    row!MSR,
                            '    row!Jahrgang,
                            '    row!Geburtstag,
                            '    row!Sex,
                            '    row!Staat,
                            '    True
                            '}
                            newAthlet!idTeilnehmer = row!Teilnehmer
                            newAthlet!GUID = row!athleten_cas_id
                            newAthlet!Startbuch_Nummer = DBNull.Value
                            newAthlet!Titel = String.Empty
                            newAthlet!Nachname = row!Nachname
                            newAthlet!Vorname = row!Vorname
                            newAthlet!Verein = row!ESR
                            newAthlet!ESR = row!ESR
                            newAthlet!MSR = row!MSR
                            newAthlet!Jahrgang = row!Jahrgang
                            newAthlet!Geburtstag = row!Geburtstag
                            newAthlet!Geschlecht = row!Sex
                            newAthlet!Staat = row!Staat
                            newAthlet!Lizenz = True
                            dtAthleten.Rows.Add(newAthlet)
                            IsDirty = True
                        Catch ex As Exception
                            LogMessage("Meldung: Athlet übernehmen(newAthlet): " & ex.Message, ex.StackTrace)
                        End Try
                    Else
                        Dim fields = {"Nachname", "Vorname", "ESR", "MSR", "Jahrgang", "Staat"}
                        For Each field In fields
                            If Not Athlet(0)(field).ToString.Equals(row(field).ToString) Then
                                Athlet(0)(field) = row(field)
                                IsDirty = True
                            End If
                        Next
                        If Not CDate(Athlet(0)!Geburtstag).ToString("d").Equals(CDate(row!Geburtstag).ToString("d")) Then
                            Athlet(0)!Geburtstag = row!Geburtstag
                            IsDirty = True
                        End If
                        If Not Athlet(0)!Geschlecht.ToString.Equals(row!Sex.ToString) Then
                            Athlet(0)!Geschlecht = row!Sex
                            IsDirty = True
                        End If
                        If Not Athlet(0)!GUID.ToString.Equals(row!athleten_cas_id.ToString) Then
                            Athlet(0)!GUID = row!athleten_cas_id
                            IsDirty = True
                        End If
                    End If
                    bsA.EndEdit()
                End If

                ' Row für Meldung erstellen
                Dim newRow = dtMeldung.NewRow
            Try

                newRow.ItemArray = {
                    row!Teilnehmer,
                    row!Sex,
                    row!Nachname,
                    row!Vorname,
                    row!Geburtstag,
                    row!Jahrgang,
                    row!Staat,
                    row!state_name,
                    row!ESR,
                    row!Verein,
                    row!MSR,
                    row!Mannschaft,
                    Wettkampf.ID,
                    Get_LastIndex() + 1,        ' Sortierung
                    DBNull.Value,               ' Losnummer
                    False,                      ' Laenderwertung
                    DBNull.Value,               ' Vereinswertung --> sonst wird eine 0 im dgv angezeigt
                    DBNull.Value,               ' Relativ_W
                    False,                      ' a_K
                    ak.Key,                     ' idAK
                    ak.Value,                   ' AK
                    DBNull.Value,               ' Gewicht
                    DBNull.Value,               ' Wiegen
                    DBNull.Value,               ' Startnummer
                    DBNull.Value,               ' idGK
                    DBNull.Value,               ' GK
                    IIf(chkDatumMerken.Checked, CDate(txtMeldedatum.Text), DBNull.Value),
                    DBNull.Value,               ' Zweikampf
                    DBNull.Value,               ' Reissen
                    DBNull.Value,               ' Stossen
                    DBNull.Value,               ' attend 
                    row!MSR,                    ' Team
                    IsDirty,                    ' von BVDG eingefügt
                    Status.Ergebnis_Nachmeldung ' OnlineStatus
                }
                dtMeldung.Rows.Add(newRow)
                bs.EndEdit()
            Catch ex As Exception
                LogMessage("Meldung: Athlet übernehmen(newMeldung): " & ex.Message, ex.StackTrace)
            End Try

            If Not IsNothing(Wettkampf.Identities) Then
                Dim pos = bs.Find("Teilnehmer", row!Teilnehmer)
                'If Wettkampf.Identities.Count = 1 Then
                dv(pos)("WK_" & Wettkampf.Identities.Keys(0)) = True
                'ElseIf Wettkampf.Identities.Count > 1 Then
                '    With dgvJoin.Rows(pos)
                '        .DefaultCellStyle.ForeColor = Color.Red
                '        For Each key As String In Wettkampf.Identities.Keys
                '            .Cells("WK_" & key).Style.BackColor = Color.Red
                '        Next
                '    End With
                'End If
                If Wettkampf.Mannschaft AndAlso CInt(row!MSR) = 0 Then
                    dgvJoin.Rows(pos).Cells("MSR").Style.BackColor = Color.Yellow
                End If
            End If
            bs.EndEdit()
            row!Übernehmen = False
            i += 1
        Next

        pgbCheckAll.Visible = False
        mnuSpeichern.Enabled = True

        Try
            loading = True
            bs.MoveLast()
            loading = False
            With dgvJoin
                .FirstDisplayedScrollingRowIndex = bs.Position
                .Rows(bs.Position).Selected = True
                .Focus()
            End With
            Fill_GK()
        Catch ex As Exception
        End Try

        'txtAthlet_Name.Text = String.Empty
        'txtAthlet_Verein.Text = String.Empty
        AthletenFilter = Get_AthletenFilter()
        Set_Filter(txtAthlet_Name)

        lblAnzahl.Text = "TN:  " & dv.Count
        Cursor = DefaultCursor
    End Sub
    Private Sub btnMove_Click(sender As Object, e As EventArgs) Handles btnWeiter.Click, btnZurück.Click

        '' Datum zuweisen
        'If chkDatumMerken.Checked Then
        '    dv(bs.Position)!Meldedatum = chkDatumMerken.Tag
        'ElseIf dpkDatum.Format = DateTimePickerFormat.Short Then
        '    dv(bs.Position)!Meldedatum = dpkDatum.Value
        'Else
        '    dv(bs.Position)!Meldedatum = DBNull.Value
        'End If

        ' Vereinsmannschaften
        'If String.IsNullOrEmpty(cboVereinsmannschaft.Text) Then
        '    dv(bs.Position)!Vereinswertung = DBNull.Value
        'End If

        ' Move
        If sender.Equals(btnWeiter) Then
            bs.MoveNext()
        Else
            bs.MovePrevious()
        End If

        dgvJoin.Rows(bs.Position).Selected = True

        If Not dgvJoin.Rows(dgvJoin.SelectedRows(0).Index).Displayed Then
            dgvJoin.FirstDisplayedScrollingRowIndex = bs.Position
        End If

        '' Datumsanzeige für nächsten Record
        'If chkDatumMerken.Checked Then
        '    dpkDatum.Format = DateTimePickerFormat.Short
        '    dpkDatum.Value = CDate(chkDatumMerken.Tag)
        'End If

        bs.EndEdit()
        dgvJoin.EndEdit()

        'txtGewicht.Focus()
        If dpkDatum.Visible Then
            dpkDatum.Focus()
        ElseIf pnlMeldedatum.Visible Then
            chkMeldedatum.Focus()
        End If

    End Sub

    Private Function Get_LastIndex() As Integer
        If dv.Count = 0 Then Return 0
        Dim Ix = (From x In dv.ToTable
                  Select x.Field(Of Integer)("Sortierung")).Max
        Return Ix
    End Function

    Private Delegate Sub CheckAllLifters(sender As Object, e As DoWorkEventArgs)
    Private Sub BackgroundWorker1_DoWork(sender As Object, e As DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        If InvokeRequired Then
            Dim d As New CheckAllLifters(AddressOf BackgroundWorker1_DoWork)
            Invoke(d, New Object() {sender, e})
        Else
            Dim CheckValue As Boolean = DirectCast(e.Argument, Boolean)
            With pgbCheckAll
                .Minimum = 0
                .Maximum = dgvAuswahl.Rows.Count
                .Visible = True
                .Refresh()
            End With
            'SetProgressBarText(pgbCheckAll, If(CheckValue, "alle auswählen", "Auswahl aufheben"), ProgressBarTextLocation.Centered, Color.Black, New Font("Segoe UI", 9.5)) '"Microsoft Sans Serif", 8.25))
            'SetProgressBarText(pgbCheckAll, dgvAuswahl.Rows.Count.ToString + " Heber bearbeiten", ProgressBarTextLocation.Centered, Color.Black, New Font("Segoe UI", 9.5)) '"Microsoft Sans Serif", 8.25))
            For i = 0 To dgvAuswahl.Rows.Count - 1 ' dvA.Count - 1
                dgvAuswahl.Rows(i).Cells("Übernehmen").Value = CheckValue
                pgbCheckAll.Value = i + 1
            Next
        End If
    End Sub
    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        pgbCheckAll.Visible = False
        bsA.EndEdit()
        dgvAuswahl.Refresh()
    End Sub

    Private Sub cboAK_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAK.SelectedIndexChanged
        If loading Then Return
        Dim row = DirectCast(cboAK.SelectedValue, DataRowView)
        If row!Altersklasse.ToString.Equals("[automatisch]") Then
            Dim ak = Glob.Get_AK(dv(bs.Position)!Jahrgang)
            dv(bs.Position)!idAK = ak.Key
            dv(bs.Position)!AK = ak.Value
        Else
            Dim rows = dtAKs.Select("Altersklasse = '" & row!Altersklasse.ToString & "'")
            If rows.Count = 1 Then
                dv(bs.Position)!idAK = rows(0)!idAltersklasse
                dv(bs.Position)!AK = rows(0)!Altersklasse
            Else
                Stop
            End If
        End If
        bs.EndEdit()
        mnuSpeichern.Enabled = True
    End Sub

    Private Sub CheckLokaleDB_CheckedChanged(sender As Object, e As EventArgs) Handles CheckLokaleDB.CheckedChanged
        mnuLokaleDatenbank.Checked = CheckLokaleDB.Checked
    End Sub

    Private Sub mnuLokaleDatenbank_Click(sender As Object, e As EventArgs) Handles mnuLokaleDatenbank.CheckedChanged
        CheckLokaleDB.Checked = mnuLokaleDatenbank.Checked
        Fill_dgvAuswahl(mnuLokaleDatenbank.Checked)
    End Sub

    Private Sub SplitContainer1_SplitterMoved(sender As Object, e As SplitterEventArgs) Handles SplitContainer1.SplitterMoved
        If loading Then Return
        If SplitContainer1.Panel2.Height < 253 Then
            SplitContainer1.SplitterDistance = SplitContainer1.Height - 257
        End If
    End Sub

End Class
