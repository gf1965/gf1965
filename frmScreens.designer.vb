﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmScreens
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmScreens))
        Me.btnIdentify = New System.Windows.Forms.Button()
        Me.btnAssign = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.pnlScreens = New System.Windows.Forms.Panel()
        Me.set14 = New System.Windows.Forms.Button()
        Me.btn14 = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cboAnzeige14 = New System.Windows.Forms.ComboBox()
        Me.set13 = New System.Windows.Forms.Button()
        Me.btn13 = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboAnzeige13 = New System.Windows.Forms.ComboBox()
        Me.set12 = New System.Windows.Forms.Button()
        Me.btn12 = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboAnzeige12 = New System.Windows.Forms.ComboBox()
        Me.set11 = New System.Windows.Forms.Button()
        Me.btn11 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cboAnzeige11 = New System.Windows.Forms.ComboBox()
        Me.set10 = New System.Windows.Forms.Button()
        Me.btn10 = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboAnzeige10 = New System.Windows.Forms.ComboBox()
        Me.set9 = New System.Windows.Forms.Button()
        Me.set8 = New System.Windows.Forms.Button()
        Me.set7 = New System.Windows.Forms.Button()
        Me.set6 = New System.Windows.Forms.Button()
        Me.set5 = New System.Windows.Forms.Button()
        Me.set4 = New System.Windows.Forms.Button()
        Me.set3 = New System.Windows.Forms.Button()
        Me.set2 = New System.Windows.Forms.Button()
        Me.set1 = New System.Windows.Forms.Button()
        Me.btn9 = New System.Windows.Forms.Button()
        Me.btn8 = New System.Windows.Forms.Button()
        Me.btn7 = New System.Windows.Forms.Button()
        Me.btn6 = New System.Windows.Forms.Button()
        Me.btn5 = New System.Windows.Forms.Button()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboAnzeige9 = New System.Windows.Forms.ComboBox()
        Me.cboAnzeige8 = New System.Windows.Forms.ComboBox()
        Me.cboAnzeige7 = New System.Windows.Forms.ComboBox()
        Me.cboAnzeige6 = New System.Windows.Forms.ComboBox()
        Me.cboAnzeige5 = New System.Windows.Forms.ComboBox()
        Me.cboAnzeige4 = New System.Windows.Forms.ComboBox()
        Me.cboAnzeige3 = New System.Windows.Forms.ComboBox()
        Me.cboAnzeige2 = New System.Windows.Forms.ComboBox()
        Me.cboAnzeige1 = New System.Windows.Forms.ComboBox()
        Me.btnDefault = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlScreens.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnIdentify
        '
        Me.btnIdentify.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnIdentify.Location = New System.Drawing.Point(411, 112)
        Me.btnIdentify.Name = "btnIdentify"
        Me.btnIdentify.Size = New System.Drawing.Size(109, 26)
        Me.btnIdentify.TabIndex = 150
        Me.btnIdentify.TabStop = False
        Me.btnIdentify.Text = "Identifizieren"
        Me.ToolTip1.SetToolTip(Me.btnIdentify, "ID auf allen Anzeigen anzeigen")
        Me.btnIdentify.UseVisualStyleBackColor = True
        '
        'btnAssign
        '
        Me.btnAssign.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAssign.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnAssign.Enabled = False
        Me.btnAssign.Location = New System.Drawing.Point(411, 16)
        Me.btnAssign.Name = "btnAssign"
        Me.btnAssign.Size = New System.Drawing.Size(109, 26)
        Me.btnAssign.TabIndex = 100
        Me.btnAssign.TabStop = False
        Me.btnAssign.Text = "Zuweisen"
        Me.btnAssign.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(411, 48)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(109, 26)
        Me.btnExit.TabIndex = 120
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "Abbrechen"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'pnlScreens
        '
        Me.pnlScreens.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlScreens.AutoScroll = True
        Me.pnlScreens.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.pnlScreens.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlScreens.Controls.Add(Me.set14)
        Me.pnlScreens.Controls.Add(Me.btn14)
        Me.pnlScreens.Controls.Add(Me.Label14)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige14)
        Me.pnlScreens.Controls.Add(Me.set13)
        Me.pnlScreens.Controls.Add(Me.btn13)
        Me.pnlScreens.Controls.Add(Me.Label13)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige13)
        Me.pnlScreens.Controls.Add(Me.set12)
        Me.pnlScreens.Controls.Add(Me.btn12)
        Me.pnlScreens.Controls.Add(Me.Label12)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige12)
        Me.pnlScreens.Controls.Add(Me.set11)
        Me.pnlScreens.Controls.Add(Me.btn11)
        Me.pnlScreens.Controls.Add(Me.Label11)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige11)
        Me.pnlScreens.Controls.Add(Me.set10)
        Me.pnlScreens.Controls.Add(Me.btn10)
        Me.pnlScreens.Controls.Add(Me.Label10)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige10)
        Me.pnlScreens.Controls.Add(Me.set9)
        Me.pnlScreens.Controls.Add(Me.set8)
        Me.pnlScreens.Controls.Add(Me.set7)
        Me.pnlScreens.Controls.Add(Me.set6)
        Me.pnlScreens.Controls.Add(Me.set5)
        Me.pnlScreens.Controls.Add(Me.set4)
        Me.pnlScreens.Controls.Add(Me.set3)
        Me.pnlScreens.Controls.Add(Me.set2)
        Me.pnlScreens.Controls.Add(Me.set1)
        Me.pnlScreens.Controls.Add(Me.btn9)
        Me.pnlScreens.Controls.Add(Me.btn8)
        Me.pnlScreens.Controls.Add(Me.btn7)
        Me.pnlScreens.Controls.Add(Me.btn6)
        Me.pnlScreens.Controls.Add(Me.btn5)
        Me.pnlScreens.Controls.Add(Me.btn4)
        Me.pnlScreens.Controls.Add(Me.btn3)
        Me.pnlScreens.Controls.Add(Me.btn2)
        Me.pnlScreens.Controls.Add(Me.btn1)
        Me.pnlScreens.Controls.Add(Me.Label9)
        Me.pnlScreens.Controls.Add(Me.Label8)
        Me.pnlScreens.Controls.Add(Me.Label7)
        Me.pnlScreens.Controls.Add(Me.Label6)
        Me.pnlScreens.Controls.Add(Me.Label5)
        Me.pnlScreens.Controls.Add(Me.Label4)
        Me.pnlScreens.Controls.Add(Me.Label3)
        Me.pnlScreens.Controls.Add(Me.Label2)
        Me.pnlScreens.Controls.Add(Me.Label1)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige9)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige8)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige7)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige6)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige5)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige4)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige3)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige2)
        Me.pnlScreens.Controls.Add(Me.cboAnzeige1)
        Me.pnlScreens.Location = New System.Drawing.Point(16, 17)
        Me.pnlScreens.Name = "pnlScreens"
        Me.pnlScreens.Size = New System.Drawing.Size(375, 440)
        Me.pnlScreens.TabIndex = 0
        '
        'set14
        '
        Me.set14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set14.Image = CType(resources.GetObject("set14.Image"), System.Drawing.Image)
        Me.set14.Location = New System.Drawing.Point(333, 397)
        Me.set14.Name = "set14"
        Me.set14.Size = New System.Drawing.Size(24, 24)
        Me.set14.TabIndex = 190
        Me.set14.TabStop = False
        Me.set14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set14, "Anzeige 1 anpassen")
        Me.set14.UseVisualStyleBackColor = True
        '
        'btn14
        '
        Me.btn14.Image = CType(resources.GetObject("btn14.Image"), System.Drawing.Image)
        Me.btn14.Location = New System.Drawing.Point(303, 397)
        Me.btn14.Name = "btn14"
        Me.btn14.Size = New System.Drawing.Size(24, 24)
        Me.btn14.TabIndex = 189
        Me.btn14.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn14, "Anzeige 9 aktualisieren")
        Me.btn14.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(17, 402)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(60, 13)
        Me.Label14.TabIndex = 188
        Me.Label14.Text = "Anzeige 14"
        '
        'cboAnzeige14
        '
        Me.cboAnzeige14.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige14.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige14.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige14.FormattingEnabled = True
        Me.cboAnzeige14.Location = New System.Drawing.Point(81, 397)
        Me.cboAnzeige14.Name = "cboAnzeige14"
        Me.cboAnzeige14.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige14.TabIndex = 187
        '
        'set13
        '
        Me.set13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set13.Image = CType(resources.GetObject("set13.Image"), System.Drawing.Image)
        Me.set13.Location = New System.Drawing.Point(333, 368)
        Me.set13.Name = "set13"
        Me.set13.Size = New System.Drawing.Size(24, 24)
        Me.set13.TabIndex = 186
        Me.set13.TabStop = False
        Me.set13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set13, "Anzeige 1 anpassen")
        Me.set13.UseVisualStyleBackColor = True
        '
        'btn13
        '
        Me.btn13.Image = CType(resources.GetObject("btn13.Image"), System.Drawing.Image)
        Me.btn13.Location = New System.Drawing.Point(303, 368)
        Me.btn13.Name = "btn13"
        Me.btn13.Size = New System.Drawing.Size(24, 24)
        Me.btn13.TabIndex = 185
        Me.btn13.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn13, "Anzeige 9 aktualisieren")
        Me.btn13.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(17, 373)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(60, 13)
        Me.Label13.TabIndex = 184
        Me.Label13.Text = "Anzeige 13"
        '
        'cboAnzeige13
        '
        Me.cboAnzeige13.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige13.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige13.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige13.FormattingEnabled = True
        Me.cboAnzeige13.Location = New System.Drawing.Point(81, 368)
        Me.cboAnzeige13.Name = "cboAnzeige13"
        Me.cboAnzeige13.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige13.TabIndex = 183
        '
        'set12
        '
        Me.set12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set12.Image = CType(resources.GetObject("set12.Image"), System.Drawing.Image)
        Me.set12.Location = New System.Drawing.Point(333, 339)
        Me.set12.Name = "set12"
        Me.set12.Size = New System.Drawing.Size(24, 24)
        Me.set12.TabIndex = 182
        Me.set12.TabStop = False
        Me.set12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set12, "Anzeige 1 anpassen")
        Me.set12.UseVisualStyleBackColor = True
        '
        'btn12
        '
        Me.btn12.Image = CType(resources.GetObject("btn12.Image"), System.Drawing.Image)
        Me.btn12.Location = New System.Drawing.Point(303, 339)
        Me.btn12.Name = "btn12"
        Me.btn12.Size = New System.Drawing.Size(24, 24)
        Me.btn12.TabIndex = 181
        Me.btn12.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn12, "Anzeige 9 aktualisieren")
        Me.btn12.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(17, 344)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 13)
        Me.Label12.TabIndex = 180
        Me.Label12.Text = "Anzeige 12"
        '
        'cboAnzeige12
        '
        Me.cboAnzeige12.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige12.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige12.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige12.FormattingEnabled = True
        Me.cboAnzeige12.Location = New System.Drawing.Point(81, 339)
        Me.cboAnzeige12.Name = "cboAnzeige12"
        Me.cboAnzeige12.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige12.TabIndex = 179
        '
        'set11
        '
        Me.set11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set11.Image = CType(resources.GetObject("set11.Image"), System.Drawing.Image)
        Me.set11.Location = New System.Drawing.Point(333, 310)
        Me.set11.Name = "set11"
        Me.set11.Size = New System.Drawing.Size(24, 24)
        Me.set11.TabIndex = 178
        Me.set11.TabStop = False
        Me.set11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set11, "Anzeige 1 anpassen")
        Me.set11.UseVisualStyleBackColor = True
        '
        'btn11
        '
        Me.btn11.Image = CType(resources.GetObject("btn11.Image"), System.Drawing.Image)
        Me.btn11.Location = New System.Drawing.Point(303, 310)
        Me.btn11.Name = "btn11"
        Me.btn11.Size = New System.Drawing.Size(24, 24)
        Me.btn11.TabIndex = 177
        Me.btn11.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn11, "Anzeige 9 aktualisieren")
        Me.btn11.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(17, 315)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(60, 13)
        Me.Label11.TabIndex = 176
        Me.Label11.Text = "Anzeige 11"
        '
        'cboAnzeige11
        '
        Me.cboAnzeige11.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige11.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige11.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige11.FormattingEnabled = True
        Me.cboAnzeige11.Location = New System.Drawing.Point(81, 310)
        Me.cboAnzeige11.Name = "cboAnzeige11"
        Me.cboAnzeige11.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige11.TabIndex = 175
        '
        'set10
        '
        Me.set10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set10.Image = CType(resources.GetObject("set10.Image"), System.Drawing.Image)
        Me.set10.Location = New System.Drawing.Point(333, 281)
        Me.set10.Name = "set10"
        Me.set10.Size = New System.Drawing.Size(24, 24)
        Me.set10.TabIndex = 174
        Me.set10.TabStop = False
        Me.set10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set10, "Anzeige 1 anpassen")
        Me.set10.UseVisualStyleBackColor = True
        '
        'btn10
        '
        Me.btn10.Image = CType(resources.GetObject("btn10.Image"), System.Drawing.Image)
        Me.btn10.Location = New System.Drawing.Point(303, 281)
        Me.btn10.Name = "btn10"
        Me.btn10.Size = New System.Drawing.Size(24, 24)
        Me.btn10.TabIndex = 173
        Me.btn10.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn10, "Anzeige 9 aktualisieren")
        Me.btn10.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(17, 286)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(60, 13)
        Me.Label10.TabIndex = 172
        Me.Label10.Text = "Anzeige 10"
        '
        'cboAnzeige10
        '
        Me.cboAnzeige10.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige10.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige10.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige10.FormattingEnabled = True
        Me.cboAnzeige10.Location = New System.Drawing.Point(81, 281)
        Me.cboAnzeige10.Name = "cboAnzeige10"
        Me.cboAnzeige10.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige10.TabIndex = 171
        '
        'set9
        '
        Me.set9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set9.Image = CType(resources.GetObject("set9.Image"), System.Drawing.Image)
        Me.set9.Location = New System.Drawing.Point(333, 252)
        Me.set9.Name = "set9"
        Me.set9.Size = New System.Drawing.Size(24, 24)
        Me.set9.TabIndex = 170
        Me.set9.TabStop = False
        Me.set9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set9, "Anzeige 1 anpassen")
        Me.set9.UseVisualStyleBackColor = True
        '
        'set8
        '
        Me.set8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set8.Image = CType(resources.GetObject("set8.Image"), System.Drawing.Image)
        Me.set8.Location = New System.Drawing.Point(333, 223)
        Me.set8.Name = "set8"
        Me.set8.Size = New System.Drawing.Size(24, 24)
        Me.set8.TabIndex = 169
        Me.set8.TabStop = False
        Me.set8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set8, "Anzeige 1 anpassen")
        Me.set8.UseVisualStyleBackColor = True
        '
        'set7
        '
        Me.set7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set7.Image = CType(resources.GetObject("set7.Image"), System.Drawing.Image)
        Me.set7.Location = New System.Drawing.Point(333, 194)
        Me.set7.Name = "set7"
        Me.set7.Size = New System.Drawing.Size(24, 24)
        Me.set7.TabIndex = 168
        Me.set7.TabStop = False
        Me.set7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set7, "Anzeige 1 anpassen")
        Me.set7.UseVisualStyleBackColor = True
        '
        'set6
        '
        Me.set6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set6.Image = CType(resources.GetObject("set6.Image"), System.Drawing.Image)
        Me.set6.Location = New System.Drawing.Point(333, 165)
        Me.set6.Name = "set6"
        Me.set6.Size = New System.Drawing.Size(24, 24)
        Me.set6.TabIndex = 167
        Me.set6.TabStop = False
        Me.set6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set6, "Anzeige 1 anpassen")
        Me.set6.UseVisualStyleBackColor = True
        '
        'set5
        '
        Me.set5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set5.Image = CType(resources.GetObject("set5.Image"), System.Drawing.Image)
        Me.set5.Location = New System.Drawing.Point(333, 136)
        Me.set5.Name = "set5"
        Me.set5.Size = New System.Drawing.Size(24, 24)
        Me.set5.TabIndex = 166
        Me.set5.TabStop = False
        Me.set5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set5, "Anzeige 1 anpassen")
        Me.set5.UseVisualStyleBackColor = True
        '
        'set4
        '
        Me.set4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set4.Image = CType(resources.GetObject("set4.Image"), System.Drawing.Image)
        Me.set4.Location = New System.Drawing.Point(333, 107)
        Me.set4.Name = "set4"
        Me.set4.Size = New System.Drawing.Size(24, 24)
        Me.set4.TabIndex = 165
        Me.set4.TabStop = False
        Me.set4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set4, "Anzeige 1 anpassen")
        Me.set4.UseVisualStyleBackColor = True
        '
        'set3
        '
        Me.set3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set3.Image = CType(resources.GetObject("set3.Image"), System.Drawing.Image)
        Me.set3.Location = New System.Drawing.Point(333, 78)
        Me.set3.Name = "set3"
        Me.set3.Size = New System.Drawing.Size(24, 24)
        Me.set3.TabIndex = 164
        Me.set3.TabStop = False
        Me.set3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set3, "Anzeige 1 anpassen")
        Me.set3.UseVisualStyleBackColor = True
        '
        'set2
        '
        Me.set2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set2.Image = CType(resources.GetObject("set2.Image"), System.Drawing.Image)
        Me.set2.Location = New System.Drawing.Point(333, 49)
        Me.set2.Name = "set2"
        Me.set2.Size = New System.Drawing.Size(24, 24)
        Me.set2.TabIndex = 163
        Me.set2.TabStop = False
        Me.set2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set2, "Anzeige 1 anpassen")
        Me.set2.UseVisualStyleBackColor = True
        '
        'set1
        '
        Me.set1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.set1.Image = CType(resources.GetObject("set1.Image"), System.Drawing.Image)
        Me.set1.Location = New System.Drawing.Point(333, 20)
        Me.set1.Name = "set1"
        Me.set1.Size = New System.Drawing.Size(24, 24)
        Me.set1.TabIndex = 162
        Me.set1.TabStop = False
        Me.set1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.set1, "Anzeige 1 anpassen")
        Me.set1.UseVisualStyleBackColor = True
        '
        'btn9
        '
        Me.btn9.Image = CType(resources.GetObject("btn9.Image"), System.Drawing.Image)
        Me.btn9.Location = New System.Drawing.Point(303, 252)
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(24, 24)
        Me.btn9.TabIndex = 161
        Me.btn9.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn9, "Anzeige 9 aktualisieren")
        Me.btn9.UseVisualStyleBackColor = True
        '
        'btn8
        '
        Me.btn8.Image = CType(resources.GetObject("btn8.Image"), System.Drawing.Image)
        Me.btn8.Location = New System.Drawing.Point(303, 223)
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(24, 24)
        Me.btn8.TabIndex = 161
        Me.btn8.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn8, "Anzeige 8 aktualisieren")
        Me.btn8.UseVisualStyleBackColor = True
        '
        'btn7
        '
        Me.btn7.Image = CType(resources.GetObject("btn7.Image"), System.Drawing.Image)
        Me.btn7.Location = New System.Drawing.Point(303, 194)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(24, 24)
        Me.btn7.TabIndex = 161
        Me.btn7.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn7, "Anzeige 7 aktualisieren")
        Me.btn7.UseVisualStyleBackColor = True
        '
        'btn6
        '
        Me.btn6.Image = CType(resources.GetObject("btn6.Image"), System.Drawing.Image)
        Me.btn6.Location = New System.Drawing.Point(303, 165)
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(24, 24)
        Me.btn6.TabIndex = 161
        Me.btn6.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn6, "Anzeige 6 aktualisieren")
        Me.btn6.UseVisualStyleBackColor = True
        '
        'btn5
        '
        Me.btn5.Image = CType(resources.GetObject("btn5.Image"), System.Drawing.Image)
        Me.btn5.Location = New System.Drawing.Point(303, 136)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(24, 24)
        Me.btn5.TabIndex = 161
        Me.btn5.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn5, "Anzeige 5 aktualisieren")
        Me.btn5.UseVisualStyleBackColor = True
        '
        'btn4
        '
        Me.btn4.Image = CType(resources.GetObject("btn4.Image"), System.Drawing.Image)
        Me.btn4.Location = New System.Drawing.Point(303, 107)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(24, 24)
        Me.btn4.TabIndex = 161
        Me.btn4.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn4, "Anzeige 4 aktualisieren")
        Me.btn4.UseVisualStyleBackColor = True
        '
        'btn3
        '
        Me.btn3.Image = CType(resources.GetObject("btn3.Image"), System.Drawing.Image)
        Me.btn3.Location = New System.Drawing.Point(303, 78)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(24, 24)
        Me.btn3.TabIndex = 161
        Me.btn3.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn3, "Anzeige 3 aktualisieren")
        Me.btn3.UseVisualStyleBackColor = True
        '
        'btn2
        '
        Me.btn2.Image = CType(resources.GetObject("btn2.Image"), System.Drawing.Image)
        Me.btn2.Location = New System.Drawing.Point(303, 49)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(24, 24)
        Me.btn2.TabIndex = 161
        Me.btn2.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn2, "Anzeige 2 aktualisieren")
        Me.btn2.UseVisualStyleBackColor = True
        '
        'btn1
        '
        Me.btn1.Image = CType(resources.GetObject("btn1.Image"), System.Drawing.Image)
        Me.btn1.Location = New System.Drawing.Point(303, 20)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(24, 24)
        Me.btn1.TabIndex = 161
        Me.btn1.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btn1, "Anzeige 1 aktualisieren")
        Me.btn1.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(17, 257)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 13)
        Me.Label9.TabIndex = 160
        Me.Label9.Text = "Anzeige 9"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(17, 228)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(54, 13)
        Me.Label8.TabIndex = 159
        Me.Label8.Text = "Anzeige 8"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(17, 199)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 13)
        Me.Label7.TabIndex = 158
        Me.Label7.Text = "Anzeige 7"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 170)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 13)
        Me.Label6.TabIndex = 157
        Me.Label6.Text = "Anzeige 6"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(17, 141)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 13)
        Me.Label5.TabIndex = 156
        Me.Label5.Text = "Anzeige 5"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 112)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 13)
        Me.Label4.TabIndex = 155
        Me.Label4.Text = "Anzeige 4"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 154
        Me.Label3.Text = "Anzeige 3"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 153
        Me.Label2.Text = "Anzeige 2"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 152
        Me.Label1.Text = "Anzeige 1"
        '
        'cboAnzeige9
        '
        Me.cboAnzeige9.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige9.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige9.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige9.FormattingEnabled = True
        Me.cboAnzeige9.Location = New System.Drawing.Point(81, 252)
        Me.cboAnzeige9.Name = "cboAnzeige9"
        Me.cboAnzeige9.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige9.TabIndex = 18
        '
        'cboAnzeige8
        '
        Me.cboAnzeige8.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige8.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige8.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige8.FormattingEnabled = True
        Me.cboAnzeige8.Location = New System.Drawing.Point(81, 223)
        Me.cboAnzeige8.Name = "cboAnzeige8"
        Me.cboAnzeige8.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige8.TabIndex = 16
        '
        'cboAnzeige7
        '
        Me.cboAnzeige7.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige7.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige7.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige7.FormattingEnabled = True
        Me.cboAnzeige7.Location = New System.Drawing.Point(81, 194)
        Me.cboAnzeige7.Name = "cboAnzeige7"
        Me.cboAnzeige7.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige7.TabIndex = 14
        '
        'cboAnzeige6
        '
        Me.cboAnzeige6.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige6.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige6.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige6.FormattingEnabled = True
        Me.cboAnzeige6.Location = New System.Drawing.Point(81, 165)
        Me.cboAnzeige6.Name = "cboAnzeige6"
        Me.cboAnzeige6.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige6.TabIndex = 12
        '
        'cboAnzeige5
        '
        Me.cboAnzeige5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige5.FormattingEnabled = True
        Me.cboAnzeige5.Location = New System.Drawing.Point(81, 136)
        Me.cboAnzeige5.Name = "cboAnzeige5"
        Me.cboAnzeige5.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige5.TabIndex = 10
        '
        'cboAnzeige4
        '
        Me.cboAnzeige4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige4.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige4.FormattingEnabled = True
        Me.cboAnzeige4.Location = New System.Drawing.Point(81, 107)
        Me.cboAnzeige4.Name = "cboAnzeige4"
        Me.cboAnzeige4.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige4.TabIndex = 8
        '
        'cboAnzeige3
        '
        Me.cboAnzeige3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige3.FormattingEnabled = True
        Me.cboAnzeige3.Location = New System.Drawing.Point(81, 78)
        Me.cboAnzeige3.Name = "cboAnzeige3"
        Me.cboAnzeige3.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige3.TabIndex = 6
        '
        'cboAnzeige2
        '
        Me.cboAnzeige2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige2.FormattingEnabled = True
        Me.cboAnzeige2.Location = New System.Drawing.Point(81, 49)
        Me.cboAnzeige2.Name = "cboAnzeige2"
        Me.cboAnzeige2.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige2.TabIndex = 4
        '
        'cboAnzeige1
        '
        Me.cboAnzeige1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboAnzeige1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAnzeige1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnzeige1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAnzeige1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.cboAnzeige1.FormattingEnabled = True
        Me.cboAnzeige1.Location = New System.Drawing.Point(81, 20)
        Me.cboAnzeige1.Name = "cboAnzeige1"
        Me.cboAnzeige1.Size = New System.Drawing.Size(216, 23)
        Me.cboAnzeige1.TabIndex = 2
        '
        'btnDefault
        '
        Me.btnDefault.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDefault.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnDefault.Location = New System.Drawing.Point(411, 399)
        Me.btnDefault.Name = "btnDefault"
        Me.btnDefault.Size = New System.Drawing.Size(109, 26)
        Me.btnDefault.TabIndex = 151
        Me.btnDefault.TabStop = False
        Me.btnDefault.Text = "Laden"
        Me.ToolTip1.SetToolTip(Me.btnDefault, "gespeichtere Konfiguration einlesen")
        Me.btnDefault.UseCompatibleTextRendering = True
        Me.btnDefault.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.Image = Global.Gewichtheben.My.Resources.Resources.Refresh_icon
        Me.btnRefresh.Location = New System.Drawing.Point(411, 431)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(109, 26)
        Me.btnRefresh.TabIndex = 152
        Me.btnRefresh.TabStop = False
        Me.btnRefresh.Text = "Aktualisieren"
        Me.btnRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip1.SetToolTip(Me.btnRefresh, "alle Anzeigen aktualisieren")
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'frmScreens
        '
        Me.AcceptButton = Me.btnAssign
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(534, 472)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.btnDefault)
        Me.Controls.Add(Me.pnlScreens)
        Me.Controls.Add(Me.btnAssign)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnIdentify)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmScreens"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Anzeigegeräte zuweisen"
        Me.pnlScreens.ResumeLayout(False)
        Me.pnlScreens.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnIdentify As System.Windows.Forms.Button
    Friend WithEvents btnAssign As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents pnlScreens As Panel
    Friend WithEvents cboAnzeige1 As ComboBox
    Friend WithEvents cboAnzeige9 As ComboBox
    Friend WithEvents cboAnzeige8 As ComboBox
    Friend WithEvents cboAnzeige7 As ComboBox
    Friend WithEvents cboAnzeige6 As ComboBox
    Friend WithEvents cboAnzeige5 As ComboBox
    Friend WithEvents cboAnzeige4 As ComboBox
    Friend WithEvents cboAnzeige3 As ComboBox
    Friend WithEvents cboAnzeige2 As ComboBox
    Friend WithEvents btnDefault As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btnRefresh As Button
    Friend WithEvents btn9 As Button
    Friend WithEvents btn8 As Button
    Friend WithEvents btn7 As Button
    Friend WithEvents btn6 As Button
    Friend WithEvents btn5 As Button
    Friend WithEvents btn4 As Button
    Friend WithEvents btn3 As Button
    Friend WithEvents btn2 As Button
    Friend WithEvents btn1 As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents set1 As Button
    Friend WithEvents set9 As Button
    Friend WithEvents set8 As Button
    Friend WithEvents set7 As Button
    Friend WithEvents set6 As Button
    Friend WithEvents set5 As Button
    Friend WithEvents set4 As Button
    Friend WithEvents set3 As Button
    Friend WithEvents set2 As Button
    Friend WithEvents set14 As Button
    Friend WithEvents btn14 As Button
    Friend WithEvents Label14 As Label
    Friend WithEvents cboAnzeige14 As ComboBox
    Friend WithEvents set13 As Button
    Friend WithEvents btn13 As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents cboAnzeige13 As ComboBox
    Friend WithEvents set12 As Button
    Friend WithEvents btn12 As Button
    Friend WithEvents Label12 As Label
    Friend WithEvents cboAnzeige12 As ComboBox
    Friend WithEvents set11 As Button
    Friend WithEvents btn11 As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents cboAnzeige11 As ComboBox
    Friend WithEvents set10 As Button
    Friend WithEvents btn10 As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents cboAnzeige10 As ComboBox
End Class
