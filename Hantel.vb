﻿
Imports System.Text.RegularExpressions
Imports MySqlConnector

Module Hantelbeladung
    Friend Hantel As New myHantel

    Class myHantel
        Const oWidth As Integer = 889
        Const oHeight As Integer = 500
        Private lstGewichte As List(Of KeyValuePair(Of Integer, Integer))
        Private dv As DataView
        Private bs As BindingSource
        Private dummy As DataView
        ' private Variablen der abgeleiteten Klasse für Update
        Private _HLast As Integer, _Stange As String, _Sex As String, _JG As Integer, _BigDisc As Boolean

        Friend Sub Change_HantelDefaults()
            Get_Data(True)
        End Sub
        Friend Sub Change_Hantel(Optional HLast As Integer = 0, Optional Stange As String = "", Optional Sex As String = "", Optional JG As Integer = 0, Optional BigDisc As Boolean = False, Optional CurrentHeber As Boolean = True)
            RaiseEvent Hantel_Changed(HLast, Stange, Sex, JG, BigDisc, CurrentHeber)
        End Sub
        Friend Event Hantel_Changed(HLast As Integer, Stange As String, Sex As String, JG As Integer, BigDisc As Boolean, CurrentHeber As Boolean)
        Friend Sub Clear_Hantel()
            RaiseEvent Hantel_Clear()
        End Sub
        Friend Event Hantel_Clear()
        Public Sub New()
            lstGewichte = New List(Of KeyValuePair(Of Integer, Integer))
            Get_Data()
        End Sub

        Friend Function Get_Stange(Sex As String, JG As Integer, HLast As Integer, Optional HStange As String = "") As String
            ' liest aus Wettkampf.Hantelstange die Hantelstange für den Heber aus 
            ' oder verwendet die angegebene HStange ("_" als Delimiter)
            ' x = 5-kg-Stange
            ' k = 7-kg-Stange
            ' w = 15-kg-Stange
            ' m = 20-kg-Stange
            ' s = Auswahl nach Sex
            ' a = Auswahl nach AK
            If String.IsNullOrEmpty(HStange) Then HStange = Wettkampf.Hantelstange
            If String.IsNullOrEmpty(HStange) Then Return String.Empty

            Dim Stange() As String = Split(HStange, "_")
            Select Case HLast
                Case < 5
                    Return String.Empty
                Case < 7
                    If Not Stange.Contains("x") Then Return String.Empty
                    Return "x"
                Case < 17
                    If Not Regex.Match(HStange, "[kx]").Success Then Return String.Empty
                    Return Stange(Stange.Count - 1)
                Case < 21 ' keine 5-kg-Stange möglich
                    If Not Stange.Contains("k") Then
                        If Regex.Match(HStange, "[wkx]").Success Then Return "w"
                        Return String.Empty
                    End If
                    Return "k"
                Case Else
                    If Stange.Contains("s") Then Return Sex
                    If Stange.Contains("a") Then
                        If Sex = "w" OrElse Year(Wettkampf.Datum) - JG < 16 Then
                            Return "w" ' weiblich oder unter AK 15
                        Else
                            Return "m" ' männlich oder ab AK 15
                        End If
                    End If
            End Select
            Return String.Empty
        End Function

        Private Function Get_Beladung(HLast As Integer, Optional Stange As String = "", Optional BigDisc As Boolean = False) As Dictionary(Of Integer, Integer)
            ' Hantelstange wird in frmWettkampf angelegt

            ' DrawHantel gibt Nothing zurück,wenn nicht verfügbar
            '   --> es wird nichts gezeichnet 
            '   --> pPanel.Parent.Visible = False setzen

            Dim ix As Integer
            Select Case Stange
                Case "m"
                    ix = 13
                Case "w"
                    ix = 14
                Case "k"
                    ix = 15
                Case "x"
                    ix = 16
                Case Else
                    Return Nothing ' Get_Stange gibt String.Empty zurück, wenn nicht verfügbar 
            End Select

            Dim ScheibenGewicht As Double = HLast

            'Scheibengewichte mit "0" anlegen
            lstGewichte.Clear()
            For i As Integer = 0 To 10
                lstGewichte.Add(New KeyValuePair(Of Integer, Integer)(i, 0))
            Next

            Dim pos = bs.Find("ID", ix)
            ScheibenGewicht -= CDbl(dv(pos)!Gewicht) ' Hantelstange von Hantelgewicht abziehen
            lstGewichte.Add(New KeyValuePair(Of Integer, Integer)(ix, CInt(dv(pos)!Dicke))) ' Hantel-Hülse m/w/k, Dicke (=Länge)
            pos = bs.Find("ID", 17)
            lstGewichte.Add(New KeyValuePair(Of Integer, Integer)(17, CInt(dv(pos)!Dicke))) ' Hantel-Anschlag
            pos = bs.Find("ID", 18)
            lstGewichte.Add(New KeyValuePair(Of Integer, Integer)(18, CInt(dv(pos)!Dicke))) ' Hantel-Griff

            ' Verschluss hinzufügen (0,5 bis 2 kg außen)
            If BigDisc Then
                If ScheibenGewicht > 6 Then
                    lstGewichte(4) = New KeyValuePair(Of Integer, Integer)(4, 1)
                End If
            ElseIf ScheibenGewicht > 9 Then
                lstGewichte(4) = New KeyValuePair(Of Integer, Integer)(4, 1)
            End If
            ScheibenGewicht -= lstGewichte(4).Value * CDbl(dv(4)!Gewicht) * 2 ' Verschluss von Hantelgewicht abziehen

            For i = 10 To 0 Step -1
                If i <> 4 Then
                    lstGewichte(i) = New KeyValuePair(Of Integer, Integer)(i, CInt(Math.Floor(ScheibenGewicht / (CDbl(dv(i)!Gewicht) * 2)))) ' größtmögliche Scheiben errechnen
                    If lstGewichte(i).Value > 0 Then
                        If BigDisc AndAlso i < 7 AndAlso i > 0 Then
                            ' bei BigDisc für alle Scheiben zwischen 5 und 1 kg prüfen, ob Dummy vorhanden
                            Dim d = bs.Find("ID", i + 100)
                            If d > -1 AndAlso CBool(dv(d)!available) Then
                                ' dem Gewicht entsprechenden Dummy gefunden
                                ' --> erste große Scheibe mit Dummy ersetzen
                                lstGewichte(10) = New KeyValuePair(Of Integer, Integer)(i + 100, lstGewichte(i).Value)
                                ' --> Anzahl für kleine Scheibe auf 0 setzen
                                lstGewichte(i) = New KeyValuePair(Of Integer, Integer)(i, 0)
                                ' --> Scheibengewicht von Hantelgwicht abziehen
                                ScheibenGewicht -= lstGewichte(10).Value * CDbl(dv(d)!Gewicht) * 2
                                BigDisc = False
                            Else
                                ' kein passender Dummy
                                For x = i + 99 To 101 Step -1
                                    ' nächstkleineren Dummy suchen
                                    d = bs.Find("ID", x)
                                    If d > -1 AndAlso CBool(dv(d)!available) Then
                                        ' dem Gewicht entsprechenden Dummy gefunden
                                        ' --> erste große Scheibe mit Dummy ersetzen
                                        lstGewichte(10) = New KeyValuePair(Of Integer, Integer)(x, lstGewichte(i).Value)
                                        ' --> Anzahl für kleine Scheibe auf 0 setzen
                                        lstGewichte(i) = New KeyValuePair(Of Integer, Integer)(i, 0)
                                        ' --> Scheibengewicht von Hantelgwicht abziehen
                                        ScheibenGewicht -= lstGewichte(10).Value * CDbl(dv(d)!Gewicht) * 2
                                        BigDisc = False
                                        Exit For
                                    Else
                                        ' Anzeige nicht möglich
                                        Return Nothing
                                    End If
                                Next
                            End If
                        Else
                            ScheibenGewicht -= lstGewichte(i).Value * CDbl(dv(i)!Gewicht) * 2 ' Scheibengewicht von Hantelgwicht abziehen
                            BigDisc = False
                        End If
                        If ScheibenGewicht = 0 Then Exit For
                    End If
                End If
            Next

            Return lstGewichte.ToDictionary(Function(p) p.Key, Function(p) p.Value)

        End Function

        '' Hantel.DrawHantel gibt ein Panel mit der gezeichneten Hantelbeladung zurück
        Friend Function DrawHantel(ParentPanel As Panel, HLast As Integer, Optional Stange As String = "", Optional Sex As String = "", Optional JG As Integer = 0, Optional BigDisc As Boolean = False) As String

            Dim dicGewichte As New Dictionary(Of Integer, Integer) ' (Position, Index in dv)

            ParentPanel.Controls.Clear()

            If String.IsNullOrEmpty(Stange) Then Stange = Get_Stange(Sex, JG, HLast)

            _HLast = HLast
            _Stange = Stange
            _Sex = Sex
            _JG = JG
            _BigDisc = BigDisc

            If HLast > 0 AndAlso Not String.IsNullOrEmpty(Stange) Then
                dicGewichte = Get_Beladung(HLast, Stange, BigDisc)
                If IsNothing(dicGewichte) Then Return Nothing
            Else
                Return Nothing
            End If

            Dim Steck_O_Mat As New List(Of String)
            Dim pWidth As Integer = ParentPanel.Width, pHeight As Integer = ParentPanel.Height
            Dim pnl As Panel
            Dim lbl As Label
            Dim fc = pHeight / oHeight
            Dim x As Integer
            Dim xs As Integer 'xs = x-Pos Scheiben
            Dim xt As Integer 'xt = x-Pos Text
            Dim mPanel As New Panel 'Hintergrund als Container

            With mPanel
                .BackColor = Color.Black
                .Size = New Size(pWidth, pHeight)
            End With

            Dim ow = 0 'Länge der Hantel
            For i = 13 To 18
                If dicGewichte.ContainsKey(i) Then ow += dicGewichte(i)
            Next
            x = CInt(pWidth - ow * fc) \ 2  'x-Pos der Hantel
            xt = x

#Region "Hantel zeichnen"
            For i = 18 To 13 Step -1
                If dicGewichte.ContainsKey(i) AndAlso dicGewichte(i) > 0 Then
                    pnl = New Panel
                    Dim pos = bs.Find("ID", i)
                    DrawDisc(mPanel, pnl, fc, pos, x, pHeight)
                    If i < 17 Then xs = pnl.Left
                    x += CInt(CInt(dv(pos)!Dicke) * fc) ' - 1 'x-Pos for next pnl
                    If i < 17 Then Steck_O_Mat.Add(String.Concat("Ix=", i, ",Bez=", dv(pos)!Bezeichnung.ToString, ",Gew=", dv(pos)!Gewicht.ToString.Replace(",", "."), ",Anz=1"))
                End If
            Next
#End Region

#Region "Scheiben zeichnen"
            x = xs + 1 'die Zahl bestimmt den Abstand der Scheiben vom Hantelanschlag
            Dim lst = dicGewichte.ToList ' zum Iterieren
            For i = 10 To 0 Step -1
                For c = 1 To lst(i).Value
                    pnl = New Panel
                    Dim pos = bs.Find("ID", lst(i).Key)
                    DrawDisc(mPanel, pnl, fc, pos, x, pHeight)
                    x += CInt(CInt(dv(pos)!Dicke) * fc) + 1 'x-Pos for next pnl; die Zahl bestimmt den Abstand der Scheiben 
                    Steck_O_Mat.Add(String.Concat("Ix=", lst(i).Key, ",Bez=", dv(pos)!Bezeichnung.ToString, ",Gew=", dv(pos)!Gewicht.ToString.Replace(",", "."), ",Anz=", lst(i).Value))
                Next
            Next
#End Region

#Region "Beschriftung Hantelstange"
            Dim y = pHeight \ 2
            lbl = New Label
            With lbl
                .Name = "lbl1"
                .BorderStyle = BorderStyle.None
                .ForeColor = Color.White
                .BackColor = Color.Transparent
                .AutoSize = True
                For i = 13 To 16
                    If dicGewichte.ContainsKey(i) Then .Text = dv(i - 2)!Gewicht.ToString + " kg" ' id 11 und 12 fehlen
                Next
                .Font = New Font("Microsoft Sans Serif", 50 * CSng(fc), GraphicsUnit.Point)
                .Parent = mPanel
                mPanel.Controls.Add(lbl)
                y = CInt(y - (.Height * 1.7))
                .Location = New Point(xt + CInt((dicGewichte(18) * fc - .Width) / 2), y)
                .BringToFront()
            End With
            lbl = New Label
            With lbl
                .Name = "lbl2"
                .BorderStyle = BorderStyle.None
                .ForeColor = Color.White
                .BackColor = Color.Transparent
                .AutoSize = True
                .Text = "Stange"
                .Font = New Font("Microsoft Sans Serif", 32 * CSng(fc), GraphicsUnit.Point)
                .Parent = mPanel
                mPanel.Controls.Add(lbl)
                y -= .Height
                .Location = New Point(xt + CInt((dicGewichte(18) * fc - .Width) / 2), y)
                .BringToFront()
            End With
#End Region

#Region "Beschriftung Hantellast"
            y = pHeight \ 2
            lbl = New Label
            With lbl
                .Name = "lbl3"
                .BorderStyle = BorderStyle.None
                .ForeColor = Color.White
                .BackColor = Color.Transparent
                .AutoSize = True
                .Text = "Last"
                .Font = New Font("Microsoft Sans Serif", 32 * CSng(fc), GraphicsUnit.Point)
                .Parent = mPanel
                mPanel.Controls.Add(lbl)
                y = CInt(y + (.Height * 1.1))
                .Location = New Point(xt + CInt((dicGewichte(18) * fc - .Width) / 2), y)
                y += .Height
                .BringToFront()
            End With
            lbl = New Label
            With lbl
                .Name = "lbl4"
                .BorderStyle = BorderStyle.None
                .ForeColor = Color.White
                .BackColor = Color.Transparent
                .AutoSize = True
                .Text = HLast.ToString + " kg"
                .Font = New Font("Microsoft Sans Serif", 50 * CSng(fc), GraphicsUnit.Point)
                .Parent = mPanel
                mPanel.Controls.Add(lbl)
                .Location = New Point(xt + CInt((dicGewichte(18) * fc - .Width) / 2), y)
                .BringToFront()
            End With
#End Region

            ParentPanel.Controls.Add(mPanel)
            Return Join(Steck_O_Mat.ToArray, ";")


        End Function

        Private Sub DrawDisc(ByRef mPanel As Panel, ByRef pnl As Panel, fc As Double, i As Integer, x As Integer, pHeight As Integer)
            With pnl
                .Name = "pnl" + i.ToString
                .BackColor = Color.FromName(dv(i)!Farbe.ToString)
                .Size = New Size(CInt(CInt(dv(i)!Dicke) * fc), CInt(CInt(dv(i)!Durchmesser) * fc))
                .Location = New Point(x, (pHeight - pnl.Height) \ 2)
                .BorderStyle = BorderStyle.FixedSingle
                .Parent = mPanel
                mPanel.Controls.Add(pnl)
                .BringToFront()
            End With

        End Sub

        Private Sub Get_Data(Optional Update As Boolean = False)
            Dim dt As New DataTable
            Try
                Using conn As New MySqlConnection((User.ConnString))
                    Dim Query = "SELECT * FROM hantel ORDER BY ID;"
                    Dim cmd = New MySqlCommand(Query, conn)
                    conn.Open()
                    dt.Load(cmd.ExecuteReader)
                End Using
            Catch ex As Exception
                dt.Columns.Add(New DataColumn With {.ColumnName = "ID", .DataType = GetType(Integer)})
                dt.Columns.Add(New DataColumn With {.ColumnName = "Bezeichnung", .DataType = GetType(String)})
                dt.Columns.Add(New DataColumn With {.ColumnName = "Gewicht", .DataType = GetType(Double)})
                dt.Columns.Add(New DataColumn With {.ColumnName = "Farbe", .DataType = GetType(String)})
                dt.Columns.Add(New DataColumn With {.ColumnName = "Durchmesser", .DataType = GetType(Integer)})
                dt.Columns.Add(New DataColumn With {.ColumnName = "Dicke", .DataType = GetType(Integer)})
                dt.Columns.Add(New DataColumn With {.ColumnName = "available", .DataType = GetType(Boolean)})
                dt.Rows.Add({0, "Scheibe", 0.5, "White", 135, 12, True})
                dt.Rows.Add({1, "Scheibe", 1.0, "Green", 160, 15, True})
                dt.Rows.Add({2, "Scheibe", 1.5, "Yellow", 175, 18, True})
                dt.Rows.Add({3, "Scheibe", 2.0, "Blue", 190, 19, True})
                dt.Rows.Add({4, "Verschluss", 2.5, "Silver", 90, 60, True})
                dt.Rows.Add({5, "Scheibe", 2.5, "Red", 210, 19, True})
                dt.Rows.Add({6, "Scheibe", 5.0, "White", 230, 26, True})
                dt.Rows.Add({7, "Scheibe", 10.0, "Green", 450, 34, True})
                dt.Rows.Add({8, "Scheibe", 15.0, "Yellow", 450, 42, True})
                dt.Rows.Add({9, "Scheibe", 20.0, "Blue", 450, 54, True})
                dt.Rows.Add({10, "Scheibe", 25.0, "Red", 450, 67, True})
                dt.Rows.Add({13, "Hantel_m", 20.0, "Gray", 50, 400, True})
                dt.Rows.Add({14, "Hantel_w", 15.0, "Gray", 50, 400, True})
                dt.Rows.Add({15, "Hantel_k", 7.0, "Gray", 50, 400, True})
                dt.Rows.Add({16, "Hantel_x", 5.0, "Gray", 50, 400, True})
                dt.Rows.Add({17, "Hantel_Anschlag", 0.00, "Gray", 70, 20, True})
                dt.Rows.Add({18, "Hantel_Stange", 0.00, "Gray", 25, 200, True})
                dt.Rows.Add({101, "Dummy", 1.0, "LimeGreen", 450, 90, True})
                dt.Rows.Add({102, "Dummy", 1.5, "Ivory", 450, 90, False})
                dt.Rows.Add({103, "Dummy", 2.0, "DodgerBlue", 450, 90, True})
                dt.Rows.Add({105, "Dummy", 2.5, "OrangeRed", 450, 90, True})
                dt.Rows.Add({106, "Dummy", 5.0, "WhiteSmoke", 450, 101, True})
            End Try

            dv = New DataView(dt)
            bs = New BindingSource With {.DataSource = dv}
            bs.Filter = "available = True"
            bs.Sort = "ID"

            dummy = New DataView(dt)
            dummy.RowFilter = "Bezeichnung = 'Dummy'"
            dummy.Sort = "Gewicht"

            ' Hanteln neu zeichnen
            If Update Then
                For Each key In IsFormPresent({"Bohle", "Hantelbeladung"})
                    Try
                        DrawHantel(CType(dicScreens(key.Key).Form.Controls("pnlHantel"), Panel), _HLast, _Stange, _Sex, _JG, _BigDisc)
                    Catch ex As Exception
                    End Try
                Next
            End If

        End Sub

    End Class

End Module
