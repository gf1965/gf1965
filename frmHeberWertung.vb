﻿Imports System.ComponentModel

Public Class frmHeberWertung
    Property Closable As Boolean
    Dim dicLampen As New Dictionary(Of Integer, Label)
    Dim dicHLast As New Dictionary(Of Integer, Label)
    Dim dicVersuch As New Dictionary(Of Integer, Label)

    Private Delegate Sub DelegateBoolean(Value As Boolean)
    Private Delegate Sub DelegateString(Value As String)
    Private Delegate Sub DelegateIntegerColor(Index As Integer, Value As Color)
    Private Delegate Sub DelegateSubName(Nachname As String, Vorname As String)

    Private Sub frmHeberWertung_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        RemoveHandler Anzeige.Lampe_Changed_Sofort, AddressOf Change_Lampe
        RemoveHandler ForeColors_Change, AddressOf Wertung_Clear
        RemoveHandler Heber.Name_Changed, AddressOf Set_Namen
        RemoveHandler Leader.Scoring_Changed, AddressOf Wertung_Changed
        RemoveHandler Heber.Versuch_Changed, AddressOf Set_Versuch
    End Sub
    Private Sub frmHeberWertung_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Using sc As New ScreenScale
            Dim mon = sc.GetDeviceNumber(Screen.FromControl(Me))
            If dicScreens.ContainsKey(mon) Then dicScreens.Remove(mon)
        End Using
    End Sub
    Private Sub frmHeberWertung_Load(sender As Object, e As EventArgs) Handles Me.Load
        AddHandler Anzeige.Lampe_Changed_Sofort, AddressOf Change_Lampe
        AddHandler ForeColors_Change, AddressOf Wertung_Clear
        AddHandler Heber.Name_Changed, AddressOf Set_Namen
        AddHandler Leader.Scoring_Changed, AddressOf Wertung_Changed
        AddHandler Heber.Versuch_Changed, AddressOf Set_Versuch

        dicLampen.Add(1, lblLampe1)
        dicLampen.Add(2, lblLampe2)
        dicLampen.Add(3, lblLampe3)

        dicHLast.Add(1, lblLast1)
        dicHLast.Add(2, lblLast2)
        dicHLast.Add(3, lblLast3)

        dicVersuch.Add(1, lblVersuch1)
        dicVersuch.Add(2, lblVersuch2)
        dicVersuch.Add(3, lblVersuch3)

        lblName.Text = String.Empty

        If Closable Then
            FormBorderStyle = FormBorderStyle.FixedSingle
        Else
            FormBorderStyle = FormBorderStyle.None
        End If
    End Sub
    Private Sub frmHeberWertung_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        For i = 1 To 3
            dicVersuch(i).ForeColor = Ansicht_Options.Shade_Color
            dicHLast(i).Visible = False
        Next
        lblVersuch.Text = String.Empty
        lblName.Text = String.Empty
        Visible = True
    End Sub

    Private Sub Wertung_Clear(HeberPresent As Boolean)
        Try
            If InvokeRequired Then
                Dim d As New DelegateBoolean(AddressOf Wertung_Clear)
                Invoke(d, New Object() {HeberPresent})
            Else
                If Not HeberPresent Then ' Wertungen zurücksetzen
                    For i = 1 To 3
                        dicLampen(i).ForeColor = Ansicht_Options.Shade_Color
                        dicHLast(i).Visible = False
                        dicVersuch(i).ForeColor = Ansicht_Options.Shade_Color
                    Next
                    'Visible = False
                End If
            End If
        Catch es As Exception
        End Try
    End Sub

    Private Sub Change_Lampe(Index As Integer, Color As Color)
        Try
            If InvokeRequired Then
                Dim d As New DelegateIntegerColor(AddressOf Change_Lampe)
                Invoke(d, New Object() {Index, Color})
            Else
                Visible = True
                dicLampen(Index).ForeColor = Color
                dicLampen(Index).Refresh()
            End If
        Catch es As Exception
        End Try
    End Sub

    Private Sub Set_Namen(Optional Nachname As String = "", Optional Vorname As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSubName(AddressOf Set_Namen)
                Invoke(d, New Object() {Nachname, Vorname})
            Else
                lblName.Text = UCase(Nachname) + " " + Vorname
                lblName.BringToFront()
                lblVersuch.Text = Leader.Durchgang
                For i = 1 To 3
                    dicHLast(i).Visible = False
                    dicVersuch(i).ForeColor = Ansicht_Options.Shade_Color
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Sub Set_Versuch(Optional value As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateString(AddressOf Set_Versuch)
                Invoke(d, New Object() {value})
            Else
                lblVersuch.Text = value & ". " & Leader.Durchgang
                Set_Wertung(bsResults.Find("Teilnehmer", Heber.Id))
            End If
        Catch ex As Exception
            LogMessage("HeberWertung: Set_Versuch: " & ex.Message, ex.StackTrace)
        Finally
            Refresh()
        End Try
    End Sub
    Private Sub Wertung_Changed(Row As Integer,
                               Col As String,
                               Optional HLast As Integer = -1,
                               Optional Wertung As Integer = 0,
                               Optional Note As Double = 0,
                               Optional ZK As String = "",
                               Optional IsCurrent As Boolean = True)

        If Not IsCurrent Then Return

        Try
            Set_Wertung(Row)
        Catch ex As Exception
            LogMessage("HeberWertung: Wertung_Changed: " & ex.Message, ex.StackTrace)
        Finally
            Refresh()
        End Try
    End Sub

    Private Sub Set_Wertung(row As Integer)
        ' dicHLast sind die Zahlen
        ' dicVersuch sind die Kreise

        Dim Durchgang = Leader.Table.Substring(0, 1)
        Dim Results = dvResults(row).Row
        For i = 1 To 3
            With dicHLast(i)
                .Visible = Not IsDBNull(Results(Durchgang & "W_" & i)) AndAlso CInt(Results(Durchgang & "W_" & i)) <> 0
                If .Visible Then
                    If CInt(Results(Durchgang & "W_" & i)) = -2 Then
                        .Text = "---"
                        .BackColor = Ansicht_Options.Shade_Color
                    Else
                        .Text = Results(Durchgang & "_" & i).ToString            '
                        .BackColor = If(CInt(Results(Durchgang & "W_" & i)) = 1, Color.White, Color.Red)
                    End If
                    .ForeColor = If(CInt(Results(Durchgang & "W_" & i)) = 1, Color.Black, Color.White)
                End If
            End With
            dicVersuch(i).ForeColor = If(IsDBNull(Results(Durchgang & "W_" & i)) OrElse CInt(Results(Durchgang & "W_" & i)) = -2 OrElse CInt(Results(Durchgang & "W_" & i)) = 0, Ansicht_Options.Shade_Color, If(CInt(Results(Durchgang & "W_" & i)) = 1, Color.White, Color.Red))
        Next
    End Sub

End Class