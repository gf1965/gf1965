﻿
Imports System.ComponentModel

Public Class frmGruppenJahrgang
    Dim Bereich As String = "Gruppen_JG"
    Dim Query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim da As MySqlDataAdapter
    Dim dtMeldung As New DataTable
    Dim dt1 As New DataTable
    Dim dt2 As New DataTable
    Dim dt3 As New DataTable
    Dim dt4 As New DataTable
    Dim dt5 As New DataTable
    Dim dtGewichtsklassen As New DataTable
    Dim dtGruppen As New DataTable
    Dim ds As New DataSet
    Dim dv As DataView

    Dim AK_w As String = "4"
    Dim AK_m As String = "10"

    Dim bs(5) As BindingSource
    Dim loading As Boolean
    Dim draging As Boolean

    Dim LayoutChanged_W As Boolean
    Dim dgvGruppe As New Dictionary(Of Integer, DataGridView)
    Dim tblGruppe As New Dictionary(Of Integer, DataTable)
    Dim lblGruppen As New Dictionary(Of Integer, Label)
    Dim cboGruppen As New Dictionary(Of Integer, ComboBox)
    Dim chkBH As New Dictionary(Of Integer, CheckBox)
    Dim pkDate As New Dictionary(Of Integer, DateTimePicker)
    Dim pkDateA As New Dictionary(Of Integer, DateTimePicker)
    Dim pkTimeR As New Dictionary(Of Integer, DateTimePicker)
    Dim pkTimeS As New Dictionary(Of Integer, DateTimePicker)
    Dim pkTimeA As New Dictionary(Of Integer, DateTimePicker)
    Dim cboBohle As New Dictionary(Of Integer, ComboBox)
    Dim pnlBohle As New Dictionary(Of Integer, Panel)

    Dim Datum As Date
    Dim nonNumberEntered As Boolean = False
    Dim ctrl As Boolean
    Dim shift As Boolean

    Dim dgvSource As Integer
    Dim dgvTarget As Integer
    Dim dgvSelection As String

    WithEvents ScrollTimer As New Timer With {.Interval = 40}
    Dim ScrollBereich As Integer = 100 ' Abstand vom Rand wo das autoscrollen losgeht
    Dim ScrollStep As Integer = 10 '* immer gleich 10 Pixel
    Dim ScrollRichtung As Direction = Direction.None
    Enum Direction
        Left
        Right
        None
    End Enum

    'Optionen
    Dim ScrollSpeed As Integer = 30
    Dim AlternatingRowsDefaultCellStyle_BackColor As Color = Color.OldLace
    Dim RowsDefaultCellStyle_BackColor As Color = Color.LightCyan
    Dim Zeit_pro_Hebung As Integer = 6
    Dim Zeit_Rundung As Integer = 5
    Dim GruppenBez() As String = {"Feder", "Leicht", "Mittel", "Halbschwer", "Schwer"}
    '*** wenn Geschlecht aufsteigend (m/w) sortiert ist
    'Dim divGruppen() As Integer = {10, 7, -1} 
    'Dim maxGruppen() As Integer = {5, 2, -1}  
    '*** wenn Geschlecht absteigend (w/m) sortiert ist
    Dim divGruppen() As Integer = {7, 10, -1} 'max. Heber pro Gruppe (-1 für gemischt)
    Dim maxGruppen() As Integer = {2, 5, -1} 'max. Gruppen pro Geschlecht (-1 für gemischt)

    Private Sub frmGruppen_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If mnuSpeichern.Checked Then
            Select Case MsgBox("Änderungen vor dem Schließen speichern?", CType(MsgBoxStyle.YesNoCancel + MsgBoxStyle.Question, MsgBoxStyle), "Gruppen-Einteilung")
                Case MsgBoxResult.Yes
                    Save()
                Case MsgBoxResult.Cancel
                    e.Cancel = True
            End Select
        End If
    End Sub

    Private Sub frmGruppen_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmGruppen_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        ScrollTimer.Interval = ScrollSpeed

        dgvGruppe.Add(1, dgvGK1)
        dgvGruppe.Add(2, dgvGK2)
        dgvGruppe.Add(3, dgvGK3)
        dgvGruppe.Add(4, dgvGK4)
        dgvGruppe.Add(5, dgvGK5)
        tblGruppe.Add(1, dt1)
        tblGruppe.Add(2, dt2)
        tblGruppe.Add(3, dt3)
        tblGruppe.Add(4, dt4)
        tblGruppe.Add(5, dt5)
        lblGruppen.Add(1, lblG1)
        lblGruppen.Add(2, lblG2)
        lblGruppen.Add(3, lblG3)
        lblGruppen.Add(4, lblG4)
        lblGruppen.Add(5, lblG5)
        cboGruppen.Add(1, cbo1)
        cboGruppen.Add(2, cbo2)
        cboGruppen.Add(3, cbo3)
        cboGruppen.Add(4, cbo4)
        cboGruppen.Add(5, cbo5)
        chkBH.Add(1, chkBH1)
        chkBH.Add(2, chkBH2)
        chkBH.Add(3, chkBH3)
        chkBH.Add(4, chkBH4)
        chkBH.Add(5, chkBH5)
        pkDate.Add(1, date1)
        pkDate.Add(2, date2)
        pkDate.Add(3, date3)
        pkDate.Add(4, date4)
        pkDate.Add(5, date5)
        pkDateA.Add(1, dateA1)
        pkDateA.Add(2, dateA2)
        pkDateA.Add(3, dateA3)
        pkDateA.Add(4, dateA4)
        pkDateA.Add(5, dateA5)
        pkTimeR.Add(1, timeR1)
        pkTimeR.Add(2, timeR2)
        pkTimeR.Add(3, timeR3)
        pkTimeR.Add(4, timeR4)
        pkTimeR.Add(5, timeR5)
        pkTimeS.Add(1, timeS1)
        pkTimeS.Add(2, timeS2)
        pkTimeS.Add(3, timeS3)
        pkTimeS.Add(4, timeS4)
        pkTimeS.Add(5, timeS5)
        pkTimeA.Add(1, timeA1)
        pkTimeA.Add(2, timeA2)
        pkTimeA.Add(3, timeA3)
        pkTimeA.Add(4, timeA4)
        pkTimeA.Add(5, timeA5)
        cboBohle.Add(1, cboB1)
        cboBohle.Add(2, cboB2)
        cboBohle.Add(3, cboB3)
        cboBohle.Add(4, cboB4)
        cboBohle.Add(5, cboB5)
        pnlBohle.Add(1, pnl1)
        pnlBohle.Add(2, pnl2)
        pnlBohle.Add(3, pnl3)
        pnlBohle.Add(4, pnl4)
        pnlBohle.Add(5, pnl5)

        If Screen.FromControl(Me).Bounds.Width < Me.Width Then '= 1252 Then
            Width = frmMain.Width - 60
            Height = 585
            pnlForm.ScrollControlIntoView(Panel6)
        Else
            Width = 1252 '1394
            Height = 571 '575
        End If
        Dim PaddingLeft As Integer = CInt((Screen.FromControl(Me).WorkingArea.Width - Width) / 2)
        If Math.Abs(Screen.FromControl(Me).WorkingArea.X + Width + PaddingLeft) <= PaddingLeft Then Left = Screen.FromControl(Me).WorkingArea.X + PaddingLeft

    End Sub

    Private Sub frmGruppen_SizeChanged(sender As Object, e As EventArgs) Handles Me.SizeChanged
        'With Me
        '    If .Width >= 1394 Then .Width = 1394
        '    If .Height >= 578 Then .Height = 578
        '    If .Height <= 388 Then .Height = 388
        'End With
    End Sub

    Private Sub frmGruppen_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Cursor = Cursors.WaitCursor
        loading = True

        Using conn As New MySqlConnection(MySqlConnString)
            Try
                conn.Open()
                'Datenquelle für Gewichtsklassen
                Query = "SELECT * FROM gewichtsklassen;"
                cmd = New MySqlCommand(Query, conn)
                dtGewichtsklassen.Load(cmd.ExecuteReader())
                'Datenquelle für Wettkampf
                Query = "SELECT Datum FROM Wettkampf WHERE idWettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                reader = cmd.ExecuteReader()
                reader.Read()
                Datum = CDate(reader(0))
                reader.Close()
                'Datenquelle für cboJahrgang
                Query = "select distinct year(Geburtstag) as Jahrgang, Wettkampf " &
                        "from meldung, athleten " &
                        "where Teilnehmer = idTeilnehmer " &
                        "having Wettkampf = " & Wettkampf.ID & " order by Jahrgang desc;"
                cmd = New MySqlCommand(Query, conn)
                reader = cmd.ExecuteReader()
                While reader.Read
                    lstJahrgang.Items.Add(reader(0))
                End While
                reader.Close()
                ''Datenquelle für cboGeschlecht --> in Eigenschaften:Liste festgelegt
                'Query = "select distinct Geschlecht, Wettkampf " & _
                '        "from meldung, athleten " & _
                '        "where Teilnehmer = idTeilnehmer " & _
                '        "having Wettkampf = " & Wettkampf.ID & " order by Geschlecht desc;"
                'cmd = New MySqlCommand(Query, conn)
                'reader = cmd.ExecuteReader()
                'While reader.Read
                '    cboGeschlecht.Items.Add(reader(0))
                'End While
                'reader.Close()
                'If cboGeschlecht.Items.Count > 1 Then cboGeschlecht.Items.Add("gemischt")
                'falls vorhanden: dtMeldung, cboGruppen & dtGruppen einlesen
                Read_Data(conn)
            Catch ex As MySqlException
                Stop
                'Finally
                '    conn.Close()
            End Try
        End Using

        'Gewichtsklassen einlesen
        bgwGK.RunWorkerAsync()

        For i As Integer = 1 To 5
            'Grids erstellen
            tblGruppe(i) = dtMeldung.Clone
            Build_Gruppen(dgvGruppe(i))
            bs(i) = New BindingSource With {.DataSource = tblGruppe(i)}
            dgvGruppe(i).DataSource = bs(i)
            Disable_Controls(i)
        Next

        loading = False
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Read_Data(conn As MySqlConnection)
        'Datenquelle für Meldung
        Query = "select Wettkampf, Nachname, Vorname, Gewicht, Wiegen, Gruppe, " &
                "concat(Altersklasse,'_',left(Geschlecht,1)) as AK, GK ,year(Geburtstag) as Jahrgang, Geschlecht " &
                "from meldung, athleten, altersklassen " &
                "where Teilnehmer = idTeilnehmer " &
                "and year(now())-year(Geburtstag) between von and bis " &
                "having Wettkampf = " & Wettkampf.ID & " order by Nachname, Vorname asc;"
        cmd = New MySqlCommand(Query, conn)
        dtMeldung.Load(cmd.ExecuteReader())
        'Datenquelle für Gruppen
        Query = "select * from gruppen where Wettkampf = " & Wettkampf.ID & " order by Gruppe asc;"
        da = New MySqlDataAdapter(Query, conn)
        da.Fill(ds, "dtGruppen")
        'gespeicherte Gruppen ermitteln
        Dim groups_saved As Integer = ds.Tables("dtGruppen").DefaultView.ToTable(True, "Gruppe").Rows.Count
        'gespeicherte Gruppen einlesen
        For c As Integer = 1 To groups_saved
            If Not cboGruppe.Items.Contains(c) Then cboGruppe.Items.Add(c)
        Next
        'Anzahl Gruppen aus 'meldung' ermitteln
        Dim rows As DataRow()
        Dim _Filter As String = ""
        Dim divider As Integer
        Dim groups As Integer = 1
        Dim sum As Integer = 0
        For i As Integer = 0 To lstJahrgang.Items.Count - 1
            For c As Integer = 0 To cboGeschlecht.Items.Count - 1
                If c < 2 Then
                    _Filter = "Jahrgang = '" + lstJahrgang.Items(i).ToString + "'"
                    If _Filter > "" Then _Filter += " AND "
                    _Filter += "Geschlecht = '" + cboGeschlecht.Items(c).ToString + "'"
                    rows = dtMeldung.Select(_Filter)
                    divider = divGruppen(c)
                    groups = CInt(Int(rows.Count / divider))
                    If groups > maxGruppen(c) Then groups = maxGruppen(c)
                    sum += groups
                End If
            Next
        Next
        'Gruppen einlesen
        If groups_saved > 0 Then sum = groups_saved
        For i As Integer = 1 To 5
            'keine gespeicherten Gruppen
            For c As Integer = 1 To sum
                If Not cboGruppen(i).Items.Contains(c) Then cboGruppen(i).Items.Add(c)
            Next
        Next
    End Sub

    Private Sub cboFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboJahrgang.TextChanged, cboGeschlecht.SelectedIndexChanged
        Filter_Changed()
    End Sub

    Private Sub Filter_Changed()
        If loading Then Exit Sub
        loading = True
        Cursor = Cursors.WaitCursor

        For i As Integer = 1 To 5
            Disable_Controls(i)
        Next
        cboGruppe.SelectedIndex = -1

        Dim _Filter As String = ""
        If cboGeschlecht.SelectedIndex > -1 And cboGeschlecht.SelectedIndex < 2 Then
            'getrennte Geschlechter
            _Filter = "Geschlecht = '" + cboGeschlecht.Text + "'"
        Else
            cboGeschlecht.SelectedIndex = 2
        End If

        If cboJahrgang.Text > "" Then
            If _Filter > "" Then _Filter += " AND "
            Dim jg As String() = Split(cboJahrgang.Text, ", ")
            _Filter += "("
            For i As Integer = 0 To jg.Count - 1
                If i > 0 Then _Filter += " OR "
                _Filter += "Jahrgang = '" + jg(i) + "'"
            Next
            _Filter += ")"
        End If

        Dim rows As DataRow() = dtMeldung.Select(_Filter)

        Dim _Sort As String = "Gewicht"
        If Not IsDBNull(rows(0)("Wiegen")) Then _Sort = "Wiegen"

        For i As Integer = 1 To 5
            dgvGruppe(i).Columns("KG").DataPropertyName = _Sort
        Next
        _Sort += " ASC"

        rows = dtMeldung.Select(_Filter, _Sort)

        Dim divider As Integer
        If cboGeschlecht.SelectedIndex > -1 And cboGeschlecht.SelectedIndex < 2 Then divider = divGruppen(cboGeschlecht.SelectedIndex)
        Dim groups As Integer = 1
        If divider > 0 Then groups = CInt(Int(rows.Count / divider))
        If cboGeschlecht.SelectedIndex > -1 And cboGeschlecht.SelectedIndex < 2 Then
            If groups > maxGruppen(cboGeschlecht.SelectedIndex) Then groups = maxGruppen(cboGeschlecht.SelectedIndex)
        End If
        Dim append As Integer = CInt(Int((rows.Count - groups * divider) / groups))
        Dim remains As Integer = rows.Count - (divider + append) * groups

        Dim dv As DataView
        Dim gr As New DataTable

        If Not CInt(rows(0)("Gruppe")) = 0 Then
            'Gruppen-Einteilung in Meldung vorhanden
            dv = New DataView(dtMeldung)
            dv.RowFilter = _Filter
            dv.Sort = "Gruppe ASC, " + _Sort
            If cboGeschlecht.SelectedIndex > -1 And cboGeschlecht.SelectedIndex < 2 Then
                gr = dv.ToTable(True, "Gruppe")
                groups = gr.Rows.Count
                rows = dtMeldung.Select(_Filter, "Gruppe ASC, " + _Sort)
            End If
        End If

        'benutzte Tabellen
        Dim t() As Integer = {groups}
        Set_Tables(t, groups)

        If CInt(rows(0)("Gruppe")) = 0 Or (cboGeschlecht.SelectedIndex = -1 Or cboGeschlecht.SelectedIndex = 2) Then
            'keine Gruppen in Meldung vorhanden oder Geschlecht nicht festgelegt
            Dim _start As Integer = 0
            'Position in rows
            Dim c As Integer = 0
            For i As Integer = 1 To groups
                c += divider + append
                'Rest auf alle Tabellen gleichmäßig verteilen
                If remains > 0 Then
                    c += 1
                    remains -= 1
                End If
                'einlesen
                For x As Integer = _start To c - 1
                    tblGruppe(t(i - 1)).ImportRow(rows(x))
                Next
                'neue Startposition in rows
                _start = c
                With lblGruppen(t(i - 1))
                    '.BackColor = Color.MediumSeaGreen
                    .Text = GruppenBez(t(i - 1) - 1) & " (" & tblGruppe(t(i - 1)).Rows.Count.ToString & ")"
                    'pnlBohle(t(i - 1)).BackColor = .BackColor
                End With
                Enable_Controls(t(i - 1))
            Next
        ElseIf Not CInt(rows(0)("Gruppe")) = 0 Then
            'Gruppen-Einteilung in Meldung vorhanden
            For i As Integer = 0 To groups - 1
                dv = New DataView(dtMeldung)
                dv.RowFilter = _Filter + " AND Gruppe = " + gr.Rows(i)("Gruppe").ToString
                dv.Sort = "Gruppe ASC, " + _Sort
                tblGruppe(t(i)) = dv.ToTable()
                bs(t(i)).DataSource = tblGruppe(t(i))
                dgvGruppe(t(i)).DataSource = bs(t(i))
                cboGruppen(t(i)).SelectedItem = tblGruppe(t(i)).Rows(0)("Gruppe")
                With lblGruppen(t(i))
                    '.BackColor = Color.MediumSeaGreen
                    .Text = GruppenBez(t(i) - 1) & " (" & tblGruppe(t(i)).Rows.Count.ToString & ")"
                    'pnlBohle(t(i)).BackColor = .BackColor
                End With
                Enable_Controls(t(i))
            Next
        End If
        Cursor = Cursors.Default
        loading = False
    End Sub

    Private Sub Set_Tables(ByRef t() As Integer, groups As Integer)
        Select Case groups
            Case 1
                t = {3}
            Case 2
                t = {2, 5}
            Case 3
                t = {2, 3, 5}
            Case 4
                t = {2, 3, 4, 5}
            Case 5
                t = {1, 2, 3, 4, 5}
        End Select
    End Sub

    Private Sub Build_Gruppen(grid As DataGridView)
        Try
            With grid
                .AutoGenerateColumns = False
                .RowHeadersVisible = False
                .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor 'Color.OldLace
                .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor 'Color.LightCyan
                .DefaultCellStyle.SelectionBackColor = RowsDefaultCellStyle_BackColor
                .DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.ControlText)
                Dim ColWidth() As Integer = {35, 65, 60, 46, 40, 35}
                Dim ColBez() As String = {"Teilnehmer", "Nachname", "Vorname", "KG", "GK", "Gruppe"}
                Dim ColData() As String = {"Teilnehmer", "Nachname", "Vorname", "Gewicht", "GK", "Gruppe"}
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Teilnehmer"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .Resizable = CType(False, DataGridViewTriState), .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .Resizable = CType(False, DataGridViewTriState), .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "KG", .Resizable = CType(False, DataGridViewTriState)})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "GK", .Resizable = CType(False, DataGridViewTriState)})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Gruppe"})
                For i As Integer = 0 To ColWidth.Count - 1
                    .Columns(i).Width = ColWidth(i)
                    .Columns(i).HeaderText = ColBez(i)
                    .Columns(i).DataPropertyName = ColData(i)
                    .Columns(i).ReadOnly = True
                Next
                '.Columns("Teilnehmer").Visible = False
                .Columns("Gruppe").Visible = False
                .Columns("KG").DefaultCellStyle.Format = "##0.00"
                .Columns("KG").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns("GK").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns("Gruppe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Sort(.Columns("KG"), ListSortDirection.Ascending)
            End With
            'Format_Grid("Gruppen", grid)
        Catch ex As Exception
            Stop
        End Try
    End Sub

    Private Sub lblGruppe_Click(sender As Object, e As EventArgs) Handles lblGruppe.Enter
        cboGruppe.Focus()
    End Sub

    Private Sub lblJahrgang_Click(sender As Object, e As EventArgs) Handles lblJahrgang.Enter
        cboJahrgang.Focus()
    End Sub

    Private Sub lblGeschlecht_Click(sender As Object, e As EventArgs) Handles lblGeschlecht.Enter
        cboGeschlecht.Focus()
    End Sub

    Private Sub bgwGK_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwGK.DoWork
        'bei Einteilung vor dem Wiegen GK ermitteln
        Dim dr As DataRow
        Dim found() As DataRow
        For Each dr In dtMeldung.Rows
            If IsDBNull(dr!GK) Then
                found = dtGewichtsklassen.Select(dr("AK").ToString.Replace("ü", "ue") + " > " + dr("Gewicht").ToString.Replace(",", "."), "id ASC")
                If found.Count = 0 Then
                    '+Klasse
                    found = dtGewichtsklassen.Select(dr("AK").ToString.Replace("ü", "ue") + " > 0", "id DESC")
                    dr!GK = "+" + found(0)(dr("AK").ToString.Replace("ü", "ue")).ToString
                Else
                    '-Klasse
                    dr!GK = "-" + found(0)(dr("AK").ToString.Replace("ü", "ue")).ToString
                End If
            End If
        Next
    End Sub

    Private Sub mnuSpeichern_Click(sender As Object, e As EventArgs) Handles mnuSpeichern.Click
        Save()
    End Sub

    Private Function Save() As Boolean
        Save = True
        Dim groups As DataRow() = dtMeldung.Select("Gruppe = 0")
        If groups.Count > 0 Then
            'Teilnehmer ohne Grupppenzuweisung
            MessageBox.Show(groups.Count.ToString + " gemeldete Heber ohne Gruppen-Zuweisung." + vbNewLine + _
                            "Die Heber erscheinen nicht in der Gruppen-Liste.", "Gruppen-Einteilung", _
                            MessageBoxButtons.OK, MessageBoxIcon.Information)
            Save = False
            Exit Function
        End If
        Dim dv As New DataView(dtMeldung, "Gruppe > 0", "Gruppe ASC", DataViewRowState.CurrentRows)
        Dim dt As DataTable = dv.ToTable(True, "Gruppe")
        For i As Integer = 1 To dt.Rows.Count
            If i <> CInt(dt.Rows(i - 1)("Gruppe")) Then
                'fehlende Gruppe in der Reihenfolge
                MessageBox.Show("Gruppen müssen in aufsteigender Reihenfolge zugewiesen werden." + vbNewLine + _
                                "Fehlerhafte Reihenfolge vor Gruppe " + groups(i - 1)("Gruppe").ToString + ".", "Gruppen-Einteilung", _
                                MessageBoxButtons.OK, MessageBoxIcon.Information)
                Save = False
                Exit Function
            End If
        Next

        Cursor = Cursors.WaitCursor
        Using conn As New MySqlConnection(MySqlConnString)
            Dim changes As DataTable = ds.Tables("dtGruppen").GetChanges()
            If Not IsNothing(changes) Then
                Try
                    conn.Open()
                    Dim cb As MySqlCommandBuilder = New MySqlCommandBuilder(da)
                    da.Update(changes)
                    ds.Tables("dtGruppen").Clear()
                Catch ex As MySqlException
                    MessageBox.Show("Änderungen in 'Gruppen' wurden nicht gespeichert." + vbNewLine + ex.ToString, "Datenbank-Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Save = False
                    'Finally
                    '    conn.Close()
                End Try
            End If

            changes = Nothing
            changes = dtMeldung.GetChanges(DataRowState.Modified)
            If Not IsNothing(changes) Then
                Try
                    'conn.Open()
                    For i As Integer = 0 To changes.Rows.Count - 1
                        'Query = "UPDATE meldung SET Gruppe = " + changes.Rows(i)("Gruppe").ToString + " WHERE idMeldung = " + changes.Rows(i)("idMeldung").ToString
                        cmd = New MySqlCommand(Query, conn)
                        reader = cmd.ExecuteReader
                        reader.Close()
                    Next
                    dtMeldung.Clear()
                    'Meldung und Gruppen einlesen
                    Read_Data(conn)
                Catch ex As MySqlException
                    MessageBox.Show("Änderungen in 'Meldung' wurden nicht gespeichert." + vbNewLine + ex.ToString, "Datenbank-Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Save = False
                    'Finally
                    '    conn.Close()
                End Try
            End If
        End Using

        If Save Then
            mnuSpeichern.Checked = False
            cboGruppe.Enabled = True
        End If
        Cursor = Cursors.Default

    End Function

    Private Sub cboGruppen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbo1.SelectedIndexChanged, cbo2.SelectedIndexChanged, cbo3.SelectedIndexChanged, cbo4.SelectedIndexChanged, cbo5.SelectedIndexChanged
        If loading Then Exit Sub
        Dim Ix As Integer
        For i As Integer = 1 To 5
            If sender.Equals(cboGruppen(i)) Then
                Ix = i
                Exit For
            End If
        Next
        If dgvGruppe(Ix).Rows.Count = 0 Then cboGruppen(Ix).SelectedIndex = -1
        If cboGruppen(Ix).SelectedIndex = -1 Then Exit Sub

        Dim result As DialogResult = DialogResult.Yes
        'bestehender Wert von Gruppe
        Dim group_old As String = dgvGruppe(Ix).Rows(0).Cells("Gruppe").Value.ToString
        'prüfen, ob Gruppe in dtMeldung vorhanden
        Dim rows As DataRow() = dtMeldung.Select("Gruppe = " + cboGruppen(Ix).Text, "Gruppe ASC")
        If rows.Count > 0 Then
            'Gruppe bereits zugewiesen
            'If cboGruppen(Ix).Items.Contains(CStr(CInt(rows(0)("Gruppe")) + 1)) Then
            If Not mnuMeldungen.Checked Then
                result = MessageBox.Show("Gruppe " + cboGruppen(Ix).Text + " wurde bereits zugewiesen." + vbNewLine + "Alle nachfolgenden Gruppen verschieben?", "Gruppen-Einteilung", _
                                         MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
            End If
            If result = DialogResult.Yes Then
                'Gruppen verschieben
                Dim group As Integer
                rows = dtMeldung.Select("Gruppe >= " + cboGruppen(Ix).Text, "Gruppe ASC")
                'alle Meldungen, deren Gruppen-Nummer gleich oder größer ist als die neu ausgewählte
                For i As Integer = 0 To rows.Count - 1
                    group = CInt(rows(i)("Gruppe")) + 1
                    rows(i)("Gruppe") = group
                Next
                draging = True
            ElseIf result = DialogResult.Cancel Then
                loading = True
                If group_old = "0" Then
                    cboGruppen(Ix).SelectedIndex = -1
                Else
                    cboGruppen(Ix).Text = group_old
                End If
                loading = False
                Exit Sub
            ElseIf result = DialogResult.No Then
                draging = True
            End If
            'End If
        End If

        'Gruppe zuweisen
        For i As Integer = 0 To dgvGruppe(Ix).Rows.Count - 1
            'rows = dtMeldung.Select("idMeldung = " + dgvGruppe(Ix).Rows(i).Cells("Meldung").Value.ToString)
            If CInt(rows(0)("Gruppe")) = 0 OrElse CInt(rows(0)("Gruppe")) <> CInt(cboGruppen(Ix).Text) Then mnuSpeichern.Checked = True
            rows(0)("Gruppe") = cboGruppen(Ix).Text 'den Zeilen in der Tabelle die Gruppe zuweisen
            dgvGruppe(Ix).Rows(i).Cells("Gruppe").Value = cboGruppen(Ix).Text 'den Zeilen im Grid die Gruppe zuweisen
        Next

        cboGeschlecht.Enabled = True
        cboJahrgang.Enabled = True

        If draging Then
            Filter_Changed()
            draging = False
        Else
            Fill_Controls(CInt(cboGruppen(Ix).Text))
        End If

    End Sub

    Private Sub cboJahrgang_Click(sender As Object, e As EventArgs) Handles cboJahrgang.Click
        With lstJahrgang
            If .Visible Then
                .Visible = False
                cboGruppe.Focus()
            ElseIf Not .Visible Then
                .Visible = True
                .Focus()
            End If
        End With
    End Sub

    Private Sub cboGruppe_LostFocus(sender As Object, e As EventArgs) Handles cboGruppe.LostFocus
        cboGruppe.Tag = cboGruppe.Text
    End Sub

    Private Sub cboGruppe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGruppe.SelectedIndexChanged
        If loading Then Exit Sub

        'If mnuSpeichern.Checked Then
        '    Select Case MessageBox.Show("Werden gespeicherte Gruppen aufgerufen, gehen alle nicht gespeicherten Änderungen verloren. Änderungen erst speichern?", _
        '                                "Gruppen-Einteilung", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information)
        '        Case DialogResult.Yes
        '            If Save() = False Then
        '                cboGruppe.Text = cboGruppe.Tag.ToString
        '                Exit Sub
        '            End If
        '        Case DialogResult.Cancel
        '            loading = True
        '            cboGruppe.SelectedIndex = -1
        '            loading = False
        '            Exit Sub
        '    End Select
        'End If

        loading = True
        Cursor = Cursors.WaitCursor

        Dim dv As DataView = New DataView(dtMeldung)
        Dim c As Integer

        'Form in Ausgangszustand versetzen
        For i As Integer = 1 To 5
            Disable_Controls(i)
        Next

        'festlegen, ob Melde_Gewicht oder Wiege_Gewicht verwendet wird
        Dim _Filter As String = "Gruppe = " + cboGruppe.Text
        Dim _Sort As String = "Gewicht"
        Dim rows As DataRow() = dtMeldung.Select(_Filter, _Sort)
        If Not IsDBNull(rows(0)("Wiegen")) Then _Sort = "Wiegen"
        For i As Integer = 1 To 5
            dgvGruppe(i).Columns("KG").DataPropertyName = _Sort
        Next
        _Sort += " ASC"
        rows = dtMeldung.Select(_Filter, "Gruppe ASC, " + _Sort)

        'Jahrgang und Geschlecht in cbo
        dv.RowFilter = _Filter
        dv.Sort = _Sort
        Dim gr As DataTable = dv.ToTable(True, "Gruppe", "Jahrgang", "Geschlecht")
        cboJahrgang.Items(0) = gr.Rows(0)("Jahrgang")
        cboJahrgang.SelectedIndex = 0
        cboGeschlecht.Text = gr.Rows(0)("Geschlecht").ToString

        'prüfe auf mehrere Gruppen im JG auf Grundlage von cboGeschlecht und cboJahrgang
        _Filter = "Geschlecht = '" + cboGeschlecht.Text + "' AND Jahrgang = '" + cboJahrgang.Text + "'"
        dv.RowFilter = _Filter
        dv.Sort = _Sort
        gr = dv.ToTable(True, "Gruppe", "Jahrgang", "Geschlecht")
        Dim groups As Integer = gr.Rows.Count

        'benutzte Tabellen
        Dim t() As Integer = {groups}
        Set_Tables(t, groups)
        'bei mehreren Gruppen die in cboGruppe ausgewählte suchen
        For c = 0 To gr.Rows.Count - 1
            If CInt(gr.Rows(c)("Gruppe")) = CInt(cboGruppe.Text) Then Exit For
        Next

        'Gruppe einlesen
        _Filter = "Gruppe = " + cboGruppe.Text
        dv.RowFilter = _Filter
        dv.Sort = _Sort
        tblGruppe(t(c)) = dv.ToTable()
        bs(t(c)).DataSource = tblGruppe(t(c))
        dgvGruppe(t(c)).DataSource = bs(t(c))
        Enable_Controls(t(c))
        With cboGruppen(t(c))
            If Not .Items.Contains(CInt(cboGruppe.Text)) Then .Items.Add(cboGruppe.Text)
            .Text = cboGruppe.Text 'CInt(tblGruppe(t(c)).Rows(0)("Gruppe")).ToString
        End With
        With lblGruppen(t(c))
            '.BackColor = Color.MediumSeaGreen
            .Text = GruppenBez(t(c) - 1) & " (" & tblGruppe(t(c)).Rows.Count.ToString & ")"
            'pnlBohle(t(c)).BackColor = .BackColor
        End With
        Fill_Controls(CInt(cboGruppe.Text), , t(c))
        Me.Cursor = Cursors.Default
        loading = False
    End Sub

    Private Sub Disable_Controls(i As Integer)
        'For i As Integer = 1 To 5
        pkDate(i).Enabled = False
        pkDateA(i).Enabled = False
        pkTimeR(i).Enabled = False
        pkTimeS(i).Enabled = False
        pkTimeA(i).Enabled = False
        chkBH(i).Enabled = False
        pkDate(i).Format = DateTimePickerFormat.Custom
        pkDate(i).CustomFormat = " "
        pkDate(i).Value = Now
        pkDateA(i).Format = DateTimePickerFormat.Custom
        pkDateA(i).CustomFormat = " "
        pkDateA(i).Value = Now
        pkTimeR(i).CustomFormat = " "
        pkTimeS(i).CustomFormat = " "
        pkTimeA(i).CustomFormat = " "
        cboBohle(i).Enabled = False
        tblGruppe(i).Clear()
        'lblGruppen(i).BackColor = Color.SteelBlue
        lblGruppen(i).Text = GruppenBez(i - 1)
        cboGruppen(i).Enabled = False
        cboGruppen(i).SelectedIndex = -1
        cboBohle(i).SelectedIndex = -1
        chkBH(i).Checked = False
        'pnlBohle(i).BackColor = Color.SteelBlue
        'Next
    End Sub

    Private Sub Enable_Controls(ix As Integer, Optional value As Boolean = True)
        If ix > 0 Then
            Try
                pkDate(ix).Enabled = value
                pkDateA(ix).Enabled = Wettkampf.Technikwertung
                pkTimeR(ix).Enabled = value
                pkTimeS(ix).Enabled = value
                pkTimeA(ix).Enabled = Wettkampf.Technikwertung
                chkBH(ix).Enabled = value
                cboGruppen(ix).Enabled = value
                If value Then
                    pkDate(ix).Format = DateTimePickerFormat.Short
                    pkDate(ix).Value = Datum
                    If pkDateA(ix).Enabled Then
                        pkDateA(ix).Format = DateTimePickerFormat.Short
                        pkDateA(ix).Value = Datum
                    End If
                End If
                cboBohle(ix).Enabled = value
                cboBohle(ix).SelectedIndex = 0
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub Fill_Controls(ByVal group As Integer, Optional ByVal delete As Boolean = False, Optional ByVal index As Integer = 0)
        'prüfen, ob Gruppe in weightlft.gruppen bereits angelegt
        Dim found As DataRow() = ds.Tables("dtGruppen").Select("Gruppe = " & group)
        If found.Count = 0 And Not delete Then
            'neue Gruppe anlegen
            Dim row As DataRow = ds.Tables("dtGruppen").NewRow
            row("Wettkampf") = Wettkampf.ID
            row("Gruppe") = group
            row("Datum") = Datum
            If Wettkampf.Technikwertung Then row("DatumA") = Datum
            row("Blockheben") = False
            row("Bohle") = 1
            ds.Tables("dtGruppen").Rows.Add(row)
        ElseIf found.Count = 1 And Not delete Then
            'vorhandene Gruppe aktualisieren
            If Not IsDBNull(found(0)("Datum")) Then
                pkDate(index).Format = DateTimePickerFormat.Short
                pkDate(index).Value = CDate(found(0)("Datum"))
            Else
                pkDate(index).Format = DateTimePickerFormat.Custom
                pkDate(index).CustomFormat = " "
                pkDate(index).Value = Now
            End If
            If Not IsDBNull(found(0)("DatumA")) Then
                pkDateA(index).Format = DateTimePickerFormat.Short
                pkDateA(index).Value = CDate(found(0)("DatumA"))
            Else
                pkDateA(index).Format = DateTimePickerFormat.Custom
                pkDateA(index).CustomFormat = " "
                pkDateA(index).Value = Now
            End If
            If Not IsDBNull(found(0)("Reissen")) Then
                pkTimeR(index).CustomFormat = "HH:mm"
                pkTimeR(index).Text = found(0)("Reissen").ToString
            Else
                pkTimeR(index).CustomFormat = " "
            End If
            If Not IsDBNull(found(0)("Stossen")) Then
                pkTimeS(index).CustomFormat = "HH:mm"
                pkTimeS(index).Text = found(0)("Stossen").ToString
            Else
                pkTimeS(index).CustomFormat = " "
            End If
            If Not IsDBNull(found(0)("Athletik")) Then
                pkTimeA(index).CustomFormat = "HH:mm"
                pkTimeA(index).Text = found(0)("Athletik").ToString
            Else
                pkTimeA(index).CustomFormat = " "
            End If
            chkBH(index).Checked = CBool(found(0)("Blockheben"))
            cboBohle(index).Text = found(0)("Bohle").ToString
        ElseIf found.Count = 1 And delete Then
            'vorhandene Gruppe löschen
            ds.Tables("dtGruppen").Rows.Remove(found(0))
        End If
    End Sub

    Private Sub Blockheben_CheckedChanged(sender As Object, e As EventArgs) Handles chkBH1.CheckedChanged, chkBH2.CheckedChanged, chkBH3.CheckedChanged, chkBH4.CheckedChanged, chkBH5.CheckedChanged
        If loading Then Exit Sub
        Dim Ix As Integer = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        Dim group() As DataRow = ds.Tables("dtGruppen").Select("Gruppe = " & cboGruppen(Ix).Text)
        group(0)("Blockheben") = chkBH(Ix).Checked
        mnuSpeichern.Checked = True
        dgvGruppe(Ix).Focus()
    End Sub

    Private Sub pkDatum_GotFocus(sender As Object, e As EventArgs) Handles date1.GotFocus, date2.GotFocus, date3.GotFocus, date4.GotFocus, date5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        If pkDate(Ix).Format = DateTimePickerFormat.Short Then Exit Sub
        pkDate(Ix).Format = DateTimePickerFormat.Short
        pkDate(Ix).Value = Datum
    End Sub

    Private Sub pkDatumA_GotFocus(sender As Object, e As EventArgs) Handles dateA1.GotFocus, dateA2.GotFocus, dateA3.GotFocus, dateA4.GotFocus, dateA5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        If pkDateA(Ix).Format = DateTimePickerFormat.Short Then Exit Sub
        pkDateA(Ix).Format = DateTimePickerFormat.Short
        pkDateA(Ix).Value = Datum
    End Sub

    Private Sub pkDatum_ValueChanged(sender As Object, e As EventArgs) Handles date1.ValueChanged, date2.ValueChanged, date3.ValueChanged, date4.ValueChanged, date5.ValueChanged
        If loading Then Exit Sub
        Dim Ix As Integer = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        Dim group() As DataRow = ds.Tables("dtGruppen").Select("Gruppe = " & cboGruppen(Ix).Text)
        Try
            If group.Count > 0 Then group(0)("Datum") = pkDate(Ix).Value
        Catch ex As Exception
            Stop
        End Try
        mnuSpeichern.Checked = True
    End Sub

    Private Sub pkDatumA_ValueChanged(sender As Object, e As EventArgs) Handles dateA1.ValueChanged, dateA2.ValueChanged, dateA3.ValueChanged, dateA4.ValueChanged, dateA5.ValueChanged
        If loading Then Exit Sub
        Dim Ix As Integer = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        Dim group() As DataRow = ds.Tables("dtGruppen").Select("Gruppe = " & cboGruppen(Ix).Text)
        Try
            If group.Count > 0 Then group(0)("DatumA") = pkDateA(Ix).Value
        Catch ex As Exception
            Stop
        End Try
        mnuSpeichern.Checked = True
    End Sub

    Private Sub pkTimeA_GotFocus(sender As Object, e As EventArgs) Handles timeA1.GotFocus, timeA2.GotFocus, timeA3.GotFocus, timeA4.GotFocus, timeA5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        If pkTimeA(Ix).CustomFormat = "HH:mm" Then Exit Sub
        pkTimeA(Ix).CustomFormat = "HH:mm"
        pkTimeA(Ix).Text = Format(Now, "HH:mm")
    End Sub

    Private Sub pkTimeR_GotFocus(sender As Object, e As EventArgs) Handles timeR1.GotFocus, timeR2.GotFocus, timeR3.GotFocus, timeR4.GotFocus, timeR5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        If pkTimeR(Ix).CustomFormat = "HH:mm" Then Exit Sub
        pkTimeR(Ix).CustomFormat = "HH:mm"
        pkTimeR(Ix).Text = Format(Now, "HH:mm")
    End Sub

    Private Sub pkTimeS_GotFocus(sender As Object, e As EventArgs) Handles timeS1.GotFocus, timeS2.GotFocus, timeS3.GotFocus, timeS4.GotFocus, timeS5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        If pkTimeS(Ix).CustomFormat = "HH:mm" Then Exit Sub
        pkTimeS(Ix).CustomFormat = "HH:mm"
        pkTimeS(Ix).Text = Format(Now, "HH:mm")
    End Sub

    Private Sub pkTimeA_ValueChanged(sender As Object, e As EventArgs) Handles timeA1.ValueChanged, timeA2.ValueChanged, timeA3.ValueChanged, timeA4.ValueChanged, timeA5.ValueChanged
        If loading Then Exit Sub
        Dim i As Integer
        For i = 1 To 5
            If sender.Equals(pkTimeA(i)) Then Exit For
        Next
        If cboGruppen(i).SelectedIndex = -1 Then
            If mnuKonflikte.Checked Then MsgBox("Zum Festlegen der Anfangszeiten muss eine Gruppe zugewiesen sein.", MsgBoxStyle.Information, "Gruppen-Einteilung")
            pkTimeA(i).CustomFormat = " "
            Exit Sub
        End If
        Dim group() As DataRow = ds.Tables("dtGruppen").Select("Gruppe = " & cboGruppen(i).Text)
        If pkTimeA(i).Text = " " Then pkTimeA(i).CustomFormat = "HH:mm"
        group(0)("Athletik") = Format(pkTimeA(i).Value, "HH:mm")
        mnuSpeichern.Checked = True
    End Sub

    Private Sub pkTimeR_ValueChanged(sender As Object, e As EventArgs) Handles timeR1.ValueChanged, timeR2.ValueChanged, timeR3.ValueChanged, timeR4.ValueChanged, timeR5.ValueChanged
        If loading Then Exit Sub
        Dim i As Integer
        For i = 1 To 5
            If sender.Equals(pkTimeR(i)) Then Exit For
        Next
        If cboGruppen(i).SelectedIndex = -1 Then
            If mnuKonflikte.Checked Then MsgBox("Zum Festlegen der Anfangszeiten muss eine Gruppe zugewiesen sein.", MsgBoxStyle.Information, "Gruppen-Einteilung")
            pkTimeR(i).CustomFormat = " "
            Exit Sub
        End If
        Dim group() As DataRow = ds.Tables("dtGruppen").Select("Gruppe = " & cboGruppen(i).Text)
        group(0)("Reissen") = Format(pkTimeR(i).Value, "HH:mm")
        mnuSpeichern.Checked = True
        'If pkTimeS(i).Text = " " And pkTimeA(i).Text = " " Then
        '    If Not mnuMeldungen.Checked Then
        '        If MsgBox("Anfangszeiten für die weiteren Gruppen vorschlagen?", CType(MsgBoxStyle.YesNo + MsgBoxStyle.Question, MsgBoxStyle), "Gruppen-Einteilung") = MsgBoxResult.No Then
        '            Exit Sub
        '        End If
        '    End If
        'End If
        'wenn die erste Gruppe geändet wird, Zeiten für die nächsten kalkulieren
        Dim minutes As Integer = tblGruppe(i).Rows.Count * Zeit_pro_Hebung
        minutes = ((minutes \ Zeit_Rundung) + CInt((minutes Mod Zeit_Rundung) / Zeit_Rundung)) * Zeit_Rundung
        Dim _zeit1 As TimeSpan = TimeSpan.Parse(pkTimeR(i).Text)
        Dim _zeit2 As New TimeSpan(minutes \ 60, minutes Mod 60, 0)
        Dim _zeit3 As TimeSpan = _zeit1 + _zeit2
        pkTimeS(i).Text = _zeit3.ToString
        _zeit3 += _zeit2
        pkTimeA(i).Text = _zeit3.ToString
    End Sub

    Private Sub pkTimeS_ValueChanged(sender As Object, e As EventArgs) Handles timeS1.ValueChanged, timeS2.ValueChanged, timeS3.ValueChanged, timeS4.ValueChanged, timeS5.ValueChanged
        If loading Then Exit Sub
        Dim i As Integer
        For i = 1 To 5
            If sender.Equals(pkTimeS(i)) Then Exit For
        Next
        If cboGruppen(i).SelectedIndex = -1 Then
            If mnuKonflikte.Checked Then MsgBox("Zum Festlegen der Anfangszeiten muss eine Gruppe zugewiesen sein.", MsgBoxStyle.Information, "Gruppen-Einteilung")
            pkTimeS(i).CustomFormat = " "
            Exit Sub
        End If
        Dim group() As DataRow = ds.Tables("dtGruppen").Select("Gruppe = " & cboGruppen(i).Text)
        If pkTimeS(i).Text = " " Then pkTimeS(i).CustomFormat = "HH:mm"
        group(0)("Stossen") = Format(pkTimeS(i).Value, "HH:mm")
        mnuSpeichern.Checked = True
    End Sub

    Private Sub cboBohle_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboB1.SelectedValueChanged, cboB2.SelectedValueChanged, cboB3.SelectedValueChanged, cboB4.SelectedValueChanged, cboB5.SelectedValueChanged
        If loading Then Exit Sub
        Dim Ix As Integer '= CInt(Strings.Right(Me.ActiveControl.Name, 1))
        For Ix = 1 To 5
            If sender.Equals(cboBohle(Ix)) Then Exit For
        Next
        If cboGruppen(Ix).Text > "" Then
            Dim group() As DataRow = ds.Tables("dtGruppen").Select("Gruppe = " & cboGruppen(Ix).Text)
            group(0)("Bohle") = cboBohle(Ix).SelectedValue
        End If
        mnuSpeichern.Checked = True
    End Sub

    Private Sub lstJahrgang_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles lstJahrgang.ItemCheck
        If loading Then Exit Sub
        loading = True
        Static Old_Index As Integer
        With lstJahrgang
            If ctrl Then
            ElseIf shift Then
                If Old_Index <= e.Index Then
                    For i As Integer = Old_Index To e.Index
                        .SetItemCheckState(i, e.NewValue)
                    Next
                ElseIf Old_Index > e.Index Then
                    For i As Integer = e.Index To Old_Index
                        .SetItemCheckState(i, e.NewValue)
                    Next
                End If
            Else
                For i As Integer = 0 To .Items.Count - 1
                    If i <> e.Index Then
                        .SetItemCheckState(i, CheckState.Unchecked)
                    End If
                Next
            End If
            .Visible = False
            cboJahrgang.Focus()
        End With
        Old_Index = e.Index
    End Sub

    Private Sub lstJahrgang_KeyDown(sender As Object, e As KeyEventArgs) Handles lstJahrgang.KeyDown
        Select Case e.KeyCode
            Case Keys.ControlKey
                ctrl = True
            Case Keys.ShiftKey
                shift = True
            Case Keys.Escape
                lstJahrgang.Visible = False
        End Select
    End Sub

    Private Sub lstJahrgang_KeyUp(sender As Object, e As KeyEventArgs) Handles lstJahrgang.KeyUp
        Select Case e.KeyCode
            Case Keys.ControlKey
                ctrl = False
            Case Keys.ShiftKey
                shift = False
        End Select
    End Sub

    Private Sub lstJahrgang_Leave(sender As Object, e As EventArgs) Handles lstJahrgang.Leave
        If Not cboJahrgang.Focused Then lstJahrgang.Visible = False
        loading = False
    End Sub

    Private Sub lstJahrgang_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstJahrgang.SelectedIndexChanged
        Dim tx As String = ""
        With lstJahrgang
            For i As Integer = .Items.Count - 1 To 0 Step -1
                If .GetItemChecked(i) Then
                    If tx > "" Then tx += ", "
                    tx += .Items(i).ToString
                End If
            Next
        End With
        Try
            With cboJahrgang
                .Items.Clear()
                .Items.Add(tx)
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            Stop
        End Try
    End Sub

    Private Sub dgvGruppe_DragDrop(sender As Object, e As DragEventArgs) Handles dgvGK1.DragDrop, dgvGK2.DragDrop, dgvGK3.DragDrop, dgvGK4.DragDrop, dgvGK5.DragDrop

        ''prüfen, ob bereits Meldungen verschoben wurden, denen noch keine Gruppe zugewiesen ist
        'Dim msg As String = String.Empty
        'For i As Integer = 1 To 5
        '    If dgvGruppe(i).Rows.Count > 0 Then
        '        If CInt(dgvGruppe(i).Rows(0).Cells("Gruppe").Value) = 0 Then
        '            If msg > String.Empty Then
        '                msg = "n " + msg
        '                msg += "',"
        '            End If
        '            msg += " '" + GruppenBez(i - 1)
        '        End If
        '    End If
        'Next
        'If msg > String.Empty Then
        '    MessageBox.Show("Bevor eine neue Gruppe eröffnet werden kann, müssen alle" + vbNewLine + "angelegten Gruppen eine Nummer zugewiesen bekommen." + vbNewLine + _
        '                    "Zuweisung der Gruppe" + msg + "' fehlt.", "Gruppen-Einteilung", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    Exit Sub
        'End If
        For Me.dgvTarget = 1 To dgvGruppe.Count
            If dgvGruppe.ContainsKey(dgvTarget) Then
                If sender.Equals(dgvGruppe(dgvTarget)) Then Exit For
            End If
        Next
        If dgvSource = dgvTarget Then Exit Sub

        If draging And dgvGruppe(dgvTarget).Rows.Count = 0 Then
            If mnuKonflikte.Checked Then
                MessageBox.Show("Bevor eine neue Gruppe eröffnet werden kann, müssen alle" + vbNewLine + "angelegten Gruppen eine Nummer zugewiesen bekommen.", _
                                "Gruppen-Einteilung", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
            Exit Sub
        End If

        Me.Cursor = Cursors.WaitCursor
        ScrollTimer.Stop()

        Dim found As DataRow()
        Dim found1 As DataRow()
        Dim s As String() = Split(dgvSelection, "-")

        With dgvGruppe(dgvTarget)
            If .Rows.Count = 0 Then
                For i As Integer = 1 To 5
                    cboGruppen(i).Items.Add(cboGruppen(i).Items.Count + 1)
                Next
            End If
            For r As Integer = 0 To s.Count - 1
                found = tblGruppe(dgvSource).Select("Teilnehmer = " + dgvGruppe(dgvSource).Rows(CInt(s(r))).Cells("Meldung").Value.ToString)
                found1 = dtMeldung.Select("Teilnehmer = " + dgvGruppe(dgvSource).Rows(CInt(s(r))).Cells("Meldung").Value.ToString)
                If cboGruppen(dgvTarget).Text > "" Then
                    found(0)("Gruppe") = cboGruppen(dgvTarget).Text ' Gruppe zuweisen (dgv)
                    found1(0)("Gruppe") = cboGruppen(dgvTarget).Text ' Gruppe zuweisen (tbl)
                Else
                    found(0)("Gruppe") = "0" ' Gruppe löschen (dgv)
                    found1(0)("Gruppe") = "0" ' Gruppe löschen (tbl)
                    cboGeschlecht.Enabled = False
                    cboJahrgang.Enabled = False
                    cboGruppe.Enabled = False
                    loading = True
                    cboGruppe.SelectedIndex = -1
                    loading = False
                End If
                tblGruppe(dgvTarget).ImportRow(found(0))
                tblGruppe(dgvSource).Rows.Remove(found(0))
            Next
        End With

        Enable_Controls(dgvTarget)
        bs(dgvTarget).Sort = "Gewicht ASC"

        If dgvGruppe(dgvSource).Rows.Count = 0 And cboGruppen(dgvSource).Text > "" Then
            'alle Heber der Gruppe entfernt
            Dim result As DialogResult = DialogResult.Yes
            Dim group As Integer
            If Not mnuMeldungen.Checked And CInt(cboGruppen(dgvSource).Text) < cboGruppen(dgvSource).Items.Count Then
                result = MessageBox.Show("Gruppe " + cboGruppen(dgvSource).Text + " löschen und alle nachfolgenden Gruppen verschieben?", "Gruppen-Einteilung", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            End If
            If result = DialogResult.Yes Then
                'Gruppen verschieben
                found = dtMeldung.Select("Gruppe > " + cboGruppen(dgvSource).Text, "Gruppe ASC")
                For i As Integer = 0 To found.Count - 1
                    group = CInt(found(i)("Gruppe")) - 1
                    found(i)("Gruppe") = group
                Next
                Fill_Controls(group + 1, True) 'letzte Gruppe löschen
                For i As Integer = 1 To 5
                    cboGruppen(i).Items.RemoveAt(cboGruppen(i).Items.Count - 1)
                Next
                Filter_Changed()
            End If
            Disable_Controls(dgvSource)
            cboGruppen(dgvSource).SelectedIndex = -1
            lblGruppen(dgvSource).Text = GruppenBez(dgvSource - 1)
        Else
            If dgvGruppe(dgvSource).Rows.Count = 0 And cboGruppen(dgvSource).Text = "" Then
                'keine Gruppen zugewiesen, aber Tabelle geleert
                For i As Integer = 1 To 5
                    cboGruppen(i).Items.RemoveAt(cboGruppen(i).Items.Count - 1)
                Next

            End If
            lblGruppen(dgvSource).Text = GruppenBez(dgvSource - 1) + " (" + tblGruppe(dgvSource).Rows.Count.ToString + ")"
            draging = dgvGruppe(dgvTarget).Rows.Count = 0 'True
        End If
        lblGruppen(dgvTarget).Text = GruppenBez(dgvTarget - 1) + " (" + tblGruppe(dgvTarget).Rows.Count.ToString + ")"

        mnuSpeichern.Checked = True
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub dgvGruppe_DragEnter(sender As Object, e As DragEventArgs) Handles dgvGK1.DragEnter, dgvGK2.DragEnter, dgvGK3.DragEnter, dgvGK4.DragEnter, dgvGK5.DragEnter
        Dim t As Integer
        For t = 1 To dgvGruppe.Count
            If sender.Equals(dgvGruppe(t)) Then Exit For
        Next
        If t = dgvSource Then
            e.Effect = DragDropEffects.None
        Else
            e.Effect = DragDropEffects.Move
        End If
    End Sub

    Private Sub dgvGruppe_DragOver(sender As Object, e As DragEventArgs) Handles dgvGK1.DragOver, dgvGK2.DragOver, dgvGK3.DragOver, dgvGK4.DragOver, dgvGK5.DragOver
        If pnlForm.PointToClient(New Point(e.X, e.Y)).X > pnlForm.Width - ScrollBereich Then
            ScrollRichtung = Direction.Right
        ElseIf Math.Abs(pnlForm.AutoScrollPosition.X) > 0 And pnlForm.PointToClient(New Point(e.X, e.Y)).X < ScrollBereich Then
            ScrollRichtung = Direction.Left
        Else
            ScrollRichtung = Direction.None
        End If
        If ScrollRichtung <> Direction.None Then
            ScrollTimer.Start()
        End If
    End Sub

    Private Sub dgvGruppe_GotFocus(sender As Object, e As EventArgs) Handles dgvGK1.GotFocus, dgvGK2.GotFocus, dgvGK3.GotFocus, dgvGK4.GotFocus, dgvGK5.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        With dgvGruppe(Ix)
            If .Rows.Count = 0 Then Exit Sub
            With .DefaultCellStyle
                .SelectionBackColor = Color.FromKnownColor(KnownColor.Highlight)
                .SelectionForeColor = Color.FromKnownColor(KnownColor.HighlightText)
            End With
        End With
    End Sub

    Private Sub dgvGruppe_LostFocus(sender As Object, e As EventArgs) Handles dgvGK1.LostFocus, dgvGK2.LostFocus, dgvGK3.LostFocus, dgvGK4.LostFocus, dgvGK5.LostFocus
        Dim Ix As Integer = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        With dgvGruppe(Ix)
            If .Rows.Count = 0 Then Exit Sub
            If .CurrentRow.Index Mod 2 = 0 Then
                .DefaultCellStyle.SelectionBackColor = RowsDefaultCellStyle_BackColor
            ElseIf .CurrentRow.Index Mod 2 = 1 Then
                .DefaultCellStyle.SelectionBackColor = AlternatingRowsDefaultCellStyle_BackColor
            End If
            .DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.ControlText)
        End With
    End Sub

    Private Sub dgvGruppe_MouseDown(sender As Object, e As MouseEventArgs) Handles dgvGK1.MouseDown, dgvGK2.MouseDown, dgvGK3.MouseDown, dgvGK4.MouseDown, dgvGK5.MouseDown
        dgvSource = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        With dgvGruppe(dgvSource)
            Dim hit As DataGridView.HitTestInfo = .HitTest(e.X, e.Y)
            dgvSelection = String.Empty
            If hit.RowIndex > -1 Then
                If .SelectedRows.Contains(.Rows(hit.RowIndex)) Then
                    For i As Integer = 0 To .SelectedRows.Count - 1
                        dgvSelection += .SelectedRows(i).Index.ToString + "-"
                    Next
                    dgvSelection = dgvSelection.Remove(dgvSelection.Length - 1)
                Else
                    dgvSelection += hit.RowIndex.ToString
                End If
                .DoDragDrop(dgvSelection, DragDropEffects.Move)
            End If
        End With
    End Sub

    Private Sub Scroll_Panel()
        With pnlForm
            Select Case ScrollRichtung
                Case Direction.Left
                    .AutoScrollPosition = New Point(Math.Abs(.AutoScrollPosition.X + ScrollStep), Math.Abs(.AutoScrollPosition.Y))
                    If Math.Abs(pnlForm.AutoScrollPosition.X) < ScrollStep Then .AutoScrollPosition = New Point(0, Math.Abs(.AutoScrollPosition.Y))
                Case Direction.Right
                    .AutoScrollPosition = New Point(Math.Abs(.AutoScrollPosition.X - ScrollStep), Math.Abs(.AutoScrollPosition.Y))
            End Select
        End With
    End Sub

    Private Sub ScrollTimer_Tick(sender As Object, e As EventArgs) Handles ScrollTimer.Tick
        Scroll_Panel()
        If Math.Abs(pnlForm.AutoScrollPosition.X) < ScrollStep Then ScrollTimer.Stop()
    End Sub

    Private Sub mnuDrucken_Click(sender As Object, e As EventArgs) Handles mnuDrucken.Click
        Dim WK_Datum As String = String.Empty
        Dim WK_Datum_E As String = String.Empty
        Dim WK_Ort As String = String.Empty
        Dim WK_Beginn As String = String.Empty
        Dim Wiegen_B As String = String.Empty
        Dim Wiegen_E As String = String.Empty
        Dim R_Datum As String = String.Empty
        Dim G_Bez As String = String.Empty

        Using conn As New MySqlConnection(MySqlConnString)
            Try
                conn.Open()
                Query = "select Ort, Datum, DatumBis from wettkampf where idWettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                reader = (cmd.ExecuteReader())
                reader.Read()
                WK_Ort = reader(0).ToString
                WK_Datum = Format(reader(1), "dd.MM.yyyy")
                If Not IsDBNull(reader(2)) Then WK_Datum_E = Format(reader(2), "dd.MM.yyyy")
                reader.Close()
                Query = "select Bezeichnung, Datum, WiegenBeginn, WiegenEnde, Reissen from gruppen where Wettkampf = " & Wettkampf.ID & " AND Gruppe = " & cboGruppe.Text & ";"
                cmd = New MySqlCommand(Query, conn)
                reader = (cmd.ExecuteReader())
                reader.Read()
                G_Bez = reader(0).ToString
                If Not IsDBNull(reader(1)) Then R_Datum = Format(reader(1), "dd.MM.yyyy")
                If Not IsDBNull(reader(2)) Then Wiegen_B = Strings.Left(Format("hh:mm", reader(2).ToString), 5)
                If Not IsDBNull(reader(3)) Then Wiegen_E = Strings.Left(Format("hh:mm", reader(3).ToString), 5)
                If Not IsDBNull(reader(4)) Then WK_Beginn = Strings.Left(Format("hh:mm", reader(4).ToString), 5)
            Catch ex As Exception
                If cboGruppe.Text = "" Then
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Keine Gruppe für den Druck ausgewählt.", "Wiegeliste drucken", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Using
                End If
                Exit Sub
            Finally
                reader.Close()
            End Try
        End Using

        Using report As New FastReport.Report
            Try
                With report
                    .Load(Path.Combine(Application.StartupPath, "Report", "Gruppenliste.frx"))
                    .RegisterData(tblGruppe(1), "Wiegeliste")
                    .SetParameterValue("pAK_w", AK_w)
                    .SetParameterValue("pAK_m", AK_m)
                    .SetParameterValue("pWK", Wettkampf.ID)
                    .SetParameterValue("pWK_Name", Wettkampf.Bezeichnung)
                    .SetParameterValue("pWK_Ort", WK_Ort)
                    .SetParameterValue("pWK_Datum", If(String.IsNullOrEmpty(WK_Datum_E), WK_Datum, Strings.Left(WK_Datum, 5)))
                    .SetParameterValue("pWK_Datum_E", If(String.IsNullOrEmpty(WK_Datum_E), "", "- " + WK_Datum_E + " "))
                    .SetParameterValue("pGruppe", cboGruppe.Text)
                    .SetParameterValue("pWK_Beginn", WK_Beginn)
                    .SetParameterValue("pWiegen_B", Wiegen_B)
                    .SetParameterValue("pWiegen_E", Wiegen_E)
                    .SetParameterValue("pR_Datum", R_Datum)
                    .SetParameterValue("pG_Bez", G_Bez)
                    .Show()
                End With
            Catch ex As Exception
                Stop
                'Using New Centered_MessageBox(Me)
                '    MessageBox.Show("Keine Kriterien für den Druck ausgewählt.", "Wiegeliste drucken", MessageBoxButtons.OK, MessageBoxIcon.Information)
                'End Using
                'Stop
            End Try
        End Using
    End Sub

End Class