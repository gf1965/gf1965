﻿
Imports System.ComponentModel
Imports Microsoft.Win32

Public Class frmAbzeichen

    Const ZA_Colon As Byte = &H20
    Const ZA_Light As Byte = &H40 'Ab-Zeichen (akustisch + visuell)
    Const ZA_Alert As Byte = &H80 'Hupe der Uhr 
    Dim Serialport1 As New SerialPort With {.BaudRate = 9600, .DataBits = 8, .ParityReplace = 63, .PortName = "COM0", .ReadBufferSize = 4096, .ReceivedBytesThreshold = 1, .ReadTimeout = -1, .WriteBufferSize = 2048, .WriteTimeout = -1}

    Private Sub frmAbzeichen_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Write_ZA(,,, False)
        Serialport1.Close()
        Serialport1.Dispose()
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim ports As New List(Of String)

        Using Key As RegistryKey = Registry.LocalMachine.OpenSubKey("HARDWARE\DEVICEMAP\SERIALCOMM", RegistryKeyPermissionCheck.ReadSubTree)
            If Not IsNothing(Key) Then
                Dim ValueNames = Key.GetValueNames
                For Each Value In ValueNames
                    If LCase(Value).Contains("serial") OrElse
                       LCase(Value).Contains("silabser") OrElse
                       LCase(Value).Contains("usbser") OrElse
                       LCase(Value).Contains("vcp") Then
                        If Not ports.Contains(Key.GetValue(Value).ToString) Then
                            ports.Add(Key.GetValue(Value).ToString)
                        End If
                    End If
                Next
            End If
        End Using
        If ports.Count = 0 Then
            'keine Ports in Registry
            ports = SerialPort.GetPortNames().ToList
        End If
        portList.Items.Clear()
        portList.Items.AddRange(ports.ToArray)
        portList.SelectedIndex = -1
    End Sub

    Private Sub Con_Click(sender As Object, e As EventArgs) Handles Con.Click
        Try
            Serialport1.Close()
            Serialport1.PortName = portList.SelectedItem.ToString
            Serialport1.Open()
            Panel1.Enabled = True
        Catch ex As Exception
            MsgBox("Fehler beim Verbinden", MsgBoxStyle.Critical)
            Panel1.Enabled = False
        End Try
    End Sub

    Private Sub Write_ZA(Optional AbZeichen As Byte = 0, Optional Alarm As Byte = 0, Optional Value As String = "", Optional NoMsg As Boolean = True)
        Dim val As String
        Dim buf(4) As Byte

        buf(0) += AbZeichen
        buf(0) += Alarm

        If numMin.Enabled AndAlso String.IsNullOrEmpty(Value) OrElse Not numMin.Enabled AndAlso Not  String.IsNullOrEmpty(Value) Then
            val = ":::" ' Anzeige aus
        Else
            val = Strings.Right(Value, 3)
            buf(0) += ZA_Colon
        End If

        Encoding.ASCII.GetBytes(val).CopyTo(buf, 1)

        Try
            Debug.WriteLine(CStr(buf(0)) + vbNewLine + CStr(buf(1)) + vbNewLine + CStr(buf(2)) + vbNewLine + CStr(buf(3)) + vbNewLine + CStr(buf(4)) + vbNewLine)
            Serialport1.Write(buf, 0, 4)
        Catch ex As Exception
            If Not NoMsg Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show(ex.Message, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End Using
            End If
            chkAZ.Checked = False
            Panel1.Enabled = False
        End Try
    End Sub

    Private Sub numMin_ValueChanged(sender As Object, e As EventArgs) Handles numMin.ValueChanged
        If btnUhr.Text = "Auschalten" Then Write_ZA(,, numMin.Value.ToString + numSek.Value.ToString("00"))
    End Sub

    Private Sub numSek_ValueChanged(sender As Object, e As EventArgs) Handles numSek.ValueChanged
        If btnUhr.Text = "Auschalten" Then Write_ZA(,, numMin.Value.ToString + numSek.Value.ToString("00"))
    End Sub

    Private Sub btnUhr_Click(sender As Object, e As EventArgs) Handles btnUhr.Click
        If btnUhr.Text = "Einschalten" Then
            btnUhr.Text = "Auschalten"
            numMin.Enabled = True
            numSek.Enabled = True
            Write_ZA(,, numMin.Value.ToString + numSek.Value.ToString("00"))
        Else
            btnUhr.Text = "Einschalten"
            Write_ZA()
            numMin.Enabled = False
            numSek.Enabled = False
        End If
        chkHupe.Checked = False
        chkAZ.Checked = False
    End Sub

    Private Sub chkHupe_CheckedChanged(sender As Object, e As EventArgs) Handles chkHupe.Click 'CheckedChanged
        Dim value As String = String.Empty
        If numMin.Enabled Then value = numMin.Value.ToString + numSek.Value.ToString("00")

        If chkHupe.Checked Then
            chkHupe.Checked = False
            Write_ZA(, , value)
        Else
            chkHupe.Checked = True
            Write_ZA(, ZA_Alert, value)
        End If

        chkAZ.Checked = False
    End Sub

    Private Sub chkAZ_CheckedChanged(sender As Object, e As EventArgs) Handles chkAZ.Click 'CheckedChanged
        Dim value As String = String.Empty
        If numMin.Enabled Then value = numMin.Value.ToString + numSek.Value.ToString("00")

        If chkAZ.Checked Then
            chkAZ.Checked = False
            Write_ZA(, , value)
        Else
            chkAZ.Checked = True
            Write_ZA(, ZA_Light, value)
        End If

        chkHupe.Checked = False
    End Sub

End Class