﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJuryEntscheid
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGültig = New System.Windows.Forms.Button()
        Me.btnUngültig = New System.Windows.Forms.Button()
        Me.txtDummy = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnGültig
        '
        Me.btnGültig.BackColor = System.Drawing.Color.Black
        Me.btnGültig.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnGültig.FlatAppearance.BorderSize = 0
        Me.btnGültig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGültig.Font = New System.Drawing.Font("Wingdings", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.btnGültig.ForeColor = System.Drawing.Color.White
        Me.btnGültig.Location = New System.Drawing.Point(0, 0)
        Me.btnGültig.Name = "btnGültig"
        Me.btnGültig.Size = New System.Drawing.Size(135, 148)
        Me.btnGültig.TabIndex = 18
        Me.btnGültig.tabstop = False
        Me.btnGültig.Text = "l"
        Me.btnGültig.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnGültig.UseVisualStyleBackColor = False
        '
        'btnUngültig
        '
        Me.btnUngültig.BackColor = System.Drawing.Color.Black
        Me.btnUngültig.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnUngültig.FlatAppearance.BorderSize = 0
        Me.btnUngültig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUngültig.Font = New System.Drawing.Font("Wingdings", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.btnUngültig.ForeColor = System.Drawing.Color.Red
        Me.btnUngültig.Location = New System.Drawing.Point(143, 0)
        Me.btnUngültig.Name = "btnUngültig"
        Me.btnUngültig.Size = New System.Drawing.Size(135, 148)
        Me.btnUngültig.TabIndex = 19
        Me.btnUngültig.tabstop = False
        Me.btnUngültig.Text = "l"
        Me.btnUngültig.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnUngültig.UseVisualStyleBackColor = False
        '
        'txtDummy
        '
        Me.txtDummy.Location = New System.Drawing.Point(397, 163)
        Me.txtDummy.Name = "txtDummy"
        Me.txtDummy.Size = New System.Drawing.Size(73, 20)
        Me.txtDummy.TabIndex = 0
        '
        'frmJuryEntscheid
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(278, 148)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnUngültig)
        Me.Controls.Add(Me.btnGültig)
        Me.Controls.Add(Me.txtDummy)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJuryEntscheid"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Jury-Entscheidung"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnGültig As System.Windows.Forms.Button
    Friend WithEvents btnUngültig As System.Windows.Forms.Button
    Friend WithEvents txtDummy As System.Windows.Forms.TextBox
End Class
