﻿
Public Class frmScreens
    Private Bereich As String = "Display"
    Private factor As Single = 0.5 ' Optionen/Display/Size = Zoomfaktor für frmScreenIdent
    Private formsIdentify As New List(Of Form)
    Private lstLabel As New SortedList(Of Integer, Label)
    Private lstComboBox As New SortedList(Of Integer, ComboBox)
    Private lstScreens As New List(Of String)
    Private lstButton As New SortedList(Of Integer, Button)
    Private setButton As New SortedList(Of Integer, Button)

    Private ShowForm As New ShowForm

    Private Sub frmScreens_Closing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Cursor = Cursors.WaitCursor
        If btnAssign.Enabled AndAlso Not HideMessages Then
            Using New Centered_MessageBox(Me)
                Dim result = MessageBox.Show("Anzeigen den jeweiligen Displays zuweisen?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                If result = DialogResult.Cancel Then
                    e.Cancel = True
                ElseIf result = DialogResult.Yes Then
                    btnAssign.PerformClick()
                End If
            End Using
        End If
    End Sub
    Private Sub frmScreens_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        ' alle Identifizieren-Forms schließen
        For Each frm In formsIdentify
            frm.Close()
        Next
        Cursor = Cursors.Default
        formMain.Focus()
    End Sub
    Private Sub frmScreens_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F4 Then
            Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
            Cursor.Show()
        End If
    End Sub
    Private Sub frmScreens_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            factor = CSng(NativeMethods.INI_Read(Bereich, "Size", App_IniFile)) / 100
        Catch ex As Exception
        End Try

        lstScreens.AddRange({
                            "",
                            "Bohle",
                            "Hantelbeladung",
                            "Moderator",
                            "TV-Wertung",
                            "Versuchsreihenfolge",
                            "Wertung"
                            })
        If Wettkampf.WK_Modus < Modus.Easy Then
            If Not Wettkampf.Mannschaft Then
                '    lstScreens.AddRange({
                '                        "Wertung + Versuchsreihenfolge"})
                'Else
                lstScreens.AddRange({
                                    "Wertung + Bestenliste",
                                    "Wertung + Bestenliste + 2.Wertung",
                                    "Wertung + 2.Wertung",
                                    "Wertung + Versuchsreihenfolge"})
            End If
        End If

        For Each ctl As Control In pnlScreens.Controls
            If TypeOf ctl Is Label Then
                lstLabel.Add(CInt(ctl.Name.Substring(5)), CType(ctl, Label))
            ElseIf TypeOf ctl Is ComboBox Then
                Dim c = CType(ctl, ComboBox)
                c.Items.AddRange(lstScreens.ToArray)
                lstComboBox.Add(CInt(c.Name.Substring(10)), c)
            ElseIf TypeOf ctl Is Button Then
                If ctl.Name.StartsWith("btn") Then
                    lstButton.Add(CInt(ctl.Name.Substring(3)), CType(ctl, Button))
                ElseIf ctl.Name.StartsWith("set") Then
                    setButton.Add(CInt(ctl.Name.Substring(3)), CType(ctl, Button))
                End If
            End If
        Next

    End Sub
    Private Sub frmScreens_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Refresh()
        Disable_Unplugged()
        btnAssign.Enabled = GetScreensFromFile()
    End Sub

    Private Sub btnAssign_Click(sender As Object, e As EventArgs) Handles btnAssign.Click
        Assign(True)
    End Sub
    Private Sub btnDefault_Click(sender As Object, e As EventArgs) Handles btnDefault.Click
        Disable_Unplugged()
        GetScreensFromFile()
    End Sub
    Private Sub btnIdentify_Click(sender As Object, e As EventArgs) Handles btnIdentify.Click
        Try
            formsIdentify.Clear()
            For i As Integer = 0 To Screen.AllScreens.Count - 1
                Dim frm As New frmScreenIdent
                formsIdentify.Add(frm)
                With frm
                    .Name = "Display" & i + 1
                    .Screen_Number.Text = CStr(i + 1)
                    Using sc As New ScreenScale
                        sc.SetWindow(i, factor, frm,,,,,, factor)
                    End Using
                    .Show()
                End With
            Next
        Catch ex As Exception
        End Try
    End Sub
    Private Sub btnRefreshAll_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Assign()
        btnAssign.Enabled = False
    End Sub
    Private Sub btnRefreshCurrent_Click(sender As Object, e As EventArgs) Handles btn1.Click, btn2.Click, btn3.Click,
                                                                                  btn4.Click, btn5.Click, btn6.Click,
                                                                                  btn7.Click, btn8.Click, btn9.Click,
                                                                                  btn10.Click, btn11.Click, btn12.Click,
                                                                                  btn13.Click, btn14.Click
        Cursor = Cursors.WaitCursor

        Dim ix = CInt(CType(sender, Button).Name.Substring(3))
        Using sc As New ScreenScale
            Dim mon = sc.GetDeviceNumber(Screen.AllScreens(ix - 1))
            If dicScreens.ContainsKey(mon) Then dicScreens(mon).Form.Close()
        End Using

        setButton(ix).Enabled = ShowForm.Create_Form(New KeyValuePair(Of Integer, String)(ix, lstComboBox(ix).Text), True)

        Cursor = Cursors.Default

    End Sub

    Private Sub btnSetting_Click(sender As Object, e As EventArgs) Handles set1.Click, set2.Click, set3.Click,
                                                                           set4.Click, set5.Click, set6.Click,
                                                                           set7.Click, set8.Click, set9.Click,
                                                                           set10.Click, set11.Click, set12.Click,
                                                                           set13.Click, set14.Click
        Dim ix = CInt(CType(sender, Button).Name.Substring(3))
        'Dim mon As Integer

        'Using sc As New ScreenScale
        '    mon = sc.GetDeviceNumber(Screen.AllScreens(ix - 1))
        'End Using

        Using SS As New frmScreenSetting
            With SS
                .FormText = lstComboBox(ix).Text  'dicScreens(mon).Form.Text
                .ShowDialog(Me)
            End With
        End Using

        If Not btnAssign.Enabled Then Close()
    End Sub

    Private Sub cboAnzeige_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnzeige1.SelectedIndexChanged,
                                                                                          cboAnzeige2.SelectedIndexChanged,
                                                                                          cboAnzeige3.SelectedIndexChanged,
                                                                                          cboAnzeige4.SelectedIndexChanged,
                                                                                          cboAnzeige5.SelectedIndexChanged,
                                                                                          cboAnzeige6.SelectedIndexChanged,
                                                                                          cboAnzeige7.SelectedIndexChanged,
                                                                                          cboAnzeige8.SelectedIndexChanged,
                                                                                          cboAnzeige9.SelectedIndexChanged,
                                                                                          cboAnzeige10.SelectedIndexChanged,
                                                                                          cboAnzeige11.SelectedIndexChanged,
                                                                                          cboAnzeige12.SelectedIndexChanged,
                                                                                          cboAnzeige13.SelectedIndexChanged,
                                                                                          cboAnzeige14.SelectedIndexChanged
        Dim ctl = CType(sender, ComboBox)
        Dim ix = CInt(ctl.Name.Substring(10))

        lstButton(ix).Enabled = ctl.Enabled
        setButton(ix).Enabled = IsFormPresent({ctl.Text}).Count > 0 AndAlso (
                                ctl.Text.StartsWith("Wertung") OrElse
                                ctl.Text.StartsWith("Bohle"))

        If ctl.Enabled Then btnAssign.Enabled = True
    End Sub

    Private Sub Assign(Optional CloseForm As Boolean = False)

        Cursor = Cursors.WaitCursor

        Disable_Unplugged(True)

        ' alle Anzeigen schließen
        Dim cnt = Application.OpenForms.Count - 1, cur = 0
        For i = 0 To cnt
            If lstScreens.Contains(Application.OpenForms(cur).Text) Then
                Application.OpenForms(cur).Close()
            Else
                cur += 1
            End If
            If cur >= Application.OpenForms.Count Then Exit For
        Next

        dicScreens.Clear()

        ' ausgewählte Anzeigen initialisieren
        For i = 1 To lstComboBox.Count
            If lstComboBox(i).Enabled AndAlso Not String.IsNullOrEmpty(lstComboBox(i).Text) Then
                setButton(i).Enabled = ShowForm.Create_Form(New KeyValuePair(Of Integer, String)(i, lstComboBox(i).Text))
            End If
        Next

        ' Anzeigen speichern
        Dim contents As New List(Of String)

        If File.Exists(ScreensFile) Then
            Dim Lines = File.ReadAllLines(ScreensFile, Encoding.UTF8).ToList
            For Each Line In Lines
                Dim Entries = Split(Line, ";")
                Dim ix = CInt(Entries(0))      ' Anzeige-Nummer in der Liste der Anzeigen
                Dim frm = Entries(2)           ' Form.Text
                Dim mon = CInt(Entries(3))     ' Device-Nummer 
                Dim fnt = If(Entries.Count = 5, CSng(Entries(4)), 0)

                If dicScreens.Keys.Contains(mon) Then
                    If dicScreens(mon).Form.Text.Equals(frm) AndAlso dicScreens(mon).Display = ix Then
                        dicScreens(mon) = New ValueList With {.Display = ix, .Form = dicScreens(mon).Form, .Fontsize = fnt}
                    End If
                End If
            Next
        End If

        For Each item In dicScreens
            Dim content = {item.Value.Display.ToString,
                           item.Value.Form.Name,
                           item.Value.Form.Text,
                           item.Key.ToString,
                           item.Value.Fontsize.ToString}
            contents.Add(Join(content, ";"))
        Next

        File.Delete(ScreensFile)

        File.WriteAllLines(ScreensFile, contents.ToArray, Encoding.UTF8)

        If CloseForm Then Close()
        Cursor = Cursors.Default
    End Sub



    Private Sub Disable_Unplugged(Optional KeepExisting As Boolean = False)
        ' alle angeschlossene Anzeigen sperren
        For i = 1 To lstLabel.Count
            lstLabel(i).Enabled = False
            lstComboBox(i).Enabled = False
            If Not KeepExisting Then lstComboBox(i).SelectedIndex = 0
            lstButton(i).Enabled = False
            setButton(i).Enabled = False
        Next
        ' angeschlossene Anzeigen freischalten
        For i = 1 To Screen.AllScreens.Count
            lstLabel(i).Enabled = True
            lstComboBox(i).Enabled = True
        Next
    End Sub
    Private Function GetScreensFromFile() As Boolean

        ' Anzeigen aus Datei in die Controls und das Dictionary einlesen

        If Not File.Exists(ScreensFile) Then Return False

        dicScreens.Clear()
        Try
            Dim contents = File.ReadAllLines(ScreensFile, Encoding.UTF8)
            For Each content In contents
                Dim line = Split(content, ";")

                Dim ix = CInt(line(0))      ' Anzeige-Nummer in der Liste der Anzeigen
                Dim mon = CInt(line(3))     ' Device-Nummer 
                Dim fnt As Single = 0
                If line.Length = 5 Then
                    fnt = CSng(line(4))
                End If
                Using sc As New ScreenScale
                    For i = 0 To Application.OpenForms.Count - 1
                        If sc.GetDeviceNumber(Screen.FromControl(Application.OpenForms(i))) = mon Then
                            Dim display = New ValueList With {.Form = Application.OpenForms(i),
                                                              .Display = ix,
                                                              .Fontsize = fnt}
                            dicScreens(mon) = display
                            setButton(ix).Enabled = display.Form.Text.StartsWith("Wertung") OrElse display.Form.Text.StartsWith("Bohle")
                        Else
                            setButton(ix).Enabled = False
                        End If
                    Next
                End Using
                lstComboBox(ix).Text = line(2)

            Next
            Return contents.Length > 0
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class

Public Class ShowForm
    Implements IDisposable

    Private disposedValue As Boolean

    Private Function Show_Form(vl As ValueList) As Boolean
        Using sc As New ScreenScale
            vl.Form.Show()
            If Not vl.Form.Equals("TV-Wertung") Then sc.SetWindow(vl.Display - 1, 1, vl.Form)

            Dim mon = sc.GetDeviceNumber(Screen.FromControl(vl.Form))

            dicScreens(mon) = New ValueList With {.Form = vl.Form,
                                                  .Display = vl.Display,
                                                  .Fontsize = vl.Fontsize}
            Return dicScreens(mon).Form.Text.StartsWith("Wertung") OrElse
                   dicScreens(mon).Form.Text.StartsWith("Bohle")

        End Using
    End Function

    Friend Function Create_Form(item As KeyValuePair(Of Integer, String),
                                Optional Refresh As Boolean = False) As Boolean
        Select Case item.Value
            Case "Bohle"
                If Refresh OrElse Not dicScreens.ContainsKey(item.Key) OrElse Not item.Value.Equals(dicScreens(item.Key).Form.Text) Then
                    Return Show_Form(New ValueList With {.Form = New frmBohle, .Display = item.Key})
                End If
            Case "Hantelbeladung"
                If Refresh OrElse Not dicScreens.ContainsKey(item.Key) OrElse Not item.Value.Equals(dicScreens(item.Key).Form.Text) Then
                    Return Show_Form(New ValueList With {.Form = New frmHantel, .Display = item.Key})
                End If
            Case "Moderator"
                If Refresh OrElse Not dicScreens.ContainsKey(item.Key) OrElse Not item.Value.Equals(dicScreens(item.Key).Form.Text) Then
                    Return Show_Form(New ValueList With {.Form = New frmModerator, .Display = item.Key})
                End If
            Case "TV-Wertung"
                If Refresh OrElse Not dicScreens.ContainsKey(item.Key) OrElse Not item.Value.Equals(dicScreens(item.Key).Form.Text) Then
                    Return Show_Form(New ValueList With {.Form = New frmHeberWertung, .Display = item.Key})
                End If
            Case "Wertung"
                If Refresh OrElse Not dicScreens.ContainsKey(item.Key) OrElse Not item.Value.Equals(dicScreens(item.Key).Form.Text) Then
                    Return Show_Form(New ValueList With {.Form = New frmPublikum With {.Text = "Wertung"}, .Display = item.Key})
                End If
            Case "Wertung + Bestenliste"
                If Refresh OrElse Not dicScreens.ContainsKey(item.Key) OrElse Not item.Value.Equals(dicScreens(item.Key).Form.Text) Then
                    Return Show_Form(New ValueList With {.Form = New frmPublikum With {.Text = "Wertung + Bestenliste"}, .Display = item.Key})
                End If
            Case "Wertung + Bestenliste + 2.Wertung"
                If Refresh OrElse Not dicScreens.ContainsKey(item.Key) OrElse Not item.Value.Equals(dicScreens(item.Key).Form.Text) Then
                    Return Show_Form(New ValueList With {.Form = New frmPublikum With {.Text = "Wertung + Bestenliste + 2.Wertung"},
                                                         .Display = item.Key})
                End If
            Case "Wertung + 2.Wertung"
                If Refresh OrElse Not dicScreens.ContainsKey(item.Key) OrElse Not item.Value.Equals(dicScreens(item.Key).Form.Text) Then
                    Return Show_Form(New ValueList With {.Form = New frmPublikum With {.Text = "Wertung + 2.Wertung"}, .Display = item.Key})
                End If
            Case "Wertung + Versuchsreihenfolge"
                If Refresh OrElse Not dicScreens.ContainsKey(item.Key) OrElse Not item.Value.Equals(dicScreens(item.Key).Form.Text) Then
                    Return Show_Form(New ValueList With {.Form = New frmPublikum With {.Text = "Wertung + Versuchsreihenfolge"}, .Display = item.Key})
                End If
            Case "Versuchsreihenfolge"
                If Refresh OrElse Not dicScreens.ContainsKey(item.Key) OrElse Not item.Value.Equals(dicScreens(item.Key).Form.Text) Then
                    Return Show_Form(New ValueList With {.Form = New frmAufwärmen, .Display = item.Key})
                End If
        End Select
        Return False
    End Function

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: Verwalteten Zustand (verwaltete Objekte) bereinigen
            End If

            ' TODO: Nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalizer überschreiben
            ' TODO: Große Felder auf NULL setzen
            disposedValue = True
        End If
    End Sub

    ' ' TODO: Finalizer nur überschreiben, wenn "Dispose(disposing As Boolean)" Code für die Freigabe nicht verwalteter Ressourcen enthält
    ' Protected Overrides Sub Finalize()
    '     ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in der Methode "Dispose(disposing As Boolean)" ein.
    '     Dispose(disposing:=False)
    '     MyBase.Finalize()
    ' End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in der Methode "Dispose(disposing As Boolean)" ein.
        Dispose(disposing:=True)
        GC.SuppressFinalize(Me)
    End Sub
End Class
