﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Drucken_Protokoll
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.chkSelectAll = New System.Windows.Forms.CheckBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnVorschau = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboVorlage = New System.Windows.Forms.ComboBox()
        Me.chkNewPage = New System.Windows.Forms.CheckBox()
        Me.btnKR = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabWettkampf = New System.Windows.Forms.TabPage()
        Me.lstWettkampf = New System.Windows.Forms.CheckedListBox()
        Me.tabKommentar = New System.Windows.Forms.TabPage()
        Me.txtKommentar = New System.Windows.Forms.TextBox()
        Me.tabGruppen = New System.Windows.Forms.TabPage()
        Me.lstGruppe = New System.Windows.Forms.CheckedListBox()
        Me.tabMännlich = New System.Windows.Forms.TabPage()
        Me.lstGK_m = New System.Windows.Forms.CheckedListBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lstAK_m = New System.Windows.Forms.CheckedListBox()
        Me.lblGK_m = New System.Windows.Forms.Label()
        Me.tabWeiblich = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblGK_w = New System.Windows.Forms.Label()
        Me.lstAK_w = New System.Windows.Forms.CheckedListBox()
        Me.lstGK_w = New System.Windows.Forms.CheckedListBox()
        Me.tabVereine = New System.Windows.Forms.TabPage()
        Me.dgvVerein = New System.Windows.Forms.DataGridView()
        Me.Verein = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Team = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PunkteV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PlatzV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tabLänder = New System.Windows.Forms.TabPage()
        Me.dgvLänder = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Punkte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Platz = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chkHide = New System.Windows.Forms.CheckBox()
        Me.dgvHeber = New System.Windows.Forms.DataGridView()
        Me.chkWertung2 = New System.Windows.Forms.CheckBox()
        Me.TabControl1.SuspendLayout()
        Me.tabWettkampf.SuspendLayout()
        Me.tabKommentar.SuspendLayout()
        Me.tabGruppen.SuspendLayout()
        Me.tabMännlich.SuspendLayout()
        Me.tabWeiblich.SuspendLayout()
        Me.tabVereine.SuspendLayout()
        CType(Me.dgvVerein, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabLänder.SuspendLayout()
        CType(Me.dgvLänder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.BackColor = System.Drawing.SystemColors.Control
        Me.chkSelectAll.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkSelectAll.Location = New System.Drawing.Point(24, 255)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(107, 18)
        Me.chkSelectAll.TabIndex = 19
        Me.chkSelectAll.Text = "alles auswählen"
        Me.chkSelectAll.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkSelectAll.UseVisualStyleBackColor = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Title = "Druckvorlagen suchen"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Location = New System.Drawing.Point(399, 88)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(100, 28)
        Me.btnCancel.TabIndex = 44
        Me.btnCancel.Text = "&Beenden"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Enabled = False
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnPrint.Location = New System.Drawing.Point(399, 54)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(100, 28)
        Me.btnPrint.TabIndex = 43
        Me.btnPrint.Tag = "Print"
        Me.btnPrint.Text = "&Drucken"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnVorschau
        '
        Me.btnVorschau.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVorschau.Enabled = False
        Me.btnVorschau.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnVorschau.Location = New System.Drawing.Point(399, 20)
        Me.btnVorschau.Name = "btnVorschau"
        Me.btnVorschau.Size = New System.Drawing.Size(100, 28)
        Me.btnVorschau.TabIndex = 42
        Me.btnVorschau.Tag = "Show"
        Me.btnVorschau.Text = "&Vorschau"
        Me.btnVorschau.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnSearch.Location = New System.Drawing.Point(379, 374)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(24, 24)
        Me.btnSearch.TabIndex = 49
        Me.btnSearch.Text = "..."
        Me.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label6.Location = New System.Drawing.Point(21, 358)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 13)
        Me.Label6.TabIndex = 48
        Me.Label6.Text = "Druck-Vorlage"
        '
        'cboVorlage
        '
        Me.cboVorlage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboVorlage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVorlage.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboVorlage.FormattingEnabled = True
        Me.cboVorlage.Location = New System.Drawing.Point(18, 376)
        Me.cboVorlage.Name = "cboVorlage"
        Me.cboVorlage.Size = New System.Drawing.Size(355, 21)
        Me.cboVorlage.Sorted = True
        Me.cboVorlage.TabIndex = 47
        '
        'chkNewPage
        '
        Me.chkNewPage.AutoSize = True
        Me.chkNewPage.Enabled = False
        Me.chkNewPage.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkNewPage.Location = New System.Drawing.Point(24, 279)
        Me.chkNewPage.Name = "chkNewPage"
        Me.chkNewPage.Size = New System.Drawing.Size(212, 18)
        Me.chkNewPage.TabIndex = 52
        Me.chkNewPage.Text = "jede Gruppe auf neuer Seite beginnen"
        Me.chkNewPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkNewPage.UseVisualStyleBackColor = True
        '
        'btnKR
        '
        Me.btnKR.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnKR.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnKR.ForeColor = System.Drawing.Color.RoyalBlue
        Me.btnKR.Location = New System.Drawing.Point(399, 221)
        Me.btnKR.Name = "btnKR"
        Me.btnKR.Size = New System.Drawing.Size(100, 28)
        Me.btnKR.TabIndex = 53
        Me.btnKR.Text = "Kampfgericht"
        Me.btnKR.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnEdit.Location = New System.Drawing.Point(409, 374)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(90, 24)
        Me.btnEdit.TabIndex = 55
        Me.btnEdit.Tag = "Edit"
        Me.btnEdit.Text = "Bearbeiten"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabWettkampf)
        Me.TabControl1.Controls.Add(Me.tabKommentar)
        Me.TabControl1.Controls.Add(Me.tabGruppen)
        Me.TabControl1.Controls.Add(Me.tabMännlich)
        Me.TabControl1.Controls.Add(Me.tabWeiblich)
        Me.TabControl1.Controls.Add(Me.tabVereine)
        Me.TabControl1.Controls.Add(Me.tabLänder)
        Me.TabControl1.HotTrack = True
        Me.TabControl1.Location = New System.Drawing.Point(18, 20)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.ShowToolTips = True
        Me.TabControl1.Size = New System.Drawing.Size(355, 229)
        Me.TabControl1.TabIndex = 56
        '
        'tabWettkampf
        '
        Me.tabWettkampf.Controls.Add(Me.lstWettkampf)
        Me.tabWettkampf.Location = New System.Drawing.Point(4, 22)
        Me.tabWettkampf.Name = "tabWettkampf"
        Me.tabWettkampf.Size = New System.Drawing.Size(347, 203)
        Me.tabWettkampf.TabIndex = 6
        Me.tabWettkampf.Text = "Wettkämpfe"
        Me.tabWettkampf.UseVisualStyleBackColor = True
        '
        'lstWettkampf
        '
        Me.lstWettkampf.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstWettkampf.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lstWettkampf.Location = New System.Drawing.Point(1, 4)
        Me.lstWettkampf.Name = "lstWettkampf"
        Me.lstWettkampf.Size = New System.Drawing.Size(345, 180)
        Me.lstWettkampf.TabIndex = 41
        Me.lstWettkampf.ThreeDCheckBoxes = True
        '
        'tabKommentar
        '
        Me.tabKommentar.BackColor = System.Drawing.Color.White
        Me.tabKommentar.Controls.Add(Me.txtKommentar)
        Me.tabKommentar.Location = New System.Drawing.Point(4, 22)
        Me.tabKommentar.Name = "tabKommentar"
        Me.tabKommentar.Size = New System.Drawing.Size(347, 203)
        Me.tabKommentar.TabIndex = 5
        Me.tabKommentar.Text = "Kommentar"
        '
        'txtKommentar
        '
        Me.txtKommentar.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtKommentar.Location = New System.Drawing.Point(3, 3)
        Me.txtKommentar.MaxLength = 1000
        Me.txtKommentar.Multiline = True
        Me.txtKommentar.Name = "txtKommentar"
        Me.txtKommentar.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtKommentar.Size = New System.Drawing.Size(341, 198)
        Me.txtKommentar.TabIndex = 0
        '
        'tabGruppen
        '
        Me.tabGruppen.Controls.Add(Me.lstGruppe)
        Me.tabGruppen.Location = New System.Drawing.Point(4, 22)
        Me.tabGruppen.Name = "tabGruppen"
        Me.tabGruppen.Padding = New System.Windows.Forms.Padding(3)
        Me.tabGruppen.Size = New System.Drawing.Size(347, 203)
        Me.tabGruppen.TabIndex = 0
        Me.tabGruppen.Tag = "Gruppe"
        Me.tabGruppen.Text = " Gruppen "
        Me.tabGruppen.UseVisualStyleBackColor = True
        '
        'lstGruppe
        '
        Me.lstGruppe.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstGruppe.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lstGruppe.Location = New System.Drawing.Point(1, 4)
        Me.lstGruppe.Name = "lstGruppe"
        Me.lstGruppe.Size = New System.Drawing.Size(345, 180)
        Me.lstGruppe.TabIndex = 40
        Me.lstGruppe.ThreeDCheckBoxes = True
        '
        'tabMännlich
        '
        Me.tabMännlich.BackColor = System.Drawing.SystemColors.Control
        Me.tabMännlich.Controls.Add(Me.lstGK_m)
        Me.tabMännlich.Controls.Add(Me.Label9)
        Me.tabMännlich.Controls.Add(Me.lstAK_m)
        Me.tabMännlich.Controls.Add(Me.lblGK_m)
        Me.tabMännlich.Location = New System.Drawing.Point(4, 22)
        Me.tabMännlich.Name = "tabMännlich"
        Me.tabMännlich.Size = New System.Drawing.Size(347, 203)
        Me.tabMännlich.TabIndex = 2
        Me.tabMännlich.Tag = "Altersklasse"
        Me.tabMännlich.Text = " AK männlich "
        Me.tabMännlich.ToolTipText = "Auflistung nach Alters- und/oder Gewichtsklassen männlich"
        '
        'lstGK_m
        '
        Me.lstGK_m.BackColor = System.Drawing.SystemColors.Window
        Me.lstGK_m.FormattingEnabled = True
        Me.lstGK_m.Location = New System.Drawing.Point(183, 33)
        Me.lstGK_m.Name = "lstGK_m"
        Me.lstGK_m.Size = New System.Drawing.Size(97, 139)
        Me.lstGK_m.TabIndex = 80
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(66, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(69, 13)
        Me.Label9.TabIndex = 79
        Me.Label9.Text = "Altersklassen"
        '
        'lstAK_m
        '
        Me.lstAK_m.BackColor = System.Drawing.SystemColors.Window
        Me.lstAK_m.FormattingEnabled = True
        Me.lstAK_m.Location = New System.Drawing.Point(65, 33)
        Me.lstAK_m.Name = "lstAK_m"
        Me.lstAK_m.Size = New System.Drawing.Size(103, 139)
        Me.lstAK_m.TabIndex = 78
        '
        'lblGK_m
        '
        Me.lblGK_m.AutoSize = True
        Me.lblGK_m.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblGK_m.Location = New System.Drawing.Point(184, 16)
        Me.lblGK_m.Name = "lblGK_m"
        Me.lblGK_m.Size = New System.Drawing.Size(87, 13)
        Me.lblGK_m.TabIndex = 76
        Me.lblGK_m.Text = "Gewichtsklassen"
        '
        'tabWeiblich
        '
        Me.tabWeiblich.BackColor = System.Drawing.SystemColors.Control
        Me.tabWeiblich.Controls.Add(Me.Label1)
        Me.tabWeiblich.Controls.Add(Me.lblGK_w)
        Me.tabWeiblich.Controls.Add(Me.lstAK_w)
        Me.tabWeiblich.Controls.Add(Me.lstGK_w)
        Me.tabWeiblich.Location = New System.Drawing.Point(4, 22)
        Me.tabWeiblich.Name = "tabWeiblich"
        Me.tabWeiblich.Padding = New System.Windows.Forms.Padding(3)
        Me.tabWeiblich.Size = New System.Drawing.Size(347, 203)
        Me.tabWeiblich.TabIndex = 1
        Me.tabWeiblich.Tag = "Altersklasse"
        Me.tabWeiblich.Text = " AK weiblich "
        Me.tabWeiblich.ToolTipText = "Auflistung nach Alters- und/oder Gewichtsklassen weiblich"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(66, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 84
        Me.Label1.Text = "Altersklassen"
        '
        'lblGK_w
        '
        Me.lblGK_w.AutoSize = True
        Me.lblGK_w.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblGK_w.Location = New System.Drawing.Point(184, 16)
        Me.lblGK_w.Name = "lblGK_w"
        Me.lblGK_w.Size = New System.Drawing.Size(87, 13)
        Me.lblGK_w.TabIndex = 83
        Me.lblGK_w.Text = "Gewichtsklassen"
        '
        'lstAK_w
        '
        Me.lstAK_w.BackColor = System.Drawing.SystemColors.Window
        Me.lstAK_w.FormattingEnabled = True
        Me.lstAK_w.Location = New System.Drawing.Point(65, 33)
        Me.lstAK_w.Name = "lstAK_w"
        Me.lstAK_w.Size = New System.Drawing.Size(103, 139)
        Me.lstAK_w.TabIndex = 82
        '
        'lstGK_w
        '
        Me.lstGK_w.BackColor = System.Drawing.SystemColors.Window
        Me.lstGK_w.FormattingEnabled = True
        Me.lstGK_w.Location = New System.Drawing.Point(183, 33)
        Me.lstGK_w.Name = "lstGK_w"
        Me.lstGK_w.Size = New System.Drawing.Size(97, 139)
        Me.lstGK_w.TabIndex = 70
        '
        'tabVereine
        '
        Me.tabVereine.BackColor = System.Drawing.SystemColors.Control
        Me.tabVereine.Controls.Add(Me.dgvVerein)
        Me.tabVereine.Location = New System.Drawing.Point(4, 22)
        Me.tabVereine.Name = "tabVereine"
        Me.tabVereine.Size = New System.Drawing.Size(347, 203)
        Me.tabVereine.TabIndex = 3
        Me.tabVereine.Tag = "0"
        Me.tabVereine.Text = "Vereine"
        '
        'dgvVerein
        '
        Me.dgvVerein.AllowUserToAddRows = False
        Me.dgvVerein.AllowUserToDeleteRows = False
        Me.dgvVerein.AllowUserToOrderColumns = True
        Me.dgvVerein.AllowUserToResizeColumns = False
        Me.dgvVerein.AllowUserToResizeRows = False
        Me.dgvVerein.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvVerein.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvVerein.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvVerein.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvVerein.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvVerein.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVerein.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Verein, Me.Team, Me.PunkteV, Me.PlatzV})
        Me.dgvVerein.Location = New System.Drawing.Point(2, 2)
        Me.dgvVerein.Name = "dgvVerein"
        Me.dgvVerein.RowHeadersVisible = False
        Me.dgvVerein.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvVerein.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVerein.Size = New System.Drawing.Size(343, 198)
        Me.dgvVerein.TabIndex = 0
        Me.dgvVerein.Tag = "0"
        '
        'Verein
        '
        Me.Verein.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Verein.DataPropertyName = "Vereinsname"
        Me.Verein.HeaderText = "Verein"
        Me.Verein.Name = "Verein"
        Me.Verein.ReadOnly = True
        Me.Verein.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Verein.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Team
        '
        Me.Team.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Team.DataPropertyName = "team"
        Me.Team.HeaderText = "Team"
        Me.Team.Name = "Team"
        Me.Team.ReadOnly = True
        Me.Team.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Team.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Team.Width = 45
        '
        'PunkteV
        '
        Me.PunkteV.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.PunkteV.DataPropertyName = "Punkte"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.PunkteV.DefaultCellStyle = DataGridViewCellStyle2
        Me.PunkteV.HeaderText = "Punkte"
        Me.PunkteV.Name = "PunkteV"
        Me.PunkteV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.PunkteV.Width = 60
        '
        'PlatzV
        '
        Me.PlatzV.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.PlatzV.DataPropertyName = "Platz"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Format = "N0"
        Me.PlatzV.DefaultCellStyle = DataGridViewCellStyle3
        Me.PlatzV.HeaderText = "Platz"
        Me.PlatzV.Name = "PlatzV"
        Me.PlatzV.ReadOnly = True
        Me.PlatzV.Width = 35
        '
        'tabLänder
        '
        Me.tabLänder.BackColor = System.Drawing.SystemColors.Control
        Me.tabLänder.Controls.Add(Me.dgvLänder)
        Me.tabLänder.Location = New System.Drawing.Point(4, 22)
        Me.tabLänder.Name = "tabLänder"
        Me.tabLänder.Size = New System.Drawing.Size(347, 203)
        Me.tabLänder.TabIndex = 4
        Me.tabLänder.Tag = "1"
        Me.tabLänder.Text = "Länder"
        '
        'dgvLänder
        '
        Me.dgvLänder.AllowUserToAddRows = False
        Me.dgvLänder.AllowUserToDeleteRows = False
        Me.dgvLänder.AllowUserToOrderColumns = True
        Me.dgvLänder.AllowUserToResizeColumns = False
        Me.dgvLänder.AllowUserToResizeRows = False
        Me.dgvLänder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvLänder.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvLänder.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvLänder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLänder.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvLänder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLänder.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.Punkte, Me.Platz})
        Me.dgvLänder.Location = New System.Drawing.Point(2, 2)
        Me.dgvLänder.Name = "dgvLänder"
        Me.dgvLänder.RowHeadersVisible = False
        Me.dgvLänder.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvLänder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLänder.Size = New System.Drawing.Size(343, 198)
        Me.dgvLänder.TabIndex = 1
        Me.dgvLänder.Tag = "1"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "region_name"
        Me.DataGridViewTextBoxColumn1.FillWeight = 38.57307!
        Me.DataGridViewTextBoxColumn1.HeaderText = "Landesverband"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "team"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Team"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 45
        '
        'Punkte
        '
        Me.Punkte.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Punkte.DataPropertyName = "Punkte"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.Punkte.DefaultCellStyle = DataGridViewCellStyle5
        Me.Punkte.HeaderText = "Punkte"
        Me.Punkte.Name = "Punkte"
        Me.Punkte.ReadOnly = True
        Me.Punkte.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Punkte.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Punkte.Width = 60
        '
        'Platz
        '
        Me.Platz.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Platz.DataPropertyName = "Platz"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.Platz.DefaultCellStyle = DataGridViewCellStyle6
        Me.Platz.FillWeight = 35.0!
        Me.Platz.HeaderText = "Platz"
        Me.Platz.Name = "Platz"
        Me.Platz.ReadOnly = True
        Me.Platz.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Platz.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Platz.Width = 35
        '
        'chkHide
        '
        Me.chkHide.AutoSize = True
        Me.chkHide.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkHide.Location = New System.Drawing.Point(24, 303)
        Me.chkHide.Name = "chkHide"
        Me.chkHide.Size = New System.Drawing.Size(136, 18)
        Me.chkHide.TabIndex = 57
        Me.chkHide.Text = "Einteilung ausblenden"
        Me.chkHide.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkHide.UseVisualStyleBackColor = True
        '
        'dgvHeber
        '
        Me.dgvHeber.AllowUserToAddRows = False
        Me.dgvHeber.AllowUserToDeleteRows = False
        Me.dgvHeber.AllowUserToOrderColumns = True
        Me.dgvHeber.AllowUserToResizeColumns = False
        Me.dgvHeber.AllowUserToResizeRows = False
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.dgvHeber.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvHeber.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvHeber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvHeber.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgvHeber.Location = New System.Drawing.Point(283, 266)
        Me.dgvHeber.Name = "dgvHeber"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvHeber.RowHeadersVisible = False
        Me.dgvHeber.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvHeber.Size = New System.Drawing.Size(216, 67)
        Me.dgvHeber.TabIndex = 114
        Me.dgvHeber.TabStop = False
        Me.dgvHeber.Visible = False
        '
        'chkWertung2
        '
        Me.chkWertung2.AutoSize = True
        Me.chkWertung2.Enabled = False
        Me.chkWertung2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkWertung2.Location = New System.Drawing.Point(24, 327)
        Me.chkWertung2.Name = "chkWertung2"
        Me.chkWertung2.Size = New System.Drawing.Size(143, 18)
        Me.chkWertung2.TabIndex = 115
        Me.chkWertung2.Text = "2. Wertung ausblenden"
        Me.chkWertung2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkWertung2.UseVisualStyleBackColor = True
        '
        'Drucken_Protokoll
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(518, 413)
        Me.Controls.Add(Me.chkWertung2)
        Me.Controls.Add(Me.chkHide)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnKR)
        Me.Controls.Add(Me.chkNewPage)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cboVorlage)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnVorschau)
        Me.Controls.Add(Me.chkSelectAll)
        Me.Controls.Add(Me.dgvHeber)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(538, 433)
        Me.Name = "Drucken_Protokoll"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Protokoll drucken"
        Me.TabControl1.ResumeLayout(False)
        Me.tabWettkampf.ResumeLayout(False)
        Me.tabKommentar.ResumeLayout(False)
        Me.tabKommentar.PerformLayout()
        Me.tabGruppen.ResumeLayout(False)
        Me.tabMännlich.ResumeLayout(False)
        Me.tabMännlich.PerformLayout()
        Me.tabWeiblich.ResumeLayout(False)
        Me.tabWeiblich.PerformLayout()
        Me.tabVereine.ResumeLayout(False)
        CType(Me.dgvVerein, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabLänder.ResumeLayout(False)
        CType(Me.dgvLänder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkSelectAll As CheckBox
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnPrint As Button
    Friend WithEvents btnVorschau As Button
    Friend WithEvents btnSearch As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents cboVorlage As ComboBox
    Friend WithEvents chkNewPage As CheckBox
    Friend WithEvents btnKR As Button
    Friend WithEvents btnEdit As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents tabGruppen As TabPage
    Friend WithEvents tabWeiblich As TabPage
    Friend WithEvents lstGruppe As CheckedListBox
    Friend WithEvents lstGK_w As CheckedListBox
    Friend WithEvents tabMännlich As TabPage
    Friend WithEvents Label9 As Label
    Friend WithEvents lstAK_m As CheckedListBox
    Friend WithEvents lblGK_m As Label
    Friend WithEvents lstGK_m As CheckedListBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lblGK_w As Label
    Friend WithEvents lstAK_w As CheckedListBox
    Friend WithEvents chkHide As CheckBox
    Friend WithEvents tabVereine As TabPage
    Friend WithEvents tabLänder As TabPage
    Friend WithEvents dgvVerein As DataGridView
    Friend WithEvents dgvLänder As DataGridView
    Friend WithEvents Verein As DataGridViewTextBoxColumn
    Friend WithEvents Team As DataGridViewTextBoxColumn
    Friend WithEvents PunkteV As DataGridViewTextBoxColumn
    Friend WithEvents PlatzV As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents Punkte As DataGridViewTextBoxColumn
    Friend WithEvents Platz As DataGridViewTextBoxColumn
    Friend WithEvents dgvHeber As DataGridView
    Friend WithEvents chkWertung2 As CheckBox
    Friend WithEvents tabKommentar As TabPage
    Friend WithEvents txtKommentar As TextBox
    Friend WithEvents tabWettkampf As TabPage
    Friend WithEvents lstWettkampf As CheckedListBox
End Class
