﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHantelBeladung
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
                Me.Close()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtGewicht = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkEnableX = New System.Windows.Forms.CheckBox()
        Me.chkEnableK = New System.Windows.Forms.CheckBox()
        Me.chkBigDisc = New System.Windows.Forms.CheckBox()
        Me.optX = New System.Windows.Forms.RadioButton()
        Me.optK = New System.Windows.Forms.RadioButton()
        Me.optW = New System.Windows.Forms.RadioButton()
        Me.optM = New System.Windows.Forms.RadioButton()
        Me.btnShow = New System.Windows.Forms.Button()
        Me.chkTopMost = New System.Windows.Forms.CheckBox()
        Me.btnClear = New System.Windows.Forms.Button()
        CType(Me.txtGewicht, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(221, 85)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(81, 25)
        Me.btnExit.TabIndex = 10
        Me.btnExit.Text = "Schließen"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Hantelgewicht"
        '
        'txtGewicht
        '
        Me.txtGewicht.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGewicht.Location = New System.Drawing.Point(106, 23)
        Me.txtGewicht.Maximum = New Decimal(New Integer() {500, 0, 0, 0})
        Me.txtGewicht.Name = "txtGewicht"
        Me.txtGewicht.Size = New System.Drawing.Size(53, 20)
        Me.txtGewicht.TabIndex = 3
        Me.txtGewicht.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.chkEnableX)
        Me.GroupBox1.Controls.Add(Me.chkEnableK)
        Me.GroupBox1.Controls.Add(Me.chkBigDisc)
        Me.GroupBox1.Controls.Add(Me.optX)
        Me.GroupBox1.Controls.Add(Me.optK)
        Me.GroupBox1.Controls.Add(Me.optW)
        Me.GroupBox1.Controls.Add(Me.optM)
        Me.GroupBox1.Location = New System.Drawing.Point(23, 68)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(165, 172)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Hantelstange"
        '
        'chkEnableX
        '
        Me.chkEnableX.AutoCheck = False
        Me.chkEnableX.AutoSize = True
        Me.chkEnableX.Location = New System.Drawing.Point(136, 102)
        Me.chkEnableX.Name = "chkEnableX"
        Me.chkEnableX.Size = New System.Drawing.Size(15, 14)
        Me.chkEnableX.TabIndex = 26
        Me.chkEnableX.UseVisualStyleBackColor = True
        '
        'chkEnableK
        '
        Me.chkEnableK.AutoCheck = False
        Me.chkEnableK.AutoSize = True
        Me.chkEnableK.Location = New System.Drawing.Point(136, 79)
        Me.chkEnableK.Name = "chkEnableK"
        Me.chkEnableK.Size = New System.Drawing.Size(15, 14)
        Me.chkEnableK.TabIndex = 25
        Me.chkEnableK.UseVisualStyleBackColor = True
        '
        'chkBigDisc
        '
        Me.chkBigDisc.AutoSize = True
        Me.chkBigDisc.Location = New System.Drawing.Point(23, 141)
        Me.chkBigDisc.Name = "chkBigDisc"
        Me.chkBigDisc.Size = New System.Drawing.Size(101, 17)
        Me.chkBigDisc.TabIndex = 9
        Me.chkBigDisc.Text = "große Scheiben"
        Me.chkBigDisc.UseVisualStyleBackColor = True
        '
        'optX
        '
        Me.optX.AutoCheck = False
        Me.optX.AutoSize = True
        Me.optX.Location = New System.Drawing.Point(23, 99)
        Me.optX.Name = "optX"
        Me.optX.Size = New System.Drawing.Size(91, 17)
        Me.optX.TabIndex = 7
        Me.optX.TabStop = True
        Me.optX.Tag = "x"
        Me.optX.Text = "  5 kg - Kinder"
        Me.optX.UseVisualStyleBackColor = True
        '
        'optK
        '
        Me.optK.AutoCheck = False
        Me.optK.AutoSize = True
        Me.optK.Location = New System.Drawing.Point(23, 76)
        Me.optK.Name = "optK"
        Me.optK.Size = New System.Drawing.Size(91, 17)
        Me.optK.TabIndex = 7
        Me.optK.TabStop = True
        Me.optK.Tag = "k"
        Me.optK.Text = "  7 kg - Kinder"
        Me.optK.UseVisualStyleBackColor = True
        '
        'optW
        '
        Me.optW.AutoCheck = False
        Me.optW.AutoSize = True
        Me.optW.Location = New System.Drawing.Point(23, 53)
        Me.optW.Name = "optW"
        Me.optW.Size = New System.Drawing.Size(94, 17)
        Me.optW.TabIndex = 6
        Me.optW.TabStop = True
        Me.optW.Tag = "w"
        Me.optW.Text = "15 kg - Frauen"
        Me.optW.UseVisualStyleBackColor = True
        '
        'optM
        '
        Me.optM.AutoCheck = False
        Me.optM.AutoSize = True
        Me.optM.Location = New System.Drawing.Point(23, 30)
        Me.optM.Name = "optM"
        Me.optM.Size = New System.Drawing.Size(97, 17)
        Me.optM.TabIndex = 5
        Me.optM.TabStop = True
        Me.optM.Tag = "m"
        Me.optM.Text = "20 kg - Männer"
        Me.optM.UseVisualStyleBackColor = True
        '
        'btnShow
        '
        Me.btnShow.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnShow.Location = New System.Drawing.Point(221, 23)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(81, 25)
        Me.btnShow.TabIndex = 9
        Me.btnShow.Text = "Anzeigen"
        Me.btnShow.UseVisualStyleBackColor = True
        '
        'chkTopMost
        '
        Me.chkTopMost.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkTopMost.AutoSize = True
        Me.chkTopMost.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkTopMost.Location = New System.Drawing.Point(220, 209)
        Me.chkTopMost.Name = "chkTopMost"
        Me.chkTopMost.Size = New System.Drawing.Size(93, 17)
        Me.chkTopMost.TabIndex = 11
        Me.chkTopMost.Text = "immer sichtbar"
        Me.chkTopMost.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClear.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClear.Location = New System.Drawing.Point(221, 54)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(81, 25)
        Me.btnClear.TabIndex = 24
        Me.btnClear.Text = "Löschen"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'frmHantelBeladung
        '
        Me.AcceptButton = Me.btnShow
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(325, 258)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.chkTopMost)
        Me.Controls.Add(Me.btnShow)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtGewicht)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnExit)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHantelBeladung"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "GFHsoft Hantel-Beladung"
        CType(Me.txtGewicht, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtGewicht As System.Windows.Forms.NumericUpDown
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optW As System.Windows.Forms.RadioButton
    Friend WithEvents optM As System.Windows.Forms.RadioButton
    Friend WithEvents btnShow As System.Windows.Forms.Button
    Friend WithEvents optK As System.Windows.Forms.RadioButton
    Friend WithEvents chkTopMost As System.Windows.Forms.CheckBox
    Friend WithEvents chkBigDisc As CheckBox
    Friend WithEvents btnClear As Button
    Friend WithEvents optX As RadioButton
    Friend WithEvents chkEnableX As CheckBox
    Friend WithEvents chkEnableK As CheckBox
End Class
