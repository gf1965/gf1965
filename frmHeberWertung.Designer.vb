﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHeberWertung
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Logo = New System.Windows.Forms.PictureBox()
        Me.lblVersuch = New System.Windows.Forms.Label()
        Me.lblVersuch1 = New System.Windows.Forms.Label()
        Me.lblVersuch2 = New System.Windows.Forms.Label()
        Me.lblVersuch3 = New System.Windows.Forms.Label()
        Me.lblLampe1 = New System.Windows.Forms.Label()
        Me.lblLampe2 = New System.Windows.Forms.Label()
        Me.lblLampe3 = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblLast2 = New System.Windows.Forms.Label()
        Me.lblLast3 = New System.Windows.Forms.Label()
        Me.lblLast1 = New System.Windows.Forms.Label()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Logo
        '
        Me.Logo.BackColor = System.Drawing.Color.Black
        Me.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Logo.Dock = System.Windows.Forms.DockStyle.Left
        Me.Logo.Image = Global.Gewichtheben.My.Resources.Resources.LogoTV
        Me.Logo.Location = New System.Drawing.Point(0, 0)
        Me.Logo.Margin = New System.Windows.Forms.Padding(2)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(626, 285)
        Me.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Logo.TabIndex = 0
        Me.Logo.TabStop = False
        '
        'lblVersuch
        '
        Me.lblVersuch.BackColor = System.Drawing.Color.Transparent
        Me.lblVersuch.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.lblVersuch.Font = New System.Drawing.Font("Microsoft Sans Serif", 50.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersuch.ForeColor = System.Drawing.Color.LightSalmon
        Me.lblVersuch.Location = New System.Drawing.Point(616, 93)
        Me.lblVersuch.Margin = New System.Windows.Forms.Padding(0)
        Me.lblVersuch.Name = "lblVersuch"
        Me.lblVersuch.Size = New System.Drawing.Size(342, 73)
        Me.lblVersuch.TabIndex = 37
        Me.lblVersuch.Text = "3. Stoßen"
        Me.lblVersuch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVersuch1
        '
        Me.lblVersuch1.Font = New System.Drawing.Font("Wingdings", 175.0!)
        Me.lblVersuch1.ForeColor = System.Drawing.Color.White
        Me.lblVersuch1.Location = New System.Drawing.Point(964, 60)
        Me.lblVersuch1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblVersuch1.Name = "lblVersuch1"
        Me.lblVersuch1.Size = New System.Drawing.Size(171, 210)
        Me.lblVersuch1.TabIndex = 39
        Me.lblVersuch1.Text = "l"
        Me.lblVersuch1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblVersuch2
        '
        Me.lblVersuch2.Font = New System.Drawing.Font("Wingdings", 175.0!)
        Me.lblVersuch2.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.lblVersuch2.Location = New System.Drawing.Point(1110, 60)
        Me.lblVersuch2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblVersuch2.Name = "lblVersuch2"
        Me.lblVersuch2.Size = New System.Drawing.Size(171, 210)
        Me.lblVersuch2.TabIndex = 40
        Me.lblVersuch2.Text = "l"
        Me.lblVersuch2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblVersuch3
        '
        Me.lblVersuch3.Font = New System.Drawing.Font("Wingdings", 175.0!)
        Me.lblVersuch3.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.lblVersuch3.Location = New System.Drawing.Point(1256, 60)
        Me.lblVersuch3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblVersuch3.Name = "lblVersuch3"
        Me.lblVersuch3.Size = New System.Drawing.Size(171, 210)
        Me.lblVersuch3.TabIndex = 41
        Me.lblVersuch3.Text = "l"
        Me.lblVersuch3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblLampe1
        '
        Me.lblLampe1.Font = New System.Drawing.Font("Wingdings", 107.0!)
        Me.lblLampe1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblLampe1.Location = New System.Drawing.Point(626, 141)
        Me.lblLampe1.Name = "lblLampe1"
        Me.lblLampe1.Size = New System.Drawing.Size(112, 129)
        Me.lblLampe1.TabIndex = 101
        Me.lblLampe1.Text = "l"
        Me.lblLampe1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLampe2
        '
        Me.lblLampe2.Font = New System.Drawing.Font("Wingdings", 107.0!)
        Me.lblLampe2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblLampe2.Location = New System.Drawing.Point(731, 141)
        Me.lblLampe2.Name = "lblLampe2"
        Me.lblLampe2.Size = New System.Drawing.Size(112, 129)
        Me.lblLampe2.TabIndex = 102
        Me.lblLampe2.Text = "l"
        Me.lblLampe2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLampe3
        '
        Me.lblLampe3.Font = New System.Drawing.Font("Wingdings", 107.0!)
        Me.lblLampe3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblLampe3.Location = New System.Drawing.Point(835, 141)
        Me.lblLampe3.Name = "lblLampe3"
        Me.lblLampe3.Size = New System.Drawing.Size(112, 129)
        Me.lblLampe3.TabIndex = 103
        Me.lblLampe3.Text = "l"
        Me.lblLampe3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.ForeColor = System.Drawing.Color.PeachPuff
        Me.lblName.Location = New System.Drawing.Point(621, 16)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(800, 73)
        Me.lblName.TabIndex = 104
        Me.lblName.Text = "ANDENABEELE Annelien "
        '
        'lblLast2
        '
        Me.lblLast2.BackColor = System.Drawing.SystemColors.ControlDark
        Me.lblLast2.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold)
        Me.lblLast2.Location = New System.Drawing.Point(1125, 157)
        Me.lblLast2.Margin = New System.Windows.Forms.Padding(0)
        Me.lblLast2.Name = "lblLast2"
        Me.lblLast2.Size = New System.Drawing.Size(120, 58)
        Me.lblLast2.TabIndex = 105
        Me.lblLast2.Text = "255"
        Me.lblLast2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLast3
        '
        Me.lblLast3.BackColor = System.Drawing.SystemColors.ControlDark
        Me.lblLast3.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold)
        Me.lblLast3.Location = New System.Drawing.Point(1271, 157)
        Me.lblLast3.Margin = New System.Windows.Forms.Padding(0)
        Me.lblLast3.Name = "lblLast3"
        Me.lblLast3.Size = New System.Drawing.Size(120, 58)
        Me.lblLast3.TabIndex = 106
        Me.lblLast3.Text = "255"
        Me.lblLast3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLast1
        '
        Me.lblLast1.BackColor = System.Drawing.Color.White
        Me.lblLast1.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold)
        Me.lblLast1.Location = New System.Drawing.Point(979, 157)
        Me.lblLast1.Margin = New System.Windows.Forms.Padding(0)
        Me.lblLast1.Name = "lblLast1"
        Me.lblLast1.Size = New System.Drawing.Size(120, 58)
        Me.lblLast1.TabIndex = 107
        Me.lblLast1.Text = "255"
        Me.lblLast1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmHeberWertung
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1433, 285)
        Me.Controls.Add(Me.lblVersuch)
        Me.Controls.Add(Me.lblLast1)
        Me.Controls.Add(Me.lblLast3)
        Me.Controls.Add(Me.lblLast2)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.lblVersuch3)
        Me.Controls.Add(Me.lblVersuch2)
        Me.Controls.Add(Me.lblVersuch1)
        Me.Controls.Add(Me.Logo)
        Me.Controls.Add(Me.lblLampe1)
        Me.Controls.Add(Me.lblLampe3)
        Me.Controls.Add(Me.lblLampe2)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1433, 285)
        Me.MinimizeBox = False
        Me.Name = "frmHeberWertung"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TV-Wertung"
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Logo As PictureBox
    Friend WithEvents lblVersuch As Label
    Friend WithEvents lblVersuch1 As Label
    Friend WithEvents lblVersuch2 As Label
    Friend WithEvents lblVersuch3 As Label
    Friend WithEvents lblLampe1 As Label
    Friend WithEvents lblLampe2 As Label
    Friend WithEvents lblLampe3 As Label
    Friend WithEvents lblName As Label
    Friend WithEvents lblLast2 As Label
    Friend WithEvents lblLast3 As Label
    Friend WithEvents lblLast1 As Label
End Class
