﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNotiz
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNotiz))
        Me.btnUndo = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.chkUnderline = New System.Windows.Forms.CheckBox()
        Me.chkItalic = New System.Windows.Forms.CheckBox()
        Me.chkBold = New System.Windows.Forms.CheckBox()
        Me.btnFontColor = New System.Windows.Forms.Button()
        Me.rtfNotiz = New System.Windows.Forms.RichTextBox()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'btnUndo
        '
        Me.btnUndo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUndo.Image = CType(resources.GetObject("btnUndo.Image"), System.Drawing.Image)
        Me.btnUndo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnUndo.Location = New System.Drawing.Point(320, 10)
        Me.btnUndo.Name = "btnUndo"
        Me.btnUndo.Size = New System.Drawing.Size(26, 26)
        Me.btnUndo.TabIndex = 517
        Me.btnUndo.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnUndo, "Rückgängig")
        Me.btnUndo.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.Image = CType(resources.GetObject("btnDelete.Image"), System.Drawing.Image)
        Me.btnDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnDelete.Location = New System.Drawing.Point(352, 10)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(26, 26)
        Me.btnDelete.TabIndex = 514
        Me.btnDelete.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnDelete, "Löschen")
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnSave.Enabled = False
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSave.Location = New System.Drawing.Point(384, 10)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(26, 26)
        Me.btnSave.TabIndex = 515
        Me.btnSave.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnSave, "Speichern und schließen")
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'chkUnderline
        '
        Me.chkUnderline.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkUnderline.AutoCheck = False
        Me.chkUnderline.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUnderline.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkUnderline.Location = New System.Drawing.Point(74, 10)
        Me.chkUnderline.Name = "chkUnderline"
        Me.chkUnderline.Size = New System.Drawing.Size(26, 26)
        Me.chkUnderline.TabIndex = 512
        Me.chkUnderline.TabStop = False
        Me.chkUnderline.Text = "U"
        Me.chkUnderline.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.chkUnderline, "Auswahl/Eingabe unterstreichen")
        Me.chkUnderline.UseVisualStyleBackColor = True
        '
        'chkItalic
        '
        Me.chkItalic.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkItalic.AutoCheck = False
        Me.chkItalic.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkItalic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkItalic.Location = New System.Drawing.Point(42, 10)
        Me.chkItalic.Name = "chkItalic"
        Me.chkItalic.Size = New System.Drawing.Size(26, 26)
        Me.chkItalic.TabIndex = 511
        Me.chkItalic.TabStop = False
        Me.chkItalic.Text = "K"
        Me.ToolTip1.SetToolTip(Me.chkItalic, "Auswahl/Eingabe kursiv")
        Me.chkItalic.UseVisualStyleBackColor = True
        '
        'chkBold
        '
        Me.chkBold.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkBold.AutoCheck = False
        Me.chkBold.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBold.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkBold.Location = New System.Drawing.Point(10, 10)
        Me.chkBold.Name = "chkBold"
        Me.chkBold.Size = New System.Drawing.Size(26, 26)
        Me.chkBold.TabIndex = 510
        Me.chkBold.TabStop = False
        Me.chkBold.Text = "F"
        Me.chkBold.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.chkBold, "Auswahl/Eingabe fett")
        Me.chkBold.UseVisualStyleBackColor = True
        '
        'btnFontColor
        '
        Me.btnFontColor.BackColor = System.Drawing.Color.Black
        Me.btnFontColor.FlatAppearance.BorderSize = 2
        Me.btnFontColor.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnFontColor.Location = New System.Drawing.Point(106, 10)
        Me.btnFontColor.Name = "btnFontColor"
        Me.btnFontColor.Size = New System.Drawing.Size(26, 26)
        Me.btnFontColor.TabIndex = 513
        Me.btnFontColor.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnFontColor, "Farbe der Auswahl/Eingabe")
        Me.btnFontColor.UseVisualStyleBackColor = False
        '
        'rtfNotiz
        '
        Me.rtfNotiz.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtfNotiz.BackColor = System.Drawing.SystemColors.Window
        Me.rtfNotiz.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtfNotiz.DetectUrls = False
        Me.rtfNotiz.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.rtfNotiz.HideSelection = False
        Me.rtfNotiz.Location = New System.Drawing.Point(13, 48)
        Me.rtfNotiz.Name = "rtfNotiz"
        Me.rtfNotiz.Size = New System.Drawing.Size(395, 136)
        Me.rtfNotiz.TabIndex = 518
        Me.rtfNotiz.Text = "Hier ist ein Testwwwwwwwww wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww" & Global.Microsoft.VisualBasic.ChrW(10) & "235; 255; 2" &
    "35" & Global.Microsoft.VisualBasic.ChrW(10) & "235; 255; 235" & Global.Microsoft.VisualBasic.ChrW(10) & "235; 255; 235" & Global.Microsoft.VisualBasic.ChrW(10) & "235; 255; 235" & Global.Microsoft.VisualBasic.ChrW(10) & "235; 255; 235" & Global.Microsoft.VisualBasic.ChrW(10) & "235; 255; 235" & Global.Microsoft.VisualBasic.ChrW(10) & "235; 25" &
    "5; 235" & Global.Microsoft.VisualBasic.ChrW(10) & "235; 255; 235"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Location = New System.Drawing.Point(10, 45)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(399, 140)
        Me.Panel1.TabIndex = 520
        '
        'ToolTip1
        '
        Me.ToolTip1.StripAmpersands = True
        '
        'frmNotiz
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(419, 195)
        Me.Controls.Add(Me.rtfNotiz)
        Me.Controls.Add(Me.btnUndo)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.chkUnderline)
        Me.Controls.Add(Me.chkItalic)
        Me.Controls.Add(Me.chkBold)
        Me.Controls.Add(Me.btnFontColor)
        Me.Controls.Add(Me.Panel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(435, 234)
        Me.Name = "frmNotiz"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Notiz"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnUndo As Button
    Friend WithEvents btnDelete As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents chkUnderline As CheckBox
    Friend WithEvents chkItalic As CheckBox
    Friend WithEvents chkBold As CheckBox
    Friend WithEvents btnFontColor As Button
    Friend WithEvents rtfNotiz As RichTextBox
    Friend WithEvents ColorDialog1 As ColorDialog
    Friend WithEvents Panel1 As Panel
    Friend WithEvents ToolTip1 As ToolTip
End Class
