﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWorldRecords
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvRecords = New System.Windows.Forms.DataGridView()
        Me.Sex = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Typ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Resultat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LifterName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Geburtstag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nation = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Datum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ort = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmnuCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmnuCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmnuPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmnuDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuImport = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.cboGK = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboAK = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboTyp = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboSex = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.cboScope = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        CType(Me.dgvRecords, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenu1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvRecords
        '
        Me.dgvRecords.AllowUserToAddRows = False
        Me.dgvRecords.AllowUserToDeleteRows = False
        Me.dgvRecords.AllowUserToResizeColumns = False
        Me.dgvRecords.AllowUserToResizeRows = False
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dgvRecords.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvRecords.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvRecords.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvRecords.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText
        Me.dgvRecords.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.ActiveCaption
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRecords.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvRecords.ColumnHeadersHeight = 24
        Me.dgvRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvRecords.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Sex, Me.AK, Me.GK, Me.Typ, Me.Resultat, Me.LifterName, Me.Geburtstag, Me.Nation, Me.Datum, Me.Ort})
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRecords.DefaultCellStyle = DataGridViewCellStyle20
        Me.dgvRecords.EnableHeadersVisualStyles = False
        Me.dgvRecords.Location = New System.Drawing.Point(0, 62)
        Me.dgvRecords.MultiSelect = False
        Me.dgvRecords.Name = "dgvRecords"
        Me.dgvRecords.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvRecords.RowHeadersVisible = False
        Me.dgvRecords.RowHeadersWidth = 62
        Me.dgvRecords.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvRecords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvRecords.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvRecords.Size = New System.Drawing.Size(884, 422)
        Me.dgvRecords.TabIndex = 510
        Me.dgvRecords.TabStop = False
        '
        'Sex
        '
        Me.Sex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Sex.DataPropertyName = "sex"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Sex.DefaultCellStyle = DataGridViewCellStyle13
        Me.Sex.FillWeight = 30.0!
        Me.Sex.HeaderText = "m/w"
        Me.Sex.MinimumWidth = 8
        Me.Sex.Name = "Sex"
        Me.Sex.ReadOnly = True
        Me.Sex.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Sex.ToolTipText = "Geschlecht"
        Me.Sex.Width = 40
        '
        'AK
        '
        Me.AK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.AK.DataPropertyName = "age_category"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.AK.DefaultCellStyle = DataGridViewCellStyle14
        Me.AK.FillWeight = 65.0!
        Me.AK.HeaderText = "AK"
        Me.AK.MinimumWidth = 8
        Me.AK.Name = "AK"
        Me.AK.ReadOnly = True
        Me.AK.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.AK.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AK.ToolTipText = "Altersklasse"
        Me.AK.Width = 70
        '
        'GK
        '
        Me.GK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.GK.DataPropertyName = "bw_category"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle15.NullValue = Nothing
        DataGridViewCellStyle15.Padding = New System.Windows.Forms.Padding(0, 0, 10, 0)
        Me.GK.DefaultCellStyle = DataGridViewCellStyle15
        Me.GK.FillWeight = 40.0!
        Me.GK.HeaderText = "GK"
        Me.GK.MinimumWidth = 8
        Me.GK.Name = "GK"
        Me.GK.ReadOnly = True
        Me.GK.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GK.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.GK.ToolTipText = "Gewichtsklasse"
        Me.GK.Width = 50
        '
        'Typ
        '
        Me.Typ.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Typ.DataPropertyName = "type"
        Me.Typ.HeaderText = "Typ"
        Me.Typ.MinimumWidth = 8
        Me.Typ.Name = "Typ"
        Me.Typ.ReadOnly = True
        Me.Typ.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Typ.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Typ.Width = 80
        '
        'Resultat
        '
        Me.Resultat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Resultat.DataPropertyName = "result"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle16.Padding = New System.Windows.Forms.Padding(0, 0, 10, 0)
        Me.Resultat.DefaultCellStyle = DataGridViewCellStyle16
        Me.Resultat.FillWeight = 40.0!
        Me.Resultat.HeaderText = "Resultat"
        Me.Resultat.MinimumWidth = 8
        Me.Resultat.Name = "Resultat"
        Me.Resultat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Resultat.Width = 60
        '
        'LifterName
        '
        Me.LifterName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.LifterName.DataPropertyName = "name"
        Me.LifterName.FillWeight = 110.0!
        Me.LifterName.HeaderText = "Name"
        Me.LifterName.MinimumWidth = 8
        Me.LifterName.Name = "LifterName"
        Me.LifterName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Geburtstag
        '
        Me.Geburtstag.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Geburtstag.DataPropertyName = "born"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle17.Format = "d"
        DataGridViewCellStyle17.NullValue = Nothing
        Me.Geburtstag.DefaultCellStyle = DataGridViewCellStyle17
        Me.Geburtstag.FillWeight = 50.0!
        Me.Geburtstag.HeaderText = "Geburtstag"
        Me.Geburtstag.MinimumWidth = 8
        Me.Geburtstag.Name = "Geburtstag"
        Me.Geburtstag.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Geburtstag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Geburtstag.Width = 80
        '
        'Nation
        '
        Me.Nation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Nation.DataPropertyName = "nation"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Nation.DefaultCellStyle = DataGridViewCellStyle18
        Me.Nation.FillWeight = 50.0!
        Me.Nation.HeaderText = "Nation"
        Me.Nation.MinimumWidth = 8
        Me.Nation.Name = "Nation"
        Me.Nation.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Nation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Nation.Width = 50
        '
        'Datum
        '
        Me.Datum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Datum.DataPropertyName = "date"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle19.Format = "d"
        Me.Datum.DefaultCellStyle = DataGridViewCellStyle19
        Me.Datum.FillWeight = 40.0!
        Me.Datum.HeaderText = "Datum"
        Me.Datum.MinimumWidth = 8
        Me.Datum.Name = "Datum"
        Me.Datum.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Datum.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Datum.Width = 80
        '
        'Ort
        '
        Me.Ort.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Ort.DataPropertyName = "place"
        Me.Ort.FillWeight = 90.0!
        Me.Ort.HeaderText = "Ort"
        Me.Ort.MinimumWidth = 8
        Me.Ort.Name = "Ort"
        Me.Ort.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Ort.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'ContextMenu1
        '
        Me.ContextMenu1.AutoSize = False
        Me.ContextMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmnuCut, Me.cmnuCopy, Me.cmnuPaste, Me.cmnuDelete})
        Me.ContextMenu1.Name = "ContextMenu"
        Me.ContextMenu1.ShowImageMargin = False
        Me.ContextMenu1.Size = New System.Drawing.Size(105, 92)
        '
        'cmnuCut
        '
        Me.cmnuCut.Name = "cmnuCut"
        Me.cmnuCut.Size = New System.Drawing.Size(129, 22)
        Me.cmnuCut.Text = "Ausscchneiden"
        '
        'cmnuCopy
        '
        Me.cmnuCopy.Name = "cmnuCopy"
        Me.cmnuCopy.Size = New System.Drawing.Size(129, 22)
        Me.cmnuCopy.Text = "Kopieren"
        '
        'cmnuPaste
        '
        Me.cmnuPaste.Name = "cmnuPaste"
        Me.cmnuPaste.Size = New System.Drawing.Size(129, 22)
        Me.cmnuPaste.Text = "Einfügen"
        '
        'cmnuDelete
        '
        Me.cmnuDelete.Name = "cmnuDelete"
        Me.cmnuDelete.Size = New System.Drawing.Size(129, 22)
        Me.cmnuDelete.Text = "Löschen"
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Enabled = False
        Me.btnSave.Location = New System.Drawing.Point(676, 499)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(95, 28)
        Me.btnSave.TabIndex = 511
        Me.btnSave.Text = "Speichern"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(777, 499)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(95, 28)
        Me.btnExit.TabIndex = 512
        Me.btnExit.Text = "Beenden"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuEdit, Me.mnuImport})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(884, 24)
        Me.MenuStrip1.TabIndex = 513
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSave, Me.ToolStripMenuItem1, Me.mnuExit})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 20)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuSave
        '
        Me.mnuSave.Enabled = False
        Me.mnuSave.Image = Global.Gewichtheben.My.Resources.Resources.saveHS
        Me.mnuSave.Name = "mnuSave"
        Me.mnuSave.Size = New System.Drawing.Size(126, 22)
        Me.mnuSave.Text = "&Speichern"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(123, 6)
        '
        'mnuExit
        '
        Me.mnuExit.Image = Global.Gewichtheben.My.Resources.Resources.Exit_icon
        Me.mnuExit.Name = "mnuExit"
        Me.mnuExit.Size = New System.Drawing.Size(126, 22)
        Me.mnuExit.Text = "&Beenden"
        '
        'mnuEdit
        '
        Me.mnuEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete})
        Me.mnuEdit.Name = "mnuEdit"
        Me.mnuEdit.Size = New System.Drawing.Size(75, 20)
        Me.mnuEdit.Text = "&Bearbeiten"
        Me.mnuEdit.Visible = False
        '
        'mnuCut
        '
        Me.mnuCut.Name = "mnuCut"
        Me.mnuCut.ShortcutKeyDisplayString = "Strg+X"
        Me.mnuCut.Size = New System.Drawing.Size(191, 22)
        Me.mnuCut.Text = "&Ausschneiden"
        '
        'mnuCopy
        '
        Me.mnuCopy.Name = "mnuCopy"
        Me.mnuCopy.ShortcutKeyDisplayString = "Strg+C"
        Me.mnuCopy.Size = New System.Drawing.Size(191, 22)
        Me.mnuCopy.Text = "&Kopieren"
        '
        'mnuPaste
        '
        Me.mnuPaste.Name = "mnuPaste"
        Me.mnuPaste.ShortcutKeyDisplayString = "Strg+V"
        Me.mnuPaste.Size = New System.Drawing.Size(191, 22)
        Me.mnuPaste.Text = "&Einfügen"
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        Me.mnuDelete.ShortcutKeyDisplayString = "Entf"
        Me.mnuDelete.Size = New System.Drawing.Size(191, 22)
        Me.mnuDelete.Text = "&Löschen"
        '
        'mnuImport
        '
        Me.mnuImport.Name = "mnuImport"
        Me.mnuImport.Size = New System.Drawing.Size(55, 20)
        Me.mnuImport.Text = "&Import"
        Me.mnuImport.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cboScope)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.btnImport)
        Me.Panel1.Controls.Add(Me.cboGK)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.cboAK)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.cboTyp)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.cboSex)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 24)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(884, 39)
        Me.Panel1.TabIndex = 514
        '
        'btnImport
        '
        Me.btnImport.Enabled = False
        Me.btnImport.ForeColor = System.Drawing.Color.Firebrick
        Me.btnImport.Location = New System.Drawing.Point(740, 7)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(132, 24)
        Me.btnImport.TabIndex = 9
        Me.btnImport.Text = "Rekorde importieren"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'cboGK
        '
        Me.cboGK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGK.FormattingEnabled = True
        Me.cboGK.Location = New System.Drawing.Point(618, 8)
        Me.cboGK.Name = "cboGK"
        Me.cboGK.Size = New System.Drawing.Size(80, 21)
        Me.cboGK.TabIndex = 8
        Me.ToolTip1.SetToolTip(Me.cboGK, "Gewichtsklasse filtern")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(594, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(22, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "GK"
        Me.ToolTip1.SetToolTip(Me.Label4, "Gewichtsklasse")
        '
        'cboAK
        '
        Me.cboAK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAK.FormattingEnabled = True
        Me.cboAK.Location = New System.Drawing.Point(486, 8)
        Me.cboAK.Name = "cboAK"
        Me.cboAK.Size = New System.Drawing.Size(80, 21)
        Me.cboAK.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.cboAK, "Altersklasse filtern")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(463, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(21, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "AK"
        Me.ToolTip1.SetToolTip(Me.Label3, "Altersklasse")
        '
        'cboTyp
        '
        Me.cboTyp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTyp.FormattingEnabled = True
        Me.cboTyp.Items.AddRange(New Object() {"", "Snatch", "Clean and Jerk", "Total"})
        Me.cboTyp.Location = New System.Drawing.Point(357, 8)
        Me.cboTyp.Name = "cboTyp"
        Me.cboTyp.Size = New System.Drawing.Size(80, 21)
        Me.cboTyp.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.cboTyp, "Durchgang filtern")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(330, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Typ"
        Me.ToolTip1.SetToolTip(Me.Label2, "Durchgang")
        '
        'cboSex
        '
        Me.cboSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSex.FormattingEnabled = True
        Me.cboSex.Items.AddRange(New Object() {"", "männlich", "weiblich"})
        Me.cboSex.Location = New System.Drawing.Point(224, 8)
        Me.cboSex.Name = "cboSex"
        Me.cboSex.Size = New System.Drawing.Size(80, 21)
        Me.cboSex.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.cboSex, "Geschlecht filtern")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(161, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Geschlecht"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Title = "Weltrekorde importieren "
        '
        'cboScope
        '
        Me.cboScope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScope.FormattingEnabled = True
        Me.cboScope.Items.AddRange(New Object() {"", "Welt", "Europa"})
        Me.cboScope.Location = New System.Drawing.Point(57, 8)
        Me.cboScope.Name = "cboScope"
        Me.cboScope.Size = New System.Drawing.Size(80, 21)
        Me.cboScope.TabIndex = 11
        Me.ToolTip1.SetToolTip(Me.cboScope, "Geschlecht filtern")
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(13, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Bereich"
        '
        'frmWorldRecords
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(884, 539)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.dgvRecords)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(900, 578)
        Me.Name = "frmWorldRecords"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Rekorde (Welt & Europa)"
        CType(Me.dgvRecords, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenu1.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvRecords As DataGridView
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents mnuDatei As ToolStripMenuItem
    Friend WithEvents mnuSave As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents mnuExit As ToolStripMenuItem
    Friend WithEvents Panel1 As Panel
    Friend WithEvents cboTyp As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents cboSex As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cboAK As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cboGK As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Sex As DataGridViewTextBoxColumn
    Friend WithEvents AK As DataGridViewTextBoxColumn
    Friend WithEvents GK As DataGridViewTextBoxColumn
    Friend WithEvents Typ As DataGridViewTextBoxColumn
    Friend WithEvents Resultat As DataGridViewTextBoxColumn
    Friend WithEvents LifterName As DataGridViewTextBoxColumn
    Friend WithEvents Geburtstag As DataGridViewTextBoxColumn
    Friend WithEvents Nation As DataGridViewTextBoxColumn
    Friend WithEvents Datum As DataGridViewTextBoxColumn
    Friend WithEvents Ort As DataGridViewTextBoxColumn
    Friend WithEvents mnuEdit As ToolStripMenuItem
    Friend WithEvents mnuCut As ToolStripMenuItem
    Friend WithEvents mnuCopy As ToolStripMenuItem
    Friend WithEvents mnuPaste As ToolStripMenuItem
    Friend WithEvents mnuDelete As ToolStripMenuItem
    Friend WithEvents ContextMenu1 As ContextMenuStrip
    Friend WithEvents cmnuCut As ToolStripMenuItem
    Friend WithEvents cmnuCopy As ToolStripMenuItem
    Friend WithEvents cmnuPaste As ToolStripMenuItem
    Friend WithEvents cmnuDelete As ToolStripMenuItem
    Friend WithEvents mnuImport As ToolStripMenuItem
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents btnImport As Button
    Friend WithEvents cboScope As ComboBox
    Friend WithEvents Label5 As Label
End Class
