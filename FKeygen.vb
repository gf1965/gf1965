﻿
Imports System.IO
Imports System.Globalization
Imports AryaSoftLock
Imports Microsoft.Win32

Public Class FKeygen
    Property Status As String = String.Empty

    Function Check_LicenceInRegistry() As String
        Dim HardCode = Get_HardwareID()
        Dim RegKeys = Get_RegKeys()
        If RegKeys.ContainsKey("Activation") And RegKeys.ContainsKey("Serial") Then
            If RegKeys("Activation") = StringEncryptorDecryptor.EncryptString(RegKeys("Serial"), HardCode.Replace("-", "")) Then
                Return "Verified"
            Else
                Return "Invalid"
            End If
        ElseIf RegKeys.ContainsKey("Valid") Then
            Return "Test"
        End If
        Return String.Empty
    End Function
    Private Sub FKeygen_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        txtHardCode.Text = Get_HardwareID()
        Dim RegKeys = Get_RegKeys()
        If RegKeys.ContainsKey("Serial") Then txtSerial.Text = Format_S(RegKeys("Serial")) '"YUYV1-06WT6-W2XX0-3F343-32652-746BA" 'Guid.NewGuid().ToString
        btnTestversion.Visible = Not RegKeys.ContainsKey("Valid")
        Cursor = Cursors.Default
    End Sub
    Private Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click
        Cursor = Cursors.WaitCursor
        Dim key As RegistryKey
        Dim ActivationCode = StringEncryptorDecryptor.EncryptString(txtSerial.Text.Replace("-", ""), txtHardCode.Text.Replace("-", ""))
        Try
            'Dim writer As New StreamWriter(New FileStream("SerialConfig.cfg", FileMode.Create, FileAccess.Write))
            'writer.WriteLine(txtSerial.Text.Replace("-", ""))
            'writer.WriteLine(txtActivationCode.Text)
            'writer.Flush()
            'writer.Close()
            key = Registry.CurrentUser.CreateSubKey("Software\AryaTranslator\AryaTranslatorSerial", RegistryKeyPermissionCheck.ReadWriteSubTree)
            key.OpenSubKey("Software\AryaTranslator\AryaTranslatorSerial", True)
            key.SetValue("Serial", txtSerial.Text.Replace("-", ""), RegistryValueKind.String)
            key.SetValue("Activation", ActivationCode, RegistryValueKind.String)
            key.Flush()
            key.Close()
            Status = "Success"
        Catch ex As Exception
            Status = "Failed"
            Using New Centered_MessageBox(Me)
                If MessageBox.Show("Lizensierung fehlgeschlagen", "GFHsoft Lizenz",
                                   MessageBoxButtons.RetryCancel,
                                   MessageBoxIcon.Information) = DialogResult.Retry Then
                    Cursor = Cursors.Default
                    Return
                End If
            End Using
        End Try
        Close()
    End Sub
    Private Sub btnTestversion_Click(sender As Object, e As EventArgs) Handles btnTestversion.Click
        Cursor = Cursors.WaitCursor
        Dim key As RegistryKey
        Dim Valid As String = Now.AddDays(30).ToString
        Dim Encrypter As New Crypter("GFHsoft")
        Dim Encrypt = Encrypter.EncryptData(Valid)
        Try
            key = Registry.CurrentUser.CreateSubKey("Software\AryaTranslator\AryaTranslatorSerial", RegistryKeyPermissionCheck.ReadWriteSubTree)
            key.OpenSubKey("Software\AryaTranslator\AryaTranslatorSerial", True)
            key.SetValue("Valid", Encrypt)
            key.Flush()
            key.Close()
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Test-Zeitraum bis zum " & Split(Valid, " ")(0), "GFHsoft Lizenz",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
        End Try
        Close()
    End Sub
    Private Function Get_HardwareID() As String
        Dim getter As New CpuIdGetter
        Return getter.ComputerHardWare_ID
    End Function
    Function Get_RegKeys() As Dictionary(Of String, String)
        Dim RegKeys As New Dictionary(Of String, String)
        Try
            Using Key As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\AryaTranslator\AryaTranslatorSerial", RegistryKeyPermissionCheck.ReadSubTree)
                If Not IsNothing(Key) Then
                    Dim ValueNames = Key.GetValueNames.ToList
                    For Each ValueName In ValueNames
                        RegKeys(ValueName) = Key.GetValue(ValueName).ToString
                    Next
                End If
            End Using
        Catch ex As Exception
        End Try
        Return RegKeys
    End Function
    Private Function Format_S(Source As String, Optional Limiter As Integer = 5, Optional Delimiter As String = "-") As String
        Dim List As New List(Of String)
        Dim Start = 1
        Do While Source.Length > Start
            List.Add(Mid(Source, Start, Limiter))
            Start += Limiter
        Loop
        Return Join(List.ToArray, Delimiter)
    End Function

    Private Sub txtHardCode_GotFocus(sender As Object, e As EventArgs) Handles txtHardCode.GotFocus
        txtSerial.Focus()
    End Sub

    Private Sub txtSerial_TextChanged(sender As Object, e As EventArgs) Handles txtSerial.TextChanged
        btnRegister.Enabled = txtSerial.Text.Length > 24
    End Sub
End Class

Public Class CpuIdGetter
    ' Methods
    Private Sub CPUCode2Serial()
        Dim startIndex As Integer = (CPUCode.Length \ 4)
        Dim s As String = CPUCode.Substring(0, startIndex - 1)
        Dim str2 As String = CPUCode.Substring(startIndex, startIndex - 1)
        Dim str3 As String = CPUCode.Substring(startIndex * 2, startIndex - 1)
        Dim str4 As String = CPUCode.Substring(startIndex * 3, startIndex - 1)
        Dim num3 As ULong = ULong.Parse(s, NumberStyles.AllowHexSpecifier)
        Dim num4 As ULong = ULong.Parse(str2, NumberStyles.AllowHexSpecifier)
        Dim num5 As ULong = ULong.Parse(str3, NumberStyles.AllowHexSpecifier)
        Dim num6 As ULong = ULong.Parse(str4, NumberStyles.AllowHexSpecifier)
        s = num3.ToString
        Do While s.Length < 5
            s = "0" & s
        Loop
        str2 = num4.ToString
        Do While str2.Length < 5
            str2 = "0" & str2
        Loop
        str3 = num5.ToString
        Do While str3.Length < 5
            str3 = "0" & str3
        Loop
        str4 = num6.ToString
        Do While str4.Length < 5
            str4 = "0" & str4
        Loop
        TempMachineId = String.Concat(New String() {s, "-", str2, "-", str3, "-", str4})
    End Sub

    Private Sub MakeHardwareID()
        ComputerHardWare_ID = ""
        Dim num As Integer = 0
        Dim num2 As Integer = 0
        'num
        For num = 0 To TempMachineId.Length - 1
            num2 += 1
            If (num2 = 6) Then
                num2 = 0
                ComputerHardWare_ID = ComputerHardWare_ID & "-"
            Else
                Dim ch As Char = TempMachineId.Chars(num)
                Dim s As String = ch.ToString
                Dim str2 As String = "0"
                If (num < BaseBoardSerial.Length) Then
                    str2 = BaseBoardSerial.Chars(num).ToString
                End If
                ComputerHardWare_ID = ComputerHardWare_ID & ((Integer.Parse(s) + Integer.Parse(str2)) Mod 10).ToString
            End If
        Next num
    End Sub

    Private Function GetCPUCode() As String
        Dim str As String = "Win32_Processor"
        Dim str2 As String = ""
        Dim searcher As New ManagementObjectSearcher("select ProcessorId from " & str)
        Dim obj2 As ManagementObject
        For Each obj2 In searcher.Get
            Dim data As PropertyData
            For Each data In obj2.Properties
                str2 = CStr(data.Value)
                Exit For
            Next
            Exit For
        Next
        CPUCode = str2
        Return str2
    End Function

    Private Function GetBaseBoardCode() As String
        Dim str As String = "Win32_BaseBoard"
        Dim str2 As String = ""
        Dim searcher As New ManagementObjectSearcher("select SerialNumber from " & str)
        Dim obj2 As ManagementObject
        For Each obj2 In searcher.Get
            Dim data As PropertyData
            For Each data In obj2.Properties
                str2 = CStr(data.Value)
                Exit For
            Next
            Exit For
        Next
        BaseBoardCode = str2
        Return str2
    End Function

    Private Function SaveCPUCode() As String
        CPUCode2 = CPUCode
        Return CPUCode
    End Function

    Private Sub BaseBoardCode2Serial()
        Dim length As Integer = BaseBoardCode.Length
        Dim num2 As Integer = 0
        Dim num3 As Integer = 0
        Dim str As String = ""
        num2 = (length - 1)
        Do While (num2 >= 0)
            Dim s As String = BaseBoardCode.Chars(num2).ToString
            Try
                Integer.Parse(s)
            Catch obj1 As Exception
                Dim ch As Char = s.Chars(0)
                Dim num4 As Integer = AscW(ch)
                s = (num4 Mod 10).ToString
            End Try
            num3 += 1
            If num3 = 6 Then
                str = str & "-"
                num3 = 1
            End If
            str = str & s
            num2 -= 1
        Loop
        BaseBoardSerial = str
    End Sub

    Public Sub New()
        Try
            CPUCode = ""
            CPUCode2 = ""
            TempMachineId = ""
            GetCPUCode()
            SaveCPUCode()
            CPUCode2Serial()
            GetBaseBoardCode()
            BaseBoardCode2Serial()
            MakeHardwareID()
        Catch obj1 As Exception
            CPUCode = ""
            CPUCode2 = ""
            TempMachineId = ""
        End Try
    End Sub


    ' Fields
    Private CPUSerial As String
    Private BaseBoardSerial As String
    Private CPUCode As String
    Private CPUCode2 As String
    Private TempMachineId As String
    Private BaseBoardCode As String
    Public ComputerHardWare_ID As String
End Class


