﻿Imports MySqlConnector

Module DataServiceModul
    Class DataService
        Implements IDisposable

        Private TableNames() As String = {"Wettkampf", "Wettkampf-Details"} ', "Zeitplan"}

#Region "IDisposable Support"
        Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.
        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
                End If

                ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
                ' TODO: große Felder auf Null setzen.
            End If
            disposedValue = True
        End Sub

        ' TODO: Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
        'Protected Overrides Sub Finalize()
        '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
            Dispose(True)
            ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
            ' GC.SuppressFinalize(Me)
        End Sub
#End Region

        Sub Set_Teams(WK_Keys As Object(), Optional conn As MySqlConnection = Nothing)
            Dim ThisConn As MySqlConnection = conn

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            ' Teams bei (Bundes)Liga-WKs
            Glob.dtTeams = New DataTable
            Dim query = "SELECT t.Id, t.Team, t.Punkte, t.Punkte2, t.Reissen, t.Stossen, t.Heben, ifnull(v.Kurzname, v.Vereinsname) Kurzname, v.Vereinsname, t.Sieger, t.Platz, " &
                                "If(t.Id = 0, 'Heim', Concat('Gast ', t.Id)) Rolle " &
                                "FROM wettkampf_teams t " &
                                "LEFT JOIN verein v ON t.Team = v.idverein " &
                                "WHERE " & Wettkampf.WhereClause("t", WK_Keys) &
                                " GROUP BY t.Team ORDER BY t.Id;"
            Dim cmd = New MySqlCommand(query, ThisConn)
            Glob.dtTeams.Load(cmd.ExecuteReader)

            ' wenn ein Team gegen alle anderen antritt
            TeamCombinations = New List(Of Object)
            Wettkampf.ShareTeam = 0
            Dim tbl As New DataTable
            query = "select distinct Team, count(*) over(partition by Team) ShareTeam
                                from wettkampf_teams 
                                where " & Wettkampf.WhereClause(, WK_Keys) &
                                "order by ShareTeam desc;"
            cmd = New MySqlCommand(query, ThisConn)
            tbl.Load(cmd.ExecuteReader)
            If tbl.Rows.Count > 0 AndAlso CInt(tbl.Rows(0)!ShareTeam) > 1 Then
                Wettkampf.ShareTeam = CInt(tbl.Rows(0)!Team)
            End If
            ' sortierte Liste der Teams
            Dim TeamIds = (From x In Glob.dtTeams
                           Select x.Field(Of Integer)("Team")).ToList()
            If Wettkampf.ShareTeam > 0 Then
                ' ShareTeam an den Anfang setzen
                Dim ShareTeam = Wettkampf.ShareTeam
                TeamIds.Remove(Wettkampf.ShareTeam)
                TeamIds.Insert(0, ShareTeam)
                ' Team-Kombinationen für Multi-WK
                For i = 1 To TeamIds.Count - 1
                    TeamCombinations.Add(New Combination With {.a = TeamIds(0), .b = TeamIds(i)})
                Next
            ElseIf Wettkampf.Finale Then
                ' Team-Kombinationen für Liga-Finale
                TeamCombinations.Add(New Combination With {.a = TeamIds(0), .b = TeamIds(1)})
                TeamCombinations.Add(New Combination With {.a = TeamIds(1), .b = TeamIds(2)})
                TeamCombinations.Add(New Combination With {.a = TeamIds(2), .b = TeamIds(0)})
                Glob.dtTeams.Columns.Add(New DataColumn With {.ColumnName = "Gegner", .Caption = "Gegner", .DataType = GetType(String)})
                For Each c As Combination In TeamCombinations
                    Dim t1 = Glob.dtTeams.Select("Team = " & c.a)
                    Dim t2 = Glob.dtTeams.Select("Team = " & c.b)
                    t1(0)!Gegner = t2(0)!Kurzname
                Next
                Glob.dtTeams.AcceptChanges()
            ElseIf Wettkampf.Mannschaft Then
                ' alle Teams kombinieren
                For n = 1 To TeamIds.Count - 1
                    For i = n To TeamIds.Count - 1
                        TeamCombinations.Add(New Combination With {.a = TeamIds(n - 1), .b = TeamIds(i)})
                    Next
                Next
            End If

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If
        End Sub
        Sub Set_Global_Data(Param As String, Optional conn As MySqlConnection = Nothing)
            Select Case Param
                Case "Rekorde"
                    Glob.dtRekorde = Get_DataTable(Param, conn)
                    Glob.dtRekorde.Columns.Remove("row_num")
                    Glob.dvRekorde = New DataView(Glob.dtRekorde)
                    Glob.dvRekorde.Sort = "scope, sex, age_id, bw_id, lift"
            End Select
        End Sub

        Private Function Get_DataTable(param As String, Optional conn As MySqlConnection = Nothing) As DataTable

            Dim ThisConn As MySqlConnection = conn

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            Dim cmd = New MySqlCommand(Get_Query(param), ThisConn)
            Dim table As New DataTable
            table.Load(cmd.ExecuteReader)

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If

            Return table
        End Function

        Private Function Get_Query(param As String) As String
            Select Case param
                Case "Rekorde"
                    Return "with _records as (
	                            select *, row_number() over (partition by sex, lift, age_id, bw_id order by result desc) row_num 
	                            from world_records
                            )
                            select *
                            from _records
                            where row_num = 1
                            order by sex, age_id, bw_id, lift;"
            End Select

            Return String.Empty
        End Function
        Function Get_Athletik_Defaults(Optional WKs As Object() = Nothing,
                                       Optional OnlyDefault As Boolean = False,
                                       Optional conn As MySqlConnection = Nothing) As DataTable
            Dim ThisConn As MySqlConnection = conn

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            ' WKs = optional List of WK-Ids, nothing if WK is selected
            ' Returns merged Athletik defaults for selected WK

            ' wettkampf_athletik_defaults
            Dim Query = "SELECT * FROM wettkampf_athletik_defaults WHERE " & Wettkampf.WhereClause(, WKs) & ";"
            Dim cmd = New MySqlCommand(Query, ThisConn)
            Dim wk_defaults As New DataTable
            wk_defaults.Load(cmd.ExecuteReader)

            ' athletik_defaults
            Query = "select " & Wettkampf.ID & " Wettkampf, a.* from athletik_defaults a 
                         where Bezeichnung = 'Faktor' or 
                            Bezeichnung = 'Kugel' or 
                           (Bezeichnung = 'Anzahl' and Disziplin <> 20 and Disziplin <> 130);"
            cmd = New MySqlCommand(Query, ThisConn)
            Dim defaults = wk_defaults.Clone
            defaults.Load(cmd.ExecuteReader)

            If OnlyDefault Then Return defaults ' ******************************

            'ElseIf WKs.Length > 0 Then
            '        For Each wk In WKs
            '        Query = "select " & wk.ToString & " Wettkampf, a.* from athletik_defaults a " & WherePart
            '        cmd = New MySqlCommand(Query, conn)
            '        Dim tmp = defaults.Clone
            '        tmp.Load(cmd.ExecuteReader)
            '        defaults.Merge(tmp)
            '    Next
            'End If

            'defaults.Merge(wk_defaults, False)

            For Each row As DataRow In wk_defaults.Rows
                Dim Filter = "Wettkampf = 0 and Disziplin = " & row!Disziplin.ToString & " and Bezeichnung = '" & row!Bezeichnung.ToString & "' and Geschlecht = '" & row!Geschlecht.ToString & "'"
                Dim old = defaults.Select(Filter)
                If old.Length > 0 Then
                    old(0).ItemArray = row.ItemArray
                Else
                    defaults.Rows.Add(row)
                End If
            Next
            defaults.AcceptChanges()

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If

            Return defaults
        End Function

        Function Get_Modus_Gewichtsgruppen(Optional conn As MySqlConnection = Nothing) As DataTable
            Dim ThisConn As MySqlConnection = conn

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            Dim Query = "SELECT * FROM modus_gewichtsgruppen ORDER BY Geschlecht, Gruppe;"
            Dim cmd = New MySqlCommand(Query, ThisConn)
            Dim table As New DataTable
            table.Load(cmd.ExecuteReader)

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If

            Return table
        End Function
        Function Get_Wettkampf(WK_Keys As Object(), Optional conn As MySqlConnection = Nothing) As DataTable
            Dim table As New DataTable
            Dim ThisConn As MySqlConnection = conn

            'Query = "Select w.Wettkampf, w.Datum, w.DatumBis, w.Kurz, b.Bez1, b.Bez2, b.Bez3, w.Ort, w.GUID 
            '         FROM wettkampf w LEFT JOIN wettkampf_bezeichnung b On w.Wettkampf = b.Wettkampf 
            '         WHERE " & Wettkampf.WhereClause("w", WK_Keys) & ";"
            Dim Query = "Select w.*, b.Bez1, b.Bez2, b.Bez3, c.Kommentar  
                         FROM wettkampf w 
                         LEFT JOIN wettkampf_bezeichnung b On w.Wettkampf = b.Wettkampf 
                         LEFT JOIN wettkampf_comment c on w.Wettkampf = c.Wettkampf
                         WHERE " & Wettkampf.WhereClause("w", WK_Keys) & ";"

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            Dim cmd = New MySqlCommand(Query, ThisConn)
            table.Load(cmd.ExecuteReader)

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If

            Return table
        End Function
        Function Get_Wettkampf_Identities(WK_ID As String,
                                          Optional myForm As Form = Nothing,
                                          Optional conn As MySqlConnection = Nothing) As Dictionary(Of String, String)
            Dim dic As New Dictionary(Of String, String)
            Dim ThisConn As MySqlConnection = conn


            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
            End If

            Do
                Dim Query = "select distinct u.*, count(*) over(partition by m.Wettkampf) Anzahl, if(w.Kurz = '', b.Bez1, w.Kurz) Bezeichnung 
                    from wettkampf_multi u 
                    left join meldung m on m.wettkampf = u.Wettkampf 
                    left join wettkampf w on w.Wettkampf = u.wettkampf 
                    left join wettkampf_bezeichnung b on b.Wettkampf = u.Wettkampf 
                    where find_in_set('" & WK_ID & "', u.Multi) != 0 
                    order by u.Primaer desc, Anzahl desc, u.Wettkampf asc;"

                Try
                    If Not ThisConn.State = ConnectionState.Open Then ThisConn.Open()

                    ' mehrere WKs gleichzeitig 
                    Dim dtWKs As New DataTable
                    Dim cmd = New MySqlCommand(Query, ThisConn)
                    dtWKs.Load(cmd.ExecuteReader)

                    ' Wettkampf.Identities muss IMMER mindestens einen Eintrag --> {Id, Bez1} enthalten
                    ' Haupt-WK ist immer Wettkampf.Identities(0) --> also Primaer = True || meiste Teilnehmer

                    If dtWKs.Rows.Count = 0 Then
                        Query = "select w.Wettkampf, if(w.Kurz = '', b.Bez1, w.Kurz) Bezeichnung 
                                from wettkampf w 
                                left join wettkampf_bezeichnung b on b.Wettkampf = w.Wettkampf   
                                where w.Wettkampf = " & WK_ID & ";"
                        cmd = New MySqlCommand(Query, ThisConn)
                        dtWKs = New DataTable
                        dtWKs.Load(cmd.ExecuteReader)
                    End If

                    For Each row As DataRow In dtWKs.Rows
                        dic(row!Wettkampf.ToString) = row!Bezeichnung.ToString
                    Next

                    ConnError = False
                Catch ex As MySqlException
                    If MySQl_Error(myForm, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                End Try
            Loop While ConnError

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If

            Return dic
        End Function
        Function Get_Wettkampf_MeldeFrist(WK_Keys As Object(), Optional conn As MySqlConnection = Nothing) As DataTable
            Dim table As New DataTable
            Dim ThisConn As MySqlConnection = conn

            Dim Query = "SELECT * FROM wettkampf_meldefrist WHERE " & Wettkampf.WhereClause(, WK_Keys) & ";"

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            Dim cmd = New MySqlCommand(Query, ThisConn)
            table.Load(cmd.ExecuteReader)

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If

            Return table
        End Function
        Function Get_Wettkampf_Startgeld(WK_Keys As Object(), Optional conn As MySqlConnection = Nothing) As DataTable
            Dim table As New DataTable
            Dim ThisConn As MySqlConnection = conn

            Dim Query = "SELECT * FROM wettkampf_startgeld WHERE " & Wettkampf.WhereClause(, WK_Keys) & ";"

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            Dim cmd = New MySqlCommand(Query, ThisConn)
            table.Load(cmd.ExecuteReader)

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If

            Return table
        End Function
        Function Get_Wettkampf_Details(WK_Keys As Object(), Optional conn As MySqlConnection = Nothing) As DataTable
            Dim table As New DataTable
            Dim ThisConn As MySqlConnection = conn

            Dim Query = "SELECT * FROM wettkampf_details WHERE " & Wettkampf.WhereClause(, WK_Keys) & ";"

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            Dim cmd = New MySqlCommand(Query, ThisConn)
            table.Load(cmd.ExecuteReader)

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If

            Return table
        End Function
        Function Get_Wettkampf_Zeitplan(WK_Keys As Object(), Optional conn As MySqlConnection = Nothing) As DataTable

            ' Zeitplan wird bei Multi-Wettkampf nicht überprüft
            ' Abfrage alle Felder mit max(Wert) ???

            Dim table As New DataTable
            Dim ThisConn As MySqlConnection = conn

            Dim Query = "SELECT * FROM wettkampf_Zeitplan WHERE " & Wettkampf.WhereClause(, WK_Keys) & ";"

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            Dim cmd = New MySqlCommand(Query, ThisConn)
            table.Load(cmd.ExecuteReader)

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If

            Return table
        End Function
        Function Get_Wettkampf_Comments(WK_Keys As Object(), Optional conn As MySqlConnection = Nothing) As String

            Dim Comment As String = String.Empty
            Dim ThisConn As MySqlConnection = conn

            Dim Query = "SELECT * FROM wettkampf_comment WHERE " & Wettkampf.WhereClause(, WK_Keys) & ";"

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            Dim cmd = New MySqlCommand(Query, ThisConn)
            Dim reader = cmd.ExecuteReader
            If reader.HasRows Then
                reader.Read()
                Comment = reader!Kommentar.ToString
            End If
            reader.Close()

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If

            Return Comment
        End Function

        Function Save_Wettkampf_Comments(WK_Keys As Object(), Comment As String, Form As Form) As Boolean

            Dim Query = "INSERT INTO wettkampf_comment (Wettkampf, Kommentar) VALUES (@wk, @comment) " &
                        "ON DUPLICATE KEY UPDATE Kommentar = @comment;"
            Using Conn As New MySqlConnection(User.ConnString)
                Dim cmd = New MySqlCommand(Query, Conn)
                With cmd.Parameters
                    .AddWithValue("@wk", WK_Keys(0).ToString)
                    .AddWithValue("@comment", Comment)
                End With
                Do
                    Try
                        Conn.Open()
                        cmd.ExecuteNonQuery()
                        ConnError = False
                    Catch ex As MySqlException
                        If MySQl_Error(Form, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                    End Try
                Loop While ConnError
            End Using

            Return CBool(ConnError)
        End Function

        'Function Get_World_Records(Optional conn As MySqlConnection = Nothing) As DataTable
        '    Dim table As New DataTable
        '    Dim ThisConn As MySqlConnection = conn

        '    Query = ""

        '    If IsNothing(ThisConn) Then
        '        ThisConn = New MySqlConnection(User.ConnString)
        '        ThisConn.Open()
        '    End If
        '    cmd = New MySqlCommand(Query, ThisConn)
        '    table.Load(cmd.ExecuteReader)

        '    If IsNothing(conn) Then
        '        ThisConn.Close()
        '        ThisConn.Dispose()
        '    End If

        '    Return table
        'End Function

        Function Get_MultiBohle(Optional conn As MySqlConnection = Nothing) As Boolean
            Dim ThisConn As MySqlConnection = conn
            Dim Query = "SELECT DISTINCT Bohle FROM gruppen WHERE " & Wettkampf.WhereClause() & ";"
            Dim table As New DataTable

            Try
                If IsNothing(ThisConn) Then
                    ThisConn = New MySqlConnection(User.ConnString)
                    ThisConn.Open()
                End If

                Dim cmd = New MySqlCommand(Query, conn)
                table.Load(cmd.ExecuteReader)

                If IsNothing(conn) Then
                    ThisConn.Close()
                    ThisConn.Dispose()
                End If
            Catch ex As Exception
            End Try

            Return table.Rows.Count > 1
        End Function

        Sub Save_Wettkampf_Multi(RowsToInsert As DataRow(),
                                 RowsToRemove As DataRow(),
                                 SrcTable As DataTable,
                                 WK_ID As String,
                                 Optional conn As MySqlConnection = Nothing)

            ' um Verknüpfung zu löschen, RowsToInsert als Nothing und nur RowsToRemove

            Dim ThisConn As MySqlConnection = conn
            Dim Idents = Get_Idents(SrcTable, WK_ID)
            Dim cmd As MySqlCommand

            Dim DeleteQuery = "delete from wettkampf_multi where Wettkampf = @wk;"
            Dim InsertQuery = "insert into wettkampf_multi (Wettkampf, Multi, Primaer) values (@wk, @multi, @prim) 
                                   on duplicate key update Multi = @multi, Primaer = @prim;"

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            If RowsToInsert.Count > 1 Then
                For Each row As DataRow In RowsToInsert
                    cmd = New MySqlCommand(InsertQuery, ThisConn)
                    With cmd.Parameters
                        .AddWithValue("@wk", row!Wettkampf)
                        .AddWithValue("@multi", Join(Idents.ToArray, ","))
                        .AddWithValue("@prim", row!Primaer)
                    End With
                    cmd.ExecuteNonQuery()
                Next
            End If
            If Not IsNothing(RowsToRemove) Then
                For Each row As DataRow In RowsToRemove
                    cmd = New MySqlCommand(DeleteQuery, ThisConn)
                    With cmd.Parameters
                        .AddWithValue("@wk", row!Wettkampf)
                    End With
                    cmd.ExecuteNonQuery()
                Next
            End If

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If

        End Sub

#Region "Wettkampf verknüpfen"
        Sub EqualizeMulti(Form As Form,
                          Source As DataTable,
                          WK_ID As String,
                          Optional conn As MySqlConnection = Nothing)

            'WK_ID == selektierter Wettkampf, an den die anderen angepasst werden

            Dim ThisConn As MySqlConnection = conn

            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            Dim Idents = Get_Idents(Source, WK_ID)
            Dim Tables = Get_Tables(Form, Idents, ThisConn)
            Dim NotCompared = Get_NotCompared()

            Idents.Remove(WK_ID)

            For Each Table In Tables
                Dim Query = "SELECT COLUMN_NAME 
                                 FROM INFORMATION_SCHEMA.COLUMNS 
                                 WHERE TABLE_SCHEMA = '" & User.Database & "' 
                                     AND TABLE_NAME = '" & LCase(Table.Key).Replace("-", "_") & "';"
                Dim cmd As New MySqlCommand(Query, ThisConn)
                Dim tmp As New DataTable
                tmp.Load(cmd.ExecuteReader)
                Dim ToEqualize = (From x In tmp Select x.Field(Of String)("COLUMN_NAME")).ToList
                For Each col In NotCompared(Table.Key)
                    ToEqualize.Remove(col)
                Next
                Dim rows = Table.Value.Select("Wettkampf = " & WK_ID)
                Dim Values As New List(Of String)
                Query = "UPDATE " & LCase(Table.Key).Replace("-", "_") & " SET "
                For Each col In ToEqualize
                    Values.Add(col & " = @" & col)
                Next
                Query += Join(Values.ToArray, ", ")
                Query += " WHERE Wettkampf = "
                For Each Ident In Idents
                    cmd = New MySqlCommand(Query + Ident & ";", ThisConn)
                    With cmd.Parameters
                        For Each col In ToEqualize
                            .AddWithValue("@" & col, rows(0)(col))
                        Next
                    End With
                    cmd.ExecuteNonQuery()
                Next
            Next

            If IsNothing(conn) Then
                ThisConn.Close()
                ThisConn.Dispose()
            End If
        End Sub
        Function ValidateEquality(Form As Form,
                                  WK_ID As String,
                                  Source As DataTable,
                                  Optional FormWK As frmWettkampf = Nothing) As KeyValuePair(Of Boolean, Dictionary(Of String, List(Of String)))

            ' WK_ID == der zu Multi hinzuzufügende Wettkampf

            Dim Idents = Get_Idents(Source, WK_ID)
            Dim Tables = Get_Tables(Form, Idents)
            Dim MisMatches As New Dictionary(Of String, List(Of String))
            Dim NoMatch As Boolean = False
            Dim NotCompared = Get_NotCompared()
            For Each Table In TableNames
                MisMatches(Table) = New List(Of String)
            Next
            Idents.Remove(WK_ID)

            For Each Table In Tables
                ' suche hinzuzufügenden Wettkampf
                Dim RowToAdd = Table.Value.Select("Wettkampf = " & WK_ID)

                If Not IsNothing(FormWK) Then
                    With FormWK
                        If RowToAdd.Count = 0 OrElse .WK_ID.Equals(WK_ID) AndAlso .mnuSpeichern.Enabled Then
#Region "Nicht gespeicherter Wettkampf"
                            Dim NewRow = Table.Value.NewRow ' das Array muss zur Tabelle passen 
                            '                                 --> bei Änderung an der Tabellenstruktur anpassen
                            Select Case Table.Key
                                Case "Wettkampf"
                                    NewRow.ItemArray = {WK_ID,
                                                        CType(.cboModus.SelectedItem, DataRowView)!idModus,
                                                        .dpkDatum.Value,
                                                        .dpkDatumBis.Value,
                                                        DBNull.Value,
                                                        DBNull.Value,
                                                        .cboOrt.Text,
                                                        .chkBigdisc.Checked,
                                                        .cboKR.Text,
                                                        .chkLiga.Checked,
                                                        .chkFlagge.Checked,
                                                        .nudMinAge.Value}
                                Case "Details"
                                    NewRow.ItemArray = {WK_ID,
                                                        .txtZK_m.Text,
                                                        .txtZK_w.Text,
                                                        .txtR_m.Text,
                                                        .txtS_m.Text,
                                                        .txtR_w.Text,
                                                        .txtS_w.Text,
                                                        .txtHantelstange.Text,
                                                        .nudSteigerung1.Value,
                                                        .nudSteigerung2.Value,
                                                        -1, -1, 'Anzahl AKs für Masters
                                                        .chkRegel20.Checked,
                                                        False, ' nurZK
                                                        .chkInternational.Checked,
                                                        .chkLosnummer.Checked,
                                                        .chkWeitererVersuch.Checked,
                                                        .chkVersucheSortieren.Checked}
                                Case "Zeitplan"
                            End Select
                            RowToAdd(0) = NewRow
#End Region
                        End If
                    End With
                End If
#Region "Validation"
                For Each Ix In Idents
                    Dim rows = Table.Value.Select("Wettkampf = " & Ix)
                    For i = 0 To rows(0).ItemArray.Length - 1
                        If Not NotCompared(Table.Key).Contains(Table.Value.Columns(i).ColumnName) Then
                            Dim typ = rows(0).ItemArray(i).GetType
                            Dim bez = Table.Value.Columns(i).ColumnName
                            Select Case typ.Name
                                Case "Int32"
                                    If Not rows(0).ItemArray(i).Equals(CType(RowToAdd(0).ItemArray(i), Integer)) Then
                                        If Not MisMatches(Table.Key).Contains(bez) Then MisMatches(Table.Key).Add(bez)
                                        NoMatch = True
                                    End If
                                Case "DateTime"
                                    If Not rows(0).ItemArray(i).Equals(CType(RowToAdd(0).ItemArray(i), Date)) Then
                                        If Not MisMatches(Table.Key).Contains(bez) Then MisMatches(Table.Key).Add(bez)
                                        NoMatch = True
                                    End If
                                Case "DBNull"
                                    If Not rows(0).ItemArray(i).Equals(RowToAdd(0).ItemArray(i)) Then
                                        If Not MisMatches(Table.Key).Contains(bez) Then MisMatches(Table.Key).Add(bez)
                                        NoMatch = True
                                    End If
                                Case "String"
                                    If Not rows(0).ItemArray(i).Equals(CType(RowToAdd(0).ItemArray(i), String)) Then
                                        If Not MisMatches(Table.Key).Contains(bez) Then MisMatches(Table.Key).Add(bez)
                                        NoMatch = True
                                    End If
                                Case "Boolean"
                                    If Not rows(0).ItemArray(i).Equals(CType(RowToAdd(0).ItemArray(i), Boolean)) Then
                                        If Not MisMatches(Table.Key).Contains(bez) Then MisMatches(Table.Key).Add(bez)
                                        NoMatch = True
                                    End If
                            End Select
                        End If
                    Next
                Next
            Next
#End Region
            Return New KeyValuePair(Of Boolean, Dictionary(Of String, List(Of String)))(Not NoMatch, MisMatches)
        End Function
        Private Function Get_Idents(Source As DataTable, WK_ID As String) As List(Of String)
            Return (From m In Source
                    Where m.Field(Of Integer)("Joined") > 0 OrElse m.Field(Of Integer)("Wettkampf") = CInt(WK_ID)
                    Select Key = m.Field(Of Integer)("Wettkampf").ToString).ToList
        End Function
        Private Function Get_Tables(Form As Form,
                                    Idents As List(Of String),
                                    Optional conn As MySqlConnection = Nothing) As Dictionary(Of String, DataTable)

            Dim ThisConn As MySqlConnection = conn
            If IsNothing(ThisConn) Then
                ThisConn = New MySqlConnection(User.ConnString)
                ThisConn.Open()
            End If

            Dim Tables As New Dictionary(Of String, DataTable)
            For Each Table In TableNames
                Tables(Table) = New DataTable
            Next

            Try
                Tables("Wettkampf") = Get_Wettkampf(Idents.ToArray, ThisConn)
                Tables("Wettkampf-Details") = Get_Wettkampf_Details(Idents.ToArray, ThisConn)
                'Tables("Zeitplan") = Get_Wettkampf_Zeitplan(Idents.ToArray, ThisConn)
                ConnError = False
            Catch ex As Exception
                MySQl_Error(Form, ex.Message, ex.StackTrace, True)
                Return Nothing
            End Try

            Return Tables
        End Function
        Private Function Get_NotCompared() As Dictionary(Of String, String())
            Dim Cols As New Dictionary(Of String, String())
            For Each Table In TableNames
                ' Felder, die nicht verglichen werden
                Cols(Table) = {"Wettkampf"}
                If Table.Equals("Wettkampf") Then Cols(Table) = {"Wettkampf", "Modus", "Veranstalter", "Kurz", "Platzierung", "GUID", "Bez1", "Bez2", "Bez3"}
            Next
            Return Cols
        End Function
#End Region
    End Class
End Module
