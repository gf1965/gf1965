﻿Imports Squirrel
Imports System.Threading.Tasks

Public Class frmUpdate
    Property Silent As Boolean = False

    Private Manager As UpdateManager
    Private Updates As UpdateInfo = Nothing
    Private Current As ReleaseEntry
    Private Releases As List(Of ReleaseEntry)
    Private ProgressDelegate As Action(Of Integer) = AddressOf Set_Progress

    Private Sub Me_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Status.Text = String.Empty
        Info.Text = String.Empty

        CheckForUpdates()
    End Sub

    Async Sub CheckForUpdates()
        Dim LogMsg As New List(Of String)

        Dim x = NativeMethods.INI_Read("Programm", "UpdateDir", App_IniFile)
        If x <> "?" Then UpdateDir = x.ToString

        Manager = New UpdateManager(UpdateDir)
        Releases = New List(Of ReleaseEntry)

        Try
            If Directory.Exists(UpdateDir) OrElse UpdateDir.StartsWith("http") Then
                Updates = Await Task.Run(Function() Manager.CheckForUpdate())
                Releases = Updates.ReleasesToApply
                If Releases.Count = 0 Then
                    Manager.Dispose()
                    Dim Response = Database_Update(Me)
                    ProgressBar1.Value = 0
                    If Response.Key = 1 Then
                        Timer1.Start()
                    Else
                        Info.Text = Response.Value
                    End If
                    SetButtons()
                Else
                    Current = Updates.CurrentlyInstalledVersion
                    If Not IsNothing(Current) Then
                        Info.Text = "Version installiert:    " + Current.Version.ToString + vbNewLine
                    End If
                    Dim Future = Updates.FutureReleaseEntry
                    If Not IsNothing(Future) Then
                        Info.Text += "Version verfügbar:  " + Future.Version.ToString + vbNewLine
                    End If
                    Info.Text += vbNewLine + "Wann soll das Update installiert werden?"

                    LogMsg.Add("CurrentVersion: " & If(IsNothing(Current), "", Current.Version.ToString))
                    LogMsg.Add("FutureVersion: " & Future.Version.ToString)
                    LogMsg.Add("PackageName: " & Future.PackageName)
                    LogMsg.Add("Filesize: " & Strings.Format(Future.Filesize / 1048576, "#,##0.00 MB"))
                    LogMsg.Add("Filename: " & Future.Filename)
                    LogUpdate(Join(LogMsg.ToArray, vbNewLine))
                End If
            Else
                Info.Text = vbNewLine & vbNewLine & "Keine Update-Informationen gefunden."
                SetButtons()
            End If
        Catch ex As Exception
            LogUpdate("Error at Update: " & ex.Message & vbNewLine & "StackTrace " &
                      If(InStr(ex.StackTrace, "Zeile") > 0, Mid(ex.StackTrace, InStr(ex.StackTrace, "Zeile")), ex.StackTrace))
            Info.Text = "Fehler beim Update." & vbNewLine & ex.Message
            SetButtons()
        End Try
    End Sub

    Private Sub SetButtons(Optional BtnText As String = "Schließen")
        Button1.Visible = False
        Button2.Text = BtnText
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Button1.Enabled = False
        Button2.Text = "Abbrechen"
        Button2.DialogResult = DialogResult.Cancel
        If Button1.Text.Equals("Wiederholen") Then
            Status.Text = String.Empty
            Set_Progress(0)
            CheckForUpdates()
        Else
            PerformUpdate()
        End If
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Button2.Text.Equals("Abschließen") Then
            UpdateManager.RestartApp()
        Else
            Manager.Dispose()
            Close()
        End If
    End Sub

    Async Sub PerformUpdate()
        Cursor = Cursors.WaitCursor

        'Dim AppRoot = Get_AppRoot()
        Try
            If File.Exists(App_IniFile) Then
                FileIO.FileSystem.CopyFile(App_IniFile, Path.Combine(Manager.RootAppDirectory, Application.ProductName & ".ini"), True)
                LogUpdate("INI saved to TempFolder: " & File.Exists(Path.Combine(Manager.RootAppDirectory, Application.ProductName & ".ini")))
            End If
        Catch ex As Exception
            LogUpdate("Error at save INI to TempFolder: " & ex.Message & vbNewLine & "Exception: " & ex.InnerException.StackTrace)
        End Try

        Try
            LogUpdate("Releases.Count: " & Releases.Count)

            'If Releases.Count = 1 AndAlso Releases(0).IsDelta Then
            Status.Text = "Updates installieren..."

            Dim Release = Await Task.Run(Function() Manager.UpdateApp(ProgressDelegate))

            LogUpdate("New Version: " & Release.Version.ToString)
            'End If

            'Status.Text = "Updates herunterladen..."
            'Await Task.Run(Function() Manager.DownloadReleases(Releases, ProgressDelegate))

            'Status.Text = "Updates installieren..."
            'Dim NewVersion = Await Task.Run(Function() Manager.ApplyReleases(Updates, ProgressDelegate).Result)

            'Status.Text = String.Empty
            'LogUpdate("New Version: " & NewVersion)
            'msg.Text = "Neue Version ist " & Split(NewVersion, "-")(1) & vbNewLine & vbNewLine '&

            Info.Text = vbNewLine & vbNewLine & "Neue Version ist " & Release.Version.ToString
            Status.Text = String.Empty
            Set_Progress(0)
            SetButtons("Abschließen")

            Try
                If File.Exists(Path.Combine(Manager.RootAppDirectory, Application.ProductName & ".ini")) Then
                    FileIO.FileSystem.CopyFile(Path.Combine(Manager.RootAppDirectory, Application.ProductName & ".ini"), Path.Combine(Manager.RootAppDirectory, "app-" & Release.Version.ToString, Application.ProductName & ".ini"), True)
                    LogUpdate("INI saved to AppFolder: " & File.Exists(Path.Combine(Manager.RootAppDirectory, "app-" & Release.Version.ToString, Application.ProductName & ".ini")))
                    'File.Delete(Path.Combine(Manager.RootAppDirectory, Application.ProductName & ".ini"))
                End If
            Catch ex As Exception
                LogUpdate("Error at save INI to AppFolder: " & ex.Message & vbNewLine & "Exception: " & ex.InnerException.StackTrace)
            End Try

        Catch ex As Exception
            LogUpdate("Error at Update: " & ex.Message & vbNewLine & "Exception: " & ex.InnerException.StackTrace)
            With Button1
                .Enabled = True
                .Text = "Wiederholen"
            End With
        Finally
            Manager.Dispose()
        End Try
        Cursor = Cursors.Default
    End Sub

    Private Delegate Sub DelegateInteger(Value As Integer)
    Private Sub Set_Progress(Value As Integer)
        Try
            If InvokeRequired Then
                Dim d As New DelegateInteger(AddressOf Set_Progress)
                Invoke(d, New Object() {Value})
            Else
                ProgressBar1.Value = Value
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Stop()
        Info.Text = vbNewLine & vbNewLine & "Das Programm ist auf dem neuesten Stand."
        Status.Text = String.Empty
    End Sub



End Class