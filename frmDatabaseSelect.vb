﻿Imports System.ComponentModel

Public Class frmDatabaseSelect
    Property Databases As String()

    Private CancelForm As Boolean = True

    Private Sub frmDatabaseSelect_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing AndAlso CancelForm Then Databases = {}
    End Sub

    Private Sub frmDatabaseSelect_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        With CheckedListBox1
            .CheckOnClick = True
            .DataSource = Databases.Clone
            .SetItemCheckState(GetIndexOfCurrentDatabase(), CheckState.Checked)
            .SetSelected(GetIndexOfCurrentDatabase(), True)
            '.SetItemCheckState(0, CheckState.Indeterminate)
        End With
        SetEnabled()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ReDim Databases(CheckedListBox1.CheckedItems.Count - 1)
        CheckedListBox1.CheckedItems.CopyTo(Databases, 0)
        CancelForm = False
        Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Databases = {User.Database}
        CancelForm = False
        Close()
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        SetChecked(CheckBox1.Checked)
        SetEnabled()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        SetChecked(False)
        'CheckedListBox1.SetItemChecked(GetIndexOfCurrentDatabase(), True)
        SetEnabled()
    End Sub

    Delegate Sub ProcessItemCheck(ByRef ListBoxObject As CheckedListBox)
    Private Sub CheckedListBox1_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles CheckedListBox1.ItemCheck
        'If e.Index = GetIndexOfCurrentDatabase() Then Return
        Dim Objects As Object() = {CheckedListBox1}
        BeginInvoke(New ProcessItemCheck(AddressOf ProcessItemCheckSub), Objects)
    End Sub
    Private Sub ProcessItemCheckSub(ByRef ListBoxObject As CheckedListBox)
        If ListBoxObject.Focused Then SetEnabled()
    End Sub

    Private Sub SetEnabled()
        Button1.Enabled = CheckedListBox1.CheckedItems.Count > 0
        Button2.Enabled = CheckedListBox1.CheckedItems.Count > 0
    End Sub

    Private Sub SetChecked(Value As Boolean)
        For i = 0 To CheckedListBox1.Items.Count - 1
            CheckedListBox1.SetItemChecked(i, Value)
        Next
    End Sub

    Private Function GetIndexOfCurrentDatabase() As Integer
        Return Databases.ToList.FindIndex(Function(value As String)
                                              Return value.Equals(User.Database)
                                          End Function)
    End Function

End Class