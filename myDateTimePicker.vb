﻿'VB CODE
' *****************************************
' ** Author: Vincenzo Rossi **
' ** Year : 2008 **
' ** Mail : redmaster@tiscali.it **
' ** **
' ** Released under **
' ** The Code Project Open License **
' *****************************************

'Imports System
'Imports System.Collections.Generic
'Imports System.Text
'Imports System.Windows.Forms
'Imports System.Drawing
Imports System.ComponentModel
Imports System.Windows.Forms.VisualStyles

Namespace DateTimePickerWithBackColor
    '' <summary>
    '' A derivation of DateTimePicker allowing to change background color
    ''
    Class BCDateTimePicker
        Inherits DateTimePicker

        Friend WithEvents DateTimePicker1 As DateTimePicker
        Private _backDisabledColor As Color

        Public Sub New()
            MyBase.New()
            SetStyle(ControlStyles.UserPaint, True)
            _backDisabledColor = Color.FromKnownColor(KnownColor.Control)
        End Sub

        '' <summary>
        '' Gets or sets the background color of the control
        ''
        <Browsable(True)>
        Public Overrides Property BackColor() As Color
            Get
                Return MyBase.BackColor
            End Get
            Set
                MyBase.BackColor = Value
            End Set
        End Property

        '' <summary>
        '' Gets or sets the background color of the control when disabled
        ''
        <Category("Appearance"), Description("The background color of the component when disabled")>
        <Browsable(True)>
        Public Property BackDisabledColor() As Color
            Get
                Return _backDisabledColor
            End Get
            Set
                _backDisabledColor = Value
            End Set
        End Property


        Protected Overrides Sub OnPaint(e As System.Windows.Forms.PaintEventArgs)
            Dim g As Graphics = Me.CreateGraphics()
            'Graphics g = e.Graphics;

            'The dropDownRectangle defines position and size of dropdownbutton block,
            'the width is fixed to 17 and height to 16. The dropdownbutton is aligned to right
            Dim dropDownRectangle As New Rectangle(ClientRectangle.Width - 17, 0, 17, 16)
            Dim bkgBrush As Brush
            Dim visualState As ComboBoxState

            'When the control is enabled the brush is set to Backcolor,
            'otherwise to color stored in _backDisabledColor
            If Enabled Then
                bkgBrush = New SolidBrush(BackColor)
                visualState = ComboBoxState.Normal
            Else
                bkgBrush = New SolidBrush(Me._backDisabledColor)
                visualState = ComboBoxState.Disabled
            End If

            ' Painting...in action

            'Filling the background
            g.FillRectangle(bkgBrush, 0, 0, ClientRectangle.Width, ClientRectangle.Height)

            'Drawing the datetime text
            g.DrawString(Text, Font, Brushes.Black, 0, 2)

            'Drawing the dropdownbutton using ComboBoxRenderer
            ComboBoxRenderer.DrawDropDownButton(g, dropDownRectangle, visualState)

            g.Dispose()
            bkgBrush.Dispose()
        End Sub

        Private Sub InitializeComponent()
            Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
            Me.SuspendLayout()
            '
            'DateTimePicker1
            '
            Me.DateTimePicker1.Location = New System.Drawing.Point(0, 0)
            Me.DateTimePicker1.Name = "DateTimePicker1"
            Me.DateTimePicker1.Size = New System.Drawing.Size(200, 20)
            Me.DateTimePicker1.TabIndex = 0
            Me.ResumeLayout(False)

        End Sub
    End Class
End Namespace
