﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmGruppen_JG
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGruppen_JG))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.picGruppe1 = New System.Windows.Forms.PictureBox()
        Me.cbo1 = New System.Windows.Forms.ComboBox()
        Me.lblG1 = New System.Windows.Forms.Label()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.pnlGruppe1 = New System.Windows.Forms.Panel()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.timeWb1 = New System.Windows.Forms.DateTimePicker()
        Me.dateW1 = New System.Windows.Forms.DateTimePicker()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.timeWe1 = New System.Windows.Forms.DateTimePicker()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.dateA1 = New System.Windows.Forms.DateTimePicker()
        Me.pnl1 = New System.Windows.Forms.Panel()
        Me.cboB1 = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.timeA1 = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.timeR1 = New System.Windows.Forms.DateTimePicker()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.timeS1 = New System.Windows.Forms.DateTimePicker()
        Me.chkBH1 = New System.Windows.Forms.CheckBox()
        Me.dgvGK1 = New System.Windows.Forms.DataGridView()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.picGruppe2 = New System.Windows.Forms.PictureBox()
        Me.lblG2 = New System.Windows.Forms.Label()
        Me.pnlGruppe2 = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.timeWb2 = New System.Windows.Forms.DateTimePicker()
        Me.dateW2 = New System.Windows.Forms.DateTimePicker()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.timeWe2 = New System.Windows.Forms.DateTimePicker()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.dateA2 = New System.Windows.Forms.DateTimePicker()
        Me.pnl2 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.cboB2 = New System.Windows.Forms.ComboBox()
        Me.timeA2 = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.timeR2 = New System.Windows.Forms.DateTimePicker()
        Me.date2 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.timeS2 = New System.Windows.Forms.DateTimePicker()
        Me.cbo2 = New System.Windows.Forms.ComboBox()
        Me.lbl2 = New System.Windows.Forms.Label()
        Me.dgvGK2 = New System.Windows.Forms.DataGridView()
        Me.chkBH2 = New System.Windows.Forms.CheckBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.picGruppe3 = New System.Windows.Forms.PictureBox()
        Me.lblG3 = New System.Windows.Forms.Label()
        Me.pnlGruppe3 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.timeWb3 = New System.Windows.Forms.DateTimePicker()
        Me.dateW3 = New System.Windows.Forms.DateTimePicker()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.timeWe3 = New System.Windows.Forms.DateTimePicker()
        Me.pnl3 = New System.Windows.Forms.Panel()
        Me.cboB3 = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.dateA3 = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.timeA3 = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.timeR3 = New System.Windows.Forms.DateTimePicker()
        Me.date3 = New System.Windows.Forms.DateTimePicker()
        Me.timeS3 = New System.Windows.Forms.DateTimePicker()
        Me.cbo3 = New System.Windows.Forms.ComboBox()
        Me.lbl3 = New System.Windows.Forms.Label()
        Me.dgvGK3 = New System.Windows.Forms.DataGridView()
        Me.chkBH3 = New System.Windows.Forms.CheckBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.picGruppe4 = New System.Windows.Forms.PictureBox()
        Me.lblG4 = New System.Windows.Forms.Label()
        Me.pnlGruppe4 = New System.Windows.Forms.Panel()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.timeWb4 = New System.Windows.Forms.DateTimePicker()
        Me.dateW4 = New System.Windows.Forms.DateTimePicker()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.timeWe4 = New System.Windows.Forms.DateTimePicker()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.dateA4 = New System.Windows.Forms.DateTimePicker()
        Me.pnl4 = New System.Windows.Forms.Panel()
        Me.cboB4 = New System.Windows.Forms.ComboBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.timeA4 = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.timeR4 = New System.Windows.Forms.DateTimePicker()
        Me.date4 = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.timeS4 = New System.Windows.Forms.DateTimePicker()
        Me.cbo4 = New System.Windows.Forms.ComboBox()
        Me.lbl4 = New System.Windows.Forms.Label()
        Me.dgvGK4 = New System.Windows.Forms.DataGridView()
        Me.chkBH4 = New System.Windows.Forms.CheckBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.picGruppe5 = New System.Windows.Forms.PictureBox()
        Me.lblG5 = New System.Windows.Forms.Label()
        Me.pnlGruppe5 = New System.Windows.Forms.Panel()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.timeWb5 = New System.Windows.Forms.DateTimePicker()
        Me.dateW5 = New System.Windows.Forms.DateTimePicker()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.timeWe5 = New System.Windows.Forms.DateTimePicker()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.dateA5 = New System.Windows.Forms.DateTimePicker()
        Me.pnl5 = New System.Windows.Forms.Panel()
        Me.cboB5 = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.timeA5 = New System.Windows.Forms.DateTimePicker()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.timeR5 = New System.Windows.Forms.DateTimePicker()
        Me.date5 = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.timeS5 = New System.Windows.Forms.DateTimePicker()
        Me.cbo5 = New System.Windows.Forms.ComboBox()
        Me.lbl5 = New System.Windows.Forms.Label()
        Me.dgvGK5 = New System.Windows.Forms.DataGridView()
        Me.chkBH5 = New System.Windows.Forms.CheckBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDrucken = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuGruppen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuGruppenLöschen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ZugewieseneGruppenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuZusammenhalten = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAufteilen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuCutGroups = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLayout = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuMeldungen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKonflikte = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuJGListeOffen = New System.Windows.Forms.ToolStripMenuItem()
        Me.bgwGK = New System.ComponentModel.BackgroundWorker()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.cboGruppenSaved = New System.Windows.Forms.ComboBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.cboGeschlecht = New System.Windows.Forms.ComboBox()
        Me.pnlForm = New System.Windows.Forms.TableLayoutPanel()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.lstJahrgang = New System.Windows.Forms.ListBox()
        Me.chkWiegenGleich = New System.Windows.Forms.CheckBox()
        Me.cboJahrgang = New System.Windows.Forms.ComboBox()
        Me.Panel1.SuspendLayout()
        CType(Me.picGruppe1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlGruppe1.SuspendLayout()
        Me.pnl1.SuspendLayout()
        CType(Me.dgvGK1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.picGruppe2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlGruppe2.SuspendLayout()
        Me.pnl2.SuspendLayout()
        CType(Me.dgvGK2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.picGruppe3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlGruppe3.SuspendLayout()
        Me.pnl3.SuspendLayout()
        CType(Me.dgvGK3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.picGruppe4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlGruppe4.SuspendLayout()
        Me.pnl4.SuspendLayout()
        CType(Me.dgvGK4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.picGruppe5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlGruppe5.SuspendLayout()
        Me.pnl5.SuspendLayout()
        CType(Me.dgvGK5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.pnlForm.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.picGruppe1)
        Me.Panel1.Controls.Add(Me.cbo1)
        Me.Panel1.Controls.Add(Me.lblG1)
        Me.Panel1.Controls.Add(Me.lbl1)
        Me.Panel1.Controls.Add(Me.pnlGruppe1)
        Me.Panel1.Controls.Add(Me.chkBH1)
        Me.Panel1.Controls.Add(Me.dgvGK1)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(232, 535)
        Me.Panel1.TabIndex = 1
        Me.Panel1.TabStop = True
        '
        'picGruppe1
        '
        Me.picGruppe1.Image = CType(resources.GetObject("picGruppe1.Image"), System.Drawing.Image)
        Me.picGruppe1.Location = New System.Drawing.Point(95, 34)
        Me.picGruppe1.Name = "picGruppe1"
        Me.picGruppe1.Size = New System.Drawing.Size(24, 24)
        Me.picGruppe1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picGruppe1.TabIndex = 7
        Me.picGruppe1.TabStop = False
        Me.picGruppe1.Visible = False
        '
        'cbo1
        '
        Me.cbo1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo1.DropDownWidth = 35
        Me.cbo1.FormattingEnabled = True
        Me.cbo1.Location = New System.Drawing.Point(50, 34)
        Me.cbo1.Name = "cbo1"
        Me.cbo1.Size = New System.Drawing.Size(41, 21)
        Me.cbo1.Sorted = True
        Me.cbo1.TabIndex = 2
        '
        'lblG1
        '
        Me.lblG1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblG1.BackColor = System.Drawing.Color.SteelBlue
        Me.lblG1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblG1.ForeColor = System.Drawing.Color.White
        Me.lblG1.Location = New System.Drawing.Point(0, 0)
        Me.lblG1.Name = "lblG1"
        Me.lblG1.Size = New System.Drawing.Size(228, 29)
        Me.lblG1.TabIndex = 0
        Me.lblG1.Text = "Feder"
        Me.lblG1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblG1.UseCompatibleTextRendering = True
        '
        'lbl1
        '
        Me.lbl1.AutoSize = True
        Me.lbl1.BackColor = System.Drawing.Color.Transparent
        Me.lbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl1.Location = New System.Drawing.Point(5, 36)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(42, 13)
        Me.lbl1.TabIndex = 1
        Me.lbl1.Text = "Gruppe"
        Me.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnlGruppe1
        '
        Me.pnlGruppe1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlGruppe1.Controls.Add(Me.Label34)
        Me.pnlGruppe1.Controls.Add(Me.Label35)
        Me.pnlGruppe1.Controls.Add(Me.timeWb1)
        Me.pnlGruppe1.Controls.Add(Me.dateW1)
        Me.pnlGruppe1.Controls.Add(Me.Label36)
        Me.pnlGruppe1.Controls.Add(Me.timeWe1)
        Me.pnlGruppe1.Controls.Add(Me.Label27)
        Me.pnlGruppe1.Controls.Add(Me.dateA1)
        Me.pnlGruppe1.Controls.Add(Me.pnl1)
        Me.pnlGruppe1.Controls.Add(Me.timeA1)
        Me.pnlGruppe1.Controls.Add(Me.Label7)
        Me.pnlGruppe1.Controls.Add(Me.Label6)
        Me.pnlGruppe1.Controls.Add(Me.timeR1)
        Me.pnlGruppe1.Controls.Add(Me.date1)
        Me.pnlGruppe1.Controls.Add(Me.Label1)
        Me.pnlGruppe1.Controls.Add(Me.timeS1)
        Me.pnlGruppe1.Location = New System.Drawing.Point(0, 387)
        Me.pnlGruppe1.Name = "pnlGruppe1"
        Me.pnlGruppe1.Size = New System.Drawing.Size(228, 144)
        Me.pnlGruppe1.TabIndex = 5
        Me.pnlGruppe1.TabStop = True
        '
        'Label34
        '
        Me.Label34.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(170, 6)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(32, 13)
        Me.Label34.TabIndex = 14
        Me.Label34.Text = "Ende"
        '
        'Label35
        '
        Me.Label35.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(103, 6)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(40, 13)
        Me.Label35.TabIndex = 12
        Me.Label35.Text = "Beginn"
        '
        'timeWb1
        '
        Me.timeWb1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeWb1.Checked = False
        Me.timeWb1.CustomFormat = "HH:mm"
        Me.timeWb1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeWb1.Location = New System.Drawing.Point(104, 22)
        Me.timeWb1.Name = "timeWb1"
        Me.timeWb1.ShowUpDown = True
        Me.timeWb1.Size = New System.Drawing.Size(51, 20)
        Me.timeWb1.TabIndex = 13
        '
        'dateW1
        '
        Me.dateW1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dateW1.Checked = False
        Me.dateW1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateW1.Location = New System.Drawing.Point(7, 22)
        Me.dateW1.Name = "dateW1"
        Me.dateW1.ShowUpDown = True
        Me.dateW1.Size = New System.Drawing.Size(80, 20)
        Me.dateW1.TabIndex = 11
        '
        'Label36
        '
        Me.Label36.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(6, 6)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(44, 13)
        Me.Label36.TabIndex = 10
        Me.Label36.Text = "Wiegen"
        '
        'timeWe1
        '
        Me.timeWe1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeWe1.Checked = False
        Me.timeWe1.CustomFormat = "HH:mm"
        Me.timeWe1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeWe1.Location = New System.Drawing.Point(171, 22)
        Me.timeWe1.Name = "timeWe1"
        Me.timeWe1.ShowUpDown = True
        Me.timeWe1.Size = New System.Drawing.Size(51, 20)
        Me.timeWe1.TabIndex = 15
        '
        'Label27
        '
        Me.Label27.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label27.AutoSize = True
        Me.Label27.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label27.Location = New System.Drawing.Point(6, 98)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(42, 13)
        Me.Label27.TabIndex = 22
        Me.Label27.Text = "Athletik"
        '
        'dateA1
        '
        Me.dateA1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dateA1.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.dateA1.Checked = False
        Me.dateA1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateA1.Location = New System.Drawing.Point(7, 114)
        Me.dateA1.Name = "dateA1"
        Me.dateA1.ShowUpDown = True
        Me.dateA1.Size = New System.Drawing.Size(80, 20)
        Me.dateA1.TabIndex = 23
        '
        'pnl1
        '
        Me.pnl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnl1.BackColor = System.Drawing.Color.SteelBlue
        Me.pnl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl1.Controls.Add(Me.cboB1)
        Me.pnl1.Controls.Add(Me.Label21)
        Me.pnl1.Location = New System.Drawing.Point(171, 95)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(61, 48)
        Me.pnl1.TabIndex = 30
        '
        'cboB1
        '
        Me.cboB1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboB1.FormattingEnabled = True
        Me.cboB1.Items.AddRange(New Object() {"1", "2"})
        Me.cboB1.Location = New System.Drawing.Point(14, 19)
        Me.cboB1.Name = "cboB1"
        Me.cboB1.Size = New System.Drawing.Size(30, 21)
        Me.cboB1.TabIndex = 32
        '
        'Label21
        '
        Me.Label21.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(12, 4)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(34, 13)
        Me.Label21.TabIndex = 31
        Me.Label21.Text = "Bohle"
        '
        'timeA1
        '
        Me.timeA1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeA1.Checked = False
        Me.timeA1.CustomFormat = "HH:mm"
        Me.timeA1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeA1.Location = New System.Drawing.Point(104, 114)
        Me.timeA1.Name = "timeA1"
        Me.timeA1.ShowUpDown = True
        Me.timeA1.Size = New System.Drawing.Size(51, 20)
        Me.timeA1.TabIndex = 24
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(170, 52)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 13)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Stoßen"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(103, 52)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 13)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Reißen"
        '
        'timeR1
        '
        Me.timeR1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeR1.Checked = False
        Me.timeR1.CustomFormat = "HH:mm"
        Me.timeR1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeR1.Location = New System.Drawing.Point(104, 68)
        Me.timeR1.Name = "timeR1"
        Me.timeR1.ShowUpDown = True
        Me.timeR1.Size = New System.Drawing.Size(51, 20)
        Me.timeR1.TabIndex = 19
        '
        'date1
        '
        Me.date1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.date1.Checked = False
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(7, 68)
        Me.date1.Name = "date1"
        Me.date1.ShowUpDown = True
        Me.date1.Size = New System.Drawing.Size(80, 20)
        Me.date1.TabIndex = 17
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 13)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Gewichtheben"
        '
        'timeS1
        '
        Me.timeS1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeS1.Checked = False
        Me.timeS1.CustomFormat = "HH:mm"
        Me.timeS1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeS1.Location = New System.Drawing.Point(171, 68)
        Me.timeS1.Name = "timeS1"
        Me.timeS1.ShowUpDown = True
        Me.timeS1.Size = New System.Drawing.Size(51, 20)
        Me.timeS1.TabIndex = 21
        '
        'chkBH1
        '
        Me.chkBH1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkBH1.AutoSize = True
        Me.chkBH1.Location = New System.Drawing.Point(136, 36)
        Me.chkBH1.Name = "chkBH1"
        Me.chkBH1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkBH1.Size = New System.Drawing.Size(83, 17)
        Me.chkBH1.TabIndex = 3
        Me.chkBH1.Text = "Blockheben"
        Me.chkBH1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkBH1.UseVisualStyleBackColor = True
        '
        'dgvGK1
        '
        Me.dgvGK1.AllowDrop = True
        Me.dgvGK1.AllowUserToAddRows = False
        Me.dgvGK1.AllowUserToDeleteRows = False
        Me.dgvGK1.AllowUserToOrderColumns = True
        Me.dgvGK1.AllowUserToResizeRows = False
        Me.dgvGK1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvGK1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvGK1.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvGK1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvGK1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGK1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvGK1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGK1.Location = New System.Drawing.Point(0, 60)
        Me.dgvGK1.Name = "dgvGK1"
        Me.dgvGK1.ReadOnly = True
        Me.dgvGK1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvGK1.RowHeadersVisible = False
        Me.dgvGK1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGK1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGK1.ShowEditingIcon = False
        Me.dgvGK1.Size = New System.Drawing.Size(228, 326)
        Me.dgvGK1.TabIndex = 4
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel2.Controls.Add(Me.picGruppe2)
        Me.Panel2.Controls.Add(Me.lblG2)
        Me.Panel2.Controls.Add(Me.pnlGruppe2)
        Me.Panel2.Controls.Add(Me.cbo2)
        Me.Panel2.Controls.Add(Me.lbl2)
        Me.Panel2.Controls.Add(Me.dgvGK2)
        Me.Panel2.Controls.Add(Me.chkBH2)
        Me.Panel2.Location = New System.Drawing.Point(241, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(232, 535)
        Me.Panel2.TabIndex = 2
        Me.Panel2.TabStop = True
        '
        'picGruppe2
        '
        Me.picGruppe2.Image = CType(resources.GetObject("picGruppe2.Image"), System.Drawing.Image)
        Me.picGruppe2.Location = New System.Drawing.Point(95, 34)
        Me.picGruppe2.Name = "picGruppe2"
        Me.picGruppe2.Size = New System.Drawing.Size(24, 24)
        Me.picGruppe2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picGruppe2.TabIndex = 6
        Me.picGruppe2.TabStop = False
        Me.picGruppe2.Visible = False
        '
        'lblG2
        '
        Me.lblG2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblG2.BackColor = System.Drawing.Color.SteelBlue
        Me.lblG2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblG2.ForeColor = System.Drawing.Color.White
        Me.lblG2.Location = New System.Drawing.Point(0, 0)
        Me.lblG2.Name = "lblG2"
        Me.lblG2.Size = New System.Drawing.Size(228, 29)
        Me.lblG2.TabIndex = 0
        Me.lblG2.Text = "Leicht"
        Me.lblG2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblG2.UseCompatibleTextRendering = True
        '
        'pnlGruppe2
        '
        Me.pnlGruppe2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlGruppe2.Controls.Add(Me.Label16)
        Me.pnlGruppe2.Controls.Add(Me.Label17)
        Me.pnlGruppe2.Controls.Add(Me.timeWb2)
        Me.pnlGruppe2.Controls.Add(Me.dateW2)
        Me.pnlGruppe2.Controls.Add(Me.Label18)
        Me.pnlGruppe2.Controls.Add(Me.timeWe2)
        Me.pnlGruppe2.Controls.Add(Me.Label28)
        Me.pnlGruppe2.Controls.Add(Me.dateA2)
        Me.pnlGruppe2.Controls.Add(Me.pnl2)
        Me.pnlGruppe2.Controls.Add(Me.timeA2)
        Me.pnlGruppe2.Controls.Add(Me.Label8)
        Me.pnlGruppe2.Controls.Add(Me.Label9)
        Me.pnlGruppe2.Controls.Add(Me.timeR2)
        Me.pnlGruppe2.Controls.Add(Me.date2)
        Me.pnlGruppe2.Controls.Add(Me.Label2)
        Me.pnlGruppe2.Controls.Add(Me.timeS2)
        Me.pnlGruppe2.Location = New System.Drawing.Point(0, 387)
        Me.pnlGruppe2.Name = "pnlGruppe2"
        Me.pnlGruppe2.Size = New System.Drawing.Size(228, 144)
        Me.pnlGruppe2.TabIndex = 5
        Me.pnlGruppe2.TabStop = True
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(170, 6)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(32, 13)
        Me.Label16.TabIndex = 14
        Me.Label16.Text = "Ende"
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(103, 6)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(40, 13)
        Me.Label17.TabIndex = 12
        Me.Label17.Text = "Beginn"
        '
        'timeWb2
        '
        Me.timeWb2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeWb2.Checked = False
        Me.timeWb2.CustomFormat = "HH:mm"
        Me.timeWb2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeWb2.Location = New System.Drawing.Point(104, 22)
        Me.timeWb2.Name = "timeWb2"
        Me.timeWb2.ShowUpDown = True
        Me.timeWb2.Size = New System.Drawing.Size(51, 20)
        Me.timeWb2.TabIndex = 13
        '
        'dateW2
        '
        Me.dateW2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dateW2.Checked = False
        Me.dateW2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateW2.Location = New System.Drawing.Point(7, 22)
        Me.dateW2.Name = "dateW2"
        Me.dateW2.ShowUpDown = True
        Me.dateW2.Size = New System.Drawing.Size(80, 20)
        Me.dateW2.TabIndex = 11
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(6, 6)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(44, 13)
        Me.Label18.TabIndex = 10
        Me.Label18.Text = "Wiegen"
        '
        'timeWe2
        '
        Me.timeWe2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeWe2.Checked = False
        Me.timeWe2.CustomFormat = "HH:mm"
        Me.timeWe2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeWe2.Location = New System.Drawing.Point(171, 22)
        Me.timeWe2.Name = "timeWe2"
        Me.timeWe2.ShowUpDown = True
        Me.timeWe2.Size = New System.Drawing.Size(51, 20)
        Me.timeWe2.TabIndex = 15
        '
        'Label28
        '
        Me.Label28.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        Me.Label28.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label28.Location = New System.Drawing.Point(6, 98)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(42, 13)
        Me.Label28.TabIndex = 22
        Me.Label28.Text = "Athletik"
        '
        'dateA2
        '
        Me.dateA2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dateA2.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.dateA2.Checked = False
        Me.dateA2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateA2.Location = New System.Drawing.Point(7, 114)
        Me.dateA2.Name = "dateA2"
        Me.dateA2.ShowUpDown = True
        Me.dateA2.Size = New System.Drawing.Size(80, 20)
        Me.dateA2.TabIndex = 23
        '
        'pnl2
        '
        Me.pnl2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnl2.BackColor = System.Drawing.Color.SteelBlue
        Me.pnl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl2.Controls.Add(Me.Label22)
        Me.pnl2.Controls.Add(Me.cboB2)
        Me.pnl2.Location = New System.Drawing.Point(171, 95)
        Me.pnl2.Name = "pnl2"
        Me.pnl2.Size = New System.Drawing.Size(61, 48)
        Me.pnl2.TabIndex = 30
        '
        'Label22
        '
        Me.Label22.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label22.AutoSize = True
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(12, 4)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(34, 13)
        Me.Label22.TabIndex = 31
        Me.Label22.Text = "Bohle"
        '
        'cboB2
        '
        Me.cboB2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboB2.FormattingEnabled = True
        Me.cboB2.Items.AddRange(New Object() {"1", "2"})
        Me.cboB2.Location = New System.Drawing.Point(14, 19)
        Me.cboB2.Name = "cboB2"
        Me.cboB2.Size = New System.Drawing.Size(30, 21)
        Me.cboB2.TabIndex = 32
        '
        'timeA2
        '
        Me.timeA2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeA2.Checked = False
        Me.timeA2.CustomFormat = "HH:mm"
        Me.timeA2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeA2.Location = New System.Drawing.Point(104, 114)
        Me.timeA2.Name = "timeA2"
        Me.timeA2.ShowUpDown = True
        Me.timeA2.Size = New System.Drawing.Size(51, 20)
        Me.timeA2.TabIndex = 23
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(170, 52)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Stoßen"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(103, 52)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(41, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Reißen"
        '
        'timeR2
        '
        Me.timeR2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeR2.Checked = False
        Me.timeR2.CustomFormat = "HH:mm"
        Me.timeR2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeR2.Location = New System.Drawing.Point(104, 68)
        Me.timeR2.Name = "timeR2"
        Me.timeR2.ShowUpDown = True
        Me.timeR2.Size = New System.Drawing.Size(51, 20)
        Me.timeR2.TabIndex = 19
        '
        'date2
        '
        Me.date2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.date2.Checked = False
        Me.date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date2.Location = New System.Drawing.Point(7, 68)
        Me.date2.Name = "date2"
        Me.date2.ShowUpDown = True
        Me.date2.Size = New System.Drawing.Size(80, 20)
        Me.date2.TabIndex = 17
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Gewichtheben"
        '
        'timeS2
        '
        Me.timeS2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeS2.Checked = False
        Me.timeS2.CustomFormat = "HH:mm"
        Me.timeS2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeS2.Location = New System.Drawing.Point(171, 68)
        Me.timeS2.Name = "timeS2"
        Me.timeS2.ShowUpDown = True
        Me.timeS2.Size = New System.Drawing.Size(51, 20)
        Me.timeS2.TabIndex = 21
        '
        'cbo2
        '
        Me.cbo2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo2.DropDownWidth = 35
        Me.cbo2.FormattingEnabled = True
        Me.cbo2.Location = New System.Drawing.Point(50, 34)
        Me.cbo2.Name = "cbo2"
        Me.cbo2.Size = New System.Drawing.Size(41, 21)
        Me.cbo2.Sorted = True
        Me.cbo2.TabIndex = 2
        '
        'lbl2
        '
        Me.lbl2.AutoSize = True
        Me.lbl2.BackColor = System.Drawing.Color.Transparent
        Me.lbl2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl2.Location = New System.Drawing.Point(5, 36)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(42, 13)
        Me.lbl2.TabIndex = 1
        Me.lbl2.Text = "Gruppe"
        Me.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvGK2
        '
        Me.dgvGK2.AllowDrop = True
        Me.dgvGK2.AllowUserToAddRows = False
        Me.dgvGK2.AllowUserToDeleteRows = False
        Me.dgvGK2.AllowUserToOrderColumns = True
        Me.dgvGK2.AllowUserToResizeRows = False
        Me.dgvGK2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvGK2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvGK2.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvGK2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvGK2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGK2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvGK2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGK2.Location = New System.Drawing.Point(0, 60)
        Me.dgvGK2.Name = "dgvGK2"
        Me.dgvGK2.ReadOnly = True
        Me.dgvGK2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvGK2.RowHeadersVisible = False
        Me.dgvGK2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGK2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGK2.ShowEditingIcon = False
        Me.dgvGK2.Size = New System.Drawing.Size(228, 326)
        Me.dgvGK2.TabIndex = 4
        '
        'chkBH2
        '
        Me.chkBH2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkBH2.AutoSize = True
        Me.chkBH2.Location = New System.Drawing.Point(136, 36)
        Me.chkBH2.Name = "chkBH2"
        Me.chkBH2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkBH2.Size = New System.Drawing.Size(83, 17)
        Me.chkBH2.TabIndex = 3
        Me.chkBH2.Text = "Blockheben"
        Me.chkBH2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkBH2.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel3.Controls.Add(Me.picGruppe3)
        Me.Panel3.Controls.Add(Me.lblG3)
        Me.Panel3.Controls.Add(Me.pnlGruppe3)
        Me.Panel3.Controls.Add(Me.cbo3)
        Me.Panel3.Controls.Add(Me.lbl3)
        Me.Panel3.Controls.Add(Me.dgvGK3)
        Me.Panel3.Controls.Add(Me.chkBH3)
        Me.Panel3.Location = New System.Drawing.Point(479, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(232, 535)
        Me.Panel3.TabIndex = 3
        Me.Panel3.TabStop = True
        '
        'picGruppe3
        '
        Me.picGruppe3.Image = CType(resources.GetObject("picGruppe3.Image"), System.Drawing.Image)
        Me.picGruppe3.Location = New System.Drawing.Point(95, 34)
        Me.picGruppe3.Name = "picGruppe3"
        Me.picGruppe3.Size = New System.Drawing.Size(24, 24)
        Me.picGruppe3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picGruppe3.TabIndex = 7
        Me.picGruppe3.TabStop = False
        Me.picGruppe3.Visible = False
        '
        'lblG3
        '
        Me.lblG3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblG3.BackColor = System.Drawing.Color.SteelBlue
        Me.lblG3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblG3.ForeColor = System.Drawing.Color.White
        Me.lblG3.Location = New System.Drawing.Point(0, 0)
        Me.lblG3.Name = "lblG3"
        Me.lblG3.Size = New System.Drawing.Size(228, 29)
        Me.lblG3.TabIndex = 0
        Me.lblG3.Text = "Mittel"
        Me.lblG3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblG3.UseCompatibleTextRendering = True
        '
        'pnlGruppe3
        '
        Me.pnlGruppe3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlGruppe3.Controls.Add(Me.Label19)
        Me.pnlGruppe3.Controls.Add(Me.Label20)
        Me.pnlGruppe3.Controls.Add(Me.timeWb3)
        Me.pnlGruppe3.Controls.Add(Me.dateW3)
        Me.pnlGruppe3.Controls.Add(Me.Label37)
        Me.pnlGruppe3.Controls.Add(Me.timeWe3)
        Me.pnlGruppe3.Controls.Add(Me.pnl3)
        Me.pnlGruppe3.Controls.Add(Me.Label26)
        Me.pnlGruppe3.Controls.Add(Me.dateA3)
        Me.pnlGruppe3.Controls.Add(Me.Label3)
        Me.pnlGruppe3.Controls.Add(Me.timeA3)
        Me.pnlGruppe3.Controls.Add(Me.Label10)
        Me.pnlGruppe3.Controls.Add(Me.Label11)
        Me.pnlGruppe3.Controls.Add(Me.timeR3)
        Me.pnlGruppe3.Controls.Add(Me.date3)
        Me.pnlGruppe3.Controls.Add(Me.timeS3)
        Me.pnlGruppe3.Location = New System.Drawing.Point(0, 387)
        Me.pnlGruppe3.Name = "pnlGruppe3"
        Me.pnlGruppe3.Size = New System.Drawing.Size(228, 144)
        Me.pnlGruppe3.TabIndex = 5
        Me.pnlGruppe3.TabStop = True
        '
        'Label19
        '
        Me.Label19.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(170, 6)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(32, 13)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Ende"
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(103, 6)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(40, 13)
        Me.Label20.TabIndex = 12
        Me.Label20.Text = "Beginn"
        '
        'timeWb3
        '
        Me.timeWb3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeWb3.Checked = False
        Me.timeWb3.CustomFormat = "HH:mm"
        Me.timeWb3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeWb3.Location = New System.Drawing.Point(104, 22)
        Me.timeWb3.Name = "timeWb3"
        Me.timeWb3.ShowUpDown = True
        Me.timeWb3.Size = New System.Drawing.Size(51, 20)
        Me.timeWb3.TabIndex = 13
        '
        'dateW3
        '
        Me.dateW3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dateW3.Checked = False
        Me.dateW3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateW3.Location = New System.Drawing.Point(7, 22)
        Me.dateW3.Name = "dateW3"
        Me.dateW3.ShowUpDown = True
        Me.dateW3.Size = New System.Drawing.Size(80, 20)
        Me.dateW3.TabIndex = 11
        '
        'Label37
        '
        Me.Label37.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(6, 6)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(44, 13)
        Me.Label37.TabIndex = 10
        Me.Label37.Text = "Wiegen"
        '
        'timeWe3
        '
        Me.timeWe3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeWe3.Checked = False
        Me.timeWe3.CustomFormat = "HH:mm"
        Me.timeWe3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeWe3.Location = New System.Drawing.Point(171, 22)
        Me.timeWe3.Name = "timeWe3"
        Me.timeWe3.ShowUpDown = True
        Me.timeWe3.Size = New System.Drawing.Size(51, 20)
        Me.timeWe3.TabIndex = 15
        '
        'pnl3
        '
        Me.pnl3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnl3.BackColor = System.Drawing.Color.SteelBlue
        Me.pnl3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl3.Controls.Add(Me.cboB3)
        Me.pnl3.Controls.Add(Me.Label23)
        Me.pnl3.Location = New System.Drawing.Point(171, 95)
        Me.pnl3.Name = "pnl3"
        Me.pnl3.Size = New System.Drawing.Size(61, 48)
        Me.pnl3.TabIndex = 30
        '
        'cboB3
        '
        Me.cboB3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboB3.FormattingEnabled = True
        Me.cboB3.Items.AddRange(New Object() {"1", "2"})
        Me.cboB3.Location = New System.Drawing.Point(14, 19)
        Me.cboB3.Name = "cboB3"
        Me.cboB3.Size = New System.Drawing.Size(30, 21)
        Me.cboB3.TabIndex = 32
        '
        'Label23
        '
        Me.Label23.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(12, 4)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(34, 13)
        Me.Label23.TabIndex = 31
        Me.Label23.Text = "Bohle"
        '
        'Label26
        '
        Me.Label26.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label26.AutoSize = True
        Me.Label26.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label26.Location = New System.Drawing.Point(6, 98)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(42, 13)
        Me.Label26.TabIndex = 22
        Me.Label26.Text = "Athletik"
        '
        'dateA3
        '
        Me.dateA3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dateA3.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.dateA3.Checked = False
        Me.dateA3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateA3.Location = New System.Drawing.Point(7, 114)
        Me.dateA3.Name = "dateA3"
        Me.dateA3.ShowUpDown = True
        Me.dateA3.Size = New System.Drawing.Size(80, 20)
        Me.dateA3.TabIndex = 23
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Gewichtheben"
        '
        'timeA3
        '
        Me.timeA3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeA3.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.timeA3.Checked = False
        Me.timeA3.CustomFormat = "HH:mm"
        Me.timeA3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeA3.Location = New System.Drawing.Point(104, 114)
        Me.timeA3.Name = "timeA3"
        Me.timeA3.ShowUpDown = True
        Me.timeA3.Size = New System.Drawing.Size(51, 20)
        Me.timeA3.TabIndex = 24
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(170, 52)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(41, 13)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Stoßen"
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(103, 52)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(41, 13)
        Me.Label11.TabIndex = 18
        Me.Label11.Text = "Reißen"
        '
        'timeR3
        '
        Me.timeR3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeR3.Checked = False
        Me.timeR3.CustomFormat = "HH:mm"
        Me.timeR3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeR3.Location = New System.Drawing.Point(104, 68)
        Me.timeR3.Name = "timeR3"
        Me.timeR3.ShowUpDown = True
        Me.timeR3.Size = New System.Drawing.Size(51, 20)
        Me.timeR3.TabIndex = 19
        '
        'date3
        '
        Me.date3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.date3.Checked = False
        Me.date3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date3.Location = New System.Drawing.Point(7, 68)
        Me.date3.Name = "date3"
        Me.date3.ShowUpDown = True
        Me.date3.Size = New System.Drawing.Size(80, 20)
        Me.date3.TabIndex = 17
        '
        'timeS3
        '
        Me.timeS3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeS3.Checked = False
        Me.timeS3.CustomFormat = "HH:mm"
        Me.timeS3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeS3.Location = New System.Drawing.Point(171, 68)
        Me.timeS3.Name = "timeS3"
        Me.timeS3.ShowUpDown = True
        Me.timeS3.Size = New System.Drawing.Size(51, 20)
        Me.timeS3.TabIndex = 21
        '
        'cbo3
        '
        Me.cbo3.BackColor = System.Drawing.SystemColors.Window
        Me.cbo3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo3.DropDownWidth = 35
        Me.cbo3.FormattingEnabled = True
        Me.cbo3.Location = New System.Drawing.Point(50, 34)
        Me.cbo3.Name = "cbo3"
        Me.cbo3.Size = New System.Drawing.Size(41, 21)
        Me.cbo3.Sorted = True
        Me.cbo3.TabIndex = 2
        '
        'lbl3
        '
        Me.lbl3.AutoSize = True
        Me.lbl3.BackColor = System.Drawing.Color.Transparent
        Me.lbl3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl3.Location = New System.Drawing.Point(5, 36)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(42, 13)
        Me.lbl3.TabIndex = 1
        Me.lbl3.Text = "Gruppe"
        Me.lbl3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvGK3
        '
        Me.dgvGK3.AllowDrop = True
        Me.dgvGK3.AllowUserToAddRows = False
        Me.dgvGK3.AllowUserToDeleteRows = False
        Me.dgvGK3.AllowUserToOrderColumns = True
        Me.dgvGK3.AllowUserToResizeRows = False
        Me.dgvGK3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvGK3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvGK3.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvGK3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvGK3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGK3.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvGK3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGK3.Location = New System.Drawing.Point(0, 60)
        Me.dgvGK3.Name = "dgvGK3"
        Me.dgvGK3.ReadOnly = True
        Me.dgvGK3.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvGK3.RowHeadersVisible = False
        Me.dgvGK3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGK3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGK3.ShowEditingIcon = False
        Me.dgvGK3.Size = New System.Drawing.Size(228, 326)
        Me.dgvGK3.TabIndex = 4
        '
        'chkBH3
        '
        Me.chkBH3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkBH3.AutoSize = True
        Me.chkBH3.Location = New System.Drawing.Point(136, 36)
        Me.chkBH3.Name = "chkBH3"
        Me.chkBH3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkBH3.Size = New System.Drawing.Size(83, 17)
        Me.chkBH3.TabIndex = 3
        Me.chkBH3.Text = "Blockheben"
        Me.chkBH3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkBH3.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel4.Controls.Add(Me.picGruppe4)
        Me.Panel4.Controls.Add(Me.lblG4)
        Me.Panel4.Controls.Add(Me.pnlGruppe4)
        Me.Panel4.Controls.Add(Me.cbo4)
        Me.Panel4.Controls.Add(Me.lbl4)
        Me.Panel4.Controls.Add(Me.dgvGK4)
        Me.Panel4.Controls.Add(Me.chkBH4)
        Me.Panel4.Location = New System.Drawing.Point(717, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(232, 535)
        Me.Panel4.TabIndex = 4
        Me.Panel4.TabStop = True
        '
        'picGruppe4
        '
        Me.picGruppe4.Image = CType(resources.GetObject("picGruppe4.Image"), System.Drawing.Image)
        Me.picGruppe4.Location = New System.Drawing.Point(95, 34)
        Me.picGruppe4.Name = "picGruppe4"
        Me.picGruppe4.Size = New System.Drawing.Size(24, 24)
        Me.picGruppe4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picGruppe4.TabIndex = 7
        Me.picGruppe4.TabStop = False
        Me.picGruppe4.Visible = False
        '
        'lblG4
        '
        Me.lblG4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblG4.BackColor = System.Drawing.Color.SteelBlue
        Me.lblG4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblG4.ForeColor = System.Drawing.Color.White
        Me.lblG4.Location = New System.Drawing.Point(0, 0)
        Me.lblG4.Name = "lblG4"
        Me.lblG4.Size = New System.Drawing.Size(228, 29)
        Me.lblG4.TabIndex = 0
        Me.lblG4.Text = "Halbschwer"
        Me.lblG4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblG4.UseCompatibleTextRendering = True
        '
        'pnlGruppe4
        '
        Me.pnlGruppe4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlGruppe4.Controls.Add(Me.Label38)
        Me.pnlGruppe4.Controls.Add(Me.Label39)
        Me.pnlGruppe4.Controls.Add(Me.timeWb4)
        Me.pnlGruppe4.Controls.Add(Me.dateW4)
        Me.pnlGruppe4.Controls.Add(Me.Label40)
        Me.pnlGruppe4.Controls.Add(Me.timeWe4)
        Me.pnlGruppe4.Controls.Add(Me.Label29)
        Me.pnlGruppe4.Controls.Add(Me.dateA4)
        Me.pnlGruppe4.Controls.Add(Me.pnl4)
        Me.pnlGruppe4.Controls.Add(Me.timeA4)
        Me.pnlGruppe4.Controls.Add(Me.Label12)
        Me.pnlGruppe4.Controls.Add(Me.Label13)
        Me.pnlGruppe4.Controls.Add(Me.timeR4)
        Me.pnlGruppe4.Controls.Add(Me.date4)
        Me.pnlGruppe4.Controls.Add(Me.Label4)
        Me.pnlGruppe4.Controls.Add(Me.timeS4)
        Me.pnlGruppe4.Location = New System.Drawing.Point(0, 387)
        Me.pnlGruppe4.Name = "pnlGruppe4"
        Me.pnlGruppe4.Size = New System.Drawing.Size(228, 144)
        Me.pnlGruppe4.TabIndex = 5
        Me.pnlGruppe4.TabStop = True
        '
        'Label38
        '
        Me.Label38.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(170, 6)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(32, 13)
        Me.Label38.TabIndex = 14
        Me.Label38.Text = "Ende"
        '
        'Label39
        '
        Me.Label39.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(103, 6)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(40, 13)
        Me.Label39.TabIndex = 12
        Me.Label39.Text = "Beginn"
        '
        'timeWb4
        '
        Me.timeWb4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeWb4.Checked = False
        Me.timeWb4.CustomFormat = "HH:mm"
        Me.timeWb4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeWb4.Location = New System.Drawing.Point(104, 22)
        Me.timeWb4.Name = "timeWb4"
        Me.timeWb4.ShowUpDown = True
        Me.timeWb4.Size = New System.Drawing.Size(51, 20)
        Me.timeWb4.TabIndex = 13
        '
        'dateW4
        '
        Me.dateW4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dateW4.Checked = False
        Me.dateW4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateW4.Location = New System.Drawing.Point(7, 22)
        Me.dateW4.Name = "dateW4"
        Me.dateW4.ShowUpDown = True
        Me.dateW4.Size = New System.Drawing.Size(80, 20)
        Me.dateW4.TabIndex = 11
        '
        'Label40
        '
        Me.Label40.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(6, 6)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(44, 13)
        Me.Label40.TabIndex = 10
        Me.Label40.Text = "Wiegen"
        '
        'timeWe4
        '
        Me.timeWe4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeWe4.Checked = False
        Me.timeWe4.CustomFormat = "HH:mm"
        Me.timeWe4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeWe4.Location = New System.Drawing.Point(171, 22)
        Me.timeWe4.Name = "timeWe4"
        Me.timeWe4.ShowUpDown = True
        Me.timeWe4.Size = New System.Drawing.Size(51, 20)
        Me.timeWe4.TabIndex = 15
        '
        'Label29
        '
        Me.Label29.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label29.AutoSize = True
        Me.Label29.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label29.Location = New System.Drawing.Point(6, 98)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(42, 13)
        Me.Label29.TabIndex = 22
        Me.Label29.Text = "Athletik"
        '
        'dateA4
        '
        Me.dateA4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dateA4.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.dateA4.Checked = False
        Me.dateA4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateA4.Location = New System.Drawing.Point(7, 114)
        Me.dateA4.Name = "dateA4"
        Me.dateA4.ShowUpDown = True
        Me.dateA4.Size = New System.Drawing.Size(80, 20)
        Me.dateA4.TabIndex = 23
        '
        'pnl4
        '
        Me.pnl4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnl4.BackColor = System.Drawing.Color.SteelBlue
        Me.pnl4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl4.Controls.Add(Me.cboB4)
        Me.pnl4.Controls.Add(Me.Label24)
        Me.pnl4.Location = New System.Drawing.Point(171, 95)
        Me.pnl4.Name = "pnl4"
        Me.pnl4.Size = New System.Drawing.Size(61, 48)
        Me.pnl4.TabIndex = 30
        '
        'cboB4
        '
        Me.cboB4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboB4.FormattingEnabled = True
        Me.cboB4.Items.AddRange(New Object() {"1", "2"})
        Me.cboB4.Location = New System.Drawing.Point(14, 19)
        Me.cboB4.Name = "cboB4"
        Me.cboB4.Size = New System.Drawing.Size(30, 21)
        Me.cboB4.TabIndex = 32
        '
        'Label24
        '
        Me.Label24.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label24.AutoSize = True
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(12, 4)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(34, 13)
        Me.Label24.TabIndex = 31
        Me.Label24.Text = "Bohle"
        '
        'timeA4
        '
        Me.timeA4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeA4.Checked = False
        Me.timeA4.CustomFormat = "HH:mm"
        Me.timeA4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeA4.Location = New System.Drawing.Point(104, 114)
        Me.timeA4.Name = "timeA4"
        Me.timeA4.ShowUpDown = True
        Me.timeA4.Size = New System.Drawing.Size(50, 20)
        Me.timeA4.TabIndex = 24
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(170, 52)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(41, 13)
        Me.Label12.TabIndex = 20
        Me.Label12.Text = "Stoßen"
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(103, 52)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(41, 13)
        Me.Label13.TabIndex = 18
        Me.Label13.Text = "Reißen"
        '
        'timeR4
        '
        Me.timeR4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeR4.Checked = False
        Me.timeR4.CustomFormat = "HH:mm"
        Me.timeR4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeR4.Location = New System.Drawing.Point(104, 68)
        Me.timeR4.Name = "timeR4"
        Me.timeR4.ShowUpDown = True
        Me.timeR4.Size = New System.Drawing.Size(50, 20)
        Me.timeR4.TabIndex = 19
        '
        'date4
        '
        Me.date4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.date4.Checked = False
        Me.date4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date4.Location = New System.Drawing.Point(7, 68)
        Me.date4.Name = "date4"
        Me.date4.ShowUpDown = True
        Me.date4.Size = New System.Drawing.Size(80, 20)
        Me.date4.TabIndex = 17
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 52)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Gewichtheben"
        '
        'timeS4
        '
        Me.timeS4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeS4.Checked = False
        Me.timeS4.CustomFormat = "HH:mm"
        Me.timeS4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeS4.Location = New System.Drawing.Point(171, 68)
        Me.timeS4.Name = "timeS4"
        Me.timeS4.ShowUpDown = True
        Me.timeS4.Size = New System.Drawing.Size(50, 20)
        Me.timeS4.TabIndex = 21
        '
        'cbo4
        '
        Me.cbo4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo4.DropDownWidth = 35
        Me.cbo4.FormattingEnabled = True
        Me.cbo4.Location = New System.Drawing.Point(50, 34)
        Me.cbo4.Name = "cbo4"
        Me.cbo4.Size = New System.Drawing.Size(41, 21)
        Me.cbo4.Sorted = True
        Me.cbo4.TabIndex = 2
        '
        'lbl4
        '
        Me.lbl4.AutoSize = True
        Me.lbl4.BackColor = System.Drawing.Color.Transparent
        Me.lbl4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl4.Location = New System.Drawing.Point(5, 36)
        Me.lbl4.Name = "lbl4"
        Me.lbl4.Size = New System.Drawing.Size(42, 13)
        Me.lbl4.TabIndex = 1
        Me.lbl4.Text = "Gruppe"
        Me.lbl4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvGK4
        '
        Me.dgvGK4.AllowDrop = True
        Me.dgvGK4.AllowUserToAddRows = False
        Me.dgvGK4.AllowUserToDeleteRows = False
        Me.dgvGK4.AllowUserToOrderColumns = True
        Me.dgvGK4.AllowUserToResizeRows = False
        Me.dgvGK4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvGK4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvGK4.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvGK4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvGK4.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGK4.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvGK4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGK4.Location = New System.Drawing.Point(0, 60)
        Me.dgvGK4.Name = "dgvGK4"
        Me.dgvGK4.ReadOnly = True
        Me.dgvGK4.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvGK4.RowHeadersVisible = False
        Me.dgvGK4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGK4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGK4.ShowEditingIcon = False
        Me.dgvGK4.Size = New System.Drawing.Size(228, 326)
        Me.dgvGK4.TabIndex = 4
        '
        'chkBH4
        '
        Me.chkBH4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkBH4.AutoSize = True
        Me.chkBH4.Location = New System.Drawing.Point(136, 36)
        Me.chkBH4.Name = "chkBH4"
        Me.chkBH4.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkBH4.Size = New System.Drawing.Size(83, 17)
        Me.chkBH4.TabIndex = 3
        Me.chkBH4.Text = "Blockheben"
        Me.chkBH4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkBH4.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel5.Controls.Add(Me.picGruppe5)
        Me.Panel5.Controls.Add(Me.lblG5)
        Me.Panel5.Controls.Add(Me.pnlGruppe5)
        Me.Panel5.Controls.Add(Me.cbo5)
        Me.Panel5.Controls.Add(Me.lbl5)
        Me.Panel5.Controls.Add(Me.dgvGK5)
        Me.Panel5.Controls.Add(Me.chkBH5)
        Me.Panel5.Location = New System.Drawing.Point(955, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(232, 535)
        Me.Panel5.TabIndex = 5
        Me.Panel5.TabStop = True
        '
        'picGruppe5
        '
        Me.picGruppe5.Image = CType(resources.GetObject("picGruppe5.Image"), System.Drawing.Image)
        Me.picGruppe5.Location = New System.Drawing.Point(95, 34)
        Me.picGruppe5.Name = "picGruppe5"
        Me.picGruppe5.Size = New System.Drawing.Size(24, 24)
        Me.picGruppe5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picGruppe5.TabIndex = 7
        Me.picGruppe5.TabStop = False
        Me.picGruppe5.Visible = False
        '
        'lblG5
        '
        Me.lblG5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblG5.BackColor = System.Drawing.Color.SteelBlue
        Me.lblG5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblG5.ForeColor = System.Drawing.Color.White
        Me.lblG5.Location = New System.Drawing.Point(0, 0)
        Me.lblG5.Name = "lblG5"
        Me.lblG5.Size = New System.Drawing.Size(230, 29)
        Me.lblG5.TabIndex = 0
        Me.lblG5.Text = "Schwer"
        Me.lblG5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblG5.UseCompatibleTextRendering = True
        '
        'pnlGruppe5
        '
        Me.pnlGruppe5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlGruppe5.Controls.Add(Me.Label41)
        Me.pnlGruppe5.Controls.Add(Me.Label42)
        Me.pnlGruppe5.Controls.Add(Me.timeWb5)
        Me.pnlGruppe5.Controls.Add(Me.dateW5)
        Me.pnlGruppe5.Controls.Add(Me.Label43)
        Me.pnlGruppe5.Controls.Add(Me.timeWe5)
        Me.pnlGruppe5.Controls.Add(Me.Label30)
        Me.pnlGruppe5.Controls.Add(Me.dateA5)
        Me.pnlGruppe5.Controls.Add(Me.pnl5)
        Me.pnlGruppe5.Controls.Add(Me.timeA5)
        Me.pnlGruppe5.Controls.Add(Me.Label14)
        Me.pnlGruppe5.Controls.Add(Me.Label15)
        Me.pnlGruppe5.Controls.Add(Me.timeR5)
        Me.pnlGruppe5.Controls.Add(Me.date5)
        Me.pnlGruppe5.Controls.Add(Me.Label5)
        Me.pnlGruppe5.Controls.Add(Me.timeS5)
        Me.pnlGruppe5.Location = New System.Drawing.Point(0, 387)
        Me.pnlGruppe5.Name = "pnlGruppe5"
        Me.pnlGruppe5.Size = New System.Drawing.Size(228, 144)
        Me.pnlGruppe5.TabIndex = 5
        Me.pnlGruppe5.TabStop = True
        '
        'Label41
        '
        Me.Label41.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(171, 6)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(32, 13)
        Me.Label41.TabIndex = 14
        Me.Label41.Text = "Ende"
        '
        'Label42
        '
        Me.Label42.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(103, 6)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(40, 13)
        Me.Label42.TabIndex = 12
        Me.Label42.Text = "Beginn"
        '
        'timeWb5
        '
        Me.timeWb5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeWb5.Checked = False
        Me.timeWb5.CustomFormat = "HH:mm"
        Me.timeWb5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeWb5.Location = New System.Drawing.Point(104, 22)
        Me.timeWb5.Name = "timeWb5"
        Me.timeWb5.ShowUpDown = True
        Me.timeWb5.Size = New System.Drawing.Size(51, 20)
        Me.timeWb5.TabIndex = 13
        '
        'dateW5
        '
        Me.dateW5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dateW5.Checked = False
        Me.dateW5.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateW5.Location = New System.Drawing.Point(7, 22)
        Me.dateW5.Name = "dateW5"
        Me.dateW5.ShowUpDown = True
        Me.dateW5.Size = New System.Drawing.Size(80, 20)
        Me.dateW5.TabIndex = 11
        '
        'Label43
        '
        Me.Label43.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(6, 6)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(44, 13)
        Me.Label43.TabIndex = 10
        Me.Label43.Text = "Wiegen"
        '
        'timeWe5
        '
        Me.timeWe5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeWe5.Checked = False
        Me.timeWe5.CustomFormat = "HH:mm"
        Me.timeWe5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeWe5.Location = New System.Drawing.Point(171, 22)
        Me.timeWe5.Name = "timeWe5"
        Me.timeWe5.ShowUpDown = True
        Me.timeWe5.Size = New System.Drawing.Size(51, 20)
        Me.timeWe5.TabIndex = 15
        '
        'Label30
        '
        Me.Label30.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label30.AutoSize = True
        Me.Label30.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label30.Location = New System.Drawing.Point(6, 98)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(42, 13)
        Me.Label30.TabIndex = 22
        Me.Label30.Text = "Athletik"
        '
        'dateA5
        '
        Me.dateA5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dateA5.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.dateA5.Checked = False
        Me.dateA5.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateA5.Location = New System.Drawing.Point(7, 114)
        Me.dateA5.Name = "dateA5"
        Me.dateA5.ShowUpDown = True
        Me.dateA5.Size = New System.Drawing.Size(80, 20)
        Me.dateA5.TabIndex = 23
        '
        'pnl5
        '
        Me.pnl5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnl5.BackColor = System.Drawing.Color.SteelBlue
        Me.pnl5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl5.Controls.Add(Me.cboB5)
        Me.pnl5.Controls.Add(Me.Label25)
        Me.pnl5.Location = New System.Drawing.Point(171, 95)
        Me.pnl5.Name = "pnl5"
        Me.pnl5.Size = New System.Drawing.Size(61, 48)
        Me.pnl5.TabIndex = 30
        '
        'cboB5
        '
        Me.cboB5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboB5.FormattingEnabled = True
        Me.cboB5.Items.AddRange(New Object() {"1", "2"})
        Me.cboB5.Location = New System.Drawing.Point(14, 19)
        Me.cboB5.Name = "cboB5"
        Me.cboB5.Size = New System.Drawing.Size(31, 21)
        Me.cboB5.TabIndex = 32
        '
        'Label25
        '
        Me.Label25.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label25.AutoSize = True
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(12, 4)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(34, 13)
        Me.Label25.TabIndex = 31
        Me.Label25.Text = "Bohle"
        '
        'timeA5
        '
        Me.timeA5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeA5.Checked = False
        Me.timeA5.CustomFormat = "HH:mm"
        Me.timeA5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeA5.Location = New System.Drawing.Point(104, 114)
        Me.timeA5.Name = "timeA5"
        Me.timeA5.ShowUpDown = True
        Me.timeA5.Size = New System.Drawing.Size(51, 20)
        Me.timeA5.TabIndex = 24
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(171, 52)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(41, 13)
        Me.Label14.TabIndex = 20
        Me.Label14.Text = "Stoßen"
        '
        'Label15
        '
        Me.Label15.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(103, 52)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(41, 13)
        Me.Label15.TabIndex = 18
        Me.Label15.Text = "Reißen"
        '
        'timeR5
        '
        Me.timeR5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeR5.Checked = False
        Me.timeR5.CustomFormat = "HH:mm"
        Me.timeR5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeR5.Location = New System.Drawing.Point(104, 68)
        Me.timeR5.Name = "timeR5"
        Me.timeR5.ShowUpDown = True
        Me.timeR5.Size = New System.Drawing.Size(51, 20)
        Me.timeR5.TabIndex = 19
        '
        'date5
        '
        Me.date5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.date5.Checked = False
        Me.date5.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date5.Location = New System.Drawing.Point(7, 68)
        Me.date5.Name = "date5"
        Me.date5.ShowUpDown = True
        Me.date5.Size = New System.Drawing.Size(80, 20)
        Me.date5.TabIndex = 17
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 52)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 13)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "Gewichtheben"
        '
        'timeS5
        '
        Me.timeS5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.timeS5.Checked = False
        Me.timeS5.CustomFormat = "HH:mm"
        Me.timeS5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeS5.Location = New System.Drawing.Point(171, 68)
        Me.timeS5.Name = "timeS5"
        Me.timeS5.ShowUpDown = True
        Me.timeS5.Size = New System.Drawing.Size(51, 20)
        Me.timeS5.TabIndex = 21
        '
        'cbo5
        '
        Me.cbo5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo5.DropDownWidth = 35
        Me.cbo5.FormattingEnabled = True
        Me.cbo5.Location = New System.Drawing.Point(50, 34)
        Me.cbo5.Name = "cbo5"
        Me.cbo5.Size = New System.Drawing.Size(41, 21)
        Me.cbo5.Sorted = True
        Me.cbo5.TabIndex = 2
        '
        'lbl5
        '
        Me.lbl5.AutoSize = True
        Me.lbl5.BackColor = System.Drawing.Color.Transparent
        Me.lbl5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl5.Location = New System.Drawing.Point(5, 36)
        Me.lbl5.Name = "lbl5"
        Me.lbl5.Size = New System.Drawing.Size(42, 13)
        Me.lbl5.TabIndex = 1
        Me.lbl5.Text = "Gruppe"
        Me.lbl5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvGK5
        '
        Me.dgvGK5.AllowDrop = True
        Me.dgvGK5.AllowUserToAddRows = False
        Me.dgvGK5.AllowUserToDeleteRows = False
        Me.dgvGK5.AllowUserToOrderColumns = True
        Me.dgvGK5.AllowUserToResizeRows = False
        Me.dgvGK5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvGK5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvGK5.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvGK5.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvGK5.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGK5.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvGK5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGK5.Location = New System.Drawing.Point(0, 60)
        Me.dgvGK5.Name = "dgvGK5"
        Me.dgvGK5.ReadOnly = True
        Me.dgvGK5.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvGK5.RowHeadersVisible = False
        Me.dgvGK5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGK5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGK5.ShowEditingIcon = False
        Me.dgvGK5.Size = New System.Drawing.Size(225, 326)
        Me.dgvGK5.TabIndex = 4
        '
        'chkBH5
        '
        Me.chkBH5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkBH5.AutoSize = True
        Me.chkBH5.Location = New System.Drawing.Point(136, 36)
        Me.chkBH5.Name = "chkBH5"
        Me.chkBH5.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkBH5.Size = New System.Drawing.Size(83, 17)
        Me.chkBH5.TabIndex = 3
        Me.chkBH5.Text = "Blockheben"
        Me.chkBH5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkBH5.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.MenuBar
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuGruppen, Me.mnuExtras})
        Me.MenuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(6, 6, 0, 6)
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.MenuStrip1.Size = New System.Drawing.Size(1198, 31)
        Me.MenuStrip1.TabIndex = 199
        Me.MenuStrip1.Tag = ""
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSpeichern, Me.mnuDrucken, Me.ToolStripMenuItem1, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 19)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuSpeichern
        '
        Me.mnuSpeichern.Name = "mnuSpeichern"
        Me.mnuSpeichern.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSpeichern.Size = New System.Drawing.Size(168, 22)
        Me.mnuSpeichern.Text = "&Speichern"
        '
        'mnuDrucken
        '
        Me.mnuDrucken.Name = "mnuDrucken"
        Me.mnuDrucken.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.mnuDrucken.Size = New System.Drawing.Size(168, 22)
        Me.mnuDrucken.Text = "&Drucken"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(165, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(168, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuGruppen
        '
        Me.mnuGruppen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.mnuGruppen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuGruppenLöschen, Me.ToolStripMenuItem3, Me.ZugewieseneGruppenToolStripMenuItem, Me.ToolStripMenuItem5, Me.mnuCutGroups})
        Me.mnuGruppen.Name = "mnuGruppen"
        Me.mnuGruppen.Size = New System.Drawing.Size(65, 19)
        Me.mnuGruppen.Text = "&Gruppen"
        '
        'mnuGruppenLöschen
        '
        Me.mnuGruppenLöschen.Name = "mnuGruppenLöschen"
        Me.mnuGruppenLöschen.Size = New System.Drawing.Size(190, 22)
        Me.mnuGruppenLöschen.Text = "Zuweisungen &löschen"
        Me.mnuGruppenLöschen.ToolTipText = "alle Gruppenzuweisungen werden aufgehoben"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(187, 6)
        '
        'ZugewieseneGruppenToolStripMenuItem
        '
        Me.ZugewieseneGruppenToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuZusammenhalten, Me.mnuAufteilen})
        Me.ZugewieseneGruppenToolStripMenuItem.Name = "ZugewieseneGruppenToolStripMenuItem"
        Me.ZugewieseneGruppenToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.ZugewieseneGruppenToolStripMenuItem.Text = "zugewiesene &Gruppen"
        '
        'mnuZusammenhalten
        '
        Me.mnuZusammenhalten.Name = "mnuZusammenhalten"
        Me.mnuZusammenhalten.Size = New System.Drawing.Size(231, 22)
        Me.mnuZusammenhalten.Text = "immer &zusammenhalten"
        '
        'mnuAufteilen
        '
        Me.mnuAufteilen.Checked = True
        Me.mnuAufteilen.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuAufteilen.Name = "mnuAufteilen"
        Me.mnuAufteilen.Size = New System.Drawing.Size(231, 22)
        Me.mnuAufteilen.Text = "nach Ausschreibung &aufteilen"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(187, 6)
        '
        'mnuCutGroups
        '
        Me.mnuCutGroups.Enabled = False
        Me.mnuCutGroups.Name = "mnuCutGroups"
        Me.mnuCutGroups.Size = New System.Drawing.Size(190, 22)
        Me.mnuCutGroups.Text = "nur bis Teiler füllen"
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLayout, Me.ToolStripMenuItem2, Me.mnuMeldungen, Me.mnuKonflikte, Me.ToolStripMenuItem4, Me.mnuJGListeOffen})
        Me.mnuExtras.Name = "mnuExtras"
        Me.mnuExtras.Size = New System.Drawing.Size(49, 19)
        Me.mnuExtras.Text = "&Extras"
        '
        'mnuLayout
        '
        Me.mnuLayout.Name = "mnuLayout"
        Me.mnuLayout.Size = New System.Drawing.Size(210, 22)
        Me.mnuLayout.Text = "&Tabellen-Layout"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(207, 6)
        '
        'mnuMeldungen
        '
        Me.mnuMeldungen.CheckOnClick = True
        Me.mnuMeldungen.Name = "mnuMeldungen"
        Me.mnuMeldungen.Size = New System.Drawing.Size(210, 22)
        Me.mnuMeldungen.Text = "&Meldungen ausblenden"
        '
        'mnuKonflikte
        '
        Me.mnuKonflikte.Checked = True
        Me.mnuKonflikte.CheckOnClick = True
        Me.mnuKonflikte.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuKonflikte.Name = "mnuKonflikte"
        Me.mnuKonflikte.Size = New System.Drawing.Size(210, 22)
        Me.mnuKonflikte.Text = "bei &Konflikten nachfragen"
        Me.mnuKonflikte.Visible = False
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(207, 6)
        '
        'mnuJGListeOffen
        '
        Me.mnuJGListeOffen.Checked = True
        Me.mnuJGListeOffen.CheckOnClick = True
        Me.mnuJGListeOffen.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuJGListeOffen.Name = "mnuJGListeOffen"
        Me.mnuJGListeOffen.Size = New System.Drawing.Size(210, 22)
        Me.mnuJGListeOffen.Text = "JG-&Liste bleibt offen"
        '
        'bgwGK
        '
        Me.bgwGK.WorkerReportsProgress = True
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.SystemColors.MenuBar
        Me.Label31.ForeColor = System.Drawing.Color.Green
        Me.Label31.Location = New System.Drawing.Point(246, 8)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(101, 17)
        Me.Label31.TabIndex = 1
        Me.Label31.Text = "angelegte Gruppen"
        Me.Label31.UseCompatibleTextRendering = True
        '
        'cboGruppenSaved
        '
        Me.cboGruppenSaved.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGruppenSaved.FormattingEnabled = True
        Me.cboGruppenSaved.IntegralHeight = False
        Me.cboGruppenSaved.Location = New System.Drawing.Point(350, 5)
        Me.cboGruppenSaved.Name = "cboGruppenSaved"
        Me.cboGruppenSaved.Size = New System.Drawing.Size(80, 21)
        Me.cboGruppenSaved.Sorted = True
        Me.cboGruppenSaved.TabIndex = 2
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.SystemColors.MenuBar
        Me.Label32.ForeColor = System.Drawing.Color.Green
        Me.Label32.Location = New System.Drawing.Point(678, 8)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(51, 17)
        Me.Label32.TabIndex = 5
        Me.Label32.Text = "Jahrgang"
        Me.Label32.UseCompatibleTextRendering = True
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.BackColor = System.Drawing.SystemColors.MenuBar
        Me.Label33.ForeColor = System.Drawing.Color.Green
        Me.Label33.Location = New System.Drawing.Point(478, 8)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(61, 17)
        Me.Label33.TabIndex = 3
        Me.Label33.Text = "Geschlecht"
        Me.Label33.UseCompatibleTextRendering = True
        '
        'cboGeschlecht
        '
        Me.cboGeschlecht.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGeschlecht.FormattingEnabled = True
        Me.cboGeschlecht.Items.AddRange(New Object() {"männlich", "weiblich"})
        Me.cboGeschlecht.Location = New System.Drawing.Point(545, 5)
        Me.cboGeschlecht.Name = "cboGeschlecht"
        Me.cboGeschlecht.Size = New System.Drawing.Size(80, 21)
        Me.cboGeschlecht.TabIndex = 4
        '
        'pnlForm
        '
        Me.pnlForm.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlForm.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnlForm.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.pnlForm.ColumnCount = 5
        Me.pnlForm.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.pnlForm.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.pnlForm.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.pnlForm.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.pnlForm.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.pnlForm.Controls.Add(Me.Panel1, 0, 0)
        Me.pnlForm.Controls.Add(Me.Panel5, 4, 0)
        Me.pnlForm.Controls.Add(Me.Panel2, 1, 0)
        Me.pnlForm.Controls.Add(Me.Panel4, 3, 0)
        Me.pnlForm.Controls.Add(Me.Panel3, 2, 0)
        Me.pnlForm.Location = New System.Drawing.Point(4, 36)
        Me.pnlForm.Name = "pnlForm"
        Me.pnlForm.RowCount = 1
        Me.pnlForm.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.pnlForm.Size = New System.Drawing.Size(1190, 541)
        Me.pnlForm.TabIndex = 100
        Me.pnlForm.TabStop = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(987, 586)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(90, 26)
        Me.btnSave.TabIndex = 210
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Speichern"
        Me.btnSave.UseCompatibleTextRendering = True
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(1083, 586)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(90, 26)
        Me.btnClose.TabIndex = 211
        Me.btnClose.TabStop = False
        Me.btnClose.Text = "Beenden"
        Me.btnClose.UseCompatibleTextRendering = True
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lstJahrgang
        '
        Me.lstJahrgang.FormattingEnabled = True
        Me.lstJahrgang.Items.AddRange(New Object() {" AK 12  -  2002"})
        Me.lstJahrgang.Location = New System.Drawing.Point(739, 27)
        Me.lstJahrgang.MultiColumn = True
        Me.lstJahrgang.Name = "lstJahrgang"
        Me.lstJahrgang.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lstJahrgang.Size = New System.Drawing.Size(80, 17)
        Me.lstJahrgang.TabIndex = 8
        Me.lstJahrgang.TabStop = False
        Me.lstJahrgang.UseTabStops = False
        Me.lstJahrgang.Visible = False
        '
        'chkWiegenGleich
        '
        Me.chkWiegenGleich.AutoSize = True
        Me.chkWiegenGleich.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkWiegenGleich.Location = New System.Drawing.Point(18, 592)
        Me.chkWiegenGleich.Name = "chkWiegenGleich"
        Me.chkWiegenGleich.Size = New System.Drawing.Size(172, 17)
        Me.chkWiegenGleich.TabIndex = 200
        Me.chkWiegenGleich.TabStop = False
        Me.chkWiegenGleich.Text = "Wiegen für alle Gruppen gleich"
        Me.chkWiegenGleich.UseVisualStyleBackColor = True
        '
        'cboJahrgang
        '
        Me.cboJahrgang.DropDownHeight = 1
        Me.cboJahrgang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJahrgang.FormattingEnabled = True
        Me.cboJahrgang.IntegralHeight = False
        Me.cboJahrgang.Location = New System.Drawing.Point(735, 6)
        Me.cboJahrgang.Name = "cboJahrgang"
        Me.cboJahrgang.Size = New System.Drawing.Size(206, 21)
        Me.cboJahrgang.TabIndex = 212
        '
        'frmGruppen_JG
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.ClientSize = New System.Drawing.Size(1198, 621)
        Me.Controls.Add(Me.cboJahrgang)
        Me.Controls.Add(Me.chkWiegenGleich)
        Me.Controls.Add(Me.lstJahrgang)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.pnlForm)
        Me.Controls.Add(Me.cboGeschlecht)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.cboGruppenSaved)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1214, 659)
        Me.Name = "frmGruppen_JG"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Gruppen einteilen"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.picGruppe1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlGruppe1.ResumeLayout(False)
        Me.pnlGruppe1.PerformLayout()
        Me.pnl1.ResumeLayout(False)
        Me.pnl1.PerformLayout()
        CType(Me.dgvGK1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.picGruppe2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlGruppe2.ResumeLayout(False)
        Me.pnlGruppe2.PerformLayout()
        Me.pnl2.ResumeLayout(False)
        Me.pnl2.PerformLayout()
        CType(Me.dgvGK2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.picGruppe3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlGruppe3.ResumeLayout(False)
        Me.pnlGruppe3.PerformLayout()
        Me.pnl3.ResumeLayout(False)
        Me.pnl3.PerformLayout()
        CType(Me.dgvGK3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.picGruppe4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlGruppe4.ResumeLayout(False)
        Me.pnlGruppe4.PerformLayout()
        Me.pnl4.ResumeLayout(False)
        Me.pnl4.PerformLayout()
        CType(Me.dgvGK4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.picGruppe5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlGruppe5.ResumeLayout(False)
        Me.pnlGruppe5.PerformLayout()
        Me.pnl5.ResumeLayout(False)
        Me.pnl5.PerformLayout()
        CType(Me.dgvGK5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.pnlForm.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkBH1 As System.Windows.Forms.CheckBox
    Friend WithEvents dgvGK1 As System.Windows.Forms.DataGridView
    Friend WithEvents dgvGK2 As System.Windows.Forms.DataGridView
    Friend WithEvents chkBH2 As System.Windows.Forms.CheckBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents dgvGK3 As System.Windows.Forms.DataGridView
    Friend WithEvents chkBH3 As System.Windows.Forms.CheckBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents dgvGK4 As System.Windows.Forms.DataGridView
    Friend WithEvents chkBH4 As System.Windows.Forms.CheckBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents dgvGK5 As System.Windows.Forms.DataGridView
    Friend WithEvents chkBH5 As System.Windows.Forms.CheckBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDatei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeichern As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuBeenden As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtras As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLayout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bgwGK As System.ComponentModel.BackgroundWorker
    Friend WithEvents lbl2 As System.Windows.Forms.Label
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents lbl3 As System.Windows.Forms.Label
    Friend WithEvents lbl4 As System.Windows.Forms.Label
    Friend WithEvents lbl5 As System.Windows.Forms.Label
    Friend WithEvents cbo2 As System.Windows.Forms.ComboBox
    Friend WithEvents cbo1 As System.Windows.Forms.ComboBox
    Friend WithEvents cbo3 As System.Windows.Forms.ComboBox
    Friend WithEvents cbo4 As System.Windows.Forms.ComboBox
    Friend WithEvents cbo5 As System.Windows.Forms.ComboBox
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents timeS1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents timeS2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents timeS3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents timeS4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents timeS5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents timeR1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents timeR2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents timeR3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents timeR4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents timeR5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents timeA3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents timeA1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents timeA2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents timeA4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents timeA5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cboB1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboB2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents cboB3 As System.Windows.Forms.ComboBox
    Friend WithEvents cboB4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents cboB5 As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents dateA3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnlGruppe2 As System.Windows.Forms.Panel
    Friend WithEvents pnlGruppe1 As System.Windows.Forms.Panel
    Friend WithEvents pnlGruppe3 As System.Windows.Forms.Panel
    Friend WithEvents pnlGruppe4 As System.Windows.Forms.Panel
    Friend WithEvents pnlGruppe5 As System.Windows.Forms.Panel
    Friend WithEvents pnl3 As System.Windows.Forms.Panel
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents pnl1 As System.Windows.Forms.Panel
    Friend WithEvents pnl2 As System.Windows.Forms.Panel
    Friend WithEvents pnl4 As System.Windows.Forms.Panel
    Friend WithEvents pnl5 As System.Windows.Forms.Panel
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents dateA1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents dateA2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents dateA4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents dateA5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblG1 As System.Windows.Forms.Label
    Friend WithEvents lblG2 As System.Windows.Forms.Label
    Friend WithEvents lblG3 As System.Windows.Forms.Label
    Friend WithEvents lblG4 As System.Windows.Forms.Label
    Friend WithEvents lblG5 As System.Windows.Forms.Label
    Friend WithEvents mnuDrucken As ToolStripMenuItem
    Friend WithEvents Label31 As Label
    Friend WithEvents cboGruppenSaved As ComboBox
    Friend WithEvents Label32 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents cboGeschlecht As ComboBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents pnlForm As TableLayoutPanel
    Friend WithEvents btnSave As Button
    Friend WithEvents btnClose As Button
    Friend WithEvents lstJahrgang As ListBox
    Friend WithEvents mnuGruppen As ToolStripMenuItem
    Friend WithEvents mnuGruppenLöschen As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripSeparator
    Friend WithEvents ZugewieseneGruppenToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents mnuZusammenhalten As ToolStripMenuItem
    Friend WithEvents mnuAufteilen As ToolStripMenuItem
    Friend WithEvents mnuMeldungen As ToolStripMenuItem
    Friend WithEvents mnuKonflikte As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As ToolStripSeparator
    Friend WithEvents mnuJGListeOffen As ToolStripMenuItem
    Friend WithEvents Label34 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents timeWb1 As DateTimePicker
    Friend WithEvents dateW1 As DateTimePicker
    Friend WithEvents Label36 As Label
    Friend WithEvents timeWe1 As DateTimePicker
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents timeWb2 As DateTimePicker
    Friend WithEvents dateW2 As DateTimePicker
    Friend WithEvents Label18 As Label
    Friend WithEvents timeWe2 As DateTimePicker
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents timeWb3 As DateTimePicker
    Friend WithEvents dateW3 As DateTimePicker
    Friend WithEvents Label37 As Label
    Friend WithEvents timeWe3 As DateTimePicker
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents timeWb4 As DateTimePicker
    Friend WithEvents dateW4 As DateTimePicker
    Friend WithEvents Label40 As Label
    Friend WithEvents timeWe4 As DateTimePicker
    Friend WithEvents Label41 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents timeWb5 As DateTimePicker
    Friend WithEvents dateW5 As DateTimePicker
    Friend WithEvents Label43 As Label
    Friend WithEvents timeWe5 As DateTimePicker
    Friend WithEvents chkWiegenGleich As CheckBox
    Friend WithEvents ToolStripMenuItem5 As ToolStripSeparator
    Friend WithEvents mnuCutGroups As ToolStripMenuItem
    Friend WithEvents picGruppe2 As PictureBox
    Friend WithEvents picGruppe1 As PictureBox
    Friend WithEvents picGruppe3 As PictureBox
    Friend WithEvents picGruppe4 As PictureBox
    Friend WithEvents picGruppe5 As PictureBox
    Friend WithEvents cboJahrgang As ComboBox
End Class
