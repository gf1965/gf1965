﻿
Public Class frmKeyboards

    Private WithEvents _rawInput As RawInput_dll.RawInput
    Event keypressed()
    Private _AnzahlKeybords As Integer
    Private lblArray() As Label
    Private cboArray() As ComboBox
    Private chkArray() As CheckBox

    Private Sub KeyboardEvent(sender As Object, e As RawInput_dll.InputEventArg) Handles _rawInput.KeyPressed
        Dim source As String, index As Integer
        source = e.KeyPressEvent.Source
        index = CInt(Mid(source, InStr(source, "_") + 1)) - 1
        lblArray(index).BackColor = Color.LightGreen
        cboArray(index).Enabled = True
    End Sub

    Private Sub frmKeyboards_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If cmdNew.Enabled = False Then
            e.Cancel = True
            cmdNew.Enabled = True
            Exit Sub
        End If
        If cmdExit.Enabled = False Then
            cmdExit.Enabled = True
            e.Cancel = True
            Exit Sub
        End If
        If cmdSave.Enabled AndAlso MsgBox("Ohne Speichern beenden?", CType(MsgBoxStyle.Question + MsgBoxStyle.YesNo, MsgBoxStyle), Me.Text) = MsgBoxResult.No Then
            e.Cancel = True
            Exit Sub
        End If
        Control_Delete(lblArray.Count - 1)
        INI_Write("HideMessage", "Keyboard", CStr(chkMsg.Checked), INI_File)
    End Sub

    Private Sub Form_Load(sender As Object, e As EventArgs) Handles Me.Load
        cmdExit.Enabled = True
        _rawInput = New RawInput_dll.RawInput(Handle)
        _rawInput.CaptureOnlyIfTopMostWindow = True    'nimmt Tastatureingaben von aktivem Fenstern
        form_Repaint()
        cmdSave.Enabled = False
    End Sub

    Private Sub cboArray_Click(sender As Object, e As EventArgs)
        Static Err_Flag As Boolean = False
        Dim itemClicked As ComboBox = CType(sender, ComboBox)
        Dim index As Integer
        If Err_Flag Then Err_Flag = False : Exit Sub
        For i As Integer = LBound(cboArray) To UBound(cboArray)
            If cboArray(i) Is itemClicked Then
                index = i
                Exit For
            End If
        Next
        For i As Integer = LBound(cboArray) To UBound(cboArray)
            If cboArray(index).Text > "" And (i <> index And cboArray(i).Text = cboArray(index).Text) Then
                If Not chkMsg.Checked Then MsgBox("Doppelte Auswahl: <" & cboArray(i).Text & ">.", CType(MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, MsgBoxStyle))
                Err_Flag = True
                cboArray(index).SelectedIndex = -1
                Exit Sub
            End If
        Next
        chkArray(index).Enabled = True
        lblArray(index).BackColor = Color.Red
        cmdSave.Enabled = True
        Panel1.Focus()
    End Sub

    Private Sub chkArray_Click(sender As Object, e As EventArgs)
        Dim itemClicked As CheckBox = CType(sender, CheckBox)
        Dim index As Integer, platform As String
        For i As Integer = LBound(chkArray) To UBound(chkArray)
            If chkArray(i) Is itemClicked Then
                index = i
                Exit For
            End If
        Next
        platform = Strings.Left(cboArray(index).Text, 7)
        For i As Integer = LBound(chkArray) To UBound(chkArray)
            If i <> index And Strings.Left(cboArray(i).Text, 7) = platform Then
                chkArray(index).Checked = MsgBox("Für " & platform & " mehr als eine Control-Einheit aktivieren?", CType(MsgBoxStyle.Question + MsgBoxStyle.YesNo, MsgBoxStyle), Me.Text) = MsgBoxResult.Yes
            End If
        Next
    End Sub

    Private Sub cmdExit_Click(sender As Object, e As EventArgs) Handles cmdExit.Click
        cmdExit.Enabled = False
        If cmdSave.Enabled AndAlso MsgBox("Ohne Speichern beenden?", CType(MsgBoxStyle.Question + MsgBoxStyle.YesNo, MsgBoxStyle), Me.Text) = MsgBoxResult.No Then Exit Sub
        cmdExit.Enabled = True
        cmdSave.Enabled = False
        Me.Close()
    End Sub

    Private Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        For i As Integer = 0 To _AnzahlKeybords - 1
            INI_Write("Keyboard_" & i + 1, "Juror", cboArray(i).Text, INI_File)
            INI_Write("Keyboard_" & i + 1, "Control", Str(chkArray(i).Checked), INI_File)
        Next
        cmdSave.Enabled = False
        Me.Close()
    End Sub

    Private Sub cmdNew_Click(sender As Object, e As EventArgs) Handles cmdNew.Click
        'Dim i As Integer
        Dim Count_Old As Integer = lblArray.Count
        cmdNew.Enabled = False
        Control_Delete(Count_Old - 1)
        Form_Repaint()
        cmdSave.Enabled = True
    End Sub

    Sub Form_Repaint()
        Dim i As Integer
        _AnzahlKeybords = CInt(_rawInput.NumberOfKeyboards.ToString(Globalization.CultureInfo.InvariantCulture))
        ReDim lblArray(_AnzahlKeybords - 1)
        ReDim cboArray(_AnzahlKeybords - 1)
        ReDim chkArray(_AnzahlKeybords - 1)
        For i = 0 To _AnzahlKeybords - 1
            lblArray(i) = New Label
            NewLabel(lblArray(i), i)
            cboArray(i) = New ComboBox
            NewCombo(cboArray(i), i)
            chkArray(i) = New CheckBox
            NewCheck(chkArray(i), i)
        Next
        If FileIO.FileSystem.FileExists(INI_File) Then
            For i = 0 To _AnzahlKeybords - 1
                cboArray(i).Text = INI_Read("Keyboard_" & CDbl(Str(i)) + 1, "Juror", INI_File)
                cboArray(i).Enabled = cboArray(i).Text <> ""
                chkArray(i).Checked = CBool(INI_Read("Keyboard_" & CDbl(Str(i)) + 1, "Control", INI_File))
            Next
            chkMsg.Checked = CBool(INI_Read("HideMessage", "Keyboard", INI_File)) 'Text = Warnungen deaktivieren
        End If
    End Sub

    Sub Control_Delete(anz As Integer)
        For i = 0 To anz
            If Panel1.Controls.Contains(lblArray(i)) Then
                Panel1.Controls.Remove(lblArray(i))
                lblArray(i).Dispose()
            End If
            If Panel1.Controls.Contains(cboArray(i)) Then
                RemoveHandler cboArray(i).SelectedIndexChanged, AddressOf cboArray_Click
                Panel1.Controls.Remove(cboArray(i))
                cboArray(i).Dispose()
            End If
            If Panel1.Controls.Contains(chkArray(i)) Then
                RemoveHandler chkArray(i).Click, AddressOf chkArray_Click
                Panel1.Controls.Remove(chkArray(i))
                chkArray(i).Dispose()
            End If
        Next
    End Sub

    Sub NewLabel(ctl As Label, i As Integer)
        With ctl 'lblArray(i)
            .Parent = Panel1
            .Parent.Controls.Add(lblArray(i))
            .Name = "lbl" & CStr(i + 1)
            .TabIndex = i
            .TextAlign = ContentAlignment.MiddleCenter
            .Text = "Eingabegerät " & CStr(i + 1)
            '.Tag = "Keyboard:Active"
            .Size = New Size(100, 25)
            .AutoSize = False
            .BackColor = Color.LightGray
            .BorderStyle = BorderStyle.Fixed3D
            .Location = New Point(15, CInt(15 + i * 1.5 * .Height))
            .Visible = True
        End With
    End Sub

    Sub NewCombo(ctl As ComboBox, i As Integer)
        With ctl 'cboArray(i)
            .Parent = Panel1
            .Parent.Controls.Add(cboArray(i))
            .Name = "cbo" & CStr(i + 1)
            .TabIndex = lblArray(i).TabIndex
            .Size = New Size(160, 15)
            '.Tag = "Keyboard:Stage;Juror"
            .Location = New Point(127, lblArray(i).Top + 2)
            .Items.Add("Bohle 1  :  Kampfrichter 1")
            .Items.Add("Bohle 1  :  Kampfrichter 2")
            .Items.Add("Bohle 1  :  Kampfrichter 3")
            .Items.Add("Bohle 2  :  Kampfrichter 1")
            .Items.Add("Bohle 2  :  Kampfrichter 2")
            .Items.Add("Bohle 2  :  Kampfrichter 3")
            .DropDownStyle = ComboBoxStyle.DropDownList
            .Visible = True
            .Enabled = False
            AddHandler .SelectedIndexChanged, AddressOf cboArray_Click
        End With
    End Sub

    Sub NewCheck(ctl As CheckBox, i As Integer)
        With ctl 'chkArray(i)
            .Parent = Panel1
            .Parent.Controls.Add(chkArray(i))
            .Name = "chk" & CStr(i + 1)
            .TabIndex = lblArray(i).TabIndex
            .Text = "Control"
            .CheckAlign = ContentAlignment.MiddleLeft
            .Size = New Size(60, 17)
            '.Tag = "Keyboard:Control"
            .Location = New Point(300, lblArray(i).Top + 4)
            .Visible = True
            .Enabled = False
            AddHandler .Click, AddressOf chkArray_Click
        End With
    End Sub

    
End Class

