﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAufwärmen
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAufwärmen))
        Me.dgvJoin = New System.Windows.Forms.DataGridView()
        Me.lblDurchgang = New System.Windows.Forms.Label()
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.lblGruppe = New System.Windows.Forms.Label()
        Me.dgv2 = New System.Windows.Forms.DataGridView()
        Me.dgvTeam = New System.Windows.Forms.DataGridView()
        Me.Rolle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Team = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gegner = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mReissen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mStossen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mHeben = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mPunkte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Punkte2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlWertung = New System.Windows.Forms.Panel()
        Me.xGültig3 = New System.Windows.Forms.TextBox()
        Me.xTechniknote = New System.Windows.Forms.TextBox()
        Me.xGültig2 = New System.Windows.Forms.TextBox()
        Me.xGültig1 = New System.Windows.Forms.TextBox()
        Me.xAufruf = New System.Windows.Forms.Label()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTeam, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlWertung.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvJoin
        '
        Me.dgvJoin.AllowUserToAddRows = False
        Me.dgvJoin.AllowUserToDeleteRows = False
        Me.dgvJoin.AllowUserToResizeColumns = False
        Me.dgvJoin.AllowUserToResizeRows = False
        Me.dgvJoin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvJoin.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.dgvJoin.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvJoin.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.AppWorkspace
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.Padding = New System.Windows.Forms.Padding(0, 2, 0, 2)
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.dgvJoin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightCyan
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvJoin.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvJoin.EnableHeadersVisualStyles = False
        Me.dgvJoin.Location = New System.Drawing.Point(0, 32)
        Me.dgvJoin.Name = "dgvJoin"
        Me.dgvJoin.ReadOnly = True
        Me.dgvJoin.RowHeadersVisible = False
        Me.dgvJoin.RowHeadersWidth = 30
        Me.dgvJoin.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvJoin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvJoin.Size = New System.Drawing.Size(804, 421)
        Me.dgvJoin.TabIndex = 0
        Me.dgvJoin.Tag = "12"
        '
        'lblDurchgang
        '
        Me.lblDurchgang.AutoSize = True
        Me.lblDurchgang.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDurchgang.ForeColor = System.Drawing.Color.White
        Me.lblDurchgang.Location = New System.Drawing.Point(94, 6)
        Me.lblDurchgang.MinimumSize = New System.Drawing.Size(34, 20)
        Me.lblDurchgang.Name = "lblDurchgang"
        Me.lblDurchgang.Size = New System.Drawing.Size(34, 20)
        Me.lblDurchgang.TabIndex = 4
        Me.lblDurchgang.Tag = "12"
        Me.lblDurchgang.Text = "DG"
        Me.lblDurchgang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption
        '
        Me.lblCaption.AutoSize = True
        Me.lblCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.lblCaption.ForeColor = System.Drawing.Color.Gold
        Me.lblCaption.Location = New System.Drawing.Point(366, 3)
        Me.lblCaption.MinimumSize = New System.Drawing.Size(68, 26)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(68, 26)
        Me.lblCaption.TabIndex = 2
        Me.lblCaption.Tag = "16"
        Me.lblCaption.Text = "Bohle"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGruppe
        '
        Me.lblGruppe.AutoSize = True
        Me.lblGruppe.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGruppe.ForeColor = System.Drawing.Color.White
        Me.lblGruppe.Location = New System.Drawing.Point(12, 6)
        Me.lblGruppe.MinimumSize = New System.Drawing.Size(63, 20)
        Me.lblGruppe.Name = "lblGruppe"
        Me.lblGruppe.Size = New System.Drawing.Size(63, 20)
        Me.lblGruppe.TabIndex = 0
        Me.lblGruppe.Tag = "12"
        Me.lblGruppe.Text = "Gruppe"
        Me.lblGruppe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgv2
        '
        Me.dgv2.AllowUserToAddRows = False
        Me.dgv2.AllowUserToDeleteRows = False
        Me.dgv2.AllowUserToResizeColumns = False
        Me.dgv2.AllowUserToResizeRows = False
        Me.dgv2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.dgv2.BackgroundColor = System.Drawing.Color.Black
        Me.dgv2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.AppWorkspace
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.Padding = New System.Windows.Forms.Padding(0, 2, 0, 2)
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.dgv2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgv2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightCyan
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv2.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgv2.EnableHeadersVisualStyles = False
        Me.dgv2.Location = New System.Drawing.Point(0, 238)
        Me.dgv2.Name = "dgv2"
        Me.dgv2.ReadOnly = True
        Me.dgv2.RowHeadersVisible = False
        Me.dgv2.RowHeadersWidth = 30
        Me.dgv2.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgv2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgv2.Size = New System.Drawing.Size(804, 53)
        Me.dgv2.TabIndex = 29
        Me.dgv2.Tag = "12"
        Me.dgv2.Visible = False
        '
        'dgvTeam
        '
        Me.dgvTeam.AllowUserToAddRows = False
        Me.dgvTeam.AllowUserToDeleteRows = False
        Me.dgvTeam.AllowUserToResizeColumns = False
        Me.dgvTeam.AllowUserToResizeRows = False
        Me.dgvTeam.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvTeam.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvTeam.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvTeam.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTeam.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvTeam.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle5.Padding = New System.Windows.Forms.Padding(0, 2, 0, 2)
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTeam.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvTeam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTeam.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Rolle, Me.Team, Me.Gegner, Me.mReissen, Me.mStossen, Me.mHeben, Me.mPunkte, Me.Punkte2})
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.Padding = New System.Windows.Forms.Padding(2, 0, 2, 2)
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTeam.DefaultCellStyle = DataGridViewCellStyle13
        Me.dgvTeam.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvTeam.EnableHeadersVisualStyles = False
        Me.dgvTeam.Location = New System.Drawing.Point(160, 386)
        Me.dgvTeam.Name = "dgvTeam"
        Me.dgvTeam.ReadOnly = True
        Me.dgvTeam.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvTeam.RowHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvTeam.RowHeadersVisible = False
        Me.dgvTeam.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvTeam.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTeam.ShowCellToolTips = False
        Me.dgvTeam.ShowEditingIcon = False
        Me.dgvTeam.ShowRowErrors = False
        Me.dgvTeam.Size = New System.Drawing.Size(269, 42)
        Me.dgvTeam.TabIndex = 509
        Me.dgvTeam.TabStop = False
        Me.dgvTeam.Visible = False
        '
        'Rolle
        '
        Me.Rolle.DataPropertyName = "Rolle"
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Gold
        Me.Rolle.DefaultCellStyle = DataGridViewCellStyle6
        Me.Rolle.HeaderText = ""
        Me.Rolle.Name = "Rolle"
        Me.Rolle.ReadOnly = True
        Me.Rolle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Rolle.Visible = False
        Me.Rolle.Width = 5
        '
        'Team
        '
        Me.Team.DataPropertyName = "Kurzname"
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Gold
        Me.Team.DefaultCellStyle = DataGridViewCellStyle7
        Me.Team.HeaderText = ""
        Me.Team.Name = "Team"
        Me.Team.ReadOnly = True
        Me.Team.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Team.Width = 5
        '
        'Gegner
        '
        Me.Gegner.DataPropertyName = "Gegner"
        Me.Gegner.HeaderText = "Gegner"
        Me.Gegner.Name = "Gegner"
        Me.Gegner.ReadOnly = True
        Me.Gegner.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Gegner.Visible = False
        Me.Gegner.Width = 52
        '
        'mReissen
        '
        Me.mReissen.DataPropertyName = "Reissen"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mReissen.DefaultCellStyle = DataGridViewCellStyle8
        Me.mReissen.HeaderText = "Reißen"
        Me.mReissen.Name = "mReissen"
        Me.mReissen.ReadOnly = True
        Me.mReissen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mReissen.Width = 51
        '
        'mStossen
        '
        Me.mStossen.DataPropertyName = "Stossen"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mStossen.DefaultCellStyle = DataGridViewCellStyle9
        Me.mStossen.HeaderText = "Stoßen"
        Me.mStossen.Name = "mStossen"
        Me.mStossen.ReadOnly = True
        Me.mStossen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mStossen.Width = 50
        '
        'mHeben
        '
        Me.mHeben.DataPropertyName = "Heben"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mHeben.DefaultCellStyle = DataGridViewCellStyle10
        Me.mHeben.HeaderText = "Gesamt"
        Me.mHeben.Name = "mHeben"
        Me.mHeben.ReadOnly = True
        Me.mHeben.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mHeben.Width = 54
        '
        'mPunkte
        '
        Me.mPunkte.DataPropertyName = "Punkte"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.Format = "N0"
        Me.mPunkte.DefaultCellStyle = DataGridViewCellStyle11
        Me.mPunkte.HeaderText = "Pkt."
        Me.mPunkte.Name = "mPunkte"
        Me.mPunkte.ReadOnly = True
        Me.mPunkte.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mPunkte.Width = 31
        '
        'Punkte2
        '
        Me.Punkte2.DataPropertyName = "Punkte2"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.Format = "N0"
        Me.Punkte2.DefaultCellStyle = DataGridViewCellStyle12
        Me.Punkte2.HeaderText = "Pkt."
        Me.Punkte2.Name = "Punkte2"
        Me.Punkte2.ReadOnly = True
        Me.Punkte2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Punkte2.Visible = False
        Me.Punkte2.Width = 31
        '
        'pnlWertung
        '
        Me.pnlWertung.BackColor = System.Drawing.Color.Black
        Me.pnlWertung.Controls.Add(Me.xGültig3)
        Me.pnlWertung.Controls.Add(Me.xTechniknote)
        Me.pnlWertung.Controls.Add(Me.xGültig2)
        Me.pnlWertung.Controls.Add(Me.xGültig1)
        Me.pnlWertung.Location = New System.Drawing.Point(526, 407)
        Me.pnlWertung.MinimumSize = New System.Drawing.Size(134, 47)
        Me.pnlWertung.Name = "pnlWertung"
        Me.pnlWertung.Size = New System.Drawing.Size(185, 47)
        Me.pnlWertung.TabIndex = 514
        '
        'xGültig3
        '
        Me.xGültig3.BackColor = System.Drawing.Color.Black
        Me.xGültig3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig3.Font = New System.Drawing.Font("Wingdings", 34.0!)
        Me.xGültig3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig3.Location = New System.Drawing.Point(70, -1)
        Me.xGültig3.Name = "xGültig3"
        Me.xGültig3.ReadOnly = True
        Me.xGültig3.Size = New System.Drawing.Size(32, 51)
        Me.xGültig3.TabIndex = 17
        Me.xGültig3.Text = "l"
        Me.xGültig3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xTechniknote
        '
        Me.xTechniknote.BackColor = System.Drawing.Color.Black
        Me.xTechniknote.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xTechniknote.Font = New System.Drawing.Font("DS-Digital", 31.0!)
        Me.xTechniknote.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xTechniknote.Location = New System.Drawing.Point(109, 2)
        Me.xTechniknote.Name = "xTechniknote"
        Me.xTechniknote.ReadOnly = True
        Me.xTechniknote.Size = New System.Drawing.Size(72, 42)
        Me.xTechniknote.TabIndex = 22
        Me.xTechniknote.Text = "X,XX"
        Me.xTechniknote.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.xTechniknote.WordWrap = False
        '
        'xGültig2
        '
        Me.xGültig2.BackColor = System.Drawing.Color.Black
        Me.xGültig2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig2.Font = New System.Drawing.Font("Wingdings", 34.0!)
        Me.xGültig2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig2.Location = New System.Drawing.Point(38, -1)
        Me.xGültig2.Name = "xGültig2"
        Me.xGültig2.ReadOnly = True
        Me.xGültig2.Size = New System.Drawing.Size(32, 51)
        Me.xGültig2.TabIndex = 16
        Me.xGültig2.Text = "l"
        Me.xGültig2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xGültig1
        '
        Me.xGültig1.BackColor = System.Drawing.Color.Black
        Me.xGültig1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig1.Font = New System.Drawing.Font("Wingdings", 34.0!)
        Me.xGültig1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig1.Location = New System.Drawing.Point(6, -1)
        Me.xGültig1.Name = "xGültig1"
        Me.xGültig1.ReadOnly = True
        Me.xGültig1.Size = New System.Drawing.Size(32, 51)
        Me.xGültig1.TabIndex = 15
        Me.xGültig1.Text = "l"
        Me.xGültig1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xAufruf
        '
        Me.xAufruf.BackColor = System.Drawing.Color.Black
        Me.xAufruf.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!)
        Me.xAufruf.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf.Location = New System.Drawing.Point(662, 407)
        Me.xAufruf.Name = "xAufruf"
        Me.xAufruf.Size = New System.Drawing.Size(142, 46)
        Me.xAufruf.TabIndex = 515
        Me.xAufruf.Text = "0:00"
        Me.xAufruf.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmAufwärmen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(804, 454)
        Me.Controls.Add(Me.pnlWertung)
        Me.Controls.Add(Me.xAufruf)
        Me.Controls.Add(Me.dgvTeam)
        Me.Controls.Add(Me.dgv2)
        Me.Controls.Add(Me.lblCaption)
        Me.Controls.Add(Me.lblDurchgang)
        Me.Controls.Add(Me.lblGruppe)
        Me.Controls.Add(Me.dgvJoin)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(804, 454)
        Me.Name = "frmAufwärmen"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Versuchsreihenfolge"
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTeam, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlWertung.ResumeLayout(False)
        Me.pnlWertung.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvJoin As System.Windows.Forms.DataGridView
    Friend WithEvents lblCaption As Label
    Friend WithEvents lblGruppe As Label
    Friend WithEvents lblDurchgang As Label
    Friend WithEvents dgv2 As DataGridView
    Friend WithEvents dgvTeam As DataGridView
    Friend WithEvents pnlWertung As Panel
    Friend WithEvents xTechniknote As TextBox
    Friend WithEvents xGültig3 As TextBox
    Friend WithEvents xGültig2 As TextBox
    Friend WithEvents xGültig1 As TextBox
    Friend WithEvents xAufruf As Label
    Friend WithEvents Rolle As DataGridViewTextBoxColumn
    Friend WithEvents Team As DataGridViewTextBoxColumn
    Friend WithEvents Gegner As DataGridViewTextBoxColumn
    Friend WithEvents mReissen As DataGridViewTextBoxColumn
    Friend WithEvents mStossen As DataGridViewTextBoxColumn
    Friend WithEvents mHeben As DataGridViewTextBoxColumn
    Friend WithEvents mPunkte As DataGridViewTextBoxColumn
    Friend WithEvents Punkte2 As DataGridViewTextBoxColumn
End Class
