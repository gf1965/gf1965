﻿Public Class frmAdopt

    Dim Func As New myFunction

    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim da As MySqlDataAdapter
    Dim cb As MySqlCommandBuilder
    Dim ds As DataSet

    Dim dv As DataView
    Dim dt As DataTable

    Private WithEvents bs As BindingSource

    Dim Wettkampf_Sel As Integer = 0

    Private Sub frmAdopt_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If btnSave.Enabled Then
            Using New Centered_MessageBox(Me)
                Dim result As DialogResult = MessageBox.Show("Ergebnisse der ausgewählten Heber übernehmen?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                If result = DialogResult.Yes Then
                    btnSave.PerformClick()
                ElseIf result = DialogResult.Cancel Then
                    e.Cancel = True
                    Return
                End If
            End Using
        End If
    End Sub
    Private Sub frmAdopt_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
    End Sub
    Private Sub frmAdopt_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        If String.IsNullOrEmpty(User.ConnString) Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Anmeldung an MySQL-Server nicht nöglich." & vbNewLine & "(Keine Anmeldedaten vorhanden.)", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            Close()
            Return
        End If

        Using conn As New MySqlConnection(User.ConnString)
            If Not conn.State = ConnectionState.Open Then conn.Open()

            Dim Query = "SELECT m.Teilnehmer, m.Gruppe, a.Nachname, a.Vorname, a.Geschlecht, Year(a.Geburtstag) Jahrgang, m.idGK, m.GK, v.Vereinsname " &
                        "FROM meldung m " &
                        "LEFT JOIN athleten a ON a.idTeilnehmer = m.Teilnehmer " &
                        "LEFT JOIN verein v ON v.idVerein = a.Verein " &
                        "WHERE m.Wettkampf = " & Wettkampf.ID & " AND m.attend = True " &
                        "ORDER BY m.Gruppe, a.Nachname, a.Vorname;"
            cmd = New MySqlCommand(Query, conn)
            dt = New DataTable
            dt.Load(cmd.ExecuteReader)
        End Using

        'dt.Columns.Add(New DataColumn With {.ColumnName = "Check", .DataType = GetType(Boolean), .DefaultValue = False})
        dv = New DataView(dt)
        bs = New BindingSource With {.DataSource = dv}
        With dgvJoin
            .AutoGenerateColumns = False
            .DataSource = bs
        End With

        Cursor = Cursors.Default
        btnSave.Enabled = False
    End Sub

    Private Sub dgvJoin_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvJoin.CellClick
        'If e.RowIndex > -1 And dgvJoin.Columns(e.ColumnIndex).Name = "Check" Then
        '    dgvJoin.CurrentCell = dgvJoin(e.ColumnIndex, e.RowIndex)
        '    If IsDBNull(dgvJoin.CurrentCell.Value) OrElse CBool(dgvJoin.CurrentCell.Value) = True Then
        '        dgvJoin.CurrentCell.Value = False
        '    ElseIf CBool(dgvJoin.CurrentCell.Value) = False Then
        '        dgvJoin.CurrentCell.Value = True
        '    End If
        'End If
    End Sub
    Private Sub dgvJoin_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvJoin.CellEndEdit
        btnSave.Enabled = True
    End Sub
    Private Sub dgvJoin_ColumnHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvJoin.ColumnHeaderMouseClick
        If e.Button = MouseButtons.Right Then
            ' Filter einbauen
        End If
    End Sub
    Private Sub dgvJoin_ColumnHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvJoin.ColumnHeaderMouseDoubleClick
        'If e.Button = MouseButtons.Left AndAlso e.ColumnIndex = 0 Then
        '    Dim Checked = (From x In dt
        '                   Where Not x.Field(Of Boolean?)("Check") = False).Count > 0

        '    For Each row As DataRowView In dv
        '        row!Check = Not Checked
        '    Next
        '    bs.EndEdit()
        '    btnSave.Enabled = True
        'End If
    End Sub
    Private Sub dgvJoin_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvJoin.DataError
        Try
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvJoin_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvJoin.RowEnter
        If e.RowIndex < 0 OrElse e.ColumnIndex < 0 Then Return
        With dgvJoin
            If .Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Red Then
                .DefaultCellStyle.SelectionForeColor = Color.Pink
            ElseIf .Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.blue Then
                .DefaultCellStyle.SelectionForeColor = Color.PowderBlue
            Else
                .DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.HighlightText)
            End If
        End With
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Dim table As New List(Of String)
        table.AddRange({"meldung", "results", "reissen", "stossen", "athletik", "athletik_results"})
        Dim tmp As DataTable = Nothing
        Dim Query = String.Empty

        For i = 0 To dv.Count - 1

            Dim result = DialogResult.Yes

            'If IsDBNull(dv(i)("Check")) OrElse CBool(dv(i)("Check")) Then
            If CBool(dgvJoin.Rows(i).Cells("Check").Value) = True Then

                If dgvJoin.Rows(i).DefaultCellStyle.ForeColor = Color.Red Then
                    Using New Centered_MessageBox(Me)
                        Dim msg = "Für " & dv(i)("Nachname").ToString & ", " & dv(i)("Vorname").ToString & " sind bereits Einträge vorhanden. " & vbNewLine &
                                  "Ergebnisse trotzdem in " & lblWK.Text & " übernehmen?"
                        result = MessageBox.Show(msg, msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    End Using
                End If

                If result = DialogResult.Yes Then

                    If String.IsNullOrEmpty(User.ConnString) Then
                        Using New Centered_MessageBox(Me)
                            MessageBox.Show("Anmeldung an MySQL-Server nicht nöglich." & vbNewLine & "(Keine Anmeldedaten vorhanden.)", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End Using
                        Close()
                        Return
                    End If

                    Using conn As New MySqlConnection(User.ConnString)
                        Try
                            If Not conn.State = ConnectionState.Open Then conn.Open()

                            For s = 0 To table.Count - 1
                                ' Source-Rows
                                Query = "SELECT * FROM " & table(s) & " WHERE Wettkampf = @wk1 AND Teilnehmer = @tn;"
                                cmd = New MySqlCommand(Query, conn)
                                With cmd.Parameters
                                    .AddWithValue("@wk0", Wettkampf.ID)
                                    .AddWithValue("@wk1", Wettkampf_Sel)
                                    .AddWithValue("@tn", dv(i)("Teilnehmer"))
                                End With
                                tmp = New DataTable
                                tmp.Load(cmd.ExecuteReader)
                                ' Target-Rows
                                Query = "DELETE FROM " & table(s) & " WHERE Wettkampf = @wk0 AND Teilnehmer = @tn;"
                                cmd.CommandText = Query
                                cmd.ExecuteNonQuery()
                                ' Insert Source into Target
                                If tmp.Rows.Count > 0 Then
                                    Query = "INSERT INTO " & table(s) & " ("
                                    For Each col As DataColumn In tmp.Columns
                                        Query += col.ColumnName + ", "
                                    Next
                                    Query = Strings.Left(Query, Query.Length - 2) + ") VALUES("
                                    For z = 1 To tmp.Columns.Count
                                        Query += "@p" & z & ", "
                                    Next
                                    Query = Strings.Left(Query, Query.Length - 2) + ");"

                                    cmd = New MySqlCommand(Query, conn)
                                    For Each row As DataRow In tmp.Rows
                                        With cmd.Parameters
                                            .AddWithValue("@p1", Wettkampf.ID)
                                            For z = 1 To tmp.Columns.Count - 1
                                                .AddWithValue("@p" & z, row(tmp.Columns(z).ColumnName))
                                            Next
                                        End With
                                        cmd.ExecuteNonQuery()
                                    Next
                                End If
                                ' End Insert
                            Next
                            ConnError = False
                        Catch ex As MySqlException
                            If MySQl_Error(Me, ex.Message) = DialogResult.Cancel Then
                                Cursor = Cursors.Default
                                Return
                            End If
                        End Try
                    End Using

                End If
            End If
        Next
    End Sub
    'Private Sub btnSelect_Click(sender As Object, e As EventArgs) Handles btnSelect.Click

    '    Dim result = Func.Wettkampf_Aufruf(Me,, True)
    '    If IsNothing(result) Then Return

    '    Cursor = Cursors.WaitCursor

    '    If Not Wettkampf_Sel = 0 OrElse Not Wettkampf_Sel = result.Key Then
    '        ' dgvJoin zurücksetzen
    '        With dgvJoin
    '            For i = 0 To .Rows.Count - 1
    '                .Rows(i).Cells("Check").Value = False
    '                .Rows(i).DefaultCellStyle.ForeColor = If(i Mod 2 = 0, .DefaultCellStyle.ForeColor, .AlternatingRowsDefaultCellStyle.ForeColor)
    '            Next
    '            .DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.HighlightText)
    '        End With
    '        bs.MoveFirst()
    '    End If

    '    Wettkampf_Sel = result.Key
    '    lblWK.Text = result.Value

    '    Dim tmp As New DataTable

    '    Using conn As New MySqlConnection(User.ConnString)
    '        Try
    '            If Not conn.State = ConnectionState.Open Then conn.Open()
    '            Dim Query = "SELECT m.Teilnehmer, r.Heben, r.Athletik " &
    '                        "FROM meldung m " &
    '                        "LEFT JOIN results r ON r.Wettkampf = m.Wettkampf And r.Teilnehmer = m.Teilnehmer " &
    '                        "WHERE m.Wettkampf = " & Wettkampf_Sel &
    '                        " ORDER BY m.Teilnehmer;"
    '            cmd = New MySqlCommand(Query, conn)
    '            tmp.Load(cmd.ExecuteReader())
    '            ConnError = False
    '        Catch ex As MySqlException
    '            If MySQl_Error(Me, ex.Message) = DialogResult.Cancel Then
    '                Cursor = Cursors.Default
    '                Return
    '            End If
    '        End Try
    '    End Using

    '    For Each row As DataRow In tmp.Rows
    '        Dim pos = bs.Find("Teilnehmer", row!Teilnehmer)
    '        If pos > -1 Then
    '            If Not IsDBNull(row!Heben) OrElse Not IsDBNull(row!Athletik) Then
    '                dgvJoin.Rows(pos).Cells("Check").Value = DBNull.Value
    '                dgvJoin.Rows(pos).DefaultCellStyle.ForeColor = Color.Red
    '            Else
    '                dgvJoin.Rows(pos).Cells("Check").Value = True
    '                dgvJoin.Rows(pos).DefaultCellStyle.ForeColor = Color.Blue
    '            End If
    '        End If
    '    Next

    '    Cursor = Cursors.Default
    'End Sub

End Class