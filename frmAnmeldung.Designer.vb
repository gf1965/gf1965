﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAnmeldung
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAnmeldung))
        Me.grpServer = New System.Windows.Forms.GroupBox()
        Me.cboDatabase = New System.Windows.Forms.ComboBox()
        Me.pnlServer = New System.Windows.Forms.Panel()
        Me.txtServer = New System.Windows.Forms.TextBox()
        Me.txtPort = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkPasswort = New System.Windows.Forms.CheckBox()
        Me.btnSaveLogin = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.grpUser = New System.Windows.Forms.GroupBox()
        Me.pnlTCP = New System.Windows.Forms.Panel()
        Me.txtTCP_Ip = New System.Windows.Forms.TextBox()
        Me.txtTCP_Port = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblBohle = New System.Windows.Forms.Label()
        Me.cboBohle = New System.Windows.Forms.ComboBox()
        Me.lblUser = New System.Windows.Forms.Label()
        Me.chkUserPW = New System.Windows.Forms.CheckBox()
        Me.lblUserPW = New System.Windows.Forms.Label()
        Me.txtUserPassword = New System.Windows.Forms.TextBox()
        Me.cboUser = New System.Windows.Forms.ComboBox()
        Me.btnAnmelden = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnServer = New System.Windows.Forms.Button()
        Me.btnConnTest = New System.Windows.Forms.Button()
        Me.btnIP = New System.Windows.Forms.Button()
        Me.btnDevices = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlIPs = New System.Windows.Forms.Panel()
        Me.dgvNetwork = New System.Windows.Forms.DataGridView()
        Me.Selected = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Netzwerk = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Typ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IPv4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmnuNetwork = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmnuAuswählen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmnuKopieren = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.btnExittt = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.pnlButtons = New System.Windows.Forms.Panel()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.dgvServer = New System.Windows.Forms.DataGridView()
        Me.IP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Device = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Network = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetTyp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.grpServer.SuspendLayout()
        Me.grpUser.SuspendLayout()
        Me.pnlIPs.SuspendLayout()
        CType(Me.dgvNetwork, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmnuNetwork.SuspendLayout()
        Me.pnlButtons.SuspendLayout()
        CType(Me.dgvServer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grpServer
        '
        Me.grpServer.Controls.Add(Me.cboDatabase)
        Me.grpServer.Controls.Add(Me.pnlServer)
        Me.grpServer.Controls.Add(Me.txtServer)
        Me.grpServer.Controls.Add(Me.txtPort)
        Me.grpServer.Controls.Add(Me.Label1)
        Me.grpServer.Controls.Add(Me.chkPasswort)
        Me.grpServer.Controls.Add(Me.btnSaveLogin)
        Me.grpServer.Controls.Add(Me.Label5)
        Me.grpServer.Controls.Add(Me.txtPassword)
        Me.grpServer.Controls.Add(Me.Label4)
        Me.grpServer.Controls.Add(Me.txtUser)
        Me.grpServer.Controls.Add(Me.Label3)
        Me.grpServer.Controls.Add(Me.Label2)
        Me.grpServer.Location = New System.Drawing.Point(24, 181)
        Me.grpServer.Name = "grpServer"
        Me.grpServer.Size = New System.Drawing.Size(264, 197)
        Me.grpServer.TabIndex = 100
        Me.grpServer.TabStop = False
        Me.grpServer.Text = "&Datenbank-Anmeldung"
        '
        'cboDatabase
        '
        Me.cboDatabase.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboDatabase.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboDatabase.FormattingEnabled = True
        Me.cboDatabase.Location = New System.Drawing.Point(69, 52)
        Me.cboDatabase.Name = "cboDatabase"
        Me.cboDatabase.Size = New System.Drawing.Size(179, 21)
        Me.cboDatabase.TabIndex = 107
        Me.cboDatabase.Tag = "weightlift"
        '
        'pnlServer
        '
        Me.pnlServer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pnlServer.Location = New System.Drawing.Point(161, 26)
        Me.pnlServer.Name = "pnlServer"
        Me.pnlServer.Size = New System.Drawing.Size(17, 20)
        Me.pnlServer.TabIndex = 103
        '
        'txtServer
        '
        Me.txtServer.Location = New System.Drawing.Point(69, 26)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(93, 20)
        Me.txtServer.TabIndex = 102
        Me.txtServer.Tag = "localhost"
        '
        'txtPort
        '
        Me.txtPort.AccessibleName = ""
        Me.txtPort.Location = New System.Drawing.Point(216, 26)
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(32, 20)
        Me.txtPort.TabIndex = 105
        Me.txtPort.Tag = "3306"
        Me.txtPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(188, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(26, 13)
        Me.Label1.TabIndex = 104
        Me.Label1.Text = "&Port"
        '
        'chkPasswort
        '
        Me.chkPasswort.AutoSize = True
        Me.chkPasswort.Location = New System.Drawing.Point(71, 133)
        Me.chkPasswort.Name = "chkPasswort"
        Me.chkPasswort.Size = New System.Drawing.Size(109, 17)
        Me.chkPasswort.TabIndex = 112
        Me.chkPasswort.TabStop = False
        Me.chkPasswort.Text = "Passwort sichtbar"
        Me.chkPasswort.UseVisualStyleBackColor = True
        '
        'btnSaveLogin
        '
        Me.btnSaveLogin.Location = New System.Drawing.Point(69, 156)
        Me.btnSaveLogin.Name = "btnSaveLogin"
        Me.btnSaveLogin.Size = New System.Drawing.Size(179, 25)
        Me.btnSaveLogin.TabIndex = 113
        Me.btnSaveLogin.TabStop = False
        Me.btnSaveLogin.Text = "Anmeldedaten speichern"
        Me.btnSaveLogin.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 13)
        Me.Label5.TabIndex = 106
        Me.Label5.Text = "&Datenbank"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(69, 105)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(179, 20)
        Me.txtPassword.TabIndex = 111
        Me.txtPassword.Tag = "Admin"
        Me.txtPassword.UseSystemPasswordChar = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 107)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 13)
        Me.Label4.TabIndex = 110
        Me.Label4.Text = "Pass&wort"
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(69, 79)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(179, 20)
        Me.txtUser.TabIndex = 109
        Me.txtUser.Tag = "Admin"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 108
        Me.Label3.Text = "&Benutzer"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 101
        Me.Label2.Text = "IP-&Adresse"
        '
        'grpUser
        '
        Me.grpUser.Controls.Add(Me.pnlTCP)
        Me.grpUser.Controls.Add(Me.txtTCP_Ip)
        Me.grpUser.Controls.Add(Me.txtTCP_Port)
        Me.grpUser.Controls.Add(Me.Label7)
        Me.grpUser.Controls.Add(Me.Label8)
        Me.grpUser.Controls.Add(Me.lblBohle)
        Me.grpUser.Controls.Add(Me.cboBohle)
        Me.grpUser.Controls.Add(Me.lblUser)
        Me.grpUser.Controls.Add(Me.chkUserPW)
        Me.grpUser.Controls.Add(Me.lblUserPW)
        Me.grpUser.Controls.Add(Me.txtUserPassword)
        Me.grpUser.Controls.Add(Me.cboUser)
        Me.grpUser.Location = New System.Drawing.Point(24, 18)
        Me.grpUser.Name = "grpUser"
        Me.grpUser.Size = New System.Drawing.Size(264, 145)
        Me.grpUser.TabIndex = 0
        Me.grpUser.TabStop = False
        Me.grpUser.Text = "Benutzer-&Anmeldung"
        '
        'pnlTCP
        '
        Me.pnlTCP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pnlTCP.Location = New System.Drawing.Point(161, 109)
        Me.pnlTCP.Name = "pnlTCP"
        Me.pnlTCP.Size = New System.Drawing.Size(17, 20)
        Me.pnlTCP.TabIndex = 10
        '
        'txtTCP_Ip
        '
        Me.txtTCP_Ip.Location = New System.Drawing.Point(69, 109)
        Me.txtTCP_Ip.Name = "txtTCP_Ip"
        Me.txtTCP_Ip.Size = New System.Drawing.Size(93, 20)
        Me.txtTCP_Ip.TabIndex = 9
        Me.txtTCP_Ip.Tag = ""
        Me.ToolTip1.SetToolTip(Me.txtTCP_Ip, "IP-Adresse des Computers, " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "dessen Datenbank genutzt werden soll")
        '
        'txtTCP_Port
        '
        Me.txtTCP_Port.AccessibleName = ""
        Me.txtTCP_Port.Location = New System.Drawing.Point(216, 109)
        Me.txtTCP_Port.Name = "txtTCP_Port"
        Me.txtTCP_Port.Size = New System.Drawing.Size(32, 20)
        Me.txtTCP_Port.TabIndex = 12
        Me.txtTCP_Port.Tag = ""
        Me.txtTCP_Port.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip1.SetToolTip(Me.txtTCP_Port, "Port des MySQL-Servers" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Standard = 8000")
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(188, 111)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(26, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "&Port"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(9, 111)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "IP-&Adresse"
        '
        'lblBohle
        '
        Me.lblBohle.AutoSize = True
        Me.lblBohle.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblBohle.Location = New System.Drawing.Point(16, 73)
        Me.lblBohle.Name = "lblBohle"
        Me.lblBohle.Size = New System.Drawing.Size(34, 13)
        Me.lblBohle.TabIndex = 5
        Me.lblBohle.Text = "&Bohle"
        Me.lblBohle.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboBohle
        '
        Me.cboBohle.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBohle.BackColor = System.Drawing.SystemColors.Window
        Me.cboBohle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBohle.FormattingEnabled = True
        Me.cboBohle.Items.AddRange(New Object() {" 1", " 2"})
        Me.cboBohle.Location = New System.Drawing.Point(55, 71)
        Me.cboBohle.Name = "cboBohle"
        Me.cboBohle.Size = New System.Drawing.Size(75, 21)
        Me.cboBohle.TabIndex = 6
        Me.cboBohle.Tag = "Server:Bohle"
        '
        'lblUser
        '
        Me.lblUser.AutoSize = True
        Me.lblUser.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblUser.Location = New System.Drawing.Point(16, 23)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(49, 13)
        Me.lblUser.TabIndex = 1
        Me.lblUser.Text = "Ben&utzer"
        Me.lblUser.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'chkUserPW
        '
        Me.chkUserPW.AutoSize = True
        Me.chkUserPW.Location = New System.Drawing.Point(143, 72)
        Me.chkUserPW.Name = "chkUserPW"
        Me.chkUserPW.Size = New System.Drawing.Size(109, 17)
        Me.chkUserPW.TabIndex = 7
        Me.chkUserPW.TabStop = False
        Me.chkUserPW.Text = "Passwort sichtbar"
        Me.chkUserPW.UseVisualStyleBackColor = True
        '
        'lblUserPW
        '
        Me.lblUserPW.AutoSize = True
        Me.lblUserPW.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblUserPW.Location = New System.Drawing.Point(142, 23)
        Me.lblUserPW.Name = "lblUserPW"
        Me.lblUserPW.Size = New System.Drawing.Size(50, 13)
        Me.lblUserPW.TabIndex = 3
        Me.lblUserPW.Text = "&Passwort"
        '
        'txtUserPassword
        '
        Me.txtUserPassword.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(212, Byte), Integer), CType(CType(212, Byte), Integer))
        Me.txtUserPassword.Location = New System.Drawing.Point(141, 42)
        Me.txtUserPassword.Name = "txtUserPassword"
        Me.txtUserPassword.Size = New System.Drawing.Size(107, 20)
        Me.txtUserPassword.TabIndex = 4
        Me.txtUserPassword.Tag = "Anmeldung:UserPW"
        Me.txtUserPassword.UseSystemPasswordChar = True
        '
        'cboUser
        '
        Me.cboUser.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboUser.BackColor = System.Drawing.SystemColors.Window
        Me.cboUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUser.DropDownWidth = 90
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(16, 41)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(114, 21)
        Me.cboUser.TabIndex = 2
        Me.cboUser.Tag = "Server:User"
        '
        'btnAnmelden
        '
        Me.btnAnmelden.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnAnmelden.Enabled = False
        Me.btnAnmelden.Location = New System.Drawing.Point(5, 5)
        Me.btnAnmelden.Name = "btnAnmelden"
        Me.btnAnmelden.Size = New System.Drawing.Size(121, 25)
        Me.btnAnmelden.TabIndex = 201
        Me.btnAnmelden.TabStop = False
        Me.btnAnmelden.Text = "Anmelden"
        Me.btnAnmelden.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(5, 36)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(121, 25)
        Me.btnCancel.TabIndex = 202
        Me.btnCancel.TabStop = False
        Me.btnCancel.Text = "Abbrechen"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnServer
        '
        Me.btnServer.ForeColor = System.Drawing.Color.Firebrick
        Me.btnServer.Location = New System.Drawing.Point(5, 119)
        Me.btnServer.Name = "btnServer"
        Me.btnServer.Size = New System.Drawing.Size(121, 25)
        Me.btnServer.TabIndex = 204
        Me.btnServer.TabStop = False
        Me.btnServer.Text = "Datenbank"
        Me.ToolTip1.SetToolTip(Me.btnServer, "Datenbank-Anmeldung einblenden")
        Me.btnServer.UseVisualStyleBackColor = True
        '
        'btnConnTest
        '
        Me.btnConnTest.Location = New System.Drawing.Point(5, 187)
        Me.btnConnTest.Name = "btnConnTest"
        Me.btnConnTest.Size = New System.Drawing.Size(121, 25)
        Me.btnConnTest.TabIndex = 205
        Me.btnConnTest.TabStop = False
        Me.btnConnTest.Text = "Verbindung testen"
        Me.ToolTip1.SetToolTip(Me.btnConnTest, "Verbindung zur Wettkampf-Datenbank testen")
        Me.btnConnTest.UseVisualStyleBackColor = True
        '
        'btnIP
        '
        Me.btnIP.Location = New System.Drawing.Point(5, 88)
        Me.btnIP.Name = "btnIP"
        Me.btnIP.Size = New System.Drawing.Size(121, 25)
        Me.btnIP.TabIndex = 203
        Me.btnIP.TabStop = False
        Me.btnIP.Text = "IP-Adresse"
        Me.ToolTip1.SetToolTip(Me.btnIP, "IP-Adresse(n) dieses Computers")
        Me.btnIP.UseVisualStyleBackColor = True
        '
        'btnDevices
        '
        Me.btnDevices.Location = New System.Drawing.Point(5, 218)
        Me.btnDevices.Name = "btnDevices"
        Me.btnDevices.Size = New System.Drawing.Size(121, 25)
        Me.btnDevices.TabIndex = 206
        Me.btnDevices.TabStop = False
        Me.btnDevices.Text = "IPs aktualisieren"
        Me.ToolTip1.SetToolTip(Me.btnDevices, "Liste der IP-Adressen im Netzwerk aktualisieren" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(bitte etwas Geduld)")
        Me.btnDevices.UseVisualStyleBackColor = True
        '
        'pnlIPs
        '
        Me.pnlIPs.BackColor = System.Drawing.Color.AliceBlue
        Me.pnlIPs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlIPs.Controls.Add(Me.dgvNetwork)
        Me.pnlIPs.Controls.Add(Me.lblMessage)
        Me.pnlIPs.Controls.Add(Me.btnExittt)
        Me.pnlIPs.Controls.Add(Me.Label6)
        Me.pnlIPs.Location = New System.Drawing.Point(412, 12)
        Me.pnlIPs.MaximumSize = New System.Drawing.Size(305, 124)
        Me.pnlIPs.MinimumSize = New System.Drawing.Size(305, 87)
        Me.pnlIPs.Name = "pnlIPs"
        Me.pnlIPs.Size = New System.Drawing.Size(305, 124)
        Me.pnlIPs.TabIndex = 1000
        Me.pnlIPs.Visible = False
        '
        'dgvNetwork
        '
        Me.dgvNetwork.AllowUserToAddRows = False
        Me.dgvNetwork.AllowUserToDeleteRows = False
        Me.dgvNetwork.AllowUserToOrderColumns = True
        Me.dgvNetwork.AllowUserToResizeColumns = False
        Me.dgvNetwork.AllowUserToResizeRows = False
        Me.dgvNetwork.BackgroundColor = System.Drawing.Color.AliceBlue
        Me.dgvNetwork.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvNetwork.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvNetwork.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvNetwork.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvNetwork.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Selected, Me.Netzwerk, Me.Typ, Me.IPv4})
        Me.dgvNetwork.ContextMenuStrip = Me.cmnuNetwork
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.AliceBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvNetwork.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvNetwork.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvNetwork.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvNetwork.Location = New System.Drawing.Point(0, 58)
        Me.dgvNetwork.MultiSelect = False
        Me.dgvNetwork.Name = "dgvNetwork"
        Me.dgvNetwork.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvNetwork.RowHeadersVisible = False
        Me.dgvNetwork.RowHeadersWidth = 82
        Me.dgvNetwork.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvNetwork.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNetwork.Size = New System.Drawing.Size(303, 66)
        Me.dgvNetwork.TabIndex = 0
        '
        'Selected
        '
        Me.Selected.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Selected.FalseValue = ""
        Me.Selected.HeaderText = "..."
        Me.Selected.MinimumWidth = 10
        Me.Selected.Name = "Selected"
        Me.Selected.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Selected.ToolTipText = "Auswahl übernehmen"
        Me.Selected.TrueValue = ""
        Me.Selected.Visible = False
        Me.Selected.Width = 24
        '
        'Netzwerk
        '
        Me.Netzwerk.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Netzwerk.DataPropertyName = "SSID"
        Me.Netzwerk.HeaderText = "Netzwerk-Name"
        Me.Netzwerk.MinimumWidth = 10
        Me.Netzwerk.Name = "Netzwerk"
        Me.Netzwerk.ReadOnly = True
        Me.Netzwerk.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Netzwerk.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Typ
        '
        Me.Typ.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Typ.DataPropertyName = "Typ"
        Me.Typ.HeaderText = "Typ"
        Me.Typ.MinimumWidth = 10
        Me.Typ.Name = "Typ"
        Me.Typ.ReadOnly = True
        Me.Typ.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Typ.Width = 60
        '
        'IPv4
        '
        Me.IPv4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.IPv4.DataPropertyName = "IP"
        Me.IPv4.HeaderText = "IP-Adresse"
        Me.IPv4.MinimumWidth = 10
        Me.IPv4.Name = "IPv4"
        Me.IPv4.ReadOnly = True
        Me.IPv4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.IPv4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IPv4.Width = 125
        '
        'cmnuNetwork
        '
        Me.cmnuNetwork.BackColor = System.Drawing.SystemColors.Info
        Me.cmnuNetwork.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.cmnuNetwork.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmnuAuswählen, Me.ToolStripMenuItem1, Me.cmnuKopieren})
        Me.cmnuNetwork.Name = "cmnuNetwork"
        Me.cmnuNetwork.ShowImageMargin = False
        Me.cmnuNetwork.Size = New System.Drawing.Size(108, 50)
        '
        'cmnuAuswählen
        '
        Me.cmnuAuswählen.Name = "cmnuAuswählen"
        Me.cmnuAuswählen.Padding = New System.Windows.Forms.Padding(0)
        Me.cmnuAuswählen.Size = New System.Drawing.Size(107, 20)
        Me.cmnuAuswählen.Text = "&Auswählen"
        Me.cmnuAuswählen.ToolTipText = "Netzwerk auswählen"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(104, 6)
        '
        'cmnuKopieren
        '
        Me.cmnuKopieren.Name = "cmnuKopieren"
        Me.cmnuKopieren.Padding = New System.Windows.Forms.Padding(0)
        Me.cmnuKopieren.Size = New System.Drawing.Size(107, 20)
        Me.cmnuKopieren.Text = "&Kopieren"
        Me.cmnuKopieren.ToolTipText = "IP-Adresse kopieren"
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.SystemColors.Window
        Me.lblMessage.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblMessage.ForeColor = System.Drawing.Color.Navy
        Me.lblMessage.Image = Global.Gewichtheben.My.Resources.Resources.Exclamation24
        Me.lblMessage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblMessage.Location = New System.Drawing.Point(0, 23)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Padding = New System.Windows.Forms.Padding(15, 0, 0, 0)
        Me.lblMessage.Size = New System.Drawing.Size(303, 35)
        Me.lblMessage.TabIndex = 3
        Me.lblMessage.Text = "             Mehrere Netzwerk-Adapter vorhanden." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "             Welcher soll genut" &
    "zt werden?"
        Me.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblMessage.Visible = False
        '
        'btnExittt
        '
        Me.btnExittt.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExittt.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnExittt.FlatAppearance.BorderSize = 0
        Me.btnExittt.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnExittt.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnExittt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExittt.Font = New System.Drawing.Font("Wingdings 2", 14.0!)
        Me.btnExittt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnExittt.Location = New System.Drawing.Point(279, -2)
        Me.btnExittt.Name = "btnExittt"
        Me.btnExittt.Size = New System.Drawing.Size(26, 25)
        Me.btnExittt.TabIndex = 2
        Me.btnExittt.TabStop = False
        Me.btnExittt.Text = ""
        Me.btnExittt.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.btnExittt.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(0, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Padding = New System.Windows.Forms.Padding(3, 0, 0, 1)
        Me.Label6.Size = New System.Drawing.Size(303, 23)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "IP-Adresse"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlButtons
        '
        Me.pnlButtons.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlButtons.Controls.Add(Me.btnDevices)
        Me.pnlButtons.Controls.Add(Me.btnIP)
        Me.pnlButtons.Controls.Add(Me.btnConnTest)
        Me.pnlButtons.Controls.Add(Me.btnServer)
        Me.pnlButtons.Controls.Add(Me.btnCancel)
        Me.pnlButtons.Controls.Add(Me.btnAnmelden)
        Me.pnlButtons.Location = New System.Drawing.Point(305, 18)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(131, 248)
        Me.pnlButtons.TabIndex = 200
        '
        'lblStatus
        '
        Me.lblStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblStatus.ForeColor = System.Drawing.Color.DarkGray
        Me.lblStatus.Location = New System.Drawing.Point(305, 272)
        Me.lblStatus.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(131, 35)
        Me.lblStatus.TabIndex = 206
        Me.lblStatus.Text = "keine IP-Adressen"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'dgvServer
        '
        Me.dgvServer.AllowUserToAddRows = False
        Me.dgvServer.AllowUserToDeleteRows = False
        Me.dgvServer.AllowUserToResizeColumns = False
        Me.dgvServer.AllowUserToResizeRows = False
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dgvServer.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvServer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvServer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvServer.ColumnHeadersVisible = False
        Me.dgvServer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IP, Me.Device, Me.Network, Me.NetTyp})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.Padding = New System.Windows.Forms.Padding(0, 0, 0, 1)
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvServer.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgvServer.Location = New System.Drawing.Point(308, 294)
        Me.dgvServer.MultiSelect = False
        Me.dgvServer.Name = "dgvServer"
        Me.dgvServer.ReadOnly = True
        Me.dgvServer.RowHeadersVisible = False
        Me.dgvServer.RowHeadersWidth = 82
        Me.dgvServer.RowTemplate.Height = 17
        Me.dgvServer.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvServer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvServer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvServer.Size = New System.Drawing.Size(206, 84)
        Me.dgvServer.TabIndex = 211
        Me.dgvServer.Visible = False
        '
        'IP
        '
        Me.IP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.IP.DataPropertyName = "IP"
        DataGridViewCellStyle4.Padding = New System.Windows.Forms.Padding(2, 0, 0, 0)
        Me.IP.DefaultCellStyle = DataGridViewCellStyle4
        Me.IP.DividerWidth = 1
        Me.IP.HeaderText = "000.000.000.000"
        Me.IP.MinimumWidth = 10
        Me.IP.Name = "IP"
        Me.IP.ReadOnly = True
        Me.IP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IP.Width = 92
        '
        'Device
        '
        Me.Device.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Device.DataPropertyName = "Device"
        DataGridViewCellStyle5.Padding = New System.Windows.Forms.Padding(2, 0, 0, 0)
        Me.Device.DefaultCellStyle = DataGridViewCellStyle5
        Me.Device.DividerWidth = 1
        Me.Device.HeaderText = "Device"
        Me.Device.MinimumWidth = 10
        Me.Device.Name = "Device"
        Me.Device.ReadOnly = True
        Me.Device.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Network
        '
        Me.Network.DataPropertyName = "Name"
        DataGridViewCellStyle6.Padding = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.Network.DefaultCellStyle = DataGridViewCellStyle6
        Me.Network.DividerWidth = 1
        Me.Network.HeaderText = "Network"
        Me.Network.MinimumWidth = 10
        Me.Network.Name = "Network"
        Me.Network.ReadOnly = True
        Me.Network.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Network.Visible = False
        Me.Network.Width = 10
        '
        'NetTyp
        '
        Me.NetTyp.DataPropertyName = "Typ"
        DataGridViewCellStyle7.Padding = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.NetTyp.DefaultCellStyle = DataGridViewCellStyle7
        Me.NetTyp.HeaderText = "Typ"
        Me.NetTyp.MinimumWidth = 10
        Me.NetTyp.Name = "NetTyp"
        Me.NetTyp.ReadOnly = True
        Me.NetTyp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.NetTyp.Visible = False
        Me.NetTyp.Width = 10
        '
        'Timer1
        '
        Me.Timer1.Interval = 350
        '
        'frmAnmeldung
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(448, 386)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.dgvServer)
        Me.Controls.Add(Me.pnlIPs)
        Me.Controls.Add(Me.pnlButtons)
        Me.Controls.Add(Me.grpUser)
        Me.Controls.Add(Me.grpServer)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(795, 444)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(468, 222)
        Me.Name = "frmAnmeldung"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GFHsoft Gewichtheben - Login"
        Me.grpServer.ResumeLayout(False)
        Me.grpServer.PerformLayout()
        Me.grpUser.ResumeLayout(False)
        Me.grpUser.PerformLayout()
        Me.pnlIPs.ResumeLayout(False)
        CType(Me.dgvNetwork, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmnuNetwork.ResumeLayout(False)
        Me.pnlButtons.ResumeLayout(False)
        CType(Me.dgvServer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents grpServer As GroupBox
    Friend WithEvents chkPasswort As CheckBox
    Friend WithEvents btnSaveLogin As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtUser As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents grpUser As GroupBox
    Friend WithEvents cboUser As ComboBox
    Friend WithEvents btnAnmelden As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents txtUserPassword As TextBox
    Friend WithEvents lblUserPW As Label
    Friend WithEvents chkUserPW As CheckBox
    Friend WithEvents btnServer As Button
    Friend WithEvents btnConnTest As Button
    Friend WithEvents lblUser As Label
    Friend WithEvents lblBohle As Label
    Friend WithEvents cboBohle As ComboBox
    Friend WithEvents btnIP As Button
    Friend WithEvents btnDevices As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents txtPort As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents pnlIPs As Panel
    Friend WithEvents btnExittt As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents dgvNetwork As DataGridView
    Friend WithEvents cmnuNetwork As ContextMenuStrip
    Friend WithEvents cmnuAuswählen As ToolStripMenuItem
    Friend WithEvents cmnuKopieren As ToolStripMenuItem
    Friend WithEvents lblMessage As Label
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents pnlButtons As Panel
    Friend WithEvents dgvServer As DataGridView
    Friend WithEvents txtServer As TextBox
    Friend WithEvents pnlServer As Panel
    Friend WithEvents IP As DataGridViewTextBoxColumn
    Friend WithEvents Device As DataGridViewTextBoxColumn
    Friend WithEvents Network As DataGridViewTextBoxColumn
    Friend WithEvents NetTyp As DataGridViewTextBoxColumn
    Friend WithEvents lblStatus As Label
    Friend WithEvents Selected As DataGridViewCheckBoxColumn
    Friend WithEvents Netzwerk As DataGridViewTextBoxColumn
    Friend WithEvents Typ As DataGridViewTextBoxColumn
    Friend WithEvents IPv4 As DataGridViewTextBoxColumn
    Friend WithEvents pnlTCP As Panel
    Friend WithEvents txtTCP_Ip As TextBox
    Friend WithEvents txtTCP_Port As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents cboDatabase As ComboBox
    Friend WithEvents Timer1 As Timer
End Class
