﻿
Imports MySqlConnector

Module Save

    Public Event Display_Status(Sender As String, Status As Boolean)

    Class mySave
        Implements IDisposable

        Sub Save_Results(Table As String, myForm As Form)
            Try
                Table = LCase(Table).Replace("ß", "ss")
                bsResults.EndEdit()

                Dim Query = "UPDATE results SET " & Table & " = @result, " & Table & "2" & " = @result2, ZK = @zk, " & Table & "_Zeit = @zeit, Heben = @heben, Wertung2 = @wert2 "
                If Wettkampf.Athletik Then Query += ", Gesamt = @gesamt "
                Query += "WHERE " & Wettkampf.WhereClause() & " And Teilnehmer = @heber;"

                Dim changes = dtResults.GetChanges()

                If Not IsNothing(changes) Then
                    Using conn As New MySqlConnection(User.ConnString)
                        conn.Open()
                        For Each r As DataRow In changes.Rows
                            Dim cmd = New MySqlCommand(Query, conn)
                            With cmd.Parameters
                                .AddWithValue("@heber", r!Teilnehmer)
                                .AddWithValue("@result", r(Table))
                                .AddWithValue("@result2", r(Table & "2"))
                                .AddWithValue("@zk", r!ZK)
                                .AddWithValue("@heben", r!Heben)
                                .AddWithValue("@zeit", r(Table & "_Zeit"))
                                If Not Wettkampf.Mannschaft Then
                                    .AddWithValue("@wert2", r!Wertung2)
                                Else
                                    .AddWithValue("@wert2", DBNull.Value)
                                End If
                                If Wettkampf.Athletik Then
                                    .AddWithValue("@gesamt", r!Gesamt)
                                End If
                            End With
                            Try
                                cmd.ExecuteNonQuery()
                                ConnError = False
                            Catch ex As Exception
                                MySQl_Error(Nothing, ex.Message, ex.StackTrace)
                            End Try
                        Next

                        Query = "UPDATE results SET " & Table & "_Platz = @result_platz, " & Table & "2_Platz = @result2_platz, Heben_Platz = @heben_platz, Wertung2_Platz = @wert2_platz"
                        If Wettkampf.Athletik Then Query += ", Gesamt_Platz = @gesamt_platz "
                        Query += " WHERE Wettkampf = @wk And Teilnehmer = @heber;"

                        For Each Key In Wettkampf.Identities.Keys
                            For Each r As DataRow In changes.Rows
                                Dim cmd = New MySqlCommand(Query, conn)
                                With cmd.Parameters
                                    .AddWithValue("@wk", Key)
                                    .AddWithValue("@heber", r!Teilnehmer)
                                    If Not Wettkampf.Mannschaft Then
                                        .AddWithValue("@result_platz", r(Table & "_Platz_" & Key))
                                        .AddWithValue("@result2_platz", r(Table & "2_Platz_" & Key))
                                        .AddWithValue("@heben_platz", r("Heben_Platz_" & Key))
                                        .AddWithValue("@wert2_platz", r("Wertung2_Platz_" & Key))
                                    Else
                                        .AddWithValue("@result_platz", DBNull.Value)
                                        .AddWithValue("@result2_platz", DBNull.Value)
                                        .AddWithValue("@heben_platz", DBNull.Value)
                                        .AddWithValue("@wert2_platz", DBNull.Value)
                                    End If
                                    If Wettkampf.Athletik Then
                                        .AddWithValue("@gesamt_platz", r("Gesamt_Platz" & Key))
                                    End If
                                End With
                                Try
                                    'If Not conn.State = ConnectionState.Open Then conn.Open()
                                    cmd.ExecuteNonQuery()
                                    ConnError = False
                                Catch ex As Exception
                                    MySQl_Error(Nothing, ex.Message, ex.StackTrace)
                                End Try
                            Next
                        Next
                    End Using
                    If Not ConnError Then dtResults.AcceptChanges()

                    RaiseEvent Display_Status("Result", CBool(ConnError))
                    Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
                    If Leader.RowNum > 0 Then Leader.DimOut = Leader.RowNum + Leader.RowNum_Offset - 1

                    'ExternCounter.Start("Resultat", 1)
                    If Wettkampf.IsSinclairWertung OrElse Wettkampf.IsSinclairWertung2 OrElse Wettkampf.IsRobiWertung OrElse Wettkampf.IsRobiWertung2 Then
                        If User.UserId = UserID.Wettkampfleiter Then
                            Using OS As New OnlineService
                                OS.UpdateResults()        ' nur bei GÜLTIG updaten??????????
                            End Using
                        End If
                    End If
                End If
            Catch ex As Exception
                LogMessage("Save: Save_Results: " & ex.Message, ex.StackTrace)
            End Try
        End Sub
        Function Save_Versuch(DataTable As DataTable, Table As String, Form As Form, Optional Korrektur As Boolean = False) As Boolean

            ' AcceptChanges() for DataTable is called at origin if returned successfully 

            Try
                Table = LCase(Table).Replace("ß", "ss")
                Try
                    If Form.Equals(formLeader) Then
                        bsLeader.EndEdit()
                        bsDelete.EndEdit()
                        'formLeader.dgvLeader.EndEdit()
                        'formLeader.Validate()
                    End If
                Catch ex As Exception
                End Try

                Using conn As New MySqlConnection(User.ConnString)
                    Dim Query = "UPDATE " & Table & " Set Wertung = @wertung, Zeit = @zeit, HLast = @last , Diff = @diff, Note = @note, Lampen = @lampe "
                    If Korrektur Then
                        Query += "Steigerung1 = @s1, Steigerung2 = @s2, Steigerung3 = @s3 "
                    End If
                    Query += "WHERE " & Wettkampf.WhereClause() & " And Teilnehmer = @heber And Versuch = @versuch;"

                    For Each r As DataRow In DataTable.Rows
                        Dim cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            .AddWithValue("@heber", r!Teilnehmer)
                            .AddWithValue("@versuch", r!Versuch)
                            .AddWithValue("@last", r!HLast)
                            .AddWithValue("@diff", r!Diff)
                            .AddWithValue("@wertung", r!Wertung)
                            .AddWithValue("@zeit", r!Zeit)
                            .AddWithValue("@note", If(Not IsDBNull(r!Note) AndAlso CDbl(r!Note) > 0, r!Note, DBNull.Value))
                            .AddWithValue("@lampe", r!Lampen)
                            If Korrektur Then
                                .AddWithValue("@s1", r!Steigerung1)
                                .AddWithValue("@s2", r!Steigerung2)
                                .AddWithValue("@s3", r!Steigerung3)
                            End If
                        End With
                        Try
                            If Not conn.State = ConnectionState.Open Then conn.Open()
                            cmd.ExecuteNonQuery()
                            ConnError = False
                        Catch ex As MySqlException
                            MySQl_Error(Nothing, ex.Message, ex.StackTrace,)
                        End Try
                    Next
                End Using

                RaiseEvent Display_Status("Versuch", CBool(ConnError))
                Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
                If Leader.RowNum > 0 Then Leader.DimOut = Leader.RowNum + Leader.RowNum_Offset - 1

                'ExternCounter.Start("Versuch", 1)
                If Wettkampf.IsSinclairWertung OrElse Wettkampf.IsSinclairWertung2 OrElse Wettkampf.IsRobiWertung OrElse Wettkampf.IsRobiWertung2 Then
                    If User.UserId = UserID.Wettkampfleiter Then
                        Using OS As New OnlineService
                            OS.UpdateHeben({Table}, CInt(DataTable(0)!Teilnehmer)) ' nur bei GÜLTIG updaten ?????????
                        End Using
                    End If
                End If

            Catch ex As Exception
                LogMessage("Save: Save_Versuch: " & ex.Message, ex.StackTrace)
                Return False
            End Try

            Return Not CBool(ConnError)
        End Function
        Sub Save_Steigerung(DataTable As DataTable, Table As String, Form As Form)

            Table = LCase(Table).Replace("ß", "ss")
            Try
                bsLeader.EndEdit()
                bsDelete.EndEdit()
                'If Form.Equals(formLeader) Then
                '    formLeader.dgvLeader.EndEdit()
                '    formLeader.Validate()
                'End If
            Catch ex As Exception
            End Try

            Try
                Using conn As New MySqlConnection(User.ConnString)

                    Dim Query = "UPDATE " & Table & " Set HLast = @last, Diff = @diff, Steigerung1 = @inc1, Steigerung2 = @inc2, Steigerung3 = @inc3 " &
                                "WHERE " & Wettkampf.WhereClause() & " And Teilnehmer = @heber And Versuch = @versuch;"

                    For Each r As DataRow In DataTable.Rows
                        If Not IsNothing(r) Then
                            Dim cmd = New MySqlCommand(Query, conn)
                            With cmd.Parameters
                                .AddWithValue("@heber", r!Teilnehmer)
                                .AddWithValue("@versuch", r!Versuch)
                                .AddWithValue("@last", r!HLast)
                                .AddWithValue("@diff", r!Diff)
                                .AddWithValue("@inc1", r!Steigerung1)
                                .AddWithValue("@inc2", r!Steigerung2)
                                .AddWithValue("@inc3", r!Steigerung3)
                            End With
                            Do
                                Try
                                    If Not conn.State = ConnectionState.Open Then conn.Open()
                                    cmd.ExecuteNonQuery()
                                    ConnError = False
                                Catch ex As Exception
                                    MySQl_Error(Nothing, ex.Message, ex.StackTrace)
                                End Try
                            Loop While ConnError
                        End If
                    Next
                End Using

                If Not ConnError Then dtLeader(Leader.TableIx).AcceptChanges()

                RaiseEvent Display_Status("Steigerung", CBool(ConnError))
                Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
                If Leader.RowNum > 0 Then Leader.DimOut = Leader.RowNum + Leader.RowNum_Offset - 1

                'ExternCounter.Start("Steigerung", 1)
                If Wettkampf.IsSinclairWertung OrElse Wettkampf.IsSinclairWertung2 OrElse Wettkampf.IsRobiWertung OrElse Wettkampf.IsRobiWertung2 Then
                    If User.UserId = UserID.Wettkampfleiter Then
                        ' immer updaten
                        Using OS As New OnlineService
                            OS.UpdateHeben({Table}, CInt(DataTable(0)!Teilnehmer))
                        End Using
                    End If
                End If
            Catch ex As Exception
                LogMessage("Save:Save_Steigerung: " & ex.Message, ex.StackTrace)
            End Try
        End Sub
        Sub Save_Punkte(Form As Form)
            Try
                Dim Query = "UPDATE wettkampf_teams SET Punkte = @pkt, Punkte2 = @pkt2, Reissen = @r, Stossen = @s, Heben = @h, Sieger = @w " &
                            "WHERE " & Wettkampf.WhereClause() & " AND Team = @team;"
                For Each row As DataRow In Glob.dtTeams.Rows
                    Using conn As New MySqlConnection(User.ConnString)
                        Dim cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            '.AddWithValue("@wk", Wettkampf.ID)
                            .AddWithValue("@pkt", row!Punkte)
                            .AddWithValue("@pkt2", row!Punkte2)
                            .AddWithValue("@r", row!Reissen)
                            .AddWithValue("@s", row!Stossen)
                            .AddWithValue("@h", row!Heben)
                            .AddWithValue("@w", row!Sieger)
                            .AddWithValue("@team", row!Team)
                        End With
                        Try
                            If conn.State = ConnectionState.Closed Then conn.Open()
                            cmd.ExecuteNonQuery()
                            ConnError = False
                        Catch ex As Exception
                            MySQl_Error(Nothing, ex.Message, ex.StackTrace)
                        End Try
                    End Using
                Next

                'If Not ConnError Then Glob.dtTeams.AcceptChanges()

            Catch ex As Exception
                LogMessage("Save: Save_Punkte: " & ex.Message, ex.StackTrace)
            End Try
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
        End Sub
    End Class
End Module
