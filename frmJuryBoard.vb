﻿
Public Class frmJuryBoard

    Private Sub sendUpdate(sender As Object, e As EventArgs) Handles Weiß1.CheckedChanged, Weiß2.CheckedChanged, Weiß3.CheckedChanged, Rot1.CheckedChanged, Rot2.CheckedChanged, Rot3.CheckedChanged, Wert1.ValueChanged, Wert2.ValueChanged, Wert3.ValueChanged, NurLeds.CheckedChanged, ledKnopf.CheckedChanged, KR1LED.CheckedChanged, KR2LED.CheckedChanged, KR3LED.CheckedChanged

        Dim buf(16) As Byte
        Dim vals As String

        If Not SerialPort1.IsOpen Then
            MsgBox("Erst den Port öffnen", MsgBoxStyle.Critical)
            Exit Sub

        End If

        If Not NurLeds.Checked Then

            REM Wenn nicht nur LEDs angezeigt werden sollen wird hier das Paket für die Anzeige gebaut

            REM Der wert für die Zeilen wird geholt, mit 100 multipliziert umd das Komma zu eliminieren
            REM und als feste 4-Zeichen formatiert in einen String zusammengeklebt

            vals = (Wert1.Value * 100).ToString("0000")
            vals += (Wert2.Value * 100).ToString("0000")
            vals += (Wert3.Value * 100).ToString("0000")

            System.Text.Encoding.ASCII.GetBytes(vals).CopyTo(buf, 0) REM Packt den String in Datenbytes für den späteren "Versand" um


        Else
            REM Sollten nur LEDs gewünscht sein wird das entsprechende Bit in der Nachricht gesetzt
            buf(12) += CByte(128)
        End If


        REM Im folgenden Teil wird das Byte für die LEDs aus den Zuständen der Checkboxen gebaut
        REM Könnte man evtl. schöner lösen indem man die Elemente als Array holt (k.A. ob&wie das in VB geht)

        If Rot1.Checked Then
            buf(12) += CByte(1)
        End If

        If Weiß1.Checked Then
            buf(12) += CByte(2)
        End If

        If Rot2.Checked Then
            buf(12) += CByte(4)
        End If

        If Weiß2.Checked Then
            buf(12) += CByte(8)
        End If

        If Rot3.Checked Then
            buf(12) += CByte(16)
        End If

        If Weiß3.Checked Then
            buf(12) += CByte(32)
        End If

        If ledKnopf.Checked Then
            buf(13) += CByte(1)
        End If

        If KR1LED.Checked Then
            buf(13) += CByte(2)
        End If

        If KR2LED.Checked Then
            buf(13) += CByte(4)
        End If

        If KR3LED.Checked Then
            buf(13) += CByte(8)
        End If

        WriteBuffer(buf)

    End Sub

    Private Sub WriteBuffer(buf() As Byte)

        Dim CRC As Long = 65535 REM Anfangswert für die CRC laden

        For x As Integer = 0 To 13 REM CRC geht über alle 14 Datenbytes
            CRC = CRC Xor buf(x) REM Aktuelles Datenbyte in die CRC-einbeziehen
            For y As Integer = 1 To 8 REM Acht Runden schieben und XOR-en
                If (CRC Mod 2) > 0 Then
                    CRC = CLng((CRC And 65534) / 2)
                    CRC = CRC Xor 40961 REM Polynom der CRC 0xA001 in dezimal ausgedrückt
                Else
                    CRC = CLng((CRC And 65534) / 2)
                End If
            Next y
        Next x

        REM Die beiden CRC-Bytes werden an die Nachricht angehängt 
        buf(14) = CByte((CRC And 65280) / 256)
        buf(15) = CByte(CRC And 255)

        SerialPort1.Write(buf, 0, 16) REM Gesamtes Paket absenden

    End Sub

    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        REM Aufnehmen vorhandener Ports in die Liste

        portList.Items.AddRange(SerialPort.GetPortNames)
        portList.SelectedIndex = 0
    End Sub

    Private Sub Con_Click(sender As Object, e As EventArgs) Handles Con.Click
        REM standart Öffnungsroutine

        Try
            SerialPort1.Close()
            SerialPort1.PortName = portList.SelectedItem.ToString
            SerialPort1.Open()
            Timer1.Enabled = True
        Catch ex As Exception
            MsgBox("Fehler beim Verbinden", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim c As Byte

        Try
            c = CByte(SerialPort1.ReadByte)
        Catch ex As Exception
            Return
        End Try

        Select Case c
            Case 70
                MsgBox("Fehlerhaftes Paket", MsgBoxStyle.Critical)
                Sync.Checked = False
            Case 79
                Sync.Checked = True
            Case Else
                If c <= 95 And c >= 80 Then
                    If (c And CByte((2 ^ 0))) = 0 Then
                        KnopfGed.CheckState = CheckState.Unchecked
                    Else
                        KnopfGed.CheckState = CheckState.Checked
                    End If

                    If (c And CByte((2 ^ 1))) = 0 Then
                        KR1ged.CheckState = CheckState.Unchecked
                    Else
                        KR1ged.CheckState = CheckState.Checked
                    End If

                    If (c And CByte((2 ^ 2))) = 0 Then
                        KR2ged.CheckState = CheckState.Unchecked
                    Else
                        KR2ged.CheckState = CheckState.Checked
                    End If
                    If (c And CByte((2 ^ 3))) = 0 Then
                        KR3ged.CheckState = CheckState.Unchecked
                    Else
                        KR3ged.CheckState = CheckState.Checked
                    End If
                End If
        End Select

    End Sub
End Class
