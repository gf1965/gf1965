﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmOptionen
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnApply = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.txtSize = New System.Windows.Forms.TextBox()
        Me.txtDisplay = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.nudTeilerZweikampf = New System.Windows.Forms.NumericUpDown()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.nudTeilerMasters = New System.Windows.Forms.NumericUpDown()
        Me.nudMaxGruppen = New System.Windows.Forms.NumericUpDown()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.chkKonflikt = New System.Windows.Forms.CheckBox()
        Me.chkMessages = New System.Windows.Forms.CheckBox()
        Me.chkUpdate = New System.Windows.Forms.CheckBox()
        Me.txtCom = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.chkSavePosition = New System.Windows.Forms.CheckBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.chkOverwrite = New System.Windows.Forms.CheckBox()
        Me.cboDecimals = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboMarkRows = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtAnzeige_Wertung = New System.Windows.Forms.TextBox()
        Me.chkMessageBohle = New System.Windows.Forms.CheckBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtAbZeichen = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtLimitTechnik = New System.Windows.Forms.TextBox()
        Me.txtHupe = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDelay_Wertung = New System.Windows.Forms.TextBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.pnlFontSize = New System.Windows.Forms.Panel()
        Me.btnFontSizeOriginal = New System.Windows.Forms.Button()
        Me.btnFontSizeSmaller = New System.Windows.Forms.Button()
        Me.btnFontSizeLarger = New System.Windows.Forms.Button()
        Me.chkPreviewFontSize = New System.Windows.Forms.CheckBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cboName = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkPrimary = New System.Windows.Forms.CheckBox()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.cboDriver = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.btnDefault = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.nudTeilerZweikampf, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudTeilerMasters, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMaxGruppen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.pnlFontSize.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnApply
        '
        Me.btnApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApply.Enabled = False
        Me.btnApply.Location = New System.Drawing.Point(396, 86)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(103, 26)
        Me.btnApply.TabIndex = 100
        Me.btnApply.Text = "Übernehmen"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(396, 54)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(103, 26)
        Me.btnExit.TabIndex = 110
        Me.btnExit.Text = "Abbrechen"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'txtSize
        '
        Me.txtSize.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSize.Location = New System.Drawing.Point(152, 48)
        Me.txtSize.MaxLength = 3
        Me.txtSize.Name = "txtSize"
        Me.txtSize.Size = New System.Drawing.Size(46, 20)
        Me.txtSize.TabIndex = 53
        Me.txtSize.Tag = "Display:Size"
        Me.txtSize.Text = "50"
        '
        'txtDisplay
        '
        Me.txtDisplay.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDisplay.Location = New System.Drawing.Point(152, 22)
        Me.txtDisplay.MaxLength = 3
        Me.txtDisplay.Name = "txtDisplay"
        Me.txtDisplay.Size = New System.Drawing.Size(46, 20)
        Me.txtDisplay.TabIndex = 51
        Me.txtDisplay.Tag = "Display:Duration"
        Me.txtDisplay.Text = "5"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Location = New System.Drawing.Point(16, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(130, 13)
        Me.Label1.TabIndex = 50
        Me.Label1.Text = "Anzeigedauer (Sekunden)"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Location = New System.Drawing.Point(19, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(127, 17)
        Me.Label2.TabIndex = 52
        Me.Label2.Text = "Größe (%)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(364, 289)
        Me.TabControl1.TabIndex = 10
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.nudTeilerZweikampf)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.nudTeilerMasters)
        Me.TabPage1.Controls.Add(Me.nudMaxGruppen)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.chkKonflikt)
        Me.TabPage1.Controls.Add(Me.chkMessages)
        Me.TabPage1.Controls.Add(Me.chkUpdate)
        Me.TabPage1.Controls.Add(Me.txtCom)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.chkSavePosition)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(356, 263)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Programm"
        '
        'nudTeilerZweikampf
        '
        Me.nudTeilerZweikampf.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.nudTeilerZweikampf.Location = New System.Drawing.Point(295, 178)
        Me.nudTeilerZweikampf.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudTeilerZweikampf.Name = "nudTeilerZweikampf"
        Me.nudTeilerZweikampf.Size = New System.Drawing.Size(40, 20)
        Me.nudTeilerZweikampf.TabIndex = 43
        Me.nudTeilerZweikampf.Tag = "Modus:TeilerZweikampf"
        Me.nudTeilerZweikampf.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudTeilerZweikampf.Value = New Decimal(New Integer() {18, 0, 0, 0})
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.Location = New System.Drawing.Point(26, 178)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(257, 20)
        Me.Label14.TabIndex = 42
        Me.Label14.Tag = ""
        Me.Label14.Text = "Standard-Gruppenteiler (Zweikampf)"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label14.UseCompatibleTextRendering = True
        '
        'nudTeilerMasters
        '
        Me.nudTeilerMasters.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.nudTeilerMasters.Location = New System.Drawing.Point(295, 156)
        Me.nudTeilerMasters.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudTeilerMasters.Name = "nudTeilerMasters"
        Me.nudTeilerMasters.Size = New System.Drawing.Size(40, 20)
        Me.nudTeilerMasters.TabIndex = 31
        Me.nudTeilerMasters.Tag = "Modus:TeilerMasters"
        Me.nudTeilerMasters.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudTeilerMasters.Value = New Decimal(New Integer() {18, 0, 0, 0})
        '
        'nudMaxGruppen
        '
        Me.nudMaxGruppen.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.nudMaxGruppen.Location = New System.Drawing.Point(295, 134)
        Me.nudMaxGruppen.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudMaxGruppen.Name = "nudMaxGruppen"
        Me.nudMaxGruppen.Size = New System.Drawing.Size(40, 20)
        Me.nudMaxGruppen.TabIndex = 29
        Me.nudMaxGruppen.Tag = "Modus:MaxGruppen"
        Me.nudMaxGruppen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudMaxGruppen.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.Location = New System.Drawing.Point(26, 156)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(257, 20)
        Me.Label13.TabIndex = 30
        Me.Label13.Tag = ""
        Me.Label13.Text = "Standard-Gruppenteiler (Masters)"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label13.UseCompatibleTextRendering = True
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.Location = New System.Drawing.Point(5, 134)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(278, 20)
        Me.Label11.TabIndex = 28
        Me.Label11.Tag = ""
        Me.Label11.Text = "maximale Gruppenanzahl (Kinder, Schüler, Jugend)"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label11.UseCompatibleTextRendering = True
        '
        'chkKonflikt
        '
        Me.chkKonflikt.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkKonflikt.AutoSize = True
        Me.chkKonflikt.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkKonflikt.Checked = True
        Me.chkKonflikt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkKonflikt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.chkKonflikt.Location = New System.Drawing.Point(151, 92)
        Me.chkKonflikt.Name = "chkKonflikt"
        Me.chkKonflikt.Size = New System.Drawing.Size(169, 18)
        Me.chkKonflikt.TabIndex = 27
        Me.chkKonflikt.Tag = "Programm:Konflikte"
        Me.chkKonflikt.Text = "bei Konflikten nachfragen      "
        Me.chkKonflikt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkKonflikt.UseCompatibleTextRendering = True
        Me.chkKonflikt.UseVisualStyleBackColor = True
        '
        'chkMessages
        '
        Me.chkMessages.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkMessages.AutoSize = True
        Me.chkMessages.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkMessages.Checked = True
        Me.chkMessages.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMessages.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.chkMessages.Location = New System.Drawing.Point(153, 69)
        Me.chkMessages.Name = "chkMessages"
        Me.chkMessages.Size = New System.Drawing.Size(167, 18)
        Me.chkMessages.TabIndex = 26
        Me.chkMessages.Tag = "Programm:ShowMessages"
        Me.chkMessages.Text = "alle Meldungen anzeigen      "
        Me.chkMessages.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkMessages.UseCompatibleTextRendering = True
        Me.chkMessages.UseVisualStyleBackColor = True
        '
        'chkUpdate
        '
        Me.chkUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkUpdate.AutoSize = True
        Me.chkUpdate.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkUpdate.Checked = True
        Me.chkUpdate.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.chkUpdate.Location = New System.Drawing.Point(22, 46)
        Me.chkUpdate.Name = "chkUpdate"
        Me.chkUpdate.Size = New System.Drawing.Size(298, 18)
        Me.chkUpdate.TabIndex = 25
        Me.chkUpdate.Tag = "Programm:AutoUpdate"
        Me.chkUpdate.Text = "bei Programmstart automatisch auf Updates prüfen      "
        Me.chkUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkUpdate.UseCompatibleTextRendering = True
        Me.chkUpdate.UseVisualStyleBackColor = True
        '
        'txtCom
        '
        Me.txtCom.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCom.Location = New System.Drawing.Point(295, 224)
        Me.txtCom.MaxLength = 4
        Me.txtCom.Name = "txtCom"
        Me.txtCom.Size = New System.Drawing.Size(40, 20)
        Me.txtCom.TabIndex = 41
        Me.txtCom.Tag = "Wettkampf:COM_Sleep"
        Me.txtCom.Text = "1000"
        Me.txtCom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.Location = New System.Drawing.Point(26, 223)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(257, 20)
        Me.Label7.TabIndex = 40
        Me.Label7.Tag = ""
        Me.Label7.Text = "Latenz-Zeit der COM-Ports (Millisekunden)"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label7.UseCompatibleTextRendering = True
        '
        'chkSavePosition
        '
        Me.chkSavePosition.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkSavePosition.AutoSize = True
        Me.chkSavePosition.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkSavePosition.Checked = True
        Me.chkSavePosition.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSavePosition.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.chkSavePosition.Location = New System.Drawing.Point(56, 23)
        Me.chkSavePosition.Name = "chkSavePosition"
        Me.chkSavePosition.Size = New System.Drawing.Size(264, 18)
        Me.chkSavePosition.TabIndex = 13
        Me.chkSavePosition.Tag = "Programm:SavePosition"
        Me.chkSavePosition.Text = "Position des Anwendungsfensters speichern      "
        Me.chkSavePosition.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkSavePosition.UseCompatibleTextRendering = True
        Me.chkSavePosition.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage2.Controls.Add(Me.chkOverwrite)
        Me.TabPage2.Controls.Add(Me.cboDecimals)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.cboMarkRows)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Controls.Add(Me.txtAnzeige_Wertung)
        Me.TabPage2.Controls.Add(Me.chkMessageBohle)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.txtAbZeichen)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.txtLimitTechnik)
        Me.TabPage2.Controls.Add(Me.txtHupe)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.txtDelay_Wertung)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(356, 263)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Wettkampf"
        '
        'chkOverwrite
        '
        Me.chkOverwrite.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkOverwrite.AutoSize = True
        Me.chkOverwrite.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkOverwrite.Location = New System.Drawing.Point(88, 235)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.Size = New System.Drawing.Size(233, 17)
        Me.chkOverwrite.TabIndex = 44
        Me.chkOverwrite.Tag = "Wettkampf:Overwrite"
        Me.chkOverwrite.Text = "nachträgliche Wertung <Gültig> möglich      "
        Me.chkOverwrite.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkOverwrite.UseVisualStyleBackColor = True
        '
        'cboDecimals
        '
        Me.cboDecimals.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDecimals.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboDecimals.FormattingEnabled = True
        Me.cboDecimals.Items.AddRange(New Object() {"0,1 - Schritte", "0,5 - Schritte"})
        Me.cboDecimals.Location = New System.Drawing.Point(204, 156)
        Me.cboDecimals.Name = "cboDecimals"
        Me.cboDecimals.Size = New System.Drawing.Size(107, 21)
        Me.cboDecimals.TabIndex = 43
        Me.cboDecimals.Tag = "Wettkampf:Decimals"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(73, 159)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(125, 13)
        Me.Label10.TabIndex = 42
        Me.Label10.Text = "Format der Technik-Note"
        '
        'cboMarkRows
        '
        Me.cboMarkRows.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMarkRows.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboMarkRows.FormattingEnabled = True
        Me.cboMarkRows.Items.AddRange(New Object() {"nur aktueller Versuch", "die ersten drei Versuche", "Versuche des akt. Hebers", "Versuche mit akt. Hantellast"})
        Me.cboMarkRows.Location = New System.Drawing.Point(146, 183)
        Me.cboMarkRows.Name = "cboMarkRows"
        Me.cboMarkRows.Size = New System.Drawing.Size(166, 21)
        Me.cboMarkRows.TabIndex = 43
        Me.cboMarkRows.Tag = "Wettkampf:MarkRows"
        Me.ToolTip1.SetToolTip(Me.cboMarkRows, "Optionen für die Markierung der nächsten Heber")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(33, 186)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(107, 13)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "Hervorhebung Heber"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.Location = New System.Drawing.Point(9, 103)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(274, 17)
        Me.Label12.TabIndex = 40
        Me.Label12.Text = "Anzeigedauer der Wertung (Sekunden)"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAnzeige_Wertung
        '
        Me.txtAnzeige_Wertung.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAnzeige_Wertung.Location = New System.Drawing.Point(289, 102)
        Me.txtAnzeige_Wertung.MaxLength = 3
        Me.txtAnzeige_Wertung.Name = "txtAnzeige_Wertung"
        Me.txtAnzeige_Wertung.Size = New System.Drawing.Size(46, 20)
        Me.txtAnzeige_Wertung.TabIndex = 41
        Me.txtAnzeige_Wertung.Tag = "Wettkampf:AnzeigeWertung"
        Me.txtAnzeige_Wertung.Text = "5"
        '
        'chkMessageBohle
        '
        Me.chkMessageBohle.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkMessageBohle.AutoSize = True
        Me.chkMessageBohle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkMessageBohle.Checked = True
        Me.chkMessageBohle.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMessageBohle.Location = New System.Drawing.Point(83, 212)
        Me.chkMessageBohle.Name = "chkMessageBohle"
        Me.chkMessageBohle.Size = New System.Drawing.Size(238, 17)
        Me.chkMessageBohle.TabIndex = 39
        Me.chkMessageBohle.Tag = "Wettkampf:ShowMessages"
        Me.chkMessageBohle.Text = "Meldungen der Anlage in Bohle anzeigen      "
        Me.chkMessageBohle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkMessageBohle.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.Location = New System.Drawing.Point(50, 129)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(233, 17)
        Me.Label9.TabIndex = 35
        Me.Label9.Tag = ""
        Me.Label9.Text = "max. erlaubte Differenz der Technik-Note"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAbZeichen
        '
        Me.txtAbZeichen.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAbZeichen.Location = New System.Drawing.Point(289, 24)
        Me.txtAbZeichen.MaxLength = 3
        Me.txtAbZeichen.Name = "txtAbZeichen"
        Me.txtAbZeichen.Size = New System.Drawing.Size(46, 20)
        Me.txtAbZeichen.TabIndex = 30
        Me.txtAbZeichen.Tag = "Wettkampf:DauerAbzeichen"
        Me.txtAbZeichen.Text = "10"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.Location = New System.Drawing.Point(69, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(214, 17)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Dauer des Ab-Zeichens (1/10 Sekunden)"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtLimitTechnik
        '
        Me.txtLimitTechnik.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLimitTechnik.Location = New System.Drawing.Point(289, 128)
        Me.txtLimitTechnik.MaxLength = 3
        Me.txtLimitTechnik.Name = "txtLimitTechnik"
        Me.txtLimitTechnik.Size = New System.Drawing.Size(46, 20)
        Me.txtLimitTechnik.TabIndex = 36
        Me.txtLimitTechnik.Tag = "Wettkampf:LimitTechniknote"
        Me.txtLimitTechnik.Text = "1,5"
        '
        'txtHupe
        '
        Me.txtHupe.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtHupe.Location = New System.Drawing.Point(289, 50)
        Me.txtHupe.MaxLength = 3
        Me.txtHupe.Name = "txtHupe"
        Me.txtHupe.Size = New System.Drawing.Size(46, 20)
        Me.txtHupe.TabIndex = 32
        Me.txtHupe.Tag = "Wettkampf:DauerHupe"
        Me.txtHupe.Text = "3"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.Location = New System.Drawing.Point(9, 77)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(274, 17)
        Me.Label8.TabIndex = 33
        Me.Label8.Text = "Verzögerung der Anzeige der Wertung (Sekunden)"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.Location = New System.Drawing.Point(47, 51)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(236, 17)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Dauer des Hup-Tons der Uhr (1/10 Sekunden)"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDelay_Wertung
        '
        Me.txtDelay_Wertung.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDelay_Wertung.Location = New System.Drawing.Point(289, 76)
        Me.txtDelay_Wertung.MaxLength = 3
        Me.txtDelay_Wertung.Name = "txtDelay_Wertung"
        Me.txtDelay_Wertung.Size = New System.Drawing.Size(46, 20)
        Me.txtDelay_Wertung.TabIndex = 34
        Me.txtDelay_Wertung.Tag = "Wettkampf:WertungDelay"
        Me.txtDelay_Wertung.Text = "3"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage3.Controls.Add(Me.pnlFontSize)
        Me.TabPage3.Controls.Add(Me.cboName)
        Me.TabPage3.Controls.Add(Me.Label6)
        Me.TabPage3.Controls.Add(Me.GroupBox1)
        Me.TabPage3.Controls.Add(Me.chkPrimary)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(356, 263)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Anzeigen"
        '
        'pnlFontSize
        '
        Me.pnlFontSize.Controls.Add(Me.btnFontSizeOriginal)
        Me.pnlFontSize.Controls.Add(Me.btnFontSizeSmaller)
        Me.pnlFontSize.Controls.Add(Me.btnFontSizeLarger)
        Me.pnlFontSize.Controls.Add(Me.chkPreviewFontSize)
        Me.pnlFontSize.Controls.Add(Me.Label17)
        Me.pnlFontSize.Location = New System.Drawing.Point(32, 178)
        Me.pnlFontSize.Name = "pnlFontSize"
        Me.pnlFontSize.Size = New System.Drawing.Size(290, 72)
        Me.pnlFontSize.TabIndex = 62
        Me.pnlFontSize.Visible = False
        '
        'btnFontSizeOriginal
        '
        Me.btnFontSizeOriginal.Location = New System.Drawing.Point(189, 30)
        Me.btnFontSizeOriginal.Name = "btnFontSizeOriginal"
        Me.btnFontSizeOriginal.Size = New System.Drawing.Size(88, 25)
        Me.btnFontSizeOriginal.TabIndex = 63
        Me.btnFontSizeOriginal.Text = "Originalgröße"
        Me.btnFontSizeOriginal.UseVisualStyleBackColor = True
        '
        'btnFontSizeSmaller
        '
        Me.btnFontSizeSmaller.Image = Global.Gewichtheben.My.Resources.Resources.arrow_down_16
        Me.btnFontSizeSmaller.Location = New System.Drawing.Point(100, 30)
        Me.btnFontSizeSmaller.Name = "btnFontSizeSmaller"
        Me.btnFontSizeSmaller.Size = New System.Drawing.Size(70, 25)
        Me.btnFontSizeSmaller.TabIndex = 62
        Me.btnFontSizeSmaller.Text = "kleiner"
        Me.btnFontSizeSmaller.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnFontSizeSmaller.UseVisualStyleBackColor = True
        '
        'btnFontSizeLarger
        '
        Me.btnFontSizeLarger.Image = Global.Gewichtheben.My.Resources.Resources.arrow_up_16
        Me.btnFontSizeLarger.Location = New System.Drawing.Point(9, 30)
        Me.btnFontSizeLarger.Name = "btnFontSizeLarger"
        Me.btnFontSizeLarger.Size = New System.Drawing.Size(70, 25)
        Me.btnFontSizeLarger.TabIndex = 61
        Me.btnFontSizeLarger.Text = "größer"
        Me.btnFontSizeLarger.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnFontSizeLarger.UseVisualStyleBackColor = True
        '
        'chkPreviewFontSize
        '
        Me.chkPreviewFontSize.AutoSize = True
        Me.chkPreviewFontSize.Checked = True
        Me.chkPreviewFontSize.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPreviewFontSize.Location = New System.Drawing.Point(212, 7)
        Me.chkPreviewFontSize.Name = "chkPreviewFontSize"
        Me.chkPreviewFontSize.Size = New System.Drawing.Size(71, 17)
        Me.chkPreviewFontSize.TabIndex = 60
        Me.chkPreviewFontSize.Text = "Vorschau"
        Me.chkPreviewFontSize.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.chkPreviewFontSize.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(6, 7)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(210, 17)
        Me.Label17.TabIndex = 58
        Me.Label17.Text = "Schriftgröße der Wertungsanzeige"
        '
        'cboName
        '
        Me.cboName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboName.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboName.FormattingEnabled = True
        Me.cboName.Items.AddRange(New Object() {"Name, Vorname", "NACHNAME, Vorname", "NACHNAME Vorname"})
        Me.cboName.Location = New System.Drawing.Point(149, 133)
        Me.cboName.Name = "cboName"
        Me.cboName.Size = New System.Drawing.Size(166, 21)
        Me.cboName.TabIndex = 57
        Me.cboName.Tag = "Format:NameAnzeige"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(40, 136)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(107, 17)
        Me.Label6.TabIndex = 56
        Me.Label6.Text = "Format des Namens"
        Me.Label6.UseCompatibleTextRendering = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtSize)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtDisplay)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 17)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(216, 85)
        Me.GroupBox1.TabIndex = 55
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Identifizieren"
        '
        'chkPrimary
        '
        Me.chkPrimary.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkPrimary.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkPrimary.Enabled = False
        Me.chkPrimary.Location = New System.Drawing.Point(249, 25)
        Me.chkPrimary.Name = "chkPrimary"
        Me.chkPrimary.Size = New System.Drawing.Size(85, 75)
        Me.chkPrimary.TabIndex = 54
        Me.chkPrimary.Tag = "Programm:Primary"
        Me.chkPrimary.Text = "Mauszeiger " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "auf " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "WK-Leitung " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "begrenzen"
        Me.chkPrimary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkPrimary.UseCompatibleTextRendering = True
        Me.chkPrimary.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage5.Controls.Add(Me.cboDriver)
        Me.TabPage5.Controls.Add(Me.Label15)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(356, 263)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "FastReport"
        '
        'cboDriver
        '
        Me.cboDriver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDriver.FormattingEnabled = True
        Me.cboDriver.Location = New System.Drawing.Point(29, 49)
        Me.cboDriver.Name = "cboDriver"
        Me.cboDriver.Size = New System.Drawing.Size(300, 21)
        Me.cboDriver.TabIndex = 1
        Me.cboDriver.Tag = "FastReport:MySQLDriver"
        Me.ToolTip1.SetToolTip(Me.cboDriver, "Verbindung zwischen Report und Datenbank" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Standard <MySQL ODBC 8.0 Unicode Driver" &
        ">")
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(28, 33)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(73, 13)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "MySQL-Driver"
        '
        'btnDefault
        '
        Me.btnDefault.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDefault.Location = New System.Drawing.Point(396, 273)
        Me.btnDefault.Name = "btnDefault"
        Me.btnDefault.Size = New System.Drawing.Size(103, 26)
        Me.btnDefault.TabIndex = 26
        Me.btnDefault.Text = "Voreinstellungen"
        Me.btnDefault.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(396, 22)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(103, 26)
        Me.btnSave.TabIndex = 111
        Me.btnSave.Text = "OK"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmOptionen
        '
        Me.AcceptButton = Me.btnSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(519, 314)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnDefault)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnApply)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOptionen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Optionen"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.nudTeilerZweikampf, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudTeilerMasters, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMaxGruppen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.pnlFontSize.ResumeLayout(False)
        Me.pnlFontSize.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents txtSize As System.Windows.Forms.TextBox
    Friend WithEvents txtDisplay As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents Label9 As Label
    Friend WithEvents txtAbZeichen As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtLimitTechnik As TextBox
    Friend WithEvents txtHupe As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtDelay_Wertung As TextBox
    Friend WithEvents chkSavePosition As CheckBox
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents txtCom As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents ColorDialog1 As ColorDialog
    Friend WithEvents btnDefault As Button
    Friend WithEvents chkPrimary As CheckBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtAnzeige_Wertung As TextBox
    Friend WithEvents btnSave As Button
    Friend WithEvents chkUpdate As CheckBox
    Friend WithEvents chkMessages As CheckBox
    Friend WithEvents cboMarkRows As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents chkKonflikt As CheckBox
    Friend WithEvents cboDecimals As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents chkMessageBohle As CheckBox
    Friend WithEvents chkOverwrite As CheckBox
    Friend WithEvents nudTeilerMasters As NumericUpDown
    Friend WithEvents nudMaxGruppen As NumericUpDown
    Friend WithEvents Label13 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents nudTeilerZweikampf As NumericUpDown
    Friend WithEvents Label14 As Label
    Friend WithEvents cboName As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents cboDriver As ComboBox
    Friend WithEvents Label15 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Label17 As Label
    Friend WithEvents chkPreviewFontSize As CheckBox
    Friend WithEvents pnlFontSize As Panel
    Friend WithEvents btnFontSizeOriginal As Button
    Friend WithEvents btnFontSizeSmaller As Button
    Friend WithEvents btnFontSizeLarger As Button
End Class
