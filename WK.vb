﻿
Imports System.Math

Public Class frmWettkampf
    Private nonNumberEntered As Boolean = False
    Dim conn As MySqlConnection
    Dim Query As String
    Dim cmd As MySqlCommand '(Query, conn)
    Dim reader As MySqlDataReader
    Dim da As MySqlDataAdapter
    Dim bsVerein As BindingSource
    Dim bsModus As BindingSource
    Dim bsVerantwortlicher As BindingSource
    Dim ds As New DataSet
    Dim cb As MySqlCommandBuilder
    Dim dtMannschaft As New DataTable 'Source für cbo
    Dim dtTeam As New DataTable 'wettkampf_teams
    Dim dtDetails As New DataTable
    Dim Bez As New Dictionary(Of Integer, ComboBox)
    'Private Team As New Dictionary(Of Integer, ComboBox)
    Dim TeamBez As New List(Of Label)
    Dim Team As New List(Of ComboBox)
    Dim listTeam As New ArrayList
    Dim loading As Boolean
    Dim actTeam As Integer

    'Optionen
    Dim Disziplinen As String() = {"Anristen", "Bankdrücken", "Beugestütz", "Differenzsprung", "Klimmzug", "Pendellauf", "Schlussdreisprung", "Schlussweitsprung", "Schockwurf", "Sprint"}

    Private Sub frmWettkampf_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If mnuSpeichern.enabled Then
            Select Case MsgBox("Änderungen speichern?", CType(MsgBoxStyle.YesNoCancel + vbQuestion, MsgBoxStyle), "Wettkampf")
                Case MsgBoxResult.Yes
                    mnuSpeichern.PerformClick()
                Case MsgBoxResult.Cancel
                    e.Cancel = True
            End Select
        End If
    End Sub

    Private Sub frmWettkampf_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        Bez.Add(1, cboBezeichnung1)
        Bez.Add(2, cboBezeichnung2)
        Bez.Add(3, cboBezeichnung3)
        Team.Add(cbo1)
        Team.Add(cbo2)
        TeamBez.Add(lbl1)
        TeamBez.Add(lbl2)
        Dim tTip As New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        tTip.SetToolTip(btnAdd, "Fügt ein Element am Ende der Liste hinzu.")
        tTip.SetToolTip(btnRemove, "Entfernt das ausgewählte Element aus der Liste.")
        tTip.SetToolTip(txtSteigerung1, "Überschreibt die Steigerung für diesen WK.")
        tTip.SetToolTip(txtSteigerung2, "Überschreibt die Steigerung für diesen WK.")
    End Sub

    'Private Sub frmWettkampf_LocationChanged(sender As Object, e As EventArgs) Handles Me.LocationChanged
    '    With formMain
    '        ' Cursor.Clip = .Bounds
    '        If Me.Bounds.X <= 0 Then Me.Location = New Point(.Bounds.X + 8, Me.Bounds.Y)
    '        If Me.Bounds.X + Me.Width >= .Width - 16 Then Me.Location = New Point(.Width - Me.Width - 16, Me.Bounds.Y)
    '        If Me.Bounds.Y <= .mnuMain.Height + 32 Then Me.Location = New Point(Me.Bounds.X, .Bounds.Y + 32 + .mnuMain.Height)
    '        If Me.Bounds.Y + Me.Height >= .Height Then Me.Location = New Point(Me.Bounds.X, .Height - Me.Height - 16)
    '    End With
    'End Sub

    'Private Sub frmWettkampf_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
    '    'Cursor.Clip = formMain.Bounds
    'End Sub

    Private Sub frmWettkampf_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        loading = True
        Dim x As Integer = 0
        Dim _athletik As String = String.Empty

        Using conn As New MySqlConnection(MySqlConnString)
            Try
                conn.Open()

                'Liste der Orte
                Query = "SELECT DISTINCT Ort FROM wettkampf;"
                cmd = New MySqlCommand(Query, conn)
                reader = cmd.ExecuteReader
                While reader.Read
                    If Not IsDBNull(reader.GetValue(0)) Then cboOrt.Items.Add(reader.GetString(0))
                End While
                reader.Close()

                'Liste der Bezeichungen
                For i As Integer = 1 To 3
                    Query = "SELECT DISTINCT Bezeichnung FROM wettkampf_bez WHERE ID = " & i.ToString & " ORDER BY Bezeichnung ASC;"
                    cmd = New MySqlCommand(Query, conn)
                    reader = cmd.ExecuteReader()
                    While reader.Read
                        If Not IsDBNull(reader.GetValue(0)) Then Bez(i).Items.Add(reader.GetString(0))
                    End While
                    reader.Close()
                Next

                'Vereine für Liste
                da = New MySqlDataAdapter("SELECT * FROM verein ORDER BY Vereinsname ASC;", conn)
                da.Fill(ds, "verein")
                'Modi für Liste
                da = New MySqlDataAdapter("SELECT * FROM modus;", conn)
                da.Fill(ds, "modus")

                'Liste Vereine
                With cboVeranstalter
                    .DataSource = ds.Tables("verein")
                    .DisplayMember = "Vereinsname"
                    .ValueMember = "idVerein"
                    If Wettkampf.ID = 0 Then .SelectedIndex = -1
                End With

                'Liste Modi
                With cboModus
                    .DataSource = ds.Tables("modus")
                    .DisplayMember = "Bezeichnung"
                    .ValueMember = "idModus"
                    If Wettkampf.ID = 0 Then .SelectedIndex = -1
                End With

                'Source für Liste von cboMannschaften
                Query = "SELECT idVerein AS ID, Vereinsname AS Mannschaft FROM verein UNION SELECT idTeam AS ID, Team_Name AS Mannschaft FROM teams ORDER BY Mannschaft ASC;"
                cmd = New MySqlCommand(Query, conn)
                reader = cmd.ExecuteReader()
                dtMannschaft.Load(reader)

                'bestehendener Wettkampf
                If Wettkampf.ID > 0 Then txtID.Text = Wettkampf.ID.ToString
                If txtID.Text > "" Then
                    'Wettkampf einlesen
                    Query = "SELECT * FROM wettkampf WHERE idWettkampf = " & CInt(txtID.Text) & ";"
                    cmd = New MySqlCommand(Query, conn)
                    reader = cmd.ExecuteReader
                    reader.Read()
                    cboVeranstalter.SelectedValue = reader("Veranstalter")
                    cboModus.SelectedValue = reader("Modus")
                    cboDatum.Value = reader.GetDateTime("Datum")
                    If Not IsDBNull(reader("DatumBis")) Then
                        cboDatumBis.Format = DateTimePickerFormat.Short
                        cboDatumBis.Value = reader.GetDateTime("DatumBis")
                    End If
                    If Not IsDBNull(reader("Ort")) Then cboOrt.Text = reader.GetString("Ort")
                    If Not IsDBNull(reader("Mannschaft")) Then chkMannschaft.Checked = reader.GetBoolean("Mannschaft")
                    If Not IsDBNull(reader("Kampfrichter")) Then cboKR.Text = reader.GetString("Kampfrichter")
                    If Not IsDBNull(reader("Athletik")) OrElse reader.GetString("Athletik") > "" Then _athletik = reader.GetString("Athletik")
                    If Not IsDBNull(reader("Bigdisc")) Then chkBigdisc.Checked = reader.GetBoolean("BigDisc")
                    reader.Close()

                    'Bezeichnung(1-3) einlesen
                    da = New MySqlDataAdapter("SELECT * FROM wettkampf_bez WHERE Wettkampf = " & txtID.Text & ";", conn)
                    da.Fill(ds, "bez")
                    For i As Integer = 1 To ds.Tables("bez").Rows.Count
                        Bez(CInt(ds.Tables("bez").Rows(i - 1)("ID"))).Text = ds.Tables("bez").Rows(i - 1)("Bezeichnung").ToString
                    Next
                    reader.Close()

                    Query = "SELECT * FROM wettkampf_meldefrist WHERE Wettkampf = " & CInt(txtID.Text) & " ORDER BY ID;"
                    cmd = New MySqlCommand(Query, conn)
                    reader = cmd.ExecuteReader
                    If reader.HasRows Then
                        While reader.Read()
                            If x = 0 Then
                                If Not IsDBNull(reader("Gebuehr")) Then txtStartgeld.Text = Format(CStr(reader.GetDouble("Gebuehr")), "fixed")
                                If Not IsDBNull(reader("Datum")) Then
                                    cboMeldeschluss.Format = DateTimePickerFormat.Short
                                    cboMeldeschluss.Value = reader.GetDateTime("Datum")
                                End If
                                x += 1
                            ElseIf x = 1 Then
                                If Not IsDBNull(reader("Gebuehr")) Then txtStartgeld_NM.Text = Format(CStr(reader.GetDouble("Gebuehr")), "fixed")
                                If Not IsDBNull(reader("Datum")) Then
                                    cboMeldeschlussNM.Format = DateTimePickerFormat.Short
                                    cboMeldeschlussNM.Value = reader.GetDateTime("Datum")
                                End If
                                x += 1
                            ElseIf x = 2 Then
                                If Not IsDBNull(reader("Gebuehr")) Then txtStartgeld_M.Text = Format(CStr(reader.GetDouble("Gebuehr")), "fixed")
                            End If
                        End While
                    End If
                    reader.Close()

                    'Source für "wettkampf_teams"
                    Query = "SELECT * FROM wettkampf_teams WHERE Wettkampf = " & Wettkampf.ID & " ORDER BY ID ASC;"
                    cmd = New MySqlCommand(Query, conn)
                    reader = cmd.ExecuteReader()
                    dtTeam.Load(reader)
                    Query = "SELECT * FROM wettkampf_details WHERE Wettkampf = " & Wettkampf.ID & ";"
                    cmd = New MySqlCommand(Query, conn)
                    reader = cmd.ExecuteReader()
                    dtDetails.Load(reader)
                End If
            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
            End Try
        End Using

        If dtDetails.Rows.Count > 0 Then
            With dtDetails.Rows(0)
                If Not IsDBNull(.Item("AK_w")) Then updAK_w.Value = CInt(.Item("AK_w"))
                If Not IsDBNull(.Item("AK_m")) Then updAK_m.Value = CInt(.Item("AK_m"))
                If Not IsDBNull(.Item("Team")) Then updTeam.Value = CInt(.Item("Team"))
                If Not IsDBNull(.Item("Team_min")) Then updTeam_min.Value = CInt(.Item("Team_min"))
                If Not IsDBNull(.Item("Leistung_w")) Then txtLeistung_w.Text = CStr(.Item("Leistung_w"))
                If Not IsDBNull(.Item("Leistung_m")) Then txtLeistung_m.Text = CStr(.Item("Leistung_m"))
            End With
        End If

        pnlMannschaft.Enabled = chkMannschaft.Checked
        btnAdd.Enabled = chkMannschaft.Checked
        btnRemove.Enabled = chkMannschaft.Checked

        If txtID.Text = "" Then
            cboModus.SelectedIndex = -1
            cboVeranstalter.SelectedIndex = -1
            cboOrt.SelectedIndex = -1
            For i As Integer = 1 To 3
                Bez(i).SelectedIndex = -1
            Next
        End If

        lstAthletik.Items.AddRange(Disziplinen.ToArray) 'CheckedList füllen mit Array (aus Optionen)
        If _athletik.Length > 0 Then
            'Disziplinen auswählen
            Dim s As String() = Split(_athletik, ";")
            For i As Integer = 0 To s.Count - 1
                lstAthletik.SetItemChecked(i, CBool(s(i)))
            Next
        End If

        If chkMannschaft.Checked Then
            'Liste der Vereine + Teams erstellen
            For c As Integer = 0 To dtMannschaft.Rows.Count - 1
                listTeam.Add(dtMannschaft.Rows(c)("Mannschaft"))
            Next
            'Liste in cbo's einlesen
            Dim row As DataRow()
            For i As Integer = 1 To dtTeam.Rows.Count
                'bei mehr als 2 Teams cbo hinzufügen
                If i > 2 Then btnAdd.PerformClick()
                'Liste zuweisen
                Team(i - 1).Items.AddRange(listTeam.ToArray)
                'Vereins-/Team-Name suchen
                row = Nothing
                row = dtMannschaft.Select("ID = " + dtTeam.Rows(i - 1)("Team").ToString)
                If row.Count > 0 Then Team(i - 1).Text = row(0)("Mannschaft").ToString
            Next
            pnlMannschaft.ScrollControlIntoView(lbl1)
        End If

        If cboModus.SelectedIndex > -1 Then
            Dim found As DataRow() = ds.Tables("modus").Select("idModus = " + cboModus.SelectedValue.ToString)
            pnlAthletik.Enabled = CBool(found(0)("Athletik"))
            If txtID.Text = "" Then
                txtSteigerung1.Text = found(0)("Steigerung1").ToString
                txtSteigerung2.Text = found(0)("Steigerung2").ToString
            Else
                txtSteigerung1.Text = Wettkampf.Steigerung(0).ToString
                txtSteigerung2.Text = Wettkampf.Steigerung(1).ToString
            End If
        End If

        mnuSpeichern.Enabled = False
        Cursor = Cursors.Default
        loading = False
    End Sub

    Private Sub mnuSpeichern_Click(sender As Object, e As EventArgs) Handles mnuSpeichern.Click

        Dim msg As String = ""
        Dim Meldeschluss As String() = {"", "", ""}
        Dim Startgebühr As String() = {"0.00", "0.00", "0.00"}
        Dim KR As String = "0"
        Dim Mannschaft As String = "False"
        Dim athletik As String = String.Empty

        If cboModus.Text = "" Then
            msg = vbTab + "- Modus fehlt"
        End If
        If cboVeranstalter.Text = "" Then
            msg += If(msg = "", "", Chr(13)) + vbTab + "- Veranstalter fehlt"
        End If
        If cboDatum.Text = " " Then
            msg += If(msg = "", "", Chr(13)) + vbTab + "- Datum fehlt"
        End If
        If cboOrt.Text = "" Then
            msg += If(msg = "", "", Chr(13)) + vbTab + "- Veranstaltungsort fehlt"
        End If
        If cboBezeichnung1.Text = "" Then
            msg += If(msg = "", "", Chr(13)) + vbTab + "- Bezeichnung fehlt"
        End If
        If cboKR.Text = "" Then
            msg += If(msg = "", "", Chr(13)) + vbTab + "- Anzahl Kampfrichter fehlt"
        End If
        If msg > "" Then
            msg = "Fehlende Angaben:" + vbNewLine + msg
            Using New Centered_MessageBox(Me)
                Select Case MessageBox.Show(msg, "Fehler bei der Eingabe", MessageBoxButtons.RetryCancel, MessageBoxIcon.Information)
                    Case DialogResult.Cancel
                        mnuSpeichern.Enabled = False
                        Close()
                    Case DialogResult.Retry
                        Exit Sub
                End Select
            End Using
        End If

        Cursor = Cursors.WaitCursor

        If Val(txtID.Text) > 0 Then
            Query = "UPDATE `wettkampf` SET `Modus`= @modus, `Datum`= @datum, `DatumBis`= @bis, `Veranstalter`= @verein, `Ort`= @ort, `Athletik`= @athletik, `Bigdisc`= @bigdisc, `Kampfrichter`= @kr, `Mannschaft`= @mannschaft " &
                    "WHERE `idWettkampf`= @id;"
        Else
            Query = "INSERT INTO `wettkampf` (`Modus`, `Datum`, `DatumBis`, `Veranstalter`, `Ort`, `Athletik`, `Bigdisc`, `Kampfrichter`, `Mannschaft`) " &
                    "VALUES(@modus, @datum, @bis, @verein, @ort, @athletik, @bigdisc, @kr, @mannschaft);" &
                    "SELECT LAST_INSERT_ID();"
        End If

        Dim modus() As DataRow = ds.Tables("modus").Select("idModus=" & cboModus.SelectedValue.ToString)
        If CBool(modus(0)("Athletik")) Then
            For i As Integer = 0 To lstAthletik.Items.Count - 1                   'durchläuft die Liste Athletik
                athletik += CStr(Abs(CInt(lstAthletik.GetItemChecked(i))))        'checked?
                If i < lstAthletik.Items.Count - 1 Then athletik += ";"           'String bauen
            Next
        End If

        Dim conn As New MySqlConnection((MySqlConnString))
        Dim cmd As New MySqlCommand(Query, conn)
        With cmd.Parameters
            .AddWithValue("@id", txtID.Text)
            .AddWithValue("@modus", cboModus.SelectedValue)
            .AddWithValue("@datum", cboDatum.Value)
            .AddWithValue("@bis", cboDatumBis.Value)
            .AddWithValue("@verein", cboVeranstalter.SelectedValue)
            .AddWithValue("@ort", cboOrt.Text)
            .AddWithValue("@athletik", athletik)
            .AddWithValue("@bigdisc", chkBigdisc.Checked)
            .AddWithValue("@kr", cboKR.Text)
            .AddWithValue("@mannschaft", chkMannschaft.Checked)
        End With

        Try
            Using conn
                conn.Open()
                If Val(txtID.Text) > 0 Then
                    cmd.ExecuteNonQuery()
                Else
                    txtID.Text = cmd.ExecuteScalar().ToString
                    cmd.Parameters("@id").Value = txtID.Text
                End If

                'Bezeichnung1-3 einlesen
                Query = "DELETE FROM wettkampf_bez WHERE Wettkampf = " & txtID.Text & ";"
                cmd = New MySqlCommand(Query, conn)
                reader = cmd.ExecuteReader
                reader.Close()
                For i As Integer = 1 To 3
                    If Bez(i).Text > "" Then
                        Query = "INSERT INTO wettkampf_bez(Wettkampf, ID, Bezeichnung) VALUES(" & txtID.Text & ", " & i.ToString & ", '" & Bez(i).Text & "');"
                        cmd = New MySqlCommand(Query, conn)
                        reader = cmd.ExecuteReader
                        reader.Close()
                    End If
                Next
                'Mannschaften aktualisieren
                If chkMannschaft.Checked Then
                    Query = "DELETE FROM wettkampf_teams WHERE Wettkampf = " & txtID.Text & ";"
                    cmd = New MySqlCommand(Query, conn)
                    reader = cmd.ExecuteReader
                    reader.Close()
                    Dim r As DataRow()
                    For i As Integer = 0 To Team.Count - 1
                        r = dtMannschaft.Select("Mannschaft ='" + Team(i).Text + "'")
                        Query = "INSERT INTO wettkampf_teams(Wettkampf, ID, Team) VALUES(" & txtID.Text & ", " & i.ToString & ", " & r(0)("ID").ToString & ");"
                        cmd = New MySqlCommand(Query, conn)
                        reader = cmd.ExecuteReader
                        reader.Close()
                    Next
                End If
                'MeldeSchluss und -gebühr aktualisieren
                Query = "DELETE FROM wettkampf_meldefrist WHERE Wettkampf = " & txtID.Text & ";"
                cmd = New MySqlCommand(Query, conn)
                reader = cmd.ExecuteReader
                reader.Close()
                If cboMeldeschluss.Text <> " " Then Meldeschluss(0) = "'" + cboMeldeschluss.Value.ToString("yyyy-MM-dd") + "'"
                If Val(txtStartgeld.Text) > 0 Then Startgebühr(0) = txtStartgeld.Text.Replace(",", ".")
                If cboMeldeschlussNM.Text <> " " Then Meldeschluss(1) = "'" + cboMeldeschlussNM.Value.ToString("yyyy-MM-dd") + "'"
                If Val(txtStartgeld_NM.Text) > 0 Then Startgebühr(1) = txtStartgeld_NM.Text.Replace(",", ".")
                If Val(txtStartgeld_M.Text) > 0 Then
                    Startgebühr(2) = txtStartgeld_M.Text.Replace(",", ".")
                    Meldeschluss(2) = "NULL"
                End If
                For i As Integer = 0 To 2
                    If Meldeschluss(i) > "" Or Startgebühr(i) <> "0.00" Then
                        Query = "INSERT INTO wettkampf_meldefrist(Wettkampf, ID, Datum, Gebuehr) VALUES(" & txtID.Text & ", " & i.ToString & ", " & Meldeschluss(i) & ", '" & Startgebühr(i) & "');"
                        cmd = New MySqlCommand(Query, conn)
                        reader = cmd.ExecuteReader
                        reader.Close()
                    End If
                Next
                Query = "DELETE FROM wettkampf_details WHERE Wettkampf = " & txtID.Text & ";"
                cmd = New MySqlCommand(Query, conn)
                reader = cmd.ExecuteReader
                reader.Close()
                Query = "INSERT INTO wettkampf_details(Wettkampf, AK_w, AK_m, Team, Team_min, Leistung_w, Leistung_m) " &
                        "VALUES(" & Wettkampf.ID & ", " &
                        If(Not updAK_w.Value.ToString = "", updAK_w.Value.ToString, "NULL") & ", " &
                        If(Not updAK_m.Value.ToString = "", updAK_m.Value.ToString, "NULL") & ", " &
                        If(Not updTeam.Value.ToString = "", updTeam.Value.ToString, "NULL") & ", " &
                        If(Not updTeam_min.Value.ToString = "", updTeam_min.Value.ToString, "NULL") & ", " &
                        If(Not txtLeistung_w.Text = "", txtLeistung_w.Text, "NULL") & ", " &
                        If(Not txtLeistung_m.Text = "", txtLeistung_m.Text, "NULL") & ");"
                cmd = New MySqlCommand(Query, conn)
                reader = cmd.ExecuteReader
                reader.Close()

            End Using


            Using New Centered_MessageBox(Me)
                If MessageBox.Show("Diesen Wettkampf aufrufen?", "GFHsoft Gewichtheben", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    Wettkampf = New myWettkampf
                    With Wettkampf
                        .ID = CInt(txtID.Text)
                        .Bezeichnung = cboBezeichnung1.Text
                        .Modus.Id = CInt(cboModus.SelectedValue)
                        .Technikwertung = CBool(modus(0)("Technikwertung"))
                        .Athletik = athletik
                        .KR = CInt(cboKR.Text)
                        .Steigerung(0) = CInt(txtSteigerung1.Text) 'CInt(modus(0)("Steigerung1"))
                        .Steigerung(1) = CInt(txtSteigerung2.Text) 'CInt(modus(0)("Steigerung2"))
                        .Mannschaft = chkMannschaft.Checked
                        .Flagge = chkFlagge.Checked

                    End With
                End If
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

        mnuSpeichern.Enabled = False
        Cursor = Cursors.Default

    End Sub

    Private Sub TextboxNum_KeyDown(sender As Object, e As KeyEventArgs) Handles txtStartgeld.KeyDown, txtStartgeld_NM.KeyDown, txtSteigerung1.KeyDown, txtSteigerung2.KeyDown
        nonNumberEntered = False
        If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
            If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                If e.KeyCode <> Keys.Back And e.KeyCode <> Keys.Oemcomma And e.KeyCode <> Keys.Decimal Then
                    nonNumberEntered = True
                End If
            End If
        End If
        If ModifierKeys = Keys.Shift Then
            nonNumberEntered = True
        End If
    End Sub

    Private Sub TextboxNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtStartgeld.KeyPress, txtStartgeld_NM.KeyPress, txtSteigerung1.KeyPress, txtSteigerung2.KeyPress
        If nonNumberEntered = True Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtStartgebühr_Leave(sender As Object, e As EventArgs) Handles txtStartgeld.Leave, txtStartgeld_NM.Leave
        With txtStartgeld
            .Text = Format(.Text, "fixed")
        End With
    End Sub

    Private Sub txtStartgeld_M_Leave(sender As Object, e As EventArgs) Handles txtStartgeld_M.Leave
        With txtStartgeld_M
            .Text = Format(.Text, "fixed")
        End With
    End Sub

    Private Sub txtStartgebührNM_Leave(sender As Object, e As EventArgs) Handles txtStartgeld_NM.Leave
        With txtStartgeld_NM
            .Text = Format(.Text, "fixed")
        End With
    End Sub

    Private Sub Changed(sender As Object, e As EventArgs) Handles cboVeranstalter.SelectedIndexChanged,
        cboBezeichnung1.TextChanged, cboBezeichnung2.TextChanged, cboBezeichnung3.TextChanged, cboDatum.ValueChanged,
        cboOrt.TextChanged, txtStartgeld.TextChanged, txtStartgeld_NM.TextChanged, cboMeldeschluss.ValueChanged, cboMeldeschlussNM.ValueChanged,
        cboKR.SelectedIndexChanged, lstAthletik.SelectedValueChanged, chkBigdisc.CheckedChanged, chkMannschaft.CheckedChanged, txtStartgeld_M.TextChanged,
        updAK_m.ValueChanged, updAK_w.ValueChanged, updTeam.ValueChanged, updTeam_min.ValueChanged, txtLeistung_m.TextChanged, txtLeistung_w.TextChanged,
        txtSteigerung1.TextChanged, txtSteigerung2.TextChanged

        If loading Then Exit Sub
        If Not loading Then mnuSpeichern.Enabled = True
        If cboDatum.Focused Then cboDatum.Format = DateTimePickerFormat.Short
        If cboMeldeschluss.Focused Then cboMeldeschluss.Format = DateTimePickerFormat.Short
        If cboMeldeschlussNM.Focused Then cboMeldeschlussNM.Format = DateTimePickerFormat.Short
    End Sub

    Private Sub btnVerein_Click(sender As Object, e As EventArgs) Handles btnVerein.Click
        Me.Cursor = Cursors.WaitCursor
        Using frmX As New frmVerein
            With frmX
                .mnuÜbernehmen.Visible = True
                .Location = New Point(CInt(IIf(formMain.Width >= .Width + 200, formMain.Left + 100, formMain.Left + (formMain.Width - .Width) / 2)),
                                      CInt(IIf(formMain.Height >= .Height + 200, formMain.Top + 100, formMain.Top + (formMain.Height - .Height) / 2)))
                .ShowDialog(Me)
                cboVeranstalter.SelectedValue = CInt(frmX.Tag)
                '.Dispose()
            End With
        End Using
        Me.Cursor = Me.DefaultCursor
    End Sub

    Private Sub btnModus_Click(sender As Object, e As EventArgs) Handles btnModus.Click
        Me.Cursor = Cursors.WaitCursor
        Using frmX As New frmModus
            With frmX
                .Location = New Point(CInt(IIf(formMain.Width >= .Width + 200, formMain.Left + 100, formMain.Left + (formMain.Width - .Width) / 2)),
                                      CInt(IIf(formMain.Height >= .Height + 200, formMain.Top + 100, formMain.Top + (formMain.Height - .Height) / 2)))
                .ShowDialog(Me)
                cboModus.SelectedValue = CInt(frmX.Tag)
                '.Dispose()
            End With
        End Using
        Me.Cursor = Me.DefaultCursor
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim ctl As New ComboBox
        With ctl
            .Name = "cbo" & Team.Count + 1
            .Size = New Size(CType(cbo1.Size, Point))
            .Location = New Point(Team(0).Left, Team(Team.Count - 1).Top + (Team(Team.Count - 1).Top - Team(Team.Count - 2).Top))
            .DropDownStyle = Team(Team.Count - 1).DropDownStyle
            .Anchor = CType(AnchorStyles.Left + AnchorStyles.Top, AnchorStyles)
            .Visible = True
            .Enabled = True
            .Parent = pnlMannschaft
            .Parent.Controls.Add(ctl)
            .AutoCompleteSource = Team(Team.Count - 1).AutoCompleteSource
            .TabIndex = Team(Team.Count - 1).TabIndex + 1
            .AutoCompleteMode = Team(Team.Count - 1).AutoCompleteMode
            .Items.AddRange(listTeam.ToArray)
            AddHandler .SelectedIndexChanged, AddressOf cbo_SelectedIndexChanged
            AddHandler .LostFocus, AddressOf cbo_LostFocus
            AddHandler .GotFocus, AddressOf cbo_GotFocus
            AddHandler .KeyDown, AddressOf cbo_KeyDown
        End With
        Team.Add(ctl)
        Dim ctl1 As New Label
        With ctl1
            .Name = "lbl" & Team.Count - 1
            .Size = New Size(CType(lbl1.Size, Point))
            .Location = New Point(lbl1.Left, lbl1.Top + (lbl2.Top - lbl1.Top) * (Team.Count - 1))
            .Anchor = CType(AnchorStyles.Left + AnchorStyles.Top, AnchorStyles)
            .Visible = True
            .Enabled = True
            .Parent = pnlMannschaft
            .Parent.Controls.Add(ctl1)
            .Text = "Gast " + CStr(Team.Count - 1)
            .AutoSize = False
            .TextAlign = ContentAlignment.MiddleRight
            pnlMannschaft.ScrollControlIntoView(ctl1)
        End With
        TeamBez.Add(ctl1)
        mnuSpeichern.Enabled = True
        btnRemove.Enabled = Team.Count > 2
    End Sub

    Private Sub chkMannschaft_CheckedChanged(sender As Object, e As EventArgs) Handles chkMannschaft.CheckedChanged

        If loading Then Exit Sub
        mnuSpeichern.Enabled = True
        pnlMannschaft.Enabled = chkMannschaft.Checked
        btnAdd.Enabled = chkMannschaft.Checked
        btnRemove.Enabled = chkMannschaft.Checked
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        If Not mnuMeldungen.Checked Then
            If MsgBox("Verein <" + Team(actTeam).Text + "> aus der Mannschaftsliste löschen?", CType(MsgBoxStyle.Question + MsgBoxStyle.YesNo, MsgBoxStyle), Wettkampf.Bezeichnung + " - Liste der Mannschaften") = MsgBoxResult.No Then Exit Sub
        End If
        loading = True
        For i As Integer = actTeam To Team.Count - 2
            Team(i).Text = Team(i + 1).Text
        Next
        With Team(Team.Count - 1)
            RemoveHandler .LostFocus, AddressOf cbo_LostFocus
            RemoveHandler .GotFocus, AddressOf cbo_GotFocus
            RemoveHandler .KeyDown, AddressOf cbo_KeyDown
            RemoveHandler .SelectedIndexChanged, AddressOf cbo_SelectedIndexChanged
            .Dispose()
        End With
        Team.RemoveAt(Team.Count - 1)
        TeamBez(Team.Count).Dispose()
        TeamBez.RemoveAt(Team.Count)
        TeamBez(0).Text = "Heim"
        For i As Integer = 1 To TeamBez.Count - 1
            TeamBez(i).Text = "Gast " + CStr(i)
        Next
        With Team(actTeam)
            .BackColor = Color.FromKnownColor(KnownColor.Window)
            .ForeColor = Color.FromKnownColor(KnownColor.ControlText)
        End With
        mnuSpeichern.Enabled = True
        btnRemove.Enabled = Team.Count > 2
        loading = False
    End Sub

    Private Sub cbo_GotFocus(sender As Object, e As EventArgs) Handles cbo1.GotFocus, cbo2.GotFocus
        With Team(actTeam)
            .BackColor = Color.FromKnownColor(KnownColor.Window)
            .ForeColor = Color.FromKnownColor(KnownColor.ControlText)
            .Tag = .Text
        End With
    End Sub

    Private Sub cbo_KeyDown(sender As Object, e As KeyEventArgs) Handles cbo1.KeyDown, cbo2.KeyDown
        If e.KeyCode = Keys.Escape Then
            For i As Integer = 0 To Team.Count - 1
                If Team(i).Equals(sender) Then
                    With Team(i)
                        .Text = .Tag.ToString
                        .Tag = .Text
                    End With
                    Exit Sub
                End If
            Next
        End If
    End Sub

    Private Sub cbo_LostFocus(sender As Object, e As EventArgs) Handles cbo1.LostFocus, cbo2.LostFocus
        For i As Integer = 0 To Team.Count - 1
            If Team(i).Equals(sender) Then
                With Team(i)
                    .BackColor = Color.FromKnownColor(KnownColor.Highlight)
                    .ForeColor = Color.FromKnownColor(KnownColor.HighlightText)
                End With
                actTeam = i
                Exit For
            End If
        Next
    End Sub

    Private Sub cbo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbo1.SelectedIndexChanged, cbo2.SelectedIndexChanged
        If loading Then Exit Sub
        Dim ix As Integer
        For ix = 0 To Team.Count - 1
            If sender.Equals(Team(ix)) Then Exit For
        Next
        For i As Integer = 0 To Team.Count - 1
            If i <> ix Then
                If Team(i).Text = Team(ix).Text Then
                    MsgBox("Eintrag bereits vorhanden (Mannschaft " + i.ToString + ").", vbInformation, "Wettkampf - Mannschaften")
                    Team(ix).SelectedIndex = -1
                    Exit Sub
                End If
            End If
        Next
        mnuSpeichern.Enabled = True
    End Sub

    Private Sub cboModus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboModus.SelectedIndexChanged
        Try
            Dim found As DataRow() = ds.Tables("modus").Select("idModus = " + cboModus.SelectedValue.ToString)
            If found.Count = 0 Then
                lblDetails1.Text = "Gruppenstärke w"
                lblDetails2.Text = "Gruppenstärke m"
                'chkBigdisc.Enabled = False
                pnlAthletik.Enabled = False
            Else
                If found(0)("Alterseinteilung").ToString <> "Altersklasse" Then
                    lblDetails1.Text = "Gruppenstärke w"
                    lblDetails2.Text = "Gruppenstärke m"
                Else
                    lblDetails1.Text = "Altersklassen w"
                    lblDetails2.Text = "Altersklassen m"
                End If
                'chkBigdisc.Enabled = CBool(found(0)("Athletik"))
                pnlAthletik.Enabled = CBool(found(0)("Athletik"))
                mnuSpeichern.Enabled = True
            End If
        Catch ex As Exception
        End Try
    End Sub


End Class