-- CREATION_TIME = 2024-06-12 00:00:00#

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

INSERT IGNORE INTO staaten VALUES(211, 'Bosnien', 'Bosnien und Herzegowina', 'BIH', 'BA', 'ba.png');

ALTER TABLE verein CHANGE Vereinsname Vereinsname VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL; 

ALTER TABLE notizen CHANGE Notiz Notiz TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci; 

ALTER TABLE results 
	CHANGE Reissen Reissen DOUBLE(8,4) NULL DEFAULT NULL,
	CHANGE Reissen2 Reissen2 DOUBLE(8,4) NULL DEFAULT NULL,
	CHANGE Stossen Stossen DOUBLE(8,4) NULL DEFAULT NULL,
	CHANGE Stossen2 Stossen2 DOUBLE(8,4) NULL DEFAULT NULL,
	CHANGE Heben Heben DOUBLE(8,4) NULL DEFAULT NULL,
	CHANGE Wertung2 Wertung2 DOUBLE(8,4) NULL DEFAULT NULL,
	CHANGE Gesamt Gesamt DOUBLE(8,4) NULL DEFAULT NULL;

ALTER TABLE wettkampf_details 
	DROP IF EXISTS Faktor_m, 
    DROP IF EXISTS Faktor_w,
	ADD Faktor_m DOUBLE(7,4) NOT NULL DEFAULT '1.0000' COMMENT 'Faktor, mit dem die Wertung multipliziert wird',
	ADD Faktor_w DOUBLE(7,4) NOT NULL DEFAULT '1.0000' COMMENT 'Faktor, mit dem die Wertung multipliziert wird';
	
DROP TABLE IF EXISTS wettkampf_comment;
CREATE TABLE wettkampf_comment (
  Wettkampf int(11) NOT NULL,
  Kommentar varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE wettkampf_comment ADD PRIMARY KEY (Wettkampf);

DROP TABLE IF EXISTS faktoren;
CREATE TABLE faktoren (
  sex varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  Faktor double(11,10) NOT NULL COMMENT 'Faktor der Sinclair-Formel',
  minKG double(6,3) NOT NULL COMMENT 'minimales KG in Sinclair-Formel',
  maxKG double(6,3) NOT NULL COMMENT 'maximales KG in Sinclair-Formel',
  maxSC double(7,6) NOT NULL COMMENT 'maximaler Sinclair-Faktor'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='2024';
INSERT INTO faktoren (sex, Faktor, minKG, maxKG, maxSC) VALUES
('m', 0.7227625210, 32.000, 193.609, 2.765241),
('w', 0.7870043410, 28.000, 153.757, 2.695177);
ALTER TABLE faktoren ADD PRIMARY KEY (sex);

CREATE TABLE IF NOT EXISTS wertung_bezeichnung (
  Platzierung int(1) NOT NULL,
  International tinyint(1) NOT NULL,
  Bezeichnung varchar(25) NOT NULL,
  Headline varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
INSERT INTO wertung_bezeichnung (Platzierung, International, Bezeichnung, Headline) VALUES
(1, 0, '', 'BESTENLISTE'),
(1, 1, '', 'BEST TOTAL'),
(2, 0, 'GRUPPE', 'BESTE IN '),
(2, 1, 'GROUP', 'BEST OF '),
(3, 0, 'ALTERSKLASSE', 'BESTE IN '),
(3, 1, 'AGE GROUP', 'BEST OF '),
(4, 0, 'GEWICHTSKLASSE', 'BESTE IN '),
(4, 1, 'CATERGORY', 'BEST OF '),
(5, 0, '', 'BESTE WEIBLICH'),
(5, 1, '', 'BEST FEMALE'),
(9, 0, 'ALTERSKLASSE', 'BESTE IN '),
(9, 1, 'AGE GROUP', 'BEST OF ');
ALTER TABLE `wertung_bezeichnung`
  ADD PRIMARY KEY (`Platzierung`,`International`);

DROP TABLE IF EXISTS relativabzug;
CREATE TABLE relativabzug (
  Gewicht double(4,1) NOT NULL,
  m double(4,1) NOT NULL,
  w double(4,1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO relativabzug (Gewicht, m, w) VALUES
(31, 22.5, 12.5),
(32, 23.0, 12.5),
(33, 23.5, 12.5),
(34, 24.0, 12.5),
(35, 24.5, 12.5),
(36, 25.0, 12.5),
(37, 25.5, 12.5),
(38, 26.0, 12.5),
(39, 26.5, 12.5),
(40, 27.0, 12.5),
(41, 27.5, 13.0),
(42, 28.0, 13.0),
(43, 28.5, 13.5),
(44, 29.0, 13.5),
(45, 29.5, 14.0),
(46, 30.0, 14.0),
(47, 30.5, 14.5),
(48, 31.0, 15.0),
(49, 32.0, 15.5),
(50, 33.0, 16.0),
(51, 34.5, 16.5),
(52, 36.0, 17.0),
(53, 37.0, 17.5),
(54, 38.5, 18.5),
(55, 40.0, 19.5),
(56, 42.0, 20.5),
(57, 44.0, 21.5),
(58, 46.0, 22.5),
(59, 48.0, 23.5),
(60, 50.0, 25.0),
(61, 52.0, 26.5),
(62, 54.0, 27.5),
(63, 56.0, 28.5),
(64, 57.5, 29.5),
(65, 59.0, 31.0),
(66, 60.5, 32.0),
(67, 62.0, 33.0),
(68, 63.5, 34.0),
(69, 65.0, 35.0),
(70, 66.5, 36.0),
(71, 68.0, 37.0),
(72, 69.5, 38.0),
(73, 70.5, 39.0),
(74, 71.5, 39.5),
(75, 72.5, 40.0),
(76, 74.0, 40.5),
(77, 75.5, 41.0),
(78, 77.0, 41.5),
(79, 78.0, 42.0),
(79.1, 0.0, 42.0),
(80, 0.0, 42.5),
(81, 0.0, 43.0),
(82, 0.0, 43.5),
(83, 0.0, 44.0),
(84, 0.0, 44.5),
(85, 0.0, 44.5),
(86, 0.0, 45.0),
(87, 0.0, 45.5),
(88, 0.0, 46.0),
(89, 0.0, 46.0),
(90, 0.0, 46.5),
(91, 0.0, 47.0),
(92, 0.0, 47.5),
(93, 0.0, 47.5),
(94, 0.0, 48.0),
(95, 0.0, 48.5),
(95.5, 0.0, 48.5),
(95.9, 95.0, 48.5),
(96, 95.5, 48.5),
(97, 96.0, 49.0),
(98, 96.5, 49.5),
(99, 97.0, 49.5),
(100, 97.5, 50.0),
(101, 98.5, 50.5),
(102, 99.5, 50.5),
(103, 100.5, 51.0),
(104, 101.0, 51.0),
(105, 102.0, 51.5),
(106, 103.0, 51.5),
(107, 103.5, 52.0),
(108, 103.5, 52.0),
(109, 104.0, 52.5),
(110, 104.0, 52.5),
(111, 104.0, 53.0),
(112, 104.5, 53.0),
(113, 104.5, 53.5),
(114, 105.0, 53.5),
(115, 105.0, 54.0),
(116, 105.5, 54.0),
(117, 106.0, 54.5),
(118, 106.5, 54.5),
(119, 107.0, 55.0),
(120, 107.5, 55.0),
(121, 108.0, 55.5),
(122, 108.5, 55.5),
(123, 109.0, 56.0),
(124, 109.5, 56.0),
(125, 110.0, 56.5),
(126, 110.5, 56.5),
(127, 111.0, 57.0),
(128, 111.5, 57.0),
(129, 112.0, 57.5),
(130, 112.5, 57.5),
(131, 113.0, 58.0),
(132, 113.5, 58.0),
(133, 114.0, 58.5),
(134, 114.5, 58.5),
(135, 115.0, 59.0),
(136, 115.5, 59.0),
(137, 116.0, 59.5),
(138, 116.5, 59.5),
(139, 117.0, 60.0),
(140, 117.5, 60.0),
(141, 118.0, 60.5),
(142, 118.5, 60.5),
(143, 119.0, 61.0),
(144, 119.5, 61.0),
(145, 120.0, 61.5),
(146, 120.5, 61.5),
(147, 121.0, 62.0),
(148, 121.5, 62.0),
(149, 122.0, 62.5),
(150, 122.5, 62.5),
(151, 123.0, 63.0),
(152, 123.5, 63.0),
(153, 124.0, 63.5),
(154, 124.5, 63.5),
(155, 125.0, 64.0),
(156, 125.5, 64.0),
(157, 126.0, 64.5),
(158, 126.5, 64.5),
(159, 127.0, 65.0),
(160, 127.5, 65.0);
ALTER TABLE relativabzug ADD PRIMARY KEY (Gewicht);

COMMIT;
