﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Drucken_Meldeliste
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Drucken_Meldeliste))
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.chkNewPage = New System.Windows.Forms.CheckBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboVorlage = New System.Windows.Forms.ComboBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnVorschau = New System.Windows.Forms.Button()
        Me.chkAnwesend = New System.Windows.Forms.CheckBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.grpGruppierung = New System.Windows.Forms.GroupBox()
        Me.optAK = New System.Windows.Forms.RadioButton()
        Me.optGK = New System.Windows.Forms.RadioButton()
        Me.optAK_GK = New System.Windows.Forms.RadioButton()
        Me.optSex = New System.Windows.Forms.RadioButton()
        Me.grpLayout = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.nudColCount = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.pnlDruckmodus = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.optDownAcross = New System.Windows.Forms.RadioButton()
        Me.optAcrossDown = New System.Windows.Forms.RadioButton()
        Me.pnlOrientierung = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.optQuer = New System.Windows.Forms.RadioButton()
        Me.optHoch = New System.Windows.Forms.RadioButton()
        Me.grpGruppierung.SuspendLayout()
        Me.grpLayout.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.nudColCount, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDruckmodus.SuspendLayout()
        Me.pnlOrientierung.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnEdit.Location = New System.Drawing.Point(333, 230)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(66, 24)
        Me.btnEdit.TabIndex = 66
        Me.btnEdit.Text = "Bearbeiten"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'chkNewPage
        '
        Me.chkNewPage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkNewPage.AutoSize = True
        Me.chkNewPage.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkNewPage.Location = New System.Drawing.Point(24, 177)
        Me.chkNewPage.Name = "chkNewPage"
        Me.chkNewPage.Size = New System.Drawing.Size(212, 18)
        Me.chkNewPage.TabIndex = 64
        Me.chkNewPage.Text = "jede Gruppe auf neuer Seite beginnen"
        Me.chkNewPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkNewPage.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnSearch.Location = New System.Drawing.Point(303, 230)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(24, 24)
        Me.btnSearch.TabIndex = 63
        Me.btnSearch.Text = "..."
        Me.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label6.Location = New System.Drawing.Point(25, 214)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 13)
        Me.Label6.TabIndex = 62
        Me.Label6.Text = "Druck-Vorlage"
        '
        'cboVorlage
        '
        Me.cboVorlage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboVorlage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVorlage.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboVorlage.FormattingEnabled = True
        Me.cboVorlage.Location = New System.Drawing.Point(22, 232)
        Me.cboVorlage.Name = "cboVorlage"
        Me.cboVorlage.Size = New System.Drawing.Size(275, 21)
        Me.cboVorlage.Sorted = True
        Me.cboVorlage.TabIndex = 61
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Location = New System.Drawing.Point(299, 90)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(100, 28)
        Me.btnCancel.TabIndex = 60
        Me.btnCancel.Text = "&Beenden"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnPrint.Location = New System.Drawing.Point(299, 56)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(100, 28)
        Me.btnPrint.TabIndex = 59
        Me.btnPrint.Tag = "Print"
        Me.btnPrint.Text = "&Drucken"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnVorschau
        '
        Me.btnVorschau.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVorschau.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnVorschau.Location = New System.Drawing.Point(299, 22)
        Me.btnVorschau.Name = "btnVorschau"
        Me.btnVorschau.Size = New System.Drawing.Size(100, 28)
        Me.btnVorschau.TabIndex = 58
        Me.btnVorschau.Tag = "Show"
        Me.btnVorschau.Text = "&Vorschau"
        Me.btnVorschau.UseVisualStyleBackColor = True
        '
        'chkAnwesend
        '
        Me.chkAnwesend.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkAnwesend.AutoSize = True
        Me.chkAnwesend.BackColor = System.Drawing.SystemColors.Control
        Me.chkAnwesend.Checked = True
        Me.chkAnwesend.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAnwesend.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkAnwesend.Location = New System.Drawing.Point(24, 153)
        Me.chkAnwesend.Name = "chkAnwesend"
        Me.chkAnwesend.Size = New System.Drawing.Size(212, 18)
        Me.chkAnwesend.TabIndex = 52
        Me.chkAnwesend.Text = "  abgemeldete Teilnehmer ausblenden"
        Me.chkAnwesend.UseVisualStyleBackColor = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Title = "Druckvorlagen suchen"
        '
        'grpGruppierung
        '
        Me.grpGruppierung.Controls.Add(Me.optAK)
        Me.grpGruppierung.Controls.Add(Me.optGK)
        Me.grpGruppierung.Controls.Add(Me.optAK_GK)
        Me.grpGruppierung.Controls.Add(Me.optSex)
        Me.grpGruppierung.Location = New System.Drawing.Point(22, 22)
        Me.grpGruppierung.Name = "grpGruppierung"
        Me.grpGruppierung.Size = New System.Drawing.Size(254, 124)
        Me.grpGruppierung.TabIndex = 68
        Me.grpGruppierung.TabStop = False
        Me.grpGruppierung.Text = "Gruppierung"
        '
        'optAK
        '
        Me.optAK.AutoSize = True
        Me.optAK.Checked = True
        Me.optAK.Location = New System.Drawing.Point(17, 47)
        Me.optAK.Name = "optAK"
        Me.optAK.Size = New System.Drawing.Size(141, 17)
        Me.optAK.TabIndex = 71
        Me.optAK.TabStop = True
        Me.optAK.Text = "Geschlecht, Altersklasse"
        Me.optAK.UseVisualStyleBackColor = True
        '
        'optGK
        '
        Me.optGK.AutoSize = True
        Me.optGK.Location = New System.Drawing.Point(17, 97)
        Me.optGK.Name = "optGK"
        Me.optGK.Size = New System.Drawing.Size(159, 17)
        Me.optGK.TabIndex = 70
        Me.optGK.Text = "Geschlecht, Gewichtsklasse"
        Me.optGK.UseVisualStyleBackColor = True
        '
        'optAK_GK
        '
        Me.optAK_GK.AutoSize = True
        Me.optAK_GK.Location = New System.Drawing.Point(17, 72)
        Me.optAK_GK.Name = "optAK_GK"
        Me.optAK_GK.Size = New System.Drawing.Size(221, 17)
        Me.optAK_GK.TabIndex = 69
        Me.optAK_GK.Text = "Geschlecht, Altersklasse, Gewichtsklasse"
        Me.optAK_GK.UseVisualStyleBackColor = True
        '
        'optSex
        '
        Me.optSex.AutoSize = True
        Me.optSex.Location = New System.Drawing.Point(17, 22)
        Me.optSex.Name = "optSex"
        Me.optSex.Size = New System.Drawing.Size(97, 17)
        Me.optSex.TabIndex = 68
        Me.optSex.Text = "nur Geschlecht"
        Me.optSex.UseVisualStyleBackColor = True
        '
        'grpLayout
        '
        Me.grpLayout.Controls.Add(Me.Panel1)
        Me.grpLayout.Controls.Add(Me.pnlDruckmodus)
        Me.grpLayout.Controls.Add(Me.pnlOrientierung)
        Me.grpLayout.Location = New System.Drawing.Point(354, 33)
        Me.grpLayout.Name = "grpLayout"
        Me.grpLayout.Size = New System.Drawing.Size(254, 174)
        Me.grpLayout.TabIndex = 69
        Me.grpLayout.TabStop = False
        Me.grpLayout.Text = "Druck-Layout"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.nudColCount)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Location = New System.Drawing.Point(8, 133)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(235, 35)
        Me.Panel1.TabIndex = 76
        '
        'nudColCount
        '
        Me.nudColCount.Location = New System.Drawing.Point(180, 8)
        Me.nudColCount.Maximum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.nudColCount.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudColCount.Name = "nudColCount"
        Me.nudColCount.Size = New System.Drawing.Size(44, 20)
        Me.nudColCount.TabIndex = 70
        Me.nudColCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudColCount.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(166, 18)
        Me.Label3.TabIndex = 70
        Me.Label3.Text = "Anzahl Spalten im Formular"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label3.UseCompatibleTextRendering = True
        '
        'pnlDruckmodus
        '
        Me.pnlDruckmodus.Controls.Add(Me.Label1)
        Me.pnlDruckmodus.Controls.Add(Me.optDownAcross)
        Me.pnlDruckmodus.Controls.Add(Me.optAcrossDown)
        Me.pnlDruckmodus.Location = New System.Drawing.Point(8, 75)
        Me.pnlDruckmodus.Name = "pnlDruckmodus"
        Me.pnlDruckmodus.Size = New System.Drawing.Size(235, 57)
        Me.pnlDruckmodus.TabIndex = 75
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 28)
        Me.Label1.TabIndex = 70
        Me.Label1.Text = "Druckmodus der Spalten"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label1.UseCompatibleTextRendering = True
        '
        'optDownAcross
        '
        Me.optDownAcross.Appearance = System.Windows.Forms.Appearance.Button
        Me.optDownAcross.BackgroundImage = CType(resources.GetObject("optDownAcross.BackgroundImage"), System.Drawing.Image)
        Me.optDownAcross.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.optDownAcross.Checked = True
        Me.optDownAcross.Location = New System.Drawing.Point(160, 6)
        Me.optDownAcross.Name = "optDownAcross"
        Me.optDownAcross.Size = New System.Drawing.Size(65, 45)
        Me.optDownAcross.TabIndex = 70
        Me.optDownAcross.TabStop = True
        Me.optDownAcross.UseVisualStyleBackColor = False
        '
        'optAcrossDown
        '
        Me.optAcrossDown.Appearance = System.Windows.Forms.Appearance.Button
        Me.optAcrossDown.BackgroundImage = CType(resources.GetObject("optAcrossDown.BackgroundImage"), System.Drawing.Image)
        Me.optAcrossDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.optAcrossDown.Location = New System.Drawing.Point(84, 6)
        Me.optAcrossDown.Name = "optAcrossDown"
        Me.optAcrossDown.Size = New System.Drawing.Size(65, 45)
        Me.optAcrossDown.TabIndex = 69
        Me.optAcrossDown.UseVisualStyleBackColor = False
        '
        'pnlOrientierung
        '
        Me.pnlOrientierung.Controls.Add(Me.Label2)
        Me.pnlOrientierung.Controls.Add(Me.optQuer)
        Me.pnlOrientierung.Controls.Add(Me.optHoch)
        Me.pnlOrientierung.Location = New System.Drawing.Point(8, 17)
        Me.pnlOrientierung.Name = "pnlOrientierung"
        Me.pnlOrientierung.Size = New System.Drawing.Size(235, 57)
        Me.pnlOrientierung.TabIndex = 74
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 28)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Orientierung"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label2.UseCompatibleTextRendering = True
        '
        'optQuer
        '
        Me.optQuer.Appearance = System.Windows.Forms.Appearance.Button
        Me.optQuer.BackgroundImage = CType(resources.GetObject("optQuer.BackgroundImage"), System.Drawing.Image)
        Me.optQuer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.optQuer.Location = New System.Drawing.Point(160, 6)
        Me.optQuer.Name = "optQuer"
        Me.optQuer.Size = New System.Drawing.Size(65, 45)
        Me.optQuer.TabIndex = 72
        Me.optQuer.UseVisualStyleBackColor = False
        '
        'optHoch
        '
        Me.optHoch.Appearance = System.Windows.Forms.Appearance.Button
        Me.optHoch.BackgroundImage = CType(resources.GetObject("optHoch.BackgroundImage"), System.Drawing.Image)
        Me.optHoch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.optHoch.Checked = True
        Me.optHoch.Location = New System.Drawing.Point(84, 6)
        Me.optHoch.Name = "optHoch"
        Me.optHoch.Size = New System.Drawing.Size(65, 45)
        Me.optHoch.TabIndex = 71
        Me.optHoch.TabStop = True
        Me.optHoch.UseVisualStyleBackColor = False
        '
        'Drucken_Meldeliste
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(418, 272)
        Me.Controls.Add(Me.grpLayout)
        Me.Controls.Add(Me.grpGruppierung)
        Me.Controls.Add(Me.chkAnwesend)
        Me.Controls.Add(Me.chkNewPage)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cboVorlage)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnVorschau)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(438, 315)
        Me.Name = "Drucken_Meldeliste"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "MELDELISTE DRUCKEN"
        Me.grpGruppierung.ResumeLayout(False)
        Me.grpGruppierung.PerformLayout()
        Me.grpLayout.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.nudColCount, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDruckmodus.ResumeLayout(False)
        Me.pnlOrientierung.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnEdit As Button
    Friend WithEvents chkNewPage As CheckBox
    Friend WithEvents btnSearch As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents cboVorlage As ComboBox
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnPrint As Button
    Friend WithEvents btnVorschau As Button
    Friend WithEvents chkAnwesend As CheckBox
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents grpGruppierung As GroupBox
    Friend WithEvents optGK As RadioButton
    Friend WithEvents optAK_GK As RadioButton
    Friend WithEvents optSex As RadioButton
    Friend WithEvents grpLayout As GroupBox
    Friend WithEvents optAcrossDown As RadioButton
    Friend WithEvents optDownAcross As RadioButton
    Friend WithEvents Label2 As Label
    Friend WithEvents optQuer As RadioButton
    Friend WithEvents optHoch As RadioButton
    Friend WithEvents Label1 As Label
    Friend WithEvents pnlDruckmodus As Panel
    Friend WithEvents pnlOrientierung As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents nudColCount As NumericUpDown
    Friend WithEvents Label3 As Label
    Friend WithEvents optAK As RadioButton
End Class
