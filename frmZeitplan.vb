﻿Imports System.ComponentModel
Imports MySqlConnector

Public Class frmZeitplan

    Private Bereich As String = "Zeitplan"
    'Dim dtTmp As DataTable
    Private dtPlan As DataTable
    Private dv As DataView
    Private WithEvents bs As BindingSource
    Private cmd As MySqlCommand
    Private ListIndex As Integer
    Dim func As New myFunction

    Private Sub frmZeitplan_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If btnSave.Enabled Then
            Using New Centered_MessageBox(Me)
                Dim result = MessageBox.Show("Änderungen vor dem Schließen speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                If result = MsgBoxResult.Cancel Then
                    e.Cancel = True
                ElseIf result = DialogResult.Yes Then
                    btnSave.PerformClick()
                End If
            End Using
        End If
    End Sub
    Private Sub frmZeitplan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        Dim tTip As New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        tTip.SetToolTip(cboDatum, "Ereignisse nur für ausgewähltes Datum anzeigen")
        tTip.SetToolTip(btnClearDate, "Filter für Datum aufheben")

    End Sub
    Private Sub frmZeitplan_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        dtPlan = func.Get_Schedule

        If IsNothing(dtPlan) Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Daten für <" & Wettkampf.Bezeichnung & "> gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Close()
                Return
            End Using
        End If

        For Each row As DataRow In dtPlan.Rows
            If Not IsDBNull(row!Gruppe) AndAlso CInt(row!Gruppe) < 0 Then lstEvents.SetItemChecked(CInt(Math.Abs(CInt(row!Gruppe)) / 10) - 1, True)
        Next

        dtPlan.AcceptChanges()

        dv = New DataView(dtPlan)
        bs = New BindingSource With {.DataSource = dv}
        bs.Sort = "Datum, Beginn, Ende"
        With dgvZeitplan
            .AutoGenerateColumns = False
            .DataSource = bs
            ' Bezeichnung für alle Zeilen mit Wettkampf und Athletik auf ReadOnly setzen, damit die Gruppe geändert werden kann
            For i = 0 To .Rows.Count - 1
                If Not IsDBNull(dv(i)("Gruppe")) AndAlso Not IsDBNull(dv(i)("Bezeichnung")) AndAlso
                                (dv(i)("Bezeichnung").ToString.Contains("Wettkampf") OrElse
                                 dv(i)("Bezeichnung").ToString.Contains("Wiegen") OrElse
                                 dv(i)("Bezeichnung").ToString.Contains("Athletik")) Then
                    .Rows(i).Cells("Bezeichnung").ReadOnly = True
                End If
            Next
        End With

        Dim dvDate = New DataView(dtPlan.DefaultView.ToTable(True, "Datum"))
        With cboDatum
            .FormatString = "dddd, dd.MM.yyyy"
            .DataSource = dvDate
            .DisplayMember = "Datum"
            .ValueMember = "Datum"
            .SelectedIndex = -1
        End With

        lstEvents.SetSelected(0, True)

        btnSave.Enabled = False
        formMain.Change_MainProgressBarValue(0)
        Cursor = Cursors.Default
    End Sub

    Private Sub btnClearDate_Click(sender As Object, e As EventArgs) Handles btnClearDate.Click
        cboDatum.SelectedIndex = -1
        dgvZeitplan.Focus()
    End Sub
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click, mnuClose.Click
        Close()
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click, mnuSave.Click

        Cursor = Cursors.WaitCursor
        Validate()
        bs.EndEdit()

        Using conn As New MySqlConnection(User.ConnString)
            Do
                Dim GroupUpdated = New List(Of Integer)
                Dim GroupChanged = False
                Try
                    If conn.State = ConnectionState.Closed Then conn.Open()

                    cmd = New MySqlCommand("DELETE FROM wettkampf_zeitplan WHERE Wettkampf = " & Wettkampf.ID & ";", conn)
                    cmd.ExecuteNonQuery()

                    For Each row As DataRow In dtPlan.Rows

                        cmd = New MySqlCommand(String.Empty, conn)

                        With cmd.Parameters
                            .AddWithValue("@wk", Wettkampf.ID)
                            .AddWithValue("@group", row!Gruppe)
                            .AddWithValue("@date", row!Datum)
                            .AddWithValue("@beginn", row!Beginn)
                            .AddWithValue("@ende", row!Ende)
                            .AddWithValue("@name", row!Bezeichnung)
                            .AddWithValue("@print", row!Drucken)
                            .AddWithValue("@old", row!GruppeOld)
                        End With

                        If IsDBNull(row!Gruppe) OrElse CInt(row!Gruppe) < 0 Then
                            ' --> in wettkampf_zeitplan einfügen
                            cmd.CommandText = "INSERT INTO wettkampf_zeitplan (Wettkampf, Gruppe, Datum, Beginn, Ende, Bezeichnung, Drucken) VALUES(@wk, @group, @date, @beginn, @ende, @name, @print);"
                        ElseIf Not IsDBNull(row!Gruppe) Then
                            If row!Bezeichnung.ToString.Contains("Wettkampf-Beginn") AndAlso CInt(row!Gruppe) <> CInt(row!GruppeOld) Then
                                If Not IsDBNull(row!TN) Then
                                    ' --> meldung.Gruppe aktualisieren
                                    cmd.CommandText = "UPDATE meldung SET Gruppe = @group *-1 WHERE Wettkampf = @wk AND Gruppe = @old;"
                                    GroupChanged = True
                                End If
                                ' --> gruppen.Gruppe aktualisieren
                                cmd.CommandText += "UPDATE gruppen SET Gruppe = @group *-1 WHERE Wettkampf = @wk AND Gruppe = @old;"
                                GroupUpdated.Add(CInt(row!Gruppe))
                            Else
                                ' --> gruppen.(Datum, Beginn, Ende) nacheinander überprüfen und ggf. aktualisieren
                                If Not IsDBNull(row!Bezeichnung) Then
                                    Dim values = String.Empty
                                    If row!Bezeichnung.ToString.Contains("Wettkampf-Beginn") Then
                                        values = "Datum = @date, Reissen = @beginn"
                                    ElseIf row!Bezeichnung.ToString.Contains("Wiegen") Then
                                        values = "DatumW = @date, WiegenBeginn = @beginn, WiegenEnde = @ende"
                                    ElseIf row!Bezeichnung.ToString.Contains("Athletik") Then
                                        values = "DatumA = @date, Athletik = @beginn"
                                    End If
                                    If Not String.IsNullOrEmpty(values) Then cmd.CommandText = "UPDATE gruppen SET " & values &
                                        " WHERE Wettkampf = @wk AND Gruppe = " & If(GroupUpdated.Contains(CInt(row!Gruppe)), "@group", "@old") & ";"
                                End If
                            End If
                        End If

                        If Not String.IsNullOrEmpty(cmd.CommandText) Then cmd.ExecuteNonQuery()
                    Next

                    If GroupChanged Then
                        cmd = New MySqlCommand("UPDATE meldung SET Gruppe = Abs(Gruppe) WHERE Wettkampf = " & Wettkampf.ID & " AND Gruppe < 0;", conn)
                        cmd.ExecuteNonQuery()
                        cmd = New MySqlCommand("UPDATE gruppen SET Gruppe = Abs(Gruppe) WHERE Wettkampf = " & Wettkampf.ID & " AND Gruppe < 0;", conn)
                        cmd.ExecuteNonQuery()
                    End If

                    ConnError = False

                Catch ex As Exception
                    If MySQl_Error(Me, "Zeitplan:Save / " & ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                End Try
            Loop While ConnError
        End Using

        If Not ConnError Then
            ' --> GruppeOld mit Gruppe ersetzen (für eventuelle weitere Änderungen)
            For Each row As DataRowView In dv
                If Not IsDBNull(row!GruppeOld) Then row!GruppeOld = row!Gruppe
            Next
            bs.EndEdit()
            dtPlan.AcceptChanges()
            btnSave.Enabled = False
        End If

        Cursor = Cursors.Default
    End Sub
    Private Sub btnSave_EnabledChanged(sender As Object, e As EventArgs) Handles btnSave.EnabledChanged
        mnuSave.Enabled = btnSave.Enabled
    End Sub
    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click, mnuPrint.Click
        Cursor = Cursors.WaitCursor
        Try
            With Drucken_Meldeliste
                .Text = "ZEITPLAN DRUCKEN  - " + Wettkampf.Bezeichnung
                .Location = New Point(Left + (Width - .Width) \ 2, Top + (Height - .Height) \ 2)
                .Tag = dv.ToTable
                .ShowDialog(Me)
                .Dispose()
            End With
        Catch ex As Exception
            LogMessage("frmZeitplan: btnPrint: " & ex.Message, ex.StackTrace)
        End Try
        Cursor = DefaultCursor
    End Sub
    Private Sub btnPrint_EnabledChanged(sender As Object, e As EventArgs) Handles btnPrint.EnabledChanged
        mnuPrint.Enabled = btnPrint.Enabled
    End Sub

    Private Sub dgvZeitplan_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvZeitplan.CellClick
        If e.RowIndex > -1 AndAlso e.ColumnIndex > -1 Then
            Dim rect = dgvZeitplan.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, False)
            Select Case dgvZeitplan.Columns(e.ColumnIndex).Name
                Case "Datum"
                    With dtpDatum
                        .Left = rect.Left + dgvZeitplan.Left + 1
                        .Top = rect.Top + dgvZeitplan.Top + 1
                        Try
                            .Value = CDate(dgvZeitplan.CurrentCell.Value)
                        Catch ex As Exception
                        End Try
                        .Visible = True
                        .Focus()
                        dtpZeit.Visible = False
                    End With
                Case "Beginn", "Ende"
                    With dtpZeit
                        .Left = rect.Left + dgvZeitplan.Left + 1
                        .Top = rect.Top + dgvZeitplan.Top + 1
                        Try
                            .Value = CDate("01.01.1753 " & CDate(dgvZeitplan.CurrentCell.Value).ToShortTimeString)
                        Catch ex As Exception
                        End Try
                        .Visible = True
                        .Focus()
                        dtpDatum.Visible = False
                    End With
            End Select
        End If
    End Sub
    Private Sub dgvZeitplan_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvZeitplan.CellEndEdit
        If dgvZeitplan.CurrentCell.IsInEditMode Then btnSave.Enabled = True
    End Sub
    Private Sub dgvZeitplan_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvZeitplan.CellMouseDown
        If e.Button = MouseButtons.Right Then
            bs.Position = e.RowIndex
            dgvZeitplan.ContextMenuStrip = ContextMenuStrip1
        End If
    End Sub
    Private Sub dgvZeitplan_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvZeitplan.DataError
        Try
        Catch ex As Exception
        End Try
    End Sub

    '' Kontextmenü
    Private Sub ContextMenuStrip_VisibleChanged(sender As Object, e As EventArgs) Handles ContextMenuStrip1.VisibleChanged
        If Not ContextMenuStrip1.Visible Then dgvZeitplan.ContextMenuStrip = ContextMenuStrip2
    End Sub
    Private Sub cmnuAdd_Click(sender As Object, e As EventArgs) Handles cmnuAdd.Click
        dtPlan.Rows.Add({DBNull.Value, dv(bs.Position)("Datum"), DateAdd(DateInterval.Second, -1, CDate(dv(bs.Position)("Beginn"))), DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, True, DBNull.Value})
        bs.EndEdit()
        bs.MovePrevious()
    End Sub
    Private Sub cmnuDelete_Click(sender As Object, e As EventArgs) Handles cmnuDelete.Click
        Using New Centered_MessageBox(Me)
            If MessageBox.Show("Dieses Ereignis entfernen?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                dv(bs.Position).Delete()
                bs.EndEdit()
            End If
        End Using
    End Sub

    '' Standard-Events
    Private Sub chkSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkSelectAll.CheckedChanged
        If Not chkSelectAll.Focused Then Return
        For i = 0 To lstEvents.Items.Count - 1
            lstEvents.SetItemChecked(i, chkSelectAll.Checked)
        Next
        lstEvents.Focus()
    End Sub
    Private Sub lstEvents_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles lstEvents.ItemCheck
        With lstEvents
            If .Focused Then
                If .CheckedItems.Count - e.CurrentValue + e.NewValue > 0 AndAlso .CheckedItems.Count - e.CurrentValue + e.NewValue < .Items.Count Then
                    chkSelectAll.CheckState = CheckState.Indeterminate
                Else
                    chkSelectAll.CheckState = CType(Math.Abs(CInt(.CheckedItems.Count - e.CurrentValue + e.NewValue > 0)), CheckState)
                End If
            End If
        End With
    End Sub
    Private Sub lstEvents_MouseDown(sender As Object, e As MouseEventArgs) Handles lstEvents.MouseDown
        If e.Button = MouseButtons.Left AndAlso e.X < 15 Then
            With lstEvents
                Dim index = .IndexFromPoint(e.X, e.Y)
                If Not ListIndex = index Then
                    ListIndex = index
                    Dim checked = .GetItemChecked(index)
                    .SetItemChecked(index, Not checked)
                End If
            End With
        End If
    End Sub
    Private Sub lstEvents_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstEvents.SelectedIndexChanged
        ListIndex = lstEvents.SelectedIndex
    End Sub
    Private Sub pnlEvents_GotFocus(sender As Object, e As EventArgs) Handles pnlEvents.GotFocus
        lstEvents.Focus()
    End Sub
    Private Sub btnAddEvents_Click(sender As Object, e As EventArgs) Handles btnAddEvents.Click
        pnlEvents.Visible = Not pnlEvents.Visible
        If pnlEvents.Visible Then
            pnlEvents.Focus()
        Else
            dgvZeitplan.Focus()
        End If
    End Sub
    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        For i = 0 To lstEvents.Items.Count - 1
            Dim pos = bs.Find("Gruppe", -(i + 1) * 10)
            If pos = -1 AndAlso lstEvents.CheckedIndices.Contains(i) Then
                dtPlan.Rows.Add({-(i + 1) * 10, If(i = 1, If(IsDBNull(Glob.dtWK(0)("DatumBis")), Wettkampf.Datum, Glob.dtWK(0)("DatumBis")), Wettkampf.Datum), DBNull.Value, DBNull.Value, lstEvents.Items(i), DBNull.Value, DBNull.Value, True, DBNull.Value})
            ElseIf Not pos = -1 AndAlso Not lstEvents.CheckedIndices.Contains(i) Then
                dv(pos).Delete()
            End If
        Next
        bs.EndEdit()
        pnlEvents.Visible = False
        btnSave.Enabled = True
    End Sub

    Private Sub dtpDatum_KeyDown(sender As Object, e As KeyEventArgs) Handles dtpDatum.KeyDown, dtpZeit.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                e.SuppressKeyPress = True
                dgvZeitplan.Focus()
            Case Keys.Enter
                e.SuppressKeyPress = True
                With dgvZeitplan.CurrentCell
                    If CType(sender, DateTimePicker).Equals(dtpZeit) Then
                        If dgvZeitplan.Columns(.ColumnIndex).Name.Equals("Beginn") AndAlso Not IsDBNull(dgvZeitplan.Rows(.RowIndex).Cells("Ende").Value) Then
                            Dim Diff = CDate("01.01.0001 " + CType(sender, DateTimePicker).Text) - CDate(.Value)
                            dgvZeitplan.Rows(.RowIndex).Cells("Ende").Value = CDate(dgvZeitplan.Rows(.RowIndex).Cells("Ende").Value) + Diff
                        End If
                        .Value = CDate("01.01.0001 " + CType(sender, DateTimePicker).Text)
                    Else
                        .Value = CType(sender, DateTimePicker).Text
                    End If
                End With
                bs.EndEdit()
                dgvZeitplan.Focus()
                btnSave.Enabled = True
        End Select
    End Sub

    Private Sub cboDatum_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDatum.SelectedIndexChanged
        Try
            bs.Filter = "Datum = '" & cboDatum.SelectedValue.ToString & "'"
        Catch ex As Exception
            bs.Filter = String.Empty
        End Try
    End Sub

    Private Sub Controls_GotFocus(sender As Object, e As EventArgs) Handles dgvZeitplan.GotFocus, btnClose.GotFocus, btnPrint.GotFocus, btnSave.GotFocus,
            cboDatum.GotFocus, btnClearDate.GotFocus, btnAddEvents.GotFocus

        dtpDatum.Visible = False
        dtpZeit.Visible = False
        If TypeOf sender Is Button AndAlso Not CType(sender, Button).Equals(btnAddEvents) Then pnlEvents.Visible = False

    End Sub

    Private Sub bs_ListChanged(sender As Object, e As ListChangedEventArgs) Handles bs.ListChanged
        Try
            Dim n = 1
            For i = 0 To dv.Count - 1
                If Not IsDBNull(dv(i)("GruppeOld")) AndAlso dv(i)("Bezeichnung").ToString.Contains("Wettkampf-Beginn") Then
                    If Not CInt(dv(i)("Gruppe")) = n Then
                        Dim rows = dtPlan.Select("GruppeOld = " & dv(i)("GruppeOld").ToString)
                        For Each row As DataRow In rows
                            Dim txt = row!Bezeichnung.ToString.Replace(dv(i)("Gruppe").ToString, n.ToString)
                            row!Bezeichnung = txt
                            row!Gruppe = n
                        Next
                    End If
                    n += 1
                End If
            Next
        Catch ex As Exception
        End Try

        bs.EndEdit()
        dgvZeitplan.Refresh()
    End Sub

End Class