﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmZeitplan
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DateiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem()
        Me.dgvZeitplan = New System.Windows.Forms.DataGridView()
        Me.Drucken = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Datum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Beginn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ende = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gewichtsklassen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmnuAdd = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmnuDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboDatum = New System.Windows.Forms.ComboBox()
        Me.btnAddEvents = New System.Windows.Forms.Button()
        Me.btnClearDate = New System.Windows.Forms.Button()
        Me.pnlEvents = New System.Windows.Forms.Panel()
        Me.chkSelectAll = New System.Windows.Forms.CheckBox()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.lstEvents = New System.Windows.Forms.CheckedListBox()
        Me.dtpZeit = New System.Windows.Forms.DateTimePicker()
        Me.dtpDatum = New System.Windows.Forms.DateTimePicker()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.dgvZeitplan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.pnlEvents.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Enabled = False
        Me.btnSave.Location = New System.Drawing.Point(732, 41)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(93, 25)
        Me.btnSave.TabIndex = 0
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Speichern"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(732, 72)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(93, 25)
        Me.btnClose.TabIndex = 1
        Me.btnClose.TabStop = False
        Me.btnClose.Text = "Schließen"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(732, 208)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(93, 25)
        Me.btnPrint.TabIndex = 2
        Me.btnPrint.TabStop = False
        Me.btnPrint.Text = "Drucken"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DateiToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(837, 24)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'DateiToolStripMenuItem
        '
        Me.DateiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSave, Me.mnuPrint, Me.ToolStripMenuItem1, Me.mnuClose})
        Me.DateiToolStripMenuItem.Name = "DateiToolStripMenuItem"
        Me.DateiToolStripMenuItem.Size = New System.Drawing.Size(46, 20)
        Me.DateiToolStripMenuItem.Text = "&Datei"
        '
        'mnuSave
        '
        Me.mnuSave.Name = "mnuSave"
        Me.mnuSave.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSave.Size = New System.Drawing.Size(168, 22)
        Me.mnuSave.Text = "&Speichern"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.mnuPrint.Size = New System.Drawing.Size(168, 22)
        Me.mnuPrint.Text = "&Drucken"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(165, 6)
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(168, 22)
        Me.mnuClose.Text = "&Beenden"
        '
        'dgvZeitplan
        '
        Me.dgvZeitplan.AllowDrop = True
        Me.dgvZeitplan.AllowUserToAddRows = False
        Me.dgvZeitplan.AllowUserToDeleteRows = False
        Me.dgvZeitplan.AllowUserToResizeColumns = False
        Me.dgvZeitplan.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        Me.dgvZeitplan.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvZeitplan.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvZeitplan.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvZeitplan.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvZeitplan.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvZeitplan.ColumnHeadersHeight = 24
        Me.dgvZeitplan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvZeitplan.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Drucken, Me.Datum, Me.Beginn, Me.Ende, Me.Bezeichnung, Me.TN, Me.Gewichtsklassen})
        Me.dgvZeitplan.ContextMenuStrip = Me.ContextMenuStrip2
        Me.dgvZeitplan.Location = New System.Drawing.Point(12, 68)
        Me.dgvZeitplan.MultiSelect = False
        Me.dgvZeitplan.Name = "dgvZeitplan"
        Me.dgvZeitplan.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvZeitplan.RowHeadersVisible = False
        Me.dgvZeitplan.RowHeadersWidth = 27
        Me.dgvZeitplan.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvZeitplan.RowTemplate.Height = 24
        Me.dgvZeitplan.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvZeitplan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvZeitplan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvZeitplan.Size = New System.Drawing.Size(697, 400)
        Me.dgvZeitplan.TabIndex = 0
        '
        'Drucken
        '
        Me.Drucken.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Drucken.DataPropertyName = "Drucken"
        Me.Drucken.FillWeight = 25.0!
        Me.Drucken.HeaderText = "..."
        Me.Drucken.Name = "Drucken"
        Me.Drucken.ToolTipText = "ausgewählte Ereignisse werden gedruckt"
        Me.Drucken.Width = 25
        '
        'Datum
        '
        Me.Datum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Datum.DataPropertyName = "Datum"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Format = "d"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Datum.DefaultCellStyle = DataGridViewCellStyle3
        Me.Datum.FillWeight = 70.0!
        Me.Datum.HeaderText = "Datum"
        Me.Datum.Name = "Datum"
        Me.Datum.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Datum.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Datum.Width = 70
        '
        'Beginn
        '
        Me.Beginn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Beginn.DataPropertyName = "Beginn"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Format = "t"
        Me.Beginn.DefaultCellStyle = DataGridViewCellStyle4
        Me.Beginn.FillWeight = 50.0!
        Me.Beginn.HeaderText = "Beginn"
        Me.Beginn.Name = "Beginn"
        Me.Beginn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Beginn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Beginn.Width = 50
        '
        'Ende
        '
        Me.Ende.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Ende.DataPropertyName = "Ende"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.Format = "t"
        Me.Ende.DefaultCellStyle = DataGridViewCellStyle5
        Me.Ende.FillWeight = 50.0!
        Me.Ende.HeaderText = "Ende"
        Me.Ende.Name = "Ende"
        Me.Ende.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Ende.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Ende.Width = 50
        '
        'Bezeichnung
        '
        Me.Bezeichnung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Bezeichnung.DataPropertyName = "Bezeichnung"
        Me.Bezeichnung.HeaderText = "Bezeichnung"
        Me.Bezeichnung.Name = "Bezeichnung"
        Me.Bezeichnung.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'TN
        '
        Me.TN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.TN.DataPropertyName = "TN"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TN.DefaultCellStyle = DataGridViewCellStyle6
        Me.TN.FillWeight = 40.0!
        Me.TN.HeaderText = "TN"
        Me.TN.Name = "TN"
        Me.TN.ReadOnly = True
        Me.TN.Width = 40
        '
        'Gewichtsklassen
        '
        Me.Gewichtsklassen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gewichtsklassen.DataPropertyName = "Gewichtsklassen"
        Me.Gewichtsklassen.FillWeight = 140.0!
        Me.Gewichtsklassen.HeaderText = "Gewichtsklassen"
        Me.Gewichtsklassen.Name = "Gewichtsklassen"
        Me.Gewichtsklassen.ReadOnly = True
        Me.Gewichtsklassen.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Gewichtsklassen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Gewichtsklassen.Width = 140
        '
        'ContextMenuStrip2
        '
        Me.ContextMenuStrip2.Name = "ContextMenuStrip2"
        Me.ContextMenuStrip2.Size = New System.Drawing.Size(61, 4)
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmnuAdd, Me.ToolStripMenuItem2, Me.cmnuDelete})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(170, 54)
        '
        'cmnuAdd
        '
        Me.cmnuAdd.Name = "cmnuAdd"
        Me.cmnuAdd.Size = New System.Drawing.Size(169, 22)
        Me.cmnuAdd.Text = "neues Ereignis"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(166, 6)
        '
        'cmnuDelete
        '
        Me.cmnuDelete.Name = "cmnuDelete"
        Me.cmnuDelete.Size = New System.Drawing.Size(169, 22)
        Me.cmnuDelete.Text = "Ereignis entfernen"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Datum"
        '
        'cboDatum
        '
        Me.cboDatum.FormatString = "D"
        Me.cboDatum.FormattingEnabled = True
        Me.cboDatum.Location = New System.Drawing.Point(57, 36)
        Me.cboDatum.Name = "cboDatum"
        Me.cboDatum.Size = New System.Drawing.Size(134, 21)
        Me.cboDatum.Sorted = True
        Me.cboDatum.TabIndex = 6
        Me.cboDatum.TabStop = False
        '
        'btnAddEvents
        '
        Me.btnAddEvents.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddEvents.Location = New System.Drawing.Point(388, 34)
        Me.btnAddEvents.Name = "btnAddEvents"
        Me.btnAddEvents.Size = New System.Drawing.Size(206, 25)
        Me.btnAddEvents.TabIndex = 7
        Me.btnAddEvents.TabStop = False
        Me.btnAddEvents.Text = "Standard-Ereignisse"
        Me.btnAddEvents.UseVisualStyleBackColor = True
        '
        'btnClearDate
        '
        Me.btnClearDate.BackgroundImage = Global.Gewichtheben.My.Resources.Resources.Clearwindowcontent_63041
        Me.btnClearDate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnClearDate.Location = New System.Drawing.Point(197, 35)
        Me.btnClearDate.Name = "btnClearDate"
        Me.btnClearDate.Size = New System.Drawing.Size(26, 23)
        Me.btnClearDate.TabIndex = 8
        Me.btnClearDate.TabStop = False
        Me.btnClearDate.UseVisualStyleBackColor = True
        '
        'pnlEvents
        '
        Me.pnlEvents.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.pnlEvents.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlEvents.Controls.Add(Me.chkSelectAll)
        Me.pnlEvents.Controls.Add(Me.btnOK)
        Me.pnlEvents.Controls.Add(Me.lstEvents)
        Me.pnlEvents.Location = New System.Drawing.Point(391, 58)
        Me.pnlEvents.Name = "pnlEvents"
        Me.pnlEvents.Size = New System.Drawing.Size(200, 106)
        Me.pnlEvents.TabIndex = 9
        Me.pnlEvents.Visible = False
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(5, 73)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(96, 17)
        Me.chkSelectAll.TabIndex = 2
        Me.chkSelectAll.TabStop = False
        Me.chkSelectAll.Text = "alle auswählen"
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnOK.Location = New System.Drawing.Point(126, 74)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(64, 21)
        Me.btnOK.TabIndex = 1
        Me.btnOK.TabStop = False
        Me.btnOK.Text = "Fertig"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'lstEvents
        '
        Me.lstEvents.FormattingEnabled = True
        Me.lstEvents.Items.AddRange(New Object() {"Eröffnung", "Abschluss", "Bestätigung der Anfangslasten", "Technische Besprechnung"})
        Me.lstEvents.Location = New System.Drawing.Point(2, 2)
        Me.lstEvents.Name = "lstEvents"
        Me.lstEvents.Size = New System.Drawing.Size(194, 64)
        Me.lstEvents.TabIndex = 0
        '
        'dtpZeit
        '
        Me.dtpZeit.CustomFormat = "HH:mm"
        Me.dtpZeit.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpZeit.Location = New System.Drawing.Point(197, 196)
        Me.dtpZeit.Name = "dtpZeit"
        Me.dtpZeit.ShowUpDown = True
        Me.dtpZeit.Size = New System.Drawing.Size(54, 20)
        Me.dtpZeit.TabIndex = 210
        Me.dtpZeit.TabStop = False
        Me.dtpZeit.Visible = False
        '
        'dtpDatum
        '
        Me.dtpDatum.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDatum.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDatum.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDatum.Location = New System.Drawing.Point(97, 196)
        Me.dtpDatum.Name = "dtpDatum"
        Me.dtpDatum.Size = New System.Drawing.Size(92, 20)
        Me.dtpDatum.TabIndex = 209
        Me.dtpDatum.TabStop = False
        Me.dtpDatum.Visible = False
        '
        'frmZeitplan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(837, 480)
        Me.Controls.Add(Me.dtpZeit)
        Me.Controls.Add(Me.dtpDatum)
        Me.Controls.Add(Me.pnlEvents)
        Me.Controls.Add(Me.btnClearDate)
        Me.Controls.Add(Me.btnAddEvents)
        Me.Controls.Add(Me.cboDatum)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvZeitplan)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmZeitplan"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Zeitplan"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.dgvZeitplan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.pnlEvents.ResumeLayout(False)
        Me.pnlEvents.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnSave As Button
    Friend WithEvents btnClose As Button
    Friend WithEvents btnPrint As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents DateiToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents mnuSave As ToolStripMenuItem
    Friend WithEvents mnuPrint As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents mnuClose As ToolStripMenuItem
    Friend WithEvents dgvZeitplan As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents cboDatum As ComboBox
    Friend WithEvents btnAddEvents As Button
    Friend WithEvents btnClearDate As Button
    Friend WithEvents pnlEvents As Panel
    Friend WithEvents chkSelectAll As CheckBox
    Friend WithEvents btnOK As Button
    Friend WithEvents lstEvents As CheckedListBox
    Friend WithEvents dtpZeit As DateTimePicker
    Friend WithEvents dtpDatum As DateTimePicker
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents cmnuDelete As ToolStripMenuItem
    Friend WithEvents cmnuAdd As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents ContextMenuStrip2 As ContextMenuStrip
    Friend WithEvents Drucken As DataGridViewCheckBoxColumn
    Friend WithEvents Datum As DataGridViewTextBoxColumn
    Friend WithEvents Beginn As DataGridViewTextBoxColumn
    Friend WithEvents Ende As DataGridViewTextBoxColumn
    Friend WithEvents Bezeichnung As DataGridViewTextBoxColumn
    Friend WithEvents TN As DataGridViewTextBoxColumn
    Friend WithEvents Gewichtsklassen As DataGridViewTextBoxColumn
End Class
