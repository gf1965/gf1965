﻿
Imports MySqlConnector

Public Class frmAthletik
    Private Bereich As String = "Athletik"
    Private nonNumberEntered As Boolean = False
    Dim cmd As MySqlCommand
    Dim loading As Boolean

    Dim dtDisziplinen As DataTable  ' ausgewählte Disziplinen des WKs
    Dim dtAthletik As DataTable
    Dim dtGruppen As DataTable

    Dim dvAthletik As DataView
    Dim dvResults_A As DataView

    Dim lstTextbox As New List(Of TextBox)
    Dim txtIndex As Integer
    Dim Athletik_Query As String

    Private Sub TextboxNum_NumOnly_KeyDown(sender As Object, e As KeyEventArgs) Handles txtDG1.KeyDown, txtDG2.KeyDown, txtDG3.KeyDown
        With CType(sender, TextBox)
            If e.KeyCode = Keys.Escape Then
                .Text = .Tag.ToString
                e.Handled = True
            ElseIf e.KeyCode = Keys.Enter Then
                If IsNumeric(lstTextbox(txtIndex - 1).Text) OrElse String.IsNullOrEmpty(lstTextbox(txtIndex - 1).Text) Then
                    If Not chkMaxOnly.Enabled OrElse chkMaxOnly.Checked = True Then
                        btnWeiter.PerformClick()
                    ElseIf txtIndex < lstTextbox.Count AndAlso lstTextbox(txtIndex).Enabled Then
                        lstTextbox(txtIndex).Focus()
                    Else
                        btnWeiter.PerformClick()
                    End If
                    e.SuppressKeyPress = True
                End If
            Else
                nonNumberEntered = False
                If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
                    If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                        If e.KeyCode <> Keys.Back Then
                            If lblEinheit.Text <> "Sekunden" AndAlso (e.KeyCode <> Keys.Oemcomma OrElse e.KeyCode <> Keys.Decimal) Then
                                nonNumberEntered = True
                            End If
                        End If
                    End If
                End If
                If ModifierKeys = Keys.Shift Then
                    nonNumberEntered = True
                End If
            End If
        End With
    End Sub
    Private Sub TextboxNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDG1.KeyPress, txtDG2.KeyPress, txtDG3.KeyPress
        If nonNumberEntered = True Then
            e.Handled = True
        End If
    End Sub

    '' Me
    Private Sub frmAthletik_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        NativeMethods.INI_Write(Bereich, "MaxOnly", CStr(chkMaxOnly.Checked), App_IniFile)
        With formMain
            .mnuClearHeben.Enabled = True
            .mnuClearMeldung.Enabled = True
        End With
    End Sub
    Private Sub frmAthletik_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If btnSave.Enabled Then
            Using New Centered_MessageBox(Me)
                Dim result = MessageBox.Show("Änderungen vor dem Beenden speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                If result = MsgBoxResult.Cancel Then
                    e.Cancel = True
                ElseIf result = DialogResult.Yes Then
                    btnSave.PerformClick()
                End If
            End Using
        End If
    End Sub
    Private Sub frmAthletik_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        Dim x As String
        x = NativeMethods.INI_Read(Bereich, "MaxOnly", App_IniFile)
        If x <> "?" Then chkMaxOnly.Checked = CBool(x)
        lstTextbox.AddRange({txtDG1, txtDG2, txtDG3})
        With formMain
            .mnuClearHeben.Enabled = False
            .mnuClearMeldung.Enabled = False
        End With
    End Sub
    Private Sub frmAthletik_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        loading = True
        Dim Query = String.Empty
        Athletik_Query = "SELECT m.Teilnehmer, m.Gruppe, m.attend, wal.Disziplin, al.DG_1, al.DG_2, al.DG_3, a.Nachname, a.Vorname, a.Geschlecht Sex, Year(a.Geburtstag) Jahrgang, " &
                         "m.Startnummer, m.Wiegen, ar.Resultat, ar.Platz, r.Athletik " &
                         "FROM meldung m " &
                         "LEFT JOIN wettkampf_athletik wal On wal.Wettkampf = m.Wettkampf " &
                         "LEFT JOIN (Select Wettkampf, Teilnehmer, Disziplin, " &
                         "SUM(IF(Durchgang=1,Wert,NULL)) DG_1, " &
                         "SUM(If(Durchgang=2,Wert,NULL)) DG_2, " &
                         "SUM(If(Durchgang=3,Wert,NULL)) DG_3 " &
                         "FROM athletik " &
                         "WHERE Wettkampf = " & Wettkampf.ID &
                         " GROUP BY Teilnehmer, Disziplin) al On al.Wettkampf = m.Wettkampf And m.Teilnehmer = al.Teilnehmer AND al.Disziplin = wal.Disziplin " &
                         "LEFT JOIN athleten a On m.Teilnehmer = a.idTeilnehmer " &
                         "LEFT JOIN athletik_results ar On ar.Wettkampf = m.Wettkampf And ar.Teilnehmer = m.Teilnehmer And ar.Disziplin = al.Disziplin " &
                         "LEFT JOIN results r On  r.Wettkampf = m.Wettkampf And r.Teilnehmer = m.Teilnehmer " &
                         "WHERE m.Wettkampf = " & Wettkampf.ID & " And (m.attend = True OR IsNull(m.attend)) " &
                         "ORDER BY m.Gruppe, m.Teilnehmer, wal.Disziplin;"

        Dim lstHeber As New List(Of Integer)

        If String.IsNullOrEmpty(User.ConnString) Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Anmeldung an MySQL-Server nicht nöglich." & vbNewLine & "(Keine Anmeldedaten vorhanden.)", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            Close()
            Return
        End If

        Using conn As New MySqlConnection(User.ConnString)
            Do
                Try
                    If Not conn.State = ConnectionState.Open Then conn.Open()

                    ' dtAthletik
                    cmd = New MySqlCommand(Athletik_Query, conn)
                    dtAthletik = New DataTable
                    dtAthletik.Load(cmd.ExecuteReader)

                    ' Source für cboDisziplinen
                    Query = "Select a.Disziplin, d.Bezeichnung, d.Durchgaenge " &
                            "FROM wettkampf_athletik a " &
                            "LEFT JOIN athletik_disziplinen d On a.Disziplin = d.idDisziplin " &
                            "WHERE a.Wettkampf = " & Wettkampf.ID &
                            " ORDER BY a.Disziplin ASC;"
                    cmd = New MySqlCommand(Query, conn)
                    dtDisziplinen = New DataTable
                    dtDisziplinen.Load(cmd.ExecuteReader)

                    ' Tabelle gruppen
                    Query = "Select * from gruppen where Wettkampf = " & Wettkampf.ID & " order by Gruppe asc;"
                    cmd = New MySqlCommand(Query, conn)
                    dtGruppen = New DataTable
                    dtGruppen.Load(cmd.ExecuteReader())

                    ConnError = False
                Catch ex As MySqlException
                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                End Try
            Loop While ConnError
        End Using
        If ConnError Then Close()

        With dtAthletik
            '    Dim cols(2) As DataColumn
            '    cols(0) = .Columns("Gruppe")
            '    cols(1) = .Columns("Teilnehmer")
            '    cols(2) = .Columns("Disziplin")
            '    .PrimaryKey = cols
            .Columns("DG_1").AllowDBNull = True
            .Columns("DG_2").AllowDBNull = True
            .Columns("DG_3").AllowDBNull = True
        End With

        dvAthletik = New DataView(dtAthletik)
        dvAthletik.Sort = "Gruppe, Disziplin"

        ' Grid_Source
        Build_Grid(Bereich, dgvJoin)
        bs = New BindingSource With {.DataSource = dvAthletik}
        bs.Sort = "Nachname, Vorname"
        dgvJoin.DataSource = bs
        txtDG1.DataBindings.Add(New Binding("Text", bs, "DG_1")) ', False, DataSourceUpdateMode.OnPropertyChanged))
        txtDG2.DataBindings.Add(New Binding("Text", bs, "DG_2")) ', False, DataSourceUpdateMode.OnPropertyChanged))
        txtDG3.DataBindings.Add(New Binding("Text", bs, "DG_3")) ', False, DataSourceUpdateMode.OnPropertyChanged))


        ' Gruppen
        With cboGruppe
            .DataSource = dtGruppen
            .ValueMember = "Gruppe"
            .DisplayMember = "Gruppe"
            .SelectedIndex = -1
        End With


        ' Disziplinen
        With cboDisziplinen
            .DataSource = dtDisziplinen
            .ValueMember = "Disziplin"
            .DisplayMember = "Bezeichnung"
            .SelectedIndex = -1
        End With

        loading = False

        Try
            cboGruppe.SelectedIndex = 0
            cboDisziplinen.SelectedIndex = 0
        Catch ex As Exception
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Gruppen-Einteilung gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            Close()
            Return
        End Try

        btnSave.Enabled = False
        Cursor = Cursors.Default

        If Not IsNothing(Tag) AndAlso Tag.Equals("Import") Then mnuImport.PerformClick()
    End Sub

    '' Filter
    Private Function Test_Wiegen() As Boolean

        ' prüfen ob Heber ohne Wiegegewicht sind
        Dim tmpView = New DataView(dvAthletik.ToTable, "Convert(IsNull(attend,''), System.String) = '' AND Convert(IsNull(Wiegen,''), System.String) = ''", Nothing, DataViewRowState.CurrentRows)

        If tmpView.Count > 0 Then
            Dim tmpDT As DataTable = Nothing
            Dim msg = "In Gruppe " & cboGruppe.Text & " fehlen Wiege-Gewichte" & vbNewLine & "bzw. die Bestätigung der Nicht-Teilnahme."
            Do
                msg += vbNewLine + vbNewLine + "Wiegen zur Berichtigung der Meldung öffnen?"
                Using New Centered_MessageBox(Me)
                    If MessageBox.Show(msg, msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then
                        With frmWiegen
                            .Text = "WIEGEN - " & Wettkampf.Bezeichnung
                            .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
                            .Tag = cboGruppe.Text
                            .ShowDialog(Me)
                            If IsNothing(.Tag) Then
                                ' Gruppe in Wiegen geändert --> dvResults aktualisieren
                                Using conn As New MySqlConnection(User.ConnString)
                                    Do
                                        Try
                                            If Not conn.State = ConnectionState.Open Then conn.Open()
                                            ' Results
                                            cmd = New MySqlCommand(Glob.Get_QueryResults("Null", Wettkampf.Athletik), conn)
                                            Dim tmp = New DataTable
                                            tmp.Load(cmd.ExecuteReader())
                                            dtResults.Merge(tmp)
                                            ' Athletik
                                            cmd = New MySqlCommand(Athletik_Query, conn)
                                            tmpDT = New DataTable
                                            tmpDT.Load(cmd.ExecuteReader)
                                            dtAthletik.Merge(tmpDT)
                                            ConnError = False
                                        Catch ex As MySqlException
                                            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                                        End Try
                                    Loop While ConnError
                                End Using
                            End If
                            .Dispose()
                        End With
                    Else
                        Exit Do
                    End If
                End Using
                tmpView = New DataView(dvAthletik.ToTable, "Convert(IsNull(attend,''), System.String) = '' AND Convert(IsNull(Wiegen,''), System.String) = ''", Nothing, DataViewRowState.CurrentRows)
            Loop While tmpView.Count > 0
        End If
        If Not ConnError AndAlso tmpView.Count = 0 Then Return True
        Return False
    End Function
    Private Sub cboDisziplinen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDisziplinen.SelectedIndexChanged
        If loading Then Return
        Try
            For i = 0 To 2
                lstTextbox(i).Enabled = False
            Next
            Dim pos = Glob.dvDisziplinen.Find(cboDisziplinen.SelectedValue)
            If chkMaxOnly.Checked Then
                lstTextbox(0).Enabled = True
            Else
                For i = 0 To CInt(Glob.dvDisziplinen(pos)("Durchgaenge")) - 1
                    lstTextbox(i).Enabled = True
                Next
            End If
            lblEinheit.Text = Glob.dvDisziplinen(pos)("Einheit").ToString
            If cboGruppe.SelectedIndex = -1 Then Return
            bs.Filter = "Gruppe = " & cboGruppe.SelectedValue.ToString & " And Disziplin = " & cboDisziplinen.SelectedValue.ToString
            bs.Position = 0
            btnWeiter.Enabled = True
        Catch ex As Exception
        End Try
    End Sub
    Private Sub cboGruppe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGruppe.SelectedIndexChanged
        If loading Then Return
        Try
            bs.Filter = "Gruppe = " & cboGruppe.SelectedValue.ToString & If(Not cboDisziplinen.SelectedIndex = -1, " And Disziplin = " & cboDisziplinen.SelectedValue.ToString, String.Empty)
            If Not Test_Wiegen() Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Eingabe der Athletik-Resultate nicht möglich.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
                'Close()
                loading = True
                cboGruppe.SelectedIndex = -1
                loading = False
            Else
                bs.Position = 0
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub lblEinheit_TextChanged(sender As Object, e As EventArgs) Handles lblEinheit.TextChanged
        If loading Then Return
        With dgvJoin
            If lblEinheit.Text = "Sekunden" Then
                .Columns("DG1").DefaultCellStyle.Format = "0.00"
                .Columns("DG2").DefaultCellStyle.Format = "0.00"
                .Columns("DG3").DefaultCellStyle.Format = "0.00"
            Else
                .Columns("DG1").DefaultCellStyle.Format = "0"
                .Columns("DG2").DefaultCellStyle.Format = "0"
                .Columns("DG3").DefaultCellStyle.Format = "0"
            End If
        End With
    End Sub

    '' Eingabe
    Private Sub txtBox_GotFocus(sender As Object, e As EventArgs) Handles txtDG1.GotFocus, txtDG2.GotFocus, txtDG3.GotFocus
        With CType(sender, TextBox)
            txtIndex = CInt(Strings.Right(.Name, 1))
            .Tag = .Text
            .SelectAll()
        End With
    End Sub
    Private Sub txtBox_LostFocus(sender As Object, e As EventArgs) Handles txtDG1.LostFocus, txtDG2.LostFocus, txtDG3.LostFocus
        Dim ctl = CType(sender, TextBox)
        If String.IsNullOrEmpty(ctl.Text) Then dvAthletik(bs.Position)(ctl.Name.Insert(5, "_").Substring(3)) = DBNull.Value
        bs.EndEdit()
        Write_Results()
    End Sub
    Private Sub txtBox_TextChanged(sender As Object, e As EventArgs) Handles txtDG1.TextChanged, txtDG2.TextChanged, txtDG3.TextChanged
        If CType(sender, TextBox).Focused Then btnSave.Enabled = True
    End Sub
    Private Sub btnWeiter_Click(sender As Object, e As EventArgs) Handles btnWeiter.Click
        If String.IsNullOrEmpty(txtDG1.Text) Then dvAthletik(bs.Position)!DG_1 = DBNull.Value
        If String.IsNullOrEmpty(txtDG2.Text) Then dvAthletik(bs.Position)!DG_2 = DBNull.Value
        If String.IsNullOrEmpty(txtDG3.Text) Then dvAthletik(bs.Position)!DG_3 = DBNull.Value
        bs.EndEdit()
        Write_Results()
        txtDG1.Focus()
        bs.MoveNext()
    End Sub
    Private Sub Write_Results()
        Cursor = Cursors.WaitCursor
        ' Eingaben sortieren
        Dim lstValues As New List(Of Double)
        For i = 0 To 2
            If IsNumeric(lstTextbox(i).Text) Then lstValues.Add(CDbl(lstTextbox(i).Text))
        Next
        lstValues.Sort()
        Dim AK = Year(Wettkampf.Datum) - CInt(dvAthletik(bs.Position)!Jahrgang)
        Dim x = bsResults.Find("Teilnehmer", dvAthletik(bs.Position)!Teilnehmer)
        Dim Sex = dvAthletik(bs.Position)!sex.ToString
        If lstValues.Count > 0 Then
            ' Eingabe vorhanden
            Dim Limit As Double
            Dim pos = Glob.dvDisziplinen.Find(cboDisziplinen.SelectedValue)
            Dim Formel As String = Glob.dvDisziplinen(pos)("Formel").ToString
            Dim disziplin = CInt(Glob.dvDisziplinen(pos)("idDisziplin"))
            Dim args As New Arguments
            Select Case disziplin
                Case 10, 60, 130 ' Anzahl * Punkte
                    args.Value = lstValues(lstValues.Count - 1)
                    Dim p = Glob.dvDefaults.Find({disziplin, "Anzahl", dvAthletik(bs.Position)("sex")})
                    If Not IsDBNull(Glob.dvDefaults(p)("ak" & AK.ToString)) Then
                        Limit = CDbl(Glob.dvDefaults(p)("ak" & AK.ToString))
                        p = Glob.dvDefaults.Find({disziplin, "Punkte", Sex})
                        args.Punkte = CDbl(Glob.dvDefaults(p)("ak" & AK.ToString))
                        If args.Value > Limit Then args.Value = Limit
                    Else
                        Cursor = Cursors.Default
                        Return
                    End If
                Case 55, 65, 80, 120 ' V_max
                    args.Value = lstValues(0)
                    Limit = CDbl(Glob.dvDisziplinen(pos)("V_max"))
                    If args.Value > Limit Then Limit = -1
                Case 90, 100 ' V_min
                    args.Value = lstValues(lstValues.Count - 1)
                    Limit = CDbl(Glob.dvDisziplinen(pos)("V_min"))
                    If args.Value < Limit Then Limit = -1
                Case 110 ' KG
                    args.Value = lstValues(lstValues.Count - 1)
                    args.KG = CDbl(dvAthletik(bs.Position)("Wiegen"))
                Case Else
                    args.Value = lstValues(lstValues.Count - 1)
            End Select
            Dim result As Double?
            If Limit = -1 Then
                result = 0
            Else
                Using Parser As New Parser
                    result = Parser.Parse_Formel(Formel, args)
                End Using
            End If
            If Not IsNothing(result) Then
                ' Einzelergebnis in Disziplin des TN eintragwn
                dvAthletik(bs.Position)("Resultat") = result
                bs.EndEdit()
            End If
        Else
            ' keine Werte vorhanden
            dvAthletik(bs.Position)("Resultat") = 0
            dvResults(x)("A_" & cboDisziplinen.SelectedIndex + 1) = DBNull.Value
            dvResults(x)("AW_" & cboDisziplinen.SelectedIndex + 1) = DBNull.Value
            dvResults(x)("Athletik") = DBNull.Value
            dvResults(x)("Gesamt") = CDbl(If(IsDBNull(dvResults(x)("Heben")), 0, dvResults(x)("Heben")))
        End If
        bsResults.EndEdit()
        bs.EndEdit()

        ' Nachkomma-Stellen
        Dim NK_Stellen = Get_NK_Stellen(5)
        ' Faktor Athletik
        Dim Modus = Wettkampf.Modus
        If Modus < 110 Then Modus = 110
        Dim Faktor As Double = CDbl(Glob.dvFormeln(Glob.dvFormeln.Find({Modus, AK, Sex}))!Faktor)

        ' alle Athletik-Ergebnisse des TN addieren
        Dim Resultat = dtAthletik.Compute("Sum(Resultat)", "Teilnehmer = " & dvAthletik(bs.Position)("Teilnehmer").ToString)
        ' Gesamtergebnis in alle Disziplinen des TN eintragen
        Dim tn = dtAthletik.Select("Teilnehmer = " & dvAthletik(bs.Position)("Teilnehmer").ToString)
        For Each row As DataRow In tn
            If Not IsDBNull(Resultat) Then row!Athletik = Math.Round(CDbl(Resultat) * Faktor, NK_Stellen)
        Next
        bs.EndEdit()
        ' Ergebnis in dvResults schreiben
        If lstValues.Count > 0 AndAlso Not IsDBNull(Resultat) Then
            dvResults(x)("A_" & cboDisziplinen.SelectedIndex + 1) = lstValues(lstValues.Count - 1)
            dvResults(x)("AW_" & cboDisziplinen.SelectedIndex + 1) = dvAthletik(bs.Position)("Resultat")
            dvResults(x)("Athletik") = Math.Round(CDbl(Resultat) * Faktor, NK_Stellen)
            dvResults(x)!Gesamt = CDbl(If(IsDBNull(dvResults(x)!Heben), 0, dvResults(x)!Heben)) + CDbl(dvResults(x)!Athletik)
        Else
            dvResults(x)("A_" & cboDisziplinen.SelectedIndex + 1) = DBNull.Value
            dvResults(x)("AW_" & cboDisziplinen.SelectedIndex + 1) = DBNull.Value
            dvResults(x)("Athletik") = DBNull.Value
            dvResults(x)("Gesamt") = CDbl(If(IsDBNull(dvResults(x)("Heben")), 0, dvResults(x)("Heben")))
        End If

        bsResults.EndEdit()
        bs.EndEdit()
        Set_Rank()

        Cursor = Cursors.Default
    End Sub
    Private Sub Set_Rank()

        Dim Gruppe = cboGruppe.Text
        Dim Sex = dvAthletik(bs.Position)!Sex.ToString
        Dim JG = CInt(dvAthletik(bs.Position)!Jahrgang)
        Dim KG = CDbl(dvAthletik(bs.Position)!Wiegen)

        Dim tmpSource = New DataView(dtAthletik)
        Using RS As New Results
            tmpSource.RowFilter = RS.Get_Wertung1_Filter(Sex, JG, Gruppe, KG, 0, 0) & " AND Disziplin = " & cboDisziplinen.SelectedValue.ToString
        End Using
        tmpSource.Sort = "Resultat DESC"

        Dim platz = 0
        Dim add = 1
        Dim result As Double = 0

        For Each row As DataRowView In tmpSource
            If Not IsDBNull(row!Resultat) Then
                If Not CDbl(row!Resultat) = result Then
                    result = CDbl(row!Resultat)
                    platz += add
                    add = 1
                Else
                    add += 1
                End If
                row!Platz = platz
            Else
                row!Platz = DBNull.Value
            End If
        Next
        bs.EndEdit()

        ' Gesamt-Platzierung
        Using RS As New Results
            Dim Kategorie = {"Gesamt"}
            For Each WK In Wettkampf.Identities.Keys
                RS.Set_Rank(Sex, JG, KG, Gruppe, Kategorie.ToList, 6, 0, 0, 0, 0, WK) ' Wertung=6 hart codiert, da bei Athletik immer Mehrkampf-Wertung
            Next
        End Using

    End Sub

    '' DGV
    Private Sub Build_Grid(Bereich As String, grid As DataGridView)
        With grid
            .AutoGenerateColumns = False
            .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
            .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor

            Dim ColWidth As New List(Of Integer)
            ColWidth.AddRange({100, 100, 30, 40, 55, 55, 55, 55, 55, 45})
            Dim ColBez As New List(Of String)
            ColBez.AddRange({"Nachname", "Vorname", "m/w", "JG", "1. DG", "2. DG", "3. DG", "Resultat", "Platz", "Athletik"})

            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .DataPropertyName = "Nachname"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .DataPropertyName = "Vorname"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Sex", .DataPropertyName = "Sex"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Jahrgang", .DataPropertyName = "Jahrgang"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "DG1", .DataPropertyName = "DG_1"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "DG2", .DataPropertyName = "DG_2"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "DG3", .DataPropertyName = "DG_3"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Resultat", .DataPropertyName = "Resultat", .ToolTipText = "Punkte In der Disziplin"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Platz", .DataPropertyName = "Platz", .ToolTipText = "Platzierung In der Disziplin"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Athletik", .DataPropertyName = "Athletik", .ToolTipText = "Gesamt-Punkte aller Disziplinen"})

            .Columns("Sex").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("Jahrgang").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("DG1").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("DG2").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("DG3").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("Resultat").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("Athletik").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("Platz").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns("Resultat").DefaultCellStyle.Format = "0.00"
            .Columns("Athletik").DefaultCellStyle.Format = "0.00"

            For i As Integer = 0 To ColBez.Count - 1
                .Columns(i).FillWeight = ColWidth(i)
                .Columns(i).HeaderText = ColBez(i)
                .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            .Columns("Nachname").SortMode = DataGridViewColumnSortMode.Automatic
            .Columns("Sex").SortMode = DataGridViewColumnSortMode.Automatic
            .Columns("Jahrgang").SortMode = DataGridViewColumnSortMode.Automatic
            .Columns("Platz").SortMode = DataGridViewColumnSortMode.Automatic
        End With
    End Sub

    '' Speichern
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click, mnuSpeichern.Click
        Validate()
        bs.EndEdit()
        bsResults.EndEdit()
        Cursor = Cursors.WaitCursor

        If String.IsNullOrEmpty(User.ConnString) Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Anmeldung an MySQL-Server nicht nöglich." & vbNewLine & "(Keine Anmeldedaten vorhanden.)", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            Close()
            Return
        End If

        Using conn As New MySqlConnection(User.ConnString)
            Dim changes As DataTable = dtAthletik '.GetChanges()
            If Not IsNothing(changes) Then
                Dim Query = "INSERT INTO athletik (Wettkampf, Teilnehmer, Disziplin, Durchgang, Wert) VALUES (@wk, @tn, @di, @dg, @val) " &
                            "ON DUPLICATE KEY UPDATE Wert = @val;"
                For Each row As DataRow In changes.Rows
                    Dim disc = dtDisziplinen.Select("Disziplin = " & row!Disziplin.ToString)
                    For i As Integer = 1 To CInt(disc(0)("Durchgaenge"))
                        If Not IsDBNull(row("DG_" & i.ToString)) Then
                            cmd = New MySqlCommand(Query, conn)
                            With cmd.Parameters
                                .AddWithValue("@wk", Wettkampf.ID)
                                .AddWithValue("@tn", row!Teilnehmer)
                                .AddWithValue("@di", row!Disziplin)
                                .AddWithValue("@dg", i)
                                If String.IsNullOrEmpty(row("DG_" & i).ToString) Then
                                    .AddWithValue("@val", DBNull.Value)
                                Else
                                    .AddWithValue("@val", row("DG_" & i))
                                End If
                            End With
                            Do
                                Try
                                    If Not conn.State = ConnectionState.Open Then conn.Open()
                                    cmd.ExecuteNonQuery()
                                    ConnError = False
                                Catch ex As MySqlException
                                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit For
                                End Try
                            Loop While ConnError
                        End If
                    Next
                    If ConnError Then Exit For
                Next
                If Not ConnError Then
                    If Not IsNothing(changes) Then
                        Query = "INSERT INTO athletik_results (Wettkampf, Teilnehmer, Disziplin, Resultat, Platz) VALUES (@wk, @heber, @disziplin, @result, @platz) " &
                                "ON DUPLICATE KEY UPDATE Resultat = @result, Platz = @platz;"
                        For Each row As DataRow In changes.Rows
                            If Not IsDBNull(row!Resultat) Then
                                cmd = New MySqlCommand(Query, conn)
                                With cmd.Parameters
                                    .AddWithValue("@wk", Wettkampf.ID)
                                    .AddWithValue("@heber", row!Teilnehmer)
                                    .AddWithValue("@disziplin", row!Disziplin)
                                    .AddWithValue("@result", row!Resultat)
                                    .AddWithValue("@platz", row!Platz)
                                End With
                                Do
                                    Try
                                        If Not conn.State = ConnectionState.Open Then conn.Open()
                                        cmd.ExecuteNonQuery()
                                        ConnError = False
                                    Catch ex As MySqlException
                                        If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit For
                                    End Try
                                Loop While ConnError
                            End If
                        Next
                    End If
                End If
                If Not ConnError Then
                    dtAthletik.AcceptChanges()
                    changes = New DataTable
                    changes = dtResults.GetChanges()
                    If Not IsNothing(changes) Then
                        Query = "INSERT INTO results (Wettkampf, Teilnehmer, Athletik, Gesamt, Gesamt_Platz) VALUES (@wk, @heber, @athletik, @gesamt, @platz) " &
                                "ON DUPLICATE KEY UPDATE Athletik = @athletik, Gesamt = @gesamt, Gesamt_Platz = @platz;"
                        For Each row As DataRow In changes.Rows
                            If Not IsDBNull(row!Athletik) Then
                                cmd = New MySqlCommand(Query, conn)
                                With cmd.Parameters
                                    .AddWithValue("@wk", Wettkampf.ID)
                                    .AddWithValue("@heber", row!Teilnehmer)
                                    .AddWithValue("@athletik", row!Athletik)
                                    .AddWithValue("@gesamt", row!Gesamt)
                                    .AddWithValue("@platz", row!Gesamt_Platz)
                                End With
                                Do
                                    Try
                                        If Not conn.State = ConnectionState.Open Then conn.Open()
                                        cmd.ExecuteNonQuery()
                                        ConnError = False
                                    Catch ex As MySqlException
                                        If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit For
                                    End Try
                                Loop While ConnError
                            End If
                        Next
                        If Not ConnError Then dtResults.AcceptChanges()
                    End If
                End If
            End If
        End Using
        Cursor = Cursors.Default
        btnSave.Enabled = CBool(ConnError)
    End Sub
    Private Sub btnSave_EnabledChanged(sender As Object, e As EventArgs) Handles btnSave.EnabledChanged
        mnuSpeichern.Enabled = btnSave.Enabled
    End Sub

    '' Menüs
    Private Sub mnuBeenden_Click(sender As Object, e As EventArgs) Handles btnExit.Click, mnuBeenden.Click
        Close()
    End Sub
    Private Sub mnuImport_Click(sender As Object, e As EventArgs) Handles mnuImport.Click

    End Sub

    Private Sub chkMaxOnly_CheckedChanged(sender As Object, e As EventArgs) Handles chkMaxOnly.CheckedChanged
        Try
            For i = 0 To 2
                lstTextbox(i).Enabled = False
            Next
            Dim x = Glob.dvDisziplinen.Find(cboDisziplinen.SelectedValue)
            If chkMaxOnly.Checked Then
                lstTextbox(0).Enabled = True
            Else
                For i = 0 To CInt(Glob.dvDisziplinen(x)("Durchgaenge")) - 1
                    lstTextbox(i).Enabled = True
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ComboBox_MouseWheel(sender As Object, e As MouseEventArgs) Handles cboGruppe.MouseWheel, cboDisziplinen.MouseWheel
        If Not CType(sender, ComboBox).DroppedDown Then
            Dim HMEA As HandledMouseEventArgs = DirectCast(e, HandledMouseEventArgs)
            HMEA.Handled = True
        End If
    End Sub


End Class