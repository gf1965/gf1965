﻿
Public Class frmHantel

    Private Delegate Sub DelegateSub(HLast As Integer, Stange As String, Sex As String, JG As Integer, BigDisc As Boolean, CurrentHeber As Boolean)

    Private Sub frmHantel_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        RemoveHandler Hantel.Hantel_Changed, AddressOf DrawHantel
        RemoveHandler Hantel.Hantel_Clear, AddressOf ClearHantel

        Using sc As New ScreenScale
            Dim mon = sc.GetDeviceNumber(Screen.FromControl(Me))
            If dicScreens.ContainsKey(mon) Then dicScreens.Remove(mon)
        End Using
    End Sub
    Private Sub frmHantel_Load(sender As Object, e As EventArgs) Handles Me.Load
        AddHandler Hantel.Hantel_Changed, AddressOf DrawHantel
        AddHandler Hantel.Hantel_Clear, AddressOf ClearHantel

        pnlHantel.Size = New Size(Width, Height)

    End Sub
    Private Sub frmHantel_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If Heber.Id > 0 Then
            DrawHantel()
        ElseIf Not IsNothing(dvLeader(Leader.TableIx)) AndAlso dvLeader(Leader.TableIx).Count > Leader.RowNum Then
            Dim row = dvLeader(Leader.TableIx)(Leader.RowNum).Row
            DrawHantel(CInt(row!HLast),, row!Sex.ToString, CInt(row!Jahrgang), Wettkampf.BigDisc, False)
        End If
    End Sub

    Private Sub DrawHantel(Optional HLast As Integer = 0, Optional Stange As String = "", Optional Sex As String = "", Optional JG As Integer = 0, Optional BigDisc As Boolean = False, Optional CurrentHeber As Boolean = True)
        If InvokeRequired Then
            Dim d As New DelegateSub(AddressOf DrawHantel)
            Invoke(d, New Object() {HLast, Stange, Sex, JG, BigDisc, CurrentHeber})
        Else
            If CurrentHeber Then
                With Heber
                    Hantel.DrawHantel(pnlHantel, .Hantellast,, .Sex, .JG, Wettkampf.BigDisc)
                End With
            Else
                Dim x = Hantel.DrawHantel(pnlHantel, HLast, Stange, Sex, JG, BigDisc)
            End If
            pnlHantel.Show()
        End If
    End Sub

    Private Sub ClearHantel()
        pnlHantel.Hide()
    End Sub

End Class