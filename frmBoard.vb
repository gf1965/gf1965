﻿
Public Class frmBoard

    Private Sub sendUpdate(sender As Object, e As EventArgs) Handles Rot1.CheckedChanged, Rot2.CheckedChanged, Rot3.CheckedChanged, Weiß1.CheckedChanged, Weiß2.CheckedChanged, Weiß3.CheckedChanged, Wert1.ValueChanged, Wert2.ValueChanged, Wert3.ValueChanged, NurLeds.CheckedChanged, ledKnopf.CheckedChanged
        Dim buf(15) As Byte
        Dim vals As String

        If Not SerialPort1.IsOpen Then
            MsgBox("Erst den Port öffnen", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If Not NurLeds.Checked Then

            'Wenn nicht nur LEDs angezeigt werden sollen wird hier das Paket für die Anzeige gebaut

            'Der wert für die Zeilen wird geholt, mit 100 multipliziert umd das Komma zu eliminieren
            'und als feste 4-Zeichen formatiert in einen String zusammengeklebt

            vals = (Wert1.Value * 100).ToString("0000")
            vals += (Wert2.Value * 100).ToString("0000")
            vals += (Wert3.Value * 100).ToString("0000")
            Encoding.ASCII.GetBytes(vals).CopyTo(buf, 0) 'Packt den String um in Datenbytes für den späteren "Versand"


        Else
            'Sollten nur LEDs gewünscht sein wird das entsprechende Bit in der Nachricht gesetzt
            buf(12) = CByte(buf(12) + 128)
        End If


        'Im folgenden Teil wird das Byte für die LEDs aus den Zuständen der Checkboxen gebaut
        'Könnte man evtl. schöner lösen indem man die Elemente als Array holt (k.A. ob&wie das in VB geht)

        If Weiß1.Checked Then
            buf(12) = CByte(buf(12) + 1)
        End If

        If Rot1.Checked Then
            buf(12) = CByte(buf(12) + 2)
        End If

        If Weiß2.Checked Then
            buf(12) = CByte(buf(12) + 4)
        End If

        If Rot2.Checked Then
            buf(12) = CByte(buf(12) + 8)
        End If

        If Weiß3.Checked Then
            buf(12) = CByte(buf(12) + 16)
        End If

        If Rot3.Checked Then
            buf(12) = CByte(buf(12) + 32)
        End If

        If ledKnopf.Checked Then
            buf(12) = CByte(buf(12) + 64)
        End If

        WriteBuffer(buf)

    End Sub

    Sub WriteBuffer(buf() As Byte)

        Dim CRC As Long = 65535 'Anfangswert für die CRC laden

        For x As Integer = 0 To 12 'CRC geht über alle 12 Datenbytes
            CRC = CRC Xor buf(CInt(x)) 'Aktuelles Datenbyte in die CRC-einbeziehen
            For y As Integer = 1 To 8 'Acht Runden schieben und XOR-en
                If (CRC Mod 2) > 0 Then
                    CRC = CLng((CRC And 65534) / 2)
                    CRC = CRC Xor 40961 'Polynom der CRC 0xA001 in dezimal ausgedrückt
                Else
                    CRC = CLng((CRC And 65534) / 2)
                End If
            Next
        Next

        'Die beiden CRC-Bytes werden an die Nachricht angehängt 
        buf(13) = CByte((CRC And 65280) / 256)
        buf(14) = CByte(CRC And 255)

        SerialPort1.Write(buf, 0, 15) 'Gesamtes Paket absenden

    End Sub

    Private Sub frmBoard_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Dim buf(15) As Byte
        buf(12) = CByte(buf(12) + 128) 'schaltet die LEDs aus
        Try
            WriteBuffer(buf)
            Threading.Thread.Sleep(200)
        Catch ex As Exception
            'Stop
        Finally
            SerialPort1.Dispose()
        End Try
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Aufnehmen vorhandener Ports in die Liste

        portList.Items.AddRange(SerialPort.GetPortNames)
        portList.SelectedIndex = 0
    End Sub

    Private Sub Con_Click(sender As Object, e As EventArgs) Handles Con.Click
        'standart Öffnungsroutine
        With SerialPort1
            If .IsOpen Then .Close()
            Try
                .PortName = portList.SelectedItem.ToString
                .BaudRate = 9600
                .Parity = Parity.None
                .DataBits = 8
                .DiscardNull = False
                .StopBits = StopBits.One
                .Handshake = Handshake.None
                .ParityReplace = 63
                .ReceivedBytesThreshold = 1
                .ReadTimeout = -1
                .RtsEnable = False
                .WriteTimeout = -1
                .ReadBufferSize = 4096
                .WriteBufferSize = 2048
                .Open()
                Timer1.Start()
            Catch ex As Exception
                MsgBox("Fehler beim Verbinden", MsgBoxStyle.Critical)
            End Try
        End With

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim c() As Char
        Dim i As Integer 'As Object
        c = SerialPort1.ReadExisting().ToCharArray

        For i = LBound(c) To UBound(c)
            Select Case c(i)
                Case CChar("K")
                    KnopfGed.CheckState = CheckState.Checked
                Case CChar("L")
                    KnopfGed.Checked = False
                Case CChar("F")
                    MsgBox("Fehlerhaftes Paket", MsgBoxStyle.Critical)
                    Sync.Checked = False
                Case CChar("O")
                    Sync.Checked = True
            End Select
        Next

    End Sub
End Class
