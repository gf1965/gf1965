﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Auswertung_Urkunden
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboAK = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboGK = New System.Windows.Forms.ComboBox()
        Me.dgvJoin = New System.Windows.Forms.DataGridView()
        Me.cboGruppe = New System.Windows.Forms.ComboBox()
        Me.lblGruppe = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboAuswertung = New System.Windows.Forms.ComboBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnVorschau = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboVorlage = New System.Windows.Forms.ComboBox()
        Me.chkSelectAll = New System.Windows.Forms.CheckBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.cboSex = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.grpPlätze = New System.Windows.Forms.GroupBox()
        Me.optSel = New System.Windows.Forms.RadioButton()
        Me.nudBis = New System.Windows.Forms.NumericUpDown()
        Me.optAlle = New System.Windows.Forms.RadioButton()
        Me.optDrei = New System.Windows.Forms.RadioButton()
        Me.optErster = New System.Windows.Forms.RadioButton()
        Me.optBis = New System.Windows.Forms.RadioButton()
        Me.pnlFilter = New System.Windows.Forms.Panel()
        Me.dgvHeber = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblGruppierung = New System.Windows.Forms.Label()
        Me.cboGruppierung = New System.Windows.Forms.ComboBox()
        Me.cboWettkampf = New System.Windows.Forms.ComboBox()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPlätze.SuspendLayout()
        CType(Me.nudBis, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFilter.SuspendLayout()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblCaption
        '
        Me.lblCaption.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption.ForeColor = System.Drawing.Color.Gold
        Me.lblCaption.Location = New System.Drawing.Point(0, 0)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(964, 30)
        Me.lblCaption.TabIndex = 1
        Me.lblCaption.Text = "Wettkampf"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(320, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 13)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "&AK"
        '
        'cboAK
        '
        Me.cboAK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAK.FormattingEnabled = True
        Me.cboAK.Location = New System.Drawing.Point(345, 7)
        Me.cboAK.Name = "cboAK"
        Me.cboAK.Size = New System.Drawing.Size(83, 21)
        Me.cboAK.TabIndex = 45
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(450, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(22, 13)
        Me.Label2.TabIndex = 50
        Me.Label2.Text = "G&K"
        '
        'cboGK
        '
        Me.cboGK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboGK.FormattingEnabled = True
        Me.cboGK.Location = New System.Drawing.Point(476, 7)
        Me.cboGK.Name = "cboGK"
        Me.cboGK.Size = New System.Drawing.Size(56, 21)
        Me.cboGK.TabIndex = 55
        '
        'dgvJoin
        '
        Me.dgvJoin.AllowUserToAddRows = False
        Me.dgvJoin.AllowUserToDeleteRows = False
        Me.dgvJoin.AllowUserToResizeColumns = False
        Me.dgvJoin.AllowUserToResizeRows = False
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.Honeydew
        Me.dgvJoin.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvJoin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvJoin.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvJoin.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvJoin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvJoin.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgvJoin.Location = New System.Drawing.Point(10, 1)
        Me.dgvJoin.MultiSelect = False
        Me.dgvJoin.Name = "dgvJoin"
        Me.dgvJoin.RowHeadersVisible = False
        Me.dgvJoin.RowHeadersWidth = 25
        Me.dgvJoin.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvJoin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvJoin.Size = New System.Drawing.Size(811, 362)
        Me.dgvJoin.TabIndex = 0
        '
        'cboGruppe
        '
        Me.cboGruppe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGruppe.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboGruppe.FormattingEnabled = True
        Me.cboGruppe.Location = New System.Drawing.Point(51, 7)
        Me.cboGruppe.Name = "cboGruppe"
        Me.cboGruppe.Size = New System.Drawing.Size(149, 21)
        Me.cboGruppe.TabIndex = 15
        '
        'lblGruppe
        '
        Me.lblGruppe.AutoSize = True
        Me.lblGruppe.Location = New System.Drawing.Point(5, 10)
        Me.lblGruppe.Name = "lblGruppe"
        Me.lblGruppe.Size = New System.Drawing.Size(42, 13)
        Me.lblGruppe.TabIndex = 10
        Me.lblGruppe.Text = "&Gruppe"
        Me.lblGruppe.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Aus&wertung"
        '
        'cboAuswertung
        '
        Me.cboAuswertung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAuswertung.DropDownWidth = 100
        Me.cboAuswertung.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAuswertung.FormattingEnabled = True
        Me.cboAuswertung.Items.AddRange(New Object() {"Gesamt", "Reißen", "Stoßen", "Punktbeste Heber", "Vereinsmannschaft", "Ländermannschaft", "2. Wertung"})
        Me.cboAuswertung.Location = New System.Drawing.Point(82, 45)
        Me.cboAuswertung.Name = "cboAuswertung"
        Me.cboAuswertung.Size = New System.Drawing.Size(119, 21)
        Me.cboAuswertung.TabIndex = 25
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Location = New System.Drawing.Point(848, 415)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(100, 28)
        Me.btnCancel.TabIndex = 110
        Me.btnCancel.TabStop = False
        Me.btnCancel.Text = "&Beenden"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnPrint.Location = New System.Drawing.Point(848, 381)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(100, 28)
        Me.btnPrint.TabIndex = 105
        Me.btnPrint.TabStop = False
        Me.btnPrint.Tag = "Print"
        Me.btnPrint.Text = "&Drucken"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnVorschau
        '
        Me.btnVorschau.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVorschau.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnVorschau.Location = New System.Drawing.Point(848, 347)
        Me.btnVorschau.Name = "btnVorschau"
        Me.btnVorschau.Size = New System.Drawing.Size(100, 28)
        Me.btnVorschau.TabIndex = 100
        Me.btnVorschau.TabStop = False
        Me.btnVorschau.Tag = "Show"
        Me.btnVorschau.Text = "&Vorschau"
        Me.btnVorschau.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnSearch.Location = New System.Drawing.Point(714, 457)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(24, 24)
        Me.btnSearch.TabIndex = 52
        Me.btnSearch.TabStop = False
        Me.btnSearch.Text = "..."
        Me.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label6.Location = New System.Drawing.Point(309, 462)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 13)
        Me.Label6.TabIndex = 51
        Me.Label6.Text = "Druck-Vorlage"
        '
        'cboVorlage
        '
        Me.cboVorlage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboVorlage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVorlage.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboVorlage.FormattingEnabled = True
        Me.cboVorlage.Location = New System.Drawing.Point(390, 459)
        Me.cboVorlage.Name = "cboVorlage"
        Me.cboVorlage.Size = New System.Drawing.Size(318, 21)
        Me.cboVorlage.Sorted = True
        Me.cboVorlage.TabIndex = 50
        Me.cboVorlage.TabStop = False
        '
        'chkSelectAll
        '
        Me.chkSelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.BackColor = System.Drawing.SystemColors.Control
        Me.chkSelectAll.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkSelectAll.Location = New System.Drawing.Point(18, 459)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(134, 18)
        Me.chkSelectAll.TabIndex = 53
        Me.chkSelectAll.TabStop = False
        Me.chkSelectAll.Text = "alle Gruppen drucken"
        Me.chkSelectAll.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkSelectAll.UseVisualStyleBackColor = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Title = "Druckvorlagen suchen"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.Location = New System.Drawing.Point(744, 457)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(85, 24)
        Me.btnEdit.TabIndex = 54
        Me.btnEdit.TabStop = False
        Me.btnEdit.Text = "Bearbeiten"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'cboSex
        '
        Me.cboSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSex.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboSex.FormattingEnabled = True
        Me.cboSex.Location = New System.Drawing.Point(252, 7)
        Me.cboSex.Name = "cboSex"
        Me.cboSex.Size = New System.Drawing.Size(46, 21)
        Me.cboSex.TabIndex = 35
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(220, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 13)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "&m/w"
        '
        'grpPlätze
        '
        Me.grpPlätze.Controls.Add(Me.optSel)
        Me.grpPlätze.Controls.Add(Me.nudBis)
        Me.grpPlätze.Controls.Add(Me.optAlle)
        Me.grpPlätze.Controls.Add(Me.optDrei)
        Me.grpPlätze.Controls.Add(Me.optErster)
        Me.grpPlätze.Controls.Add(Me.optBis)
        Me.grpPlätze.Location = New System.Drawing.Point(848, 174)
        Me.grpPlätze.Name = "grpPlätze"
        Me.grpPlätze.Size = New System.Drawing.Size(100, 152)
        Me.grpPlätze.TabIndex = 111
        Me.grpPlätze.TabStop = False
        Me.grpPlätze.Text = "Plätze"
        '
        'optSel
        '
        Me.optSel.AutoSize = True
        Me.optSel.Location = New System.Drawing.Point(16, 120)
        Me.optSel.Name = "optSel"
        Me.optSel.Size = New System.Drawing.Size(79, 17)
        Me.optSel.TabIndex = 5
        Me.optSel.Text = "ausgewählt"
        Me.optSel.UseVisualStyleBackColor = True
        '
        'nudBis
        '
        Me.nudBis.Location = New System.Drawing.Point(50, 74)
        Me.nudBis.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.nudBis.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudBis.Name = "nudBis"
        Me.nudBis.Size = New System.Drawing.Size(36, 20)
        Me.nudBis.TabIndex = 4
        Me.nudBis.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudBis.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'optAlle
        '
        Me.optAlle.AutoSize = True
        Me.optAlle.Checked = True
        Me.optAlle.Location = New System.Drawing.Point(16, 97)
        Me.optAlle.Name = "optAlle"
        Me.optAlle.Size = New System.Drawing.Size(41, 17)
        Me.optAlle.TabIndex = 3
        Me.optAlle.TabStop = True
        Me.optAlle.Text = "alle"
        Me.optAlle.UseVisualStyleBackColor = True
        '
        'optDrei
        '
        Me.optDrei.AutoSize = True
        Me.optDrei.Location = New System.Drawing.Point(16, 51)
        Me.optDrei.Name = "optDrei"
        Me.optDrei.Size = New System.Drawing.Size(68, 17)
        Me.optDrei.TabIndex = 1
        Me.optDrei.Text = "erste drei"
        Me.optDrei.UseVisualStyleBackColor = True
        '
        'optErster
        '
        Me.optErster.AutoSize = True
        Me.optErster.Location = New System.Drawing.Point(16, 28)
        Me.optErster.Name = "optErster"
        Me.optErster.Size = New System.Drawing.Size(69, 17)
        Me.optErster.TabIndex = 0
        Me.optErster.Text = "nur erster"
        Me.optErster.UseVisualStyleBackColor = True
        '
        'optBis
        '
        Me.optBis.AutoSize = True
        Me.optBis.Location = New System.Drawing.Point(15, 74)
        Me.optBis.Name = "optBis"
        Me.optBis.Size = New System.Drawing.Size(37, 17)
        Me.optBis.TabIndex = 2
        Me.optBis.Text = "1 -"
        Me.optBis.UseVisualStyleBackColor = True
        '
        'pnlFilter
        '
        Me.pnlFilter.Controls.Add(Me.cboSex)
        Me.pnlFilter.Controls.Add(Me.Label3)
        Me.pnlFilter.Controls.Add(Me.lblGruppe)
        Me.pnlFilter.Controls.Add(Me.cboGruppe)
        Me.pnlFilter.Controls.Add(Me.cboGK)
        Me.pnlFilter.Controls.Add(Me.Label2)
        Me.pnlFilter.Controls.Add(Me.cboAK)
        Me.pnlFilter.Controls.Add(Me.Label1)
        Me.pnlFilter.Location = New System.Drawing.Point(218, 38)
        Me.pnlFilter.Name = "pnlFilter"
        Me.pnlFilter.Size = New System.Drawing.Size(542, 36)
        Me.pnlFilter.TabIndex = 112
        '
        'dgvHeber
        '
        Me.dgvHeber.AllowUserToAddRows = False
        Me.dgvHeber.AllowUserToDeleteRows = False
        Me.dgvHeber.AllowUserToOrderColumns = True
        Me.dgvHeber.AllowUserToResizeColumns = False
        Me.dgvHeber.AllowUserToResizeRows = False
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.dgvHeber.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvHeber.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvHeber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHeber.ColumnHeadersVisible = False
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvHeber.DefaultCellStyle = DataGridViewCellStyle13
        Me.dgvHeber.Location = New System.Drawing.Point(330, 141)
        Me.dgvHeber.Name = "dgvHeber"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.RowHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvHeber.RowHeadersVisible = False
        Me.dgvHeber.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvHeber.Size = New System.Drawing.Size(199, 74)
        Me.dgvHeber.TabIndex = 113
        Me.dgvHeber.TabStop = False
        Me.dgvHeber.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.dgvHeber)
        Me.Panel1.Controls.Add(Me.dgvJoin)
        Me.Panel1.Location = New System.Drawing.Point(7, 80)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(830, 363)
        Me.Panel1.TabIndex = 114
        '
        'lblGruppierung
        '
        Me.lblGruppierung.AutoSize = True
        Me.lblGruppierung.Enabled = False
        Me.lblGruppierung.Location = New System.Drawing.Point(771, 48)
        Me.lblGruppierung.Name = "lblGruppierung"
        Me.lblGruppierung.Size = New System.Drawing.Size(38, 13)
        Me.lblGruppierung.TabIndex = 115
        Me.lblGruppierung.Text = "AK-Gr."
        '
        'cboGruppierung
        '
        Me.cboGruppierung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGruppierung.Enabled = False
        Me.cboGruppierung.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboGruppierung.FormattingEnabled = True
        Me.cboGruppierung.Location = New System.Drawing.Point(812, 45)
        Me.cboGruppierung.Name = "cboGruppierung"
        Me.cboGruppierung.Size = New System.Drawing.Size(136, 21)
        Me.cboGruppierung.TabIndex = 116
        '
        'cboWettkampf
        '
        Me.cboWettkampf.BackColor = System.Drawing.SystemColors.Window
        Me.cboWettkampf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWettkampf.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWettkampf.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cboWettkampf.FormattingEnabled = True
        Me.cboWettkampf.Items.AddRange(New Object() {"Test1"})
        Me.cboWettkampf.Location = New System.Drawing.Point(193, 2)
        Me.cboWettkampf.Name = "cboWettkampf"
        Me.cboWettkampf.Size = New System.Drawing.Size(572, 26)
        Me.cboWettkampf.TabIndex = 117
        '
        'Auswertung_Urkunden
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(964, 497)
        Me.Controls.Add(Me.cboWettkampf)
        Me.Controls.Add(Me.cboGruppierung)
        Me.Controls.Add(Me.lblGruppierung)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pnlFilter)
        Me.Controls.Add(Me.grpPlätze)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.chkSelectAll)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cboVorlage)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnVorschau)
        Me.Controls.Add(Me.cboAuswertung)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblCaption)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(884, 539)
        Me.Name = "Auswertung_Urkunden"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Urkunden"
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPlätze.ResumeLayout(False)
        Me.grpPlätze.PerformLayout()
        CType(Me.nudBis, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFilter.ResumeLayout(False)
        Me.pnlFilter.PerformLayout()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCaption As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cboAK As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents cboGK As ComboBox
    Friend WithEvents dgvJoin As DataGridView
    Friend WithEvents cboGruppe As ComboBox
    Friend WithEvents lblGruppe As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cboAuswertung As ComboBox
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnPrint As Button
    Friend WithEvents btnVorschau As Button
    Friend WithEvents btnSearch As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents cboVorlage As ComboBox
    Friend WithEvents chkSelectAll As CheckBox
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents btnEdit As Button
    Friend WithEvents cboSex As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents grpPlätze As GroupBox
    Friend WithEvents nudBis As NumericUpDown
    Friend WithEvents optAlle As RadioButton
    Friend WithEvents optBis As RadioButton
    Friend WithEvents optDrei As RadioButton
    Friend WithEvents optErster As RadioButton
    Friend WithEvents pnlFilter As Panel
    Friend WithEvents dgvHeber As DataGridView
    Friend WithEvents Panel1 As Panel
    Friend WithEvents optSel As RadioButton
    Friend WithEvents lblGruppierung As Label
    Friend WithEvents cboGruppierung As ComboBox
    Friend WithEvents cboWettkampf As ComboBox
End Class
