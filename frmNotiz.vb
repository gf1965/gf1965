﻿Imports MySqlConnector

Public Class frmNotiz
    Property AthleteId As Integer
    Property Notiz As Byte()

    Private dtNotiz As DataTable

    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If btnSave.Enabled Then
            Using New Centered_MessageBox(Me)
                Dim result As DialogResult = MessageBox.Show("Änderungen an den Notizen speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                If result = DialogResult.Yes Then
                    btnSave.PerformClick()
                ElseIf result = DialogResult.Cancel Then
                    e.Cancel = True
                End If
            End Using
        End If
    End Sub
    Private Sub Me_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Close()
        End Select
    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Cursor = Cursors.WaitCursor
        Using conn As New MySqlConnection(User.ConnString)
            Dim Query = "select * from notizen where Heber = @id;"
            Dim cmd = New MySqlCommand(Query, conn)
            cmd.Parameters.AddWithValue("@id", AthleteId)
            dtNotiz = New DataTable
            conn.Open()
            dtNotiz.Load(cmd.ExecuteReader)
        End Using

        Load_RTF()

        rtfNotiz.Focus()
        rtfNotiz.Select(rtfNotiz.TextLength, 0)

        btnSave.Enabled = False
        btnUndo.Enabled = False
        btnDelete.Enabled = rtfNotiz.TextLength > 0

        Cursor = Cursors.Default
    End Sub

    Private Sub btnFontColor_Click(sender As Object, e As EventArgs) Handles btnFontColor.Click
        If ColorDialog1.ShowDialog() = DialogResult.OK Then
            btnFontColor.BackColor = ColorDialog1.Color
            rtfNotiz.SelectionColor = btnFontColor.BackColor
        End If
        rtfNotiz.Focus()
    End Sub
    Private Sub chkFontStyle_Click(sender As Object, e As EventArgs) Handles chkBold.Click, chkItalic.Click, chkUnderline.Click
        Dim chk = CType(sender, CheckBox)
        chk.Checked = Not chk.Checked
        Dim value As Integer = 0 'Regular
        If chkBold.Checked Then value += 1
        If chkItalic.Checked Then value += 2
        If chkUnderline.Checked Then value += 4
        Dim fntStyle = New FontStyle With {.value__ = value}
        With rtfNotiz
            .SelectionFont = New Font(.SelectionFont.Name, .SelectionFont.Size, fntStyle)
            .Focus()
        End With

    End Sub

    Private Sub btnUndo_Click(sender As Object, e As EventArgs) Handles btnUndo.Click
        With rtfNotiz
            Load_RTF()
            .Focus()
            btnDelete.Enabled = .TextLength > 0
        End With
        btnSave.Enabled = True
        btnUndo.Enabled = False
    End Sub
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        With rtfNotiz
            .Clear()
            .Focus()
        End With
        btnSave.Enabled = True
        btnDelete.Enabled = False
        btnUndo.Enabled = True
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Cursor = Cursors.WaitCursor

        Dim Query As String

        If rtfNotiz.TextLength = 0 Then
            Query = "DELETE FROM notizen WHERE Heber = @id;"
        Else
            Query = "INSERT INTO notizen (Heber, Notiz) VALUES (@id, @note) 
                     ON DUPLICATE KEY UPDATE Notiz = @note;"
        End If

        Notiz = Save_RTF()

        Using conn As New MySqlConnection(User.ConnString)
            Dim cmd As New MySqlCommand(Query, conn)
            With cmd.Parameters
                .AddWithValue("@id", AthleteId)
                .AddWithValue("@note", Notiz)
            End With
            Do
                Try
                    If conn.State = ConnectionState.Closed Then conn.Open()
                    cmd.ExecuteNonQuery()
                    ConnError = False
                Catch ex As Exception
                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                End Try
            Loop While ConnError
        End Using

        If Not ConnError Then
            btnSave.Enabled = False
            Close()
        End If

        Cursor = Cursors.Default
    End Sub

    Private Sub rtfNotiz_SelectionChanged(sender As Object, e As EventArgs) Handles rtfNotiz.SelectionChanged
        Select Case rtfNotiz.SelectionFont.Style
            Case FontStyle.Regular
                chkBold.Checked = False
                chkItalic.Checked = False
                chkUnderline.Checked = False
            Case FontStyle.Bold
                chkBold.Checked = True
                chkItalic.Checked = False
                chkUnderline.Checked = False
            Case FontStyle.Italic
                chkItalic.Checked = True
                chkBold.Checked = False
                chkUnderline.Checked = False
            Case FontStyle.Underline
                chkUnderline.Checked = True
                chkBold.Checked = False
                chkItalic.Checked = False
            Case FontStyle.Bold Or FontStyle.Italic
                chkBold.Checked = True
                chkItalic.Checked = True
                chkUnderline.Checked = False
            Case FontStyle.Bold Or FontStyle.Underline
                chkBold.Checked = True
                chkUnderline.Checked = True
                chkItalic.Checked = False
            Case FontStyle.Italic Or FontStyle.Underline
                chkItalic.Checked = True
                chkUnderline.Checked = True
                chkBold.Checked = False
            Case FontStyle.Bold Or FontStyle.Italic Or FontStyle.Underline
                chkBold.Checked = True
                chkItalic.Checked = True
                chkUnderline.Checked = True
        End Select
        btnFontColor.BackColor = rtfNotiz.SelectionColor
    End Sub
    Private Sub rtfNotiz_TextChanged(sender As Object, e As EventArgs) Handles rtfNotiz.TextChanged
        btnSave.Enabled = True
        btnDelete.Enabled = rtfNotiz.TextLength > 0
        btnUndo.Enabled = True
    End Sub

    Private Sub Load_RTF()
        rtfNotiz.Clear()

        If dtNotiz.Rows.Count = 0 Then
            Dim NewRow = dtNotiz.NewRow
            NewRow.ItemArray = {AthleteId, DBNull.Value}
            dtNotiz.Rows.Add(NewRow)
        Else
            If Not IsDBNull(dtNotiz(0)!Notiz) Then
                'If dtNotiz(0)!Notiz.ToString.StartsWith("{\rtf1") Then
                '    rtfNotiz.Rtf = dtNotiz(0)!Notiz.ToString
                'Else
                '    Dim RtfString = ConvertTextToRTF(dtNotiz(0)!Notiz.ToString)
                '    rtfNotiz.Rtf = RtfString
                'End If
                rtfNotiz.Rtf = ConvertTextToRTF(dtNotiz(0)!Notiz.ToString)
                'Try
                '    Dim Content = CType(dtNotiz(0)!Notiz, Byte())
                '    Using Stream = New MemoryStream(Content)
                '        rtfNotiz.LoadFile(Stream, RichTextBoxStreamType.RichText)
                '    End Using
                'Catch ex As Exception
                '    Dim RtfString = ConvertTextToRTF(dtNotiz(0)!Notiz.ToString)
                '        'Content = Encoding.Unicode.GetBytes(RtfString)
                '        rtfNotiz.Rtf = RtfString
                'End Try
            End If
        End If
    End Sub
    Private Function Save_RTF() As Byte()
        Using ms = New MemoryStream()
            rtfNotiz.SaveFile(ms, RichTextBoxStreamType.RichText)
            Return ms.ToArray
        End Using
    End Function

End Class
