﻿
Imports MySqlConnector

Public Class Import_Ergebnisse

    Dim _path As String = String.Empty
    Dim _files As Dictionary(Of Integer, String) ' WK.RowIndex, XML.FileName
    Dim _groups As Dictionary(Of Integer, List(Of Boolean)) ' WK.RowIndex, Gruppe.Checked
    Dim CurrentRow As Integer = 0

    Private _checkerGruppe As Integer
    Private Property checkerGruppe As Integer
        Get
            Return _checkerGruppe
        End Get
        Set(value As Integer)
            _checkerGruppe = value
            If dgvGruppe.Visible Then
                If _checkerGruppe = 0 Then
                    dgvErgebnisse.CurrentRow.Cells("Checked").Value = False
                ElseIf _checkerGruppe = dgvGruppe.Rows.Count Then
                    dgvErgebnisse.CurrentRow.Cells("Checked").Value = True
                Else
                    dgvErgebnisse.CurrentRow.Cells("Checked").Value = DBNull.Value
                End If
            End If
        End Set
    End Property

    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Cursor = Cursors.WaitCursor

        dgvErgebnisse.Rows.Clear()
        _files = New Dictionary(Of Integer, String)
        _groups = New Dictionary(Of Integer, List(Of Boolean))

        lblMsg.Text = "Wettkämpfe suchen"
        Refresh()

        dgvErgebnisse.Columns("Expand").Visible = Not Text.Contains("Ergebnis")

        Dim drvs = Computer.FileSystem.Drives.ToList
        For Each drv In drvs
            If drv.IsReady Then
                Dim folders = Computer.FileSystem.GetDirectories(drv.ToString, FileIO.SearchOption.SearchTopLevelOnly).ToList
                For Each folder In folders
                    If folder.Contains("GFHsoft Gewichtheben") Then
                        _path = Path.Combine(drv.ToString, folder)
                        Exit For
                    End If
                Next
                If Not String.IsNullOrEmpty(_path) Then
                    Dim subfolders = Computer.FileSystem.GetDirectories(_path, FileIO.SearchOption.SearchTopLevelOnly).ToList
                    For Each sf In subfolders
                        Dim files = Computer.FileSystem.GetFiles(sf)
                        For Each file In files
                            Write_File_To_Grid(file)
                        Next
                    Next
                    Exit For
                End If
            End If
        Next
        Cursor = Cursors.Default

        lblMsg.Text = _files.Count & " Datei" & If(_files.Count <> 1, "en", "") & " gefunden"

        If String.IsNullOrEmpty(_path) OrElse dgvErgebnisse.Rows.Count = 0 Then
            btnImport.Enabled = False
            btnFile.PerformClick()
        End If
    End Sub

    Private Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        Cursor = Cursors.WaitCursor
        Dim Query = String.Empty
        Dim cmd As MySqlCommand
        Dim ds As DataSet
        For Each file In _files
            If CBool(dgvErgebnisse("Checked", file.Key).Value) Then
                ds = New DataSet
                ds.ReadXml(file.Value)
                Using conn As New MySqlConnection(User.ConnString)
                    conn.Open()
                    For Each tbl As DataTable In ds.Tables
                        With ProgressBar1
                            .Value = 0
                            .Maximum = tbl.Rows.Count + 2
                        End With
                        lblMsg.Text = "Tabelle <" & tbl.TableName & "> wird in Datenbank importiert"
                        Select Case tbl.TableName
                            Case "reissen", "stossen" ' **************************************** reissen / stossen ******************************************************
                                Query = "INSERT IGNORE INTO " & tbl.TableName & " (Wettkampf, Teilnehmer, Versuch, HLast, Diff, Steigerung1, Steigerung2, Steigerung3, Wertung, Zeit, Note, Lampen, Updated) " &
                                        "VALUES (@wk, @tn, @vers, @last, @diff, @s1, @s2, @s3, @wert, @zeit, @note, @lamp, @update); " &
                                        "UPDATE " & tbl.TableName & " SET HLast = @last, Diff = @diff, Steigerung1 = @s1, Steigerung2 = @s2, Steigerung3 = @s3, " &
                                                                         "Wertung = @wert, Zeit = @zeit, Note = @note, Lampen = @lamp " &
                                        "WHERE Wettkampf = @wk AND Teilnehmer = @tn AND Versuch = @vers AND @update > Updated;"
                                For Each row As DataRow In tbl.Rows
                                    ProgressBar1.Value += 1
                                    cmd = New MySqlCommand(Query, conn)
                                    Try
                                        With cmd.Parameters
                                            .AddWithValue("@wk", row!Wettkampf)
                                            .AddWithValue("@tn", row!Teilnehmer)
                                            .AddWithValue("@vers", row!Versuch)
                                            .AddWithValue("@last", row!HLast)
                                            .AddWithValue("@diff", row!Diff)
                                            .AddWithValue("@s1", row!Steigerung1)
                                            .AddWithValue("@s2", row!Steigerung2)
                                            .AddWithValue("@s3", row!Steigerung3)
                                            .AddWithValue("@wert", row!Wertung)
                                            .AddWithValue("@zeit", row!Zeit)
                                            .AddWithValue("@note", row!Note)
                                            .AddWithValue("@lamp", row!Lampen)
                                            .AddWithValue("@update", row!Updated)
                                        End With
                                        cmd.ExecuteNonQuery()
                                    Catch ex As Exception
                                    End Try
                                Next
                            Case "results" ' ******************************************************** results ***********************************************************

                                ' Zeit

                                Query = "INSERT IGNORE INTO results (Wettkampf, Teilnehmer, Reissen, Reissen_Platz, Stossen, Stossen_Platz, ZK, Zeit, Heben, Heben_Platz, Wertung2, Wertung2_Platz,Athletik,Gesamt, Gesamt_Platz, Updated) " &
                                        "VALUES (@wk, @tn, @r, @rp, @s, @sp, @zk, @zeit, @h, @hp, @w2, @w2p, @a, @g, @gp, @update); " &
                                        "UPDATE results SET Reissen = @r, Reissen_Platz = @rp, Stossen = @s, Stossen_Platz = @sp, ZK = @zk, Zeit = @zeit, Heben = @h, Heben_Platz = @hp, Wertung2 = @w2, Wertung2_Platz = @w2p, Athletik = @a, Gesamt = @g, Gesamt_Platz = @gp " &
                                        "WHERE Wettkampf = @wk AND Teilnehmer = @tn AND @update > Updated;"
                                For Each row As DataRow In tbl.Rows
                                    ProgressBar1.Value += 1
                                    cmd = New MySqlCommand(Query, conn)
                                    Try
                                        With cmd.Parameters
                                            .AddWithValue("@wk", row!Wettkampf)
                                            .AddWithValue("@tn", row!Teilnehmer)
                                            .AddWithValue("@r", If(tbl.Columns.Contains("Reissen"), row!Reissen, DBNull.Value))
                                            .AddWithValue("@rp", If(tbl.Columns.Contains("Reissen_Platz"), row!Reissen_Platz, DBNull.Value))
                                            .AddWithValue("@s", If(tbl.Columns.Contains("Stossen"), row!Stossen, DBNull.Value))
                                            .AddWithValue("@sp", If(tbl.Columns.Contains("Stossen_Platz"), row!Stossen_Platz, DBNull.Value))
                                            .AddWithValue("@zk", If(tbl.Columns.Contains("ZK"), row!ZK, DBNull.Value))
                                            .AddWithValue("@zeit", row!Zeit)
                                            .AddWithValue("@h", row!Heben)
                                            .AddWithValue("@hp", row!Heben_Platz)
                                            .AddWithValue("@w2", IIf(tbl.Columns.Contains("Wertung2"), row!Wertung2, DBNull.Value))
                                            .AddWithValue("@w2p", If(tbl.Columns.Contains("Wertung2_Platz"), row!Wertung2_Platz, DBNull.Value))
                                            .AddWithValue("@a", If(tbl.Columns.Contains("Athletik"), row!Athletik, DBNull.Value))
                                            .AddWithValue("@g", If(tbl.Columns.Contains("Gesamt"), row!Gesamt, DBNull.Value))
                                            .AddWithValue("@gp", If(tbl.Columns.Contains("Gesamt_Platz"), row!Gesamt_Platz, DBNull.Value))
                                            .AddWithValue("@update", row!Updated)
                                        End With
                                        cmd.ExecuteNonQuery()
                                    Catch ex As Exception
                                    End Try
                                Next
                            Case "athletik" ' ******************************************************* athletik ***********************************************************
                                Query = "INSERT IGNORE INTO athletik (Wettkampf, Teilnehmer, Disziplin, Durchgang, Wert, Updated) " &
                                        "VALUES (@wk, @tn, @dis, @dg, @w, @update); " &
                                        "UPDATE results SET Wert = @w " &
                                        "WHERE Wettkampf = @wk AND Teilnehmer = @tn AND Disziplin = @dis AND Durchgang = @dg AND @update > Updated;"
                                For Each row As DataRow In tbl.Rows
                                    ProgressBar1.Value += 1
                                    cmd = New MySqlCommand(Query, conn)
                                    Try
                                        With cmd.Parameters
                                            .AddWithValue("@wk", row!Wettkampf)
                                            .AddWithValue("@tn", row!Teilnehmer)
                                            .AddWithValue("@dis", row!Disziplin)
                                            .AddWithValue("@dg", row!Durchgang)
                                            .AddWithValue("@w", row!Wert)
                                            .AddWithValue("@update", row!Updated)
                                        End With
                                        cmd.ExecuteNonQuery()
                                    Catch ex As Exception
                                    End Try
                                Next
                            Case "athletik_results" ' ******************************************* athletik_results *******************************************************
                                Query = "INSERT IGNORE INTO athletik_results (Wettkampf, Teilnehmer, Disziplin, Resultat, Platz, Updated) " &
                                        "VALUES (@wk, @tn, @dis, @res, @pl, @update); " &
                                        "UPDATE results SET Resultat = @res, Platz = @pl " &
                                        "WHERE Wettkampf = @wk AND Teilnehmer = @tn AND Disziplin = @dis AND @update > Updated;"
                                For Each row As DataRow In tbl.Rows
                                    ProgressBar1.Value += 1
                                    cmd = New MySqlCommand(Query, conn)
                                    Try
                                        With cmd.Parameters
                                            .AddWithValue("@wk", row!Wettkampf)
                                            .AddWithValue("@tn", row!Teilnehmer)
                                            .AddWithValue("@dis", row!Disziplin)
                                            .AddWithValue("@res", row!Resultat)
                                            .AddWithValue("@pl", row!Platz)
                                            .AddWithValue("@update", row!Updated)
                                        End With
                                        cmd.ExecuteNonQuery()
                                    Catch ex As Exception
                                    End Try
                                Next
                        End Select
                        ProgressBar1.Value = ProgressBar1.Maximum
                    Next
                End Using
                dgvErgebnisse("Checked", file.Key).Value = False
                dgvErgebnisse.Rows(file.Key).DefaultCellStyle.ForeColor = Color.Green
                If dgvErgebnisse.SelectedRows(0).Index = file.Key Then dgvErgebnisse.Rows(file.Key).DefaultCellStyle.SelectionForeColor = Color.Green
            End If
        Next
        ProgressBar1.Value = 0
        lblMsg.Text = "Import abgeschlossen"
        dgvErgebnisse.Focus()
        Cursor = Cursors.Default
    End Sub

    Private Sub btnWK_Click(sender As Object, e As EventArgs) Handles btnFile.Click
        Cursor = Cursors.WaitCursor

        dgvErgebnisse.Rows.Clear()
        _files = New Dictionary(Of Integer, String)
        _groups = New Dictionary(Of Integer, List(Of Boolean))

        With OpenFileDialog1
            .InitialDirectory = If(String.IsNullOrEmpty(_path), Environment.SpecialFolder.MyComputer.ToString, _path)
            If Not .ShowDialog() = DialogResult.Cancel Then Write_File_To_Grid(.FileName, True)
        End With

        lblMsg.Text = _files.Count & " Datei" & If(_files.Count <> 1, "en", "") & " gefunden"

        dgvErgebnisse.Focus()
        Cursor = Cursors.Default
    End Sub

    Private Sub Write_File_To_Grid(File As String, Optional Checked As Boolean = False)
        If Checked OrElse File.Contains("Ergebnisse.xml") Then
            Try
                Dim xDoc As XDocument = XDocument.Load(File)
                Dim xNummer = xDoc.<NewDataSet>.<wettkampf>.<Id>.Value
                Dim xBezeichnung = xDoc.<NewDataSet>.<wettkampf>.<Bezeichnung>.Value
                Dim xDatum = CDate(xDoc.<NewDataSet>.<wettkampf>.<Datum>.Value).ToShortDateString
                dgvErgebnisse.Rows.Add({"+", Checked, xNummer, xBezeichnung, xDatum})
                _files.Add(dgvErgebnisse.Rows.Count - 1, File)
                _groups.Add(dgvErgebnisse.Rows.Count - 1, New List(Of Boolean))
            Catch ex As Exception
                If Checked Then
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Ausgewählte Datei ist keine gültige Import-Datei.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                End If
            End Try
            btnImport.Enabled = dgvErgebnisse.Rows.Count > 0
        End If
    End Sub

    Private Sub dgvErgebnisse_SelectionChanged(sender As Object, e As EventArgs) Handles dgvErgebnisse.SelectionChanged

        CurrentRow = dgvErgebnisse.CurrentRow.Index

        'With dgvErgebnisse.CurrentRow.DefaultCellStyle
        '    .SelectionForeColor = .ForeColor
        'End With
        'With dgvErgebnisse
        '    .DefaultCellStyle.SelectionBackColor = If(.CurrentRow.Index Mod 2 = 0, .DefaultCellStyle.BackColor, .AlternatingRowsDefaultCellStyle.BackColor)
        'End With
    End Sub

    Private Sub dgvErgebnisse_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvErgebnisse.CellContentClick

        If e.RowIndex < 0 Then Return

        If e.ColumnIndex = 0 Then
            ' Expand
            dgvGruppe.Visible = e.RowIndex = CurrentRow AndAlso Not dgvGruppe.Visible
            If Not dgvGruppe.Visible Then
                'dgvErgebnisse.Rows(e.RowIndex).DividerHeight = 0
                Return
            End If

            Dim xDoc As XElement = XElement.Load(_files(e.RowIndex))
            Dim Groups = (From x In xDoc.Descendants("results")
                          Group By grp = x.Element("Gruppe").Value Into gr = Group).ToDictionary(Function(g) g.grp, Function(g) g.gr(0).<Bezeichnung>)

            With dgvGruppe
                .Rows.Clear()
                Dim i = 0
                For Each item In Groups
                    If _groups(dgvErgebnisse.CurrentRow.Index).Count = i Then
                        _groups(dgvErgebnisse.CurrentRow.Index).Add(CBool(dgvErgebnisse.CurrentRow.Cells("Checked").Value))
                    End If
                    dgvGruppe.Rows.Add({_groups(dgvErgebnisse.CurrentRow.Index)(i), item.Key, item.Value.Value})
                    i += 1
                Next
                If Not IsDBNull(dgvErgebnisse.CurrentRow.Cells("Checked").Value) Then checkerGruppe = If(CBool(dgvErgebnisse.CurrentRow.Cells("Checked").Value), .Rows.Count, 0)
                Dim RowCount = .Rows.Count
                If RowCount > 5 Then RowCount = 5
                .Height = .ColumnHeadersHeight + .RowTemplate.Height * RowCount + 2
                'dgvErgebnisse.Rows(e.RowIndex).DividerHeight = .Height
                Using f As New myFunction
                    .Location = .FindForm.PointToClient(f.Get_Location(dgvErgebnisse, e.ColumnIndex, e.RowIndex, Position.BottomRight, -1))
                End Using
                .Focus()
            End With
        ElseIf e.ColumnIndex = 1 Then
            ' Checked
            dgvErgebnisse.EndEdit()
            If CType(dgvErgebnisse(e.ColumnIndex, e.RowIndex).Value, CheckState) = CheckState.Indeterminate Then
                dgvErgebnisse(e.ColumnIndex, e.RowIndex).Value = False
                dgvErgebnisse.EndEdit()
            End If

            For i = 0 To dgvGruppe.Rows.Count - 1
                If dgvGruppe.Visible Then dgvGruppe("GruppeSelected", i).Value = dgvErgebnisse(e.ColumnIndex, e.RowIndex).Value
                If _groups.ContainsKey(e.RowIndex) Then _groups(e.RowIndex)(i) = CBool(dgvErgebnisse(e.ColumnIndex, e.RowIndex).Value)
            Next
        End If
    End Sub

    Private Sub dgvGruppe_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGruppe.CellContentClick
        If e.RowIndex < 0 OrElse e.ColumnIndex > 0 Then Return
        dgvGruppe.EndEdit()
        checkerGruppe += Math.Abs(CInt(dgvGruppe(e.ColumnIndex, e.RowIndex).Value)) * 2 - 1
        _groups(dgvErgebnisse.CurrentRow.Index)(e.RowIndex) = CBool(dgvGruppe(e.ColumnIndex, e.RowIndex).Value)
    End Sub

    Private Sub dgvGruppe_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvGruppe.KeyDown
        If e.KeyCode = Keys.Escape Then
            e.Handled = True
            dgvGruppe.Visible = False
        End If
    End Sub

    Private Sub dgvGruppe_VisibleChanged(sender As Object, e As EventArgs) Handles dgvGruppe.VisibleChanged
        If dgvGruppe.Visible Then
            CancelButton = Nothing
        Else
            CancelButton = btnCancel
        End If
    End Sub

End Class