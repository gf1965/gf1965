﻿Imports MySqlConnector

Module NewDatabase
    Class MySQL_Database
        Public Function CreateDatabase(Server As String, User As String, PW As String, Port As String, Database As String) As String
            Try
                Dim ConnString = "server=" + Server + ";userid=root;"
                If Not Port.Equals(String.Empty) Then ConnString += "port=" + Port + ";"

                Using Conn As New MySqlConnection(ConnString)
                    Conn.Open()
                    Dim cmd = New MySqlCommand("CREATE DATABASE " & Database & " CHARACTER SET utf8 COLLATE utf8_unicode_ci;", Conn)
                    cmd.ExecuteNonQuery()
                End Using
                Return String.Empty
            Catch ex As MySqlException
                Return ex.Message
            End Try
        End Function

        Public Function CreateUser(Server As String, User As String, PW As String, Port As String, Database As String) As String
            Dim cmd As MySqlCommand
            Dim query As String
            Dim info As New DataTable
            Dim Hosts = {"%", "localhost", "127.0.0.1", "::1"}
            Try
                Dim ConnString = "server=" + Server + ";userid=root;"
                If Not Port.Equals(String.Empty) Then ConnString += "port=" + Port + ";"

                Using Conn As New MySqlConnection(ConnString)
                    Conn.Open()
                    cmd = New MySqlCommand("SELECT user, host FROM mysql. user WHERE user = '" & User & "';", Conn)
                    info.Load(cmd.ExecuteReader)
                    For Each Host In Hosts
                        If info.Select("User = '" & User & "' And Host = '" & Host & "'").Length = 0 Then
                            query = "create user '" & User & "'@'" & Host & "'"
                            'If Not PW.Equals(String.Empty) Then query += " identified with mysql_native_password by '" & PW & "';"
                            If Not PW.Equals(String.Empty) Then query += " identified by '" & PW & "';"
                            cmd = New MySqlCommand(query, Conn)
                            cmd.ExecuteNonQuery()
                        End If
                        cmd = New MySqlCommand("GRANT ALL PRIVILEGES ON *.* TO '" & User & "'@'" & Host & "' WITH GRANT OPTION; FLUSH PRIVILEGES;", Conn)
                        cmd.ExecuteNonQuery()
                    Next
                End Using
                Return String.Empty
            Catch ex As MySqlException
                Return ex.Message
            End Try
        End Function

        Public Function CreateTables(Server As String, User As String, PW As String, Port As String, Database As String, InsertValues As Boolean) As String
            Try
                Dim Query = String.Empty
                'Dim ConnString = "server=" + Server + ";port=" + Port + ";database=" + Database + ";userid=root;"
                Dim ConnString = "server=" + Server + ";database=" + Database + ";userid=" & User & ";"
                If Not String.IsNullOrEmpty(PW) Then ConnString += "password=" + PW + ";"
                If Not String.IsNullOrEmpty(Port) Then ConnString += "port=" + Port + ";"

                Using Conn As New MySqlConnection(ConnString)
                    Conn.Open()

                    Query = CreateTables()
                    Dim cmd As New MySqlCommand(Query, Conn)
                    cmd.ExecuteNonQuery()

                    Query = Insert_Vereine()
                    cmd = New MySqlCommand(Query, Conn)
                    cmd.ExecuteNonQuery()

                End Using
                Return String.Empty
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Function DropDatabase(Server As String, User As String, PW As String, Port As String, Database As String) As String
            Try
                Dim ConnString = "server=" + Server + ";port=" + Port + ";userid=root;"
                Using Conn As New MySqlConnection(ConnString)
                    Conn.Open()
                    Dim cmd = New MySqlCommand("DROP Database IF EXISTS " & Database & ";", Conn)
                    cmd.ExecuteNonQuery()
                End Using
                Return String.Empty
            Catch ex As MySqlException
                Return ex.Message
            End Try
        End Function

        Private Function CreateTables() As String
            Dim Query = "
SET SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO';
START TRANSACTION;
SET time_zone = '+00:00';


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `weightlift`
--

-- --------------------------------------------------------
"
            Query += CreateAltersklassen()
            Query += CreateAlterskoeffizient()
            Query += CreateAthleten()
            Query += CreateAthletik()
            Query += CreateAthletikDefaults()
            Query += CreateAthletikDisziplinen()
            Query += CreateAthletikResults()
            Query += CreateFaktoren()
            Query += CreateGewichtsklassen()
            Query += CreateGruppen()
            Query += CreateHantel()
            Query += CreateKampfrichter()
            Query += CreateLänderwertung()
            Query += CreateMannschaftswertung()
            Query += CreateMastersNorm()
            Query += CreateMeldung()
            Query += CreateModus()
            Query += CreateModusFormeln()
            Query += CreateModusGewichtsgruppen()
            Query += CreateModusMannschaft()
            Query += CreateNotizen()
            Query += CreateRegion()
            Query += CreateReissen()
            Query += CreateRelativabzug()
            Query += CreateResults()
            Query += CreateStaaten()
            Query += CreateStossen()
            Query += CreateTeams()
            Query += CreateUsers()
            Query += CreateVerein()
            Query += CreateWertung()
            Query += CreateWertungBez()
            Query += CreateWettkampf()
            Query += CreateWettkampfAltersgruppen()
            Query += CreateWettkampfAltersklassen()
            Query += CreateWettkampfAthletik()
            Query += CreateWettkampfAthletikDefaults()
            Query += CreateWettkampfBez()
            Query += CreateWettkampfComment()
            Query += CreateWettkampfDetails()
            Query += CreateWettkampfGewichtsgruppen()
            Query += CreateWettkampfGewichtsteiler()
            Query += CreateWettkampfMannschaft()
            Query += CreateWettkampfMeldefrist()
            Query += CreateWettkampfMulti()
            Query += CreateWettkampfRestriction()
            Query += CreateWettkampfStartgeld()
            Query += CreateWettkampfTeams()
            Query += CreateWettkampfWertung()
            Query += CreateWettkampfZeitplan()
            Query += CreateWorldrecords()
            Query += CreateIndizes()
            Query += CreateAutoIncrement()
            Query += "
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
"
            Return Query
        End Function

        Private Function CreateAltersklassen() As String
            Return "
--
-- Tabellenstruktur für Tabelle `altersklassen`
--
DROP TABLE IF EXISTS altersklassen;

CREATE TABLE `altersklassen` (
  `idAltersklasse` int(2) NOT NULL,
  `International` tinyint(1) NOT NULL,
  `Altersklasse` varchar(15) DEFAULT NULL,
  `AK` int(2) DEFAULT NULL,
  `von` int(3) NOT NULL,
  `bis` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `altersklassen`
--

INSERT INTO `altersklassen` (`idAltersklasse`, `International`, `Altersklasse`, `AK`, `von`, `bis`) VALUES
(10, 0, 'Kinder', 10, 7, 10),
(11, 0, 'Kinder', 11, 11, 11),
(12, 0, 'Kinder', 12, 12, 12),
(20, 0, 'Schüler', 13, 13, 13),
(20, 1, 'U15', NULL, 13, 15),
(21, 0, 'Schüler', 14, 14, 14),
(22, 0, 'Schüler', 15, 15, 15),
(30, 0, 'Jugend', NULL, 16, 17),
(30, 1, 'Youth', NULL, 16, 17),
(40, 0, 'Junioren', NULL, 18, 20),
(40, 1, 'Junior', NULL, 18, 20),
(45, 1, 'U23', NULL, 21, 23),
(50, 0, 'Aktive', NULL, 21, 29),
(50, 1, 'Senior', NULL, 24, 34),
(100, 0, 'Masters', 0, 30, 34),
(110, 0, 'Masters', 1, 35, 39),
(110, 1, 'Masters', 1, 35, 39),
(120, 0, 'Masters', 2, 40, 44),
(120, 1, 'Masters', 2, 40, 44),
(130, 0, 'Masters', 3, 45, 49),
(130, 1, 'Masters', 3, 45, 49),
(140, 0, 'Masters', 4, 50, 54),
(140, 1, 'Masters', 4, 50, 54),
(150, 0, 'Masters', 5, 55, 59),
(150, 1, 'Masters', 5, 55, 59),
(160, 0, 'Masters', 6, 60, 64),
(160, 1, 'Masters', 6, 60, 64),
(170, 0, 'Masters', 7, 65, 69),
(170, 1, 'Masters', 7, 65, 69),
(180, 0, 'Masters', 8, 70, 74),
(180, 1, 'Masters', 8, 70, 74),
(190, 0, 'Masters', 9, 75, 79),
(190, 1, 'Masters', 9, 75, 79),
(200, 0, 'Masters', 10, 80, 120),
(200, 1, 'Masters', 10, 80, 120);

-- --------------------------------------------------------
"
        End Function

        Private Function CreateAlterskoeffizient() As String
            Return "
--
-- Tabellenstruktur für Tabelle `alterskoeffizient`
--
DROP TABLE IF EXISTS alterskoeffizient;


CREATE TABLE `alterskoeffizient` (
  `idAlter` int(2) NOT NULL DEFAULT 0,
  `Faktor` double(4,3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ab 2016';

--
-- Daten für Tabelle `alterskoeffizient`
--

INSERT INTO `alterskoeffizient` (`idAlter`, `Faktor`) VALUES
(30, 1.000),
(31, 1.016),
(32, 1.031),
(33, 1.046),
(34, 1.059),
(35, 1.072),
(36, 1.083),
(37, 1.096),
(38, 1.109),
(39, 1.122),
(40, 1.135),
(41, 1.149),
(42, 1.162),
(43, 1.176),
(44, 1.189),
(45, 1.203),
(46, 1.218),
(47, 1.233),
(48, 1.248),
(49, 1.263),
(50, 1.279),
(51, 1.297),
(52, 1.316),
(53, 1.338),
(54, 1.361),
(55, 1.385),
(56, 1.411),
(57, 1.437),
(58, 1.462),
(59, 1.488),
(60, 1.514),
(61, 1.541),
(62, 1.568),
(63, 1.598),
(64, 1.629),
(65, 1.663),
(66, 1.699),
(67, 1.738),
(68, 1.779),
(69, 1.823),
(70, 1.867),
(71, 1.910),
(72, 1.953),
(73, 2.004),
(74, 2.060),
(75, 2.117),
(76, 2.181),
(77, 2.255),
(78, 2.336),
(79, 2.419),
(80, 2.504),
(81, 2.597),
(82, 2.702),
(83, 2.831),
(84, 2.981),
(85, 3.153),
(86, 3.352),
(87, 3.580),
(88, 3.843),
(89, 4.145),
(90, 4.493);

-- --------------------------------------------------------
"
        End Function

        Private Function CreateAthleten() As String
            Return "
--
-- Tabellenstruktur für Tabelle `athleten`
--
DROP TABLE IF EXISTS athleten;

CREATE TABLE `athleten` (
  `idTeilnehmer` int(11) NOT NULL,
  `GUID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Startbuch_Nummer` int(11) DEFAULT NULL,
  `Titel` varchar(20) NOT NULL DEFAULT '',
  `Nachname` varchar(45) NOT NULL,
  `Vorname` varchar(45) NOT NULL,
  `Verein` int(11) NOT NULL DEFAULT 0 COMMENT 'Heimatverein',
  `Verein_GUID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ESR` int(11) DEFAULT NULL COMMENT 'Einzelstartrecht',
  `MSR` int(11) DEFAULT NULL COMMENT 'Mannschaftsstartrecht',
  `Jahrgang` year(4) NOT NULL,
  `Geburtstag` date DEFAULT NULL,
  `Geschlecht` varchar(8) DEFAULT NULL,
  `Staat` int(3) NOT NULL DEFAULT 0,
  `Lizenz` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateAthletik() As String
            Return "
--
-- Tabellenstruktur für Tabelle `athletik`
--
DROP TABLE IF EXISTS athletik;

CREATE TABLE `athletik` (
  `Wettkampf` int(11) NOT NULL,
  `Teilnehmer` int(11) NOT NULL,
  `Disziplin` int(2) NOT NULL,
  `Durchgang` int(1) NOT NULL,
  `Wert` double(7,2) DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateAthletikDefaults() As String
            Return "
--
-- Tabellenstruktur für Tabelle `athletik_defaults`
--
DROP TABLE IF EXISTS athletik_defaults;

CREATE TABLE `athletik_defaults` (
  `Disziplin` int(2) NOT NULL,
  `Bezeichnung` varchar(15) NOT NULL,
  `Geschlecht` varchar(1) NOT NULL,
  `ak5` double(4,2) DEFAULT NULL,
  `ak6` double(4,2) DEFAULT NULL,
  `ak7` double(4,2) DEFAULT NULL,
  `ak8` double(4,2) DEFAULT NULL,
  `ak9` double(4,2) DEFAULT NULL,
  `ak10` double(4,2) DEFAULT NULL,
  `ak11` double(4,2) DEFAULT NULL,
  `ak12` double(4,2) DEFAULT NULL,
  `ak13` double(4,2) DEFAULT NULL,
  `ak14` double(4,2) DEFAULT NULL,
  `ak15` double(4,2) DEFAULT NULL,
  `ak16` double(4,2) DEFAULT NULL,
  `ak17` double(4,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `athletik_defaults`
--

INSERT INTO `athletik_defaults` (`Disziplin`, `Bezeichnung`, `Geschlecht`, `ak5`, `ak6`, `ak7`, `ak8`, `ak9`, `ak10`, `ak11`, `ak12`, `ak13`, `ak14`, `ak15`, `ak16`, `ak17`) VALUES
(10, 'Anzahl', 'm', NULL, NULL, NULL, NULL, NULL, 10.00, 10.00, 10.00, 12.00, 15.00, 15.00, 15.00, 15.00),
(10, 'Anzahl', 'w', NULL, NULL, NULL, NULL, NULL, 10.00, 10.00, 10.00, 12.00, 15.00, 15.00, 15.00, 15.00),
(10, 'Punkte', 'm', NULL, NULL, NULL, NULL, NULL, 15.00, 15.00, 15.00, 12.50, 10.00, 10.00, 10.00, 10.00),
(10, 'Punkte', 'w', NULL, NULL, NULL, NULL, NULL, 15.00, 15.00, 15.00, 12.50, 10.00, 10.00, 10.00, 10.00),
(20, 'Anzahl', 'm', NULL, NULL, NULL, NULL, NULL, 15.00, 15.00, 15.00, 15.00, 15.00, 15.00, NULL, NULL),
(20, 'Anzahl', 'w', NULL, NULL, NULL, NULL, NULL, 15.00, 15.00, 15.00, 15.00, 15.00, 15.00, NULL, NULL),
(20, 'Faktor', 'm', NULL, NULL, NULL, NULL, NULL, 0.50, 0.57, 0.65, 0.70, 0.75, 0.75, NULL, NULL),
(20, 'Faktor', 'w', NULL, NULL, NULL, NULL, NULL, 0.45, 0.48, 0.52, 0.57, 0.63, 0.63, NULL, NULL),
(20, 'Punkte', 'm', NULL, NULL, NULL, NULL, NULL, 10.00, 10.00, 10.00, 10.00, 10.00, 10.00, NULL, NULL),
(20, 'Punkte', 'w', NULL, NULL, NULL, NULL, NULL, 10.00, 10.00, 10.00, 10.00, 10.00, 10.00, NULL, NULL),
(60, 'Anzahl', 'm', NULL, NULL, NULL, NULL, NULL, 10.00, 10.00, 10.00, 12.00, 15.00, 15.00, 15.00, 15.00),
(60, 'Anzahl', 'w', NULL, NULL, NULL, NULL, NULL, 10.00, 10.00, 10.00, 10.00, 12.00, 12.00, 12.00, 12.00),
(60, 'Punkte', 'm', NULL, NULL, NULL, NULL, NULL, 15.00, 15.00, 15.00, 12.50, 10.00, 10.00, 10.00, 10.00),
(60, 'Punkte', 'w', NULL, NULL, NULL, NULL, NULL, 15.00, 15.00, 15.00, 15.00, 12.50, 12.50, 12.50, 12.50),
(110, 'Kugel', 'm', 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 3.00, 3.00, 4.00, 4.00, 5.00, 5.00, NULL),
(110, 'Kugel', 'w', 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 3.00, 3.00, 4.00, 4.00, NULL),
(130, 'Anzahl', 'm', NULL, NULL, NULL, NULL, NULL, 15.00, 15.00, 15.00, 15.00, 15.00, 15.00, 12.00, 10.00),
(130, 'Anzahl', 'w', NULL, NULL, NULL, NULL, NULL, 15.00, 15.00, 15.00, 15.00, 15.00, 15.00, 12.00, 10.00),
(130, 'Faktor', 'm', NULL, NULL, NULL, NULL, NULL, 0.80, 0.80, 0.80, 0.80, 0.80, 0.80, 0.90, 1.00),
(130, 'Faktor', 'w', NULL, NULL, NULL, NULL, NULL, 0.60, 0.60, 0.60, 0.60, 0.60, 0.60, 0.67, 0.75),
(130, 'Punkte', 'm', NULL, NULL, NULL, NULL, NULL, 10.00, 10.00, 10.00, 10.00, 10.00, 10.00, 12.50, 15.00),
(130, 'Punkte', 'w', NULL, NULL, NULL, NULL, NULL, 10.00, 10.00, 10.00, 10.00, 10.00, 10.00, 12.50, 15.00);

-- --------------------------------------------------------
"
        End Function

        Private Function CreateAthletikDisziplinen() As String
            Return "
--
-- Tabellenstruktur für Tabelle `athletik_disziplinen`
--
DROP TABLE IF EXISTS athletik_disziplinen;

CREATE TABLE `athletik_disziplinen` (
  `idDisziplin` int(2) NOT NULL COMMENT 'athletik: D+idDisziplin',
  `Bezeichnung` varchar(30) NOT NULL,
  `Durchgaenge` int(1) NOT NULL,
  `Formel` varchar(60) NOT NULL,
  `V_min` double(4,1) DEFAULT NULL,
  `V_max` double(4,1) DEFAULT NULL,
  `Einheit` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `athletik_disziplinen`
--

INSERT INTO `athletik_disziplinen` (`idDisziplin`, `Bezeichnung`, `Durchgaenge`, `Formel`, `V_min`, `V_max`, `Einheit`) VALUES
(10, 'Anristen', 1, 'Anzahl * Punkte', NULL, NULL, 'Anzahl'),
(20, 'Bankdrücken', 1, 'Anzahl * 10', NULL, NULL, 'Anzahl'),
(30, 'Beugestütz', 1, 'Anzahl * 5', NULL, NULL, 'Anzahl'),
(40, 'CounterMoveJump', 2, 'Differenz * 3', NULL, NULL, 'Zentimeter'),
(50, 'Differenzsprung', 2, 'Differenz * 2,25', NULL, NULL, 'Zentimeter'),
(55, 'Kastenbumeranglauf', 2, '(15.5 - Zeit) * 20 + 100', NULL, 20.5, 'Sekunden'),
(60, 'Klimmzug', 1, 'Anzahl * Punkte', NULL, NULL, 'Anzahl'),
(65, 'Lauftest', 2, '400 - Zeit * 20', NULL, 20.0, 'Sekunden'),
(70, 'Liegestütz', 1, 'Anzahl * 4,5', NULL, NULL, 'Anzahl'),
(80, 'Pendellauf', 2, '(13 - Zeit) * 20 + 100', NULL, 18.0, 'Sekunden'),
(90, 'Schlussdreisprung', 3, 'Weite * 0,2', 300.0, NULL, 'Zentimeter'),
(100, 'Schlussweitsprung', 3, '75 + (Weite - 100) / 2', 100.0, NULL, 'Zentimeter'),
(110, 'Schockwurf', 3, 'Weite * 7,5 / KG', NULL, NULL, 'Zentimeter'),
(120, '30m-Sprint', 2, '(6 - Zeit) * 40 + 100', NULL, 8.5, 'Sekunden'),
(130, 'Zug liegend', 1, 'Anzahl * Punkte', NULL, NULL, 'Anzahl');

-- --------------------------------------------------------
"
        End Function

        Private Function CreateAthletikResults() As String
            Return "
--
-- Tabellenstruktur für Tabelle `athletik_results`
--
DROP TABLE IF EXISTS athletik_results;

CREATE TABLE `athletik_results` (
  `Wettkampf` int(11) NOT NULL,
  `Teilnehmer` int(11) NOT NULL,
  `Disziplin` int(2) NOT NULL,
  `Resultat` double(5,2) DEFAULT NULL,
  `Platz` int(3) DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateFaktoren() As String
            Return "
--
-- Tabellenstruktur für Tabelle `faktoren`
--
DROP TABLE IF EXISTS faktoren;

CREATE TABLE `faktoren` (
  `sex` varchar(1) NOT NULL,
  `Faktor` double(11,10) NOT NULL COMMENT 'Faktor der Sinclair-Formel',
  `minKG` double(6,3) NOT NULL COMMENT 'minimales KG in Sinclair-Formel',
  `maxKG` double(6,3) NOT NULL COMMENT 'maximales KG in Sinclair-Formel',
  `maxSC` double(7,6) NOT NULL COMMENT 'maximaler Sinclair-Faktor'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `faktoren`
--

INSERT INTO `faktoren` (`sex`, `Faktor`, `minKG`, `maxKG`, `maxSC`) VALUES
('m', 0.7227625210, 32.000, 193.609, 2.765241),
('w', 0.7870043410, 28.000, 153.757, 2.695177);

-- --------------------------------------------------------
"
        End Function

        Private Function CreateGewichtsklassen() As String
            Return "
--
-- Tabellenstruktur für Tabelle `gewichtsklassen`
--

CREATE TABLE `gewichtsklassen` (
  `International` tinyint(1) NOT NULL,
  `idAK` int(3) NOT NULL,
  `Sex` varchar(1) NOT NULL,
  `idGK` int(3) NOT NULL,
  `GK` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `gewichtsklassen`
--

INSERT INTO `gewichtsklassen` (`International`, `idAK`, `Sex`, `idGK`, `GK`) VALUES
(0, 10, 'm', 5, '-25'),
(0, 10, 'm', 10, '-30'),
(0, 10, 'm', 20, '-35'),
(0, 10, 'm', 30, '-40'),
(0, 10, 'm', 40, '-45'),
(0, 10, 'm', 50, '-49'),
(0, 10, 'm', 60, '-55'),
(0, 10, 'm', 70, '+55'),
(0, 10, 'w', 5, '-25'),
(0, 10, 'w', 10, '-30'),
(0, 10, 'w', 20, '-35'),
(0, 10, 'w', 30, '-40'),
(0, 10, 'w', 40, '-45'),
(0, 10, 'w', 50, '-49'),
(0, 10, 'w', 60, '+49'),
(0, 11, 'm', 10, '-30'),
(0, 11, 'm', 20, '-35'),
(0, 11, 'm', 30, '-40'),
(0, 11, 'm', 40, '-45'),
(0, 11, 'm', 50, '-49'),
(0, 11, 'm', 60, '-55'),
(0, 11, 'm', 70, '-61'),
(0, 11, 'm', 80, '+61'),
(0, 11, 'w', 10, '-30'),
(0, 11, 'w', 20, '-35'),
(0, 11, 'w', 30, '-40'),
(0, 11, 'w', 40, '-45'),
(0, 11, 'w', 50, '-49'),
(0, 11, 'w', 60, '-55'),
(0, 11, 'w', 70, '+55'),
(0, 12, 'm', 10, '-30'),
(0, 12, 'm', 20, '-35'),
(0, 12, 'm', 30, '-40'),
(0, 12, 'm', 40, '-45'),
(0, 12, 'm', 50, '-49'),
(0, 12, 'm', 60, '-55'),
(0, 12, 'm', 70, '-61'),
(0, 12, 'm', 80, '-67'),
(0, 12, 'm', 90, '+67'),
(0, 12, 'w', 10, '-30'),
(0, 12, 'w', 20, '-35'),
(0, 12, 'w', 30, '-40'),
(0, 12, 'w', 40, '-45'),
(0, 12, 'w', 50, '-49'),
(0, 12, 'w', 60, '-55'),
(0, 12, 'w', 70, '-59'),
(0, 12, 'w', 80, '+59'),
(0, 20, 'm', 20, '-35'),
(0, 20, 'm', 30, '-40'),
(0, 20, 'm', 40, '-45'),
(0, 20, 'm', 50, '-49'),
(0, 20, 'm', 60, '-55'),
(0, 20, 'm', 70, '-61'),
(0, 20, 'm', 80, '-67'),
(0, 20, 'm', 90, '-73'),
(0, 20, 'm', 100, '+73'),
(0, 20, 'w', 20, '-35'),
(0, 20, 'w', 30, '-40'),
(0, 20, 'w', 40, '-45'),
(0, 20, 'w', 50, '-49'),
(0, 20, 'w', 60, '-55'),
(0, 20, 'w', 70, '-59'),
(0, 20, 'w', 80, '-64'),
(0, 20, 'w', 90, '+64'),
(0, 21, 'm', 30, '-40'),
(0, 21, 'm', 40, '-45'),
(0, 21, 'm', 50, '-49'),
(0, 21, 'm', 60, '-55'),
(0, 21, 'm', 70, '-61'),
(0, 21, 'm', 80, '-67'),
(0, 21, 'm', 90, '-73'),
(0, 21, 'm', 100, '-81'),
(0, 21, 'm', 110, '-89'),
(0, 21, 'm', 120, '+89'),
(0, 21, 'w', 30, '-40'),
(0, 21, 'w', 40, '-45'),
(0, 21, 'w', 50, '-49'),
(0, 21, 'w', 60, '-55'),
(0, 21, 'w', 70, '-59'),
(0, 21, 'w', 80, '-64'),
(0, 21, 'w', 90, '-71'),
(0, 21, 'w', 100, '+71'),
(0, 22, 'm', 40, '-45'),
(0, 22, 'm', 50, '-49'),
(0, 22, 'm', 60, '-55'),
(0, 22, 'm', 70, '-61'),
(0, 22, 'm', 80, '-67'),
(0, 22, 'm', 90, '-73'),
(0, 22, 'm', 100, '-81'),
(0, 22, 'm', 110, '-89'),
(0, 22, 'm', 120, '-96'),
(0, 22, 'm', 130, '+96'),
(0, 22, 'w', 30, '-40'),
(0, 22, 'w', 40, '-45'),
(0, 22, 'w', 50, '-49'),
(0, 22, 'w', 60, '-55'),
(0, 22, 'w', 70, '-59'),
(0, 22, 'w', 80, '-64'),
(0, 22, 'w', 90, '-71'),
(0, 22, 'w', 100, '-76'),
(0, 22, 'w', 110, '+76'),
(0, 30, 'm', 50, '-49'),
(0, 30, 'm', 60, '-55'),
(0, 30, 'm', 70, '-61'),
(0, 30, 'm', 80, '-67'),
(0, 30, 'm', 90, '-73'),
(0, 30, 'm', 100, '-81'),
(0, 30, 'm', 110, '-89'),
(0, 30, 'm', 120, '-96'),
(0, 30, 'm', 130, '-102'),
(0, 30, 'm', 140, '+102'),
(0, 30, 'w', 30, '-40'),
(0, 30, 'w', 40, '-45'),
(0, 30, 'w', 50, '-49'),
(0, 30, 'w', 60, '-55'),
(0, 30, 'w', 70, '-59'),
(0, 30, 'w', 80, '-64'),
(0, 30, 'w', 90, '-71'),
(0, 30, 'w', 100, '-76'),
(0, 30, 'w', 110, '-81'),
(0, 30, 'w', 120, '+81'),
(0, 40, 'm', 60, '-55'),
(0, 40, 'm', 70, '-61'),
(0, 40, 'm', 80, '-67'),
(0, 40, 'm', 90, '-73'),
(0, 40, 'm', 100, '-81'),
(0, 40, 'm', 110, '-89'),
(0, 40, 'm', 120, '-96'),
(0, 40, 'm', 130, '-102'),
(0, 40, 'm', 140, '-109'),
(0, 40, 'm', 150, '+109'),
(0, 40, 'w', 40, '-45'),
(0, 40, 'w', 50, '-49'),
(0, 40, 'w', 60, '-55'),
(0, 40, 'w', 70, '-59'),
(0, 40, 'w', 80, '-64'),
(0, 40, 'w', 90, '-71'),
(0, 40, 'w', 100, '-76'),
(0, 40, 'w', 110, '-81'),
(0, 40, 'w', 120, '-87'),
(0, 40, 'w', 130, '+87'),
(0, 50, 'm', 60, '-55'),
(0, 50, 'm', 70, '-61'),
(0, 50, 'm', 80, '-67'),
(0, 50, 'm', 90, '-73'),
(0, 50, 'm', 100, '-81'),
(0, 50, 'm', 110, '-89'),
(0, 50, 'm', 120, '-96'),
(0, 50, 'm', 130, '-102'),
(0, 50, 'm', 140, '-109'),
(0, 50, 'm', 150, '+109'),
(0, 50, 'w', 40, '-45'),
(0, 50, 'w', 50, '-49'),
(0, 50, 'w', 60, '-55'),
(0, 50, 'w', 70, '-59'),
(0, 50, 'w', 80, '-64'),
(0, 50, 'w', 90, '-71'),
(0, 50, 'w', 100, '-76'),
(0, 50, 'w', 110, '-81'),
(0, 50, 'w', 120, '-87'),
(0, 50, 'w', 130, '+87'),
(0, 100, 'm', 60, '-55'),
(0, 100, 'm', 70, '-61'),
(0, 100, 'm', 80, '-67'),
(0, 100, 'm', 90, '-73'),
(0, 100, 'm', 100, '-81'),
(0, 100, 'm', 110, '-89'),
(0, 100, 'm', 120, '-96'),
(0, 100, 'm', 130, '-102'),
(0, 100, 'm', 140, '-109'),
(0, 100, 'm', 150, '+109'),
(0, 100, 'w', 40, '-45'),
(0, 100, 'w', 50, '-49'),
(0, 100, 'w', 60, '-55'),
(0, 100, 'w', 70, '-59'),
(0, 100, 'w', 80, '-64'),
(0, 100, 'w', 90, '-71'),
(0, 100, 'w', 100, '-76'),
(0, 100, 'w', 110, '-81'),
(0, 100, 'w', 120, '-87'),
(0, 100, 'w', 130, '+87'),
(1, 20, 'm', 20, '-35'),
(1, 20, 'm', 30, '-40'),
(1, 20, 'm', 40, '-45'),
(1, 20, 'm', 50, '-49'),
(1, 20, 'm', 60, '-55'),
(1, 20, 'm', 70, '-61'),
(1, 20, 'm', 80, '-67'),
(1, 20, 'm', 90, '-73'),
(1, 20, 'm', 100, '+73'),
(1, 20, 'w', 20, '-35'),
(1, 20, 'w', 30, '-40'),
(1, 20, 'w', 40, '-45'),
(1, 20, 'w', 50, '-49'),
(1, 20, 'w', 60, '-55'),
(1, 20, 'w', 70, '-59'),
(1, 20, 'w', 80, '-64'),
(1, 20, 'w', 90, '+64'),
(1, 21, 'm', 30, '-40'),
(1, 21, 'm', 40, '-45'),
(1, 21, 'm', 50, '-49'),
(1, 21, 'm', 60, '-55'),
(1, 21, 'm', 70, '-61'),
(1, 21, 'm', 80, '-67'),
(1, 21, 'm', 90, '-73'),
(1, 21, 'm', 100, '-81'),
(1, 21, 'm', 110, '-89'),
(1, 21, 'm', 120, '+89'),
(1, 21, 'w', 30, '-40'),
(1, 21, 'w', 40, '-45'),
(1, 21, 'w', 50, '-49'),
(1, 21, 'w', 60, '-55'),
(1, 21, 'w', 70, '-59'),
(1, 21, 'w', 80, '-64'),
(1, 21, 'w', 90, '-71'),
(1, 21, 'w', 100, '+71'),
(1, 22, 'm', 40, '-45'),
(1, 22, 'm', 50, '-49'),
(1, 22, 'm', 60, '-55'),
(1, 22, 'm', 70, '-61'),
(1, 22, 'm', 80, '-67'),
(1, 22, 'm', 90, '-73'),
(1, 22, 'm', 100, '-81'),
(1, 22, 'm', 110, '-89'),
(1, 22, 'm', 120, '-96'),
(1, 22, 'm', 130, '+96'),
(1, 22, 'w', 30, '-40'),
(1, 22, 'w', 40, '-45'),
(1, 22, 'w', 50, '-49'),
(1, 22, 'w', 60, '-55'),
(1, 22, 'w', 70, '-59'),
(1, 22, 'w', 80, '-64'),
(1, 22, 'w', 90, '-71'),
(1, 22, 'w', 100, '-76'),
(1, 22, 'w', 110, '+76'),
(1, 30, 'm', 50, '-49'),
(1, 30, 'm', 60, '-55'),
(1, 30, 'm', 70, '-61'),
(1, 30, 'm', 80, '-67'),
(1, 30, 'm', 90, '-73'),
(1, 30, 'm', 100, '-81'),
(1, 30, 'm', 110, '-89'),
(1, 30, 'm', 120, '-96'),
(1, 30, 'm', 130, '-102'),
(1, 30, 'm', 140, '+102'),
(1, 30, 'w', 30, '-40'),
(1, 30, 'w', 40, '-45'),
(1, 30, 'w', 50, '-49'),
(1, 30, 'w', 60, '-55'),
(1, 30, 'w', 70, '-59'),
(1, 30, 'w', 80, '-64'),
(1, 30, 'w', 90, '-71'),
(1, 30, 'w', 100, '-76'),
(1, 30, 'w', 110, '-81'),
(1, 30, 'w', 120, '+81'),
(1, 40, 'm', 60, '-55'),
(1, 40, 'm', 70, '-61'),
(1, 40, 'm', 80, '-67'),
(1, 40, 'm', 90, '-73'),
(1, 40, 'm', 100, '-81'),
(1, 40, 'm', 110, '-89'),
(1, 40, 'm', 120, '-96'),
(1, 40, 'm', 130, '-102'),
(1, 40, 'm', 140, '-109'),
(1, 40, 'm', 150, '+109'),
(1, 40, 'w', 40, '-45'),
(1, 40, 'w', 50, '-49'),
(1, 40, 'w', 60, '-55'),
(1, 40, 'w', 70, '-59'),
(1, 40, 'w', 80, '-64'),
(1, 40, 'w', 90, '-71'),
(1, 40, 'w', 100, '-76'),
(1, 40, 'w', 110, '-81'),
(1, 40, 'w', 120, '-87'),
(1, 40, 'w', 130, '+87'),
(1, 45, 'm', 60, '-55'),
(1, 45, 'm', 70, '-61'),
(1, 45, 'm', 80, '-67'),
(1, 45, 'm', 90, '-73'),
(1, 45, 'm', 100, '-81'),
(1, 45, 'm', 110, '-89'),
(1, 45, 'm', 120, '-96'),
(1, 45, 'm', 130, '-102'),
(1, 45, 'm', 140, '-109'),
(1, 45, 'm', 150, '+109'),
(1, 45, 'w', 40, '-45'),
(1, 45, 'w', 50, '-49'),
(1, 45, 'w', 60, '-55'),
(1, 45, 'w', 70, '-59'),
(1, 45, 'w', 80, '-64'),
(1, 45, 'w', 90, '-71'),
(1, 45, 'w', 100, '-76'),
(1, 45, 'w', 110, '-81'),
(1, 45, 'w', 120, '-87'),
(1, 45, 'w', 130, '+87'),
(1, 50, 'm', 60, '-55'),
(1, 50, 'm', 70, '-61'),
(1, 50, 'm', 80, '-67'),
(1, 50, 'm', 90, '-73'),
(1, 50, 'm', 100, '-81'),
(1, 50, 'm', 110, '-89'),
(1, 50, 'm', 120, '-96'),
(1, 50, 'm', 130, '-102'),
(1, 50, 'm', 140, '-109'),
(1, 50, 'm', 150, '+109'),
(1, 50, 'w', 40, '-45'),
(1, 50, 'w', 50, '-49'),
(1, 50, 'w', 60, '-55'),
(1, 50, 'w', 70, '-59'),
(1, 50, 'w', 80, '-64'),
(1, 50, 'w', 90, '-71'),
(1, 50, 'w', 100, '-76'),
(1, 50, 'w', 110, '-81'),
(1, 50, 'w', 120, '-87'),
(1, 50, 'w', 130, '+87'),
(1, 100, 'm', 60, '-55'),
(1, 100, 'm', 70, '-61'),
(1, 100, 'm', 80, '-67'),
(1, 100, 'm', 90, '-73'),
(1, 100, 'm', 100, '-81'),
(1, 100, 'm', 110, '-89'),
(1, 100, 'm', 120, '-96'),
(1, 100, 'm', 130, '-102'),
(1, 100, 'm', 140, '-109'),
(1, 100, 'm', 150, '+109'),
(1, 100, 'w', 40, '-45'),
(1, 100, 'w', 50, '-49'),
(1, 100, 'w', 60, '-55'),
(1, 100, 'w', 70, '-59'),
(1, 100, 'w', 80, '-64'),
(1, 100, 'w', 90, '-71'),
(1, 100, 'w', 100, '-76'),
(1, 100, 'w', 110, '-81'),
(1, 100, 'w', 120, '-87'),
(1, 100, 'w', 130, '+87');

-- --------------------------------------------------------
"
        End Function

        Private Function CreateGruppen() As String
            Return "
--
-- Tabellenstruktur für Tabelle `gruppen`
--

CREATE TABLE `gruppen` (
  `Wettkampf` int(11) NOT NULL,
  `Gruppe` int(4) NOT NULL COMMENT 'Wertungsgruppe',
  `Teilgruppe` varchar(1) NOT NULL DEFAULT '' COMMENT 'A-/B-Gruppen',
  `WG` int(4) DEFAULT NULL COMMENT 'Wettkampf-Gruppe',
  `Einteilung` varchar(100) NOT NULL DEFAULT '',
  `Bezeichnung` varchar(100) NOT NULL DEFAULT '' COMMENT 'Bezeichnung Wertungsgruppe',
  `Bez_WG` varchar(100) DEFAULT NULL COMMENT 'Bezeichnung WK-Gruppe',
  `Datum` date DEFAULT NULL COMMENT 'Datum Wettkampf',
  `DatumW` date DEFAULT NULL COMMENT 'Datum Wiegen',
  `WiegenBeginn` datetime DEFAULT NULL,
  `WiegenEnde` datetime DEFAULT NULL,
  `Reissen` datetime DEFAULT NULL,
  `Stossen` datetime DEFAULT NULL,
  `DatumA` date DEFAULT NULL,
  `Athletik` datetime DEFAULT NULL,
  `Blockheben` tinyint(1) NOT NULL DEFAULT 0,
  `Bohle` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateKampfrichter() As String
            Return "
--
-- Tabellenstruktur für Tabelle `gruppen_kampfrichter`
--

CREATE TABLE `gruppen_kampfrichter` (
  `Wettkampf` int(11) NOT NULL,
  `Gruppe` int(4) NOT NULL,
  `Jury_1` varchar(50) DEFAULT NULL,
  `Jury_2` varchar(50) DEFAULT NULL,
  `Jury_3` varchar(50) DEFAULT NULL,
  `Jury_4` varchar(50) DEFAULT NULL,
  `Jury_5` varchar(50) DEFAULT NULL,
  `KR_1` varchar(50) DEFAULT NULL,
  `KR_2` varchar(50) DEFAULT NULL,
  `KR_3` varchar(50) DEFAULT NULL,
  `ZN` varchar(50) DEFAULT NULL COMMENT 'Zeitnehmer',
  `TC` varchar(50) DEFAULT NULL COMMENT 'Teschnicher Kontrolleur',
  `CM` varchar(50) DEFAULT NULL COMMENT 'Wettkampfleiter',
  `Sec` varchar(50) DEFAULT NULL COMMENT 'Listenführer',
  `Doc` varchar(50) DEFAULT NULL COMMENT 'Arzt'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateHantel() As String
            Return "
--
-- Tabellenstruktur für Tabelle `hantel`
--

CREATE TABLE `hantel` (
  `ID` int(2) NOT NULL,
  `Bezeichnung` varchar(15) NOT NULL,
  `Gewicht` double(4,2) NOT NULL,
  `Farbe` varchar(15) NOT NULL COMMENT 'System.Drawing.Color.FromName(Farbe)',
  `Durchmesser` int(3) NOT NULL,
  `Dicke` int(3) NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `hantel`
--

INSERT INTO `hantel` (`ID`, `Bezeichnung`, `Gewicht`, `Farbe`, `Durchmesser`, `Dicke`, `available`) VALUES
(0, 'Scheibe', 0.50, 'White', 135, 12, 1),
(1, 'Scheibe', 1.00, 'Green', 160, 15, 1),
(2, 'Scheibe', 1.50, 'Yellow', 175, 18, 1),
(3, 'Scheibe', 2.00, 'Blue', 190, 19, 1),
(4, 'Verschluss', 2.50, 'Silver', 90, 60, 1),
(5, 'Scheibe', 2.50, 'Red', 210, 19, 1),
(6, 'Scheibe', 5.00, 'White', 230, 26, 1),
(7, 'Scheibe', 10.00, 'Green', 450, 34, 1),
(8, 'Scheibe', 15.00, 'Yellow', 450, 42, 1),
(9, 'Scheibe', 20.00, 'Blue', 450, 54, 1),
(10, 'Scheibe', 25.00, 'Red', 450, 67, 1),
(13, 'Hantel_m', 20.00, 'Gray', 50, 415, 1),
(14, 'Hantel_w', 15.00, 'Gray', 50, 415, 1),
(15, 'Hantel_k', 7.00, 'Gray', 50, 415, 1),
(16, 'Hantel_x', 5.00, 'Gray', 50, 415, 1),
(17, 'Hantel_Anschlag', 0.00, 'Gray', 70, 20, 1),
(18, 'Hantel_Stange', 0.00, 'Gray', 25, 200, 1),
(101, 'Dummy', 1.00, 'LimeGreen', 450, 90, 1),
(102, 'Dummy', 1.50, 'Green', 450, 30, 0),
(103, 'Dummy', 2.00, 'DodgerBlue', 450, 90, 1),
(105, 'Dummy', 2.50, 'OrangeRed', 450, 90, 1),
(106, 'Dummy', 5.00, 'WhiteSmoke', 450, 101, 1);

-- --------------------------------------------------------
"
        End Function

        Private Function CreateLänderwertung() As String
            Return "
--
-- Tabellenstruktur für Tabelle `laenderwertung`
--

CREATE TABLE `laenderwertung` (
  `Platz` int(3) NOT NULL,
  `Punkte` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateMannschaftswertung() As String
            Return "
--
-- Tabellenstruktur für Tabelle `mannschaftswertung`
--

CREATE TABLE `mannschaftswertung` (
  `Platz` int(2) NOT NULL,
  `Punkte` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `mannschaftswertung`
--

INSERT INTO `mannschaftswertung` (`Platz`, `Punkte`) VALUES
(1, 28),
(2, 25),
(3, 23),
(4, 22),
(5, 21),
(6, 20),
(7, 19),
(8, 18),
(9, 17),
(10, 16),
(11, 15),
(12, 14),
(13, 13),
(14, 12),
(15, 11),
(16, 10),
(17, 9),
(18, 8),
(19, 7),
(20, 6),
(21, 5),
(22, 4),
(23, 3),
(24, 2),
(25, 1);

-- --------------------------------------------------------
"
        End Function

        Private Function CreateMastersNorm() As String
            Return "
--
-- Tabellenstruktur für Tabelle `masters_norm`
--

CREATE TABLE `masters_norm` (
  `Sex` varchar(1) NOT NULL,
  `GK` int(2) NOT NULL,
  `AK_0` int(3) DEFAULT NULL,
  `AK_1` int(3) DEFAULT NULL,
  `AK_2` int(3) DEFAULT NULL,
  `AK_3` int(3) DEFAULT NULL,
  `AK_4` int(3) DEFAULT NULL,
  `AK_5` int(3) DEFAULT NULL,
  `AK_6` int(3) DEFAULT NULL,
  `AK_7` int(3) DEFAULT NULL,
  `AK_8` int(3) DEFAULT NULL,
  `AK_9` int(3) DEFAULT NULL,
  `AK_10` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `masters_norm`
--

INSERT INTO `masters_norm` (`Sex`, `GK`, `AK_0`, `AK_1`, `AK_2`, `AK_3`, `AK_4`, `AK_5`, `AK_6`, `AK_7`, `AK_8`, `AK_9`, `AK_10`) VALUES
('m', 1, 143, 137, 130, 125, 115, 102, 92, 80, 67, 62, 55),
('m', 2, 158, 152, 145, 137, 127, 112, 102, 90, 75, 68, 55),
('m', 3, 182, 167, 160, 150, 140, 125, 112, 97, 82, 75, 55),
('m', 4, 192, 182, 172, 165, 150, 135, 122, 107, 90, 82, 55),
('m', 5, 202, 192, 182, 175, 160, 142, 130, 112, 95, 87, 55),
('m', 6, 212, 202, 192, 182, 167, 150, 137, 120, 100, 90, 55),
('m', 7, 220, 210, 200, 190, 175, 157, 142, 122, 102, 95, 55),
('m', 8, 227, 217, 207, 197, 182, 165, 150, 127, 107, 100, 55),
('w', 1, 75, 70, 65, 62, 60, 55, 52, 50, 47, NULL, NULL),
('w', 2, 78, 72, 70, 65, 62, 57, 55, 52, 48, NULL, NULL),
('w', 3, 82, 77, 72, 70, 65, 62, 57, 55, 49, NULL, NULL),
('w', 4, 85, 80, 75, 72, 70, 65, 60, 57, 50, NULL, NULL),
('w', 5, 88, 85, 80, 75, 72, 67, 62, 60, 51, NULL, NULL),
('w', 6, 92, 87, 82, 77, 75, 70, 65, 62, 52, NULL, NULL),
('w', 7, 100, 95, 90, 85, 82, 77, 67, 65, 53, NULL, NULL),
('w', 8, 105, 100, 95, 90, 87, 82, 72, 68, 55, NULL, NULL);

-- --------------------------------------------------------
"
        End Function

        Private Function CreateMeldung() As String
            Return "
--
-- Tabellenstruktur für Tabelle `meldung`
--

CREATE TABLE `meldung` (
  `Wettkampf` int(11) NOT NULL,
  `Teilnehmer` int(11) NOT NULL,
  `Sortierung` int(11) DEFAULT NULL,
  `Gewicht` double(5,2) DEFAULT NULL,
  `Meldedatum` datetime DEFAULT NULL,
  `Laenderwertung` tinyint(1) NOT NULL DEFAULT 0,
  `Vereinswertung` int(2) DEFAULT NULL COMMENT 'Vereinsmannschaft',
  `Relativ_W` tinyint(1) DEFAULT 0 COMMENT 'True=w_select, Null=w_autom., False=m',
  `Team` int(11) DEFAULT NULL,
  `a_K` tinyint(1) NOT NULL DEFAULT 0,
  `Wiegen` double(5,2) DEFAULT NULL,
  `Losnummer` int(11) DEFAULT NULL,
  `Startnummer` int(11) DEFAULT NULL,
  `idAK` int(3) DEFAULT NULL,
  `AK` varchar(20) DEFAULT NULL,
  `idGK` int(3) DEFAULT NULL,
  `GK` varchar(5) DEFAULT NULL,
  `Gruppe` int(11) DEFAULT NULL,
  `GG` int(3) DEFAULT NULL,
  `Zweikampf` int(11) DEFAULT NULL,
  `Reissen` int(11) DEFAULT NULL,
  `Stossen` int(11) DEFAULT NULL,
  `attend` tinyint(1) DEFAULT NULL,
  `bezahlt` tinyint(1) NOT NULL DEFAULT 0,
  `OnlineStatus` int(1) NOT NULL DEFAULT 0 COMMENT '3 = Ergebnis (aus teilnahme_bl eingelesen)\r\n4 = abgeschlossen\r\n5 = Ergebnis aus Nachmeldung (aus athleten eingelesen)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateModus() As String
            Return "
--
-- Tabellenstruktur für Tabelle `modus`
--

CREATE TABLE `modus` (
  `idModus` int(3) NOT NULL,
  `Bezeichnung` varchar(100) DEFAULT NULL,
  `Technikwertung` tinyint(1) NOT NULL DEFAULT 0,
  `Athletik` tinyint(1) NOT NULL DEFAULT 0,
  `Steigerung1` int(2) NOT NULL DEFAULT 0,
  `Steigerung2` int(2) NOT NULL DEFAULT 0,
  `Alterseinteilung` varchar(40) NOT NULL,
  `Gewichtseinteilung` char(20) NOT NULL,
  `Wertung` int(1) NOT NULL,
  `Platzierung` int(3) NOT NULL DEFAULT 0,
  `Hantelstange` varchar(10) NOT NULL COMMENT 's=m/w, a=AK(w/u15, m/ü15), k=7kg, x=5kg',
  `MinAlter` int(3) NOT NULL,
  `MaxAlter` int(3) NOT NULL,
  `Regel20` tinyint(1) NOT NULL,
  `IncAfterNoLift` tinyint(1) NOT NULL DEFAULT 1,
  `AutoLosnummer` tinyint(1) NOT NULL,
  `locked` tinyint(1) NOT NULL COMMENT 'Default-Modi'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `modus`
--

INSERT INTO `modus` (`idModus`, `Bezeichnung`, `Technikwertung`, `Athletik`, `Steigerung1`, `Steigerung2`, `Alterseinteilung`, `Gewichtseinteilung`, `Wertung`, `Platzierung`, `Hantelstange`, `MinAlter`, `MaxAlter`, `Regel20`, `IncAfterNoLift`, `AutoLosnummer`, `locked`) VALUES
(100, 'Sächsischer Schülerpokal', 1, 1, 1, 1, 'Altersgruppen', '', 6, 2, 'a_x', 7, 14, 0, 0, 0, 0),
(110, 'DGJ Schülerpokal (MK mit Technik und Athletik)', 1, 1, 1, 1, 'Jahrgang', 'Gewichtsgruppen', 6, 2, 'a_x', 7, 14, 0, 0, 0, 1),
(120, 'DM Schüler (MK mit Technik und Athletik)', 1, 1, 1, 1, 'Jahrgang', 'Gewichtsgruppen', 6, 2, 'a_x', 7, 14, 0, 0, 0, 1),
(130, 'DGJ Jugendpokal (MK und Athletik)', 0, 1, 1, 1, 'Altersgruppen', 'Gewichtsgruppen', 6, 212, 's', 15, 17, 0, 1, 0, 1),
(140, 'IDJM (MK und Athletik)', 0, 1, 1, 1, 'Jahrgang', 'Gewichtsgruppen', 6, 212, 's', 15, 17, 0, 1, 0, 1),
(160, 'DM Jugend (ZK)', 0, 0, 1, 1, 'Altersgruppen', 'Gewichtsklassen', 5, 4, 's', 16, 17, 0, 1, 0, 1),
(200, 'Regionale MKM bis AK 15 (mit Technik und Athletik)', 1, 1, 1, 1, 'Jahrgang', 'Gewichtsgruppen', 6, 2, 'a_x', 10, 15, 0, 0, 0, 1),
(400, 'Masters (Sinclair-Punkte)', 0, 0, 1, 1, 'Altersklassen', 'Gewichtsklassen', 3, 3, 's', 18, 120, 0, 1, 0, 1),
(410, 'Masters (S-M Punkte)', 0, 0, 1, 1, 'Altersklassen', 'Gewichtsklassen', 4, 3, 's', 18, 120, 0, 1, 0, 1),
(500, 'Olympischer Zweikampf (Alters- und Gewichtsklassen)', 0, 0, 1, 1, 'Altersklassen', 'Gewichtsklassen', 5, 3, 's', 13, 120, 0, 1, 0, 1),
(505, 'Olympischer Zweikampf (Gewichtsklassen)', 0, 0, 1, 1, '', 'Gewichtsklassen', 5, 3, 's', 13, 120, 0, 1, 0, 1),
(510, 'Olympischer Zweikampf (Relativ-Abzug)', 0, 0, 1, 1, '', '', 2, 4, 's', 13, 120, 0, 1, 0, 1),
(520, 'Olympischer Zweikampf (Sinclair-Punkte)', 0, 0, 1, 1, '', '', 3, 3, 's', 13, 120, 0, 1, 0, 1),
(530, 'Olympischer Zweikampf (S-M Punkte)', 0, 0, 1, 1, '', '', 4, 3, 's', 13, 120, 0, 1, 0, 1),
(540, 'Olympischer Zweikampf (Robi-Punkte)', 0, 0, 1, 1, '', '', 1, 3, 's', 13, 120, 0, 1, 0, 1),
(600, 'Mannschaft (Olympischer Zweikampf)', 0, 0, 1, 1, '', '', 5, 0, 's', 7, 120, 0, 1, 1, 1),
(610, 'Mannschaft (Relativ-Abzug)', 0, 0, 1, 1, '', '', 2, 0, 's', 15, 120, 0, 1, 1, 1),
(620, 'Mannschaft (Sinclair-Punkte)', 0, 0, 1, 1, '', '', 3, 0, 's', 7, 120, 0, 1, 1, 1),
(630, 'Mannschaft (S-M Punkte)', 0, 0, 1, 1, '', '', 4, 0, 's', 7, 120, 0, 1, 1, 1);

-- --------------------------------------------------------
"
        End Function

        Private Function CreateModusFormeln() As String
            Return "
--
-- Tabellenstruktur für Tabelle `modus_formeln`
--

CREATE TABLE `modus_formeln` (
  `Modus` int(3) NOT NULL,
  `AK` int(2) NOT NULL,
  `Sex` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Faktor` double(3,2) NOT NULL,
  `Reissen` varchar(60) NOT NULL,
  `Stossen` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `modus_formeln`
--

INSERT INTO `modus_formeln` (`Modus`, `AK`, `Sex`, `Faktor`, `Reissen`, `Stossen`) VALUES
(110, 6, 'm', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 6, 'w', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 7, 'm', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 7, 'w', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 8, 'm', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 8, 'w', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 9, 'm', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 9, 'w', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 10, 'm', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 10, 'w', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 11, 'm', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 11, 'w', 0.50, 'HL * 50 / KG + TN * 25', ''),
(110, 12, 'm', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(110, 12, 'w', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(110, 13, 'm', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(110, 13, 'w', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(110, 14, 'm', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(110, 14, 'w', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(120, 13, 'm', 0.66, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(120, 13, 'w', 0.66, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(120, 14, 'm', 0.66, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(120, 14, 'w', 0.66, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(120, 15, 'm', 0.66, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(120, 15, 'w', 0.66, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(130, 15, 'm', 0.66, 'HL * 135 / KG', 'HL * 100 / KG'),
(130, 15, 'w', 0.66, 'HL * 135 / KG * 1,5', 'HL * 100 / KG * 1,5'),
(130, 16, 'm', 0.66, 'HL * 135 / KG', 'HL * 100 / KG'),
(130, 16, 'w', 0.66, 'HL * 135 / KG * 1,5', 'HL * 100 / KG * 1,5'),
(130, 17, 'm', 0.66, 'HL * 135 / KG', 'HL * 100 / KG'),
(130, 17, 'w', 0.66, 'HL * 135 / KG * 1,5', 'HL * 100 / KG * 1,5'),
(140, 15, 'm', 0.66, 'HL * 135 / KG', 'HL * 100 / KG'),
(140, 15, 'w', 0.66, 'HL * 135 / KG', 'HL * 100 / KG'),
(140, 16, 'm', 0.66, 'HL * 135 / KG', 'HL * 100 / KG'),
(140, 16, 'w', 0.66, 'HL * 135 / KG', 'HL * 100 / KG'),
(140, 17, 'm', 0.66, 'HL * 135 / KG', 'HL * 100 / KG'),
(140, 17, 'w', 0.66, 'HL * 135 / KG', 'HL * 100 / KG'),
(160, 16, 'm', 0.00, '', ''),
(160, 16, 'w', 0.00, '', ''),
(160, 17, 'm', 0.00, '', ''),
(160, 17, 'w', 0.00, '', ''),
(200, 10, 'm', 0.40, 'HL * 50 / KG + TN * 25', ''),
(200, 10, 'w', 0.40, 'HL * 50 / KG + TN * 25', ''),
(200, 11, 'm', 0.40, 'HL * 50 / KG + TN * 25', ''),
(200, 11, 'w', 0.40, 'HL * 50 / KG + TN * 25', ''),
(200, 12, 'm', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(200, 12, 'w', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(200, 13, 'm', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(200, 13, 'w', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(200, 14, 'm', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(200, 14, 'w', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(200, 15, 'm', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10'),
(200, 15, 'w', 0.50, 'HL * 50 / KG + TN * 10', 'HL * 50 / KG + TN * 10');

-- --------------------------------------------------------
"
        End Function

        Private Function CreateModusGewichtsgruppen() As String
            Return "
--
-- Tabellenstruktur für Tabelle `modus_gewichtsgruppen`
--

CREATE TABLE `modus_gewichtsgruppen` (
  `Geschlecht` varchar(1) NOT NULL,
  `Gruppe` int(1) NOT NULL,
  `Heber` int(2) NOT NULL COMMENT 'Heber/Gruppe=TN(Gruppe)',
  `Bezeichnung` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `modus_gewichtsgruppen`
--

INSERT INTO `modus_gewichtsgruppen` (`Geschlecht`, `Gruppe`, `Heber`, `Bezeichnung`) VALUES
('m', 1, 15, 'Feder'),
('m', 2, 16, 'Leicht'),
('m', 3, 27, 'Mittel'),
('m', 4, 36, 'Halbschwer'),
('m', 5, 45, 'Schwer'),
('w', 1, 13, 'Feder'),
('w', 2, 14, 'Leicht'),
('w', 3, 21, 'Mittel'),
('w', 4, 28, 'Halbschwer'),
('w', 5, 35, 'Schwer');

-- --------------------------------------------------------
"
        End Function

        Private Function CreateModusMannschaft() As String
            Return "
--
-- Tabellenstruktur für Tabelle `modus_mannschaft`
--

CREATE TABLE `modus_mannschaft` (
  `Modus` int(11) NOT NULL,
  `idMannschaft` int(1) NOT NULL COMMENT '0=Verein; 1=Land; 2=Liga',
  `min` int(2) NOT NULL,
  `max` int(2) NOT NULL,
  `gewertet` int(2) NOT NULL,
  `m_w` tinyint(1) NOT NULL,
  `multi` tinyint(1) NOT NULL COMMENT 'Mehrere Mannschaften pro Verein',
  `relativ` int(2) DEFAULT NULL COMMENT 'Relativwertung W (weitere M)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `modus_mannschaft`
--

INSERT INTO `modus_mannschaft` (`Modus`, `idMannschaft`, `min`, `max`, `gewertet`, `m_w`, `multi`, `relativ`) VALUES
(110, 0, 3, 4, 4, 0, 1, NULL),
(110, 1, 2, 8, 5, 1, 0, NULL),
(130, 1, 2, 8, 5, 1, 0, NULL),
(140, 0, 3, 4, 4, 0, 1, 2);

-- --------------------------------------------------------
"
        End Function

        Private Function CreateNotizen() As String
            Return "
--
-- Tabellenstruktur für Tabelle `notizen`
--

CREATE TABLE `notizen` (
  `Heber` int(11) NOT NULL,
  `Notiz` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateRegion() As String
            Return "
--
-- Tabellenstruktur für Tabelle `region`
--

CREATE TABLE `region` (
  `region_id` int(2) NOT NULL,
  `region_name` varchar(25) NOT NULL,
  `Bezeichnung` varchar(75) DEFAULT NULL,
  `Kurz` varchar(25) DEFAULT NULL,
  `region_2` char(2) NOT NULL,
  `region_3` char(3) NOT NULL,
  `Adresse` varchar(50) NOT NULL DEFAULT '',
  `PLZ` varchar(5) NOT NULL DEFAULT '',
  `Ort` varchar(50) NOT NULL DEFAULT '',
  `region_flag` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `region`
--

INSERT INTO `region` (`region_id`, `region_name`, `Bezeichnung`, `Kurz`, `region_2`, `region_3`, `Adresse`, `PLZ`, `Ort`, `region_flag`) VALUES
(0, '', NULL, NULL, '', '', '', '', '', ''),
(1, 'Schleswig-Holstein', NULL, NULL, 'SH', 'SHS', '', '', '', ''),
(2, 'Hamburg', NULL, NULL, 'HH', 'HAM', '', '', '', ''),
(3, 'Niedersachsen', NULL, NULL, 'NI', 'NDS', '', '', '', ''),
(4, 'Bremen', NULL, NULL, 'HB', 'BRE', '', '', '', ''),
(5, 'Nordrhein-Westfalen', NULL, NULL, 'NW', 'NRW', '', '', '', ''),
(6, 'Hessen', NULL, NULL, 'HE', 'HES', '', '', '', ''),
(7, 'Rheinland-Pfalz', NULL, NULL, 'RP', 'RLP', '', '', '', ''),
(8, 'Baden-Württemberg', NULL, NULL, 'BW', 'BWG', '', '', '', ''),
(9, 'Bayern', NULL, NULL, 'BY', 'BAY', '', '', '', ''),
(10, 'Saarland', NULL, NULL, 'SL', 'SAR', '', '', '', ''),
(11, 'Berlin', NULL, NULL, 'BE', 'BER', '', '', '', ''),
(12, 'Brandenburg', NULL, NULL, 'BB', 'BRA', '', '', '', ''),
(13, 'Mecklenburg-Vorpommern', NULL, NULL, 'MV', 'NVP', '', '', '', ''),
(14, 'Sachsen', 'Verband für Gewichtheben, Kraftdreikampf und Fitness Sachsen e.V.', 'VGKF Sachsen', 'SN', 'SAS', 'Käthe-Kollwitz-Str. 22', '02827', 'Görlitz', ''),
(15, 'Sachsen-Anhalt', NULL, NULL, 'ST', 'SAA', '', '', '', ''),
(16, 'Thüringen', NULL, NULL, 'TH', 'THÜ', '', '', '', '');

-- --------------------------------------------------------
"
        End Function

        Private Function CreateReissen() As String
            Return "
--
-- Tabellenstruktur für Tabelle `reissen`
--

CREATE TABLE `reissen` (
  `Wettkampf` int(11) NOT NULL,
  `Teilnehmer` int(11) NOT NULL,
  `Versuch` int(1) NOT NULL,
  `HLast` int(3) NOT NULL,
  `Diff` int(2) NOT NULL DEFAULT 0,
  `Steigerung1` int(2) NOT NULL DEFAULT 0,
  `Steigerung2` int(2) NOT NULL DEFAULT 0,
  `Steigerung3` int(2) NOT NULL DEFAULT 0,
  `Wertung` int(2) DEFAULT NULL,
  `Zeit` datetime DEFAULT NULL,
  `Note` double(4,2) DEFAULT NULL,
  `Lampen` varchar(3) DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateRelativabzug() As String
            Return "
--
-- Tabellenstruktur für Tabelle `relativabzug`
--

CREATE TABLE `relativabzug` (
  `Gewicht` double(4,1) NOT NULL,
  `m` double(4,1) NOT NULL,
  `w` double(4,1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `relativabzug`
--

INSERT INTO `relativabzug` (`Gewicht`, `m`, `w`) VALUES
(31, 22.5, 12.5),
(32, 23.0, 12.5),
(33, 23.5, 12.5),
(34, 24.0, 12.5),
(35, 24.5, 12.5),
(36, 25.0, 12.5),
(37, 25.5, 12.5),
(38, 26.0, 12.5),
(39, 26.5, 12.5),
(40, 27.0, 12.5),
(41, 27.5, 13.0),
(42, 28.0, 13.0),
(43, 28.5, 13.5),
(44, 29.0, 13.5),
(45, 29.5, 14.0),
(46, 30.0, 14.0),
(47, 30.5, 14.5),
(48, 31.0, 15.0),
(49, 32.0, 15.5),
(50, 33.0, 16.0),
(51, 34.5, 16.5),
(52, 36.0, 17.0),
(53, 37.0, 17.5),
(54, 38.5, 18.5),
(55, 40.0, 19.5),
(56, 42.0, 20.5),
(57, 44.0, 21.5),
(58, 46.0, 22.5),
(59, 48.0, 23.5),
(60, 50.0, 25.0),
(61, 52.0, 26.5),
(62, 54.0, 27.5),
(63, 56.0, 28.5),
(64, 57.5, 29.5),
(65, 59.0, 31.0),
(66, 60.5, 32.0),
(67, 62.0, 33.0),
(68, 63.5, 34.0),
(69, 65.0, 35.0),
(70, 66.5, 36.0),
(71, 68.0, 37.0),
(72, 69.5, 38.0),
(73, 70.5, 39.0),
(74, 71.5, 39.5),
(75, 72.5, 40.0),
(76, 74.0, 40.5),
(77, 75.5, 41.0),
(78, 77.0, 41.5),
(79, 78.0, 42.0),
(79.1, 0.0, 42.0),
(80, 0.0, 42.5),
(81, 0.0, 43.0),
(82, 0.0, 43.5),
(83, 0.0, 44.0),
(84, 0.0, 44.5),
(85, 0.0, 44.5),
(86, 0.0, 45.0),
(87, 0.0, 45.5),
(88, 0.0, 46.0),
(89, 0.0, 46.0),
(90, 0.0, 46.5),
(91, 0.0, 47.0),
(92, 0.0, 47.5),
(93, 0.0, 47.5),
(94, 0.0, 48.0),
(95, 0.0, 48.5),
(95.5, 0.0, 48.5),
(95.9, 95.0, 48.5),
(96, 95.5, 48.5),
(97, 96.0, 49.0),
(98, 96.5, 49.5),
(99, 97.0, 49.5),
(100, 97.5, 50.0),
(101, 98.5, 50.5),
(102, 99.5, 50.5),
(103, 100.5, 51.0),
(104, 101.0, 51.0),
(105, 102.0, 51.5),
(106, 103.0, 51.5),
(107, 103.5, 52.0),
(108, 103.5, 52.0),
(109, 104.0, 52.5),
(110, 104.0, 52.5),
(111, 104.0, 53.0),
(112, 104.5, 53.0),
(113, 104.5, 53.5),
(114, 105.0, 53.5),
(115, 105.0, 54.0),
(116, 105.5, 54.0),
(117, 106.0, 54.5),
(118, 106.5, 54.5),
(119, 107.0, 55.0),
(120, 107.5, 55.0),
(121, 108.0, 55.5),
(122, 108.5, 55.5),
(123, 109.0, 56.0),
(124, 109.5, 56.0),
(125, 110.0, 56.5),
(126, 110.5, 56.5),
(127, 111.0, 57.0),
(128, 111.5, 57.0),
(129, 112.0, 57.5),
(130, 112.5, 57.5),
(131, 113.0, 58.0),
(132, 113.5, 58.0),
(133, 114.0, 58.5),
(134, 114.5, 58.5),
(135, 115.0, 59.0),
(136, 115.5, 59.0),
(137, 116.0, 59.5),
(138, 116.5, 59.5),
(139, 117.0, 60.0),
(140, 117.5, 60.0),
(141, 118.0, 60.5),
(142, 118.5, 60.5),
(143, 119.0, 61.0),
(144, 119.5, 61.0),
(145, 120.0, 61.5),
(146, 120.5, 61.5),
(147, 121.0, 62.0),
(148, 121.5, 62.0),
(149, 122.0, 62.5),
(150, 122.5, 62.5),
(151, 123.0, 63.0),
(152, 123.5, 63.0),
(153, 124.0, 63.5),
(154, 124.5, 63.5),
(155, 125.0, 64.0),
(156, 125.5, 64.0),
(157, 126.0, 64.5),
(158, 126.5, 64.5),
(159, 127.0, 65.0),
(160, 127.5, 65.0);

-- --------------------------------------------------------
"
        End Function

        Private Function CreateResults() As String
            Return "
--
-- Tabellenstruktur für Tabelle `results`
--

CREATE TABLE `results` (
  `Wettkampf` int(11) NOT NULL,
  `Teilnehmer` int(11) NOT NULL,
  `Reissen` double(8,4) DEFAULT NULL,
  `Reissen_Zeit` datetime DEFAULT NULL,
  `Reissen_Platz` int(4) DEFAULT NULL,
  `Reissen2` double(8,4) DEFAULT NULL,
  `Reissen2_Platz` int(3) DEFAULT NULL,
  `Stossen` double(8,4) DEFAULT NULL,
  `Stossen_Zeit` datetime DEFAULT NULL,
  `Stossen_Platz` int(4) DEFAULT NULL,
  `Stossen2` double(8,4) DEFAULT NULL,
  `Stossen2_Platz` int(3) DEFAULT NULL,
  `ZK` int(3) DEFAULT NULL,
  `Heben` double(8,4) DEFAULT NULL,
  `Heben_Platz` int(4) DEFAULT NULL,
  `Wertung2` double(8,4) DEFAULT NULL,
  `Wertung2_Platz` int(4) DEFAULT NULL,
  `Athletik` double(5,2) DEFAULT NULL,
  `Gesamt` double(8,4) DEFAULT NULL,
  `Gesamt_Platz` int(4) DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateStaaten() As String
            Return "
--
-- Tabellenstruktur für Tabelle `staaten`
--

CREATE TABLE `staaten` (
  `state_id` int(3) NOT NULL,
  `state_name` varchar(50) NOT NULL,
  `state_long` varchar(50) NOT NULL,
  `state_iso3` char(3) NOT NULL,
  `state_iso2` char(2) NOT NULL,
  `state_flag` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `staaten`
--

INSERT INTO `staaten` (`state_id`, `state_name`, `state_long`, `state_iso3`, `state_iso2`, `state_flag`) VALUES
(0, '', '', '', '', ''),
(1, 'Abchasien', 'Republik Abchasien', 'ABC', '', ''),
(2, 'Afghanistan', 'Islamische Republik Afghanistan', 'AFG', 'AF', 'af.png'),
(3, 'Ägypten', 'Arabische Republik Ägypten', 'EGY', 'EG', 'eg.png'),
(4, 'Albanien', 'Republik Albanien', 'ALB', 'AL', 'al.png'),
(5, 'Algerien', 'Demokratische Volksrepublik Algerien', 'DZA', 'DZ', 'dz.png'),
(6, 'Andorra', 'Fürstentum Andorra', 'AND', 'AD', 'ad.png'),
(7, 'Angola', 'Republik Angola', 'AGO', 'AO', 'ao.png'),
(8, 'Antigua und Barbuda', 'Antigua und Barbuda', 'ATG', 'AG', 'ag.png'),
(9, 'Äquatorialguinea', 'Republik Äquatorialguinea', 'GNQ', 'GQ', 'gq.png'),
(10, 'Argentinien', 'Argentinische Republik', 'ARG', 'AR', 'ar.png'),
(11, 'Armenien', 'Republik Armenien', 'ARM', 'AM', 'am.png'),
(12, 'Aserbaidschan', 'Republik Aserbaidschan', 'AZE', 'AZ', 'az.png'),
(13, 'Äthiopien', 'Demokratische Bundesrepublik Äthiopien', 'ETH', 'ET', 'et.png'),
(14, 'Australien', 'Australien', 'AUS', 'AU', 'au.png'),
(15, 'Bahamas', 'Commonwealth der Bahamas', 'BHS', 'BS', 'bs.png'),
(16, 'Bahrain', 'Königreich Bahrain', 'BHR', 'BH', 'bh.png'),
(17, 'Bangladesch', 'Volksrepublik Bangladesch', 'BGD', 'BD', 'bd.png'),
(18, 'Barbados', 'Barbados', 'BRB', 'BB', 'bb.png'),
(19, 'Belgien', 'Königreich Belgien', 'BEL', 'BE', 'be.png'),
(20, 'Belize', 'Belize', 'BLZ', 'BZ', 'bz.png'),
(21, 'Benin', 'Republik Benin', 'BEN', 'BJ', 'bj.png'),
(22, 'Bergkarabach', 'Republik Bergkarabach', '', '', ''),
(23, 'Bhutan', 'Königreich Bhutan', 'BTN', 'BT', 'bt.png'),
(24, 'Bolivien', 'Plurinationaler Staat Bolivien', 'BOL', 'BO', 'bo.png'),
(25, 'Bosnien-Herzegowina', 'Bosnien und Herzegowina', 'BIH', 'BA', 'ba.png'),
(26, 'Botswana', 'Republik Botsuana', 'BWA', 'BW', 'bw.png'),
(27, 'Brasilien', 'Föderative Republik Brasilien', 'BRA', 'BR', 'br.png'),
(28, 'Brunei', 'Brunei Darussalam', 'BRN', 'BN', 'bn.png'),
(29, 'Bulgarien', 'Republik Bulgarien', 'BGR', 'BG', 'bg.png'),
(30, 'Burkina Faso', 'Burkina Faso', 'BFA', 'BF', 'bf.png'),
(31, 'Burundi', 'Republik Burundi', 'BDI', 'BI', 'bi.png'),
(32, 'Chile', 'Republik Chile', 'CHL', 'CL', 'cl.png'),
(33, 'Taiwan', 'Republik China', 'TWN', 'TW', 'tw.png'),
(34, 'China', 'Volksrepublik China', 'CHN', 'CN', 'cn.png'),
(35, 'Hongkong', '', 'CHN', 'HK', ''),
(36, 'Macau', '', 'CHN', 'MO', ''),
(37, 'Cookinseln', 'Cookinseln', 'COK', 'CK', ''),
(38, 'Costa Rica', 'Republik Costa Rica', 'CRI', 'CR', 'cr.png'),
(39, 'Dänemark', 'Königreich Dänemark', 'DNK', 'DK', 'dk.png'),
(40, 'Färöer Inseln', 'Königreich Dänemark', 'DNK', 'FO', ''),
(41, 'Grönland', 'Königreich Dänemark', 'DNK', 'GL', ''),
(42, 'Deutschland', 'Bundesrepublik Deutschland', 'DEU', 'DE', 'de.png'),
(43, 'Dominica', 'Commonwealth Dominica', 'DMA', 'DM', 'dm.png'),
(44, 'Dominikanische Republik', 'Dominikanische Republik', 'DOM', 'DO', 'do.png'),
(45, 'Dschibuti', 'Republik Dschibuti', 'DJI', 'DJ', 'dj.png'),
(46, 'Ecuador', 'Republik Ecuador', 'ECU', 'EC', 'ec.png'),
(47, 'El Salvador', 'Republik El Salvador', 'SLV', 'SV', 'sv.png'),
(48, 'Elfenbeinküste', 'Republik Côte d’Ivoire', 'CIV', 'CI', 'ci.png'),
(49, 'Eritrea', 'Staat Eritrea', 'ERI', 'ER', 'er.png'),
(50, 'Estland', 'Republik Estland', 'EST', 'EE', 'ee.png'),
(51, 'Fidschi', 'Republik Fidschi', 'FJI', 'FJ', 'fj.png'),
(52, 'Finnland', 'Republik Finnland', 'FIN', 'FI', 'fi.png'),
(53, 'Frankreich', 'Französische Republik', 'FRA', 'FR', 'fr.png'),
(54, 'Gabun', 'Gabunische Republik', 'GAB', 'GA', 'ga.png'),
(55, 'Gambia', 'Republik Gambia', 'GMB', 'GM', 'gm.png'),
(56, 'Georgien', 'Georgien', 'GEO', 'GE', 'ge.png'),
(57, 'Ghana', 'Republik Ghana', 'GHA', 'GH', 'gh.png'),
(58, 'Grenada', 'Grenada', 'GRD', 'GD', 'gd.png'),
(59, 'Griechenland', 'Hellenische Republik', 'GRC', 'GR', 'gr.png'),
(60, 'Guatemala', 'Republik Guatemala', 'GTM', 'GT', 'gt.png'),
(61, 'Guinea', 'Republik Guinea', 'GIN', 'GN', 'gn.png'),
(62, 'Guinea-Bissau', 'Republik Guinea-Bissau', 'GNB', 'GW', 'gw.png'),
(63, 'Guyana', 'Kooperative Republik Guyana', 'GUY', 'GY', 'gy.png'),
(64, 'Haiti', 'Republik Haiti', 'HTI', 'HT', 'ht.png'),
(65, 'Honduras', 'Republik Honduras', 'HND', 'HN', 'hn.png'),
(66, 'Indien', 'Republik Indien', 'IND', 'IN', 'in.png'),
(67, 'Indonesien', 'Republik Indonesien', 'IDN', 'ID', 'id.png'),
(68, 'Irak', 'Republik Irak', 'IRQ', 'IQ', 'iq.png'),
(69, 'Iran', 'Islamische Republik Iran', 'IRN', 'IR', 'ir.png'),
(70, 'Irland', 'Irland', 'IRL', 'IE', 'ie.png'),
(71, 'Island', 'Republik Island', 'ISL', 'IS', 'is.png'),
(72, 'Israel', 'Staat Israel', 'ISR', 'IL', 'il.png'),
(73, 'Italien', 'Italienische Republik', 'ITA', 'IT', 'it.png'),
(74, 'Jamaika', 'Jamaika', 'JAM', 'JM', 'jm.png'),
(75, 'Japan', 'Japan', 'JPN', 'JP', 'jp.png'),
(76, 'Jemen', 'Republik Jemen', 'YEM', 'YE', 'ye.png'),
(77, 'Jordanien', 'Haschemitisches Königreich Jordanien', 'JOR', 'JO', 'jo.png'),
(78, 'Kambodscha', 'Königreich Kambodscha', 'KHM', 'KH', 'kh.png'),
(79, 'Kamerun', 'Republik Kamerun', 'CMR', 'CM', 'cm.png'),
(80, 'Kanada', 'Kanada', 'CAN', 'CA', 'ca.png'),
(81, 'Kap Verde', 'Republik Cabo Verde', 'CPV', 'CV', 'cv.png'),
(82, 'Kasachstan', 'Republik Kasachstan', 'KAZ', 'KZ', 'kz.png'),
(83, 'Katar', 'Staat Katar', 'QAT', 'QA', 'qa.png'),
(84, 'Kenia', 'Republik Kenia', 'KEN', 'KE', 'ke.png'),
(85, 'Kirgisistan', 'Kirgisische Republik', 'KGZ', 'KG', 'kg.png'),
(86, 'Kiribati', 'Republik Kiribati', 'KIR', 'KI', 'ki.png'),
(87, 'Kolumbien', 'Republik Kolumbien', 'COL', 'CO', 'co.png'),
(88, 'Komoren', 'Union der Komoren', 'COM', 'KM', 'km.png'),
(89, 'Kongo', 'Demokratische Republik Kongo', 'COD', 'CD', 'cd.png'),
(90, 'Republik Kongo', 'Republik Kongo', 'COG', 'CG', 'cg.png'),
(91, 'Nord-Korea', 'Demokratische Volksrepublik Korea', 'PRK', 'KP', 'kp.png'),
(92, 'Süd-Korea', 'Republik Korea', 'KOR', 'KR', 'kr.png'),
(93, 'Kosovo', 'Republik Kosovo', 'RKS', '', ''),
(94, 'Kroatien', 'Republik Kroatien', 'HRV', 'HR', 'hr.png'),
(95, 'Kuba', 'Republik Kuba', 'CUB', 'CU', 'cu.png'),
(96, 'Kuwait', 'Staat Kuwait', 'KWT', 'KW', 'kw.png'),
(97, 'Laos', 'Demokratische Volksrepublik Laos', 'LAO', 'LA', 'la.png'),
(98, 'Lesotho', 'Königreich Lesotho', 'LSO', 'LS', 'ls.png'),
(99, 'Lettland', 'Republik Lettland', 'LVA', 'LV', 'lv.png'),
(100, 'Libanon', 'Libanesische Republik', 'LBN', 'LB', 'lb.png'),
(101, 'Liberia', 'Republik Liberia', 'LBR', 'LR', 'lr.png'),
(102, 'Libyen', 'Libyen', 'LBY', 'LY', 'ly.png'),
(103, 'Liechtenstein', 'Fürstentum Liechtenstein', 'LIE', 'LI', 'li.png'),
(104, 'Litauen', 'Republik Litauen', 'LTU', 'LT', 'lt.png'),
(105, 'Luxemburg', 'Großherzogtum Luxemburg', 'LUX', 'LU', 'lu.png'),
(106, 'Madagaskar', 'Republik Madagaskar', 'MDG', 'MG', 'mg.png'),
(107, 'Malawi', 'Republik Malawi', 'MWI', 'MW', 'mw.png'),
(108, 'Malaysia', 'Malaysia', 'MYS', 'MY', 'my.png'),
(109, 'Malediven', 'Republik Malediven', 'MDV', 'MV', 'mv.png'),
(110, 'Mali', 'Republik Mali', 'MLI', 'ML', 'ml.png'),
(111, 'Malta', 'Republik Malta', 'MLT', 'MT', 'mt.png'),
(112, 'Marokko', 'Königreich Marokko', 'MAR', 'MA', 'ma.png'),
(113, 'Marshallinseln', 'Republik Marshallinseln', 'MHL', 'MH', 'mh.png'),
(114, 'Mauretanien', 'Islamische Republik Mauretanien', 'MRT', 'MR', 'mr.png'),
(115, 'Mauritius', 'Republik Mauritius', 'MUS', 'MU', 'mu.png'),
(116, 'Mazedonien', 'Ehemalige jugoslawische Republik Mazedonien', 'MKD', 'MK', 'mk.png'),
(117, 'Mexiko', 'Vereinigte Mexikanische Staaten', 'MEX', 'MX', 'mx.png'),
(118, 'Mikronesien', 'Föderierte Staaten von Mikronesien', 'FSM', 'FM', 'fm.png'),
(119, 'Moldawien', 'Republik Moldau', 'MDA', 'MD', 'md.png'),
(120, 'Monaco', 'Fürstentum Monaco', 'MCO', 'MC', 'mc.png'),
(121, 'Mongolei', 'Mongolei', 'MNG', 'MN', 'mn.png'),
(122, 'Montenegro', 'Montenegro', 'MNE', 'ME', 'me.png'),
(123, 'Mosambik', 'Republik Mosambik', 'MOZ', 'MZ', 'mz.png'),
(124, 'Myanmar', 'Republik der Union Myanmar', 'MMR', 'MM', 'mm.png'),
(125, 'Namibia', 'Republik Namibia', 'NAM', 'NA', 'na.png'),
(126, 'Nauru', 'Republik Nauru', 'NRU', 'NR', 'nr.png'),
(127, 'Nepal', 'Demokratische Bundesrepublik Nepal', 'NPL', 'NP', 'np.png'),
(128, 'Neuseeland', 'Neuseeland', 'NZL', 'NZ', 'nz.png'),
(129, 'Nicaragua', 'Republik Nicaragua', 'NIC', 'NI', 'ni.png'),
(130, 'Niederlande', 'Königreich der Niederlande', 'NLD', 'NL', 'nl.png'),
(131, 'Niger', 'Republik Niger', 'NER', 'NE', 'ne.png'),
(132, 'Nigeria', 'Bundesrepublik Nigeria', 'NGA', 'NG', 'ng.png'),
(133, 'Niue', 'Niue', 'NIU', 'NU', ''),
(134, 'Nordzypern', 'Türkische Republik Nordzypern', '', '', ''),
(135, 'Norwegen', 'Königreich Norwegen', 'NOR', 'NO', 'no.png'),
(136, 'Oman', 'Sultanat Oman', 'OMN', 'OM', 'om.png'),
(137, 'Österreich', 'Republik Österreich', 'AUT', 'AT', 'at.png'),
(138, 'Osttimor', 'Demokratische Republik Timor-Leste', 'TLS', 'TL', 'tl.png'),
(139, 'Pakistan', 'Islamische Republik Pakistan', 'PAK', 'PK', 'pk.png'),
(140, 'Palästina', '', 'PSE', 'PS', ''),
(141, 'Palau', 'Republik Palau', 'PLW', 'PW', 'pw.png'),
(142, 'Panama', 'Republik Panama', 'PAN', 'PA', 'pa.png'),
(143, 'Papua-Neuguinea', 'Unabhängiger Staat Papua-Neuguinea', 'PNG', 'PG', 'pg.png'),
(144, 'Paraguay', 'Republik Paraguay', 'PRY', 'PY', 'py.png'),
(145, 'Peru', 'Republik Peru', 'PER', 'PE', 'pe.png'),
(146, 'Philippinen', 'Republik der Philippinen', 'PHL', 'PH', 'ph.png'),
(147, 'Polen', 'Republik Polen', 'POL', 'PL', 'pl.png'),
(148, 'Portugal', 'Portugiesische Republik', 'PRT', 'PT', 'pt.png'),
(149, 'Ruanda', 'Republik Ruanda', 'RWA', 'RW', 'rw.png'),
(150, 'Rumänien', 'Republik Rumänien', 'ROU', 'RO', 'ro.png'),
(151, 'Russland', 'Russische Föderation', 'RUS', 'RU', 'ru.png'),
(152, 'Salomonen', 'Salomonen', 'SLB', 'SB', 'sb.png'),
(153, 'Sambia', 'Republik Sambia', 'ZMB', 'ZM', 'zm.png'),
(154, 'Samoa', 'Unabhängiger Staat Samoa', 'WSM', 'WS', 'ws.png'),
(155, 'San Marino', 'Republik San Marino', 'SMR', 'SM', 'sm.png'),
(156, 'São Tomé und Príncipe', 'Demokratische Republik São Tomé und Príncipe', 'STP', 'ST', 'st.png'),
(157, 'Saudi-Arabien', 'Königreich Saudi-Arabien', 'SAU', 'SA', 'sa.png'),
(158, 'Schweden', 'Königreich Schweden', 'SWE', 'SE', 'se.png'),
(159, 'Schweiz', 'Schweizerische Eidgenossenschaft', 'CHE', 'CH', 'ch.png'),
(160, 'Senegal', 'Republik Senegal', 'SEN', 'SN', 'sn.png'),
(161, 'Serbien', 'Republik Serbien', 'SRB', 'RS', 'rs.png'),
(162, 'Seychellen', 'Republik Seychellen', 'SYC', 'SC', 'sc.png'),
(163, 'Sierra Leone', 'Republik Sierra Leone', 'SLE', 'SL', 'sl.png'),
(164, 'Simbabwe', 'Republik Simbabwe', 'ZWE', 'ZW', 'zw.png'),
(165, 'Singapur', 'Republik Singapur', 'SGP', 'SG', 'sg.png'),
(166, 'Slowakei', 'Slowakische Republik', 'SVK', 'SK', 'sk.png'),
(167, 'Slowenien', 'Republik Slowenien', 'SVN', 'SI', 'si.png'),
(168, 'Somalia', 'Bundesrepublik Somalia', 'SOM', 'SO', 'so.png'),
(169, 'Somaliland', 'Republik Somaliland', '', '', ''),
(170, 'Spanien', 'Königreich Spanien', 'ESP', 'ES', 'es.png'),
(171, 'Sri Lanka', 'Demokratische Sozialistische Republik Sri Lanka', 'LKA', 'LK', 'lk.png'),
(172, 'St. Kitts und Nevis', 'Föderation St. Kitts und Nevis', 'KNA', 'KN', 'kn.png'),
(173, 'St. Lucia', 'St. Lucia', 'LCA', 'LC', 'lc.png'),
(174, 'St. Vincent und die Grenadinen', 'St. Vincent und die Grenadinen', 'VCT', 'VC', 'vc.png'),
(175, 'Südafrika', 'Republik Südafrika', 'ZAF', 'ZA', 'za.png'),
(176, 'Sudan', 'Republik Sudan', 'SDN', 'SD', 'sd.png'),
(177, 'Südossetien', 'Südossetien', 'SOS', '', ''),
(178, 'Südsudan', 'Republik Südsudan', 'SSD', 'SS', ''),
(179, 'Suriname', 'Republik Suriname', 'SUR', 'SR', 'sr.png'),
(180, 'Swasiland', 'Königreich Swasiland', 'SWZ', 'SZ', 'sz.png'),
(181, 'Syrien', 'Arabische Republik Syrien', 'SYR', 'SY', 'sy.png'),
(182, 'Tadschikistan', 'Republik Tadschikistan', 'TJK', 'TJ', 'tj.png'),
(183, 'Tansania', 'Vereinigte Republik Tansania', 'TZA', 'TZ', 'tz.png'),
(184, 'Thailand', 'Königreich Thailand', 'THA', 'TH', 'th.png'),
(185, 'Togo', 'Republik Togo', 'TGO', 'TG', 'tg.png'),
(186, 'Tonga', 'Königreich Tonga', 'TON', 'TO', 'to.png'),
(187, 'Transnistrien', 'Transnistrische Moldauische Republik', '', '', ''),
(188, 'Trinidad und Tobago', 'Republik Trinidad und Tobago', 'TTO', 'TT', 'tt.png'),
(189, 'Tschad', 'Republik Tschad', 'TCD', 'TD', 'td.png'),
(190, 'Tschechien', 'Tschechische Republik', 'CZE', 'CZ', 'cz.png'),
(191, 'Tunesien', 'Tunesische Republik', 'TUN', 'TN', 'tn.png'),
(192, 'Türkei', 'Republik Türkei', 'TUR', 'TR', 'tr.png'),
(193, 'Turkmenistan', 'Turkmenistan', 'TKM', 'TM', 'tm.png'),
(194, 'Tuvalu', 'Tuvalu', 'TUV', 'TV', 'tv.png'),
(195, 'Uganda', 'Republik Uganda', 'UGA', 'UG', 'ug.png'),
(196, 'Ukraine', 'Ukraine', 'UKR', 'UA', 'ua.png'),
(197, 'Ungarn', 'Ungarn', 'HUN', 'HU', 'hu.png'),
(198, 'Uruguay', 'Republik Östlich des Uruguay', 'URY', 'UY', 'uy.png'),
(199, 'Usbekistan', 'Republik Usbekistan', 'UZB', 'UZ', 'uz.png'),
(200, 'Vanuatu', 'Republik Vanuatu', 'VUT', 'VU', 'vu.png'),
(201, 'Vatikanstadt', 'Staat Vatikanstadt', 'VAT', 'VA', 'va.png'),
(202, 'Venezuela', 'Bolivarische Republik Venezuela', 'VEN', 'VE', 've.png'),
(203, 'Vereinigte Arabische Emirate', 'Vereinigte Arabische Emirate', 'ARE', 'AE', 'ae.png'),
(204, 'Vereinigte Staaten', 'Vereinigte Staaten von Amerika', 'USA', 'US', 'us.png'),
(205, 'Vereinigtes Königreich', 'Vereinigtes Königreich Großbritannien und Nordirla', 'GBR', 'GB', 'gb.png'),
(206, 'Vietnam', 'Sozialistische Republik Vietnam', 'VNM', 'VN', 'vn.png'),
(207, 'Weißrussland', 'Republik Belarus', 'BLR', 'BY', 'by.png'),
(208, 'Westsahara', 'Demokratische Arabische Republik Sahara', 'ESH', 'EH', 'eh.png'),
(209, 'Zentralafrikanische Republik', 'Zentralafrikanische Republik', 'CAF', 'CF', 'cf.png'),
(210, 'Zypern', 'Republik Zypern', 'CYP', 'CY', 'cy.png');

-- --------------------------------------------------------
"
        End Function

        Private Function CreateStossen() As String
            Return "
--
-- Tabellenstruktur für Tabelle `stossen`
--

CREATE TABLE `stossen` (
  `Wettkampf` int(11) NOT NULL,
  `Teilnehmer` int(11) NOT NULL,
  `Versuch` int(1) NOT NULL,
  `HLast` int(3) NOT NULL,
  `Diff` int(2) NOT NULL DEFAULT 0,
  `Steigerung1` int(2) NOT NULL DEFAULT 0,
  `Steigerung2` int(2) NOT NULL DEFAULT 0,
  `Steigerung3` int(2) NOT NULL DEFAULT 0,
  `Wertung` int(2) DEFAULT NULL,
  `Zeit` datetime DEFAULT NULL,
  `Note` double(4,2) DEFAULT NULL,
  `Lampen` varchar(3) DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateTeams() As String
            Return "
--
-- Tabellenstruktur für Tabelle `teams`
--

CREATE TABLE `teams` (
  `idTeam` int(11) NOT NULL,
  `idVerein` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Index beginnt bei 10.000';

-- --------------------------------------------------------
"
        End Function

        Private Function CreateUsers() As String
            Return "
--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `UserId` int(1) NOT NULL,
  `Bohle` int(1) NOT NULL,
  `Server` varchar(15) DEFAULT NULL,
  `Port` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateVerein() As String
            Return "
--
-- Tabellenstruktur für Tabelle `verein`
--

CREATE TABLE `verein` (
  `idVerein` int(11) NOT NULL,
  `GUID` varchar(50) DEFAULT NULL,
  `Vereinsname` varchar(100) NOT NULL,
  `Kurzname` varchar(50) DEFAULT NULL,
  `Region` int(2) DEFAULT 0,
  `Staat` int(3) NOT NULL DEFAULT 0,
  `Adresse` varchar(50) DEFAULT '',
  `PLZ` varchar(5) DEFAULT '',
  `Ort` varchar(50) DEFAULT '',
  `Team` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Verein | Kampfgemeinschaft'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `verein`
--

INSERT INTO `verein` (`idVerein`, `GUID`, `Vereinsname`, `Kurzname`, `Region`, `Staat`, `Adresse`, `PLZ`, `Ort`, `Team`) VALUES
(0, NULL, '', NULL, 0, 0, '', '', '', 0);

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWertung() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wertung`
--

CREATE TABLE `wertung` (
  `idWertung` int(1) NOT NULL,
  `Bezeichnung` varchar(20) NOT NULL,
  `Kurz` varchar(10) DEFAULT NULL,
  `Genauigkeit` int(1) NOT NULL DEFAULT 0 COMMENT 'Nachkomma-Stellen',
  `Sortierung` varchar(50) NOT NULL COMMENT 'Publikumsanzeige'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `wertung`
--

INSERT INTO `wertung` (`idWertung`, `Bezeichnung`, `Kurz`, `Genauigkeit`, `Sortierung`) VALUES
(1, 'Robi-Punkte', 'Robi', 2, ''),
(2, 'Relativpunkte', 'Relativ', 2, ''),
(3, 'Sinclairpunkte', 'Sinclair', 4, ''),
(4, 'S-M-Punkte', 'S-M', 2, ''),
(5, 'Zweikampf', 'ZK', 0, ''),
(6, 'Mehrkampf', NULL, 2, '');

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWertungBez() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wertung_bezeichnung`
--

CREATE TABLE `wertung_bezeichnung` (
  `Platzierung` int(1) NOT NULL,
  `International` tinyint(1) NOT NULL,
  `Bezeichnung` varchar(25) NOT NULL,
  `Headline` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `wertung_bezeichnung`
--

INSERT INTO `wertung_bezeichnung` (`Platzierung`, `International`, `Bezeichnung`, `Headline`) VALUES
(1, 0, '', 'BESTENLISTE'),
(1, 1, '', 'BEST TOTAL'),
(2, 0, 'GRUPPE', 'BESTE IN '),
(2, 1, 'GROUP', 'BEST OF '),
(3, 0, 'ALTERSKLASSE', 'BESTE IN '),
(3, 1, 'AGE GROUP', 'BEST OF '),
(4, 0, 'GEWICHTSKLASSE', 'BESTE IN '),
(4, 1, 'CATERGORY', 'BEST OF '),
(5, 0, '', 'BESTE WEIBLICH'),
(5, 1, '', 'BEST FEMALE'),
(9, 0, 'ALTERSKLASSE', 'BESTE IN '),
(9, 1, 'AGE GROUP', 'BEST OF ');

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampf() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf`
--

CREATE TABLE `wettkampf` (
  `Wettkampf` int(11) NOT NULL,
  `Modus` int(11) NOT NULL,
  `Datum` date NOT NULL,
  `DatumBis` date DEFAULT NULL,
  `Veranstalter` int(11) NOT NULL,
  `Kurz` varchar(25) NOT NULL DEFAULT '',
  `Ort` varchar(100) NOT NULL,
  `Bigdisc` tinyint(1) NOT NULL,
  `Kampfrichter` varchar(4) DEFAULT NULL,
  `Mannschaft` tinyint(1) NOT NULL,
  `Flagge` tinyint(1) NOT NULL,
  `Mindestalter` int(3) NOT NULL,
  `Platzierung` varchar(11) NOT NULL DEFAULT '' COMMENT '1.Stelle = Platzierung, \r\n2.Stelle = Platzierung2, \r\n3.Stelle = Wertung2 bzw. 9 für Wertung2 nach AKs',
  `GUID` varchar(32) NOT NULL DEFAULT '' COMMENT 'cas_gguid aus api_bvdg',
  `Finale` tinyint(1) DEFAULT 0 COMMENT '3 Gruppen,\r\n2 TN pro Gruppe',
  `SameAK` varchar(225) DEFAULT NULL,
  `UseAgeGroups` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfAltersgruppen() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_altersgruppen`
--

CREATE TABLE `wettkampf_altersgruppen` (
  `Wettkampf` int(11) NOT NULL,
  `Reihenfolge` int(2) NOT NULL,
  `Sex` varchar(1) DEFAULT NULL COMMENT 'm, w oder leer für beide',
  `Jahrgang` varchar(50) DEFAULT NULL COMMENT 'zusammengefasste JGs Komma getrennt '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfAltersklassen() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_altersklassen`
--

CREATE TABLE `wettkampf_altersklassen` (
  `Wettkampf` int(11) NOT NULL,
  `Geschlecht` varchar(1) NOT NULL DEFAULT '' COMMENT 'leer für 2.Wertung',
  `idAK` int(11) NOT NULL COMMENT '= altersklassen.idAltersklasse',
  `Wertung` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Masters (erweiterte AKs mit Zuordnung der Wertung) ';

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfAthletik() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_athletik`
--

CREATE TABLE `wettkampf_athletik` (
  `Wettkampf` int(11) NOT NULL,
  `Disziplin` int(3) NOT NULL,
  `AKs` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfAthletikDefaults() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_athletik_defaults`
--

CREATE TABLE `wettkampf_athletik_defaults` (
  `Wettkampf` int(11) NOT NULL,
  `Disziplin` int(2) NOT NULL,
  `Bezeichnung` varchar(15) NOT NULL,
  `Geschlecht` varchar(1) NOT NULL,
  `ak5` double(4,2) DEFAULT NULL,
  `ak6` double(4,2) DEFAULT NULL,
  `ak7` double(4,2) DEFAULT NULL,
  `ak8` double(4,2) DEFAULT NULL,
  `ak9` double(4,2) DEFAULT NULL,
  `ak10` double(4,2) DEFAULT NULL,
  `ak11` double(4,2) DEFAULT NULL,
  `ak12` double(4,2) DEFAULT NULL,
  `ak13` double(4,2) DEFAULT NULL,
  `ak14` double(4,2) DEFAULT NULL,
  `ak15` double(4,2) DEFAULT NULL,
  `ak16` double(4,2) DEFAULT NULL,
  `ak17` double(4,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfBez() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_bezeichnung`
--

CREATE TABLE `wettkampf_bezeichnung` (
  `Wettkampf` int(11) NOT NULL,
  `Bez1` varchar(100) NOT NULL,
  `Bez2` varchar(100) DEFAULT NULL COMMENT 'Vereins-WK',
  `Bez3` varchar(100) DEFAULT NULL COMMENT 'Mannschafts-WK'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfComment() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_Comment`
--

CREATE TABLE `wettkampf_comment` (
  `Wettkampf` int(11) NOT NULL,
  `Kommentar` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfDetails() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_details`
--

CREATE TABLE `wettkampf_details` (
  `Wettkampf` int(11) NOT NULL,
  `ZK_m` int(3) NOT NULL,
  `ZK_w` int(3) NOT NULL,
  `R_m` int(3) NOT NULL,
  `S_m` int(3) NOT NULL,
  `R_w` int(3) NOT NULL,
  `S_w` int(3) NOT NULL,
  `Hantelstange` varchar(10) NOT NULL,
  `Steigerung1` int(1) NOT NULL,
  `Steigerung2` int(1) NOT NULL,
  `AKs_m` int(2) NOT NULL DEFAULT -1 COMMENT 'Anzahl AKs bei Masters',
  `AKs_w` int(2) NOT NULL DEFAULT -1 COMMENT 'Anzahl AKs bei Masters',
  `Regel20` tinyint(1) NOT NULL,
  `nurZK` tinyint(1) NOT NULL,
  `International` tinyint(1) NOT NULL,
  `AutoLosnummer` tinyint(1) NOT NULL DEFAULT 1,
  `ThirdAttempt` tinyint(1) NOT NULL DEFAULT 1,
  `OrderByLifter` tinyint(1) NOT NULL DEFAULT 1,
  `Faktor_m` DOUBLE(7,4) NOT NULL DEFAULT '1.0000' COMMENT 'Faktor, mit dem die Wertung multipliziert wird',
  `Faktor_w` DOUBLE(7,4) NOT NULL DEFAULT '1.0000' COMMENT 'Faktor, mit dem die Wertung multipliziert wird' 

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfGewichtsgruppen() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_gewichtsgruppen`
--

CREATE TABLE `wettkampf_gewichtsgruppen` (
  `Wettkampf` int(11) NOT NULL,
  `Geschlecht` varchar(1) NOT NULL,
  `Gruppe` int(1) NOT NULL,
  `Heber` int(2) NOT NULL COMMENT 'Heber/Gruppe=TN(Gruppe)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfGewichtsteiler() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_gewichtsteiler`
--

CREATE TABLE `wettkampf_gewichtsteiler` (
  `Wettkampf` int(11) NOT NULL,
  `Gruppe` int(3) NOT NULL,
  `GG` int(3) NOT NULL,
  `KG_von` double(5,2) DEFAULT NULL,
  `KG_bis` double(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfMannschaft() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_mannschaft`
--

CREATE TABLE `wettkampf_mannschaft` (
  `Wettkampf` int(11) NOT NULL,
  `idMannschaft` int(1) NOT NULL COMMENT '0=Verein; 1=Land; 2=Liga',
  `min` int(2) NOT NULL,
  `max` int(2) NOT NULL,
  `gewertet` int(2) NOT NULL,
  `m_w` tinyint(1) NOT NULL,
  `multi` tinyint(1) NOT NULL COMMENT 'Mehrere Mannschaften pro Verein',
  `relativ` int(2) DEFAULT NULL COMMENT 'Relativwertung W (weitere M)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfMeldefrist() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_meldefrist`
--

CREATE TABLE `wettkampf_meldefrist` (
  `Wettkampf` int(11) NOT NULL,
  `Datum1` date DEFAULT NULL,
  `Datum2` date DEFAULT NULL,
  `Verein` double(5,2) DEFAULT NULL COMMENT 'Startgeld',
  `Land` double(5,2) DEFAULT NULL COMMENT 'Startgeld',
  `Liga` double(5,2) DEFAULT NULL COMMENT 'Startgeld'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfMulti() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_multi`
--

CREATE TABLE `wettkampf_multi` (
  `Wettkampf` int(11) NOT NULL,
  `Multi` varchar(50) NOT NULL,
  `Primaer` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfRestriction() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_restriction`
--

CREATE TABLE `wettkampf_restriction` (
  `Wettkampf` int(11) NOT NULL,
  `Durchgang` int(1) NOT NULL COMMENT 'Reißen=0, Stossen=1',
  `Altersklasse` int(2) NOT NULL COMMENT 'diese AK (nicht JG) und jünger',
  `Versuche` int(1) NOT NULL COMMENT 'zu absolvierende Versuche'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfStartgeld() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_startgeld`
--

CREATE TABLE `wettkampf_startgeld` (
  `Wettkampf` int(11) NOT NULL,
  `AK` int(3) NOT NULL DEFAULT 0 COMMENT '0 = ohne AKs',
  `Gebuehr` double(5,2) DEFAULT NULL,
  `NM_Gebuehr` double(5,2) DEFAULT NULL,
  `Gebuehr_oL` double(5,2) DEFAULT NULL COMMENT 'ohne BVDG-Lizenz',
  `NM_Gebuehr_oL` double(5,2) DEFAULT NULL COMMENT 'ohne BVDG-Lizenz'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfTeams() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_teams`
--

CREATE TABLE `wettkampf_teams` (
  `Wettkampf` int(11) NOT NULL,
  `Team` int(11) NOT NULL,
  `Id` int(1) NOT NULL COMMENT '0=Heim, 1=Gast1, 2=Gast2',
  `Punkte` int(1) DEFAULT 0,
  `Punkte2` int(1) DEFAULT 0,
  `Reissen` double(8,4) DEFAULT NULL,
  `Stossen` double(8,4) DEFAULT NULL,
  `Heben` double(8,4) DEFAULT NULL,
  `Platz` int(1) DEFAULT NULL,
  `Sieger` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfWertung() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_wertung`
--

CREATE TABLE `wettkampf_wertung` (
  `Wettkampf` int(11) NOT NULL,
  `idAK` int(4) NOT NULL,
  `Wertung` int(3) DEFAULT NULL,
  `Wertung2` int(3) DEFAULT NULL,
  `Gruppierung` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Masters (erweiterte AKs mit Zuordnung der Wertung) ';

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWettkampfZeitplan() As String
            Return "
--
-- Tabellenstruktur für Tabelle `wettkampf_zeitplan`
--

CREATE TABLE `wettkampf_zeitplan` (
  `Wettkampf` int(11) NOT NULL,
  `Gruppe` int(4) NOT NULL,
  `Datum` date NOT NULL,
  `Beginn` datetime NOT NULL,
  `Ende` datetime DEFAULT NULL,
  `Bezeichnung` varchar(100) NOT NULL,
  `Drucken` tinyint(1) NOT NULL,
  `TN` int(3) DEFAULT NULL,
  `Gewichtsklassen` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------
"
        End Function

        Private Function CreateWorldrecords() As String
            Return "
--
-- Tabellenstruktur für Tabelle `world_records`
--

CREATE TABLE `world_records` (
  `id` smallint(6) NOT NULL,
  `scope` varchar(10) NOT NULL,
  `sex` varchar(6) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `lift` int(1) NOT NULL,
  `result` varchar(20) DEFAULT '',
  `name` varchar(254) DEFAULT '',
  `born` date DEFAULT NULL,
  `nation` varchar(4) DEFAULT '',
  `age_category` varchar(15) NOT NULL DEFAULT '',
  `age_id` int(3) NOT NULL,
  `bw_category` varchar(5) DEFAULT '',
  `bw_id` int(3) NOT NULL,
  `date` date DEFAULT '2018-11-01',
  `place` varchar(254) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_hungarian_ci;

--
-- Daten für Tabelle `world_records`
--

INSERT INTO `world_records` (`id`, `scope`, `sex`, `type`, `lift`, `result`, `name`, `born`, `nation`, `age_category`, `age_id`, `bw_category`, `bw_id`, `date`, `place`) VALUES
(1, 'Welt', 'm', 'Total', 3, '293', 'World Standard', NULL, NULL, 'Senior', 50, '-55', 60, '2018-11-01', NULL),
(2, 'Welt', 'm', 'Total', 3, '312', 'World Standard', NULL, NULL, 'Senior', 50, '-61', 70, '2018-11-01', NULL),
(3, 'Welt', 'm', 'Total', 3, '331', 'World Standard', NULL, NULL, 'Senior', 50, '-67', 80, '2018-11-01', NULL),
(4, 'Welt', 'm', 'Total', 3, '348', 'World Standard', NULL, NULL, 'Senior', 50, '-73', 90, '2018-11-01', NULL),
(5, 'Welt', 'm', 'Total', 3, '368', 'World Standard', NULL, NULL, 'Senior', 50, '-81', 100, '2018-11-01', NULL),
(6, 'Welt', 'm', 'Total', 3, '387', 'World Standard', NULL, NULL, 'Senior', 50, '-89', 110, '2018-11-01', NULL),
(7, 'Welt', 'm', 'Total', 3, '401', 'World Standard', NULL, NULL, 'Senior', 50, '-96', 120, '2018-11-01', NULL),
(8, 'Welt', 'm', 'Total', 3, '412', 'World Standard', NULL, NULL, 'Senior', 50, '-102', 130, '2018-11-01', NULL),
(9, 'Welt', 'm', 'Total', 3, '424', 'World Standard', NULL, NULL, 'Senior', 50, '-109', 140, '2018-11-01', NULL),
(10, 'Welt', 'm', 'Total', 3, '453', 'World Standard', NULL, NULL, 'Senior', 50, '+109', 150, '2018-11-01', NULL),
(11, 'Welt', 'w', 'Total', 3, '191', 'World Standard', NULL, NULL, 'Senior', 50, '-45', 40, '2018-11-01', NULL),
(12, 'Welt', 'w', 'Total', 3, '203', 'World Standard', NULL, NULL, 'Senior', 50, '-49', 50, '2018-11-01', NULL),
(13, 'Welt', 'w', 'Total', 3, '221', 'World Standard', NULL, NULL, 'Senior', 50, '-55', 60, '2018-11-01', NULL),
(14, 'Welt', 'w', 'Total', 3, '232', 'World Standard', NULL, NULL, 'Senior', 50, '-59', 70, '2018-11-01', NULL),
(15, 'Welt', 'w', 'Total', 3, '245', 'World Standard', NULL, NULL, 'Senior', 50, '-64', 80, '2018-11-01', NULL),
(16, 'Welt', 'w', 'Total', 3, '261', 'World Standard', NULL, NULL, 'Senior', 50, '-71', 90, '2018-11-01', NULL),
(17, 'Welt', 'w', 'Total', 3, '272', 'World Standard', NULL, NULL, 'Senior', 50, '-76', 100, '2018-11-01', NULL),
(18, 'Welt', 'w', 'Total', 3, '283', 'World Standard', NULL, NULL, 'Senior', 50, '-81', 110, '2018-11-01', NULL),
(19, 'Welt', 'w', 'Total', 3, '294', 'World Standard', NULL, NULL, 'Senior', 50, '-87', 120, '2018-11-01', NULL),
(20, 'Welt', 'w', 'Total', 3, '320', 'World Standard', NULL, NULL, 'Senior', 50, '+87', 130, '2018-11-01', NULL),
(21, 'Welt', 'm', 'Total', 3, '264', 'World Standard', NULL, NULL, 'Junior', 40, '-55', 60, '2018-11-01', NULL),
(22, 'Welt', 'm', 'Total', 3, '285', 'World Standard', NULL, NULL, 'Junior', 40, '-61', 70, '2018-11-01', NULL),
(23, 'Welt', 'm', 'Total', 3, '305', 'World Standard', NULL, NULL, 'Junior', 40, '-67', 80, '2018-11-01', NULL),
(24, 'Welt', 'm', 'Total', 3, '323', 'World Standard', NULL, NULL, 'Junior', 40, '-73', 90, '2018-11-01', NULL),
(25, 'Welt', 'm', 'Total', 3, '344', 'World Standard', NULL, NULL, 'Junior', 40, '-81', 100, '2018-11-01', NULL),
(26, 'Welt', 'm', 'Total', 3, '362', 'World Standard', NULL, NULL, 'Junior', 40, '-89', 110, '2018-11-01', NULL),
(27, 'Welt', 'm', 'Total', 3, '376', 'World Standard', NULL, NULL, 'Junior', 40, '-96', 120, '2018-11-01', NULL),
(28, 'Welt', 'm', 'Total', 3, '385', 'World Standard', NULL, NULL, 'Junior', 40, '-102', 130, '2018-11-01', NULL),
(29, 'Welt', 'm', 'Total', 3, '394', 'World Standard', NULL, NULL, 'Junior', 40, '-109', 140, '2018-11-01', NULL),
(30, 'Welt', 'm', 'Total', 3, '407', 'World Standard', NULL, NULL, 'Junior', 40, '+109', 150, '2018-11-01', NULL),
(31, 'Welt', 'w', 'Total', 3, '176', 'World Standard', NULL, NULL, 'Junior', 40, '-45', 40, '2018-11-01', NULL),
(32, 'Welt', 'w', 'Total', 3, '188', 'World Standard', NULL, NULL, 'Junior', 40, '-49', 50, '2018-11-01', NULL),
(33, 'Welt', 'w', 'Total', 3, '203', 'World Standard', NULL, NULL, 'Junior', 40, '-55', 60, '2018-11-01', NULL),
(34, 'Welt', 'w', 'Total', 3, '213', 'World Standard', NULL, NULL, 'Junior', 40, '-59', 70, '2018-11-01', NULL),
(35, 'Welt', 'w', 'Total', 3, '225', 'World Standard', NULL, NULL, 'Junior', 40, '-64', 80, '2018-11-01', NULL),
(36, 'Welt', 'w', 'Total', 3, '240', 'World Standard', NULL, NULL, 'Junior', 40, '-71', 90, '2018-11-01', NULL),
(37, 'Welt', 'w', 'Total', 3, '250', 'World Standard', NULL, NULL, 'Junior', 40, '-76', 100, '2018-11-01', NULL),
(38, 'Welt', 'w', 'Total', 3, '259', 'World Standard', NULL, NULL, 'Junior', 40, '-81', 110, '2018-11-01', NULL),
(39, 'Welt', 'w', 'Total', 3, '269', 'World Standard', NULL, NULL, 'Junior', 40, '-87', 120, '2018-11-01', NULL),
(40, 'Welt', 'w', 'Total', 3, '285', 'World Standard', NULL, NULL, 'Junior', 40, '+87', 130, '2018-11-01', NULL),
(41, 'Welt', 'm', 'Total', 3, '206', 'World Standard', NULL, NULL, 'U15', 20, '-49', 50, '2018-11-01', NULL),
(41, 'Welt', 'm', 'Total', 3, '206', 'World Standard', NULL, NULL, 'Youth', 30, '-49', 50, '2018-11-01', NULL),
(42, 'Welt', 'm', 'Total', 3, '236', 'World Standard', NULL, NULL, 'U15', 20, '-55', 60, '2018-11-01', NULL),
(42, 'Welt', 'm', 'Total', 3, '236', 'World Standard', NULL, NULL, 'Youth', 30, '-55', 60, '2018-11-01', NULL),
(43, 'Welt', 'm', 'Total', 3, '263', 'World Standard', NULL, NULL, 'U15', 20, '-61', 70, '2018-11-01', NULL),
(43, 'Welt', 'm', 'Total', 3, '263', 'World Standard', NULL, NULL, 'Youth', 30, '-61', 70, '2018-11-01', NULL),
(44, 'Welt', 'm', 'Total', 3, '286', 'World Standard', NULL, NULL, 'U15', 20, '-67', 80, '2018-11-01', NULL),
(44, 'Welt', 'm', 'Total', 3, '286', 'World Standard', NULL, NULL, 'Youth', 30, '-67', 80, '2018-11-01', NULL),
(45, 'Welt', 'm', 'Total', 3, '306', 'World Standard', NULL, NULL, 'U15', 20, '-73', 90, '2018-11-01', NULL),
(45, 'Welt', 'm', 'Total', 3, '306', 'World Standard', NULL, NULL, 'Youth', 30, '-73', 90, '2018-11-01', NULL),
(46, 'Welt', 'm', 'Total', 3, '327', 'World Standard', NULL, NULL, 'U15', 20, '-81', 100, '2018-11-01', NULL),
(46, 'Welt', 'm', 'Total', 3, '327', 'World Standard', NULL, NULL, 'Youth', 30, '-81', 100, '2018-11-01', NULL),
(47, 'Welt', 'm', 'Total', 3, '342', 'World Standard', NULL, NULL, 'U15', 20, '-89', 110, '2018-11-01', NULL),
(47, 'Welt', 'm', 'Total', 3, '342', 'World Standard', NULL, NULL, 'Youth', 30, '-89', 110, '2018-11-01', NULL),
(48, 'Welt', 'm', 'Total', 3, '350', 'World Standard', NULL, NULL, 'U15', 20, '-96', 120, '2018-11-01', NULL),
(48, 'Welt', 'm', 'Total', 3, '350', 'World Standard', NULL, NULL, 'Youth', 30, '-96', 120, '2018-11-01', NULL),
(49, 'Welt', 'm', 'Total', 3, '353', 'World Standard', NULL, NULL, 'U15', 20, '-102', 130, '2018-11-01', NULL),
(49, 'Welt', 'm', 'Total', 3, '353', 'World Standard', NULL, NULL, 'Youth', 30, '-102', 130, '2018-11-01', NULL),
(50, 'Welt', 'm', 'Total', 3, '352', 'World Standard', NULL, NULL, 'U15', 20, '+102', 140, '2018-11-01', NULL),
(50, 'Welt', 'm', 'Total', 3, '352', 'World Standard', NULL, NULL, 'Youth', 30, '+102', 140, '2018-11-01', NULL),
(51, 'Welt', 'w', 'Total', 3, '135', 'World Standard', NULL, NULL, 'U15', 20, '-40', 30, '2018-11-01', NULL),
(51, 'Welt', 'w', 'Total', 3, '135', 'World Standard', NULL, NULL, 'Youth', 30, '-40', 30, '2018-11-01', NULL),
(52, 'Welt', 'w', 'Total', 3, '156', 'World Standard', NULL, NULL, 'U15', 20, '-45', 40, '2018-11-01', NULL),
(52, 'Welt', 'w', 'Total', 3, '156', 'World Standard', NULL, NULL, 'Youth', 30, '-45', 40, '2018-11-01', NULL),
(53, 'Welt', 'w', 'Total', 3, '172', 'World Standard', NULL, NULL, 'U15', 20, '-49', 50, '2018-11-01', NULL),
(53, 'Welt', 'w', 'Total', 3, '172', 'World Standard', NULL, NULL, 'Youth', 30, '-49', 50, '2018-11-01', NULL),
(54, 'Welt', 'w', 'Total', 3, '192', 'World Standard', NULL, NULL, 'U15', 20, '-55', 60, '2018-11-01', NULL),
(54, 'Welt', 'w', 'Total', 3, '192', 'World Standard', NULL, NULL, 'Youth', 30, '-55', 60, '2018-11-01', NULL),
(55, 'Welt', 'w', 'Total', 3, '203', 'World Standard', NULL, NULL, 'U15', 20, '-59', 70, '2018-11-01', NULL),
(55, 'Welt', 'w', 'Total', 3, '203', 'World Standard', NULL, NULL, 'Youth', 30, '-59', 70, '2018-11-01', NULL),
(56, 'Welt', 'w', 'Total', 3, '214', 'World Standard', NULL, NULL, 'U15', 20, '-64', 80, '2018-11-01', NULL),
(56, 'Welt', 'w', 'Total', 3, '214', 'World Standard', NULL, NULL, 'Youth', 30, '-64', 80, '2018-11-01', NULL),
(57, 'Welt', 'w', 'Total', 3, '225', 'World Standard', NULL, NULL, 'U15', 20, '-71', 90, '2018-11-01', NULL),
(57, 'Welt', 'w', 'Total', 3, '225', 'World Standard', NULL, NULL, 'Youth', 30, '-71', 90, '2018-11-01', NULL),
(58, 'Welt', 'w', 'Total', 3, '229', 'World Standard', NULL, NULL, 'U15', 20, '-76', 100, '2018-11-01', NULL),
(58, 'Welt', 'w', 'Total', 3, '229', 'World Standard', NULL, NULL, 'Youth', 30, '-76', 100, '2018-11-01', NULL),
(59, 'Welt', 'w', 'Total', 3, '231', 'World Standard', NULL, NULL, 'U15', 20, '-81', 110, '2018-11-01', NULL),
(59, 'Welt', 'w', 'Total', 3, '231', 'World Standard', NULL, NULL, 'Youth', 30, '-81', 110, '2018-11-01', NULL),
(60, 'Welt', 'w', 'Total', 3, '230', 'World Standard', NULL, NULL, 'U15', 20, '+81', 120, '2018-11-01', NULL),
(60, 'Welt', 'w', 'Total', 3, '230', 'World Standard', NULL, NULL, 'Youth', 30, '+81', 120, '2018-11-01', NULL),
(61, 'Welt', 'm', 'Snatch', 1, '135', 'World Standard', NULL, NULL, 'Senior', 50, '-55', 60, '2018-11-01', NULL),
(62, 'Welt', 'm', 'Snatch', 1, '144', 'World Standard', NULL, NULL, 'Senior', 50, '-61', 70, '2018-11-01', NULL),
(63, 'Welt', 'm', 'Snatch', 1, '153', 'World Standard', NULL, NULL, 'Senior', 50, '-67', 80, '2018-11-01', NULL),
(64, 'Welt', 'm', 'Snatch', 1, '160', 'World Standard', NULL, NULL, 'Senior', 50, '-73', 90, '2018-11-01', NULL),
(65, 'Welt', 'm', 'Snatch', 1, '170', 'World Standard', NULL, NULL, 'Senior', 50, '-81', 100, '2018-11-01', NULL),
(66, 'Welt', 'm', 'Snatch', 1, '179', 'World Standard', NULL, NULL, 'Senior', 50, '-89', 110, '2018-11-01', NULL),
(67, 'Welt', 'm', 'Snatch', 1, '185', 'World Standard', NULL, NULL, 'Senior', 50, '-96', 120, '2018-11-01', NULL),
(68, 'Welt', 'm', 'Snatch', 1, '191', 'World Standard', NULL, NULL, 'Senior', 50, '-102', 130, '2018-11-01', NULL),
(69, 'Welt', 'm', 'Snatch', 1, '196', 'World Standard', NULL, NULL, 'Senior', 50, '-109', 140, '2018-11-01', NULL),
(70, 'Welt', 'm', 'Snatch', 1, '210', 'World Standard', NULL, NULL, 'Senior', 50, '+109', 150, '2018-11-01', NULL),
(71, 'Welt', 'w', 'Snatch', 1, '85', 'World Standard', NULL, NULL, 'Senior', 50, '-45', 40, '2018-11-01', NULL),
(72, 'Welt', 'w', 'Snatch', 1, '90', 'World Standard', NULL, NULL, 'Senior', 50, '-49', 50, '2018-11-01', NULL),
(73, 'Welt', 'w', 'Snatch', 1, '99', 'World Standard', NULL, NULL, 'Senior', 50, '-55', 60, '2018-11-01', NULL),
(74, 'Welt', 'w', 'Snatch', 1, '104', 'World Standard', NULL, NULL, 'Senior', 50, '-59', 70, '2018-11-01', NULL),
(75, 'Welt', 'w', 'Snatch', 1, '110', 'World Standard', NULL, NULL, 'Senior', 50, '-64', 80, '2018-11-01', NULL),
(76, 'Welt', 'w', 'Snatch', 1, '117', 'World Standard', NULL, NULL, 'Senior', 50, '-71', 90, '2018-11-01', NULL),
(77, 'Welt', 'w', 'Snatch', 1, '122', 'World Standard', NULL, NULL, 'Senior', 50, '-76', 100, '2018-11-01', NULL),
(78, 'Welt', 'w', 'Snatch', 1, '127', 'World Standard', NULL, NULL, 'Senior', 50, '-81', 110, '2018-11-01', NULL),
(79, 'Welt', 'w', 'Snatch', 1, '132', 'World Standard', NULL, NULL, 'Senior', 50, '-87', 120, '2018-11-01', NULL),
(80, 'Welt', 'w', 'Snatch', 1, '143', 'World Standard', NULL, NULL, 'Senior', 50, '+87', 130, '2018-11-01', NULL),
(81, 'Welt', 'm', 'Snatch', 1, '122', 'World Standard', NULL, NULL, 'Junior', 40, '-55', 60, '2018-11-01', NULL),
(82, 'Welt', 'm', 'Snatch', 1, '132', 'World Standard', NULL, NULL, 'Junior', 40, '-61', 70, '2018-11-01', NULL),
(83, 'Welt', 'm', 'Snatch', 1, '141', 'World Standard', NULL, NULL, 'Junior', 40, '-67', 80, '2018-11-01', NULL),
(84, 'Welt', 'm', 'Snatch', 1, '149', 'World Standard', NULL, NULL, 'Junior', 40, '-73', 90, '2018-11-01', NULL),
(85, 'Welt', 'm', 'Snatch', 1, '158', 'World Standard', NULL, NULL, 'Junior', 40, '-81', 100, '2018-11-01', NULL),
(86, 'Welt', 'm', 'Snatch', 1, '166', 'World Standard', NULL, NULL, 'Junior', 40, '-89', 110, '2018-11-01', NULL),
(87, 'Welt', 'm', 'Snatch', 1, '172', 'World Standard', NULL, NULL, 'Junior', 40, '-96', 120, '2018-11-01', NULL),
(88, 'Welt', 'm', 'Snatch', 1, '177', 'World Standard', NULL, NULL, 'Junior', 40, '-102', 130, '2018-11-01', NULL),
(89, 'Welt', 'm', 'Snatch', 1, '181', 'World Standard', NULL, NULL, 'Junior', 40, '-109', 140, '2018-11-01', NULL),
(90, 'Welt', 'm', 'Snatch', 1, '186', 'World Standard', NULL, NULL, 'Junior', 40, '+109', 150, '2018-11-01', NULL),
(91, 'Welt', 'w', 'Snatch', 1, '78', 'World Standard', NULL, NULL, 'Junior', 40, '-45', 40, '2018-11-01', NULL),
(92, 'Welt', 'w', 'Snatch', 1, '84', 'World Standard', NULL, NULL, 'Junior', 40, '-49', 50, '2018-11-01', NULL),
(93, 'Welt', 'w', 'Snatch', 1, '92', 'World Standard', NULL, NULL, 'Junior', 40, '-55', 60, '2018-11-01', NULL),
(94, 'Welt', 'w', 'Snatch', 1, '96', 'World Standard', NULL, NULL, 'Junior', 40, '-59', 70, '2018-11-01', NULL),
(95, 'Welt', 'w', 'Snatch', 1, '102', 'World Standard', NULL, NULL, 'Junior', 40, '-64', 80, '2018-11-01', NULL),
(96, 'Welt', 'w', 'Snatch', 1, '109', 'World Standard', NULL, NULL, 'Junior', 40, '-71', 90, '2018-11-01', NULL),
(97, 'Welt', 'w', 'Snatch', 1, '114', 'World Standard', NULL, NULL, 'Junior', 40, '-76', 100, '2018-11-01', NULL),
(98, 'Welt', 'w', 'Snatch', 1, '118', 'World Standard', NULL, NULL, 'Junior', 40, '-81', 110, '2018-11-01', NULL),
(99, 'Welt', 'w', 'Snatch', 1, '122', 'World Standard', NULL, NULL, 'Junior', 40, '-87', 120, '2018-11-01', NULL),
(100, 'Welt', 'w', 'Snatch', 1, '129', 'World Standard', NULL, NULL, 'Junior', 40, '+87', 130, '2018-11-01', NULL),
(101, 'Welt', 'm', 'Snatch', 1, '94', 'World Standard', NULL, NULL, 'U15', 20, '-49', 50, '2018-11-01', NULL),
(101, 'Welt', 'm', 'Snatch', 1, '94', 'World Standard', NULL, NULL, 'Youth', 30, '-49', 50, '2018-11-01', NULL),
(102, 'Welt', 'm', 'Snatch', 1, '107', 'World Standard', NULL, NULL, 'U15', 20, '-55', 60, '2018-11-01', NULL),
(102, 'Welt', 'm', 'Snatch', 1, '107', 'World Standard', NULL, NULL, 'Youth', 30, '-55', 60, '2018-11-01', NULL),
(103, 'Welt', 'm', 'Snatch', 1, '119', 'World Standard', NULL, NULL, 'U15', 20, '-61', 70, '2018-11-01', NULL),
(103, 'Welt', 'm', 'Snatch', 1, '119', 'World Standard', NULL, NULL, 'Youth', 30, '-61', 70, '2018-11-01', NULL),
(104, 'Welt', 'm', 'Snatch', 1, '129', 'World Standard', NULL, NULL, 'U15', 20, '-67', 80, '2018-11-01', NULL),
(104, 'Welt', 'm', 'Snatch', 1, '129', 'World Standard', NULL, NULL, 'Youth', 30, '-67', 80, '2018-11-01', NULL),
(105, 'Welt', 'm', 'Snatch', 1, '138', 'World Standard', NULL, NULL, 'U15', 20, '-73', 90, '2018-11-01', NULL),
(105, 'Welt', 'm', 'Snatch', 1, '138', 'World Standard', NULL, NULL, 'Youth', 30, '-73', 90, '2018-11-01', NULL),
(106, 'Welt', 'm', 'Snatch', 1, '148', 'World Standard', NULL, NULL, 'U15', 20, '-81', 100, '2018-11-01', NULL),
(106, 'Welt', 'm', 'Snatch', 1, '148', 'World Standard', NULL, NULL, 'Youth', 30, '-81', 100, '2018-11-01', NULL),
(107, 'Welt', 'm', 'Snatch', 1, '155', 'World Standard', NULL, NULL, 'U15', 20, '-89', 110, '2018-11-01', NULL),
(107, 'Welt', 'm', 'Snatch', 1, '155', 'World Standard', NULL, NULL, 'Youth', 30, '-89', 110, '2018-11-01', NULL),
(108, 'Welt', 'm', 'Snatch', 1, '159', 'World Standard', NULL, NULL, 'U15', 20, '-96', 120, '2018-11-01', NULL),
(108, 'Welt', 'm', 'Snatch', 1, '159', 'World Standard', NULL, NULL, 'Youth', 30, '-96', 120, '2018-11-01', NULL),
(109, 'Welt', 'm', 'Snatch', 1, '162', 'World Standard', NULL, NULL, 'U15', 20, '-102', 130, '2018-11-01', NULL),
(109, 'Welt', 'm', 'Snatch', 1, '162', 'World Standard', NULL, NULL, 'Youth', 30, '-102', 130, '2018-11-01', NULL),
(110, 'Welt', 'm', 'Snatch', 1, '162', 'World Standard', NULL, NULL, 'U15', 20, '+102', 140, '2018-11-01', NULL),
(110, 'Welt', 'm', 'Snatch', 1, '162', 'World Standard', NULL, NULL, 'Youth', 30, '+102', 140, '2018-11-01', NULL),
(111, 'Welt', 'w', 'Snatch', 1, '61', 'World Standard', NULL, NULL, 'U15', 20, '-40', 30, '2018-11-01', NULL),
(111, 'Welt', 'w', 'Snatch', 1, '61', 'World Standard', NULL, NULL, 'Youth', 30, '-40', 30, '2018-11-01', NULL),
(112, 'Welt', 'w', 'Snatch', 1, '70', 'World Standard', NULL, NULL, 'U15', 20, '-45', 40, '2018-11-01', NULL),
(112, 'Welt', 'w', 'Snatch', 1, '70', 'World Standard', NULL, NULL, 'Youth', 30, '-45', 40, '2018-11-01', NULL),
(113, 'Welt', 'w', 'Snatch', 1, '77', 'World Standard', NULL, NULL, 'U15', 20, '-49', 50, '2018-11-01', NULL),
(113, 'Welt', 'w', 'Snatch', 1, '77', 'World Standard', NULL, NULL, 'Youth', 30, '-49', 50, '2018-11-01', NULL),
(114, 'Welt', 'w', 'Snatch', 1, '86', 'World Standard', NULL, NULL, 'U15', 20, '-55', 60, '2018-11-01', NULL),
(114, 'Welt', 'w', 'Snatch', 1, '86', 'World Standard', NULL, NULL, 'Youth', 30, '-55', 60, '2018-11-01', NULL),
(115, 'Welt', 'w', 'Snatch', 1, '91', 'World Standard', NULL, NULL, 'U15', 20, '-59', 70, '2018-11-01', NULL),
(115, 'Welt', 'w', 'Snatch', 1, '91', 'World Standard', NULL, NULL, 'Youth', 30, '-59', 70, '2018-11-01', NULL),
(116, 'Welt', 'w', 'Snatch', 1, '96', 'World Standard', NULL, NULL, 'U15', 20, '-64', 80, '2018-11-01', NULL),
(116, 'Welt', 'w', 'Snatch', 1, '96', 'World Standard', NULL, NULL, 'Youth', 30, '-64', 80, '2018-11-01', NULL),
(117, 'Welt', 'w', 'Snatch', 1, '102', 'World Standard', NULL, NULL, 'U15', 20, '-71', 90, '2018-11-01', NULL),
(117, 'Welt', 'w', 'Snatch', 1, '102', 'World Standard', NULL, NULL, 'Youth', 30, '-71', 90, '2018-11-01', NULL),
(118, 'Welt', 'w', 'Snatch', 1, '104', 'World Standard', NULL, NULL, 'U15', 20, '-76', 100, '2018-11-01', NULL),
(118, 'Welt', 'w', 'Snatch', 1, '104', 'World Standard', NULL, NULL, 'Youth', 30, '-76', 100, '2018-11-01', NULL),
(119, 'Welt', 'w', 'Snatch', 1, '106', 'World Standard', NULL, NULL, 'U15', 20, '-81', 110, '2018-11-01', NULL),
(119, 'Welt', 'w', 'Snatch', 1, '106', 'World Standard', NULL, NULL, 'Youth', 30, '-81', 110, '2018-11-01', NULL),
(120, 'Welt', 'w', 'Snatch', 1, '106', 'World Standard', NULL, NULL, 'U15', 20, '+81', 120, '2018-11-01', NULL),
(120, 'Welt', 'w', 'Snatch', 1, '106', 'World Standard', NULL, NULL, 'Youth', 30, '+81', 120, '2018-11-01', NULL),
(121, 'Welt', 'm', 'Clean and Jerk', 2, '161', 'World Standard', NULL, NULL, 'Senior', 50, '-55', 60, '2018-11-01', NULL),
(122, 'Welt', 'm', 'Clean and Jerk', 2, '173', 'World Standard', NULL, NULL, 'Senior', 50, '-61', 70, '2018-11-01', NULL),
(123, 'Welt', 'm', 'Clean and Jerk', 2, '184', 'World Standard', NULL, NULL, 'Senior', 50, '-67', 80, '2018-11-01', NULL),
(124, 'Welt', 'm', 'Clean and Jerk', 2, '194', 'World Standard', NULL, NULL, 'Senior', 50, '-73', 90, '2018-11-01', NULL),
(125, 'Welt', 'm', 'Clean and Jerk', 2, '206', 'World Standard', NULL, NULL, 'Senior', 50, '-81', 100, '2018-11-01', NULL),
(126, 'Welt', 'm', 'Clean and Jerk', 2, '216', 'World Standard', NULL, NULL, 'Senior', 50, '-89', 110, '2018-11-01', NULL),
(127, 'Welt', 'm', 'Clean and Jerk', 2, '225', 'World Standard', NULL, NULL, 'Senior', 50, '-96', 120, '2018-11-01', NULL),
(128, 'Welt', 'm', 'Clean and Jerk', 2, '231', 'World Standard', NULL, NULL, 'Senior', 50, '-102', 130, '2018-11-01', NULL),
(129, 'Welt', 'm', 'Clean and Jerk', 2, '237', 'World Standard', NULL, NULL, 'Senior', 50, '-109', 140, '2018-11-01', NULL),
(130, 'Welt', 'm', 'Clean and Jerk', 2, '250', 'World Standard', NULL, NULL, 'Senior', 50, '+109', 150, '2018-11-01', NULL),
(131, 'Welt', 'w', 'Clean and Jerk', 2, '108', 'World Standard', NULL, NULL, 'Senior', 50, '-45', 40, '2018-11-01', NULL),
(132, 'Welt', 'w', 'Clean and Jerk', 2, '115', 'World Standard', NULL, NULL, 'Senior', 50, '-49', 50, '2018-11-01', NULL),
(133, 'Welt', 'w', 'Clean and Jerk', 2, '124', 'World Standard', NULL, NULL, 'Senior', 50, '-55', 60, '2018-11-01', NULL),
(134, 'Welt', 'w', 'Clean and Jerk', 2, '131', 'World Standard', NULL, NULL, 'Senior', 50, '-59', 70, '2018-11-01', NULL),
(135, 'Welt', 'w', 'Clean and Jerk', 2, '138', 'World Standard', NULL, NULL, 'Senior', 50, '-64', 80, '2018-11-01', NULL),
(136, 'Welt', 'w', 'Clean and Jerk', 2, '147', 'World Standard', NULL, NULL, 'Senior', 50, '-71', 90, '2018-11-01', NULL),
(137, 'Welt', 'w', 'Clean and Jerk', 2, '153', 'World Standard', NULL, NULL, 'Senior', 50, '-76', 100, '2018-11-01', NULL),
(138, 'Welt', 'w', 'Clean and Jerk', 2, '158', 'World Standard', NULL, NULL, 'Senior', 50, '-81', 110, '2018-11-01', NULL),
(139, 'Welt', 'w', 'Clean and Jerk', 2, '164', 'World Standard', NULL, NULL, 'Senior', 50, '-87', 120, '2018-11-01', NULL),
(140, 'Welt', 'w', 'Clean and Jerk', 2, '177', 'World Standard', NULL, NULL, 'Senior', 50, '+87', 130, '2018-11-01', NULL),
(141, 'Welt', 'm', 'Clean and Jerk', 2, '146', 'World Standard', NULL, NULL, 'Junior', 40, '-55', 60, '2018-11-01', NULL),
(142, 'Welt', 'm', 'Clean and Jerk', 2, '158', 'World Standard', NULL, NULL, 'Junior', 40, '-61', 70, '2018-11-01', NULL),
(143, 'Welt', 'm', 'Clean and Jerk', 2, '169', 'World Standard', NULL, NULL, 'Junior', 40, '-67', 80, '2018-11-01', NULL),
(144, 'Welt', 'm', 'Clean and Jerk', 2, '179', 'World Standard', NULL, NULL, 'Junior', 40, '-73', 90, '2018-11-01', NULL),
(145, 'Welt', 'm', 'Clean and Jerk', 2, '190', 'World Standard', NULL, NULL, 'Junior', 40, '-81', 100, '2018-11-01', NULL),
(146, 'Welt', 'm', 'Clean and Jerk', 2, '200', 'World Standard', NULL, NULL, 'Junior', 40, '-89', 110, '2018-11-01', NULL),
(147, 'Welt', 'm', 'Clean and Jerk', 2, '207', 'World Standard', NULL, NULL, 'Junior', 40, '-96', 120, '2018-11-01', NULL),
(148, 'Welt', 'm', 'Clean and Jerk', 2, '212', 'World Standard', NULL, NULL, 'Junior', 40, '-102', 130, '2018-11-01', NULL),
(149, 'Welt', 'm', 'Clean and Jerk', 2, '217', 'World Standard', NULL, NULL, 'Junior', 40, '-109', 140, '2018-11-01', NULL),
(150, 'Welt', 'm', 'Clean and Jerk', 2, '221', 'World Standard', NULL, NULL, 'Junior', 40, '+109', 150, '2018-11-01', NULL),
(151, 'Welt', 'w', 'Clean and Jerk', 2, '99', 'World Standard', NULL, NULL, 'Junior', 40, '-45', 40, '2018-11-01', NULL),
(152, 'Welt', 'w', 'Clean and Jerk', 2, '105', 'World Standard', NULL, NULL, 'Junior', 40, '-49', 50, '2018-11-01', NULL),
(153, 'Welt', 'w', 'Clean and Jerk', 2, '114', 'World Standard', NULL, NULL, 'Junior', 40, '-55', 60, '2018-11-01', NULL),
(154, 'Welt', 'w', 'Clean and Jerk', 2, '120', 'World Standard', NULL, NULL, 'Junior', 40, '-59', 70, '2018-11-01', NULL),
(155, 'Welt', 'w', 'Clean and Jerk', 2, '126', 'World Standard', NULL, NULL, 'Junior', 40, '-64', 80, '2018-11-01', NULL),
(156, 'Welt', 'w', 'Clean and Jerk', 2, '135', 'World Standard', NULL, NULL, 'Junior', 40, '-71', 90, '2018-11-01', NULL),
(157, 'Welt', 'w', 'Clean and Jerk', 2, '140', 'World Standard', NULL, NULL, 'Junior', 40, '-76', 100, '2018-11-01', NULL),
(158, 'Welt', 'w', 'Clean and Jerk', 2, '145', 'World Standard', NULL, NULL, 'Junior', 40, '-81', 110, '2018-11-01', NULL),
(159, 'Welt', 'w', 'Clean and Jerk', 2, '150', 'World Standard', NULL, NULL, 'Junior', 40, '-87', 120, '2018-11-01', NULL),
(160, 'Welt', 'w', 'Clean and Jerk', 2, '158', 'World Standard', NULL, NULL, 'Junior', 40, '+87', 130, '2018-11-01', NULL),
(161, 'Welt', 'm', 'Clean and Jerk', 2, '116', 'World Standard', NULL, NULL, 'U15', 20, '-49', 50, '2018-11-01', NULL),
(161, 'Welt', 'm', 'Clean and Jerk', 2, '116', 'World Standard', NULL, NULL, 'Youth', 30, '-49', 50, '2018-11-01', NULL),
(162, 'Welt', 'm', 'Clean and Jerk', 2, '132', 'World Standard', NULL, NULL, 'U15', 20, '-55', 60, '2018-11-01', NULL),
(162, 'Welt', 'm', 'Clean and Jerk', 2, '132', 'World Standard', NULL, NULL, 'Youth', 30, '-55', 60, '2018-11-01', NULL),
(163, 'Welt', 'm', 'Clean and Jerk', 2, '147', 'World Standard', NULL, NULL, 'U15', 20, '-61', 70, '2018-11-01', NULL),
(163, 'Welt', 'm', 'Clean and Jerk', 2, '147', 'World Standard', NULL, NULL, 'Youth', 30, '-61', 70, '2018-11-01', NULL),
(164, 'Welt', 'm', 'Clean and Jerk', 2, '160', 'World Standard', NULL, NULL, 'U15', 20, '-67', 80, '2018-11-01', NULL),
(164, 'Welt', 'm', 'Clean and Jerk', 2, '160', 'World Standard', NULL, NULL, 'Youth', 30, '-67', 80, '2018-11-01', NULL),
(165, 'Welt', 'm', 'Clean and Jerk', 2, '171', 'World Standard', NULL, NULL, 'U15', 20, '-73', 90, '2018-11-01', NULL),
(165, 'Welt', 'm', 'Clean and Jerk', 2, '171', 'World Standard', NULL, NULL, 'Youth', 30, '-73', 90, '2018-11-01', NULL),
(166, 'Welt', 'm', 'Clean and Jerk', 2, '183', 'World Standard', NULL, NULL, 'U15', 20, '-81', 100, '2018-11-01', NULL),
(166, 'Welt', 'm', 'Clean and Jerk', 2, '183', 'World Standard', NULL, NULL, 'Youth', 30, '-81', 100, '2018-11-01', NULL),
(167, 'Welt', 'm', 'Clean and Jerk', 2, '191', 'World Standard', NULL, NULL, 'U15', 20, '-89', 110, '2018-11-01', NULL),
(167, 'Welt', 'm', 'Clean and Jerk', 2, '191', 'World Standard', NULL, NULL, 'Youth', 30, '-89', 110, '2018-11-01', NULL),
(168, 'Welt', 'm', 'Clean and Jerk', 2, '195', 'World Standard', NULL, NULL, 'U15', 20, '-96', 120, '2018-11-01', NULL),
(168, 'Welt', 'm', 'Clean and Jerk', 2, '195', 'World Standard', NULL, NULL, 'Youth', 30, '-96', 120, '2018-11-01', NULL),
(169, 'Welt', 'm', 'Clean and Jerk', 2, '197', 'World Standard', NULL, NULL, 'U15', 20, '-102', 130, '2018-11-01', NULL),
(169, 'Welt', 'm', 'Clean and Jerk', 2, '197', 'World Standard', NULL, NULL, 'Youth', 30, '-102', 130, '2018-11-01', NULL),
(170, 'Welt', 'm', 'Clean and Jerk', 2, '196', 'World Standard', NULL, NULL, 'U15', 20, '+102', 140, '2018-11-01', NULL),
(170, 'Welt', 'm', 'Clean and Jerk', 2, '196', 'World Standard', NULL, NULL, 'Youth', 30, '+102', 140, '2018-11-01', NULL),
(171, 'Welt', 'w', 'Clean and Jerk', 2, '75', 'World Standard', NULL, NULL, 'U15', 20, '-40', 30, '2018-11-01', NULL),
(171, 'Welt', 'w', 'Clean and Jerk', 2, '75', 'World Standard', NULL, NULL, 'Youth', 30, '-40', 30, '2018-11-01', NULL),
(172, 'Welt', 'w', 'Clean and Jerk', 2, '87', 'World Standard', NULL, NULL, 'U15', 20, '-45', 40, '2018-11-01', NULL),
(172, 'Welt', 'w', 'Clean and Jerk', 2, '87', 'World Standard', NULL, NULL, 'Youth', 30, '-45', 40, '2018-11-01', NULL),
(173, 'Welt', 'w', 'Clean and Jerk', 2, '96', 'World Standard', NULL, NULL, 'U15', 20, '-49', 50, '2018-11-01', NULL),
(173, 'Welt', 'w', 'Clean and Jerk', 2, '96', 'World Standard', NULL, NULL, 'Youth', 30, '-49', 50, '2018-11-01', NULL),
(174, 'Welt', 'w', 'Clean and Jerk', 2, '107', 'World Standard', NULL, NULL, 'U15', 20, '-55', 60, '2018-11-01', NULL),
(174, 'Welt', 'w', 'Clean and Jerk', 2, '107', 'World Standard', NULL, NULL, 'Youth', 30, '-55', 60, '2018-11-01', NULL),
(175, 'Welt', 'w', 'Clean and Jerk', 2, '113', 'World Standard', NULL, NULL, 'U15', 20, '-59', 70, '2018-11-01', NULL),
(175, 'Welt', 'w', 'Clean and Jerk', 2, '113', 'World Standard', NULL, NULL, 'Youth', 30, '-59', 70, '2018-11-01', NULL),
(176, 'Welt', 'w', 'Clean and Jerk', 2, '120', 'World Standard', NULL, NULL, 'U15', 20, '-64', 80, '2018-11-01', NULL),
(176, 'Welt', 'w', 'Clean and Jerk', 2, '120', 'World Standard', NULL, NULL, 'Youth', 30, '-64', 80, '2018-11-01', NULL),
(177, 'Welt', 'w', 'Clean and Jerk', 2, '126', 'World Standard', NULL, NULL, 'U15', 20, '-71', 90, '2018-11-01', NULL),
(177, 'Welt', 'w', 'Clean and Jerk', 2, '126', 'World Standard', NULL, NULL, 'Youth', 30, '-71', 90, '2018-11-01', NULL),
(178, 'Welt', 'w', 'Clean and Jerk', 2, '128', 'World Standard', NULL, NULL, 'U15', 20, '-76', 100, '2018-11-01', NULL),
(178, 'Welt', 'w', 'Clean and Jerk', 2, '128', 'World Standard', NULL, NULL, 'Youth', 30, '-76', 100, '2018-11-01', NULL),
(179, 'Welt', 'w', 'Clean and Jerk', 2, '129', 'World Standard', NULL, NULL, 'U15', 20, '-81', 110, '2018-11-01', NULL),
(179, 'Welt', 'w', 'Clean and Jerk', 2, '129', 'World Standard', NULL, NULL, 'Youth', 30, '-81', 110, '2018-11-01', NULL),
(180, 'Welt', 'w', 'Clean and Jerk', 2, '128', 'World Standard', NULL, NULL, 'U15', 20, '+81', 120, '2018-11-01', NULL),
(180, 'Welt', 'w', 'Clean and Jerk', 2, '128', 'World Standard', NULL, NULL, 'Youth', 30, '+81', 120, '2018-11-01', NULL),
(181, 'Welt', 'm', 'Snatch', 1, '108', 'RUSEV Angel Hriskov', '2001-07-13', 'BUL', 'Youth', 30, '-55', 60, '2018-11-02', 'Ashgabat'),
(182, 'Welt', 'm', 'Clean and Jerk', 2, '141', 'CHOMCHUEN Teerapat', '2001-07-31', 'THA', 'Youth', 30, '-55', 60, '2018-11-02', 'Ashgabat'),
(183, 'Welt', 'm', 'Total', 3, '248', 'RUSEV Angel Hriskov', '2001-07-13', 'BUL', 'Youth', 30, '-55', 60, '2018-11-02', 'Ashgabat'),
(184, 'Welt', 'm', 'Clean and Jerk', 2, '162', 'OM Yun Chol', '1991-11-18', 'PRK', 'Senior', 50, '-55', 60, '2018-11-02', 'Ashgabat'),
(185, 'Welt', 'm', 'Snatch', 1, '136', 'ERGASHEV Adkhamjon', '1999-03-12', 'UZB', 'Junior', 40, '-61', 70, '2018-11-03', 'Ashgabat'),
(186, 'Welt', 'm', 'Total', 3, '293', 'ERGASHEV Adkhamjon', '1999-03-12', 'UZB', 'Junior', 40, '-61', 70, '2018-11-03', 'Ashgabat'),
(187, 'Welt', 'm', 'Clean and Jerk', 2, '174', 'IRAWAN Eko Yuli', '1989-07-24', 'INA', 'Senior', 50, '-61', 70, '2018-11-03', 'Ashgabat'),
(188, 'Welt', 'm', 'Total', 3, '317', 'IRAWAN Eko Yuli', '1989-07-24', 'INA', 'Senior', 50, '-61', 70, '2018-11-03', 'Ashgabat'),
(189, 'Welt', 'w', 'Clean and Jerk', 2, '90', 'ECHANDIA ZARATE Katherin Oriana', '2001-08-14', 'VEN', 'Youth', 30, '-45', 40, '2018-11-02', 'Ashgabat'),
(190, 'Welt', 'w', 'Clean and Jerk', 2, '104', 'DZHUMABAYEVA Yulduz', '1998-04-22', 'TKM', 'Junior', 40, '-45', 40, '2018-11-02', 'Ashgabat'),
(191, 'Welt', 'w', 'Total', 3, '179', 'DZHUMABAYEVA Yulduz', '1998-04-22', 'TKM', 'Junior', 40, '-45', 40, '2018-11-02', 'Ashgabat'),
(192, 'Welt', 'w', 'Snatch', 1, '92', 'JIANG Huihua', '1998-01-22', 'CHN', 'Junior', 40, '-49', 50, '2018-11-03', 'Ashgabat'),
(193, 'Welt', 'w', 'Clean and Jerk', 2, '114', 'JIANG Huihua', '1998-01-22', 'CHN', 'Junior', 40, '-49', 50, '2018-11-03', 'Ashgabat'),
(194, 'Welt', 'w', 'Total', 3, '206', 'JIANG Huihua', '1998-01-22', 'CHN', 'Junior', 40, '-49', 50, '2018-11-03', 'Ashgabat'),
(195, 'Welt', 'w', 'Snatch', 1, '93', 'TANASAN Sopita', '1994-12-23', 'THA', 'Senior', 50, '-49', 50, '2018-11-03', 'Ashgabat'),
(196, 'Welt', 'w', 'Clean and Jerk', 2, '120', 'PRAMONGKHOL Chayuttra', '1994-11-29', 'THA', 'Senior', 50, '-49', 50, '2018-11-03', 'Ashgabat'),
(197, 'Welt', 'w', 'Total', 3, '209', 'PRAMONGKHOL Chayuttra', '1994-11-29', 'THA', 'Senior', 50, '-49', 50, '2018-11-03', 'Ashgabat'),
(198, 'Welt', 'w', 'Snatch', 1, '97', 'LANDOULSI Nouha', '1998-05-05', 'TUN', 'Junior', 40, '-55', 60, '2018-11-03', 'Ashgabat'),
(199, 'Welt', 'w', 'Total', 3, '211', 'LANDOULSI Nouha', '1998-05-05', 'TUN', 'Junior', 40, '-55', 60, '2018-11-03', 'Ashgabat'),
(200, 'Welt', 'w', 'Snatch', 1, '105', 'SRISURAT Sukanya', '1995-05-03', 'THA', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat'),
(201, 'Welt', 'w', 'Clean and Jerk', 2, '125', 'SRISURAT Sukanya', '1995-05-03', 'THA', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat'),
(202, 'Welt', 'w', 'Total', 3, '230', 'SRISURAT Sukanya', '1995-05-03', 'THA', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat'),
(203, 'Welt', 'm', 'Clean and Jerk', 2, '195', 'WON Jeongsik', '1990-12-09', 'KOR', 'Senior', 50, '-73', 90, '2018-11-04', 'Ashgabat'),
(204, 'Welt', 'm', 'Total', 3, '332', 'CHEN Lijun', '1993-02-08', 'CHN', 'Senior', 50, '-67', 80, '2018-11-04', 'Ashgabat'),
(205, 'Welt', 'w', 'Snatch', 1, '103', 'KOHA Rebeka', '1998-05-19', 'LAT', 'Junior', 40, '-59', 70, '2018-11-04', 'Ashgabat'),
(206, 'Welt', 'w', 'Clean and Jerk', 2, '124', 'KOHA Rebeka', '1998-05-19', 'LAT', 'Junior', 40, '-59', 70, '2018-11-04', 'Ashgabat'),
(207, 'Welt', 'w', 'Total', 3, '227', 'KOHA Rebeka', '1998-05-19', 'LAT', 'Junior', 40, '-59', 70, '2018-11-04', 'Ashgabat'),
(208, 'Welt', 'w', 'Snatch', 1, '105', 'KUO Hsing-Chun', '1993-11-26', 'TPE', 'Senior', 50, '-59', 70, '2018-11-04', 'Ashgabat'),
(209, 'Welt', 'w', 'Clean and Jerk', 2, '133', 'CHEN Guiming', '1994-01-03', 'CHN', 'Senior', 50, '-59', 70, '2018-11-04', 'Ashgabat'),
(210, 'Welt', 'w', 'Total', 3, '237', 'KUO Hsing-Chun', '1993-11-26', 'TPE', 'Senior', 50, '-59', 70, '2018-11-04', 'Ashgabat'),
(211, 'Welt', 'm', 'Snatch', 1, '164', 'SHI Zhiyong', '1993-10-10', 'CHN', 'Senior', 50, '-73', 90, '2018-11-04', 'Ashgabat'),
(212, 'Welt', 'm', 'Clean and Jerk', 2, '196', 'SHI Zhiyong', '1993-10-10', 'CHN', 'Senior', 50, '-73', 90, '2018-11-04', 'Ashgabat'),
(213, 'Welt', 'm', 'Total', 3, '360', 'SHI Zhiyong', '1993-10-10', 'CHN', 'Senior', 50, '-73', 90, '2018-11-04', 'Ashgabat'),
(214, 'Welt', 'm', 'Clean and Jerk', 2, '187', 'CUMMINGS JR Clarence', '2000-06-06', 'USA', 'Junior', 40, '-73', 90, '2018-11-04', 'Ashgabat'),
(215, 'Welt', 'm', 'Total', 3, '335', 'CUMMINGS JR Clarence', '2000-06-06', 'USA', 'Junior', 40, '-73', 90, '2018-11-04', 'Ashgabat'),
(216, 'Welt', 'm', 'Snatch', 1, '159', 'SUHAREVS Ritvars', '1999-01-11', 'LAT', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(217, 'Welt', 'm', 'Clean and Jerk', 2, '133', 'CHOMCHUEN Teerapat', '2001-07-31', 'THA', 'Youth', 30, '-55', 60, '2018-11-02', 'Ashgabat'),
(218, 'Welt', 'm', 'Clean and Jerk', 2, '135', 'RUSEV Angel Hriskov', '2001-07-13', 'BUL', 'Youth', 30, '-55', 60, '2018-11-02', 'Ashgabat'),
(219, 'Welt', 'm', 'Clean and Jerk', 2, '136', 'CHOMCHUEN Teerapat', '2001-07-31', 'THA', 'Youth', 30, '-55', 60, '2018-11-02', 'Ashgabat'),
(220, 'Welt', 'm', 'Clean and Jerk', 2, '140', 'RUSEV Angel Hriskov', '2001-07-13', 'BUL', 'Youth', 30, '-55', 60, '2018-11-02', 'Ashgabat'),
(221, 'Welt', 'm', 'Total', 3, '243', 'RUSEV Angel Hriskov', '2001-07-13', 'BUL', 'Youth', 30, '-55', 60, '2018-11-02', 'Ashgabat'),
(222, 'Welt', 'm', 'Total', 3, '238', 'RUSEV Angel Hriskov', '2001-07-13', 'BUL', 'Youth', 30, '-55', 60, '2018-11-02', 'Ashgabat'),
(223, 'Welt', 'w', 'Snatch', 1, '93', 'LANDOULSI Nouha', '1998-05-05', 'TUN', 'Junior', 40, '-55', 60, '2018-11-03', 'Ashgabat'),
(224, 'Welt', 'w', 'Snatch', 1, '100', 'LI Yajun', '1993-04-27', 'CHN', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat'),
(225, 'Welt', 'w', 'Snatch', 1, '101', 'SRISURAT Sukanya', '1995-05-03', 'THA', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat'),
(226, 'Welt', 'w', 'Snatch', 1, '102', 'LI Yajun', '1993-04-27', 'CHN', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat'),
(227, 'Welt', 'w', 'Snatch', 1, '103', 'SRISURAT Sukanya', '1995-05-03', 'THA', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat'),
(228, 'Welt', 'w', 'Total', 3, '223', 'LI Yajun', '1993-04-27', 'CHN', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat'),
(229, 'Welt', 'w', 'Total', 3, '227', 'SRISURAT Sukanya', '1995-05-03', 'THA', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat'),
(230, 'Welt', 'w', 'Snatch', 1, '87', 'JIANG Huihua', '1998-01-22', 'CHN', 'Junior', 40, '-49', 50, '2018-11-03', 'Ashgabat'),
(231, 'Welt', 'w', 'Snatch', 1, '90', 'JIANG Huihua', '1998-01-22', 'CHN', 'Junior', 40, '-49', 50, '2018-11-03', 'Ashgabat'),
(232, 'Welt', 'w', 'Clean and Jerk', 2, '108', 'JIANG Huihua', '1998-01-22', 'CHN', 'Junior', 40, '-49', 50, '2018-11-03', 'Ashgabat'),
(233, 'Welt', 'w', 'Snatch', 1, '92', 'JIANG Huihua', '1998-01-22', 'CHN', 'Senior', 50, '-49', 50, '2018-11-03', 'Ashgabat'),
(234, 'Welt', 'w', 'Total', 3, '205', 'HOU Zhihui', '1997-03-18', 'CHN', 'Senior', 50, '-49', 50, '2018-11-03', 'Ashgabat'),
(235, 'Welt', 'w', 'Total', 3, '206', 'JIANG Huihua', '1998-01-22', 'CHN', 'Senior', 50, '-49', 50, '2018-11-03', 'Ashgabat'),
(236, 'Welt', 'w', 'Total', 3, '208', 'HOU Zhihui', '1997-03-18', 'CHN', 'Senior', 50, '-49', 50, '2018-11-03', 'Ashgabat'),
(237, 'Welt', 'w', 'Total', 3, '200', 'JIANG Huihua', '1998-01-22', 'CHN', 'Junior', 40, '-49', 50, '2018-11-03', 'Ashgabat'),
(238, 'Welt', 'm', 'Snatch', 1, '133', 'ERGASHEV Adkhamjon', '1999-03-12', 'UZB', 'Junior', 40, '-61', 70, '2018-11-03', 'Ashgabat'),
(239, 'Welt', 'm', 'Total', 3, '313', 'IRAWAN Eko Yuli', '1989-07-24', 'INA', 'Senior', 50, '-61', 70, '2018-11-03', 'Ashgabat'),
(240, 'Welt', 'w', 'Snatch', 1, '98', 'KOHA Rebeka', '1998-05-19', 'LAT', 'Junior', 40, '-59', 70, '2018-11-04', 'Ashgabat'),
(241, 'Welt', 'w', 'Clean and Jerk', 2, '121', 'KOHA Rebeka', '1998-05-19', 'LAT', 'Junior', 40, '-59', 70, '2018-11-04', 'Ashgabat'),
(242, 'Welt', 'w', 'Total', 3, '220', 'KOHA Rebeka', '1998-05-19', 'LAT', 'Junior', 40, '-59', 70, '2018-11-04', 'Ashgabat'),
(243, 'Welt', 'w', 'Total', 3, '224', 'KOHA Rebeka', '1998-05-19', 'LAT', 'Junior', 40, '-59', 70, '2018-11-04', 'Ashgabat'),
(244, 'Welt', 'w', 'Total', 3, '233', 'KUO Hsing-Chun', '1993-11-26', 'TPE', 'Senior', 50, '-59', 70, '2018-11-04', 'Ashgabat'),
(245, 'Welt', 'w', 'Clean and Jerk', 2, '132', 'KUO Hsing-Chun', '1993-11-26', 'TPE', 'Senior', 50, '-59', 70, '2018-11-04', 'Ashgabat'),
(246, 'Welt', 'm', 'Clean and Jerk', 2, '181', 'CUMMINGS JR Clarence', '2000-06-06', 'USA', 'Junior', 40, '-73', 90, '2018-11-04', 'Ashgabat'),
(247, 'Welt', 'm', 'Total', 3, '329', 'CUMMINGS JR Clarence', '2000-06-06', 'USA', 'Junior', 40, '-73', 90, '2018-11-04', 'Ashgabat'),
(248, 'Welt', 'm', 'Snatch', 1, '161', 'SHI Zhiyong', '1993-10-10', 'CHN', 'Senior', 50, '-73', 90, '2018-11-04', 'Ashgabat'),
(249, 'Welt', 'm', 'Total', 3, '352', 'SHI Zhiyong', '1993-10-10', 'CHN', 'Senior', 50, '-73', 90, '2018-11-04', 'Ashgabat'),
(250, 'Welt', 'w', 'Total', 3, '247', 'DENG Wei', '1993-02-14', 'CHN', 'Senior', 50, '-64', 80, '2018-11-05', 'Ashgabat'),
(251, 'Welt', 'w', 'Total', 3, '250', 'DENG Wei', '1993-02-14', 'CHN', 'Senior', 50, '-64', 80, '2018-11-05', 'Ashgabat'),
(252, 'Welt', 'w', 'Total', 3, '252', 'DENG Wei', '1993-02-14', 'CHN', 'Senior', 50, '-64', 80, '2018-11-05', 'Ashgabat'),
(253, 'Welt', 'w', 'Snatch', 1, '112', 'DENG Wei', '1993-02-14', 'CHN', 'Senior', 50, '-64', 80, '2018-11-05', 'Ashgabat'),
(254, 'Welt', 'w', 'Clean and Jerk', 2, '140', 'DENG Wei', '1993-02-14', 'CHN', 'Senior', 50, '-64', 80, '2018-11-05', 'Ashgabat'),
(255, 'Welt', 'm', 'Snatch', 1, '163', 'LI Dayin', '1998-02-12', 'CHN', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(256, 'Welt', 'm', 'Snatch', 1, '168', 'LI Dayin', '1998-02-12', 'CHN', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(257, 'Welt', 'm', 'Snatch', 1, '172', 'LYU Xiaojun', '1984-07-27', 'CHN', 'Senior', 50, '-81', 100, '2018-11-05', 'Ashgabat'),
(258, 'Welt', 'm', 'Snatch', 1, '173', 'MAHMOUD Mohamed Ihab Youssef Ahmed', '1989-11-21', 'EGY', 'Senior', 50, '-81', 100, '2018-11-05', 'Ashgabat'),
(259, 'Welt', 'm', 'Clean and Jerk', 2, '191', 'MAURUS Harrison James', '2000-02-26', 'USA', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(260, 'Welt', 'm', 'Total', 3, '348', 'MAURUS Harrison James', '2000-02-26', 'USA', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(261, 'Welt', 'm', 'Clean and Jerk', 2, '193', 'LI Dayin', '1998-02-12', 'CHN', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(262, 'Welt', 'm', 'Total', 3, '361', 'LI Dayin', '1998-02-12', 'CHN', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(263, 'Welt', 'm', 'Clean and Jerk', 2, '195', 'MAURUS Harrison James', '2000-02-26', 'USA', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(264, 'Welt', 'm', 'Total', 3, '369', 'MAHMOUD Mohamed Ihab Youssef Ahmed', '1989-11-21', 'EGY', 'Senior', 50, '-81', 100, '2018-11-05', 'Ashgabat'),
(265, 'Welt', 'm', 'Clean and Jerk', 2, '198', 'LI Dayin', '1998-02-12', 'CHN', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(266, 'Welt', 'm', 'Total', 3, '366', 'LI Dayin', '1998-02-12', 'CHN', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(267, 'Welt', 'm', 'Total', 3, '373', 'MAHMOUD Mohamed Ihab Youssef Ahmed', '1989-11-21', 'EGY', 'Senior', 50, '-81', 100, '2018-11-05', 'Ashgabat'),
(268, 'Welt', 'm', 'Clean and Jerk', 2, '200', 'MAURUS Harrison James', '2000-02-26', 'USA', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(269, 'Welt', 'm', 'Total', 3, '374', 'LYU Xiaojun', '1984-07-27', 'CHN', 'Senior', 50, '-81', 100, '2018-11-05', 'Ashgabat'),
(270, 'Welt', 'm', 'Clean and Jerk', 2, '204', 'LI Dayin	', '1998-02-12', 'CHN', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(271, 'Welt', 'm', 'Total', 3, '372', 'LI Dayin	', '1998-02-12', 'CHN', 'Junior', 40, '-81', 100, '2018-11-05', 'Ashgabat'),
(272, 'Welt', 'w', 'Snatch', 1, '111', 'AHMED Sara Samir Elsayed Mohamed', '1998-01-01', 'EGY', 'Junior', 40, '-71', 90, '2018-11-06', 'Ashgabat'),
(273, 'Welt', 'w', 'Clean and Jerk', 2, '136', 'AHMED Sara Samir Elsayed Mohamed', '1998-01-01', 'EGY', 'Junior', 40, '-71', 90, '2018-11-06', 'Ashgabat'),
(274, 'Welt', 'w', 'Total', 3, '247', 'AHMED Sara Samir Elsayed Mohamed', '1998-01-01', 'EGY', 'Junior', 40, '-71', 90, '2018-11-06', 'Ashgabat'),
(275, 'Welt', 'w', 'Clean and Jerk', 2, '141', 'AHMED Sara Samir Elsayed Mohamed', '1998-01-01', 'EGY', 'Junior', 40, '-71', 90, '2018-11-06', 'Ashgabat'),
(276, 'Welt', 'w', 'Total', 3, '252', 'AHMED Sara Samir Elsayed Mohamed', '1998-01-01', 'EGY', 'Junior', 40, '-71', 90, '2018-11-06', 'Ashgabat'),
(277, 'Welt', 'w', 'Clean and Jerk', 2, '148', 'ZHANG Wangli', '1996-05-27', 'CHN', 'Senior', 50, '-71', 90, '2018-11-06', 'Ashgabat'),
(278, 'Welt', 'w', 'Total', 3, '263', 'ZHANG Wangli', '1996-05-27', 'CHN', 'Senior', 50, '-71', 90, '2018-11-06', 'Ashgabat'),
(279, 'Welt', 'w', 'Clean and Jerk', 2, '152', 'ZHANG Wangli', '1996-05-27', 'CHN', 'Senior', 50, '-71', 90, '2018-11-06', 'Ashgabat'),
(280, 'Welt', 'w', 'Total', 3, '267', 'ZHANG Wangli', '1996-05-27', 'CHN', 'Senior', 50, '-71', 90, '2018-11-06', 'Ashgabat'),
(281, 'Welt', 'm', 'Snatch', 1, '168', 'DAVITADZE Revaz', '1998-10-16', 'GEO', 'Junior', 40, '-89', 110, '2018-11-06', 'Ashgabat'),
(282, 'Welt', 'm', 'Clean and Jerk', 2, '201', 'VALLENILLA SANCHEZ Keydomar Giovanni', '1999-10-08', 'VEN', 'Junior', 40, '-89', 110, '2018-11-06', 'Ashgabat'),
(283, 'Welt', 'm', 'Total', 3, '366', 'VALLENILLA SANCHEZ Keydomar Giovanni', '1999-10-08', 'VEN', 'Junior', 40, '-89', 110, '2018-11-06', 'Ashgabat'),
(284, 'Welt', 'm', 'Clean and Jerk', 2, '203', 'DAVITADZE Revaz', '1998-10-16', 'GEO', 'Junior', 40, '-89', 110, '2018-11-06', 'Ashgabat'),
(285, 'Welt', 'm', 'Total', 3, '371', 'DAVITADZE Revaz', '1998-10-16', 'GEO', 'Junior', 40, '-89', 110, '2018-11-06', 'Ashgabat'),
(286, 'Welt', 'm', 'Clean and Jerk', 2, '204', 'VALLENILLA SANCHEZ Keydomar Giovanni', '1999-10-08', 'VEN', 'Junior', 40, '-89', 110, '2018-11-06', 'Ashgabat'),
(287, 'Welt', 'm', 'Snatch', 1, '173', 'TSIKHANTSOU Yauheni', '1998-11-04', 'BLR', 'Junior', 40, '-96', 120, '2018-11-07', 'Ashgabat'),
(288, 'Welt', 'm', 'Snatch', 1, '180', 'TSIKHANTSOU Yauheni', '1998-11-04', 'BLR', 'Junior', 40, '-96', 120, '2018-11-07', 'Ashgabat'),
(289, 'Welt', 'm', 'Total', 3, '377', 'RIVAS MOSQUERA Jhonatan', '1998-07-11', 'COL', 'Junior', 40, '-96', 120, '2018-11-07', 'Ashgabat'),
(290, 'Welt', 'm', 'Snatch', 1, '176', 'RIVAS MOSQUERA Jhonatan', '1998-07-11', 'COL', 'Junior', 40, '-96', 120, '2018-11-07', 'Ashgabat'),
(291, 'Welt', 'm', 'Clean and Jerk', 2, '210', 'TSIKHANTSOU Yauheni', '1998-11-04', 'BLR', 'Junior', 40, '-96', 120, '2018-11-07', 'Ashgabat'),
(292, 'Welt', 'm', 'Total', 3, '390', 'TSIKHANTSOU Yauheni', '1998-11-04', 'BLR', 'Junior', 40, '-96', 120, '2018-11-07', 'Ashgabat'),
(293, 'Welt', 'm', 'Clean and Jerk', 2, '217', 'ELBAKH Fares Ibrahim E. H.', '1998-06-04', 'QAT', 'Junior', 40, '-96', 120, '2018-11-07', 'Ashgabat'),
(294, 'Welt', 'm', 'Snatch', 1, '186', 'MORADI Sohrab', '1988-09-22', 'IRI', 'Senior', 50, '-96', 120, '2018-11-07', 'Ashgabat'),
(295, 'Welt', 'm', 'Total', 3, '409', 'MORADI Sohrab', '1988-09-22', 'IRI', 'Senior', 50, '-96', 120, '2018-11-07', 'Ashgabat'),
(296, 'Welt', 'm', 'Total', 3, '416', 'MORADI Sohrab', '1988-09-22', 'IRI', 'Senior', 50, '-96', 120, '2018-11-07', 'Ashgabat'),
(297, 'Welt', 'm', 'Clean and Jerk', 2, '230', 'MORADI Sohrab', '1988-09-22', 'IRI', 'Senior', 50, '-96', 120, '2018-11-07', 'Ashgabat'),
(298, 'Welt', 'm', 'Clean and Jerk', 2, '226', 'TIAN Tao', '1994-04-08', 'CHN', 'Senior', 50, '-96', 120, '2018-11-07', 'Ashgabat'),
(299, 'Welt', 'w', 'Snatch', 1, '115', 'DAJOMES BARRERA Neisi Patricia', '1998-05-12', 'ECU', 'Junior', 40, '-76', 100, '2018-11-07', 'Ashgabat'),
(300, 'Welt', 'w', 'Snatch', 1, '117', 'DAJOMES BARRERA Neisi Patricia', '1998-05-12', 'ECU', 'Junior', 40, '-76', 100, '2018-11-07', 'Ashgabat'),
(301, 'Welt', 'w', 'Total', 3, '254', 'DAJOMES BARRERA Neisi Patricia', '1998-05-12', 'ECU', 'Junior', 40, '-76', 100, '2018-11-07', 'Ashgabat'),
(302, 'Welt', 'w', 'Clean and Jerk', 2, '142', 'DAJOMES BARRERA Neisi Patricia', '1998-05-12', 'ECU', 'Junior', 40, '-76', 100, '2018-11-07', 'Ashgabat'),
(303, 'Welt', 'w', 'Total', 3, '259', 'DAJOMES BARRERA Neisi Patricia', '1998-05-12', 'ECU', 'Junior', 40, '-76', 100, '2018-11-07', 'Ashgabat'),
(304, 'Welt', 'm', 'Snatch', 1, '178', 'DJURAEV Akbar', '1999-10-08', 'UZB', 'Junior', 40, '-102', 130, '2018-11-08', 'Ashgabat'),
(305, 'Welt', 'm', 'Snatch', 1, '180', 'DJURAEV Akbar', '1999-10-08', 'UZB', 'Junior', 40, '-102', 130, '2018-11-08', 'Ashgabat'),
(306, 'Welt', 'm', 'Total', 3, '387', 'DJURAEV Akbar', '1999-10-08', 'UZB', 'Junior', 40, '-102', 130, '2018-11-08', 'Ashagabt'),
(307, 'Welt', 'm', 'Total', 3, '392', 'DJURAEV Akbar', '1999-10-08', 'UZB', 'Junior', 40, '-102', 130, '2018-11-08', 'Ashgabat'),
(308, 'Welt', 'm', 'Total', 3, '425', 'MARTIROSYAN Simon', '1997-02-17', 'ARM', 'Senior', 50, '-109', 140, '2018-11-09', 'Ashgabat'),
(309, 'Welt', 'm', 'Total', 3, '435', 'MARTIROSYAN Simon', '1997-02-17', 'ARM', 'Senior', 50, '-109', 140, '2018-11-09', 'Ashgabat'),
(310, 'Welt', 'm', 'Clean and Jerk', 2, '240', 'MARTIROSYAN Simon', '1997-02-17', 'ARM', 'Senior', 50, '-109', 140, '2018-11-09', 'Ashgabat'),
(311, 'Welt', 'w', 'Snatch', 1, '145', 'KASHIRINA Tatiana', '1991-01-24', 'RUS', 'Senior', 50, '+87', 130, '2018-11-10', 'Ashgabat'),
(312, 'Welt', 'w', 'Clean and Jerk', 2, '178', 'KASHIRINA Tatiana', '1991-01-24', 'RUS', 'Senior', 50, '+87', 130, '2018-11-10', 'Ashgabat'),
(313, 'Welt', 'w', 'Total', 3, '323', 'KASHIRINA Tatiana', '1991-01-24', 'RUS', 'Senior', 50, '+87', 130, '2018-11-10', 'Ashgabat'),
(314, 'Welt', 'w', 'Clean and Jerk', 2, '182', 'KASHIRINA Tatiana', '1991-01-24', 'RUS', 'Senior', 50, '+87', 130, '2018-11-10', 'Ashgabat'),
(315, 'Welt', 'w', 'Total', 3, '327', 'KASHIRINA Tatiana', '1991-01-24', 'RUS', 'Senior', 50, '+87', 130, '2018-11-10', 'Ashgabat'),
(316, 'Welt', 'w', 'Clean and Jerk', 2, '184', 'MENG Suping', '1989-07-17', 'CHN', 'Senior', 50, '+87', 130, '2018-11-10', 'Ashgabat'),
(317, 'Welt', 'w', 'Clean and Jerk', 2, '185', 'KASHIRINA Tatiana', '1991-01-24', 'RUS', 'Senior', 50, '+87', 130, '2018-11-10', 'Ashgabat'),
(318, 'Welt', 'w', 'Total', 3, '330', 'KASHIRINA Tatiana', '1991-01-24', 'RUS', 'Senior', 50, '+87', 130, '2018-11-10', 'Ashgabat'),
(319, 'Welt', 'm', 'Snatch', 1, '189', 'ZIAZIULIN Eduard', '1998-10-29', 'BLR', 'Junior', 40, '+109', 150, '2018-11-10', 'Ashgabat'),
(320, 'Welt', 'm', 'Snatch', 1, '192', 'DAVOUDI Ali', '1999-03-22', 'IRI', 'Junior', 40, '+109', 150, '2018-11-10', 'Ashgabat'),
(321, 'Welt', 'm', 'Snatch', 1, '193', 'ZIAZIULIN Eduard', '1998-10-29', 'BLR', 'Junior', 40, '+109', 150, '2018-11-10', 'Ashgabat'),
(322, 'Welt', 'm', 'Snatch', 1, '197', 'DAVOUDI Ali', '1999-03-22', 'IRI', 'Junior', 40, '+109', 150, '2018-11-10', 'Ashgabat'),
(323, 'Welt', 'm', 'Snatch', 1, '212', 'TALAKHADZE Lasha', '1993-10-02', 'GEO', 'Senior', 50, '+109', 150, '2018-11-10', 'Ashgabat'),
(324, 'Welt', 'm', 'Snatch', 1, '217', 'TALAKHADZE Lasha', '1993-10-02', 'GEO', 'Senior', 50, '+109', 150, '2018-11-10', 'Ashgabat'),
(325, 'Welt', 'm', 'Clean and Jerk', 2, '222', 'ZIAZIULIN Eduard', '1998-10-29', 'BLR', 'Junior', 40, '+109', 150, '2018-11-10', 'Ashgabat'),
(326, 'Welt', 'm', 'Total', 3, '415', 'ZIAZIULIN Eduard', '1998-10-29', 'BLR', 'Junior', 40, '+109', 150, '2018-11-10', 'Ashgabat'),
(327, 'Welt', 'm', 'Clean and Jerk', 2, '227', 'DAVOUDI Ali', '1999-03-22', 'IRI', 'Junior', 40, '+109', 150, '2018-11-10', 'Ashgabat'),
(328, 'Welt', 'm', 'Total', 3, '424', 'DAVOUDI Ali', '1999-03-22', 'IRI', 'Junior', 40, '+109', 150, '2018-11-10', 'Ashgabat'),
(329, 'Welt', 'm', 'Clean and Jerk', 2, '228', 'ZIAZIULIN Eduard', '1998-10-29', 'BLR', 'Junior', 40, '+109', 150, '2018-11-10', 'Ashgabat'),
(330, 'Welt', 'm', 'Total', 3, '462', 'TALAKHADZE Lasha', '1993-10-02', 'GEO', 'Senior', 50, '+109', 150, '2018-11-10', 'Ashgabat'),
(331, 'Welt', 'm', 'Clean and Jerk', 2, '252', 'TALAKHADZE Lasha', '1993-10-02', 'GEO', 'Senior', 50, '+109', 150, '2018-11-10', 'Ashgabat'),
(332, 'Welt', 'm', 'Total', 3, '469', 'TALAKHADZE Lasha', '1993-10-02', 'GEO', 'Senior', 50, '+109', 150, '2018-11-10', 'Ashgabat'),
(333, 'Welt', 'm', 'Clean and Jerk', 2, '257', 'TALAKHADZE Lasha', '1993-10-02', 'GEO', 'Senior', 50, '+109', 150, '2018-11-10', 'Ashgabat'),
(334, 'Welt', 'm', 'Total', 3, '474', 'TALAKHADZE Lasha', '1993-10-02', 'GEO', 'Senior', 50, '+109', 150, '2018-11-10', 'Ashgabat'),
(335, 'Welt', 'w', 'Clean and Jerk', 2, '127', 'SRISURAT Sukanya', '1995-05-03', 'THA', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat'),
(336, 'Welt', 'w', 'Total', 3, '232', 'SRISURAT Sukanya', '1995-05-03', 'THA', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat'),
(337, 'Welt', 'w', 'Total', 3, '157', 'ECHANDIA ZARATE Katherin Oriana', '2001-08-14', 'VEN', 'Youth', 30, '-45', 40, '2018-11-02', 'Ashgabat'),
(338, 'Welt', 'w', 'Snatch', 1, '92', 'GHOFRANE Belkhir', '2001-08-11', 'TUN', 'Youth', 30, '-59', 70, '2018-12-10', 'Cairo'),
(339, 'Welt', 'w', 'Total', 3, '206', 'GHOFRANE Belkhir', '2001-08-11', 'TUN', 'Youth', 30, '-59', 70, '2018-12-10', 'Cairo'),
(340, 'Welt', 'w', 'Clean and Jerk', 2, '114', 'GHOFRANE Belkhir', '2001-08-11', 'TUN', 'Youth', 30, '-59', 70, '2018-12-10', 'Cairo'),
(341, 'Welt', 'w', 'Snatch', 1, '103', 'HUANG Ting', '1999-01-16', 'CHN', 'Junior', 40, '-64', 80, '2018-12-21', 'Doha'),
(342, 'Welt', 'w', 'Snatch', 1, '105', 'HUANG Ting', '1999-01-16', 'CHN', 'Junior', 40, '-64', 80, '2018-12-21', 'Doha'),
(343, 'Welt', 'w', 'Clean and Jerk', 2, '127', 'HUANG Ting', '1999-01-16', 'CHN', 'Junior', 40, '-64', 80, '2018-12-21', 'Doha'),
(344, 'Welt', 'w', 'Total', 3, '226', 'HUANG Ting', '1999-01-16', 'CHN', 'Junior', 40, '-64', 80, '2018-12-21', 'Doha'),
(345, 'Welt', 'w', 'Total', 3, '232', 'HUANG Ting', '1999-01-16', 'CHN', 'Junior', 40, '-64', 80, '2018-12-21', 'Doha'),
(346, 'Welt', 'm', 'Snatch', 1, '142', 'ERGASHEV Adkhamjon', '1999-03-12', 'UZB', 'Junior', 40, '-67', 80, '2018-12-20', 'Doha'),
(347, 'Welt', 'm', 'Total', 3, '310', 'ERGASHEV Adkhamjon', '1999-03-12', 'UZB', 'Junior', 40, '-67', 80, '2018-12-20', 'Doha'),
(348, 'Welt', 'm', 'Clean and Jerk', 2, '219', 'ELBAKH Fares Ibrahim E. H.', '1998-06-04', 'QAT', 'Junior', 40, '-96', 120, '2018-12-21', 'Doha');
INSERT INTO `world_records` (`id`, `scope`, `sex`, `type`, `lift`, `result`, `name`, `born`, `nation`, `age_category`, `age_id`, `bw_category`, `bw_id`, `date`, `place`) VALUES
(349, 'Welt', 'm', 'Clean and Jerk', 2, '225', 'ELBAKH Fares Ibrahim E. H.', '1998-06-04', 'QAT', 'Junior', 40, '-96', 120, '2018-12-21', 'Doha'),
(350, 'Welt', 'm', 'Total', 3, '391', 'ELBAKH Fares Ibrahim E. H.', '1998-06-04', 'QAT', 'Junior', 40, '-96', 120, '2018-12-21', 'Doha'),
(351, 'Welt', 'm', 'Total', 3, '397', 'ELBAKH Fares Ibrahim E. H.', '1998-06-04', 'QAT', 'Junior', 40, '-96', 120, '2018-12-21', 'Doha'),
(352, 'Welt', 'm', 'snatch', 1, '131', 'JEREMY Lalrinnunga', '2002-10-26', 'IND', 'Youth', 30, '-67', 80, '2019-02-08', 'Chiang Mai'),
(354, 'Welt', 'm', 'Total', 3, '288', 'JEREMY Lalrinnunga', '2002-10-26', 'IND', 'Youth', 30, '-67', 80, '2019-02-08', 'Chiang Mai'),
(355, 'Welt', 'w', 'snatch', 1, '104', 'LUO Xiaomin', '1999-11-14', 'CHN', 'Junior', 40, '-59', 70, '2019-02-08', 'Chiang Mai'),
(356, 'Welt', 'w', 'Clean and Jerk', 2, '130', 'HUANG Ting', '1999-01-16', 'CHN', 'Junior', 40, '-64', 80, '2019-02-08', 'Chiang Mai'),
(357, 'Welt', 'w', 'Clean and Jerk', 2, '135', 'HUANG Ting', '1999-01-16', 'CHN', 'Junior', 40, '-64', 80, '2019-02-08', 'Chiang Mai'),
(358, 'Welt', 'w', 'Total', 3, '235', 'HUANG Ting', '1999-01-16', 'CHN', 'Junior', 40, '-64', 80, '2019-02-08', 'Chiang Mai'),
(359, 'Welt', 'w', 'Total', 3, '240', 'HUANG Ting', '1999-01-16', 'CHN', 'Junior', 40, '-64', 80, '2019-02-08', 'Chiang Mai'),
(360, 'Welt', 'w', 'Total', 3, '210', 'HOU Zhihui', '1997-03-18', 'CHN', 'Senior', 50, '-49', 50, '2019-02-23', 'Fuzhou'),
(361, 'Welt', 'w', 'Snatch', 1, '94', 'HOU Zhihui', '1997-03-18', 'CHN', 'Senior', 50, '-49', 50, '2019-02-23', 'Fuzhou'),
(362, 'Welt', 'w', 'Clean and Jerk', 2, '136', 'CHEN Guiming', '1994-01-03', 'CHN', 'Senior', 50, '-59', 70, '2019-02-24', 'Fuzhou'),
(363, 'Welt', 'w', 'Clean and Jerk', 2, '141', 'DENG Wei', '1993-02-14', 'CHN', 'Senior', 50, '-64', 80, '2019-02-25', 'Fuzhou'),
(364, 'Welt', 'w', 'Snatch', 1, '113', 'DENG Wei', '1993-02-14', 'CHN', 'Senior', 50, '-64', 80, '2019-02-25', 'Fuzhou'),
(366, 'Welt', 'w', 'Total', 3, '254', 'DENG Wei', '1993-02-14', 'CHN', 'Senior', 50, '-64', 80, '2019-02-25', 'Fuzhou'),
(367, 'Welt', 'w', 'Total', 3, '274', 'ZHANG Wangli', '1996-05-27', 'CHN', 'Senior', 50, '-76', 100, '2019-02-26', 'Fuzhou'),
(368, 'Welt', 'w', 'Clean and Jerk', 2, '156', 'ZHANG Wangli', '1996-05-27', 'CHN', 'Senior', 50, '-76', 100, '2019-02-26', 'Fuzhou'),
(369, 'Welt', 'w', 'Total', 3, '317', 'LI Wenwen', '2000-03-05', 'CHN', 'Junior', 40, '+87', 130, '2019-02-27', 'Fuzhou'),
(370, 'Welt', 'w', 'Total', 3, '324', 'LI Wenwen', '2000-03-05', 'CHN', 'Junior', 40, '+87', 130, '2019-02-27', 'Fuzhou'),
(371, 'Welt', 'w', 'Clean and Jerk', 2, '175', 'LI Wenwen', '2000-03-05', 'CHN', 'Junior', 40, '+87', 130, '2019-02-27', 'Fuzhou'),
(372, 'Welt', 'w', 'Clean and Jerk', 2, '182', 'LI Wenwen', '2000-03-05', 'CHN', 'Junior', 40, '+87', 130, '2019-02-27', 'Fuzhou'),
(373, 'Welt', 'w', 'Snatch', 1, '135', 'LI Wenwen', '2000-03-05', 'CHN', 'Junior', 40, '+87', 130, '2019-02-27', 'Fuzhou'),
(374, 'Welt', 'w', 'Snatch', 1, '142', 'LI Wenwen', '2000-03-05', 'CHN', 'Junior', 40, '+87', 130, '2019-02-27', 'Fuzhou'),
(375, 'Welt', 'm', 'Total', 3, '375', 'LI Dayin', '1998-02-12', 'CHN', 'Senior', 50, '-81', 100, '2019-02-24', 'Fuzhou'),
(376, 'Welt', 'w', 'Clean and Jerk', 2, '122', 'FAYZULLAEVA Kumushkhon', '2002-01-20', 'UZB', 'Youth', 30, '-64', 80, '2019-03-12', 'Las Vegas'),
(377, 'Welt', 'w', 'Total', 3, '215', 'FAYZULLAEVA Kumushkhon', '2002-01-20', 'UZB', 'Youth', 30, '-64', 80, '2019-03-12', 'Las Vegas'),
(378, 'Welt', 'w', 'Clean and Jerk', 2, '129', 'NARIN Dilara ', '2002-03-17', 'TUR', 'Youth', 30, '-76', 100, '2019-03-13', 'Las Vegas'),
(379, 'Welt', 'm', 'Clean and Jerk', 2, '117', 'DO Tu Tung', '2004-01-10', 'VIE', 'Youth', 30, '-49', 50, '2019-03-08', 'Las Vegas'),
(380, 'Welt', 'm', 'Snatch', 1, '95', 'DO Tu Tung', '2004-01-10', 'VIE', 'Youth', 30, '-49', 50, '2019-03-08', 'Las Vegas'),
(381, 'Welt', 'm', 'Clean and Jerk', 2, '125', 'DO Tu Tung', '2004-01-10', 'VIE', 'Youth', 30, '-49', 50, '2019-03-08', 'Las Vegas'),
(382, 'Welt', 'm', 'Total', 3, '212', 'DO Tu Tung', '2004-01-10', 'VIE', 'Youth', 30, '-49', 50, '2019-03-08', 'Las Vegas'),
(383, 'Welt', 'm', 'Total', 3, '220', 'DO Tu Tung', '2004-01-10', 'VIE', 'Youth', 30, '-49', 50, '2019-03-08', 'Las Vegas'),
(384, 'Welt', 'm', 'Clean and Jerk', 2, '161', 'TAISUYEV Saikhan', '2002-05-21', 'KAZ', 'Youth', 30, '-67', 80, '2019-03-08', 'Las Vegas'),
(385, 'Welt', 'm', 'Clean & Jerk', 2, '166', 'OM Yun Chol', '1991-11-18', 'PRK', 'Senior', 50, '-55', 60, '2019-09-18', 'Pattaya - lHA'),
(386, 'Welt', 'm', 'Total', 3, '294', 'OM Yun Chol', '1991-11-18', 'PRK', 'Senior', 50, '-55', 60, '2018-11-01', 'Pattaya - lHA'),
(387, 'Welt', 'm', 'Snatch', 1, '145', 'LI Fabin', '1993-01-15', 'CHN', 'Senior', 50, '-61', 70, '2019-09-19', 'Pattaya - lHA'),
(388, 'Welt', 'm', 'Clean & Jerk', 2, '175', 'LI Fabin', '1993-01-15', 'CHN', 'Senior', 50, '-61', 70, '2022-12-07', 'Bogota- COL'),
(389, 'Welt', 'm', 'Total', 3, '318', 'LI Fabin', '1993-01-15', 'CHN', 'Senior', 50, '-61', 70, '2019-09-19', 'Pattaya - lHA'),
(390, 'Welt', 'm', 'Snatch', 1, '155', 'HUANG Minhao', '1992-08-21', 'CHN', 'Senior', 50, '-67', 80, '2019-07-06', 'Tokyo - JPN'),
(391, 'Welt', 'm', 'Clean & Jerk', 2, '188', 'PAK Jong Ju', '1997-03-14', 'PRK', 'Senior', 50, '-67', 80, '2019-09-20', 'Pattaya - lHA'),
(392, 'Welt', 'm', 'Total', 3, '339', 'CHEN Lijun', '1993-02-08', 'CHN', 'Senior', 50, '-67', 80, '2019-04-21', 'Ningbo- CHN'),
(393, 'Welt', 'm', 'Snatch', 1, '169', 'SHI Zhiyong', '1993-10-10', 'CHN', 'Senior', 50, '-73', 90, '2021-04-20', 'Tashkent - UZB'),
(394, 'Welt', 'm', 'Clean & Jerk', 2, '200', 'ABDULLAH Rahmat Erwin', '2000-10-13', 'INA', 'Senior', 50, '-73', 90, '2022-12-09', 'Bogota - COL'),
(395, 'Welt', 'm', 'Total', 3, '364', 'SHI Zhiyong', '1993-10-10', 'CHN', 'Senior', 50, '-73', 90, '2021-07-28', 'Tokyo - JPN'),
(396, 'Welt', 'm', 'Snatch', 1, '175', 'LI Dayin', '1998-12-02', 'CHN', 'Senior', 50, '-81', 100, '2021-04-21', 'Tashkent - UZB'),
(397, 'Welt', 'm', 'Clean & Jerk', 2, '208', 'NASAR Karlas May Hasan', '2004-04-12', 'BUL', 'Senior', 50, '-81', 100, '2021-12-12', 'Tashkent - UZB'),
(398, 'Welt', 'm', 'Total', 3, '378', 'LYU Xiaojun', '1984-07-27', 'CHN', 'Senior', 50, '-81', 100, '2019-09-22', 'Pattaya - lHA'),
(399, 'Welt', 'm', 'Snatch', 1, '180', 'LI Dayin', '1998-12-02', 'CHN', 'Senior', 50, '-89', 110, '2023-05-10', 'Jinju - KOR'),
(400, 'Welt', 'm', 'Clean & Jerk', 2, '222', 'llAN Tao', '1994-04-08', 'CHN', 'Senior', 50, '-89', 110, '2023-05-10', 'Jinju - KOR'),
(401, 'Welt', 'm', 'Total', 3, '396', 'LI Dayin', '1998-12-02', 'CHN', 'Senior', 50, '-89', 110, '2023-05-10', 'Jinju - KOR'),
(402, 'Welt', 'm', 'Snatch', 1, '187', 'PAREDES MONTANO L.', '1996-03-05', 'COL', 'Senior', 50, '-96', 120, '2021-12-14', 'Tashkent - UZB'),
(403, 'Welt', 'm', 'Clean & Jerk', 2, '231', 'llAN Tao', '1994-04-08', 'CHN', 'Senior', 50, '-96', 120, '2019-07-07', 'Tokyo - JPN'),
(404, 'Welt', 'm', 'Snatch', 1, '200', 'YANG Zhe', '1991-07-14', 'CHN', 'Senior', 50, '-109', 140, '2021-04-24', 'Tashkent - UZB'),
(405, 'Welt', 'm', 'Clean & Jerk', 2, '241', 'NURUDINOV Ruslan', '1991-09-24', 'UZB', 'Senior', 50, '-109', 140, '2021-04-24', 'Tashkent - UZB'),
(406, 'Welt', 'm', 'Snatch', 1, '225', 'TALAKHADZE  Lasha', '1993-10-02', 'GEO', 'Senior', 50, '+109', 150, '2021-12-17', 'Tashkent - UZB'),
(407, 'Welt', 'm', 'Clean & Jerk', 2, '267', 'TALAKHADZE  Lasha', '1993-10-02', 'GEO', 'Senior', 50, '+109', 150, '2021-12-17', 'Tashkent - UZB'),
(408, 'Welt', 'm', 'Total', 3, '492', 'TALAKHADZE  Lasha', '1993-10-02', 'GEO', 'Senior', 50, '+109', 150, '2021-12-17', 'Tashkent - UZB'),
(409, 'Welt', 'w', 'Snatch', 1, '148', 'LI Wenwen', '2000-05-05', 'CHN', 'Senior', 50, '+87', 130, '2021-04-25', 'Tashkent - UZB'),
(410, 'Welt', 'w', 'Snatch', 1, '96', 'HOU Zhihui', '1997-03-18', 'CHN', 'Senior', 50, '-49', 50, '2021-04-17', 'Tashkent - UZB'),
(411, 'Welt', 'w', 'Total', 3, '213', 'HOU Zhihui', '1997-03-18', 'CHN', 'Senior', 50, '-49', 50, '2021-04-17', 'Tashkent - UZB'),
(412, 'Welt', 'w', 'Snatch', 1, '102', 'LI Yajun', '1993-04-27', 'CHN', 'Senior', 50, '-55', 60, '2018-11-03', 'Ashgabat - TKM'),
(413, 'Welt', 'w', 'Clean & Jerk', 2, '129', 'LIAO Qiuyun', '1995-07-13', 'CHN', 'Senior', 50, '-55', 60, '2019-09-20', 'Pattaya - THA'),
(414, 'Welt', 'w', 'Snatch', 1, '110', 'KUO Hsing-Chun', '1993-11-26', 'TPE', 'Senior', 50, '-59', 70, '2021-04-19', 'Tashkent - UZB'),
(415, 'Welt', 'w', 'Clean & Jerk', 2, '140', 'KUO Hsing-Chun', '1993-11-26', 'TPE', 'Senior', 50, '-59', 70, '2021-04-19', 'Pattaya - THA'),
(416, 'Welt', 'w', 'Total', 3, '247', 'KUO Hsing-Chun', '1993-11-26', 'TPE', 'Senior', 50, '-59', 70, '2021-04-19', 'Tashkent - UZB'),
(417, 'Welt', 'w', 'Snatch', 1, '117', 'DENG Wei', '1993-02-14', 'CHN', 'Senior', 50, '-64', 80, '2019-12-11', 'Tianjin - CHN'),
(418, 'Welt', 'w', 'Clean & Jerk', 2, '145', 'DENG Wei', '1993-02-14', 'CHN', 'Senior', 50, '-64', 80, '2019-02-25', 'Pattaya - THA'),
(419, 'Welt', 'w', 'Total', 3, '261', 'DENG Wei', '1993-02-14', 'CHN', 'Senior', 50, '-64', 80, '2019-02-25', 'Pattaya - THA'),
(420, 'Welt', 'w', 'Snatch', 1, '121', 'PALACIOS  DAJOMES Angie', '2000-12-09', 'ECU', 'Senior', 50, '-71', 90, '2023-06-14', 'Havana- CUB'),
(421, 'Welt', 'w', 'Total', 3, '268', 'LIAO Guifang', '2001-05-10', 'CHN', 'Senior', 50, '-71', 90, '2023-05-09', 'Jinju - KOR'),
(422, 'Welt', 'w', 'Snatch', 1, '124', 'RIM Jong Sim', '1993-02-05', 'PRK', 'Senior', 50, '-76', 100, '2019-09-24', 'Pattaya - THA'),
(423, 'Welt', 'w', 'Total', 3, '278', 'RIM Jong Sim', '1993-02-05', 'PRK', 'Senior', 50, '-76', 100, '2019-04-26', 'Ningbo - CHN'),
(424, 'Welt', 'w', 'Clean & Jerk', 2, '187', 'LI Wenwen', '2000-05-05', 'CHN', 'Senior', 50, '+87', 130, '2021-04-25', 'Tashkent - UZB'),
(425, 'Welt', 'w', 'Total', 3, '335', 'LI Wenwen', '2000-05-05', 'CHN', 'Senior', 50, '+87', 130, '2021-04-25', 'Tashkent - UZB');
"
        End Function

        Private Function CreateIndizes() As String
            Return "
ALTER TABLE `altersklassen`
  ADD PRIMARY KEY (`idAltersklasse`,`International`);

ALTER TABLE `alterskoeffizient`
  ADD PRIMARY KEY (`idAlter`);

ALTER TABLE `athleten`
  ADD PRIMARY KEY (`idTeilnehmer`);

ALTER TABLE `athletik`
  ADD PRIMARY KEY (`Wettkampf`,`Teilnehmer`,`Disziplin`,`Durchgang`);

ALTER TABLE `athletik_defaults`
  ADD PRIMARY KEY (`Disziplin`,`Bezeichnung`,`Geschlecht`);

ALTER TABLE `athletik_disziplinen`
  ADD PRIMARY KEY (`idDisziplin`);

ALTER TABLE `athletik_results`
  ADD PRIMARY KEY (`Wettkampf`,`Teilnehmer`,`Disziplin`);

ALTER TABLE `faktoren`
  ADD PRIMARY KEY (`sex`);

ALTER TABLE `gewichtsklassen`
  ADD PRIMARY KEY (`International`,`idAK`,`Sex`,`idGK`);

ALTER TABLE `gruppen`
  ADD PRIMARY KEY (`Wettkampf`,`Gruppe`) USING BTREE;

ALTER TABLE `gruppen_kampfrichter`
  ADD PRIMARY KEY (`Wettkampf`,`Gruppe`);

ALTER TABLE `hantel`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `laenderwertung`
  ADD PRIMARY KEY (`Platz`);

ALTER TABLE `mannschaftswertung`
  ADD PRIMARY KEY (`Platz`);

ALTER TABLE `masters_norm`
  ADD PRIMARY KEY (`Sex`,`GK`);

ALTER TABLE `meldung`
  ADD PRIMARY KEY (`Wettkampf`,`Teilnehmer`),
  ADD KEY `Athleten` (`Teilnehmer`);

ALTER TABLE `modus`
  ADD PRIMARY KEY (`idModus`);

ALTER TABLE `modus_formeln`
  ADD PRIMARY KEY (`Modus`,`AK`,`Sex`);

ALTER TABLE `modus_gewichtsgruppen`
  ADD PRIMARY KEY (`Geschlecht`,`Gruppe`);

ALTER TABLE `modus_mannschaft`
  ADD PRIMARY KEY (`Modus`,`idMannschaft`);

ALTER TABLE `notizen`
  ADD PRIMARY KEY (`Heber`);

ALTER TABLE `region`
  ADD PRIMARY KEY (`region_id`);

ALTER TABLE `reissen`
  ADD PRIMARY KEY (`Wettkampf`,`Teilnehmer`,`Versuch`);

ALTER TABLE `relativabzug`
  ADD PRIMARY KEY (`Gewicht`);

ALTER TABLE `results`
  ADD PRIMARY KEY (`Wettkampf`,`Teilnehmer`);

ALTER TABLE `staaten`
  ADD PRIMARY KEY (`state_id`),
  ADD UNIQUE KEY `state` (`state_name`(15)) USING BTREE,
  ADD KEY `iso3` (`state_iso3`);

ALTER TABLE `stossen`
  ADD PRIMARY KEY (`Wettkampf`,`Teilnehmer`,`Versuch`);

ALTER TABLE `teams`
  ADD PRIMARY KEY (`idTeam`,`idVerein`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`UserId`,`Bohle`);

ALTER TABLE `verein`
  ADD PRIMARY KEY (`idVerein`),
  ADD KEY `idVerein_Name` (`Vereinsname`),
  ADD KEY `idVerein_Bundesland` (`Region`,`Vereinsname`);

ALTER TABLE `wertung`
  ADD PRIMARY KEY (`idWertung`);

ALTER TABLE `wertung_bezeichnung`
  ADD PRIMARY KEY (`Platzierung`,`International`);

ALTER TABLE `wettkampf`
  ADD PRIMARY KEY (`Wettkampf`);

ALTER TABLE `wettkampf_altersgruppen`
  ADD PRIMARY KEY (`Wettkampf`,`Reihenfolge`);

ALTER TABLE `wettkampf_altersklassen`
  ADD PRIMARY KEY (`Wettkampf`,`Geschlecht`,`idAK`);

ALTER TABLE `wettkampf_athletik`
  ADD PRIMARY KEY (`Wettkampf`,`Disziplin`);

ALTER TABLE `wettkampf_athletik_defaults`
  ADD PRIMARY KEY (`Wettkampf`,`Disziplin`,`Bezeichnung`,`Geschlecht`);

ALTER TABLE `wettkampf_bezeichnung`
  ADD PRIMARY KEY (`Wettkampf`);

ALTER TABLE `wettkampf_comment` 
  ADD PRIMARY KEY (`Wettkampf`);

ALTER TABLE `wettkampf_details`
  ADD PRIMARY KEY (`Wettkampf`);

ALTER TABLE `wettkampf_gewichtsgruppen`
  ADD PRIMARY KEY (`Wettkampf`,`Geschlecht`,`Gruppe`);

ALTER TABLE `wettkampf_gewichtsteiler`
  ADD PRIMARY KEY (`Wettkampf`,`Gruppe`,`GG`);

ALTER TABLE `wettkampf_mannschaft`
  ADD PRIMARY KEY (`Wettkampf`,`idMannschaft`);

ALTER TABLE `wettkampf_meldefrist`
  ADD PRIMARY KEY (`Wettkampf`);

ALTER TABLE `wettkampf_multi`
  ADD PRIMARY KEY (`Wettkampf`);

ALTER TABLE `wettkampf_restriction`
  ADD PRIMARY KEY (`Wettkampf`,`Durchgang`,`Altersklasse`);

ALTER TABLE `wettkampf_startgeld`
  ADD PRIMARY KEY (`Wettkampf`,`AK`);

ALTER TABLE `wettkampf_teams`
  ADD PRIMARY KEY (`Wettkampf`,`Team`);

ALTER TABLE `wettkampf_wertung`
  ADD PRIMARY KEY (`Wettkampf`,`idAK`);

ALTER TABLE `wettkampf_zeitplan`
  ADD PRIMARY KEY (`Wettkampf`,`Datum`,`Beginn`,`Bezeichnung`);

ALTER TABLE `world_records`
  ADD PRIMARY KEY (`id`,`scope`,`sex`,`type`,`age_id`,`bw_id`);
"
        End Function

        Private Function CreateAutoIncrement() As String
            Return "
ALTER TABLE `athleten`
  MODIFY `idTeilnehmer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200076;

ALTER TABLE `laenderwertung`
  MODIFY `Platz` int(3) NOT NULL AUTO_INCREMENT;

ALTER TABLE `staaten`
  MODIFY `state_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;
"
        End Function

        Private Function Insert_Athleten() As String
            Return "        
INSERT INTO `athleten` (`idTeilnehmer`, `GUID`, `Startbuch_Nummer`, `Titel`, `Nachname`, `Vorname`, `Verein`, `Verein_GUID`, `ESR`, `MSR`, `Jahrgang`, `Geburtstag`, `Geschlecht`, `Staat`, `Lizenz`) VALUES
(106, NULL, NULL, '', 'Steinbach', 'Cedric', 14007, NULL, 14007, 14007, '2007', '2007-01-01', 'm', 42, 1),
(107, '9C0C313D0BCB4910A239DD144D384052', NULL, '', 'Kunz', 'Elias', 14007, '7277ACBBEA45490E9377AAC5E3716CD2', 14007, 14007, '2008', '2008-01-01', 'm', 42, 1),
(202, NULL, NULL, '', 'Heinz', 'Tim', 14005, NULL, 14005, 14005, '2007', '2007-01-01', 'm', 42, 1),
(203, NULL, NULL, '', 'Hentschel', 'Maximilian', 14005, NULL, 14005, 14005, '2007', '2007-01-01', 'm', 42, 1),
(204, NULL, NULL, '', 'Heinrich', 'Marten', 14005, NULL, 14005, 14005, '2007', '2007-01-01', 'm', 42, 1),
(205, NULL, NULL, '', 'Hiller', 'Jean-Pierre', 14005, NULL, 14005, 14005, '2007', '2007-01-01', 'm', 42, 1),
(206, NULL, NULL, '', 'Kröher', 'Nils', 14005, NULL, 14005, 14005, '2007', '2007-01-01', 'm', 42, 1),
(208, NULL, NULL, '', 'Göttlich', 'Marla', 14005, NULL, 14005, 14005, '2008', '2008-01-01', 'w', 42, 1),
(210, NULL, NULL, '', 'Neumann', 'Nicolas', 14005, NULL, 14005, 14005, '2008', '2008-01-01', 'm', 42, 1),
(211, NULL, NULL, '', 'Fobe', 'Kurt', 14005, NULL, 14005, 14005, '2008', '2008-01-01', 'm', 42, 1),
(212, NULL, NULL, '', 'Göttlich', 'Smilla', 14005, NULL, 14005, 14005, '2011', '2011-01-01', 'w', 42, 1),
(244, 'B3F46A35FDF14859A24B1D9DA04CF5BD', NULL, '', 'Herberg', 'Harald', 14009, 'E332586D9B0344D4BFCEF66152F334C0', 14009, 14009, '1954', '1954-11-01', 'm', 42, 1),
(301, 'F12BE327A2A4436FAAD8F9F01C6168C5', NULL, '', 'Altmann', 'Robin', 14010, 'DE70F950C90D4B86AA3DCCFFCD6CEBDA', 14010, 14010, '2006', '2006-01-01', 'm', 42, 1),
(302, NULL, NULL, '', 'Glaubitz', 'Louis', 14010, NULL, 14010, 14010, '2006', '2006-01-01', 'm', 42, 1),
(303, NULL, NULL, '', 'Kubelka', 'Elisa', 14010, NULL, 14010, 14010, '2007', '2007-01-01', 'w', 42, 1),
(304, NULL, NULL, '', 'Hofer', 'Robin', 14010, NULL, 14010, 14010, '2007', '2007-01-01', 'm', 42, 1),
(305, NULL, NULL, '', 'Simmank', 'Lara', 14010, NULL, 14010, 14010, '2007', '2007-01-01', 'w', 42, 1),
(309, NULL, NULL, '', 'Trenkler', 'Lilly', 14010, NULL, 14010, 14010, '2009', '2009-01-01', 'w', 42, 1),
(310, NULL, NULL, '', 'Schulz', 'Marie', 14010, NULL, 14010, 14010, '2011', '2011-01-01', 'w', 42, 1),
(363, NULL, NULL, '', 'Müller', 'Michael', 15009, NULL, 15009, 15009, '1987', '1987-01-01', 'm', 42, 1),
(402, NULL, NULL, '', 'Heidemann', 'Felix', 14008, NULL, 14008, 14008, '2006', '2006-01-01', 'm', 42, 1),
(403, NULL, NULL, '', 'Barth', 'Jason', 14008, NULL, 14008, 14008, '2006', '2006-01-01', 'm', 42, 1),
(405, NULL, NULL, '', 'Wael', 'Alkalabi', 14008, NULL, 14008, 14008, '2006', '2006-01-01', 'm', 42, 1),
(407, NULL, NULL, '', 'Uday', 'Abdulmajeed', 14008, NULL, 14008, 14008, '2008', '2008-01-01', 'm', 42, 1),
(408, 'F6521FDF4E0947C29E1BAD8C37989ED2', NULL, '', 'Al-Hrishat', 'Al Hassan', 14008, '0AD370587D694E54A0974C741E40A88E', 14008, 14008, '2010', '2010-01-01', 'm', 42, 1),
(409, '2F6B8435CE1B4A94AD6711AB77F3A0EC', NULL, '', 'Pomsel', 'Jan', 14008, '03F32A5B5E43499AA213C8CD388D1726', 14008, 14008, '2011', '2011-01-01', 'm', 42, 1),
(502, '6D22449535BB4413ADAB73009858A6A2', NULL, '', 'Winkler', 'Lukas', 14016, '7277ACBBEA45490E9377AAC5E3716CD2', 14016, 14016, '2007', '2007-01-01', 'm', 42, 1),
(503, NULL, NULL, '', 'Blagnis', 'Sascha', 14016, NULL, 14016, 14016, '2007', '2007-01-01', 'm', 42, 1),
(504, NULL, NULL, '', 'Albrecht', 'Nino', 14016, NULL, 14016, 14016, '2008', '2008-01-01', 'm', 42, 1),
(506, NULL, NULL, '', 'Fuhrmann', 'Fabian', 14016, NULL, 14016, 14016, '2008', '2008-01-01', 'm', 42, 1),
(507, NULL, NULL, '', 'Seifert', 'Eric', 14016, NULL, 14016, 14016, '2009', '2009-01-01', 'm', 42, 1),
(508, NULL, NULL, '', 'Nurz', 'Nico', 14016, NULL, 14016, 14016, '2009', '2009-01-01', 'm', 42, 1),
(509, NULL, NULL, '', 'Schlosser', 'Amy', 14016, NULL, 14016, 14016, '2009', '2009-01-01', 'w', 42, 1),
(510, NULL, NULL, '', 'Helmbold', 'Carolina', 14016, NULL, 14016, 14016, '2009', '2009-01-01', 'w', 42, 1),
(511, NULL, NULL, '', 'Fedarau', 'Hannah', 14016, NULL, 14016, 14016, '2010', '2010-01-01', 'w', 42, 1),
(512, NULL, NULL, '', 'Wolf', 'Luis', 14016, NULL, 14016, 14016, '2010', '2010-01-01', 'm', 42, 1),
(513, NULL, NULL, '', 'Salzmesser', 'Lilly', 14016, NULL, 14016, 14016, '2010', '2010-01-01', 'w', 42, 1),
(514, NULL, NULL, '', 'Eschebach', 'Josephin', 14016, NULL, 14016, 14016, '2011', '2011-01-01', 'w', 42, 1),
(602, NULL, NULL, '', 'Raulf', 'Luis', 14004, NULL, 14004, 14004, '2006', '2006-01-01', 'm', 42, 1),
(604, NULL, NULL, '', 'Uhlig', 'Dennis', 14004, NULL, 14004, 14004, '2006', '2006-01-01', 'm', 42, 1),
(605, NULL, NULL, '', 'Jogwick', 'Erwin', 14004, NULL, 14004, 14004, '2006', '2006-01-01', 'm', 42, 1),
(610, NULL, NULL, '', 'Schumann', 'Ashley', 14004, NULL, 14004, 14004, '2010', '2010-01-01', 'w', 42, 1),
(611, 'F4650AAE587340A798C932685FDD0CDA', NULL, '', 'Krause', 'Clara', 14004, '5FDB03E18CAF4F69B7F7A74C7D1AFCEA', 14004, 14004, '2011', '2011-01-01', 'w', 42, 1),
(701, NULL, NULL, '', 'Müller', 'Tim', 14038, NULL, 14038, 14038, '2007', '2007-01-01', 'm', 42, 1),
(702, NULL, NULL, '', 'Kazmirek', 'Erik', 14038, NULL, 14038, 14038, '2008', '2008-01-01', 'm', 42, 1),
(703, NULL, NULL, '', 'Westphal', 'Karl', 14038, NULL, 14038, 14038, '2008', '2008-01-01', 'm', 42, 1),
(704, NULL, NULL, '', 'Matlab', 'MuhiEddin', 14038, NULL, 14038, 14038, '2009', '2009-01-01', 'm', 42, 1),
(705, NULL, NULL, '', 'Helbig', 'Aaron', 14038, NULL, 14038, 14038, '2011', '2011-01-01', 'm', 42, 1),
(706, NULL, NULL, '', 'Kazmirek', 'Tristan', 14038, NULL, 14038, 14038, '2012', '2012-01-01', 'm', 42, 1),
(804, NULL, NULL, '', 'Valodze', 'Elina', 14009, NULL, 14009, 14009, '2006', '2006-01-01', 'w', 42, 1),
(806, NULL, NULL, '', 'Hammer', 'Lukas', 14009, NULL, 14009, 14009, '2008', '2008-01-01', 'm', 42, 1),
(810, NULL, NULL, '', 'Proft', 'Jasmin', 14009, NULL, 14009, 14009, '2010', '2010-01-01', 'w', 42, 1),
(812, NULL, NULL, '', 'Neundorf', 'Theodor', 14009, NULL, 14009, 14009, '2011', '2011-01-01', 'm', 42, 1),
(903, NULL, NULL, '', 'Thieme', 'Hanna-Christin', 14011, NULL, 14011, 14011, '2006', '2006-01-01', 'w', 42, 1),
(904, NULL, NULL, '', 'Berge', 'Joyce', 14011, NULL, 14011, 14011, '2007', '2007-01-01', 'w', 42, 1),
(906, NULL, NULL, '', 'Spetter', 'Dominik', 14011, NULL, 14011, 14011, '2012', '2012-01-01', 'm', 42, 1),
(1237, 'BB41B1BE14D645CCB29C51C9EED850F6', NULL, '', 'Neufeld', 'Jakob', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '1983', '1983-01-01', 'm', 42, 1),
(1430, '6EBFA78ED74D4E628EF0E5C779D11594', NULL, '', 'Rief', 'Karlheinz', 9001, 'E38DA50A843741E491CA9EAB34975D61', 9001, 9001, '1958', '1958-07-21', 'm', 42, 1),
(2475, '573D35BA40164D27983B2A8091A913E9', NULL, '', 'Rüdiger', 'René', 3009, '525195E25E9148CC9071D802318844FB', 3009, 3009, '1967', '1967-01-01', 'm', 42, 1),
(3350, NULL, NULL, '', 'Wetzel', 'Hendrik', 0, NULL, 0, 14007, '1982', '1982-01-01', 'm', 42, 1),
(3757, 'F048A2F2C41B45A7A50C11FDD25DB7F7', NULL, '', 'Geier', 'Thomas', 16011, '394F189769D446669BB54390A45C6334', 16011, 16011, '1982', '1982-01-01', 'm', 42, 1),
(4141, '987A49491A854834AC4B2647C300224A', NULL, '', 'Burkhardt', 'Steve', 14005, '37668537FC6147F59E03477C6788809F', 14005, 14005, '1985', '1985-01-01', 'm', 42, 1),
(4555, '4020534BFF9E439E8F389702E71DAC70', NULL, '', 'Spieß', 'Jürgen', 14007, '728BCFEBAEE94E56A0AA57156D1AE0B1', 14007, 9010, '1984', '1984-01-01', 'm', 42, 1),
(5478, NULL, NULL, '', 'Winter', 'Andre', 14007, NULL, 14007, 9010, '1985', '1985-01-01', 'm', 42, 1),
(6141, NULL, NULL, '', 'Schiweck', 'Sebastian', 17011, NULL, 17011, 17011, '1985', '1985-01-01', 'm', 42, 1),
(6269, '1627AAE853CC44328F47323CAB6D0853', NULL, '', 'Bellmann', 'Anja', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 147, '1977', '1977-01-01', 'w', 42, 1),
(6998, 'F750D8E806B74BEB93824D6C0D13B70F', NULL, '', 'Schweizer', 'Kevin', 2003, 'B515D63CC1074ECB9D5C0D3962A05680', 2003, 2003, '1986', '1986-01-01', 'm', 42, 1),
(7788, 'E9C55B7ADF0F49298AE293EEE600E74C', NULL, '', 'Krüger', 'Nico', 15009, 'BE0FB064DFD34F2AB48082AA9C14CF44', 15009, 15009, '1987', '1987-01-01', 'm', 42, 1),
(7984, 'BE7741268CB640ABB029E9EB9BE404E2', NULL, '', 'Bug', 'Sabrina', 9001, 'E38DA50A843741E491CA9EAB34975D61', 9001, 9001, '1988', '1988-04-13', 'w', 42, 1),
(8369, '234FEBC55F71472AB1AC40E362976EEA', NULL, '', 'Wenz', 'Christian', 9001, 'E38DA50A843741E491CA9EAB34975D61', 9001, 9001, '1985', '1985-07-04', 'm', 42, 1),
(8503, NULL, NULL, '', 'Kusterer', 'Sabine', 2003, NULL, 2003, 2003, '1991', '1991-01-01', 'w', 42, 1),
(8808, NULL, NULL, '', 'Oswald', 'Robert', 17021, NULL, 17021, 17021, '1988', '1988-01-01', 'm', 42, 1),
(8923, '1938956AA0704B8C86A285DD90AF6A2E', NULL, '', 'Koch', 'Mario', 16011, '394F189769D446669BB54390A45C6334', 16011, 16011, '1989', '1989-01-01', 'm', 42, 1),
(9200, NULL, NULL, '', 'Jurke', 'Äneas', 17011, NULL, 17011, 17011, '1989', '1989-01-01', 'm', 42, 1),
(9249, 'C898D16CC71443E497109671426B9ADB', NULL, '', 'Stecklum', 'Philipp', 16003, '15FAEC1DD19145A983D96F5A3974C7D1', 16003, 16003, '1988', '1988-01-01', 'm', 42, 1),
(9448, 'D01FCFA4B1554AB7AE8C2DADCB460DD9', NULL, '', 'Schroth', 'Nina', 9007, '04B61F2CA6F743F897BFDFDCC2A48712', 9007, 9007, '1991', '1991-01-01', 'w', 42, 1),
(9549, 'E545817CB18446FCB190F1BF0F7D2D3F', NULL, '', 'Brandhuber', 'Simon', 9010, '728BCFEBAEE94E56A0AA57156D1AE0B1', 9010, 9010, '1991', '1991-01-01', 'm', 42, 1),
(9774, '01E172D4047140E99D336A95B8483B66', NULL, '', 'Krieger', 'Sandro', 9013, '68D501EAA45F4E32B77FE56C4B2BE68D', 9013, 9013, '1979', '1979-07-04', 'm', 42, 1),
(9846, 'B025874CF09F471681F824A98BCAD18B', NULL, '', 'Meerkamp', 'Steffen', 14011, 'E7A6654C1F154D1C9932270AF73E1136', 14011, 14011, '1986', '1986-01-01', 'm', 42, 1),
(9998, NULL, NULL, '', 'Yüksel', 'Yasin', 14007, NULL, 14007, 9010, '1991', '1991-01-01', 'm', 42, 1),
(10614, 'F7DC0A23AB47498281F1C1CD992A0905', NULL, '', 'Hoblos', 'Mohammed', 2003, 'B515D63CC1074ECB9D5C0D3962A05680', 2003, 2003, '1990', '1990-01-01', 'm', 42, 1),
(10632, '8D0BD452B15F4278A763B87A8ACA2C2E', NULL, '', 'Müller', 'Nico', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '1993', '1993-01-01', 'm', 42, 1),
(10846, 'AEBD9387253C4078A508011A8EB1E83B', NULL, '', 'Griebel', 'Philipp', 16003, 'D1AF5E9A96C543DE9E86713062A5C1B2', 16003, 16003, '1996', '1996-01-01', 'm', 42, 1),
(10847, 'F2817C1C35D142BBBDFE5BBAC93F2F77', NULL, '', 'Holtmann', 'Nico', 16003, 'D1AF5E9A96C543DE9E86713062A5C1B2', 16003, 16003, '1996', '1996-01-01', 'm', 42, 1),
(10912, 'BF956D14975140C6A2BCE987B29C92B2', NULL, '', 'Hofmann', 'Matthäus', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '1994', '1994-01-01', 'm', 42, 1),
(10924, 'DCC25DF38CDF44AD802A96C5CEB30B57', NULL, '', 'Steinhöfel', 'Marlon', 2003, 'B515D63CC1074ECB9D5C0D3962A05680', 2003, 2003, '1993', '1993-01-01', 'm', 42, 1),
(10960, '8F16B4562B0D4165957C34F19AA45BB6', NULL, '', 'Eckhardt', 'Mark', 15009, 'BE0FB064DFD34F2AB48082AA9C14CF44', 15009, 15009, '1989', '1989-01-01', 'm', 42, 1),
(11057, '6B31782DC4114C88A7DD9689655F38FF', NULL, '', 'Pichler', 'Christoph', 0, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 0, 14007, '1993', '1993-01-01', 'm', 42, 1),
(11188, '2437675D39154327A59F0071622C7C2F', NULL, '', 'Spindler', 'Christina', 14007, '728BCFEBAEE94E56A0AA57156D1AE0B1', 14007, 9010, '1995', '1995-01-01', 'w', 42, 1),
(11211, 'B4F9372FDE1F4212921E60411418B241', NULL, '', 'Klytta', 'Dominik', 14010, '1FC24AEE6E3F4A59A9A4D4779B6AE767', 14010, 14010, '1994', '1994-09-21', 'm', 42, 1),
(11226, '8E76793FD3BF49A8A375E67690F8288C', NULL, '', 'Lang', 'Max', 9007, '04B61F2CA6F743F897BFDFDCC2A48712', 9007, 9007, '1992', '1992-01-01', 'm', 42, 1),
(11229, NULL, NULL, '', 'Rosol', 'Tomasz', 3013, NULL, 3013, 3013, '1989', '1989-01-01', 'm', 42, 1),
(11257, 'A107DF45A2B1441D9BD2F95CD188395F', NULL, '', 'Gross', 'Lukas', 9005, '32230541F9A3489EA9C2CB633C1581DB', 9005, 9005, '1994', '1994-11-25', 'm', 42, 1),
(11336, NULL, NULL, '', 'Mockert', 'Tim', 147, NULL, 147, 147, '1995', '1995-01-01', 'm', 42, 1),
(11357, 'C529AFD7352E4BA4B78619CA76A6D204', NULL, '', 'Bouratn', 'Martin', 17011, 'CC00AF6F34B441DB9F2D64B11D895A9D', 17011, 17011, '1995', '1995-01-01', 'm', 42, 1),
(11376, 'DB5E507E9FAA4D939E0BB283963BEA24', NULL, '', 'Attilo', 'Joshua', 9002, 'C457BA6948FE415CAF879BE0F635D14B', 9002, 9002, '1996', '1996-06-24', 'm', 42, 1),
(11432, 'DCB26FDDBE1B47AABC5DC180EF9871D0', NULL, '', 'Hülser', 'Philipp', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '1995', '1995-01-01', 'm', 0, 1),
(11439, 'F9740D90113640139CD854629C7AAB3A', NULL, '', 'Günther', 'Björn', 15009, 'BE0FB064DFD34F2AB48082AA9C14CF44', 15009, 15009, '1996', '1996-01-01', 'm', 42, 1),
(11525, 'F5A64A3906FF41EC977D28FD0C15E615', NULL, '', 'Berthold', 'Sebastian', 3009, '525195E25E9148CC9071D802318844FB', 3009, 3009, '1995', '1995-01-01', 'm', 42, 1),
(11526, NULL, NULL, '', 'Mummhardt', 'Philip', 3013, NULL, 3013, 3013, '1995', '1995-01-01', 'm', 42, 1),
(11726, '30A01DDE61644E21A21F7CD563097E41', NULL, '', 'Schweizer', 'Lisa Marie', 9010, '728BCFEBAEE94E56A0AA57156D1AE0B1', 9010, 9010, '1995', '1995-01-01', 'w', 42, 1),
(11832, '910E330FF7514FF59888097422185BFD', NULL, '', 'Varlamov', 'Michael', 2009, '6F59120BAB7B44F887F042E9F31E6F5A', 9007, 9007, '1997', '1997-01-01', 'm', 42, 1),
(11848, '8CAA189BEDB04846AC8529DFA97BBB24', NULL, '', 'Schroll', 'Jacqueline', 2003, '20388E1EF1E74D8986B4137EC7201DE6', 2003, 2003, '1997', '1997-01-01', 'w', 42, 1),
(11865, 'F4DFADDB16094544884900B63EB0DA45', NULL, '', 'Fischer', 'Ken', 17021, '1F5CB42677F04D3F8B41312A82B98BD7', 17021, 17021, '1997', '1997-01-01', 'm', 42, 1),
(11912, NULL, NULL, '', 'Männecke', 'Max', 17011, NULL, 17011, 17011, '1995', '1995-01-01', 'm', 42, 1),
(11942, 'D3BA6647D1574BF1B917EADFBFAD9903', NULL, '', 'Dancz', 'Lara', 2009, 'C457BA6948FE415CAF879BE0F635D14B', 9007, 9007, '2000', '2000-01-01', 'w', 42, 1),
(11943, 'A8BC2B08282A4591B813527ED92806DB', NULL, '', 'Perthel', 'Kurt', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '1997', '1997-01-01', 'm', 42, 1),
(12043, NULL, NULL, '', 'Hartenberger', 'Florian', 147, NULL, 147, 147, '1999', '1999-01-01', 'm', 42, 1),
(12056, 'B85AACC9EE0B4C5FA378CF7B18C64894', NULL, '', 'Huber', 'Moritz', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '1998', '1998-01-01', 'm', 42, 1),
(12062, 'F1435E3BFFC34FE0A520B469AE296A1F', NULL, '', 'Adler', 'Martin', 17021, '1B3FD10EC13E47158FEF3A4EC41C4451', 17021, 17021, '1999', '1999-01-01', 'm', 42, 1),
(12077, '093DACC11E404EF982335C066B97C447', NULL, '', 'Schedler', 'Leon', 3013, '1F5CB42677F04D3F8B41312A82B98BD7', 3013, 3013, '1998', '1998-01-01', 'm', 42, 1),
(12080, NULL, NULL, '', 'Atti', 'Nadia', 17021, NULL, 17021, 17021, '1998', '1998-01-01', 'w', 42, 1),
(12089, '8EC043252A3C49D4B7840A25C3D15B38', NULL, '', 'Dauth', 'Carolin', 2003, 'B515D63CC1074ECB9D5C0D3962A05680', 2003, 2003, '1999', '1999-01-01', 'w', 42, 1),
(12144, NULL, NULL, '', 'Zandeck', 'Manuel', 17011, NULL, 17011, 17011, '1998', '1998-01-01', 'm', 42, 1),
(12152, '974020A564C74F829DF3E71F84EFFFE0', NULL, '', 'Heyer', 'Fritz', 16003, 'D1AF5E9A96C543DE9E86713062A5C1B2', 16003, 16003, '1999', '1999-01-01', 'm', 42, 1),
(12156, '0E28C6BE64474149B207BD60C4AA0E8C', NULL, '', 'Michalski', 'Arkadiusz', 15009, 'BE0FB064DFD34F2AB48082AA9C14CF44', 15009, 15009, '1990', '1990-01-01', 'm', 147, 1),
(12172, 'EC8BB77343EF4DA89440A7DACB56CE4D', NULL, '', 'Trautmann', 'Frank', 10003, '5CC1AFFE0271477B93370DBBF42C3DE7', 10003, 10003, '1996', '1996-01-08', 'm', 42, 1),
(12206, 'FE0331DDE384415D84198C639EEBBF82', NULL, '', 'Schreiber', 'Chantal', 15009, '49ACF8809DC04B4CBB8116A953F79B7C', 15009, 15009, '1998', '1998-01-01', 'w', 42, 1),
(12208, NULL, NULL, '', 'Pianski', 'Julian', 0, NULL, 0, 14007, '1998', '1998-01-01', 'm', 42, 1),
(12294, NULL, NULL, '', 'Janta', 'Hagen', 0, NULL, 0, 14007, '1999', '1999-01-01', 'm', 42, 1),
(12295, 'D85926951B5541C8939151D184116BFA', NULL, '', 'Weser', 'Martin', 14004, 'AE4894CD8D3A487081765D1B6C3BD372', 14004, 14004, '1999', '1999-04-19', 'm', 42, 1),
(12330, '74CEF9FEDA324CDC8F1669754FAD1D56', NULL, '', 'Gnauck', 'Markus Eberhard', 16011, '394F189769D446669BB54390A45C6334', 16011, 16011, '1980', '1980-01-01', 'm', 42, 1),
(12366, NULL, NULL, '', 'Taubert', 'Anna', 147, NULL, 147, 147, '2000', '2000-01-01', 'w', 42, 1),
(12369, '6DC9194EB9764B8C90A48A64571C8733', NULL, '', 'Perlt', 'Julia', 16003, 'D1AF5E9A96C543DE9E86713062A5C1B2', 16003, 16003, '2001', '2001-01-01', 'w', 42, 1),
(12371, 'D05FACF0268C4ED199DF9A3D84AA4607', NULL, '', 'Schwarzbach', 'Julia', 9010, '728BCFEBAEE94E56A0AA57156D1AE0B1', 9010, 9010, '1989', '1989-01-01', 'w', 42, 1),
(12438, NULL, NULL, '', 'Leuschner', 'Maximilian', 17011, NULL, 17011, 17011, '1999', '1999-01-01', 'm', 42, 1),
(12472, NULL, NULL, '', 'Krause', 'Sandra', 17021, NULL, 17021, 17021, '2000', '2000-01-01', 'w', 42, 1),
(12506, 'BB619D65FDB146EEA9C3B3B53FAD912A', NULL, '', 'Friedrich', 'Raphael', 14016, 'E4DB92F8FA894A15A094F7CB44271046', 14016, 14007, '2001', '2001-01-01', 'm', 42, 1),
(12541, '55C9B268DDC44D9DBBC3F4EC5B997D32', NULL, '', 'Janta', 'Gerit', 14004, 'AE4894CD8D3A487081765D1B6C3BD372', 14004, 14004, '1997', '1997-01-01', 'w', 42, 1),
(12547, 'EAB1F9CF11E74A669A8C28A38A0A5898', NULL, '', 'Izere Shima', 'Antoine de Padou', 9002, 'C457BA6948FE415CAF879BE0F635D14B', 9002, 9002, '2000', '2000-08-14', 'm', 42, 1),
(12696, '7C080A8300654D4E8ADF28ED5452CD40', NULL, '', 'Vogel', 'Marc-Niklas', 16011, '394F189769D446669BB54390A45C6334', 16011, 16011, '2000', '2000-01-01', 'm', 42, 1),
(12702, '8D25B0A90ACB47DB89D140E61E43CA13', NULL, '', 'Ludwig', 'Nancy', 14005, '37668537FC6147F59E03477C6788809F', 14005, 14005, '2000', '2000-01-01', 'w', 42, 1),
(12739, '714C3311F5A04ACEBB712A106FFD34CB', NULL, '', 'Langkabel', 'André', 16003, 'D1AF5E9A96C543DE9E86713062A5C1B2', 16003, 16003, '2000', '2000-01-01', 'm', 42, 1),
(12765, NULL, NULL, '', 'Hartenberger', 'Michelle', 147, NULL, 147, 147, '2002', '2002-01-01', 'w', 42, 1),
(12773, NULL, NULL, '', 'Hein', 'Karl', 3009, NULL, 3009, 3009, '2002', '2002-01-01', 'm', 42, 1),
(12774, 'F64BB70585AE479DB3ED6187B5BEF0DD', NULL, '', 'Ackermann', 'Antonia', 3009, '525195E25E9148CC9071D802318844FB', 3009, 3009, '2002', '2002-01-01', 'w', 42, 1),
(12780, 'AE9D5C08D63B4C95AEEBB3584C3FD117', NULL, '', 'Pilz', 'Annika', 15009, '75FC4094E98C4EA7870DD0AE7724E9A5', 15009, 15009, '2002', '2002-01-01', 'w', 42, 1),
(12788, NULL, NULL, '', 'Bröse', 'Maximilian', 3009, NULL, 3009, 3009, '2002', '2002-01-01', 'm', 42, 1),
(12825, 'AE2F97ACD8F24B3F859678307AD63555', NULL, '', 'Ludwig', 'Erik', 14005, '37668537FC6147F59E03477C6788809F', 14005, 14005, '2001', '2001-03-18', 'm', 42, 1),
(12885, '6EB2C808BAC4487898A111D96AAE3313', NULL, '', 'Hofmann', 'Ruben', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '2000', '2000-01-01', 'm', 42, 1),
(12911, '582A43FCD0274E68A35B8B4D013CF1BD', NULL, '', 'Gutu', 'Roberto', 15009, 'BE0FB064DFD34F2AB48082AA9C14CF44', 15009, 15009, '2000', '2000-01-01', 'm', 42, 1),
(12915, '59EDF096124E4995B83B8B5952F5A395', NULL, '', 'Drechsel', 'Nikita', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '2001', '2001-01-01', 'm', 42, 1),
(12989, '7014AD69E5B84E56AE7CC0FA850EE9F5', NULL, '', 'Barthel', 'Marlen', 147, '0D3D021A79F44069917CFF2D9BDC045F', 147, 147, '2003', '2003-01-01', 'w', 42, 1),
(13030, '1D279029616C40529BA86F32E146EAD0', NULL, '', 'Wang', 'Jingyi', 1040, '37DF1B72D037483796FBCD977BE39622', 1040, 1040, '1992', '1992-01-01', 'm', 137, 1),
(13035, 'BB48ECBEE85E4BE99FDE1438895AC16B', NULL, '', 'Exner', 'Elias', 3009, '525195E25E9148CC9071D802318844FB', 3009, 3009, '2003', '2003-01-01', 'm', 42, 1),
(13094, NULL, NULL, '', 'Schnurrer', 'Florian', 1040, NULL, 1040, 1040, '1993', '1993-01-01', 'm', 42, 1),
(13113, NULL, NULL, '', 'Rieß', 'Lisa', 1040, NULL, 1040, 1040, '1989', '1989-01-01', 'w', 42, 1),
(13176, '3E18444A7D3243E1A63BDF857C4E9F6E', NULL, '', 'Viol', 'Laura', 0, 'A603DD6BA44A4D65B4B3B04D2E893463', 0, 0, '2005', '2005-09-29', 'w', 42, 1),
(13251, '4A1AD0E8B5234B578031FFAB05265BF2', NULL, '', 'Davies', 'Sarah', 2009, '04B61F2CA6F743F897BFDFDCC2A48712', 9007, 9007, '1992', '1992-01-01', 'w', 42, 1),
(13262, '0C39E60EA86049B7A0D94470A4BE064E', NULL, '', 'Pester', 'Julian', 147, 'E4DB92F8FA894A15A094F7CB44271046', 147, 147, '2003', '2003-01-01', 'm', 42, 1),
(13303, 'A506D6F1B73D4619979F367E3B86D02C', NULL, '', 'Breitschuh', 'Marie-Sophie', 16003, 'D1AF5E9A96C543DE9E86713062A5C1B2', 16003, 16003, '2003', '2003-01-01', 'w', 42, 1),
(13326, 'A05AEFA413A84EA8BFCEEAE1CBB82F0A', NULL, '', 'Wilhelm', 'Julian', 13002, '84AD09BB4714415F80B7BA081B9FB4D6', 13002, 13002, '1999', '1999-05-28', 'm', 42, 1),
(13372, 'BC308538E88E47848D5786772383B1F9', NULL, '', 'Elsner', 'Eric Enrico', 14010, '1FC24AEE6E3F4A59A9A4D4779B6AE767', 14010, 14010, '2002', '2002-06-20', 'm', 42, 1),
(13498, '4DE9178088524DE9B7AD45F64FFE2299', NULL, '', 'Ghazal', 'Alaa', 2003, 'B515D63CC1074ECB9D5C0D3962A05680', 2003, 2003, '1985', '1985-01-01', 'm', 42, 1),
(13528, 'BDE43335D85146EF91242AF5ABFEC9AA', NULL, '', 'Ielezov', 'Konstiantyn', 147, '0D3D021A79F44069917CFF2D9BDC045F', 147, 147, '1987', '1987-01-01', 'm', 196, 1),
(13630, 'AD389C471BBA461D986EDC81BDF45309', NULL, '', 'Wille', 'Tobias', 9005, '32E55FD794C343D2B58C1E0A411A214F', 9005, 9005, '1990', '1990-08-01', 'm', 42, 1),
(13662, NULL, NULL, '', 'Taach', 'Ilja', 17021, NULL, 17021, 17021, '2003', '2003-01-01', 'm', 151, 1),
(13665, 'D3979C62C16844AFAE5986226682ECD8', NULL, '', 'Martin', 'Leonie Mercedes', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '2004', '2004-01-01', 'w', 42, 1),
(13667, 'A284BFBEF0844573A0D03D0C548D078E', NULL, '', 'Müller', 'Lucas', 14004, 'AE4894CD8D3A487081765D1B6C3BD372', 14004, 14004, '2004', '2004-01-01', 'm', 42, 1),
(13672, 'D0C461C72BE24AFA966D0772FA6A4634', NULL, '', 'Sanchez Lopez', 'David', 14007, '728BCFEBAEE94E56A0AA57156D1AE0B1', 14007, 9010, '1994', '1994-01-01', 'm', 170, 1),
(13730, '4BEFA4E6B7314AFB87B791F89465DD1A', NULL, '', 'Pudivitrova', 'Eliska', 147, '0D3D021A79F44069917CFF2D9BDC045F', 147, 147, '1994', '1994-01-01', 'w', 190, 1),
(13731, NULL, NULL, '', 'Petrov', 'Petr', 147, NULL, 147, 147, '1989', '1989-01-01', 'm', 190, 1),
(20007, 'F9E157A63DF94F09820D5701DE9334C6', NULL, '', 'Kingue Matam', 'Bernardin Ledoux', 2003, 'B515D63CC1074ECB9D5C0D3962A05680', 2003, 2003, '1990', '1990-01-01', 'm', 53, 1),
(20059, '8672346CB7904BC08EDF89087FF88D42', NULL, '', 'Wüst', 'Carolin Annabell', 9007, '04B61F2CA6F743F897BFDFDCC2A48712', 9007, 9007, '1999', '1999-01-01', 'w', 42, 1),
(20169, '780112812C2A41D983DA0D8033489F2F', NULL, '', 'Suharevs', 'Ritvars', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '1999', '1999-01-01', 'm', 99, 1),
(20203, '46C0589A3C454EEB9DC5F04EB5A9D713', NULL, '', 'Andreev', 'Bozhidar, Dimitrov', 2003, '7F0FFAF94E3C43A18F6DDEAC2FBDE1DF', 2003, 2003, '1997', '1997-01-01', 'm', 29, 1),
(20215, 'C124CB0BE0B84EB6A7ED77AEF75ED228', NULL, '', 'Klitzke', 'Sarah', 1040, '37DF1B72D037483796FBCD977BE39622', 1040, 1040, '1993', '1993-01-01', 'w', 42, 1),
(20251, '90D714D85A0B4465978D0245EFCA8D07', NULL, '', 'Ahmetov', 'Salina', 3013, '49ACF8809DC04B4CBB8116A953F79B7C', 3013, 3013, '2003', '2003-01-01', 'w', 42, 1),
(20259, '96EE13D3374E497CB7358EFFC9852406', NULL, '', 'Tas', 'Sinem', 9002, 'C457BA6948FE415CAF879BE0F635D14B', 9002, 9002, '2004', '2004-05-17', 'w', 42, 1),
(20294, 'ED0C4821DCD44E7687FCCD81FD7A11EA', NULL, '', 'Zimmermann', 'Oliver', 14010, '1FC24AEE6E3F4A59A9A4D4779B6AE767', 14010, 14010, '2002', '2002-01-01', 'm', 42, 1),
(20365, '480BE4C2BAB34B3A995BFC18A11AB4BB', NULL, '', 'Menzel', 'Ricardo', 14009, '1FC24AEE6E3F4A59A9A4D4779B6AE767', 14009, 14009, '2002', '2002-10-12', 'm', 42, 1),
(20428, 'C421D602C0D7493CB83F606106922FFF', NULL, '', 'Stauder', 'Vanessa', 9013, '68D501EAA45F4E32B77FE56C4B2BE68D', 9013, 9013, '1992', '1992-06-27', 'w', 42, 1),
(20532, 'C26678919F3F44BC840F4B0F4AFEFDA1', NULL, '', 'Schmitt', 'Riccardo', 0, '1C0D47EDDB3D4ED69DF0B756468139EC', 0, 0, '2003', '2003-01-25', 'm', 42, 1),
(20550, 'E123A922EB8C49A783C0E00B526C1043', NULL, '', 'Häusler', 'Anna-Carina', 1040, '37DF1B72D037483796FBCD977BE39622', 1040, 1040, '1989', '1989-01-01', 'w', 42, 1),
(20552, '7267B3632B56447E941706EE8FB76DBD', NULL, '', 'Rudkovskaya', 'Viktoria', 3009, 'CC00AF6F34B441DB9F2D64B11D895A9D', 3009, 3009, '1987', '1987-01-01', 'w', 151, 1),
(20639, 'F3EDBA8BDB5B4DC5BEB4AF5771FECF36', NULL, '', 'Reinstädtler', 'Fabian', 13002, '84AD09BB4714415F80B7BA081B9FB4D6', 13002, 13002, '1995', '1995-05-12', 'm', 42, 1),
(20640, '47AEA39D05DF40BABCFAE0F705ADFB92', NULL, '', 'Siegel', 'Daniel Ou-Yang', 9007, '04B61F2CA6F743F897BFDFDCC2A48712', 9007, 9007, '1996', '1996-01-01', 'm', 42, 1),
(20663, '38095CC484FF4A7D924B5726FBFBAE12', NULL, '', 'Plesnieks', 'Arturs', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '1992', '1992-01-01', 'm', 99, 1),
(20704, 'D2B9FD1CF9554DD4BD6896FCB1DF657C', NULL, '', 'May-Günthert', 'Irina', 9013, '68D501EAA45F4E32B77FE56C4B2BE68D', 9013, 9013, '1979', '1979-11-26', 'w', 42, 1),
(20781, '0468A94DE2FE474BBDB9087F9D9265A8', NULL, '', 'Schwab', 'Olivia', 10004, '5CC1AFFE0271477B93370DBBF42C3DE7', 10004, 10004, '1999', '1999-01-01', 'w', 42, 1),
(20789, '13945DF196F04C6F9C65BD56E8AA7322', NULL, '', 'Richter', 'Mario', 14009, 'ECD0DEBEC0414F5DA9421350774855FF', 14009, 14009, '1973', '1973-03-02', 'm', 42, 1),
(20815, NULL, NULL, '', 'Isilay', 'Ali', 1040, NULL, 1040, 1040, '1994', '1994-01-01', 'm', 134, 1),
(20819, 'B226ED366B3D4D75A4D55D7A7F3C0D35', NULL, '', 'Hollerith', 'Anna', 1040, '37DF1B72D037483796FBCD977BE39622', 1040, 1040, '1996', '1996-01-01', 'w', 42, 1),
(20820, '3AFDDC4D384E4546BF16B72FF6BEB5D7', NULL, '', 'Büller', 'Christina', 16003, 'D1AF5E9A96C543DE9E86713062A5C1B2', 16003, 16003, '1991', '1991-01-01', 'w', 137, 1),
(20846, 'C29926D9C3B744A1A900261FE4693291', NULL, '', 'Thiele', 'Milena', 14007, '728BCFEBAEE94E56A0AA57156D1AE0B1', 14007, 14007, '2002', '2002-01-01', 'w', 42, 1),
(20878, NULL, NULL, '', 'Haspel', 'Stephanie', 14007, NULL, 14007, 9010, '1992', '1992-01-01', 'w', 42, 1),
(20935, NULL, NULL, '', 'Wellnitz', 'Sheraida', 3009, NULL, 3009, 3009, '1986', '1986-01-01', 'w', 42, 1),
(20974, '2021C724451E49B0BA41BB3331C03BAC', NULL, '', 'Diehl', 'Korinna', 9004, '28B6BA97A27C4D1685A55E462F588D03', 9004, 9004, '1963', '1963-11-09', 'w', 42, 1),
(20987, '63627445FE6D47439D8D4E170867A553', NULL, '', 'Schönsiegel', 'Celina', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '2002', '2002-01-01', 'w', 42, 1),
(21057, '0DB57643A60C4F9E8EE8AA5F675E0BAB', NULL, '', 'Tomkowiak', 'Lena', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '1995', '1995-01-01', 'w', 42, 1),
(21122, 'D049B9369C094103B64D6D3C12652E5D', NULL, '', 'Oettel', 'Ole', 9014, 'A912DB3A59FC47B9A5578073856AAAA8', 9014, 9014, '1983', '1983-10-15', 'm', 42, 1),
(21152, 'F09FA7AB9F094D0F99CA3E9803ACAA87', NULL, '', 'Rusch', 'Valentino', 14005, '37668537FC6147F59E03477C6788809F', 14005, 14005, '2005', '2005-01-01', 'm', 42, 1),
(21231, '23817B21D6654CBDA9EBD8DBAACAD88E', NULL, '', 'Hansch', 'Dennis', 17021, '1F5CB42677F04D3F8B41312A82B98BD7', 17021, 17021, '2002', '2002-01-01', 'm', 42, 1),
(21238, '7671F74DBD6242E59C05F00013463C61', NULL, '', 'Hacker', 'Sandra', 2003, '728BCFEBAEE94E56A0AA57156D1AE0B1', 2003, 2003, '1992', '1992-01-01', 'w', 42, 1),
(21241, '87AF367422B04D24BF48CC58CDA2F66E', NULL, '', 'Scholz', 'Florian', 14005, '37668537FC6147F59E03477C6788809F', 14005, 14005, '2005', '2005-01-01', 'm', 42, 1),
(21254, '43E42A46F5A14CDB9FBA8440204339D3', NULL, '', 'Chrysochoidis', 'Alexandros', 15001, '5FD25818C7594211854C728D57470203', 15001, 15001, '2005', '2005-04-22', 'm', 42, 1),
(21331, '083D80F78B544BE1BD7D73BBAD7F1E12', NULL, '', 'Soldner', 'Farin', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '2005', '2005-01-01', 'm', 42, 1),
(21437, '6A9F6DC58E6E45B8BF517C8ADE2637C8', NULL, '', 'Tabaku', 'Arsian', 20542, '4C238E7074B947E784CB529EA424E5FC', 20542, 20542, '1990', '1990-11-02', 'm', 42, 1),
(21452, '25A12F552C8C4842B005C2E8DFD192BA', NULL, '', 'Nützel', 'Lucas', 9002, 'C457BA6948FE415CAF879BE0F635D14B', 9002, 9002, '2004', '2004-12-25', 'm', 42, 1),
(21498, NULL, NULL, '', 'Papanikolaou', 'Athanasions', 3013, NULL, 3013, 3013, '1990', '1990-01-01', 'm', 59, 1),
(21555, NULL, NULL, '', 'Biega', 'Tadeusz', 3013, NULL, 3013, 3013, '1986', '1986-01-01', 'm', 147, 1),
(21580, '50796FF0FCC8422DBCD7CBAB27078DE9', NULL, '', 'Martirosjan', 'Sargis', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '1986', '1986-01-01', 'm', 137, 1),
(21659, '09702F9F85CF4C63A3D73EA35AC51485', NULL, '', 'Weidlich', 'Alina', 100000, '037CDAC962F34E64B6D701A596355DF3', 100000, 100000, '1988', '1988-01-01', 'w', 42, 1),
(21722, 'A1A9754FB66A447FA25F90EFA0048238', NULL, '', 'Pomsel', 'Maurice', 147, '0D3D021A79F44069917CFF2D9BDC045F', 147, 147, '2006', '2006-01-01', 'm', 42, 1),
(21732, '753DB3D1E8B4419B973F0C9016C3909F', NULL, '', 'Weißmann', 'Ron', 147, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 147, 147, '2006', '2006-01-01', 'm', 42, 1),
(21734, '86BE2F5AF8A24CC89553F71BF287AD45', NULL, '', 'Bellmann', 'Ole', 147, 'E4DB92F8FA894A15A094F7CB44271046', 147, 147, '2006', '2006-01-01', 'm', 42, 1),
(21735, 'D983823AA7E84FA7B24D9FFE5EDF907E', NULL, '', 'Neubert', 'Nora', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '2006', '2006-01-01', 'w', 42, 1),
(21737, '1B99CB9E1FA443C4A53C8B89F5745009', NULL, '', 'Schlittig', 'Robby', 14011, 'E7A6654C1F154D1C9932270AF73E1136', 14011, 14011, '2006', '2006-01-01', 'm', 42, 1),
(21740, '1673517BE4454C4CB3DD9ACC94D86012', NULL, '', 'Kuworge', 'Enzo Kofi', 9007, '04B61F2CA6F743F897BFDFDCC2A48712', 9007, 9007, '2001', '2001-01-01', 'm', 130, 1),
(21778, 'E2E4F65176034C3EA8EED5DA7A59964F', NULL, '', 'Pfeifer', 'Nicco', 14004, 'AE4894CD8D3A487081765D1B6C3BD372', 14004, 14004, '2006', '2006-01-01', 'm', 42, 1),
(21840, '61D802A101144221BC2B6ACEF0A33B3D', NULL, '', 'Sterckx', 'Nina', 0, '728BCFEBAEE94E56A0AA57156D1AE0B1', 0, 0, '2002', '2002-07-26', 'w', 19, 1),
(21908, '1CFA0CBC26794CECB340D9FBDBEC63C5', NULL, '', 'Karnatz', 'Louis', 14005, '37668537FC6147F59E03477C6788809F', 14005, 14005, '2005', '2005-01-01', 'm', 42, 1),
(21938, 'DB83155EF33E4F97BE1790845B8CC864', NULL, '', 'Kolar', 'Josef', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '1997', '1997-01-01', 'm', 0, 1),
(21986, '52D5AF94168344DCBBE4CD06B72158D5', NULL, '', 'Behm', 'Alexander', 2016, 'A41D3F4B6B8340FFA559473D3BA66183', 2016, 2016, '2007', '2007-06-20', 'm', 42, 1),
(22054, '9D8E198D2F3E4D30BE112BEA4DE2E7F4', NULL, '', 'Nützel', 'Sarah', 9002, 'C457BA6948FE415CAF879BE0F635D14B', 9002, 9002, '2007', '2007-01-14', 'w', 42, 1),
(22055, '32C41A881F4942BDB4C2DF7F42531091', NULL, '', 'Hammer', 'Falk', 9002, 'C457BA6948FE415CAF879BE0F635D14B', 9002, 9002, '2007', '2007-09-18', 'm', 42, 1),
(22066, '29FEF345C70E46EFA279AC2EE8F05E64', NULL, '', 'Campbell', 'Emily Jade', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '1994', '1994-01-01', 'w', 205, 1),
(22075, '9BC7AC1845784368BCF718CB10DC61CD', NULL, '', 'Plischke', 'Marc', 14004, 'AE4894CD8D3A487081765D1B6C3BD372', 14004, 14004, '2004', '2004-01-01', 'm', 42, 1),
(22082, '37DA070AB6F04B3F901C3A970ED37325', NULL, '', 'Steinbach', 'Sven', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '1967', '1967-01-01', 'm', 42, 1),
(22103, 'B3B8004A58CA4D539ED802182E798ABF', NULL, '', 'Castaneda', 'Steven', 9004, '28B6BA97A27C4D1685A55E462F588D03', 9004, 9004, '2003', '2003-10-12', 'm', 42, 1),
(22114, '4CB0F751E0D84C848B4DEA04C7C0CDF6', NULL, '', 'Sahin', 'Silvan', 22070, '61A8579B31164C0B9599530618E8E71B', 22070, 22070, '2006', '2006-01-20', 'm', 42, 1),
(22122, 'D456B350F41F48D2AF295193D4599F1F', NULL, '', 'Zierer', 'Verena', 1040, '37DF1B72D037483796FBCD977BE39622', 1040, 1040, '1994', '1994-01-01', 'w', 42, 1),
(22213, NULL, NULL, '', 'Hänsch', 'Sebastian', 147, NULL, 147, 147, '1993', '1993-01-01', 'm', 42, 1),
(22270, '4B1DFF6374F9424CB892DA35989C7CD6', NULL, '', 'Benz', 'Etienne', 2016, 'A41D3F4B6B8340FFA559473D3BA66183', 2016, 2016, '2003', '2003-06-28', 'm', 42, 1),
(22271, NULL, NULL, '', 'Timme', 'Sinika', 17011, NULL, 17011, 17011, '1992', '1992-01-01', 'w', 42, 1),
(22282, '834DF20E15ED4DD2B4E1A63AC4789057', NULL, '', 'Tóth ', 'Adrienn ', 17011, 'CC00AF6F34B441DB9F2D64B11D895A9D', 17011, 17011, '1995', '1995-01-01', 'w', 197, 1),
(22285, '9E06900417E54CAD8CCA31D33C6B844A', NULL, '', 'Daum', 'Siegfried', 9014, 'A912DB3A59FC47B9A5578073856AAAA8', 9014, 9014, '1949', '1949-08-25', 'm', 42, 1),
(22305, '4F1F5886AB054D57B342DC45829D6065', NULL, '', 'Calja', 'Briken', 14007, '728BCFEBAEE94E56A0AA57156D1AE0B1', 14007, 9010, '1990', '1990-01-01', 'm', 4, 1),
(22308, 'A1EC5E939B66410C85747629B97D9390', NULL, '', 'Mezinskis', 'Armands', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '2000', '2000-01-01', 'm', 99, 1),
(22312, '32BF3301923740BFAFF2929EC9389A0B', NULL, '', 'Wiedmann', 'Corina', 9003, 'F882AB2802C541DC8C8E26654E128418', 9003, 9003, '1997', '1997-05-21', 'w', 42, 1),
(22374, '32D67572090E4EB8A38833BFC050C4E8', NULL, '', 'Henzmann', 'Peter', 9009, '32230541F9A3489EA9C2CB633C1581DB', 9009, 9009, '1967', '1967-05-10', 'm', 42, 1),
(22403, '7B60B144553A4AC691C01DF2B914176C', NULL, '', 'Roy', 'Nicole ', 15009, 'BE0FB064DFD34F2AB48082AA9C14CF44', 15009, 15009, '1983', '1983-01-01', 'w', 42, 1),
(22437, '88F5BF71A4C04FB88F600CEF47E9F35D', NULL, '', 'Kormann', 'Annika Andrea', 2003, 'ECDB7980EE544527BDD9880A3CE7FBE5', 2003, 2003, '1995', '1995-12-13', 'w', 42, 1),
(22517, NULL, NULL, '', 'Qerimaj', 'Erkand', 9010, NULL, 9010, 9010, '1988', '1988-01-01', 'm', 4, 1),
(22520, 'C6C4ACA375374EBFB1BB3FE223322034', NULL, '', 'Schaub', 'Vanessa', 2003, 'B515D63CC1074ECB9D5C0D3962A05680', 2003, 2003, '1992', '1992-01-01', 'w', 42, 1),
(22524, 'B372D5129615485CB1EDAC7FA2BD5E3B', NULL, '', 'Smeenk', 'Elena', 9007, 'A41D3F4B6B8340FFA559473D3BA66183', 9007, 9007, '1998', '1998-01-01', 'w', 42, 1),
(22538, '08722F5EDF8E430F839CD74C654819AE', NULL, '', 'Hristov', 'Hristo', 9007, '04B61F2CA6F743F897BFDFDCC2A48712', 9007, 9007, '2001', '2001-01-01', 'm', 29, 1),
(22563, '55256A574C894E8D92304258C958F2BF', NULL, '', 'Casser', 'Carolin', 16011, '5CC1AFFE0271477B93370DBBF42C3DE7', 16011, 16011, '1999', '1999-01-01', 'w', 42, 1),
(22565, 'EA01B1BB130C416A874809EF372190D4', NULL, '', 'Schneider', 'Steffen', 9002, 'C457BA6948FE415CAF879BE0F635D14B', 9002, 9002, '1992', '1992-12-10', 'm', 42, 1),
(22643, '7BB0D6A9A57644ADBE56E3DADBCB6B7D', NULL, '', 'Thiele', 'Elisabeth', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '1989', '1989-01-01', 'w', 42, 1),
(22685, '3E29A0B166C34D2FA374099FF92E8FD5', NULL, '', 'Gasior', 'Jiri', 15009, 'BE0FB064DFD34F2AB48082AA9C14CF44', 15009, 15009, '1991', '1991-01-01', 'm', 190, 1),
(22687, '8D6D9674090B487789168994673F00E5', NULL, '', 'Wagner', 'Lukas', 9014, 'A912DB3A59FC47B9A5578073856AAAA8', 9014, 9014, '2001', '2001-05-12', 'm', 42, 1),
(22703, '52ECE32B4341441A992B11E8D626C3A7', NULL, '', 'Böhmig', 'Marie', 14005, '37668537FC6147F59E03477C6788809F', 14005, 14005, '2000', '2000-01-01', 'w', 42, 1),
(22722, '5BF984520C774F49939C0D1E0BEF90CE', NULL, '', 'Schlittig', 'Joey', 14011, 'E7A6654C1F154D1C9932270AF73E1136', 14011, 14011, '2008', '2008-01-01', 'm', 42, 1),
(22736, NULL, NULL, '', 'Meinel', 'Lena', 147, NULL, 147, 147, '2005', '2005-01-01', 'w', 42, 1),
(22752, 'FEAC7E376D764F4B90351BC42695E73F', NULL, '', 'Perthel', 'Willy', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '2006', '2006-01-01', 'm', 42, 1),
(22759, '3D71441417B245498B56624AFD82447A', NULL, '', 'Wirkner ', 'Kay Tabea ', 100000, '037CDAC962F34E64B6D701A596355DF3', 100000, 100000, '1995', '1995-01-01', 'w', 42, 1),
(22796, '934A227A4ACB462BA2609B78FB449673', NULL, '', 'Baumbach', 'Nick', 14008, '0D3D021A79F44069917CFF2D9BDC045F', 14008, 14008, '2005', '2005-01-01', 'm', 42, 1),
(22920, '66EAC9E3387B4AC791618617A9820C14', NULL, '', 'Rue', 'Derek', 9014, 'A912DB3A59FC47B9A5578073856AAAA8', 9014, 9014, '1981', '1981-12-21', 'm', 42, 1),
(22921, 'E0BA2EA593A141A0ADD0285BF573BE08', NULL, '', 'Schu', 'Muriel', 9014, 'A912DB3A59FC47B9A5578073856AAAA8', 9014, 9014, '1995', '1995-11-09', 'w', 42, 1),
(22926, 'A5AE4F22A7C1450A95D11BD5FF5B6E50', NULL, '', 'Meissner', 'Leonie', 100000, '037CDAC962F34E64B6D701A596355DF3', 100000, 100000, '1996', '1996-01-01', 'w', 42, 1),
(22932, NULL, NULL, '', 'Krywult', 'Patrik', 14007, NULL, 14007, 14007, '1990', '1990-01-01', 'm', 190, 1),
(23004, '430C644706F547668CACC747D7DA94BD', NULL, '', 'Ritzmann', 'René', 16011, '394F189769D446669BB54390A45C6334', 16011, 16011, '2004', '2004-01-01', 'm', 42, 1),
(23009, '0078BC7A6D85432EA253CDD42696EB6A', NULL, '', 'Hüllebrand', 'Oskar', 14016, 'E4DB92F8FA894A15A094F7CB44271046', 14016, 14016, '2008', '2008-01-01', 'm', 42, 1),
(23017, 'B1689EEA8E354DD0828D675F8F4BD14D', NULL, '', 'Baal', 'Janina', 9007, '04B61F2CA6F743F897BFDFDCC2A48712', 9007, 9007, '1998', '1998-01-01', 'w', 42, 1),
(23021, '1046185246AD4F0CAB161EA80875FEDC', NULL, '', 'Proft', 'Leon', 14009, 'E332586D9B0344D4BFCEF66152F334C0', 14009, 14009, '2008', '2008-01-01', 'm', 42, 1),
(23022, 'A35AE4A574CE4A5B822C53C639FEFD1C', NULL, '', 'Neundorf', 'Magdalena', 14009, 'E332586D9B0344D4BFCEF66152F334C0', 14009, 14009, '2009', '2009-01-01', 'w', 42, 1),
(23023, 'D9F5DF69D42F4EE6AF30806AC7C86784', NULL, '', 'Bär', 'Florian', 14009, 'E332586D9B0344D4BFCEF66152F334C0', 14009, 14009, '2009', '2009-01-01', 'm', 42, 1),
(23074, '948DB149E30E49839BC1DF446A581845', NULL, '', 'Meister', 'Scheila', 0, '786E972AD819439BB98C4A511A7B2789', 0, 0, '1987', '1987-05-02', 'w', 159, 1),
(23084, '44169E7FF6A047F289BC1C68006A998A', NULL, '', 'Wolf', 'Johannes', 14009, 'E332586D9B0344D4BFCEF66152F334C0', 14009, 14009, '1996', '1996-03-05', 'm', 42, 1),
(23129, '97B4FD9914EF477DACC3F6CB587E8613', NULL, '', 'Strzykala', 'Mara', 0, 'C457BA6948FE415CAF879BE0F635D14B', 0, 0, '1992', '1992-03-13', 'w', 105, 1),
(23161, '0A296BCD75D847DF89CF8BFC4E961AD6', NULL, '', 'Tackmann', 'Morris', 14005, '37668537FC6147F59E03477C6788809F', 14005, 14005, '2008', '2008-01-01', 'm', 42, 1),
(23162, '9CB08D1156FE41ED827AA6FF57F18C93', NULL, '', 'Brock', 'Marcel', 14005, '37668537FC6147F59E03477C6788809F', 14005, 14005, '2008', '2008-01-01', 'm', 42, 1),
(23163, '70D6D0718B9E493D9F08D15C8E65E7D8', NULL, '', 'Galm', 'Linda', 9010, '728BCFEBAEE94E56A0AA57156D1AE0B1', 9010, 9010, '1993', '1993-12-16', 'w', 42, 1),
(23171, '552EDDD9D8374AE79E6F9EC8F8FC7B4D', NULL, '', 'Andersson', 'Ine Drablos ', 15009, 'BE0FB064DFD34F2AB48082AA9C14CF44', 15009, 15009, '1989', '1989-01-01', 'w', 135, 1),
(23190, '32B79536652A4723AA44CB33430C6D6E', NULL, '', 'Wittmeyer', 'Hannes', 14010, '1FC24AEE6E3F4A59A9A4D4779B6AE767', 14010, 14010, '2008', '2008-01-01', 'm', 42, 1),
(23191, '61C9063265A44292AF3A36360615AFFF', NULL, '', 'Sobon', 'Dominik', 14010, '1FC24AEE6E3F4A59A9A4D4779B6AE767', 14010, 14010, '2008', '2008-01-01', 'm', 42, 1),
(23192, 'FDA27A4E2CE4420EA3A6E54D461F1419', NULL, '', 'Altmann', 'Anton', 14010, '1FC24AEE6E3F4A59A9A4D4779B6AE767', 14010, 14010, '2008', '2008-01-01', 'm', 42, 1),
(23215, '659EAA0BC6D448F5A8D42DA035146111', NULL, '', 'Lassig', 'Nico', 14004, 'AE4894CD8D3A487081765D1B6C3BD372', 14004, 14004, '2008', '2008-01-01', 'm', 42, 1),
(23217, 'C5B79501C2D949BC8A4AD9F3A79AE68A', NULL, '', 'König', 'Stanley', 14004, 'AE4894CD8D3A487081765D1B6C3BD372', 14004, 14004, '2008', '2008-01-01', 'm', 42, 1),
(23244, 'C9969663AA774B17A3F95AE28F592DFE', NULL, '', 'Mahler', 'Konstantin', 14008, '0D3D021A79F44069917CFF2D9BDC045F', 14008, 14008, '2008', '2008-01-01', 'm', 42, 1),
(23254, 'E8FEF06351B54147BA9F8F838DD02F40', NULL, '', 'Misakyan', 'Hmayak', 2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 2033, 2033, '2002', '2002-01-01', 'm', 137, 1),
(23275, '451C6018837040A69472FF21A2F3360A', NULL, '', 'Klug', 'Kiara', 0, '525195E25E9148CC9071D802318844FB', 0, 0, '2003', '2003-04-12', 'w', 42, 1),
(23294, 'FEDF6F7E962E43208ABBDD1DC65F0F55', NULL, '', 'Karp', 'Lisa', 9002, 'C457BA6948FE415CAF879BE0F635D14B', 9002, 9002, '1985', '1985-04-08', 'w', 42, 1),
(23344, 'B58C5EB4198140EE8B6D4DC3C3A33B0C', NULL, '', 'Zuchold', 'Eric', 100000, '037CDAC962F34E64B6D701A596355DF3', 100000, 100000, '2000', '2000-01-01', 'm', 42, 1),
(23355, '0DE0BB5F50FE43D8838F2F3F75F1F73F', NULL, '', 'Schiff', 'Ramona', 9010, '728BCFEBAEE94E56A0AA57156D1AE0B1', 9010, 9010, '1990', '1990-01-23', 'w', 42, 1),
(23377, 'D6B4DF7ECF7E4F749D335B792FCBEF20', NULL, '', 'Zeller', 'Pia', 100000, '037CDAC962F34E64B6D701A596355DF3', 100000, 100000, '2000', '2000-01-01', 'w', 42, 1),
(23391, '6ADE8BAFEDED4CF18402ED691DCAFE26', NULL, '', 'Michael', 'Gustaf', 14004, 'AE4894CD8D3A487081765D1B6C3BD372', 14004, 14004, '2004', '2004-01-01', 'm', 42, 1),
(23392, 'DFC5555EC7FB4F0BA40526E8F2766126', NULL, '', 'Janta', 'Tristan', 14004, 'AE4894CD8D3A487081765D1B6C3BD372', 14004, 14004, '2008', '2008-01-01', 'm', 42, 1),
(23393, '5123A9C0FB2F442AB77E2631FF868076', NULL, '', 'Große', 'Lars', 14004, 'AE4894CD8D3A487081765D1B6C3BD372', 14004, 14004, '2009', '2009-01-01', 'm', 42, 1),
(23439, '186C5018A3AB4FE4A56E60CF2CACBE72', NULL, '', 'Weber', 'Tanja', 9001, 'E38DA50A843741E491CA9EAB34975D61', 9001, 9001, '1986', '1986-11-25', 'w', 42, 1),
(23490, '5FB1A206B559423E9882B3D448CF8208', NULL, '', 'Nitschke', 'Moritz', 14005, '37668537FC6147F59E03477C6788809F', 14005, 14005, '2005', '2005-01-30', 'm', 42, 1),
(23537, 'F7A8A20A4CB14626ADAD22518EF5F823', NULL, '', 'Tretter', 'Jonas', 9007, '04B61F2CA6F743F897BFDFDCC2A48712', 9007, 9007, '2006', '2006-08-25', 'm', 42, 1),
(23539, '26F13E1E9054423EB8533BEF666C6AFD', NULL, '', 'Kühnel', 'Josy', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '2010', '2010-01-01', 'w', 42, 1),
(23542, '1184E25A59F044368A91F9A932944CEC', NULL, '', 'Weber', 'Katharina', 10004, '5CC1AFFE0271477B93370DBBF42C3DE7', 10004, 10004, '2000', '2000-01-01', 'w', 42, 1),
(23553, 'C7E161D0EA4240FAABB409F9CBAB3930', NULL, '', 'Vogel', 'Arnost', 147, '0D3D021A79F44069917CFF2D9BDC045F', 147, 147, '2000', '2000-01-01', 'm', 190, 1),
(23563, 'B7DF2448D0564169AF77A4A734BFC0D4', NULL, '', 'Auer', 'Justin', 13004, '08604513AC4F4B04B833777BFAC71F64', 13004, 13004, '2005', '2005-09-21', 'm', 42, 1),
(23566, '3DA12B6F79294FB388A02413941EB4A6', NULL, '', 'Müller', 'Michelle', 13004, '08604513AC4F4B04B833777BFAC71F64', 13004, 13004, '1998', '1998-10-17', 'w', 42, 1),
(23592, '607A3E6414584C77A70E3E3FB223A2BA', NULL, '', 'Schulz-Kroenert', 'Nils', 14009, 'E332586D9B0344D4BFCEF66152F334C0', 14009, 14009, '2003', '2003-11-18', 'm', 42, 1),
(23675, 'CC565DC83E184BEFA4FA71D49454113D', NULL, '', 'Vasilonoks', 'Arturs', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '1998', '1998-01-01', 'm', 99, 1),
(23725, 'A8F6935820BB43F293D9CDF6241DDBC0', NULL, '', 'Piatkowski', 'Mateusz', 14009, 'E332586D9B0344D4BFCEF66152F334C0', 14009, 14009, '1985', '1985-06-20', 'm', 42, 1),
(23729, 'E3554917B0D9467D82E78D3D21BDC340', NULL, '', 'Nowara', 'Caroline', 9014, 'A912DB3A59FC47B9A5578073856AAAA8', 9014, 9014, '1990', '1990-11-01', 'w', 42, 1),
(23768, '9CBBFA30A30A418BAFA509E6B3F24D9F', NULL, '', 'Ardalsbakke', 'Marit', 2003, 'B515D63CC1074ECB9D5C0D3962A05680', 2003, 2003, '1992', '1992-05-11', 'w', 42, 1),
(23816, 'BC581B8AA1A643EB9391601EC9804319', NULL, '', 'Firl', 'Judith', 14005, '37668537FC6147F59E03477C6788809F', 14005, 14005, '2003', '2003-01-01', 'w', 42, 1),
(23832, '0C94D92168F74D75B3E3A28D4DB4C95F', NULL, '', 'Kasperl', 'Johannes', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '1994', '1994-01-01', 'm', 42, 1),
(23839, '0B7878439A2F479BB61855C549CA1739', NULL, '', 'Poitschke', 'Miko-Leander', 14010, '1FC24AEE6E3F4A59A9A4D4779B6AE767', 14010, 14010, '2008', '2008-12-05', 'm', 42, 1),
(23852, '72B3DA0BDB154478988C22C712F88A66', NULL, '', 'Börner', 'Arno', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '2007', '2007-01-01', 'm', 42, 1),
(23853, 'F32216A362DC4CF4BFD52474FC9A64AE', NULL, '', 'Stieß', 'Julia', 9010, '728BCFEBAEE94E56A0AA57156D1AE0B1', 9010, 9010, '1993', '1993-05-25', 'w', 42, 1),
(23855, '1988B8F7D9B84B51A8E53955229F0C2D', NULL, '', 'Tong', 'Jenny', 0, '037CDAC962F34E64B6D701A596355DF3', 0, 100000, '1996', '1996-01-01', 'w', 205, 1),
(23863, 'B6D5115D63D244F39D5BC525A24C0733', NULL, '', 'Rach', 'Sarah', 9003, 'F882AB2802C541DC8C8E26654E128418', 9003, 9003, '2008', '2008-12-16', 'w', 42, 1),
(23911, '49673AD6F8E641D1A21C687B517FDB07', NULL, '', 'Beimel', 'Theo', 2016, 'A41D3F4B6B8340FFA559473D3BA66183', 2016, 2016, '2002', '2002-08-04', 'm', 42, 1),
(23932, '3B31FBD7523D4B71A64CDD9182FB2ED3', NULL, '', 'Bespalov', 'Gerasim', 9004, '28B6BA97A27C4D1685A55E462F588D03', 9004, 9004, '2007', '2007-03-31', 'm', 42, 1),
(24016, '0C0121D873ED4438BE64E9B822DC3320', NULL, '', 'Keller', 'Theresia', 9002, 'C457BA6948FE415CAF879BE0F635D14B', 9002, 9002, '2004', '2004-05-19', 'w', 42, 1),
(24061, '2D8DB7D176C74F5296E8DC75444F0809', NULL, '', 'Rach', 'Simon', 9003, 'F882AB2802C541DC8C8E26654E128418', 9003, 9003, '2010', '2010-09-01', 'm', 42, 1),
(24062, '39B043E608FA4D9684580A61F0E55044', NULL, '', 'Rach', 'Lukas', 9003, 'F882AB2802C541DC8C8E26654E128418', 9003, 9003, '2010', '2010-09-01', 'm', 42, 1),
(24101, '98CF7ECBEAFF4CD9B9F9AC71B4F008E6', NULL, '', 'Meyer-Buchhardt', 'Nic', 13002, '84AD09BB4714415F80B7BA081B9FB4D6', 13002, 13002, '2006', '2006-09-01', 'm', 42, 1),
(24119, 'B7603C5AB89941B7B8D1D57C21475702', NULL, '', 'Abdo', 'Roshine', 9002, 'C457BA6948FE415CAF879BE0F635D14B', 9002, 9002, '2008', '2008-05-10', 'w', 42, 1),
(24235, '1BF3A01F50724A4A8386936507D1FD5A', NULL, '', 'Michel', 'Marlene', 9002, 'C457BA6948FE415CAF879BE0F635D14B', 9002, 9002, '2006', '2006-07-28', 'w', 42, 1),
(24248, '750B32A233CE460D890552EE0E433E8C', NULL, '', 'Teichert', 'Anthony', 14008, '0D3D021A79F44069917CFF2D9BDC045F', 14008, 14008, '2005', '2005-08-29', 'm', 42, 1),
(24250, '719BE12F156E4FC9AB616B153BCE952B', NULL, '', 'Weber', 'Sabine', 13004, '08604513AC4F4B04B833777BFAC71F64', 13004, 13004, '1966', '1966-03-14', 'w', 42, 1),
(24301, '58E6F415402C4E1CB89958629FF46E84', NULL, '', 'Nuber', 'Maximilian', 9010, '728BCFEBAEE94E56A0AA57156D1AE0B1', 9010, 9010, '1991', '1991-11-25', 'm', 42, 1),
(24302, '34EFCB181F4D423B83C41EE24EC44488', NULL, '', 'Fechner', 'Marc', 9010, '728BCFEBAEE94E56A0AA57156D1AE0B1', 9010, 9010, '1986', '1986-01-23', 'm', 42, 1),
(24319, '55CE408E7C594F2A9B22DC550C7932B9', NULL, '', 'Moussaoui', 'Ayoub', 9014, 'A912DB3A59FC47B9A5578073856AAAA8', 9014, 9014, '1989', '1989-03-15', 'm', 42, 1),
(24324, '4DBC1BEDBD3E437483877143E8697FA6', NULL, '', 'Schäfer', 'Tom', 9003, 'F882AB2802C541DC8C8E26654E128418', 9003, 9003, '1990', '1990-09-13', 'm', 42, 1),
(24325, '10EA212B57C847A68842F5617F5701A9', NULL, '', 'Zickgraf', 'Fabian', 9010, '728BCFEBAEE94E56A0AA57156D1AE0B1', 9010, 9010, '2007', '2007-07-14', 'm', 42, 1),
(24329, 'BA19E7148F2A4B98AE35FA76FFA6AD88', NULL, '', 'Goljasz', 'Daniel', 0, '04B61F2CA6F743F897BFDFDCC2A48712', 0, 0, '1997', '1997-08-14', 'm', 147, 1),
(24402, 'C7C31ADB014C47BA8486656C4E2593DB', NULL, '', 'Dimov', 'Ivan', 14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 14007, 14007, '2002', '2002-01-01', 'm', 29, 1),
(100001, NULL, NULL, '', 'Wissing', 'Nele', 14008, NULL, 14008, 147, '2005', '2005-01-01', 'w', 42, 1),
(100002, NULL, NULL, '', 'Neukirchner', 'Laura', 14008, NULL, 14008, 147, '2005', '2005-01-01', 'w', 42, 1),
(100003, NULL, NULL, '', 'Weißgerber', 'Tim', 14016, NULL, 14016, 14016, '2012', '2012-01-01', 'm', 42, 1),
(100006, NULL, NULL, '', 'Ahmed Mahmoud', 'Mohamed Ihab Youssef', 2009, NULL, 9007, 9007, '1989', '1989-01-01', 'm', 42, 1),
(100014, NULL, NULL, '', 'Todor', 'Yordanov', 2003, NULL, 2003, 2003, '2001', '2001-01-01', 'm', 42, 1),
(100023, NULL, NULL, '', 'Tinney', 'Hanna-Luise', 14009, NULL, 14009, 14009, '1994', '1994-05-20', 'w', 42, 1),
(100027, NULL, NULL, '', 'Neumann', 'Mandy', 14005, NULL, 14005, 14005, '1995', '1995-01-01', 'w', 42, 1),
(100028, NULL, NULL, '', 'Schiffmann', 'Luisa', 14016, NULL, 14016, 14016, '1996', '1996-01-01', 'w', 42, 1),
(100033, NULL, NULL, '', 'Kulke', 'Leif', 14011, NULL, 14011, 14011, '2007', '2007-01-01', 'm', 42, 1),
(100034, NULL, NULL, '', 'Hanisch', 'Dave', 14011, NULL, 14011, 14011, '2007', '2007-01-01', 'm', 42, 1),
(100036, NULL, NULL, '', 'Nietzschke', 'Moritz', 14005, NULL, 14005, 14005, '2005', '2005-01-01', 'm', 42, 1),
(100044, NULL, NULL, 'Dr.', 'Faber', 'Friedrich', 14005, NULL, 14005, 14005, '1940', '1940-01-01', 'm', 42, 1),
(100047, NULL, NULL, '', 'Brodi', 'Anton', 14008, NULL, 14008, 14008, '2003', '2003-01-01', 'm', 42, 1),
(100048, NULL, NULL, '', 'Schindler', 'Philipp', 14005, NULL, 14005, 14005, '1994', '1994-01-01', 'm', 42, 1),
(100064, NULL, NULL, '', 'Lagrou', 'Ilke', 0, NULL, 0, 0, '1998', '1998-01-27', 'w', 19, 1),
(100067, NULL, NULL, '', 'Miljukow', 'Olga', 14004, NULL, 14004, 14004, '1991', '1991-06-19', 'w', 42, 1),
(100068, NULL, NULL, '', 'Keskitalo', 'Hannes', 0, NULL, 0, 0, '1996', '1996-11-10', 'm', 52, 1),
(100069, NULL, NULL, '', 'Nykänen', 'Jesse', 0, NULL, 0, 0, '1996', '1996-02-06', 'm', 52, 1),
(100070, NULL, NULL, '', 'Rechul', 'Emilia', 0, NULL, 0, 0, '1998', '1998-08-03', 'w', 147, 1),
(100071, NULL, NULL, '', 'Luterek', 'Klaudia', 0, NULL, 0, 0, '1999', '1999-02-20', 'w', 147, 1),
(100072, NULL, NULL, '', 'Augusto', 'Catarina', 0, NULL, 0, 0, '1994', '1994-11-12', 'w', 148, 1),
(100073, NULL, NULL, '', 'Peinado', 'Laura', 0, NULL, 0, 0, '1992', '1992-10-21', 'w', 202, 1),
(100074, NULL, NULL, '', 'Lugo', 'Wilkeinner', 0, NULL, 0, 0, '1997', '1997-03-25', 'm', 202, 1),
(100075, NULL, NULL, '', 'Vandenabeele', 'Annelien', 0, NULL, 0, 0, '2004', '2004-11-26', 'w', 19, 1),
(100076, NULL, NULL, '', 'Gude', 'Line', 0, NULL, 0, 0, '1998', '1998-02-19', 'w', 39, 1),
(100078, NULL, NULL, '', 'Peltokangas', 'Antti', 0, NULL, 0, 0, '1995', '1995-05-20', 'm', 52, 1),
(100079, NULL, NULL, '', 'Suoniemi', 'Konsta', 0, NULL, 0, 0, '2004', '2004-06-10', 'm', 52, 1),
(100082, NULL, NULL, '', 'Chirinos', 'Dayana', 0, NULL, 0, 0, '1991', '1991-10-04', 'w', 202, 1),
(100083, NULL, NULL, '', 'Uzcategui', 'Rosselyn', 0, NULL, 0, 0, '2002', '2002-07-11', 'w', 202, 1),
(100084, NULL, NULL, '', 'Mayora', 'Julio', 0, NULL, 0, 0, '1996', '1996-09-02', 'm', 202, 1),
(100086, NULL, NULL, '', 'Selin', 'Jutta', 0, NULL, 0, 0, '1999', '1999-06-01', 'w', 52, 1),
(100087, NULL, NULL, '', 'Kudłaszyk', 'Piotr', 0, NULL, 0, 0, '1999', '1999-05-29', 'm', 147, 1);
INSERT INTO `athleten` (`idTeilnehmer`, `GUID`, `Startbuch_Nummer`, `Titel`, `Nachname`, `Vorname`, `Verein`, `Verein_GUID`, `ESR`, `MSR`, `Jahrgang`, `Geburtstag`, `Geschlecht`, `Staat`, `Lizenz`) VALUES
(100088, NULL, NULL, '', 'Almeida', 'Miguel', 0, NULL, 0, 0, '1999', '1999-07-09', 'm', 148, 1),
(100090, NULL, NULL, '', 'Vallenilla', 'Keydomar', 0, NULL, 0, 0, '1999', '1999-10-08', 'm', 202, 1),
(100091, NULL, NULL, '', 'Castro', 'Darvin', 0, NULL, 0, 0, '1995', '1995-06-11', 'm', 202, 1),
(100092, NULL, NULL, '', 'Quintana', 'Rosielis', 0, NULL, 0, 0, '2000', '2000-03-18', 'w', 202, 1),
(100093, NULL, NULL, '', 'Thornton', 'Tenishia', 0, NULL, 0, 0, '2005', '2005-09-01', 'w', 111, 1),
(200000, NULL, NULL, '', 'Kirchbauer', 'Peter', 10003, NULL, 10003, 10003, '1948', '1948-08-25', 'm', 42, 1),
(200001, NULL, NULL, '', 'Joos', 'Horst', 10003, NULL, 10003, 10003, '1940', '1940-02-21', 'm', 42, 1),
(200006, NULL, NULL, '', 'Braun', 'Lukas', 13004, NULL, 13004, 13004, '1995', '1995-04-20', 'm', 42, 1),
(200007, NULL, NULL, '', 'Werner', 'Anna', 13004, NULL, 13004, 13004, '1998', '1998-10-13', 'w', 42, 1),
(200011, NULL, NULL, '', 'Wolf', 'Jens', 9005, NULL, 9005, 9005, '1995', '1995-03-08', 'm', 42, 1),
(200018, NULL, NULL, '', 'Kihm', 'Mattis', 9007, NULL, 9007, 9007, '2010', '2010-11-29', 'm', 42, 1),
(200019, NULL, NULL, '', 'Lorenz', 'Jakob', 9007, NULL, 9007, 9007, '2010', '2010-07-02', 'm', 42, 1),
(200020, NULL, NULL, '', 'Kihm', 'Jaron', 9007, NULL, 9007, 9007, '2007', '2007-10-16', 'm', 42, 1),
(200029, NULL, NULL, '', 'Stiefel', 'Christoph', 9010, NULL, 9010, 9010, '1989', '1989-04-17', 'm', 42, 1),
(200031, NULL, NULL, '', 'Jess', 'Andreas', 9010, NULL, 9010, 9010, '1988', '1988-07-29', 'm', 42, 1),
(200033, NULL, NULL, '', 'Kaufmann', 'Luis', 9013, NULL, 9013, 9013, '2009', '2009-04-22', 'm', 42, 1),
(200035, NULL, NULL, '', 'Beutling', 'Volker', 9013, NULL, 9013, 9013, '1965', '1965-11-01', 'm', 42, 1),
(200036, NULL, NULL, '', 'Tröller', 'Jennifer', 9013, NULL, 9013, 9013, '1998', '1998-12-31', 'w', 42, 1),
(200043, NULL, NULL, '', 'Bach', 'Lina', 9002, NULL, 9002, 9002, '1997', '1997-12-08', 'w', 42, 1),
(200053, NULL, NULL, '', 'Merkel', ' Andreas', 20542, NULL, 20542, 20542, '2003', '2003-03-20', 'm', 42, 1),
(200057, NULL, NULL, '', 'Blang', 'Nico', 9014, NULL, 9014, 9014, '1992', '1992-04-18', 'm', 42, 1),
(200060, NULL, NULL, '', 'Shahnavazi', 'Amir', 9014, NULL, 9014, 9014, '1997', '1997-05-25', 'm', 42, 1),
(200069, NULL, NULL, '', 'Bönig', 'Finn', 9004, NULL, 9004, 9004, '2009', '2009-12-01', 'm', 42, 1),
(200073, NULL, NULL, '', 'Schlampp', 'Yvo', 9002, NULL, 9002, 9002, '2002', '2002-11-20', 'm', 42, 1);
"
        End Function

        Private Function Insert_Vereine() As String
            Return "
INSERT INTO teams (idTeam, idVerein) VALUES
(147, 14008),
(147, 14016),
(148, 14003),
(148, 14010);

INSERT INTO verein (idVerein, GUID, Vereinsname, Kurzname, Region, Staat, Adresse, PLZ, Ort, Team) VALUES
(147, NULL, 'Athletenteam Vogtland', 'AT Vogtland', 14, 42, '', '', '', 1),
(148, NULL, 'Kampfgemeinschaft Görlitz-Zittau', 'KG Görlitz-Zittau', 14, 42, NULL, NULL, NULL, 1),
(1001, NULL, 'TSV 1946 Altenberg u. Umgebung e. V.', NULL, 9, 42, 'Blumenstraße 5', '91126', 'Kammerstein', 0),
(1002, NULL, 'SV Gold-Blau Augsburg e.V.', NULL, 9, 42, 'Unterer Talweg 100', '86179', 'Augsburg', 0),
(1005, '8B768D81A83248D4B36F278400F1F45C', 'TSG 1885 Augsburg  e. V.', NULL, 9, 42, 'Schillstraße 105-109', '86169', 'Augsburg', 0),
(1007, 'F9659EEEC4664AA5AF65E89275A69969', '1. Athletik-Club 1954 Bayreuth e.V.', NULL, 9, 42, 'Sickenreuther Str. 31', '95497', 'Goldkronach', 0),
(1008, '4532B38A186141498D9C221336C77C4F', 'TSV Burgau e. V.', NULL, 9, 42, 'An der Linde 3', '89364', 'Rettenbach', 0),
(1010, 'E03C695C8FBE4F99ABBFA9EA1B87A5C1', 'Kraftsportclub Attila Dachau e.V.', NULL, 9, 42, 'Glonnblick 7', '85258', 'Weichs', 0),
(1012, 'DE5171A1620C4A5CB42EB1C566735EFC', 'Eichenauer Sportverein e.V.', NULL, 9, 42, 'Hauptstraße 60', '82223', 'Eichenau', 0),
(1013, 'B6902D53AB5245169CD2363A44BCE8EB', 'TSV 1862 Erding - Abt. Gewichtheben e. V.', NULL, 9, 42, 'Freisinger Straße 64', '85435', 'Erding', 0),
(1017, 'EB5EA7B4264A41A697849BC8D0921836', 'TSV Forstenried e. V.', NULL, 9, 42, 'Graubündenerstraße 100', '81475', 'München', 0),
(1025, NULL, 'SSV Höchstädt', NULL, 9, 42, 'Kirchgasse 2', '89420', 'Höchstädt', 0),
(1028, 'B4A705C9B81A4DDAB55A5084AD42FA73', 'TSV Ingolstadt-Nord 1897/1913 e. V.', NULL, 9, 42, 'Fauststraße 29', '85051', 'Ingolstadt', 0),
(1030, 'CCF97919A5094D72B590B47C0CD3EC94', 'Athletik-Club Kaufbeuren e. V.', NULL, 9, 42, 'Bleichanger 7', '87600', 'Kaufbeuren', 0),
(1032, '536A606A75B04F59BBA3DB5AB56EF720', 'Kraftsportverein 1894/96 Kitzingen e.V.', NULL, 9, 42, 'Im Tännig 36 B', '97320', 'Mainstockheim', 0),
(1033, NULL, 'SV - DJK  Kolbermoor e. V.', NULL, 9, 42, 'Willinger Str. 8', '83043', 'Bad Aibling', 0),
(1036, 'E31560F35DB54C128EE5CC0006140799', 'STC Bavaria 20 Landshut e. V.', NULL, 9, 42, 'Sandstr. 35 a', '84036', 'Landshut', 0),
(1037, 'B4BD5F1E247E479399EE876B038CDA01', 'Turngemeinde Landshut v. 1861 e. V.', NULL, 9, 42, 'Sandnerstraße 7', '84034', 'Landshut', 0),
(1040, '37DF1B72D037483796FBCD977BE39622', 'ESV-Freimann München e.V.', NULL, 9, 42, 'Frankplatz 15', '80939', 'München', 0),
(1041, '25C27C48DB654D4D858C4C8E6D413A74', 'ESV München-Ost e.V.', NULL, 9, 42, 'Baumkirchner Straße 57', '81673', 'München', 0),
(1042, NULL, 'SC Armin München von 1893 e. V.', NULL, 9, 42, 'Max-von-Gruber-Str. 3', '80804', 'München', 0),
(1043, '3B3E86D0D4E141B1AD231862F047AAC2', 'SC München von 1906 e. V.', NULL, 9, 42, 'St.-Martin-Str. 35 a', '81541', 'München', 0),
(1046, '604FD9B6B5244E50B8A14F547532F91E', 'ESV München-Neuaubing e. V.', NULL, 9, 42, 'Papinstraße 22', '81249', 'München', 0),
(1048, '2D52A2FB61874B64AF0B1CE54F8BAD3D', 'ASV 1860 Neumarkt i.d.OPf. e. V.', NULL, 9, 42, 'Deininger Weg 78', '92318', 'Neumarkt', 0),
(1049, 'D86DD378809C4B83ACA758EF855B8DC6', 'ASV 1897 Neu-Ulm e. V.', NULL, 9, 42, 'Hausenerstr. 32/2', '89233', 'Gerlenhofen/Neu-Ulm', 0),
(1053, NULL, 'Athletik-Sportverein 1893 Passau e.V.', NULL, 9, 42, 'Eckerichweg 1', '94136', 'Thyrnau', 0),
(1060, '953359B51F6E4E178AFA6D728DF7AED8', 'TuS Raubling e. V.', NULL, 9, 42, 'Buchenweg 3', '83126', 'Flintsbach', 0),
(1061, 'FF04C18678524ACC90C719B23939626A', 'TSV Regen e.V.', NULL, 9, 42, 'V.d.k Str. 13', '94209', 'Regen', 0),
(1062, '17CD02D7AB8648F48C787A11EF4D6A23', 'KSV Bavaria Regensburg e. V.', NULL, 9, 42, 'Albrecht-Altdorfer-Ring 24', '93083', 'Obertraubling', 0),
(1064, '9F4A772E29354997B659178ADB8CDE14', '1. AC Regensburg e.V.', NULL, 9, 42, 'Frobenius-Forster-Straße  1a', '93055', 'Regensburg', 0),
(1065, '75FC4094E98C4EA7870DD0AE7724E9A5', 'TB 03 Roding e. V.', NULL, 9, 42, 'Kaitersbergstr. 2', '93426', 'Roding', 0),
(1066, NULL, 'TSV Röthenbach eV', NULL, 9, 42, 'Bachstraße 12', '91233', 'Neunkirchen', 0),
(1067, NULL, 'TSV 1880 Schwandorf e. V.', NULL, 9, 42, 'Wackersdorfer-Str. 88', '92421', 'Schwandorf', 0),
(1069, 'F35FF0161DCF427F8783C585D626BBB7', 'Athleten - Club 82 Schweinfurt e. V.', NULL, 9, 42, 'Am Marienberg 22', '97490', 'Poppenhausen', 0),
(1070, NULL, 'AC Olympia Schrobenhausen v.1895', NULL, 9, 42, 'Katharinenweg 9b', '86529', 'Schrobenhausen', 0),
(1072, 'BBEBD30B7820482EA38FBA4C5E74F679', 'TSV Waldkirchen e. V.', NULL, 9, 42, 'Kirchensteig 11', '94065', 'Waldkirchen', 0),
(1073, '0AD370587D694E54A0974C741E40A88E', '1. Athletenclub 1897 Weiden', NULL, 9, 42, 'Bonhoefferstrasse 1', '92708', 'Mantel', 0),
(1075, NULL, 'ASV 81 Würzburg e. V.', NULL, 9, 42, 'Mittlerer Katzenbergweg 15', '97084', 'Würzburg', 0),
(1096, 'EA514FEC55164A049A19EEEC0E7C2AB8', 'Kraftmühle Würzburg e.V. ', NULL, 9, 42, 'Nürnbergerstraße 84', '97076', 'Würzburg', 0),
(1097, '20388E1EF1E74D8986B4137EC7201DE6', 'ASC Boxdorf', NULL, 9, 42, 'Boxdorfer Hauptstraße 37a', '90427', 'Nürnberg', 0),
(2001, NULL, 'TSG Backnang 1920 e. V.', NULL, 8, 42, 'Danziger Straße 24', '71522', 'Backnang', 0),
(2002, NULL, 'GV Donaueschingen e.V.', NULL, 8, 42, 'Postafch 1906', '78166', 'Donaueschingen', 0),
(2003, 'B515D63CC1074ECB9D5C0D3962A05680', '1. KSV 1896 Durlach e.V.', 'KSV Durlach', 8, 42, 'Lenzenhubweg 14', '76205', 'Karlsruhe', 0),
(2005, '2C194BEB50AB4F87A3B21EB2039BCBFD', 'GV Eisenbach 1976 e.V.', NULL, 8, 42, 'Thomaweg 11', '79871', 'Eisenbach', 0),
(2006, '37A8823F327C460B952DAD3FD9CE1514', 'Turnverein Feldrennach 1896 e. V.', NULL, 8, 42, 'Kirchstraße 12', '75334', 'Straubenhardt', 0),
(2007, 'B90FAFD5C44641A2AB8FFFBFAFCAFD28', 'SV Fellbach 1890 e. V.', NULL, 8, 42, 'Schillerstraße 8', '70734', 'Fellbach', 0),
(2008, '911DC0ED59FE425489707384F8DEF949', 'Sportverein Flözlingen 1920 e. V.', NULL, 8, 42, 'Schellenwasen 9', '78667', 'Villingendorf', 0),
(2009, '336A9B7E9B3B41D8AD11A53F10E6CBDA', 'Athleten Club 1978 e.V. Forst', NULL, 8, 42, 'Paulusstr.16', '76694', 'Forst', 0),
(2012, NULL, 'KSV 1898 e.V. St. Georgen e. V.', NULL, 8, 42, 'Rossbergstraße 20', '78112', 'St. Georgen', 0),
(2014, NULL, 'SG Heidelberg-Kirchheim e. V.', NULL, 8, 42, 'Wörthstraße 1', '69115', 'Heidelberg', 0),
(2015, '7F0FFAF94E3C43A18F6DDEAC2FBDE1DF', 'TSV Heinsheim e. V.', NULL, 8, 42, 'Neckarstraße 1', '74906', 'Bad Rappenau', 0),
(2016, 'A41D3F4B6B8340FFA559473D3BA66183', 'AC Germania St. Ilgen e.V.', NULL, 8, 42, 'Wilhelm-Röntgen-Weg 1', '69181', 'Leimen', 0),
(2017, '9971BAFE4F4E40A9A5276AE1F8FDE1F7', 'ASV 1901 Ladenburg e.V.', NULL, 8, 42, 'Hinterer Rindweg 125', '68526', 'Ladenburg', 0),
(2018, '29B74844D58D422DA0446262F5ACA288', 'SV 08 Laufenburg e. V. Abt.Gewichtheben', NULL, 8, 42, 'Langmattstraße 32', '79730', 'Murg', 0),
(2019, '786E972AD819439BB98C4A511A7B2789', 'KSV Lörrach 1902 e. V.', NULL, 8, 42, 'Materaweg 16', '79576', 'Weil am Rhein', 0),
(2020, '1BB02268D3ED4992BBDDE279392559A0', 'SV Magstadt 1897 e. V.', NULL, 8, 42, 'Zeilweg 15/1', '71106', 'Magstadt', 0),
(2021, 'EEE370B6AE2F4F538ABCD52E0310901F', 'KSV 1884 Mannheim e.V.', NULL, 8, 42, 'Theodor-Heuss-Anlage 17', '68165', 'Mannheim', 0),
(2025, NULL, 'TV 1884 Neckarau e. V.', NULL, 8, 42, 'Luisenstraße 41', '68199', 'Mannheim', 0),
(2027, NULL, 'TV 1877 Waldhof-Mannheim e.V. ', NULL, 8, 42, 'Deutsche Gasse, 6a', '68307', 'Mannheim', 0),
(2028, 'F04AEDB793554DDBA5D548083DBA35F3', 'TV Mengen 1863 e. V.', NULL, 8, 42, 'Hinterdorfstraße 13', '88512', 'Mengen', 0),
(2030, 'E3E8B4EF02884E6B8FED83F12E5712D5', 'AC 1894 e.V. Neulußheim e.V.', NULL, 8, 42, 'Goethestraße 65', '68804', 'Altlußheim', 0),
(2031, '0857094B6A6A4B5DB99E9D664F7ED002', 'SGV Böbingen 1920 e. V.', NULL, 8, 42, 'Bucher Straße 17', '73560', 'Böbingen', 0),
(2032, NULL, 'KSV Germania Offenburg 1891 e. V.', NULL, 8, 42, 'C.-Robert-Dold-Str. 1', '77654', 'Offenburg', 0),
(2033, 'FCD297C0B0C14544B09D9CB9130E6AC3', 'SV Germania Obrigheim e. V.', 'SV Obrigheim', 8, 42, 'Im Valtert 37', '74847', 'Obrigheim', 0),
(2034, '6677BC18181C423C931EB0A0B7F5FE26', 'Sportclub Pforzheim e. V.', NULL, 8, 42, 'Markgrafenstr. 19b', '75177', 'Pforzheim', 0),
(2035, '86278CAD25B64D89B8A826421AB9ED6B', 'Hebergemeinschaft Rastatt 1972 e. V.', NULL, 8, 42, 'Hansjakobstrasse 25', '76437', 'Rastatt', 0),
(2036, '30C3D8868BB8478BA61A3F9269E99FFB', 'TSV Rettigheim 1902 e.V.', NULL, 8, 42, 'Kraichgauweg, 34', '69234', 'Dielheim', 0),
(2038, NULL, 'VfL Sindelfingen e. V.', NULL, 8, 42, 'Pfarrwiesen Allee 5 ', '71067', 'Sindelfingen', 0),
(2041, 'AB68D6E9494549E4B1A064DD5C52DFE5', 'ASV 1897 Tuttlingen e.V.', NULL, 8, 42, 'Bischofszellerstr. 3', '78532', 'Tuttlingen', 0),
(2045, '1B3FD10EC13E47158FEF3A4EC41C4451', 'Athletik Club 1892 Weinheim e. V.', NULL, 8, 42, 'Waidallee 8', '69469', 'Weinheim', 0),
(2046, NULL, 'TV Schwäbisch Gmünd-Wetzgau 1920 e. V.', NULL, 8, 42, 'Asternweg 3', '73527', 'Schwäbisch Gmünd', 0),
(2047, NULL, 'Turn- und Sportverein Freiheit  e.V. Hert', 'TSV Hert', 8, 42, 'Postfach 4034', '79611', 'Rheinfelden', 0),
(2048, '433805DEC1914E819FB6F1EB0EE46B01', 'Kraft-Werk Schwarzach e.V.', NULL, 8, 42, 'Tonwerkstraße 1', '74869', 'Schwarzach', 0),
(2049, 'EF999F1A4F08451FAE0F300A0B286A0C', 'Turnverein Eintracht Diersburg e. V.', NULL, 8, 42, 'Ölerstraße 28', '77749', 'Hohberg', 0),
(2050, '56C20DCF28264FB1B5476673DD101820', 'Sportverein Freiburg-Haslach 1895 e.V. ', NULL, 8, 42, 'Bauhöferstrasse 73', '79115', 'Freiburg im Breisgau', 0),
(2051, '7D7785FCDC10468983173044EDCED36E', 'Turnverein Ettenheim e.V.', NULL, 8, 42, 'Im Pfaffenbach 5', '77955', 'Ettenheim', 0),
(3001, NULL, 'Athletik Club Heros Berlin e.V.', NULL, 11, 42, 'Karlsbader Straße 9', '14193', 'Berlin', 0),
(3009, '525195E25E9148CC9071D802318844FB', 'SV Empor Berlin e. V.', NULL, 11, 42, 'Cantianstr. 24', '10437', 'Berlin', 0),
(3010, NULL, 'SG Anton Saefkow 83', NULL, 11, 42, 'Alfred-Jung-Straße 18', '10369', 'Berlin', 0),
(3011, NULL, 'SV Blau-Gelb Berlin e. V.', NULL, 11, 42, 'Paul-Junius-Strasse 31', '10369', 'Berlin', 0),
(3013, '49ACF8809DC04B4CBB8116A953F79B7C', 'Berliner Turn- und Sportclub e.V.', NULL, 11, 42, 'Paul-Heyse-Straße 25', '10407', 'Berlin', 0),
(4001, NULL, 'Freie Turner Blumenthal e.V. zHd Werner Kucht', NULL, 4, 42, 'Auf der Ahnte 6', '28779', 'Bremen', 0),
(5001, NULL, 'BKSV Hamburg e.V.', NULL, 2, 42, 'Osterbekstraße 96', '22083', 'Hamburg', 0),
(5002, NULL, 'Sportklub Hansa Germania von 1881 Hamburg e. ', NULL, 2, 42, 'Solferinostraße 55', '22417', 'Hamburg', 0),
(5006, NULL, 'Athletenclub Hamburg e.V.', NULL, 2, 42, 'Osterbekstraße 96', '22083', 'Hamburg', 0),
(5007, '870CC00ABC9240BA9B08D5396C0A7FB9', 'Scoop e.V. ', NULL, 2, 42, 'Schlankreye 63', '20144', 'Hamburg', 0),
(5008, 'A8F11CFD2C06424C8C7D9583339902B5', 'Athletik Hamburg e.V', NULL, 2, 42, 'Stubbenhof 16', '21147', 'Hamburg', 0),
(6002, NULL, 'AC Germania Aschaffenburg-Schweinheim', NULL, 9, 42, 'Rat-Scholz Straße 4', '63768', 'Hösbach', 0),
(6003, '8AB889F41A364F9F895888E743569D81', 'GSV Eintracht Baunatal e.V.', NULL, 6, 42, 'Stettiner Straße 4', '34225', 'Baunatal', 0),
(6007, '5805223528E848FBBD8CEFBBAB97DCFD', 'Turnverein 1898 e.V. Elz - Abtl. Gewichtheben', NULL, 6, 42, 'Siemensstraße 28', '65549', 'Limburg', 0),
(6009, '25ADDD3FFB5D4CB685C405AA16C0A0EB', 'PSV Blau Gelb Fulda 1934/61 e. V.', NULL, 6, 42, 'Hermann-Kirchhoff-Weg 4', '36093', 'Künzell', 0),
(6011, '73874B61496C4A608158AF955AD0F6B3', 'AV Groß-Zimmern', NULL, 6, 42, 'Johannes-Ohl-Straße 8', '64846', 'Groß-Zimmern', 0),
(6012, NULL, 'TV Heppenheim e. V.', NULL, 6, 42, 'Siegfriedstraße 345', '64646', 'Heppenheim', 0),
(6013, 'ECD0DEBEC0414F5DA9421350774855FF', 'SAV Kassel 1993 e. V.', NULL, 6, 42, 'Rengershäuser Str. 26', '34132', 'Kassel', 0),
(6015, '2FA2DE920A9E4F808379013AC3DFF3D9', 'KSV 1959 Langen e.V.', NULL, 6, 42, 'Zimmerstraße 1.', '63225', 'Langen', 0),
(6018, 'C170F64B44DD412C9E62C4C6726F0FFB', 'FTG 1900 Pfungstadt e. V. Abt. Gewichtheben', NULL, 6, 42, 'Weserstraße 5', '64319', 'Pfungstadt', 0),
(6021, NULL, 'SV Athletia Wiesbaden 1892 ', NULL, 6, 42, 'Unter den Buchen 6', '65817', 'Eppstein', 0),
(6022, 'FC210F95DCD8437DA262499E3752CA3B', 'ASC 1906 Zeilsheim e.V.', NULL, 6, 42, 'Blauwiesenweg 95', '60439', 'Frankfurt am Main', 0),
(6048, NULL, 'Armwrestling Club Over the Top Hanau e.V.', NULL, 6, 42, 'Birkenstraße 40', '63549', 'Ronneburg', 0),
(7002, NULL, 'V.f.V. Braunschweig von 1898 e. V.', NULL, 3, 42, 'Güldenkamp 42A', '38108', 'Braunschweig', 0),
(7005, NULL, 'VfK Hannover v. 1903 e.V.', NULL, 3, 42, 'Hildesheimer Straße 105 ', '30173', 'Hannover', 0),
(7006, 'A9E037924D974A2AAB513720D74F2B8B', 'SC Lüchow v. 1861 e. V.', NULL, 3, 42, 'Schulweg 4', '29439', 'Lüchow', 0),
(7007, NULL, 'Vater Jahn Peine', NULL, 3, 42, 'Hollandsmühle 4', '31226', 'Peine', 0),
(7009, NULL, 'TSV Schwalbe Tündern v. 1911 e. V.', NULL, 3, 42, 'Gretchenbrink 16', '31789', 'Hameln', 0),
(7011, NULL, 'AV Eiche 09 Wolfenbüttel e. V.', NULL, 3, 42, 'Alter Winkel 5a  ', '38300', 'Wolfenbüttel', 0),
(7012, '2179396810684FBBBA10ACD7EF8D6E2F', 'VfL Wolfsburg e. V.', NULL, 3, 42, 'Elsterweg 5', '38446', 'Wolfsburg', 0),
(7021, NULL, 'Todtglüsinger Sportverein v. 1930 e.V.', NULL, 3, 42, 'Tostedter Straße 20', '21255', 'Todtglüsingen', 0),
(7034, 'E2CCD92E68C54A149F8919E9BED2AAC0', 'SV Quitt Ankum 1919 e.V.', NULL, 3, 42, 'Postfach 1102', '49572', 'Ankum', 0),
(7039, 'DE70F950C90D4B86AA3DCCFFCD6CEBDA', 'Sportvereinigung Gifhorn von 1912 e.V.', NULL, 3, 42, 'Wegenerring 26', '38524', 'Sassenburg', 0),
(7040, NULL, 'BSC Kenpokan e.V.', NULL, 3, 42, 'Jathostraße 11', '30163', 'Hannover', 0),
(7041, '827E393F48F2496D8186FBDC656EC868', 'Turn-Club Hameln von 1880 e.V. ', NULL, 3, 42, 'Rennacker 2', '31787', 'Hameln', 0),
(7829, NULL, 'FUNCTIONAL GYM Augsburg e.V. ', NULL, 9, 42, 'Partnachweg 1', '86165', 'Augsburg', 0),
(8001, '2574F3097DF24DC6893D6225D5A82443', 'Bielefelder TG v. 1848 e.V.', NULL, 5, 42, 'Am Brodhagen 54', '33613', 'Bielefeld', 0),
(8006, NULL, 'KSV Bochum 1898 e.V. c/o Philipp Steinke', NULL, 5, 42, 'Korbstück, 16', '44894', 'Bochum', 0),
(8008, 'FD572660B8F5435D9A099971378528C9', 'SuS Dortmund Derne e. V.', NULL, 5, 42, 'Flughafenstraße 622', '44329', 'Dortmund', 0),
(8009, NULL, 'VfL Duisburg-Süd 1920 e.V.', NULL, 5, 42, 'Am Förkelsgraben 55', '47259', 'Duisburg', 0),
(8013, NULL, 'KSV 1920 Essen-Borbeck e.V.', NULL, 5, 42, 'Hugo-Knippen-Straße 3', '45357', 'Essen', 0),
(8014, NULL, 'DJK Sportfreunde Katernberg 13/19 e. V.', NULL, 5, 42, 'Husemannweg 4', '45327', 'Essen', 0),
(8015, '7277ACBBEA45490E9377AAC5E3716CD2', 'Kraftsportverein Essen 1888 e.V.', NULL, 5, 42, 'Am Kringel 15', '45277', 'Essen', 0),
(8019, '3EDA9F9740E64415BB2AF1AA502857B9', 'Athletik-Sport-Verein ADLER  e.V.', NULL, 5, 42, 'Radmacherstr. 3', '47574', 'Goch', 0),
(8020, NULL, 'Johannes Sarasa  / SSV  Hagen Gewichtheben e.', NULL, 5, 42, 'Ährenstr. 35', '58135', 'Hagen', 0),
(8021, NULL, 'AC Homberg 70 e.V. ', NULL, 5, 42, 'Dresdener-Ring  24', '47441', 'Moers', 0),
(8022, NULL, 'KSV Eiserfeld e.V.', NULL, 5, 42, 'Freiengründerstraße 52', '57080', 'Siegen', 0),
(8024, NULL, 'Kölner-Athleten-Club 1882 e. V. ', NULL, 5, 42, 'Schanzenstraße 6-20  (Spulenfabrik)', '51069', 'Köln', 0),
(8026, '31A02C520B984B599D4F3E5144A058E6', 'AC Goliath Mengede e.V.', NULL, 5, 42, 'Adalmundstraße 23', '44359', 'Dortmund', 0),
(8027, NULL, 'KSV Moers 1899 e.V.', NULL, 5, 42, 'Bonifatiusstraße 1', '47441', 'Moers', 0),
(8030, 'F8093574BB464F33A84CEAE3D21C961C', 'TV Rhede 1925 e. V.', NULL, 5, 42, 'Heidewg 29', '46414', 'Rhede', 0),
(8031, NULL, 'TV Jahn von 1879 e. V. Siegen', NULL, 5, 42, 'Diemstraße 12', '57072', 'Siegen', 0),
(8033, '635BF1CCC49C497B9F4D59AC72AB9CD8', 'SV 14/19 Westerholt e. V.', NULL, 5, 42, 'Flachsstraße 1a', '45896', 'Gelsenkirchen', 0),
(8035, '5FDB03E18CAF4F69B7F7A74C7D1AFCEA', 'SV Bayer Wuppertal e. V.', NULL, 5, 42, 'Paul-Kottsieper-Str. 30', '42899', 'Remscheid', 0),
(8036, '90F460E42CCE49389EFC513144EC1CE2', 'KSV 1896 Wuppertal e.V.', NULL, 5, 42, 'Wettinerstraße 76', '42287', 'Wuppertal', 0),
(8037, '03F32A5B5E43499AA213C8CD388D1726', 'Turnverein Eichen v. 1888 e.V.', NULL, 5, 42, 'Jahnstraße 11', '57223', 'Kreuztal', 0),
(8050, '0DAF8F46B1DE454FB6B0E2F631BFDF17', 'ATSV Espelkamp', NULL, 5, 42, 'Bultweg 6', '32369', 'Rahden', 0),
(8053, NULL, 'TV Röhlinghausen 1883 e. V.', NULL, 5, 42, 'Am Bollwerk 17', '44651', 'Herne', 0),
(8057, NULL, 'Athletic-Sport-Club Stemwede e.V.', NULL, 5, 42, 'Dielinger Str.33', '32351', 'Stemwede', 0),
(8063, '9EE2637124E843AB8B48D6F6D0F86B4E', 'Kraftsportverein Helios Simpelveld ', NULL, NULL, 130, 'Hoefblad 9      ( Postadresse )', '6418 ', 'Heerlen', 0),
(8067, NULL, 'Jugger Münster e.V.', NULL, 5, 42, 'Von-Esmarch-Straße 10', '48149', 'Münster', 0),
(8068, NULL, 'Jugger Wuppertal e.V.', NULL, 5, 42, 'Opphoferstraße 64', '42107', 'Wuppertal', 0),
(8069, NULL, '1. Jugger-Club Hagen', NULL, 5, 42, 'Lennestr. 63a', '58093', 'Hagen', 0),
(8070, NULL, 'Athletik Club Bochum e.V.', NULL, 5, 42, 'Harpener Heide 5', '44805', 'Bochum', 0),
(8071, NULL, 'Gewichtheberverein CF Lüdenscheid', NULL, 5, 42, 'Europa-Allee 50', '58515', 'Lüdenscheid', 0),
(9001, 'E38DA50A843741E491CA9EAB34975D61', 'Athleten-Club 1923 Altrip e.V.', 'AC Altrip', 7, 42, 'Ludwigsplatz 9', '67122', 'Altrip', 0),
(9002, 'C457BA6948FE415CAF879BE0F635D14B', 'KSV Grünstadt e. V.', 'KSV Grünstadt', 7, 42, 'Neugasse 17 A', '67269', 'Grünstadt', 0),
(9003, 'F882AB2802C541DC8C8E26654E128418', 'TSG Haßloch e. V.', 'TSG Haßloch', 7, 42, 'Ahornweg 14', '67454', 'Haßloch', 0),
(9004, '28B6BA97A27C4D1685A55E462F588D03', 'TSG 1861 Kaiserslautern e. V.', 'TSG Kaiserslautern', 7, 42, 'Hermann-Löns-Straße 25', '67663', 'Kaiserslautern', 0),
(9005, '32E55FD794C343D2B58C1E0A411A214F', 'Athletik-Club 1972 e.V. Kindsbach e. V.', 'AC Kindsbach', 7, 42, 'Hirtenpfad 27', '66862', 'Kindsbach', 0),
(9006, NULL, 'Kraftsportverein 1895 Mundenheim e. V.', NULL, 7, 42, 'Wasgaustraße 24', '67065', 'Ludwigshafen', 0),
(9007, '04B61F2CA6F743F897BFDFDCC2A48712', 'Athleten-Club 1892 Mutterstadt e.V.', 'AC Mutterstadt', 7, 42, 'Waldstraße 63', '67112', 'Mutterstadt', 0),
(9009, '32230541F9A3489EA9C2CB633C1581DB', 'VfL Rodalben e. V.', NULL, 7, 42, 'Fritz-Claus-Ring 15a', '66976', 'Rodalben', 0),
(9010, '728BCFEBAEE94E56A0AA57156D1AE0B1', 'Athletenverein 1903 e.V. Speyer', 'AV Speyer', 7, 42, 'Raiffeisenstr. 14', '67346', 'Speyer', 0),
(9013, '68D501EAA45F4E32B77FE56C4B2BE68D', 'Kraft-Sport-Club 07 Schifferstadt e.V.', 'KSC Schifferstadt', 7, 42, 'Albert-Einstein Allee 13b', '67117', 'Limburgerhof', 0),
(9014, 'A912DB3A59FC47B9A5578073856AAAA8', 'Kylltalheber Ehrang 1973 e. V.', 'KTH Ehrang', 7, 42, 'Friedhofstraße 45', '54293', 'Trier', 0),
(10003, NULL, 'Athleten-Club 09 e.V. Laubenheim e. V.', 'AC Laubenheim', 7, 42, 'Auf der Weide 11', '55130', 'Mainz-Laubenheim', 0),
(10004, NULL, 'AC 1904/20 e.V. Mainz-Weisenau e.V.', 'AC Weisenau', 7, 42, 'Portlandstrasse 26', '55130', 'Mainz', 0),
(12004, NULL, 'Preetzer Turn- u. Sportverein v. 1861 e. V.', NULL, 1, 42, 'Drosselweg 20', '24211', 'Preetz', 0),
(12008, NULL, 'Turn- und Sportvereinigung Gaarden von 1875 e', NULL, 1, 42, 'Röntgenstraße 5', '24143', 'Kiel', 0),
(12012, NULL, 'Sportverein Tungendorf Neumünster von 1911 e.', NULL, 1, 42, 'Süderdorfkamp 22', '24536', 'Neumünster', 0),
(13002, '84AD09BB4714415F80B7BA081B9FB4D6', 'KSV Hostenbach', NULL, 10, 42, 'Neustraße 8', '66787', 'Wadgassen', 0),
(13004, '08604513AC4F4B04B833777BFAC71F64', 'AC Heros Wemmetsweiler ', 'AC Wemmetsweiler', 10, 42, 'Zeisweilerweg 3', '66589', 'Merchweiler', 0),
(14003, NULL, 'Gewichtheber und Aerobic Verein Zittau 04 e. ', NULL, 14, 42, 'Im Städtel 13 b', '02785', 'Olbersdorf', 0),
(14004, 'AE4894CD8D3A487081765D1B6C3BD372', 'Athletikclub Meißen e. V.', NULL, 14, 42, 'Goethestraße 27', '01662', 'Meißen', 0),
(14005, '37668537FC6147F59E03477C6788809F', 'SG Fortschritt Eibau e. V.', 'SGF Eibau', 14, 42, 'Bergblick 16', '02739', 'Kottmar, OT Eibau', 0),
(14007, '470BD9F4F09A40D7B79F9F2ACD2B8A6E', 'Chemnitzer Athletenclub e.V.', 'Chemnitzer AC', 14, 42, 'Zwickauer Straße 485', '09117', 'Chemnitz', 0),
(14008, '0D3D021A79F44069917CFF2D9BDC045F', 'AC Atlas Plauen e.V.', NULL, 14, 42, 'Kasernenstraße 2a', '08523', 'Plauen', 0),
(14009, NULL, 'Dresdner Sportclub 1898 e.V.', NULL, 14, 42, 'Magdeburger Straße 12', '01067', 'Dresden', 0),
(14010, '1FC24AEE6E3F4A59A9A4D4779B6AE767', 'Niederschlesischer AC Görlitz e. V.', NULL, 14, 42, 'Käthe-Kollwitz-Straße 22', '02827', 'Görlitz', 0),
(14011, 'E7A6654C1F154D1C9932270AF73E1136', 'Riesaer Athletikclub 1969 e. V.', NULL, 14, 42, 'Dorfstraße 39', '01594', 'Hirschstein', 0),
(14016, 'E4DB92F8FA894A15A094F7CB44271046', 'TSG - Rodewisch e. V.', 'TSG Rodewisch', 14, 42, 'Schillerstraße 2 Nebengebäude der Grundschule', '08228', 'Rodewisch', 0),
(14038, 'D7435510877142C4B00658C38DF53D9E', 'SSV 1952 Torgau e. V.', NULL, 14, 42, 'Ziegeleiweg 2B', '04860', 'Torgau', 0),
(15001, '5FD25818C7594211854C728D57470203', 'Fitness und Athletenclub Sangerhausen e. V.', NULL, 15, 42, 'Grundschule am Rosarium Otto Grotewohl Straße', '06526', 'Sangerhausen', 0),
(15004, '2952E372097949D3ACDE42CB9B9EE061', 'Fermersleber Sportverein 1895 Magdeburg e.V.', NULL, 15, 42, 'Alt Fermersleben 1', '39122', 'Magdeburg', 0),
(15005, 'C9F1F4AB9152495183F17068840B42EB', 'MSV Buna Schkopau e. V.', NULL, 15, 42, 'Hohendorfer Weg 10', '06217', 'Merseburg', 0),
(15009, 'BE0FB064DFD34F2AB48082AA9C14CF44', 'SSV Samswegen 1884 e. V.', 'SSV Samswegen', 15, 42, 'Jersleber Straße 7 B', '39326', 'Samswegen', 0),
(15038, NULL, 'GKV Bad Dürrenberg e. V.', NULL, 15, 42, 'Lerchebweg 8b', '06231', 'Bad Dürrenberg/ OT Tollwitz', 0),
(16001, '15FAEC1DD19145A983D96F5A3974C7D1', 'Ohrdrufer Sportverein e. V.', NULL, 16, 42, 'Große Bahnhofstr. 6', '99885', 'Ohrdruf', 0),
(16002, 'CEB6BB8D6EB34C25A7FE16939ECF657A', 'KSV Sömmerda 1910 e. V.', NULL, 16, 42, 'Mozartstraße 69', '99610', 'Sömmerda', 0),
(16003, 'D1AF5E9A96C543DE9E86713062A5C1B2', 'SV 90 Gräfenroda e. V.', NULL, 16, 42, 'Zum Wolfstal 07', '99330', 'Geratal OT Gräfenroda', 0),
(16007, 'D09990FB80094BF19B072671CEC8F0D6', 'Jugendkraft Crawinkel e.V.', NULL, 16, 42, 'Karl - Marx - Straße 13', '99885', 'Ohrdruf OT Crawinkel', 0),
(16008, 'E577D6C053AF49F69E75ACBBCC350070', 'ASV Herbsleben e.V.', NULL, 16, 42, 'Valentin- Thau- Straße 6', '99955', 'Herbsleben', 0),
(16009, 'CC966BE4491840009960BE884C1A0215', 'ESV LOK Mühlhausen ', NULL, 16, 42, 'Körnersche Straße 37', '99974', 'Mühlhausen', 0),
(16011, '394F189769D446669BB54390A45C6334', 'Athleten-Club Suhl e.V.', NULL, 16, 42, 'Löffeltal 19', '98527', 'Suhl', 0),
(16012, 'C657FD5C10114CA0B3C1FA6B6A298AAB', 'ASV 1932 Schleusingen e. V.', NULL, 16, 42, 'Am Kochsberg 20', '98553', 'Schleusingen / OT Erlau', 0),
(16014, '3C79E5344DE24BE9866C2CD6D80FD4C7', 'Breitunger Athletik Verein e.V.', NULL, 16, 42, 'Petersberger Straße 6', '98597', 'Breitungen', 0),
(16017, NULL, '1. SV Gera e.V.', NULL, 16, 42, 'Hofwiesenpark 6', '07548', 'Gera', 0),
(16020, '5BFA602E72814BA9BFF4D79F1EBA6189', 'SV 1883 Schwarza e. V.', NULL, 16, 42, 'Corrensring 22b', '07407', 'Rudolstadt', 0),
(16028, NULL, 'SV Einheit Altenburg e. V.', NULL, 16, 42, 'Bonhoefferstraße 14', '04600', 'Altenburg', 0),
(16030, NULL, 'Gewichtheber u.  Fitness Club Artern e. V', NULL, 16, 42, 'Leipziger Straße 2', '06556', 'Artern', 0),
(17011, 'CC00AF6F34B441DB9F2D64B11D895A9D', 'Athletik-Club Potsdam e.V.', NULL, 12, 42, 'Baumhaselring 117', '14469', 'Potsdam', 0),
(17020, NULL, 'SV Motor Eberswalde e. V.', NULL, 12, 42, 'Potsdamer Allee 37', '16227', 'Eberswalde', 0),
(17021, '1F5CB42677F04D3F8B41312A82B98BD7', 'TSV Blau-Weiß 65 Schwedt e. V.', NULL, 12, 42, 'Dr.-W.-Külz-Viertel 2b', '16303', 'Schwedt', 0),
(17025, NULL, 'TSG Angermünde e. V.', NULL, 12, 42, 'Heinrichstraße 10', '16278', 'Angermünde', 0),
(17028, '6F59120BAB7B44F887F042E9F31E6F5A', 'TSV Cottbus e. V.', NULL, 12, 42, 'Leistikowstraße 16', '03042', 'Cottbus', 0),
(17040, NULL, 'Gymnastikverein Luckenwalde e.V.,Stefanie Wen', NULL, 12, 42, 'Kleiner Haag 7', '14943', 'Luckenwalde', 0),
(17044, 'A603DD6BA44A4D65B4B3B04D2E893463', 'USC Viadrina Frankfurt (Oder) e. V.', NULL, 12, 42, 'Logenstraße 3', '15230', 'Frankfurt (Oder)', 0),
(17047, '8A55D1EEAA284976819A1B7235E9889C', 'Athletik-Sport-Klub Frankfurt (Oder) e.V.', NULL, 12, 42, 'Stendaler Straße 26', '15234', 'Frankfurt (Oder)', 0),
(18004, '2855B01DA822421C8A2A34900C2F3F92', 'TSV 1860 Stralsund e. V.', NULL, 13, 42, 'Karl-Marx-Straße 11', '18439', 'Stralsund', 0),
(18007, 'BE1BD03B4F724DB8B9104FE1A935DFF4', 'Herrnburger Athletenverein 77 e.V. ', NULL, 13, 42, 'Straße Schattin 20', '23923', 'Herrnburg', 0),
(18017, 'B1CEBBE22B2346D68C90CB25D80D6A66', 'Malchower Athleten Club e. V.', NULL, 13, 42, 'Speicherstraße :16c', '18273', 'Güstrow', 0),
(18048, '62FF964E9831496EA30FA14C73C63C37', 'TSV Grün-Weiß Rostock 1895 e. V.', NULL, 13, 42, 'Albert-Einstein-Straße 29', '18059', 'Rostock', 0),
(20542, '4C238E7074B947E784CB529EA424E5FC', 'Kraftsportverein 1905 Worms e.V.', 'KSV Worms', 7, 42, 'Rebgartenstr. 28 b', '67551', 'Worms', 0),
(20576, '9476CED63C25496B9BE6D061CA7EF0D2', 'Athletikclub Konstanz e.V.', NULL, 8, 42, 'Münzgasse 16', '78462', 'Konstanz', 0),
(20605, 'E9AA740643C1493393E442F043D34706', 'Kraftsport Colonia e.V.', NULL, 5, 42, 'Neusser Straße 411', '50733', 'Köln', 0),
(20922, NULL, 'BC Vechta e.V.', NULL, 3, 42, 'Bremer Tor 16 ', '49377', 'Vechta', 0),
(20975, NULL, 'Powerlifting Würzburg e.V (z. Hd. v. Cathrin ', NULL, 9, 42, 'Felix-Dahn-Str. 5', '97072', 'Würzburg', 0),
(21205, NULL, 'Gewichtheberverein Aarau', NULL, NULL, 159, 'Dammweg 16', '5000', 'Aarau', 0),
(21285, NULL, 'Soester Turn-Verein 1862 e.V.', NULL, 5, 42, 'Puppenstaße 7', '59494', 'Soest', 0),
(21383, NULL, 'Testverein 3', NULL, 8, 42, 'Badener Platz 6', '69181', 'Leimen', 0),
(21822, '1C0D47EDDB3D4ED69DF0B756468139EC', 'Weightlifting & Fitness Club Nagold e.V.', NULL, 8, 42, 'Erwin-Rommelstraße 16', '72202', 'Nagold', 0),
(21824, NULL, 'Sportverein für Gesundheit & Rehabilitationss', NULL, 1, 42, 'Kornkamp 46c ', '22926', 'Ahrensburg', 0),
(21936, NULL, 'Kraft-Mühle Minden e.V. ', NULL, 5, 42, 'Steinbrede 12 ', '32423', 'Minden', 0),
(22070, '61A8579B31164C0B9599530618E8E71B', 'KraftWerkstatt Lörrach e.V.', NULL, 8, 42, 'Am Palmrain 8', '79576', 'Weil am Rhein', 0),
(22072, '9104E7370FE842B9A5971530E1B69707', 'CF Tübingen e.V.', NULL, 8, 42, 'Alte Landstraße 57 ', '72072', 'Tübingen', 0),
(22095, '58DCEB0FFBC24BC6BF6E47303EA7CE2C', 'MTV Soltau ', NULL, 3, 42, 'Stubbendorfweg 8', '29614', 'Soltau', 0),
(22203, NULL, 'Weightlifting Team Nord e.V.', NULL, 3, 42, 'Handwerkerhof 1', '28857', 'Syke', 0),
(22287, NULL, 'SportFabrik Munster 2018 e.V.', NULL, 3, 42, 'Brombeerweg 6', '29633', 'Munster', 0),
(22291, NULL, 'ETSV Würzburg 1928 e.V. ', NULL, 9, 42, 'Mergentheimerstraße 17', '97082', 'Würzburg', 0),
(22298, 'ECDB7980EE544527BDD9880A3CE7FBE5', 'Kraftclub am Lech e.V.', NULL, 9, 42, 'Wankstraße 3 ', '86165', 'Augsburg', 0),
(22309, 'E30FB435631442E0B6C851005B865EC6', 'Lifting Rebels Club Neumarkt e.V.', NULL, 9, 42, 'EFA-Str. 6 ', '92318', 'Neumarkt', 0),
(100000, '037CDAC962F34E64B6D701A596355DF3', 'Athletenschmiede Kiel e.V...', 'AS Kiel', 1, 42, NULL, NULL, NULL, 0),
(100001, 'D8CD56D7EB9A46DEA76F77EABA07F3EE', 'Kraft- und Athletikklub Osnabrück e.V. ', NULL, 3, 42, 'Sandforter Straße 116 B', '49086', 'Osnabrück', 0);
"
        End Function

    End Class
End Module
