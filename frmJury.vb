﻿
Public Class frmJury

    Private Sub frmJury_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Cursor = Cursors.WaitCursor
        Text += " - Bohle " & User.Bohle
    End Sub
    Private Sub frmJury_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Cursor = Cursors.Default
    End Sub

    Private Sub Lampen_SizeChanged(sender As Object, e As EventArgs) Handles LampeReferee1.SizeChanged, LampeReferee2.SizeChanged, LampeReferee3.SizeChanged,
                                                LampeJury1.SizeChanged, LampeJury2.SizeChanged, LampeJury3.SizeChanged, LampeJury4.SizeChanged, LampeJury5.SizeChanged
        Try
            With CType(sender, TextBox)
                .Font = New Font(.Font.FontFamily, CSng(120 * Width / MinimumSize.Width))
            End With
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Technik_SizeChanged(sender As Object, e As EventArgs) Handles Technik1.SizeChanged, Technik2.SizeChanged, Technik3.SizeChanged
        Try
            With CType(sender, TextBox)
                .Font = New Font(.Font.FontFamily, CSng(50 * Width / MinimumSize.Width))
            End With
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Label_SizeChanged(sender As Object, e As EventArgs) Handles Label1.SizeChanged, Label2.SizeChanged, Label3.SizeChanged
        Try
            With CType(sender, Label)
                .Font = New Font(.Font.FontFamily, CSng(18 * Width / MinimumSize.Width))
            End With
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TextBox_MouseEnter(sender As Object, e As EventArgs) Handles LampeReferee1.MouseEnter, LampeReferee2.MouseEnter, LampeReferee3.MouseEnter,
                                                                            Technik1.MouseEnter, Technik2.MouseEnter, Technik3.MouseEnter
        CType(sender, TextBox).BackColor = Color.DarkSlateGray
    End Sub

    Private Sub TextBox_MouseLeave(sender As Object, e As EventArgs) Handles LampeReferee1.MouseLeave, LampeReferee2.MouseLeave, LampeReferee3.MouseLeave,
                                                                            Technik1.MouseLeave, Technik2.MouseLeave, Technik3.MouseLeave
        CType(sender, TextBox).BackColor = Color.Black
    End Sub


End Class