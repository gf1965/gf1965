﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmBohleTV
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBohleTV))
        Me.pnlUhr = New System.Windows.Forms.Panel()
        Me.xAufruf_std = New System.Windows.Forms.TextBox()
        Me.xAufruf_sek = New System.Windows.Forms.TextBox()
        Me.xAufruf_min = New System.Windows.Forms.TextBox()
        Me.xAufruf_pkt = New System.Windows.Forms.TextBox()
        Me.xAufruf_p_s = New System.Windows.Forms.TextBox()
        Me.xTechniknote = New System.Windows.Forms.TextBox()
        Me.xTeilnehmer = New System.Windows.Forms.Label()
        Me.pnlWertung = New System.Windows.Forms.Panel()
        Me.xGültig3 = New System.Windows.Forms.TextBox()
        Me.xGültig2 = New System.Windows.Forms.TextBox()
        Me.xGültig1 = New System.Windows.Forms.TextBox()
        Me.xVerein = New System.Windows.Forms.Label()
        Me.xAK = New System.Windows.Forms.Label()
        Me.xVersuch = New System.Windows.Forms.Label()
        Me.xHantelgewicht = New System.Windows.Forms.Label()
        Me.xDurchgang = New System.Windows.Forms.Label()
        Me.xGewichtsklasse = New System.Windows.Forms.TextBox()
        Me.pnlHantel = New System.Windows.Forms.Panel()
        Me.JB_Port = New System.IO.Ports.SerialPort(Me.components)
        Me.Port1 = New System.IO.Ports.SerialPort(Me.components)
        Me.Port2 = New System.IO.Ports.SerialPort(Me.components)
        Me.Port3 = New System.IO.Ports.SerialPort(Me.components)
        Me.ZN_Port = New System.IO.Ports.SerialPort(Me.components)
        Me.ZA_Port = New System.IO.Ports.SerialPort(Me.components)
        Me.lblJury = New System.Windows.Forms.Label()
        Me.picFlagge = New System.Windows.Forms.PictureBox()
        Me.Dummy = New System.Windows.Forms.Button()
        Me.xVersuchText = New System.Windows.Forms.Label()
        Me.pnlDurchgang = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.pnlUhr.SuspendLayout()
        Me.pnlWertung.SuspendLayout()
        CType(Me.picFlagge, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDurchgang.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlUhr
        '
        Me.pnlUhr.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnlUhr.BackColor = System.Drawing.Color.Black
        Me.pnlUhr.Controls.Add(Me.xAufruf_std)
        Me.pnlUhr.Controls.Add(Me.xAufruf_sek)
        Me.pnlUhr.Controls.Add(Me.xAufruf_min)
        Me.pnlUhr.Controls.Add(Me.xAufruf_pkt)
        Me.pnlUhr.Controls.Add(Me.xAufruf_p_s)
        Me.pnlUhr.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.pnlUhr.Location = New System.Drawing.Point(632, 297)
        Me.pnlUhr.Name = "pnlUhr"
        Me.pnlUhr.Size = New System.Drawing.Size(332, 117)
        Me.pnlUhr.TabIndex = 13
        '
        'xAufruf_std
        '
        Me.xAufruf_std.BackColor = System.Drawing.Color.Black
        Me.xAufruf_std.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xAufruf_std.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xAufruf_std.Font = New System.Drawing.Font("Digital-7", 84.74999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xAufruf_std.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf_std.Location = New System.Drawing.Point(2, 5)
        Me.xAufruf_std.Name = "xAufruf_std"
        Me.xAufruf_std.Size = New System.Drawing.Size(53, 112)
        Me.xAufruf_std.TabIndex = 20
        Me.xAufruf_std.Text = "0"
        Me.xAufruf_std.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.xAufruf_std.WordWrap = False
        '
        'xAufruf_sek
        '
        Me.xAufruf_sek.BackColor = System.Drawing.Color.Black
        Me.xAufruf_sek.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xAufruf_sek.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xAufruf_sek.Font = New System.Drawing.Font("Digital-7", 84.74999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xAufruf_sek.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf_sek.Location = New System.Drawing.Point(211, 5)
        Me.xAufruf_sek.Name = "xAufruf_sek"
        Me.xAufruf_sek.Size = New System.Drawing.Size(107, 112)
        Me.xAufruf_sek.TabIndex = 16
        Me.xAufruf_sek.Text = "00"
        Me.xAufruf_sek.WordWrap = False
        '
        'xAufruf_min
        '
        Me.xAufruf_min.BackColor = System.Drawing.Color.Black
        Me.xAufruf_min.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xAufruf_min.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xAufruf_min.Font = New System.Drawing.Font("Digital-7", 84.74999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xAufruf_min.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf_min.Location = New System.Drawing.Point(80, 5)
        Me.xAufruf_min.Name = "xAufruf_min"
        Me.xAufruf_min.Size = New System.Drawing.Size(105, 112)
        Me.xAufruf_min.TabIndex = 18
        Me.xAufruf_min.Text = "00"
        Me.xAufruf_min.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.xAufruf_min.WordWrap = False
        '
        'xAufruf_pkt
        '
        Me.xAufruf_pkt.BackColor = System.Drawing.Color.Black
        Me.xAufruf_pkt.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xAufruf_pkt.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xAufruf_pkt.Font = New System.Drawing.Font("Digital-7", 60.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xAufruf_pkt.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf_pkt.Location = New System.Drawing.Point(183, 16)
        Me.xAufruf_pkt.Name = "xAufruf_pkt"
        Me.xAufruf_pkt.Size = New System.Drawing.Size(31, 80)
        Me.xAufruf_pkt.TabIndex = 19
        Me.xAufruf_pkt.Text = ":"
        Me.xAufruf_pkt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.xAufruf_pkt.WordWrap = False
        '
        'xAufruf_p_s
        '
        Me.xAufruf_p_s.BackColor = System.Drawing.Color.Black
        Me.xAufruf_p_s.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xAufruf_p_s.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xAufruf_p_s.Font = New System.Drawing.Font("Digital-7", 60.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xAufruf_p_s.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf_p_s.Location = New System.Drawing.Point(53, 16)
        Me.xAufruf_p_s.Name = "xAufruf_p_s"
        Me.xAufruf_p_s.Size = New System.Drawing.Size(31, 80)
        Me.xAufruf_p_s.TabIndex = 21
        Me.xAufruf_p_s.Text = ":"
        Me.xAufruf_p_s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.xAufruf_p_s.WordWrap = False
        '
        'xTechniknote
        '
        Me.xTechniknote.BackColor = System.Drawing.Color.Black
        Me.xTechniknote.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xTechniknote.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xTechniknote.Font = New System.Drawing.Font("DS-Digital", 99.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xTechniknote.ForeColor = System.Drawing.Color.Lime
        Me.xTechniknote.Location = New System.Drawing.Point(351, 291)
        Me.xTechniknote.Name = "xTechniknote"
        Me.xTechniknote.Size = New System.Drawing.Size(300, 133)
        Me.xTechniknote.TabIndex = 15
        Me.xTechniknote.Text = "X,XX"
        Me.xTechniknote.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.xTechniknote.WordWrap = False
        '
        'xTeilnehmer
        '
        Me.xTeilnehmer.AutoSize = True
        Me.xTeilnehmer.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xTeilnehmer.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.xTeilnehmer.ForeColor = System.Drawing.Color.Gold
        Me.xTeilnehmer.Location = New System.Drawing.Point(21, 23)
        Me.xTeilnehmer.Name = "xTeilnehmer"
        Me.xTeilnehmer.Size = New System.Drawing.Size(470, 63)
        Me.xTeilnehmer.TabIndex = 16
        Me.xTeilnehmer.Text = "Friedrich, Raphael"
        Me.xTeilnehmer.Visible = False
        '
        'pnlWertung
        '
        Me.pnlWertung.BackColor = System.Drawing.Color.Black
        Me.pnlWertung.Controls.Add(Me.xGültig3)
        Me.pnlWertung.Controls.Add(Me.xGültig2)
        Me.pnlWertung.Controls.Add(Me.xGültig1)
        Me.pnlWertung.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.pnlWertung.Location = New System.Drawing.Point(17, 277)
        Me.pnlWertung.Name = "pnlWertung"
        Me.pnlWertung.Size = New System.Drawing.Size(328, 164)
        Me.pnlWertung.TabIndex = 17
        '
        'xGültig3
        '
        Me.xGültig3.BackColor = System.Drawing.Color.Black
        Me.xGültig3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig3.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xGültig3.Font = New System.Drawing.Font("Wingdings", 100.0!)
        Me.xGültig3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.xGültig3.Location = New System.Drawing.Point(206, 10)
        Me.xGültig3.Name = "xGültig3"
        Me.xGültig3.ReadOnly = True
        Me.xGültig3.Size = New System.Drawing.Size(95, 148)
        Me.xGültig3.TabIndex = 17
        Me.xGültig3.Tag = "32"
        Me.xGültig3.Text = "l"
        Me.xGültig3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xGültig2
        '
        Me.xGültig2.BackColor = System.Drawing.Color.Black
        Me.xGültig2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig2.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xGültig2.Font = New System.Drawing.Font("Wingdings", 100.0!)
        Me.xGültig2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.xGültig2.Location = New System.Drawing.Point(109, 10)
        Me.xGültig2.Name = "xGültig2"
        Me.xGültig2.ReadOnly = True
        Me.xGültig2.Size = New System.Drawing.Size(95, 148)
        Me.xGültig2.TabIndex = 16
        Me.xGültig2.Tag = "8"
        Me.xGültig2.Text = "l"
        Me.xGültig2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xGültig1
        '
        Me.xGültig1.BackColor = System.Drawing.Color.Black
        Me.xGültig1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig1.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xGültig1.Font = New System.Drawing.Font("Wingdings", 100.0!)
        Me.xGültig1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.xGültig1.Location = New System.Drawing.Point(12, 10)
        Me.xGültig1.Name = "xGültig1"
        Me.xGültig1.ReadOnly = True
        Me.xGültig1.Size = New System.Drawing.Size(95, 148)
        Me.xGültig1.TabIndex = 15
        Me.xGültig1.Tag = "2"
        Me.xGültig1.Text = "l"
        Me.xGültig1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xVerein
        '
        Me.xVerein.AutoSize = True
        Me.xVerein.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xVerein.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.xVerein.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.xVerein.Location = New System.Drawing.Point(21, 114)
        Me.xVerein.Name = "xVerein"
        Me.xVerein.Size = New System.Drawing.Size(417, 63)
        Me.xVerein.TabIndex = 18
        Me.xVerein.Text = "TSG Rodewisch"
        Me.xVerein.Visible = False
        '
        'xAK
        '
        Me.xAK.AutoSize = True
        Me.xAK.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xAK.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.xAK.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.xAK.Location = New System.Drawing.Point(21, 205)
        Me.xAK.Name = "xAK"
        Me.xAK.Size = New System.Drawing.Size(261, 63)
        Me.xAK.TabIndex = 19
        Me.xAK.Text = "C-Jugend"
        Me.xAK.Visible = False
        '
        'xVersuch
        '
        Me.xVersuch.AutoSize = True
        Me.xVersuch.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xVersuch.Font = New System.Drawing.Font("Microsoft Sans Serif", 50.0!)
        Me.xVersuch.ForeColor = System.Drawing.Color.Coral
        Me.xVersuch.Location = New System.Drawing.Point(709, 205)
        Me.xVersuch.Name = "xVersuch"
        Me.xVersuch.Size = New System.Drawing.Size(89, 76)
        Me.xVersuch.TabIndex = 2
        Me.xVersuch.Text = "3."
        Me.xVersuch.Visible = False
        '
        'xHantelgewicht
        '
        Me.xHantelgewicht.AutoSize = True
        Me.xHantelgewicht.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xHantelgewicht.Font = New System.Drawing.Font("Microsoft Sans Serif", 50.0!)
        Me.xHantelgewicht.ForeColor = System.Drawing.Color.Coral
        Me.xHantelgewicht.Location = New System.Drawing.Point(12, 0)
        Me.xHantelgewicht.Name = "xHantelgewicht"
        Me.xHantelgewicht.Size = New System.Drawing.Size(233, 76)
        Me.xHantelgewicht.TabIndex = 1
        Me.xHantelgewicht.Text = "262 kg"
        Me.xHantelgewicht.Visible = False
        '
        'xDurchgang
        '
        Me.xDurchgang.AutoSize = True
        Me.xDurchgang.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xDurchgang.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.xDurchgang.ForeColor = System.Drawing.Color.Coral
        Me.xDurchgang.Location = New System.Drawing.Point(712, 23)
        Me.xDurchgang.Name = "xDurchgang"
        Me.xDurchgang.Size = New System.Drawing.Size(201, 63)
        Me.xDurchgang.TabIndex = 0
        Me.xDurchgang.Text = "Stoßen"
        Me.xDurchgang.Visible = False
        '
        'xGewichtsklasse
        '
        Me.xGewichtsklasse.BackColor = System.Drawing.Color.Black
        Me.xGewichtsklasse.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGewichtsklasse.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xGewichtsklasse.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.xGewichtsklasse.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.xGewichtsklasse.Location = New System.Drawing.Point(500, 208)
        Me.xGewichtsklasse.Name = "xGewichtsklasse"
        Me.xGewichtsklasse.Size = New System.Drawing.Size(122, 61)
        Me.xGewichtsklasse.TabIndex = 22
        Me.xGewichtsklasse.Text = "-75"
        Me.xGewichtsklasse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.xGewichtsklasse.Visible = False
        '
        'pnlHantel
        '
        Me.pnlHantel.BackColor = System.Drawing.Color.Black
        Me.pnlHantel.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.pnlHantel.Location = New System.Drawing.Point(384, 270)
        Me.pnlHantel.Name = "pnlHantel"
        Me.pnlHantel.Size = New System.Drawing.Size(280, 175)
        Me.pnlHantel.TabIndex = 24
        Me.pnlHantel.Visible = False
        '
        'JB_Port
        '
        Me.JB_Port.PortName = "COM0"
        Me.JB_Port.ReadTimeout = 500
        Me.JB_Port.WriteTimeout = 500
        '
        'Port1
        '
        Me.Port1.PortName = "COM0"
        '
        'Port2
        '
        Me.Port2.PortName = "COM0"
        '
        'Port3
        '
        Me.Port3.PortName = "COM0"
        '
        'ZN_Port
        '
        Me.ZN_Port.PortName = "COM0"
        '
        'ZA_Port
        '
        Me.ZA_Port.PortName = "COM0"
        '
        'lblJury
        '
        Me.lblJury.BackColor = System.Drawing.Color.Yellow
        Me.lblJury.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.lblJury.Font = New System.Drawing.Font("Microsoft Sans Serif", 45.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJury.ForeColor = System.Drawing.Color.Red
        Me.lblJury.Location = New System.Drawing.Point(9, 275)
        Me.lblJury.Name = "lblJury"
        Me.lblJury.Size = New System.Drawing.Size(18, 165)
        Me.lblJury.TabIndex = 27
        Me.lblJury.Text = "JURY-ENTSCHEIDUNG"
        Me.lblJury.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblJury.Visible = False
        '
        'picFlagge
        '
        Me.picFlagge.BackColor = System.Drawing.Color.Green
        Me.picFlagge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.picFlagge.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.picFlagge.ImageLocation = ""
        Me.picFlagge.Location = New System.Drawing.Point(33, 102)
        Me.picFlagge.Name = "picFlagge"
        Me.picFlagge.Padding = New System.Windows.Forms.Padding(1)
        Me.picFlagge.Size = New System.Drawing.Size(130, 91)
        Me.picFlagge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picFlagge.TabIndex = 0
        Me.picFlagge.TabStop = False
        Me.picFlagge.Visible = False
        '
        'Dummy
        '
        Me.Dummy.BackColor = System.Drawing.Color.Black
        Me.Dummy.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.Dummy.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.Dummy.FlatAppearance.BorderSize = 0
        Me.Dummy.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Dummy.ForeColor = System.Drawing.Color.White
        Me.Dummy.Location = New System.Drawing.Point(633, 263)
        Me.Dummy.Name = "Dummy"
        Me.Dummy.Size = New System.Drawing.Size(54, 23)
        Me.Dummy.TabIndex = 0
        Me.Dummy.UseVisualStyleBackColor = False
        '
        'xVersuchText
        '
        Me.xVersuchText.AutoSize = True
        Me.xVersuchText.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xVersuchText.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!)
        Me.xVersuchText.ForeColor = System.Drawing.Color.Coral
        Me.xVersuchText.Location = New System.Drawing.Point(777, 230)
        Me.xVersuchText.Name = "xVersuchText"
        Me.xVersuchText.Size = New System.Drawing.Size(167, 46)
        Me.xVersuchText.TabIndex = 30
        Me.xVersuchText.Text = "Versuch"
        Me.xVersuchText.Visible = False
        '
        'pnlDurchgang
        '
        Me.pnlDurchgang.Controls.Add(Me.xHantelgewicht)
        Me.pnlDurchgang.Location = New System.Drawing.Point(699, 109)
        Me.pnlDurchgang.Name = "pnlDurchgang"
        Me.pnlDurchgang.Size = New System.Drawing.Size(255, 84)
        Me.pnlDurchgang.TabIndex = 31
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(1, Byte), Integer))
        Me.Panel1.BackgroundImage = Global.Gewichtheben.My.Resources.Resources.LogoTV
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel1.Location = New System.Drawing.Point(0, 447)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(980, 104)
        Me.Panel1.TabIndex = 32
        '
        'frmBohleTV
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(980, 551)
        Me.Controls.Add(Me.xVersuchText)
        Me.Controls.Add(Me.xVersuch)
        Me.Controls.Add(Me.pnlDurchgang)
        Me.Controls.Add(Me.lblJury)
        Me.Controls.Add(Me.xDurchgang)
        Me.Controls.Add(Me.pnlWertung)
        Me.Controls.Add(Me.pnlUhr)
        Me.Controls.Add(Me.Dummy)
        Me.Controls.Add(Me.picFlagge)
        Me.Controls.Add(Me.xTechniknote)
        Me.Controls.Add(Me.xAK)
        Me.Controls.Add(Me.xVerein)
        Me.Controls.Add(Me.xTeilnehmer)
        Me.Controls.Add(Me.xGewichtsklasse)
        Me.Controls.Add(Me.pnlHantel)
        Me.Controls.Add(Me.Panel1)
        Me.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(980, 551)
        Me.Name = "frmBohleTV"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Bohle"
        Me.TransparencyKey = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(1, Byte), Integer))
        Me.pnlUhr.ResumeLayout(False)
        Me.pnlUhr.PerformLayout()
        Me.pnlWertung.ResumeLayout(False)
        Me.pnlWertung.PerformLayout()
        CType(Me.picFlagge, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDurchgang.ResumeLayout(False)
        Me.pnlDurchgang.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnlUhr As System.Windows.Forms.Panel
    Friend WithEvents xAufruf_sek As System.Windows.Forms.TextBox
    Friend WithEvents xTeilnehmer As System.Windows.Forms.Label
    Friend WithEvents pnlWertung As System.Windows.Forms.Panel
    Friend WithEvents xVerein As System.Windows.Forms.Label
    Friend WithEvents xAK As System.Windows.Forms.Label
    Friend WithEvents xVersuch As System.Windows.Forms.Label
    Friend WithEvents xHantelgewicht As System.Windows.Forms.Label
    Friend WithEvents xDurchgang As System.Windows.Forms.Label
    Friend WithEvents xAufruf_min As System.Windows.Forms.TextBox
    Friend WithEvents xAufruf_pkt As System.Windows.Forms.TextBox
    Friend WithEvents xGültig3 As System.Windows.Forms.TextBox
    Friend WithEvents xGültig2 As System.Windows.Forms.TextBox
    Friend WithEvents xGültig1 As System.Windows.Forms.TextBox
    Friend WithEvents xGewichtsklasse As System.Windows.Forms.TextBox
    Friend WithEvents pnlHantel As System.Windows.Forms.Panel
    Friend WithEvents Port2 As Ports.SerialPort
    Friend WithEvents Port3 As Ports.SerialPort
    Friend WithEvents JB_Port As SerialPort
    Friend WithEvents Port1 As SerialPort
    Friend WithEvents ZN_Port As SerialPort
    Friend WithEvents ZA_Port As SerialPort
    Friend WithEvents xTechniknote As TextBox
    Friend WithEvents lblJury As Label
    Friend WithEvents picFlagge As PictureBox
    Friend WithEvents Dummy As Button
    Friend WithEvents xAufruf_p_s As TextBox
    Friend WithEvents xAufruf_std As TextBox
    Friend WithEvents xVersuchText As Label
    Friend WithEvents pnlDurchgang As Panel
    Friend WithEvents Panel1 As Panel
End Class
