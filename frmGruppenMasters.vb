﻿Imports System.ComponentModel

Public Class frmGruppenMasters
    Dim Bereich As String = "Gruppen_Masters"

    Dim cmd As MySqlCommand
    Dim Query As String
    Dim dtGK As New DataTable
    Dim dtDetails As New DataTable
    Dim dtHeber As New DataTable
    Dim dv As DataView
    Dim bs As BindingSource
    Dim AK_w As String = "4"
    Dim AK_m As String = "10"
    Dim Liste As New List(Of CheckedListBox)
    Dim GK As New List(Of Button)
    Dim Gruppen As New List(Of TextBox)
    Dim GK_w As New List(Of String)
    Dim GK_m As New List(Of String)
    Dim Teams_W As New Dictionary(Of Integer, List(Of Integer))
    Dim Teams_M As New Dictionary(Of Integer, List(Of Integer))
    Dim nonNumberEntered As Boolean = False

    Private Sub TextboxNum_NumOnly_KeyDown(sender As Object, e As KeyEventArgs) Handles txtGr1.KeyDown, txtGr2.KeyDown, txtGr3.KeyDown, txtGr4.KeyDown, txtGr5.KeyDown, txtGr6.KeyDown, txtGr7.KeyDown, txtGr8.KeyDown, txtGr9.KeyDown, txtGr10.KeyDown
        nonNumberEntered = False
        If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
            If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                If e.KeyCode <> Keys.Back And e.KeyCode <> Keys.Oemcomma And e.KeyCode <> Keys.Decimal Then
                    nonNumberEntered = True
                End If
            End If
        End If
        If ModifierKeys = Keys.Shift Then
            nonNumberEntered = True
        End If
    End Sub

    Private Sub TextboxNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtGr1.KeyPress, txtGr2.KeyPress, txtGr3.KeyPress, txtGr4.KeyPress, txtGr5.KeyPress, txtGr6.KeyPress, txtGr7.KeyPress, txtGr8.KeyPress, txtGr9.KeyPress, txtGr10.KeyPress
        If nonNumberEntered = True Then
            e.Handled = True
        End If
    End Sub

    Private Sub Save()

    End Sub

    Private Sub frmMastersGruppen_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If mnuSpeichern.Enabled Then
            Using New Centered_MessageBox(Me)
                If MessageBox.Show("Neue Gruppen-Einteilung speichern?", Wettkampf.Bezeichnung, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                    e.Cancel = True
                Else
                    Save()
                End If
            End Using
        End If
    End Sub

    Private Sub frmMastersGruppen_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        Liste.AddRange({lstGK1, lstGK2, lstGK3, lstGK4, lstGK5, lstGK6, lstGK7, lstGK8, lstGK9, lstGK10})
        GK.AddRange({btnGK1, btnGK2, btnGK3, btnGK4, btnGK5, btnGK6, btnGK7, btnGK8, btnGK9, btnGk10})
        Gruppen.AddRange({txtGr1, txtGr2, txtGr3, txtGr4, txtGr5, txtGr6, txtGr7, txtGr8, txtGr9, txtGr10})
    End Sub

    Private Sub frmMastersGruppen_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Using conn As New MySqlConnection(MySqlConnString)
            Try
                conn.Open()
                Query = "Select Masters_w, Masters_m FROM gewichtsklassen order by id;"
                cmd = New MySqlCommand(Query, conn)
                dtGK.Load(cmd.ExecuteReader)
                Query = "SELECT AK_w, AK_m, Leistung_w, Leistung_m FROM wettkampf_details WHERE Wettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtDetails.Load(cmd.ExecuteReader())
                Query = "select m.Teilnehmer, a.Nachname, a.Vorname, a.Jahrgang, a.Geschlecht, v.Vereinsname, m.Gruppe " &
                        "from athleten a inner join meldung m on a.idTeilnehmer = m.Teilnehmer " &
                        "inner join verein v on a.Verein = v.idVerein " &
                        "left join altersklassen ak on year(now())-a.Jahrgang between von and bis " &
                        "where Wettkampf = " & Wettkampf.ID & " order by m.Gruppe, a.Geschlecht, a.Nachname, a.Vorname;"
                cmd = New MySqlCommand(Query, conn)
                dtHeber.Load(cmd.ExecuteReader())
            Catch ex As Exception
                Stop
            End Try
        End Using

        dv = New DataView(dtHeber)
        bs = New BindingSource With {.DataSource = dv}

        With dgv
            .AutoGenerateColumns = False
            .AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(192, 255, 192)
            .RowsDefaultCellStyle.BackColor = Color.LightCyan
            Dim ColWidth() As Integer = {40, 30, 50, 50, 45, 130, 50}
            Dim ColBez() As String = {"WK", "Teilnehmer", "Nachname", "Vorname", "JG", "Verein", "Gruppe"}
            Dim ColData() As String = {"attend", "Teilnehmer", "Nachname", "Vorname", "Jahrgang", "Vereinsname", "Gruppe"}
            .Columns.Add(New DataGridViewCheckBoxColumn With {.Name = "WK"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Teilnehmer"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Jahrgang"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Verein"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Gruppe"})
            For i As Integer = 0 To ColWidth.Count - 1
                .Columns(i).Width = ColWidth(i)
                .Columns(i).HeaderText = ColBez(i)
                .Columns(i).DataPropertyName = ColData(i)
            Next
            .Columns("Ix").Visible = False
            .Columns("Teilnehmer").Visible = False
            .Columns("Gruppe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            '.DataSource = bs
        End With

        ' Gewichtsklassen in Listen eintragen
        Dim plus(2) As Boolean
        For i As Integer = 0 To dtGK.Rows.Count - 1
            If CInt(dtGK.Rows(i)("Masters_w")) > 0 Then
                GK_w.Add("  -" + dtGK.Rows(i)("Masters_w").ToString)
            ElseIf CInt(dtGK.Rows(i)("Masters_w")) = 0 And Not plus(0) Then
                GK_w.Add("  +" + dtGK.Rows(i - 1)("Masters_w").ToString)
                plus(0) = True
            End If
            If CInt(dtGK.Rows(i)("Masters_m")) > 0 Then
                GK_m.Add("  -" + dtGK.Rows(i)("Masters_m").ToString)
            ElseIf CInt(dtGK.Rows(i)("Masters_m")) = 0 And Not plus(1) Then
                GK_m.Add("  +" + dtGK.Rows(i - 1)("Masters_m").ToString)
                plus(1) = True
            End If
            If plus(0) And plus(1) Then Exit For
        Next
        GK_w.Insert(0, "  [alle]")
        GK_m.Insert(0, "  [alle]")


        If dtDetails.Rows.Count > 0 Then
            If Not IsDBNull(dtDetails.Rows(0)("AK_w")) Then AK_w = dtDetails.Rows(0)("AK_w").ToString
            If Not IsDBNull(dtDetails.Rows(0)("AK_m")) Then AK_m = dtDetails.Rows(0)("AK_m").ToString
        End If





        Cursor = Cursors.Default
        mnuSpeichern.Enabled = False
    End Sub

    Private Sub lstGK_Leave(sender As Object, e As EventArgs) Handles lstGK1.Leave, lstGK2.Leave, lstGK3.Leave, lstGK4.Leave, lstGK5.Leave, lstGK6.Leave, lstGK7.Leave, lstGK8.Leave, lstGK9.Leave, lstGK10.Leave
        Dim lst As CheckedListBox = CType(sender, CheckedListBox)
        Dim GK As New List(Of Integer)
        With lst
            For i As Integer = 0 To .Items.Count - 1
                If .GetItemChecked(i) Then
                    GK.Add(i)
                    If i = 0 Then Exit For
                End If
            Next
            If cboGeschlecht.Text = "weiblich" Then
                If Teams_W.ContainsKey(CInt(lst.Tag)) Then Teams_W.Remove(CInt(lst.Tag))
                If GK.Count > 0 Then Teams_W.Add((CInt(.Tag)), GK)
            ElseIf cboGeschlecht.Text = "männlich" Then
                If Teams_M.ContainsKey(CInt(lst.Tag)) Then Teams_M.Remove(CInt(lst.Tag))
                If GK.Count > 0 Then Teams_M.Add((CInt(.Tag)), GK)
            End If
            .Tag = "left"
            .Visible = False
        End With
    End Sub

    Private Sub cboGeschlecht_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGeschlecht.SelectedIndexChanged
        Select Case cboGeschlecht.Text
            Case "weiblich"
                For i As Integer = 0 To CInt(AK_w) - 1
                    GK(i).Enabled = True
                    Gruppen(i).Enabled = True
                    Liste(i).Items.Clear()
                    Liste(i).Items.AddRange(GK_w.ToArray)
                    If Teams_W.ContainsKey(i + 1) Then
                        For r As Integer = 0 To Teams_W(i + 1).Count - 1
                            Liste(i).SetItemChecked(Teams_W(i + 1)(r), True)
                            If Teams_W(i + 1)(r) = 0 Then Exit For
                        Next
                    End If
                Next
                For i As Integer = CInt(AK_w) To 9
                    GK(i).Enabled = False
                    Gruppen(i).Enabled = False
                    Liste(i).Items.Clear()
                Next
            Case "männlich"
                For i As Integer = 0 To CInt(AK_m) - 1
                    GK(i).Enabled = True
                    Gruppen(i).Enabled = True
                    Liste(i).Items.Clear()
                    Liste(i).Items.AddRange(GK_m.ToArray)
                    If Teams_M.ContainsKey(i + 1) Then
                        For r As Integer = 0 To Teams_M(i + 1).Count - 1
                            Liste(i).SetItemChecked(Teams_M(i + 1)(r), True)
                            If Teams_M(i + 1)(r) = 0 Then Exit For
                        Next
                    End If
                Next
                For i As Integer = CInt(AK_m) To 9
                    GK(i).Enabled = False
                    Gruppen(i).Enabled = False
                    Liste(i).Items.Clear()
                Next
        End Select
    End Sub

    Private Sub btnGK_Click(sender As Object, e As EventArgs) Handles btnGK1.Click, btnGK2.Click, btnGK3.Click, btnGK4.Click, btnGK5.Click, btnGK6.Click, btnGK7.Click, btnGK8.Click, btnGK9.Click, btnGk10.Click
        Dim btn As Button = CType(sender, Button)
        With Liste(CInt(btn.Tag) - 1)
            If .Visible Then
                .Visible = False
            ElseIf Not IsNumeric(.Tag) Then
                .Tag = btn.Tag
            ElseIf CInt(.Tag) = CInt(btn.Tag) Then
                .Visible = True
                .Focus()
                .BringToFront()
                .SetSelected(0, True)
            End If
        End With
    End Sub

    Private Sub lstGK_KeyDown(sender As Object, e As KeyEventArgs) Handles lstGK1.KeyDown, lstGK2.KeyDown, lstGK3.KeyDown, lstGK4.KeyDown, lstGK5.KeyDown, lstGK6.KeyDown, lstGK7.KeyDown, lstGK8.KeyDown, lstGK9.KeyDown, lstGK10.KeyDown
        If e.KeyCode = Keys.Escape Then
            Dim lst As CheckedListBox = CType(sender, CheckedListBox)
            With lst
                .Visible = False
                .Tag = "left"
            End With
        End If
    End Sub

    Private Sub lstGK_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles lstGK1.ItemCheck, lstGK2.ItemCheck, lstGK3.ItemCheck, lstGK4.ItemCheck, lstGK5.ItemCheck, lstGK6.ItemCheck, lstGK7.ItemCheck, lstGK8.ItemCheck, lstGK9.ItemCheck, lstGK10.ItemCheck
        Static changing As Boolean = False
        If changing Then Exit Sub
        Dim lst As CheckedListBox = CType(sender, CheckedListBox)
        With lst
            If e.Index = 0 Then
                changing = True
                'alles
                For r As Integer = 0 To .Items.Count - 1
                    .SetItemCheckState(r, e.NewValue)
                Next
                changing = False
            Else
                changing = True
                Dim i As Integer
                For i = 1 To .Items.Count - 1
                    If i <> e.Index Then
                        If .GetItemCheckState(i) <> e.NewValue Then Exit For
                    End If
                Next
                If i = .Items.Count Then
                    .SetItemCheckState(0, e.NewValue)
                Else
                    .SetItemCheckState(0, CheckState.Unchecked)
                End If
                changing = False
            End If
        End With
        mnuSpeichern.Enabled = True
    End Sub

    Private Sub txtGr_Leave(sender As Object, e As EventArgs) Handles txtGr1.Leave, txtGr2.Leave, txtGr3.Leave, txtGr4.Leave, txtGr5.Leave, txtGr6.Leave, txtGr7.Leave, txtGr8.Leave, txtGr9.Leave, txtGr10.Leave
        Dim ctl As TextBox = CType(sender, TextBox)
        With ctl
            If .Text = "" Then
                cboGruppe.Items.Remove(.Text)
            Else
                cboGruppe.Items.Add(.Text)
            End If
        End With
    End Sub

    Private Sub txtGr_TextChanged(sender As Object, e As EventArgs) Handles txtGr1.TextChanged, txtGr2.TextChanged, txtGr3.TextChanged, txtGr4.TextChanged, txtGr5.TextChanged, txtGr6.TextChanged, txtGr7.TextChanged, txtGr8.TextChanged, txtGr9.TextChanged, txtGr10.TextChanged
        mnuSpeichern.Enabled = True
    End Sub

    Private Sub mnuSpeichern_Click(sender As Object, e As EventArgs) Handles mnuSpeichern.Click
        If mnuSpeichern.Enabled Then
            If Not mnuMeldungen.Checked Then
                Using New Centered_MessageBox(Me)
                    If MessageBox.Show("Neue Gruppen-Einteilung speichern?", Wettkampf.Bezeichnung, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                        Exit Sub
                    Else
                        Save()
                    End If
                End Using
            Else
                Save()
            End If
        End If
    End Sub

    Private Sub mnuBeenden_Click(sender As Object, e As EventArgs) Handles mnuBeenden.Click
        Close()
    End Sub

End Class