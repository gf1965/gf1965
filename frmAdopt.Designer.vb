﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdopt
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvJoin = New System.Windows.Forms.DataGridView()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnSelect = New System.Windows.Forms.Button()
        Me.lblWK = New System.Windows.Forms.Label()
        Me.Check = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Gruppe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nachname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vorname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sex = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jahrgang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Verein = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvJoin
        '
        Me.dgvJoin.AllowUserToAddRows = False
        Me.dgvJoin.AllowUserToDeleteRows = False
        Me.dgvJoin.AllowUserToOrderColumns = True
        Me.dgvJoin.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        Me.dgvJoin.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvJoin.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvJoin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvJoin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvJoin.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Check, Me.Gruppe, Me.Nachname, Me.Vorname, Me.Sex, Me.Jahrgang, Me.GK, Me.Verein})
        Me.dgvJoin.Location = New System.Drawing.Point(12, 12)
        Me.dgvJoin.MultiSelect = False
        Me.dgvJoin.Name = "dgvJoin"
        Me.dgvJoin.RowHeadersVisible = False
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.Honeydew
        Me.dgvJoin.RowsDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvJoin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvJoin.Size = New System.Drawing.Size(745, 355)
        Me.dgvJoin.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Enabled = False
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSave.Location = New System.Drawing.Point(551, 383)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(100, 25)
        Me.btnSave.TabIndex = 3
        Me.btnSave.Text = "Übernehmen"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnExit.Location = New System.Drawing.Point(657, 383)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(100, 25)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "Schließen"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSelect
        '
        Me.btnSelect.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSelect.ForeColor = System.Drawing.Color.Firebrick
        Me.btnSelect.Location = New System.Drawing.Point(12, 383)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(100, 25)
        Me.btnSelect.TabIndex = 5
        Me.btnSelect.Text = "WK auswählen"
        Me.btnSelect.UseVisualStyleBackColor = True
        '
        'lblWK
        '
        Me.lblWK.BackColor = System.Drawing.SystemColors.Window
        Me.lblWK.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblWK.Location = New System.Drawing.Point(118, 385)
        Me.lblWK.Name = "lblWK"
        Me.lblWK.Padding = New System.Windows.Forms.Padding(2, 0, 0, 0)
        Me.lblWK.Size = New System.Drawing.Size(276, 21)
        Me.lblWK.TabIndex = 6
        Me.lblWK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Check
        '
        Me.Check.DataPropertyName = "Check"
        Me.Check.FalseValue = "False"
        Me.Check.FillWeight = 15.0!
        Me.Check.HeaderText = ""
        Me.Check.IndeterminateValue = "Null"
        Me.Check.Name = "Check"
        Me.Check.TrueValue = "True"
        '
        'Gruppe
        '
        Me.Gruppe.DataPropertyName = "Gruppe"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Gruppe.DefaultCellStyle = DataGridViewCellStyle3
        Me.Gruppe.FillWeight = 30.0!
        Me.Gruppe.HeaderText = "Gruppe"
        Me.Gruppe.Name = "Gruppe"
        Me.Gruppe.ReadOnly = True
        '
        'Nachname
        '
        Me.Nachname.DataPropertyName = "Nachname"
        Me.Nachname.HeaderText = "Nachname"
        Me.Nachname.Name = "Nachname"
        Me.Nachname.ReadOnly = True
        Me.Nachname.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Vorname
        '
        Me.Vorname.DataPropertyName = "Vorname"
        Me.Vorname.HeaderText = "Vorname"
        Me.Vorname.Name = "Vorname"
        '
        'Sex
        '
        Me.Sex.DataPropertyName = "Geschlecht"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Sex.DefaultCellStyle = DataGridViewCellStyle4
        Me.Sex.FillWeight = 20.0!
        Me.Sex.HeaderText = "m/w"
        Me.Sex.Name = "Sex"
        Me.Sex.ReadOnly = True
        '
        'Jahrgang
        '
        Me.Jahrgang.DataPropertyName = "Jahrgang"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Jahrgang.DefaultCellStyle = DataGridViewCellStyle5
        Me.Jahrgang.FillWeight = 40.0!
        Me.Jahrgang.HeaderText = "Jahrgang"
        Me.Jahrgang.Name = "Jahrgang"
        Me.Jahrgang.ReadOnly = True
        '
        'GK
        '
        Me.GK.DataPropertyName = "GK"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.GK.DefaultCellStyle = DataGridViewCellStyle6
        Me.GK.FillWeight = 40.0!
        Me.GK.HeaderText = "GK"
        Me.GK.Name = "GK"
        Me.GK.ReadOnly = True
        '
        'Verein
        '
        Me.Verein.DataPropertyName = "Vereinsname"
        Me.Verein.FillWeight = 120.0!
        Me.Verein.HeaderText = "Verein"
        Me.Verein.Name = "Verein"
        Me.Verein.ReadOnly = True
        '
        'frmAdopt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(769, 420)
        Me.Controls.Add(Me.lblWK)
        Me.Controls.Add(Me.btnSelect)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.dgvJoin)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAdopt"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Ergebnisse übernehmen"
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvJoin As DataGridView
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents btnSelect As Button
    Friend WithEvents lblWK As Label
    Friend WithEvents Check As DataGridViewCheckBoxColumn
    Friend WithEvents Gruppe As DataGridViewTextBoxColumn
    Friend WithEvents Nachname As DataGridViewTextBoxColumn
    Friend WithEvents Vorname As DataGridViewTextBoxColumn
    Friend WithEvents Sex As DataGridViewTextBoxColumn
    Friend WithEvents Jahrgang As DataGridViewTextBoxColumn
    Friend WithEvents GK As DataGridViewTextBoxColumn
    Friend WithEvents Verein As DataGridViewTextBoxColumn
End Class
