﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmBohle
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBohle))
        Me.xTechniknote = New System.Windows.Forms.TextBox()
        Me.xTeilnehmer = New System.Windows.Forms.Label()
        Me.pnlWertung = New System.Windows.Forms.Panel()
        Me.xGültig3 = New System.Windows.Forms.TextBox()
        Me.xGültig2 = New System.Windows.Forms.TextBox()
        Me.xGültig1 = New System.Windows.Forms.TextBox()
        Me.xVerein = New System.Windows.Forms.Label()
        Me.xAK = New System.Windows.Forms.Label()
        Me.xVersuch = New System.Windows.Forms.Label()
        Me.xHantelgewicht = New System.Windows.Forms.Label()
        Me.xDurchgang = New System.Windows.Forms.Label()
        Me.xGewichtsklasse = New System.Windows.Forms.TextBox()
        Me.pnlHantel = New System.Windows.Forms.Panel()
        Me.JB_Port = New System.IO.Ports.SerialPort(Me.components)
        Me.Port1 = New System.IO.Ports.SerialPort(Me.components)
        Me.Port2 = New System.IO.Ports.SerialPort(Me.components)
        Me.Port3 = New System.IO.Ports.SerialPort(Me.components)
        Me.ZN_Port = New System.IO.Ports.SerialPort(Me.components)
        Me.ZA_Port = New System.IO.Ports.SerialPort(Me.components)
        Me.pnlWerbung = New System.Windows.Forms.Panel()
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.Dummy = New System.Windows.Forms.Button()
        Me.lblMsg = New System.Windows.Forms.Label()
        Me.xStartnummer = New System.Windows.Forms.Label()
        Me.picFlagge = New System.Windows.Forms.PictureBox()
        Me.xAufruf = New System.Windows.Forms.Label()
        Me.pnlWertung.SuspendLayout()
        CType(Me.picFlagge, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xTechniknote
        '
        Me.xTechniknote.BackColor = System.Drawing.Color.Black
        Me.xTechniknote.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xTechniknote.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xTechniknote.Font = New System.Drawing.Font("DS-Digital", 99.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xTechniknote.ForeColor = System.Drawing.Color.Lime
        Me.xTechniknote.Location = New System.Drawing.Point(328, 291)
        Me.xTechniknote.Name = "xTechniknote"
        Me.xTechniknote.Size = New System.Drawing.Size(226, 133)
        Me.xTechniknote.TabIndex = 15
        Me.xTechniknote.Text = "X,XX"
        Me.xTechniknote.WordWrap = False
        '
        'xTeilnehmer
        '
        Me.xTeilnehmer.AutoSize = True
        Me.xTeilnehmer.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xTeilnehmer.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.xTeilnehmer.ForeColor = System.Drawing.Color.Gold
        Me.xTeilnehmer.Location = New System.Drawing.Point(21, 23)
        Me.xTeilnehmer.Name = "xTeilnehmer"
        Me.xTeilnehmer.Size = New System.Drawing.Size(470, 63)
        Me.xTeilnehmer.TabIndex = 16
        Me.xTeilnehmer.Text = "Friedrich, Raphael"
        Me.xTeilnehmer.Visible = False
        '
        'pnlWertung
        '
        Me.pnlWertung.BackColor = System.Drawing.Color.Black
        Me.pnlWertung.Controls.Add(Me.xGültig3)
        Me.pnlWertung.Controls.Add(Me.xGültig2)
        Me.pnlWertung.Controls.Add(Me.xGültig1)
        Me.pnlWertung.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.pnlWertung.Location = New System.Drawing.Point(4, 283)
        Me.pnlWertung.Name = "pnlWertung"
        Me.pnlWertung.Size = New System.Drawing.Size(292, 164)
        Me.pnlWertung.TabIndex = 17
        '
        'xGültig3
        '
        Me.xGültig3.BackColor = System.Drawing.Color.Black
        Me.xGültig3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig3.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xGültig3.Font = New System.Drawing.Font("Wingdings", 100.0!)
        Me.xGültig3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.xGültig3.Location = New System.Drawing.Point(190, 2)
        Me.xGültig3.Name = "xGültig3"
        Me.xGültig3.ReadOnly = True
        Me.xGültig3.Size = New System.Drawing.Size(98, 148)
        Me.xGültig3.TabIndex = 17
        Me.xGültig3.Tag = "32"
        Me.xGültig3.Text = "l"
        Me.xGültig3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xGültig2
        '
        Me.xGültig2.BackColor = System.Drawing.Color.Black
        Me.xGültig2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig2.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xGültig2.Font = New System.Drawing.Font("Wingdings", 100.0!)
        Me.xGültig2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.xGültig2.Location = New System.Drawing.Point(96, 2)
        Me.xGültig2.Name = "xGültig2"
        Me.xGültig2.ReadOnly = True
        Me.xGültig2.Size = New System.Drawing.Size(98, 148)
        Me.xGültig2.TabIndex = 16
        Me.xGültig2.Tag = "8"
        Me.xGültig2.Text = "l"
        Me.xGültig2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xGültig1
        '
        Me.xGültig1.BackColor = System.Drawing.Color.Black
        Me.xGültig1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig1.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xGültig1.Font = New System.Drawing.Font("Wingdings", 100.0!)
        Me.xGültig1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.xGültig1.Location = New System.Drawing.Point(2, 2)
        Me.xGültig1.Name = "xGültig1"
        Me.xGültig1.ReadOnly = True
        Me.xGültig1.Size = New System.Drawing.Size(98, 148)
        Me.xGültig1.TabIndex = 15
        Me.xGültig1.Tag = "2"
        Me.xGültig1.Text = "l"
        Me.xGültig1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xVerein
        '
        Me.xVerein.AutoSize = True
        Me.xVerein.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xVerein.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.xVerein.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.xVerein.Location = New System.Drawing.Point(21, 109)
        Me.xVerein.Name = "xVerein"
        Me.xVerein.Size = New System.Drawing.Size(417, 63)
        Me.xVerein.TabIndex = 18
        Me.xVerein.Text = "TSG Rodewisch"
        Me.xVerein.Visible = False
        '
        'xAK
        '
        Me.xAK.AutoSize = True
        Me.xAK.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xAK.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.xAK.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.xAK.Location = New System.Drawing.Point(21, 196)
        Me.xAK.Name = "xAK"
        Me.xAK.Size = New System.Drawing.Size(235, 63)
        Me.xAK.TabIndex = 19
        Me.xAK.Text = "Junioren"
        Me.xAK.Visible = False
        '
        'xVersuch
        '
        Me.xVersuch.AutoSize = True
        Me.xVersuch.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xVersuch.Font = New System.Drawing.Font("Microsoft Sans Serif", 50.0!)
        Me.xVersuch.ForeColor = System.Drawing.Color.Coral
        Me.xVersuch.Location = New System.Drawing.Point(568, 186)
        Me.xVersuch.Name = "xVersuch"
        Me.xVersuch.Size = New System.Drawing.Size(70, 76)
        Me.xVersuch.TabIndex = 2
        Me.xVersuch.Text = "3"
        Me.xVersuch.Visible = False
        '
        'xHantelgewicht
        '
        Me.xHantelgewicht.AutoSize = True
        Me.xHantelgewicht.BackColor = System.Drawing.Color.Black
        Me.xHantelgewicht.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xHantelgewicht.Font = New System.Drawing.Font("Microsoft Sans Serif", 50.0!)
        Me.xHantelgewicht.ForeColor = System.Drawing.Color.Coral
        Me.xHantelgewicht.Location = New System.Drawing.Point(543, 99)
        Me.xHantelgewicht.Name = "xHantelgewicht"
        Me.xHantelgewicht.Padding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.xHantelgewicht.Size = New System.Drawing.Size(258, 76)
        Me.xHantelgewicht.TabIndex = 1
        Me.xHantelgewicht.Text = "262 kg"
        Me.xHantelgewicht.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.xHantelgewicht.Visible = False
        '
        'xDurchgang
        '
        Me.xDurchgang.AutoSize = True
        Me.xDurchgang.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xDurchgang.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!)
        Me.xDurchgang.ForeColor = System.Drawing.Color.Coral
        Me.xDurchgang.Location = New System.Drawing.Point(637, 210)
        Me.xDurchgang.Name = "xDurchgang"
        Me.xDurchgang.Size = New System.Drawing.Size(147, 46)
        Me.xDurchgang.TabIndex = 0
        Me.xDurchgang.Text = "Reißen"
        Me.xDurchgang.Visible = False
        '
        'xGewichtsklasse
        '
        Me.xGewichtsklasse.BackColor = System.Drawing.Color.Black
        Me.xGewichtsklasse.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGewichtsklasse.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xGewichtsklasse.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.xGewichtsklasse.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.xGewichtsklasse.Location = New System.Drawing.Point(336, 197)
        Me.xGewichtsklasse.Multiline = True
        Me.xGewichtsklasse.Name = "xGewichtsklasse"
        Me.xGewichtsklasse.Size = New System.Drawing.Size(195, 63)
        Me.xGewichtsklasse.TabIndex = 22
        Me.xGewichtsklasse.Text = "+135 kg"
        Me.xGewichtsklasse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.xGewichtsklasse.Visible = False
        '
        'pnlHantel
        '
        Me.pnlHantel.BackColor = System.Drawing.Color.Black
        Me.pnlHantel.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.pnlHantel.Location = New System.Drawing.Point(291, 272)
        Me.pnlHantel.Name = "pnlHantel"
        Me.pnlHantel.Size = New System.Drawing.Size(280, 175)
        Me.pnlHantel.TabIndex = 24
        Me.pnlHantel.Visible = False
        '
        'JB_Port
        '
        Me.JB_Port.PortName = "COM0"
        Me.JB_Port.ReadTimeout = 500
        Me.JB_Port.WriteTimeout = 500
        '
        'Port1
        '
        Me.Port1.PortName = "COM0"
        '
        'Port2
        '
        Me.Port2.PortName = "COM0"
        '
        'Port3
        '
        Me.Port3.PortName = "COM0"
        '
        'ZN_Port
        '
        Me.ZN_Port.PortName = "COM0"
        '
        'ZA_Port
        '
        Me.ZA_Port.PortName = "COM0"
        '
        'pnlWerbung
        '
        Me.pnlWerbung.BackColor = System.Drawing.Color.Black
        Me.pnlWerbung.BackgroundImage = Global.Gewichtheben.My.Resources.Resources.Logo_WL_VB_HG
        Me.pnlWerbung.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pnlWerbung.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.pnlWerbung.Location = New System.Drawing.Point(0, 0)
        Me.pnlWerbung.Name = "pnlWerbung"
        Me.pnlWerbung.Size = New System.Drawing.Size(27, 450)
        Me.pnlWerbung.TabIndex = 25
        Me.pnlWerbung.Visible = False
        '
        'lblInfo
        '
        Me.lblInfo.BackColor = System.Drawing.Color.Red
        Me.lblInfo.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.lblInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 45.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfo.ForeColor = System.Drawing.Color.Yellow
        Me.lblInfo.Location = New System.Drawing.Point(159, 97)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(39, 165)
        Me.lblInfo.TabIndex = 27
        Me.lblInfo.Text = "JURY-ENTSCHEIDUNG"
        Me.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblInfo.Visible = False
        '
        'Dummy
        '
        Me.Dummy.BackColor = System.Drawing.Color.Black
        Me.Dummy.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.Dummy.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.Dummy.FlatAppearance.BorderSize = 0
        Me.Dummy.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Dummy.ForeColor = System.Drawing.Color.White
        Me.Dummy.Location = New System.Drawing.Point(633, 263)
        Me.Dummy.Name = "Dummy"
        Me.Dummy.Size = New System.Drawing.Size(54, 23)
        Me.Dummy.TabIndex = 0
        Me.Dummy.UseVisualStyleBackColor = False
        '
        'lblMsg
        '
        Me.lblMsg.AutoSize = True
        Me.lblMsg.BackColor = System.Drawing.Color.Red
        Me.lblMsg.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.0!)
        Me.lblMsg.ForeColor = System.Drawing.Color.Yellow
        Me.lblMsg.Location = New System.Drawing.Point(18, 23)
        Me.lblMsg.Margin = New System.Windows.Forms.Padding(0)
        Me.lblMsg.Name = "lblMsg"
        Me.lblMsg.Padding = New System.Windows.Forms.Padding(0, 7, 0, 0)
        Me.lblMsg.Size = New System.Drawing.Size(83, 46)
        Me.lblMsg.TabIndex = 28
        Me.lblMsg.Text = "Msg"
        Me.lblMsg.Visible = False
        '
        'xStartnummer
        '
        Me.xStartnummer.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.xStartnummer.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.xStartnummer.ForeColor = System.Drawing.Color.Gold
        Me.xStartnummer.Location = New System.Drawing.Point(684, 23)
        Me.xStartnummer.Name = "xStartnummer"
        Me.xStartnummer.Padding = New System.Windows.Forms.Padding(25, 0, 5, 0)
        Me.xStartnummer.Size = New System.Drawing.Size(117, 63)
        Me.xStartnummer.TabIndex = 32
        Me.xStartnummer.Text = "20"
        Me.xStartnummer.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.xStartnummer.Visible = False
        '
        'picFlagge
        '
        Me.picFlagge.BackColor = System.Drawing.Color.Green
        Me.picFlagge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.picFlagge.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.picFlagge.ImageLocation = ""
        Me.picFlagge.Location = New System.Drawing.Point(33, 99)
        Me.picFlagge.Name = "picFlagge"
        Me.picFlagge.Padding = New System.Windows.Forms.Padding(1)
        Me.picFlagge.Size = New System.Drawing.Size(120, 84)
        Me.picFlagge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picFlagge.TabIndex = 0
        Me.picFlagge.TabStop = False
        Me.picFlagge.Visible = False
        '
        'xAufruf
        '
        Me.xAufruf.Font = New System.Drawing.Font("Microsoft Sans Serif", 84.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xAufruf.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf.Location = New System.Drawing.Point(406, 271)
        Me.xAufruf.Name = "xAufruf"
        Me.xAufruf.Size = New System.Drawing.Size(399, 173)
        Me.xAufruf.TabIndex = 34
        Me.xAufruf.Text = "0:00"
        Me.xAufruf.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmBohle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.xTechniknote)
        Me.Controls.Add(Me.xAK)
        Me.Controls.Add(Me.xGewichtsklasse)
        Me.Controls.Add(Me.xHantelgewicht)
        Me.Controls.Add(Me.xStartnummer)
        Me.Controls.Add(Me.xDurchgang)
        Me.Controls.Add(Me.xVersuch)
        Me.Controls.Add(Me.pnlWertung)
        Me.Controls.Add(Me.lblMsg)
        Me.Controls.Add(Me.Dummy)
        Me.Controls.Add(Me.picFlagge)
        Me.Controls.Add(Me.pnlWerbung)
        Me.Controls.Add(Me.xVerein)
        Me.Controls.Add(Me.xTeilnehmer)
        Me.Controls.Add(Me.pnlHantel)
        Me.Controls.Add(Me.xAufruf)
        Me.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(800, 450)
        Me.Name = "frmBohle"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Bohle"
        Me.pnlWertung.ResumeLayout(False)
        Me.pnlWertung.PerformLayout()
        CType(Me.picFlagge, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xTeilnehmer As System.Windows.Forms.Label
    Friend WithEvents pnlWertung As System.Windows.Forms.Panel
    Friend WithEvents xVerein As System.Windows.Forms.Label
    Friend WithEvents xAK As System.Windows.Forms.Label
    Friend WithEvents xVersuch As System.Windows.Forms.Label
    Friend WithEvents xHantelgewicht As System.Windows.Forms.Label
    Friend WithEvents xDurchgang As System.Windows.Forms.Label
    Friend WithEvents xGültig3 As System.Windows.Forms.TextBox
    Friend WithEvents xGültig2 As System.Windows.Forms.TextBox
    Friend WithEvents xGültig1 As System.Windows.Forms.TextBox
    Friend WithEvents xGewichtsklasse As System.Windows.Forms.TextBox
    Friend WithEvents pnlHantel As System.Windows.Forms.Panel
    Friend WithEvents Port2 As Ports.SerialPort
    Friend WithEvents Port3 As Ports.SerialPort
    Friend WithEvents JB_Port As SerialPort
    Friend WithEvents Port1 As SerialPort
    Friend WithEvents ZN_Port As SerialPort
    Friend WithEvents ZA_Port As SerialPort
    Friend WithEvents xTechniknote As TextBox
    Friend WithEvents pnlWerbung As Panel
    Friend WithEvents lblInfo As Label
    Friend WithEvents Dummy As Button
    Friend WithEvents lblMsg As Label
    Friend WithEvents xStartnummer As Label
    Friend WithEvents picFlagge As PictureBox
    Friend WithEvents xAufruf As Label
End Class
