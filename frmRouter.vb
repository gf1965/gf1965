﻿Public Class frmRouter

    Private _ipv4 As String
    Public Property IPv4 As String
        Get
            Return _ipv4
        End Get
        Set(value As String)
            _ipv4 = value
        End Set
    End Property

    Private Sub frmRouter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Cursor = Cursors.WaitCursor
        Dim dic = New Dictionary(Of String, String)
        dic.Add(String.Empty, String.Empty)

        'Alle Interfaces durchlaufen
        For Each iface As NetworkInformation.NetworkInterface In NetworkInformation.NetworkInterface.GetAllNetworkInterfaces
            'Wenn dem Interface eine oder mehrere Adresse zugewiesen ist
            If iface.GetIPProperties.UnicastAddresses().Count > 0 Then
                For i As Integer = 0 To iface.GetIPProperties.UnicastAddresses.Count - 1
                    'Loopback-Interfaces haben keine Subnet Mask
                    If iface.OperationalStatus = NetworkInformation.OperationalStatus.Up AndAlso
                            Not IPAddress.IsLoopback(iface.GetIPProperties.UnicastAddresses(0).Address) Then
                        Dim OpStatus = iface.OperationalStatus
                        Dim Device = iface.GetIPProperties.DnsSuffix
                        Dim GatewayAdresses = iface.GetIPProperties.GatewayAddresses.ToList
                        Dim GateWay = ""
                        For c = 0 To GatewayAdresses.Count - 1
                            If GatewayAdresses(c).Address.AddressFamily.Equals(AddressFamily.InterNetwork) Then
                                GateWay = GatewayAdresses(c).Address.ToString
                            End If
                        Next
                        Dim IPv4SubNetMask = iface.GetIPProperties.UnicastAddresses(i).IPv4Mask
                        Dim IP = iface.GetIPProperties.UnicastAddresses(i).Address

                        If IP.AddressFamily.Equals(AddressFamily.InterNetwork) Then dic.Add(Device.ToString + "  IP = " + GateWay, IP.ToString)
                    End If
                Next
            End If
        Next
        With cboRouter
            .DataSource = dic.ToList
            .DisplayMember = "Key"
            .ValueMember = "Value"
        End With
        Cursor = Cursors.Default
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        IPv4 = cboRouter.SelectedValue.ToString
        Hide()
    End Sub

End Class