﻿
Imports MySqlConnector

Public Class frmAthleten
    Dim cmd As MySqlCommand
    Dim Query As String
    Dim dtAthleten As New DataTable
    Dim dtVerein As New DataTable
    Dim dtRegion As New DataTable
    Dim dtStaat As New DataTable
    Dim dvVerein As DataView
    Dim dv As DataView
    WithEvents bs As BindingSource
    Dim IsNan As Boolean
    Dim txt As TextBox
    Dim cbo As ComboBox
    Dim btn As Button

    Class myAthlet
        Property Id As Integer
        Property Titel As String
        Property Nachname As String
        Property Vorname As String
        Property idVerein As Integer
        Property ESR As Integer
        Property MSR As Integer
        Property Jahrgang As Integer
        Property Geburtstag As Date
        Property Geschlecht As String
        Property state_id As Integer
        Property state_name As String
        Property Lizenz As Boolean
    End Class
    Public Athlet As New myAthlet

    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If btnSave.Enabled Then
            Using New Centered_MessageBox(Me)
                Dim result As DialogResult = MessageBox.Show("Änderungen an den Stammdaten speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                If result = DialogResult.Yes Then
                    btnSave.PerformClick()
                ElseIf result = DialogResult.Cancel Then
                    e.Cancel = True
                    Cursor = Cursors.Default
                Else
                    Tag = Nothing
                End If
            End Using
        End If
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        If Not IsNothing(Tag) Then
            MenuStrip1.Visible = False
            pnlFilter.Visible = False
            dgv.Visible = False
            pnlButtons.Visible = True
            Height = MinimumSize.Height
            Width = MinimumSize.Width
            btnExpand.Visible = True
            CancelButton = btnCancel
        End If

        Dim tTip As New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        tTip.SetToolTip(btnExpand, "Startrechte ein-/ausblenden")
        'tTip.SetToolTip(txtID, "automatisch vergebene Athleten-Nummer überschreiben")

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim dtTitel As New DataTable
        'Refresh()
        'SuspendLayout()

        If String.IsNullOrEmpty(User.ConnString) Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Anmeldung an MySQL-Server nicht nöglich." & vbNewLine & "(Keine Anmeldedaten vorhanden.)", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            Close()
            Return
        End If

        Using conn As New MySqlConnection((User.ConnString))
            Try
                conn.Open()

                Query = "Select a.*, v.Vereinsname, r.region_name Bundesland " &
                        "From athleten a " &
                        "Left Join verein v On v.idVerein = ifnull(a.ESR, a.Verein) " &
                        "Left Join region r On r.region_id = v.Region " &
                        "Order By a.Nachname, a.Vorname;"
                cmd = New MySqlCommand(Query, conn)
                dtAthleten.Load(cmd.ExecuteReader())

                Query = "Select v.idVerein, v.Vereinsname, v.Region, v.Staat FROM verein v WHERE idVerein > 0 ORDER BY v.Vereinsname;"
                cmd = New MySqlCommand(Query, conn)
                dtVerein.Load(cmd.ExecuteReader())

                Query = "Select region_id, region_name From region Where region_id > 0 Order by region_name;"
                cmd = New MySqlCommand(Query, conn)
                dtRegion.Load(cmd.ExecuteReader())

                Query = "Select state_id, state_name, state_flag From staaten Where state_id > 0 Order by state_name;"
                cmd = New MySqlCommand(Query, conn)
                dtStaat.Load(cmd.ExecuteReader())

                cmd = New MySqlCommand("Select Distinct Titel from athleten Order By Titel;", conn)
                dtTitel.Load(cmd.ExecuteReader())

            Catch ex As MySqlException
                MySQl_Error(Me, ex.Message, ex.StackTrace)
            End Try
        End Using

        If Not ConnError Then

            dtAthleten.Columns("Titel").AllowDBNull = True
            dtAthleten.Columns("Verein").AllowDBNull = True
            dtAthleten.Columns("Jahrgang").AllowDBNull = True

            With cboTitel
                .DataSource = dtTitel
                .DisplayMember = "Titel"
                .ValueMember = "Titel"
            End With

            With cboLand
                .DataSource = dtRegion
                .DisplayMember = "region_name"
                .ValueMember = "region_id"
            End With

            With cboStaat
                .DataSource = dtStaat
                .DisplayMember = "state_name"
                .ValueMember = "state_id"
            End With

            dvVerein = New DataView(dtVerein, "Region > 0", "Vereinsname", DataViewRowState.CurrentRows)
            With cboVereinsname
                .DataSource = New DataView(dtVerein, "Region > 0", "Vereinsname", DataViewRowState.CurrentRows)
                .DisplayMember = "Vereinsname"
                .ValueMember = "idVerein"
            End With

            With cboESR
                .DataSource = New DataView(dtVerein, "Region > 0", "Vereinsname", DataViewRowState.CurrentRows)
                .DisplayMember = "Vereinsname"
                .ValueMember = "idVerein"
            End With

            With cboMSR
                .DataSource = New DataView(dtVerein, "Region > 0", "Vereinsname", DataViewRowState.CurrentRows)
                .DisplayMember = "Vereinsname"
                .ValueMember = "idVerein"
            End With

            dv = New DataView(dtAthleten, Nothing, "Nachname, Vorname", DataViewRowState.CurrentRows)
            bs = New BindingSource With {.DataSource = dv}

            Build_Grid("Athleten", dgv)
            dgv.DataSource = bs

            txtID.DataBindings.Add(New Binding("Text", bs, "idTeilnehmer", False, DataSourceUpdateMode.OnPropertyChanged))
            cboTitel.DataBindings.Add(New Binding("Text", bs, "Titel", False, DataSourceUpdateMode.OnPropertyChanged))
            txtNachname.DataBindings.Add(New Binding("Text", bs, "Nachname", False, DataSourceUpdateMode.OnPropertyChanged))
            txtVorname.DataBindings.Add(New Binding("Text", bs, "Vorname", False, DataSourceUpdateMode.OnPropertyChanged))
            txtGeburtstag.DataBindings.Add(New Binding("Text", bs, "Geburtstag", False, DataSourceUpdateMode.OnPropertyChanged))
            cboGeschlecht.DataBindings.Add(New Binding("Text", bs, "Geschlecht", False, DataSourceUpdateMode.OnPropertyChanged))
            cboVereinsname.DataBindings.Add(New Binding("SelectedValue", bs, "Verein", False, DataSourceUpdateMode.OnPropertyChanged))
            cboLand.DataBindings.Add(New Binding("Text", bs, "Bundesland", False, DataSourceUpdateMode.OnPropertyChanged))
            cboStaat.DataBindings.Add(New Binding("SelectedValue", bs, "Staat", False, DataSourceUpdateMode.OnPropertyChanged))
            cboESR.DataBindings.Add(New Binding("SelectedValue", bs, "ESR", False, DataSourceUpdateMode.OnPropertyChanged))
            cboMSR.DataBindings.Add(New Binding("SelectedValue", bs, "MSR", False, DataSourceUpdateMode.OnPropertyChanged))
            chkLizenz.DataBindings.Add(New Binding("Checked", bs, "Lizenz", False, DataSourceUpdateMode.OnPropertyChanged))

            If Not IsNothing(Tag) Then
                ' Aufruf aus anderer Form
                If Tag.ToString.Equals("New") Then
                    Text = "Neuer Heber"
                    btnNew.PerformClick()
                Else
                    ' ausgewählter Athlet
                    Text = "Stammdaten bearbeiten"
                    bs.Position = bs.Find("idTeilnehmer", Tag)
                End If
            End If
        End If
        'ResumeLayout()
        btnSave.Enabled = False 'Not IsNothing(Tag)

        If bs.Count = 0 Then btnNew.PerformClick()

        txtNachname.Focus()
        Cursor = Cursors.Default
    End Sub

    Private Sub Build_Grid(Bereich As String, grid As DataGridView)
        With grid
            .AutoGenerateColumns = False
            .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
            .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor

            Dim ColWidth() As Integer = {40, 110, 110, 80, 80, 120, 110}
            Dim ColBez() As String = {"ID", "Nachname", "Vorname", "Geschlecht", "Geburtstag", "Verein", "Bundesland"}
            Dim ColData() As String = {"idTeilnehmer", "Nachname", "Vorname", "Geschlecht", "Geburtstag", "Vereinsname", "Bundesland"}

            With .Columns
                .Add(New DataGridViewTextBoxColumn With {.Name = "ID"})
                .Add(New DataGridViewTextBoxColumn With {.Name = "Nachname"})
                .Add(New DataGridViewTextBoxColumn With {.Name = "Vorname"})
                .Add(New DataGridViewTextBoxColumn With {.Name = "Geschlecht"})
                .Add(New DataGridViewTextBoxColumn With {.Name = "Geburtstag"})
                .Add(New DataGridViewTextBoxColumn With {.Name = "Verein", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Add(New DataGridViewTextBoxColumn With {.Name = "Bundesland"})
            End With
            For i = 0 To ColWidth.Count - 1
                With .Columns(i)
                    .Width = ColWidth(i)
                    .HeaderText = ColBez(i)
                    .DataPropertyName = ColData(i)
                End With
            Next
        End With
        'Format_Grid(Bereich, grid)
    End Sub

    Private Sub mnuClose_Click(sender As Object, e As EventArgs) Handles mnuClose.Click, btnClose.Click
        Close()
    End Sub
    Private Sub mnuDelete_Click(sender As Object, e As EventArgs) Handles mnuDelete.Click
        dv(bs.Position).Delete()
        btnSave.Enabled = True
    End Sub

    '' Aufruf aus anderer Form --> Return
    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        btnSave.PerformClick()
        If ConnError Then Return

        ' successfully saved, return to calling form
        DialogResult = DialogResult.OK
        Dim pos As Integer
        If IsNumeric(Tag) Then
            pos = bs.Find("idTeilnehmer", Tag)
        Else
            pos = bs.Find("idTeilnehmer", txtID.Text)
        End If

        With Athlet
            .Id = CInt(dv(pos)!idTeilnehmer)
            .Titel = dv(pos)!Titel.ToString
            .Nachname = dv(pos)!Nachname.ToString
            .Vorname = dv(pos)!Vorname.ToString
            .idVerein = If(IsDBNull(dv(pos)!Verein), 0, CInt(dv(pos)!Verein))
            .ESR = If(IsDBNull(dv(pos)!ESR), 0, CInt(dv(pos)!ESR))
            .MSR = If(IsDBNull(dv(pos)!MSR), 0, CInt(dv(pos)!MSR))
            .Jahrgang = Year(CDate(dv(pos)!Geburtstag))
            .Geburtstag = CDate(dv(pos)!Geburtstag)
            .Geschlecht = dv(pos)!Geschlecht.ToString
            .state_id = CInt(dv(pos)!Staat)
            .state_name = cboStaat.Text
            .Lizenz = CBool(dv(pos)!Lizenz)
        End With

        Close()
    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Tag = Nothing
    End Sub

    '' Buttons
    Private Sub mnuVereinNeu_Click(sender As Object, e As EventArgs) Handles mnuVereinNeu.Click
        Dim old_value = cboVereinsname.SelectedValue
        Using formVerein As New frmVerein
            With formVerein
                .Tag = "New"
                .ShowDialog(Me)
                If IsNothing(.Tag) Then Return
                Dim row = DirectCast(.Tag, DataRow)
                If IsNumeric(old_value) AndAlso CInt(old_value) = CInt(row(0)) Then Return
                Cursor = Cursors.WaitCursor
                '(0): idVerein
                '(1): Vereinsname
                '(2): Region
                '(3): Staat
                '(4): Adresse
                '(5): PLZ
                '(6): Ort
                '(7): region_name = Null
                '(8): region_3 = Null
                '(9): state_name) = Null
                Dim nrow = dtVerein.NewRow()
                nrow.ItemArray = {row(0), row(1), row(2), row(3)}
                dtVerein.Rows.Add(nrow)
                cboVereinsname.SelectedValue = row(0)
            End With
        End Using
        btnSave.Enabled = True
        cboVereinsname.Focus()
        Cursor = Cursors.Default
    End Sub
    Private Sub btnExpand_Click(sender As Object, e As EventArgs) Handles btnExpand.Click
        If Width = MaximumSize.Width Then
            Width = MinimumSize.Width
            btnExpand.Image = arrow_right_16
        Else
            Width = MaximumSize.Width
            btnExpand.Image = arrow_left_16
        End If
        grpStartrecht.Enabled = Width = MaximumSize.Width
    End Sub
    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click, mnuNew.Click

        Dim row = dtAthleten.NewRow()
        Using GF As New myFunction
            Dim LastId = GF.Get_LastId("athleten", "idTeilnehmer")
            If Not IsNothing(LastId) Then
                row!idTeilnehmer = LastId + 1
            Else
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Datenbank-Fehler beim Erstellen des neuen Datensatzes", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
                Return
            End If
        End Using
        row!Nachname = String.Empty
        row!Vorname = String.Empty
        row!Staat = 0
        row!Lizenz = 1
        dtAthleten.Rows.Add(row)
        bs.MoveFirst()

        cboGeschlecht.SelectedIndex = -1
        cboLand.SelectedIndex = -1
        chkLizenz.Checked = True

        btnSave.Enabled = True
        btnNew.Enabled = False
        pnlFilter.Enabled = False
        dgv.Enabled = False

        txtNachname.Focus()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click, mnuSave.Click

        Validate()
        bs.EndEdit()

#Region "Validate"
        If dv.Count > 0 Then
            If String.IsNullOrEmpty(txtNachname.Text) OrElse String.IsNullOrEmpty(txtVorname.Text) Then
                Dim msg = If(String.IsNullOrEmpty(txtNachname.Text), "Nachname", "Vorname")
                Using New Centered_MessageBox(Me)
                    MessageBox.Show(msg & " nicht korrekt eingegeben.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If msg.Equals("Nachname") Then
                        txtNachname.Focus()
                    Else
                        txtVorname.Focus()
                    End If
                    Return
                End Using
            End If

            If String.IsNullOrEmpty(txtGeburtstag.Text) OrElse txtGeburtstag.Text.Equals("  .  .") Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Geburtstag/Jahrgang nicht korrekt eingegeben.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtGeburtstag.Focus()
                    Return
                End Using
            End If

            If cboGeschlecht.SelectedIndex = -1 Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Geschlecht nicht ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                cboGeschlecht.Focus()
                Return
            End If

            If IsNothing(cboStaat.SelectedValue) Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Nationalität (Staat) nicht ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                cboStaat.Focus()
                Return
            End If
        End If
#End Region

        Cursor = Cursors.WaitCursor

        Dim tn As Integer
        Dim changes As DataTable = dtAthleten.GetChanges()
        If Not IsNothing(changes) Then
            Using conn As New MySqlConnection(User.ConnString)
                For Each row As DataRow In changes.Rows
                    If row.RowState = DataRowState.Deleted Then
                        Query = "DELETE FROM athleten WHERE idTeilnehmer = @tn;"
                    Else
                        Query = "INSERT INTO athleten (idTeilnehmer, Titel, Nachname, Vorname, Verein, Jahrgang, Geburtstag, Geschlecht, ESR, MSR, Staat, Lizenz) " &
                                              "VALUES (@tn, @title, @name, @fname, @verein, @jg, @dob, @sex, @esr, @msr, @staat, @license) " &
                                              "ON DUPLICATE KEY UPDATE Titel = @title, Nachname = @name, Vorname = @fname, Verein = @verein, Jahrgang = @jg, Geburtstag = @dob, " &
                                              "Geschlecht = @sex, ESR = @esr, MSR = @msr, Staat = @staat, Lizenz = @license;"
                    End If
                    If Not row.RowState = DataRowState.Deleted Then
                        If IsDBNull(row!Titel) Then row!Titel = cboTitel.Text
                        If IsDBNull(row!Verein) Then row!Verein = 0 'CreateVerein(row)
                    End If
                    cmd = New MySqlCommand(Query, conn)
                    With cmd.Parameters
                        If Not row.RowState = DataRowState.Deleted Then
                            .AddWithValue("@tn", row!idTeilnehmer)
                            .AddWithValue("@title", row!Titel)
                            .AddWithValue("@name", row!Nachname)
                            .AddWithValue("@fname", row!Vorname)
                            .AddWithValue("@verein", IIf(IsDBNull(row!ESR), 0, row!ESR))
                            .AddWithValue("@esr", IIf(IsDBNull(row!ESR), 0, row!ESR))
                            .AddWithValue("@msr", IIf(IsDBNull(row!MSR), 0, row!MSR))
                            .AddWithValue("@dob", row!Geburtstag)
                            .AddWithValue("@jg", Year(CDate(row!Geburtstag)))
                            .AddWithValue("@sex", row!Geschlecht)
                            .AddWithValue("@staat", row!Staat)
                            .AddWithValue("@license", row!Lizenz)
                        ElseIf row.RowState = DataRowState.Deleted Then
                            .AddWithValue("@tn", row("idTeilnehmer", DataRowVersion.Original))
                        End If
                    End With
                    Do
                        Try
                            If conn.State = ConnectionState.Closed Then conn.Open()
                            cmd.ExecuteNonQuery()
                            If row.RowState = DataRowState.Added Then
                                'tn = CInt(cmd.LastInsertedId)
                                'Dim r = dtAthleten.Select("idTeilnehmer = " & row!idTeilnehmer.ToString)
                                'r(0)!idTeilnehmer = tn
                                tn = CInt(row!idTeilnehmer)
                            End If
                            ConnError = False
                        Catch ex As Exception
                            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                        End Try
                    Loop While ConnError
                Next
            End Using
        End If

        Cursor = Cursors.Default

        If ConnError Then Return

        dtAthleten.AcceptChanges()
        If tn > 0 Then
            Dim pos = bs.Find("idTeilnehmer", tn)
            bs.Position = pos
        End If
        btnSave.Enabled = False
        btnNew.Enabled = True
        pnlFilter.Enabled = True
        dgv.Enabled = True
    End Sub
    Private Sub btnNew_EnabledChanged(sender As Object, e As EventArgs) Handles btnNew.EnabledChanged
        mnuNew.Enabled = btnNew.Enabled
    End Sub
    Private Sub btnSave_EnabledChanged(sender As Object, e As EventArgs) Handles btnSave.EnabledChanged
        mnuSave.Enabled = btnSave.Enabled
        btnAccept.Enabled = btnSave.Enabled
    End Sub
    Private Function CreateVerein(row As DataRow) As Object
        If CInt(row!Staat) <> 42 Then
            ' nicht Deutschland
            Dim x = dtStaat.Select("state_id = " & row!Staat.ToString)
            Dim tmp = dtVerein.Select("Staat = " & row!Staat.ToString)
            If tmp.Count = 0 Then
                Using conn As New MySqlConnection(User.ConnString)
                    Try
                        cmd = New MySqlCommand("INSERT INTO verein (Vereinsname, Staat) VALUES (@name, @id);", conn)
                        With cmd.Parameters
                            .AddWithValue("@name", x(0)!state_name)
                            .AddWithValue("@id", x(0)!state_id)
                        End With
                        conn.Open()
                        cmd.ExecuteNonQuery()
                        Dim nrow = dtVerein.NewRow()
                        nrow.ItemArray = {cmd.LastInsertedId, cmd.Parameters("@name").Value, 0, cmd.Parameters("@id").Value}
                        dtVerein.Rows.Add(nrow)
                        cboVereinsname.SelectedValue = cmd.LastInsertedId
                        Return cmd.LastInsertedId
                    Catch ex As Exception
                        Return 0
                    End Try
                End Using
            Else
                Dim s = dvVerein.Find(x(0)!state_name)
                If s = -1 Then Return 0
                Return dvVerein(s)!idVerein
            End If
        Else
            Return 0
        End If
    End Function

    '' Filter
    Private Sub btnFilter_Click(sender As Object, e As EventArgs) Handles btnName.Click, btnVerein.Click, btnLand.Click
        With CType(sender, Button)
            Select Case .Name
                Case "btnName"
                    With txtName
                        If .ForeColor <> Color.DarkGray Then .Text = ""
                        .Focus()
                    End With
                Case "btnVerein"
                    With txtVerein
                        If .ForeColor <> Color.DarkGray Then .Text = ""
                        .Focus()
                    End With
                Case "btnBundesland"
                    With txtLand
                        If .ForeColor <> Color.DarkGray Then .Text = ""
                        .Focus()
                    End With
            End Select
        End With
    End Sub
    Private Sub txtFilter_GotFocus(sender As Object, e As EventArgs) Handles txtName.GotFocus, txtVerein.GotFocus, txtLand.GotFocus, txtName.Click, txtVerein.Click, txtLand.Click
        With CType(sender, TextBox)
            If .ForeColor = Color.DarkGray Then
                .SelectionLength = 0
                .SelectionStart = 0
            End If
        End With
    End Sub
    Private Sub txtFilter_KeyDown(sender As Object, e As KeyEventArgs) Handles txtName.KeyDown, txtVerein.KeyDown, txtLand.KeyDown
        With CType(sender, TextBox)
            Select Case e.KeyCode
                Case Keys.Escape
                    e.Handled = True
                    If .ForeColor <> Color.DarkGray Then .Text = ""
                    dgv.Focus()
            End Select
        End With
    End Sub
    Private Sub txtFilter_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtName.KeyPress, txtVerein.KeyPress, txtLand.KeyPress
        With CType(sender, TextBox)
            Select Case e.KeyChar
                Case CChar(vbBack)
                    Exit Sub
                Case Else
                    If .ForeColor = Color.DarkGray Then
                        e.Handled = True
                        If Asc(e.KeyChar) <> 27 Then
                            .Text = e.KeyChar
                            .ForeColor = Color.FromKnownColor(KnownColor.WindowText)
                        End If
                        .SelectionStart = .Text.Length
                    End If
            End Select
        End With
    End Sub
    Private Sub txtFilter_TextChanged(sender As Object, e As EventArgs) Handles txtName.TextChanged, txtVerein.TextChanged, txtLand.TextChanged
        If IsNothing(bs) Then Return
        With CType(sender, TextBox)
            If .Text = String.Empty Then
                .ForeColor = Color.DarkGray
                .Text = .Tag.ToString ' Vorgabe bei inaktiver Box
            ElseIf .Text = .Tag.ToString And .ForeColor = Color.DarkGray Then
                bs.Filter = ""
            ElseIf .Text.Length > 0 And .ForeColor <> Color.DarkGray Then
                bs.Filter = .Tag.ToString & " Like '*" & .Text & "*'"
            End If
        End With
    End Sub

    '' Input
    Private Sub cboStaat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboStaat.SelectedIndexChanged
        Dim FileName = String.Empty
        Try
            If Not IsNothing(cboStaat.SelectedItem) Then FileName = Path.Combine(Application.StartupPath, "flag", DirectCast(cboStaat.SelectedItem, DataRowView)("state_flag").ToString)
        Catch ex As Exception
            FileName = Path.Combine(Application.StartupPath, "flag", "Empty.png")
        End Try
        picFlagge.ImageLocation = FileName

        If cboStaat.Focused Then btnSave.Enabled = True
    End Sub
    Private Sub cboVereine_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVereinsname.SelectedIndexChanged, cboESR.SelectedIndexChanged, cboMSR.SelectedIndexChanged

        Dim ctl = CType(sender, ComboBox)

        If ctl.Focused Then
            btnSave.Enabled = True
        Else
            Return
        End If

        dv(bs.Position)!Vereinsname = cboVereinsname.Text

        SearchVerein(ctl.Name.Substring(3), False)

        If Not ctl.Name.Substring(3).Contains("Verein") Then Return

        Try
            cboStaat.SelectedValue = DirectCast(cboVereinsname.SelectedItem, DataRowView)("Staat")
        Catch ex As Exception
        End Try
        Try
            cboLand.SelectedValue = DirectCast(cboVereinsname.SelectedItem, DataRowView)("Region")
        Catch ex As Exception
        End Try
        Try
            cboESR.SelectedValue = DirectCast(cboVereinsname.SelectedItem, DataRowView)("idVerein")
        Catch ex As Exception
        End Try
        Try
            cboMSR.SelectedValue = DirectCast(cboVereinsname.SelectedItem, DataRowView)("idVerein")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGeschlecht.SelectedIndexChanged, cboESR.SelectedIndexChanged, cboMSR.SelectedIndexChanged
        If CType(sender, ComboBox).Focused Then btnSave.Enabled = True
    End Sub
    Private Sub TextBox_TextChanged(sender As Object, e As EventArgs) Handles txtNachname.TextChanged, txtVorname.TextChanged
        If CType(sender, TextBox).Focused Then btnSave.Enabled = True
    End Sub


    Private Sub txtID_TextChanged(sender As Object, e As EventArgs) Handles txtID.TextChanged
        If txtID.Focused Then btnSave.Enabled = True
    End Sub
    Private Sub txtID_EnabledChanged(sender As Object, e As EventArgs) Handles txtID.EnabledChanged
        btnEditID.Enabled = Not txtID.Enabled
        If Not txtID.Enabled Then txtID.Tag = Nothing
    End Sub
    Private Sub txtID_GotFocus(sender As Object, e As EventArgs) Handles txtID.GotFocus
        txtID.Tag = txtID.Text
        CancelButton = Nothing
    End Sub
    Private Sub txtID_KeyDown(sender As Object, e As KeyEventArgs) Handles txtID.KeyDown
        IsNan = False
        Select Case e.KeyCode
            Case Keys.Escape
                e.Handled = True
                e.SuppressKeyPress = True
                Dim row = dv(bs.Position).Row
                txtID.Text = row("idTeilnehmer", DataRowVersion.Original).ToString
                txtID.Enabled = False
                txtNachname.Focus()
            Case Else
                Using GF As New myFunction
                    IsNan = GF.IsNan(e.KeyCode)
                End Using
        End Select
        If ModifierKeys = Keys.Shift Then
            IsNan = True
        End If
    End Sub
    Private Sub txtID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtID.KeyPress
        If IsNan Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtID_LostFocus(sender As Object, e As EventArgs) Handles txtID.LostFocus
        If txtID.Text.Equals(String.Empty) Then
            Using New Centered_MessageBox(Me, {"Auslesen", "Eingeben"})
                If MessageBox.Show("Die ID muss eine Zahl größer 0 sein." & "Sie kann aus der Datenbank ausgelesen oder manuell vergeben werden.", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Error) = DialogResult.Yes Then
                    Dim row = dv(bs.Position).Row
                    If row.RowState = DataRowState.Added Then
                        Using GF As New myFunction
                            txtID.Text = (GF.Get_LastId("athleten", "idTeilnehmer") + 1).ToString
                        End Using
                    Else
                        txtID.Text = row("idTeilnehmer", DataRowVersion.Original).ToString
                    End If
                Else
                    txtID.Focus()
                End If
            End Using
        End If
        CancelButton = btnCancel
    End Sub


    Private Sub txtGeburtstag_Leave(sender As Object, e As EventArgs) Handles txtGeburtstag.Leave
        With txtGeburtstag
            Do While .Text.Contains("  .")
                .Text = .Text.Replace("  .", "01.")
            Loop
        End With
    End Sub
    Private Sub txtGeburtstag_TextChanged(sender As Object, e As EventArgs) Handles txtGeburtstag.TextChanged
        If txtGeburtstag.Focused AndAlso Not String.IsNullOrEmpty(txtGeburtstag.Text) Then btnSave.Enabled = True
    End Sub

    '' MouseWheel ausschalten
    Private Sub Prohibited_MouseWheel(sender As Object, e As MouseEventArgs) Handles cboGeschlecht.MouseWheel, cboVereinsname.MouseWheel, cboLand.MouseWheel, cboStaat.MouseWheel,
                                                                                     cboESR.MouseWheel, cboMSR.MouseWheel, cboTitel.MouseWheel
        If Not CType(sender, Control).Capture Then
            Dim HMEA As HandledMouseEventArgs = DirectCast(e, HandledMouseEventArgs)
            HMEA.Handled = True
        End If
    End Sub

    Private Sub mnuMauszeiger_Click(sender As Object, e As EventArgs) Handles mnuMauszeiger.Click
        Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
        Cursor.Show()
    End Sub

    Private Sub chkLizenz_CheckedChanged(sender As Object, e As EventArgs) Handles chkLizenz.CheckedChanged
        If chkLizenz.Focused Then btnSave.Enabled = True
    End Sub

    Private Sub btnVereine_Click(sender As Object, e As EventArgs) Handles btnVereinsname.Click, btnESR.Click, btnMSR.Click
        SearchVerein(CType(sender, Button).Name.Substring(3), True)
    End Sub

    Private Sub txtVereine_TextChanged(sender As Object, e As EventArgs) Handles txtVereinsname.TextChanged, txtESR.TextChanged, txtMSR.TextChanged

        If IsNothing(dvVerein) Then Return

        Dim dv As DataView = Nothing

        Select Case CType(sender, TextBox).Name.Substring(3)
            Case "Vereinsname"
                txt = txtVereinsname
                cbo = cboVereinsname
                dv = CType(cboVereinsname.DataSource, DataView)
            Case "ESR"
                txt = txtESR
                cbo = cboESR
                dv = CType(cboESR.DataSource, DataView)
            Case "MSR"
                txt = txtMSR
                cbo = cboMSR
                dv = CType(cboMSR.DataSource, DataView)
        End Select

        dv.RowFilter = "Vereinsname LIKE '*" + txt.Text + "*'"
    End Sub

    Private Sub txtVereine_KeyDown(sender As Object, e As KeyEventArgs) Handles txtVereinsname.KeyDown, txtESR.KeyDown, txtMSR.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter, Keys.Escape
                e.SuppressKeyPress = True
                SearchVerein(CType(sender, TextBox).Name.Substring(3), False)
        End Select
    End Sub

    Private Sub SearchVerein(Control As String, Activated As Boolean)

        Select Case Control
            Case "Vereinsname"
                txt = txtVereinsname
                cbo = cboVereinsname
                btn = btnVereinsname
            Case "ESR"
                txt = txtESR
                cbo = cboESR
                btn = btnESR
            Case "MSR"
                txt = txtMSR
                cbo = cboMSR
                btn = btnMSR
        End Select

        With txt
            .Visible = Activated
            cbo.DroppedDown = Activated
            btn.Enabled = Not Activated
            If Activated Then
                CancelButton = Nothing
                AcceptButton = Nothing
                .Text = String.Empty
                .BringToFront()
                .Focus()
            Else
                CancelButton = btnCancel
                AcceptButton = btnAccept
                cbo.Focus()
            End If
        End With
    End Sub

    Private Sub cboVereine_DropDown(sender As Object, e As EventArgs) Handles cboVereinsname.DropDown, cboESR.DragDrop, cboMSR.DragDrop
        Dim ctl = CType(sender, ComboBox)
        Dim verein = ctl.SelectedItem
        Dim dv As DataView = Nothing

        Select Case ctl.Name.Substring(3)
            Case "Vereinsname"
                dv = CType(cboVereinsname.DataSource, DataView)
            Case "ESR"
                dv = CType(cboESR.DataSource, DataView)
            Case "MSR"
                dv = CType(cboMSR.DataSource, DataView)
        End Select

        dv.RowFilter = ""

        Try
            If Not IsNothing(verein) Then ctl.SelectedValue = DirectCast(verein, DataRowView)("idVerein")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub mnuNotiz_Click(sender As Object, e As EventArgs) Handles mnuNotiz.Click
        Using formNotiz As New frmNotiz
            With formNotiz
                .Text = "Notiz - " & txtNachname.Text & ", " & txtVorname.Text
                .AthleteId = CInt(txtID.Text)
                .ShowDialog(Me)
            End With
        End Using
    End Sub

    Private Sub btnEditID_Click(sender As Object, e As EventArgs) Handles btnEditID.Click
        With txtID
            .Enabled = True
            .Focus()
        End With
    End Sub

End Class
