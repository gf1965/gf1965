﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmKampfgericht
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvKR = New System.Windows.Forms.DataGridView()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.Gruppe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KR_1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KR_2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KR_3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ZN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvKR, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvKR
        '
        Me.dgvKR.AllowUserToAddRows = False
        Me.dgvKR.AllowUserToDeleteRows = False
        Me.dgvKR.AllowUserToResizeColumns = False
        Me.dgvKR.AllowUserToResizeRows = False
        Me.dgvKR.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvKR.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvKR.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvKR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvKR.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Gruppe, Me.KR_1, Me.KR_2, Me.KR_3, Me.ZN, Me.TC, Me.CM, Me.Sec})
        Me.dgvKR.Location = New System.Drawing.Point(12, 12)
        Me.dgvKR.Name = "dgvKR"
        Me.dgvKR.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvKR.RowHeadersVisible = False
        Me.dgvKR.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvKR.Size = New System.Drawing.Size(950, 275)
        Me.dgvKR.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(756, 303)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(100, 27)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "Speichern"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(862, 303)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(100, 27)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Beenden"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Gruppe
        '
        Me.Gruppe.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.Gruppe.DataPropertyName = "Gruppe"
        Me.Gruppe.FillWeight = 50.0!
        Me.Gruppe.HeaderText = "Gruppe"
        Me.Gruppe.Name = "Gruppe"
        Me.Gruppe.ReadOnly = True
        Me.Gruppe.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Gruppe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Gruppe.Width = 48
        '
        'KR_1
        '
        Me.KR_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.KR_1.DataPropertyName = "KR_1"
        Me.KR_1.HeaderText = "KR 1"
        Me.KR_1.Name = "KR_1"
        Me.KR_1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'KR_2
        '
        Me.KR_2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.KR_2.DataPropertyName = "KR_2"
        Me.KR_2.HeaderText = "KR 2"
        Me.KR_2.Name = "KR_2"
        Me.KR_2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'KR_3
        '
        Me.KR_3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.KR_3.DataPropertyName = "KR_3"
        Me.KR_3.HeaderText = "KR 3"
        Me.KR_3.Name = "KR_3"
        Me.KR_3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'ZN
        '
        Me.ZN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ZN.DataPropertyName = "ZN"
        Me.ZN.HeaderText = "ZN"
        Me.ZN.Name = "ZN"
        Me.ZN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ZN.ToolTipText = "Zeitnehmer"
        '
        'TC
        '
        Me.TC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.TC.DataPropertyName = "TC"
        Me.TC.HeaderText = "TC"
        Me.TC.Name = "TC"
        Me.TC.ToolTipText = "Technischer Kontrolleur"
        '
        'CM
        '
        Me.CM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CM.DataPropertyName = "CM"
        Me.CM.HeaderText = "CM"
        Me.CM.Name = "CM"
        Me.CM.ToolTipText = "Chief Marshal"
        '
        'Sec
        '
        Me.Sec.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Sec.DataPropertyName = "Sec"
        Me.Sec.HeaderText = "Sekretär"
        Me.Sec.Name = "Sec"
        '
        'frmKampfgericht
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(974, 342)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.dgvKR)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmKampfgericht"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Kampfgericht"
        CType(Me.dgvKR, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgvKR As DataGridView
    Friend WithEvents btnSave As Button
    Friend WithEvents btnClose As Button
    Friend WithEvents Gruppe As DataGridViewTextBoxColumn
    Friend WithEvents KR_1 As DataGridViewTextBoxColumn
    Friend WithEvents KR_2 As DataGridViewTextBoxColumn
    Friend WithEvents KR_3 As DataGridViewTextBoxColumn
    Friend WithEvents ZN As DataGridViewTextBoxColumn
    Friend WithEvents TC As DataGridViewTextBoxColumn
    Friend WithEvents CM As DataGridViewTextBoxColumn
    Friend WithEvents Sec As DataGridViewTextBoxColumn
End Class
