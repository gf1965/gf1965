﻿Imports System.ComponentModel

Public Class frmJuryEntscheid

    Private Sub frmJuryEntscheid_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            With formLeader
                Location = New Point(.Bounds.X + 400, .Bounds.Y + 250)
            End With
        Catch ex As Exception
            With Screen.FromControl(formLeader).Bounds
                Location = New Point(CInt(.X + (.Width - Width) / 2), CInt(.Y + (.Height - Height) / 2))
            End With
        End Try
        'formLeader.Set_Wert.manuelleWertung = DialogResult.None
    End Sub

    Private Sub frmJuryEntscheid_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        txtDummy.Focus()
    End Sub

    Private Sub btnGültig_Click(sender As Object, e As EventArgs) Handles btnGültig.Click
        If Not IsNothing(Tag) Then
            'If Tag.ToString = "Wertung" Then formLeader.Set_Wert.manuelleWertung = DialogResult.Yes
        Else
            DialogResult = DialogResult.Yes
            Close()
        End If
    End Sub

    Private Sub btnUngültig_Click(sender As Object, e As EventArgs) Handles btnUngültig.Click
        If Not IsNothing(Tag) Then
            'If Tag.ToString = "Wertung" Then formLeader.Set_Wert.manuelleWertung = DialogResult.No
        Else
            DialogResult = DialogResult.No
            Close()
        End If
    End Sub

    Private Sub frmJuryEntscheid_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Tag = Nothing
    End Sub

End Class