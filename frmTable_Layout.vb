﻿Public Class frmLayout

    Declare Function GetForegroundWindow Lib "user32" () As IntPtr
    Declare Function MoveWindow Lib "User32" (hWnd As IntPtr, x As Int32, y As Int32, width As Int32, height As Int32, redraw As Boolean) As Boolean

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim r, c As Integer, x As String

        INI_Write("Meldung", "ForeColor", cboForeColor.BackColor.ToArgb.ToString, INI_File)
        INI_Write("Meldung", "BackColor", cboBackColor.BackColor.ToArgb.ToString, INI_File)
        INI_Write("Meldung", "SelForeColor", cboSelForeColor.BackColor.ToArgb.ToString, INI_File)
        INI_Write("Meldung", "SelBackColor", cboSelBackColor.BackColor.ToArgb.ToString, INI_File)
        INI_Write("Meldung", "ActForeColor", cboAktuellerFC.BackColor.ToArgb.ToString, INI_File)
        INI_Write("Meldung", "ActBackColor", cboAktuellerBC.BackColor.ToArgb.ToString, INI_File)
        INI_Write("Meldung", "NextForeColor", cboNächsterFC.BackColor.ToArgb.ToString, INI_File)
        INI_Write("Meldung", "NextBackColor", cboNächsterBC.BackColor.ToArgb.ToString, INI_File)
        INI_Write("Meldung", "PrevForeColor", cboLetzterFC.BackColor.ToArgb.ToString, INI_File)
        INI_Write("Meldung", "PrevNextColor", cboLetzterBC.BackColor.ToArgb.ToString, INI_File)
        INI_Write("Meldung", "Font", cboFont.Text, INI_File)
        INI_Write("Meldung", "FontActual", cboActual.Text, INI_File)
        INI_Write("Meldung", "FontNext", cboNext.Text, INI_File)
        INI_Write("Meldung", "FontPrev", cboPrev.Text, INI_File)
        For r = 0 To grdColums.Rows.Count - 1
            x = ""
            For c = 0 To grdColums.Columns.Count - 1
                x = x & CStr(grdColums(c, r).Value)
                If c < grdColums.Columns.Count - 1 Then x = x & ";"
            Next
            INI_Write("Meldung", CStr(r + 1) & ".Spalte", x, INI_File)
        Next
        Me.Close()
    End Sub

    Private Sub chkAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkAll.CheckedChanged
        For i As Integer = 0 To grdColums.Rows.Count - 1
            grdColums.Rows(i).Cells("colSichtbar").Value = chkAll.Checked
        Next
    End Sub

    Private Sub btnUp_Click(sender As Object, e As EventArgs) Handles btnUp.Click
        With grdColums
            Dim pos As Integer = .SelectedCells(0).RowIndex
            If (pos > 0) Then
                .Rows.InsertCopy(pos, pos - 1)
                For x As Integer = 0 To .Columns.Count - 1
                    .Rows(pos - 1).Cells(x).Value = .Rows(pos + 1).Cells(x).Value
                Next
                .Rows.RemoveAt(pos + 1)
                .Rows(pos - 1).Cells(0).Selected = True
            End If
        End With
    End Sub

    Private Sub btnDown_Click(sender As Object, e As EventArgs) Handles btnDown.Click
        With grdColums
            Dim pos As Integer = .SelectedCells(0).RowIndex
            If (pos < .Rows.Count - 1) Then
                .Rows.InsertCopy(pos, pos + 2)
                For x As Integer = 0 To .Columns.Count - 1
                    .Rows(pos + 2).Cells(x).Value = .Rows(pos).Cells(x).Value
                Next
                .Rows.RemoveAt(pos)
                .Rows(pos + 1).Cells(0).Selected = True
            End If
        End With
    End Sub

    Private Sub Color_Click(sender As Object, e As EventArgs) Handles cboForeColor.Click, cboSelBackColor.Click, cboSelForeColor.Click, _
        cboAktuellerBC.Click, cboAktuellerFC.Click, cboAktuellerBC.Click, cboAktuellerFC.Click, cboLetzterBC.Click, cboLetzterFC.Click, _
        cboNächsterBC.Click, cboNächsterFC.Click, cboBackColor.Click

        Dim Th As New Threading.Thread(AddressOf MoveDialogColor)
        Th.Start()
        Using CD As New ColorDialog
            If CD.ShowDialog(Me) <> Windows.Forms.DialogResult.Cancel Then
                If cboForeColor.Focused Then cboForeColor.BackColor = CD.Color
                If cboBackColor.Focused Then cboBackColor.BackColor = CD.Color
                If cboSelBackColor.Focused Then cboSelBackColor.BackColor = CD.Color
                If cboSelForeColor.Focused Then cboSelForeColor.BackColor = CD.Color
                If cboAktuellerBC.Focused Then cboAktuellerBC.BackColor = CD.Color
                If cboAktuellerFC.Focused Then cboAktuellerFC.BackColor = CD.Color
                If cboNächsterBC.Focused Then cboNächsterBC.BackColor = CD.Color
                If cboNächsterFC.Focused Then cboNächsterFC.BackColor = CD.Color
                If cboLetzterBC.Focused Then cboLetzterBC.BackColor = CD.Color
                If cboLetzterFC.Focused Then cboLetzterFC.BackColor = CD.Color
            Else
                Th.Abort()
            End If
        End Using
        Th.Abort()
        grdColums.Focus()
    End Sub

    Private Sub MoveDialogColor()
        Do
            Threading.Thread.Sleep(10)
        Loop While Me Is Form.ActiveForm
        MoveWindow(GetForegroundWindow, CInt(Me.Left + Me.Width / 2 - 120), CInt(Me.Top + Me.Height / 2 - 170), 240, 340, True)
    End Sub

    Private Sub cboFont_Click(sender As Object, e As EventArgs) Handles cboFont.Click, cboActual.Click, cboNext.Click, cboPrev.Click

        Dim Th As New Threading.Thread(AddressOf MoveDialogFont)
        Th.Start()
        Using FD As New FontDialog
            If FD.ShowDialog(Me) <> Windows.Forms.DialogResult.Cancel Then
                Dim x As String = FD.Font.Name & ";" & FD.Font.Size & CStr(IIf(FD.Font.Bold, ";Fett", "")) & CStr(IIf(FD.Font.Italic, ";Kursiv", ""))
                If cboFont.Focused Then cboFont.Text = x
                If cboActual.Focused Then cboActual.Text = x
                If cboNext.Focused Then cboNext.Text = x
                If cboPrev.Focused Then cboPrev.Text = x
            Else
                Th.Abort()
            End If
        End Using
        Th.Abort()
        grdColums.Focus()
    End Sub

    Private Sub MoveDialogFont()
        Do
            Threading.Thread.Sleep(10)
        Loop While Me Is Form.ActiveForm
        MoveWindow(GetForegroundWindow, CInt(Me.Left + Me.Width / 2 - 225), CInt(Me.Top + Me.Height / 2 - 163), 450, 326, True)
    End Sub

    Private Sub btnDefault_Click(sender As Object, e As EventArgs) Handles btnDefault.Click
        cboForeColor.BackColor = Color.Black
        cboBackColor.BackColor = Color.White
        cboSelBackColor.BackColor = Color.DodgerBlue
        cboSelForeColor.BackColor = Color.White
        cboAktuellerBC.BackColor = Color.Red
        cboAktuellerFC.BackColor = Color.Gold
        cboNächsterBC.BackColor = Color.DarkCyan
        cboNächsterFC.BackColor = Color.WhiteSmoke
        cboLetzterBC.BackColor = Color.Gray
        cboLetzterFC.BackColor = Color.White
        cboFont.Text = "Microsoft Sans Serif;8,25"
        cboActual.Text = "Microsoft Sans Serif;8,25;Fett"
        cboNext.Text = "Microsoft Sans Serif;8,25"
        cboPrev.Text = "Microsoft Sans Serif;8,25"
    End Sub

    Private Sub frmLayout_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.Tag Is "ohne" Then
            Me.Height = Me.Height - 162
            cboAktuellerBC.Visible = False
            cboAktuellerFC.Visible = False
            cboNächsterBC.Visible = False
            cboNächsterFC.Visible = False
            cboLetzterBC.Visible = False
            cboLetzterFC.Visible = False
            lblAkt1.Visible = False
            lblAkt2.Visible = False
            lblNext1.Visible = False
            lblNext2.Visible = False
            lblPrev1.Visible = False
            lblPrev2.Visible = False
            cboActual.Visible = False
            cboNext.Visible = False
            cboPrev.Visible = False
            GroupBox2.Height = GroupBox2.Height - 162
            GroupBox3.Height = GroupBox3.Height - 162
            btnDefault.Top = GroupBox3.Top + 5
            btnSave.Top = btnSave.Top - 162
            btnExit.Top = btnExit.Top - 162
        End If
    End Sub
End Class