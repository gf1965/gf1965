﻿
Imports System.ComponentModel
Imports MySqlConnector

Public Class frmWiegen

    Dim Bereich As String = "Wiegen"
    Dim Query As String
    Dim cmd As MySqlCommand
    Dim dv As DataView

    Dim dtMeldung As New DataTable
    Dim dtGruppen As New DataTable
    Dim dtJahr As New DataTable

    Dim LayoutChanged_W As Boolean
    Private nonNumberEntered As Boolean = False
    'Private btnWeiterPressed As Boolean
    'Private RowValidating As Boolean
    Private loading As Boolean
    'Private IsRowDirty As Integer = -1

    Property CurrentGroup As Integer ' wenn > 0, wurde Wiegen von Leader aufgerufen

    Private NoStartnumbers As SortedList(Of Integer, Integer) ' Losnummer, ManuellyRemoved (Menü oder CheckBox): 0=False, 1=True, 2=verschoben 
    Dim dicLotNumbers As New Dictionary(Of KeyValuePair(Of Integer, String), Integer) ' Key = Losnummer,Geschlecht; Value = Teilnehmer
    Private Max_W_Count As Integer
    Private SelectedRow As Integer
    Private selectedRows As DataGridViewSelectedRowCollection

    Private _DirtyRow As Integer?
    Private Property DirtyRow As Integer?
        Get
            Return _DirtyRow
        End Get
        Set(value As Integer?)
            _DirtyRow = value
            Panel1.Enabled = IsNothing(value)
            Panel2.Enabled = IsNothing(value)
        End Set
    End Property

    Enum DelayReasons
        NotAssigned = 0 ' nicht gewogen
        NotAttended = 1 ' ausgeschlossen (attend=False)
        NotAppeared = 2 ' nicht erschienen --> ans Ende stellen
    End Enum
    Private Sub TextboxNum_KeyDown(sender As Object, e As KeyEventArgs) Handles txtWiegen.KeyDown, txtStossen.KeyDown, txtReissen.KeyDown, txtStartnummer.KeyDown
        Dim ctl = CType(sender, TextBox)
        With ctl
            If e.KeyCode = Keys.Escape Then
                .Text = .Tag.ToString
                e.Handled = True
            Else
                nonNumberEntered = False
                If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
                    If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                        If e.KeyCode <> Keys.Back Then
                            If ctl.Name <> "txtWiegen" And (e.KeyCode <> Keys.Oemcomma OrElse e.KeyCode <> Keys.Decimal) Then
                                nonNumberEntered = True
                            End If
                        End If
                    End If
                End If
            End If
            If ModifierKeys = Keys.Shift Then
                nonNumberEntered = True
            End If
        End With
    End Sub
    Private Sub TextboxNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtWiegen.KeyPress, txtStossen.KeyPress, txtReissen.KeyPress, txtStartnummer.KeyPress
        If nonNumberEntered = True Then
            e.Handled = True
        End If
    End Sub

    '' Me
    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        NativeMethods.INI_Write(Bereich, "SkipRules", CStr(mnuSkipRules.Checked), App_IniFile)

        If btnSave.Enabled Then
            Using New Centered_MessageBox(Me)
                Select Case MessageBox.Show("Änderungen vor dem Schließen speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                    Case DialogResult.Cancel
                        e.Cancel = True
                    Case DialogResult.Yes
                        Save()
                End Select
            End Using
        End If
        'If LayoutChanged_W AndAlso MsgBox("Änderungen an der Tabelle " & Chr(34) & "Wiegen" & Chr(34) & " speichern?", MsgBoxStyle.YesNo, "Tabellen-Layout geändert") = MsgBoxResult.Yes Then
        '    Write_ColumnLayout_ToINI(dgvJoin, "Wiegen")
        'End If
    End Sub
    Private Sub Me_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then
            If ActiveControl.Equals(txtWiegen) Then
                e.SuppressKeyPress = True
                If txtStartnummer.Enabled Then
                    txtStartnummer.Focus()
                Else
                    txtReissen.Focus()
                End If
            ElseIf ActiveControl.Equals(txtStartnummer) Then
                txtReissen.Focus()
            ElseIf ActiveControl.Equals(txtReissen) Then
                txtStossen.Focus()
            ElseIf ActiveControl.Equals(txtStossen) Then
                If chkLänderwertung.Visible Then
                    chkLänderwertung.Focus()
                ElseIf cboVereinswertung.Visible Then
                    cboVereinswertung.Focus()
                ElseIf cboGruppenZuweisung.Visible Then
                    cboGruppenZuweisung.Focus()
                Else
                    'dv(bs.Position)("Vereinswertung") = False
                    'dv(bs.Position)("Laenderwertung") = False
                    'chkVereinswertung.Checked = False
                    'chkLänderwertung.Checked = False
                    'btnWeiterPressed = True
                    btnWeiter.PerformClick()
                End If
            ElseIf ActiveControl.Equals(chkLänderwertung) Then
                If cboVereinswertung.Visible Then
                    cboVereinswertung.Focus()
                ElseIf cboGruppenZuweisung.Visible Then
                    cboGruppenZuweisung.Focus()
                Else
                    'dv(bs.Position)("Vereinswertung") = False
                    'btnWeiterPressed = True
                    btnWeiter.PerformClick()
                End If
            ElseIf ActiveControl.Equals(cboVereinswertung) Then
                If cboGruppenZuweisung.Visible Then
                    cboGruppenZuweisung.Focus()
                Else
                    'btnWeiterPressed = True
                    btnWeiter.PerformClick()
                End If
            ElseIf ActiveControl.Equals(cboGruppenZuweisung) Then
                'btnWeiterPressed = True
                btnWeiter.PerformClick()
            End If
        End If
    End Sub
    Private Sub Me_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        'If btnWeiterPressed Then
        '    e.Handled = True
        '    btnWeiter.PerformClick()
        'End If
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        loading = True

        'Dim tTip As New ToolTip()
        'tTip.AutoPopDelay = 5000
        'tTip.InitialDelay = 1000
        'tTip.ReshowDelay = 500
        'tTip.ShowAlways = True
        'tTip.SetToolTip(txtReissen, "Bei Nicht-Teilnahme an der Disziplin eine '0' (Null) eintragen.")
        'tTip.SetToolTip(txtStossen, "Bei Nicht-Teilnahme an der Disziplin eine '0' (Null) eintragen.")

        Dim x As String
        x = NativeMethods.INI_Read(Bereich, "SkipRules", App_IniFile)
        If x <> "?" Then mnuSkipRules.Checked = CBool(x)

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

#Region "Daten"
        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                Dim v = If(Wettkampf.Mannschaft, "vv", "v")
                ' Meldung
                Query = "Select m.Teilnehmer, a.Geschlecht sex, a.Titel, " & If(Wettkampf.International, "Ucase(a.Nachname) ", "a.") & "Nachname, a.Vorname, a.Geburtstag, a.Jahrgang, " &
                        If(Wettkampf.Mannschaft, "vv", "v") & ".idVerein, IfNull(" & v & ".Kurzname, " & v & ".Vereinsname) Vereinsname, s.state_iso3, s.state_name, r.region_3, r.region_name, IFNULL(a.MSR, a.Verein) MSR, " &
                        "m.Sortierung, m.Losnummer, m.Startnummer, m.Gewicht, m.Wiegen, m.idAK, m.AK, m.idGK, m.GK, m.Reissen, m.Stossen, m.Zweikampf, m.attend, if(m.Gruppe = 1000, null, m.Gruppe) Gruppe, m.Laenderwertung, m.Vereinswertung, m.Relativ_W, m.Meldedatum " &
                        "FROM meldung m " &
                        "LEFT JOIN athleten a On m.Teilnehmer = a.idTeilnehmer " &
                        "Left Join verein v ON IFNULL(a.ESR, a.Verein) = v.idVerein " &
                        "left join teams t on t.idVerein = IFNULL(a.MSR, a.Verein) " &
                        "Left Join verein vv On vv.idVerein = IFNULL(m.Team, IFNULL(t.idTeam, IFNULL(a.MSR, a.Verein))) " &
                        "Left Join region r on r.region_id = " & If(Wettkampf.Mannschaft, "vv", "v") & ".Region " &
                        "Left Join staaten s on s.state_id = " & If(Wettkampf.Mannschaft, "vv", "a") & ".Staat " &
                        "WHERE " & Wettkampf.WhereClause("m") &
                        "GROUP BY m.Teilnehmer;"
                '"WHERE m.Wettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtMeldung.Load(cmd.ExecuteReader())

                ' Gruppen
                Query = "Select * from gruppen " &
                        "where " & Wettkampf.WhereClause() &
                        "group by Gruppe " &
                        "order by Gruppe;"
                cmd = New MySqlCommand(Query, conn)
                'cmd = New MySqlCommand("Select * from gruppen where Wettkampf = " & Wettkampf.ID & " order by Gruppe;", conn)
                dtGruppen.Load(cmd.ExecuteReader())

            Catch ex As MySqlException
                LogMessage("frmWiegen: Shown: Daten: " & ex.Message, ex.StackTrace)
            End Try
        End Using
#End Region

        ' Sortierspalte für Startnummern hinzufügen und füllen
        ' --> bei vorhandenen Losnummern ist Sort richtig
        ' --> ohne Losnummern ist Sort alphabetisch nach Namen -> bei Speichern nach Losnummern, Nachname, Vorname sortieren und Sort neu vergeben
        dv = New DataView(dtMeldung, Nothing, "Startnummer, Losnummer, Nachname, Vorname", DataViewRowState.CurrentRows)

        Build_Grid()

        bs = New BindingSource With {.DataSource = dv, .Sort = "Sortierung"}
        dgvJoin.DataSource = bs

#Region "Bindings"
        txtWiegen.DataBindings.Add(New Binding("Text", bs, "Wiegen", False, DataSourceUpdateMode.OnValidation, String.Empty))
        txtStartnummer.DataBindings.Add(New Binding("Text", bs, "Startnummer", False, DataSourceUpdateMode.OnValidation, String.Empty))
        txtReissen.DataBindings.Add(New Binding("Text", bs, "Reissen", False, DataSourceUpdateMode.OnValidation, String.Empty))
        txtStossen.DataBindings.Add(New Binding("Text", bs, "Stossen", False, DataSourceUpdateMode.OnValidation, String.Empty))
        chkLänderwertung.DataBindings.Add(New Binding("Checked", bs, "Laenderwertung", False, DataSourceUpdateMode.OnValidation, False))
        cboVereinswertung.DataBindings.Add(New Binding("SelectedValue", bs, "Vereinswertung", False, DataSourceUpdateMode.OnValidation, String.Empty))
        chkRelativ.DataBindings.Add(New Binding("CheckState", bs, "Relativ_W", True, DataSourceUpdateMode.OnValidation, 2)) 'DBNull.Value)) ' 0 bei ThreeState=False
        cboGruppenZuweisung.DataBindings.Add(New Binding("Text", bs, "Gruppe", False, DataSourceUpdateMode.OnValidation, String.Empty))
#End Region

#Region "ComboBoxes"
        With cboJG
            Try
                .DataSource = New DataView(dv.ToTable(True, "Jahrgang"), Nothing, "Jahrgang", DataViewRowState.CurrentRows)
                .DisplayMember = "Jahrgang"
                .SelectedIndex = -1
            Catch ex As Exception
                .Enabled = False
            End Try
        End With

        With cboSex
            .Items.AddRange({"männlich", "weiblich"})
            .SelectedIndex = -1
        End With

        With cboGruppe
            .DataSource = dtGruppen
            .DisplayMember = "Gruppe"
            .ValueMember = "Gruppe"
            .SelectedIndex = -1
        End With

        With cboVerein
            If Wettkampf.International Then
                .DataSource = New DataView(dv.ToTable(True, {"state_iso3", "state_name"}), Nothing, "state_name", DataViewRowState.CurrentRows)
                .DisplayMember = "state_name"
                .ValueMember = "state_iso3"
                lblVerein.Text = "&Land"
            Else
                .DataSource = New DataView(dv.ToTable(True, {"idVerein", "Vereinsname"}), Nothing, "Vereinsname", DataViewRowState.CurrentRows)
                .DisplayMember = "Vereinsname"
                .ValueMember = "idVerein"
                lblVerein.Text = "&Verein"
            End If
            .Tag = .ValueMember
            .SelectedIndex = -1
        End With
#End Region

        'dicLotNumbers_Fill()

#Region "Input"
        If Wettkampf.Mannschaft Then
            ' Mannschafts_WK (Liga)
            cboGruppenZuweisung.Items.Add(String.Empty)
            If dtGruppen.Rows.Count > 0 Then
                For i = 1 To dtGruppen.Rows.Count
                    cboGruppenZuweisung.Items.Add(i)
                Next
            End If
            cboGruppenZuweisung.Visible = True
            lblGruppen.Visible = True
            GroupBox1.Height = GroupBox1.MinimumSize.Height
            With dgvJoin
                .Columns("Teilnehmer").Visible = False
                .Columns("Geburtstag").Visible = False
                .Columns("Nation").Visible = False
                .Columns("Land").Visible = False
                .Columns("Gewicht").Visible = False
            End With
        Else
            mnuNotAppeared.Visible = Wettkampf.AutoLosnummern
            cmnuNotAppeared.Visible = Wettkampf.AutoLosnummern
            tsm1.Visible = Wettkampf.AutoLosnummern
            ctsm1.Visible = Wettkampf.AutoLosnummern
            ' Mannschaftswertungen
            'GroupBox1.Height = GroupBox1.MinimumSize.Height
            For Each row As DataRowView In Glob.dvMannschaft
                If CInt(row!idMannschaft) = 0 Then
                    ' Vereinsmannschaft
                    Dim dic As New Dictionary(Of String, Integer?)
                    dic("") = Nothing
                    dic("1") = 1
                    dic("2") = 2
                    dic("3") = 3
                    With cboVereinswertung
                        .Visible = True
                        .DataSource = New BindingSource(dic, Nothing)
                        .DisplayMember = "Key"
                        .ValueMember = "Value"
                        .Visible = True
                    End With
                    chkRelativ.Visible = Not IsDBNull(row!relativ) AndAlso CInt(row!relativ) > 0
                    dgvJoin.Columns("Vereinswertung").Visible = True
                    dgvJoin.Columns("RelativW").Visible = Not IsDBNull(row!relativ) AndAlso CInt(row!relativ) > 0
                    If Not IsDBNull(row!relativ) Then Max_W_Count = CInt(row!relativ)
                    'GroupBox1.Height += 35
                    'cboVereinswertung.Top = btnWeiter.Top - 42
                ElseIf CInt(row!idMannschaft) = 1 Then
                    ' Ländermannschaft
                    chkLänderwertung.Visible = True
                    dgvJoin.Columns("Länderwertung").Visible = True
                    'GroupBox1.Height += 35
                    'chkLänderwertung.Top = btnWeiter.Top - 42
                End If
            Next
        End If
#End Region

        loading = False

#Region "Gruppe auswählen"
        If CurrentGroup > 0 Then
            ' frmWiegen aus frmLeader.Gruppe_Load aufgerufen --> Änderungen werden in frmLeader übernommen
            cboGruppe.SelectedValue = CurrentGroup
        ElseIf Not IsNothing(Leader.Gruppe) AndAlso Leader.Gruppe.Count > 0 Then
            ' frmLeader ist geöffnet, frmWiegen aus frmMain aufgerufen --> Änderungen werden in frmLeader übernommen
            cboGruppe.SelectedValue = Leader.Gruppe(0)
        ElseIf cboGruppenZuweisung.Items.Count = 1 Then
            cboGruppenZuweisung.SelectedIndex = 0
            btnGruppe.Enabled = False
            'Else
            '    ' erste Gruppe suchen, in der Startnummern fehlen
            '    Dim groups = (From x In dtMeldung
            '                  Where (IsNothing(x.Field(Of Boolean?)("attend")) OrElse x.Field(Of Boolean?)("attend") = True) AndAlso IsNothing(x.Field(Of Integer?)("Startnummer"))
            '                  Order By x.Field(Of Integer?)("Gruppe")
            '                  Group x By G = x.Field(Of Integer?)("Gruppe") Into Group
            '                  Select G).ToList
            '    ' groups(0) = Nothing ???????????????????????????????????????
            '    If groups.Count > 1 AndAlso Not IsNothing(groups(1)) Then cboGruppe.SelectedValue = groups(1)
        End If
#End Region

        formMain.Change_MainProgressBarValue(0)

        Cursor = Cursors.Default
        txtWiegen.Focus()
        LayoutChanged_W = False
        btnSave.Enabled = False ' Not bs.Position = 0
        loading = False
    End Sub

    'Private Sub dicLotNumbers_Fill()
    '    Dim lstLotsDouble As New List(Of DataRow)
    '    dicLotNumbers.Clear()
    '    For Each row As DataRow In dtMeldung.Rows
    '        Try
    '            If Not IsDBNull(row!Losnummer) Then dicLotNumbers.Add(New KeyValuePair(Of Integer, String)(CInt(row!Losnummer), row!sex.ToString), CInt(row!Teilnehmer))
    '        Catch ex As Exception
    '            lstLotsDouble.Add(row)
    '        End Try
    '    Next
    '    Dim msg = String.Empty
    '    btnSave.Enabled = lstLotsDouble.Count > 0

    '    Do While lstLotsDouble.Count > 0
    '        msg = "Die Losnummer " & lstLotsDouble(0)!Losnummer.ToString & " wurde mehrfach vergeben."
    '        Using New Centered_MessageBox(Me, {"Korrigieren", "Alle neu", "Abbrechen"})
    '            Dim result = MessageBox.Show(msg, msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation)
    '            Select Case result
    '                Case DialogResult.Yes
    '                    Dim maxLot = (From k In dicLotNumbers
    '                                  Group By k.Key.Value Into Group
    '                                  Select Group.Max(Function(r) r.Key.Key)).Max
    '                    dicLotNumbers.Add(New KeyValuePair(Of Integer, String)(maxLot + 1, lstLotsDouble(0)!sex.ToString), CInt(lstLotsDouble(0)!Teilnehmer))
    '                    lstLotsDouble(0)!Losnummer = maxLot + 1
    '                    lstLotsDouble.RemoveAt(0)
    '                Case DialogResult.No
    '                    Using F As New myFunction
    '                        F.Generate_Lotnumbers(dtMeldung, "Sort")
    '                    End Using
    '                    'dicLotNumbers_Fill()
    '                    Exit Do
    '                Case DialogResult.Cancel
    '                    Exit Do
    '            End Select
    '        End Using
    '    Loop
    'End Sub

    '' Menüs
    Private Sub mnuBeenden_Click(sender As Object, e As EventArgs) Handles mnuBeenden.Click, btnExit.Click
        Close()
    End Sub
    Private Sub mnuLayout_Click(sender As Object, e As EventArgs)
        Read_TableToLayout(dgvJoin, Bereich)
        frmTableLayout.Tag = Bereich
        If Not frmTableLayout.ShowDialog() = DialogResult.Cancel Then
            Format_Grid(Bereich, dgvJoin)
            LayoutChanged_W = False
        End If
        frmTableLayout.Dispose()
    End Sub

    Private Sub mnuNotAppeared_Click(sender As Object, e As EventArgs) Handles mnuNotAppeared.Click, cmnuNotAppeared.Click

        Dim pos = bs.Position
        NoStartnumbers(CInt(dv(pos)("Losnummer"))) = DelayReasons.NotAppeared
        dv(pos)("attend") = DBNull.Value
        For i = pos To dv.Count - 1
            dv(i)!Startnummer = DBNull.Value
        Next
        loading = True
        dv(pos)!Sort = CInt(dv(pos)!Sort) * 100
        bs.EndEdit()
        bs.Position = pos
        loading = False
        btnSave.Enabled = True
    End Sub
    Private Sub mnuExclude_Click(sender As Object, e As EventArgs) Handles mnuExclude.Click, cmnuExclude.Click
        dv(bs.Position)!attend = False
        bs.EndEdit()
    End Sub
    Private Sub mnuInclude_Click(sender As Object, e As EventArgs) Handles mnuInclude.Click, cmnuInclude.Click
        dv(bs.Position)!attend = True
        bs.EndEdit()

        'Dim Lot = CInt(dv(bs.Position)("Losnummer"))
        'If NoStartnumbers.Keys.Contains(Lot) AndAlso NoStartnumbers(Lot) = 2 Then NoStartnumbers.Remove(Lot)
        'Create_Startnumbers(bs.Position)
        'loading = False
    End Sub
    Private Sub mnuHeber_Click(sender As Object, e As EventArgs) Handles mnuHeber.Click, cmnuHeber.Click

        Dim TN = dv(bs.Position)!Teilnehmer.ToString
        Dim idVerein = 0
        If Not IsDBNull(dv(bs.Position)!idVerein) Then idVerein = CInt(dv(bs.Position)!idVerein)

        Using formAthleten As New frmAthleten
            With formAthleten
                .Tag = dv(bs.Position)!Teilnehmer
                .ShowDialog(Me)
                If Not .DialogResult = DialogResult.Cancel Then
                    Cursor = Cursors.WaitCursor
                    Dim Verein_Changed = .Athlet.idVerein <> idVerein
                    bs.EndEdit()
                    Dim pos = bs.Find("Teilnehmer", TN)
                    With .Athlet
                        dv(pos)!Nachname = If(Wettkampf.International, .Nachname.ToUpper, .Nachname)
                        dv(pos)!Vorname = .Vorname
                        dv(pos)!idVerein = .idVerein
                        dv(pos)!Jahrgang = .Jahrgang
                        dv(pos)!Sex = .Geschlecht
                        Dim ak = Glob.Get_AK(.Jahrgang)
                        If Not IsNothing(ak.Value) Then
                            dv(pos)!idAK = ak.Key
                            dv(pos)!AK = ak.Value
                        End If
                        If Verein_Changed Then
                            RowsVerein_Change({dv(pos).Row}, idVerein)
                        End If
                    End With
                    Cursor = Cursors.Default
                End If
            End With
        End Using
        bs.EndEdit()
    End Sub
    Private Sub mnuVerein_Click(sender As Object, e As EventArgs) Handles mnuVerein.Click, cmnuVerein.Click

        If IsDBNull(dv(bs.Position)!idVerein) Then
            Beep()
            Return
        End If

        Dim idVerein = CInt(dv(bs.Position)!idVerein)
        Dim idHeber = CInt(dv(bs.Position)!Teilnehmer)

        Using formVerein As New frmVerein
            With formVerein
                .Tag = dv(bs.Position)!idVerein
                .ShowDialog(Me)
                If Not .DialogResult = DialogResult.Cancel Then
                    Cursor = Cursors.WaitCursor
                    bs.EndEdit()
                    Dim rows = dtMeldung.Select("idVerein = " & idVerein)
                    RowsVerein_Change(rows, idVerein)
                    Cursor = Cursors.Default
                End If
            End With
        End Using
        bs.EndEdit()
    End Sub

    Private Sub mnuÜbernahme_Click(sender As Object, e As EventArgs) Handles cmnuÜbernahme.Click
        'Dim wk As KeyValuePair(Of String, String)

        'Using f As New myFunction
        '    wk = f.Wettkampf_Auswahl(Me, "Athleten übernehmen",, True)
        'End Using

        'If CInt(wk.Key) > 0 Then
        '    Dim result = SaveToOtherCompetition(CInt(wk.Key), selectedRows)
        '    Using New Centered_MessageBox(Me)
        '        Dim icon As MessageBoxIcon
        '        Dim msg As String
        '        If IsNothing(result) OrElse result = False Then
        '            icon = MessageBoxIcon.Information
        '            msg = "Änderungen in " & wk.Value & " übernommen."
        '        Else
        '            icon = MessageBoxIcon.Error
        '            msg = "Übernahme in " & wk.Value & " fehlgeschlagen."
        '        End If
        '        MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, icon)
        '    End Using
        'End If

        'For Each row As DataGridViewRow In selectedRows
        '    dgvJoin.Rows(row.Index).DefaultCellStyle.BackColor = Color.FromKnownColor(KnownColor.Window)
        'Next
    End Sub

    Private Sub RowsVerein_Change(rows() As DataRow, idVerein As Integer)
        Using f As New myFunction
            Dim tmp = f.Get_VereinInfo(idVerein.ToString)
            For Each r As DataRow In rows
                If Not IsNothing(tmp) Then
                    If dtMeldung.Columns.Contains("Vereinsname") Then r!Vereinsname = tmp(0)!Vereinsname
                    If dtMeldung.Columns.Contains("region_3") Then r!region_3 = tmp(0)!Verband
                    If dtMeldung.Columns.Contains("region_name") Then r!region_name = tmp(0)!region_name
                    If dtMeldung.Columns.Contains("state_iso3") Then r!state_iso3 = tmp(0)!state_iso3
                    If dtMeldung.Columns.Contains("state_name") Then r!state_name = tmp(0)!state_name
                End If
            Next
        End Using
    End Sub

    Private Sub mnuSpeichern_Click(sender As Object, e As EventArgs) Handles mnuSpeichern.Click, btnSave.Click
        Save()
    End Sub
    Private Sub mnuSperren_Click(sender As Object, e As EventArgs) Handles mnuSperren.Click
        Lock_Application(Me)
    End Sub
    Private Sub mnuListenDruck(sender As Object, e As EventArgs) Handles mnuWiegeliste_Druck.Click, mnuStartliste_Druck.Click, mnuGruppenliste_Druck.Click
        Cursor = Cursors.WaitCursor
        Dim Caption = CType(sender, ToolStripMenuItem).Tag.ToString

        With Drucken_Gruppenliste
            .Text = UCase(Caption) & " DRUCKEN  - " + Wettkampf.Bezeichnung
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
            .Tag = Caption
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = DefaultCursor
    End Sub
    Private Sub mnuMauszeiger_Click(sender As Object, e As EventArgs) Handles mnuMauszeiger.Click
        Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
        Cursor.Show()
    End Sub

    'Private Sub mnuSet_Losnummer_Click(sender As Object, e As EventArgs) Handles mnuSet_Losnummer.Click

    '    ' Losnummern können immer neu zugewiesen werden (ist eine Random-Funktion)

    '    Cursor = Cursors.WaitCursor

    '    Dim success = Write_Lotnumbers()

    '    Dim msg = If(success, "Losnummern erfolgreich aktualisiert.", "Fehler bei der Losnummern-Vergabe")

    '    Using New Centered_MessageBox(Me)
    '        MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    End Using

    '    btnSave.Enabled = success

    '    Cursor = Cursors.Default
    'End Sub

    'Private Function Write_Lotnumbers() As Boolean

    '    Dim returnvalue As New List(Of Integer)

    '    Using F As New myFunction
    '        returnvalue = F.Generate_Lotnumbers(dtMeldung)
    '    End Using
    '    If IsNothing(returnvalue) Then
    '        Return False
    '    Else
    '        Try
    '            For i = 0 To dv.Count - 1
    '                dv(i)!Losnummer = returnvalue(i)
    '                dv(i)!Startnummer = DBNull.Value
    '                bs.EndEdit()
    '            Next
    '        Catch ex As Exception
    '            Return False
    '        End Try
    '    End If

    '    Return True
    'End Function

    '' Speichern
    'Private Function SaveToOtherCompetition(WK_ID As Integer, data As DataGridViewSelectedRowCollection) As Boolean?
    '    loading = True

    '    Query = "UPDATE meldung Set attend = @attend, Wiegen = @wiegen, Losnummer = @lot, Startnummer = @start, Reissen = @reissen, Stossen = @stossen, Zweikampf = @zk, " &
    '                "idGK = @idgk, GK = @gk, idAK = @idak, AK = @ak, Laenderwertung = @land, Vereinswertung = @verein, Relativ_W = @rel, Gruppe = @group " &
    '                "WHERE Wettkampf= @wk And Teilnehmer = @id;"
    '    Using conn As New MySqlConnection(User.ConnString)
    '        For Each selrow As DataGridViewRow In data
    '            Dim row = dv(selrow.Index).Row
    '            cmd = New MySqlCommand(Query, conn)
    '            With cmd.Parameters
    '                .AddWithValue("@wk", WK_ID)
    '                .AddWithValue("@id", row!Teilnehmer)
    '                .AddWithValue("@attend", row!attend)
    '                .AddWithValue("@wiegen", row!Wiegen)
    '                .AddWithValue("@lot", row!Losnummer)
    '                .AddWithValue("@start", row!Startnummer)
    '                .AddWithValue("@reissen", row!Reissen)
    '                .AddWithValue("@stossen", row!Stossen)
    '                .AddWithValue("@zk", row!Zweikampf)
    '                .AddWithValue("@idak", row!idAK)
    '                .AddWithValue("@ak", row!AK)
    '                .AddWithValue("@idgk", row!idGK)
    '                .AddWithValue("@gk", row!GK)
    '                .AddWithValue("@land", row!Laenderwertung)
    '                .AddWithValue("@verein", row!Vereinswertung)
    '                .AddWithValue("@rel", row!Relativ_W)
    '                .AddWithValue("@group", row!Gruppe)
    '            End With
    '            Do
    '                Try
    '                    If conn.State = ConnectionState.Closed Then conn.Open()
    '                    cmd.ExecuteNonQuery()
    '                    ConnError = False
    '                Catch ex As Exception
    '                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit For
    '                End Try
    '            Loop While ConnError
    '        Next
    '    End Using
    '    dgvJoin.Focus()
    '    Cursor = Cursors.Default
    '    Return ConnError
    'End Function
    Private Sub Save()
        dgvJoin.EndEdit()
        ValidateChildren()
        Validate()
        bs.EndEdit()

        If Not IsNothing(DirtyRow) Then Return

        Cursor = Cursors.WaitCursor
        loading = True
        Dim msg = String.Empty

#Region "Realtiv weiblich"
        If chkRelativ.Visible Then
            Dim Teams = From x In dtMeldung
                        Where Not IsNothing(x.Field(Of Integer?)("Vereinswertung")) AndAlso x.Field(Of Integer?)("Vereinswertung") > 0 AndAlso x.Field(Of String)("Sex") = "w"
                        Group x By v = x.Field(Of Integer)("idVerein") Into Verein = Group
                        Select Id = Verein.First.Field(Of Integer)("idVerein"), Bezeichnung = Verein.First.Field(Of String)("Vereinsname"),
                             Females = From y In Verein
                                       Order By y.Field(Of Integer)("Vereinswertung") Ascending
                                       Group y By t = y.Field(Of Integer)("Vereinswertung") Into Team = Group
                                       Select Team, Checked = From z In Team
                                                              Order By z.Field(Of Boolean?)("Relativ_W") Ascending
                                                              Group z By Checked = z.Field(Of Boolean?)("Relativ_W") Into TN = Group
                                                              Select TN, Relativ_W = TN.First.Field(Of Boolean?)("Relativ_W")
            For i = 0 To Teams.Count - 1
                ' jeder Verein
                For c = 0 To Teams(i).Females.Count - 1
                    ' jede Mannschaft im Verein
                    Dim Wertung = {0, 0, 0} ' 0 = männlich, 1 = weiblich, 2 = autom. weiblich
                    For t = 0 To Teams(i).Females(c).Checked.Count - 1
                        If IsNothing(Teams(i).Females(c).Checked(t).Relativ_W) Then
                            Wertung(2) = Teams(i).Females(c).Checked(t).TN.Count
                        Else
                            Wertung(Math.Abs(CInt(Teams(i).Females(c).Checked(t).Relativ_W))) = Teams(i).Females(c).Checked(t).TN.Count
                        End If
                    Next
                    If Wertung(1) < Max_W_Count AndAlso Wertung(1) + Wertung(2) > Max_W_Count Then
                        Using New Centered_MessageBox(Me)
                            msg = "In der " & c + 1 & ". Mannschaft von '" & Teams(i).Bezeichnung & "'" & vbNewLine &
                                      "muss angegeben werden, welche " & Max_W_Count & " der " & Wertung(1) + Wertung(2) & " Heberinnen" & vbNewLine &
                                      "Relativ weiblich gewertet werden." & vbNewLine &
                                      "(Alle anderen werden Relativ männlich gewertet.)"
                            MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        Cursor = Cursors.Default
                        loading = False
                        Return
                    End If
                    If Wertung(1) = Max_W_Count Then
                        ' es sind alle weiblich gewerteten richtig gespeichert ==> Rest als männlich speichern
                        For t = 0 To Teams(i).Females(c).Checked.Count - 1
                            If t >= Teams(i).Females(c).Checked.Count Then Exit For
                            If IsNothing(Teams(i).Females(c).Checked(t).Relativ_W) Then
                                For Each row As DataRow In Teams(i).Females(c).Checked(t).TN
                                    row!Relativ_W = False
                                Next
                            End If
                        Next
                    ElseIf Wertung(1) + Wertung(2) = Max_W_Count Then
                        ' es sind insgesamt Max_W_Count*Females vorhanden ==> alle als Relativ weiblich speichern (Abfrage mit Bestätigung erstellen???)
                        For t = 0 To Teams(i).Females(c).Checked.Count - 1
                            If t >= Teams(i).Females(c).Checked.Count Then Exit For
                            If IsNothing(Teams(i).Females(c).Checked(t).Relativ_W) Then
                                For Each row As DataRow In Teams(i).Females(c).Checked(t).TN
                                    row!Relativ_W = True
                                Next
                            End If
                        Next
                    ElseIf Teams(i).Females(c).Team.Count < Max_W_Count Then
                        For Each row As DataRow In Teams(i).Females(c).Team
                            row!Relativ_W = DBNull.Value
                        Next
                    End If
                Next
            Next
        End If
#End Region

#Region "Startnummern" ' RHT Gr3 klappt gar nicht
        '' dv mit allen indizierbaren Athleten (attend <> False und nicht aus Meldung gelöscht (RowState = CurrentRows))
        'Dim CurrentLifters = New DataView(dtMeldung, "Convert(IsNull(attend,''), System.String) = '' OR attend = True", "Losnummer, Meldedatum", DataViewRowState.CurrentRows)
        'Dim ErrorGruppe As Integer

        'Try
        '    Dim LotNumbersMissing = (From x In CurrentLifters.ToTable
        '                             Where IsNothing(x.Field(Of Integer?)("Losnummer"))).Count > 0
        '    If Not LotNumbersMissing Then
        '        ' auf mehrfach vergebene Losnummern prüfen -------------> sollte eigentlich durch dgvJoin_RowValidating ausgeschlossen sein
        '        Dim MultiLots = (From x In dtMeldung
        '                         Group By lot = x.Field(Of Integer?)("Losnummer") Into Group
        '                         Select lot, cnt = Group.Count
        '                         Where cnt > 1).ToDictionary(Function(p) p.lot, Function(p) p.cnt)
        '        If MultiLots.Count > 0 Then
        '            If Not Write_Lotnumbers() Then msg = "Fehler bei dem Versuch, die fehlenden Losnummern zuzuweisen."
        '        End If
        '    End If
        '    If LotNumbersMissing Then
        '        If Not Write_Lotnumbers() Then msg = "Fehler bei dem Versuch, die fehlenden Losnummern zuzuweisen."
        '    End If
        'Catch ex As Exception
        'End Try

        'If String.IsNullOrEmpty(msg) Then
        '    If Not Wettkampf.Mannschaft Then
        '        For Each row As DataRow In dtGruppen.Rows
        '            Dim tmp = New DataView(dtMeldung, "Gruppe = " + row!Gruppe.ToString, "Losnummer, Meldedatum", DataViewRowState.CurrentRows)
        '            Dim cancel = (From x In tmp.ToTable
        '                          Where IsNothing(x.Field(Of Boolean?)("attend"))).Count > 0
        '            If Not cancel Then
        '                Dim Startnummer = 1
        '                For Each item As DataRowView In tmp
        '                    If Not IsDBNull(item!attend) AndAlso CBool(item!attend) Then
        '                        item!Startnummer = Startnummer
        '                        Startnummer += 1
        '                    Else
        '                        item!Startnummer = DBNull.Value
        '                    End If
        '                Next
        '            ElseIf row!Gruppe.ToString.Equals(cboGruppe.Text) Then
        '                msg = "Für Gruppe " & row!Gruppe.ToString & " können keine Startnummern vergeben werden." & vbNewLine &
        '                      "(Anmeldung unvollständig / TN nicht abgemeldet)"
        '                ErrorGruppe = CInt(row!Gruppe)
        '            End If
        '        Next
        '    Else
        '        Dim cancel = (From x In CurrentLifters.ToTable
        '                      Where IsNothing(x.Field(Of Boolean?)("attend"))).Count > 0
        '        If Not cancel Then
        '            Dim num As New Dictionary(Of Integer, Integer)
        '            Dim start = 1
        '            Try
        '                Dim rec = (From x In CurrentLifters.ToTable
        '                           Group By team = x.Item("MSR") Into Group
        '                           Select key = team, value = Group.Count).ToDictionary(Function(p) p.key, Function(p) p.value)
        '                For Each row As DataRow In Glob.dtTeams.Rows
        '                    num(CInt(row!Team)) = start
        '                    start += rec(CInt(row!Team))
        '                Next
        '                For Each row As DataRowView In CurrentLifters
        '                    Dim team = CInt(row!MSR)
        '                    row!Startnummer = num(team)
        '                    num(team) += 1
        '                Next
        '            Catch ex As Exception
        '                Dim _path = Path.Combine(Application.StartupPath, "log", "err_" & Format(Now, "yyyy-MM-dd") & ".log")
        '                LogMessage("Wiegen:Save:Generate_Startnumbers / " & ex.Message & " / StackTrace " &
        '                If(InStr(ex.StackTrace, "Zeile") > 0, Mid(ex.StackTrace, InStr(ex.StackTrace, "Zeile")), String.Empty), _path)
        '                msg = "Unbekannter Fehler bei der Startnummern-Vergabe." & vbNewLine &
        '                      "(Details in: " & _path & ")"
        '            End Try
        '        Else
        '            msg = "Für den Wettkampf können keine Startnummern vergeben werden." & vbNewLine &
        '                  "(Anmeldung unvollständig / TN nicht abgemeldet)"
        '        End If
        '    End If
        'End If
        'If Not String.IsNullOrEmpty(msg) Then
        '    Dim NoReport As Boolean
        '    If ErrorGruppe > 0 Then
        '        NoReport = (From x In dtMeldung
        '                    Where x.Field(Of Integer)("Gruppe") = ErrorGruppe AndAlso x.Field(Of Boolean?)("attend") = True).Count = 0
        '    End If
        '    If Not NoReport OrElse msg.Contains("Wettkampf") Then
        '        Using New Centered_MessageBox(Me)
        '            If MessageBox.Show(msg, msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) = DialogResult.Cancel Then
        '                loading = False
        '                Cursor = Cursors.Default
        '                Return
        '            End If
        '        End Using
        '    End If
        'End If
#End Region

        Validate()
        bs.EndEdit()

#Region "Save"
        Dim changes As DataTable = dtMeldung.GetChanges()
        If Not IsNothing(changes) Then
            Query = "UPDATE meldung SET attend = @attend, Wiegen = @wiegen, Losnummer = @lot, Startnummer = @start, Reissen = @reissen, Stossen = @stossen, Zweikampf = @zk, " &
                    "idGK = @idgk, GK = @gk, idAK = @idak, AK = @ak, Laenderwertung = @land, Vereinswertung = @verein, Relativ_W = @rel, Gruppe = @group " &
                    "WHERE " & Wettkampf.WhereClause() & " AND Teilnehmer = @id;"
            Using conn As New MySqlConnection(User.ConnString)
                For Each row As DataRow In changes.Rows
                    cmd = New MySqlCommand(Query, conn)
                    With cmd.Parameters
                        '.AddWithValue("@wk", Wettkampf.ID)
                        .AddWithValue("@id", row!Teilnehmer)
                        .AddWithValue("@attend", row!attend)
                        .AddWithValue("@wiegen", row!Wiegen)
                        .AddWithValue("@lot", row!Losnummer)
                        .AddWithValue("@start", row!Startnummer)
                        .AddWithValue("@reissen", row!Reissen)
                        .AddWithValue("@stossen", row!Stossen)
                        .AddWithValue("@zk", row!Zweikampf)
                        .AddWithValue("@idak", row!idAK)
                        .AddWithValue("@ak", row!AK)
                        .AddWithValue("@idgk", row!idGK)
                        .AddWithValue("@gk", row!GK)
                        .AddWithValue("@land", row!Laenderwertung)
                        .AddWithValue("@verein", row!Vereinswertung)
                        .AddWithValue("@rel", row!Relativ_W)
                        .AddWithValue("@group", row!Gruppe)
                    End With
                    Do
                        Try
                            If conn.State = ConnectionState.Closed Then conn.Open()
                            cmd.ExecuteNonQuery()
                            ConnError = False
                        Catch ex As Exception
                            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit For
                        End Try
                    Loop While ConnError
                Next
            End Using
        End If
#End Region

        dgvJoin.Focus()
        Cursor = Cursors.Default

        If Not ConnError Then
            dtMeldung.AcceptChanges()
            btnSave.Enabled = False

            Using OS As New OnlineService
                If Not AppConnState = ConnState.Connected Then
                    AppConnState = OS.Check_Wettkampf()
                End If
                If AppConnState = ConnState.Connected Then
                    OS.UpdateMeldung()
                End If
            End Using

            If CurrentGroup > 0 Then
                ' von frmLeader.Gruppe_Load aufgerufen
                CurrentGroup = -1
                Close()
            ElseIf Not IsNothing(formLeader) AndAlso Not formLeader.IsDisposed Then
                ' bei offener frmLeader aufgerufen
                Change_LeaderGruppe()
                Close()
            End If
        End If

        loading = False
    End Sub

    ' Buttons
    Private Sub btnSave_EnabledChanged(sender As Object, e As EventArgs) Handles btnSave.EnabledChanged
        mnuSpeichern.Enabled = btnSave.Enabled
        cmnuÜbernahme.Enabled = Not btnSave.Enabled
    End Sub
    Private Sub btnWeiter_Click(sender As Object, e As EventArgs) Handles btnWeiter.Click
        btnWeiterClick()
    End Sub
    Private Sub btnWeiterClick()

        'btnWeiterPressed = True
        'RowValidating = False
        Try
            'cboGruppen.SelectedItem = -1  --> nur bei Mannschaft
            If dgvJoin.CurrentRow.Index = dgvJoin.Rows.Count - 1 Then
                dgvJoin.CurrentCell = dgvJoin(0, 0) 'dgvJoin.CurrentRow.Index - 1)
            Else
                dgvJoin.CurrentCell = dgvJoin(0, dgvJoin.CurrentRow.Index + 1)
            End If
        Catch ex As Exception
        End Try
        txtWiegen.Focus()
        'btnWeiterPressed = False
    End Sub

    '' dgvJoin
    Private Sub Build_Grid(Optional Bereich As String = "")

        With dgvJoin
            .AutoGenerateColumns = False
            .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
            .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor

            .Columns("Gruppe").Visible = Wettkampf.Mannschaft

            .Columns("Verein").Visible = Not Wettkampf.International
            .Columns("Land").Visible = Not Wettkampf.International
            '.Columns("Länderwertung").Visible = Not Wettkampf.International
            '.Columns("Vereinswertung").Visible = Not Wettkampf.International
            .Columns("Nation").Visible = Wettkampf.International
            .Columns("Geburtstag").Visible = Wettkampf.International
            .Columns("Jahrgang").Visible = Not Wettkampf.International


            'Format_Grid(Bereich, grid)
            LayoutChanged_W = False
        End With
    End Sub
    Private Sub dgvJoin_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvJoin.CellClick
        If e.RowIndex > -1 Then
            If dgvJoin.Columns(e.ColumnIndex).Name = "WK" Then
                ' WK-Teilnahme CheckBoxColumn
                If IsDBNull(dv(e.RowIndex)("attend")) OrElse CBool(dv(e.RowIndex)("attend")) = True Then
                    dgvJoin.CurrentCell.Value = False
                ElseIf CBool(dv(e.RowIndex)("attend")) = False Then
                    dgvJoin.CurrentCell.Value = True
                End If
                bs.EndEdit()
                btnSave.Enabled = True
            End If
        End If
    End Sub
    Private Sub dgvJoin_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvJoin.CellMouseDown

        selectedRows = dgvJoin.SelectedRows

        If selectedRows.Count > 1 Then
            For Each row As DataGridViewRow In selectedRows
                dgvJoin.Rows(row.Index).DefaultCellStyle.BackColor = Color.FromKnownColor(KnownColor.Highlight)
            Next
        End If

        If e.Button = MouseButtons.Right Then
            SelectedRow = e.RowIndex
            bs.Position = e.RowIndex
        End If
    End Sub
    Private Sub dgvJoin_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvJoin.DataError
        Try
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvJoin_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvJoin.RowEnter
        If loading Then Return
        chkRelativ.Enabled = dv(e.RowIndex)!Sex.ToString.Equals("w") AndAlso Not IsDBNull(dv(e.RowIndex)!Vereinswertung)
    End Sub
    Private Sub dgvJoin_RowValidating(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dgvJoin.RowValidating

        If loading Then Return

        Dim Restriction = Get_Restrictions(dv(e.RowIndex)!Jahrgang)

        If Not IsDBNull(dv(e.RowIndex)!attend) AndAlso Not CBool(dv(e.RowIndex)!attend) Then
            ' --> attend = False
            Dim ReturnValue = RowValidation(e.RowIndex, True)
            If ReturnValue.Equals("Valid") Then
                Using New Centered_MessageBox(Me)
                    If MessageBox.Show("Die eingebenen Daten sind korrekt." & vbNewLine &
                                       "Teilnahme am Wettkampf zulassen?",
                                        msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        dv(e.RowIndex)!attend = True
                        bs.EndEdit()
                    End If
                End Using
            End If
        ElseIf IsDBNull(dv(e.RowIndex)!Wiegen) OrElse
               (Not Restriction(0) AndAlso IsDBNull(dv(e.RowIndex)!Reissen)) OrElse
               (Not Restriction(1) AndAlso IsDBNull(dv(e.RowIndex)!Stossen)) Then
            ' mindestens ein Eintrag fehlt
            If Not IsDBNull(dv(e.RowIndex)!attend) AndAlso CBool(dv(e.RowIndex)!attend) Then
                ' --> attend = True
                e.Cancel = True
                DirtyRow = e.RowIndex
                ActiveControl.Focus()
            End If
        ElseIf Not IsDBNull(dv(e.RowIndex)!Wiegen) AndAlso
               (Restriction(0) OrElse (Not Restriction(0) AndAlso Not IsDBNull(dv(e.RowIndex)!Reissen))) AndAlso
               (Restriction(1) OrElse (Not Restriction(1) AndAlso Not IsDBNull(dv(e.RowIndex)!Stossen))) Then
            ' alle Einträge vorhanden 
            Dim ReturnValue = RowValidation(e.RowIndex)
            If ReturnValue.Equals("Cancel") Then
                e.Cancel = True
                DirtyRow = e.RowIndex
                ActiveControl.Focus()
            ElseIf Not ReturnValue.Equals("Valid") OrElse
                   IsDBNull(dv(e.RowIndex)!attend) Then
                If Not (ActiveControl.Name.Equals("btnExit") AndAlso Not btnSave.Enabled) Then
                    dv(e.RowIndex)!attend = Not ReturnValue.Equals("False")
                    bs.EndEdit()
                    DirtyRow = Nothing
                End If
            Else
                DirtyRow = Nothing
            End If
        End If
    End Sub

    Private Function Validate_GK(pos As Integer, Optional Silent As Boolean = False) As String
        Dim msg = String.Empty
        Dim dlgResult As DialogResult = DialogResult.None

        If Not IsDBNull(dv(pos)!Wiegen) Then
            Dim GK = Glob.Get_GK(dv(pos)!Sex.ToString, CInt(dv(pos)!idAK), CDbl(dv(pos)!Wiegen))
            Dim GK_Saved = GK
            If Not IsDBNull(dv(pos)!Gewicht) Then
                GK_Saved = Glob.Get_GK(dv(pos)!Sex.ToString, CInt(dv(pos)!idAK), CDbl(dv(pos)!Gewicht))
            End If
            If IsDBNull(dv(pos)!idGK) OrElse IsDBNull(dv(pos)!GK) Then
                dv(pos)!GK = GK.Value
                dv(pos)!idGK = GK.Key
            ElseIf CInt(dv(pos)!idGK) <> GK.Key Then
                If GK_Saved.Key = GK.Key Then
                    dv(pos)!GK = GK.Value
                    dv(pos)!idGK = GK.Key
                ElseIf CInt(dv(pos)!idGK) > GK.Key Then
                    msg = "Das Wiege-Gewicht unterschreitet" 'Body weight falls below
                ElseIf CInt(dv(pos)!idGK) < GK.Key Then
                    msg += "Das Wiege-Gewicht überschreitet" 'Body weight exceeds
                End If
                If Not String.IsNullOrEmpty(msg) Then
                    msg += " die gemeldete Gewichtsklasse." ' declared BW Category
                    msg += StrDup(2, vbNewLine) + "Teilnahme am Wettkampf zulassen?"
                    Dim Btns = MessageBoxButtons.YesNoCancel
                    Dim Icon = MessageBoxIcon.Exclamation
                    If Not IsDBNull(dv(pos)!attend) AndAlso CBool(dv(pos)!attend) OrElse Not IsNothing(DirtyRow) Then
                        Btns = MessageBoxButtons.YesNo
                        'Icon = MessageBoxIcon.Question
                    End If
                    If Not Silent Then
                        Using New Centered_MessageBox(Me)
                            dlgResult = MessageBox.Show(msg, msgCaption, Btns, Icon)
                        End Using
                    Else
                        dlgResult = If(String.IsNullOrEmpty(msg), DialogResult.Yes, DialogResult.No)
                    End If
                    Select Case dlgResult
                        Case DialogResult.Yes ' GK ändern
                            dv(pos)!GK = GK.Value
                            dv(pos)!idGK = GK.Key
                        Case DialogResult.No ' vom WK ausschließen
                            Return "False"
                        Case DialogResult.Cancel
                            Return "Cancel"
                    End Select
                End If
            End If
        End If
        Return String.Empty
    End Function
    Private Function Validate_MinLast(pos As Integer, Optional Silent As Boolean = False) As String
        Dim msg = String.Empty
        Dim dlgResult As DialogResult = DialogResult.None

        If Not IsDBNull(dv(pos)!Reissen) AndAlso CInt(dv(pos)!Reissen) < Wettkampf.MinLast(dv(pos)!Sex.ToString, 0) Then
            msg = "Die Anfangslast im Reißen unterschreitet die Mindestlast" & vbNewLine &
                  "(" & Format(Wettkampf.MinLast(dv(pos)!Sex.ToString, 0), "0 kg)")
        End If
        If Not IsDBNull(dv(pos)!Stossen) AndAlso CInt(dv(pos)!Stossen) < Wettkampf.MinLast(dv(pos)!Sex.ToString, 1) Then
            msg += If(String.IsNullOrEmpty(msg), String.Empty, StrDup(2, vbNewLine))
            msg += "Die Anfangslast im Stoßen unterschreitet die Mindestlast" & vbNewLine &
                   "(" & Format(Wettkampf.MinLast(dv(pos)!Sex.ToString, 1), "0 kg)")
        End If
        If Not String.IsNullOrEmpty(msg) Then
            msg += StrDup(2, vbNewLine) + "Teilnahme am Wettkampf zulassen?"
            Dim Btns = MessageBoxButtons.YesNoCancel
            Dim Icon = MessageBoxIcon.Exclamation
            If Not IsDBNull(dv(pos)!attend) AndAlso CBool(dv(pos)!attend) Then
                Btns = MessageBoxButtons.YesNo
                'Icon = MessageBoxIcon.Question
            End If
            If Not Silent Then
                Using New Centered_MessageBox(Me)
                    dlgResult = MessageBox.Show(msg, msgCaption, Btns, Icon)
                End Using
            Else
                dlgResult = If(String.IsNullOrEmpty(msg), DialogResult.Yes, DialogResult.No)
            End If
            Select Case dlgResult
                Case DialogResult.No
                    Return "False"
                Case DialogResult.Cancel
                    Return "Cancel"
            End Select
        End If
        Return String.Empty
    End Function
    Private Function Validate_20kgRule(pos As Integer, Optional Silent As Boolean = False) As String
        Dim dlgResult As DialogResult = DialogResult.None

        If Wettkampf.Regel20 AndAlso Not IsDBNull(dv(pos)!Zweikampf) Then
            ' 20 kg ist hart codiert --> bei Änderung der kg-Regel hier ändern
            If Not IsDBNull(dv(pos)!Reissen) AndAlso Not IsDBNull(dv(pos)!Stossen) Then
                If CInt(dv(pos)!Reissen) + CInt(dv(pos)!Stossen) < CInt(dv(pos)!Zweikampf) - 20 Then
                    Dim msg = "Die gemeldeten Anfangslasten verletzen die 20-kg-Regel." & StrDup(2, vbNewLine) &
                              "Teilnahme am Wettkampf zulassen?"
                    Dim Btns = MessageBoxButtons.YesNoCancel
                    Dim Icon = MessageBoxIcon.Exclamation
                    If Not IsDBNull(dv(pos)!attend) AndAlso CBool(dv(pos)!attend) Then
                        Btns = MessageBoxButtons.YesNo
                        'Icon = MessageBoxIcon.Question
                    End If
                    If Not Silent Then
                        Using New Centered_MessageBox(Me)
                            dlgResult = MessageBox.Show(msg, msgCaption, Btns, Icon)
                        End Using
                    Else
                        dlgResult = If(String.IsNullOrEmpty(msg), DialogResult.Yes, DialogResult.No)
                    End If
                    Select Case dlgResult
                        Case DialogResult.No
                            Return "False"
                        Case DialogResult.Cancel
                            Return "Cancel"
                    End Select
                End If
            End If
        End If
        Return String.Empty
    End Function

    Private Function RowValidation(pos As Integer, Optional Silent As Boolean = False) As String
        ' ReturnValue   False   --> exclude from competition
        '               True    --> include into competition
        '               Cancel  --> return to row
        '               Valid   --> all entries correct

        Dim ReturnValue = "True"
        If mnuSkipRules.Checked Then Return ReturnValue

        Try
            ReturnValue = Validate_GK(pos, Silent)
            If Not String.IsNullOrEmpty(ReturnValue) Then Return ReturnValue

            ReturnValue = Validate_MinLast(pos, Silent)
            If Not String.IsNullOrEmpty(ReturnValue) Then Return ReturnValue

            ReturnValue = Validate_20kgRule(pos, Silent)
            If Not String.IsNullOrEmpty(ReturnValue) Then Return ReturnValue

            If String.IsNullOrEmpty(ReturnValue) Then
                Return ReturnValue
            Else
                Return "Valid"
            End If

        Catch ex As Exception
            LogMessage("Wiegen: dgvJoin: RowValidating: " & ex.Message, ex.StackTrace)
            Return "Error"
        End Try
    End Function
    Private Function Get_Restrictions(Jahrgang As Object) As Boolean()
        Try
            Dim NoReissen = False
            Dim NoStossen = False
            For Each row As DataRow In Glob.dtRestrict.Rows
                Dim maxJG = Year(Wettkampf.Datum) - CInt(row!Altersklasse) ' alle Jahrgänge >= maxJG
                If CInt(Jahrgang) >= maxJG Then
                    If CInt(row!Durchgang) = 0 Then
                        NoReissen = CInt(row!Versuche) = 0
                    ElseIf CInt(row!Durchgang) = 1 Then
                        NoStossen = CInt(row!Versuche) = 0
                    End If
                End If
            Next
            Return {NoReissen, NoStossen}
        Catch
            Return {False, False}
        End Try
    End Function
    Private Function Create_Startnumbers(pos As Integer) As Integer

        Try
            If pos > 0 Then
                Dim prevLot = CInt(dv(pos - 1)("Losnummer"))
                If NoStartnumbers.Keys.Contains(prevLot) AndAlso NoStartnumbers(prevLot) = DelayReasons.NotAssigned Then Return pos ' wenn nicht manuell entfernt, keine weiteren Startnummern vergeben
            End If

            Dim Lot = CInt(dv(pos)("Losnummer"))
            Dim x = NoStartnumbers.IndexOfValue(DelayReasons.NotAppeared)
            If x > -1 AndAlso NoStartnumbers.Keys(x) < Lot Then Return pos ' wenn eine verschobene Losnummer vor der aktuellen ist, keine weiteren Startnummern vergeben


            Dim Restriction = Get_Restrictions(dv(pos)("Jahrgang"))

            If Not IsDBNull(dv(pos)("Wiegen")) AndAlso
               Not IsDBNull(dv(pos)("Losnummer")) AndAlso
               (Restriction(0) OrElse (Not Restriction(0) AndAlso Not IsDBNull(dv(pos)("Reissen")))) AndAlso
               (Restriction(1) OrElse (Not Restriction(1) AndAlso Not IsDBNull(dv(pos)("Stossen")))) Then

                loading = True
                If NoStartnumbers.Keys.Contains(Lot) AndAlso NoStartnumbers(Lot) = DelayReasons.NotAppeared Then dv(pos)("Sort") = CInt(dv(pos)("Sort")) / 100
                NoStartnumbers.Remove(CInt(dv(pos)("Losnummer")))
                dv(pos)("attend") = True
                bs.EndEdit()
                pos = bs.Find("Losnummer", Lot)

                'If (Wettkampf.International OrElse Lotnumbers_Assigned) AndAlso IsDBNull(dv(pos)("Startnummer")) AndAlso Not All_Startnumbers_Allotted Then
                '    pos = Write_Startnumbers(pos)
                'End If

            End If
        Catch ex As Exception
        End Try

        loading = False

        Return pos

    End Function

    Private Function Write_Startnumbers(pos As Integer) As Integer
        Try
            If pos >= dv.Count Then pos = dv.Count - 1
            Dim Lot = CInt(dv(pos)("Losnummer"))

            Dim Startnumber = 1
            Try
                Startnumber = CInt((From x In dv.ToTable
                                    Where x.Field(Of Integer?)("Startnummer") < pos
                                    Select z = x.Field(Of Integer?)("Startnummer")).Max) + 1 ' = nächste zu vergebende Startnummer
            Catch ex As Exception
            End Try

            Do While pos < dv.Count AndAlso Not IsDBNull(dv(pos)("attend"))
                If NoStartnumbers.Keys.Contains(Lot) Then
                    If NoStartnumbers(Lot) = DelayReasons.NotAssigned OrElse NoStartnumbers(Lot) = DelayReasons.NotAppeared Then Exit Do
                ElseIf Not NoStartnumbers.Keys.Contains(CInt(dv(pos)("Losnummer"))) OrElse
                          (NoStartnumbers.Keys.Contains(CInt(dv(pos)("Losnummer"))) AndAlso NoStartnumbers(CInt(dv(pos)("Losnummer"))) = DelayReasons.NotAssigned) Then
                    dv(pos)("Startnummer") = Startnumber
                    bs.EndEdit()
                    NoStartnumbers.Remove(Lot)
                    Startnumber += 1
                    btnSave.Enabled = True
                End If
                pos += 1
            Loop

        Catch ex As Exception
        End Try

        If pos = dv.Count Then pos -= 1
        Return pos
    End Function

    '' Eingabe
    Private Sub txtWiegen_LostFocus(sender As Object, e As EventArgs) Handles txtWiegen.LostFocus
        'Dim Response = Validate_GK(bs.Position)
        'If Response.Equals("Cancel") Then
        '    DirtyRow = bs.Position
        '    txtWiegen.Focus()
        'ElseIf Response.Equals("False") Then
        '    dv(bs.Position)!attend = False
        '    bs.EndEdit()
        'End If
    End Sub
    Private Sub TextBox_GotFocus(sender As Object, e As EventArgs) Handles txtWiegen.GotFocus, txtStossen.GotFocus, txtReissen.GotFocus, txtStartnummer.GotFocus
        With CType(sender, TextBox)
            .Tag = .Text
            .SelectionStart = 0
            .SelectionLength = .Text.Length
        End With
    End Sub
    Private Sub textBox_TextChanged(sender As Object, e As EventArgs) Handles txtWiegen.TextChanged, txtStossen.TextChanged, txtReissen.TextChanged, txtStartnummer.TextChanged
        If loading Then Return
        Dim ctl = CType(sender, TextBox)
        If ctl.Focused Then
            Try
                If String.IsNullOrEmpty(ctl.Text) Then
                    dv(bs.Position)(ctl.Name.Substring(3)) = DBNull.Value
                End If
            Catch ex As Exception
            End Try
            btnSave.Enabled = True
            'IsRowDirty = dgvJoin.CurrentRow.Index
        End If
    End Sub
    Private Sub chkLänderwertung_CheckedChanged(sender As Object, e As EventArgs) Handles chkLänderwertung.CheckedChanged, chkRelativ.CheckedChanged
        If CType(sender, CheckBox).Focused Then
            btnSave.Enabled = True
            'IsRowDirty = dgvJoin.CurrentRow.Index
        End If
    End Sub
    Private Sub chkRelativ_Click(sender As Object, e As EventArgs) Handles chkRelativ.CheckStateChanged

        'If chkRelativ.CheckState = CheckState.Unchecked Then
        '    chkRelativ.CheckState = CheckState.Checked
        '    'dv(dgvJoin.CurrentRow.Index)!Relativ_W = True
        'ElseIf chkRelativ.CheckState = CheckState.Checked Then
        '    chkRelativ.CheckState = CheckState.Indeterminate
        '    'dv(dgvJoin.CurrentRow.Index)!Relativ_W = DBNull.Value
        'ElseIf chkRelativ.CheckState = CheckState.Indeterminate Then
        '    chkRelativ.CheckState = CheckState.Unchecked
        '    'dv(dgvJoin.CurrentRow.Index)!Relativ_W = False
        'End If
        ''bs.EndEdit()
    End Sub
    Private Sub chkRelativ_Validating(sender As Object, e As CancelEventArgs) Handles chkRelativ.Validating

        If Not chkRelativ.Visible Then Return

        Dim msg = String.Empty

        Dim cnt = (From x In dtMeldung
                   Where x.Field(Of Integer)("idVerein") = CInt(dv(bs.Position)!idVerein) AndAlso
                       Not IsNothing(x.Field(Of Integer?)("Vereinswertung")) AndAlso Not x.Field(Of Integer)("Vereinswertung") = 0 AndAlso
                       x.Field(Of Integer)("Vereinswertung") = CInt(dv(bs.Position)!Vereinswertung) AndAlso
                       (x.Field(Of Boolean?)("Relativ_W") = True OrElse x.Field(Of Boolean?)("Relativ_W") Is Nothing)).Count

        If chkRelativ.Checked AndAlso cnt > Max_W_Count Then
            msg = "Es dürfen maximal " & Max_W_Count & " Heberinnen pro Mannschaft ausgewählt werden."
            Using New Centered_MessageBox(Me)
                MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
            chkRelativ.CheckState = CheckState.Unchecked
            e.Cancel = True
        End If
    End Sub
    Private Sub chkMannschaft_GotFocus(sender As Object, e As EventArgs) Handles chkLänderwertung.GotFocus, chkRelativ.GotFocus
        Dim ctl = CType(sender, CheckBox)
        ctl.ForeColor = Color.FromKnownColor(KnownColor.HighlightText)
        ctl.BackColor = Color.FromKnownColor(KnownColor.Highlight)
    End Sub
    Private Sub chkMannschaft_LostFocus(sender As Object, e As EventArgs) Handles chkLänderwertung.LostFocus, chkRelativ.LostFocus
        Dim ctl = CType(sender, CheckBox)
        ctl.ForeColor = Color.FromKnownColor(KnownColor.ControlText)
        ctl.BackColor = BackColor
    End Sub
    Private Sub cboGruppenZuweisung_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGruppenZuweisung.SelectedIndexChanged
        If cboGruppenZuweisung.Focused Then
            If cboGruppenZuweisung.Text = String.Empty Then
                dv(bs.Position)("Gruppe") = DBNull.Value
            Else
                dv(bs.Position)("Gruppe") = cboGruppenZuweisung.Text
            End If
            btnSave.Enabled = True
        End If
    End Sub
    Private Sub cboVereinswertung_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboVereinswertung.SelectedValueChanged
        If cboVereinswertung.Focused() Then btnSave.Enabled = True
    End Sub
    Private Sub cboVereinswertung_VisibleChanged(sender As Object, e As EventArgs) Handles cboVereinswertung.VisibleChanged
        lblVereinswertung.Visible = cboVereinswertung.Visible
    End Sub

    '' Filtern
    Private Sub btnName_Click(sender As Object, e As EventArgs) Handles btnName.Click
        With txtName
            If .ForeColor <> Color.Silver Then .Text = ""
            .Focus()
        End With
    End Sub
    Private Sub txtName_GotFocus(sender As Object, e As EventArgs) Handles txtName.GotFocus, txtName.Click
        With txtName
            If .ForeColor = Color.Silver Then
                .SelectionLength = 0
                .SelectionStart = 0
                .Tag = .Text
            End If
        End With
    End Sub
    Private Sub txtName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtName.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        Dim ctl As TextBox = txtName
        With ctl
            If .ForeColor = Color.Silver Then
                e.Handled = True
                .ForeColor = Color.FromKnownColor(KnownColor.WindowText)
                .Text = e.KeyChar
                .SelectionStart = .Text.Length
            End If
        End With
    End Sub
    Private Sub txtName_TextChanged(sender As Object, e As EventArgs) Handles txtName.TextChanged
        With txtName
            If .Text = "" Then
                .ForeColor = Color.Silver
                .Text = "Nachname"
                If .ForeColor <> Color.Silver Then
                    bs.Filter = "Nachname Like '*" & .Text & "*'"
                End If
            ElseIf .Text = "Nachname" And .ForeColor = Color.Silver Then
                bs.Filter = ""
            ElseIf .Text.Length > 0 And .ForeColor <> Color.Silver Then
                bs.Filter = "Nachname LIKE '*" & .Text & "*'"
            End If
        End With
    End Sub
    Private Sub cboFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVerein.SelectedIndexChanged, cboSex.SelectedIndexChanged, cboJG.SelectedIndexChanged, cboGruppe.SelectedIndexChanged

        If loading Then Return
        loading = True

        Dim _filter = String.Empty
        Try
            If CType(sender, ComboBox).Name.Equals("cboGruppe") AndAlso cboGruppe.SelectedIndex > -1 Then
                btnName.PerformClick()
                btnJG.PerformClick()
                btnSex.PerformClick()
                btnVerein.PerformClick()
                _filter = "Gruppe = '" & cboGruppe.Text + "'"
            Else
                btnGruppe.PerformClick()
                If cboSex.SelectedIndex > -1 Then _filter = "sex = '" + cboSex.Text.Substring(0, 1) + "'"
                If cboJG.SelectedIndex > -1 Then _filter += If(_filter > "", " AND ", "") + "Jahrgang = '" + cboJG.Text + "'"
                If cboVerein.SelectedIndex > -1 Then _filter += If(_filter > "", " AND ", "") + cboVerein.Tag.ToString + " = '" + cboVerein.SelectedValue.ToString + "'"
            End If
            'If Called Then _filter += If(_filter > "", " AND ", "") + "(Convert(IsNull(attend,''), System.String) = '' OR attend = True)"
            bs.Filter = _filter
        Catch ex As Exception
        End Try

        'If Wettkampf.AutoLosnummern Then
        '    If Not cboGruppe.SelectedIndex = -1 Then
        '        ' prüfen, ob ALLE Athleten der ausgewählten Gruppe Startnummern haben
        '        Dim All_Startnumbers_Allotted = (From x In dv.ToTable Where x.Field(Of Integer)("Gruppe") = CInt(cboGruppe.Text) AndAlso
        '                                                              (IsNothing(x.Field(Of Integer?)("Startnummer")) OrElse x.Field(Of Integer)("Startnummer") = 0)).Count = 0
        '        If Not All_Startnumbers_Allotted Then
        '            NoStartnumbers = New SortedList(Of Integer, Integer)
        '            For i = 0 To dv.Count - 1
        '                If Not IsDBNull(dv(i)("Losnummer")) Then
        '                    If IsDBNull(dv(i)("attend")) Then
        '                        NoStartnumbers.Add(CInt(dv(i)("Losnummer")), DelayReasons.NotAssigned)
        '                    ElseIf Not CBool(dv(i)("attend")) Then
        '                        NoStartnumbers.Add(CInt(dv(i)("Losnummer")), DelayReasons.NotAttended)
        '                    End If
        '                End If
        '            Next
        '        End If
        '    End If
        '    ' Startnummern vergeben und Current auf erste Zeile ohne Startnummer setzen
        '    dgvJoin.CurrentCell = dgvJoin(0, Create_Startnumbers(0))
        'End If

        mnuStartnummern.Enabled = cboGruppe.SelectedIndex > -1
        txtWiegen.Focus()
        loading = False
    End Sub
    Private Sub btnJG_Click(sender As Object, e As EventArgs) Handles btnJG.Click
        cboJG.SelectedIndex = -1
    End Sub
    Private Sub btnVerein_Click(sender As Object, e As EventArgs) Handles btnVerein.Click
        cboVerein.SelectedIndex = -1
    End Sub
    Private Sub btnSex_Click(sender As Object, e As EventArgs) Handles btnSex.Click
        cboSex.SelectedIndex = -1
    End Sub
    Private Sub btnGruppe_Click(sender As Object, e As EventArgs) Handles btnGruppe.Click
        cboGruppe.SelectedIndex = -1
    End Sub

    Private Sub mnuWiegeliste_Export_Click(sender As Object, e As EventArgs) Handles mnuWiegeliste_Export.Click

    End Sub

    Private Sub mnuStartnummern_Click(sender As Object, e As EventArgs) Handles mnuStartnummern.Click
        Using New Centered_MessageBox(Me)
            If MessageBox.Show("Startnummern werden entsprechend der angezeigten Sortierung vergeben." & StrDup(2, vbNewLine) &
                               "(Die Sortierung kann durch Klicken auf den entsprechenden Spaltenkopf geändert werden.)",
                               msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) = DialogResult.Cancel Then Return
        End Using

        Dim nr = 1
        For Each row As DataRowView In dv
            If IsDBNull(row!attend) OrElse CBool(row!attend) Then
                row!Startnummer = nr
                nr += 1
            End If
        Next

        bs.EndEdit()
        dgvJoin.Refresh()
        btnSave.Enabled = True
    End Sub

    Private Sub mnuSkipRules_Click(sender As Object, e As EventArgs) Handles mnuSkipRules.Click

    End Sub



End Class
