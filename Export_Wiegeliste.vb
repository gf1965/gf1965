﻿Imports MySqlConnector

Public Class frmExport_Wiegeliste

    Private Sub frmExport_Wiegeliste_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim tTip As New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        tTip.SetToolTip(optSQL, "Datenbank muss auf dem PC vorhanden sein, auf den importiert wird")
        tTip.SetToolTip(optXML, "Export als eigenständiges Programm")
    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim ds As New DataSet
        Dim da As MySqlDataAdapter
        Dim dtJoin As New DataTable With {.TableName = "Liste"}
        Dim dtGK As New DataTable With {.TableName = "GK"}
        Dim dtFilter As New DataTable With {.TableName = "Filter"}
        Dim Query As String = String.Empty
        Dim fail As Boolean = False
        Dim pfad As String = String.Empty

        Cursor = Cursors.WaitCursor

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                'Tabelle
                'Query = "select m.attend, m.Teilnehmer, a.Nachname, a.Vorname, v.idVerein, v.Vereinsname, v.Bundesland, m.Startnummer, m.Wiegen, m.Reissen, m.Stossen, " &
                '        "k.Altersklasse AK, a.Geschlecht sex, m.GK, m.Gruppe, Year(a.Geburtstag) Jahrgang " &
                '        "from meldung m left join athleten a on m.Teilnehmer = a.idTeilnehmer " &
                '        "left join verein v on a.Verein = v.idVerein " &
                '        "left join altersklassen" & If(Wettkampf.International, "_international", "") & " k on " & Year(Wettkampf.Datum) & " - Year(a.Geburtstag) between von and bis " &
                '        "where m.wettkampf = " & Wettkampf.ID & " " &
                '        "order by m.Gruppe, a.Geschlecht, a.Nachname, a.Vorname ASC;"
                Query = "select m.attend, m.Teilnehmer, a.Nachname, a.Vorname, v.idVerein, v.Vereinsname, v.Bundesland, m.Startnummer, m.Wiegen, m.Reissen, m.Stossen, " &
                        "k.Altersklasse AK, a.Geschlecht sex, m.GK, m.Gruppe, Year(a.Geburtstag) Jahrgang " &
                        "from meldung m left join athleten a on m.Teilnehmer = a.idTeilnehmer " &
                        "left join verein v on a.Verein = v.idVerein " &
                        "left join altersklassen k on " & Year(Wettkampf.Datum) & " - Year(a.Geburtstag) between von and bis and k.International = " & Wettkampf.International &
                        " where m.wettkampf = " & Wettkampf.ID &
                        " order by m.Gruppe, a.Geschlecht, a.Nachname, a.Vorname ASC;"
                da = New MySqlDataAdapter(Query, conn)
                da.Fill(ds, "dtJoin")

                'Gewichtsprüfung
                Query = "SELECT * FROM gewichtsklassen;" ' ist in LOAD_WK_TABLES
                da = New MySqlDataAdapter(Query, conn)
                da.Fill(ds, "dtGK")

                'cboFilterJahr ################## aus dtJoin extrahieren ?????
                Query = "select distinct Year(a.Geburtstag) Jahrgang " &
                        "from meldung m " &
                        "left join athleten a on m.Teilnehmer = a.idTeilnehmer " &
                        "where Wettkampf = " & Wettkampf.ID &
                        " order by Year(a.Geburtstag) asc;"
                da = New MySqlDataAdapter(Query, conn)
                da.Fill(ds, "dtFilter")

            Catch ex As Exception
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Export der Wiegeliste fehlgeschlagen", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
                fail = True
            End Try
        End Using
        If fail Then
            Cursor = Cursors.Default
            Exit Sub
        End If

        ' Primärschlussel für dtJoin 
        Dim col(1) As DataColumn
        col(0) = dtJoin.Columns("Teilnehmer")
        dtJoin.PrimaryKey = col


        If optXML.Checked Then

            dlgFolder.Description = "Alle Daten werden im angegebenen Ordner/Laufwerk im Verzeichnis <Wiegeliste\Wettkampf-Bezeichnung> gespeichert."
            If dlgFolder.ShowDialog() = DialogResult.OK Then
                pfad = dlgFolder.SelectedPath

                If Not Directory.Exists(pfad + "\GFHsoft Gewichtheben Wiegeliste\" + Wettkampf.Bezeichnung) Then
                    pfad += "\GFHsoft Gewichtheben Wiegeliste\" + Wettkampf.Bezeichnung
                    Directory.CreateDirectory(pfad)
                End If
                ds.WriteXml(pfad + "\Wiegeliste.xml") ', XmlWriteMode.WriteSchema)

                ' Pfad zur EXE => aufgelöst ohne Filename mit \ am Ende
                Dim exe_pfad = Application.ExecutablePath
                Dim p As String() = Split(exe_pfad, "\")
                exe_pfad = String.Empty
                For i As Integer = 0 To p.Count - 2
                    exe_pfad += p(i) + "\"
                Next
                If Not File.Exists(pfad + "\Wiegeliste.exe") Then
                    File.Copy(exe_pfad + "Wiegeliste.exe", pfad + "\Wiegeliste.exe")
                End If
            End If

        ElseIf optSQL.Checked Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Modul nicht installiert.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        End If

        Cursor = Cursors.Default
        Close()
    End Sub

    Private Sub optSQL_Click(sender As Object, e As EventArgs) Handles optSQL.Click
        optSQL.Checked = Not optSQL.Checked
        optXML.Checked = Not optSQL.Checked
        btnExport.Enabled = True
    End Sub

    Private Sub optXML_Click(sender As Object, e As EventArgs) Handles optXML.Click
        optXML.Checked = Not optXML.Checked
        optSQL.Checked = Not optXML.Checked
        btnExport.Enabled = True
    End Sub

End Class