﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAthleten
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim VerantwortlicherLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAthleten))
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DateiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuVereinNeu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuNotiz = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMauszeiger = New System.Windows.Forms.ToolStripMenuItem()
        Me.cboTitel = New System.Windows.Forms.ComboBox()
        Me.cboVereinsname = New System.Windows.Forms.ComboBox()
        Me.pnlAthlet = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnEditID = New System.Windows.Forms.Button()
        Me.btnVereinsname = New System.Windows.Forms.Button()
        Me.txtVereinsname = New System.Windows.Forms.TextBox()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.btnExpand = New System.Windows.Forms.Button()
        Me.txtGeburtstag = New System.Windows.Forms.MaskedTextBox()
        Me.grpStartrecht = New System.Windows.Forms.GroupBox()
        Me.txtMSR = New System.Windows.Forms.TextBox()
        Me.btnMSR = New System.Windows.Forms.Button()
        Me.btnESR = New System.Windows.Forms.Button()
        Me.txtESR = New System.Windows.Forms.TextBox()
        Me.chkLizenz = New System.Windows.Forms.CheckBox()
        Me.cboMSR = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboESR = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.cboLand = New System.Windows.Forms.ComboBox()
        Me.cboStaat = New System.Windows.Forms.ComboBox()
        Me.picFlagge = New System.Windows.Forms.PictureBox()
        Me.lblID = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboGeschlecht = New System.Windows.Forms.ComboBox()
        Me.lblJahrgang = New System.Windows.Forms.Label()
        Me.txtVorname = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNachname = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtVerein = New System.Windows.Forms.TextBox()
        Me.txtLand = New System.Windows.Forms.TextBox()
        Me.pnlFilter = New System.Windows.Forms.Panel()
        Me.btnLand = New System.Windows.Forms.Button()
        Me.btnVerein = New System.Windows.Forms.Button()
        Me.btnName = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnAccept = New System.Windows.Forms.Button()
        Me.pnlButtons = New System.Windows.Forms.Panel()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnNew = New System.Windows.Forms.Button()
        VerantwortlicherLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.pnlAthlet.SuspendLayout()
        Me.grpStartrecht.SuspendLayout()
        CType(Me.picFlagge, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFilter.SuspendLayout()
        Me.pnlButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'VerantwortlicherLabel
        '
        VerantwortlicherLabel.AutoSize = True
        VerantwortlicherLabel.Location = New System.Drawing.Point(40, 126)
        VerantwortlicherLabel.Name = "VerantwortlicherLabel"
        VerantwortlicherLabel.Size = New System.Drawing.Size(37, 13)
        VerantwortlicherLabel.TabIndex = 35
        VerantwortlicherLabel.Text = "Verein"
        VerantwortlicherLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(51, 20)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(27, 13)
        Label1.TabIndex = 10
        Label1.Text = "Titel"
        Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToOrderColumns = True
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgv.Location = New System.Drawing.Point(0, 258)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowHeadersWidth = 24
        Me.dgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(746, 263)
        Me.dgv.StandardTab = True
        Me.dgv.TabIndex = 1
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DateiToolStripMenuItem, Me.mnuOptionen, Me.mnuExtras})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(746, 24)
        Me.MenuStrip1.TabIndex = 151
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'DateiToolStripMenuItem
        '
        Me.DateiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNew, Me.mnuSave, Me.ToolStripMenuItem3, Me.mnuDelete, Me.ToolStripMenuItem1, Me.mnuClose})
        Me.DateiToolStripMenuItem.Name = "DateiToolStripMenuItem"
        Me.DateiToolStripMenuItem.Size = New System.Drawing.Size(46, 20)
        Me.DateiToolStripMenuItem.Text = "&Datei"
        '
        'mnuNew
        '
        Me.mnuNew.Name = "mnuNew"
        Me.mnuNew.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuNew.Size = New System.Drawing.Size(168, 22)
        Me.mnuNew.Text = "&Neu"
        '
        'mnuSave
        '
        Me.mnuSave.Name = "mnuSave"
        Me.mnuSave.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSave.Size = New System.Drawing.Size(168, 22)
        Me.mnuSave.Text = "&Speichern"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(165, 6)
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        Me.mnuDelete.Size = New System.Drawing.Size(168, 22)
        Me.mnuDelete.Text = "&Löschen"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(165, 6)
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(168, 22)
        Me.mnuClose.Text = "&Beenden"
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuVereinNeu, Me.ToolStripMenuItem2, Me.mnuNotiz})
        Me.mnuOptionen.Name = "mnuOptionen"
        Me.mnuOptionen.Size = New System.Drawing.Size(69, 20)
        Me.mnuOptionen.Text = "&Optionen"
        '
        'mnuVereinNeu
        '
        Me.mnuVereinNeu.Name = "mnuVereinNeu"
        Me.mnuVereinNeu.Size = New System.Drawing.Size(151, 22)
        Me.mnuVereinNeu.Text = "&Verein anlegen"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(148, 6)
        '
        'mnuNotiz
        '
        Me.mnuNotiz.Name = "mnuNotiz"
        Me.mnuNotiz.Size = New System.Drawing.Size(151, 22)
        Me.mnuNotiz.Text = "&Notiz"
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMauszeiger})
        Me.mnuExtras.Name = "mnuExtras"
        Me.mnuExtras.Size = New System.Drawing.Size(50, 20)
        Me.mnuExtras.Text = "&Extras"
        '
        'mnuMauszeiger
        '
        Me.mnuMauszeiger.Name = "mnuMauszeiger"
        Me.mnuMauszeiger.ShortcutKeys = System.Windows.Forms.Keys.F4
        Me.mnuMauszeiger.Size = New System.Drawing.Size(186, 22)
        Me.mnuMauszeiger.Text = "&Mauszeiger holen"
        '
        'cboTitel
        '
        Me.cboTitel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboTitel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboTitel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cboTitel.FormattingEnabled = True
        Me.cboTitel.Location = New System.Drawing.Point(82, 17)
        Me.cboTitel.Name = "cboTitel"
        Me.cboTitel.Size = New System.Drawing.Size(72, 21)
        Me.cboTitel.Sorted = True
        Me.cboTitel.TabIndex = 11
        Me.cboTitel.TabStop = False
        '
        'cboVereinsname
        '
        Me.cboVereinsname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboVereinsname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboVereinsname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVereinsname.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboVereinsname.FormattingEnabled = True
        Me.cboVereinsname.Location = New System.Drawing.Point(82, 123)
        Me.cboVereinsname.Name = "cboVereinsname"
        Me.cboVereinsname.Size = New System.Drawing.Size(222, 21)
        Me.cboVereinsname.Sorted = True
        Me.cboVereinsname.TabIndex = 36
        '
        'pnlAthlet
        '
        Me.pnlAthlet.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.pnlAthlet.Controls.Add(Me.Label2)
        Me.pnlAthlet.Controls.Add(Me.btnEditID)
        Me.pnlAthlet.Controls.Add(Me.btnVereinsname)
        Me.pnlAthlet.Controls.Add(Me.txtVereinsname)
        Me.pnlAthlet.Controls.Add(Me.txtID)
        Me.pnlAthlet.Controls.Add(Me.btnExpand)
        Me.pnlAthlet.Controls.Add(Me.txtGeburtstag)
        Me.pnlAthlet.Controls.Add(Me.grpStartrecht)
        Me.pnlAthlet.Controls.Add(Me.Label21)
        Me.pnlAthlet.Controls.Add(Me.cboLand)
        Me.pnlAthlet.Controls.Add(Me.cboStaat)
        Me.pnlAthlet.Controls.Add(Me.picFlagge)
        Me.pnlAthlet.Controls.Add(Me.lblID)
        Me.pnlAthlet.Controls.Add(Me.Label7)
        Me.pnlAthlet.Controls.Add(Me.cboGeschlecht)
        Me.pnlAthlet.Controls.Add(Me.lblJahrgang)
        Me.pnlAthlet.Controls.Add(VerantwortlicherLabel)
        Me.pnlAthlet.Controls.Add(Me.txtVorname)
        Me.pnlAthlet.Controls.Add(Me.cboVereinsname)
        Me.pnlAthlet.Controls.Add(Me.Label4)
        Me.pnlAthlet.Controls.Add(Me.txtNachname)
        Me.pnlAthlet.Controls.Add(Me.Label3)
        Me.pnlAthlet.Controls.Add(Me.cboTitel)
        Me.pnlAthlet.Controls.Add(Label1)
        Me.pnlAthlet.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlAthlet.Location = New System.Drawing.Point(0, 24)
        Me.pnlAthlet.Name = "pnlAthlet"
        Me.pnlAthlet.Size = New System.Drawing.Size(746, 190)
        Me.pnlAthlet.TabIndex = 159
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(342, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 626
        Me.Label2.Text = "Staat"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnEditID
        '
        Me.btnEditID.BackgroundImage = Global.Gewichtheben.My.Resources.Resources.edit16
        Me.btnEditID.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnEditID.Location = New System.Drawing.Point(280, 15)
        Me.btnEditID.Name = "btnEditID"
        Me.btnEditID.Size = New System.Drawing.Size(24, 24)
        Me.btnEditID.TabIndex = 625
        Me.btnEditID.UseVisualStyleBackColor = True
        '
        'btnVereinsname
        '
        Me.btnVereinsname.Image = Global.Gewichtheben.My.Resources.Resources.Zoom16
        Me.btnVereinsname.Location = New System.Drawing.Point(308, 122)
        Me.btnVereinsname.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnVereinsname.Name = "btnVereinsname"
        Me.btnVereinsname.Size = New System.Drawing.Size(26, 23)
        Me.btnVereinsname.TabIndex = 624
        Me.btnVereinsname.UseVisualStyleBackColor = True
        '
        'txtVereinsname
        '
        Me.txtVereinsname.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVereinsname.Location = New System.Drawing.Point(86, 127)
        Me.txtVereinsname.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtVereinsname.Name = "txtVereinsname"
        Me.txtVereinsname.Size = New System.Drawing.Size(199, 13)
        Me.txtVereinsname.TabIndex = 623
        Me.txtVereinsname.Text = "Rodewisch"
        Me.txtVereinsname.Visible = False
        '
        'txtID
        '
        Me.txtID.Enabled = False
        Me.txtID.Location = New System.Drawing.Point(207, 17)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(67, 20)
        Me.txtID.TabIndex = 622
        Me.txtID.Text = "1000000"
        '
        'btnExpand
        '
        Me.btnExpand.Image = Global.Gewichtheben.My.Resources.Resources.arrow_right_16
        Me.btnExpand.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnExpand.Location = New System.Drawing.Point(390, 150)
        Me.btnExpand.Name = "btnExpand"
        Me.btnExpand.Padding = New System.Windows.Forms.Padding(2, 0, 0, 0)
        Me.btnExpand.Size = New System.Drawing.Size(77, 25)
        Me.btnExpand.TabIndex = 620
        Me.btnExpand.TabStop = False
        Me.btnExpand.Text = "Startrecht"
        Me.btnExpand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExpand.UseCompatibleTextRendering = True
        Me.btnExpand.UseVisualStyleBackColor = True
        Me.btnExpand.Visible = False
        '
        'txtGeburtstag
        '
        Me.txtGeburtstag.Culture = New System.Globalization.CultureInfo("")
        Me.txtGeburtstag.HidePromptOnLeave = True
        Me.txtGeburtstag.HideSelection = False
        Me.txtGeburtstag.Location = New System.Drawing.Point(81, 95)
        Me.txtGeburtstag.Mask = "00.00.0000"
        Me.txtGeburtstag.Name = "txtGeburtstag"
        Me.txtGeburtstag.Size = New System.Drawing.Size(64, 20)
        Me.txtGeburtstag.TabIndex = 26
        Me.txtGeburtstag.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'grpStartrecht
        '
        Me.grpStartrecht.Controls.Add(Me.txtMSR)
        Me.grpStartrecht.Controls.Add(Me.btnMSR)
        Me.grpStartrecht.Controls.Add(Me.btnESR)
        Me.grpStartrecht.Controls.Add(Me.txtESR)
        Me.grpStartrecht.Controls.Add(Me.chkLizenz)
        Me.grpStartrecht.Controls.Add(Me.cboMSR)
        Me.grpStartrecht.Controls.Add(Me.Label6)
        Me.grpStartrecht.Controls.Add(Me.cboESR)
        Me.grpStartrecht.Controls.Add(Me.Label5)
        Me.grpStartrecht.Location = New System.Drawing.Point(499, 13)
        Me.grpStartrecht.Name = "grpStartrecht"
        Me.grpStartrecht.Size = New System.Drawing.Size(230, 158)
        Me.grpStartrecht.TabIndex = 100
        Me.grpStartrecht.TabStop = False
        Me.grpStartrecht.Text = "Startrechte"
        '
        'txtMSR
        '
        Me.txtMSR.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMSR.Location = New System.Drawing.Point(22, 93)
        Me.txtMSR.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtMSR.Name = "txtMSR"
        Me.txtMSR.Size = New System.Drawing.Size(142, 13)
        Me.txtMSR.TabIndex = 627
        Me.txtMSR.Text = "Rodewisch"
        Me.txtMSR.Visible = False
        '
        'btnMSR
        '
        Me.btnMSR.Image = Global.Gewichtheben.My.Resources.Resources.Zoom16
        Me.btnMSR.Location = New System.Drawing.Point(187, 88)
        Me.btnMSR.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnMSR.Name = "btnMSR"
        Me.btnMSR.Size = New System.Drawing.Size(26, 23)
        Me.btnMSR.TabIndex = 626
        Me.btnMSR.UseVisualStyleBackColor = True
        '
        'btnESR
        '
        Me.btnESR.Image = Global.Gewichtheben.My.Resources.Resources.Zoom16
        Me.btnESR.Location = New System.Drawing.Point(187, 40)
        Me.btnESR.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnESR.Name = "btnESR"
        Me.btnESR.Size = New System.Drawing.Size(26, 23)
        Me.btnESR.TabIndex = 625
        Me.btnESR.UseVisualStyleBackColor = True
        '
        'txtESR
        '
        Me.txtESR.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtESR.Location = New System.Drawing.Point(22, 45)
        Me.txtESR.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtESR.Name = "txtESR"
        Me.txtESR.Size = New System.Drawing.Size(142, 13)
        Me.txtESR.TabIndex = 624
        Me.txtESR.Text = "Rodewisch"
        Me.txtESR.Visible = False
        '
        'chkLizenz
        '
        Me.chkLizenz.AutoSize = True
        Me.chkLizenz.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLizenz.ForeColor = System.Drawing.Color.Firebrick
        Me.chkLizenz.Location = New System.Drawing.Point(18, 126)
        Me.chkLizenz.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkLizenz.Name = "chkLizenz"
        Me.chkLizenz.Size = New System.Drawing.Size(135, 17)
        Me.chkLizenz.TabIndex = 105
        Me.chkLizenz.Text = "gültige Start-Lizenz"
        Me.chkLizenz.UseVisualStyleBackColor = True
        '
        'cboMSR
        '
        Me.cboMSR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboMSR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboMSR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMSR.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboMSR.FormattingEnabled = True
        Me.cboMSR.Location = New System.Drawing.Point(18, 89)
        Me.cboMSR.Name = "cboMSR"
        Me.cboMSR.Size = New System.Drawing.Size(164, 21)
        Me.cboMSR.TabIndex = 104
        Me.cboMSR.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(18, 72)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 13)
        Me.Label6.TabIndex = 103
        Me.Label6.Text = "Mannschaft"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboESR
        '
        Me.cboESR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboESR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboESR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboESR.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboESR.FormattingEnabled = True
        Me.cboESR.Location = New System.Drawing.Point(18, 41)
        Me.cboESR.Name = "cboESR"
        Me.cboESR.Size = New System.Drawing.Size(164, 21)
        Me.cboESR.TabIndex = 102
        Me.cboESR.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(18, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 101
        Me.Label5.Text = "Einzel"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(12, 155)
        Me.Label21.MinimumSize = New System.Drawing.Size(36, 13)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(64, 17)
        Me.Label21.TabIndex = 40
        Me.Label21.Text = "Bundesland"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label21.UseCompatibleTextRendering = True
        '
        'cboLand
        '
        Me.cboLand.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboLand.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboLand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLand.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboLand.FormattingEnabled = True
        Me.cboLand.Location = New System.Drawing.Point(82, 152)
        Me.cboLand.Name = "cboLand"
        Me.cboLand.Size = New System.Drawing.Size(222, 21)
        Me.cboLand.TabIndex = 41
        Me.cboLand.TabStop = False
        '
        'cboStaat
        '
        Me.cboStaat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboStaat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboStaat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStaat.DropDownWidth = 170
        Me.cboStaat.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboStaat.FormattingEnabled = True
        Me.cboStaat.Location = New System.Drawing.Point(343, 41)
        Me.cboStaat.Name = "cboStaat"
        Me.cboStaat.Size = New System.Drawing.Size(121, 21)
        Me.cboStaat.TabIndex = 46
        '
        'picFlagge
        '
        Me.picFlagge.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.picFlagge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.picFlagge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picFlagge.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.picFlagge.ImageLocation = ""
        Me.picFlagge.Location = New System.Drawing.Point(349, 71)
        Me.picFlagge.Name = "picFlagge"
        Me.picFlagge.Size = New System.Drawing.Size(90, 63)
        Me.picFlagge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picFlagge.TabIndex = 619
        Me.picFlagge.TabStop = False
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(187, 21)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(18, 13)
        Me.lblID.TabIndex = 618
        Me.lblID.Text = "ID"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(177, 98)
        Me.Label7.MinimumSize = New System.Drawing.Size(36, 13)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 17)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "Geschlecht"
        Me.Label7.UseCompatibleTextRendering = True
        '
        'cboGeschlecht
        '
        Me.cboGeschlecht.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cboGeschlecht.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboGeschlecht.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGeschlecht.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboGeschlecht.FormattingEnabled = True
        Me.cboGeschlecht.Items.AddRange(New Object() {"m", "w"})
        Me.cboGeschlecht.Location = New System.Drawing.Point(243, 95)
        Me.cboGeschlecht.Name = "cboGeschlecht"
        Me.cboGeschlecht.Size = New System.Drawing.Size(61, 21)
        Me.cboGeschlecht.TabIndex = 31
        '
        'lblJahrgang
        '
        Me.lblJahrgang.AutoSize = True
        Me.lblJahrgang.Location = New System.Drawing.Point(19, 99)
        Me.lblJahrgang.Name = "lblJahrgang"
        Me.lblJahrgang.Size = New System.Drawing.Size(59, 13)
        Me.lblJahrgang.TabIndex = 25
        Me.lblJahrgang.Text = "Geburtstag"
        Me.lblJahrgang.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtVorname
        '
        Me.txtVorname.Location = New System.Drawing.Point(81, 70)
        Me.txtVorname.Name = "txtVorname"
        Me.txtVorname.Size = New System.Drawing.Size(224, 20)
        Me.txtVorname.TabIndex = 21
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(29, 73)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Vorname"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtNachname
        '
        Me.txtNachname.Location = New System.Drawing.Point(81, 44)
        Me.txtNachname.Name = "txtNachname"
        Me.txtNachname.Size = New System.Drawing.Size(224, 20)
        Me.txtNachname.TabIndex = 16
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Nachname"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtName
        '
        Me.txtName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtName.ForeColor = System.Drawing.Color.DarkGray
        Me.txtName.Location = New System.Drawing.Point(14, 5)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(128, 20)
        Me.txtName.TabIndex = 3
        Me.txtName.Tag = "Nachname"
        Me.txtName.Text = "Nachname"
        Me.txtName.WordWrap = False
        '
        'txtVerein
        '
        Me.txtVerein.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtVerein.ForeColor = System.Drawing.Color.DarkGray
        Me.txtVerein.Location = New System.Drawing.Point(198, 5)
        Me.txtVerein.Name = "txtVerein"
        Me.txtVerein.Size = New System.Drawing.Size(128, 20)
        Me.txtVerein.TabIndex = 5
        Me.txtVerein.Tag = "Vereinsname"
        Me.txtVerein.Text = "Vereinsname"
        Me.txtVerein.WordWrap = False
        '
        'txtLand
        '
        Me.txtLand.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLand.ForeColor = System.Drawing.Color.DarkGray
        Me.txtLand.Location = New System.Drawing.Point(382, 5)
        Me.txtLand.Name = "txtLand"
        Me.txtLand.Size = New System.Drawing.Size(128, 20)
        Me.txtLand.TabIndex = 7
        Me.txtLand.Tag = "Bundesland"
        Me.txtLand.Text = "Bundesland"
        Me.txtLand.WordWrap = False
        '
        'pnlFilter
        '
        Me.pnlFilter.Controls.Add(Me.btnLand)
        Me.pnlFilter.Controls.Add(Me.txtLand)
        Me.pnlFilter.Controls.Add(Me.btnVerein)
        Me.pnlFilter.Controls.Add(Me.btnName)
        Me.pnlFilter.Controls.Add(Me.txtVerein)
        Me.pnlFilter.Controls.Add(Me.txtName)
        Me.pnlFilter.Location = New System.Drawing.Point(12, 225)
        Me.pnlFilter.Name = "pnlFilter"
        Me.pnlFilter.Size = New System.Drawing.Size(540, 31)
        Me.pnlFilter.TabIndex = 2
        '
        'btnLand
        '
        Me.btnLand.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnLand.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnLand.Image = CType(resources.GetObject("btnLand.Image"), System.Drawing.Image)
        Me.btnLand.Location = New System.Drawing.Point(516, 4)
        Me.btnLand.Name = "btnLand"
        Me.btnLand.Size = New System.Drawing.Size(23, 23)
        Me.btnLand.TabIndex = 8
        Me.btnLand.TabStop = False
        Me.btnLand.UseVisualStyleBackColor = True
        Me.btnLand.Visible = False
        '
        'btnVerein
        '
        Me.btnVerein.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnVerein.Image = CType(resources.GetObject("btnVerein.Image"), System.Drawing.Image)
        Me.btnVerein.Location = New System.Drawing.Point(332, 4)
        Me.btnVerein.Name = "btnVerein"
        Me.btnVerein.Size = New System.Drawing.Size(23, 23)
        Me.btnVerein.TabIndex = 6
        Me.btnVerein.TabStop = False
        Me.btnVerein.UseVisualStyleBackColor = True
        Me.btnVerein.Visible = False
        '
        'btnName
        '
        Me.btnName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnName.Image = CType(resources.GetObject("btnName.Image"), System.Drawing.Image)
        Me.btnName.Location = New System.Drawing.Point(148, 4)
        Me.btnName.Name = "btnName"
        Me.btnName.Size = New System.Drawing.Size(23, 23)
        Me.btnName.TabIndex = 4
        Me.btnName.TabStop = False
        Me.btnName.UseVisualStyleBackColor = True
        Me.btnName.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(96, 5)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 202
        Me.btnCancel.TabStop = False
        Me.btnCancel.Text = "Abbrechen"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnAccept.Location = New System.Drawing.Point(5, 5)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(85, 25)
        Me.btnAccept.TabIndex = 201
        Me.btnAccept.Text = "Übernehmen"
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'pnlButtons
        '
        Me.pnlButtons.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlButtons.Controls.Add(Me.btnCancel)
        Me.pnlButtons.Controls.Add(Me.btnAccept)
        Me.pnlButtons.Location = New System.Drawing.Point(561, 190)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(194, 44)
        Me.pnlButtons.TabIndex = 200
        Me.pnlButtons.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(555, 530)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(85, 25)
        Me.btnSave.TabIndex = 201
        Me.btnSave.Text = "Speichern"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(646, 530)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 25)
        Me.btnClose.TabIndex = 202
        Me.btnClose.TabStop = False
        Me.btnClose.Text = "Schließen"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNew.Location = New System.Drawing.Point(12, 530)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(53, 25)
        Me.btnNew.TabIndex = 203
        Me.btnNew.TabStop = False
        Me.btnNew.Text = "Neu"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'frmAthleten
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CancelButton = Me.btnCancel
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(746, 564)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.pnlButtons)
        Me.Controls.Add(Me.pnlFilter)
        Me.Controls.Add(Me.pnlAthlet)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.dgv)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(770, 618)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(498, 266)
        Me.Name = "frmAthleten"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Stammdaten: Athleten"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.pnlAthlet.ResumeLayout(False)
        Me.pnlAthlet.PerformLayout()
        Me.grpStartrecht.ResumeLayout(False)
        Me.grpStartrecht.PerformLayout()
        CType(Me.picFlagge, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFilter.ResumeLayout(False)
        Me.pnlFilter.PerformLayout()
        Me.pnlButtons.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DateiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboTitel As ComboBox
    Friend WithEvents cboVereinsname As ComboBox
    Friend WithEvents pnlAthlet As Panel
    Friend WithEvents txtName As TextBox
    Friend WithEvents txtVerein As TextBox
    Friend WithEvents btnName As Button
    Friend WithEvents btnVerein As Button
    Friend WithEvents txtLand As TextBox
    Friend WithEvents btnLand As Button
    Friend WithEvents pnlFilter As Panel
    Friend WithEvents mnuDelete As ToolStripMenuItem
    Friend WithEvents cboMSR As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cboESR As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents cboLand As ComboBox
    Friend WithEvents cboStaat As ComboBox
    Friend WithEvents picFlagge As PictureBox
    Friend WithEvents lblID As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents cboGeschlecht As ComboBox
    Friend WithEvents lblJahrgang As Label
    Friend WithEvents txtVorname As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtNachname As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents ToolStripMenuItem3 As ToolStripSeparator
    Friend WithEvents grpStartrecht As GroupBox
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnAccept As Button
    Friend WithEvents pnlButtons As Panel
    Friend WithEvents btnSave As Button
    Friend WithEvents btnClose As Button
    Friend WithEvents btnNew As Button
    Friend WithEvents txtGeburtstag As MaskedTextBox
    Friend WithEvents btnExpand As Button
    Friend WithEvents txtID As TextBox
    Friend WithEvents chkLizenz As CheckBox
    Friend WithEvents mnuExtras As ToolStripMenuItem
    Friend WithEvents mnuMauszeiger As ToolStripMenuItem
    Friend WithEvents btnVereinsname As Button
    Friend WithEvents txtVereinsname As TextBox
    Friend WithEvents mnuOptionen As ToolStripMenuItem
    Friend WithEvents mnuVereinNeu As ToolStripMenuItem
    Friend WithEvents txtMSR As TextBox
    Friend WithEvents btnMSR As Button
    Friend WithEvents btnESR As Button
    Friend WithEvents txtESR As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btnEditID As Button
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents mnuNotiz As ToolStripMenuItem
End Class
