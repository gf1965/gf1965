﻿
Imports System.ComponentModel
Imports System.IO
Imports MySqlConnector

Public Class frmWorldRecords

    Private WithEvents bsRekorde As BindingSource
    Private WithEvents bsAK As BindingSource
    Private WithEvents bsGK As BindingSource
    Private Filters As New Dictionary(Of String, String)
    Private dtGK As DataTable
    Private init As Boolean = True

    Private Sub Me_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If IsNothing(CancelButton) Then dgvRecords.EndEdit()
        If btnSave.Enabled Then
            Using New Centered_MessageBox(Me)
                Select Case MessageBox.Show("Änderungen vor dem Schließen speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                    Case DialogResult.Cancel
                        e.Cancel = True
                    Case DialogResult.Yes
                        Save()
                End Select
            End Using
        End If
        bsAK.Dispose()
        bsGK.Dispose()
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        bsRekorde = New BindingSource With {.DataSource = Glob.dvRekorde}

        With dgvRecords
            .AutoGenerateColumns = False
            .DataSource = bsRekorde
        End With

        Dim dtAK = New DataView(Glob.dvRekorde.ToTable(True, {"age_category", "age_id"})).ToTable
        dtAK.Rows.Add({"", 0})
        bsAK = New BindingSource With {.DataSource = dtAK, .Sort = "age_id"}
        With cboAK
            .DataSource = bsAK
            .DisplayMember = "age_category"
            .ValueMember = "age_id"
        End With

        dtGK = New DataView(Glob.dvRekorde.ToTable(True, {"sex", "bw_category", "bw_id", "age_id"})).ToTable
        dtGK.Rows.Add({"", "", 0, 0})
        bsGK = New BindingSource With {.DataSource = dtGK, .Sort = "sex, bw_id", .Filter = "sex = ''"}
        With cboGK
            .DataSource = bsGK
            .DisplayMember = "bw_category"
            .ValueMember = "bw_id"
        End With

        init = False
        Cursor = Cursors.Default
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click, mnuExit.Click
        Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click, mnuSave.Click
        Save()
    End Sub
    Private Sub btnSave_EnabledChanged(sender As Object, e As EventArgs) Handles btnSave.EnabledChanged
        mnuSave.Enabled = btnSave.Enabled
    End Sub

    Private Sub Save(Optional ReLoad As Boolean = False)

        Cursor = Cursors.WaitCursor
        Dim Err As Boolean
        Validate()
        bsRekorde.EndEdit()
        Dim Validators = {"name", "born", "nation", "date", "place"}

        Dim query = "INSERT INTO world_records 
                     VALUES (@id, @scope, @sex, @type, @lift, @result, @name, @born, @nation, @age_category, @age_id, @bw_category, @bw_id, @date, @place) 
                     ON DUPLICATE KEY UPDATE result = @result, name = @name, born = @born, nation = @nation, date = @date, place = @place;"
        Using conn As New MySqlConnection(User.ConnString)
            Try
                Dim changes = Glob.dtRekorde.GetChanges
                If Not IsNothing(changes) Then
                    conn.Open()
                    For Each row As DataRow In changes.Rows
                        For Each v In Validators
                            Set_Value(row, v)
                        Next
                        Dim cmd = New MySqlCommand(query, conn)
                        With cmd.Parameters
                            .AddWithValue("@id", row!id)
                            .AddWithValue("@scope", row!scope)
                            .AddWithValue("@sex", row!sex)
                            .AddWithValue("@type", row!type)
                            .AddWithValue("@lift", row!lift)
                            .AddWithValue("@result", row!result)
                            .AddWithValue("@name", row!name)
                            .AddWithValue("@born", row!born)
                            .AddWithValue("@nation", row!nation)
                            .AddWithValue("@age_category", row!age_category)
                            .AddWithValue("@age_id", row!age_id)
                            .AddWithValue("@bw_category", row!bw_category)
                            .AddWithValue("@bw_id", row!bw_id)
                            .AddWithValue("@date", row!date)
                            .AddWithValue("@place", row!place)
                        End With
                        cmd.ExecuteNonQuery()
                    Next
                End If
            Catch ex As MySqlException
                Err = True
                MySQl_Error(Nothing, "World_Records: Save: " & ex.Message, ex.StackTrace, True)
            End Try
        End Using
        If Not Err Then
            Glob.dtRekorde.AcceptChanges()
            dgvRecords.ClearSelection()
            btnSave.Enabled = False
            If ReLoad Then
                Using DS As New DataService
                    DS.Set_Global_Data("Rekorde")
                End Using
                bsRekorde = New BindingSource With {.DataSource = Glob.dvRekorde}
                dgvRecords.DataSource = Nothing
                dgvRecords.DataSource = bsRekorde
                Set_Filter()
            End If
        End If
        Cursor = Cursors.Default
    End Sub
    Private Sub Set_Value(row As DataRow, field As String)
        Dim pos = Glob.dvRekorde.Find({row!sex, row!age_id, row!bw_id, row!lift})
        Select Case field
            Case "name"
                If IsDBNull(row(field)) OrElse row(field).ToString.Equals("") Then
                    Glob.dvRekorde(pos)(field) = "World Standard"
                End If
            Case "nation"
                If IsDBNull(row(field)) OrElse row(field).ToString.Equals("") Then
                    Glob.dvRekorde(pos)(field) = "IWF"
                End If
            Case "born", "date"
                If Not IsDBNull(row(field)) AndAlso Not row(field).ToString.Equals("") Then
                    Glob.dvRekorde(pos)(field) = CDate(row(field))
                End If
            Case Else
                If IsDBNull(row(field)) OrElse row(field).ToString.Equals("") Then
                    Glob.dvRekorde(pos)(field) = DBNull.Value
                End If
        End Select
    End Sub
    Private Sub cboScope_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboScope.SelectedIndexChanged
        Dim filter = "scope LIKE '%'"
        If cboScope.SelectedIndex > 0 Then filter = "scope = '" & cboScope.Text & "'"
        Set_Filter("scope", filter)
    End Sub

    Private Sub cboSex_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboSex.SelectedValueChanged
        Dim filter = "sex LIKE '%'"
        Try
            Dim sex = cboSex.SelectedItem.ToString.Substring(0, 1)
            filter = "sex = '" & sex & "'"
            bsGK.Filter = "sex = '" & sex & "' and age_id = " & cboAK.SelectedValue.ToString & " or bw_id = 0"
        Catch ex As Exception
            bsGK.Filter = "sex = ''"
        End Try
        Set_Filter("sex", filter)
    End Sub

    Private Sub cboTyp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTyp.SelectedIndexChanged
        Dim filter = "lift > 0"
        If cboTyp.SelectedIndex > 0 Then filter = "lift = " & cboTyp.SelectedIndex
        Set_Filter("lift", filter)
    End Sub

    Private Sub cboAK_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAK.SelectedIndexChanged
        Dim filter = "age_id > 0"
        If cboAK.SelectedIndex > 0 Then
            filter = "age_id = " & cboAK.SelectedValue.ToString()
            Try
                bsGK.Filter = "sex = '" & cboSex.SelectedItem.ToString.Substring(0, 1) & "' and age_id = " & cboAK.SelectedValue.ToString & " or bw_id = 0"
                Dim found = bsGK.Find("bw_category", DirectCast(cboGK.SelectedItem, DataRowView)!bw_category)
                If found > 0 Then
                    Filters("bw") = "bw_id = " & DirectCast(bsGK(found), DataRowView)!bw_id.ToString
                Else
                    Filters("bw") = "bw_id > 0"
                End If
            Catch ex As Exception
                bsGK.Filter = "sex = ''"
            End Try
        End If
        Set_Filter("age", filter)
    End Sub

    Private Sub cboGK_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGK.SelectedIndexChanged
        Dim filter = "bw_id > 0"
        If cboGK.SelectedIndex > 0 Then
            filter = "bw_id = " & cboGK.SelectedValue.ToString()
        End If
        Set_Filter("bw", filter)
    End Sub

    Private Sub Set_Filter(Optional key As String = "", Optional value As String = "")
        If Not String.IsNullOrEmpty(key) Then Filters(key) = value
        bsRekorde.Filter = Join(Filters.Values.ToArray, " and ")
        If cboTyp.SelectedIndex < 1 Then
            For i = 2 To bsRekorde.Count - 2 Step 3
                dgvRecords.Rows(i).DividerHeight = 2
            Next
        End If
        dgvRecords.ClearSelection()
        btnImport.Enabled = cboSex.SelectedIndex > 0 AndAlso cboAK.SelectedIndex > 0 AndAlso cboScope.SelectedIndex > 0
    End Sub

    Private Sub dgvRecords_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dgvRecords.CellBeginEdit
        CancelButton = Nothing
    End Sub

    Private Sub dgvRecords_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvRecords.CellEndEdit
        CancelButton = btnExit
    End Sub

    Private Sub dgvRecords_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvRecords.CellValueChanged
        If init Then Return
        btnSave.Enabled = True
    End Sub

    Private Sub mnuImport_Click(sender As Object, e As EventArgs) Handles mnuImport.Click
        With OpenFileDialog1
            .Multiselect = True
            .Filter = "CSV-Datei (*.csv)|*.csv|Alle Dateien (*.*)|*.*"
            .InitialDirectory = Path.Combine(Application.StartupPath, "Records")
            If .ShowDialog() = DialogResult.Cancel Then Exit Sub
            For i = 0 To .FileNames.Length - 1
                If Not .FileNames(i).Contains(".csv") Then
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("'" & .SafeFileNames(i) & "' kann nicht importiert werden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                Else
                    Dim array As String() = File.ReadAllLines(.FileNames(i))

                End If
            Next

        End With
    End Sub

    Private Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        With OpenFileDialog1
            .Filter = "CSV-Datei (*.csv)|*.csv|Alle Dateien (*.*)|*.*"
            .InitialDirectory = Path.Combine(Application.StartupPath, "Records")
            If .ShowDialog() = DialogResult.Cancel Then Exit Sub
            Using New Centered_MessageBox(Me)
                If Not .FileName.Contains(".csv") Then
                    MessageBox.Show("'" & .SafeFileName & "' kann nicht importiert werden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Else
                    If MessageBox.Show("Die Daten von '" & .SafeFileName.Substring(0, .SafeFileName.Length - 4) & "' werden im Bereich <" & cboScope.Text & "> in die AK " & cboAK.Text & " (" & cboSex.Text & ") importiert.", msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) = DialogResult.OK Then
                        cboGK.SelectedIndex = 0
                        cboTyp.SelectedIndex = 0
                        Update_DataView(.FileName)
                    End If
                End If
            End Using
        End With
    End Sub

    Private Sub Update_DataView(FileName As String)
        ' Fields:   Categegory;     Lift;       Result;     Name;       Born;       Nation;     Date;       Place

        Dim lines As String() = File.ReadAllLines(FileName)
        Dim sex = cboSex.SelectedItem.ToString.Substring(0, 1)
        Dim age = CInt(cboAK.SelectedValue)
        Dim scope = cboScope.Text
        Dim cmd As MySqlCommand
        Dim NewId = 0
        Dim bw_category = String.Empty

        Using conn As New MySqlConnection(User.ConnString)
            conn.Open()
            cmd = New MySqlCommand("select max(id) lastId from world_records;", conn)
            Dim reader = cmd.ExecuteReader
            If reader.HasRows Then
                reader.Read()
                NewId = CInt(reader!lastId) + 1
            End If
            reader.Close()
        End Using

        For Each line In lines
            Dim items As List(Of String) = Split(line, ";").ToList
            If items(0).StartsWith("-") OrElse items(0).StartsWith("+") OrElse LCase(items(0)).EndsWith("kg") OrElse String.IsNullOrEmpty(items(0)) Then
                If String.IsNullOrEmpty(items(0)) Then
                    ' vorherige idGK übernehmen
                    items(0) = bw_category
                ElseIf Not (items(0).StartsWith("-") OrElse items(0).StartsWith("+")) Then
                    ' ist eine -GK
                    items(0) = "-" + items(0)
                End If
                bw_category = Split(items(0), " ")(0)
                Dim idGK = dtGK.Select("sex = '" & sex & "' and bw_category = '" & bw_category & "' and age_id = " & age)
                Dim lift = Get_Lift(items(1))
                Dim pos = Glob.dvRekorde.Find({scope, sex, age, idGK(0)!bw_id, lift})

                If CInt(Glob.dvRekorde(pos)!result) < CInt(items(2)) Then
                    Glob.dtRekorde.Rows.Add({NewId, scope, sex, Get_Type(lift), lift, items(2), items(3), CDate(items(4)),
                                             items(5), Glob.dvRekorde(pos)!age_category, Glob.dvRekorde(pos)!age_id,
                                             Glob.dvRekorde(pos)!bw_category, Glob.dvRekorde(pos)!bw_id, CDate(items(6)), items(7)})
                    NewId += 1
                End If
            End If
        Next

        Save(ReLoad:=True)
    End Sub

    Private Function Get_Lift(type As String) As Integer
        If UCase(type).Contains("S") Then Return 1
        If UCase(type).Contains("J") Then Return 2
        Return 3
    End Function
    Private Function Get_Type(lift As Integer) As String
        Select Case lift
            Case 1 : Return "Snatch"
            Case 2 : Return "Clean & Jerk"
            Case Else : Return "Total"
        End Select
    End Function

End Class