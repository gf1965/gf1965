﻿
Imports System.ComponentModel
Imports System.Math
Imports MySqlConnector
'Imports Microsoft.Office.Interop.Excel

Public Class frmGruppen

    Dim Bereich As String = "Gruppen"
    Dim Func As New myFunction

    Dim conn As MySqlConnection
    Dim Query As String
    Dim cmd As MySqlCommand

    Dim dtGruppen As New DataTable
    Dim dtMeldung As New DataTable
    'Dim dtAK As New DataTable
    Dim dtGG_Modus As New DataTable ' in modus_gewichtsgruppen definierte Defaults für GGs
    'Dim dtAK_GK As New DataTable
    Dim dtGG_WK As New DataTable ' bei Modus < 400 = in wettkampf_gewichtsgruppen definierte Teiler

    Dim dvM As DataView
    Dim dvU As DataView
    Dim dvG As DataView

    WithEvents bsG As BindingSource
    WithEvents bsM As BindingSource
    WithEvents bsU As BindingSource

    Dim lstAK As New SortedList(Of Integer, String)
    'Dim dicAK As New Dictionary(Of String, Dictionary(Of Integer, List(Of Integer)))

    Dim dicGruppenBezeichnung As New Dictionary(Of String, List(Of String))
    Dim dicGruppenTeiler As New Dictionary(Of String, List(Of Integer))
    Dim dicMaxGruppen As New Dictionary(Of String, Integer)
    Dim sx() As String = {"m", "w"}
    Dim SexName As Dictionary(Of String, String)

    Dim DragSource As DataGridView
    Dim DropIndex As Integer = -1
    Dim DragIndex As Integer = -1
    Dim DragRect As Rectangle

    Dim Unallocated_Flag As String

    Dim ShowAll As New Dictionary(Of Boolean, String)

    Dim maxCount As Integer
    Dim Ctrl_Pressed As Boolean ' If True --> alle TN der ausgewählten Gruppe (also die Gruppe) in andere Gruppe verschieben

    Private Sub frmGruppen_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If mnuSpeichern.Enabled Then
            Using New Centered_MessageBox(Me)
                Select Case MessageBox.Show("Änderungen vor dem Schließen speichern?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                    Case DialogResult.Yes
                        Save()
                    Case DialogResult.Cancel
                        e.Cancel = True
                End Select
            End Using
        End If
        'NativeMethods.INI_Write(Bereich, "Konflikte", CStr(mnuKonflikte.Checked), App_IniFile)
        'NativeMethods.INI_Write(Bereich, "ShowMessages", CStr(mnuMeldungen.Checked), App_IniFile)
    End Sub
    Private Sub frmGruppen_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            If pnlWG.Visible Then pnlWG.Visible = False
        End If
    End Sub
    Private Sub frmGruppen_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        If Wettkampf.Modus < 500 Then
            maxCount = WK_Modus.TeilerMasters
        ElseIf Wettkampf.Modus < 600 Then
            maxCount = WK_Modus.TeilerZweikampf
        Else
            mnuOptionen.Enabled = False
        End If

        SexName = New Dictionary(Of String, String)
        SexName.Add("w", "weiblich")
        SexName.Add("m", "männlich")

        ShowAll(False) = " AND (Convert(IsNull(attend,''), System.String) = '' OR attend = 1)"
        ShowAll(True) = ""

        'Dim x As String
        'x = NativeMethods.INI_Read(Bereich, "Konflikte", App_IniFile)
        'If x <> "?" Then mnuKonflikte.Checked = CBool(x)
        'x = NativeMethods.INI_Read(Bereich, "ShowMessages", App_IniFile)
        'If x <> "?" Then mnuMeldungen.Checked = CBool(x)
    End Sub
    Private Sub frmGruppen_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Try
            Using conn As New MySqlConnection(User.ConnString)
                conn.Open()
                ' Gruppen
                Query = "SELECT g.Gruppe, g.Teilgruppe, g.WG, g.Einteilung, g.Bezeichnung, m.TN, g.Datum, g.DatumW, 
	                        If (Hour(g.WiegenBeginn) + Minute(g.WiegenBeginn) = 0, null, g.WiegenBeginn) WiegenBeginn, 
	                        If (Hour(g.WiegenEnde) + Minute(g.WiegenEnde) = 0, null, g.WiegenEnde) WiegenEnde, 
	                        If (Hour(g.Reissen) + Minute(g.Reissen) = 0, null, g.Reissen) Reissen, 
	                        If (Hour(g.Stossen) + Minute(g.Stossen) = 0, null, Stossen) Stossen, 
                            g.DatumA,
	                        If (Hour(g.Athletik) + Minute(g.Athletik) = 0, null, g.Athletik) Athletik, 
	                        g.Bohle, g.Blockheben 
                        From gruppen g 
                        Left Join 
	                        (
		                        Select distinct Wettkampf, Gruppe, count(Gruppe) over(partition by Wettkampf, Gruppe) TN
                                From meldung
                                Where Wettkampf = " & Wettkampf.ID & " And Not isnull(Gruppe) And Gruppe < 1000
	                        ) m ON m.Gruppe = g.Gruppe 
                        WHERE g.Wettkampf = " & Wettkampf.ID & "
                        GROUP BY g.Gruppe;"
                'WHERE " & Wettkampf.WhereClause("g") & " 
                cmd = New MySqlCommand(Query, conn)
                dtGruppen.Load(cmd.ExecuteReader())

                ' Meldung *****************************************************
                Query = "SELECT DISTINCT m.Teilnehmer, m.Gruppe, m.GG, m.Losnummer, m.Startnummer, a.Nachname, a.Vorname, IfNull(v.Kurzname, v.Vereinsname) Vereinsname, a.Geschlecht Sex, " &
                        "IfNull(m.Wiegen, m.Gewicht) Wiegen, a.Jahrgang JG, IfNull(m.Zweikampf, m.Reissen + m.Stossen) Zweikampf, m.idAK, m.AK, m.idGK, m.GK, m.attend " &
                        "FROM meldung m " &
                        "LEFT JOIN athleten a On m.Teilnehmer = a.idTeilnehmer "

                If Wettkampf.Modus < 400 Then
                    ' Kinder, Schüler, Jugend

                    'Datenquelle für Wettkampf_Gewichtsgruppen
                    cmd = New MySqlCommand("Select * FROM wettkampf_gewichtsgruppen WHERE " & Wettkampf.WhereClause() & " ORDER BY Geschlecht, Gruppe;", conn)
                    dtGG_WK.Load(cmd.ExecuteReader)

                    ' Meldung --> Verein *****************************************
                    Query += "LEFT JOIN verein v ON a.Verein = v.idVerein " &
                             "WHERE " & Wettkampf.WhereClause("m") &
                             "ORDER BY Gruppe ASC, Sex DESC, IfNull(m.Wiegen, m.Gewicht), a.Nachname, a.Vorname;"

                ElseIf Wettkampf.Mannschaft Then
                    ' Meldung --> Mannschaft ************************************
                    Query += "LEFT JOIN Verein v On IFNULL(m.Team,IFNULL(a.MSR, a.Verein)) = v.idVerein " &
                             "WHERE " & Wettkampf.WhereClause("m") &
                             "ORDER BY m.Gruppe, m.idGK, a.Nachname, a.Vorname;"
                Else
                    ' meldung --> Masters, Zweikampf ***************************
                    Query += "LEFT JOIN Verein v ON IFNULL(a.ESR, a.Verein) = v.idVerein " &
                             "WHERE " & Wettkampf.WhereClause("m") &
                             "ORDER BY m.Gruppe ASC, Sex DESC, idAK ASC, idGK ASC, Zweikampf ASC, Losnummer ASC, Wiegen ASC, Nachname ASC, Vorname ASC;"
                End If

                cmd = New MySqlCommand(Query, conn)
                dtMeldung.Load(cmd.ExecuteReader())
                ConnError = False
            End Using
        Catch ex As MySqlException
            MySQl_Error(Nothing, ex.Message, ex.StackTrace, True)
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Datenbank-Fehler (Gruppen einlesen)" & vbNewLine & "Hinweise im Log-Verzeichnis", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            formMain.Change_MainProgressBarValue(0)
            ConnError = True
            Close()
            Return
        End Try

        Try
            If Wettkampf.Modus < 400 Then ' Kinder, Schüler, Jugend
                ' Source für Gruppenbezeichnung und Anzahl Heber/Gruppe
                Using s As New DataService
                    dtGG_Modus = s.Get_Modus_Gewichtsgruppen()
                End Using
            End If
        Catch ex As Exception
        End Try

        Dim dv = New DataView(dtMeldung)
        ' auf nicht gemeldete JG prüfen
        dv.RowFilter = "Convert(IsNull(JG,''), System.String) = '' And attend = true"
        If dv.Count > 0 Then
            Using New Centered_MessageBox(Me, {"Ignorieren"})
                If MessageBox.Show("Athleten ohne Geburtsdatum/Jahrgang gemeldet." + vbNewLine +
                                "Bitte Stammdaten für " & dv(0)!Nachname.ToString.ToUpper & " " & dv(0)!Vorname.ToString &
                                If(dv.Count > 1, " und " & CStr(dv.Count - 1) & " weitere" & If(dv.Count = 2, "n", ""), "") & " korrigieren.",
                                msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then
                    formMain.Change_MainProgressBarValue(0)
                    mnuSpeichern.Enabled = False
                    Close()
                    Return
                End If
            End Using
        End If
        ' auf nicht gemeldete GK prüfen
        dv.RowFilter = "Convert(IsNull(idGK,''), System.String) = '' And attend = true"
        If dv.Count > 0 Then
            Using New Centered_MessageBox(Me, {"Ignorieren"})
                If MessageBox.Show("Heber/innen ohne gültige Gewichtsklasse gemeldet." + vbNewLine +
                                   "Bitte Melde-/Wiegegewicht für " & dv(0)!Nachname.ToString.ToUpper & " " & dv(0)!Vorname.ToString &
                                   If(dv.Count > 1, " und " & CStr(dv.Count - 1) & " weitere" & If(dv.Count = 2, "n", ""), "") & " korrigieren.",
                                   msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then
                    formMain.Change_MainProgressBarValue(0)
                    mnuSpeichern.Enabled = False
                    Close()
                    Return
                End If
            End Using
        End If

        For i = 0 To sx.Count - 1
            If Wettkampf.Modus < 400 Then
                ' Gruppenbezeichnung
                dv = New DataView(dtGG_Modus)
                dv.RowFilter = "Geschlecht = '" & sx(i) & "'"
                Dim slst = New List(Of String)
                Dim ilst = New List(Of Integer)
                For Each r As DataRowView In dv
                    slst.Add(r!Bezeichnung.ToString)
                Next
                dicGruppenBezeichnung.Add(sx(i), slst)
                ' Gruppenteiler WK-spezifisch
                dv = New DataView(dtGG_WK)
                If dv.Count = 0 Then dv = New DataView(dtGG_Modus) ' Gruppenteiler Default
                dv.RowFilter = "Geschlecht = '" & sx(i) & "'"
                For Each r As DataRowView In dv
                    ilst.Add(CInt(r!Heber))
                Next
                dicGruppenTeiler.Add(sx(i), ilst)
                ' maxGruppen 
                dicMaxGruppen.Add(sx(i), If(dv.Count = 1, CInt(dv(0)("Gruppe")), dv.Count))
                'ElseIf Wettkampf.Modus < 600 Then ' ---------------------------------------------------------- Masters (< 500), Zweikampf (< 600)
                '    ' Dictionary mit allen idAK und idGK des WK
                '    Dim dic As New Dictionary(Of Integer, List(Of Integer))
                '    Dim ii = i
                '    Try
                '        Dim tmp = From x In dtAK_GK
                '                  Where x.Field(Of String)("Sex") = sx(ii) And Not IsDBNull(x.Field(Of Integer)("idGK"))
                '                  Group x By idAK = x.Field(Of Integer)("idAK") Into Group
                '                  Select idAK, idGK = (From y In Group Select y.Field(Of Integer)("idGK")).ToList()
                '        For z = 0 To tmp.Count - 1
                '            dic.Add(tmp(z).idAK, tmp(z).idGK)
                '        Next
                '    Catch ex As Exception
                '    End Try
                '    dicAK.Add(sx(i), dic)
            End If
        Next

        ' Liste mit allen AKs
        For Each row As DataRow In Glob.dtAK.Rows
            Dim _ak = row!AK.ToString
            If Not IsDBNull(row!MastersAK) Then _ak += "(" + row!MastersAK.ToString + ")"
            lstAK.Add(CInt(row!idAK), _ak)
        Next

        ' Tabelle Heber (Meldung)
        dvM = New DataView(dtMeldung)
        bsM = New BindingSource With {.DataSource = dvM}
        bsM.Filter = "Convert(IsNull(Gruppe,''), System.String) <> '' And Gruppe < 1000" & ShowAll(False)
        bsM.Sort = Set_Sort() ' Sortierreihenfolge in Meldung
        Setup_Grid(dgvMeldung)

        ' Tabbelle nicht zugewiesene Heber
        dvU = New DataView(dtMeldung)
        bsU = New BindingSource With {.DataSource = dvU}
        bsU.Filter = "(Gruppe = '1000' OR Convert(IsNull(Gruppe,''), System.String) = '')" & ShowAll(False)
        bsU.Sort = Set_Sort() ' Sortierreihenfolge in Meldung

        With dgvUnallocated
            .AutoGenerateColumns = False
            .DataSource = bsU
            .ClearSelection()
        End With

        ' Tabelle Gruppen
        Setup_Grid(dgvGruppen)
        dtGruppen.Columns("TN").AllowDBNull = True

        For Each row As DataRow In dtGruppen.Rows
            dgvGruppen.Rows.Add(row.ItemArray)
        Next

        mnuSpeichern.Enabled = False
        Define_Grid()
        Refresh()

        Dim result = DialogResult.Yes
        If Wettkampf.Modus < 600 Then
            ' alles außer Mannschafts-WK
            If dtGruppen.Rows.Count = 0 Then ' OrElse bsU.Count = dtMeldung.Rows.Count Then
                dgvGruppen.Rows.Clear()
                'If Not HideMessages Then
                '    Using New Centered_MessageBox(Me)
                '        result = MessageBox.Show("Keine Gruppen-Einteilung gefunden." & vbNewLine & "Gruppen jetzt automatisch generieren?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                '    End Using
                'End If
                '' Gruppen anlegen
                'If result = DialogResult.Yes Then
                '    If Wettkampf.Modus < 400 Then
                '        Create_Gruppen100()
                '    ElseIf Wettkampf.Alterseinteilung.Equals("Altersklassen") Then
                '        Create_GruppenAK()
                '    ElseIf Wettkampf.Gewichtseinteilung.Equals("Gewichtsklassen") Then
                '        Create_GruppenGK()
                '    End If
                '    mnuSpeichern.Enabled = True
                'End If
            Else
                Color_Rows()
            End If
        End If

        ' DataSource meldung (wird erst hier gesetzt damit die Tabelle leer bleibt, wenn keine Gruppen angelegt sind)
        dgvMeldung.DataSource = bsM

        If dgvGruppen.Rows.Count > 0 Then
            bsM.Filter = "Gruppe = " & dgvGruppen.Rows(0).Cells("Gruppe").Value.ToString & ShowAll(False) ' " AND (Convert(IsNull(attend,''), System.String) = '' OR attend = 1)"
        Else
            bsM.Filter = "Gruppe = -1"
        End If

        mnuTeilen.Enabled = Wettkampf.Modus < 400
        cmnuTeilen.Enabled = Wettkampf.Modus < 400

        nudMaxCount.Enabled = Wettkampf.Modus >= 400
        btnDown.Enabled = dgvGruppen.Rows.Count > 0

        If dgvGruppen.Rows.Count > 0 Then dgvGruppen.CurrentCell = dgvGruppen("Bezeichnung", 0)

        If dgvGruppen.Rows.Count > 0 AndAlso IsNumeric(dgvGruppen("TN", 0).Value) Then
            optZugewiesen.Checked = True
        Else
            optNichtZugewiesen.Checked = True
        End If

        formMain.Change_MainProgressBarValue(0)

        Cursor = Cursors.Default
    End Sub

    Private Function Set_Sort() As String
        If Wettkampf.Modus < 400 Then       'Schüler
            Return "JG DESC, Wiegen ASC, Nachname ASC, Vorname ASC"
        ElseIf Wettkampf.Modus < 500 Then   ' Masters
            Return "idAK ASC, idGK ASC, Nachname ASC, Vorname ASC"
        ElseIf Wettkampf.Modus < 600 Then   ' Zweikampf
            Return "idAK ASC, idGK ASC, Zweikampf ASC, Losnummer DESC, Nachname ASC, Vorname ASC"
        Else                                ' Mannschaft
            Return "Vereinsname, Nachname, Vorname"
        End If
    End Function

    Private Sub DateTimePicker_Show(ColumnIndex As Integer, RowIndex As Integer)

        Dim loc As Point
        With dgvGruppen
            Dim rect = .GetCellDisplayRectangle(ColumnIndex, RowIndex, True)
            loc = .PointToScreen(New Point(rect.X, rect.Y))
        End With

        Dim ctl As New DateTimePicker

        Select Case dgvGruppen.Columns(ColumnIndex).Name
            Case "Datum", "DatumW", "DatumA"
                ctl = dtpDatum
                dtpZeit.Visible = False
            Case "WiegenBeginn", "WiegenEnde", "Reissen", "Stossen", "Athletik"
                ctl = dtpZeit
                dtpDatum.Visible = False
            Case Else
                Return
        End Select

        With ctl
            .Location = .FindForm.PointToClient(loc)
            Try
                .Value = CDate(dgvGruppen.CurrentCell.Value)
            Catch ex As Exception
            End Try
            .Visible = True
            .Focus()
        End With
    End Sub
    Private Sub DateTimePicker_KeyDown(sender As Object, e As KeyEventArgs) Handles dtpDatum.KeyDown, dtpZeit.KeyDown
        Dim ctl = CType(sender, DateTimePicker)
        With dgvGruppen
            Select Case e.KeyCode
                Case Keys.Escape
                    e.SuppressKeyPress = True
                    .Focus()
                Case Keys.Enter
                    e.SuppressKeyPress = True
                    .CurrentCell.Value = ctl.Value
                    'Dim NextRow = Set_WG_Values(ctl.Text) ' alle Values der aktuellen Column in der WG ändern und RowIndex der letzten geänderten Cell zurückgeben
                    mnuSpeichern.Enabled = True
                    'If NextRow < .Rows.Count - 1 Then
                    '    .CurrentCell = dgvGruppen(.CurrentCell.ColumnIndex, NextRow + 1)
                    '    .FirstDisplayedScrollingRowIndex = NextRow
                    '    DateTimePicker_Show(.CurrentCell.ColumnIndex, NextRow + 1)
                    'Else
                    ctl.Visible = False
                    dgvGruppen.Focus()
                    'End If
            End Select
        End With
    End Sub

    Private Function Set_WG_Values(value As Object) As Integer

        With dgvGruppen
            Dim NextRow = .CurrentRow.Index

            If Not IsNothing(.CurrentRow.Cells("WG").Value) AndAlso
               Not IsDBNull(.CurrentRow.Cells("WG").Value) AndAlso
               Not String.IsNullOrEmpty(.CurrentRow.Cells("WG").Value.ToString) Then

                Dim wg = CInt(.CurrentRow.Cells("WG").Value)

                For i = 0 To .Rows.Count - 1
                    If Not IsDBNull(.Rows(i).Cells("WG").Value) AndAlso CInt(.Rows(i).Cells("WG").Value) = wg Then
                        .Rows(i).Cells(.CurrentCell.ColumnIndex).Value = value
                        NextRow = i
                    End If
                Next
            Else
                .Rows(NextRow).Cells(.CurrentCell.ColumnIndex).Value = value
            End If

            Return NextRow
        End With
    End Function

    ' dgvGruppen
    Private Sub dgvGruppen_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGruppen.CellClick
        If e.RowIndex > -1 AndAlso e.ColumnIndex > -1 Then
            If dgvGruppen.Columns(e.ColumnIndex).Name.Equals("Blockheben") Then
                Set_WG_Values(CBool(Abs(CInt(dgvGruppen.CurrentCell.Value)) - 1)) ' alle Values von Blockheben in der WG ändern
                dgvGruppen.EndEdit()
            Else
                DateTimePicker_Show(e.ColumnIndex, e.RowIndex)
            End If
        End If
    End Sub
    Private Sub dgvGruppen_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGruppen.CellValueChanged
        Static InEdit As Boolean
        If InEdit Then Return
        Try
            With dgvGruppen
                If .Columns(e.ColumnIndex).ReadOnly = False Then mnuSpeichern.Enabled = True
                If .Columns(e.ColumnIndex).Name.Equals("Bohle") OrElse .Columns(e.ColumnIndex).Name.Equals("Blockheben") Then
                    InEdit = True
                    Dim NextRow = Set_WG_Values(.CurrentCell.Value) ' alle Values von Bohle in der WG ändern
                    .EndEdit()
                    'If NextRow < .Rows.Count - 1 Then
                    '    .CurrentCell = dgvGruppen(.CurrentCell.ColumnIndex, NextRow + 1)
                    '    .FirstDisplayedScrollingRowIndex = NextRow
                    'End If
                    InEdit = False
                End If
            End With
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvGruppen_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvGruppen.DataError
        'Try
        'Catch ex As Exception
        'End Try
    End Sub
    Private Sub dgvGruppen_DragDrop(sender As Object, e As DragEventArgs) Handles dgvGruppen.DragDrop, pnlDragRowIndicator.DragDrop
        'Dim msg = String.Empty
        Dim IndexOfRemovedRow = -1
        Dim GroupNumbers() As Integer = Nothing ' (DragGruppe, DropGruppe)

        If DragSource Is sender AndAlso DragIndex.Equals(DropIndex) Then Return

        Cursor = Cursors.WaitCursor

        Dim RowsToDrop As DataGridViewSelectedRowCollection = CType(e.Data.GetData(GetType(DataGridViewSelectedRowCollection)), DataGridViewSelectedRowCollection)

        With dgvGruppen
            'If (DragSource.Name.Equals("dgvMeldung") OrElse DragSource.Name.Equals("dgvUnallocated")) OrElse (DragSource.Name.Equals("dgvGruppen") AndAlso Ctrl_Pressed) Then
            If DragSource.Name.Equals("dgvMeldung") OrElse DragSource.Name.Equals("dgvUnallocated") Then

                ' alle RowsToDrop in Gruppe(DropIndex) droppen
                Dim point As Point = .PointToClient(New Point(e.X, e.Y))
                DropIndex = .HitTest(point.X, point.Y).RowIndex

                If DropIndex < 0 Then
                    ' neue Gruppe anhängen
                    DropIndex = .Rows.Count
                    If .Rows.Count = 0 Then
                        .Rows.Add({DropIndex + 1, "", DBNull.Value, "", "", DBNull.Value, Format(Glob.dtWK(0)("Datum"), "d"), Format(Glob.dtWK(0)("Datum"), "d"), DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, Format(Glob.dtWK(0)("Datum"), "d"), DBNull.Value, 1, False})
                    Else
                        Dim cells = dgvGruppen.Rows(dgvGruppen.CurrentRow.Index).Cells
                        .Rows.AddCopy(0)
                        With .Rows(DropIndex)
                            .Cells(0).Value = DropIndex + 1
                            For i = 1 To cells.Count - 1
                                .Cells(i).Value = cells(i).Value
                            Next
                        End With
                    End If
                End If
                Update_Meldungen(RowsToDrop)
                Update_Teilnehmerzahl()

                .Rows(DropIndex).Cells("Bezeichnung").Selected = True
                mnuSpeichern.Enabled = True

                'ElseIf DragSource.Name.Equals("dgvGruppen") Then
            ElseIf Not Ctrl_Pressed AndAlso DragSource.Name.Equals("dgvGruppen") Then

                ' Gruppe per Drag & Drop verschieben
                If DragIndex > -1 AndAlso DropIndex > -1 Then
                    If Not e.AllowedEffect = DragDropEffects.None Then
                        .Rows.RemoveAt(DragIndex)
                        .Rows.Insert(DropIndex, RowsToDrop(0))
                    End If
                End If
                pnlDragRowIndicator.Visible = False
                Update_Gruppen(DragIndex, DropIndex)
            End If
        End With

        Color_Rows()
        DragIndex = -1
        DropIndex = -1
        DragSource = Nothing
        Cursor = Cursors.Default
    End Sub
    Private Sub dgvGruppen_DragEnter(sender As Object, e As DragEventArgs) Handles dgvGruppen.DragEnter, pnlDragRowIndicator.DragEnter
        e.Effect = DragDropEffects.Move
    End Sub
    Private Sub dgvGruppen_DragLeave(sender As Object, e As EventArgs) Handles dgvGruppen.DragLeave
        pnlDragRowIndicator.Visible = False
    End Sub
    Private Sub dgvGruppen_DragOver(sender As Object, e As DragEventArgs) Handles dgvGruppen.DragOver
        With dgvGruppen
            Dim p = .PointToClient(New Point(e.X, e.Y))
            DropIndex = .HitTest(p.X, p.Y).RowIndex
            If (e.KeyState And (1 + 8)) = (1 + 8) Then ' linke Maustaste + Ctrl = Gruppen-Position verschieben
                .Rows(DropIndex).Selected = True
                e.Effect = DragDropEffects.Move
            Else
                ' Heber in andere Gruppe verschieben
                Try
                    If DragSource Is dgvGruppen Then
                        If DropIndex < 0 OrElse DropIndex.Equals(dgvGruppen.SelectedRows(0).Index) OrElse (dgvGruppen.SelectedRows(0).Index = 0 AndAlso DropIndex <= 0) Then
                            pnlDragRowIndicator.Visible = False
                            e.Effect = DragDropEffects.None
                        Else
                            Rows_DragOver(p.Y, DropIndex)
                            e.Effect = DragDropEffects.Move
                        End If
                    ElseIf DropIndex > -1 Then
                        .Rows(DropIndex).Selected = True
                        .CurrentCell = dgvGruppen(.HitTest(p.X, p.Y).ColumnIndex, DropIndex)
                    ElseIf DropIndex = -1 Then
                        .ClearSelection()
                    End If
                Catch ex As Exception
                    .ClearSelection()
                End Try
            End If
        End With
    End Sub
    Private Sub dgvGruppen_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvGruppen.KeyDown
        Select Case e.KeyCode
            Case Keys.ControlKey
                Ctrl_Pressed = e.KeyCode = Keys.ControlKey
            Case Keys.F2
                DateTimePicker_Show(dgvGruppen.CurrentCell.ColumnIndex, dgvGruppen.CurrentCell.RowIndex)
            Case Keys.Delete
                With dgvGruppen.Columns(dgvGruppen.CurrentCell.ColumnIndex)
                    If Not (.Name.Equals("Gruppe") AndAlso .Name.Equals("TN") AndAlso .Name.Equals("Blockheben")) Then
                        dgvGruppen.CurrentCell.Value = DBNull.Value
                        mnuSpeichern.Enabled = True
                    End If
                End With
        End Select
    End Sub
    Private Sub dgvGruppen_KeyUp(sender As Object, e As KeyEventArgs) Handles dgvGruppen.KeyUp
        If e.KeyCode = Keys.ControlKey Then Ctrl_Pressed = False
    End Sub
    Private Sub dgvGruppen_MouseDown(sender As Object, e As MouseEventArgs) Handles dgvGruppen.MouseDown
        With dgvGruppen
            Try
                If e.Button = MouseButtons.Left Then
                    'DragIndex = -1
                    DropIndex = -1
                    DragIndex = .HitTest(e.X, e.Y).RowIndex
                    If DragIndex > -1 Then
                        Dim dragSize As Size = SystemInformation.DragSize
                        DragRect = New Rectangle(New Point(e.X - (dragSize.Width \ 2), e.Y - (dragSize.Height \ 2)), dragSize)
                    Else
                        DragRect = Rectangle.Empty
                    End If
                End If
            Catch ex As Exception
            End Try
        End With
    End Sub
    Private Sub dgvGruppen_MouseMove(sender As Object, e As MouseEventArgs) Handles dgvGruppen.MouseMove
        If e.Button = MouseButtons.Left AndAlso (DragRect <> Rectangle.Empty AndAlso Not DragRect.Contains(e.X, e.Y)) Then
            DragSource = dgvGruppen
            If Not Ctrl_Pressed Then
                ' Gruppe in der Reihenfolge verschieben
                Unallocated_Flag = String.Empty
                With dgvGruppen
                    If .SelectedRows.Count = 0 Then
                        .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                        .Rows(.CurrentRow.Index).Selected = True
                    End If
                    .DoDragDrop(.SelectedRows, DragDropEffects.Move)
                End With
                'ElseIf Ctrl_Pressed Then
                '    ' alle TN der ausgewählten Gruppe (also die Gruppe) in andere Gruppe verschieben
                '    With dgvMeldung
                '        .SelectAll()
                '        .DoDragDrop(.SelectedRows, DragDropEffects.Move)
                '    End With
            End If
            With dgvGruppen
                .SelectionMode = DataGridViewSelectionMode.CellSelect
            End With
        End If
    End Sub
    Private Sub dgvGruppen_SelectionChanged(sender As Object, e As EventArgs) Handles dgvGruppen.SelectionChanged

        btnDown.Enabled = Not IsNothing(dgvGruppen.CurrentRow) AndAlso dgvGruppen.CurrentRow.Index < dgvGruppen.Rows.Count - 1
        btnUp.Enabled = Not IsNothing(dgvGruppen.CurrentRow) AndAlso dgvGruppen.CurrentRow.Index > 0
        btnRemove.Enabled = dgvGruppen.Rows.Count > 0

        If dgvGruppen.Rows.Count = 0 Then Return

        If IsNothing(DragSource) Then
            'bsM.Filter = "Gruppe = " & dgvGruppen.CurrentRow.Cells("Gruppe").Value.ToString
            bsM.Filter = "Gruppe = " & dgvGruppen.CurrentRow.Cells("Gruppe").Value.ToString & ShowAll(chkShowAll.Checked) '" AND (Convert(IsNull(attend,''), System.String) = '' OR attend = 1)"
        End If
    End Sub

    ' alle TN-Grids
    Private Sub dgvTeilnehmer_GotFocus(sender As Object, e As EventArgs) Handles dgvMeldung.GotFocus, dgvUnallocated.GotFocus
        CType(sender, DataGridView).ClearSelection()
    End Sub
    Private Sub dgvTeilnehmer_SelectionChanged(sender As Object, e As EventArgs) Handles dgvMeldung.SelectionChanged, dgvUnallocated.SelectionChanged
        If DragSource Is sender Then DragSource = Nothing
        btnMoveLeft.Enabled = sender.Equals(dgvUnallocated) AndAlso dgvUnallocated.SelectedRows.Count > 0
    End Sub

    ' dgvMeldung 
    Private Sub dgvMeldung_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvMeldung.CellFormatting
        If e.RowIndex > -1 AndAlso Not IsDBNull(e.Value) AndAlso e.ColumnIndex = dgvMeldung.Columns("Sex").Index Then
            e.CellStyle.BackColor = SexColor(e.Value.ToString)
        End If
    End Sub
    Private Sub dgvMeldung_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvMeldung.KeyDown
        Select Case e.KeyCode
            Case Keys.A
                If ModifierKeys = Keys.Control Then dgvMeldung.SelectAll()
        End Select
    End Sub
    Private Sub dgvMeldung_MouseDown(sender As Object, e As MouseEventArgs) Handles dgvMeldung.MouseDown
        With dgvMeldung
            Try
                If e.Button = MouseButtons.Left Then
                    DragIndex = .HitTest(e.X, e.Y).RowIndex
                    DragRect = .GetRowDisplayRectangle(DragIndex, True)
                    If .SelectedRows.Contains(.Rows(DragIndex)) AndAlso .SelectedRows.Count > 1 Then
                        Unallocated_Flag = String.Empty
                        DragSource = dgvMeldung
                        .DoDragDrop(.SelectedRows, DragDropEffects.Move)
                    End If
                End If
            Catch ex As Exception
            End Try
        End With
    End Sub
    Private Sub dgvMeldung_MouseMove(sender As Object, e As MouseEventArgs) Handles dgvMeldung.MouseMove
        With dgvMeldung
            If .SelectedRows.Count > 0 AndAlso e.Button = MouseButtons.Left Then 'AndAlso (DragRect <> Rectangle.Empty AndAlso Not DragRect.Contains(e.X, e.Y)) Then
                Unallocated_Flag = String.Empty
                DragSource = dgvMeldung
                .DoDragDrop(.SelectedRows, DragDropEffects.Move)
            End If
        End With
    End Sub

    ' dgvUnallocated
    Private Sub dgvUnallocated_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvUnallocated.CellFormatting
        If e.RowIndex > -1 AndAlso e.ColumnIndex = dgvUnallocated.Columns("uSex").Index Then
            e.CellStyle.BackColor = SexColor(e.Value.ToString)
        End If
    End Sub
    Private Sub dgvUnallocated_LostFocus(sender As Object, e As EventArgs) Handles dgvUnallocated.LostFocus
        If Not (btnMoveLeft.Focused() OrElse btnMoveRight.Focused()) Then
            dgvUnallocated.ClearSelection()
        End If
    End Sub
    Private Sub dgvUnallocated_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvUnallocated.KeyDown
        Select Case e.KeyCode
            Case Keys.A
                If ModifierKeys = Keys.Control Then dgvUnallocated.SelectAll()
        End Select
    End Sub
    Private Sub dgvUnallocated_MouseDown(sender As Object, e As MouseEventArgs) Handles dgvUnallocated.MouseDown
        With dgvUnallocated
            Try
                If e.Button = MouseButtons.Left Then
                    DragIndex = .HitTest(e.X, e.Y).RowIndex
                    DragRect = .GetRowDisplayRectangle(DragIndex, True)
                    If .SelectedRows.Contains(.Rows(DragIndex)) AndAlso .SelectedRows.Count > 1 Then
                        Unallocated_Flag = "u"
                        DragSource = dgvUnallocated
                        .DoDragDrop(.SelectedRows, DragDropEffects.Move)
                    End If
                End If
            Catch ex As Exception
            End Try
        End With
    End Sub
    Private Sub dgvUnallocated_MouseMove(sender As Object, e As MouseEventArgs) Handles dgvUnallocated.MouseMove
        With dgvUnallocated
            If .SelectedRows.Count = 1 AndAlso e.Button = MouseButtons.Left Then ' AndAlso (DragRect <> Rectangle.Empty AndAlso Not DragRect.Contains(e.X, e.Y)) Then
                Unallocated_Flag = "u"
                DragSource = dgvUnallocated
                .DoDragDrop(.SelectedRows, DragDropEffects.Move)
            End If
        End With
    End Sub

    '' Drag & Drop - Funktionen
    Private Sub Update_Meldungen(draggedRows As DataGridViewSelectedRowCollection)

        For i = 0 To draggedRows.Count - 1
            Try
                'sucht TN im DV der zugeordneten Meldungen
                dvM(bsM.Find("Teilnehmer", draggedRows(i).Cells(Unallocated_Flag + "Teilnehmer").Value))("Gruppe") = DropIndex + 1
            Catch ex As Exception
                ' TN in Unallocated --> sucht TN in table(Meldung)
                Dim tmp = dtMeldung.Select("Teilnehmer = " & draggedRows(i).Cells(Unallocated_Flag + "Teilnehmer").Value.ToString)
                tmp(0)("Gruppe") = DropIndex + 1
            End Try
            bsM.EndEdit()
        Next
        dtMeldung.AcceptChanges()

        'For Each row As DataGridViewRow In draggedRows
        '    If row.Index > -1 Then
        '        'If pos > -1 Then
        '        If Unallocated_Flag.Equals(String.Empty) Then
        '            Dim pos = bsM.Find("Teilnehmer", row.Cells(Unallocated_Flag + "Teilnehmer").Value)
        '            dvM(pos)!Gruppe = DropIndex + 1
        '            bsM.EndEdit()
        '        Else
        '            Dim pos = bsU.Find("Teilnehmer", row.Cells(Unallocated_Flag + "Teilnehmer").Value)
        '            dvU(pos)!Gruppe = DropIndex + 1
        '            bsU.EndEdit()
        '        End If
        '    End If
        'Next
        'dtMeldung.AcceptChanges()
    End Sub
    Private Sub Update_Teilnehmerzahl()
        For Each row As DataGridViewRow In dgvGruppen.Rows
            Dim Gruppe = CInt(row.Cells("Gruppe").Value)
            Dim Count = (From x In dtMeldung
                         Where x.Field(Of Integer?)("Gruppe") = Gruppe).Count
            row.Cells("TN").Value = Count
        Next
    End Sub
    Private Sub Rows_DragOver(Y_Pos As Integer, curRow As Integer)
        With dgvGruppen
            Dim RowHeight As Integer = .RowTemplate.Height
            Dim Loc_X As Integer = .Location.X + 2
            Dim Loc_Y As Integer = .ColumnHeadersHeight

            If curRow <> -1 Then
                If curRow < .SelectedRows(0).Index Then
                    ' Verschieben nach oberhalb der Selektion
                    Loc_Y += .Location.Y + (.Rows(curRow).Index) * RowHeight - .VerticalScrollingOffset
                ElseIf curRow > .SelectedRows(0).Index Then
                    ' Verschieben nach unterhalb der Selektion
                    Loc_Y += .Location.Y + (.Rows(curRow).Index + 1) * RowHeight - .VerticalScrollingOffset
                End If
            ElseIf Y_Pos > .Location.Y + .ColumnHeadersHeight Then
                Loc_Y += .Location.Y + (.Rows.Count) * RowHeight
            Else
                Loc_Y += .Location.Y
            End If

            With pnlDragRowIndicator
                .Location = New Point(Loc_X, Loc_Y)
                .Visible = True
            End With
        End With
    End Sub
    Private Sub Update_Gruppen(DragIndex As Integer, DropIndex As Integer)
        ' Parameter     DragIndex = removed rowIndex
        '               DropIndex = added rowIndex

        With dgvGruppen
            ' wenn verschoben, verschobene Gruppe zwischenspeichern (bei Entfernen nicht notwendig)
            If DropIndex > -1 Then Write_Group(DragIndex + 1, -1)

            If DragIndex < DropIndex Then
                ' Gruppe nach unten verschieben
                For i = DragIndex + 1 To DropIndex
                    .Rows(i - 1).Cells("Gruppe").Value = i
                    Write_Group(i + 1, i)
                Next
            ElseIf DropIndex = -1 Then
                For i = DragIndex To .Rows.Count - 1
                    .Rows(i).Cells("Gruppe").Value = i + 1
                    Write_Group(i + 2, i + 1)
                Next
            Else
                ' Gruppe nach oben verschieben
                For i = DragIndex To DropIndex + 1 Step -1
                    .Rows(i).Cells("Gruppe").Value = i + 1
                    Write_Group(i, i + 1)
                Next
            End If

            .SelectionMode = DataGridViewSelectionMode.FullRowSelect

            ' wenn verschoben, verschobene Gruppe zurückspeichern (bei Entfernen nicht notwendig)
            If DropIndex > -1 Then
                .Rows(DropIndex).Cells("Gruppe").Value = DropIndex + 1
                Write_Group(-1, DropIndex + 1)
                bsM.Filter = "Gruppe = " + .Rows(DropIndex).Cells("Gruppe").Value.ToString & ShowAll(chkShowAll.Checked)
                .Rows(DropIndex).Selected = True
            Else
                .Rows(DragIndex).Selected = True
            End If

            .Focus()

            btnDown.Enabled = Not IsNothing(.CurrentRow) AndAlso .CurrentRow.Index < .Rows.Count
            btnUp.Enabled = Not IsNothing(.CurrentRow) AndAlso .CurrentRow.Index > 0
            btnRemove.Enabled = .Rows.Count > 0
        End With

        mnuSpeichern.Enabled = True
    End Sub
    Private Sub Write_Group(CurrGroup As Integer?, NewGroup As Integer)
        Dim tmp = New DataView(dtMeldung, "Gruppe = " & CurrGroup, Nothing, DataViewRowState.CurrentRows)
        For Each row As DataRowView In tmp
            row!Gruppe = NewGroup
        Next
        bsM.EndEdit()
    End Sub
    Private Sub pnlDragRowIndicator_VisibleChanged(sender As Object, e As EventArgs) Handles pnlDragRowIndicator.VisibleChanged
        If pnlDragRowIndicator.Visible Then
            With dgvGruppen
                Dim PnlWidth = If(.RowHeadersVisible, .RowHeadersWidth, 0)
                For i = 0 To .ColumnCount - 1
                    If .Columns(i).Visible Then PnlWidth += .Columns(i).Width
                Next
                pnlDragRowIndicator.Size = New Size(PnlWidth - 2, 2)
            End With
        End If
    End Sub










    Private Function Gleiche_Gewichte_korrigieren() As Boolean
        Dim dv = New DataView(dtMeldung)
        dv.Sort = "Sex, JG, Wiegen"

        Dim WeighIn = From x In dv.ToTable
                      Group By Jahrgang = New With {
                        Key .Sex = x.Field(Of String)("Sex"),
                        Key .JG = x.Field(Of Integer)("JG")} Into JG_Group = Group
                      Select Gewichtsgruppen = New With {Key .GG = (From t In JG_Group
                                                                    Group By GruppenNr = t.Field(Of Integer)("Gruppe") Into TN_Group = Group)}
                      Where Gewichtsgruppen.GG.Count > 1
                      Select Gewichtsgruppen

        For Each item In WeighIn
            ' alle in Gewichtsgruppen geteilte Jahrgänge
            Dim lst() = {New SortedList(Of Double, Integer), New SortedList(Of Double, Integer)} ' Wiegen, Anzahl Heber mit diesem Gewicht in der Gruppe
            Dim BeginAtUpper = True

            For i = 0 To item.GG.Count - 2
                If BeginAtUpper Then
                    lst(0) = Get_Lifters(item.GG(i).TN_Group, True)
                    lst(1) = Get_Lifters(item.GG(i + 1).TN_Group, False)
                Else
                    lst(0) = Get_Lifters(item.GG(i - 1).TN_Group, True)
                    lst(1) = Get_Lifters(item.GG(i).TN_Group, False)
                End If

                If Abs(lst(0).Keys(0)) = lst(1).Keys(0) Then
                    Dim msg = "In Gruppe "
                    msg += If(BeginAtUpper, item.GG(i).GruppenNr.ToString, item.GG(i - 1).GruppenNr.ToString)
                    msg += " und "
                    msg += If(BeginAtUpper, item.GG(i + 1).GruppenNr.ToString, item.GG(i).GruppenNr.ToString)
                    msg += " sind gleiche Körpergewichte vorhanden." + vbNewLine + "Gruppen-Einteilung ändern?"

                    Using New Centered_MessageBox(Me)
                        If MessageBox.Show(msg, msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then Return True
                    End Using

                    ' gleiche KGs in verschiedenen GGs
                    Dim KG = lst(1).Keys(0)
                    Dim GruppenCount() = {item.GG(i).TN_Group.Count, item.GG(i + 1).TN_Group.Count}
                    Dim HeberCount() = {lst(0).Values(0), lst(1).Values(0)}
                    Dim NächstesKG() = {Abs(lst(0).Keys(1)), lst(1).Keys(1)}

                    ' Gruppengröße nach Verschieben
                    If GruppenCount(1) - HeberCount(1) = GruppenCount(0) + HeberCount(1) Then
                        ' Heber MUSS in 1.Gruppe, weil dann die Gruppen ausgeglichen sind
                    ElseIf GruppenCount(0) - HeberCount(0) = GruppenCount(1) + HeberCount(0) Then
                        ' Heber MUSS in 2.Gruppe, weil dann die Gruppen ausgeglichen sind
                    End If

                    ' KG-Abstand zum nächsten 
                    If NächstesKG(0) - KG < NächstesKG(1) - KG Then
                        ' Heber müsste in 1.Gruppe
                    Else
                        ' Heber müsste in 2.Gruppe
                    End If

                    If HeberCount(0) > HeberCount(1) Then
                        ' Heber müsste in 1.Gruppe
                    Else
                        ' Heber müsste in 2.Gruppe
                    End If

                    If GruppenCount(0) + HeberCount(1) <= GruppenCount(1) + HeberCount(0) Then
                        ' Heber müsste in 1.Gruppe
                    Else
                        ' Heber müsste in 2.Gruppe
                    End If
                End If
                ' Listen löschen
                lst(0).Clear()
                lst(1).Clear()
                BeginAtUpper = CBool(Abs(CInt(BeginAtUpper)) - 1)
            Next
        Next

        Return False
    End Function
    Private Function Get_Lifters(Lifters As IEnumerable(Of DataRow), BeginAtUpper As Boolean) As SortedList(Of Double, Integer)
        Dim lst As New SortedList(Of Double, Integer)
        If BeginAtUpper Then
            ' negative Werte, um absteigend zu sortieren (größtes KG zuerst)
            For i = Lifters.Count - 1 To 0 Step -1
                If Not lst.ContainsKey(-CDbl(Lifters(i)!Wiegen)) Then
                    ' Gewicht in Liste
                    lst.Add(-CDbl(Lifters(i)!Wiegen), 1)
                Else
                    'Anzahl addieren
                    lst.Item(-CDbl(Lifters(i)!Wiegen)) = lst.Item(-CDbl(Lifters(i)!Wiegen)) + 1
                End If
                If lst.Count = 2 Then Exit For
            Next
        Else
            ' positive Werte, um aufsteigend zu sortieren (kleinstes KG zuerst)
            For i = 0 To Lifters.Count - 1
                If Not lst.ContainsKey(CDbl(Lifters(i)!Wiegen)) Then
                    ' Gewicht in Liste
                    lst.Add(CDbl(Lifters(i)!Wiegen), 1)
                Else
                    'Anzahl addieren
                    lst.Item(CDbl(Lifters(i)!Wiegen)) = lst.Item(CDbl(Lifters(i)!Wiegen)) + 1
                End If
                If lst.Count = 2 Then Exit For
            Next
        End If
        Return lst
    End Function
    Private Sub Controls_GotFocus(sender As Object, e As EventArgs) Handles dgvGruppen.GotFocus, dgvMeldung.GotFocus,
        btnAdd.GotFocus, btnRemove.GotFocus, btnUp.GotFocus, btnDown.GotFocus, btnSave.GotFocus ', Me.GotFocus

        dtpDatum.Visible = False
        dtpZeit.Visible = False
    End Sub
    Private Sub Define_Grid()
        Try
            If Wettkampf.Modus < 400 Then
                ' Kinder, Schüler, Jugend
                With dgvGruppen
                    .Columns("DatumW").Visible = True
                    .Columns("DatumA").Visible = True
                    .Columns("Athletik").Visible = True
                    .Columns("Einteilung").Visible = False
                    .Columns("Datum").DisplayIndex = .Columns("Reissen").DisplayIndex - 1
                    .Width += 90
                End With
                With dgvMeldung
                    .Columns("JG").Visible = True
                    .Columns("AK").Visible = False
                    .Columns("GK").Visible = False
                    .Left += 90
                    .Width -= 90
                    'lblGruppe.Left = .Left + 3
                End With
                With dgvUnallocated
                    .Columns("uJG").Visible = True
                    .Columns("uAK").Visible = False
                    .Columns("uGK").Visible = False
                    '.Left += 90
                    '.Width -= 90
                End With
            ElseIf Wettkampf.Modus < 500 Then
                ' Masters
                With dgvGruppen
                    '.Width += 90
                    .Columns("Einteilung").Visible = False
                End With
                With dgvMeldung
                    .Columns("ZK").Visible = True
                    .Columns("Wiegen").Visible = False
                    '    .Left += 90
                    '    .Width -= 90
                    '    lblGruppe.Left = .Left + 3
                End With
                With dgvUnallocated
                    .Columns("uZK").Visible = True
                    .Columns("uWiegen").Visible = False
                    '    .Left += 90
                    '    .Width -= 90
                End With
                'btnUpdate.Left += 90
                'lblUnallocated.Left += 90
            ElseIf Wettkampf.Modus < 600 Then
                ' Zweikampf
                With dgvGruppen
                    .Columns("Einteilung").Visible = False
                End With
                With dgvMeldung
                    .Columns("Wiegen").Visible = False
                    .Columns("ZK").Visible = True
                    .Columns("Nr").Visible = True
                End With
                With dgvUnallocated
                    .Columns("uWiegen").Visible = False
                    .Columns("uZK").Visible = True
                    .Columns("uNr").Visible = True
                End With
            Else
                ' Mannschaft
                With dgvGruppen
                    .Columns("Einteilung").Visible = False
                End With
                With dgvMeldung
                    .Columns("Team").Visible = True
                    .Columns("AK").Visible = False
                    .Columns("GK").Visible = False
                    .Columns("Wiegen").Visible = False
                    .Columns("Sex").Visible = True
                End With
                With dgvUnallocated
                    .Columns("uTeam").Visible = True
                    .Columns("uAK").Visible = False
                    .Columns("uGK").Visible = False
                    .Columns("uWiegen").Visible = False
                End With
            End If
            With dgvUnallocated
                .Location = dgvMeldung.Location
                .Size = dgvMeldung.Size
                .Visible = True
            End With
            Dim x1 = dgvGruppen.Left + dgvGruppen.Width
            Dim x2 = dgvMeldung.Left
            Dim x = x1 + (x2 - x1) \ 2 - btnMoveLeft.Width \ 2
            btnMoveLeft.Left = x
            chkShowAll.Left = dgvMeldung.Left + 1
        Catch ex As Exception
        End Try

    End Sub
    Private Sub Color_Rows()
        With dgvGruppen
            For Each row As DataGridViewRow In .Rows
                Try
                    'Dim sex = Strings.Left(row.Cells("Bezeichnung").Value.ToString, 1)
                    Dim sex = If(row.Cells("Bezeichnung").Value.ToString.ToLower.Contains("w"), "w", "m")
                    If Not sex.Equals("w") AndAlso Not sex.Equals("m") Then
                        sex = LCase(row.Cells("Bezeichnung").Value.ToString)
                        If sex.Contains("weib") OrElse
                                sex.Contains(") w") OrElse
                                sex.Contains("w(") OrElse
                                sex.Contains("w (") Then
                            sex = "w"
                        ElseIf sex.Contains("männ") OrElse
                                sex.Contains("man") OrElse
                                sex.Contains(") m") OrElse
                                sex.Contains("m(") OrElse
                                sex.Contains("m (") Then
                            sex = "m"
                        End If
                    End If
                    row.Cells("Gruppe").Style.BackColor = SexColor(sex)
                    row.Cells("Teilgruppe").Style.BackColor = row.Cells("Gruppe").Style.BackColor
                Catch ex As Exception
                End Try
            Next
            .Focus()
        End With
    End Sub





    Private Sub Create_Gruppen100()
        ' Kinder, Schüler, Jugend
        For i = sx.Count - 1 To 0 Step -1
            Dim c = i

            Dim _grpcnt As Integer

            If Wettkampf.Alterseinteilung.Equals("Altersgruppen") OrElse Glob.dtAG.Rows.Count > 0 Then
#Region "Einteilung AG"
                ' jede AG in eine eigene Gruppe
                ' Liste aller Jahrgänge des Geschlechts 
                Dim AGs = (From x In dtMeldung
                           Where x.Field(Of String)("Sex") = sx(c)
                           Order By x.Field(Of Integer)("JG") Descending
                           Group x By a = x.Field(Of Integer)("JG") Into Group
                           Select a).ToList

                Do While AGs.Count > 0
                    Dim _jgs() As String
                    Dim JGs = From j In Glob.dtAG
                              Where (IsDBNull(j.Field(Of String)("Sex")) OrElse j.Field(Of String)("Sex") = sx(c)) AndAlso j.Field(Of String)("Jahrgang").Contains(AGs(0).ToString)
                              Select j.Field(Of String)("Jahrgang")

                    Dim Filter = String.Empty
                    If JGs.Count = 0 Then
                        _jgs = {AGs(0).ToString}
                        Filter = "Sex = '" + sx(c) + "' And JG = " + AGs(0).ToString
                    Else
                        _jgs = Split(JGs(0), ",")
                        Filter = " Sex = '" + sx(c) + "' AND (JG = " + String.Join(" OR JG = ", _jgs) + ")"
                    End If

                    dvM.RowFilter = Filter

                    Dim lst As New List(Of String)
                    ' hinzugefügte Jahrgänge in AGs löschen
                    For Each item In _jgs
                        AGs.Remove(CInt(item))
                        lst.Add(CStr(Year(Wettkampf.Datum) - CInt(item)))
                    Next
                    lst.Sort()

                    ' Gruppen erstellen 
                    Dim _gruppe = dgvGruppen.Rows.Count + 1
                    'Dim _name = sx(c) + "(" + String.Join(", ", _jgs) + ")"
                    Dim _name = "AK " + String.Join("/", lst) + " " + sx(c)
                    Dim _tn = dvM.Count

                    ' Heber der Gruppe zuweisen
                    For Each row As DataRowView In dvM
                        row!Gruppe = _gruppe
                    Next
                    bsM.EndEdit()

                    ' Gruppe einfügen
                    dgvGruppen.Rows.Add({_gruppe, "", DBNull.Value, "", _name, _tn, Format(Glob.dtWK(0)("Datum"), "d"), Format(Glob.dtWK(0)("Datum"), "d"), DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, Format(Glob.dtWK(0)("Datum"), "d"), DBNull.Value, 1, False})
                Loop
#End Region
            ElseIf Wettkampf.Alterseinteilung.Equals("Jahrgang") Then
#Region "Einteilung AK"
                ' jede AK in eine eigene Gruppe
                ' Auflistung: JG, Anzahl TN im JG
                Dim lst As New List(Of KeyValuePair(Of Integer, Integer))
                Dim tmp = From g In dtMeldung
                          Order By g.Field(Of Integer?)("JG")', g.Field(Of Integer?)("idGK"), g.Field(Of Double?)("Wiegen")
                          Where g.Field(Of String)("Sex") = sx(c)
                          Group g By Key = g.Field(Of Integer)("JG") Into Group
                          Select JG = Key, nCount = Group.Count

                For r = tmp.Count - 1 To 0 Step -1
                    lst.Add(New KeyValuePair(Of Integer, Integer)(tmp(r).JG, tmp(r).nCount))
                Next

                For r = 0 To lst.Count - 1
                    Dim rr = r
                    ' belegte Gruppen ermitteln
                    dvM.Sort = "Wiegen ASC"
                    dvM.RowFilter = "Sex = '" & sx(c) & "' AND JG = " & lst(rr).Key
                    _grpcnt = dicGruppenTeiler(sx(c)).FindIndex(Function(value As Integer)
                                                                    Return value > lst(rr).Value
                                                                End Function)

                    If _grpcnt = 0 Then _grpcnt = 1 ' erste Gruppe hat bis-Wert, die weiteren Gruppen von-Werte
                    Dim _groups = Select_Groups(_grpcnt) ' Gruppenbezeichnung

                    ' TN-Anzahl der einzelnen Gruppen
                    Dim _grps(4) As Integer
                    For t = 0 To _grps.Count - 1
                        If _groups.Contains(t) Then _grps(t) = dvM.Count \ _groups.Count
                    Next

                    ' restliche TN verteilen
                    Dim a As Integer = 0
                    For t = 0 To (dvM.Count Mod _groups.Count)
                        If _groups.Contains(t) Then
                            _grps(t) += 1
                            a += 1
                            If a = dicMaxGruppen(sx(i)) Then a = 0
                        End If
                    Next

                    Dim start As Integer = 0
                    For z = 0 To _groups.Count - 1
                        ' Gruppen erstellen 
                        Dim _gruppe = dgvGruppen.Rows.Count + 1
                        ' Dim _einteilung = 
                        'Dim _einteilung = sx(c) + "(" + tmp(rr).JG.ToString + ")"
                        Dim _name = sx(c) + "(" + lst(rr).Key.ToString + ")" + " " + dicGruppenBezeichnung(sx(c))(_groups(z))
                        Dim _tn = _grps(_groups(z))

                        ' Heber selektieren und Gruppe zuweisen
                        Dim ende = start + _tn - 1
                        For o = start To ende
                            dvM(o)("Gruppe") = _gruppe
                        Next
                        bsM.EndEdit()
                        start += _tn
                        ' bei gesplitteten Gruppen -> Gewicht zur Einteilung hinzufügen
                        'If _groups.Length > 1 Then _einteilung += CDbl(dvM(ende)("Wiegen")).ToString(" bis 0.00 kg")

                        ' Gruppe einfügen
                        dgvGruppen.Rows.Add({_gruppe, "", DBNull.Value, "", _name, _tn, Format(Glob.dtWK(0)("Datum"), "d"), Format(Glob.dtWK(0)("Datum"), "d"), DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, Format(Glob.dtWK(0)("Datum"), "d"), DBNull.Value, 1, False})

                    Next
                Next
#End Region
            ElseIf Wettkampf.Gewichtseinteilung.Equals("Gewichtsgruppen") Then 'Alterseinteilung.Equals("Altersgruppen") Then
#Region "Einteilung GG"
                ' alle TN auf Gewichtsgruppen aufteilen
                ' Auflistung: KG(Wiegen/Gewicht), Anzahl TN / KG --> IsDbNull(Wiegen) wird beim Erstellen überprüft
                Dim tmp = (From g In dtMeldung Order By g.Field(Of Double)("Wiegen")
                           Where g.Field(Of String)("Sex") = sx(c)
                           Group g By Key = g.Field(Of Double?)("Wiegen") Into Group
                           Select KG = Key, nCount = Group.Count).ToList

                dvM.Sort = "idGK, Wiegen"
                dvM.RowFilter = "Sex = '" & sx(c) & "'"

                ' belegte Gruppen ermitteln
                _grpcnt = dicGruppenTeiler(sx(c)).FindIndex(Function(value As Integer)
                                                                Return value >= dvM.Count ' Return value > dvM.Count ????? --> testen
                                                            End Function)
                If _grpcnt = 0 Then _grpcnt = 1
                Dim SelectedGroups = Select_Groups(_grpcnt)

                ' TN-Anzahl der einzelnen Gruppen
                Dim AllGroups(dicMaxGruppen(sx(i)) - 1) As Integer
                For t = 0 To AllGroups.Count - 1
                    If SelectedGroups.Contains(t) Then AllGroups(t) = dvM.Count \ SelectedGroups.Count
                Next

                ' restliche TN verteilen
                Dim a As Integer = 0
                For t = 0 To (dvM.Count Mod SelectedGroups.Count)
                    If SelectedGroups.Contains(t) Then
                        AllGroups(t) += 1
                        a += 1
                        If a = dicMaxGruppen(sx(i)) Then a = 0
                    End If
                Next

                ' gleiche Gewichte in eine Gruppe verschieben
                For z = 0 To SelectedGroups.Count - 1
                    Dim _tn = AllGroups(SelectedGroups(z)) ' TN in Gruppe
                    Dim Sum_TN_tmp = 0 ' tempCount TN
                    For t = 0 To tmp.Count - 1
                        If _tn - Sum_TN_tmp > tmp(t).nCount / 2 Then
                            ' verbleibende Anzahl TN in Gruppe ist kleiner 
                            ' als die Hälfte der hinzuzufügenden TN mit dem gleichem Gewicht
                            ' --> Anzahl TN mit diesem Gewicht passt in Gruppe
                            Sum_TN_tmp += tmp(t).nCount
                        ElseIf z < SelectedGroups.Count - 1 Then  'If Sum_TN_tmp + tmp(t).nCount <= _tn Then
                            ' weitere Gruppen vorhanden
                            ' --> Anzahl TN in Gruppe(z) korrigieren
                            AllGroups(SelectedGroups(z)) = Sum_TN_tmp
                            ' --> Differenz aus aktueller Gruppe zur nächsten Gruppe addieren
                            AllGroups(SelectedGroups(z + 1)) = AllGroups(SelectedGroups(z + 1)) + _tn - Sum_TN_tmp
                            Exit For
                        End If
                    Next
                Next

                ' Gruppen erstellen 
                Dim start As Integer = 0
                For z = 0 To SelectedGroups.Count - 1
                    Dim _gruppe = dgvGruppen.Rows.Count + 1
                    'Dim _einteilung = 
                    Dim _name = SexName(sx(c)) + " " + dicGruppenBezeichnung(sx(c))(SelectedGroups(z))
                    Dim _tn = AllGroups(SelectedGroups(z))
                    'dvM.RowFilter = "Sex = '" & sx(c) & "'"

                    ' Heber selektieren und Gruppe zuweisen
                    Dim ende = start + _tn - 1
                    For pos = start To ende
                        dvM(pos)("Gruppe") = _gruppe
                    Next
                    bsM.EndEdit()
                    start = ende + 1

                    ' bei gesplitteten Gruppen -> Gewicht zur Einteilung hinzufügen
                    'If SelectedGroups.Count > 1 Then _einteilung += CDbl(dvM(ende)("Wiegen")).ToString(" bis 0.00 kg")

                    ' Gruppe einfügen
                    dgvGruppen.Rows.Add({_gruppe, "", DBNull.Value, "", _name, _tn, Format(Glob.dtWK(0)("Datum"), "d"), Format(Glob.dtWK(0)("Datum"), "d"), DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, Format(Glob.dtWK(0)("Datum"), "d"), DBNull.Value, 1, False})
                Next
#End Region
            End If
        Next
        Color_Rows()
    End Sub
    Private Function Write_GK(Group As String, Sex As String, vonGK As Integer, bisGK As Integer, AK As Integer) As String()

        ' Parameter - Group = Einteilung der Gruppe
        '           - Sex   = Geschlecht der AK
        '           - vonGK = erste GK der AK in der Gruppe
        '           - bisGK = letzte GK der AK in der Gruppe
        ' Rückgabe  (0) idGK(s) für Einteilung (Leerzeichen getrennt)
        '           (1) formatierte idGK(s) für Bezeichnung
        '           (2) Filter-String für dvM

        'Sex = LCase(Sex)

        'If Group.Contains(GetAK(lstAK(AK), Sex)) Then
        '    ' AK ist in der Gruppe enthalten

        '    Dim alleGKs = dicAK(Sex)(AK).IndexOf(vonGK) <= 0 AndAlso dicAK(Sex)(AK).IndexOf(bisGK) = dicAK(Sex)(AK).Count - 1

        '    If dicAK(Sex)(AK).Count > 0 AndAlso bisGK > vonGK Then
        '        ' mehrere GKs
        '        Dim _filter = " AND ("
        '        For i = dicAK(Sex)(AK).IndexOf(vonGK) To dicAK(Sex)(AK).IndexOf(bisGK)
        '            _filter += String.Concat("idGK = ", dicAK(Sex)(AK)(i), " OR ")
        '        Next
        '        _filter = Strings.Left(_filter, _filter.Length - 4) + ")"
        '        If alleGKs Then
        '            Return {String.Empty, String.Empty, _filter}
        '        Else
        '            Dim GKs = GetGK(AK, Sex, vonGK, bisGK)
        '            Return {" " + vonGK.ToString + " " + bisGK.ToString, " " + GKs(0) + " bis " + GKs(1), _filter}
        '        End If
        '    Else
        '        'eine GK
        '        Dim GKs = GetGK(AK, Sex, vonGK)
        '        If Not alleGKs Then Return {" " + vonGK.ToString, " " + GKs(0), " AND idGK = " + vonGK.ToString}
        '    End If
        'End If
        Return {String.Empty, String.Empty, String.Empty}
    End Function
    Private Function GetGK(AK As Integer, sex As String, vonGK As Integer, Optional bisGK As Integer = -1) As String()
        Dim p = Glob.dvGK.Find(vonGK)
        Dim gkVon = Glob.dvGK(p)(Func.Format_AK(lstAK(AK), sex)).ToString
        If bisGK = -1 Then Return {gkVon}
        p = Glob.dvGK.Find(bisGK)
        Dim gkBis = Glob.dvGK(p)(Func.Format_AK(lstAK(AK), sex)).ToString
        Return {gkVon, gkBis}
    End Function
    Private Sub Create_GruppenAK()

        Dim ShowParts = False
        Dim TN_Count = 0

        Dim tmp = New DataTable
        tmp.Columns.Add(New DataColumn With {.ColumnName = "Gruppe", .DataType = GetType(Integer)})
        tmp.Columns.Add(New DataColumn With {.ColumnName = "Parts", .DataType = GetType(Integer)})
        tmp.Columns.Add(New DataColumn With {.ColumnName = "Sex", .DataType = GetType(String)})
        tmp.Columns.Add(New DataColumn With {.ColumnName = "AK", .DataType = GetType(Integer)})
        tmp.Columns.Add(New DataColumn With {.ColumnName = "GK", .DataType = GetType(Integer)})
        tmp.Columns.Add(New DataColumn With {.ColumnName = "TN", .DataType = GetType(Integer), .DefaultValue = DBNull.Value, .AllowDBNull = True})

        Dim Group = 1 '0
        Dim PartCode = 65 ' ASCII-Code für "A"

        For s = 1 To 0 Step -1 ' weiblich --> männlich
            TN_Count = 0
            Dim ss = s
            Dim AKs = From x In dtMeldung
                      Where x.Field(Of String)("Sex") = sx(ss)
                      Group x By AK = x.Field(Of Integer)("idAK") Into aGroup = Group
                      Select AK, GKs = From y In aGroup
                                       Group y By GK = y.Field(Of Integer?)("idGK") Into TN = Group

            ' AKs                = Liste der gemeldeten idAKs pro Geschlecht
            ' AKs(Ix).AK         = idAK
            ' AKs(Ix).GKs        = Liste der GKs in der jeweiligen AK
            ' AKs(Ix).GKs(Ix).TN = Heber in der jeweiligen GK
            ' AKs(Ix).GKs(Ix).GK = idGK der jeweiligen GK

            For a = 0 To AKs.Count - 1
                ' jede AK durchlaufen
                For g = 0 To AKs(a).GKs.Count - 1
                    ' jede GK der AK durchlaufen
                    If TN_Count + AKs(a).GKs(g).TN.Count > maxCount Then
                        ' GK zu groß für aktuelle Gruppe
                        If AKs(a).GKs(g).TN.Count > maxCount Then
                            ' GK zu groß für eine Gruppe --> A/B-Gruppen anlegen
                            Dim Parts_Count = CInt(Ceiling(AKs(a).GKs(g).TN.Count / maxCount))
                            Dim Parts_TN = Floor(AKs(a).GKs(g).TN.Count / Parts_Count) ' rundet ab --> der Rest muss verteilt werden
                            Dim Parts_Rest = AKs(a).GKs(g).TN.Count Mod Parts_Count
                            ' wenn die Gruppe bereits existiert, neue Gruppe beginnen
                            If tmp.Rows.Count > 0 AndAlso CInt(tmp(tmp.Rows.Count - 1)("Gruppe")) = Group Then Group += 1
                            For i = Parts_Count - 1 To 0 Step -1
                                tmp.Rows.Add({Group, PartCode + i, sx(s), AKs(a).AK, AKs(a).GKs(g).GK, Parts_TN + If(Parts_Rest > 0, 1, 0)}) ' A-Gruppe hat immer am wenigsten TNs
                                If Parts_Rest > 0 Then Parts_Rest -= 1
                                Group += 1
                            Next
                            TN_Count = 0
                            ShowParts = True
                        Else
                            ' neue Gruppe mit aktueller GK beginnen
                            Group += 1
                            TN_Count = AKs(a).GKs(g).TN.Count
                            tmp.Rows.Add({Group, PartCode, sx(s), AKs(a).AK, AKs(a).GKs(g).GK})
                        End If
                    Else
                        ' GK passt in Gruppe
                        TN_Count += AKs(a).GKs(g).TN.Count
                        tmp.Rows.Add({Group, PartCode, sx(s), AKs(a).AK, AKs(a).GKs(g).GK})
                    End If
                Next
            Next
            Group += 1
        Next

        Dim _sx = String.Empty
        Dim _AKs As New Dictionary(Of Integer, String)
        Dim _GKs As New List(Of KeyValuePair(Of Integer, String))
        Dim _AK = String.Empty
        Dim _GK = String.Empty
        Group = 1
        Dim cnt = 0
        Dim _Filter = String.Empty
        Dim _TN = 0

        For Each row As DataRow In tmp.Rows
            If Not _sx.Equals(row!Sex.ToString) Then _sx = row!Sex.ToString
            If Not _AKs.ContainsKey(CInt(row!AK)) Then _AKs(CInt(row!AK)) = GetAK(lstAK(CInt(row!AK)), row!Sex.ToString)
            cnt += 1
            If cnt < tmp.Rows.Count AndAlso row!Sex.ToString.Equals(tmp(cnt)("Sex").ToString) AndAlso CInt(tmp(cnt)("Gruppe")) > Group AndAlso CInt(row!AK) = CInt(tmp(cnt)("AK")) Then
                ' wenn AK gesplittet wird
                '_GKs.Add(New KeyValuePair(Of Integer, String)(CInt(row!GK), dicGKs_BySex_ByAK(row!Sex.ToString)(CInt(row!AK))(CInt(row!GK))))
                'If Not cnt = tmp.Rows.Count Then _GKs.Add(New KeyValuePair(Of Integer, String)(CInt(tmp(cnt)("GK")), dicGKs_BySex_ByAK(tmp(cnt)("Sex").ToString)(CInt(tmp(cnt)("AK")))(CInt(tmp(cnt)("GK")))))
                Dim __gk = Glob.dvGK.Find({row!AK, row!Sex, row!GK})
                _GKs.Add(New KeyValuePair(Of Integer, String)(CInt(row!GK), Glob.dvGK(__gk)!GK.ToString))

            End If
            If cnt = tmp.Rows.Count OrElse CInt(tmp(cnt)("Gruppe")) > Group Then
                ' letzte Zeile oder neue Gruppe in nächster Zeile
                _AK = String.Join(", ", _AKs.Values)
                _Filter = "idAK = " & String.Join(" OR idAK = ", _AKs.Keys)
                If _GKs.Count > 0 Then
                    ' --> GKs anhängen
                    If _GKs.Count > 1 AndAlso _GKs.Count < 3 AndAlso CInt(row!AK) = CInt(tmp(cnt)("AK")) Then
                        ' nur eine AK ODER nächste Gruppe mit gleicher AK
                        If IsDBNull(row!TN) Then
                            ' --> bis GK(0)
                            _GK = String.Concat(" bis ", _GKs(0).Value)
                        Else
                            _GK = String.Concat(" ", _GKs(0).Value)
                        End If
                        ' --> an letzte AK anhängen
                        _AK += _GK
                        _Filter += " AND idGK <= " & _GKs(0).Key
                        ' --> alle bis auf letzte GK löschen
                        For i = 0 To _GKs.Count - 2
                            _GKs.RemoveAt(0)
                        Next
                    ElseIf _GKs.Count > 1 AndAlso _AKs.Keys(0) <> CInt(row!AK) Then
                        ' AK wechselt in der Gruppe --> _GKs.Count = 3 
                        _GK = String.Concat(" ab ", _GKs(0).Value)
                        _AK = _AK.Insert(_AK.IndexOf(","), _GK)
                        _GK = String.Concat(" bis ", _GKs(1).Value)
                        _AK += _GK
                        _Filter = _Filter.Insert(_Filter.IndexOf("OR"), "AND idGK >= " & _GKs(0).Key & " ")
                        _Filter += " AND idGK <= " & _GKs(1).Key
                        If CInt(row!AK) = CInt(tmp(cnt)("AK")) Then
                            ' --> alle bis auf letzte GK löschen
                            For i = 0 To _GKs.Count - 2
                                _GKs.RemoveAt(0)
                            Next
                        Else
                            _GKs.Clear()
                        End If
                    ElseIf _AKs.Count = 1 AndAlso CInt(row!AK) = CInt(tmp(cnt)("AK")) Then
                        ' nur eine AK UND nächste Gruppe mit gleicher AK
                        ' --> GK(0) bis GK(1)
                        _GK = String.Concat(" ", _GKs(0).Value)
                        If Not _GKs(0).Value.Equals(_GKs(1).Value) Then _GK = String.Concat(_GK, " bis ", _GKs(1).Value)
                        ' --> an AK anhängen
                        _AK += _GK
                        _Filter += " AND idGK >= " & _GKs(0).Key & " AND idGK <= " & _GKs(1).Key
                        ' --> alle bis auf letzte GK löschen
                        For i = 0 To _GKs.Count - 2
                            _GKs.RemoveAt(0)
                        Next
                    ElseIf _AKs.Count > 1 OrElse CInt(row!AK) <> CInt(tmp(cnt)("AK")) Then
                        ' mehrere AKs oder nächste Gruppe mit anderer AK
                        If IsDBNull(row!TN) Then
                            ' --> ab GK(0)
                            _GK = String.Concat(" ab ", _GKs(0).Value)
                        Else
                            _GK = String.Concat(" ", _GKs(0).Value)
                        End If
                        ' --> an erste AK anhängen
                        If _AKs.Count > 1 Then
                            _AK = _AK.Insert(_AK.IndexOf(","), _GK)
                            _Filter = _Filter.Insert(_Filter.IndexOf("OR"), "AND idGK >= " & _GKs(0).Key & " ")
                        Else
                            _AK += _GK
                            _Filter += " AND idGK >= " & _GKs(0).Key
                        End If
                        _GKs.Clear()
                    End If
                End If
                ' --> TN der Gruppe selektieren und Nummer zuweisen
                Dim dv = New DataView(dtMeldung)
                dv.RowFilter = "Sex = '" & _sx & "' AND (" & _Filter & ")"
                dv.Sort = "Losnummer DESC, Zweikampf DESC"
                If Not IsDBNull(row!TN) Then
                    ' A/B-Gruppen
                    TN_Count = 0
                    For i = 0 To CInt(row!TN) - 1
                        Try
                            dv(i + _TN)("Gruppe") = Group
                            TN_Count += 1
                        Catch ex As Exception
                        End Try
                    Next
                    If CInt(row!Parts) = PartCode Then
                        _TN = 0
                    Else
                        _TN += CInt(row!TN)
                    End If
                Else
                    For Each tn As DataRowView In dv
                        tn!Gruppe = Group
                    Next
                    TN_Count = dv.Count
                End If
                ' --> Gruppe zu dgv hinzufügen
                dgvGruppen.Rows.Add({Group, Chr(CInt(row!Parts)), String.Empty, _AK, TN_Count, Format(Glob.dtWK(0)("Datum"), "dd.MM.yyyy"), Format(Glob.dtWK(0)("Datum"), "dd.MM.yyyy"), DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, Format(Glob.dtWK(0)("Datum"), "dd.MM.yyyy"), DBNull.Value, 1, False})
                Refresh()
                ' --> Gruppe hochzählen und alles zurücksetzen
                Group += 1
                _AKs.Clear()
            End If
        Next
        dgvGruppen.Columns("Teilgruppe").Visible = ShowParts
        dgvGruppen.Focus()
        dgvUnallocated.ClearSelection()
        Color_Rows()
    End Sub
    Private Sub Create_GruppenGK()
        Refresh()
        Dim ShowParts = False
        Dim TN_Count = 0

        Dim tbl = New DataTable
        tbl.Columns.Add(New DataColumn With {.ColumnName = "Gruppe", .DataType = GetType(Integer)})
        tbl.Columns.Add(New DataColumn With {.ColumnName = "Parts", .DataType = GetType(Integer)})
        tbl.Columns.Add(New DataColumn With {.ColumnName = "Sex", .DataType = GetType(String)})
        tbl.Columns.Add(New DataColumn With {.ColumnName = "AK", .DataType = GetType(Integer)})
        tbl.Columns.Add(New DataColumn With {.ColumnName = "GK", .DataType = GetType(Integer)})
        tbl.Columns.Add(New DataColumn With {.ColumnName = "TN", .DataType = GetType(Integer), .DefaultValue = DBNull.Value, .AllowDBNull = True})

        Dim Group = 1
        Dim PartCode = 65 ' ASCII-Code für "A"

        For s = 1 To 0 Step -1 ' weiblich --> männlich
            Dim ss = s
            Dim GKs = From g In dtMeldung
                      Where g.Field(Of String)("Sex") = sx(ss)
                      Order By g.Field(Of Integer)("idGK")
                      Group g By GK = g.Field(Of Integer)("idGK") Into TN = Group

            ' GKs(Ix).TN.Count = Heber in der jeweiligen GK
            ' GKs(Ix).GK       = idGK der jeweiligen GK

            For g = 0 To GKs.Count - 1
                ' jede GK durchlaufen
                If TN_Count + GKs(g).TN.Count > maxCount Then
                    ' GK zu groß für aktuelle Gruppe
                    If GKs(g).TN.Count > maxCount Then
                        ' GK zu groß für eine Gruppe --> A/B-Gruppen anlegen
                        Dim Parts_Count = CInt(Ceiling(GKs(g).TN.Count / maxCount))
                        Dim Parts_TN = Floor(GKs(g).TN.Count / Parts_Count) ' rundet ab --> der Rest muss verteilt werden
                        Dim Parts_Rest = GKs(g).TN.Count Mod Parts_Count
                        ' wenn die Gruppe bereits existiert, neue Gruppe beginnen
                        If tbl.Rows.Count > 0 AndAlso CInt(tbl(tbl.Rows.Count - 1)("Gruppe")) = Group Then Group += 1
                        For i = Parts_Count - 1 To 0 Step -1
                            tbl.Rows.Add({Group, PartCode + i, sx(s), GKs(g).TN(i)!idAK, GKs(g).GK, Parts_TN + If(Parts_Rest > 0, 1, 0)}) ' A-Gruppe hat immer am wenigsten TNs
                            If Parts_Rest > 0 Then Parts_Rest -= 1
                            Group += 1
                        Next
                        TN_Count = 0
                        ShowParts = True
                    Else
                        ' neue Gruppe mit aktueller GK beginnen
                        Group += 1
                        TN_Count = GKs(g).TN.Count
                        tbl.Rows.Add({Group, PartCode, sx(s), GKs(g).TN(0)!idAK, GKs(g).GK})
                    End If
                Else
                    ' GK passt in Gruppe
                    TN_Count += GKs(g).TN.Count
                    tbl.Rows.Add({Group, PartCode, sx(s), GKs(g).TN(0)!idAK, GKs(g).GK})
                End If
            Next
            Group += 1
        Next

        Dim Groups = From x In tbl
                     Group x By GroupNumber = x.Field(Of Integer)("Gruppe") Into GKs = Group

        Dim dv = New DataView(dtMeldung)
        dv.Sort = "Losnummer DESC, Zweikampf DESC"
        Dim tmpTN = 0

        For i = 0 To Groups.Count - 1
            Group = CInt(Groups(i).GKs(0)!Gruppe)
            Dim _Filter = String.Empty
            Dim _Name = Groups(i).GKs(0)!Sex.ToString + " "
            For z = 0 To Groups(i).GKs.Count - 1
                _Filter += "idGK = " + Groups(i).GKs(z)!GK.ToString
                '_Name += dicGKs_BySex_ByAK(Groups(i).GKs(z)!Sex.ToString)(CInt(Groups(i).GKs(z)!AK))(CInt(Groups(i).GKs(z)!GK)) '+ " " + Chr(CInt(Groups(i).GKs(z)!Parts))
                Dim _gk = Glob.dvGK.Find({Groups(i).GKs(z)!AK, Groups(i).GKs(z)!Sex, Groups(i).GKs(z)!GK})
                _Name += Glob.dvGK(_gk)!GK.ToString
                If z + 1 < Groups(i).GKs.Count Then
                    _Filter += " OR "
                    _Name += ", "
                End If
            Next
            ' --> TN der Gruppe selektieren und Nummer zuweisen
            dv.RowFilter = "Sex = '" & Groups(i).GKs(0)!Sex.ToString & "' AND (" & _Filter & ")"
            If Not IsDBNull(Groups(i).GKs(0)!TN) Then
                ' A/B-Gruppen
                TN_Count = CInt(Groups(i).GKs(0)!TN)
                For c = 0 To TN_Count - 1
                    Try
                        dv(c + tmpTN)("Gruppe") = Group
                    Catch ex As Exception
                    End Try
                Next
                If CInt(Groups(i).GKs(0)!Parts) = PartCode Then
                    tmpTN = 0
                Else
                    tmpTN += CInt(Groups(i).GKs(0)!TN)
                End If
            Else
                For Each row As DataRowView In dv
                    row!Gruppe = Group
                Next
                TN_Count = dv.Count
            End If
            ' --> Gruppe zu dgv hinzufügen
            dgvGruppen.Rows.Add({Group, Chr(CInt(Groups(i).GKs(0)!Parts)), String.Empty, _Name, TN_Count, Format(Glob.dtWK(0)("Datum"), "dd.MM.yyyy"), Format(Glob.dtWK(0)("Datum"), "dd.MM.yyyy"), DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, Format(Glob.dtWK(0)("Datum"), "dd.MM.yyyy"), DBNull.Value, 1, False})
            Refresh()
        Next

        dgvGruppen.Columns("Teilgruppe").Visible = ShowParts
        dgvGruppen.Focus()
        dgvUnallocated.ClearSelection()
        Color_Rows()
    End Sub
    Private Function GetAK(AK As String, Optional Sex As String = Nothing) As String
        ' "Masters" entfernen: Masters(AK) --> AK
        ' wenn Sex vorhanden --> formatieren: sex(AK)
        If AK.Contains("(") Then AK = Val((Split(AK, "(")(1))).ToString
        If Not IsNothing(Sex) Then Return String.Concat(Sex, "(", AK, ")")
        Return AK
    End Function


    Private Sub RemoveLifterFromGroup()
        Dim rows As DataGridViewSelectedRowCollection = dgvMeldung.SelectedRows
        Using New Centered_MessageBox(Me)
            If MessageBox.Show("Ausgewählte Teilnehmer werden aus der Gruppe entfernt.", msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) = DialogResult.OK Then
                For Each row As DataGridViewRow In rows
                    Dim pos = bsM.Find("Teilnehmer", row.Cells("Teilnehmer").Value)
                    dvM(pos)!Gruppe = DBNull.Value
                Next
                bsM.EndEdit()
                Update_Teilnehmerzahl()
                mnuSpeichern.Enabled = True
            End If
        End Using
    End Sub



    Private Sub Update_Startnummern()

        Dim lstWGs As New SortedList(Of Integer, Object)
        For Each row As DataGridViewRow In dgvGruppen.Rows
            lstWGs.Add(CInt(row.Cells("Gruppe").Value), row.Cells("WG").Value)
        Next

        Dim grps = From x In lstWGs
                   Group By x.Value Into Group

        Dim filter = String.Empty
        For Each kvp In grps
            If IsDBNull(kvp.Value) Then
                ' Startnummern pro Gruppe
                For Each key In kvp.Group
                    filter = "Gruppe = " & key.Key
                    Write_Startnummern(filter)
                Next
            Else
                ' Startnummern pro WG
                Dim lst As New List(Of String)
                For Each key In kvp.Group
                    lst.Add(key.Key.ToString)
                Next
                filter = "Gruppe = " & Join(lst.ToArray, " OR Gruppe = ")
                Write_Startnummern(filter)
            End If
        Next
    End Sub
    Private Sub Write_Startnummern(Filter As String)
        Dim dv = New DataView(dtMeldung)
        dv.Sort = "Losnummer"
        dv.RowFilter = Filter

        Dim Startnummer = 1
        For Each row As DataRowView In dv
            If Not IsDBNull(row!attend) AndAlso CBool(row!attend) AndAlso Not IsDBNull(row!Losnummer) Then
                row!Startnummer = Startnummer
                Startnummer += 1
            Else
                row!Startnummer = DBNull.Value
            End If
        Next
    End Sub
    Private Sub Save()
        Cursor = Cursors.WaitCursor
        Validate()
        dgvGruppen.EndEdit()
        bsM.EndEdit()
        bsU.EndEdit()

        If Wettkampf.Gewichtseinteilung.Equals("Gewichtsgruppen") Then
            If Gleiche_Gewichte_korrigieren() Then Return
        End If

        'Update_Startnummern() ' wo ist das sinnvoll?

        Dim GroupQuery = "INSERT INTO gruppen (Wettkampf, Gruppe, Teilgruppe, WG, Einteilung, Bezeichnung, Datum, DatumW, WiegenBeginn, WiegenEnde, Reissen, Stossen, DatumA, Athletik, Blockheben, Bohle) " &
                          "VALUES(@wk, @gr, @tgr, @wg, @grp, @bez, @date, @dateW, @wiegen_start, @wiegen_ende, @reissen, @stossen, @dateA, @athletik, @bh, @bohle) " &
                          "ON DUPLICATE KEY UPDATE Teilgruppe = @tgr, WG = @wg, Einteilung = @grp, Bezeichnung = @bez, Datum = @date, DatumW = @dateW, WiegenBeginn = @wiegen_start, WiegenEnde = @wiegen_ende, Reissen = @reissen, Stossen = @stossen, DatumA = @dateA, Athletik = @athletik, Blockheben = @bh, Bohle = @bohle;"

        Using conn As New MySqlConnection(User.ConnString)
            ' für jeden ausgewählten WK speichern
            For Each WK In Wettkampf.Identities.Keys
                ' Gruppen speichern
                For i = 0 To dgvGruppen.Rows.Count - 1
                    cmd = New MySqlCommand(GroupQuery, conn)
                    With cmd.Parameters
                        .AddWithValue("@wk", WK)
                        .AddWithValue("@gr", dgvGruppen("Gruppe", i).Value)
                        .AddWithValue("@tgr", dgvGruppen("Teilgruppe", i).Value)
                        .AddWithValue("@wg", IIf(String.IsNullOrEmpty(dgvGruppen("WG", i).Value.ToString), DBNull.Value, dgvGruppen("WG", i).Value))
                        .AddWithValue("@grp", dgvGruppen("Einteilung", i).Value)
                        .AddWithValue("@bez", dgvGruppen("Bezeichnung", i).Value)
                        .AddWithValue("@date", Get_DateTime(i, dgvGruppen("Datum", i).Value, DBNull.Value))
                        .AddWithValue("@dateW", Get_DateTime(i, dgvGruppen("DatumW", i).Value, DBNull.Value))
                        .AddWithValue("@dateA", Get_DateTime(i, dgvGruppen("DatumA", i).Value, DBNull.Value))
                        .AddWithValue("@wiegen_start", Get_DateTime(i, dgvGruppen("DatumW", i).Value, dgvGruppen("WiegenBeginn", i).Value, "DatumW"))
                        .AddWithValue("@wiegen_ende", Get_DateTime(i, dgvGruppen("DatumW", i).Value, dgvGruppen("WiegenEnde", i).Value, "DatumW"))
                        .AddWithValue("@reissen", Get_DateTime(i, dgvGruppen("Datum", i).Value, dgvGruppen("Reissen", i).Value, "Datum"))
                        .AddWithValue("@stossen", Get_DateTime(i, dgvGruppen("Datum", i).Value, dgvGruppen("Stossen", i).Value, "Datum"))
                        .AddWithValue("@athletik", Get_DateTime(i, dgvGruppen("DatumA", i).Value, dgvGruppen("Athletik", i).Value, "DatumA"))
                        .AddWithValue("@bh", dgvGruppen("Blockheben", i).Value)
                        .AddWithValue("@bohle", dgvGruppen("Bohle", i).Value)
                    End With
                    Try
                        If Not conn.State = ConnectionState.Open Then conn.Open()
                        cmd.ExecuteNonQuery()
                        ConnError = False
                    Catch ex As MySqlException
                        MySQl_Error(Me, ex.Message, ex.StackTrace)
                    End Try
                    ' Kampfrichter für jede Gruppe anlegen
                    If Not ConnError Then
                        cmd = New MySqlCommand("INSERT IGNORE gruppen_kampfrichter (Wettkampf, Gruppe) VALUES (@wk, @gr);", conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", WK)
                            .AddWithValue("@gr", dgvGruppen("Gruppe", i).Value)
                        End With
                        Try
                            If Not conn.State = ConnectionState.Open Then conn.Open()
                            cmd.ExecuteNonQuery()
                            ConnError = False
                        Catch ex As Exception
                            MySQl_Error(Me, ex.Message, ex.StackTrace)
                        End Try
                    End If
                Next
                ' gelöschte Gruppen in DB löschen
                If Not ConnError Then
                    Query = "DELETE FROM gruppen WHERE Wettkampf = @wk AND Gruppe > @gr;" &
                            "DELETE FROM gruppen_kampfrichter WHERE Wettkampf = @wk AND Gruppe > @gr;"
                    cmd = New MySqlCommand(Query, conn)
                    With cmd.Parameters
                        .AddWithValue("@wk", WK)
                        .AddWithValue("@gr", dgvGruppen.Rows.Count)
                    End With
                    If Not conn.State = ConnectionState.Open Then conn.Open()
                    Try
                        If Not conn.State = ConnectionState.Open Then conn.Open()
                        cmd.ExecuteNonQuery()
                        ConnError = False
                    Catch ex As Exception
                        MySQl_Error(Me, ex.Message, ex.StackTrace)
                    End Try
                End If
            Next
            If Not ConnError Then
                ' Meldung mit Gruppe und Startnummer aktualisieren
                Query = "UPDATE meldung SET Gruppe = @gr, Startnummer = @st WHERE " & Wettkampf.WhereClause() & " AND Teilnehmer = @tn;"
                For Each row As DataRow In dtMeldung.Rows
                    cmd = New MySqlCommand(Query, conn)
                    With cmd.Parameters
                        '.AddWithValue("@wk", Wettkampf.ID)
                        .AddWithValue("@gr", row!Gruppe)
                        .AddWithValue("@st", row!Startnummer)
                        .AddWithValue("@tn", row!Teilnehmer)
                    End With
                    Try
                        If Not conn.State = ConnectionState.Open Then conn.Open()
                        cmd.ExecuteNonQuery()
                        ConnError = False
                    Catch ex As Exception
                        MySQl_Error(Me, ex.Message, ex.StackTrace)
                    End Try
                Next
                ' MultiBohle setzen
                Using DS As New DataService
                    Wettkampf.MultiBohle = DS.Get_MultiBohle(conn)
                End Using
            End If
        End Using

        If Not ConnError Then
            mnuSpeichern.Enabled = False

            Using OS As New OnlineService
                If Not AppConnState = ConnState.Connected Then
                    AppConnState = OS.Check_Wettkampf()
                End If
                If AppConnState = ConnState.Connected Then
                    Using conn As New MySqlConnection(User.APP_ConnString)
                        conn.Open()
                        OS.UpdateGruppen(conn)
                        OS.UpdateMeldung(conn)
                    End Using
                End If
            End Using

            Change_LeaderGruppe() ' Änderungen an FormLeader weitergeben
            If Not IsNothing(formLeader) AndAlso Not formLeader.IsDisposed Then Close() ' wenn frmLeader offen ist, Me schließen
        End If
        Cursor = Cursors.Default
    End Sub
    Private Function Get_DateTime(RowIndex As Integer, DatePart As Object, TimePart As Object, Optional Column As String = "") As Object
        Try
            If Not String.IsNullOrEmpty(Column) Then
                If IsDBNull(TimePart) OrElse IsNothing(TimePart) Then Return DBNull.Value
                If IsDBNull(DatePart) Then
                    If Not IsDBNull(dgvGruppen(Column, RowIndex).Value) Then
                        DatePart = dgvGruppen(Column, RowIndex).Value
                    ElseIf Not IsDBNull(dgvGruppen("Datum", RowIndex).Value) Then
                        DatePart = dgvGruppen("Datum", RowIndex).Value
                    Else
                        DatePart = Now.Date
                    End If
                End If
                Dim parts = Split(TimePart.ToString, " ")
                Dim time = Split(parts(1), ":")
                time(2) = "00"
                Return CDate(String.Concat(parts(0), " ", Join(time, ":")))
            Else
                If IsDBNull(DatePart) Then Return DBNull.Value
                Return CDate(DatePart)
            End If
        Catch ex As Exception
            Return DBNull.Value
        End Try
    End Function
    Private Function Select_Groups(groups As Integer) As Integer()
        Select Case groups
            Case 1
                Return {2}
            Case 2
                Return {1, 4}
            Case 3
                Return {1, 2, 4}
            Case 4
                Return {1, 2, 3, 4}
            Case Else
                Return {0, 1, 2, 3, 4}
        End Select
    End Function
    Private Sub Setup_Grid(Grid As DataGridView)
        With Grid
            .AutoGenerateColumns = False
            .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
            .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor
        End With
    End Sub

    ' Buttons
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        With dgvGruppen
            .Rows.Add({ .Rows.Count + 1, "", "", "", "", DBNull.Value, Format(Glob.dtWK(0)("Datum"), "dd.MM.yyyy")})
            .CurrentCell = dgvGruppen(4, .Rows.Count - 1)
            .CurrentRow.Cells("DatumW").Value = Format(Glob.dtWK(0)("Datum"), "dd.MM.yyyy")
            .CurrentRow.Cells("DatumA").Value = Format(Glob.dtWK(0)("Datum"), "dd.MM.yyyy")
            .CurrentRow.Cells("Bohle").Value = 1
            .CurrentRow.Cells("Blockheben").Value = Wettkampf.Mannschaft
            mnuSpeichern.Enabled = True
            btnRemove.Enabled = True
            btnUp.Enabled = .CurrentRow.Index > 0
            btnDown.Enabled = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .CurrentRow.Selected = True
            .Focus()
        End With
    End Sub
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Close()
    End Sub
    Private Sub btnClose_EnabledChanged(sender As Object, e As EventArgs) Handles btnClose.EnabledChanged
        mnuBeenden.Enabled = btnClose.Enabled
    End Sub
    Private Sub btnMove_Click(sender As Object, e As EventArgs) Handles btnDown.Click, btnUp.Click
        With dgvGruppen
            DragIndex = .CurrentRow.Index
            DropIndex = DragIndex + CInt(CType(sender, Button).Tag)
            Dim DraggedRow = .Rows(DragIndex)

            .Rows.RemoveAt(DragIndex)
            .Rows.Insert(DropIndex, DraggedRow)

            Update_Gruppen(DragIndex, DropIndex)
        End With

        Color_Rows()
        DragIndex = -1
        DropIndex = -1
    End Sub
    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        Dim group = CInt(dgvGruppen.CurrentRow.Cells("Gruppe").Value)

        Using New Centered_MessageBox(Me)
            If MessageBox.Show("Gruppe " & group & " wird gelöscht.",
                                msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) = DialogResult.Cancel Then Return
        End Using

        ' TN aus Gruppe entfernen
        bsM.Filter = "Gruppe = " & group & ShowAll(chkShowAll.Checked)
        For Each r As DataRowView In dvM
            r!Gruppe = DBNull.Value
        Next
        bsM.EndEdit()

        dgvGruppen.Rows.RemoveAt(dgvGruppen.CurrentRow.Index)

        Update_Gruppen(dgvGruppen.CurrentRow.Index, -1)
    End Sub
    Private Sub btnMoveLeft_Click(sender As Object, e As EventArgs) Handles btnMoveLeft.Click

        Unallocated_Flag = "u"
        DropIndex = CInt(dgvGruppen.CurrentRow.Cells("Gruppe").Value) - 1

        Update_Meldungen(dgvUnallocated.SelectedRows)
        Update_Teilnehmerzahl()

        If dgvUnallocated.Rows.Count = 0 Then
            optZugewiesen.Checked = True
        Else
            dgvUnallocated.Focus()
        End If

        DropIndex = -1
        mnuSpeichern.Enabled = True
    End Sub

    '' pnlOptionen
    Private Sub btnApply_Click(sender As Object, e As EventArgs) Handles btnApply.Click
        pnlOptionen.Visible = False
        If nudMaxCount.Value <> maxCount Then
            maxCount = CInt(nudMaxCount.Value)
            dgvGruppen.Rows.Clear()
            'If Wettkampf.Modus < 500 Then
            '    Create_Gruppen400()
            'ElseIf Wettkampf.Modus < 600 Then
            '    Create_Gruppen500()
            'End If
        End If
    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnExit.Click, btnCancel.Click
        pnlOptionen.Visible = False
    End Sub
    Private Sub Optionen_ValueChanged(sender As Object, e As EventArgs) Handles optGK_Keep.CheckedChanged, optGK_Group.CheckedChanged, optAK_Keep.CheckedChanged, optAK_Group.CheckedChanged, nudMaxCount.ValueChanged

        btnApply.Enabled = True
    End Sub

    Dim pnlPos As Point = New Point(425, 170)
    Private Sub Panel_Headers_MouseDown(sender As Object, e As MouseEventArgs) Handles lblWG.MouseDown
        If e.Button = MouseButtons.Left Then
            pnlWG.BorderStyle = BorderStyle.None
            pnlPos = New Point(e.X, e.Y)
            lblWG.Cursor = Cursors.SizeAll
        End If
    End Sub
    Private Sub Panel_Headers_MouseMove(sender As Object, e As MouseEventArgs) Handles lblWG.MouseMove
        If (e.Button And MouseButtons.Left) = MouseButtons.Left And pnlWG.BorderStyle = BorderStyle.None Then

            Dim loc = New Point(pnlWG.Location.X + (e.X - pnlPos.X), pnlWG.Location.Y + (e.Y - pnlPos.Y))

            If loc.X < 0 Then loc.X = 0
            If loc.X + pnlWG.Width > Width - 18 Then loc.X = Width - 18 - pnlWG.Width
            If loc.Y < 0 Then loc.Y = 0
            If loc.Y + pnlWG.Height > Height - MenuStrip1.Height - 18 Then loc.Y = Height - MenuStrip1.Height - 18 - pnlWG.Height

            pnlWG.Location = loc
        End If
    End Sub
    Private Sub Panel_Headers_MouseUp(sender As Object, e As MouseEventArgs) Handles lblWG.MouseUp
        If e.Button = MouseButtons.Left And pnlOptionen.BorderStyle = BorderStyle.None Then
            pnlOptionen.BorderStyle = BorderStyle.FixedSingle
            pnlPos = New Point(pnlWG.Left, pnlWG.Top)
            lblWG.Cursor = Cursors.Default
        End If
    End Sub

    '' Menüs
    Private Sub mnuSpeichern_Click(sender As Object, e As EventArgs) Handles mnuSpeichern.Click, btnSave.Click
        Save()
    End Sub
    Private Sub mnuSpeichern_EnabledChanged(sender As Object, e As EventArgs) Handles mnuSpeichern.EnabledChanged
        btnSave.Enabled = mnuSpeichern.Enabled
    End Sub

    Private Sub mnuDrucken_Click(sender As Object, e As EventArgs) Handles mnuDrucken.Click
        Cursor = Cursors.WaitCursor

        Dim Caption = "Gruppenlisten"
        Try
            With Drucken_Gruppenliste
                .Text = UCase(Caption) & " DRUCKEN  - " + Wettkampf.Bezeichnung
                .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
                .Tag = Replace(Caption, "listen", "liste")
                .ShowDialog(Me)
                .Dispose()
            End With
        Catch ex As Exception
            LogMessage("Gruppen: Drucken: " & ex.Message, ex.StackTrace)
        End Try

        Cursor = DefaultCursor
    End Sub
    Private Sub mnuKampfgericht_Click(sender As Object, e As EventArgs) Handles mnuKampfgericht.Click
        Cursor = Cursors.WaitCursor
        With frmKampfgericht
            .Text = "Kampfgericht - " & Wettkampf.Bezeichnung
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuOptionen_Click(sender As Object, e As EventArgs) Handles mnuOptionen.Click
        'nudMaxCount.Value = maxCount
        'btnApply.Enabled = False
        'pnlOptionen.Location = pnlPos
        'pnlOptionen.Visible = True
    End Sub
    Private Sub mnuExcelExport_Click(sender As Object, e As EventArgs) Handles mnuExcelExport.Click

        Dim Divider = ";"
        Dim oWrite As StreamWriter
        Dim _File = String.Empty
        With SaveFileDialog1
            .Filter = "CSV-Datei (*.csv)|*.csv|Alle Dateien (*.*)|*.*"
            .InitialDirectory = Path.Combine(Application.StartupPath, "Export")
            If .ShowDialog() = DialogResult.Cancel Then Return
            _File = .FileName
        End With

        If Not _File.Contains(".csv") Then _File += ".csv"

        oWrite = File.CreateText(_File)

        Dim CSVHeader As New StringBuilder
        For Each c As DataGridViewColumn In dgvGruppen.Columns
            CSVHeader.Append(c.HeaderText.ToString() & Divider)
        Next

        oWrite.WriteLine(CSVHeader.ToString())
        oWrite.Flush()

        For r As Integer = 0 To dgvGruppen.Rows.Count - 1
            Dim CSVLine As New StringBuilder
            Dim s = String.Empty
            For c As Integer = 0 To dgvGruppen.Columns.Count - 1
                s = s & dgvGruppen.Rows(r).Cells(c).Value.ToString() & Divider
            Next
            oWrite.WriteLine(s)
            oWrite.Flush()
        Next

        oWrite.Close()
        oWrite = Nothing
        GC.Collect()

    End Sub
    Private Sub mnuTeilen_Click(sender As Object, e As EventArgs) Handles cmnuTeilen.Click
        ' Gruppe ab <Gewicht> teilen

        Dim Gewicht = String.Empty
        Dim pos = Glob.dvGewicht.Find(dgvGruppen.CurrentRow.Cells("Gruppe").Value)

        Do
            Using InputBox As New Custom_InputBox("Ab diesem Gewicht wird die Gruppe geteilt",
                                                  msgCaption,
                                                  If(pos > -1, Format(Glob.dvGewicht(pos)("Gewicht").ToString, "0.00"), String.Empty))
                Gewicht = InputBox.Response
                If IsNothing(Gewicht) OrElse String.IsNullOrEmpty(Gewicht) Then Return
            End Using
            'Gewicht = InputBox("Ab diesem Gewicht wird die Gruppe geteilt", msgCaption, If(pos > -1, Format(Glob.dvGewicht(pos)("Gewicht").ToString, "0.00"), String.Empty))
            'If String.IsNullOrEmpty(Gewicht) Then Return
        Loop While Not IsNumeric(Gewicht)

        Using conn As New MySqlConnection(User.ConnString)
            If conn.State = ConnectionState.Closed Then conn.Open()
            Dim Query = "INSERT INTO wettkampf_gewichtsteiler (Wettkampf, Gruppe, Gewicht) VALUES (@wk, @gr, @gewicht);"
            Dim cmd = New MySqlCommand(Query, conn)
            With cmd.Parameters
                .AddWithValue("@wk", Wettkampf.ID)
                .AddWithValue("@gr", dgvGruppen.CurrentRow.Cells("Gruppe").Value)
                .AddWithValue("@gewicht", CDbl(Gewicht))
                Do
                    Try
                        cmd.ExecuteNonQuery()
                        ConnError = False
                    Catch ex As Exception
                        If LCase(ex.Message).Contains("duplicate entry") Then
                            Try
                                cmd.CommandText = "UPDATE wettkampf_gewichtsteiler SET Gewicht = @gewicht WHERE Wettkampf = @wk And Gruppe = @gr;"
                                cmd.ExecuteNonQuery()
                                ConnError = False
                            Catch ex1 As MySqlException
                                If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                            End Try
                        Else
                            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                        End If
                    End Try
                Loop While ConnError
            End With
        End Using

        If Not ConnError Then
            If pos = -1 Then
                Glob.dvGewicht.AddNew()
                pos = Glob.dvGewicht.Count - 1
            End If
            Glob.dvGewicht(pos)("Gruppe") = dgvGruppen.CurrentRow.Cells("Gruppe").Value
            Glob.dvGewicht(pos)("Gewicht") = CDbl(Gewicht)
        End If
    End Sub
    Private Sub mnuWG_Click(sender As Object, e As EventArgs) Handles mnuWG.Click, cmnuWG.Click
        Dim ctl As New List(Of ComboBox)
        ctl.AddRange({cboWG_1, cboWG_2})
        For i = 0 To 1
            ctl(i).Items.Clear()
            With ctl(i)
                ctl(i).Items.Add(String.Empty)
                For r = 0 To dgvGruppen.Rows.Count - 1
                    .Items.Add(" Gruppe " + dgvGruppen.Rows(r).Cells("Gruppe").Value.ToString)
                Next
            End With
        Next
        With dgvGruppen.CurrentRow
            If Not IsDBNull(.Cells("WG").Value) Then
                cboWG_1.SelectedIndex = CInt(.Cells("WG").Value)     ' immer die andere Gruppe
                cboWG_2.SelectedIndex = CInt(.Cells("Gruppe").Value) ' selektierte Gruppe
            Else
                cboWG_1.SelectedIndex = CInt(.Cells("Gruppe").Value)
            End If
        End With
        btnWG_Apply.Enabled = False
        pnlWG.Visible = True
    End Sub
    Private Sub mnuRemoveLifter_Click(sender As Object, e As EventArgs) Handles mnuRemoveLifter.Click
        RemoveLifterFromGroup()
    End Sub

    '' pnlWK
    Private Sub btnWG_Apply_Click(sender As Object, e As EventArgs) Handles btnWG_Apply.Click
        pnlWG.Visible = False
        With dgvGruppen
            If String.IsNullOrEmpty(cboWG_1.Text) OrElse String.IsNullOrEmpty(cboWG_2.Text) Then
                .Rows(CInt(.CurrentRow.Cells("WG").Value)).Cells("WG").Value = DBNull.Value
                .CurrentRow.Cells("WG").Value = DBNull.Value
            Else
                ' cboWG_1 = nicht selektierte Gruppe
                ' cboWG_2 = selektierte Gruppe
                Dim lst As New List(Of Integer)
                lst.AddRange({CInt(Split(Trim(cboWG_1.Text), " ")(1)), CInt(Split(Trim(cboWG_2.Text), " ")(1))})
                lst.Sort()
                .Rows(lst(0) - 1).Cells("WG").Value = lst(0)
                .Rows(lst(1) - 1).Cells("WG").Value = lst(0)
                '.CurrentRow.Cells("WG").Value = lst(0)
            End If
            Dim wg = False
            For i = 0 To .Rows.Count - 1
                wg = Not IsDBNull(.Rows(i).Cells("WG").Value)
                If wg Then Exit For
            Next
            .Columns("WG").Visible = wg
            .Focus()
        End With
        mnuSpeichern.Enabled = True
    End Sub
    Private Sub btnWG_Cancel_Click(sender As Object, e As EventArgs) Handles btnWG_Exit.Click, btnWG_Cancel.Click
        pnlWG.Visible = False
        dgvGruppen.Focus()
    End Sub

    Private Sub cboWG_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboWG_2.SelectedIndexChanged, cboWG_1.SelectedIndexChanged
        If CType(sender, ComboBox).Focused Then btnWG_Apply.Enabled = True
    End Sub

    Private Sub optZugewiesen_CheckedChanged(sender As Object, e As EventArgs) Handles optZugewiesen.CheckedChanged
        If optZugewiesen.Checked Then
            dgvMeldung.BringToFront()
            dgvMeldung.Focus()
        End If
    End Sub

    Private Sub optNichtZugewiesen_CheckedChanged(sender As Object, e As EventArgs) Handles optNichtZugewiesen.CheckedChanged
        'chkShowAll.Enabled = optNichtZugewiesen.Checked
        If optNichtZugewiesen.Checked Then
            dgvUnallocated.BringToFront()
            dgvUnallocated.Focus()
        End If
    End Sub

    Private Sub chkShowAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkShowAll.CheckedChanged
        'If chkShowAll.Checked Then
        '    bsU.Filter = "Gruppe = '1000' OR Convert(IsNull(Gruppe,''), System.String) = '' AND (Convert(IsNull(attend,''), System.String) = '' OR attend = 1)"
        'Else
        '    bsU.Filter = "Gruppe = '1000' OR Convert(IsNull(Gruppe,''), System.String) = ''"
        'End If
        bsU.Filter = "Gruppe = '1000' OR Convert(IsNull(Gruppe,''), System.String) = '' " & ShowAll(chkShowAll.Checked)
        bsM.Filter = "Gruppe = " & dgvGruppen.CurrentRow.Cells("Gruppe").Value.ToString & ShowAll(chkShowAll.Checked)
        dgvUnallocated.Focus()
    End Sub

End Class