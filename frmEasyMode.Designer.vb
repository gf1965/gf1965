﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmEasyMode
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEasyMode))
        Me.lblKR = New System.Windows.Forms.Label()
        Me.chkHantel = New System.Windows.Forms.CheckBox()
        Me.cboKR = New System.Windows.Forms.ComboBox()
        Me.chkTechnik = New System.Windows.Forms.CheckBox()
        Me.cboBohle = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.statusMsg = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.pnlAufruf = New System.Windows.Forms.Panel()
        Me.xAufruf_min = New System.Windows.Forms.TextBox()
        Me.xAufruf_pkt = New System.Windows.Forms.TextBox()
        Me.xAufruf_sek = New System.Windows.Forms.TextBox()
        Me.pnlWertung = New System.Windows.Forms.Panel()
        Me.xTechniknote = New System.Windows.Forms.TextBox()
        Me.xGültig3 = New System.Windows.Forms.TextBox()
        Me.xGültig2 = New System.Windows.Forms.TextBox()
        Me.xGültig1 = New System.Windows.Forms.TextBox()
        Me.btnAufruf = New System.Windows.Forms.Button()
        Me.StatusStrip1.SuspendLayout()
        Me.pnlAufruf.SuspendLayout()
        Me.pnlWertung.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblKR
        '
        Me.lblKR.AutoSize = True
        Me.lblKR.Location = New System.Drawing.Point(33, 55)
        Me.lblKR.Name = "lblKR"
        Me.lblKR.Size = New System.Drawing.Size(101, 13)
        Me.lblKR.TabIndex = 4
        Me.lblKR.Text = "Anzahl Kampfrichter"
        '
        'chkHantel
        '
        Me.chkHantel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkHantel.AutoSize = True
        Me.chkHantel.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkHantel.Location = New System.Drawing.Point(384, 52)
        Me.chkHantel.Name = "chkHantel"
        Me.chkHantel.Size = New System.Drawing.Size(108, 17)
        Me.chkHantel.TabIndex = 7
        Me.chkHantel.Text = "Hantel- Beladung"
        Me.chkHantel.UseVisualStyleBackColor = True
        '
        'cboKR
        '
        Me.cboKR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboKR.BackColor = System.Drawing.SystemColors.Window
        Me.cboKR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboKR.ForeColor = System.Drawing.Color.RoyalBlue
        Me.cboKR.FormattingEnabled = True
        Me.cboKR.Items.AddRange(New Object() {"1", "3"})
        Me.cboKR.Location = New System.Drawing.Point(140, 52)
        Me.cboKR.Name = "cboKR"
        Me.cboKR.Size = New System.Drawing.Size(34, 21)
        Me.cboKR.TabIndex = 5
        '
        'chkTechnik
        '
        Me.chkTechnik.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkTechnik.AutoSize = True
        Me.chkTechnik.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkTechnik.Location = New System.Drawing.Point(383, 29)
        Me.chkTechnik.Name = "chkTechnik"
        Me.chkTechnik.Size = New System.Drawing.Size(109, 17)
        Me.chkTechnik.TabIndex = 6
        Me.chkTechnik.Text = "Technik-Wertung"
        Me.chkTechnik.UseVisualStyleBackColor = True
        '
        'cboBohle
        '
        Me.cboBohle.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBohle.BackColor = System.Drawing.SystemColors.Window
        Me.cboBohle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBohle.FormattingEnabled = True
        Me.cboBohle.Items.AddRange(New Object() {"1", "2"})
        Me.cboBohle.Location = New System.Drawing.Point(140, 25)
        Me.cboBohle.Name = "cboBohle"
        Me.cboBohle.Size = New System.Drawing.Size(34, 21)
        Me.cboBohle.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(100, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "&Bohle"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.statusMsg})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 236)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(545, 22)
        Me.StatusStrip1.TabIndex = 54
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'statusMsg
        '
        Me.statusMsg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.statusMsg.ForeColor = System.Drawing.Color.Red
        Me.statusMsg.Name = "statusMsg"
        Me.statusMsg.Size = New System.Drawing.Size(130, 17)
        Me.statusMsg.Text = "WK-Anlage nicht bereit"
        Me.statusMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnStart
        '
        Me.btnStart.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStart.Enabled = False
        Me.btnStart.Location = New System.Drawing.Point(375, 94)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(118, 25)
        Me.btnStart.TabIndex = 1
        Me.btnStart.Text = "Wettkampf starten"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'pnlAufruf
        '
        Me.pnlAufruf.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnlAufruf.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnlAufruf.BackColor = System.Drawing.Color.Black
        Me.pnlAufruf.Controls.Add(Me.xAufruf_min)
        Me.pnlAufruf.Controls.Add(Me.xAufruf_pkt)
        Me.pnlAufruf.Controls.Add(Me.xAufruf_sek)
        Me.pnlAufruf.Location = New System.Drawing.Point(0, 142)
        Me.pnlAufruf.Name = "pnlAufruf"
        Me.pnlAufruf.Size = New System.Drawing.Size(188, 93)
        Me.pnlAufruf.TabIndex = 57
        '
        'xAufruf_min
        '
        Me.xAufruf_min.BackColor = System.Drawing.Color.Black
        Me.xAufruf_min.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xAufruf_min.Font = New System.Drawing.Font("Digital-7", 60.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xAufruf_min.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf_min.Location = New System.Drawing.Point(11, 6)
        Me.xAufruf_min.Name = "xAufruf_min"
        Me.xAufruf_min.Size = New System.Drawing.Size(74, 80)
        Me.xAufruf_min.TabIndex = 18
        Me.xAufruf_min.TabStop = False
        Me.xAufruf_min.Text = "0"
        Me.xAufruf_min.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.xAufruf_min.WordWrap = False
        '
        'xAufruf_pkt
        '
        Me.xAufruf_pkt.BackColor = System.Drawing.Color.Black
        Me.xAufruf_pkt.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xAufruf_pkt.Font = New System.Drawing.Font("Digital-7", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xAufruf_pkt.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf_pkt.Location = New System.Drawing.Point(77, 16)
        Me.xAufruf_pkt.Name = "xAufruf_pkt"
        Me.xAufruf_pkt.Size = New System.Drawing.Size(20, 53)
        Me.xAufruf_pkt.TabIndex = 19
        Me.xAufruf_pkt.TabStop = False
        Me.xAufruf_pkt.Text = ":"
        Me.xAufruf_pkt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.xAufruf_pkt.WordWrap = False
        '
        'xAufruf_sek
        '
        Me.xAufruf_sek.BackColor = System.Drawing.Color.Black
        Me.xAufruf_sek.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xAufruf_sek.Font = New System.Drawing.Font("Digital-7", 60.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xAufruf_sek.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf_sek.Location = New System.Drawing.Point(100, 6)
        Me.xAufruf_sek.Name = "xAufruf_sek"
        Me.xAufruf_sek.Size = New System.Drawing.Size(77, 80)
        Me.xAufruf_sek.TabIndex = 16
        Me.xAufruf_sek.TabStop = False
        Me.xAufruf_sek.Text = "00"
        Me.xAufruf_sek.WordWrap = False
        '
        'pnlWertung
        '
        Me.pnlWertung.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlWertung.BackColor = System.Drawing.Color.Black
        Me.pnlWertung.Controls.Add(Me.xTechniknote)
        Me.pnlWertung.Controls.Add(Me.xGültig3)
        Me.pnlWertung.Controls.Add(Me.xGültig2)
        Me.pnlWertung.Controls.Add(Me.xGültig1)
        Me.pnlWertung.Location = New System.Drawing.Point(180, 142)
        Me.pnlWertung.Name = "pnlWertung"
        Me.pnlWertung.Size = New System.Drawing.Size(366, 93)
        Me.pnlWertung.TabIndex = 56
        '
        'xTechniknote
        '
        Me.xTechniknote.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.xTechniknote.BackColor = System.Drawing.Color.Black
        Me.xTechniknote.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xTechniknote.Font = New System.Drawing.Font("DS-Digital", 60.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xTechniknote.ForeColor = System.Drawing.Color.Lime
        Me.xTechniknote.Location = New System.Drawing.Point(208, 7)
        Me.xTechniknote.Name = "xTechniknote"
        Me.xTechniknote.ReadOnly = True
        Me.xTechniknote.Size = New System.Drawing.Size(140, 80)
        Me.xTechniknote.TabIndex = 20
        Me.xTechniknote.TabStop = False
        Me.xTechniknote.Text = "X,XX"
        Me.xTechniknote.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.xTechniknote.Visible = False
        Me.xTechniknote.WordWrap = False
        '
        'xGültig3
        '
        Me.xGültig3.BackColor = System.Drawing.Color.Black
        Me.xGültig3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig3.Font = New System.Drawing.Font("Wingdings", 60.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.xGültig3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig3.Location = New System.Drawing.Point(132, 3)
        Me.xGültig3.Name = "xGültig3"
        Me.xGültig3.ReadOnly = True
        Me.xGültig3.Size = New System.Drawing.Size(61, 89)
        Me.xGültig3.TabIndex = 17
        Me.xGültig3.TabStop = False
        Me.xGültig3.Text = "l"
        Me.xGültig3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xGültig2
        '
        Me.xGültig2.BackColor = System.Drawing.Color.Black
        Me.xGültig2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig2.Font = New System.Drawing.Font("Wingdings", 60.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.xGültig2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig2.Location = New System.Drawing.Point(76, 3)
        Me.xGültig2.Name = "xGültig2"
        Me.xGültig2.ReadOnly = True
        Me.xGültig2.Size = New System.Drawing.Size(61, 89)
        Me.xGültig2.TabIndex = 16
        Me.xGültig2.TabStop = False
        Me.xGültig2.Text = "l"
        Me.xGültig2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xGültig1
        '
        Me.xGültig1.BackColor = System.Drawing.Color.Black
        Me.xGültig1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig1.Font = New System.Drawing.Font("Wingdings", 60.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.xGültig1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig1.Location = New System.Drawing.Point(20, 3)
        Me.xGültig1.Name = "xGültig1"
        Me.xGültig1.ReadOnly = True
        Me.xGültig1.Size = New System.Drawing.Size(61, 89)
        Me.xGültig1.TabIndex = 15
        Me.xGültig1.TabStop = False
        Me.xGültig1.Text = "l"
        Me.xGültig1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnAufruf
        '
        Me.btnAufruf.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAufruf.Location = New System.Drawing.Point(49, 94)
        Me.btnAufruf.Name = "btnAufruf"
        Me.btnAufruf.Size = New System.Drawing.Size(125, 25)
        Me.btnAufruf.TabIndex = 8
        Me.btnAufruf.Text = "Aufruf starten"
        Me.btnAufruf.UseVisualStyleBackColor = True
        Me.btnAufruf.Visible = False
        '
        'frmEasyMode
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(545, 258)
        Me.Controls.Add(Me.btnAufruf)
        Me.Controls.Add(Me.pnlAufruf)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.lblKR)
        Me.Controls.Add(Me.cboBohle)
        Me.Controls.Add(Me.chkHantel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboKR)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.chkTechnik)
        Me.Controls.Add(Me.pnlWertung)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEasyMode"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "GFHsoft Easy-Modus"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.pnlAufruf.ResumeLayout(False)
        Me.pnlAufruf.PerformLayout()
        Me.pnlWertung.ResumeLayout(False)
        Me.pnlWertung.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblKR As Label
    Friend WithEvents chkHantel As CheckBox
    Friend WithEvents cboKR As ComboBox
    Friend WithEvents chkTechnik As CheckBox
    Friend WithEvents cboBohle As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents statusMsg As ToolStripStatusLabel
    Friend WithEvents btnStart As Button
    Friend WithEvents pnlAufruf As Panel
    Friend WithEvents xAufruf_min As TextBox
    Friend WithEvents xAufruf_pkt As TextBox
    Friend WithEvents xAufruf_sek As TextBox
    Friend WithEvents pnlWertung As Panel
    Friend WithEvents xTechniknote As TextBox
    Friend WithEvents xGültig3 As TextBox
    Friend WithEvents xGültig2 As TextBox
    Friend WithEvents xGültig1 As TextBox
    Friend WithEvents btnAufruf As Button
End Class
