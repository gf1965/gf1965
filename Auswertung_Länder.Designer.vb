﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Auswertung_Länder
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboVorlage = New System.Windows.Forms.ComboBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnVorschau = New System.Windows.Forms.Button()
        Me.dgvGruppe = New System.Windows.Forms.DataGridView()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.dgvHeber = New System.Windows.Forms.DataGridView()
        Me.Gruppierung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Punkte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Platz = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vorname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Wertung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEdit = New System.Windows.Forms.Button()
        CType(Me.dgvGruppe, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnSearch.Location = New System.Drawing.Point(367, 386)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(24, 24)
        Me.btnSearch.TabIndex = 57
        Me.btnSearch.Text = "..."
        Me.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label6.Location = New System.Drawing.Point(22, 369)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 13)
        Me.Label6.TabIndex = 56
        Me.Label6.Text = "Druck-Vorlage"
        '
        'cboVorlage
        '
        Me.cboVorlage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboVorlage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVorlage.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboVorlage.FormattingEnabled = True
        Me.cboVorlage.Location = New System.Drawing.Point(19, 387)
        Me.cboVorlage.Name = "cboVorlage"
        Me.cboVorlage.Size = New System.Drawing.Size(342, 21)
        Me.cboVorlage.Sorted = True
        Me.cboVorlage.TabIndex = 55
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Location = New System.Drawing.Point(741, 87)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(100, 28)
        Me.btnCancel.TabIndex = 54
        Me.btnCancel.Text = "&Beenden"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnPrint.Location = New System.Drawing.Point(741, 53)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(100, 28)
        Me.btnPrint.TabIndex = 53
        Me.btnPrint.Tag = "Print"
        Me.btnPrint.Text = "&Drucken"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnVorschau
        '
        Me.btnVorschau.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVorschau.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnVorschau.Location = New System.Drawing.Point(741, 19)
        Me.btnVorschau.Name = "btnVorschau"
        Me.btnVorschau.Size = New System.Drawing.Size(100, 28)
        Me.btnVorschau.TabIndex = 52
        Me.btnVorschau.Tag = "Show"
        Me.btnVorschau.Text = "&Vorschau"
        Me.btnVorschau.UseVisualStyleBackColor = True
        '
        'dgvGruppe
        '
        Me.dgvGruppe.AllowUserToAddRows = False
        Me.dgvGruppe.AllowUserToDeleteRows = False
        Me.dgvGruppe.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgvGruppe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvGruppe.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGruppe.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.dgvGruppe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGruppe.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Gruppierung, Me.Punkte, Me.Platz})
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGruppe.DefaultCellStyle = DataGridViewCellStyle21
        Me.dgvGruppe.Location = New System.Drawing.Point(19, 19)
        Me.dgvGruppe.Name = "dgvGruppe"
        Me.dgvGruppe.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvGruppe.RowHeadersVisible = False
        Me.dgvGruppe.RowTemplate.Height = 21
        Me.dgvGruppe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGruppe.Size = New System.Drawing.Size(338, 326)
        Me.dgvGruppe.TabIndex = 50
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Title = "Druckvorlagen suchen"
        '
        'dgvHeber
        '
        Me.dgvHeber.AllowUserToAddRows = False
        Me.dgvHeber.AllowUserToDeleteRows = False
        Me.dgvHeber.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvHeber.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvHeber.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.dgvHeber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHeber.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.Vorname, Me.Wertung, Me.DataGridViewTextBoxColumn2})
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.DefaultCellStyle = DataGridViewCellStyle24
        Me.dgvHeber.Location = New System.Drawing.Point(363, 19)
        Me.dgvHeber.Name = "dgvHeber"
        Me.dgvHeber.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvHeber.RowHeadersVisible = False
        Me.dgvHeber.RowTemplate.Height = 21
        Me.dgvHeber.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHeber.Size = New System.Drawing.Size(356, 326)
        Me.dgvHeber.TabIndex = 58
        '
        'Gruppierung
        '
        Me.Gruppierung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Gruppierung.DataPropertyName = "Vereinsname"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.NullValue = Nothing
        Me.Gruppierung.DefaultCellStyle = DataGridViewCellStyle18
        Me.Gruppierung.HeaderText = "Verein/LFV"
        Me.Gruppierung.Name = "Gruppierung"
        Me.Gruppierung.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Punkte
        '
        Me.Punkte.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Punkte.DataPropertyName = "Gesamt"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle19.Format = "N2"
        DataGridViewCellStyle19.NullValue = Nothing
        DataGridViewCellStyle19.Padding = New System.Windows.Forms.Padding(0, 0, 2, 0)
        Me.Punkte.DefaultCellStyle = DataGridViewCellStyle19
        Me.Punkte.HeaderText = "Punkte"
        Me.Punkte.Name = "Punkte"
        Me.Punkte.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Punkte.Width = 70
        '
        'Platz
        '
        Me.Platz.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Platz.DataPropertyName = "Platz"
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle20.Format = "N0"
        DataGridViewCellStyle20.NullValue = Nothing
        Me.Platz.DefaultCellStyle = DataGridViewCellStyle20
        Me.Platz.HeaderText = "Platz"
        Me.Platz.Name = "Platz"
        Me.Platz.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Platz.Width = 40
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Nachname"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Nachname"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Vorname
        '
        Me.Vorname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Vorname.DataPropertyName = "Vorname"
        Me.Vorname.HeaderText = "Vorname"
        Me.Vorname.Name = "Vorname"
        Me.Vorname.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Wertung
        '
        Me.Wertung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Wertung.DataPropertyName = "Wertung"
        Me.Wertung.HeaderText = "Wertung"
        Me.Wertung.Name = "Wertung"
        Me.Wertung.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Punkte"
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle23.Format = "N2"
        DataGridViewCellStyle23.NullValue = Nothing
        DataGridViewCellStyle23.Padding = New System.Windows.Forms.Padding(0, 0, 2, 0)
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle23
        Me.DataGridViewTextBoxColumn2.HeaderText = "Punkte"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 40
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnEdit.Location = New System.Drawing.Point(397, 386)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(90, 24)
        Me.btnEdit.TabIndex = 59
        Me.btnEdit.Tag = "Edit"
        Me.btnEdit.Text = "Bearbeiten"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'Auswertung_Länder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(860, 424)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.dgvHeber)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cboVorlage)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnVorschau)
        Me.Controls.Add(Me.dgvGruppe)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Auswertung_Länder"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Länderwertung"
        CType(Me.dgvGruppe, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnSearch As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents cboVorlage As ComboBox
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnPrint As Button
    Friend WithEvents btnVorschau As Button
    Friend WithEvents dgvGruppe As DataGridView
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents dgvHeber As DataGridView
    Friend WithEvents Gruppierung As DataGridViewTextBoxColumn
    Friend WithEvents Mannschaft As DataGridViewTextBoxColumn
    Friend WithEvents Punkte As DataGridViewTextBoxColumn
    Friend WithEvents Platz As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents Vorname As DataGridViewTextBoxColumn
    Friend WithEvents Wertung As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents btnEdit As Button
End Class
