﻿
Imports System.ComponentModel
Imports MySqlConnector

Public Class frmMannschaften

    Dim Bereich As String = "Mannschaften"

    Dim Query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim dtMeldung As New DataTable
    Dim dtGruppen As New DataTable
    Dim ds As New DataSet

    Dim loading As Boolean
    Dim LayoutChanged_W As Boolean

    Dim pnlGruppe As New Dictionary(Of Integer, Panel)
    Dim dgvGruppe As New Dictionary(Of Integer, DataGridView)
    Dim chkBH As New Dictionary(Of Integer, CheckBox)
    Dim pkDate As New Dictionary(Of Integer, DateTimePicker)
    Dim pkTimeR As New Dictionary(Of Integer, DateTimePicker)
    Dim pkTimeS As New Dictionary(Of Integer, DateTimePicker)
    Dim cboBohle As New Dictionary(Of Integer, ComboBox)

    Dim Datum As Date
    Dim nonNumberEntered As Boolean = False
    Dim ctl As Boolean
    Dim shift As Boolean
    Dim dgvSource As Integer
    Dim dgvTarget As Integer
    Dim dgvSelection As String

    WithEvents ScrollTimer As New Timer With {.Interval = 40}
    Dim ScrollBreite As Integer 'virtuelle Breite von pnlForm
    Dim ScrollBereich As Integer = 100 ' Abstand vom Rand wo das autoscrollen losgeht
    Dim ScrollStep As Integer = 10 '* immer gleich 10 Pixel
    Dim ScrollRichtung As Direction = Direction.None
    Enum Direction
        Left
        Right
        None
    End Enum

    'Optionen
    Dim ScrollSpeed As Integer = 30
    Dim Zeit_pro_Hebung As Integer = 6
    Dim Zeit_Rundung As Integer = 5
    Dim AlternatingRowsDefaultCellStyle_BackColor As Color = Color.OldLace
    Dim RowsDefaultCellStyle_BackColor As Color = Color.LightCyan

    Private Sub frmGruppen_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If mnuSpeichern.Checked Then
            Select Case MsgBox("Änderungen vor dem Schließen speichern?", CType(MsgBoxStyle.YesNoCancel + MsgBoxStyle.Question, MsgBoxStyle), "Gruppen-Einteilung")
                Case MsgBoxResult.Yes
                    Save()
                Case MsgBoxResult.Cancel
                    e.Cancel = True
            End Select
        End If
    End Sub

    Private Sub frmGruppen_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmGruppen_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        ScrollTimer.Interval = ScrollSpeed

        pnlGruppe.Add(1, pnlG1)
        dgvGruppe.Add(1, dgvG1)
        chkBH.Add(1, chkBH1)
        pkDate.Add(1, dpkDatum)
        pkTimeR.Add(1, dpkWiegen)
        pkTimeS.Add(1, dpkWK)
        cboBohle.Add(1, cboB1)

        With dgvGruppe(1)
            AddHandler .MouseDown, AddressOf dgv_MouseDown
            AddHandler .DragDrop, AddressOf dgv_DragDrop
            AddHandler .DragEnter, AddressOf dgv_DragEnter
            AddHandler .GotFocus, AddressOf dgv_GotFocus
            AddHandler .LostFocus, AddressOf dgv_LostFocus
            AddHandler .DragOver, AddressOf dgv_DragOver
        End With

        'If Screen.FromControl(Me).Bounds.Width <= 1280 Then
        '    Me.Width = 1142
        '    Me.Height = 589
        '    pnlForm.ScrollControlIntoView(pnlBtn)
        'Else
        '    Me.Width = 1394
        '    Me.Height = 575
        'End If

        Dim tTip As New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        tTip.SetToolTip(btnAdd, "Fügt eine Gruppe am Ende der Liste hinzu.")
        tTip.SetToolTip(btnRemove, "Entfernt die letzte Gruppe der Liste.")
    End Sub

    Private Sub frmGruppen_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Me.Cursor = Cursors.WaitCursor
        loading = True
        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                'Datenquelle für Wettkampf-Datum
                Query = "SELECT Datum FROM Wettkampf WHERE Wettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                reader = cmd.ExecuteReader()
                reader.Read()
                Datum = CDate(reader(0))
                reader.Close()
                'Datenquelle für Gruppen
                Query = "select * from gruppen where Wettkampf = " & Wettkampf.ID & " order by Gruppe asc;"
                cmd = New MySqlCommand(Query, conn)
                dtGruppen.Load(cmd.ExecuteReader())
                'Datenquelle für Meldung
                Query = "SELECT m.idMeldung, m.Teilnehmer, a.Nachname, a.Vorname, w.Team, t.Team_Name, m.Gruppe, m.Reissen, m.Stossen " &
                  "FROM meldung m INNER JOIN athleten a " &
                  "ON m.Teilnehmer = a.idTeilnehmer " &
                  "LEFT JOIN verein v ON a.Verein = v.idVerein " &
                  "LEFT JOIN wettkampf_heber_team w ON w.Heber = a.idTeilnehmer " &
                  "Left Join " &
                  "(SELECT Team, Vereinsname AS Team_Name " &
                  "FROM(wettkampf_teams, verein) " &
                  "WHERE Team = idVerein And Wettkampf = " & Wettkampf.ID &
                  " UNION SELECT Team, Team_Name " &
                  "FROM(wettkampf_teams, teams) " &
                  "WHERE Team = idTeam AND Wettkampf =" & Wettkampf.ID & ") t " &
                  "ON w.Team = t.Team " &
                  "WHERE m.Wettkampf = " & Wettkampf.ID &
                  " And w.Wettkampf = " & Wettkampf.ID &
                  " ORDER BY Gruppe, Nachname, Vorname ASC;"
                cmd = New MySqlCommand(Query, conn)
                dtMeldung.Load(cmd.ExecuteReader())
            Catch ex As MySqlException
            End Try
        End Using

        If dtMeldung.Rows.Count = 0 Then
            If mnuMeldungen.Checked Then
                If MsgBox("Für den gewählten Wettkampf liegen keine Meldungen vor.", CType(MsgBoxStyle.OkCancel + MsgBoxStyle.Information, MsgBoxStyle), "Gruppen-Einteilung") = MsgBoxResult.Cancel Then
                    Wettkampf.ID = 0
                    Me.Close()
                    Exit Sub
                End If
            Else
                Wettkampf.ID = 0
                Me.Close()
                Exit Sub
            End If
        End If

        'gespeicherte Gruppen ermitteln
        Dim Gruppen As DataTable = dtMeldung.DefaultView.ToTable(True, "Gruppe")

        'Panels für gespeicherte Gruppen erstellen
        Dim r As DataRow()
        For c As Integer = 0 To Gruppen.Rows.Count - 1
            r = dtMeldung.Select("Gruppe = " + Gruppen.Rows(c)("Gruppe").ToString, "Nachname ASC, Vorname ASC")
            If c = 0 Then
                Build_Gruppen(dgvGruppe(c + 1))
                chkBH(c + 1).Checked = True
            Else
                btnAdd.PerformClick()
                loading = True
            End If
            For i As Integer = 0 To r.Count - 1
                dgvGruppe(c + 1).Rows.Add(r(i)("Teilnehmer"), r(i)("Nachname"), r(i)("Vorname"), r(i)("Team"), r(i)("Team_Name"), r(i)("Reissen"), r(i)("Stossen"))
            Next
            'Gruppen-Infos einlesen
            r = dtGruppen.Select("Gruppe = " + Gruppen.Rows(c)("Gruppe").ToString, "Gruppe ASC")
            If CInt(Gruppen.Rows(c)("Gruppe")) > 0 Then
                If r.Count > 0 Then
                    If CInt(r(0)("Gruppe")) = c Then
                        Get_Controls_Value(r(0), c + 1)
                    Else
                        pkDate(c + 1).Format = DateTimePickerFormat.Short
                        pkDate(c + 1).Value = Datum
                    End If
                Else
                    pkDate(c + 1).Format = DateTimePickerFormat.Short
                    pkDate(c + 1).Value = Datum
                End If
            End If
        Next

        cboArt.SelectedIndex = 0

        loading = False
        mnuSpeichern.Checked = False
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Build_Gruppen(grid As DataGridView)
        Try
            With grid
                .AutoGenerateColumns = False
                .RowHeadersVisible = False
                .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor 'Color.OldLace
                .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor 'Color.LightCyan
                .AllowUserToAddRows = False
                .AllowUserToDeleteRows = False
                .AllowUserToOrderColumns = True
                .AllowUserToResizeColumns = False
                .AllowUserToResizeRows = False
                .AllowDrop = True
                .BackgroundColor = pnlForm.BackColor
                .BorderStyle = BorderStyle.Fixed3D
                .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .ReadOnly = True
                .RowHeadersVisible = False
                .ScrollBars = ScrollBars.Vertical
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                .ShowEditingIcon = False
                Dim ColWidth() As Integer = {35, 75, 75, 35, 130, 40, 40}
                Dim ColBez() As String = {"Teilnehmer", "Name", "Vorname", "ID", "Team", "Reißen", "Stoßen"}
                Dim ColData() As String = {"Teilnehmer", "Nachname", "Vorname", "Team", "Team_Name", "Reissen", "Stossen"}
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Teilnehmer"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .Resizable = CType(False, DataGridViewTriState)})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .Resizable = CType(False, DataGridViewTriState)})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "ID"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Team", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Reißen"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Stoßen"})

                For i As Integer = 0 To ColWidth.Count - 1
                    .Columns(i).Width = ColWidth(i)
                    .Columns(i).HeaderText = ColBez(i)
                    .Columns(i).DataPropertyName = ColData(i)
                    .Columns(i).ReadOnly = True
                Next
                .Columns("Teilnehmer").Visible = False
                .Columns("ID").Visible = False
                .Columns("Reißen").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns("Stoßen").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            'Format_Grid("Gruppen", grid)
        Catch ex As Exception
            'Stop
        End Try
    End Sub

    Private Sub mnuDrucken_Click(sender As Object, e As EventArgs) Handles mnuDrucken.Click

    End Sub

    Private Sub mnuBeenden_Click(sender As Object, e As EventArgs) Handles mnuBeenden.Click

    End Sub

    Private Sub mnuSpeichern_Click(sender As Object, e As EventArgs) Handles mnuSpeichern.Click
        If mnuSpeichern.Enabled Then
            If mnuMeldungen.Checked Then
                Using New Centered_MessageBox(Me)
                    If MessageBox.Show("Neue Gruppen-Einteilung speichern?", Wettkampf.Bezeichnung, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                        Exit Sub
                    Else
                        Save()
                    End If
                End Using
            Else
                Save()
            End If
        End If
    End Sub

    Private Sub Save()
        Dim _Gruppe As Integer = 0
        Dim _wk As String = Wettkampf.ID.ToString
        Dim _Datum As String = String.Empty
        Dim _Reissen As String = String.Empty
        Dim _Stossen As String = String.Empty
        Dim _Block As String = String.Empty
        Dim _Bohle As String = String.Empty
        Dim _meldung As String = String.Empty

        Dim act As Boolean
        Dim s As Integer = 1
        If lblG1.Text = "Heber" Then s = 2
        For i As Integer = pnlGruppe.Count To s Step -1
            If dgvGruppe(i).Rows.Count = 0 And act Then
                MsgBox("Gruppen müssen in aufsteigender Reihenfolge belegt werden.", MsgBoxStyle.Information, "Gruppen-Einteilung")
                Exit Sub
            End If
            act = dgvGruppe(i).Rows.Count > 0
        Next

        Me.Cursor = Cursors.WaitCursor

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                'alle Einträge für diesen Wettkampf in "gruppen" löschen
                Query = "DELETE FROM gruppen WHERE Wettkampf = " & _wk & ";"
                reader = New MySqlCommand(Query, conn).ExecuteReader
                reader.Close()
                For i As Integer = 1 To dgvGruppe.Count
                    'alle DGVs durchlaufen
                    _Gruppe = i
                    'wenn mehrere Gruppen dann 1.Gruppe = 0 (Reserve)
                    If pnlGruppe.Count > 1 Then _Gruppe = i - 1
                    For c As Integer = 0 To dgvGruppe(i).Rows.Count - 1
                        'alle Rows im DGV druchlaufen und in "meldung" speichern 
                        _meldung = dgvGruppe(i).Rows(c).Cells("Meldung").Value.ToString
                        Query = "UPDATE meldung SET Gruppe = " & _Gruppe & " WHERE idMeldung = " & _meldung & ";"
                        reader = New MySqlCommand(Query, conn).ExecuteReader
                        reader.Close()
                    Next
                    'in "gruppen" speichern
                    If _Gruppe > 0 Then
                        _Datum = pkDate(i).Value.ToString("yyyy-MM-dd")
                        _Reissen = "'" + Trim(pkTimeR(i).Text) + "'"
                        If _Reissen = "''" Then _Reissen = "NULL"
                        _Stossen = "'" + Trim(pkTimeS(i).Text) + "'"
                        If _Stossen = "''" Then _Stossen = "NULL"
                        _Block = chkBH(i).Checked.ToString
                        _Bohle = cboBohle(i).Text
                        If _Bohle = "" Then
                            _Bohle = "1"
                            cboBohle(i).Text = "1"
                        End If
                        Query = "INSERT INTO gruppen (Wettkampf, Gruppe, Datum, Reissen, Stossen, Blockheben, Bohle) " &
                            "VALUES(" & _wk & ", " & _Gruppe & ", '" & _Datum & "', " & _Reissen & ", " & _Stossen & ", " & _Block & ", " & _Bohle & ");"
                        reader = New MySqlCommand(Query, conn).ExecuteReader
                        reader.Close()
                    End If
                Next
                mnuSpeichern.Checked = False
            Catch ex As MySqlException
                'Stop
                'Finally
                '    conn.Close()
            End Try
        End Using
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Disable_Controls(i As Integer)
        'pkDate(i).Format = DateTimePickerFormat.Custom
        'pkDate(i).CustomFormat = " "
        pkDate(i).Format = DateTimePickerFormat.Short
        pkDate(i).Value = Datum 'Now
        pkTimeR(i).CustomFormat = " "
        pkTimeR(i).Value = Now
        pkTimeS(i).CustomFormat = " "
        pkTimeS(i).Value = Now
        cboBohle(i).SelectedIndex = 0 '-1
        chkBH(i).Checked = True
    End Sub

    Private Sub Get_Controls_Value(r As DataRow, c As Integer)
        pkDate(c).Format = DateTimePickerFormat.Short
        If IsDBNull(r("Datum")) Then
            pkDate(c).Value = Datum
        Else
            pkDate(c).Value = CDate(r("Datum"))
        End If
        If Not IsDBNull(r("Reissen")) Then
            pkTimeR(c).CustomFormat = "HH:mm"
            pkTimeR(c).Text = r("Reissen").ToString
        End If
        If Not IsDBNull(r("Stossen")) Then
            pkTimeS(c).CustomFormat = "HH:mm"
            pkTimeS(c).Text = r("Stossen").ToString
        End If
        cboBohle(c).Text = r("Bohle").ToString
        chkBH(c).Checked = CBool(r("Blockheben"))
    End Sub

    Private Sub Blockheben_CheckedChanged(sender As Object, e As EventArgs) ' Handles chkB1.CheckedChanged
        If loading Then Exit Sub
        Dim Ix As Integer = CInt(Strings.Mid(Me.ActiveControl.Name, 5))
        mnuSpeichern.Checked = True
        dgvGruppe(Ix).Focus()
    End Sub

    Private Sub pkDatum_GotFocus(sender As Object, e As EventArgs)
        Dim Ix As Integer = CInt(Strings.Mid(Me.ActiveControl.Name, 5))
        If pkDate(Ix).Format = DateTimePickerFormat.Short Then Exit Sub
        loading = True
        pkDate(Ix).Format = DateTimePickerFormat.Short
        pkDate(Ix).Value = Datum
        loading = False
    End Sub

    Private Sub pkDatum_ValueChanged(sender As Object, e As EventArgs) ' Handles date1.ValueChanged
        If Not loading Then mnuSpeichern.Checked = True
    End Sub

    Private Sub pkTimeR_GotFocus(sender As Object, e As EventArgs) 'Handles timR1.GotFocus
        Dim Ix As Integer = CInt(Strings.Mid(Me.ActiveControl.Name, 5))
        If pkTimeR(Ix).CustomFormat = "HH:mm" Then Exit Sub
        'loading = True
        pkTimeR(Ix).CustomFormat = "HH:mm"
        pkTimeR(Ix).Text = Microsoft.VisualBasic.Strings.Format(Now, "HH:mm")
        'loading = False
    End Sub

    Private Sub pkTimeS_GotFocus(sender As Object, e As EventArgs) 'Handles timS1.GotFocus
        Dim Ix As Integer = CInt(Strings.Right(Me.ActiveControl.Name, 1))
        If pkTimeS(Ix).CustomFormat = "HH:mm" Then Exit Sub
        pkTimeS(Ix).CustomFormat = "HH:mm"
        pkTimeS(Ix).Text = Microsoft.VisualBasic.Strings.Format(Now, "HH:mm")
    End Sub

    Private Sub pkTimeR_ValueChanged(sender As Object, e As EventArgs) ' Handles timR1.ValueChanged
        If loading Then Exit Sub
        Dim i As Integer = CInt(Strings.Mid(Me.ActiveControl.Name, 5))
        Dim minutes As Integer = dgvGruppe(i).Rows.Count * Zeit_pro_Hebung
        minutes = ((minutes \ Zeit_Rundung) + CInt((minutes Mod Zeit_Rundung) / Zeit_Rundung)) * Zeit_Rundung
        Dim _zeit1 As TimeSpan = TimeSpan.Parse(pkTimeR(i).Text)
        Dim _zeit2 As New TimeSpan(minutes \ 60, minutes Mod 60, 0)
        Dim _zeit3 As TimeSpan = _zeit1 + _zeit2
        pkTimeS(i).CustomFormat = "HH:mm"
        pkTimeS(i).Text = _zeit3.ToString
        mnuSpeichern.Checked = True
    End Sub

    Private Sub pkTimeS_ValueChanged(sender As Object, e As EventArgs) 'Handles timS1.ValueChanged
        If Not loading Then mnuSpeichern.Checked = True
    End Sub

    Private Sub cboBohle_SelectedValueChanged(sender As Object, e As EventArgs) 'Handles cboB1.SelectedValueChanged
        If Not loading Then mnuSpeichern.Checked = True
    End Sub

    Private Sub cboArt_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboArt.SelectedIndexChanged
        For i As Integer = 1 To dgvGruppe.Count
            dgvGruppe(i).Columns("Reißen").Visible = cboArt.SelectedIndex = 0
            dgvGruppe(i).Columns("Stoßen").Visible = cboArt.SelectedIndex = 1
            dgvGruppe(i).Sort(dgvGruppe(i).Columns(cboArt.Text), ListSortDirection.Ascending)
        Next
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Me.Cursor = Cursors.WaitCursor

        Dim NewPanel As New Panel
        Dim NewIx As Integer = pnlGruppe.Count + 1
        Dim newctl As Control = Nothing

        If NewIx > 1 Then
            lblG1.Text = "Heber"
            For Each ctl As Control In pnlG1.Controls
                If Not TypeOf ctl Is DataGridView And Not ctl.Name.Contains("lblG") Then ctl.Visible = False
            Next
        End If

        btnRemove.Enabled = NewIx > 1

        With NewPanel
            .Name = "pnlG" + NewIx.ToString
            .Parent = pnlForm
            .Parent.Controls.Add(NewPanel)
            .Location = New Point(pnlG1.Left + (pnlG1.Width + 10) * (NewIx - 1), pnlG1.Top)
            .Size = pnlG1.Size
            .BorderStyle = pnlG1.BorderStyle
            .Visible = True
            .AutoScroll = False
            .Anchor = pnlG1.Anchor
            .BackColor = pnlG1.BackColor
            .TabIndex = 101
        End With
        pnlGruppe.Add(NewIx, NewPanel)
        With pnlGruppe(NewIx)
            pnlBtn.Location = New Point(.Left + .Width + 6, .Top)
        End With

        For Each ctl As Control In pnlG1.Controls
            If TypeOf ctl Is Label Then
                newctl = New Label
            ElseIf TypeOf ctl Is DateTimePicker Then
                newctl = New DateTimePicker
            ElseIf TypeOf ctl Is ComboBox Then
                newctl = New ComboBox
            ElseIf TypeOf ctl Is DataGridView Then
                newctl = New DataGridView
            ElseIf TypeOf ctl Is CheckBox Then
                newctl = New CheckBox
            End If
            If Not TypeOf ctl Is VScrollBar And Not TypeOf ctl Is HScrollBar Then
                CopyProperties(newctl, ctl, pnlGruppe(NewIx))
            End If
        Next
        loading = True
        Disable_Controls(NewIx)
        loading = False
        pnlForm.ScrollControlIntoView(pnlBtn)

        ScrollBreite = dgvGruppe.Count * (pnlGruppe(1).Width + 10) + 42
        If ScrollBreite > pnlForm.ClientSize.Width Then Me.Height = Me.MinimumSize.Height + 15

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        Dim Ix As Integer = pnlGruppe.Count 'es wird immer das letzte Element entfernt
        If dgvGruppe(Ix).Rows.Count > 0 Then
            'der Gruppe sind Heber zugeordnet
            If mnuKonflikte.Checked Then MsgBox("Gruppe muss leer sein, bevor sie gelöscht werden kann.", CType(MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, MsgBoxStyle), "Gruppen-Einteilung")
            Exit Sub
        End If
        If Ix = 2 Then
            lblG1.Text = " Gruppe 1"
            For Each ctl As Control In pnlG1.Controls
                ctl.Visible = True
            Next
            btnRemove.Enabled = False
        End If
        With dgvGruppe(Ix)
            RemoveHandler .MouseDown, AddressOf dgv_MouseDown
            RemoveHandler .DragDrop, AddressOf dgv_DragDrop
            RemoveHandler .DragEnter, AddressOf dgv_DragEnter
            RemoveHandler .GotFocus, AddressOf dgv_GotFocus
            RemoveHandler .LostFocus, AddressOf dgv_LostFocus
            RemoveHandler .DragOver, AddressOf dgv_DragOver
        End With
        RemoveHandler pkDate(Ix).GotFocus, AddressOf pkDatum_GotFocus
        RemoveHandler pkDate(Ix).ValueChanged, AddressOf pkDatum_ValueChanged
        RemoveHandler pkTimeS(Ix).GotFocus, AddressOf pkTimeS_GotFocus
        RemoveHandler pkTimeS(Ix).ValueChanged, AddressOf pkTimeS_ValueChanged
        RemoveHandler pkTimeR(Ix).GotFocus, AddressOf pkTimeR_GotFocus
        RemoveHandler pkTimeR(Ix).ValueChanged, AddressOf pkTimeR_ValueChanged
        dgvGruppe.Remove(Ix)
        chkBH.Remove(Ix)
        pkDate.Remove(Ix)
        pkTimeR.Remove(Ix)
        pkTimeS.Remove(Ix)
        cboBohle.Remove(Ix)
        pnlGruppe(Ix).Dispose()
        pnlGruppe.Remove(Ix)
        With pnlGruppe(Ix - 1)
            pnlBtn.Location = New Point(.Left + .Width + 6, .Top)
        End With
        ScrollBreite = dgvGruppe.Count * (pnlGruppe(1).Width + 10) + 42
        If ScrollBreite <= pnlForm.ClientSize.Width Then Me.Height = Me.MinimumSize.Height

    End Sub

    Private Sub CopyProperties(ByRef newctl As Control, ByRef source As Control, ByRef parent As Panel)
        Dim Ix As Integer = pnlGruppe.Count
        With newctl
            .Name = Strings.Left(source.Name, 4) + Ix.ToString
            .Parent = parent
            .Parent.Controls.Add(newctl)
            .AutoSize = source.AutoSize
            .Location = New Point(source.Left, source.Top)
            .Size = source.Size
            .Dock = source.Dock
            .Anchor = source.Anchor
            .Text = source.Text
            .Padding = source.Padding
            .BackColor = source.BackColor
            .ForeColor = source.ForeColor
            .Font = New Font(source.Font.FontFamily, source.Font.Size)
            .TabIndex = CInt(source.TabIndex.ToString + "0" + Ix.ToString)
            .Visible = True
        End With
        If TypeOf newctl Is Label Then
            Dim ctl As Label = CType(newctl, Label)
            With ctl
                .TextAlign = ContentAlignment.MiddleLeft
                If .Name.Contains("lblG") Then
                    .Text = "Gruppe " + (pnlGruppe.Count - 1).ToString
                End If
            End With
        ElseIf TypeOf source Is DateTimePicker Then
            Dim ctl As DateTimePicker = CType(newctl, DateTimePicker)
            With ctl
                .Checked = False
                If .Name.Contains("date") Then
                    .Format = DateTimePickerFormat.Short
                    .ShowUpDown = True
                    pkDate.Add(Ix, ctl)
                    AddHandler ctl.GotFocus, AddressOf pkDatum_GotFocus
                    AddHandler ctl.ValueChanged, AddressOf pkDatum_ValueChanged
                ElseIf .Name.Contains("tim") Then
                    .Format = DateTimePickerFormat.Custom
                    .CustomFormat = "HH:mm"
                    .ShowUpDown = True
                    If .Name.Contains("S") Then
                        pkTimeS.Add(Ix, ctl)
                        AddHandler ctl.GotFocus, AddressOf pkTimeS_GotFocus
                        AddHandler ctl.ValueChanged, AddressOf pkTimeS_ValueChanged
                    ElseIf .Name.Contains("R") Then
                        pkTimeR.Add(Ix, ctl)
                        AddHandler ctl.GotFocus, AddressOf pkTimeR_GotFocus
                        AddHandler ctl.ValueChanged, AddressOf pkTimeR_ValueChanged
                    End If
                End If
            End With
        ElseIf TypeOf source Is ComboBox Then
            Dim ctl As ComboBox = CType(newctl, ComboBox)
            With ctl
                .DropDownStyle = ComboBoxStyle.DropDownList
                .Items.AddRange({1, 2})
                .SelectedIndex = -1
            End With
            cboBohle.Add(Ix, ctl)
        ElseIf TypeOf source Is DataGridView Then
            Dim ctl As DataGridView = CType(newctl, DataGridView)
            dgvGruppe.Add(Ix, ctl)
            Build_Gruppen(ctl)
            With dgvGruppe(Ix)
                AddHandler .MouseDown, AddressOf dgv_MouseDown
                AddHandler .DragDrop, AddressOf dgv_DragDrop
                AddHandler .DragEnter, AddressOf dgv_DragEnter
                AddHandler .GotFocus, AddressOf dgv_GotFocus
                AddHandler .LostFocus, AddressOf dgv_LostFocus
                AddHandler .DragOver, AddressOf dgv_DragOver
                .DefaultCellStyle.SelectionBackColor = RowsDefaultCellStyle_BackColor
                .DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.ControlText)
            End With
        ElseIf TypeOf source Is CheckBox Then
            Dim ctl As CheckBox = CType(newctl, CheckBox)
            With ctl
                .TextAlign = ContentAlignment.MiddleLeft
                .CheckAlign = ContentAlignment.MiddleRight
                .AutoCheck = True
                .Left = source.Left
            End With
            chkBH.Add(Ix, ctl)
        End If

    End Sub

    Private Sub dgv_MouseDown(sender As Object, e As MouseEventArgs) 'Handles dgvG1.MouseDown
        dgvSource = CInt(Strings.Mid(Me.ActiveControl.Name, 5))
        With dgvGruppe(dgvSource)
            Dim hit As DataGridView.HitTestInfo = .HitTest(e.X, e.Y)
            dgvSelection = String.Empty
            If hit.RowIndex > -1 Then
                If .SelectedRows.Contains(.Rows(hit.RowIndex)) Then
                    For i As Integer = 0 To .SelectedRows.Count - 1
                        dgvSelection += .SelectedRows(i).Index.ToString + "-"
                    Next
                    dgvSelection = dgvSelection.Remove(dgvSelection.Length - 1)
                Else
                    dgvSelection += hit.RowIndex.ToString
                End If
                .DoDragDrop(dgvSelection, DragDropEffects.Move)
            End If
        End With
    End Sub

    Private Sub dgv_DragDrop(sender As Object, e As System.Windows.Forms.DragEventArgs)
        For Me.dgvTarget = 1 To dgvGruppe.Count
            If dgvGruppe.ContainsKey(dgvTarget) Then
                If sender.Equals(dgvGruppe(dgvTarget)) Then Exit For
            End If
        Next
        If dgvSource = dgvTarget Then Exit Sub
        ScrollTimer.Stop()
        With dgvGruppe(dgvTarget)
            Dim clientPoint As Point = .PointToClient(New Point(e.X, e.Y))
            Dim hit As DataGridView.HitTestInfo = .HitTest(clientPoint.X, clientPoint.Y)
            Dim i As Integer
            If hit.RowIndex = -1 Then
                i = .Rows.Count 'Einfügen nach letzter Zeile
            Else
                i = hit.RowIndex
            End If
            Dim s As String() = Split(dgvSelection, "-")
            For r As Integer = s.Count - 1 To 0 Step -1
                .Rows.Insert(i)
                For c As Integer = 0 To dgvGruppe(dgvSource).Columns.Count - 1
                    .Rows(i).Cells(c).Value = dgvGruppe(dgvSource).Rows(CInt(s(r))).Cells(c).Value
                Next
            Next
            For r As Integer = 0 To s.Count - 1
                dgvGruppe(dgvSource).Rows.RemoveAt(CInt(s(r)))
            Next
            If .CurrentRow.Index Mod 2 = 1 Then .DefaultCellStyle.SelectionBackColor = AlternatingRowsDefaultCellStyle_BackColor
            .Sort(.Columns(cboArt.Text), CType(Math.Abs(.SortOrder - 1), ListSortDirection))
        End With
        mnuSpeichern.Checked = True
    End Sub

    Private Sub dgv_DragEnter(sender As Object, e As System.Windows.Forms.DragEventArgs)
        Dim t As Integer
        For t = 1 To dgvGruppe.Count
            If sender.Equals(dgvGruppe(t)) Then Exit For
        Next
        If t = dgvSource Then
            e.Effect = DragDropEffects.None
        Else
            e.Effect = DragDropEffects.Move
        End If
    End Sub

    Private Sub dgv_DragOver(sender As Object, e As DragEventArgs) 'Handles dgvG1.DragOver
        If ScrollBreite > pnlForm.Width Then
            If pnlForm.PointToClient(New Point(e.X, e.Y)).X > pnlForm.Width - ScrollBereich Then
                ScrollRichtung = Direction.Right
            ElseIf Math.Abs(pnlForm.AutoScrollPosition.X) > 0 And pnlForm.PointToClient(New Point(e.X, e.Y)).X < ScrollBereich Then
                ScrollRichtung = Direction.Left
            Else
                ScrollRichtung = Direction.None
            End If
            If ScrollRichtung <> Direction.None Then
                ScrollTimer.Start()
            End If
        End If
    End Sub

    Private Sub dgv_GotFocus(sender As Object, e As EventArgs) 'Handles dgvG1.GotFocus
        Dim Ix As Integer = CInt(Strings.Mid(Me.ActiveControl.Name, 5))
        With dgvGruppe(Ix)
            If .Rows.Count = 0 Then Exit Sub
            With .DefaultCellStyle
                .SelectionBackColor = Color.FromKnownColor(KnownColor.Highlight)
                .SelectionForeColor = Color.FromKnownColor(KnownColor.HighlightText)
            End With
        End With
    End Sub

    Private Sub dgv_LostFocus(sender As Object, e As EventArgs) 'Handles dgvG1.LostFocus
        Dim Ix As Integer = CInt(Strings.Mid(Me.ActiveControl.Name, 5))
        With dgvGruppe(Ix)
            If .Rows.Count = 0 Then Exit Sub
            If .CurrentRow.Index Mod 2 = 0 Then
                .DefaultCellStyle.SelectionBackColor = RowsDefaultCellStyle_BackColor
            ElseIf .CurrentRow.Index Mod 2 = 1 Then
                .DefaultCellStyle.SelectionBackColor = AlternatingRowsDefaultCellStyle_BackColor
            End If
            .DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.ControlText)
        End With
    End Sub

    Private Sub Scroll_Panel()
        With pnlForm
            Select Case ScrollRichtung
                Case Direction.Left
                    .AutoScrollPosition = New Point(Math.Abs(.AutoScrollPosition.X + ScrollStep), Math.Abs(.AutoScrollPosition.Y))
                    If Math.Abs(pnlForm.AutoScrollPosition.X) < ScrollStep Then .AutoScrollPosition = New Point(0, Math.Abs(.AutoScrollPosition.Y))
                Case Direction.Right
                    .AutoScrollPosition = New Point(Math.Abs(.AutoScrollPosition.X - ScrollStep), Math.Abs(.AutoScrollPosition.Y))
            End Select
        End With
    End Sub

    Private Sub ScrollTimer_Tick(sender As Object, e As EventArgs) Handles ScrollTimer.Tick
        Scroll_Panel()
        If Math.Abs(pnlForm.AutoScrollPosition.X) < ScrollStep Then ScrollTimer.Stop()
    End Sub


End Class