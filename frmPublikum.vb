﻿
Imports System.Timers
Imports MySqlConnector

Public Class frmPublikum

#Region "CustomColumns"
    Public Class CustomColumn_LeftRightBorder
        Inherits DataGridViewColumn
        Public Sub New()
            CellTemplate = New DataGridViewCustomCell_lrBorder()
        End Sub
    End Class
    Public Class CustomColumn_RightBorder
        Inherits DataGridViewColumn
        Public Sub New()
            CellTemplate = New DataGridViewCustomCell_rBorder()
        End Sub
    End Class
    Public Class CustomDataGridView
        Inherits DataGridView
        Public Sub New()
            With Me
                .RowTemplate = New DataGridViewCustomRow()
                .CellBorderStyle = DataGridViewCellBorderStyle.None
                .ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single
                .EnableHeadersVisualStyles = False
                .RowHeadersVisible = False
            End With
        End Sub
        Public Overrides Function AdjustColumnHeaderBorderStyle(
                 dataGridViewAdvancedBorderStyleInput As DataGridViewAdvancedBorderStyle,
                 dataGridViewAdvancedBorderStylePlaceHolder As DataGridViewAdvancedBorderStyle,
                 firstDisplayedColumn As Boolean,
                 lastVisibleColumn As Boolean) As DataGridViewAdvancedBorderStyle

            With dataGridViewAdvancedBorderStylePlaceHolder
                .Top = DataGridViewAdvancedCellBorderStyle.None
                .Left = DataGridViewAdvancedCellBorderStyle.Single
                .Right = DataGridViewAdvancedCellBorderStyle.None
                .Bottom = DataGridViewAdvancedCellBorderStyle.None
            End With

            Return dataGridViewAdvancedBorderStylePlaceHolder
        End Function
    End Class
    Public Class DataGridViewCustomRow
        Inherits DataGridViewRow
        Public Overrides Function AdjustRowHeaderBorderStyle(
                 dataGridViewAdvancedBorderStyleInput As DataGridViewAdvancedBorderStyle,
                 dataGridViewAdvancedBorderStylePlaceHolder As DataGridViewAdvancedBorderStyle,
                 singleVerticalBorderAdded As Boolean,
                 singleHorizontalBorderAdded As Boolean,
                 isFirstDisplayedRow As Boolean,
                 isLastDisplayedRow As Boolean) As DataGridViewAdvancedBorderStyle

            If Not isLastDisplayedRow Then
                dataGridViewAdvancedBorderStylePlaceHolder.Bottom = DataGridViewAdvancedCellBorderStyle.None
            Else
                dataGridViewAdvancedBorderStylePlaceHolder.Bottom = DataGridViewAdvancedCellBorderStyle.Single
            End If

            With dataGridViewAdvancedBorderStylePlaceHolder
                .Right = DataGridViewAdvancedCellBorderStyle.None
                .Left = DataGridViewAdvancedCellBorderStyle.None
                .Top = DataGridViewAdvancedCellBorderStyle.None
            End With

            Return dataGridViewAdvancedBorderStylePlaceHolder
        End Function
    End Class
    Public Class DataGridViewCustomCell_rBorder
        Inherits DataGridViewTextBoxCell
        Public Overrides Function AdjustCellBorderStyle(
                     dataGridViewAdvancedBorderStyleInput As DataGridViewAdvancedBorderStyle,
                     dataGridViewAdvancedBorderStylePlaceHolder As DataGridViewAdvancedBorderStyle,
                     singleVerticalBorderAdded As Boolean,
                     singleHorizontalBorderAdded As Boolean,
                     firstVisibleColumn As Boolean,
                     firstVisibleRow As Boolean) As DataGridViewAdvancedBorderStyle

            With dataGridViewAdvancedBorderStylePlaceHolder
                .Top = DataGridViewAdvancedCellBorderStyle.None
                .Left = DataGridViewAdvancedCellBorderStyle.None
                .Right = DataGridViewAdvancedCellBorderStyle.Single
                .Bottom = DataGridViewAdvancedCellBorderStyle.None
            End With

            Return dataGridViewAdvancedBorderStylePlaceHolder
        End Function
    End Class
    Public Class DataGridViewCustomCell_lrBorder
        Inherits DataGridViewTextBoxCell
        Public Overrides Function AdjustCellBorderStyle(
                     dataGridViewAdvancedBorderStyleInput As DataGridViewAdvancedBorderStyle,
                     dataGridViewAdvancedBorderStylePlaceHolder As DataGridViewAdvancedBorderStyle,
                     singleVerticalBorderAdded As Boolean,
                     singleHorizontalBorderAdded As Boolean,
                     firstVisibleColumn As Boolean,
                     firstVisibleRow As Boolean) As DataGridViewAdvancedBorderStyle

            With dataGridViewAdvancedBorderStylePlaceHolder
                .Top = DataGridViewAdvancedCellBorderStyle.None
                .Left = DataGridViewAdvancedCellBorderStyle.Single
                .Right = DataGridViewAdvancedCellBorderStyle.Single
                .Bottom = DataGridViewAdvancedCellBorderStyle.None
            End With

            Return dataGridViewAdvancedBorderStylePlaceHolder
        End Function
    End Class
#End Region

    Dim Design As String = "Nacht"

    Dim Query As String
    Dim cmd As MySqlCommand

    Dim dv1, dv2 As DataView
    WithEvents bs1, bs2 As BindingSource

    Private Delegate Sub Delegate_String(Value As String)
    Private Delegate Sub Delegate_Color(Color As Color)
    Private Delegate Sub Delegate_Integer(Id As Integer)
    Private Delegate Sub Delegate_Integer2(Id As Integer, Value As Integer)
    Private Delegate Sub DelegateSub2(Info As String)
    Private Delegate Sub Delegate_Sub()
    Private Delegate Sub Delegate_List_String(Value As List(Of String))


    Dim LifterPositions As New List(Of KeyValuePair(Of Integer, String))
    Dim Color_Not_Declared As Color = Color.Silver 'LightGray'Gainsboro
    Dim Color_Declared As Color = Color.Lime 'Color.FromArgb(0, 158, 0)

    Public dicLabels As Dictionary(Of Integer, Label)
    Dim lstResult As New List(Of Integer)

    Dim Wertung_1 As Integer
    Dim NK_String As String
    Dim NK_String_2 As String
    Dim GroupDivider_Height As Integer = 5 ' Höhe der Divider zwischen Gruppen

    Private func As myFunction = New myFunction

    Private WithEvents TimerMsg As New Timer With {.Interval = 375, .AutoReset = True, .SynchronizingObject = Me}   ' Blinken von Jury-Entscheid
    Private timerMsg_Running As Boolean

    Private IsDisplayed As Boolean = False
    Private TeamsCount As Integer
    Private GroupDividersCount As Integer

    Private CurrentFontsize As Single
    Private CurrentDisplay As Integer

    Property WK_ID As String

    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        RemoveHandler Wettkampf.ID_Changed, AddressOf Wettkampf_Changed
        RemoveHandler User.Bohle_Changed, AddressOf Bohle_Changed
        RemoveHandler Wettkampf.Technik_Changed, AddressOf Technik_Changed
        'RemoveHandler Wettkampf.Bezeichnung_Changed, AddressOf WK_BezeichungChanged
        'RemoveHandler Wettkampf.Mannschaft_Changed, AddressOf WK_MannschaftChanged
        RemoveHandler Leader.Gruppe_Changed, AddressOf Gruppe_Changed
        RemoveHandler Leader.Durchgang_Changed, AddressOf Durchgang_Changed
        RemoveHandler Leader.Scoring_Changed, AddressOf Wertung_Changed
        'RemoveHandler Leader.CurrenScore_Changed, AddressOf CurrentScore_Changed
        'RemoveHandler Leader.Score_Changed, AddressOf Set_Score
        RemoveHandler Leader.Team_Result, AddressOf Update_TeamResults
        RemoveHandler Leader.NextHeber_Mark, AddressOf Mark_NextHebers
        AddHandler Leader.Set_AV, AddressOf Set_Anfangslast
        RemoveHandler Leader.Change_AV, AddressOf Change_Anfangslast
        RemoveHandler Leader.Change_Lifter, AddressOf Lifter_Exchange

        RemoveHandler Anzeige.CountDown_Changed, AddressOf CountDown_Changed
        RemoveHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed
        'RemoveHandler Anzeige.Minuten_Changed, AddressOf Minuten_Changed
        'RemoveHandler Anzeige.Sekunden_Changed, AddressOf Sekunden_Changed
        'RemoveHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed

        'RemoveHandler FontSize_Changed, AddressOf frmPublikum_SizeChanged ' Event in ScreenScale
        RemoveHandler Ansicht_Options.FontSizeChanged, AddressOf Set_FontSize
        RemoveHandler Ansicht_Options.NameFormat_Changed, AddressOf NameFormat_Changed

        RemoveHandler Leader.MasterData_Changed, AddressOf MasterData_Changed
        RemoveHandler Leader.Teamwertung_Changed, AddressOf Change_Teamwertung
        RemoveHandler Leader.Mark_Rows, AddressOf Heber_Highlight
        RemoveHandler Leader.Dim_Rows, AddressOf Heber_Dim

        'RemoveHandler JuryMsg_Change, AddressOf Call_TimerMsg
        RemoveHandler Leader.Jury_Entscheidung, AddressOf InfoMessage_Show

        'RemoveHandler ReturnFromKorrektur, AddressOf Update_From_Korrektur

        Using sc As New ScreenScale
            Dim mon = sc.GetDeviceNumber(Screen.FromControl(Me))
            If dicScreens.ContainsKey(mon) Then dicScreens.Remove(mon)
        End Using

    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Cursor = Cursors.WaitCursor
        AddHandler Wettkampf.ID_Changed, AddressOf Wettkampf_Changed
        AddHandler User.Bohle_Changed, AddressOf Bohle_Changed
        AddHandler Wettkampf.Technik_Changed, AddressOf Technik_Changed
        'AddHandler Wettkampf.Bezeichnung_Changed, AddressOf WK_BezeichungChanged
        'AddHandler Wettkampf.Mannschaft_Changed, AddressOf WK_MannschaftChanged
        AddHandler Leader.Gruppe_Changed, AddressOf Gruppe_Changed
        AddHandler Leader.Durchgang_Changed, AddressOf Durchgang_Changed
        AddHandler Leader.Scoring_Changed, AddressOf Wertung_Changed
        'AddHandler Leader.CurrenScore_Changed, AddressOf CurrentScore_Changed
        'AddHandler Leader.Score_Changed, AddressOf Set_Score
        AddHandler Leader.Team_Result, AddressOf Update_TeamResults
        AddHandler Leader.NextHeber_Mark, AddressOf Mark_NextHebers
        AddHandler Leader.Set_AV, AddressOf Set_Anfangslast
        AddHandler Leader.Change_AV, AddressOf Change_Anfangslast
        AddHandler Leader.Change_Lifter, AddressOf Lifter_Exchange

        AddHandler Anzeige.CountDown_Changed, AddressOf CountDown_Changed
        AddHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed
        'AddHandler Anzeige.Minuten_Changed, AddressOf Minuten_Changed
        'AddHandler Anzeige.Sekunden_Changed, AddressOf Sekunden_Changed
        'AddHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed

        'AddHandler FontSize_Changed, AddressOf frmPublikum_SizeChanged ' Event in ScreenScale
        AddHandler Ansicht_Options.FontSizeChanged, AddressOf Set_FontSize
        AddHandler Ansicht_Options.NameFormat_Changed, AddressOf NameFormat_Changed

        AddHandler Leader.MasterData_Changed, AddressOf MasterData_Changed
        AddHandler Leader.Teamwertung_Changed, AddressOf Change_Teamwertung
        AddHandler Leader.Mark_Rows, AddressOf Heber_Highlight
        AddHandler Leader.Dim_Rows, AddressOf Heber_Dim

        'AddHandler JuryMsg_Change, AddressOf Call_TimerMsg
        AddHandler Leader.Jury_Entscheidung, AddressOf InfoMessage_Show ' JuryControl Raspi

        'AddHandler ReturnFromKorrektur, AddressOf Update_From_Korrektur

        Try
            xAufruf.Font = New Font(FontFamilies(Fonts.Zeitanzeige), xAufruf.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf.ForeColor = Anzeige.Clock_Colors(Clock_Status.Waiting)
        Catch ex As Exception
        End Try

        lstResult.AddRange({0, 0})

        dgvJoin.Columns.Clear()

        dicLabels = New Dictionary(Of Integer, Label)
        dicLabels(0) = lblVerein1
        dicLabels(1) = lblVerein2
        dicLabels(2) = lblVerein3
        dicLabels(3) = lblVerein4
        dicLabels(4) = lblVerein5
        dicLabels(5) = lblVerein6

        'If Wettkampf.ID > 0 Then Build_Grid()

        'WK_MannschaftChanged(Wettkampf.Mannschaft)

        WK_ID = Wettkampf.Identities.Keys(0)

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Using sc As New ScreenScale
            CurrentDisplay = sc.GetDeviceNumber(Screen.FromControl(Me))
        End Using

        If Not IsNothing(Leader.Gruppe) Then
            Gruppe_Changed(Leader.Gruppe)
        Else
            lblGruppe.Text = String.Empty
        End If

        If Not String.IsNullOrEmpty(Leader.Durchgang) Then
            Durchgang_Changed(Leader.Durchgang)
        Else
            lblDurchgang.Text = String.Empty
        End If

        If Heber.Id > 0 AndAlso Heber.Wertung = 0 Then Leader.Mark_CalledLifters(Leader.Get_Called_Lifters)
    End Sub

    Private Sub Set_FontSize(Increase As Single, Optional Reset As Boolean = False)

        Dim Fontsize As Single = dgvJoin.DefaultCellStyle.Font.Size

        If Reset Then
            Fontsize = dgvJoin.DefaultCellStyle.Font.Size * dgvJoin.Width / Get_TextWidth()
        Else
            Fontsize += Increase
        End If

        Change_FontSize(Fontsize)

    End Sub

    Private Sub Change_FontSize(FontSize As Single)
        With dgvTeam
            If .Visible Then
                .DefaultCellStyle.Font = New Font(.Font.FontFamily, FontSize, FontStyle.Regular, GraphicsUnit.Point)
                .ColumnHeadersDefaultCellStyle.Font = New Font(.Font.FontFamily, FontSize, FontStyle.Regular, GraphicsUnit.Point)
                .AutoResizeColumns()
                .AutoResizeRows()
                .AutoResizeColumnHeadersHeight()
                .Height = .ColumnHeadersHeight + .Rows.Count * .Rows(0).Height
                dgvJoin.Top = .Top + .Height + 5
            End If
        End With

        With dgvJoin
            .DefaultCellStyle.Font = New Font(.Font.FontFamily, FontSize, FontStyle.Regular, GraphicsUnit.Point)
            .ColumnHeadersDefaultCellStyle.Font = New Font(.Font.FontFamily, FontSize, FontStyle.Regular, GraphicsUnit.Point)
            .AutoResizeColumns()
            .AutoResizeRows()
            .AutoResizeColumnHeadersHeight()
            .Height = Get_GridHeight(dgvJoin, TeamsCount, GroupDividersCount)
            If Wettkampf.Mannschaft Then
                .ColumnHeadersHeight += .Rows(0).Height
                For Each Label In dicLabels.Values
                    If Label.Visible Then
                        Label.Font = New Font(.Font.FontFamily, FontSize, FontStyle.Regular, GraphicsUnit.Point)
                    End If
                Next
                Set_TeamPanels(dgvJoin)
            End If
        End With

        Dim contents As New List(Of String)
        Try
            ' Fontsize in Screens.src ändern
            If File.Exists(ScreensFile) Then
                contents = File.ReadAllLines(ScreensFile, Encoding.UTF8).ToList

                For i = 0 To contents.Count - 1
                    Dim line = Split(contents(i), ";").ToList

                    If line(2).Equals(dicScreens(CurrentDisplay).Form.Text) Then
                        If line.Count = 5 Then
                            If line(4).Equals(FontSize.ToString) Then Return
                            line(4) = FontSize.ToString
                        ElseIf line.Count = 4 Then
                            line.Add(FontSize.ToString)
                        End If
                    End If
                    contents(i) = Join(line.ToArray, ";")
                Next
                File.Delete(ScreensFile)
            End If
            File.WriteAllLines(ScreensFile, contents.ToArray, Encoding.UTF8)
        Catch ex As Exception
        End Try

        ' Fontsize in dicScreens ändern
        If dicScreens.ContainsKey(CurrentDisplay) Then
            Dim vl = New ValueList With {.Form = dicScreens(CurrentDisplay).Form,
                                         .Display = dicScreens(CurrentDisplay).Display,
                                         .Fontsize = FontSize}
            dicScreens(CurrentDisplay) = vl
        End If

    End Sub

    Private Sub Heber_Highlight(Zeile As Integer)

        ' Hervorhebung des aktuellen Hebers in Tabelle Versuchsreihenfolge

        If Not Text.Contains("Versuchsreihenfolge") Then Return

        SuspendLayout()
        Try
            With dgvBest1
                .Rows(Zeile).DefaultCellStyle.ForeColor = Color.FromArgb(0, 240, 0) '(40, 164, 40) 'Lime

                For i As Integer = 1 To .Rows.Count - 1
                    Dim SetColor As Boolean
                    Select Case WK_Options.Markierung
                        Case 1 ' nächsten zwei Versuche
                            SetColor = i < 3
                        Case 2 ' Versuche des Hebers 
                            SetColor = CInt(dvLeader(Leader.TableIx)(i)("Teilnehmer")) = CInt(dvLeader(Leader.TableIx)(0)("Teilnehmer"))
                        Case 3 'Versuche mit gleicher Last
                            SetColor = CInt(dvLeader(Leader.TableIx)(i)("HLast")) = CInt(dvLeader(Leader.TableIx)(0)("HLast"))
                    End Select
                    If SetColor = True Then
                        .Rows(i).DefaultCellStyle.ForeColor = Color.FromArgb(180, 255, 180) '(204, 255, 204) 'Color.Yellow
                    Else
                        .Rows(i).DefaultCellStyle.ForeColor = Color.White
                    End If
                Next
                .AutoResizeRows()
                .SendToBack()
                .ClearSelection()
            End With
        Catch ex As Exception
        End Try
        ResumeLayout()
    End Sub
    Private Sub Heber_Dim(Zeile As Integer)
        If Not Text.Contains("Versuchsreihenfolge") Then Return
        If Zeile < 0 Then Return
        With dgvBest1
            .Rows(Zeile).DefaultCellStyle.BackColor = Color.DimGray
            .Rows(Zeile).DefaultCellStyle.ForeColor = Color.DarkGray
        End With
    End Sub

    Private Sub lblCaption_SizeChanged(sender As Object, e As EventArgs) Handles lblCaption.SizeChanged
        'With lblCaption
        '    Dim LimitLeft = lblDurchgang.Left + lblDurchgang.Width
        '    Dim LimitRight = xAufruf.Width
        '    If .Width >= Width - LimitLeft - LimitRight Then ' Bezeichnung zu lang
        '        If Not String.IsNullOrEmpty(Wettkampf.Kurz) Then
        '            lblCaption.Text = Wettkampf.Kurz '& " (" & User.Bohle & ")"
        '        End If
        '        If .Width >= Width - LimitLeft - LimitRight Then ' Bezeichnung zu lang
        '            Using GF As New myFunction
        '                '.Text = GF.String_Shorten(.Text, .Font, Width - LimitLeft - LimitRight, True)
        '            End Using
        '        End If
        '    End If
        '    .Left = (Width - LimitLeft - LimitRight - .Width) \ 2 + LimitLeft
        'End With

        'If Not IsNothing(Wettkampf.Identities) AndAlso Wettkampf.Identities.Count > 1 Then
        '    dgvJoin.Top = lblCaption.Top + lblCaption.Height + 10
        'End If
    End Sub
    Private Sub lblCaption_TextChanged(sender As Object, e As EventArgs) Handles lblCaption.TextChanged
        'Debug.WriteLine(lblCaption.Text)
        'Debug.WriteLine(lblCaption.Top + lblCaption.Height)
        'Debug.WriteLine(dgvJoin.Top)
    End Sub

    Private Sub Wettkampf_Changed(Id As Integer)
        If InvokeRequired Then
            Dim d As New Delegate_Integer(AddressOf Wettkampf_Changed)
            Invoke(d, New Object() {Id})
        Else
            If Id = 0 Then
                With dgvJoin
                    .Rows.Clear()
                    .Columns.Clear()
                End With
            End If
        End If
    End Sub
    Private Sub Bohle_Changed()
        Adjust_Caption()
    End Sub
    Private Sub Durchgang_Changed(Durchgang As String)

        If Not String.IsNullOrEmpty(Durchgang) Then
            Dim dg As New List(Of String)
            dg.AddRange({"R", "S"})
            If Leader.Durchgang.Equals("Stoßen") Then dg.Reverse()

            With dgvJoin
                If .ColumnCount = 0 Then Gruppe_Changed(Leader.Gruppe)
                Try
                    If Text.Contains("Bestenliste") OrElse
                        Text.Contains("Versuchsreihenfolge") OrElse
                        Text.Contains("2.Wertung") Then
                        For i = 1 To 3
                            ' Spalten des Durchgangs einblenden
                            .Columns(dg(0) & "_" & i).Visible = True
                            If .Columns.Contains(dg(0) & "T_" & i) Then .Columns(dg(0) & "T_" & i).Visible = Wettkampf.Technikwertung
                            ' Spalten des anderen Durchgangs ausblenden
                            .Columns(dg(1) & "_" & i).Visible = False
                            If .Columns.Contains(dg(1) & "T_" & i) Then .Columns(dg(1) & "T_" & i).Visible = False
                        Next
                        ' überflüssige Spalten ausblenden
                        If dg(0).Equals("R") Then
                            .Columns("Reissen").Visible = Wertung_1 > 1 AndAlso Wertung_1 < 5 OrElse Wertung_1 = 6
                            .Columns("Stossen").Visible = False
                        Else
                            .Columns("Reissen").Visible = False
                            .Columns("Stossen").Visible = Wertung_1 > 1 AndAlso Wertung_1 < 5 OrElse Wertung_1 = 6
                        End If
                        If .Columns.Contains("ZK") Then .Columns("ZK").Visible = Wertung_1 = 1 Or Wertung_1 = 5
                        '.Columns("Heben").Visible = False
                        'If .Columns.Contains("Heben_Platz") Then .Columns("Heben_Platz").Visible = False
                        If .Columns.Contains("Athletik") Then .Columns("Athletik").Visible = False
                        If .Columns.Contains("Gesamt") Then .Columns("Gesamt").Visible = Text.Contains("Versuchsreihenfolge")

                        If .Columns.Contains("Gesamt_Platz") Then .Columns("Gesamt_Platz").Visible = Text.Contains("Versuchsreihenfolge")
                    End If

                    .AutoResizeColumns()
                    .AutoResizeColumnHeadersHeight()
                    If dicScreens.ContainsKey(CurrentDisplay) AndAlso dicScreens(CurrentDisplay).Fontsize > 0 Then
                        CurrentFontsize = dicScreens(CurrentDisplay).Fontsize
                    Else
                        CurrentFontsize = .DefaultCellStyle.Font.Size * .Width / Get_TextWidth()
                    End If
                    Change_FontSize(CurrentFontsize)

                    ' dgv1 aktualisieren, wenn Versuchsreihenfolge angezeigt wird und Publikum geöffnet wurde, bevor Leader.Durchgang eingelesen war
                    If Text.Contains("Versuchsreihenfolge") Then
                        dgvBest1.DataSource = bsLeader
                        If Not IsNothing(bsLeader) Then
                            dgvBest1.Height = Get_GridHeight(dgvBest1,,, bsLeader.Count)
                            If bsLeader.Count > 0 Then pnlBest1.Height = dgvBest1.Top + dgvBest1.Height
                        End If
                    End If

                Catch ex As Exception
                End Try
            End With
            lblDurchgang.Text = If(String.IsNullOrEmpty(lblGruppe.Text), String.Empty, "-  ") & Leader.Durchgang
        Else
            lblDurchgang.Text = String.Empty
        End If
        Adjust_Caption()
    End Sub

    Private Sub Adjust_Caption()
        Dim LimitLeft = lblDurchgang.Left + lblDurchgang.Width
        Dim LimitRight = 0
        With lblCaption
            Using GF As New myFunction
                '.Text = GF.String_Shorten(If(String.IsNullOrEmpty(Wettkampf.Kurz), Wettkampf.Bezeichnung, Wettkampf.Kurz),
                .Text = GF.String_Shorten(Join(Wettkampf.Identities.Values.ToArray, ", "),
                                          .Font,
                                          Width - LimitLeft - LimitRight,
                                          If(Wettkampf.MultiBohle, " (" & User.Bohle & ")", ""))
            End Using
            .Left = (Width - LimitLeft - LimitRight - .Width) \ 2 + LimitLeft
        End With
    End Sub

    '' Hier wird das Layout der Anzeige gebaut
    Private Sub Gruppe_Changed(Gruppe As List(Of String))

        If InvokeRequired Then
            Dim d As New Delegate_List_String(AddressOf Gruppe_Changed)
            Invoke(d, New Object() {Gruppe})
        Else
            Try

                'If Not Wettkampf.Mannschaft Then
                '    lblCaption.Text = Wettkampf.Identities.Values(0) 'Bezeichnung ' & " (" & User.Bohle & ")"
                'End If
                dgvJoin.BringToFront()

                If IsNothing(dvResults) Then Return ' leere Anzeige

                Adjust_Caption()

#Region "NK-Stellen"
                'If Wettkampf.Platzierung = 0 Then
                Dim found = Glob.dtModus.Select("idModus = " & Wettkampf.Modus)
                If found.Length > 0 Then Wertung_1 = CInt(found(0)!Wertung)
                'Else
                '    Wertung = CInt(Wettkampf.Platzierung.ToString.Substring(0, 1))
                'End If
                NK_String = "F" & Get_NK_Stellen(Wertung_1)

                If Wettkampf.Platzierung.ToString.Length > 2 Then
                    Dim w = CInt(Wettkampf.Platzierung.ToString.Substring(2, 1))
                    NK_String_2 = "F" & Get_NK_Stellen(w)
                End If
#End Region
                If dgvJoin.Rows.Count > 0 Then
                    dgvJoin.Rows.Clear()
                ElseIf Wettkampf.Mannschaft OrElse Not IsNothing(Leader.Gruppe) AndAlso Leader.Gruppe.Count > 0 Then
                    Build_Grid()
                End If

                If Not IsNothing(Gruppe) AndAlso Not Gruppe.Count = 0 Then
                    lblGruppe.Text = "Gruppe " + String.Join(", ", Gruppe)
                    lblDurchgang.Left = lblGruppe.Left + lblGruppe.Width
                Else
                    lblGruppe.Text = String.Empty
                    lblDurchgang.Text = String.Empty
                    dgvJoin.Rows.Clear()
                End If

                With dgvJoin
                    '.SuspendLayout()
                    Dim pos = 0
                    If .ColumnCount = 0 Then Return
                    If dvResults.Count = 0 Then Return
                    Dim grp = 0
                    Dim team = -1
                    Try
                        grp = CInt(dvResults(0)!Gruppe)
                        team = CInt(dvResults(0)!idVerein)
                    Catch ex As Exception
                        ' hier könnte eine Fehlermeldung kommen, wenn Gruppe oder Team NULL ist (keine Gruppe oder kein Team)
                        LogMessage("Publilkum:Gruppe_Changed: Gruppe/Team: " & ex.Message, ex.StackTrace)
                    End Try

#Region "Heber einlesen"
                    Dim img As Bitmap = Nothing
                    Dim EmergencyLifters = New List(Of Integer)

                    For Each row As DataRowView In dvResults
                        Try
                            If CInt(row!Gruppe) = 1000 AndAlso Not IsDBNull(row!idVerein) AndAlso CInt(row!idVerein) = team Then
                                ' Mannschafts-WK: Ersatzheber
                                EmergencyLifters.Add(dgvJoin.Rows.Count)
                            ElseIf CInt(row!Gruppe) <> grp Then
                                grp = CInt(row!Gruppe)
                                team = CInt(row!idVerein)
                                GroupDividersCount += 1
                                .Rows(.Rows.Count - 1).DividerHeight = GroupDivider_Height
                            End If
                            Dim vReissen As Object = DBNull.Value
                            Dim vStossen As Object = DBNull.Value
                            Dim vHeben As Object = DBNull.Value
                            Dim params As New List(Of Object)

                            If Not IsDBNull(row!RW_1) AndAlso CInt(row!RW_1) > -2 Then vReissen = row!Reissen ' mindestens 1 gewerteter Versuch & nicht alle Versuche verzichtet
                            If Not IsDBNull(row!SW_1) AndAlso CInt(row!SW_1) > -2 Then vStossen = row!Stossen ' mindestens 1 gewerteter Versuch & nicht alle Versuche verzichtet
                            If Not IsDBNull(vReissen) OrElse Not IsDBNull(vStossen) Then vHeben = row!Heben

                            For Each col As DataGridViewColumn In .Columns
                                'Debug.WriteLine(col.Name & vbTab & col.DataPropertyName)
                                If col.Name.Equals("Flagge") Then
                                    params.Add(img)
                                ElseIf col.Name.Equals("Reissen") Then
                                    params.Add(vReissen)
                                ElseIf col.Name.Equals("Stossen") Then
                                    params.Add(vStossen)
                                ElseIf col.Name.Equals("Heben") Then
                                    params.Add(vHeben)
                                ElseIf String.IsNullOrEmpty(col.DataPropertyName) Then
                                    params.Add(DBNull.Value)
                                ElseIf dtResults.Columns.Contains(col.DataPropertyName) Then
                                    params.Add(row(col.DataPropertyName))
                                End If
                            Next
                            .Rows.Add(params.ToArray)
                        Catch ex As Exception
                            LogMessage("Publilkum: Gruppe_Changed: Heber_eintragen: " & ex.Message, ex.StackTrace)
                        End Try
                    Next

                    If .Rows.Count = 0 Then Return ' keine DataSource

                    .Rows(.Rows.Count - 1).DividerHeight = 1 ' Strich unter Tabelle

                    ' Einträge formatieren
                    For r = 0 To .Rows.Count - 1
                        For c = 0 To .Columns.Count - 1
                            CellFormatting(r, .Columns(c).Name)
                        Next
                    Next

                    Set_Anfangslast()

                    Change_NameFormat(dgvJoin)

                    For Each row In EmergencyLifters
                        dgvJoin.Rows(row).DefaultCellStyle.ForeColor = Color.DimGray
                    Next

                    '' überlange Vereinsname kürzen
                    'If .Columns.Contains("Verein") Then
                    '    With .Columns("Verein")
                    '        If .Visible Then
                    '            For Each row As DataGridViewRow In dgvJoin.Rows
                    '                If TextRenderer.MeasureText(row.Cells("Verein").Value.ToString, .DefaultCellStyle.Font).Width >= .Width Then ' Bezeichnung zu lang
                    '                    Using GF As New myFunction
                    '                        row.Cells("Verein").Value = GF.String_Shorten(row.Cells("Verein").Value.ToString, .DefaultCellStyle.Font, .Width)
                    '                    End Using
                    '                End If
                    '            Next
                    '        End If
                    '    End With
                    'End If

                    If IsDisplayed AndAlso Not Wettkampf.Mannschaft Then Return
#End Region

#Region "Mannschafts-WK"
                    If Wettkampf.Mannschaft Then
                        Try
                            With dgvTeam
                                .Visible = True
                                .AutoGenerateColumns = False
                                .Columns("Gegner").Visible = Wettkampf.Finale
                                .Columns("Punkte2").Visible = Wettkampf.Finale
                                With .Columns("Team")
                                    .HeaderCell.Style.ForeColor = Color.LightBlue
                                    .HeaderText = Wettkampf.Bezeichnung.Substring(0, 1) & ". Bundesliga"
                                End With
                                .DataSource = Glob.dtTeams
                                .Height = .ColumnHeadersHeight + .Rows.Count * .Rows(0).Height
                                .Columns("mReissen").DefaultCellStyle.Format = NK_String
                                .Columns("mStossen").DefaultCellStyle.Format = NK_String
                                .Columns("mHeben").DefaultCellStyle.Format = NK_String
                                .Width = 0
                                For Each col As DataGridViewColumn In .Columns
                                    .Width += col.Width
                                Next
                                .ClearSelection()
                                .Location = New Point(0, 0)
                                dgvJoin.Top = .Top + .Height + 5
                            End With
                        Catch ex As Exception
                            dgvTeam.Visible = False
                        End Try

                        .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing
                        '.ColumnHeadersDefaultCellStyle.Padding = New Padding(0, 0, 0, 0)
                        '.ColumnHeadersHeight = CInt(1.65 * .Rows(0).Height) + 2
                        '.AutoResizeRows()
                        '.AutoResizeColumnHeadersHeight()
                        '.ColumnHeadersHeight = .ColumnHeadersHeight + .Rows(0).Height
                        .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter


                        TeamsCount = Glob.dtTeams.Rows.Count

                        lblGruppe.Visible = False
                        lblDurchgang.Visible = False
                        'lblCaption.TextAlign = ContentAlignment.TopLeft
                        lblCaption.Visible = False
                    End If
#End Region
                    ' Grid formatieren
                    .AutoResizeColumns()
                    .AutoResizeColumnHeadersHeight()
                    .Height = Get_GridHeight(dgvJoin, TeamsCount, GroupDividersCount)
                    .SendToBack()

                    If Wettkampf.Mannschaft Then
                        .ColumnHeadersHeight = .ColumnHeadersHeight + .Rows(0).Height
                        Set_TeamPanels(dgvJoin)

                        If dicScreens.Keys.Contains(CurrentDisplay) AndAlso dicScreens(CurrentDisplay).Fontsize > 0 Then
                            CurrentFontsize = dicScreens(CurrentDisplay).Fontsize
                        Else
                            CurrentFontsize = dgvJoin.DefaultCellStyle.Font.Size * dgvJoin.Width / Get_TextWidth()
                        End If
                        Change_FontSize(CurrentFontsize)

                        '.ResumeLayout()
                        '.Refresh()
                        Return
                    End If
                End With

#Region "Bestenliste"
                ' Wertung + Bestenliste
                ' Wertung + Bestenliste + 2.Wertung
                ' Wertung + 2.Wertung

                Dim Filter1 = String.Empty ' = gesamter Wettkampf

                Dim RowHeight = 0 ' RowHeight of ScoreLists 

                If Text.Contains("Bestenliste") Then
                    lblBest1.Text = Get_Wertung_Label(1)
                    Build_Total(dgvBest1)
                    dv1 = New DataView(dtResults)
                    bs1 = New BindingSource With {.DataSource = dv1}
                End If

                If Text.Contains("2.") Then
                    lblBest2.Text = Get_Wertung_Label(2)
                    Build_Total(dgvBest2)
                    dv2 = New DataView(dtResults)
                    bs2 = New BindingSource With {.DataSource = dv2}
                End If

                Dim Platzierung = Wettkampf.Platzierung.ToString

                If Text.Contains("Bestenliste") AndAlso Not Text.Contains("2.") Then
                    ' nur Bestenliste anzeigen --> Aufteilung = ganze Höhe
                    RowHeight = PanelBest_Resize(pnlBest1, dgvJoin.Top, Height)
                    If Platzierung.Substring(0, 1) = "3" Then
                        ' Platzierung_1 nach AK 
                        dgvBest1.DataSource = Get_DataByAK(dgvBest1.Height \ RowHeight)
                        Set_AK_Labels(dgvBest1)
                    Else
                        ' Platzierung_1 nicht nach AK 
                        dgvBest1.DataSource = bs1
                    End If
                ElseIf Text.Contains("Bestenliste") AndAlso Text.Contains("2.") Then
                    ' Bestenliste und Platzierung_2 anzeigen
                    If Platzierung.Length > 1 Then
                        ' Platzierung_2 --> Aufteilung = Höhe / 2
                        RowHeight = PanelBest_Resize(pnlBest1, dgvJoin.Top, (Height - dgvJoin.Top) \ 2)
                        PanelBest_Resize(pnlBest2, pnlBest1.Top + pnlBest1.Height, (Height - dgvJoin.Top) \ 2)
                        If Platzierung.Substring(2, 1) = "9" Then ' ################################### war 1,1
                            ' Platzierung_2 nach AK  
                            dgvBest2.DataSource = Get_DataByAK(dgvBest2.Height \ RowHeight)
                            Set_AK_Labels(dgvBest2)
                        Else
                            ' Platzierung_2 nicht nach AK 
                            dgvBest2.DataSource = bs2
                        End If
                    Else
                        ' keine Platzierung_2 in WK --> Aufteilung = ganze Höhe
                        RowHeight = PanelBest_Resize(pnlBest1, dgvJoin.Top, Height)
                    End If
                    If Platzierung.Substring(0, 1) = "3" Then
                        ' Platzierung_1 nach AK 
                        dgvBest1.DataSource = Get_DataByAK(dgvBest1.Height \ RowHeight)
                        Set_AK_Labels(dgvBest1)
                    Else
                        ' Platzierung_1 nicht nach AK 
                        dgvBest1.DataSource = bs1
                    End If
                ElseIf Text.Contains("2.") AndAlso Platzierung.Length > 1 Then
                    ' nur Platzierung_2 anzeigen --> Aufteilung = ganze Höhe
                    RowHeight = PanelBest_Resize(pnlBest2, dgvJoin.Top, Height)
                    If Platzierung.Substring(2, 1) = "9" Then ' ################################### war 1,1
                        ' Platzierung_2 nach AK  
                        dgvBest2.DataSource = Get_DataByAK(dgvBest2.Height \ RowHeight)
                        Set_AK_Labels(dgvBest2)
                    Else
                        ' Platzierung_2 nicht nach AK 
                        dgvBest2.DataSource = bs2
                    End If
                End If
#End Region
#Region "Versuchsreihenfolge"
                If Text.Contains("Versuchsreihenfolge") Then
                    lblBest1.Text = "   Versuchsreihenfolge   "
                    lblBest1.Left = 0 '(pnl1.Width - lbl1.Width) \ 2
                    pnlBest1.Location = New Point(0, dgvJoin.Top)
                    pnlBest1.Height = Height - pnlBest1.Top
                    pnlBest1.Width = lblBest1.Width
                    pnlBest1.Visible = True
                    dgvJoin.Left = pnlBest1.Width + 10
                    dgvJoin.Width = Width - dgvJoin.Left

                    Build_Versuch(dgvBest1)
                    dgvBest1.DataSource = bsLeader

                    dgvBest1.ColumnHeadersDefaultCellStyle.Font = New Font(dgvJoin.Font.FontFamily, dgvJoin.DefaultCellStyle.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
                    dgvBest1.DefaultCellStyle.Font = New Font(dgvJoin.Font.FontFamily, dgvJoin.DefaultCellStyle.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
                    If Not IsNothing(bsLeader) Then
                        dgvBest1.Height = Get_GridHeight(dgvBest1,,, bsLeader.Count)
                        If bsLeader.Count > 0 Then pnlBest1.Height = dgvBest1.Top + dgvBest1.Height
                    End If
                    dgvBest1.Width = pnlBest1.Width
                    Change_NameFormat(dgvBest1)
                End If
#End Region
                If Not IsNothing(bs1) Then
                    bs1.Filter = Get_Wertung1_Filter(Gruppe.ToArray, 0, 0)
                    bs1.Sort = "Heben_Platz_" & WK_ID
                End If
                If Not IsNothing(bs2) Then
                    bs2.Filter = Get_Wertung2_Filter()
                    bs2.Sort = "Wertung2_Platz_" & WK_ID
                End If

                '.AutoResizeColumns()
                '.AutoResizeColumnHeadersHeight()
                'Change_FontSize(.DefaultCellStyle.Font.Size * .Width / Get_TextWidth())

            Catch ex As Exception
                LogMessage("Publikum: Gruppe_Changed: " & ex.Message, ex.StackTrace)
                Close()
            End Try
        End If

    End Sub

    Private Function Get_TextWidth() As Single
        ' automatische Anpassung von FontSize 
        Dim TextWidth As Integer
        Dim NameCol As New List(Of String) '= String.Empty
        Dim NameWidth = 0
        For Each col As DataGridViewColumn In dgvJoin.Columns
            If col.Visible Then
                If LCase(col.Name).Contains("name") Then
                    NameCol.Add(col.Name)
                Else
                    TextWidth += TextRenderer.MeasureText(col.Name, dgvJoin.DefaultCellStyle.Font).Width
                End If
            End If
        Next
        For Each row As DataGridViewRow In dgvJoin.Rows
            For Each col In NameCol
                Dim tmp = TextRenderer.MeasureText(row.Cells(col).Value.ToString, dgvJoin.DefaultCellStyle.Font).Width
                If tmp > NameWidth Then
                    NameWidth = tmp
                End If
            Next
        Next
        TextWidth += NameWidth
        Return TextWidth
    End Function

    Private Function Get_TeamsCount() As Integer
        ' Gruppen bei Liga-WK
        Dim Teams = From x In dvResults.ToTable
                    Where x.Field(Of Integer)("TeamID") < 1000
                    Group x By Id = x.Field(Of Integer)("idVerein") Into Group
        Return Teams.Count
    End Function

    Private Sub Set_TeamPanels(Grid As DataGridView)

        ' Gruppen bei Liga-WK
        Dim Teams = From x In dvResults.ToTable
                    Where x.Field(Of Integer)("TeamID") < 1000
                    Group x By Id = x.Field(Of Integer)("idVerein"), Verein = x.Field(Of String)("Verein") Into Group
                    Select Id, Verein, Group.Count

        ' für Mannschafts-WK DividerHeight auf doppelte RowHeight festlegen, um Vereins-Namen anzuzeigen
        ' (ColumnHeader.Height ist bereits gesetzt)

        Dim pos = 0
        For c = 0 To Teams.Count - 2
            pos += Teams(c).Count
            Grid.Rows(pos - 1).DividerHeight = Grid.Rows(0).Height
        Next

        pos = -1
        For c = 0 To Teams.Count - 1
            Dim rect As Rectangle = Grid.GetRowDisplayRectangle(pos + 1, False)
            With dicLabels(c)
                .Text = Teams(c).Verein
                .Bounds = New Rectangle(rect.X,
                                        rect.Y + dgvJoin.Top - rect.Height + 1 - GroupDividersCount \ TeamsCount * GroupDivider_Height,
                                        rect.Width + 1,
                                        rect.Height - 2 + GroupDividersCount \ TeamsCount * GroupDivider_Height)
                .Visible = True
                .BringToFront()
            End With
            pos += Teams(c).Count
        Next
    End Sub

    Private Sub Set_AK_Labels(Grid As DataGridView)
        Dim AK = String.Empty
        Dim Data = CType(Grid.DataSource, DataTable)

        For i = 0 To Data.Rows.Count - 1
            If Not IsNumeric(Data.Rows(i)(0)) AndAlso Not Data.Rows(i)(0).ToString.Equals(AK) Then
                AK = Data.Rows(i)(0).ToString
                Dim rect = Grid.GetRowDisplayRectangle(i, True)
                Dim lbl = New Label With {
                    .AutoSize = False,
                    .BackColor = Color.Black,
                    .ForeColor = Color.Gold,
                    .Text = AK,
                    .Padding = New Padding(20, 0, 0, 0),
                    .Parent = Grid}
                lbl.SetBounds(rect.X, rect.Y, rect.Width, rect.Height)
                lbl.BringToFront()
            End If
        Next
    End Sub

    Private Function PanelBest_Resize(Panel As Panel, PanelTop As Integer, PanelHeight As Integer) As Integer
        Dim RowHeight As Integer
        Panel.Location = New Point(0, PanelTop)
        Panel.Height = PanelHeight
        Panel.Visible = True
        Panel.BringToFront()
        If Panel.Equals(pnlBest1) Then
            dgvBest1.Width = Panel.Width
            dgvBest1.Height = Panel.Height
            dgvBest1.Rows.Add(1)
            RowHeight = dgvBest1.Rows(0).Height
            dgvBest1.Rows.Clear()
        Else
            dgvBest2.Width = Panel.Width
            dgvBest2.Height = Panel.Height - dgvBest2.Top
            dgvBest2.Rows.Add(1)
            RowHeight = dgvBest2.Rows(0).Height
            dgvBest2.Rows.Clear()
        End If
        dgvJoin.Left = Panel.Width + 10
        dgvJoin.Width = Width - dgvJoin.Left

        Return RowHeight
    End Function

    Private Sub Technik_Changed(T_Wertung As Boolean)

        If Not Wettkampf.Technikwertung AndAlso Not IsNothing(dvDelete(Leader.TableIx)) AndAlso dvDelete(Leader.TableIx).Count > 0 Then Return ' Technikwertung während des WKs ausgeschaltet

        If (Wettkampf.Mannschaft OrElse Not IsNothing(Leader.Gruppe) AndAlso Leader.Gruppe.Count > 0) AndAlso (Wettkampf.Mannschaft OrElse Not String.IsNullOrEmpty(Leader.Durchgang)) Then
            Build_Grid()
        End If

        Gruppe_Changed(Leader.Gruppe)

    End Sub

    'Private Sub WK_BezeichungChanged(Bezeichnung As String)
    '    If InvokeRequired Then
    '        Dim d As New Delegate_String(AddressOf WK_BezeichungChanged)
    '        Invoke(d, New Object() {Bezeichnung})
    '    Else
    '        If Not Wettkampf.Mannschaft Then
    '            lblCaption.Text = Wettkampf.Identities.Values(0) 'Bezeichnung '& " (" & User.Bohle & ")"
    '        End If
    '        dgvJoin.BringToFront()
    '    End If

    'End Sub

    Private Sub Lifter_Exchange(Row As Integer, Group As Integer)
        If InvokeRequired Then
            Dim d As New Delegate_Integer2(AddressOf Lifter_Exchange)
            Invoke(d, New Object() {Row, Group})
        Else
            dgvJoin.Rows.Clear()
            Gruppe_Changed(Leader.Gruppe)
        End If
    End Sub

    Private Sub MasterData_Changed(pos As Integer, Nachname As String, Vorname As String, Sex As String, Verein As String, Jahrgang As Integer, AK As String)
        With dgvJoin.Rows(pos)
            .Cells("Nachname").Value = Nachname
            .Cells("Vorname").Value = Vorname
            .Cells("Sex").Value = Sex
            If dgvJoin.Columns.Contains("Verein") Then .Cells("Verein").Value = Verein
            If dgvJoin.Columns.Contains("Jahrgang") Then .Cells("Jahrgang").Value = Jahrgang
            If dgvJoin.Columns.Contains("AK") Then .Cells("AK").Value = AK
        End With
    End Sub

    Private Sub Change_Teamwertung()
        '' wird in frmLeader.OrderTable ausgelöst
        'Dim punkte = (From x In Glob.dtTeams Select p = x.Field(Of Integer)("Punkte").ToString).ToArray
        'lblWertung.Text = Join(punkte, "  ")
    End Sub

    Private Sub Set_Anfangslast()
        Dim col As String
        Dim dis = {"R", "S"}
        Dim HLast As Integer

        For row = 0 To dvResults.Count - 1
            For i = 0 To 1
                If Not IsNothing(dvLeader(i)) AndAlso dvLeader(i).Count > 0 Then
                    For c = 1 To 3
                        Try
                            col = dis(i) & "_" & c
                            If IsDBNull(dvResults(row)(col.Insert(1, "W"))) Then
                                If IsDBNull(dvResults(row)(col)) Then
                                    ' HLast von dtLeader -> nicht bestätigt
                                    Dim _tmp = dtLeader(i).Select("Teilnehmer = " & dvResults(row)("Teilnehmer").ToString & " And Versuch = " & c)
                                    HLast = CInt(_tmp(0)("HLast"))
                                    If Not CInt(dvResults(row)!Gruppe) = 1000 Then dgvJoin.Rows(row).Cells(col).Style.ForeColor = Color_Not_Declared
                                Else
                                    ' HLast von dvResults -> bestätigt
                                    HLast = CInt(dvResults(row)(col))
                                    If Not CInt(dvResults(row)!Gruppe) = 1000 Then dgvJoin.Rows(row).Cells(col).Style.ForeColor = Color_Declared
                                End If
                                dgvJoin.Rows(row).Cells(col).Value = HLast
                                Exit For
                            ElseIf CInt(dvResults(row)(col.Insert(1, "W"))) = -2 Then
                                'dgvJoin.Rows(row).Cells(col).Value = DBNull.Value
                                CellFormatting(row, col)
                            End If
                        Catch ex As Exception
                        End Try
                    Next
                End If
            Next
        Next
    End Sub
    Private Sub Change_Anfangslast(HLast As Integer, Row As Integer, Col As String, Optional Declared? As Boolean = False, Optional Panik As Boolean = False)
        If dgvJoin.Rows.Count = 0 Then Return
        With dgvJoin.Rows(Row).Cells(Col)
            If Panik AndAlso (LifterPositions.Count > 0 AndAlso LifterPositions(0).Key = Row) Then
                .Style.ForeColor = dgvJoin.DefaultCellStyle.ForeColor
            ElseIf Declared Then
                .Style.ForeColor = Color_Declared
            ElseIf Not Declared Then
                .Style.ForeColor = Color_Not_Declared
            End If
            .Value = HLast
            ' Verzicht
            If HLast = 0 Then
                '.Value = DBNull.Value
                CellFormatting(Row, Col)
            End If
        End With
        Refresh()
    End Sub
    Private Sub CellFormatting(Row As Integer, Col As String)

        ' formatiert HLast, nächste HLast, Techniknote, Results, Sex, Gruppe
        Try
            With dgvJoin.Rows(Row).Cells(Col)
                If Col.Contains("Sex") Then
                    If Not CInt(dvResults(Row)!Gruppe) = 1000 Then .Style.ForeColor = SexColor(.Value.ToString)
                ElseIf Col.Contains("T_") Then
                    ' Techniknote
                    If Not IsDBNull(.Value) Then
                        If CDbl(.Value) > 0 Then
                            If CDbl((.Value)) = 10 Then
                                .Style.Format = "0.0"
                            Else
                                .Style.Format = "0.00"
                            End If
                        ElseIf CDbl(.Value) = 0 Then
                            .Value = DBNull.Value
                        Else
                            .Style.Format = "-"
                        End If
                    End If
                ElseIf Col.Contains("R_") OrElse Col.Contains("S_") Then
                    If Not IsDBNull(dvResults(Row)(Col.Insert(1, "W"))) AndAlso CInt(dvResults(Row)(Col.Insert(1, "W"))) = -2 Then
                        ' Verzicht
                        .Style.ForeColor = dgvJoin.DefaultCellStyle.ForeColor
                        .Value = "---"
                    ElseIf IsDBNull(dvResults(Row)(Col)) OrElse CInt(dvResults(Row)(Col)) = -1 Then
                        ' gelöscht
                        .Style.ForeColor = dgvJoin.DefaultCellStyle.ForeColor
                        .Value = DBNull.Value
                    End If
                ElseIf "ReissenStossenHebenGesamt".Contains(Col) Then
                    ' Verzicht oder Nichts
                    If Col.Equals("Reissen") AndAlso Not IsDBNull(dvResults(Row)(Col)) AndAlso CInt(dvResults(Row)(Col)) = 0 AndAlso Not IsDBNull(dvResults(Row)("RW_1")) AndAlso CInt(dvResults(Row)("RW_1")) = -2 Then
                        .Value = "---"
                        '.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    ElseIf Col.Equals("Stossen") AndAlso Not IsDBNull(dvResults(Row)(Col)) AndAlso CInt(dvResults(Row)(Col)) = 0 AndAlso Not IsDBNull(dvResults(Row)("SW_1")) AndAlso CInt(dvResults(Row)("SW_1")) = -2 Then
                        .Value = "---"
                        '.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    ElseIf Col.Equals("Heben") AndAlso Not IsDBNull(dvResults(Row)(Col)) AndAlso CInt(dvResults(Row)(Col)) = 0 AndAlso
                            Not IsDBNull(dvResults(Row)("RW_1")) AndAlso CInt(dvResults(Row)("RW_1")) = -2 AndAlso
                            (IsDBNull(dvResults(Row)("SW_1")) OrElse CInt(dvResults(Row)("SW_1")) = -2) Then
                        .Value = "---"
                        '.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    ElseIf Col.Equals("Gesamt") AndAlso Wettkampf.Athletik Then
                        'Stop
                        '.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    ElseIf IsDBNull(.Value) Then
                        .Value = DBNull.Value
                    Else
                        '.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                    End If
                ElseIf Col.Equals("Gruppe") Then
                    If CInt(dvResults(Row)(Col)) = 1000 Then .Value = DBNull.Value
                End If
            End With
        Catch ex As Exception
            LogMessage("Publikum: CellFormatting: " & ex.Message, ex.StackTrace)
        End Try
    End Sub

    Private Function Get_Wertung1_Filter(Gruppe() As String, idAK As Integer, idGK As Integer) As String
        Dim Filter = String.Empty
        Dim Group = String.Empty
        If Not IsNothing(Gruppe) AndAlso Not Gruppe.Length = 0 Then
            Group = Join(Gruppe, " And Gruppe = ")
        End If
        If Wettkampf.SameAK > 0 Then idAK = Wettkampf.SameAK

        Using RS As New Results
            Filter = RS.Get_Wertung1_Filter("", 0, Group, 0, idAK, idGK)
            If Not String.IsNullOrEmpty(Filter) Then Filter = " And " & Filter
            Filter = Filter & " And Convert(IsNull(Heben_Platz_" & WK_ID & ", ''), System.String) <> ''"
        End Using
        Return "Wettkampf LIKE '%" & WK_ID & "%'" & Filter
    End Function
    Private Function Get_Wertung2_Filter() As String
        Dim Filter = String.Empty
        Using RS As New Results
            Filter = RS.Get_Wertung2_Filter(, WK_ID)
            Filter += " AND Convert(IsNull(Wertung2_Platz_" & WK_ID & ", ''), System.String) <> ''"
        End Using
        Return Filter
    End Function

    Class Altersklasse
        Property Ak As String
        Property Sex As String
        Property Gr As Integer?
    End Class
    Private Function Get_AKs() As List(Of Altersklasse)

        Dim tmp = From x In dvResults.ToTable
                  Where x.Field(Of String)("Wettkampf").Contains(WK_ID)
                  Order By x.Field(Of Integer?)("Gruppierung"), x.Field(Of Integer)("idAK"), x.Field(Of String)("Sex")
                  Group By AKs = New With {
                      Key .Ak = x.Field(Of String)("AK").Split({CChar(" ")})(0),
                      Key .Sex = x.Field(Of String)("Sex"),
                      Key .Gr = x.Field(Of Integer?)("Gruppierung")
                  } Into Group
                  Select AKs.Ak, AKs.Sex, AKs.Gr

        Dim Result As New List(Of Altersklasse)
        Dim Grp = -1

        For Each t In tmp
            If Result.Count = 0 OrElse Not Result(Result.Count - 1).Gr = t.Gr OrElse Not Result(Result.Count - 1).Sex.Equals(t.Sex) Then
                Result.Add(New Altersklasse With {.Ak = t.Ak, .Sex = t.Sex, .Gr = t.Gr})
            Else
                Result(Result.Count - 1).Ak += ", " + t.Ak
            End If
        Next

        Return Result
    End Function

    Private Function Get_DataByAK(RowCount As Integer) As DataTable
        Dim AK_List = Get_AKs()
        Dim AK_RowsCount = RowCount \ AK_List.Count
        Dim RowNum = 0
        Dim Data = dtResults.Clone
        Data.Columns("a_k").AllowDBNull = True
        Data.Columns("TeamID").AllowDBNull = True

        For Each Item In AK_List
            Dim NewRow = dtResults.NewRow.ItemArray
            NewRow(0) = Item.Ak & " (" & Item.Sex & ")"
            NewRow(2) = RowNum
            Data.Rows.Add(NewRow)
            RowNum -= 1
            Dim AK_Names = Split(Item.Ak, ", ")
            Dim AK_String = "(AK LIKE '%" & Join(AK_Names, "%' OR AK LIKE '%") & "%') AND "
            Dim Sex_String = If(Wettkampf.IgnoreSex, "", "Sex = '" & Item.Sex & "' AND ")
            Dim RowFilter = "Wettkampf LIKE '%" & WK_ID & "%' AND " &
                             AK_String &
                             Sex_String &
                             "Convert(IsNull((Wertung2_Platz_" & WK_ID & "),''), System.String) <> ''"
            Dim tmp = New DataView(dvResults.ToTable, RowFilter, "Wertung2_Platz_" & WK_ID, DataViewRowState.CurrentRows).ToTable
            For i = 0 To AK_RowsCount - 2
                If i < tmp.Rows.Count Then
                    'tmp(i).ItemArray(0) = Item.Ak & " (" & Item.Sex & ")"
                    Data.Rows.Add(tmp(i).ItemArray)
                Else
                    NewRow(2) = RowNum
                    Data.Rows.Add(NewRow)
                End If
                RowNum -= 1
            Next
        Next
        Return Data
    End Function

    Private Function Get_Wertung_Label(Liste As Integer) As String ' Bestenliste 1 oder 2
        Liste -= 1
        Dim Platzierung = "0"
        If Wettkampf.Platzierung.ToString.Length > Liste Then Platzierung = Wettkampf.Platzierung.ToString.Substring(Liste, 1)
        Dim Row = Glob.dtWertung_Bezeichnung.Select("Platzierung = " & Platzierung & " And International = " & Wettkampf.International)
        Return Row(0)!Headline.ToString & Row(0)!Bezeichnung.ToString
    End Function

    Private Sub Mark_NextHebers(Positions As List(Of KeyValuePair(Of Integer, String)))

        ' LifterPositions enthält die aktuell markierten Zeilen (0 = aktueller, 1 = nächster)
        ' die werden zurückgesetzt
        ' dann wird LifterPositions die zu markierenden Zeilen zugewiesen und markiert

        With dgvJoin
            Try
                For i = 0 To LifterPositions.Count - 1
                    .Rows(LifterPositions(i).Key).Cells("Startnummer").Style.BackColor = Nothing
                    .Rows(LifterPositions(i).Key).Cells("Nachname").Style.BackColor = Nothing
                    .Rows(LifterPositions(i).Key).Cells("Vorname").Style.BackColor = Nothing
                    .Rows(LifterPositions(i).Key).Cells("Sex").Style.BackColor = Nothing
                    If .Columns.Contains("Gruppe") Then .Rows(LifterPositions(i).Key).Cells("Gruppe").Style.BackColor = Nothing
                    If .Columns.Contains("Flagge") Then .Rows(LifterPositions(i).Key).Cells("Flagge").Style.BackColor = Nothing
                    .Rows(LifterPositions(i).Key).Cells("Verein").Style.BackColor = Nothing
                    If .Columns.Contains("AK") Then .Rows(LifterPositions(i).Key).Cells("AK").Style.BackColor = Nothing
                    If .Columns.Contains("KG") Then .Rows(LifterPositions(i).Key).Cells("KG").Style.BackColor = Nothing
                    If .Columns.Contains("GK") Then .Rows(LifterPositions(i).Key).Cells("GK").Style.BackColor = Nothing
                    If .Columns.Contains("Jahrgang") Then .Rows(LifterPositions(i).Key).Cells("Jahrgang").Style.BackColor = Nothing
                    If .Columns.Contains("Relativ") Then .Rows(LifterPositions(i).Key).Cells("Relativ").Style.BackColor = Nothing
                Next
                ' Markierung des letzten aktuellen Versuches löschen
                .Rows(LifterPositions(0).Key).Cells(LifterPositions(0).Value).Style.BackColor = Nothing
            Catch ex As Exception
            End Try

            If IsNothing(Positions) Then Return

            LifterPositions = Positions

            Dim Colors As New List(Of Color)
            Colors.Add(Color.FromArgb(0, 158, 0))
            Colors.Add(Color.FromArgb(199, 139, 0))
            Colors.Add(Color.FromArgb(139, 139, 109))

            Try
                For i = LifterPositions.Count - 1 To 0 Step -1
                    .Rows(LifterPositions(i).Key).Cells("Startnummer").Style.BackColor = Colors(i)
                    .Rows(LifterPositions(i).Key).Cells("Nachname").Style.BackColor = Colors(i)
                    .Rows(LifterPositions(i).Key).Cells("Vorname").Style.BackColor = Colors(i)
                    .Rows(LifterPositions(i).Key).Cells("Sex").Style.BackColor = Colors(i)
                    If .Columns.Contains("Gruppe") Then .Rows(LifterPositions(i).Key).Cells("Gruppe").Style.BackColor = Colors(i)
                    If .Columns.Contains("Flagge") Then .Rows(LifterPositions(i).Key).Cells("Flagge").Style.BackColor = Colors(i)
                    .Rows(LifterPositions(i).Key).Cells("Verein").Style.BackColor = Colors(i)
                    If .Columns.Contains("AK") Then .Rows(LifterPositions(i).Key).Cells("AK").Style.BackColor = Colors(i)
                    If .Columns.Contains("KG") Then .Rows(LifterPositions(i).Key).Cells("KG").Style.BackColor = Colors(i)
                    If .Columns.Contains("GK") Then .Rows(LifterPositions(i).Key).Cells("GK").Style.BackColor = Colors(i)
                    If .Columns.Contains("Jahrgang") Then .Rows(LifterPositions(i).Key).Cells("Jahrgang").Style.BackColor = Colors(i)
                    If .Columns.Contains("Relativ") Then .Rows(LifterPositions(i).Key).Cells("Relativ").Style.BackColor = Colors(i)
                Next
                ' Markierung des aktuellen Versuches
                .Rows(LifterPositions(0).Key).Cells(LifterPositions(0).Value).Style.BackColor = Colors(0)
                .Rows(LifterPositions(0).Key).Cells(LifterPositions(0).Value).Style.ForeColor = .DefaultCellStyle.ForeColor
            Catch ex As Exception
            End Try
            ' .ClearSelection()
        End With
    End Sub

    'Private Sub Update_From_Korrektur(CorrData As Correction)
    'End Sub

    Private Sub Update_TeamResults(Team As Integer)
        ' wird in frmLeader.Durchgang_beendet#Region "Results & Platzierung" ausgelöst
        ' dvResults wird in Leader geschrieben, bevor gespeichert wird --> Berechnung hier vornehmen, damit Leader.Save asynchron erfolgen könnte

        'Dim Summe(2) As Double?
        'Dim Wertung = {"Reissen", "Stossen", "Heben"}
        'For i = 0 To Wertung.Count - 1
        '    Dim ii = i
        '    Summe(ii) = (From x In dvResults.ToTable Where x.Field(Of Integer)("idVerein") = Team Select x.Field(Of Double?)(Wertung(ii))).Sum()
        '    If Summe(ii) > 0 Then dicLabels(Team)(ii + 1).Text = Format(Summe(ii), "#,##0.0")
        'Next

        ' // das war der letzte Stand, aber Resulte werden niht mehr in dgvJoin geschrieben
        'Using RS As New Results
        '    Dim lst = RS.Get_TeamResult(Team)
        '    For Each item In lst
        '        dicLabels(Team)(item.Key).Text = Format(item.Value, "#,##0.0")
        '    Next
        'End Using
        ' // Ende letzter Stand

        'Dim SumR = (From x In dvResults.ToTable Where x.Field(Of Integer)("idVerein") = Team Select x.Field(Of Double?)("Reissen")).Sum()
        'Dim SumS = (From x In dvResults.ToTable Where x.Field(Of Integer)("idVerein") = Team Select x.Field(Of Double?)("Stossen")).Sum()
        'Dim SumH = (From x In dvResults.ToTable Where x.Field(Of Integer)("idVerein") = Team Select x.Field(Of Double?)("Heben")).Sum()

        'If SumR > 0 Then dicLabels(Team)(1).Text = Format(SumR, "#,##0.0")
        'If SumS > 0 Then dicLabels(Team)(2).Text = Format(SumS, "#,##0.0")
        'If SumH > 0 Then dicLabels(Team)(3).Text = Format(SumH, "#,##0.0")

    End Sub

    '' Aktualisierung von Leader anzeigen
    Private Sub Wertung_Changed(Row As Integer,
                                Col As String,
                                Optional HLast As Integer = -1,
                                Optional Wertung As Integer = 0,
                                Optional Note As Double = 0,
                                Optional ZK As String = "",
                                Optional IsCurrent As Boolean = True)
        ', Optional Refreshing As Boolean = False) ', Optional nCol As String = "", Optional Refreshing As Boolean = False)

        With dgvJoin
            If .Rows.Count = 0 Then Return
            With .Rows(Row)
                If Wettkampf.Technikwertung AndAlso Heber.Wertung <> Wertung AndAlso Heber.T_Note = Note Then
                    ' nur Wertung geändert und in Results geschrieben
                    Refresh()
                    Return
                End If

                If Not String.IsNullOrEmpty(Col) Then ' AndAlso Wertung > -2 Then
                    ' Hantellast
                    If HLast > -1 Then
                        .Cells(Col).Value = HLast
                        .Cells(Col).Style.ForeColor = dgvJoin.DefaultCellStyle.ForeColor
                    ElseIf Wertung = -2 Then
                        .Cells(Col).Value = "---"
                    Else
                        .Cells(Col).Value = DBNull.Value
                    End If
                    ' Technik-Note
                    If Wettkampf.Technikwertung Then ' AndAlso Note > -1 AndAlso Wertung = 1 Then
                        .Cells(Col.Insert(1, "T")).Value = Note
                        CellFormatting(Row, Col.Insert(1, "T"))
                    End If
                End If

                ' ZK
                Try
                    If Not String.IsNullOrEmpty(ZK) Then
                        .Cells("ZK").Value = ZK
                    Else
                        .Cells("ZK").Value = DBNull.Value
                    End If
                Catch ex As Exception
                End Try

                ' Ergebnis - Reißen
                If Not IsDBNull(dvResults(Row)!Reissen) Then
                    If Not IsDBNull(dvResults(Row)!RW_1) AndAlso CInt(dvResults(Row)!RW_1) = -2 Then
                        ' Verzicht
                        .Cells("Reissen").Value = "---"
                        '.Cells("Reissen").Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    Else
                        .Cells("Reissen").Value = dvResults(Row)!Reissen
                        '.Cells("Reissen").Style.Alignment = DataGridViewContentAlignment.MiddleRight
                    End If
                Else
                    .Cells("Reissen").Value = DBNull.Value
                End If

                ' Ergebnis - Stoßen
                If Not IsDBNull(dvResults(Row)!Stossen) Then
                    If Not IsDBNull(dvResults(Row)!SW_1) AndAlso CInt(dvResults(Row)!SW_1) = -2 Then
                        ' Verzicht
                        .Cells("Stossen").Value = "---"
                        '.Cells("Stossen").Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    Else
                        .Cells("Stossen").Value = dvResults(Row)!Stossen
                        '.Cells("Stossen").Style.Alignment = DataGridViewContentAlignment.MiddleRight
                    End If
                Else
                    .Cells("Stossen").Value = DBNull.Value
                End If

                ' Ergebnis - Heben
                If Not IsDBNull(dvResults(Row)!Heben) Then
                    If Not IsDBNull(dvResults(Row)!RW_1) AndAlso CInt(dvResults(Row)!RW_1) = -2 AndAlso
                        (IsDBNull(dvResults(Row)!SW_1) OrElse CInt(dvResults(Row)!SW_1) = -2) Then
                        ' wenn 1.Versuch im Reißen verzichtet und 1.Versuch im Stoßen aussteht oder verzichtet ist
                        .Cells("Heben").Value = "---"
                        '.Cells("Heben").Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    ElseIf IsDBNull(dvResults(Row)!RW_1) AndAlso IsDBNull(dvResults(Row)!SW_1) Then
                        .Cells("Heben").Value = DBNull.Value
                    Else
                        .Cells("Heben").Value = dvResults(Row)!Heben
                        '.Cells("Stossen").Style.Alignment = DataGridViewContentAlignment.MiddleRight
                    End If
                Else
                    .Cells("Heben").Value = DBNull.Value
                End If
            End With

            If Not Wettkampf.Mannschaft Then
                'If Wettkampf.Athletik Then
                '    If Not IsDBNull(dvResults(Row)("Gesamt")) AndAlso CDbl(dvResults(Row)("Gesamt")) = 0 Then
                '        .Rows(Row).Cells("Gesamt").Value = "---"
                '    ElseIf Not IsDBNull(dvResults(Row)("Gesamt")) Then
                '        .Rows(Row).Cells("Gesamt").Value = dvResults(Row)("Gesamt")
                '    Else
                '        .Rows(Row).Cells("Gesamt").Value = DBNull.Value
                '    End If
                'End If

                ' 2. Wertung
                If .Columns.Contains("Wertung2") Then .Rows(Row).Cells("Wertung2").Value = dvResults(Row)!Wertung2

                ' Platzierung
                For i = 0 To .Rows.Count - 1
                    Try
                        If .Columns.Contains("Reissen_Platz") Then .Rows(i).Cells("Reissen_Platz").Value = dvResults(i)("Reissen_Platz_" & WK_ID)
                        If .Columns.Contains("Stossen_Platz") Then .Rows(i).Cells("Stossen_Platz").Value = dvResults(i)("Stossen_Platz_" & WK_ID)
                        If .Columns.Contains("Heben_Platz") Then .Rows(i).Cells("Heben_Platz").Value = dvResults(i)("Heben_Platz_" & WK_ID)
                        If .Columns.Contains("Gesamt_Platz") Then .Rows(i).Cells("Gesamt_Platz").Value = dvResults(i)("Gesamt_Platz_" & WK_ID)
                        If .Columns.Contains("Wertung2_Platz") Then .Rows(i).Cells("Wertung2_Platz").Value = dvResults(i)("Wertung2_Platz_" & WK_ID)
                    Catch ex As Exception
                        LogMessage("Publikum: Wertung_Changed: Platzierung: " & ex.Message, ex.StackTrace)
                    End Try
                Next

                ' 1. Bestenliste
                'If dgvBest1.DataSource.Equals(bs1) AndAlso (Wettkampf.Platzierung.ToString.Substring(0, 1).Equals("3") OrElse Wettkampf.Platzierung.ToString.Substring(0, 1).Equals("4")) Then
                '    Dim Gruppe = dvResults(Row)!Gruppe.ToString
                '    Dim idAK = CInt(dvResults(Row)!idAK)
                '    Dim idGK = CInt(dvResults(Row)!idGK)
                '    bs1.Filter = Get_Wertung1_Filter({Gruppe}, idAK, idGK)
                'ElseIf Not IsNothing(dgvBest1.DataSource) AndAlso TypeOf dgvBest1.DataSource Is DataTable Then
                '    dgvBest1.DataSource = Get_DataByAK(dgvBest1.Rows.Count)
                'End If

                ' 2. Bestenliste
                If Not IsNothing(dgvBest2.DataSource) AndAlso Not dgvBest2.DataSource.Equals(bs2) AndAlso TypeOf dgvBest2.DataSource Is DataTable Then
                    dgvBest2.DataSource = Get_DataByAK(dgvBest2.Rows.Count)
                End If

            End If

            If Not String.IsNullOrEmpty(Col) Then
                .AutoResizeColumns()
                'If Refreshing Then
                .Refresh()
                'End If
            End If

        End With
    End Sub
    Structure ColumnDef
        Dim Title As String
        Dim Field As String
        Dim Visible As Boolean
        Dim Align As DataGridViewContentAlignment
    End Structure

    Dim ColumnDefs As Dictionary(Of String, ColumnDef)

    Private Function Get_ShowCol_Gruppen() As Boolean
        Return (From x In dvResults.ToTable
                Group x By g = x.Field(Of Integer?)("Gruppe") Into Group).Count > 1 AndAlso
                                   Not (Wettkampf.Mannschaft OrElse
                                        Text.Contains("Bestenliste") OrElse
                                        Text.Contains("2.Wertung") OrElse
                                        Wettkampf.International)
    End Function
    Private Function Get_ShowCol_GK() As Boolean
        Return (From x In dvResults.ToTable
                Group x By g = x.Field(Of Integer?)("idGK") Into Group).Count > 1
    End Function
    Private Sub Build_Grid()

        If InvokeRequired Then
            Dim d As New Delegate_Sub(AddressOf Build_Grid)
            Invoke(d, New Object() {})
        Else

            If dgvJoin.ColumnCount > 0 Then dgvJoin.Columns.Clear()

#Region "PreRequisite"
            Dim ShowCol_Gruppen = Get_ShowCol_Gruppen()
            Dim ShowCol_GK = Get_ShowCol_GK()
            Dim ShowCol_Wertung2 = False
            Dim Show_ak = func.Show_a_K

            Dim _wertung = {String.Empty, String.Empty, String.Empty}
            Select Case Wertung_1
                Case 1
                    _wertung(0) = "" '  Robi  "
                    _wertung(1) = "" 'Robi"
                Case 2
                    _wertung(0) = "Relativ"
                    _wertung(1) = "Relativ"
                    ShowCol_GK = False
                Case 3, 4
                    _wertung(0) = " Sincl. "
                    _wertung(1) = "Sinclair"
                    ShowCol_GK = False
                    'Case 4
                    '    _wertung(0) = "  S-M  "
                    '    _wertung(1) = "S_M"
                Case 5
                    _wertung(0) = ""
                    _wertung(1) = "" '"Heben"
            End Select
            ' zweite Wertung
            If Wettkampf.Platzierung > 99 Then
                ShowCol_Wertung2 = Wertung_1 <> CInt(Wettkampf.Platzierung.ToString.Substring(2, 1))
                Select Case Wettkampf.Platzierung.ToString.Substring(2, 1)
                    Case "1"
                        _wertung(2) = "  Robi  "
                    Case "2"
                        _wertung(2) = " Relativ "
                    Case "3"
                        _wertung(2) = "  Sinc  "
                    Case "4"
                        _wertung(2) = "  S-M  "
                    Case "5"
                        _wertung(2) = "  ZK  "
                        ShowCol_Wertung2 = False ' ZK wird immer angezeigt
                    Case "9"
                        Select Case Wettkampf.Platzierung.ToString.Substring(1, 1)
                            Case "1"
                                _wertung(2) = "  Robi  "
                            Case "2"
                                _wertung(2) = " Relativ "
                            Case "3"
                                _wertung(2) = "  Sinc  "
                            Case "4"
                                _wertung(2) = "  S-M  "
                            Case "5"
                                _wertung(2) = "  ZK  "
                                ShowCol_Wertung2 = False ' ZK wird immer angezeigt
                            Case "9"
                        End Select
                End Select
            End If
#End Region

#Region "ColumnDefs"
            ColumnDefs = New Dictionary(Of String, ColumnDef)
            ColumnDefs("Gruppe") = New ColumnDef With {
                    .Title = " Gr.",
                    .Visible = ShowCol_Gruppen}
            ColumnDefs("Startnummer") = New ColumnDef With {
                    .Title = If(Wettkampf.International, " No.", " Nr."),
                    .Visible = True}
            ColumnDefs("Nachname") = New ColumnDef With {
                    .Title = "Name",
                    .Visible = True,
                    .Align = DataGridViewContentAlignment.MiddleLeft}
            ColumnDefs("Vorname") = New ColumnDef With {
                    .Title = "",
                    .Visible = If(Text.Contains("Bestenliste"), Not Wettkampf.International, True),
                    .Align = DataGridViewContentAlignment.MiddleLeft}
            ColumnDefs("Sex") = New ColumnDef With {
                    .Title = "",
                    .Visible = True}
            ColumnDefs("Flagge") = New ColumnDef With {
                    .Title = "         ",
                    .Visible = Wettkampf.Flagge}
            ColumnDefs("Verein") = New ColumnDef With {
                    .Title = If(Wettkampf.International, "Nation", "Verein/Team"),
                    .Visible = Not Wettkampf.Mannschaft,
                    .Field = If(Wettkampf.International, "state_iso3", "VereinKurz"),
                    .Align = DataGridViewContentAlignment.MiddleLeft}
            ColumnDefs(_wertung(1)) = New ColumnDef With {
                    .Title = "" & _wertung(0) & "",
                    .Visible = If(Wettkampf.Technikwertung OrElse Wettkampf.Athletik, False, Not String.IsNullOrEmpty(_wertung(0))),
                    .Align = DataGridViewContentAlignment.MiddleRight}
            ColumnDefs("Jahrgang") = New ColumnDef With {
                    .Title = If(Wettkampf.International, " Year ", "  JG  "),
                    .Visible = Not Wettkampf.Alterseinteilung.Equals("Altersklassen") AndAlso Not Wettkampf.Mannschaft}
            ColumnDefs("R_1") = New ColumnDef With {
                    .Title = "  1. ",
                    .Visible = True,
                    .Align = DataGridViewContentAlignment.MiddleCenter}
            ColumnDefs("RT_1") = New ColumnDef With {
                    .Title = "        ",
                    .Visible = Wettkampf.Technikwertung}
            ColumnDefs("R_2") = New ColumnDef With {
                    .Title = "  2. ",
                    .Visible = True,
                    .Align = DataGridViewContentAlignment.MiddleCenter}
            ColumnDefs("RT_2") = New ColumnDef With {
                    .Title = "        ",
                    .Visible = Wettkampf.Technikwertung}
            ColumnDefs("R_3") = New ColumnDef With {
                    .Title = "  3. ",
                    .Visible = True,
                    .Align = DataGridViewContentAlignment.MiddleCenter}
            ColumnDefs("RT_3") = New ColumnDef With {
                    .Title = "        ",
                    .Visible = Wettkampf.Technikwertung}
            ColumnDefs("S_1") = New ColumnDef With {
                    .Title = "  1. ",
                    .Visible = True,
                    .Align = DataGridViewContentAlignment.MiddleCenter}
            ColumnDefs("ST_1") = New ColumnDef With {
                    .Title = "        ",
                    .Visible = Wettkampf.Technikwertung}
            ColumnDefs("S_2") = New ColumnDef With {
                    .Title = "  2. ",
                    .Visible = True,
                    .Align = DataGridViewContentAlignment.MiddleCenter}
            ColumnDefs("ST_2") = New ColumnDef With {
                    .Title = "        ",
                    .Visible = Wettkampf.Technikwertung}
            ColumnDefs("S_3") = New ColumnDef With {
                    .Title = "  3. ",
                    .Visible = True,
                    .Align = DataGridViewContentAlignment.MiddleCenter}
            ColumnDefs("ST_3") = New ColumnDef With {
                    .Title = "        ",
                    .Visible = Wettkampf.Technikwertung}
            ColumnDefs("Reissen") = New ColumnDef With {
                    .Title = If(Wettkampf.International, "Snatch", "Reißen"),
                    .Visible = Wertung_1 > 1 AndAlso Wertung_1 < 5 OrElse Wertung_1 = 6,
                    .Align = DataGridViewContentAlignment.MiddleRight}
            ColumnDefs("Stossen") = New ColumnDef With {
                    .Title = If(Wettkampf.International, " C & J ", "Stoßen"),
                    .Visible = Wertung_1 > 1 AndAlso Wertung_1 < 5 OrElse Wertung_1 = 6,
                    .Align = DataGridViewContentAlignment.MiddleRight}
            ColumnDefs("ZK") = New ColumnDef With {
                    .Title = If(Wettkampf.International, "Total", " ZK "),
                    .Visible = Not Wettkampf.Technikwertung,
                    .Align = DataGridViewContentAlignment.MiddleRight}
            ColumnDefs("Heben_Platz") = New ColumnDef With {
                    .Title = "Pl.",
                    .Field = "Heben_Platz_" & WK_ID,
                    .Visible = Not Wettkampf.Mannschaft}
            ColumnDefs("Wertung2_Platz") = New ColumnDef With {
                    .Title = " 2.",
                    .Field = "Wertung2_Platz_" & WK_ID,
                    .Visible = False}
            ColumnDefs("a_K") = New ColumnDef With {
                    .Title = "    ",
                    .Visible = Show_ak,
                    .Align = DataGridViewContentAlignment.MiddleRight}

            If Wettkampf.Technikwertung Then
                ColumnDefs("AK") = New ColumnDef With {
                    .Title = "       AK       ",
                    .Visible = False}
                ColumnDefs("KG") = New ColumnDef With {
                    .Title = "   KG   ",
                    .Visible = Not ShowCol_GK,
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("GK") = New ColumnDef With {
                    .Title = "   GK   ",
                    .Visible = ShowCol_GK}
                ColumnDefs("Heben") = New ColumnDef With {
                    .Title = "  Heben  ",
                    .Visible = True,
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("Wertung2") = New ColumnDef With {
                    .Title = _wertung(2),
                    .Visible = False}

            ElseIf Wettkampf.Athletik Then ' alle DGJ-WKs mit Athletik ohne Technikwertung (Jugend)
                ColumnDefs("AK") = New ColumnDef With {
                    .Title = "       AK       ",
                    .Visible = False}
                ColumnDefs("KG") = New ColumnDef With {
                    .Title = "   KG   ",
                    .Visible = Not ShowCol_GK,
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("GK") = New ColumnDef With {
                    .Title = "   GK   ",
                    .Visible = ShowCol_GK}
                ColumnDefs("Heben") = New ColumnDef With {
                    .Title = "  Heben  ",
                    .Visible = True,
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("Wertung2") = New ColumnDef With {
                    .Title = _wertung(2),
                    .Visible = False}

            ElseIf Text.Contains("Bestenliste") OrElse
                   Text.Contains("2.Wertung") OrElse
                   Wettkampf.International Then
                ColumnDefs("AK") = New ColumnDef With {
                    .Title = If(Wettkampf.International, "   Age-Group    ", "       AK       "),
                    .Visible = Not Text.Contains("Bestenliste") AndAlso Text.Contains("2.Wertung")}
                ColumnDefs("KG") = New ColumnDef With {
                    .Title = If(Wettkampf.International, "   BW   ", "   KG   "),
                    .Visible = Not ShowCol_GK,
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("GK") = New ColumnDef With {
                    .Title = If(Wettkampf.International, "  Cat.  ", "   GK   "),
                    .Visible = ShowCol_GK}
                ColumnDefs("Heben") = New ColumnDef With {
                    .Title = If(Wertung_1 = 1, "  Robi  ", If(Wettkampf.International, " Result ", " Heben ")),
                    .Visible = Wertung_1 <> 5,
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("Wertung2") = New ColumnDef With {
                    .Title = _wertung(2),
                    .Visible = If(Wettkampf.International, ShowCol_Wertung2, False)}

                'ElseIf Text.Contains("2.Wertung") Then
                '    ColumnDefs("AK") = New ColumnDef With {
                '        .Title = "       AK       ",
                '        .Visible = True}
                '    ColumnDefs("KG") = New ColumnDef With {
                '        .Title = "   KG   ",
                '        .Visible = Not ShowCol_GK,
                '        .Align = DataGridViewContentAlignment.MiddleRight}
                '    ColumnDefs("GK") = New ColumnDef With {
                '        .Title = "   GK   ",
                '        .Visible = ShowCol_GK}
                '    ColumnDefs("Heben") = New ColumnDef With {
                '        .Title = " Heben ",
                '        .Visible = True,
                '        .Align = DataGridViewContentAlignment.MiddleRight}
                '    ColumnDefs("Wertung2") = New ColumnDef With {
                '        .Title = _wertung(2),
                '        .Visible = False}

                'ElseIf Wettkampf.International Then
                '    ColumnDefs("AK") = New ColumnDef With {
                '        .Title = "       AK       ",
                '        .Visible = True}
                '    ColumnDefs("KG") = New ColumnDef With {
                '        .Title = "   KG   ",
                '        .Visible = Not ShowCol_GK,
                '        .Align = DataGridViewContentAlignment.MiddleRight}
                '    ColumnDefs("GK") = New ColumnDef With {
                '        .Title = "   GK   ",
                '        .Visible = ShowCol_GK}
                '    ColumnDefs("Heben") = New ColumnDef With {
                '        .Title = " Heben ",
                '        .Visible = True,
                '        .Align = DataGridViewContentAlignment.MiddleRight}
                '    ColumnDefs("Wertung2") = New ColumnDef With {
                '        .Title = _wertung(2),
                '        .Visible = ShowCol_Wertung2}

            ElseIf Wettkampf.Mannschaft Then
                ColumnDefs("AK") = New ColumnDef With {
                    .Title = "           AK           ",
                    .Visible = False}
                ColumnDefs("KG") = New ColumnDef With {
                    .Title = "   KG   ",
                    .Visible = Not ShowCol_GK,
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("GK") = New ColumnDef With {
                    .Title = "     GK     ",
                    .Visible = ShowCol_GK}
                ColumnDefs("Heben") = New ColumnDef With {
                    .Title = " Heben ",
                    .Visible = Not String.IsNullOrEmpty(_wertung(0)),
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("Wertung2") = New ColumnDef With {
                    .Title = _wertung(2),
                    .Visible = False}

            ElseIf Wettkampf.Alterseinteilung.Equals("Altersklassen") Then
                ColumnDefs("AK") = New ColumnDef With {
                    .Title = "       AK       ",
                    .Visible = True}
                ColumnDefs("KG") = New ColumnDef With {
                    .Title = "   KG   ",
                    .Visible = Not ShowCol_GK,
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("GK") = New ColumnDef With {
                    .Title = "   GK   ",
                    .Visible = ShowCol_GK}
                ColumnDefs("Heben") = New ColumnDef With {
                    .Title = " Heben ",
                    .Visible = Wertung_1 < 5,
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("Wertung2") = New ColumnDef With {
                    .Title = _wertung(2),
                    .Visible = ShowCol_Wertung2}

            Else
                ColumnDefs("AK") = New ColumnDef With {
                    .Title = "       AK       ",
                    .Visible = False}
                ColumnDefs("KG") = New ColumnDef With {
                    .Title = "   KG   ",
                    .Visible = Not ShowCol_GK,
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("GK") = New ColumnDef With {
                    .Title = "   GK   ",
                    .Visible = ShowCol_GK}
                ColumnDefs("Heben") = New ColumnDef With {
                    .Title = " Heben ",
                    .Visible = True,
                    .Align = DataGridViewContentAlignment.MiddleRight}
                ColumnDefs("Wertung2") = New ColumnDef With {
                    .Title = _wertung(2),
                    .Visible = ShowCol_Wertung2}
                ColumnDefs("Wertung2_Platz") = New ColumnDef With {
                    .Title = "  2. ",
                    .Field = "Wertung2_Platz_" & WK_ID,
                    .Visible = Not String.IsNullOrEmpty(_wertung(2))}
            End If
#End Region

            '#Region "Columns.DataPropertyName"
            '            Dim Fields As New Dictionary(Of String, String)
            '            Fields("Gruppe") = "Gruppe"
            '            Fields("Startnummer") = "Startnummer"
            '            Fields("Nachname") = "Nachname"
            '            Fields("Vorname") = "Vorname"
            '            Fields("Sex") = "Sex"
            '            Fields("Flagge") = ""
            '            Fields("Verein") = If(Wettkampf.International, "state_iso3", "Verein")
            '            Fields("AK") = "AK"
            '            Fields("Jahrgang") = "Jahrgang"
            '            Fields("KG") = "KG"
            '            Fields("GK") = "GK"
            '            Fields(_wertung(1)) = _wertung(1)
            '            Fields("R_1") = "R_1"
            '            Fields("RT_1") = "RT_1"
            '            Fields("R_2") = "R_2"
            '            Fields("RT_2") = "RT_2"
            '            Fields("R_3") = "R_3"
            '            Fields("RT_3") = "RT_3"
            '            Fields("Reissen") = "Reissen"
            '            Fields("S_1") = "S_1"
            '            Fields("ST_1") = "ST_1"
            '            Fields("S_2") = "S_2"
            '            Fields("ST_2") = "ST_2"
            '            Fields("S_3") = "S_3"
            '            Fields("ST_3") = "ST_3"
            '            Fields("Stossen") = "Stossen"
            '            Fields("ZK") = "ZK"
            '            Fields("Heben") = "Heben"
            '            Fields("Heben_Platz") = "Heben_Platz_" & WK_ID
            '            Fields("Wertung2") = "Wertung2"
            '            Fields("Wertung2_Platz") = "Wertung2_Platz_" & WK_ID
            '            Fields("a_K") = "a_K"
            '#End Region

            With dgvJoin
#Region "Design"
                Try
                    Select Case Design
                        Case "Standard"
                            .AlternatingRowsDefaultCellStyle.BackColor = Color.OldLace
                            .RowsDefaultCellStyle.BackColor = Color.LightCyan
                            .BackgroundColor = SystemColors.AppWorkspace
                            .GridColor = SystemColors.ControlDark
                            With .DefaultCellStyle
                                .ForeColor = SystemColors.ControlText
                                .SelectionBackColor = Color.LightCyan
                                .SelectionForeColor = SystemColors.ControlText
                            End With
                        Case "Nacht"
                            .AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(40, 50, 60)
                            .RowsDefaultCellStyle.BackColor = Color.FromArgb(15, 15, 50) 'Color.Black
                            .BackgroundColor = Color.FromArgb(15, 15, 90)
                            With .DefaultCellStyle
                                .ForeColor = Color.White
                                .SelectionBackColor = Color.White
                                .SelectionForeColor = Color.Black
                            End With
                            .GridColor = Color.White
                    End Select
                Catch ex As Exception
                End Try
                '.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells 'ColumnHeader 
                .AutoGenerateColumns = False
#End Region

#Region "Columns"
                .Columns.Add(New CustomColumn_LeftRightBorder With {.Name = "Gruppe"})
                .Columns.Add(New CustomColumn_LeftRightBorder With {.Name = "Startnummer"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .FillWeight = 100, .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .FillWeight = 100, .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                If Wettkampf.Flagge Then
                    .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Sex"})
                Else
                    .Columns.Add(New CustomColumn_RightBorder With {.Name = "Sex"})
                End If
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Flagge"})
                If Wettkampf.International Then
                    .Columns.Add(New CustomColumn_RightBorder With {.Name = "Verein"})
                Else
                    .Columns.Add(New CustomColumn_RightBorder With {.Name = "Verein", .FillWeight = 100, .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
                End If
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "AK"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "Jahrgang"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "KG"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "GK"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = Trim(_wertung(1))})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "R_1"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "RT_1"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "R_2"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "RT_2"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "R_3"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "RT_3"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "Reissen"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "S_1"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "ST_1"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "S_2"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "ST_2"})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "S_3"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "ST_3"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "Stossen"})
                .Columns.Add(New CustomColumn_LeftRightBorder With {.Name = "ZK"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "Heben"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "Heben_Platz"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "Wertung2"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "Wertung2_Platz"})
                .Columns.Add(New CustomColumn_RightBorder With {.Name = "a_K"})

                For Each col As DataGridViewColumn In .Columns
                    col.HeaderText = ColumnDefs(col.Name).Title
                    col.Visible = ColumnDefs(col.Name).Visible
                    col.SortMode = DataGridViewColumnSortMode.NotSortable ' damit alle Headertexte zentriert sind
                    col.DataPropertyName = If(IsNothing(ColumnDefs(col.Name).Field), col.Name, ColumnDefs(col.Name).Field)
                    If Not col.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill Then
                        col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                    End If
                    If Not IsNothing(ColumnDefs(col.Name).Align) Then
                        col.DefaultCellStyle.Alignment = ColumnDefs(col.Name).Align
                    End If
                Next

                'For i = 0 To ColBez.Count - 1
                '    .Columns(i).HeaderText = ColBez(i).Key
                '    .Columns(i).Visible = ColBez(i).Value
                '    .Columns(i).DataPropertyName = ColData(i)
                '    .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable ' damit alle Headertexte zentriert sind
                '    If Not .Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill Then
                '        .Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                '    End If
                'Next
#End Region

#Region "Format"
                '.Columns("Nachname").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                '.Columns("Vorname").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                '.Columns("Verein").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                If Wettkampf.Flagge Then
                    .Columns("Verein").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
                    .Columns("Verein").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                End If
                '.Columns("Sex").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                '.Columns("a_K").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns("a_K").MinimumWidth = 50

                .Columns(_wertung(1)).DefaultCellStyle.Format = NK_String
                .Columns("KG").DefaultCellStyle.Format = "0.00"
                .Columns("Reissen").DefaultCellStyle.ForeColor = Color.Gold 'LightBlue
                .Columns("Stossen").DefaultCellStyle.ForeColor = Color.Gold 'LightBlue
                .Columns("Heben").DefaultCellStyle.ForeColor = Color.Gold 'LightBlue
                .Columns("Heben_Platz").DefaultCellStyle.ForeColor = Color.Gold

                .Columns("Reissen").DefaultCellStyle.Format = NK_String
                .Columns("Stossen").DefaultCellStyle.Format = NK_String
                .Columns("Heben").DefaultCellStyle.Format = NK_String 'If(.Columns("Heben").DataPropertyName.Contains("2"), NK_String_2, NK_String)
                .Columns("Wertung2").DefaultCellStyle.Format = NK_String_2
#End Region
                .BringToFront()
            End With
        End If
    End Sub
    Private Sub Build_Total(Grid As DataGridView)

        With Grid
            If Not IsNothing(.DataSource) Then Return

            Dim ColWeight() As Integer = Nothing
            Dim ColBez() As String = Nothing
            Dim ColData() As String = Nothing

            .DataSource = Nothing
            .AutoGenerateColumns = False
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            .ColumnHeadersVisible = False

#Region "Design"
            Select Case Design
                Case "Standard"
                    .AlternatingRowsDefaultCellStyle.BackColor = Color.OldLace
                    .RowsDefaultCellStyle.BackColor = Color.LightCyan
                    .BackgroundColor = SystemColors.AppWorkspace
                    .GridColor = SystemColors.ControlDark
                    With .DefaultCellStyle
                        .ForeColor = SystemColors.ControlText
                        .SelectionBackColor = Color.LightCyan
                        .SelectionForeColor = SystemColors.ControlText
                    End With
                    ColWeight = {60, 100, 100, 110 + CInt(NK_String_2.Substring(1)) * 10}
                    ColBez = {"Pl.", "Name", "", "Pkt."}
                    If .Equals(dgvBest1) Then
                        ColData = {"Heben_Platz_" & WK_ID, "Nachname", "Vorname", "Heben"}
                    Else
                        ColData = {"Wertung2_Platz_" & WK_ID, "Nachname", "Vorname", "Wertung2"}
                    End If
                Case "Nacht"
                    '.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(40, 50, 60)
                    .RowsDefaultCellStyle.BackColor = Color.FromArgb(30, 30, 98) 'rgb(15, 15, 50) 'Color.Black
                    .BackgroundColor = Color.Black
                    .GridColor = Color.Silver
                    With .DefaultCellStyle
                        .ForeColor = Color.White
                        .SelectionBackColor = Grid.RowsDefaultCellStyle.BackColor
                        .SelectionForeColor = .ForeColor
                    End With
                    ColWeight = {60, 100, 110 + CInt(NK_String_2.Substring(1)) * 10}
                    ColBez = {"Pl.", "Name", "Pkt."}
                    If .Equals(dgvBest1) Then
                        ColData = {"Heben_Platz_" & WK_ID, "Name2", "Heben"}
                    Else
                        ColData = {"Wertung2_Platz_" & WK_ID, "Name2", "Wertung2"}
                    End If
            End Select
#End Region
            .Columns.Add(New CustomColumn_RightBorder With {.Name = "Heben_Platz"})
            If Design.Equals("standard") Then .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
            .Columns.Add(New CustomColumn_RightBorder With {.Name = "Name2", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Heben"})

            For i = 0 To ColBez.Count - 1
                .Columns(i).HeaderText = ColBez(i)
                .Columns(i).MinimumWidth = ColWeight(i)
                .Columns(i).FillWeight = ColWeight(i)
                Try
                    .Columns(i).DataPropertyName = ColData(i)
                Catch ex As Exception
                End Try
                .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            .Columns("Heben_Platz").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            If Design.Equals("standard") Then .Columns("Nachname").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Columns("Name2").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Columns("Heben").DefaultCellStyle.Format = If(Grid.Equals(dgvBest1), NK_String, NK_String_2)
            .Columns("Heben").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
    End Sub
    Private Sub Build_Versuch(Grid As DataGridView)

        If Not IsNothing(Grid.DataSource) Then Return

        Dim ColBez() = {"    ", "Name", "", " V."} ', "Last"}

        Dim ColData() = {"Reihenfolge", "Nachname", "Vorname", "Versuch"} ', "HLast"}

        Dim ColWeight() = {40, 40, 40, 40} ', 50}

        With Grid
            .DataSource = Nothing
#Region "Design"
            Select Case Design
                Case "Standard"
                    .AlternatingRowsDefaultCellStyle.BackColor = Color.OldLace
                    .RowsDefaultCellStyle.BackColor = Color.LightCyan
                    .BackgroundColor = SystemColors.AppWorkspace
                    .GridColor = SystemColors.ControlDark
                    With .DefaultCellStyle
                        .ForeColor = SystemColors.ControlText
                        .SelectionBackColor = Color.LightCyan
                        .SelectionForeColor = SystemColors.ControlText
                    End With
                Case "Nacht"
                    .AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(40, 50, 60)
                    .RowsDefaultCellStyle.BackColor = Color.FromArgb(30, 30, 98) 'rgb(15, 15, 50) 'Color.Black
                    .BackgroundColor = BackColor 'Color.Black
                    .GridColor = Color.WhiteSmoke
                    With .DefaultCellStyle
                        .ForeColor = Color.White
                        .SelectionBackColor = Color.White
                        .SelectionForeColor = Color.Black
                    End With
            End Select
            .AutoGenerateColumns = False
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            .ColumnHeadersVisible = True
#End Region
            .Columns.Add(New CustomColumn_RightBorder With {.Name = "Reihenfolge"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
            .Columns.Add(New CustomColumn_RightBorder With {.Name = "Vorname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
            .Columns.Add(New CustomColumn_RightBorder With {.Name = "Versuch"})
            '.Columns.Add(New DataGridViewTextBoxColumn With {.Name = "HLast"})

            For i = 0 To ColBez.Count - 1
                .Columns(i).HeaderText = ColBez(i)
                .Columns(i).MinimumWidth = ColWeight(i)
                .Columns(i).FillWeight = ColWeight(i)
                Try
                    .Columns(i).DataPropertyName = ColData(i)
                Catch ex As Exception
                End Try
                .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            .Columns("Reihenfolge").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("Versuch").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            '.Columns("HLast").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        End With
    End Sub

    Private Sub NameFormat_Changed()
        Change_NameFormat(dgvJoin)
        Change_NameFormat(dgvBest1)
        Change_NameFormat(dgvBest2)
    End Sub
    Private Sub Change_NameFormat(dgv As DataGridView)
        Try
            With dgv
                If .Name.Equals("dgvJoin") Then
                    For Each row As DataGridViewRow In .Rows
                        Select Case Ansicht_Options.NameFormat
                            Case NameFormat.Standard
                                row.Cells("Nachname").Value = StrConv(row.Cells("Nachname").Value.ToString, VbStrConv.ProperCase)
                            Case NameFormat.Standard_UCase
                                row.Cells("Nachname").Value = StrConv(row.Cells("Nachname").Value.ToString, VbStrConv.Uppercase)
                            Case NameFormat.International
                                row.Cells("Nachname").Value = StrConv(row.Cells("Nachname").Value.ToString, VbStrConv.Uppercase) + " " + row.Cells("Vorname").Value.ToString
                        End Select
                    Next
                ElseIf .Columns.Contains("Nachname") Then
                    Select Case Ansicht_Options.NameFormat
                        Case NameFormat.Standard
                            .Columns("Nachname").DataPropertyName = "Nachname"
                        Case NameFormat.Standard_UCase
                            .Columns("Nachname").DataPropertyName = "Name1"
                        Case NameFormat.International
                            .Columns("Nachname").DataPropertyName = "Name2"
                    End Select
                End If
                If .Columns.Contains("Nachname") Then .Columns("Vorname").Visible = Ansicht_Options.NameFormat < NameFormat.International
                .AutoResizeColumns()
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Function Get_GridHeight(Grid As DataGridView, Optional Panels As Integer = 0, Optional GroupDividers As Integer = 0, Optional RowCount As Integer = 0, Optional Grid_Above As DataGridView = Nothing) As Integer
        ' --> Höhe des Grid berechnen
        ' --> Padding für alle Rows setzen = Zeilenhöhe anpassen

        If Grid.Rows.Count = 0 And RowCount = 0 Then Return 0

        If RowCount = 0 Then RowCount = Grid.Rows.Count

        Dim MaxHeight = Grid.Parent.Height - Grid.Top - If(Grid.ColumnHeadersVisible, Grid.ColumnHeadersHeight, 0) - If(Panels > 0, Grid.Rows(0).Height, 0)
        Dim RowHeight = Grid.Rows(0).Height
        Dim LineHeight = RowHeight - Grid.RowsDefaultCellStyle.Padding.Vertical

        'If IsDisplayed Then Return Height


        ' maximal mögliches Padding = verfügbare Höhe / Anzahl aller Zeilen * Zeilenhöhe
        Dim MaxPadding = CSng((MaxHeight - (GroupDividers * GroupDivider_Height) - (RowCount + Panels * 2) * LineHeight) / (RowCount + Panels * 2))

        If MaxPadding < 0 Then
            If Not Wettkampf.Mannschaft Then
                If IsNothing(Grid_Above) Then
                    ' oberes Grid (Bestenliste) hat zu viele Zeilen
                    RowCount = MaxHeight \ LineHeight
                    Return RowCount * LineHeight + If(Grid.ColumnHeadersVisible, Grid.ColumnHeadersHeight, 0)
                Else
                    ' 2. Liste hat zu viele Zeilen
                    Dim RowCount1 = Grid_Above.DisplayedRowCount(False)
                    If RowCount1 > 12 Then RowCount1 = 12
                    Dim SumRowCount = RowCount + RowCount1
                    Dim SumMaxHeight = MaxHeight + Grid_Above.Height - If(Grid.ColumnHeadersVisible, Grid.ColumnHeadersHeight, 0)
                    MaxPadding = CSng((SumMaxHeight - SumRowCount * LineHeight) / SumRowCount)
                    If MaxPadding < 0 Then
                        ' Zeilenanzahl verringern --> RowCount für unteres Grid, RowCount1 für oberes Grid
                        ' --> hier wird die Verteilung oberes / unteres Grid vorgenommen
                        If RowCount > SumRowCount \ 2 Then RowCount = SumRowCount \ 2
                        RowCount1 = SumRowCount - RowCount - SumRowCount Mod 2
                        MaxPadding = 0
                    End If
                    ' Padding für oberes Grid
                    Set_RowHeight(dgvBest1, CInt(Math.Floor(MaxPadding / 2)))
                    ' Höhe des oberen Grid aktualisieren
                    RowHeight = dgvBest1.Rows(0).Height
                    dgvBest1.Height = RowCount1 * RowHeight + If(Grid_Above.ColumnHeadersVisible, Grid_Above.ColumnHeadersHeight, 0) + 1
                    ' unteres Panel neu positionieren
                    pnlBest2.Location = New Point(0, pnlBest1.Top + dgvBest1.Top + dgvBest1.Height)
                    pnlBest2.Height = Height - pnlBest2.Top
                End If
            Else
                ' Mannschafts-WK passt nicht in Anzeige --> Schrift verkleinern
                'With Grid.DefaultCellStyle
                '    .Font = New Font(.Font.FontFamily, .Font.Size + MaxPadding, .Font.Style, GraphicsUnit.Point)
                'End With
                'For Each lbl As Label In dicLabels.Values
                '    lbl.Font = New Font(lbl.Font.FontFamily, lbl.Font.Size + MaxPadding, lbl.Font.Style, GraphicsUnit.Point)
                'Next
                MaxPadding = 0
                Set_RowHeight(Grid, CInt(Math.Floor(MaxPadding / 2)))
                ' Höhe des Grid aktualisieren
                RowHeight = Grid.Rows(0).Height
                Grid.Height = RowCount * RowHeight + Grid.ColumnHeadersHeight + 1
            End If
        ElseIf MaxPadding > 20 Then
            MaxPadding = 20
        End If

        ' Padding für Grid setzen
        Set_RowHeight(Grid, CInt(Math.Floor(MaxPadding / 2)))

        ' erneut einlesen, da von Set_RowHeight geändert
        RowHeight = Grid.Rows(0).Height

        ' Gesamthöhe der Rows + Header + TeamPanels
        Dim _height = RowCount * RowHeight + If(Grid.ColumnHeadersVisible, Grid.ColumnHeadersHeight, 0) + Panels * 2 * RowHeight + GroupDividers * GroupDivider_Height

        IsDisplayed = True
        Return _height + 1 ' letzte DividerLine addieren

    End Function

    Private Sub Set_RowHeight(Grid As DataGridView, Padding As Integer)
        Try
            Grid.RowsDefaultCellStyle.Padding = New Padding(0, Padding, 0, Padding)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dgvJoin_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgvJoin.CellPainting
        Try
            If e.RowIndex > -1 Then
                If Not IsDBNull(e.Value) AndAlso (dgvJoin.Columns(e.ColumnIndex).Name.Contains("R_") OrElse dgvJoin.Columns(e.ColumnIndex).Name.Contains("S_")) Then
                    If Not IsDBNull(dvResults(e.RowIndex)(dgvJoin.Columns(e.ColumnIndex).Name.Insert(1, "W"))) Then
                        e.Handled = True
                        Dim img As Image = Nothing
                        If CInt(dvResults(e.RowIndex)(dgvJoin.Columns(e.ColumnIndex).Name.Insert(1, "W"))) = 1 Then
                            img = ImageList1.Images("SquareBlue.jpg")
                        ElseIf CInt(dvResults(e.RowIndex)(dgvJoin.Columns(e.ColumnIndex).Name.Insert(1, "W"))) = -1 Then
                            img = ImageList1.Images("SquareRed.jpg")
                        End If
                        If Not IsNothing(img) Then e.CellStyle.ForeColor = dgvJoin.DefaultCellStyle.ForeColor
                        e.PaintBackground(e.CellBounds, e.RowIndex Mod 2 = 1)
                        If Not IsNothing(img) Then
                            Dim pX = 4 'dgvJoin.RowsDefaultCellStyle.Padding.Left
                            Dim pY = 4 'dgvJoin.RowsDefaultCellStyle.Padding.Top
                            e.Graphics.DrawImage(img, e.CellBounds.Left + pX, e.CellBounds.Top + pY, e.CellBounds.Width - 2 * pX, e.CellBounds.Height - 2 * pY - dgvJoin.Rows(e.RowIndex).DividerHeight)
                        End If
                        e.PaintContent(e.CellBounds)
                    End If
                ElseIf Not IsDBNull(e.Value) AndAlso dgvJoin.Columns(e.ColumnIndex).Name.Contains("_Platz") AndAlso e.Value.ToString = "-1" Then
                    ' a.K.
                    e.Handled = True
                    dgvJoin.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = "a.K."
                ElseIf Wettkampf.Flagge AndAlso dgvJoin.Columns(e.ColumnIndex).Name.Equals("Flagge") Then
                    Try
                        e.Handled = True
                        e.PaintBackground(e.CellBounds, e.RowIndex Mod 2 = 1)
                        Dim img As Image = Image.FromFile(Path.Combine(Application.StartupPath, "flag", dvResults(e.RowIndex)("state_flag").ToString), True)
                        If Not IsNothing(img) Then
                            Dim fx As New List(Of Double)
                            Dim px As New Padding(0, 0, 0, 1) 'Bottom=0 --> wird nur eine horizontale Linie angezeigt

                            fx.Add(dgvJoin.Rows(e.RowIndex).Height / img.Height)
                            fx.Add(dgvJoin.Columns(e.ColumnIndex).Width / img.Width)

                            If fx(1) < fx(0) Then fx.Reverse() ' Faktoren tauschen, wenn Breite zu groß für Spalte --> img.Height wird kleiner als Column.Height

                            Dim newHeight = CInt(Math.Floor(img.Height * fx(0)))
                            Dim newWidth = CInt(Math.Floor(img.Width * fx(0)))

                            px.Left = (e.CellBounds.Width - newWidth) \ 2
                            px.Top = (e.CellBounds.Height - newHeight) \ 2
                            px.Right = e.CellBounds.Width - newWidth - px.Left
                            If px.Horizontal = 0 Then px.Right += 1
                            px.Bottom = e.CellBounds.Height - newHeight - px.Top + 1

                            e.Graphics.DrawImage(img, e.CellBounds.Left + px.Left, e.CellBounds.Top + px.Top, e.CellBounds.Width - px.Horizontal, e.CellBounds.Height - px.Vertical)

                            Dim rect As New Rectangle(e.CellBounds.Left + px.Left, e.CellBounds.Top + px.Top, e.CellBounds.Width - px.Horizontal, e.CellBounds.Height - px.Vertical)
                            e.Graphics.DrawRectangle(Pens.Green, rect)
                        End If
                    Catch ex As Exception
                    End Try
                    'ElseIf dgvJoin.Columns(e.ColumnIndex).Name.Equals("Nachname") Then
                    '    dgvJoin.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = e.Value.ToString.ToUpper
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvJoin_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvJoin.CellFormatting

        If dgvJoin.Columns(e.ColumnIndex).Name.Equals("a_K") Then
            If Equals(e.Value, False) Then
                e.Value = ""
                e.FormattingApplied = True
            ElseIf Equals(e.Value, True) Then
                e.Value = "a.K."
                e.FormattingApplied = True
            End If
        End If
    End Sub

    Private Sub CountDown_Changed(Value As String)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_String(AddressOf CountDown_Changed)
                Invoke(d, New Object() {Value})
            Else
                xAufruf.Text = Value
            End If
        Catch
        End Try
    End Sub
    Private Sub ClockColor_Changed(Color As Color)
        Try
            If InvokeRequired Then
                Dim d As New Delegate_Color(AddressOf ClockColor_Changed)
                Invoke(d, New Object() {Color})
            Else
                xAufruf.ForeColor = Color
            End If
        Catch
        End Try
    End Sub
    Private Sub dgv_SelectionChanged(sender As Object, e As EventArgs) Handles dgvJoin.SelectionChanged, dgvBest1.SelectionChanged, dgvBest2.SelectionChanged
        CType(sender, DataGridView).ClearSelection()
    End Sub

    Private Sub InfoMessage_Show(Info As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub2(AddressOf InfoMessage_Show)
                Invoke(d, New Object() {Info})
            Else
                If Not String.IsNullOrEmpty(Info) Then
                    With lblJury
                        .Text = Info
                        .Left = 0
                        .Width = Width
                        .Visible = True
                        .BringToFront()
                    End With
                    timerMsg_Running = True
                    TimerMsg.Start()
                Else
                    timerMsg_Running = False
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub TimerMsg_Tick(sender As Object, e As ElapsedEventArgs) Handles TimerMsg.Elapsed
        Static _blink As Integer
        If _blink = 90 Then timerMsg_Running = False
        With lblJury
            If Not timerMsg_Running Then
                TimerMsg.Stop()
                _blink = 0
            Else
                'If .ForeColor = Color.Yellow Then
                '    .BackColor = Color.Yellow
                '    .ForeColor = Color.Red
                'Else
                '    .ForeColor = Color.Yellow
                '    .BackColor = Color.Red
                'End If
                _blink += 1
            End If
            .Visible = _blink Mod 3 <> 0
        End With
    End Sub

    Private Sub dgvTeam_SizeChanged(sender As Object, e As EventArgs) Handles dgvTeam.SizeChanged
        lblCaption.Left = dgvTeam.Left + dgvTeam.Width - 380
    End Sub

End Class