﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmKorrektur
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmKorrektur))
        Me.pnlForm = New System.Windows.Forms.Panel()
        Me.lblGruppe = New System.Windows.Forms.Label()
        Me.pnlGruppe = New System.Windows.Forms.Panel()
        Me.dgvGruppe = New System.Windows.Forms.DataGridView()
        Me.Groups = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlError = New System.Windows.Forms.Panel()
        Me.lblError = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtWertung3 = New System.Windows.Forms.TextBox()
        Me.txtWertung1 = New System.Windows.Forms.TextBox()
        Me.txtWertung2 = New System.Windows.Forms.TextBox()
        Me.chkV3 = New System.Windows.Forms.CheckBox()
        Me.chkU3 = New System.Windows.Forms.CheckBox()
        Me.chkG3 = New System.Windows.Forms.CheckBox()
        Me.chkV2 = New System.Windows.Forms.CheckBox()
        Me.chkU2 = New System.Windows.Forms.CheckBox()
        Me.chkG2 = New System.Windows.Forms.CheckBox()
        Me.chkV1 = New System.Windows.Forms.CheckBox()
        Me.chkU1 = New System.Windows.Forms.CheckBox()
        Me.chkG1 = New System.Windows.Forms.CheckBox()
        Me.lblVersuch3 = New System.Windows.Forms.Label()
        Me.lblVersuch2 = New System.Windows.Forms.Label()
        Me.lblVersuch1 = New System.Windows.Forms.Label()
        Me.pnlTechnik = New System.Windows.Forms.Panel()
        Me.txtTechnik3 = New System.Windows.Forms.TextBox()
        Me.ContextMenuTechnik = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuNote = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblN3 = New System.Windows.Forms.Label()
        Me.lblN2 = New System.Windows.Forms.Label()
        Me.lblN1 = New System.Windows.Forms.Label()
        Me.txtTechnik2 = New System.Windows.Forms.TextBox()
        Me.txtTechnik1 = New System.Windows.Forms.TextBox()
        Me.btnWiederholen = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.txtLast3 = New System.Windows.Forms.NumericUpDown()
        Me.ContextMenuLast = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuLast = New System.Windows.Forms.ToolStripMenuItem()
        Me.txtLast2 = New System.Windows.Forms.NumericUpDown()
        Me.txtLast1 = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboDurchgang = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboHeber = New System.Windows.Forms.ComboBox()
        Me.ContextMenuHeber = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuHeberId = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnEscape = New System.Windows.Forms.Button()
        Me.lblTitleBar = New System.Windows.Forms.Label()
        Me.pnlForm.SuspendLayout()
        CType(Me.dgvGruppe, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlError.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTechnik.SuspendLayout()
        Me.ContextMenuTechnik.SuspendLayout()
        CType(Me.txtLast3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuLast.SuspendLayout()
        CType(Me.txtLast2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLast1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuHeber.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlForm
        '
        Me.pnlForm.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlForm.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pnlForm.Controls.Add(Me.lblGruppe)
        Me.pnlForm.Controls.Add(Me.pnlGruppe)
        Me.pnlForm.Controls.Add(Me.dgvGruppe)
        Me.pnlForm.Controls.Add(Me.pnlError)
        Me.pnlForm.Controls.Add(Me.Label6)
        Me.pnlForm.Controls.Add(Me.txtWertung3)
        Me.pnlForm.Controls.Add(Me.txtWertung1)
        Me.pnlForm.Controls.Add(Me.txtWertung2)
        Me.pnlForm.Controls.Add(Me.chkV3)
        Me.pnlForm.Controls.Add(Me.chkU3)
        Me.pnlForm.Controls.Add(Me.chkG3)
        Me.pnlForm.Controls.Add(Me.chkV2)
        Me.pnlForm.Controls.Add(Me.chkU2)
        Me.pnlForm.Controls.Add(Me.chkG2)
        Me.pnlForm.Controls.Add(Me.chkV1)
        Me.pnlForm.Controls.Add(Me.chkU1)
        Me.pnlForm.Controls.Add(Me.chkG1)
        Me.pnlForm.Controls.Add(Me.lblVersuch3)
        Me.pnlForm.Controls.Add(Me.lblVersuch2)
        Me.pnlForm.Controls.Add(Me.lblVersuch1)
        Me.pnlForm.Controls.Add(Me.pnlTechnik)
        Me.pnlForm.Controls.Add(Me.btnWiederholen)
        Me.pnlForm.Controls.Add(Me.btnCancel)
        Me.pnlForm.Controls.Add(Me.btnSave)
        Me.pnlForm.Controls.Add(Me.txtLast3)
        Me.pnlForm.Controls.Add(Me.txtLast2)
        Me.pnlForm.Controls.Add(Me.txtLast1)
        Me.pnlForm.Controls.Add(Me.Label3)
        Me.pnlForm.Controls.Add(Me.cboDurchgang)
        Me.pnlForm.Controls.Add(Me.Label2)
        Me.pnlForm.Controls.Add(Me.cboHeber)
        Me.pnlForm.Controls.Add(Me.Label5)
        Me.pnlForm.Controls.Add(Me.Label4)
        Me.pnlForm.Controls.Add(Me.Label1)
        Me.pnlForm.Location = New System.Drawing.Point(0, 31)
        Me.pnlForm.MinimumSize = New System.Drawing.Size(353, 250)
        Me.pnlForm.Name = "pnlForm"
        Me.pnlForm.Size = New System.Drawing.Size(427, 286)
        Me.pnlForm.TabIndex = 501
        '
        'lblGruppe
        '
        Me.lblGruppe.BackColor = System.Drawing.SystemColors.Window
        Me.lblGruppe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblGruppe.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblGruppe.Location = New System.Drawing.Point(79, 22)
        Me.lblGruppe.Name = "lblGruppe"
        Me.lblGruppe.Size = New System.Drawing.Size(73, 28)
        Me.lblGruppe.TabIndex = 568
        Me.lblGruppe.Text = "[alle]"
        Me.lblGruppe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlGruppe
        '
        Me.pnlGruppe.BackColor = System.Drawing.Color.Transparent
        Me.pnlGruppe.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pnlGruppe.Location = New System.Drawing.Point(151, 22)
        Me.pnlGruppe.Name = "pnlGruppe"
        Me.pnlGruppe.Size = New System.Drawing.Size(17, 28)
        Me.pnlGruppe.TabIndex = 562
        '
        'dgvGruppe
        '
        Me.dgvGruppe.AllowUserToAddRows = False
        Me.dgvGruppe.AllowUserToDeleteRows = False
        Me.dgvGruppe.AllowUserToResizeColumns = False
        Me.dgvGruppe.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGruppe.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvGruppe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGruppe.ColumnHeadersVisible = False
        Me.dgvGruppe.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Groups, Me.TG, Me.WG, Me.Bezeichnung})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvGruppe.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvGruppe.Location = New System.Drawing.Point(203, 55)
        Me.dgvGruppe.MultiSelect = False
        Me.dgvGruppe.Name = "dgvGruppe"
        Me.dgvGruppe.RowHeadersVisible = False
        Me.dgvGruppe.RowHeadersWidth = 62
        Me.dgvGruppe.RowTemplate.Height = 26
        Me.dgvGruppe.RowTemplate.ReadOnly = True
        Me.dgvGruppe.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvGruppe.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGruppe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGruppe.ShowCellToolTips = False
        Me.dgvGruppe.ShowEditingIcon = False
        Me.dgvGruppe.ShowRowErrors = False
        Me.dgvGruppe.Size = New System.Drawing.Size(191, 60)
        Me.dgvGruppe.TabIndex = 561
        Me.dgvGruppe.Visible = False
        '
        'Groups
        '
        Me.Groups.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Groups.DataPropertyName = "Gruppe"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Groups.DefaultCellStyle = DataGridViewCellStyle2
        Me.Groups.HeaderText = "#"
        Me.Groups.MinimumWidth = 8
        Me.Groups.Name = "Groups"
        Me.Groups.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Groups.Width = 35
        '
        'TG
        '
        Me.TG.DataPropertyName = "Teilgruppe"
        Me.TG.HeaderText = "Teilgruppe"
        Me.TG.MinimumWidth = 8
        Me.TG.Name = "TG"
        Me.TG.Visible = False
        Me.TG.Width = 150
        '
        'WG
        '
        Me.WG.DataPropertyName = "WG"
        Me.WG.HeaderText = "WG"
        Me.WG.MinimumWidth = 8
        Me.WG.Name = "WG"
        Me.WG.Visible = False
        Me.WG.Width = 150
        '
        'Bezeichnung
        '
        Me.Bezeichnung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Bezeichnung.DataPropertyName = "Bezeichnung"
        Me.Bezeichnung.HeaderText = "Bezeichnung"
        Me.Bezeichnung.MinimumWidth = 8
        Me.Bezeichnung.Name = "Bezeichnung"
        Me.Bezeichnung.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'pnlError
        '
        Me.pnlError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pnlError.Controls.Add(Me.lblError)
        Me.pnlError.Controls.Add(Me.PictureBox1)
        Me.pnlError.Location = New System.Drawing.Point(0, 118)
        Me.pnlError.MaximumSize = New System.Drawing.Size(412, 100)
        Me.pnlError.Name = "pnlError"
        Me.pnlError.Size = New System.Drawing.Size(17, 100)
        Me.pnlError.TabIndex = 560
        Me.pnlError.Visible = False
        '
        'lblError
        '
        Me.lblError.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.AutoEllipsis = True
        Me.lblError.BackColor = System.Drawing.Color.Transparent
        Me.lblError.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblError.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.lblError.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblError.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblError.ImageKey = "(Keine)"
        Me.lblError.Location = New System.Drawing.Point(153, 18)
        Me.lblError.MaximumSize = New System.Drawing.Size(399, 100)
        Me.lblError.Name = "lblError"
        Me.lblError.Padding = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblError.Size = New System.Drawing.Size(0, 63)
        Me.lblError.TabIndex = 547
        Me.lblError.Text = "Korrektur nicht möglich," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "da der Aufruf nicht beendet ist."
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblError.UseCompatibleTextRendering = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(46, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(80, 100)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label6.Location = New System.Drawing.Point(12, 25)
        Me.Label6.MinimumSize = New System.Drawing.Size(60, 13)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 20)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "&Gruppe"
        '
        'txtWertung3
        '
        Me.txtWertung3.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtWertung3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtWertung3.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtWertung3.Font = New System.Drawing.Font("Wingdings", 24.0!)
        Me.txtWertung3.Location = New System.Drawing.Point(151, 182)
        Me.txtWertung3.MinimumSize = New System.Drawing.Size(22, 27)
        Me.txtWertung3.Name = "txtWertung3"
        Me.txtWertung3.ReadOnly = True
        Me.txtWertung3.Size = New System.Drawing.Size(22, 36)
        Me.txtWertung3.TabIndex = 526
        Me.txtWertung3.TabStop = False
        Me.txtWertung3.Text = "l"
        Me.txtWertung3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtWertung1
        '
        Me.txtWertung1.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtWertung1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtWertung1.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtWertung1.Font = New System.Drawing.Font("Wingdings", 24.0!)
        Me.txtWertung1.ForeColor = System.Drawing.Color.LimeGreen
        Me.txtWertung1.Location = New System.Drawing.Point(151, 118)
        Me.txtWertung1.MinimumSize = New System.Drawing.Size(22, 27)
        Me.txtWertung1.Name = "txtWertung1"
        Me.txtWertung1.ReadOnly = True
        Me.txtWertung1.Size = New System.Drawing.Size(22, 36)
        Me.txtWertung1.TabIndex = 279
        Me.txtWertung1.TabStop = False
        Me.txtWertung1.Text = ""
        Me.txtWertung1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtWertung2
        '
        Me.txtWertung2.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtWertung2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtWertung2.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtWertung2.Font = New System.Drawing.Font("Wingdings", 24.0!)
        Me.txtWertung2.ForeColor = System.Drawing.Color.Red
        Me.txtWertung2.Location = New System.Drawing.Point(151, 150)
        Me.txtWertung2.MinimumSize = New System.Drawing.Size(22, 27)
        Me.txtWertung2.Name = "txtWertung2"
        Me.txtWertung2.ReadOnly = True
        Me.txtWertung2.Size = New System.Drawing.Size(22, 36)
        Me.txtWertung2.TabIndex = 518
        Me.txtWertung2.TabStop = False
        Me.txtWertung2.Text = ""
        Me.txtWertung2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'chkV3
        '
        Me.chkV3.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkV3.AutoCheck = False
        Me.chkV3.BackColor = System.Drawing.Color.Gray
        Me.chkV3.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.chkV3.FlatAppearance.BorderSize = 2
        Me.chkV3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Black
        Me.chkV3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(74, Byte), Integer))
        Me.chkV3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black
        Me.chkV3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkV3.ForeColor = System.Drawing.Color.White
        Me.chkV3.Location = New System.Drawing.Point(339, 187)
        Me.chkV3.Name = "chkV3"
        Me.chkV3.Size = New System.Drawing.Size(65, 26)
        Me.chkV3.TabIndex = 93
        Me.chkV3.TabStop = False
        Me.chkV3.Text = "Verzicht"
        Me.chkV3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkV3.UseVisualStyleBackColor = False
        '
        'chkU3
        '
        Me.chkU3.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkU3.AutoCheck = False
        Me.chkU3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(179, Byte), Integer))
        Me.chkU3.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.chkU3.FlatAppearance.BorderSize = 2
        Me.chkU3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red
        Me.chkU3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.chkU3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red
        Me.chkU3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkU3.Location = New System.Drawing.Point(266, 187)
        Me.chkU3.Name = "chkU3"
        Me.chkU3.Size = New System.Drawing.Size(65, 26)
        Me.chkU3.TabIndex = 92
        Me.chkU3.TabStop = False
        Me.chkU3.Text = "Ungültig"
        Me.chkU3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkU3.UseVisualStyleBackColor = False
        '
        'chkG3
        '
        Me.chkG3.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkG3.AutoCheck = False
        Me.chkG3.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.chkG3.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.chkG3.FlatAppearance.BorderSize = 2
        Me.chkG3.FlatAppearance.CheckedBackColor = System.Drawing.Color.White
        Me.chkG3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke
        Me.chkG3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.chkG3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkG3.Location = New System.Drawing.Point(192, 187)
        Me.chkG3.Name = "chkG3"
        Me.chkG3.Size = New System.Drawing.Size(65, 26)
        Me.chkG3.TabIndex = 91
        Me.chkG3.TabStop = False
        Me.chkG3.Text = "Gültig"
        Me.chkG3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkG3.UseVisualStyleBackColor = False
        '
        'chkV2
        '
        Me.chkV2.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkV2.AutoCheck = False
        Me.chkV2.BackColor = System.Drawing.Color.Gray
        Me.chkV2.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.chkV2.FlatAppearance.BorderSize = 2
        Me.chkV2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Black
        Me.chkV2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(74, Byte), Integer))
        Me.chkV2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black
        Me.chkV2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkV2.ForeColor = System.Drawing.Color.White
        Me.chkV2.Location = New System.Drawing.Point(339, 155)
        Me.chkV2.Name = "chkV2"
        Me.chkV2.Size = New System.Drawing.Size(65, 26)
        Me.chkV2.TabIndex = 83
        Me.chkV2.TabStop = False
        Me.chkV2.Text = "Verzicht"
        Me.chkV2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkV2.UseVisualStyleBackColor = False
        '
        'chkU2
        '
        Me.chkU2.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkU2.AutoCheck = False
        Me.chkU2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(179, Byte), Integer))
        Me.chkU2.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.chkU2.FlatAppearance.BorderSize = 2
        Me.chkU2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red
        Me.chkU2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.chkU2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red
        Me.chkU2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkU2.Location = New System.Drawing.Point(266, 155)
        Me.chkU2.Name = "chkU2"
        Me.chkU2.Size = New System.Drawing.Size(65, 26)
        Me.chkU2.TabIndex = 82
        Me.chkU2.TabStop = False
        Me.chkU2.Text = "Ungültig"
        Me.chkU2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkU2.UseVisualStyleBackColor = False
        '
        'chkG2
        '
        Me.chkG2.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkG2.AutoCheck = False
        Me.chkG2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.chkG2.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.chkG2.FlatAppearance.BorderSize = 2
        Me.chkG2.FlatAppearance.CheckedBackColor = System.Drawing.Color.White
        Me.chkG2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke
        Me.chkG2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.chkG2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkG2.Location = New System.Drawing.Point(192, 155)
        Me.chkG2.Name = "chkG2"
        Me.chkG2.Size = New System.Drawing.Size(65, 26)
        Me.chkG2.TabIndex = 81
        Me.chkG2.TabStop = False
        Me.chkG2.Text = "Gültig"
        Me.chkG2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkG2.UseVisualStyleBackColor = False
        '
        'chkV1
        '
        Me.chkV1.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkV1.AutoCheck = False
        Me.chkV1.BackColor = System.Drawing.Color.Gray
        Me.chkV1.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.chkV1.FlatAppearance.BorderSize = 2
        Me.chkV1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Black
        Me.chkV1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(74, Byte), Integer))
        Me.chkV1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black
        Me.chkV1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkV1.ForeColor = System.Drawing.Color.White
        Me.chkV1.Location = New System.Drawing.Point(339, 123)
        Me.chkV1.Name = "chkV1"
        Me.chkV1.Size = New System.Drawing.Size(65, 26)
        Me.chkV1.TabIndex = 73
        Me.chkV1.TabStop = False
        Me.chkV1.Text = "Verzicht"
        Me.chkV1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkV1.UseVisualStyleBackColor = False
        '
        'chkU1
        '
        Me.chkU1.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkU1.AutoCheck = False
        Me.chkU1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(179, Byte), Integer))
        Me.chkU1.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.chkU1.FlatAppearance.BorderSize = 2
        Me.chkU1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red
        Me.chkU1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(110, Byte), Integer))
        Me.chkU1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red
        Me.chkU1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkU1.Location = New System.Drawing.Point(266, 123)
        Me.chkU1.Name = "chkU1"
        Me.chkU1.Size = New System.Drawing.Size(65, 26)
        Me.chkU1.TabIndex = 72
        Me.chkU1.TabStop = False
        Me.chkU1.Text = "Ungültig"
        Me.chkU1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkU1.UseVisualStyleBackColor = False
        '
        'chkG1
        '
        Me.chkG1.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkG1.AutoCheck = False
        Me.chkG1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.chkG1.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.chkG1.FlatAppearance.BorderSize = 2
        Me.chkG1.FlatAppearance.CheckedBackColor = System.Drawing.Color.White
        Me.chkG1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.chkG1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.chkG1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkG1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.chkG1.Location = New System.Drawing.Point(192, 123)
        Me.chkG1.Margin = New System.Windows.Forms.Padding(0)
        Me.chkG1.Name = "chkG1"
        Me.chkG1.Size = New System.Drawing.Size(65, 26)
        Me.chkG1.TabIndex = 71
        Me.chkG1.TabStop = False
        Me.chkG1.Text = "Gültig"
        Me.chkG1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkG1.UseVisualStyleBackColor = False
        '
        'lblVersuch3
        '
        Me.lblVersuch3.AutoSize = True
        Me.lblVersuch3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblVersuch3.Location = New System.Drawing.Point(23, 190)
        Me.lblVersuch3.Name = "lblVersuch3"
        Me.lblVersuch3.Size = New System.Drawing.Size(22, 20)
        Me.lblVersuch3.TabIndex = 549
        Me.lblVersuch3.Text = "3."
        '
        'lblVersuch2
        '
        Me.lblVersuch2.AutoSize = True
        Me.lblVersuch2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblVersuch2.Location = New System.Drawing.Point(23, 158)
        Me.lblVersuch2.Name = "lblVersuch2"
        Me.lblVersuch2.Size = New System.Drawing.Size(22, 20)
        Me.lblVersuch2.TabIndex = 548
        Me.lblVersuch2.Text = "2."
        '
        'lblVersuch1
        '
        Me.lblVersuch1.AutoSize = True
        Me.lblVersuch1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblVersuch1.Location = New System.Drawing.Point(23, 126)
        Me.lblVersuch1.Name = "lblVersuch1"
        Me.lblVersuch1.Size = New System.Drawing.Size(22, 20)
        Me.lblVersuch1.TabIndex = 547
        Me.lblVersuch1.Text = "1."
        '
        'pnlTechnik
        '
        Me.pnlTechnik.Controls.Add(Me.txtTechnik3)
        Me.pnlTechnik.Controls.Add(Me.lblN3)
        Me.pnlTechnik.Controls.Add(Me.lblN2)
        Me.pnlTechnik.Controls.Add(Me.lblN1)
        Me.pnlTechnik.Controls.Add(Me.txtTechnik2)
        Me.pnlTechnik.Controls.Add(Me.txtTechnik1)
        Me.pnlTechnik.Location = New System.Drawing.Point(389, 121)
        Me.pnlTechnik.Name = "pnlTechnik"
        Me.pnlTechnik.Size = New System.Drawing.Size(110, 97)
        Me.pnlTechnik.TabIndex = 95
        Me.pnlTechnik.Visible = False
        '
        'txtTechnik3
        '
        Me.txtTechnik3.ContextMenuStrip = Me.ContextMenuTechnik
        Me.txtTechnik3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txtTechnik3.Location = New System.Drawing.Point(53, 66)
        Me.txtTechnik3.MinimumSize = New System.Drawing.Size(35, 20)
        Me.txtTechnik3.Name = "txtTechnik3"
        Me.txtTechnik3.Size = New System.Drawing.Size(57, 26)
        Me.txtTechnik3.TabIndex = 120
        Me.txtTechnik3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ContextMenuTechnik
        '
        Me.ContextMenuTechnik.AllowMerge = False
        Me.ContextMenuTechnik.BackColor = System.Drawing.SystemColors.Info
        Me.ContextMenuTechnik.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.ContextMenuTechnik.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.ContextMenuTechnik.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNote})
        Me.ContextMenuTechnik.Name = "ContextMenuTechnik"
        Me.ContextMenuTechnik.ShowImageMargin = False
        Me.ContextMenuTechnik.Size = New System.Drawing.Size(97, 30)
        '
        'mnuNote
        '
        Me.mnuNote.Name = "mnuNote"
        Me.mnuNote.Size = New System.Drawing.Size(96, 26)
        Me.mnuNote.Text = "Note: "
        Me.mnuNote.ToolTipText = "Klick = Übernehmen" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ESC   = Schließen"
        '
        'lblN3
        '
        Me.lblN3.AutoSize = True
        Me.lblN3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblN3.Location = New System.Drawing.Point(7, 69)
        Me.lblN3.MinimumSize = New System.Drawing.Size(30, 13)
        Me.lblN3.Name = "lblN3"
        Me.lblN3.Size = New System.Drawing.Size(43, 20)
        Me.lblN3.TabIndex = 541
        Me.lblN3.Text = "Note"
        '
        'lblN2
        '
        Me.lblN2.AutoSize = True
        Me.lblN2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblN2.Location = New System.Drawing.Point(7, 37)
        Me.lblN2.MinimumSize = New System.Drawing.Size(30, 13)
        Me.lblN2.Name = "lblN2"
        Me.lblN2.Size = New System.Drawing.Size(43, 20)
        Me.lblN2.TabIndex = 539
        Me.lblN2.Text = "Note"
        '
        'lblN1
        '
        Me.lblN1.AutoSize = True
        Me.lblN1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblN1.Location = New System.Drawing.Point(7, 5)
        Me.lblN1.MinimumSize = New System.Drawing.Size(30, 13)
        Me.lblN1.Name = "lblN1"
        Me.lblN1.Size = New System.Drawing.Size(43, 20)
        Me.lblN1.TabIndex = 537
        Me.lblN1.Text = "Note"
        '
        'txtTechnik2
        '
        Me.txtTechnik2.ContextMenuStrip = Me.ContextMenuTechnik
        Me.txtTechnik2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txtTechnik2.Location = New System.Drawing.Point(53, 34)
        Me.txtTechnik2.MinimumSize = New System.Drawing.Size(35, 20)
        Me.txtTechnik2.Name = "txtTechnik2"
        Me.txtTechnik2.Size = New System.Drawing.Size(57, 26)
        Me.txtTechnik2.TabIndex = 110
        Me.txtTechnik2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTechnik1
        '
        Me.txtTechnik1.ContextMenuStrip = Me.ContextMenuTechnik
        Me.txtTechnik1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txtTechnik1.Location = New System.Drawing.Point(53, 2)
        Me.txtTechnik1.MinimumSize = New System.Drawing.Size(35, 20)
        Me.txtTechnik1.Name = "txtTechnik1"
        Me.txtTechnik1.Size = New System.Drawing.Size(57, 26)
        Me.txtTechnik1.TabIndex = 100
        Me.txtTechnik1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnWiederholen
        '
        Me.btnWiederholen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnWiederholen.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnWiederholen.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnWiederholen.Location = New System.Drawing.Point(18, 238)
        Me.btnWiederholen.MinimumSize = New System.Drawing.Size(77, 25)
        Me.btnWiederholen.Name = "btnWiederholen"
        Me.btnWiederholen.Size = New System.Drawing.Size(110, 30)
        Me.btnWiederholen.TabIndex = 130
        Me.btnWiederholen.Text = "Wiederholen"
        Me.btnWiederholen.UseVisualStyleBackColor = True
        Me.btnWiederholen.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnCancel.Location = New System.Drawing.Point(295, 238)
        Me.btnCancel.MinimumSize = New System.Drawing.Size(77, 25)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(110, 30)
        Me.btnCancel.TabIndex = 150
        Me.btnCancel.Text = "Beenden"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Enabled = False
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnSave.Location = New System.Drawing.Point(179, 238)
        Me.btnSave.MinimumSize = New System.Drawing.Size(77, 25)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(110, 30)
        Me.btnSave.TabIndex = 140
        Me.btnSave.Text = "Speichern"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtLast3
        '
        Me.txtLast3.BackColor = System.Drawing.Color.White
        Me.txtLast3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLast3.ContextMenuStrip = Me.ContextMenuLast
        Me.txtLast3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txtLast3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtLast3.Location = New System.Drawing.Point(51, 187)
        Me.txtLast3.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.txtLast3.MinimumSize = New System.Drawing.Size(40, 0)
        Me.txtLast3.Name = "txtLast3"
        Me.txtLast3.Size = New System.Drawing.Size(54, 26)
        Me.txtLast3.TabIndex = 90
        Me.txtLast3.Tag = ""
        Me.txtLast3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ContextMenuLast
        '
        Me.ContextMenuLast.BackColor = System.Drawing.SystemColors.Info
        Me.ContextMenuLast.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.ContextMenuLast.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.ContextMenuLast.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLast})
        Me.ContextMenuLast.Name = "ContextMenuLast"
        Me.ContextMenuLast.ShowImageMargin = False
        Me.ContextMenuLast.Size = New System.Drawing.Size(138, 30)
        '
        'mnuLast
        '
        Me.mnuLast.DoubleClickEnabled = True
        Me.mnuLast.Name = "mnuLast"
        Me.mnuLast.Size = New System.Drawing.Size(137, 26)
        Me.mnuLast.Text = "Hantel-Last:"
        Me.mnuLast.ToolTipText = "Klick = Übernehmen" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ESC   = Schließen"
        '
        'txtLast2
        '
        Me.txtLast2.BackColor = System.Drawing.Color.White
        Me.txtLast2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLast2.ContextMenuStrip = Me.ContextMenuLast
        Me.txtLast2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txtLast2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtLast2.Location = New System.Drawing.Point(51, 155)
        Me.txtLast2.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.txtLast2.MinimumSize = New System.Drawing.Size(40, 0)
        Me.txtLast2.Name = "txtLast2"
        Me.txtLast2.Size = New System.Drawing.Size(54, 26)
        Me.txtLast2.TabIndex = 80
        Me.txtLast2.Tag = ""
        Me.txtLast2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtLast1
        '
        Me.txtLast1.BackColor = System.Drawing.Color.White
        Me.txtLast1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLast1.ContextMenuStrip = Me.ContextMenuLast
        Me.txtLast1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.txtLast1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtLast1.Location = New System.Drawing.Point(51, 123)
        Me.txtLast1.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.txtLast1.MinimumSize = New System.Drawing.Size(40, 0)
        Me.txtLast1.Name = "txtLast1"
        Me.txtLast1.Size = New System.Drawing.Size(54, 26)
        Me.txtLast1.TabIndex = 70
        Me.txtLast1.Tag = ""
        Me.txtLast1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Yellow
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(216, 23)
        Me.Label3.MinimumSize = New System.Drawing.Size(60, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 25)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "&Durchgang"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboDurchgang
        '
        Me.cboDurchgang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboDurchgang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboDurchgang.BackColor = System.Drawing.SystemColors.Window
        Me.cboDurchgang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDurchgang.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cboDurchgang.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDurchgang.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cboDurchgang.FormattingEnabled = True
        Me.cboDurchgang.Items.AddRange(New Object() {"Reißen", "Stoßen"})
        Me.cboDurchgang.Location = New System.Drawing.Point(309, 22)
        Me.cboDurchgang.MinimumSize = New System.Drawing.Size(74, 0)
        Me.cboDurchgang.Name = "cboDurchgang"
        Me.cboDurchgang.Size = New System.Drawing.Size(96, 28)
        Me.cboDurchgang.TabIndex = 20
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label2.Location = New System.Drawing.Point(22, 74)
        Me.Label2.MinimumSize = New System.Drawing.Size(36, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 20)
        Me.Label2.TabIndex = 50
        Me.Label2.Text = "&Heber"
        '
        'cboHeber
        '
        Me.cboHeber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cboHeber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboHeber.ContextMenuStrip = Me.ContextMenuHeber
        Me.cboHeber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeber.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cboHeber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.cboHeber.FormattingEnabled = True
        Me.cboHeber.Location = New System.Drawing.Point(79, 71)
        Me.cboHeber.MinimumSize = New System.Drawing.Size(235, 0)
        Me.cboHeber.Name = "cboHeber"
        Me.cboHeber.Size = New System.Drawing.Size(326, 28)
        Me.cboHeber.TabIndex = 60
        '
        'ContextMenuHeber
        '
        Me.ContextMenuHeber.AllowMerge = False
        Me.ContextMenuHeber.BackColor = System.Drawing.SystemColors.Info
        Me.ContextMenuHeber.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.ContextMenuHeber.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.ContextMenuHeber.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHeberId})
        Me.ContextMenuHeber.Name = "ContextMenuHeber"
        Me.ContextMenuHeber.ShowImageMargin = False
        Me.ContextMenuHeber.ShowItemToolTips = False
        Me.ContextMenuHeber.Size = New System.Drawing.Size(122, 30)
        '
        'mnuHeberId
        '
        Me.mnuHeberId.Name = "mnuHeberId"
        Me.mnuHeberId.Size = New System.Drawing.Size(121, 26)
        Me.mnuHeberId.Text = "Heber-ID:"
        Me.mnuHeberId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label5.Location = New System.Drawing.Point(111, 190)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(26, 20)
        Me.Label5.TabIndex = 545
        Me.Label5.Text = "kg"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label4.Location = New System.Drawing.Point(111, 158)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(26, 20)
        Me.Label4.TabIndex = 545
        Me.Label4.Text = "kg"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label1.Location = New System.Drawing.Point(111, 126)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(26, 20)
        Me.Label1.TabIndex = 545
        Me.Label1.Text = "kg"
        '
        'btnEscape
        '
        Me.btnEscape.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEscape.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnEscape.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnEscape.FlatAppearance.BorderSize = 0
        Me.btnEscape.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnEscape.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnEscape.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEscape.Font = New System.Drawing.Font("Wingdings 2", 14.0!, System.Drawing.FontStyle.Bold)
        Me.btnEscape.ForeColor = System.Drawing.Color.DarkGray
        Me.btnEscape.Location = New System.Drawing.Point(389, 0)
        Me.btnEscape.Name = "btnEscape"
        Me.btnEscape.Size = New System.Drawing.Size(38, 30)
        Me.btnEscape.TabIndex = 563
        Me.btnEscape.TabStop = False
        Me.btnEscape.Text = ""
        Me.btnEscape.UseVisualStyleBackColor = False
        '
        'lblTitleBar
        '
        Me.lblTitleBar.BackColor = System.Drawing.Color.Transparent
        Me.lblTitleBar.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTitleBar.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitleBar.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitleBar.ForeColor = System.Drawing.Color.White
        Me.lblTitleBar.Location = New System.Drawing.Point(0, 0)
        Me.lblTitleBar.Name = "lblTitleBar"
        Me.lblTitleBar.Padding = New System.Windows.Forms.Padding(7, 0, 0, 0)
        Me.lblTitleBar.Size = New System.Drawing.Size(423, 30)
        Me.lblTitleBar.TabIndex = 501
        Me.lblTitleBar.Text = "Korrektur"
        Me.lblTitleBar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmKorrektur
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.RoyalBlue
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(423, 313)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnEscape)
        Me.Controls.Add(Me.pnlForm)
        Me.Controls.Add(Me.lblTitleBar)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmKorrektur"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.TopMost = True
        Me.pnlForm.ResumeLayout(False)
        Me.pnlForm.PerformLayout()
        CType(Me.dgvGruppe, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlError.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTechnik.ResumeLayout(False)
        Me.pnlTechnik.PerformLayout()
        Me.ContextMenuTechnik.ResumeLayout(False)
        CType(Me.txtLast3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuLast.ResumeLayout(False)
        CType(Me.txtLast2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLast1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuHeber.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnlForm As Panel
    Friend WithEvents txtWertung3 As TextBox
    Friend WithEvents txtWertung1 As TextBox
    Friend WithEvents txtWertung2 As TextBox
    Friend WithEvents chkV3 As CheckBox
    Friend WithEvents chkU3 As CheckBox
    Friend WithEvents chkG3 As CheckBox
    Friend WithEvents chkV2 As CheckBox
    Friend WithEvents chkU2 As CheckBox
    Friend WithEvents chkG2 As CheckBox
    Friend WithEvents chkV1 As CheckBox
    Friend WithEvents chkU1 As CheckBox
    Friend WithEvents chkG1 As CheckBox
    Friend WithEvents lblVersuch3 As Label
    Friend WithEvents lblVersuch2 As Label
    Friend WithEvents lblVersuch1 As Label
    Friend WithEvents pnlTechnik As Panel
    Friend WithEvents txtTechnik3 As TextBox
    Friend WithEvents lblN3 As Label
    Friend WithEvents lblN2 As Label
    Friend WithEvents lblN1 As Label
    Friend WithEvents txtTechnik2 As TextBox
    Friend WithEvents txtTechnik1 As TextBox
    Friend WithEvents btnWiederholen As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents txtLast3 As NumericUpDown
    Friend WithEvents txtLast2 As NumericUpDown
    Friend WithEvents txtLast1 As NumericUpDown
    Friend WithEvents Label3 As Label
    Friend WithEvents cboDurchgang As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents cboHeber As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents ContextMenuHeber As ContextMenuStrip
    Friend WithEvents mnuHeberId As ToolStripMenuItem
    Friend WithEvents ContextMenuLast As ContextMenuStrip
    Friend WithEvents mnuLast As ToolStripMenuItem
    Friend WithEvents pnlError As Panel
    Friend WithEvents lblError As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ContextMenuTechnik As ContextMenuStrip
    Friend WithEvents mnuNote As ToolStripMenuItem
    Friend WithEvents dgvGruppe As DataGridView
    Friend WithEvents Groups As DataGridViewTextBoxColumn
    Friend WithEvents TG As DataGridViewTextBoxColumn
    Friend WithEvents WG As DataGridViewTextBoxColumn
    Friend WithEvents Bezeichnung As DataGridViewTextBoxColumn
    Friend WithEvents pnlGruppe As Panel
    Friend WithEvents lblGruppe As Label
    Friend WithEvents btnEscape As Button
    Friend WithEvents lblTitleBar As Label
End Class
