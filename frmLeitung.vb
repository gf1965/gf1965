﻿
Imports System.ComponentModel
Imports System.Math
Imports System.Threading.Tasks
Imports MySqlConnector

Public Class frmLeitung

    Dim Bereich As String = "Leitung"

    'Private Hantel As New myHantel

    Private ZN_Sperrzeit As Integer = 5000 ' G E S P E R R T

    Dim cConn As MySqlConnection
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader

    Dim dvGruppen As DataView
    Dim bsGruppen As BindingSource
    Dim dvNotGrouped(1) As DataView

    Dim dtGruppen As DataTable

    Dim loading As Boolean = True

    Private func As myFunction = New myFunction

    Private Delegate Sub DelegateSub()
    Private Delegate Sub DelegateSubArgs(sender As Object, e As EventArgs)
    Private Delegate Function FunctionBoolean() As Boolean
    Private Delegate Function FunctionStringList(Value As List(Of String)) As Boolean
    Private Delegate Sub DelegateIntegerString(ix As Integer, value As String)
    Private Delegate Sub DelegateInteger(Value As Integer)
    Private Delegate Sub DelegateIntegerInteger(Value1 As Integer, Value2 As Integer)
    Private Delegate Sub DelegateString(Value As String)
    Private Delegate Sub DelegateStringList(Value As List(Of String))
    Private Delegate Sub DelegateStringString(Value1 As String, Value2 As String)
    Private Delegate Sub DelegateBoolean(Value As Boolean)
    Private Delegate Sub DelegateColor(Value As Color)
    Private Delegate Sub DelegateIntegerColor(Index As Integer, Value As Color)
    Private Delegate Sub DelegateDurchgangBeendet(Wertung As Integer, Zeit As Date, Note As Double, Lampen As String, JuryWertung As Boolean) ', ByVal HeberID As Integer)
    Private Delegate Sub DelegateSaveKorrektur(Teilnehmer As Integer, TableIndex As Integer, Versuche As Dictionary(Of Integer, List(Of String)))
    Private Delegate Sub DelegateWrite(addition As Integer, row As DataRow())
    Private Delegate Sub DelegateVerzicht(addition As Integer, row As DataRow(), ExchangeLifter As Boolean)

    Dim Gültig As String = Chr(252)
    Dim Ungültig As String = Chr(251)
    Dim Verzicht As String = "l" 'Chr(75) '164)    'Chr(75) Chr(163) Chr(161)

    Dim pnlPos As Point
    Dim dicLampen As New Dictionary(Of Integer, TextBox) 'Container für Lampen-Anzeige

    Dim loadingKorrektur As Boolean
    Dim rowsHeber(1)() As DataRow
    Dim dtKorrektur(1) As DataTable
    Dim dvKorrektur(1) As DataView

    Dim Panels As New Dictionary(Of String, Panel)
    Dim PanelsLocation As New Dictionary(Of String, Point)

    Dim nonNumberEntered As Boolean

    Dim IsMouseWeeling As Boolean

    Dim dgvCtrlPressed As Boolean
    Private dgvFontSize As Single

    Dim CloseForm As Boolean

    Private _sortorder As String
    'Private _insertion As String
    Property SortInsertion As String
    'Get
    '    Return _insertion
    'End Get
    'Set(value As String)
    '    _insertion = value
    '    Try
    '        'bsLeader.Sort = _insertion + SortOrder
    '        'bsDelete.Sort = _insertion + SortOrder
    '        'dvLeader(1).Sort = _insertion + SortOrder
    '        'Sortierung()
    '        'dgv2_Reihenfolge()
    '    Catch ex As Exception
    '    End Try
    'End Set
    'End Property
    Property SortOrder As String
        Get
            Return _sortorder
        End Get
        Set(value As String)
            _sortorder = value
            If Not loading Then
                Try
                    bsLeader.Sort = SortInsertion + _sortorder
                    bsDelete.Sort = SortInsertion + _sortorder
                    dvLeader(1).Sort = SortInsertion + _sortorder
                    Sortierung()
                Catch ex As Exception
                End Try
            End If
        End Set
    End Property

    'Private timerPause As Threading.Timer

    ' Manuelle Wertung
    Private Event Verify_Technik(Index As Integer)
    Private T_Verified(3) As Boolean
    Private dicTechnik As New Dictionary(Of Integer, TextBox)
    Private dicWertung As New Dictionary(Of Integer, Dictionary(Of Integer, RadioButton)) ' Wertung(Index, Button)

    ' ToDo: Heber einlesen mit folgendem Status und entsprechend speichern
    Private Enum AttemptState
        Pending = 0     ' noch nicht gelaufen (dvLeader & dvResults)
        Active = 1      ' gelaufen, aber noch in der Anzeige
        Complete = 2    ' fertig (dvLeader) --> in dvDelete verschieben
        Finished = 3    ' beendet (3. Versuch in dvResults geschrieben)
    End Enum

    Private _panik As Boolean
    Property Panik As Boolean
        Get
            Return _panik
        End Get
        Set(value As Boolean)
            _panik = value
            If Not _panik Then Leader.RowNum_Offset = 0
            btnVerzicht.Enabled = Not _panik
        End Set
    End Property

    Dim Panik_Raised As Boolean ' Panik-Modus eingeschaltet --> SendToBohle muss mit RowNum ohne RowNumOffset arbeiten

    Private DisplayedForms As List(Of KeyValuePair(Of Integer, String))

#Region "TCP Server"
    Private Server As TcpListener
    Private Client As TcpClient
    Private IPEndPoint As IPEndPoint = New IPEndPoint(IPAddress.Any, CInt(User.ServerPort))
    ' Server
    Structure TCPConnection
        Dim Stream As NetworkStream
        Dim StreamW As StreamWriter
        Dim StreamR As StreamReader
        Dim NickName As String
    End Structure
    Private TCPConnections As New List(Of TCPConnection)
    ' Client
    Private Stream As NetworkStream
    Private StreamW As StreamWriter
    Private StreamR As StreamReader

    Private Event VE_verbinden(Connected As Boolean)
    Private Event TcpIpStatus_Changed(TcpIpStarted As Boolean)
    Private _tcpipstarted As Boolean
    Private Property TcpIpStarted As Boolean
        Get
            Return _tcpipstarted
        End Get
        Set(value As Boolean)
            _tcpipstarted = value
            RaiseEvent TcpIpStatus_Changed(value)
        End Set
    End Property
    Private _VE_verbunden As Boolean
    Property VE_verbunden As Boolean
        Get
            Return _VE_verbunden
        End Get
        Set(value As Boolean)
            'If Not value = _VE_verbunden Then
            _VE_verbunden = value
            RaiseEvent VE_verbinden(_VE_verbunden)
            'End If
        End Set
    End Property

    Private timerSuspend As Threading.Timer ' ZN-Pad nach "Zeit gestoppt" von Versuchermittler kurz sperren, damit der ZN nicht aus Versehen nochmal drücken kann

    Private Event Durchgang_Changed(Durchgang As Integer)
    'Private Event Gruppe_Changed(Gruppe As List(Of String))
    Private Event Blockheben_Changed(Value As Boolean) ' braucht nur lokal geändert werden, da die DataSources der Anzeigen die von Leader sind
    Private Event Wertung_Changed(Index As Integer, Wertung As Integer)
    Private Event Press_btnNext(HeberId As Integer) ' SyncLock
    Private Event Techniknote_Changed(Note As String)
    Private Event TechnikColor_Changed(Farbe As Color)
    Private Event Attempt_Finished(Wertung As Integer, Zeit As Date, Note As Double, Zeit As String, JuryWertung As Boolean) ' SyncLock
    Private Event Score_Clear(IsHeberPresent As Boolean)
    Private Event Save_Korrektur(TN As Integer, Index As Integer, Versuche As Dictionary(Of Integer, List(Of String))) ' Synclock
    'Private Event Steigerung_Write(addition As Integer, row As DataRow()) ' SyncLock
    'Private Event Bestätigung_Write(Versuch As Integer, row As DataRow()) ' SyncLock
    'Private Event Verzicht_Write(Versuch As Integer, row As DataRow(), ExchangeLifter As Boolean) ' SyncLock

    Private LockObject_Bohle As New Object
    Private LockObject_Versuch As New Object
    Private LockObject_Korrektur As New Object
    Private LockObject_Steigerung As New Object
    Private LockObject_Bestätigung As New Object
    Private LockObject_Verzicht As New Object
    Private Com_Thread As Threading.Thread
    Private Function Get_WertungFromKR_Pad(Optional Index As Integer = -1) As String
        If Not Index = -1 Then Return String.Concat(Index, ":", KR_Pad(Index).Wertung, ":", KR_Pad(Index).Techniknote)
        Dim pad As New List(Of String)
        Dim pads As New List(Of String)
        For Each item In KR_Pad
            pad.Add(item.Value.Index.ToString) ' bei Index = 0 --> nicht verbunden 
            pad.Add(item.Value.Wertung.ToString)
            pad.Add(item.Value.Techniknote.ToString.Replace(",", "."))
            pads.Add(Join(pad.ToArray, ":"))
            pad.Clear()
        Next
        Return Join(pads.ToArray, ",")
    End Function
    Private Sub Mit_VE_verbunden(Connected As Boolean)
        Try
            If InvokeRequired Then
                Dim d As New DelegateBoolean(AddressOf Mit_VE_verbunden)
                Invoke(d, New Object() {Connected})
            ElseIf User.UserId = UserID.Wettkampfleiter Then
                mnuHeberWechsel.Visible = Not Connected And Wettkampf.Mannschaft
                mnuZNPadSperrzeit.Visible = Connected
                btnSteigerung.Enabled = Not Connected
                btnVerzicht.Enabled = Not Connected
                bsLeader_PositionChanged(dgvLeader.CurrentRow.Index)
                ComMsg.Visible = Connected
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub bgwServer_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwServer.DoWork
        'AddHandler Steigerung_Write, AddressOf Write_Steigerung
        'AddHandler Bestätigung_Write, AddressOf Write_Bestätigung
        'AddHandler Verzicht_Write, AddressOf Write_Verzicht
        'AddHandler VE_verbinden, AddressOf Mit_VE_verbunden

        Try
            Server = New TcpListener(IPEndPoint)
            Server.Start()
            TcpIpStarted = True
            Client = New TcpClient
            Client = Server.AcceptTcpClient
            Stream = Client.GetStream
            StreamR = New StreamReader(Stream)
            StreamW = New StreamWriter(Stream)

            While Not bgwServer.CancellationPending
                'Com_Thread = New Threading.Thread(AddressOf ReceiveFromClients)
                'Com_Thread.Start()
                'Dim c As New TCPConnection
                'c.Stream = Client.GetStream
                'c.StreamR = New StreamReader(c.Stream)
                'c.StreamW = New StreamWriter(c.Stream)
                'c.NickName = c.StreamR.ReadLine
                'TCPConnections.Add(c)
                'Dim t As New Threading.Thread(AddressOf ReceiveFromClients)
                't.Start(c)
                ReceiveFromClients()
            End While
        Catch ex As Exception
            TcpIpStarted = False
            ' If Com_Thread.IsAlive Then Com_Thread.Abort()
            If Not bgwServer.CancellationPending Then
                Select Case Err.Number
                    Case <> 5
                        LogMessage("Leader: bgwServer_DoWork: " & ex.Message, ex.StackTrace &
                                   If(Not IsNothing(ex.InnerException), vbNewLine & ex.InnerException.Message + " ErrorCode:   " + Err.Number.ToString, String.Empty))
                End Select
            End If
        End Try
    End Sub
    Private Sub bgwServer_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwServer.RunWorkerCompleted

        If Not IsNothing(Server) Then Server.Stop()

        'If Com_Thread.IsAlive Then Com_Thread.Abort()

        'RemoveHandler Steigerung_Write, AddressOf Write_Steigerung
        'RemoveHandler Bestätigung_Write, AddressOf Write_Bestätigung
        'RemoveHandler Verzicht_Write, AddressOf Write_Verzicht
        'RemoveHandler VE_verbinden, AddressOf Mit_VE_verbunden

        If CloseForm Then Return

        TcpIpStarted = False

        'Using New Centered_MessageBox(Me)
        '    MessageBox.Show("Kommunikations-Server gestoppt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        'End Using
    End Sub
    'Private Sub ReceiveFromClients(ByVal con As Object)
    Private Sub ReceiveFromClients()
        'Dim c = CType(con, TCPConnection)
        Do
            Try
                'Dim msg = c.StreamR.ReadLine
                Dim msg = StreamR.ReadLine
                If Not IsNothing(msg) Then
                    If Not String.IsNullOrEmpty(msg) Then
                        Try
                            If msg.Equals("S") Then ' STOPP
                                If User.UserId = UserID.Wettkampfleiter Then
                                    Try
                                        Client = New TcpClient
                                        Client = Server.AcceptTcpClient
                                        Stream = Client.GetStream
                                        StreamR = New StreamReader(Stream)
                                        StreamW = New StreamWriter(Stream)
                                    Catch ex As Exception
                                        VE_verbunden = False
                                        LogMessage("Leader: ReceiveFromClients: STOPP: " & ex.Message, ex.StackTrace &
                                                    If(Not IsNothing(ex.InnerException), vbNewLine & ex.InnerException.Message + " ErrorCode: " + Err.Number.ToString, String.Empty))
                                    End Try
                                End If
                            End If
                            If msg.Substring(0, 1).Equals("V") Then
                                ' die Message kommt vom Versuchsermittler
                                Dim Target = String.Empty
                                Select Case msg.Substring(1, 1)
                                    Case "Q" ' Request for Status
                                        If Not IsNothing(dvLeader(0)) Then
                                            'Client_Send(Join(m.ToArray, ";")) ', c)
                                            Client_Send("Q" & String.Join(";", {"WK=" & Wettkampf.ID,
                                                                                "GP=" & If(Not IsNothing(Leader.Gruppe), Join(Leader.Gruppe.ToArray, ","), "0"),
                                                                                "IX=" & Leader.TableIx,
                                                                                "BH=" & Leader.Blockheben,
                                                                                "ID=" & Heber.Id,
                                                                                "VS=" & Heber.Versuch,
                                                                                "RN=" & Leader.RowNum,
                                                                                "CD=" & xAufruf.Text,               ' Countdown
                                                                                "UC=" & Get_ClockColor(Color.Black),  ' ClockColor richtig anzeigen, Zeit kommt von Timer
                                                                                "WE=" & Get_WertungFromKR_Pad(),    ' Wertungen übergeben, damit sie bei RowNum=1 angezeigt werden kann
                                                                                "TN=" & xTechniknote.Text}))
                                            VE_verbunden = True
                                            mnuHeberWechsel.Visible = False
                                            mnuZNPadSperrzeit.Visible = True
                                        End If
                                    Case "I" ' Steigerung
                                        Target = "Steigerung"
                                    Case "D" ' Bestätigung
                                        Target = "Bestätigung"
                                    Case "R" ' Verzicht
                                        Target = "Verzicht"
                                    Case "M" ' InfoMessage
                                    Case "Z" ' Zeit stoppen
                                        Zeit_stoppen()
                                    Case "S" ' Client gestoppt
                                        VE_verbunden = False
                                        mnuHeberWechsel.Visible = True
                                        mnuZNPadSperrzeit.Visible = False
                                    Case "X" ' Heberwechsel Mannschafts-WK
                                        Target = "Wechsel"
                                End Select
                                If Not String.IsNullOrEmpty(Target) Then TCPMessage_Proceed(Target, msg.Substring(2))

                            ElseIf msg.Substring(0, 1).Equals("J") Then ' ------------------------------------- JURY --------------------------------------------
                                ' die Message kommt von JuryControl
                                With JB_Dev
                                    If .Device = DevType.ControlUnit Then
                                        Using New Centered_MessageBox(Me)
                                            If MessageBox.Show("Ein Jury-Board (Typ: " & .Device & ") ist bereits angesteckt." + vbNewLine +
                                                               "Deaktivieren und mit Jury-Board (Typ: Raspberry) ersetzen?",
                                                                msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.No Then Return
                                        End Using
                                    End If
                                End With
                                Select Case msg.Substring(1, 1)
                                    Case "Q" ' Request for Status
                                        If Not IsNothing(dvLeader(0)) Then
                                            With JB_Dev
                                                .Device = DevType.Raspberry
                                                .Connected = True
                                            End With
                                            ' Status in Statusleiste aktualisieren --> JB angeschlossen
                                            Dim m As New List(Of String)
                                            m.Add("Q")
                                            m.Add(Wettkampf.KR(User.Bohle).ToString)    ' Anzahl KRs(Bohle)
                                            m.Add(User.Bohle.ToString)
                                            m.Add(Math.Abs(CInt(Wettkampf.Technikwertung)).ToString)
                                            m.Add(xAufruf.Text)                         ' Countdown
                                            m.Add(Get_ClockColor(Color.Black).ToString)   ' ClockColor richtig anzeigen
                                            m.Add(Leader.TableIx.ToString)              ' Validierung
                                            m.Add(Heber.Id.ToString)                    ' Validierung
                                            m.Add(Heber.Versuch.ToString)               ' Validierung
                                            m.Add(Leader.RowNum.ToString)               ' RowNum = 1: Versuch beendet
                                            m.Add(Get_WertungFromKR_Pad())              ' Wertungen übergeben, damit sie bei RowNum=1 angezeigt werden kann 
                                            m.Add(CInt(JB_Dev.KR_Pressed(1)).ToString & ":" & CInt(JB_Dev.KR_Pressed(2)).ToString & ":" & CInt(JB_Dev.KR_Pressed(3)).ToString)
                                            Client_Send(Join(m.ToArray, ";")) ', c)
                                        End If
                                    Case "E" ' Jury-Entscheidung
                                        Leader.Change_JuryEntscheid("Jury-Entscheidung")                           ' Jury-Entscheidung' anzeigen
                                        Leader.Change_InfoMessage("Jury-Entscheidung")
                                        If msg.Length > 2 Then
                                            Set_JuryWertung(msg.Substring(2))    ' Wertung 5 | 7
                                        End If
                                    Case "V" ' Verstoß-Text
                                        Dim v = Split(msg.Substring(2), ":")
                                        v(0) = Trim(v(0))
                                        Leader.Change_JuryEntscheid("Jury-Entscheidung" & vbNewLine & Trim(v(0)))   ' Verstoß anzeigen
                                        Leader.Change_InfoMessage("Jury-Entscheidung" & vbNewLine & Trim(v(0)))
                                        If v.Length > 1 AndAlso v(1).Equals("True") Then
                                            Set_JuryWertung(If(v(0).Substring(0, 1).Equals("G"), 5, 7).ToString) ' Verstoß nach Jury-Stopp gesendet --> msg == Wertung
                                        End If
                                    Case "R" ' Referee-Call (Ix)
                                        Dim ix = CInt(Split(msg, ":")(1))

                                    Case "S" ' Jury-Control beendet
                                        With JB_Dev
                                            .Device = DevType.None
                                            .Connected = False
                                        End With
                                        ' Status in Statusleiste aktualisieren --> JB entfernt
                                End Select
                            End If
                        Catch ex As Exception
                            LogMessage("Leader: ReceiveFromClients (ReceivedMessage: " & msg & "): " & ex.Message, ex.StackTrace)
                        End Try
                    End If
                ElseIf IsNothing(msg) Then
                    'Client = New TcpClient
                    'Client = Server.AcceptTcpClient
                    'Stream = Client.GetStream
                    'StreamRead = New StreamReader(Stream)
                    'StreamWrite = New StreamWriter(Stream)
                End If
            Catch ' ex As Exception
                'Try
                '    TCPConnections.Remove(c)
                'Catch
                'End Try
                Try
                    Client = New TcpClient
                    Client = Server.AcceptTcpClient
                    Stream = Client.GetStream
                    StreamR = New StreamReader(Stream)
                    StreamW = New StreamWriter(Stream)
                Catch ex As Exception
                    VE_verbunden = False
                    LogMessage("Leader: ReceiveFromClients: StreamReader: " & ex.Message, ex.StackTrace &
                                If(Not IsNothing(ex.InnerException), vbNewLine & ex.InnerException.Message + " ErrorCode: " + Err.Number.ToString, String.Empty))
                End Try
                Exit Do
            End Try
        Loop
    End Sub

    Private Sub Set_JuryWertung(_wertung As String)

        Dim _result = If(_wertung.Equals("5"), 1, -1)
        Dim _lampen = String.Empty
        Dim _note = Heber.T_Note

        For i = 1 To 3
            Anzeige.Change_Lampe(i, Anzeige.ScoreColors(CInt(_wertung)))
            'Leader.Change_Wertung(i, Anzeige.ScoreColors(CInt(_wertung)))
            If Wettkampf.KR(User.Bohle) = 3 OrElse i = 1 Then
                _lampen += _wertung
            Else
                _lampen += "0"
            End If
            With KR_Pad(i)
                If .Wertung <> CInt(_wertung) Then
                    .Wertung = CInt(_wertung)
                    If Wettkampf.Technikwertung Then
                        .Verified = .Wertung = 7
                        If .Wertung = 7 Then
                            .Techniknote = "0"
                            Change_Ungültig(i)
                        Else
                            Change_Technik_Display(i, , If(.Verified, LineType.Bestätigt, LineType.Techniknote))
                        End If
                    Else
                        If .Wertung = 5 Then
                            Change_Gültig(i)
                        Else
                            Change_Ungültig(i)
                        End If
                    End If
                End If
            End With
        Next

        With Heber
            If .Id > 0 Then
                If _result = 1 Then
                    If Wettkampf.Technikwertung AndAlso _note = 0 Then Return
                Else
                    _note = 0
                End If
                Versuch_beendet(_result, Now(), _note, _lampen, True)
                If Wettkampf.Technikwertung Then
                    Anzeige.T_Farbe = Anzeige.TechnikColors(_result)
                    Anzeige.T_Wert = If(_note = 0, "X,XX", Strings.Format(_note, "Fixed"))
                End If
            End If
        End With

    End Sub

    Private Function Client_Start() As DialogResult
        Dim dlgResult = DialogResult.None
        TcpIpStarted = False

        Do While String.IsNullOrEmpty(User.ServerIP)
            Using func As New myFunction
                User.ServerIP = func.Get_ServerIP(Me)
            End Using
            If String.IsNullOrEmpty(User.ServerIP) Then
                Using New Centered_MessageBox(Me)
                    dlgResult = MessageBox.Show("TCP/IP-Server nicht gefunden." & vbNewLine &
                                                 "(" & User.IPs(User.NetworkID).Typ & "-SSID: " & User.IPs(User.NetworkID).SSID & " - Wettkampf-Leitung nicht angemeldet)", msgCaption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation)
                End Using
                Return dlgResult
            End If
        Loop

        Cursor = Cursors.WaitCursor
        Try
            Client = New TcpClient
            Client.Connect(User.ServerIP, CInt(User.ServerPort))

            If Client.Connected Then
                Stream = Client.GetStream
                StreamR = New StreamReader(Stream)
                StreamW = New StreamWriter(Stream)
                TcpIpStarted = True
                bgwClient.RunWorkerAsync()
                Client_Send("VQ")
            Else
                ' TIPP: WK-Leitung erneut anmelden (Main/Anmeldung)
                ' --> ServerIP könnte durch Anmeldung des Versuchsermittlers als Wettkampfleiter in der DB überschrieben worden sein
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Keine Verbindung zum TCP/IP-Server." & vbNewLine & "(Server-IP: " & User.ServerIP & ":" & User.ServerPort & ")", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
                User.ServerIP = String.Empty
                Return DialogResult.Cancel
            End If
        Catch ex As Exception
            ' TIPP: WK-Leitung erneut anmelden (Main/Anmeldung)
            ' --> ServerIP könnte durch Anmeldung des Versuchsermittlers als Wettkampfleiter in der DB überschrieben worden sein
            Using New Centered_MessageBox(Me)
                dlgResult = MessageBox.Show("Keine Antwort von TCP/IP-Server " & User.ServerIP & ":" & User.ServerPort & vbNewLine &
                                            "(Wettkampf-Leitung nicht gefunden)", msgCaption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation)
            End Using
            User.ServerIP = String.Empty
        End Try

        Cursor = Cursors.Default
        Return dlgResult
    End Function
    Function Client_Receive() As String
        Try
            Return StreamR.ReadLine
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Sub Client_Send(ByVal Msg As String, Optional con As TCPConnection = Nothing)
        ' wenn c = Nothing --> an alle CLients senden
        Try
            If Not IsNothing(con.StreamW) Then
                con.StreamW.WriteLine(Msg)
                con.StreamW.Flush()
            ElseIf TCPConnections.Count > 0 Then
                For Each c As TCPConnection In TCPConnections
                    c.StreamW.WriteLine(Msg)
                    c.StreamW.Flush()
                Next
            ElseIf Not IsNothing(StreamW) Then
                StreamW.WriteLine(Msg)
                StreamW.Flush()
            End If
        Catch ex As Exception
            'TcpIpStarted = False
            'Using New Centered_MessageBox(Me)
            '    If MessageBox.Show("Die Verbindung zum " & ComMsg.Text & " wurde geschlossen." & vbNewLine & "Versuchen, die Verbindung wieder herzustellen?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then
            '        mnuTCP.PerformClick()
            '    ElseIf User.UserId = UserID.Wettkampfleiter Then
            '        btnSteigerung.Enabled = CInt(dvLeader(Leader.TableIx)(bsLeader.Position)!Reihenfolge) > 0
            '        btnVerzicht.Enabled = CInt(dvLeader(Leader.TableIx)(bsLeader.Position)!Reihenfolge) > 0
            '    ElseIf User.UserId = UserID.Versuchsermittler Then
            '        CloseForm = True
            '        bgwClient.CancelAsync()
            '        Close()
            '    End If
            'End Using
        End Try
    End Sub

    Sub bgwClient_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwClient.DoWork
        ' Client receives messages from server (ContestLeader)

        'AddHandler Gruppe_Changed, AddressOf Change_Gruppe
        'AddHandler Durchgang_Changed, AddressOf Change_Durchgang
        'AddHandler Blockheben_Changed, AddressOf Change_BH
        AddHandler Wertung_Changed, AddressOf Set_Wertung
        AddHandler Press_btnNext, AddressOf btnNext_Pressed
        AddHandler Techniknote_Changed, AddressOf TechnikWert_Changed
        AddHandler TechnikColor_Changed, AddressOf TechnikFarbe_Changed
        AddHandler Attempt_Finished, AddressOf Versuch_beendet
        AddHandler Score_Clear, AddressOf Wertung_Clear

        Try
            While Client.Connected AndAlso Not bgwClient.CancellationPending
                Try
                    ' Auswertung der vom Server empfangenen Daten
                    Dim msg = Client_Receive()
                    If Not String.IsNullOrEmpty(msg) Then
                        If User.UserId = UserID.Versuchsermittler Then
                            Select Case msg.Substring(0, 1)
                                Case "S" ' Server gestoppt
                                    CloseForm = True
                                    bgwClient.CancelAsync()
                                    Application.Exit()
                                Case "W" ' Wertung
                                    Dim value = Split(msg.Substring(1), "=")
                                    RaiseEvent Wertung_Changed(CInt(value(0)), CInt(value(1)))
                                Case "T" ' Techniknote
                                    Dim value = Split(msg, "=")
                                    RaiseEvent Techniknote_Changed(value(1))
                                Case "C" ' Technik-Farbe
                                    Dim value = CInt(Split(msg, "=")(1))
                                    RaiseEvent TechnikColor_Changed(Anzeige.TechnikColors(value))
                                Case "Z" ' CountDown
                                    Dim value = Split(msg.Substring(1), "=")
                                    Select Case value(0)
                                        Case "A" ' Aufruf
                                            Anzeige.CountDown = value(1)
                                        Case "C"
                                            Anzeige.ClockColor = Anzeige.Clock_Colors(CInt(value(1)))
                                    End Select
                                Case "N" ' nächster Heber
                                    RaiseEvent Press_btnNext(0)
                                Case "E" ' Versuch beendet
                                    Dim dic As New Dictionary(Of String, String)
                                    Dim kvp = Split(msg.Substring(1), ";")
                                    For Each item In kvp
                                        Dim s = Split(item, "=")
                                        dic(s(0)) = s(1)
                                    Next
                                    RaiseEvent Attempt_Finished(CInt(dic("W")), CDate(dic("Z")), CDbl(dic("N")), dic("L"), CBool(dic("K")))
                                    For i = 1 To 3
                                        If i > dic("L").Length Then
                                            Change_Lampe(i, Anzeige.ScoreColors(0))
                                            Anzeige.Change_Lampe(i, Anzeige.ScoreColors(0))
                                        Else
                                            Change_Lampe(i, Anzeige.ScoreColors(CInt(dic("L").Substring(i - 1, 1))))
                                            Anzeige.Change_Lampe(i, Anzeige.ScoreColors(CInt(dic("L").Substring(i - 1, 1))))
                                        End If
                                    Next
                                Case "L" ' Wertung löschen
                                    Dim value = CBool(Split(msg, "=")(1))
                                    RaiseEvent Score_Clear(value)
                                Case "G" ' Gruppe geändert
                                    Stop
                                    'RaiseEvent Gruppe_Changed(Split(msg.Substring(1), "=")(1))
                                Case "D" ' Durchgang geändert
                                    Dim dic = Dictionarize(msg, ";")
                                    If Not IsNothing(dvLeader(0)) Then
                                        RaiseEvent Durchgang_Changed(CInt(dic("DG")))
                                    Else
                                        If Wettkampf.Set_Properties(Me, dic("WK")) > 0 Then
                                            Glob.Load_Wettkampf_Tables(Me, Wettkampf.Identities.Keys.ToArray)
                                            Wettkampf_Load(dic("GP"), CInt(dic("RN")), CInt(dic("DG")), CBool(dic("BH")))
                                        End If
                                    End If
                                Case "B" ' Blockheben ausgewählt
                                    RaiseEvent Blockheben_Changed(CBool(Split(msg.Substring(1), "=")(1)))
                                Case "K" ' Korrektur
                                    Stop ' in allen Anzeigen des Clients ändern ???
                                    Dim values = Split(msg.Substring(1), ";")
                                    Dim dic As New Dictionary(Of Integer, List(Of String))
                                    For i = 2 To values.Count - 1
                                        Dim lst = Split(values(i), "_").ToList
                                        dic(i - 1) = lst
                                    Next
                                    RaiseEvent Save_Korrektur(CInt(values(1)), CInt(values(0)), dic)
                                Case "Q" ' Status des WKs von Leader 
                                    Dim dic = Dictionarize(msg.Substring(1), ";")
                                    Heber.Id = CInt(dic("ID"))
                                    Heber.Versuch = CInt(dic("VS"))
                                    Dim pads = Split(dic("WE"), ",")

                                    If Wettkampf.Set_Properties(Me, dic("WK")) > 0 Then
                                        Glob.Load_Wettkampf_Tables(Me, Wettkampf.Identities.Keys.ToArray)
                                        Wettkampf_Load(dic("GP"), CInt(dic("RN")), CInt(dic("IX")), CBool(dic("BH")))
                                    End If

                                    Anzeige.ClockColor = Anzeige.Clock_Colors(CInt(dic("UC")))
                                    Anzeige.CountDown = dic("CD")
                                    ' Wertungen setzen
                                    If Leader.RowNum = 1 Then
                                        Leader.DimOut = 0
                                        For i = 0 To pads.Count - 1
                                            Dim pad = Split(pads(i), ":")
                                            Dim Ix = CInt(pad(0))
                                            Dim Wertung = CInt(pad(1))
                                            Dim Technik = CDbl(pad(2))
                                            If Ix = 0 Then
                                                Change_Lampe(i + 1, Anzeige.ScoreColors(0))
                                                Anzeige.Change_Lampe(i + 1, Anzeige.ScoreColors(0))
                                            Else
                                                Change_Lampe(Ix, Anzeige.ScoreColors(Wertung))
                                                Anzeige.Change_Lampe(Ix, Anzeige.ScoreColors(Wertung))
                                            End If
                                        Next
                                        xTechniknote.Text = dic("TN")
                                        xTechniknote.ForeColor = Anzeige.TechnikColors(Leader.RowNum)
                                    End If
                            End Select
                        End If
                    End If
                Catch ex As Exception
                End Try
            End While
        Catch ex As Exception
            Stop ' Thread übergreifender Vorgang bei WK-Leiter beennden
        End Try
    End Sub
    Private Sub bgwClient_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwClient.RunWorkerCompleted

        If Not IsNothing(Client) Then Client.Close()
        If Not IsNothing(StreamR) Then StreamR.Close()
        If Not IsNothing(StreamW) Then StreamW.Close()
        If Not IsNothing(Stream) Then Stream.Close()

        TcpIpStarted = False

        'RemoveHandler Gruppe_Changed, AddressOf Change_Gruppe
        'RemoveHandler Durchgang_Changed, AddressOf Change_Durchgang
        'RemoveHandler Blockheben_Changed, AddressOf Change_BH
        RemoveHandler Wertung_Changed, AddressOf Set_Wertung
        RemoveHandler Press_btnNext, AddressOf btnNext_Pressed
        RemoveHandler Techniknote_Changed, AddressOf TechnikWert_Changed
        RemoveHandler TechnikColor_Changed, AddressOf TechnikFarbe_Changed
        RemoveHandler Attempt_Finished, AddressOf Versuch_beendet
        RemoveHandler Score_Clear, AddressOf Wertung_Clear

        If CloseForm Then
            Close()
            Return
        End If

        Try
            cboDurchgang.SelectedIndex = -1
        Catch ex As Exception
        End Try
        chkBH.CheckState = CheckState.Indeterminate
    End Sub

    Private Function Dictionarize(data As String, delimiter As String) As Dictionary(Of String, String)
        ' Usage:
        ' Dim dic = Dictionarize(msg, ";")

        Dim values = Split(data, delimiter)
        If values.Count = 0 Then Return Nothing
        Dim dic As New Dictionary(Of String, String)
        For Each value In values
            If value.Contains("=") Then
                Dim v = Split(value, "=")
                dic.Add(v(0), v(1))
            End If
        Next
        Return dic
    End Function
    Private Sub TCPMessage_Proceed(Target As String, Value As String)
        If InvokeRequired Then
            Dim d As New DelegateStringString(AddressOf TCPMessage_Proceed)
            Invoke(d, New Object() {Target, Value})
        Else
            Try
                Dim Teilnehmer = String.Empty
                Dim Versuch = String.Empty
                Dim Hantellast = String.Empty
                Dim Position = String.Empty

                Dim lst = Split(Value, ";")

                For Each item In lst
                    Dim p = Split(item, "=")
                    Select Case p(0)
                        Case "T" : Teilnehmer = p(1)
                        Case "V" : Versuch = p(1)
                        Case "H" : Hantellast = p(1)
                        Case "P" : Position = p(1)
                        Case "A" : Position = bsLeader.Find("Teilnehmer", p(1)).ToString
                    End Select
                Next

                If String.IsNullOrEmpty(Teilnehmer) Then Return

                ' alle verbleibenden Versuche des Hebers
                Dim row() = dtLeader(Leader.TableIx).Select("Teilnehmer = " + Teilnehmer +
                                                        " AND Convert(IsNull(Wertung,''), System.String) = '' AND Convert(IsNull(Reihenfolge,''), System.String) <> ''",
                                                        "Versuch ASC")
                If Not Target.Equals("Wechsel") Then
                    If row.Count = 0 Then Return
                    If Not IsNumeric(Versuch) AndAlso Not row(0)!Versuch.ToString.Equals(Versuch) Then Return
                End If

                Select Case Target
                    Case "Steigerung"
                        If String.IsNullOrEmpty(Hantellast) OrElse Not IsNumeric(Hantellast) Then Return
                        Dim addition = CInt(Hantellast) - CInt(row(0)!HLast)
                        Write_Steigerung(addition, row)
                    Case "Bestätigung"
                        Write_Bestätigung(CInt(Versuch), row)
                    Case "Verzicht"
                        Write_Verzicht(CInt(Versuch), row)
                    Case "Wechsel"
                        dgvLeader.Enabled = False
                        bsLeader.Position = CInt(Position)
                        Change_Lifters(CInt(Teilnehmer), dvLeader(Leader.TableIx)(bsLeader.Position)!Gruppe.ToString)
                        dgvLeader.Enabled = True
                End Select

            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub Change_TcpIpStatus(Started As Boolean)
        Try
            If InvokeRequired Then
                Dim d As New DelegateBoolean(AddressOf Change_TcpIpStatus)
                Invoke(d, New Object() {Started})
            Else
                If Started Then
                    ComMsg.Image = ImageList1.Images("Client_Up.png")
                    mnuTCP.Text = ComMsg.Text & " stoppen"
                Else
                    ComMsg.Image = ImageList1.Images("Client_Down.png")
                    mnuTCP.Text = ComMsg.Text & " starten"
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region

    Private Sub MoveToDelete()

        ' oberste Zeile ausblenden
        Leader.RowNum = 0
        'Dim pos = 0
        Dim pos = Leader.RowNum_Offset

        ' gewerteten Heber in Delete verschieben
        loading = True
        dtDelete(Leader.TableIx).ImportRow(dvLeader(Leader.TableIx)(pos).Row)
        dvLeader(Leader.TableIx)(pos).Delete()
        bsDelete.EndEdit()
        bsLeader.EndEdit()
        dtLeader(Leader.TableIx).AcceptChanges()
        dtDelete(Leader.TableIx).AcceptChanges()
        loading = False

        ' erste Zeile zum Bearbeiten freigeben
        If dgvLeader.Rows.Count > 0 Then
            dgvLeader.Rows(Leader.RowNum + Leader.RowNum_Offset).ReadOnly = False
        End If

        ' dgvWertung & dgvTechnik in Moderator ausblenden
        'Leader.Change_Wertung(0)
    End Sub
    Private Sub SendtoBohle(Optional HeberId As Integer = 0) ' HeberId > 0 ---> bei Korrektur (sonst immer 0)
        If InvokeRequired Then
            Dim d As New DelegateInteger(AddressOf SendtoBohle)
            Invoke(d, New Object() {HeberId})
        Else
            SyncLock LockObject_Bohle

                Dim Uhr_angelaufen = False
                Dim PrevHeberId As Integer
                Dim ActHeberRow As DataRow

                Try
                    ' bei Korrektur ist der ActHeber der nächstfolgende Heber, da er von Delete in Leader verschoben worden ist
                    ActHeberRow = dvLeader(Leader.TableIx)(Leader.RowNum + Leader.RowNum_Offset).Row

                    If Leader.RowNum = 1 Then
                        MoveToDelete()
                    Else
                        Uhr_angelaufen = Anzeige.ClockColor <> Anzeige.Clock_Colors(Clock_Status.Waiting)
                    End If

                    WK_Reset() ' Wertung zurücksetzen (Blaue Taste in Steuerung)

                    With Heber
                        .Id = CInt(ActHeberRow!Teilnehmer)
                        .Nachname = ActHeberRow!Nachname.ToString
                        .Vorname = ActHeberRow!Vorname.ToString
                        .Startnummer = ActHeberRow!Startnummer.ToString
                        .Set_Name(.Nachname, .Vorname)
                        If Wettkampf.International Then
                            .Verein = ActHeberRow!state_iso3.ToString
                        Else
                            .Verein = ActHeberRow!Vereinsname.ToString
                        End If
                        .Auswertung = CInt(ActHeberRow!Auswertung)
                        .Auswertung2 = CInt(ActHeberRow!Auswertung2)
                        .Gruppierung2 = CInt(ActHeberRow!Gruppierung2)
                        .Flagge = ActHeberRow!state_flag.ToString
                        .Sex = ActHeberRow!sex.ToString
                        .Wiegen = CDbl(ActHeberRow!Wiegen)
                        .AK = Split(ActHeberRow!AK.ToString, " ")(0) 'Replace(ActHeberRow!AK.ToString, " ", "") 'Join(Split(ActHeberRow!AK.ToString, " "), "")
                        If Wettkampf.Mannschaft Then
                            .GK = Strings.Format(CDbl(ActHeberRow!Wiegen), "#.0 kg")
                        Else
                            .GK = ActHeberRow!GK.ToString + " kg"
                        End If
                        .JG = CInt(ActHeberRow!Jahrgang)
                        .Versuch = CInt(ActHeberRow!Versuch)
                        .Hantellast = CInt(ActHeberRow!HLast)
                        .Hantelstange = Hantel.Get_Stange(.Sex, CInt(.JG), .Hantellast)

                        'Try
                        ' letzter nicht verzichteter Versuch aus Delete
                        Dim tmp = New DataView(dvDelete(Leader.TableIx).ToTable, "Wertung > -2", Nothing, DataViewRowState.CurrentRows)
                        If tmp.Count > 0 Then PrevHeberId = CInt(tmp(tmp.Count - 1)!Teilnehmer)
                        'Catch ex As Exception
                        '    ' wenn kein Einrtag vorhanden, aktueller Heber
                        '    PrevHeberId = Heber.Id
                        'End Try

                        ' Countdown bestimmen
                        '   H_1 hat 2 Minuten
                        '   H_1 steigert --> H_2 wird aufgerufen
                        '   H_2 hat 1 Minute
                        '   H_2 steigert --> H_1 wird aufgerufen	--> Zeit für H_2 ist angelaufen 	    -> H_1 hat 1 Minute
                        '	    				    				--> Zeit für H_2 ist nicht angelaufen   -> H_1 hat 2 Minuten                    
                        Try
                            If PrevHeberId = CInt(ActHeberRow!Teilnehmer) Then
                                If Not Uhr_angelaufen Then
                                    .CountDown = 120
                                Else
                                    .CountDown = 60
                                End If
                            Else
                                .CountDown = 60
                            End If
                        Catch ex As Exception
                            .CountDown = 60
                        Finally
                            Leader.CountDown = .CountDown
                        End Try
                        .Wertung = 0
                        .T_Note = 0
                        ' Caption in pnlManuell ändern
                        If pnlManuell.Visible Then lblManuell.Text = ActHeberRow!Nachname.ToString + ", " + ActHeberRow!Vorname.ToString

                        ' Panikmodus --> für aktuellen Versuch HLast eintragen
                        If Panik Then
                            Dim pos = bsResults.Find("Teilnehmer", .Id)
                            Leader.Change_Anfangslast(.Hantellast, pos, Strings.Left(Leader.Table, 1) + "_" + .Versuch.ToString,, True)
                        End If

                    End With

                    ' aufzurufende Versuche in Publikum markieren
                    Leader.Mark_CalledLifters(Leader.Get_Called_Lifters())

                    With dgvLeader
                        .Refresh()
                        .Focus()
                        .ClearSelection()
                    End With

                    ' aufgerufenen Versuch markieren
                    Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
                    Dim Id = CInt(dvLeader(Leader.TableIx)(Leader.RowNum + Leader.RowNum_Offset)!Teilnehmer)
                    ' Event für frmModerator, um rtfNotiz & dgvHeber neu einzulesen
                    Leader.Set_Notiz(Id)

                    ' Panik-Modus 
                    For i = 0 To Leader.RowNum_Offset - 1
                        dgvLeader.Rows(i).DefaultCellStyle.BackColor = Color.Orange
                    Next
                Catch ex As Exception
                End Try
            End SyncLock
        End If
    End Sub

    Private Sub Versuch_beendet(Wertung As Integer, Zeit As Date, Optional Note As Double = 0, Optional Lampen As String = "", Optional Korrektur As Boolean = False) ' Optional HeberID As Integer = 0)
        If InvokeRequired Then
            Dim d As New DelegateDurchgangBeendet(AddressOf Versuch_beendet)
            Invoke(d, New Object() {Wertung, Zeit, Note, Lampen, Korrektur})
        Else
            'SyncLock LockObject_Versuch
            Cursor = Cursors.WaitCursor

            ' Durchgang beendet an Client
            'If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then Client_Send(String.Concat("EW=", Wertung, ";Z=", Zeit.ToString, ";N=", Note.ToString, ";F=", "K=", Korrektur.ToString))
            'If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then Client_Send(String.Concat("EW=", Wertung, ";Z=", Zeit.ToString, ";N=", Note.ToString, ";L=", Lampen, ";K=", Korrektur.ToString))
            If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then Client_Send(Join({"EW=" & Wertung,
                                                                                                "Z=" & Zeit,
                                                                                                "N=" & Note,
                                                                                                "L=" & Lampen,
                                                                                                "K=" & Korrektur}, ";"))
            Using af As New AttemptFinished(Wertung, Zeit, Note, Lampen, Korrektur, Me)
                Dim t As New Threading.Thread(New Threading.ThreadStart(AddressOf af.Set_Wertung))
                t.Priority = Threading.ThreadPriority.Highest
                t.Start()
                'af.Set_Wertung()
            End Using

            Cursor = Cursors.Default
            'End SyncLock
        End If
    End Sub

    'Private Async Sub Call_OrderTable()
    Private Sub Call_OrderTable()
        If InvokeRequired Then
            Dim d As New DelegateSub(AddressOf Call_OrderTable)
            Invoke(d, New Object() {})
        Else
            Leader.RowNum = 1 + Leader.RowNum_Offset
            'If Await Task.Run(Function() OrderTable()) Then
            If OrderTable() Then
                Sortierung()
                Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
                Leader.DimOut = 1 - Leader.RowNum + Leader.RowNum_Offset
                For i = 0 To Leader.RowNum_Offset - 1
                    dgvLeader.Rows(i).DefaultCellStyle.BackColor = Color.Orange
                Next
            Else
                ' letzter Versuch
                dvLeader(Leader.TableIx)(0)("Reihenfolge") = 0
                Leader.DimOut = 1 - Leader.RowNum + Leader.RowNum_Offset
                'Leader.Change_Wertung(0)
            End If
        End If
    End Sub

    '' NumericInput, Eingabesteuerung
    Private Sub TextboxNumDecimal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtT1.KeyDown, txtT2.KeyDown, txtT3.KeyDown ', txtTechnik1.KeyDown, txtTechnik2.KeyDown, txtTechnik3.KeyDown
        nonNumberEntered = False

        Select Case e.KeyCode

            Case Keys.Enter, Keys.Down, Keys.Up
                Dim increase = 1
                If e.KeyCode = Keys.Up Then increase = -1
                Dim ix = CInt(Strings.Right(CType(sender, TextBox).Name, 1))
                If Not KR_Pad(ix).Techniknote.Equals("0") Then WA_Events.Push_KR(ix, vbLf)
                Do
                    ix += increase
                    If ix > 3 Then
                        ix = 1
                    ElseIf ix < 1 Then
                        ix = 3
                    End If
                    If dicTechnik.ContainsValue(CType(sender, TextBox)) AndAlso dicTechnik(ix).Enabled Then
                        dicTechnik(ix).Focus()
                        Exit Do
                    End If
                Loop
                e.Handled = True
                e.SuppressKeyPress = True

            Case Keys.Escape
                Dim ctl = CType(sender, TextBox)
                ctl.Text = If(IsNothing(ctl.Tag), String.Empty, ctl.Tag.ToString)
                e.Handled = True
                e.SuppressKeyPress = True

            Case Keys.Back
                CType(sender, TextBox).Text = String.Empty
                Dim ix = CInt(Strings.Right(CType(sender, TextBox).Name, 1))
                WA_Events.Push_KR(ix, vbCr)
                e.Handled = True
                e.SuppressKeyPress = True

            Case < Keys.D0, > Keys.D9
                If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                    If sender.Equals(txtT1) OrElse sender.Equals(txtT2) OrElse sender.Equals(txtT3) Then
                        If e.KeyCode = Keys.Oemcomma OrElse e.KeyCode = Keys.Decimal Then
                            nonNumberEntered = True
                        End If
                    Else
                        If e.KeyCode <> Keys.Oemcomma AndAlso e.KeyCode <> Keys.Decimal Then
                            nonNumberEntered = True
                        End If
                    End If
                End If
        End Select

        If ModifierKeys = Keys.Shift Then
            nonNumberEntered = True
        End If
    End Sub
    Private Sub TextboxNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtT1.KeyPress, txtT3.KeyPress, txtT2.KeyPress
        If nonNumberEntered = True Then
            e.Handled = True
        End If
    End Sub

    Private Sub Display_Status(Sender As String, Status As Boolean)
        Dim imgs = {ImageList1.Images("btnGreen.png"), ImageList1.Images("btnRed.png")}
        Select Case Sender
            Case "Result"
                statusResultat.Image = imgs(Abs(CInt(Status)))
            Case "Versuch"
                statusVersuch.Image = imgs(Abs(CInt(Status)))
            Case "Steigerung"
                statusSteigerung.Image = imgs(Abs(CInt(Status)))
        End Select
    End Sub

    '' Me
    Private Sub frmLeader_Closed(sender As Object, e As EventArgs) Handles Me.Closed

        RemoveHandler WA_Message.Status_Changed, AddressOf statusMsg_Update
        RemoveHandler Anzeige.CountDown_Changed, AddressOf CountDown_Changed
        RemoveHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed
        RemoveHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        RemoveHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed
        RemoveHandler Return_Wertung, AddressOf Versuch_beendet

        RemoveHandler Leader.Mark_Rows, AddressOf Heber_Highlight
        RemoveHandler Leader.Dim_Rows, AddressOf Heber_Dim
        RemoveHandler Leader.Pause_Changed, AddressOf Change_Pause

        RemoveHandler Verify_Technik, AddressOf txtTechniknote_Validating
        'RemoveHandler KR_Initialize, AddressOf Set_Initialized
        RemoveHandler Set_formLeader_btnNext_Enabled, AddressOf btnNext_Enabled
        RemoveHandler ForeColors_Change, AddressOf Wertung_Clear

        RemoveHandler WK_Options.MarkRows_Changed, AddressOf Heber_Highlight
        RemoveHandler LeaderGruppe_Changed, AddressOf Change_Gruppe

        'RemoveHandler RowCount_Changed, AddressOf dgv2_Move
        RemoveHandler RowIndex_Changed, AddressOf bsLeader_PositionChanged

        'RemoveHandler Anzeige.Clock_Stopped, AddressOf Countdown_Stoppen

        RemoveHandler Anzeige.Lampe_Changed_Sofort, AddressOf Change_Lampe

        RemoveHandler User.Bohle_Changed, AddressOf Change_Bohle

        'RemoveHandler ComMsg_Arrived, AddressOf ComMsg_TextChanged
        RemoveHandler TcpIpStatus_Changed, AddressOf Change_TcpIpStatus
        RemoveHandler ReturnFromKorrektur, AddressOf Update_From_Korrektur

        RemoveHandler Ansicht_Options.NameFormat_Changed, AddressOf NameFormat_Changed

        RemoveHandler Leader.Techniknote_Changed, AddressOf Techniknote_Jury
        RemoveHandler Leader.OutOfRange_Changed, AddressOf OutOfRange_Jury
        'RemoveHandler Leader.Ab_Zeichen, AddressOf AbZeichen_Jury

        RemoveHandler AttemptFinished.CallOrderTable, AddressOf Call_OrderTable
        'RemoveHandler AttemptFinished.SaveSteigerung, AddressOf Save_Steigerung
        'RemoveHandler AttemptFinished.SaveVersuch, AddressOf Save_Versuch
        'RemoveHandler AttemptFinished.SaveResults, AddressOf Save_Results
        'RemoveHandler AttemptFinished.SavePunkte, AddressOf Save_Punkte

        RemoveHandler Save.Display_Status, AddressOf Display_Status

        RemoveHandler VE_verbinden, AddressOf Mit_VE_verbunden

        RemoveHandler Wettkampf.Technik_Changed, AddressOf Technik_Changed

        Try
            For i = 0 To 1
                If Not IsNothing(dvLeader(i)) Then dvLeader(i).Dispose()
                If Not IsNothing(dvDelete(i)) Then dvDelete(i).Dispose()
                If Not IsNothing(dtLeader(i)) Then dtLeader(i).Dispose()
                If Not IsNothing(dtDelete(i)) Then dtDelete(i).Dispose()
            Next
        Catch ex As Exception
        End Try

        Leader.RowNum_Offset = 0

        MdiParent.TopMost = False

    End Sub
    Private Sub frmLeader_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        If Not btnNext.Text.Equals("WK beenden") Then
            If User.UserId = UserID.Wettkampfleiter AndAlso dgvLeader.Rows.Count > 0 Then
                Using New Centered_MessageBox(Me)
                    If MessageBox.Show("Wettkampf beenden?", "Gewichtheben", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = MsgBoxResult.No Then
                        e.Cancel = True
                        Return
                    End If
                End Using
            End If
        End If

        Client_Send("S") ' Stopp an Server/Client senden

        CloseForm = True
        NativeMethods.INI_Write(Bereich, "Konflikte", CStr(mnuKonflikte.Checked), App_IniFile)
        NativeMethods.INI_Write(Bereich, "ShowMessages", CStr(mnuMeldungen.Checked), App_IniFile)
        'NativeMethods.INI_Write(Bereich, "Bestätigung", CStr(mnuConfirm.Checked), App_IniFile)
        NativeMethods.INI_Write(Bereich, "Versuch", CStr(mnuAktuellenVersuch.Checked), App_IniFile)
        NativeMethods.INI_Write(Bereich, "Sperrzeit", ZN_Sperrzeit.ToString, App_IniFile)

        loading = True
        Heber.Clear()
        Heber.Id = 0
        Leader.RowNum = 0
        Leader.RowNum_Offset = 0
        If Not IsNothing(Leader.Gruppe) Then Leader.Gruppe.Clear()
        Leader.Durchgang = String.Empty

        If User.UserId = UserID.Wettkampfleiter Then
            RemoveHandler ZN_Pad.Change_btnAufruf_Enabled, AddressOf btnAufruf_Enabled_Changed
            Steuerung.KR_Steuerung_Dispose()
        End If

        frmPause.Close()

        ' TcpIp beenden
        ComMsg.Visible = False
        If User.UserId = UserID.Wettkampfleiter AndAlso Not IsNothing(Server) Then
            Server.Stop()
            bgwServer.CancelAsync()
        ElseIf Not IsNothing(Client) Then
            Client.Close()
            If Not IsNothing(StreamR) Then StreamR.Close()
            If Not IsNothing(StreamW) Then StreamW.Close()
            If Not IsNothing(Stream) Then Stream.Close()
        End If

        With formMain
            .mnuWettkampf.Enabled = True
            .mnuWK_New.Enabled = True
            .mnuWK_Edit.Enabled = True
            .mnuWK_Select.Enabled = True
            .mnuKorrektur.Enabled = True
            .mnuMeldung.Enabled = True
            '.mnuKampfgericht.Enabled = True
            .mnuEasyModus.Enabled = True
            .PictureBox1.Show()
        End With

    End Sub
    Private Sub frmLeader_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed

        ' Markierung der Versuche in Publikum entfernen
        Leader.Mark_CalledLifters(Nothing)

        For Each key In IsFormPresent({"Bohle", "Hantelbeladung", "TV-Wertung"}) ', "Moderator", "Versuchsreihenfolge"})
            Try
                dicScreens(key.Key).Form.Close()
            Catch ex As Exception
            End Try
        Next

        AppConnState = ConnState.NotAvailable

    End Sub
    Private Sub frmLeader_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        'AddHandler ComMsg_Arrived, AddressOf ComMsg_TextChanged

        With formMain
            .mnuWettkampf.Enabled = False
            .mnuWK_New.Enabled = False
            .mnuWK_Edit.Enabled = False
            .mnuWK_Select.Enabled = False
            .mnuKorrektur.Enabled = False
            .mnuMeldung.Enabled = False
            '.mnuKampfgericht.Enabled = False
            .mnuEasyModus.Enabled = False
            .PictureBox1.Hide()
        End With

        Wettkampf.WK_Modus = Modus.Indeterminate


        Panels.Add("Notizen", pnlNotizen)
        Panels.Add("Manuell", pnlManuell)
        Panels.Add("Steigerung", pnlSteigerung)

        PanelsLocation.Add("Notizen", New Point(pnlNotizen.Left, pnlNotizen.Top))
        PanelsLocation.Add("Manuell", New Point(pnlManuell.Left, pnlManuell.Top))
        PanelsLocation.Add("Steigerung", New Point((dgvLeader.Width - pnlSteigerung.Width) \ 2 - 30, (dgvLeader.Height - pnlSteigerung.Height) \ 2))

        btnAufruf.Enabled = False

        'Wertung
        dicLampen.Add(1, xGültig1)
        dicLampen.Add(2, xGültig2)
        dicLampen.Add(3, xGültig3)

        ' manuelle Wertung
        With dicTechnik
            .Add(1, txtT1)
            .Add(2, txtT2)
            .Add(3, txtT3)
        End With
        Dim dicG As New Dictionary(Of Integer, RadioButton)
        Dim dicU As New Dictionary(Of Integer, RadioButton)
        With dicG
            .Add(1, btnG1)
            .Add(2, btnG2)
            .Add(3, btnG3)
        End With
        With dicU
            .Add(1, btnU1)
            .Add(2, btnU2)
            .Add(3, btnU3)
        End With
        With dicWertung
            .Add(5, dicG)
            .Add(7, dicU)
        End With

        Try
            For i As Integer = 1 To 3
                dicLampen(i).Font = New Font(FontFamilies(Fonts.Wertungsanzeige), dicLampen(i).Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            Next
            xTechniknote.Font = New Font(FontFamilies(Fonts.Techniknote), xTechniknote.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf.Font = New Font(FontFamilies(Fonts.Zeitanzeige), xAufruf.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf.ForeColor = Anzeige.Clock_Colors(Clock_Status.Waiting)
        Catch ex As Exception
        End Try

        AddHandler WA_Message.Status_Changed, AddressOf statusMsg_Update
        AddHandler Anzeige.CountDown_Changed, AddressOf CountDown_Changed
        AddHandler Anzeige.ClockColor_Changed, AddressOf ClockColor_Changed
        AddHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        AddHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed
        AddHandler Return_Wertung, AddressOf Versuch_beendet

        AddHandler Leader.Mark_Rows, AddressOf Heber_Highlight
        AddHandler Leader.Dim_Rows, AddressOf Heber_Dim
        AddHandler Leader.Pause_Changed, AddressOf Change_Pause

        AddHandler Verify_Technik, AddressOf txtTechniknote_Validating
        AddHandler Set_formLeader_btnNext_Enabled, AddressOf btnNext_Enabled
        AddHandler ForeColors_Change, AddressOf Wertung_Clear

        AddHandler WK_Options.MarkRows_Changed, AddressOf Heber_Highlight

        AddHandler LeaderGruppe_Changed, AddressOf Change_Gruppe

        'AddHandler RowCount_Changed, AddressOf dgv2_Move
        AddHandler RowIndex_Changed, AddressOf bsLeader_PositionChanged

        AddHandler Anzeige.Lampe_Changed_Sofort, AddressOf Change_Lampe

        AddHandler User.Bohle_Changed, AddressOf Change_Bohle

        AddHandler TcpIpStatus_Changed, AddressOf Change_TcpIpStatus

        AddHandler ReturnFromKorrektur, AddressOf Update_From_Korrektur

        AddHandler Ansicht_Options.NameFormat_Changed, AddressOf NameFormat_Changed

        AddHandler Leader.Techniknote_Changed, AddressOf Techniknote_Jury
        AddHandler Leader.OutOfRange_Changed, AddressOf OutOfRange_Jury
        'AddHandler Leader.Ab_Zeichen, AddressOf AbZeichen_Jury

        AddHandler AttemptFinished.CallOrderTable, AddressOf Call_OrderTable
        'AddHandler AttemptFinished.SaveSteigerung, AddressOf Save_Steigerung
        'AddHandler AttemptFinished.SaveVersuch, AddressOf Save_Versuch
        'AddHandler AttemptFinished.SaveResults, AddressOf Save_Results
        'AddHandler AttemptFinished.SavePunkte, AddressOf Save_Punkte

        AddHandler Save.Display_Status, AddressOf Display_Status

        AddHandler VE_verbinden, AddressOf Mit_VE_verbunden

        AddHandler Wettkampf.Technik_Changed, AddressOf Technik_Changed

        'tTip = New ToolTip()
        'tTip.AutoPopDelay = 5000
        'tTip.InitialDelay = 1000
        'tTip.ReshowDelay = 500
        'tTip.ShowAlways = True
        'ToolTip1.SetToolTip(btnBestätigung, "Automatische Steigerung bestätigen")
        'ToolTip1.SetToolTip(btnPanik, "WK mit ausgewähltem Heber fortsetzen")
        'ToolTip1.SetToolTip(btnManuell, "aktuelle Wertung überschreiben")
        'ToolTip1.SetToolTip(btnNext, "Strg+N")
        'ToolTip1.SetToolTip(btnSteigerung, "Strg+S")
        'ToolTip1.SetToolTip(btnVerzicht, "Strg+V")
        'ToolTip1.SetToolTip(btnKorrektur, "Strg+K")
        'ToolTip1.SetToolTip(btnBestätigung, "Strg+B")
        'ToolTip1.SetToolTip(btnAufruf, "Strg+A")

        'bsLeader.RaiseListChangedEvents = False

        Progress1.Visible = False
        statusVersuch.Visible = False
        statusResultat.Visible = False
        statusSteigerung.Visible = False

        Glob.dtPrognose = New DataTable

        'If dicScreens.Count = 1 AndAlso dicScreens.Values(0).Key.Name.Equals("frmPublikum") Then
        '    dicScreens.Values(0).Key.Close()
        '    dicScreens.Remove(dicScreens.Keys(0))
        'End If

        DisplayedForms = New List(Of KeyValuePair(Of Integer, String))
        For Each key In dicScreens.Keys
            DisplayedForms.Add(New KeyValuePair(Of Integer, String)(dicScreens(key).Display, dicScreens(key).Form.Text))
        Next

        For Each key In IsFormPresent({"Wertung", "Moderator", "Versuchsreihenfolge"})
            Try
                dicScreens(key.Key).Form.Close()
            Catch ex As Exception
            End Try
        Next

    End Sub
    Private Sub frmLeader_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            If dgvHeber.Visible Then
                dgvHeber.Visible = False
            ElseIf pnlNotizen.Visible Then
                pnlNotizen.Visible = False
            ElseIf pnlSteigerung.Visible Then
                pnlSteigerung.Visible = False
            ElseIf pnlManuell.Visible AndAlso pnlManuell.Height = pnlManuell.MaximumSize.Height Then
                pnlManuell.Visible = False
            ElseIf dgvGruppe.Visible Then
                dgvGruppe.Visible = False
            End If
            e.SuppressKeyPress = True
            e.Handled = True
        ElseIf e.Control Then
            Select Case e.KeyCode
                Case Keys.N
                    If btnNext.Enabled Then btnNext.PerformClick()
                Case Keys.S
                    If btnSteigerung.Enabled Then btnSteigerung.PerformClick()
                Case Keys.V
                    If btnVerzicht.Enabled Then btnVerzicht.PerformClick()
                Case Keys.K
                    If btnKorrektur.Enabled Then btnKorrektur.PerformClick()
                Case Keys.B
                    If btnBestätigung.Enabled Then btnBestätigung.PerformClick()
                Case Keys.A
                    If btnAufruf.Enabled Then btnAufruf.PerformClick()
                    'Case Keys.H
                    '    ' kill unexpected horn signal
                    '    Try
                    '        Steuerung.ZA_Write()
                    '    Catch ex As Exception
                    '    End Try
            End Select
        End If
    End Sub
    Private Sub frmLeader_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Cursor = Cursors.WaitCursor

        If User.UserId = UserID.Wettkampfleiter AndAlso Not KR_IsInitialized Then
            Steuerung.KR_Steuerung_Load()
            AddHandler ZN_Pad.Change_btnAufruf_Enabled, AddressOf btnAufruf_Enabled_Changed
        End If

        'lblBohle.Text = "Bohle " & User.Bohle
        mnuHeberWechsel.Visible = Wettkampf.Mannschaft

        Refresh()

        ' Technikwertung positionieren
        With pnlWertung
            If Wettkampf.Technikwertung Then
                .Width = .MaximumSize.Width
                .Location = New Point(xAufruf.Left - .MaximumSize.Width + 100, xAufruf.Top)
            Else
                .Width = .MinimumSize.Width
                .Location = New Point(xAufruf.Left - .MinimumSize.Width + 100, xAufruf.Top)
            End If
        End With

        ' Kommunikation starten
        If User.UserId = UserID.Wettkampfleiter Then
            ComMsg.Text = "Server"
            bgwServer.RunWorkerAsync()
            If Not Wettkampf_Load() Then ' Invoke
                bgwServer.CancelAsync()
                Close()
                Return
            End If
            'WA_Message.Update()
            Defaults_Set() ' Invoke
        ElseIf User.UserId = UserID.Versuchsermittler Then
            ComMsg.Text = "Client"
            Dim dlgResult = DialogResult.None
            Do
                dlgResult = Client_Start()
                If dlgResult = DialogResult.Cancel Then
                    Close()
                    Return
                End If
            Loop While dlgResult = DialogResult.Retry
            Dim menus = {mnuOptionen.DropDownItems, ContextMenuGrid.Items}
            For Each mctl In menus
                For Each item In mctl
                    If TypeOf item Is ToolStripMenuItem Then
                        Dim ctl = CType(item, ToolStripMenuItem)
                        ctl.Visible = IsNothing(ctl.Tag)
                    ElseIf TypeOf item Is ToolStripSeparator Then
                        Dim ctl = CType(item, ToolStripSeparator)
                        ctl.Visible = IsNothing(ctl.Tag)
                    End If
                Next
            Next
            mnuPause.Visible = False
            btnPanik.Visible = False
            'btnManuell.Visible = False
            'ComMsg.Visible = False

            '' Test
            'Dim msg = "Q;44;0;0;True;0;1;0"
            'Dim sp = Split(msg, ";")
            'Dim WK = sp(1)
            'Dim Gruppe = sp(2)
            'Dim Durchgang = sp(3)
            'Dim Blockheben = sp(4)
            'Dim Teilnehmer = sp(5)
            'Dim Versuch = sp(6)
            'Dim RowNum = sp(7)
            'Wettkampf.ID = Wettkampf.Set_Properties(Me, WK)
            '' Tabellen für Wk einlesen
            'If Glob.Load_Wettkampf_Tables(Me) AndAlso Wettkampf.ID > 0 Then
            '    Wettkampf_Load(Gruppe, CInt(RowNum), CInt(Durchgang), CBool(Blockheben))
            'End If
        End If

        loading = False

        Grid_Resize(CSng(dgvLeader.Tag) * dgvLeader.Width / dgvLeader.MinimumSize.Width)

    End Sub
    Private Sub frmLeader_SizeChanged(sender As Object, e As EventArgs) Handles Me.SizeChanged
        If Not IsMouseWeeling Then
            Dim X, Y As Single
            Try
                With dgvLeader
                    X = CSng(.Tag) * .Width / .MinimumSize.Width
                    Y = CSng(.Tag) * .Height / .MinimumSize.Height
                End With
                'If WindowState = FormWindowState.Maximized Then
                '    FormBorderStyle = FormBorderStyle.None
                'Else
                '    FormBorderStyle = FormBorderStyle.Sizable
                'End If
                Grid_Resize(X)
            Catch ex As Exception
            End Try
        End If

        With dgvTeam
            .Location = New Point(pnlWertung.Left - .Width - 10, xAufruf.Top)
            '.Visible = Wettkampf.Mannschaft AndAlso .Left > pnlButtons.Left + pnlButtons.Width
            .Visible = Wettkampf.Mannschaft AndAlso .Left > btnMinute2.Left + btnMinute2.Width + 10
            .BringToFront()
        End With
        'ShowIcon = WindowState <> FormWindowState.Maximized
    End Sub

    Private Sub frmLeader_MouseWheel(sender As Object, e As MouseEventArgs) Handles Me.MouseWheel
        If Not dgvCtrlPressed Then Return
        Timer1.Stop()
        Try
            With dgvLeader
                If e.Delta > 0 Then
                    dgvFontSize += 1
                ElseIf dgvFontSize - 1 >= CSng(.Tag) Then
                    dgvFontSize -= 1
                End If
                Timer1.Start()
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Stop()
        Grid_Resize(dgvFontSize)
    End Sub

    Private Sub Grid_Resize(X As Single, Optional Y As Single = 1)
        If loading Then Return
        With dgvLeader
            .DefaultCellStyle.Font = New Font(.Font.FontFamily, X, FontStyle.Regular, GraphicsUnit.Point)
            .ColumnHeadersDefaultCellStyle.Font = New Font(.Font.FontFamily, X, FontStyle.Regular, GraphicsUnit.Point)
            .Columns("Reihenfolge").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, .DefaultCellStyle.Font.Size, FontStyle.Bold, GraphicsUnit.Point)
            .Columns("HLast").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, X, FontStyle.Bold, GraphicsUnit.Point)
            .Columns("Versuch").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, X, FontStyle.Bold, GraphicsUnit.Point)
        End With
        ContextMenuGrid.Font = New Font(ContextMenuGrid.Font.FontFamily, X, FontStyle.Regular, GraphicsUnit.Point)
        With dgv2
            .DefaultCellStyle.Font = New Font(.Font.FontFamily, X, FontStyle.Regular, GraphicsUnit.Point)
            .ColumnHeadersDefaultCellStyle.Font = New Font(.Font.FontFamily, X, FontStyle.Regular, GraphicsUnit.Point)
            .Columns("Reihenfolge").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, .DefaultCellStyle.Font.Size, FontStyle.Bold, GraphicsUnit.Point)
            .Columns("HLast").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, X, FontStyle.Bold, GraphicsUnit.Point)
            .Columns("Versuch").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, X, FontStyle.Bold, GraphicsUnit.Point)
        End With

        dgv2_Move()
        'If WindowState = FormWindowState.Normal Then dgv2_Move()
    End Sub

    Private Function Defaults_Set() As Boolean
        'If InvokeRequired Then
        '    Dim d As New DelegateSub(AddressOf Defaults_Set)
        '    Invoke(d, New Object() {})
        'Else
        Try
            ' pnlButtons (manuelle Wertung)
            If Wettkampf.KR(User.Bohle) = 1 Then
                btnG2.Enabled = False
                btnG3.Enabled = False
                btnU2.Enabled = False
                btnU3.Enabled = False
            End If
            With pnlManuell
                If Wettkampf.Technikwertung Then
                    .Width = .MaximumSize.Width
                    mnuTechniknote.Enabled = True
                Else
                    .Width = MinimumSize.Width
                    mnuTechniknote.Visible = False
                End If
            End With

            Refresh()

            cboDurchgang.Enabled = Wettkampf.Mannschaft AndAlso User.UserId = UserID.Wettkampfleiter
            mnuPrognose.Visible = Wettkampf.Mannschaft

            'chkBH.CheckState = CheckState.Indeterminate
            'btnSteigerung.Enabled = False


            Build_Grid(dgvLeader, "Reißen", Bereich)
            Build_Grid(dgv2, "Stoßen", Bereich)

            Dim x As String
            x = NativeMethods.INI_Read(Bereich, "Konflikte", App_IniFile)
            If x <> "?" Then mnuKonflikte.Checked = CBool(x)
            x = NativeMethods.INI_Read(Bereich, "ShowMessages", App_IniFile)
            If x <> "?" Then mnuMeldungen.Checked = CBool(x)
            'x = NativeMethods.INI_Read(Bereich, "Bestätigung", App_IniFile)
            'If x <> "?" Then mnuConfirm.Checked = CBool(x)
            x = NativeMethods.INI_Read(Bereich, "Versuch", App_IniFile)
            If x <> "?" Then mnuAktuellenVersuch.Checked = CBool(x)
            x = NativeMethods.INI_Read(Bereich, "Sperrzeit", App_IniFile)
            If x <> "?" AndAlso IsNumeric(x) Then ZN_Sperrzeit = CInt(x)

            xTechniknote.ForeColor = Ansicht_Options.Shade_Color
            mnuConfirm.Checked = Wettkampf.International

            'CountDown_Change(0)

            ' Anzeige von Me
            Size = MinimumSize
            'WindowState = formMain.WindowState
            'If WindowState = FormWindowState.Maximized Then
            '    'Debug.WriteLine(dgvLeader.DefaultCellStyle.Font.Size)
            '    With dgvLeader
            '        .DefaultCellStyle.Font = New Font(.Font.FontFamily, 18, FontStyle.Regular, GraphicsUnit.Point)
            '        .Columns("Reihenfolge").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, .DefaultCellStyle.Font.Size, FontStyle.Bold, GraphicsUnit.Point)
            '        .Columns("Versuch").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, .DefaultCellStyle.Font.Size, FontStyle.Bold, GraphicsUnit.Point)
            '        .Columns("HLast").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, .DefaultCellStyle.Font.Size, FontStyle.Bold, GraphicsUnit.Point)
            '    End With
            'End If

            Dim Wertung = CInt(Glob.dtModus.Select("idModus = " & Wettkampf.Modus)(0)("Wertung"))
            Dim NK_Stellen = Get_NK_Stellen(Wertung)
            Dim NK_String = "#,##0." + StrDup(NK_Stellen, "0")

            ' Wertungsanzeige 
            With dgvHeber
                .Columns("T1").Visible = Wettkampf.Technikwertung
                .Columns("T2").Visible = Wettkampf.Technikwertung
                .Columns("T3").Visible = Wettkampf.Technikwertung
                .Columns("Wert").DefaultCellStyle.Format = NK_String
                If Wettkampf.Technikwertung Then .Width = .MaximumSize.Width
                .Rows.Add({"Reißen", DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value})
                .Rows.Add({"Stoßen", DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value})
                .Rows.Add({"Total", DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value})
                '.ClearSelection()
            End With

            ' AGs trennen
            mnuSplit_AG.Visible = Not Wettkampf.Mannschaft AndAlso Wettkampf.Alterseinteilung.Equals("Gewichtsgruppen")
            mnuSplitItem1.Visible = mnuSplit_AG.Visible 'Not Wettkampf.Mannschaft AndAlso Wettkampf.Alterseinteilung.Equals("Gewichtsgruppen")

            ' Team-Wertung
            If Wettkampf.Mannschaft Then
                lblGruppe.Enabled = False
                btnGruppe.Enabled = False
                Label13.Enabled = False
                With dgvTeam
                    .Parent = pnlForm
                    .AutoGenerateColumns = False
                    .Columns("Gegner").Visible = Wettkampf.Finale
                    .Columns("Punkte2").Visible = Wettkampf.Finale
                    .DataSource = bsTeam
                    .Height = .ColumnHeadersHeight + .Rows.Count * .RowTemplate.Height + 4
                    .Columns("mReissen").DefaultCellStyle.Format = NK_String
                    .Columns("mStossen").DefaultCellStyle.Format = NK_String
                    .Columns("mHeben").DefaultCellStyle.Format = NK_String
                    Dim w = 0
                    For Each col As DataGridViewColumn In .Columns
                        If col.Visible Then w += col.Width
                    Next
                    .Width = w
                End With
            End If

            dgvLeader.Focus()

            Cursor = Cursors.Default
        Catch ex As Exception
            Return False
        End Try
        'End If
        Return True
    End Function

    '' Daten-Bereitstellung
    Private Sub Wettkampf_Load(Optional Gruppe As String = "", Optional RowNum As Integer = 0, Optional Durchgang As Integer = 0, Optional Blockheben As Boolean = False)
        Dim Load_WK As New FunctionBoolean(AddressOf Wettkampf_Load)
        If CBool(Invoke(Load_WK, New Object() {})) Then
            Dim Set_Defaults As New FunctionBoolean(AddressOf Defaults_Set)
            If CBool(Invoke(Set_Defaults, New Object() {})) Then
                If User.UserId = UserID.Versuchsermittler Then
                    Leader.RowNum = RowNum
                    Dim IsGroupReady = True
                    If Not Gruppe.Equals("0") Then
                        Dim lstGruppe = Split(Gruppe, ",").ToList
                        Dim Set_Gruppe As New FunctionStringList(AddressOf Change_Gruppe)
                        IsGroupReady = CBool(Invoke(Set_Gruppe, New Object() {lstGruppe}))
                    End If
                    If IsGroupReady Then
                        Dim Set_Durchgang As New DelegateInteger(AddressOf Change_Durchgang)
                        Invoke(Set_Durchgang, New Object() {Durchgang})
                        Dim Set_Blockheben As New DelegateBoolean(AddressOf Change_BH)
                        Invoke(Set_Blockheben, New Object() {Blockheben})
                        Sortierung()
                    Else
                        Using New Centered_MessageBox(Me)
                            MessageBox.Show("Fehler beim Laden der Gruppen", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                    End If
                    'Private tGruppe As Threading.Thread
                    'Private Sub ChangeGruppe_From_Thread(Parameter As Object)
                    '    Dim Gruppe = Split(Parameter.ToString, ",").ToList
                    '    Invoke(New DelegateStringList(AddressOf Change_Gruppe), Gruppe)
                    'End Sub

                    'If Not IsNothing(tGruppe) AndAlso tGruppe.IsAlive Then
                    '    ' Thread läuft noch - Abbruch begonnen
                    'End If
                    'tGruppe = New Threading.Thread(AddressOf ChangeGruppe_From_Thread)
                    'tGruppe.Start(Gruppe)
                End If
            Else
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Fehler beim Laden der Voreinstellungen", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
            End If
        Else
            Close()
            If User.UserId = UserID.Wettkampfleiter Then
                bgwServer.CancelAsync()
            Else
                bgwClient.CancelAsync()
            End If
        End If
    End Sub
    Private Function Wettkampf_Load() As Boolean
        'If InvokeRequired Then
        '    Dim d As New FunctionBoolean(AddressOf Wettkampf_Load)
        '    Invoke(d, New Object() {})
        'Else
        Cursor = Cursors.WaitCursor
        Try
            For i = 0 To 1
                dvLeader(i) = Nothing
                dvDelete(i) = Nothing
            Next
            ' dtResults neu erstellen
            Glob.Refresh_ResultsTable()
        Catch : End Try

        Leader.RowNum = 0
        Leader.RowNum_Offset = 0

        dtGruppen = New DataTable
        Using conn As New MySqlConnection(User.ConnString)
            Do
                Try
                    If Not conn.State = ConnectionState.Open Then conn.Open()
                    ' Tabelle gruppen
                    dtGruppen = New DataTable
                    Dim Query = "select IfNull(WG,Gruppe) WG, Gruppe, Bezeichnung, Bohle, Blockheben " &
                                "from gruppen where " & Wettkampf.WhereClause() & " order by WG;"
                    cmd = New MySqlCommand(Query, conn)
                    dtGruppen.Load(cmd.ExecuteReader())
                    ConnError = False
                Catch ex As MySqlException
                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                End Try
            Loop While ConnError
        End Using
        If ConnError OrElse dtGruppen.Rows.Count = 0 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Gruppen gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            Return False
        End If

        Load_Gruppen()

        Cursor = Cursors.Default
        Return True
    End Function

    Private Sub Load_Gruppen()

        Cursor = Cursors.WaitCursor

        Dim tmpGruppen As New DataTable
        tmpGruppen.Columns.Add(New DataColumn With {.ColumnName = "WG", .DataType = GetType(Integer)})
        tmpGruppen.Columns.Add(New DataColumn With {.ColumnName = "Gruppe", .DataType = GetType(String)})
        tmpGruppen.Columns.Add(New DataColumn With {.ColumnName = "Bezeichnung", .DataType = GetType(String)})
        tmpGruppen.Columns.Add(New DataColumn With {.ColumnName = "Bohle", .DataType = GetType(Integer)})
        tmpGruppen.Columns.Add(New DataColumn With {.ColumnName = "Blockheben", .DataType = GetType(Boolean)})
        tmpGruppen.Columns.Add(New DataColumn With {.ColumnName = "Fertig", .DataType = GetType(Boolean), .DefaultValue = False})

        Dim MultiSelect = True

        Dim Groups = From x In dtGruppen
                     Where x.Field(Of Integer)("Bohle") = User.Bohle
                     Group By WG = x.Field(Of Integer)("WG") Into Group

        For Each grp In Groups
            Dim _grp As New List(Of String)
            Dim _bez As New List(Of String)
            For Each gr In grp.Group
                _grp.Add(gr!Gruppe.ToString)
                _bez.Add(gr!Bezeichnung.ToString)
            Next
            tmpGruppen.Rows.Add({grp.WG, Join(_grp.ToArray, ", "), Join(_bez.ToArray, ", "), grp.Group(0)!Bohle, grp.Group(0)!Blockheben})
            If _grp.Count > 1 Then MultiSelect = False ' wenn Gruppen durch WG zusammengefasst --> kein Multiselect
        Next

        dvGruppen = New DataView(tmpGruppen)
        dvGruppen.Sort = "WG ASC"
        bsGruppen = New BindingSource With {.DataSource = dvGruppen}
        bsGruppen.Filter = "Fertig = False"

        With dgvGruppe
            .AutoGenerateColumns = False
            .DataSource = bsGruppen
            If dvGruppen.Count < 7 Then
                .Height = dvGruppen.Count * .Rows(0).Height + .ColumnHeadersHeight + 2
            Else
                .Height = 6 * .Rows(0).Height + .ColumnHeadersHeight + 2
            End If
            .Height = Get_dgvGruppen_Height()
            .Parent = Me
            .Top = pnlFilter.Top + pnlFilter.Height - 1
            .MultiSelect = MultiSelect
            'If MultiSelect Then
            '    ToolTip1.SetToolTip(dgvGruppe, "Strg = Mehrfachauswahl, Strg+Shift = Bereich auswählen" & vbNewLine & "Enter = Übernehmen, ESC = Abbrechen")
            'Else
            '    ToolTip1.SetToolTip(dgvGruppe, "Enter/Doppelklick = Übernehmen, ESC = Abbrechen")
            'End If
            .Tag = Nothing
        End With

        Cursor = Cursors.Default
    End Sub

    Private Function Get_dgvGruppen_Height() As Integer
        With dgvGruppe
            If dvGruppen.Count < 7 Then
                Return dvGruppen.Count * .Rows(0).Height + .ColumnHeadersHeight + 2
            Else
                Return 6 * .Rows(0).Height + .ColumnHeadersHeight + 2
            End If
        End With
    End Function

    Private Sub Change_Gruppe()
        ' Änderung der automatischen Steigerung in Wettkampf, der Restrictions(Schüler) und von Wiegen verarbeiten
        ' --> Gruppe neu einlesen und DataSources neu setzen

        Try
            If Not IsNothing(bsLeader) Then bsLeader.EndEdit()
            If Not IsNothing(bsDelete) Then bsDelete.EndEdit()
            dgvLeader.DataSource = Nothing
            Glob.Refresh_ResultsTable() ' dtResults neu erstellen

            Load_Gruppen()

            Dim pos = bsGruppen.Find("Gruppe", lblGruppe.Text)
            If pos = -1 AndAlso Not Wettkampf.Mannschaft Then
                lblGruppe.Text = String.Empty
                Leader.Gruppe.Clear()
            ElseIf Not (IsNothing(bsLeader) AndAlso IsNothing(bsDelete)) Then
                If Not Wettkampf.Mannschaft Then bsResults.Filter = "attend = True And (Gruppe = " & String.Join(" Or Gruppe = ", Leader.Gruppe) & ")"
                If Not Gruppe_Load() Then Return
                Gruppe_Show(True)
                Dim HeberChanged = Sortierung()
                If Heber.Id > 0 Then
                    If HeberChanged Then btnNext_Pressed() 'SendtoBohle()
                End If
            End If

            Progress1.Visible = False

            Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
        Catch ex As Exception
        End Try
    End Sub
    Private Function Gruppe_Load() As Boolean

        Cursor = Cursors.WaitCursor
        loading = True
        Dim _table() As String = {"Reissen", "Stossen"}
        'Dim lstHeber As New List(Of Integer)
        Dim Query = String.Empty

        ' Restrictions erstellen für Schülerpokal
        Dim dvRest As DataView = New DataView(Glob.dtRestrict)
        dvRest.Sort = "Altersklasse"

        '' dtResults neu erstellen
        'Glob.Refresh_ResultsTable()

        Using conn As New MySqlConnection(User.ConnString)
            If Not conn.State = ConnectionState.Open Then conn.Open()

            Dim tmp As DataTable
            Dim tmpView As DataView = Nothing

#Region "Meldungen auf Vollständigkeit prüfen"
            Do
                Dim msg = String.Empty
                Dim s = String.Empty
                ' Teilnehmer der Gruppe
                Query = "Select Wettkampf, MSR, Gruppe, Teilnehmer, Wiegen, Losnummer, Startnummer, Reissen, Stossen, attend " &
                        "FROM meldung " &
                        "left join athleten on idTeilnehmer = Teilnehmer " &
                        "WHERE " & Wettkampf.WhereClause() & " And " &
                        If(Not Wettkampf.Mannschaft AndAlso Leader.Gruppe.Count > 0, "(Gruppe = " & String.Join(" Or Gruppe = ", Leader.Gruppe) & ") And ", "") &
                        "(attend = True Or Isnull(attend));"
                cmd = New MySqlCommand(Query, conn)
                tmp = New DataTable
                Try
                    tmp.Load(cmd.ExecuteReader())
                    ConnError = False
                Catch ex As MySqlException
                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                End Try

                ' prüfen ob Heber.attend = NULL vorhanden sind
                tmpView = New DataView(tmp, "Convert(IsNull(attend,''), System.String) = ''", Nothing, DataViewRowState.CurrentRows)
                If tmpView.Count > 0 Then
                    ' Gruppen mit Heber.attend = NULL selektieren
                    Dim gr = From x In tmpView.ToTable
                             Group By Gruppe = x.Field(Of Integer?)("Gruppe") Into Group
                    ' String mit Gruppen-Nummern zusammensetzen
                    If IsNothing(gr(0).Gruppe) Then
                        s = "en"
                    Else
                        msg = " in Gruppe "
                        For i = 0 To gr.Count - 1
                            msg += gr(i).Gruppe.ToString
                            If i < gr.Count - 2 Then
                                msg += ", "
                            ElseIf i < gr.Count - 1 Then
                                msg += " und "
                            End If
                        Next
                        s = If(gr.Count > 1, "en", "")
                    End If
                    ' IsNull(Heber) bedeutet, Heber ist nicht ausgeschlossen, aber Meldung ist nicht erfolgt oder unvollständig
                    msg = "Unvollständige Meldung" & s & " bzw." & vbNewLine & "fehlende Absage-Bestätigung" & s & msg & "."
                    msg += vbNewLine + vbNewLine + "Wiegen zur Berichtigung der Meldung öffnen?"
                    Using New Centered_MessageBox(Me)
                        If MessageBox.Show(msg, msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then
                            Using formWiegen As New frmWiegen
                                With formWiegen
                                    .Text = "WIEGEN - " & Wettkampf.Bezeichnung
                                    .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
                                    .CurrentGroup = CInt(Leader.Gruppe(0))
                                    .ShowDialog(Me)
                                    If .CurrentGroup = -1 Then
                                        ' Gruppe in Wiegen geändert --> dvResults aktualisieren
                                        Glob.Refresh_ResultsTable()
                                        If Not Wettkampf.Mannschaft Then bsResults.Filter = "attend = True AND (Gruppe = " & String.Join(" OR Gruppe = ", Leader.Gruppe) & ")"
                                    End If
                                End With
                            End Using
                        Else
                            Cursor = Cursors.Default
                            Return False
                        End If
                    End Using
                End If
            Loop While tmpView.Count > 0
#End Region

            If ConnError Then Return False

            tmpView = New DataView(tmp, "attend = True", Nothing, DataViewRowState.CurrentRows)

            statusVersuch.Visible = False
            statusResultat.Visible = False
            statusSteigerung.Visible = False

            Progress1.Maximum = tmp.Rows.Count * 2 + 2
            Progress1.Value = 0
            Progress1.Visible = True

#Region "Tabellen Reissen, Stossen, Results anlegen"
            For i As Integer = 0 To 1
                ' Reißen / Stoßen
                Progress1.Value += 1

                For Each row As DataRowView In tmpView
                    Dim HLast = 0
                    If Not IsDBNull(row(_table(i))) Then HLast = CInt(row(_table(i)))
                    Try
                        cmd = New MySqlCommand("", conn)
                        With cmd.Parameters
                            .AddWithValue("@wk", row!Wettkampf)
                            .AddWithValue("@tn", row!Teilnehmer)
                            .AddWithValue("@h1", HLast)
                            .AddWithValue("@h2", HLast + Wettkampf.Steigerung(0))
                            .AddWithValue("@h3", HLast + Wettkampf.Steigerung(0) + Wettkampf.Steigerung(1))
                            .AddWithValue("@d1", 0)
                            .AddWithValue("@d2", Wettkampf.Steigerung(0))
                            .AddWithValue("@d3", Wettkampf.Steigerung(1))
                            .AddWithValue("@s1", HLast)
                            .AddWithValue("@s2", 0)
                            .AddWithValue("@s3", 0)
                        End With

                        Dim Query_SELECT = "SELECT Teilnehmer, Versuch, HLast, Diff, Steigerung1, Steigerung2, Steigerung3, Wertung, Zeit, Note " &
                                           "FROM " & LCase(_table(i)) &
                                           " WHERE Wettkampf = @wk AND Teilnehmer = @tn " &
                                           "ORDER BY Teilnehmer, Versuch;"

                        Dim Query_INSERT = "INSERT INTO " & LCase(_table(i)) & " (Wettkampf, Teilnehmer, Versuch, HLast, Diff, Steigerung1) " &
                                           "VALUES (@wk, @tn, 1, @h1, @d1, @s1), (@wk, @tn, 2, @h2, @d2, @s2), (@wk, @tn, 3, @h3, @d3, @s3);"

                        ' prüfen, ob für TN bereits Versuche gespeichert sind
                        cmd.CommandText = Query_SELECT
                        Dim attempts = New DataTable
                        attempts.Load(cmd.ExecuteReader)
                        If attempts.Rows.Count > 0 Then
                            For Each r As DataRow In attempts.Rows
                                If Not IsDBNull(r!Wertung) Then
                                    ' gewerteter Versuch --> keine Änderung am Durchgang des Hebers
                                    Exit For
                                Else
                                    Dim v = CInt(r!Versuch)
                                    Dim Query_UPDATE = String.Empty
                                    If CInt(r!Steigerung2) = 0 AndAlso CInt(r!Steigerung3) = 0 Then
                                        If CInt(r!Versuch) > 1 AndAlso CInt(r!Steigerung1) = 0 Then
                                            Query_UPDATE = "UPDATE " & LCase(_table(i)) & " Set HLast = @h" & v & ", Diff = @d" & v & " WHERE Wettkampf = @wk And Teilnehmer = @tn And Versuch = " & v & ";"
                                        Else
                                            Query_UPDATE = "UPDATE " & LCase(_table(i)) & " Set HLast = @h" & v & ", Diff = @d" & v & ", Steigerung1 = @s" & v &
                                                           " WHERE Wettkampf = @wk And Teilnehmer = @tn And Versuch = " & v & ";"
                                        End If
                                    End If
                                    cmd.CommandText = Query_UPDATE
                                    Do
                                        Try
                                            cmd.ExecuteNonQuery()
                                            ConnError = False
                                        Catch ex As MySqlException
                                            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                                        End Try
                                    Loop While ConnError
                                End If
                            Next
                        Else
                            ' Anfangslasten in weightlift.reissen/stossen speichern (Steigerung ist die jeweilige HLast + Steigerung(Versuch))
                            cmd.CommandText = Query_INSERT
                            Do
                                Try
                                    cmd.ExecuteNonQuery()
                                    ConnError = False
                                Catch ex As MySqlException
                                    If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                                End Try
                            Loop While ConnError
                        End If

                        If ConnError Then Return False

                        ' Datensatz in weightlift.results anlegen, falls nicht vorhanden
                        cmd.CommandText = "INSERT IGNORE INTO results (Wettkampf, Teilnehmer) VALUES (@wk, @tn);"
                        Try
                            cmd.ExecuteNonQuery()
                            ConnError = False
                        Catch ex As Exception
                            MySQl_Error(Me, ex.Message, ex.StackTrace)
                        End Try

                        Progress1.Value += 1
                    Catch ex As Exception
                    End Try
                Next
#End Region

#Region "Tabelle der Heber für Leader --> enthält Wertung, Wertung2 und Wertung.Gruppierung für die Platzierung"
                Query = "
                    SELECT m.Teilnehmer, wt.Id TeamID, " & If(Wettkampf.Mannschaft, "IfNull(m.Gruppe, 1000)", "m.Gruppe") & " Gruppe, m.GG, a.Geschlecht sex, a.Nachname, a.Vorname, UCase(a.Nachname) Name1, concat(UCase(a.Nachname), ' ', a.Vorname) Name2, v.idVerein, 
	                    IfNull(v.Kurzname, v.Vereinsname) Vereinsname, m.Losnummer, m.Startnummer, m.Wiegen, m.Reissen, m.idAK, m.AK, m.idGK, m.GK, m.a_K, a.Jahrgang, 
                        IfNull(r.Kurz, r.region_3) Verband, r.region_flag, s.state_iso3, s.state_name, s.state_flag, 
                        rs.Versuch, rs.HLast, rs.Diff, rs.Wertung, rs.Zeit, rs.Note, rs.Lampen, rs.Steigerung1, rs.Steigerung2, rs.Steigerung3, 
	                    IfNull((Select Wertung FROM wettkampf_wertung WHERE Wettkampf = m.Wettkampf And idAK = m.idAK Limit 1), 
		                       (select Wertung from wettkampf left join modus on idModus = Modus where Wettkampf = m.Wettkampf Limit 1)) Auswertung, 
                        IfNull((Select Wertung2 FROM wettkampf_wertung WHERE Wettkampf = m.Wettkampf And idAK = m.idAK Limit 1), 
                                IfNull((nullif(substr(w.Platzierung, 3, 1), '')), 0)) Auswertung2, 
                        IfNull((Select Gruppierung FROM wettkampf_wertung WHERE Wettkampf = m.Wettkampf And idAK = m.idAK Limit 1), 0) Gruppierung2
                    FROM " & LCase(_table(i)) & " rs 
                    LEFT JOIN meldung m On m.Wettkampf = rs.Wettkampf And m.Teilnehmer = rs.Teilnehmer 
                    LEFT JOIN athleten a On a.idTeilnehmer = m.Teilnehmer " &
                    If(Wettkampf.Mannschaft, "LEFT JOIN teams t On t.idVerein = IfNull(a.MSR, a.Verein) " &
                                             "LEFT JOIN verein v On v.idVerein = IfNull(t.idTeam, IfNull(a.MSR, a.Verein)) ",
                                             "LEFT JOIN verein v On v.idVerein = IfNull(a.ESR, a.Verein) ") & "
                    LEFT JOIN staaten s On s.state_id = a.Staat 
                    LEFT JOIN region r On r.region_id = v.Region 
                    LEFT JOIN wettkampf_teams wt On wt.Wettkampf = m.Wettkampf And wt.Team = a.MSR 
                    LEFT JOIN wettkampf w On w.Wettkampf = m.Wettkampf 
                    WHERE m.attend = True And " & Wettkampf.WhereClause("m") & If(Not Wettkampf.Mannschaft AndAlso Leader.Gruppe.Count > 0, " And (m.Gruppe = " & String.Join(" Or m.Gruppe = ", Leader.Gruppe) & ") ", " ") & "
                    GROUP BY Teilnehmer, Versuch 
                    ORDER BY m.idGK, a.Nachname, a.Vorname;"
                cmd = New MySqlCommand(Query, conn)
                dtLeader(i) = New DataTable
                dtLeader(i).Load(cmd.ExecuteReader)
            Next
#End Region
        End Using

#Region "DataViews für Reißen und Stoßen erstellen"
        For i = 0 To 1
            ' dtLeader: Spalte Reihenfolge anhängen
            dtLeader(i).Columns.Add(New DataColumn With {.ColumnName = "Reihenfolge", .DataType = GetType(Integer)})

            ' Tabelle dtDelete erstellen und füllen
            Dim dv As DataView = New DataView(dtLeader(i))
            dv.RowFilter = "(Convert(IsNull(Wertung,''), System.String) <> '' Or Wertung <> 0)"
            dtDelete(i) = New DataTable
            dtDelete(i) = dv.ToTable
            If Leader.RowNum = 1 Then
                Dim r = dtDelete(i).Select("Teilnehmer = " & Heber.Id.ToString & " And Versuch = " & Heber.Versuch.ToString)
                If r.Count > 0 Then dtDelete(i).Rows.Remove(r(0))
            End If
            dtDelete(i).AcceptChanges()
            dvDelete(i) = New DataView(dtDelete(i))

            ' Tabelle dtLeader gelaufene Versuche löschen
            For Each r As DataRow In dtDelete(i).Rows
                Dim d = dtLeader(i).Select("Teilnehmer = " & r!Teilnehmer.ToString & " And Versuch = " & r!Versuch.ToString)
                If d.Count > 0 Then dtLeader(i).Rows.Remove(d(0))
            Next

            dtLeader(i).AcceptChanges()
            dvLeader(i) = New DataView(dtLeader(i)) With {.RowFilter = "Gruppe < 1000"}
            dvNotGrouped(i) = New DataView(dtLeader(i)) With {.RowFilter = "Gruppe = 1000", .Sort = "Teilnehmer"}
        Next
#End Region

#Region "Listen Reißen/Stoßen bereinigen --> Restrictions im Schülerpokal"
        Using conn As New MySqlConnection(User.ConnString)
            Dim Restrictions() As DataRow = Nothing
            For Each row As DataRowView In dvRest
                ' alle Jahrgänge >= maxJG
                Dim maxJG = Year(Wettkampf.Datum) - CInt(row!Altersklasse)

                Query = "UPDATE " & _table(CInt(row!Durchgang)) &
                        " Set Wertung = @wertung, Zeit = @zeit, HLast = @last , Diff = @diff, Note = @note, Lampen = @lampe " &
                        "WHERE " & Wettkampf.WhereClause & " And Teilnehmer = @heber And Versuch = @versuch;"

                ' alle Versuche im jeweiligen Durchgang
                Restrictions = dtLeader(CInt(row!Durchgang)).Select("Jahrgang >= " & maxJG.ToString & " AND Versuch > " & row!Versuche.ToString)

                For Each r As DataRow In Restrictions
                    r!HLast = 0
                    r!Diff = 0
                    r!Steigerung1 = 0
                    r!Steigerung2 = 0
                    r!Steigerung3 = 0
                    r!Wertung = -2
                    ' Wertung in Results eintragen
                    Dim pos = bsResults.Find("Teilnehmer", r!Teilnehmer)
                    Dim col = Strings.Left(_table(CInt(row!Durchgang)), 1) & "_" + r!Versuch.ToString
                    dvResults(pos)(col.Insert(1, "W")) = -2
                    If CInt(row!Versuche) = 0 Then dvResults(pos)(_table(CInt(row!Durchgang))) = 0
                    bsResults.EndEdit()

                    ' restriktierten Versuch als Verzicht speichern 
                    If User.UserId = UserID.Wettkampfleiter Then
                        cmd = New MySqlCommand(Query, conn)
                        With cmd.Parameters
                            '.AddWithValue("@wk", Wettkampf.ID)
                            .AddWithValue("@heber", r!Teilnehmer)
                            .AddWithValue("@versuch", r!Versuch)
                            .AddWithValue("@last", r!HLast)
                            .AddWithValue("@diff", r!Diff)
                            .AddWithValue("@wertung", r!Wertung)
                            .AddWithValue("@zeit", r!Zeit)
                            .AddWithValue("@note", If(Not IsDBNull(r!Note) AndAlso CDbl(r!Note) > 0, r!Note, DBNull.Value))
                            .AddWithValue("@lampe", r!Lampen)
                        End With
                        Do
                            Try
                                If conn.State = ConnectionState.Closed Then
                                    conn.Open()
                                End If
                                cmd.ExecuteNonQuery()
                                ConnError = False
                            Catch ex As MySqlException
                                If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit For
                            End Try
                        Loop While ConnError
                    End If

                    'Save_Versuch({r}.CopyToDataTable, _table(CInt(row!Durchgang)))

                    ' Zeile in Delete verschieben
                    dtDelete(CInt(row!Durchgang)).ImportRow(r)
                    dtLeader(CInt(row!Durchgang)).Rows.Remove(r)
                    dtDelete(CInt(row!Durchgang)).AcceptChanges()
                    dtLeader(CInt(row!Durchgang)).AcceptChanges()
                Next
            Next
        End Using
#End Region

        If Not ConnError Then
            dtLeader(0).AcceptChanges()
            dtLeader(1).AcceptChanges()
            dtResults.AcceptChanges()
        End If

#Region "HLast für nächsten Versuch in Results eintragen"
        '       wenn keine Steigerung in dvLeader eingetragen ist, dann keine HLast in dvResults speichern (außer mnuBestätigungNeeded:=unchecked)
        '       --> wird dann in formPublikum.Set_AnfangsLast als unbestätigt eingetragen
        Dim _tables() As String = {"R_", "S_"}
        Try
            For i = 0 To 1
                Dim lifter = From v In dtLeader(i)
                             Where v.Field(Of Integer)("Steigerung1") > 0 OrElse v.Field(Of Integer)("Steigerung2") > 0 OrElse v.Field(Of Integer)("Steigerung3") > 0
                             Group By TN = v.Field(Of Integer)("Teilnehmer") Into Group
                             Select New With {Key TN,
                                       .Versuch = Group.Max(Function(r) r.Field(Of Integer)("Versuch")),
                                       .Steigerung1 = Group.Max(Function(r) r.Field(Of Integer)("Steigerung1")),
                                       .Steigerung2 = Group.Max(Function(r) r.Field(Of Integer)("Steigerung2")),
                                       .Steigerung3 = Group.Max(Function(r) r.Field(Of Integer)("Steigerung3"))}
                For c = 0 To lifter.Count - 1
                    Dim HLast As Integer = 0
                    If lifter(c).Steigerung1 > 0 Then HLast = lifter(c).Steigerung1
                    If lifter(c).Steigerung2 > 0 Then HLast = lifter(c).Steigerung2
                    If lifter(c).Steigerung3 > 0 Then HLast = lifter(c).Steigerung3
                    Dim row = bsResults.Find("Teilnehmer", lifter(c).TN)
                    Dim col = _tables(i) + lifter(c).Versuch.ToString
                    dvResults(row)(col) = HLast
                Next
            Next
        Catch ex As Exception
            LogMessage("frmLeader.Gruppe_Load: HLast eintragen: " & ex.Message, ex.StackTrace)
        End Try
        bsResults.EndEdit()
#End Region

        'loading = False
        'mnuPrognose.Enabled = Wettkampf.Mannschaft
        Cursor = Cursors.Default
        Return True
    End Function
    Private Sub Gruppe_Show(Optional Settings_Changed As Boolean = False)
        Cursor = Cursors.WaitCursor
        loading = True
        ' DataSource Leader 
        bsLeader = New BindingSource With {.DataSource = dvLeader(Leader.TableIx)}
        bsDelete = New BindingSource With {.DataSource = dvDelete(Leader.TableIx)}

        If Not chkBH.CheckState = CheckState.Indeterminate Then
            bsLeader.Sort = SortOrder
        End If

        ' Gruppe in allen Anzeigen refreshen
        If Not IsNothing(Leader.Gruppe) OrElse Wettkampf.Mannschaft Then
            Leader.Change_Gruppe()
        End If

        With dgvLeader
            .Columns("HLast").HeaderText = Leader.Table ' Spalten Reißen/Stoßen festlegen
            .DataSource = Nothing
            .DataSource = bsLeader
            If .Rows.Count = 0 Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Keine Heber/innen im " & Leader.Table & ".", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
            ElseIf Leader.TableIx = 0 Then
                ' 2. DGV
                dgv2.DataSource = dvLeader(1)
                dgv2_Move()
            ElseIf Leader.TableIx = 1 Then
                ' 2. DGV
                dgv2.DataSource = Nothing
                dgv2.Visible = False
            End If
        End With

        loading = False

        Cursor = Cursors.Default

        If Settings_Changed Then Return

        WK_Reset(False)

        ' Buttons freischalten
        If cboDurchgang.SelectedIndex > -1 Then
            Dim enableWL = User.UserId = UserID.Wettkampfleiter AndAlso dgvLeader.Rows.Count > 0
            btnNext.Enabled = enableWL
            btnKorrektur.Enabled = enableWL

            btnSteigerung.Enabled = dgvLeader.Rows.Count > 0 AndAlso Not VE_verbunden
            btnVerzicht.Enabled = dgvLeader.Rows.Count > 0 AndAlso Not VE_verbunden

            btnBestätigung.Enabled = False
        End If

        ' zuvor geöffnete Anzeigen, die bei Load geschlossen wurden, wieder öffnen
        Try
            If DisplayedForms.Count > 0 Then
                Using SF As New ShowForm
                    For Each Item In DisplayedForms
                        SF.Create_Form(Item, True)
                    Next
                End Using
            End If
        Catch ex As Exception
            LogMessage("frmLeader.Gruppe_Show().DisplayedForms: " & ex.Message, ex.StackTrace)
        End Try

        Cursor = Cursors.Default
    End Sub
    Private Sub Durchgang_Show()
        Cursor = Cursors.WaitCursor
        Try
            If dvLeader(Leader.TableIx).Count - Leader.RowNum = 0 Then
                ' alle Versuche des Durchgangs absolviert
                OrderTable()
            ElseIf dtDelete(Leader.TableIx).Rows.Count > 0 AndAlso Leader.Durchgang = Leader.Table Then
                ' nicht alle Versuche des Durchgangs absolviert
                btnNext.Text = "Nächster Heber"
                'btnSteigerung.Enabled = True
                'btnGruppe.Enabled = False
                'cboDurchgang.Enabled = False
                'chkBH.Enabled = False
                'cboBohle.Enabled = False
                If Wettkampf.Mannschaft Then
                    bsResults.EndEdit()
                    Using RS As New Results
                        RS.TeamWertung(dvResults.ToTable)
                    End Using
                    bsTeam.ResetBindings(False)
                    If User.UserId = UserID.Wettkampfleiter Then
                        Using save As New mySave
                            save.Save_Punkte(Me) ' speichern, falls die Teams in Wettkampf/Bearbeiten geändert wurden
                        End Using
                    End If
                End If
            Else
                ' noch kein Versuch absolviert
                btnNext.Text = "Nächster Heber"
                Leader.RowNum = 0
                Sortierung()
                Heber.Id = 0
            End If

            If bsLeader.Count > 0 Then
                bsLeader.Position = 0
                dgvLeader.FirstDisplayedScrollingRowIndex = 0
                ' normales Heben/Blockheben aus Gruppe auslesen --> bei Mehrfachauswahl wird nur die erste Gruppe berücksichtigt
                Dim pos = -1
                If Not IsNothing(dgvGruppe.Tag) Then pos = bsGruppen.Find("WG", CType(dgvGruppe.Tag, List(Of Integer))(0))
                'If Wettkampf.Mannschaft OrElse chkBH.CheckState = CheckState.Checked OrElse (pos = -1 AndAlso CBool(dvGruppen(pos)!Blockheben)) Then
                If Wettkampf.Mannschaft OrElse chkBH.CheckState = CheckState.Checked OrElse CBool(dvGruppen(pos)!Blockheben) Then
                    chkBH.CheckState = CheckState.Indeterminate
                    chkBH.CheckState = CheckState.Checked
                Else
                    chkBH.CheckState = CheckState.Indeterminate
                    chkBH.CheckState = CheckState.Unchecked
                End If
            End If

            loading = False
            Progress1.Visible = False

            If dvLeader(Leader.TableIx).Count > Leader.RowNum Then
                Dim row = dvLeader(Leader.TableIx)(Leader.RowNum).Row
                Hantel.Change_Hantel(CInt(row!HLast),, row!sex.ToString, CInt(row!Jahrgang), Wettkampf.BigDisc, False)
            End If

            ' Leader sendet an alle Clients --> dazu müsste Versuchsermittler.frmLeader geöffnet sein
            'If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then
            '    Client_Send(String.Concat("Q;", Wettkampf.ID, ";",
            '                              If(Not IsNothing(Leader.Gruppe), Join(Leader.Gruppe.ToArray, ","), "0"), ";",
            '                              Leader.TableIx, ";",
            '                              Leader.Blockheben, ";",
            '                              Heber.Id, ";",
            '                              Heber.Versuch, ";",
            '                              Leader.RowNum))
            'End If

        Catch ex As Exception
        End Try

        'dgvLeader_RowEnter(bsLeader, Nothing)

        bsLeader_PositionChanged(0)

        statusVersuch.Visible = True
        statusResultat.Visible = True
        statusSteigerung.Visible = True

        Cursor = Cursors.Default
    End Sub

    '' Filter Tabelle
    Private Function Change_Gruppe(Gruppe As List(Of String)) As Boolean
        ' holt GruppenListe aus Get_Gruppen
        ' 
        ' ändert die Gruppe     wenn Leader als Client läuft --> von Server ausgelöst
        '                       wenn dgvGruppe geändert wird

        'If InvokeRequired Then
        '    Dim d As New DelegateStringList(AddressOf Change_Gruppe)
        '    Invoke(d, New Object() {Gruppe})
        'Else
        ' Filter für Results aus neu eingelesener Gruppe (= DataSource for Publikum.dgvJoin)

        Try
            Try
                If Not Wettkampf.Mannschaft Then
                    If Gruppe.Count > 0 Then
                        bsResults.Filter = "attend = True AND (Gruppe = " & String.Join(" OR Gruppe = ", Gruppe) & ")"
                    Else
                        bsResults.Filter = "Gruppe = -1"
                    End If
                End If
            Catch ex As Exception
                bsResults.Filter = "Gruppe = -1"
            End Try

            If IsNothing(Gruppe) OrElse Gruppe.Count = 0 Then
                Leader.Gruppe.Clear()
                lblGruppe.Text = String.Empty
            Else
                Leader.Gruppe = Gruppe ' hier wird frmPublikum.dgvJoin neu eingelesen
                lblGruppe.Text = Join(Gruppe.ToArray, ", ")
            End If
            Refresh()

            ' Tabellen für Gruppe erstellen und anzeigen
            If Not Wettkampf.Mannschaft AndAlso Leader.Gruppe.Count > 0 AndAlso cboDurchgang.SelectedIndex > -1 Then
                ' Durchgang bereits ausgewählt => neue Tabellen für Gruppe erstellen
                If Not Gruppe_Load() Then Return False
                ' wenn Tabellen für Gruppe erfolgreich erstellt --> anzeigen
                If Leader.Gruppe.Count > 0 Then
                    Gruppe_Show()
                    Durchgang_Show()
                    If btnNext.Text.Contains("Gruppe") Then OrderTable()
                End If
            End If

            Leader.Change_Gruppe()

            If Wettkampf.Mannschaft OrElse Leader.Gruppe.Count > 0 AndAlso cboDurchgang.SelectedIndex > -1 Then
                If Not String.IsNullOrEmpty(SortInsertion) Then
                    SortInsertion = String.Empty
                    Sortierung()
                End If
            End If

            If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then Client_Send("G=" + Join(Leader.Gruppe.ToArray, ","))

        Catch ex As Exception
            Return False
        End Try
        Return True
        '       End If

    End Function
    Private Sub cboDurchgang_SelectdIndexChanged(sender As Object, e As EventArgs) Handles cboDurchgang.SelectedIndexChanged
        If cboDurchgang.SelectedIndex = -1 Then Return
        Cursor = Cursors.WaitCursor
        Try
            ' Durchgang festlegen
            Leader.Table = Trim(cboDurchgang.Text)
            Leader.TableIx = cboDurchgang.SelectedIndex

            If Wettkampf.Mannschaft OrElse Leader.Gruppe.Count > 0 Then
                If IsNothing(dvLeader(Leader.TableIx)) OrElse dvLeader(Leader.TableIx).Count = 0 Then
                    ' Tabellen für Gruppe erstellen
                    If Not Gruppe_Load() Then Return
                End If
                Gruppe_Show()
            End If
            ' Tabellen anzeigen
            Durchgang_Show()

            If Not String.IsNullOrEmpty(Leader.Table) Then Leader.Durchgang = Leader.Table

            If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then
                Client_Send(String.Join(";", {"DG=" & cboDurchgang.SelectedIndex,
                                              "WK=" & Wettkampf.ID,
                                              "GP=" & lblGruppe.Text,
                                              "RN=" & Leader.RowNum,
                                              "BH=" & Leader.Blockheben}))
            End If

        Catch ex As Exception
            Dim msg = "Fehler beim Laden des Wettkampfes"
            'If Wettkampf.Identities.Count > 0 Then
            '    msg += vbNewLine & "Meldung aus anderem WK importiert?"
            'End If
            Using New Centered_MessageBox(Me)
                MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            LogMessage("frmLeader.cboDurchgang_SelectdIndexChanged: " & ex.Message, ex.StackTrace)
        End Try
        Cursor = Cursors.Default
    End Sub
    Private Sub Change_Durchgang(Durchgang As Integer)
        'If InvokeRequired Then
        '    Dim d As New DelegateInteger(AddressOf Change_Durchgang)
        '    Invoke(d, New Object() {Durchgang})
        'Else
        cboDurchgang.SelectedIndex = Durchgang
        'End If
    End Sub
    Private Sub chkBH_CheckedChanged(sender As Object, e As EventArgs) Handles chkBH.CheckStateChanged
        Get_Sortierung()
    End Sub
    Private Sub Get_Sortierung()
        Try
            If chkBH.CheckState = CheckState.Unchecked Then
                'Normal
                Dim s = 0
                If Wettkampf.OrderByLifter Then
                    ' prüfen, ob Gruppe restriktiert --> Feld in 
                    Dim r = Glob.dtRestrict.Select("Durchgang = " & Leader.TableIx)
                    If r.Count > 0 Then s = (From x In dvResults.ToTable
                                             Where Year(Wettkampf.Datum) - CInt(r(0)!Altersklasse) <= x.Field(Of Integer)("Jahrgang")).Count
                End If
                If s > 0 AndAlso s = dvResults.Count Then
                    SortOrder = "Startnummer ASC, Losnummer ASC, Versuch ASC"
                ElseIf Not Wettkampf.Mannschaft Then
                    SortOrder = "HLast ASC, Versuch ASC, Diff DESC, Zeit ASC, Startnummer ASC, Losnummer ASC, Wiegen ASC"
                Else
                    SortOrder = "Gruppe ASC, HLast ASC, Versuch ASC, Diff DESC, Zeit ASC, Startnummer ASC, Losnummer ASC, Wiegen ASC"
                End If
            ElseIf chkBH.CheckState = CheckState.Checked Then
                'Blockheben
                If Not Wettkampf.Mannschaft Then
                    SortOrder = "Versuch ASC, HLast ASC, Diff DESC, Zeit ASC, Startnummer ASC, Losnummer ASC, Wiegen ASC"
                Else
                    SortOrder = "Gruppe ASC, Versuch ASC, HLast ASC, Diff DESC, Zeit ASC, Startnummer ASC, Losnummer ASC, Wiegen ASC"
                End If
            Else
                Return
            End If
            Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
            Leader.Blockheben = chkBH.Checked
            If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted AndAlso VE_verbunden Then Client_Send("B=" + chkBH.Checked.ToString)
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Change_BH(Value As Boolean)
        'If InvokeRequired Then
        '    Dim d As New DelegateBoolean(AddressOf Change_BH)
        '    Invoke(d, New Object() {Value})
        'Else
        chkBH.Checked = Value
        'chkBH.Enabled = User.UserId < UserID.Versuchsermittler
        'End If
    End Sub
    Private Sub btnAufruf_Enabled_Changed(SetEnabled As Boolean)
        If InvokeRequired Then
            Dim d As New DelegateBoolean(AddressOf btnAufruf_Enabled_Changed)
            Invoke(d, New Object() {SetEnabled})
        Else
            Try
                btnAufruf.Enabled = SetEnabled AndAlso Not btnNext.Enabled AndAlso Not IsNothing(dvLeader(Leader.TableIx)) AndAlso dvLeader(Leader.TableIx).Count >= Leader.RowNum 'AndAlso Heber.Id > 0
            Catch ex As Exception
                btnAufruf.Enabled = False
            End Try
        End If
    End Sub

    '' WK-Buttons
    Private Sub btnAufruf_Click(sender As Object, e As EventArgs) Handles btnAufruf.Click
        If ZN_Pad.IsAttemptRated Then Return
        With btnAufruf
            If .Text = "Aufruf starten" Then
                .Text = "Aufruf stoppen"
            ElseIf .Text = "Aufruf stoppen" Then
                .Text = "Aufruf starten"
            End If
            If .Text.Contains("Aufruf") Then
                WA_Events.Push_ZN(vbLf)
            ElseIf .Text.Contains("Pause") Then
                WA_Events.Push_ZN(vbCr)
                .Enabled = False
                .Text = "Aufruf starten"
                btnNext.Enabled = CBool(btnNext.Tag)
            End If
        End With
    End Sub
    Private Sub btnAufruf_EnabledChanged(sender As Object, e As EventArgs) Handles btnAufruf.EnabledChanged
        If User.UserId > UserID.Wettkampfleiter Then
            btnAufruf.Enabled = False
        End If
        btnMinute1.Enabled = btnAufruf.Enabled
        btnMinute2.Enabled = btnAufruf.Enabled
    End Sub
    Private Sub btnBestätigung_Click(sender As Object, e As EventArgs) Handles btnBestätigung.Click, mnuBestätigung.Click

        If Permission(bsLeader.Position) Then
            Dim row As DataRow = dvLeader(Leader.TableIx)(bsLeader.Position).Row
            Write_Bestätigung(CInt(row!Versuch), {row})
        End If

    End Sub
    Private Sub btnBestätigung_EnabledChanged(sender As Object, e As EventArgs) Handles btnBestätigung.EnabledChanged
        mnuBestätigung.Enabled = btnBestätigung.Enabled
    End Sub
    Private Sub btnKorrektur_EnabledChanged(sender As Object, e As EventArgs) Handles btnKorrektur.EnabledChanged
        If User.UserId > UserID.Wettkampfleiter Then btnKorrektur.Enabled = False
    End Sub
    Private Sub btnKorrektur_Click(sender As Object, e As EventArgs) Handles btnKorrektur.Click, mnuKorrektur.Click

        Using formKorrektur As New frmKorrektur
            If IsNothing(dvLeader(0)) Then Return
            With formKorrektur
                If dvLeader(Leader.TableIx).Count > 0 Then
                    .Teilnehmer = CInt(dvLeader(Leader.TableIx)(bsLeader.Position)!Teilnehmer)
                End If
                If Leader.RowNum > 0 Then .ScoredLifter = CInt(dvLeader(Leader.TableIx)(0)!Teilnehmer)
                .CalledFromLeader = True
                .ShowDialog(Me)
                'If .Wiederholen Then ' betrifft nur den aktuell gewerteten Heber
                '    Cursor = Cursors.WaitCursor
                '    Leader.RowNum = 0
                '    Save_Versuch(RemainingRows(.Teilnehmer, CInt(dvLeader(Leader.TableIx)(bsLeader.Position)!Versuch)), Leader.Table)
                '    Sortierung()
                '    WK_Reset()
                '    Cursor = Cursors.Default
                'End If
            End With
        End Using
    End Sub
    Private Sub btnManuell_Click()
        For i = 1 To Wettkampf.KR(User.Bohle)
            If KR_Pad.ContainsKey(i) Then
                If KR_Pad(i).Wertung = 5 Then
                    If i = 1 Then
                        btnG1.Checked = True
                        txtT1.Enabled = True
                        txtT1.Text = KR_Pad(i).Techniknote
                    ElseIf i = 2 Then
                        btnG2.Checked = True
                        txtT2.Enabled = True
                        txtT2.Text = KR_Pad(i).Techniknote
                    ElseIf i = 3 Then
                        btnG3.Checked = True
                        txtT3.Enabled = True
                        txtT3.Text = KR_Pad(i).Techniknote
                    End If
                ElseIf KR_Pad(i).Wertung = 7 Then
                    If i = 1 Then
                        btnU1.Checked = True
                        txtT1.Enabled = False
                    ElseIf i = 2 Then
                        btnU2.Checked = True
                        txtT2.Enabled = False
                    ElseIf i = 3 Then
                        btnU3.Checked = True
                        txtT3.Enabled = False
                    End If
                End If
            End If
        Next
        With pnlManuell
            .Height = .MaximumSize.Height
            .Visible = True
            .BringToFront()
        End With
        btnExitManuell.Visible = True
        lblManuell.Padding = New Padding(10, 0, 27, 0)
        lblManuell.Text = dvLeader(Leader.TableIx)(Leader.RowNum_Offset)("Nachname").ToString + ", " + dvLeader(Leader.TableIx)(Leader.RowNum_Offset)("Vorname").ToString
    End Sub
    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        btnNext_Pressed()
    End Sub
    Private Sub btnNext_Pressed(Optional HeberId As Integer = 0)
        If InvokeRequired Then
            Dim d As New DelegateInteger(AddressOf btnNext_Pressed)
            Invoke(d, New Object() {HeberId})
        Else
            Dim _send As Boolean = True
            btnNext.Tag = Nothing
            statusResultat.Image = ImageList1.Images("btnGray.png")
            statusVersuch.Image = ImageList1.Images("btnGray.png")
            statusSteigerung.Image = ImageList1.Images("btnGray.png")

            Try
                Select Case btnNext.Text
                    Case "Stoßen beginnen"
                        ' letzten Versuch in Delete verschieben
                        If Leader.TableIx = 0 AndAlso Leader.RowNum = 1 Then MoveToDelete()
                        dgv2.Visible = False
                        dgv2.DataSource = Nothing
                        If cboDurchgang.SelectedIndex <> 1 Then cboDurchgang.SelectedIndex = 1
                        btnNext.Enabled = User.UserId = UserID.Wettkampfleiter
                        _send = False
                    Case "WK beenden"
                        formLeader.Close()
                        Return
                    Case "WK beginnen"
                        btnNext.Text = "Nächster Heber"
                    Case "Nächste Gruppe"

                        WK_Reset()

                        dvLeader(0) = Nothing
                        dvLeader(1) = Nothing
                        dvDelete(0) = Nothing
                        dvDelete(1) = Nothing

                        If Not Wettkampf.Mannschaft Then
                            Change_Gruppe(Get_Gruppen(dgvGruppe.SelectedRows))

                            dgvGruppe.Rows(0).Selected = True
                            cboDurchgang.SelectedIndex = 0

                            If Not IsNothing(dvLeader(Leader.TableIx)) Then
                                btnNext.Text = "Nächster Heber"
                            End If
                        End If
                        Return
                End Select
            Catch ex As Exception
                ' bei Mannschaft macht "Gruppe X laden" einen Fehler
            End Try

            Wettkampf.WK_Modus = Modus.Normal
            Leader.Change_InfoMessage(String.Empty) ' Moderator --> spätestens hier Info löschen --> Timer in Moderator löscht Info auch

            '' das ist für Mannschafts-WK --> testen --> funktioniert nicht
            'If btnNext.Text <> "Nächster Heber" Then
            '    If Not btnNext.Text.Contains("Reißen") Then btnNext.Text = "Nächster Heber"
            '    Return
            'End If

            If _send Then
                cboDurchgang.Enabled = False
                btnGruppe.Enabled = False
                'mnuAnzeigeLöschen.Enabled = False
                'chkBH.Enabled = False 'Not _send

                btnAufruf.Enabled = (IsNothing(ZN_Pad) OrElse Not ZN_Pad.Connected) AndAlso dvLeader(Leader.TableIx).Count >= Leader.RowNum
                'pnlManuell.Visible = Leader.ManuelleWertung

                'Select Case WA_Message.Status
                '    Case 7, 15, 23, 31, 39, 47, 55, 63, 70, 163 ' manuelle Wertung
                '        Leader.ManuelleWertung = True
                'End Select

                'btnManuell.Enabled = Not btnNext.Enabled

                btnNext.Enabled = False

                ' NächsterHeber an Client
                If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then Client_Send("N")

                SendtoBohle(HeberId)

                pnlManuell.Visible = Leader.ManuelleWertung

            End If
        End If

    End Sub
    Private Sub btnNext_EnabledChanged(sender As Object, e As EventArgs) Handles btnNext.EnabledChanged

        If User.UserId > UserID.Wettkampfleiter Then btnNext.Enabled = False

        mnuResultsLifter.Enabled = Not btnNext.Enabled AndAlso Not IsNothing(dvLeader(0))

        btnNext.BackColor = If(btnNext.Enabled, Color.FromArgb(0, 240, 0), Color.FromArgb(140, 240, 140))

        'mnuPause.Enabled = IsNothing(dvLeader(0)) OrElse btnNext.Enabled

        'btnPanik.Enabled = Not Panik AndAlso btnNext.Enabled AndAlso Not (dvLeader(Leader.TableIx).Count > 0 AndAlso IsDBNull(dvLeader(Leader.TableIx)(0)("Reihenfolge"))) AndAlso bsLeader.Position > Leader.RowNum
    End Sub
    Private Sub btnNext_Enabled(Value As Boolean)
        If InvokeRequired Then
            Dim d As New DelegateBoolean(AddressOf btnNext_Enabled)
            Invoke(d, New Object() {Value})
        Else
            If Value Then
                btnNext.Enabled = dgvLeader.Rows.Count > Leader.RowNum + Leader.RowNum_Offset  'Not IsNothing(dvLeader(0)) AndAlso Not IsNothing(dvLeader(1))
                If Not IsNothing(btnNext.Tag) Then btnNext.Enabled = CBool(btnNext.Tag)
            Else
                btnNext.Tag = btnNext.Enabled
                btnNext.Enabled = False
            End If
        End If
    End Sub
    Private Sub btnPanik_Click(sender As Object, e As EventArgs) Handles btnPanik.Click, mnuPanik.Click

        Panik = True
        Leader.RowNum_Offset = bsLeader.Position - Leader.RowNum
        Panik_Raised = True

        With dgvLeader
            For i = Leader.RowNum To Leader.RowNum_Offset
                .Rows(i).Cells("Reihenfolge").Value = DBNull.Value
                .Rows(i).DefaultCellStyle.BackColor = Color.Orange
            Next
        End With

        'MarkRows(Leader.RowNum + Leader.RowNum_Offset)
        Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset

        Sortierung()

        btnNext.PerformClick()

        Panik_Raised = False

    End Sub
    Private Sub btnPanik_EnabledChanged(sender As Object, e As EventArgs) Handles btnPanik.EnabledChanged
        mnuPanik.Enabled = btnPanik.Enabled
    End Sub
    Private Sub btnPanik_VisibleChanged(sender As Object, e As EventArgs) Handles btnPanik.VisibleChanged
        mnuPanik.Visible = btnPanik.Visible
    End Sub
    Private Sub btnSteigerung_Click(sender As Object, e As EventArgs) Handles btnSteigerung.Click, mnuSteigerung.Click

        If pnlSteigerung.Visible Then
            pnlSteigerung.Visible = False
            Return
        End If

        If bsLeader.Position < Leader.RowNum Then
            ' gewerteter Versuch kann nicht gesteigert werden
            Beep()
            Return
        End If

        statusSteigerung.Image = ImageList1.Images("btnGray.png")

        Dim Steigern As Boolean

        If Not Panik Then
            Steigern = Permission(bsLeader.Position)
        Else
            Steigern = True
        End If

        If Steigern Then ' Steigerung zeitlich korrekt

            'alle verbleibenden Versuche des Hebers
            Dim row() = dtLeader(Leader.TableIx).Select("Teilnehmer = " + dvLeader(Leader.TableIx)(bsLeader.Position)("Teilnehmer").ToString + " And Convert(IsNull(Wertung,''), System.String) = ''", "Versuch ASC")
            'neues Hantelgewicht
            lblSteigernName.Text = row(0)!Nachname.ToString & ", " & row(0)!Vorname.ToString
            If HantelLast_ExceedsMax(CInt(row(0)!Versuch), CInt(row(0)!HLast)) Then Return
            nudHantellast.Value = CInt(row(0)!HLast) + 1
            pnlSteigerung.Tag = row
            pnlSteigerung.Visible = True
        End If

    End Sub
    Private Sub btnSteigerung_EnabledChanged(sender As Object, e As EventArgs) Handles btnSteigerung.EnabledChanged
        mnuSteigerung.Enabled = btnSteigerung.Enabled
    End Sub
    Private Sub btnVerzicht_Click(sender As Object, e As EventArgs) Handles btnVerzicht.Click, mnuVerzicht.Click
        Verzichten()
    End Sub
    Private Sub btnVerzicht_EnabledChanged(sender As Object, e As EventArgs) Handles btnVerzicht.EnabledChanged
        mnuVerzicht.Enabled = btnVerzicht.Enabled
    End Sub

    Private Sub Update_From_Korrektur(CorrData As Correction)

        Cursor = Cursors.WaitCursor

        'If TcpIpStarted Then Client_Send(ReturnForClient)
        Dim IsManuellVisible = pnlManuell.Visible
        Dim IsNextEnabled = btnNext.Enabled

        If CorrData.angezeigt Then
            ' Heber wird in Bohle angezeigt und hatte eine Wertung
            If CorrData.gewertet Then
                For i = 1 To 3
                    If i = Heber.Versuch Then
                        For w = 0 To 2
                            Anzeige.Change_Lampe(w + 1, Anzeige.ScoreColors(CInt(CorrData.versuch(i).lampen.Substring(w, 1))))
                            Anzeige.Change_Lampe_Sofort(w + 1, Anzeige.ScoreColors(CInt(CorrData.versuch(i).lampen.Substring(w, 1))))
                        Next
                        If Wettkampf.Technikwertung Then
                            Anzeige.T_Farbe = Anzeige.TechnikColors(CInt(CorrData.versuch(i).wertung))
                            Anzeige.T_Wert = If(CDbl(CorrData.versuch(i).note) = 0, "X,XX", Strings.Format(CDbl(CorrData.versuch(i).note), "Fixed"))
                        End If
                    End If
                Next
                Leader.Change_InfoMessage("Korrektur", InfoTarget.Moderator)
            Else
                Leader.RowNum = 0
                Sortierung()
                btnNext_Pressed()
                Leader.Change_InfoMessage("Wiederholen", InfoTarget.Moderator)
            End If
        ElseIf OrderTable(IsManuellVisible, IsNextEnabled) Then
            If Heber.Id > 0 Then
                Leader.RowNum = 0
                If Sortierung() Then btnNext_Pressed()
            End If
        End If

        If Not Wettkampf.Mannschaft AndAlso CorrData.gruppe > 0 Then
            Dim found = dvGruppen.Table.Select("Gruppe = " & CorrData.gruppe)
            If found.Count > 0 Then
                'If pos > -1 Then
                'Dim W_Grp = CInt(found(0)!Gruppe)
                Dim Groups = (From x In dtGruppen
                              Where x.Field(Of Integer)("WG") = CInt(found(0)!WG)
                              Select x.Field(Of Object)("Gruppe")).ToList()
                'check if group is finished
                Dim RatedLifters = dtResults.Select("Gruppe IN (" & Join(Groups.ToArray, ",") & ") AND attend = True AND Convert(IsNull(RW_3, ''), System.String) <> '' AND Convert(IsNull(SW_3, ''), System.String) <> ''")
                Dim WG_Lifters = dtResults.Select("Gruppe IN (" & Join(Groups.ToArray, ",") & ") AND attend = True")

                found(0)!Fertig = RatedLifters.Count = WG_Lifters.Count

                dvGruppen.Table.AcceptChanges()
                bsGruppen.EndEdit()
                dgvGruppe.Height = Get_dgvGruppen_Height()
            End If
        End If

        Cursor = Cursors.Default
    End Sub

    Private Function Verzichten(Optional UseFirstAttempt As Boolean = False,
                                Optional NoEnquiry As Boolean = False,
                                Optional ExchangeLifter As Integer = 0,
                                Optional Course As Integer = -1) As Boolean  'sender As Object, e As VerzichtEventArgs) As Boolean

        ' UseFirstAttempt As Boolean  ' immer ab dem ersten Versuch verzichten (nur für HeberAustausch in Bundesliga)
        ' NoEnquiry As Boolean        ' keine Nachfrage, ob Verzicht durchgeführt werden soll (bei HeberAustausch in Bundesliga ... und sonst?)

        Dim IsExchange = ExchangeLifter > 0
        If Course = -1 Then Course = Leader.TableIx
        Dim _Heber = If(IsExchange, ExchangeLifter.ToString, dvLeader(Course)(bsLeader.Position)("Teilnehmer").ToString)

        If Not IsExchange Then

            If bsLeader.Position < Leader.RowNum Then
                ' gewerteter Versuch kann nicht verzichtet werden
                Beep()
                Return False
            End If

            If Not NoEnquiry Then
                ' Zeitraum zwischen Wertung und Techniknote
                If Wettkampf.Technikwertung AndAlso Leader.RowNum = 0 AndAlso bsLeader.Position = 0 Then
                    For i = 1 To 3
                        If dicLampen(i).ForeColor <> Anzeige.ScoreColors(0) Then
                            If Not IsNumeric(xTechniknote.Text) Then
                                If mnuMeldungen.Checked Then
                                    Using New Centered_MessageBox(Me)
                                        MessageBox.Show("Verzicht für laufenden Versuch nicht möglich.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Question)
                                    End Using
                                Else
                                    Beep()
                                End If
                                Return False
                            End If
                            Return False
                        End If
                    Next
                End If
            End If
        End If


        'alle verbleibenden Versuche des Hebers

        Dim row As DataRow() = dtLeader(Course).Select("Teilnehmer = " + _Heber + " AND (Convert(IsNull(Wertung,''), System.String) = '')", "Versuch ASC")

        Dim ResignedAttempt As Integer

        If Not UseFirstAttempt Then
            ResignedAttempt = CInt(row(0)!Versuch)
        ElseIf UseFirstAttempt AndAlso CInt(row(0)!Versuch) = 1 Then
            ResignedAttempt = 1
        Else
            Dim v = CInt(row(0)!Versuch) - 1
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Heber/in hat bereits " & v & " Versuch" & If(v = 1, "", "e") & " absolviert.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            Return False
        End If

        Dim msg = String.Empty

        ' prüfen, aktueller Versuch ausgewählt ist
        If Not UseFirstAttempt AndAlso CInt(row(0)!Versuch) < ResignedAttempt Then

            Dim Zahlen = {"", "", "zweiten und dritten", "dritten"}

            msg = "Der ausgewählte Versuch ist nicht der nächstfolgende des Hebers." & vbNewLine & "Verzicht für den " & Zahlen(ResignedAttempt) & " Versuch durchführen?"

            row = dtLeader(Course).Select("Teilnehmer = " + _Heber + " AND Versuch >= " + ResignedAttempt.ToString + " AND (Convert(IsNull(Wertung,''), System.String) = '')", "Versuch ASC")

        ElseIf Heber.Id = CInt(_Heber) AndAlso ResignedAttempt = CInt(row(0)!Versuch) Then
            ' aufgerufener Heber verzichtet
            If Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Started) Then
                Zeit_stoppen()
            ElseIf Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Stopped) Then
                msg = "CountDown gestoppt... Verzicht trotzdem durchführen?"
            End If
        End If

        If Not String.IsNullOrEmpty(msg) Then
            Using New Centered_MessageBox(Me)
                If MessageBox.Show(msg, msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.No Then Return False
            End Using
        ElseIf Not NoEnquiry Then
            Using New Centered_MessageBox(Me)
                If MessageBox.Show("Verzicht für '" + row(0)!Nachname.ToString + ", " + row(0)!Vorname.ToString + "' durchführen?",
                                   msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then Return False
            End Using
        End If

        Write_Verzicht(ResignedAttempt, row, IsExchange, Course)

        Return True
    End Function

    Class VerzichtEventArgs
        Inherits EventArgs
        Public _UseFirstAttempt As Boolean  ' immer ab dem ersten Versuch verzichten (nur für HeberAustausch in Bundesliga)
        Public _NoEnquiry As Boolean        ' keine Nachfrage, ob Verzicht durchgeführt werden soll (bei HeberAustausch in Bundesliga ... und sonst?)
        Public Sub New(UseFirstAttempt As Boolean, NoEnquiry As Boolean)
            MyBase.New()
            _UseFirstAttempt = UseFirstAttempt
            _NoEnquiry = NoEnquiry
        End Sub
    End Class

    '' Funktionen
    Private Function Get_NextAttempt(Row As DataRow) As Integer
        ' prüft ob der zu steigernde Versuch der nächste ist

        If CInt(dvLeader(Leader.TableIx)(bsLeader.Position)("Versuch")) > CInt(Row!Versuch) Then
            If mnuConfirm.Checked AndAlso Not mnuMeldungen.Checked Then
                Dim Lifter = If(Row!Sex.ToString = "m", "des Hebers", "der Heberin")
                Using New Centered_MessageBox(Me)
                    If MessageBox.Show("Ausgewählter Versuch ist nicht der nächstfolgende " & Lifter & "." & StrDup(2, vbNewLine) & "Nächstfolgenden Versuch " & Lifter & " auswählen?",
                                   msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.No Then
                        Return 3
                    End If
                End Using
            Else
                Beep()
            End If
            bsLeader.Position = bsLeader.Find("Reihenfolge", Row!Reihenfolge)
        End If
        Return 2
    End Function
    Private Function OrderTable(Optional IsManuellVisible As Boolean = False, Optional IsNextEnabled As Boolean = True) As Boolean

        ' Leader.RowNum = 1 --> wird in Call_OrderTable gesetzt
        Dim ReturnValue = False

        Try
            If InvokeRequired Then
                Dim d As New FunctionBoolean(AddressOf OrderTable)
                Invoke(d, New Object() {IsManuellVisible, IsNextEnabled})
            Else
                pnlManuell.Visible = IsManuellVisible
                Try
                    If dvLeader(Leader.TableIx).Count = 0 OrElse dvLeader(Leader.TableIx).Count - Leader.RowNum = 0 Then
                        ' letzter Versuch des Durchgangs beendet
                        If Leader.TableIx = 0 AndAlso dvLeader(1).Count > 0 Then
                            ' Versuche im Stoßen noch offen
                            btnNext.Text = "Stoßen beginnen"
                            ' Markierung in Publikum löschen
                            Leader.Mark_CalledLifters(Nothing)
                            If Not Wettkampf.Mannschaft Then btnGruppe.Enabled = True
                            ReturnValue = True
                        ElseIf Leader.TableIx = 1 Then
                            ' wenn Reißen nicht leer, kann Gruppe nicht beendet (asugeblendet) werden
                            If dvLeader(0).Count > 0 Then
                                ' noch Versuche im Reißen der Gruppe
                            Else
                                ' Gruppe beendet ODER kein Stoßen == Gruppe beendet
                                If Not IsNothing(dgvGruppe.Tag) Then
                                    For Each item In CType(dgvGruppe.Tag, List(Of Integer))
                                        Dim pos = bsGruppen.Find("WG", item)
                                        dvGruppen(pos)!Fertig = True
                                        bsGruppen.EndEdit()
                                        If dvGruppen.Count > 0 Then dgvGruppe.Height -= dgvGruppe.Rows(0).Height
                                    Next
                                End If
                            End If
                            If dvGruppen.Count = 0 OrElse Wettkampf.Mannschaft AndAlso Leader.TableIx = 1 AndAlso dvLeader(1).Count - Leader.RowNum = 0 Then
                                ' Wettkampf beendet
                                btnNext.Text = "WK beenden"
                            ElseIf Not Wettkampf.Mannschaft Then
                                ' weitere Gruppen laden
                                btnNext.Text = "Nächste Gruppe"
                                btnGruppe.Enabled = True
                                lblGruppe.Text = String.Empty
                                cboDurchgang.Enabled = True
                                cboDurchgang.SelectedIndex = -1
                                'chkBH.Enabled = User.UserId < UserID.Versuchsermittler
                                chkBH.CheckState = CheckState.Indeterminate
                                SortInsertion = String.Empty
                            ElseIf Wettkampf.Mannschaft AndAlso Leader.TableIx = 0 Then
                                ' Mannschafts-WK --> Stoßen beginnen
                                btnNext.Text = "Stoßen beginnen"
                            End If
                            ' Markierung in Publikum löschen
                            Leader.Mark_CalledLifters(Nothing)
                        End If
                        If Wettkampf.Mannschaft Then
                            bsResults.EndEdit()
                            Using RS As New Results
                                RS.TeamWertung(dvResults.ToTable)
                            End Using
                            bsTeam.ResetBindings(False)
                            If User.UserId = UserID.Wettkampfleiter Then
                                Using save As New mySave
                                    save.Save_Punkte(Me)
                                End Using
                            End If
                        End If
                    Else
                        ' noch ausstehende Versuche im Durchgang
                        ReturnValue = True
                        btnNext.Text = "Nächster Heber"
                    End If
                Catch ex As Exception
                End Try

                If IsNextEnabled Then
                    btnNext.Enabled = User.UserId = UserID.Wettkampfleiter
                Else
                    btnNext.Enabled = False
                End If
                btnAufruf.Enabled = False
                btnKorrektur.Enabled = ReturnValue OrElse btnNext.Text.Contains("beenden")
                btnSteigerung.Enabled = ReturnValue AndAlso Not VE_verbunden
                btnVerzicht.Enabled = ReturnValue AndAlso Not VE_verbunden

                If btnNext.Text.Contains("beenden") Then
                    Anzeige.CountDown = "0:00"
                    Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Waiting)
                End If

            End If
        Catch ex As Exception
        End Try

        Return ReturnValue
    End Function
    Private Function Permission(Position As Integer) As Boolean

        ' liest alle nicht gewerteten Versuche des ausgewählten Hebers
        bsLeader.EndEdit()
        Dim tn = dvLeader(Leader.TableIx)(Position)("Teilnehmer").ToString
        Dim rowLeader = dtLeader(Leader.TableIx).Select("Teilnehmer = " + tn, "Versuch ASC") ' aufsteigernd sortiert = 0 ist der aktuelle Versuch (gewertet -falls noch angezeigt- oder ausstehend)

        ' überprüft, ob der nächstfolgende Versuch ausgewählt ist
        For Each r As DataRow In rowLeader
            If Not CInt(r!Reihenfolge) = 0 AndAlso IsDBNull(r!Wertung) Then
                If Get_NextAttempt(r) = 3 Then
                    ' nicht zum richtigen Versuch wechseln
                    Return False
                End If
                Exit For
            End If
        Next

#Region "Prüfung auf Ungültig im vorhergehenden Versuch"
        ' liest alle gelaufenen Versuche 
        Dim rowGesamt As DataTable = dtDelete(Leader.TableIx).Clone
        Try
            rowGesamt = dtDelete(Leader.TableIx).Select("Teilnehmer = " + tn, "Versuch DESC").CopyToDataTable() ' absteigernd sortiert = 0 ist der als letztes gewertete Versuch
        Catch ex As Exception
        End Try
        ' noch nicht gespeicherte gewertete Versuche einlesen
        For Each x As DataRow In rowLeader
            rowGesamt.ImportRow(x)
        Next

        Dim row = New DataView(rowGesamt)
        If row.Count > 0 AndAlso Not IsDBNull(row(0)("Wertung")) Then
            If Not Wettkampf.IncreaseAfterNoLift AndAlso CInt(row(0)("Wertung")) = -1 Then
                'vorhergehender Versuch ungültig und Steigern verboten
                Using New Centered_MessageBox(Me, {"Ignorieren"})
                    If MessageBox.Show("In AK '" & row(0)("AK").ToString & "' kann nach einem ungültigem Versuch" + vbNewLine + "nicht gesteigert werden.", msgCaption,
                                MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then
                        Return False
                    End If
                End Using
            End If
        End If
#End Region

        ' prüft ob der anstehende Versuch bereits 3x gesteigert wurde
        If rowLeader.Count > 0 AndAlso CInt(rowLeader(0)("Steigerung3")) > 0 Then
            Using New Centered_MessageBox(Me, {"Ignorieren"})
                If MessageBox.Show("Es sind bereits 3 Steigerungen eingetragen.",
                                       msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = DialogResult.Cancel Then
                    Return False
                End If
            End Using
        End If

        If CInt(tn) = Heber.Id Then
            ' Steigerung betrifft Heber, der als nächstes dran ist

            If Heber.CountDown = 120 AndAlso ZN_Pad.CountDown <= 90 AndAlso
                CInt(dvLeader(Leader.TableIx)(bsLeader.Position)("Steigerung1")) = 0 AndAlso
               ZN_Pad.CountDown > 30 Then
                ' Heber hat 2 Minuten und bei 90 Sekunden keine Bestätigung/Steigerung
                Using New Centered_MessageBox(Me, {"Ignorieren"})
                    If MessageBox.Show("Keine Bestätigung/Steigerung innerhalb der ersten 30 Sekunden.", msgCaption,
                                       MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = DialogResult.Cancel Then
                        Return False
                    End If
                End Using
            End If

            If (ZN_Pad.CountDown <= 30 AndAlso Heber.CountDown = 120 AndAlso CInt(dvLeader(Leader.TableIx)(bsLeader.Position)("Steigerung1")) > 0) OrElse
               (ZN_Pad.CountDown <= 30 AndAlso Heber.CountDown = 60) Then
                ' Final Call ist vorbei
                Using New Centered_MessageBox(Me, {"Ignorieren"})
                    If MessageBox.Show("Der Countdown steht bei " + xAufruf.Text + " Minuten.", msgCaption,
                                       MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = DialogResult.Cancel Then
                        Return False
                    End If
                End Using
            End If

            If Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Stopped) AndAlso Heber.Wertung = 0 Then
                ' Aufruf im Limit gestoppt --> bereits gehoben?
                Using New Centered_MessageBox(Me, {"Ja", "Nein"})
                    If MessageBox.Show("Hat die Hebung bereits begonnen?", msgCaption,
                                       MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = DialogResult.OK Then
                        Return False
                    End If
                End Using
            ElseIf Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Started) Then
                ' Zeit läuft --> stoppen
                Zeit_stoppen()
            End If

        End If
        Return True
    End Function
    Private Function Sortierung() As Boolean
        Try
            If InvokeRequired Then
                Dim d As New FunctionBoolean(AddressOf Sortierung)
                Invoke(d, New Object() {})
            Else
                Validate()
                bsLeader.EndEdit()
                bsDelete.EndEdit()
                'Reihenfolge in obere Tabelle schreiben
                For i = 0 To dvLeader(Leader.TableIx).Count - 1
                    dvLeader(Leader.TableIx)(i)!Reihenfolge = i + 1 - Leader.RowNum
                Next
                bsLeader.EndEdit()
                'bsLeader.Position = Leader.RowNum + Leader.RowNum_Offset

                If Leader.RowNum = 1 Then Return False

                'If dvLeader(Leader.TableIx).Count > 1 - Leader.RowNum Then ' auskommentiert, weil bei Verzicht desd vorletzten Hebers dvLeader.Count == 1 ist
                If Heber.Id > 0 AndAlso Heber.Id <> CInt(dvLeader(Leader.TableIx)(Leader.RowNum)!Teilnehmer) Then
                    'aktueller Heber hat sich geändert
                    'If Not Panik Then 
                    'Leader.Set_Notiz(CInt(dvLeader(Leader.TableIx)(Leader.RowNum + Leader.RowNum_Offset)!Teilnehmer)) ' Event für frmModerator, um RTF neu einzulesen
                    'dgvLeader.ClearSelection()
                    Return True
                End If
                'End If
                'dgvLeader.ClearSelection()
            End If
        Catch ex As Exception
        End Try

        Return False
    End Function
    Private Sub Write_Bestätigung(attempt As Integer, row As DataRow())
        Try
            If InvokeRequired Then
                Dim d As New DelegateWrite(AddressOf Write_Bestätigung)
                Invoke(d, New Object() {attempt, row})
            Else
                SyncLock LockObject_Bestätigung
                    loading = True
                    Validate()
                    dgvLeader.EndEdit()
                    bsLeader.EndEdit()
                    bsResults.EndEdit()
                    row(0)!Steigerung1 = row(0)!HLast
                    ' Publikum
                    Dim pos = bsResults.Find("Teilnehmer", row(0)!Teilnehmer)
                    Dim col = UCase(Strings.Left(Leader.Table, 1)) + "_" + row(0)!Versuch.ToString
                    dvResults(pos)(col) = row(0)!HLast
                    Leader.Change_Anfangslast(CInt(row(0)!HLast), pos, col, True)
                    ' Speichern
                    If User.UserId = UserID.Wettkampfleiter Then
                        Using save As New mySave
                            save.Save_Steigerung({row(0)}.CopyToDataTable, Leader.Table, Me)
                        End Using
                    End If
                    btnBestätigung.Enabled = False
                    loading = False
                End SyncLock
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Write_Steigerung(addition As Integer, row As DataRow())
        ' Parameter: row = alle nicht gelaufenen Versuche aus Leader ---> row(0) = aktueller Versuch, Sortierung = ASC
        Try
            If InvokeRequired Then
                Dim d As New DelegateWrite(AddressOf Write_Steigerung)
                Invoke(d, New Object() {addition, row})
            Else
                SyncLock LockObject_Steigerung
                    loading = True
                    Validate()
                    dgvLeader.EndEdit()
                    bsLeader.EndEdit()
                    bsResults.EndEdit()
                    ' Steigerung des aktuellen Versuchs
                    row(0)!HLast = CInt(row(0)!HLast) + addition

                    ' Differenz für aktuellen Versuch (ab 2.V)
                    If CInt(row(0)!Versuch) > 1 Then row(0)!Diff = CInt(row(0)!Diff) + addition

                    'Steigerung für alle nächsten Versuche
                    For i = 1 To row.Count - 1
                        row(i)!HLast = CInt(row(i)!HLast) + addition
                    Next

                    ' neue HLast wird in nächste freie Steigerung geschrieben
                    If CInt(row(0)!Steigerung1) = 0 Then
                        row(0)!Steigerung1 = row(0)!HLast
                    ElseIf CInt(row(0)!Steigerung2) = 0 Then
                        row(0)!Steigerung2 = row(0)!HLast
                    Else
                        row(0)!Steigerung3 = row(0)!HLast
                    End If

                    bsLeader.EndEdit()

                    ' Steigerung speichern
                    If User.UserId = UserID.Wettkampfleiter Then ' nur in Leader durchführen
                        Using save As New mySave
                            save.Save_Steigerung(row.CopyToDataTable, Leader.Table, Me)
                        End Using
                    Else 'If User.UserId = UserID.Versuchsermittler  Then 
                        Client_Send("VIT=" & row(0)!Teilnehmer.ToString & ";V=" & row(0)!Versuch.ToString & ";H=" & row(0)!HLast.ToString) ' Steigerung an Server senden
                    End If

                    ' HLast in Resultat ändern 
                    Dim pos = bsResults.Find("Teilnehmer", row(0)!Teilnehmer)
                    Dim col = UCase(Strings.Left(Leader.Table, 1)) + "_" + row(0)!Versuch.ToString
                    dvResults(pos)(col) = row(0)!HLast
                    bsResults.EndEdit()

                    loading = False
                    Dim Heber_Changed = False

#Region "Panik"
                    If Panik Then
                        'Reihenfolge in obere Tabelle schreiben
                        For i = 0 To dvLeader(Leader.TableIx).Count - 1
                            dvLeader(Leader.TableIx)(i)!Reihenfolge = i + 1 - Leader.RowNum
                            'Debug.WriteLine(dvLeader(Leader.TableIx)(i)!Reihenfolge)
                        Next
                        bsLeader.EndEdit()

                        Dim SteigerndePos = bsLeader.Find("Reihenfolge", row(0)!Reihenfolge)
                        If CInt(row(0)!Teilnehmer) = Heber.Id Then
                            Leader.RowNum_Offset = SteigerndePos
                        ElseIf SteigerndePos <= Leader.RowNum_Offset + 1 Then
                            Leader.RowNum_Offset += 1
                        Else
                            Leader.RowNum_Offset -= 1
                        End If
                        ''MarkRows(Leader.RowNum + Leader.RowNum_Offset)
                        'Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
                        ''DimRow(Leader.RowNum + Leader.RowNum_Offset - 1)
                        'Leader.DimOut = Leader.RowNum + Leader.RowNum_Offset - 1
                        For i = 0 To Leader.RowNum_Offset - 1
                            dgvLeader.Rows(i).DefaultCellStyle.BackColor = Color.Orange
                        Next
                    End If
#End Region

                    Heber_Changed = Sortierung() ' muss bei Leader und Versuchsermittler durchgeführt werden

                    Dim Declared As Boolean? = Not mnuConfirm.Checked
                    Try
                        If Heber.Id > 0 AndAlso Leader.RowNum = 0 Then
                            If Heber_Changed AndAlso Not btnNext.Enabled Then
                                ' Heber wechselt nach Aufruf
                                ' --> Moderator Meldung
                                Zeit_stoppen()
                                ' --> Bohle, Aufwärmen
                                SendtoBohle()
                                Leader.Change_InfoMessage("Heber-Wechsel", InfoTarget.NotPublic)
                                ' --> in Publikum nur Last ändern, Farbe bleibt
                                Declared = True 'Nothing
                            ElseIf Heber.Id = CInt(row(0)!Teilnehmer) AndAlso Not btnNext.Enabled Then
                                ' aufgerufener Heber steigert
                                Zeit_stoppen()
                                If Heber_Changed Then
                                    ' --> Bohle, Aufwärmen
                                    SendtoBohle()
                                Else
                                    ' --> Bohle, Hantel
                                    Heber.Hantellast = CInt(row(0)!HLast)
                                    ' --> in Publikum nur Last ändern, Farbe bleibt
                                    Declared = Nothing
                                    'Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
                                    'If Leader.RowNum > 0 Then
                                    '    Leader.DimOut = Leader.RowNum + Leader.RowNum_Offset - 1
                                    'End If
                                End If
                                ' --> Moderator Meldung
                                If Heber_Changed Then
                                    Leader.Change_InfoMessage("Heber-Wechsel", InfoTarget.NotPublic)
                                Else
                                    Leader.Change_InfoMessage("neue Hantellast " & row(0)!HLast.ToString & " kg", InfoTarget.Moderator)
                                End If
                            Else
                                'Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
                                'If Leader.RowNum > 0 Then
                                '    Leader.DimOut = Leader.RowNum + Leader.RowNum_Offset - 1
                                'End If
                            End If
                        Else
                            ' Hantelbeladung vor WK-Beginn ändern
                            Dim started = (From x In dvDelete(Leader.TableIx).ToTable
                                           Where x.Field(Of Integer?)("Wertung") > -2).Count > 0
                            If Not started Then
                                Dim _row = dvLeader(Leader.TableIx)(Leader.RowNum).Row
                                Hantel.Change_Hantel(CInt(_row!HLast),, _row!sex.ToString, CInt(_row!Jahrgang), Wettkampf.BigDisc, False)
                            End If

                            'Dim tmp = dvLeader(Leader.TableIx)(Leader.RowNum + Leader.RowNum_Offset) ' Leader.RowNum war 0
                            'Leader.Update_Hantel(Leader.Set_Hantelstange(tmp!sex.ToString, tmp!Jahrgang.ToString, CInt(tmp!HLast)), CInt(tmp!HLast))
                            'Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
                            'If Leader.RowNum > 0 Then
                            '    Leader.DimOut = Leader.RowNum + Leader.RowNum_Offset - 1
                            'End If
                        End If
                        ' Publikum
                        Leader.Change_Anfangslast(CInt(row(0)!HLast), pos, col, Declared) ', Panik)
                    Catch ex As Exception
                    End Try
                End SyncLock
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Write_Verzicht(Versuch As Integer, row As DataRow(), Optional ExchangeLifter As Boolean = False, Optional Course As Integer = -1)
        Try
            If InvokeRequired Then
                Dim d As New DelegateVerzicht(AddressOf Write_Verzicht)
                Invoke(d, New Object() {Versuch, row, ExchangeLifter})
            Else
                SyncLock LockObject_Verzicht
                    loading = True
                    Validate()
                    dgvLeader.EndEdit()
                    bsLeader.EndEdit()
                    bsResults.EndEdit()
                    Dim _Table = If(Course = -1, Leader.Table, {"Reissen", "Stossen"}(Course))
                    ' Zeile in Results
                    Dim pos = bsResults.Find("Teilnehmer", row(0)!Teilnehmer)
                    Dim col = String.Empty

                    ' Wertung und HLast in dvLeader schreiben
                    For Each r As DataRow In row
                        r!Wertung = -2
                        r!HLast = 0
                        r!Note = DBNull.Value
                        ' Spalte in Results
                        col = UCase(Strings.Left(_Table, 1)) + "_" + r!Versuch.ToString
                        ' Wertung und HLast in dvResults schreiben
                        dvResults(pos)(col) = 0
                        dvResults(pos)(col.Insert(1, "W")) = -2
                        dvResults(pos)(col.Insert(1, "T")) = DBNull.Value ' damit kein Strich bei Note erscheint
                        bsResults.EndEdit()
                        ' Publikum
                        Leader.Change_Anfangslast(CInt(row(0)!HLast), pos, col)
                    Next

                    ' bei 3 verzichteten Versuchen die Wertung auf "0" (nicht "---") setzen
                    If Versuch = 1 Then
                        dvResults(pos)(_Table) = 0
                        dvResults(pos)(_Table + "_Platz") = DBNull.Value

                        ' bei Zweikampf-Wertung (Wertung = 5) gibt es kein Gesamt-Resultat, wenn eine Disziplin geplatzt ist (Disziplin = 0) --> Heben = 0
                        If Heber.Wertung = 5 Then
                            dvResults(pos)!Heben = 0
                            dvResults(pos)!Heben_Platz = DBNull.Value
                        Else
                            dvResults(pos)!Heben = CDbl(If(IsDBNull(dvResults(pos)!Reissen), 0, dvResults(pos)!Reissen)) + CDbl(If(IsDBNull(dvResults(pos)!Stossen), 0, dvResults(pos)!Stossen))
                            If CDbl(dvResults(pos)!Heben) = 0 Then dvResults(pos)!Heben_Platz = DBNull.Value
                        End If
                        If Wettkampf.Athletik Then
                            dvResults(pos)!Gesamt = dvResults(pos)!Heben
                            If dtResults.Columns.Contains("Athletik") AndAlso Not IsDBNull(dvResults(pos)!Athletik) Then
                                ' Athletik-Wertung einrechnen
                                dvResults(pos)!Gesamt = CDbl(If(IsDBNull(dvResults(pos)!Gesamt), 0, dvResults(pos)!Gesamt)) + CDbl(dvResults(pos)!Athletik)
                            End If
                            If CDbl(dvResults(pos)!Gesamt) = 0 Then dvResults(pos)!Gesamt_Platz = DBNull.Value
                        End If
                        If IsDBNull(dvResults(pos)!Wertung2) Then dvResults(pos)!Wertung2 = 0

                        bsResults.EndEdit()
                        Leader.Write_Scoring(pos, col, , -2)
                    End If

                    loading = False

                    If User.UserId = UserID.Wettkampfleiter Then
                        Using save As New mySave
                            save.Save_Results(_Table, Me)
                            If save.Save_Versuch(row.CopyToDataTable, _Table, Me) Then dtLeader(Course).AcceptChanges()
                        End Using
                    ElseIf Not ExchangeLifter Then
                        ' bei Heberwechsel wird Verzicht in Change_Lifter durchgeführt
                        Client_Send("VRT=" & row(0)!Teilnehmer.ToString & ";V=" & row(0)!Versuch.ToString) ' Verzicht an Server
                    End If

                    ' verzichteten Heber in Delete verschieben
                    For Each r As DataRow In row
                        dtDelete(Course).ImportRow(r)
                        r.Delete()
                    Next
                    bsLeader.EndEdit() 'stand davor
                    bsDelete.EndEdit() 'stand davor

                    If Not ExchangeLifter Then
                        If dgvLeader.Rows.Count - Leader.RowNum = 0 Then
                            OrderTable()
                        Else
                            Dim Heber_Changed = Sortierung()
                            If Heber.Id > 0 AndAlso Not btnNext.Enabled Then
                                If Heber_Changed Then
                                    SendtoBohle()
                                    Leader.Change_InfoMessage("Heber-Wechsel", InfoTarget.NotPublic)
                                End If
                            End If
                            'Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
                            'Leader.DimOut = Leader.RowNum + Leader.RowNum_Offset - 1
                        End If
                    End If
                    bsLeader.Position = Leader.RowNum + Leader.RowNum_Offset
                    dgvLeader.ClearSelection()
                End SyncLock
            End If
        Catch ex As Exception
        End Try
    End Sub

    '' Aufrufe
    Private Sub Change_Bohle()
        'lblBohle.Text = "Bohle " & User.Bohle
    End Sub
    Private Sub Heber_Dim(Row As Integer)
        If InvokeRequired Then
            Dim d As New DelegateInteger(AddressOf Heber_Dim)
            Invoke(d, New Object() {Row})
        Else
            Try
                Cursor = Cursors.WaitCursor
                SuspendLayout()
                With dgvLeader
                    .Rows(Row).DefaultCellStyle.BackColor = Color.DarkGray
                    .Rows(Row).DefaultCellStyle.ForeColor = Color.WhiteSmoke
                    .Rows(Row).Cells("Up").Value = arrow_up_16_grey
                    .Rows(Row).Cells("Down").Value = arrow_down_16_grey
                    .Rows(Row).Cells("Notizen").Value = edit16_gray
                    .ClearSelection()
                    .Rows(Row).ReadOnly = True
                    '.Refresh()
                    .Focus()
                End With
            Catch ex As Exception
            Finally
                ResumeLayout()
                Cursor = Cursors.Default
            End Try
        End If
    End Sub
    Private Sub Heber_Highlight(Row As Integer)
        If InvokeRequired Then
            Dim d As New DelegateInteger(AddressOf Heber_Highlight)
            Invoke(d, New Object() {Row})
        Else
            Try
                Cursor = Cursors.WaitCursor
                SuspendLayout()
                With dgvLeader
                    .Rows(Row).DefaultCellStyle.BackColor = Color.FromArgb(0, 240, 0) ' Color.Lime
                    .Rows(Row).Cells("Up").Value = arrow_up_16_light
                    .Rows(Row).Cells("Down").Value = arrow_down_16_light
                    .Rows(Row).Cells("Notizen").Value = edit16
                    .Rows(Row).DefaultCellStyle.ForeColor = .DefaultCellStyle.ForeColor
                    .Rows(Row).Cells("Versuch").Style.ForeColor = .Columns("Versuch").DefaultCellStyle.ForeColor
                    .Rows(Row).Cells("HLast").Style.ForeColor = .Columns("HLast").DefaultCellStyle.ForeColor
                    For i = Row + 1 To .Rows.Count - 1
                        Dim SetColor = False
                        Select Case WK_Options.Markierung
                            Case 1 ' nächsten zwei Versuche
                                SetColor = i < 3 + Leader.RowNum + Leader.RowNum_Offset
                            Case 2 ' Versuche des Hebers 
                                SetColor = CInt(dvLeader(Leader.TableIx)(i)!Teilnehmer) = CInt(dvLeader(Leader.TableIx)(Leader.RowNum + Leader.RowNum_Offset)!Teilnehmer)
                            Case 3 'Versuche mit gleicher Last
                                SetColor = CInt(dvLeader(Leader.TableIx)(i)!HLast) = CInt(dvLeader(Leader.TableIx)(Leader.RowNum + Leader.RowNum_Offset)!HLast)
                        End Select
                        If SetColor Then
                            .Rows(i).DefaultCellStyle.BackColor = Color.FromArgb(204, 255, 204) 'Color.Yellow
                        Else
                            ' normale BackColor
                            .Rows(i).DefaultCellStyle.BackColor = If(i Mod 2 = 0, .RowsDefaultCellStyle.BackColor, .AlternatingRowsDefaultCellStyle.BackColor)
                        End If
                    Next
                    .Focus()
                    .ClearSelection()
                End With
            Catch ex As Exception
            Finally
                ResumeLayout()
                Cursor = Cursors.Default
            End Try
        End If

    End Sub
    Private Sub Controls_Focused(sender As Object, e As EventArgs) Handles chkBH.GotFocus, btnVerzicht.GotFocus, btnSteigerung.GotFocus, btnNext.GotFocus, btnKorrektur.GotFocus, btnBestätigung.GotFocus, btnAufruf.GotFocus,
        btnAufruf.GotFocus, btnKorrektur.GotFocus, btnNext.GotFocus, btnSteigerung.GotFocus, btnBestätigung.GotFocus, btnVerzicht.GotFocus,
        chkBH.GotFocus, btnMinute1.GotFocus, btnMinute2.GotFocus
        dgvLeader.Focus()
    End Sub
    Private Sub statusMsg_Update(Status As Integer, Message As String)

        If InvokeRequired Then
            Dim d As New DelegateIntegerString(AddressOf statusMsg_Update)
            Invoke(d, New Object() {Status, Message})
        Else
            If statusMsg.Text.Equals(Message) Then Return

            statusMsg.Text = Message
            statusMsg.ForeColor = Color.Red

            Leader.ManuelleWertung = False

            Select Case Status
                Case 0
                    If Not HideMessages Then
                        Using New Centered_MessageBox(Me)
                            MessageBox.Show("Keine KR-Pads angeschlossen." + vbNewLine +
                                        "Wertung erfolgt am PC...", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                    End If
                    Leader.ManuelleWertung = True
                Case 2
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("2 KR-Pads angeschlossen." + vbNewLine +
                                        "Wertung erfolgt am PC...", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                    Leader.ManuelleWertung = True
                Case 1000 ' Init
                    statusMsg.ForeColor = Color.Black
                Case 1 '>= Wettkampf.KR(User.Bohle)
                    statusMsg.ForeColor = Color.Green
                Case < 2000
                    Dim NoPads As New List(Of String)
                    For i = 1 To 3
                        If Not KR_Pad(i).Connected Then NoPads.Add(i.ToString)
                    Next
                    Using New Centered_MessageBox(Me, {If(Wettkampf.KR(User.Bohle) = 1, "KR-Pad", "KR-Pads"), "Computer"})
                        If MessageBox.Show("KR-Pad " & Join(NoPads.ToArray, " und ") & If(NoPads.Count = 1, " ist", " sind") & " nicht angeschlossen." + vbNewLine +
                                           "Wo soll die Wertung durchgeführt werden?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = DialogResult.No Then
                            WA_Message.Status = 2000
                            Leader.ManuelleWertung = True
                            Return
                        End If
                    End Using
            End Select

            pnlManuell.Visible = Leader.ManuelleWertung AndAlso Heber.Id > 0 AndAlso Not btnNext.Enabled

            btnAufruf.Enabled = Not ZN_Pad.Connected AndAlso Heber.Id > 0 AndAlso Not btnNext.Enabled AndAlso Not ZN_Pad.IsAttemptRated
        End If
    End Sub
    Private Sub Zeit_stoppen()
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub(AddressOf Zeit_stoppen)
                Invoke(d, New Object() {})
            Else
                If Anzeige.ClockColor = Anzeige.Clock_Colors(Clock_Status.Started) Then
                    If User.UserId = UserID.Versuchsermittler Then
                        Client_Send("VZ") ' Zeit stoppen an Server
                    ElseIf btnAufruf.Enabled Then
                        btnAufruf.PerformClick()
                    ElseIf User.UserId = UserID.Wettkampfleiter Then
                        WA_Events.Push_ZN(vbLf)
                        'WA_Events.Set_ZN_Suspended(True)
                        WA_Events.Push_ZN("Lock")
                        timerSuspend = New Threading.Timer(AddressOf TimerSuspend_Tick)
                        timerSuspend.Change(ZN_Sperrzeit, Threading.Timeout.Infinite)
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub TimerSuspend_Tick(state As Object)
        'WA_Events.Set_ZN_Suspended(False)
        WA_Events.Push_ZN("Unlock")
        timerSuspend.Dispose()
    End Sub

    '' Speichern
    'Structure Save_Args
    '    Dim Rows As DataTable ' DataRow()
    '    Dim Table As String
    'End Structure

    ''Private Delegate Sub bgwDelegate(sender As Object, e As DoWorkEventArgs)
    'Private Delegate Sub bgwDelegate(Rows As DataTable, Table As String)
    'Private Delegate Sub bgwDelegate1(Table As String)

    '' dgvLeader
    Private Sub Build_Grid(grid As DataGridView, table As String, Optional Bereich As String = "")
        With grid
            .AutoGenerateColumns = False
            .AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke
            .RowsDefaultCellStyle.BackColor = Color.White
            .DefaultCellStyle.SelectionForeColor = Color.FromKnownColor(KnownColor.HighlightText)
            .DefaultCellStyle.SelectionBackColor = Color.FromKnownColor(KnownColor.Highlight)
            .AllowUserToOrderColumns = True

            Dim ColBez() As String = {"TN", "Nr.", "Gr.", "Nachname", "Vorname", "m/w", "...", "Nation", "Verein", "Jahrgang", "Gewicht", "Versuch",
                                      table, "+", "-", "Wertung", "Technik", "Zeit", "Start-Nr", "GK", "AK", ""}
            Dim ColData() As String = {"Teilnehmer", "Reihenfolge", "Gruppe", If(Wettkampf.International, "Name1", "Nachname"), "Vorname", "sex", "", "state_name", "Vereinsname", "Jahrgang", "Wiegen", "Versuch", "HLast",
                                       "", "", "", "Note", "Zeit", "Startnummer", "GK", "AK", "a_K"}
            Dim ColReadOnly() As String = {"Teilnehmer", "Reihenfolge", "Nachname", "Vorname", "Geschlecht", "Notizen", "Vereinsname", "Jahrgang", "Wiegen",
                                           "Versuch", "HLast", "Startnummer", "Gruppe", "Zeit", "GK", "AK", "a_K", "Up", "Down", "Wertung"}
            Dim ColAlignCenter() As String = {"Gruppe", "Geschlecht", "Notizen", "Wiegen", "Reihenfolge", "Versuch", "HLast", "Up", "Down", "Note", "Startnummer", "Wertung", "GK", "Jahrgang"}

            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Teilnehmer", .Visible = False})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Reihenfolge"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Gruppe", .Visible = Wettkampf.Mannschaft})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Geschlecht"}) ', .Visible = Wettkampf.Mannschaft})
            .Columns.Add(New DataGridViewImageColumn With {.Name = "Notizen", .Resizable = DataGridViewTriState.False, .Image = edit16})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nation", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill, .Visible = Wettkampf.International})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vereinsname", .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill, .FillWeight = 125, .Visible = Not Wettkampf.International})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Jahrgang"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Wiegen"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Versuch"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "HLast"})
            .Columns.Add(New DataGridViewImageColumn With {.Name = "Up", .Resizable = DataGridViewTriState.False, .Image = arrow_up_16_light, .Visible = False})
            .Columns.Add(New DataGridViewImageColumn With {.Name = "Down", .Resizable = DataGridViewTriState.False, .Image = arrow_down_16_light, .Visible = False})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Wertung", .ValueType = GetType(String), .Visible = False})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Note", .Visible = False})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Zeit"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Startnummer"}) ', .Visible = Not Wettkampf.Mannschaft})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "GK"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "AK"})
            .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "a_K", .Visible = func.Show_a_K})
            .Columns("Wiegen").DefaultCellStyle.Format = "0.00"
            .Columns("Note").DefaultCellStyle.Format = "0.00"

            '.Columns("Wertung").DefaultCellStyle.Font = New Font(fontFamilies(2), 14, FontStyle.Regular, GraphicsUnit.Point) 'New Font("Wingdings", 14)
            .Columns("Teilnehmer").DefaultCellStyle.ForeColor = Color.LightGray
            .Columns("Teilnehmer").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("Reihenfolge").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, .DefaultCellStyle.Font.Size, FontStyle.Bold, GraphicsUnit.Point)
            .Columns("Versuch").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, .DefaultCellStyle.Font.Size, FontStyle.Bold, GraphicsUnit.Point)
            .Columns("Versuch").DefaultCellStyle.ForeColor = Color.Red
            .Columns("HLast").DefaultCellStyle.Font = New Font(.DefaultCellStyle.Font.FontFamily, .DefaultCellStyle.Font.Size, FontStyle.Bold, GraphicsUnit.Point)
            .Columns("HLast").DefaultCellStyle.ForeColor = Color.Blue

            For i As Integer = 0 To ColBez.Count - 1
                .Columns(i).HeaderText = ColBez(i) ' + vbNewLine + vbNewLine
                .Columns(i).DataPropertyName = ColData(i)
                .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            'Spalten zentriert
            For i As Integer = 0 To ColAlignCenter.Count - 1
                .Columns(ColAlignCenter(i)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            Next

            'legt fest, welche Spalten editierbar sind
            For i As Integer = 0 To ColReadOnly.Length - 1
                .Columns(ColReadOnly(i)).ReadOnly = True
            Next

            ''legt fest, welche Spalten unsichtbar sind
            'For i As Integer = 0 To ColInVisible.Length - 1
            '    .Columns(ColInVisible(i)).Visible = False
            'Next

            .Columns("Zeit").DefaultCellStyle.Format = "T"

            'Format_Grid(Bereich, grid)

        End With

        'Change_NameFormat(grid)

    End Sub

    Private Sub dgv_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvLeader.CellFormatting, dgv2.CellFormatting

        If CType(sender, DataGridView).Columns(e.ColumnIndex).Name.Equals("a_K") Then
            If Equals(e.Value, False) Then
                e.Value = ""
                e.FormattingApplied = True
            ElseIf Equals(e.Value, True) Then
                e.Value = "a.K."
                e.FormattingApplied = True
            End If
        End If
    End Sub

    Private Sub dgvLeader_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvLeader.CellClick
        If e.ColumnIndex < 0 Or e.RowIndex < 0 Then Return

        'If Panik OrElse IsDBNull(dgvLeader.Rows(e.RowIndex).Cells("Reihenfolge").Value) OrElse CInt(dgvLeader.Rows(e.RowIndex).Cells("Reihenfolge").Value) = 0 Then
        If CInt(dgvLeader.Rows(e.RowIndex).Cells("Reihenfolge").Value) = 0 Then
            Beep()
            Return
        End If

        Dim msg As String = String.Empty
        Dim addition As Integer
        Dim Steigern As Boolean

        Select Case dgvLeader.Columns(e.ColumnIndex).Name
            Case "Notizen"
                Using formNotiz As New frmNotiz
                    With formNotiz
                        .StartPosition = FormStartPosition.Manual
                        .Text = "Notiz - " & dvLeader(TabIndex)(bsLeader.Position)!Name2.ToString
                        .AthleteId = CInt(dvLeader(TabIndex)(bsLeader.Position)!Teilnehmer)
                        Using GF As New myFunction
                            .Location = .FindForm.PointToClient(GF.Get_Location(dgvLeader, e.ColumnIndex, e.RowIndex, Position.BottomLeft, -1))
                        End Using
                        .ShowDialog(Me)
                        If .DialogResult.Equals(DialogResult.OK) Then
                            Dim pos = bsNotizen.Find("Teilnehmer", .AthleteId)
                            dvNotizen(pos)!Notiz = .Notiz
                            bsNotizen.EndEdit()
                            If .AthleteId = Heber.Id OrElse .AthleteId = CInt(dvLeader(Leader.TableIx)(Leader.RowNum)!Teilnehmer) Then
                                Leader.Set_Notiz(.AthleteId)
                            End If
                        End If
                    End With
                End Using
            Case "Up", "Down"
                statusSteigerung.Image = ImageList1.Images("btnGray.png")
                Steigern = Permission(e.RowIndex)
                If Steigern = True Then
                    If msg = String.Empty Then
                        'alle verbleibenden Versuche des Hebers
                        Dim row() = dtLeader(Leader.TableIx).Select("Teilnehmer = " + dvLeader(Leader.TableIx)(bsLeader.Position)("Teilnehmer").ToString +
                                                                    " AND Convert(IsNull(Wertung,''), System.String) = '' AND Convert(IsNull(Reihenfolge,''), System.String) <> ''", "Versuch ASC")
                        'neues Hantelgewicht
                        If dgvLeader.Columns(e.ColumnIndex).Name = "Up" Then
                            addition = 1
                        Else
                            addition = -1
                        End If
                        If msg = String.Empty Then HantelLast_Validate(row, addition)
                    End If
                End If
                If Steigern = True AndAlso Not String.IsNullOrEmpty(msg) Then
                    If mnuKonflikte.Checked Then
                        Using New Centered_MessageBox(Me)
                            MessageBox.Show(msg, "Steigerung Hantelgewicht", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End Using
                    Else
                        Beep()
                    End If
                End If
        End Select

    End Sub
    Private Sub dgvLeader_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvLeader.CellMouseDown
        If e.Button = MouseButtons.Right AndAlso e.RowIndex > -1 Then
            bsLeader.Position = e.RowIndex
            If dgvLeader.Rows(bsLeader.Position).ReadOnly Then
                dgvLeader.ContextMenuStrip = Nothing
                Beep()
            Else
                dgvLeader.ContextMenuStrip = ContextMenuGrid
            End If
        End If
    End Sub
    Private Sub dgvLeader_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvLeader.DataError
        Try
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvLeader_DataSourceChanged(sender As Object, e As EventArgs) Handles dgvLeader.DataSourceChanged
        'With dgvLeader
        '    If .DataSource IsNot Nothing Then
        '        .ContextMenuStrip = ContextMenuGrid
        '    Else
        '        .ContextMenuStrip = ContextMenuEmpty
        '    End If
        'End With
    End Sub
    Private Sub dgvLeader_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvLeader.KeyDown
        If dgvCtrlPressed Then Return
        dgvCtrlPressed = e.Control
        If dgvCtrlPressed Then dgvFontSize = dgvLeader.DefaultCellStyle.Font.Size
    End Sub
    Private Sub dgvLeader_KeyUp(sender As Object, e As KeyEventArgs) Handles dgvLeader.KeyUp
        dgvCtrlPressed = False
    End Sub

    'Private Sub dgvLeader_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles dgvLeader.RowsAdded
    '    If Leader.TableIx = 0 Then
    '        SuspendLayout()
    '        dgv2_Move()
    '        ResumeLayout()
    '    End If
    'End Sub
    Private Sub dgvLeader_RowsRemoved(sender As Object, e As DataGridViewRowsRemovedEventArgs) Handles dgvLeader.RowsRemoved
        If Leader.TableIx = 0 Then
            SuspendLayout()
            dgv2_Move()
            ResumeLayout()
        End If
    End Sub

    Private Sub NameFormat_Changed()
        Change_NameFormat(dgvLeader)
        Change_NameFormat(dgv2)
    End Sub
    Private Sub Change_NameFormat(dgv As DataGridView)
        Try
            With dgv
                Select Case Ansicht_Options.NameFormat
                    Case NameFormat.Standard
                        .Columns("Nachname").DataPropertyName = "Nachname"
                    Case NameFormat.Standard_UCase
                        .Columns("Nachname").DataPropertyName = "Name1"
                    Case NameFormat.International
                        .Columns("Nachname").DataPropertyName = "Name2"
                        .Columns("Vorname").Visible = False
                End Select
                .AutoResizeColumns()
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Function IsWertungSet(RowIndex As Integer) As Boolean
        If (Not btnNext.Enabled OrElse User.UserId = UserID.Versuchsermittler) AndAlso
                Leader.RowNum + Leader.RowNum_Offset = 1 AndAlso RowIndex + Leader.RowNum_Offset = 0 Then
            ' Aufruf gestartet und aufgerufener Heber = ausgewählter Heber
            For i = 1 To 3
                If Not loading AndAlso (dicLampen(i).ForeColor = Anzeige.ScoreColors(5) OrElse dicLampen(i).ForeColor = Anzeige.ScoreColors(7)) Then
                    ' mindestens eine Wertung abgegeben
                    Return False
                    Exit For
                End If
            Next
        End If
        Return True
    End Function
    Private Sub bsLeader_PositionChanged(RowIndex As Integer)

        dgvLeader.MultiSelect = False
        Try
            Dim Enable = IsWertungSet(RowIndex)
            btnSteigerung.Enabled = Enable AndAlso Not VE_verbunden
            btnVerzicht.Enabled = Enable AndAlso Not VE_verbunden

            btnKorrektur.Enabled = User.UserId = UserID.Wettkampfleiter AndAlso Enable

            If loading Then Return

            btnPanik.Enabled = Not Panik AndAlso btnNext.Enabled AndAlso Not IsDBNull(dvLeader(Leader.TableIx)(RowIndex)("Reihenfolge")) AndAlso RowIndex > Leader.RowNum

            With dgvLeader.DefaultCellStyle
                Try
                    If CInt(dvLeader(Leader.TableIx)(RowIndex)("Reihenfolge")) = 0 Then
                        .SelectionForeColor = Color.Transparent
                        .SelectionBackColor = Color.Transparent
                    Else
                        .SelectionForeColor = Color.FromKnownColor(KnownColor.HighlightText)
                        .SelectionBackColor = Color.FromKnownColor(KnownColor.Highlight)
                    End If
                Catch ex As Exception
                End Try
            End With
            ' Durch Bestätigung/Steigerung vor jedem Versuch (also vor dem Aufruf = btnNext:Enabled
            ' sind für den Heber zwei weitere Änderungen möglich
            If RowIndex = 0 AndAlso Leader.RowNum = 0 Then Return ' Zeiger steht auf gewertetem Heber
            If CInt(dvLeader(Leader.TableIx)(RowIndex)("Teilnehmer")) = Heber.Id AndAlso Not btnNext.Enabled Then Return ' Zeiger steht auf aufgerufenem Heber

            ' kleinsten Versuch suchen
            Dim minV = (From x In dtLeader(Leader.TableIx)
                        Where x.Field(Of Integer)("Teilnehmer") = CInt(dvLeader(Leader.TableIx)(RowIndex)("Teilnehmer")) And x.Field(Of Integer)("Reihenfolge") > 0
                        Select x.Field(Of Integer)("Versuch")).Min

            btnBestätigung.Enabled = minV = CInt(dvLeader(Leader.TableIx)(RowIndex)!Versuch) AndAlso CInt(dvLeader(Leader.TableIx)(RowIndex)!Steigerung1) <= CInt(dvLeader(Leader.TableIx)(RowIndex)!HLast)

        Catch ex As Exception
            btnBestätigung.Enabled = False
        End Try
    End Sub

    '' 2. DGV
    Private Sub dgv2_Move()

        Dim rect As Rectangle
        Dim loc As Point

        Try
            With dgvLeader
                rect = .GetRowDisplayRectangle(.Rows.GetFirstRow(DataGridViewElementStates.Visible) + .Rows.GetRowCount(DataGridViewElementStates.Visible) - 1, True)
                rect = New Rectangle(.Left, .Top, .Width, .Rows.GetRowCount(DataGridViewElementStates.Visible) * .Rows(0).Height + .ColumnHeadersHeight)
                loc = .PointToScreen(New Point(rect.Left, rect.Bottom)) ' + 5)) ' dgv2 wird 5 pt unterhalb der letzten Zeile von dgvLeader positioniert
            End With

            With dgv2
                Dim minHeight = .ColumnHeadersHeight + .RowTemplate.Height + 2
                If .Rows.Count > 0 Then 'AndAlso dgvLeader.Height - loc.Y > minHeight Then
                    .Location = .FindForm.PointToClient(loc)
                    '.Size = New Size(dgvLeader.Width, dgvLeader.Height - .Top)
                    .Size = New Size(rect.Width, dgvLeader.Height - rect.Bottom)
                    .Visible = True
                    .BringToFront()
                Else
                    .Visible = False
                End If
            End With
        Catch ex As Exception
        End Try

        If pnlManuell.Visible Then pnlManuell.BringToFront()
        If pnlSteigerung.Visible Then pnlSteigerung.BringToFront()
        If pnlNotizen.Visible Then pnlNotizen.BringToFront()
    End Sub
    Private Sub dgv2_SelectionChanged(sender As Object, e As EventArgs) Handles dgv2.SelectionChanged
        dgv2.ClearSelection()
    End Sub
    Private Sub dgv2_Reihenfolge()
        ' Reihenfolge für 2. DGV
        If Leader.TableIx = 0 Then
            For i = 0 To dvLeader(1).Count - 1
                dvLeader(1)(i)("Reihenfolge") = i + 1
            Next
        End If
    End Sub

    '' Menü
    Private Sub mnuSplit_AG_Click(sender As Object, e As EventArgs) Handles mnuSplit_AG.Click
        Do
            Using InputBox As New Custom_InputBox("Eingabe der Jahrgänge" + vbNewLine +
                                                  "im Format [JJJJ] in der Reihenfolge," + vbNewLine +
                                                  "in der sie in der Tabelle erscheinen sollen" + vbNewLine +
                                                  "(Jahrgänge mit Komma trennen):",
                                                  "Sortierung nach Jahrgang",
                                                  SortInsertion,
                                                  Left + 250, Top + 200)
                SortInsertion = InputBox.Response
            End Using

            If Not IsNothing(SortInsertion) AndAlso Not SortInsertion.Equals(String.Empty) Then
                Dim tmp = Split(SortInsertion, ",")
                Dim lst As New List(Of Integer)
                Try
                    For Each s In tmp
                        lst.Add(CInt(Trim(s)))
                    Next
                    Dim Sort = String.Empty
                    For i = 0 To lst.Count - 2
                        If lst(i) < lst(i + 1) Then
                            If Sort.Equals("DESC") Then Throw New Exception("'s gladschd glei")
                            Sort = "ASC"
                        ElseIf lst(i) > lst(i + 1) Then
                            If Sort.Equals("ASC") Then Throw New Exception("'s gladschd glei")
                            Sort = "DESC"
                        End If
                    Next
                    SortInsertion = "Jahrgang " + Sort + ", "
                    Exit Do
                Catch ex As Exception
                    If ex.Message.Equals("'s gladschd glei") Then
                        ' Sortier-Spalte im dv einfügen und entsprechend der JGs füllen
                        SortInsertion = String.Empty ' "SortCol ASC"
                    Else
                        Using New Centered_MessageBox(Me, {"Wiederholen"})
                            If MessageBox.Show("Jahrgänge müssen als Komma getrennte numerische Werte" + vbNewLine + "im Format [JJJJ] eingegeben werden.",
                                                msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = DialogResult.Cancel Then
                                SortInsertion = String.Empty
                                Return
                            End If
                        End Using
                    End If
                End Try
            End If
        Loop While Not IsNothing(SortInsertion) AndAlso Not SortInsertion.Equals(String.Empty)

        Dim HeberChanged = Sortierung()
        If Heber.Id > 0 AndAlso HeberChanged Then SendtoBohle()

        'MarkRows(Leader.RowNum + Leader.RowNum_Offset)
        'DimRow(Leader.RowNum + Leader.RowNum_Offset - 1)
        Leader.Highlight = Leader.RowNum + Leader.RowNum_Offset
        Leader.DimOut = Leader.RowNum + Leader.RowNum_Offset - 1

    End Sub
    Private Sub mnuAktuellenVersuch_CheckedChanged(sender As Object, e As EventArgs) Handles mnuAktuellenVersuch.CheckedChanged
        If loading Then Return
        Try
            dgvLeader.Columns("Up").Visible = Not mnuAktuellenVersuch.Checked
            dgvLeader.Columns("Down").Visible = Not mnuAktuellenVersuch.Checked
            dgv2.Columns("Up").Visible = Not mnuAktuellenVersuch.Checked
            dgv2.Columns("Down").Visible = Not mnuAktuellenVersuch.Checked
        Catch ex As Exception
        End Try
    End Sub
    Private Sub mnuBeenden_Click(sender As Object, e As EventArgs) Handles mnuBeenden.Click
        Close()
    End Sub
    Private Sub mnuGesamtTechniknote_CheckedChanged(sender As Object, e As EventArgs) Handles mnuTechniknote.CheckedChanged
        txtT2.Visible = Not mnuTechniknote.Checked
        txtT3.Visible = Not mnuTechniknote.Checked
    End Sub
    Private Sub mnuMauszeiger_Click(sender As Object, e As EventArgs) Handles mnuMauszeiger.Click
        Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
        Cursor.Show()
    End Sub
    Private Sub mnuPause_Click(sender As Object, e As EventArgs) Handles mnuPause.Click

        'With btnNext
        '    .Tag = .Enabled
        '    .Enabled = False
        'End With

        'btnAufruf.Enabled = False

        frmPause.Show()

    End Sub
    Private Sub mnuTableLayout_Click(sender As Object, e As EventArgs) Handles mnuTableLayout.Click

    End Sub
    Private Sub mnuTCP_Click(sender As Object, e As EventArgs) Handles mnuTCP.Click

        If mnuTCP.Text.Contains("starten") Then
            If User.UserId = UserID.Wettkampfleiter Then
                While bgwServer.IsBusy
                    Threading.Thread.Sleep(100)
                End While
                If Not bgwServer.IsBusy Then
                    If Not IsNothing(Server) Then Server.Stop()
                End If
                'While bgwServer.CancellationPending
                '    Threading.Thread.Sleep(100)
                'End While
                'If Not bgwServer.CancellationPending Then bgwServer.RunWorkerAsync()
                Server.Start()
            Else
                Client_Start()
                Client_Send("VQ")
            End If
        Else
            Client_Send("S") ' Stopp an Server/Client senden
            ComMsg.Visible = False
            Client.Close()
            If User.UserId = UserID.Wettkampfleiter Then
                Server.Stop()
                bgwServer.CancelAsync()
            Else
                bgwClient.CancelAsync()
            End If
        End If
    End Sub
    Private Sub mnuHeber_Click(sender As Object, e As EventArgs) Handles mnuHeber.Click, cmnuHeber.Click

        Dim TN = dvLeader(Leader.TableIx)(bsLeader.Position)!Teilnehmer.ToString
        Dim idVerein = CInt(dvLeader(Leader.TableIx)(bsLeader.Position)!idVerein)

        Using formAthleten As New frmAthleten
            With formAthleten
                .Tag = TN
                .ShowDialog(Me)
                If Not .DialogResult = DialogResult.Cancel Then
                    Cursor = Cursors.WaitCursor
                    Dim selection = "Teilnehmer = " & TN
                    Dim Verein_Changed = .Athlet.idVerein <> idVerein
                    For i = 0 To 1
                        RowsHeber_Change(dtLeader(i).Select(selection), .Athlet)
                        RowsHeber_Change(dtDelete(i).Select(selection), .Athlet)
                        If Verein_Changed Then
                            RowsVerein_Change(dtLeader(i), .Athlet.idVerein.ToString)
                            RowsVerein_Change(dtDelete(i), .Athlet.idVerein.ToString)
                        End If
                    Next
                    RowsHeber_Change(dtResults.Select(selection), .Athlet)
                    If Verein_Changed Then RowsVerein_Change(dtResults, .Athlet.idVerein.ToString)
                    Update_Anzeige(.Athlet, Nothing)
                    Cursor = Cursors.Default
                End If
            End With
        End Using
    End Sub
    Private Sub mnuVerein_Click(sender As Object, e As EventArgs) Handles mnuVerein.Click, cmnuVerein.Click

        Dim idVerein = CInt(dvLeader(Leader.TableIx)(bsLeader.Position)!idVerein)
        Dim idHeber = CInt(dvLeader(Leader.TableIx)(bsLeader.Position)!Teilnehmer)

        If idVerein = 0 Then
            Beep()
            Return
        End If

        Using formVerein As New frmVerein
            With formVerein
                .Tag = dvLeader(Leader.TableIx)(bsLeader.Position)!idVerein
                .ShowDialog(Me)
                If Not .DialogResult = DialogResult.Cancel Then
                    Cursor = Cursors.WaitCursor
                    For i = 0 To 1
                        RowsVerein_Change(dtLeader(i), idVerein.ToString)
                        RowsVerein_Change(dtDelete(i), idVerein.ToString)
                    Next
                    RowsVerein_Change(dtResults, idVerein.ToString)
                    Update_Anzeige(Nothing, idHeber, idVerein)
                    Cursor = Cursors.Default
                End If
            End With
        End Using
    End Sub
    Private Sub mnuResultsLifter_Click(sender As Object, e As EventArgs) Handles mnuResultsLifter.Click

        Dim ix = bsResults.Find("Teilnehmer", dvLeader(Leader.TableIx)(bsLeader.Position)!Teilnehmer)
        Dim row = dvResults(ix).Row
        Dim rect = dgvLeader.GetCellDisplayRectangle(dgvLeader.Columns("Nachname").Index, dgvLeader.CurrentRow.Index, False)
        Dim dg = {"R", "S"}

        With dgvHeber
            For z = 0 To 1
                For s = 1 To 3
                    .Rows(z).Cells("D" & s).Value = row(dg(z) & "_" & s)
                    .Rows(z).Cells("T" & s).Value = row(dg(z) & "T_" & s)
                Next
                .Rows(z).Cells("Wert").Value = row(If(dg(z).Equals("R"), "Reissen", "Stossen"))
            Next
            .Rows(2).Cells("Wert").Value = row!Heben

            .Location = New Point(rect.X + 1, rect.Y + rect.Height)
            If .Top + .Height > dgvLeader.Height Then
                If rect.Y - .Height >= dgvLeader.Top Then
                    .Top = rect.Y - .Height
                Else
                    .Top = dgvLeader.Height - .Height
                End If
            End If
            .ClearSelection()
            .Visible = True
            .Focus()
            .BringToFront()
        End With
    End Sub
    Private Sub mnuZNPadSperrzeit_Click(sender As Object, e As EventArgs) Handles mnuZNPadSperrzeit.Click

        Dim Sperrzeit As String
        Do
            Using InputBox As New Custom_InputBox("Sperrzeit in Sekunden" + vbNewLine + "(Standard = 5 s)",
                                                  "ZN-Pad Sperrzeit",
                                                  (ZN_Sperrzeit \ 1000).ToString,
                                                  Left + 50,
                                                  Top + 350)
                Sperrzeit = InputBox.Response
            End Using

            If Not IsNothing(Sperrzeit) AndAlso Not Sperrzeit.Equals(String.Empty) Then
                If IsNumeric(Sperrzeit) Then
                    ZN_Sperrzeit = CInt(Sperrzeit) * 1000
                    Exit Do
                End If
            End If
        Loop While Not IsNothing(Sperrzeit)

    End Sub

    Private Sub RowsHeber_Change(Rows() As DataRow, Athlet As frmAthleten.myAthlet)
        bsLeader.EndEdit()
        bsDelete.EndEdit()
        bsResults.EndEdit()
        With Athlet
            For Each r As DataRow In Rows
                r!Nachname = .Nachname
                r!Vorname = .Vorname
                r!idVerein = .idVerein
                r!Jahrgang = .Jahrgang
                r!sex = .Geschlecht
                Dim ak = Glob.Get_AK(.Jahrgang)
                If Not IsNothing(ak.Value) Then
                    r!idAK = ak.Key
                    r!AK = ak.Value
                End If
            Next
        End With
    End Sub
    Private Sub RowsVerein_Change(Table As DataTable, idVerein As String)
        bsLeader.EndEdit()
        bsDelete.EndEdit()
        bsResults.EndEdit()
        Dim rows = Table.Select("idVerein = " & idVerein)
        Using f As New myFunction
            For Each r As DataRow In rows
                Dim tmp = f.Get_VereinInfo(idVerein.ToString)
                If Not IsNothing(tmp) Then
                    If Table.Columns.Contains("Vereinsname") Then r!Vereinsname = tmp(0)!Vereinsname
                    If Table.Columns.Contains("Verband") Then r!Verband = tmp(0)!Verband
                    If Table.Columns.Contains("region_flag") Then r!region_flag = tmp(0)!region_flag
                    If Table.Columns.Contains("state_iso3") Then r!state_iso3 = tmp(0)!state_iso3
                    If Table.Columns.Contains("state_name") Then r!state_name = tmp(0)!state_name
                    If Table.Columns.Contains("state_flag") Then r!state_flag = tmp(0)!state_flag
                End If
            Next
        End Using
    End Sub
    Private Sub Update_Anzeige(Athlet As frmAthleten.myAthlet, idHeber As Integer, Optional idVerein As Integer = -1)
        Dim tmp As DataTable
        Using f As New myFunction
            If Not IsNothing(Athlet) Then
                With Athlet
                    Dim AKs = Glob.Get_AK(.Jahrgang)
                    tmp = f.Get_VereinInfo(.Id.ToString)
                    If Heber.Id > 0 AndAlso .Id = Heber.Id Then
                        ' Bohle aktualisieren
                        Heber.Set_Name(.Nachname, .Vorname)
                        Heber.Flagge = tmp(0)!state_flag.ToString
                        Heber.Verein = tmp(0)!Vereinsname.ToString
                        Heber.Sex = .Geschlecht
                        If Not IsNothing(AKs) Then Heber.AK = AKs.Value
                    End If
                    ' Publikum aktualisieren
                    Dim pos = bsResults.Find("Teilnehmer", .Id)
                    Leader.Change_MasterData(pos, .Nachname, .Vorname, .Geschlecht, tmp(0)!Vereinsname.ToString, .Jahrgang, AKs.Value)
                End With
            End If

            If Not IsNothing(idHeber) Then
                tmp = f.Get_VereinInfo(idHeber.ToString)
                With Heber
                    If .Id > 0 AndAlso idHeber = .Id Then
                        .Verein = tmp(0)!Vereinsname.ToString
                    End If
                    For pos = 0 To dvResults.Count - 1
                        Try
                            If CInt(dvResults(pos)!idVerein) = idVerein Then
                                Leader.Change_MasterData(pos, .Nachname, .Vorname, .Sex, .Verein, .JG, .AK)
                            End If
                        Catch ex As Exception
                        End Try
                    Next
                End With
            End If
        End Using
    End Sub

    '' ContextMenu
    Private Sub mnuHighLightAttempts_Click(sender As Object, e As EventArgs) Handles mnuHighLightAttempts.Click
        Dim tmp = New DataView(dvLeader(Leader.TableIx).ToTable, "Teilnehmer = " & dvLeader(Leader.TableIx)(bsLeader.Position)("Teilnehmer").ToString, "Reihenfolge", DataViewRowState.CurrentRows)
        dgvLeader.MultiSelect = tmp.Count > 1
        For Each row As DataRowView In tmp
            Try
                dgvLeader.Rows(CInt(row!Reihenfolge) - 1 + Leader.RowNum + Leader.RowNum_Offset).Selected = True
            Catch ex As Exception
            End Try
        Next
    End Sub
    Private Sub mnuManuallyCall_Click(sender As Object, e As EventArgs) Handles mnuPanik.Click
        ' Panik-Modus: 
        '		der ausgewählte Versuch wird aufgerufen (SendToBohle) -–> Zeile rot markieren
        '		Flag setzen, um alle Überprüfungen beim Steigern zu ignorieren und ggf. HLast in Bohle ändern
        '       Durchgang_beendet speichert diesen Versuch ganz normal 

        ' während der Hebung
        '		alle vorhergehenden Heber   - entweder      steigern --> sie rutschen dadurch hinter den manuell aufgerufenen Versuch
        '                                   - oder          Korrektur-Werten, wenn Versuch schon absolviert ist

        ' Falls die Zeit einer Hebung nicht ausreicht, um alle Versuche vor dem manuell aufgerufenen wegzubekommen,
        ' dann so oft wiederholen, bis mit btnNext der korrekte Heber aufgerufen werden kann.

    End Sub

    '' Events der Anzeige
    Private Sub Wertung_Clear(HeberPresent As Boolean)
        Try
            If InvokeRequired Then
                Dim d As New DelegateBoolean(AddressOf Wertung_Clear)
                Invoke(d, New Object() {HeberPresent})
            Else
                If Not HeberPresent Then ' Wertungen zurücksetzen
                    For i = 1 To 3
                        dicLampen(i).ForeColor = Ansicht_Options.Shade_Color
                    Next
                    xTechniknote.ForeColor = Ansicht_Options.Shade_Color
                    xTechniknote.Text = "X,XX"
                    If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted AndAlso VE_verbunden Then Client_Send("L=0") ' WertungLöschen an Client
                End If
            End If
        Catch es As Exception
        End Try
    End Sub
    Private Sub CountDown_Changed(Value As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateString(AddressOf CountDown_Changed)
                Invoke(d, New Object() {Value})
            Else
                If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted AndAlso VE_verbunden Then Client_Send("ZA=" & Value) ' CountDown an Client
                xAufruf.Text = Value
            End If
        Catch
        End Try
    End Sub
    Private Function Get_ClockColor(Optional color As Color = Nothing) As Integer
        If color.Equals(Color.Black) Then color = Anzeige.ClockColor
        Select Case color
            Case Anzeige.Clock_Colors(Clock_Status.Started)
                Return 1
            Case Anzeige.Clock_Colors(Clock_Status.Stopped)
                Return 2
            Case Else
                Return 0
        End Select
    End Function
    Private Sub ClockColor_Changed(Color As Color)
        If InvokeRequired Then
            Dim d As New DelegateColor(AddressOf ClockColor_Changed)
            Invoke(d, New Object() {Color})
        Else
            xAufruf.ForeColor = Color
            If User.UserId = UserID.Wettkampfleiter Then
                If TcpIpStarted Then Client_Send("ZC=" & Get_ClockColor(Color)) ' UhrFarbe an Client
                Select Case Color
                    Case Anzeige.Clock_Colors(Clock_Status.Started)
                        btnAufruf.Text = "Aufruf stoppen"
                    Case Else
                        btnAufruf.Text = "Aufruf starten"
                End Select
            End If
        End If
    End Sub
    Private Sub Change_Pause(IsPause As Boolean)
        Try
            If InvokeRequired Then
                Dim d As New DelegateBoolean(AddressOf Change_Pause)
                Invoke(d, New Object() {IsPause})
            Else
                pnlWertung.Visible = Not IsPause
                xAufruf.BringToFront()
                pnlWertung.BringToFront()
                'If Not IsPause Then
                '    'btnNext.Enabled = CBool(btnNext.Tag)
                '    'btnAufruf.Enabled = Not ZN_Pad.Connected AndAlso Not btnNext.Enabled
                'End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub TechnikWert_Changed(Wert As String)
        If InvokeRequired Then
            Dim d As New DelegateString(AddressOf TechnikWert_Changed)
            Invoke(d, New Object() {Wert})
        Else
            If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then Client_Send("T=" & Wert) ' Technikwert an Versuchsermittler
            xTechniknote.Text = Wert
        End If
    End Sub
    Private Sub TechnikFarbe_Changed(Farbe As Color)
        If InvokeRequired Then
            Dim d As New DelegateColor(AddressOf TechnikFarbe_Changed)
            Invoke(d, New Object() {Farbe})
        Else
            If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then Client_Send("C=" & Abs(CInt(Farbe = Anzeige.TechnikColors(1))).ToString) ' Technikfarbe an Versuchsermittler (1 = aktiv, 0 = abgeblendet)
            xTechniknote.ForeColor = Farbe
        End If
    End Sub
    Private Sub TechnikVisible_Changed(Value As Boolean)
        If InvokeRequired Then
            Dim d As New DelegateBoolean(AddressOf TechnikVisible_Changed)
            Invoke(d, New Object() {Value})
        Else
            xTechniknote.Visible = Value
        End If
    End Sub
    Private Sub Technik_Changed(Technikwertung As Boolean)
        With pnlWertung
            If Technikwertung Then
                .Width = .MaximumSize.Width
                .Location = New Point(xAufruf.Left - .MaximumSize.Width + 100, xAufruf.Top)
            Else
                .Width = .MinimumSize.Width
                .Location = New Point(xAufruf.Left - .MinimumSize.Width + 100, xAufruf.Top)
            End If
            .BringToFront()
        End With
        With dgvTeam
            .Location = New Point(pnlWertung.Left - .Width - 10, xAufruf.Top)
            '.Visible = Wettkampf.Mannschaft AndAlso WindowState = FormWindowState.Maximized
            '.Visible = Wettkampf.Mannschaft AndAlso .Left > pnlButtons.Left + pnlButtons.Width
            .Visible = Wettkampf.Mannschaft AndAlso .Left > btnMinute2.Left + btnMinute2.Width + 10
            .BringToFront()
        End With
    End Sub

    Private Sub Techniknote_Jury(Index As Integer, Note As String)
        If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then Client_Send("TN=" & Index.ToString & ":" & Note)
    End Sub
    Private Sub OutOfRange_Jury(R1 As Boolean, R2 As Boolean, R3 As Boolean)
        If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then Client_Send("OOR=" & CInt(R1).ToString & ":" & CInt(R2).ToString & ":" & CInt(R3).ToString)
    End Sub
    'Private Sub AbZeichen_Jury(Wertung As Integer)
    '    If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then Client_Send("Ab=" & Wertung.ToString)
    'End Sub

    'Private Sub Countdown_Stoppen(state As Boolean)
    '    If InvokeRequired Then
    '        Dim d As New DelegateBoolean(AddressOf Countdown_Stoppen)
    '        Invoke(d, New Object() {state})
    '    Else
    '        btnAufruf.Text = "Aufruf " + If(state, "starten", "stoppen")
    '    End If
    'End Sub
    Private Sub Set_Wertung(Index As Integer, Wertung As Integer)
        Try
            If InvokeRequired Then
                Dim d As New DelegateIntegerInteger(AddressOf Set_Wertung)
                Invoke(d, New Object() {Index, Wertung})
            Else
                'dicLampen(Index).ForeColor = Anzeige.ScoreColors(Wertung)
                'If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then Client_Send("W" & Index & "=" & Wertung) ' Wertung an Client
                If Wertung > 0 AndAlso bsLeader.Position = Leader.RowNum + Leader.RowNum_Offset Then
                    btnKorrektur.Enabled = False
                    btnSteigerung.Enabled = False
                    btnVerzicht.Enabled = False
                End If
            End If
        Catch es As Exception
        End Try
    End Sub
    Private Sub Change_Lampe(Index As Integer, Color As Color)
        Try
            If InvokeRequired Then
                Dim d As New DelegateIntegerColor(AddressOf Change_Lampe)
                Invoke(d, New Object() {Index, Color})
            Else
                Dim Wertung = If(Color.Equals(Anzeige.ScoreColors(5)), 5, 7)

                dicLampen(Index).ForeColor = Color
                dicLampen(Index).Refresh()

                If pnlManuell.Visible Then
                    ' könnte auch immer gesetzt werden, um beim Ausfall der KRs up to date zu sein 
                    ' --> Visible müsste bei KR_Steuerung.HandleRemoval ausgelöst werden
                    dicWertung(Wertung)(Index).Checked = True
                    dicWertung(If(Wertung = 5, 7, 5))(Index).Checked = False
                End If

                If User.UserId = UserID.Wettkampfleiter AndAlso TcpIpStarted Then
                    For Each c As TCPConnection In TCPConnections
                        If c.NickName.Equals("Jury") Then
                            Client_Send("W" & Index & "=" & Wertung, c) ' Wertung an Jury-Client
                            Exit For
                        End If
                    Next
                End If
            End If
        Catch es As Exception
        End Try
    End Sub

    '' Panels - MOVE
    Private Sub Panel_Headers_MouseDown(sender As Object, e As MouseEventArgs) Handles lblNotizen.MouseDown, lblManuell.MouseDown, lblSteigerung.MouseDown
        If e.Button = MouseButtons.Left Then
            Dim ctl = CType(sender, Label)
            Dim pnl As String = Mid(ctl.Name, 4)
            Panels(pnl).BorderStyle = BorderStyle.None
            pnlPos = New Point(e.X, e.Y)
            ctl.Cursor = Cursors.SizeAll
        End If
    End Sub
    Private Sub Panel_Headers_MouseMove(sender As Object, e As MouseEventArgs) Handles lblNotizen.MouseMove, lblManuell.MouseMove, lblSteigerung.MouseMove
        Dim pnl As String = Mid(CType(sender, Label).Name, 4)

        If (e.Button And MouseButtons.Left) = MouseButtons.Left And Panels(pnl).BorderStyle = BorderStyle.None Then

            Dim loc = New Point(Panels(pnl).Location.X + (e.X - pnlPos.X), Panels(pnl).Location.Y + (e.Y - pnlPos.Y))

            If loc.X < 0 Then loc.X = 0
            If loc.X + Panels(pnl).Width > Width - 18 Then loc.X = Width - 18 - Panels(pnl).Width
            If loc.Y < 0 Then loc.Y = 0
            If loc.Y + Panels(pnl).Height > dgvLeader.Height Then loc.Y = dgvLeader.Height - Panels(pnl).Height
            Panels(pnl).Location = loc
        End If
    End Sub
    Private Sub Panel_Headers_MouseUp(sender As Object, e As MouseEventArgs) Handles lblNotizen.MouseUp, lblManuell.MouseUp, lblSteigerung.MouseUp
        Dim ctl = CType(sender, Label)
        Dim pnl As String = Mid(ctl.Name, 4)
        If e.Button = MouseButtons.Left And Panels(pnl).BorderStyle = BorderStyle.None Then
            Panels(pnl).BorderStyle = BorderStyle.FixedSingle
            PanelsLocation(pnl) = New Point(Panels(pnl).Left, Panels(pnl).Top + If(pnl = "Korrektur", Panels(pnl).Height, 0))
            ctl.Cursor = Cursors.Default
        End If
    End Sub

    '' pnlNotizen
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnNotizExit.Click
        pnlNotizen.Visible = False
    End Sub
    Private Sub btnColor_Click(sender As Object, e As EventArgs) Handles btnNotizForeColor.Click
        If ColorDialog1.ShowDialog() = DialogResult.OK Then
            btnNotizForeColor.BackColor = ColorDialog1.Color
            rtfNotiz.SelectionColor = btnNotizForeColor.BackColor
        End If
        rtfNotiz.Focus()
    End Sub
    Private Sub chkFontStyle_Click(sender As Object, e As EventArgs) Handles chkNotizBold.Click, chkNotizItalic.Click, chkNotizUnderline.Click
        Dim chk = CType(sender, CheckBox)
        chk.Checked = Not chk.Checked
        Dim value As Integer = 0 'Regular
        If chkNotizBold.Checked Then value += 1
        If chkNotizItalic.Checked Then value += 2
        If chkNotizUnderline.Checked Then value += 4
        Dim fntStyle = New FontStyle With {.value__ = value}
        With rtfNotiz
            .SelectionFont = New Font(.SelectionFont.Name, .SelectionFont.Size, fntStyle)
            .Focus()
        End With
    End Sub
    Private Sub rtfNotiz_SelectionChanged(sender As Object, e As EventArgs) Handles rtfNotiz.SelectionChanged
        Select Case rtfNotiz.SelectionFont.Style
            Case FontStyle.Regular
                chkNotizBold.Checked = False
                chkNotizItalic.Checked = False
                chkNotizUnderline.Checked = False
            Case FontStyle.Bold
                chkNotizBold.Checked = True
                chkNotizItalic.Checked = False
                chkNotizUnderline.Checked = False
            Case FontStyle.Italic
                chkNotizItalic.Checked = True
                chkNotizBold.Checked = False
                chkNotizUnderline.Checked = False
            Case FontStyle.Underline
                chkNotizUnderline.Checked = True
                chkNotizBold.Checked = False
                chkNotizItalic.Checked = False
            Case FontStyle.Bold Or FontStyle.Italic
                chkNotizBold.Checked = True
                chkNotizItalic.Checked = True
                chkNotizUnderline.Checked = False
            Case FontStyle.Bold Or FontStyle.Underline
                chkNotizBold.Checked = True
                chkNotizUnderline.Checked = True
                chkNotizItalic.Checked = False
            Case FontStyle.Italic Or FontStyle.Underline
                chkNotizItalic.Checked = True
                chkNotizUnderline.Checked = True
                chkNotizBold.Checked = False
            Case FontStyle.Bold Or FontStyle.Italic Or FontStyle.Underline
                chkNotizBold.Checked = True
                chkNotizItalic.Checked = True
                chkNotizUnderline.Checked = True
        End Select
        btnNotizForeColor.BackColor = rtfNotiz.SelectionColor
    End Sub
    Private Sub btnDelete_Notiz_Click(sender As Object, e As EventArgs) Handles btnDelete_Notiz.Click
        With rtfNotiz
            .Tag = .Rtf
            .Clear()
            .Focus()
        End With
        btnSave_Notiz.Enabled = True
        btnDelete_Notiz.Enabled = False
        btnUndo_Notiz.Enabled = True
    End Sub
    Private Sub btnUndo_Notiz_Click(sender As Object, e As EventArgs) Handles btnUndo_Notiz.Click
        With rtfNotiz
            .Rtf = .Tag.ToString
            .Focus()
        End With
        btnSave_Notiz.Enabled = True
        btnUndo_Notiz.Enabled = False
        btnDelete_Notiz.Enabled = True
    End Sub
    Private Sub btnSave_Notiz_Click(sender As Object, e As EventArgs) Handles btnSave_Notiz.Click

        Cursor = Cursors.WaitCursor

        Dim TN = CInt(dvLeader(Leader.TableIx)(bsLeader.Position)!Teilnehmer)
        Dim pos = bsNotizen.Find("Teilnehmer", TN)

        Using ms = New MemoryStream()
            rtfNotiz.SaveFile(ms, RichTextBoxStreamType.RichText)
            Dim note = ms.ToArray
            dvNotizen(pos)!Notiz = note
        End Using

        bsNotizen.EndEdit()

        If Heber.Id = TN OrElse TN = CInt(dvLeader(Leader.TableIx)(Leader.RowNum)!Teilnehmer) Then Leader.Set_Notiz(TN)

        Dim InsertQuery = "INSERT INTO notizen (Heber, Notiz) VALUES (@id, @note) ON DUPLICATE KEY UPDATE Notiz = @note;"
        Dim DeleteQuery = "DELETE FROM notizen WHERE Heber = @id;"


        Dim changes As DataTable = Glob.dtNotizen.GetChanges
        If Not IsNothing(changes) Then
            Using conn As New MySqlConnection(User.ConnString)
                For Each row As DataRow In changes.Rows
                    Dim cmd As New MySqlCommand(InsertQuery, conn)
                    With cmd.Parameters
                        .AddWithValue("@id", row!Teilnehmer)
                        .AddWithValue("@note", row!Notiz)
                    End With
                    Do
                        Try
                            If conn.State = ConnectionState.Closed Then conn.Open()
                            cmd.ExecuteNonQuery()
                            btnSave_Notiz.Enabled = False
                        Catch ex As Exception
                            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                        End Try
                    Loop While ConnError
                Next
            End Using
        End If
        Cursor = Cursors.Default
    End Sub
    Private Sub rtfNotiz_LostFocus(sender As Object, e As EventArgs) Handles rtfNotiz.LostFocus
        rtfNotiz.Tag = rtfNotiz.Rtf
    End Sub
    Private Sub rtfNotiz_TextChanged(sender As Object, e As EventArgs) Handles rtfNotiz.TextChanged
        If rtfNotiz.Focused Then btnSave_Notiz.Enabled = True
        btnDelete_Notiz.Enabled = True
        btnUndo_Notiz.Enabled = True
    End Sub
    Private Sub pnlNotizen_VisibleChanged(sender As Object, e As EventArgs) Handles pnlNotizen.VisibleChanged
        If loading Then Return
        If pnlNotizen.Visible Then
            rtfNotiz.Tag = rtfNotiz.Rtf
            pnlNotizen.BringToFront()
        End If
    End Sub
    Private Sub Set_pnlNotizen_Location(rect As Rectangle)
        rect.X += rect.Width
        If WindowState = FormWindowState.Maximized Then
            rect.Y -= Top
        Else
            rect.Y += dgvLeader.Top
        End If
        With pnlNotizen
            If rect.X + .Width > ClientSize.Width Then rect.X = rect.X - rect.Width - .Width
            If rect.Y + .Height > ClientSize.Height Then rect.Y = rect.Y + rect.Height - .Height
            .Location = New Point(rect.X, rect.Y)
            .BringToFront()
        End With
    End Sub

    '' pnlManuell
    Private Sub pnlManuell_VisibleChanged(sender As Object, e As EventArgs) Handles pnlManuell.VisibleChanged
        Try
            If InvokeRequired Then
                Dim d As New DelegateSubArgs(AddressOf pnlManuell_VisibleChanged)
                Invoke(d, New Object() {sender, e})
            Else
                With pnlManuell
                    If .Visible Then
                        '.Top = pnlDataGrid.Top + pnlDataGrid.Height - .Height
                        '.Left = pnlDataGrid.Left + (pnlDataGrid.Width - .Width) \ 2
                        .Controls.Item("pnlT").Visible = Wettkampf.Technikwertung

                        pnlW1.Width = If(Wettkampf.KR(User.Bohle) = 1, pnlW1.MaximumSize.Width, pnlW1.MinimumSize.Width)

                        If .Height = .MaximumSize.Height Then Return ' ---------------- KR-Pad angeschlossen --> Wertung überschreiben

                        lblManuell.Text = Heber.Nachname + ", " + Heber.Vorname
                        For i = 1 To 3
                            dicWertung(5)(i).Checked = False
                            dicWertung(7)(i).Checked = False
                            loading = True
                            dicTechnik(i).Enabled = False
                            dicTechnik(i).Text = String.Empty
                            dicTechnik(i).Tag = Nothing
                            T_Verified(i) = False
                            loading = False
                            dicLampen(i).ForeColor = Ansicht_Options.Shade_Color
                        Next
                        xTechniknote.ForeColor = Ansicht_Options.Shade_Color
                        xTechniknote.Text = "X,XX"
                        Dummy.Focus()
                        .BringToFront()
                    Else
                        .Height = .MinimumSize.Height
                        btnExitManuell.Visible = False
                        lblManuell.Padding = New Padding(10, 0, 0, 0)
                    End If
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnG_CheckedChanged(sender As Object, e As EventArgs) Handles btnG1.CheckedChanged, btnG2.CheckedChanged, btnG3.CheckedChanged
        Dim ctl = CType(sender, RadioButton)
        'ctl.FlatAppearance.MouseOverBackColor = If(ctl.Checked, Color.White, Color.Gainsboro)
        ctl.BackColor = If(ctl.Checked, Color.White, Color.LightGray)
    End Sub
    Private Sub btnU_CheckedChanged(sender As Object, e As EventArgs) Handles btnU1.CheckedChanged, btnU2.CheckedChanged, btnU3.CheckedChanged
        Dim ctl = CType(sender, RadioButton)
        'ctl.FlatAppearance.MouseOverBackColor = If(ctl.Checked, Color.Red, Color.FromArgb(190, 130, 130))
        ctl.BackColor = If(ctl.Checked, Color.Red, Color.FromArgb(170, 120, 120)) '(190, 130, 130))
    End Sub
    Private Sub btnG_Click(sender As Object, e As EventArgs) Handles btnG1.Click, btnG2.Click, btnG3.Click

        If Not Leader.ManuelleWertung AndAlso pnlManuell.Height = pnlManuell.MinimumSize.Height Then Return

        Dim ix = CInt(Strings.Right(CType(sender, RadioButton).Name, 1))

        If Wettkampf.Technikwertung Then ' Technikeingabe freischalten
            dicTechnik(ix).Enabled = True
            If ix = 3 OrElse mnuTechniknote.Checked Then
                For i = 1 To 3
                    If dicTechnik(i).Enabled Then
                        dicTechnik(i).Focus()
                        Exit For
                    End If
                Next
            End If
        End If

        If Not pnlManuell.Height = pnlManuell.MaximumSize.Height Then WA_Events.Push_KR(ix, "G")
    End Sub
    Private Sub btnU_Click(sender As Object, e As EventArgs) Handles btnU1.Click, btnU2.Click, btnU3.Click

        If Not Leader.ManuelleWertung AndAlso pnlManuell.Height = pnlManuell.MinimumSize.Height Then Return

        Dim ix = CInt(Strings.Right(CType(sender, RadioButton).Name, 1))

        If Wettkampf.Technikwertung Then ' Technikeingabe sperren
            dicTechnik(ix).Text = String.Empty
            dicTechnik(ix).Enabled = False
        End If

        If Not pnlManuell.Height = pnlManuell.MaximumSize.Height Then WA_Events.Push_KR(ix, "U")
    End Sub

    Private Sub txtTechniknote_GotFocus(sender As Object, e As EventArgs) Handles txtT1.GotFocus, txtT2.GotFocus, txtT3.GotFocus
        With CType(sender, TextBox)
            .Tag = .Text
        End With
    End Sub
    Private Sub txtTechniknote_Validating(Index As Integer)
        Stop
        ' Was macht dieses Ding ?????????????????????
        If Wettkampf.KR(User.Bohle) = 3 AndAlso mnuTechniknote.Checked AndAlso (Index = 1 AndAlso txtT1.Text <> String.Empty) Then
            For i = 1 To 3
                WA_Events.Push_KR(i, dicTechnik(1).Text)
                WA_Events.Push_KR(i, vbLf)
            Next
        ElseIf dicTechnik(Index).Text <> String.Empty Then
            WA_Events.Push_KR(Index, vbLf)
            T_Verified(Index) = True
        ElseIf dicTechnik(Index).Text = String.Empty Then
            WA_Events.Push_KR(Index, vbCr)
            T_Verified(Index) = False
        End If
    End Sub
    Private Sub txtTechniknote_TextChanged(sender As Object, e As EventArgs) Handles txtT1.TextChanged, txtT2.TextChanged, txtT3.TextChanged
        If loading Then Return
        loading = True
        With CType(sender, TextBox)
            Dim Ix = CInt(Strings.Right(.Name, 1))
            If .Text.Length = 1 Then
                KR_Pad(Ix).Techniknote = "0" + .Text
            ElseIf .Text.Length = 2 Then
                If IsNumeric(.Text.Substring(1, 1)) OrElse .Text.Substring(1, 1).Equals(",") Then
                    ' zweistellige Zahl
                    Dim decTest = True
                    If Not .Text.Substring(0, 2).Equals("10") Then
                        ' zwei Zahlen außer 10 --> Komma einfügen
                        If Not .Text.Contains(",") Then .Text = .Text.Insert(1, ",")
                        If .Text.Length > 3 Then .Text = .Text.Substring(0, 3)
                        decTest = DecimalTest(.Text.Substring(2))
                        If Not decTest Then .Text = .Text.Substring(0, 1)
                    End If
                    If decTest Then KR_Pad(Ix).Techniknote = "0" + .Text
                End If
            ElseIf Not .Text.Contains(",") Then
                .Text = .Text.Substring(0, 2)
            Else
                .Text = .Text.Substring(0, 3)
            End If
            .SelectionStart = .TextLength
        End With
        loading = False
    End Sub

    Private Function DecimalTest(NK As String) As Boolean
        If WK_Options.Decimals = 1 Then ' 0,5
            Return CInt(NK) Mod 5 = 0
        Else
            Return True
        End If
    End Function

    Private Sub pnlT_VisibleChanged(sender As Object, e As EventArgs) Handles pnlT.VisibleChanged
        If pnlT.Visible Then
            pnlManuell.Width = pnlManuell.MaximumSize.Width
        Else
            pnlManuell.Width = pnlManuell.MinimumSize.Width
        End If
    End Sub

    ' Manuelle Korrektur der Wertung
    Private Sub btnExitManuell_Click(sender As Object, e As EventArgs) Handles btnExitManuell.Click
        pnlManuell.Visible = False
    End Sub
    Private Sub btnFertig_Click(sender As Object, e As EventArgs) Handles btnFertig.Click
        loading = True
        Dim Wertung As Integer
        Dim Note As Double = 0
        Dim Result = "0"
        Dim Divider = 0
        Dim Lampen = String.Empty
        For i = 1 To Wettkampf.KR(User.Bohle)
            If KR_Pad.ContainsKey(i) Then
                If i = 1 Then
                    KR_Pad(i).Wertung = If(btnG1.Checked, 5, 7)
                    KR_Pad(i).Techniknote = If(btnG1.Checked, txtT1.Text, "0")
                    KR_Pad(i).Verified = txtT1.Enabled AndAlso IsNumeric(txtT1.Text)
                    Divider += If(btnG1.Checked, 1, 0)
                ElseIf i = 2 Then
                    KR_Pad(i).Wertung = If(btnG2.Checked, 5, 7)
                    KR_Pad(i).Techniknote = If(btnG2.Checked, txtT2.Text, "0")
                    KR_Pad(i).Verified = txtT2.Enabled AndAlso IsNumeric(txtT2.Text)
                    Divider += If(btnG2.Checked, 1, 0)
                ElseIf i = 3 Then
                    KR_Pad(i).Wertung = If(btnG3.Checked, 5, 7)
                    KR_Pad(i).Techniknote = If(btnG3.Checked, txtT3.Text, "0")
                    KR_Pad(i).Verified = txtT3.Enabled AndAlso IsNumeric(txtT3.Text)
                    Divider += If(btnG3.Checked, 1, 0)
                End If
                KR_Pad(i).Verified = True ' das sollte anhand der Eingabe im Textfeld bzw. Sichtbarkeit des Textfeldes geprüft werden
                If Wettkampf.Technikwertung AndAlso Not String.IsNullOrEmpty(KR_Pad(i).Techniknote) Then Note += CDbl(KR_Pad(i).Techniknote)
                Lampen += KR_Pad(i).Wertung.ToString
                Wertung += KR_Pad(i).Wertung
                Anzeige.Change_Lampe(i, Anzeige.ScoreColors(KR_Pad(i).Wertung))
                'If KR_Pad(i).Wertung = 5 Then Anzeige.Change_Gültig(i, Color.White)
                'If KR_Pad(i).Wertung = 7 Then Anzeige.Change_Ungültig(i, Color.Red)
            End If
        Next
        loading = False
        If Wettkampf.Technikwertung Then
            If Note > 0 Then
                Dim NK_Stellen = Get_NK_Stellen(Heber.Auswertung)
                Dim rList = Split(CStr(Note / Divider), ",").ToList     ' Ergebnis in Vor- und Nachkomma-Teil splitten
                If rList.Count = 1 Then rList.Add("00")                 ' falls Ganzzahl, 2 Nullen als Nachkomma-Teil
                rList(1) = rList(1).Substring(0, 3 - rList(0).Length)   ' Nachkomma-Teil auf 2 bzw 1 (bei Note 10) Stellen abschneiden
                Result = String.Concat(rList(0), ",", rList(1))         ' Note zusammensetzen
                Anzeige.T_Wert = Result
            Else
                Anzeige.T_Wert = "X.XX"
            End If
            Anzeige.T_Farbe = If(Note > 0, Anzeige.TechnikColors(1), Anzeige.TechnikColors(0))
        End If

        Dim _wert As Integer = 0
        Select Case Wertung
            Case 5, 15, 17
                _wert = 1
            Case 7, 19, 21
                _wert = -1
        End Select
        Leader.RowNum = 0
        Versuch_beendet(_wert, Now, CDbl(Result), Lampen)
        pnlManuell.Visible = False
    End Sub
    Private Sub btnManuell_EnabledChanged(sender As Object, e As EventArgs)
        'mnuManuell.Enabled = btnManuell.Enabled
    End Sub

    '' pnlSteigern
    Private Sub btnExitSteigerung_Click(sender As Object, e As EventArgs) Handles btnExitSteigerung.Click
        pnlSteigerung.Visible = False
    End Sub
    Private Sub btnSteigernOK_Click(sender As Object, e As EventArgs) Handles btnSteigernOK.Click
        HantelLast_Validate()
    End Sub
    Private Sub nudHantellast_KeyDown(sender As Object, e As KeyEventArgs) Handles nudHantellast.KeyDown
        If e.KeyCode = Keys.Enter Then
            HantelLast_Validate()
            e.SuppressKeyPress = True
            e.Handled = True
        End If
    End Sub
    Private Function HantelLast_ExceedsMax(Versuch As Integer, HLast As Integer) As Boolean
        Dim maxValue = HLast
        Try
            maxValue += Wettkampf.Steigerung(Versuch - 1)
            maxValue += Wettkampf.Steigerung(Versuch)
        Catch ex As Exception
            If Versuch = 3 Then maxValue += Wettkampf.Steigerung(1)
        End Try
        If maxValue > nudHantellast.Maximum Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Hantel-Last kann nicht verarbeitet werden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            Return True
        End If
        Return False
    End Function
    Private Sub HantelLast_Validate(Optional row As DataRow() = Nothing, Optional addition As Integer? = Nothing)
        Dim msg = String.Empty
        Dim Ignore_Validation = False

#Region "Committed Data"
        If IsNothing(row) Then
            ' von pnlSteigerung
            row = TryCast(pnlSteigerung.Tag, DataRow())
        End If
        If IsNothing(addition) Then
            ' von pnlSteigerung
            addition = CInt(nudHantellast.Value) - CInt(row(0)!HLast)
        Else
            ' von Up/Down
            If HantelLast_ExceedsMax(CInt(row(0)!Versuch), CInt(row(0)!HLast)) Then Return
            nudHantellast.Value = CInt(row(0)!HLast) + CInt(addition)
        End If
#End Region

#Region "Attempt started"
        If CInt(row(0)!Teilnehmer) = Heber.Id AndAlso Anzeige.ClockColor <> Anzeige.Clock_Colors(Clock_Status.Waiting) AndAlso Heber.Hantellast > CInt(nudHantellast.Value) Then
            msg = "Das Hantelgewicht muss mindestens " & Heber.Hantellast & " kg betragen." & vbNewLine & "(Reduzierung nicht möglich: Countdown gestartet)"
            Using New Centered_MessageBox(Me, {"Ignorieren"})
                If MessageBox.Show(msg, msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then Return
            End Using
            Ignore_Validation = True
        End If
#End Region

        Dim MindestLast = Wettkampf.MinLast(row(0)!sex.ToString, Leader.TableIx)

        If MindestLast = 0 Then
            Select Case Hantel.Get_Stange(row(0)!sex.ToString, CInt(row(0)!Jahrgang), CInt(row(0)!HLast))
                Case "m"
                    MindestLast = 20
                Case "w"
                    MindestLast = 15
                Case "k"
                    MindestLast = 7
                Case "x"
                    MindestLast = 5
            End Select
        End If

        If Not Panik AndAlso mnuEingabePrüfen.Checked Then
#Region "Decreasement"
            If addition < 0 Then
                ' negative Steigerung

                If Not Ignore_Validation Then

                    ' Prüfung auf kleiner als Mindesgewicht
                    If CInt(row(0)!HLast) + addition < MindestLast Then
                        msg = "Das Hantelgewicht muss mindestens " & MindestLast & " kg betragen." & vbNewLine & "(Mindestlast oder letzte gehobene Last unterschritten)"
                    Else
                        ' Prüfung auf kleiner als beim vorhergehenden Versuch des Hebers
                        If CInt(row(0)!Versuch) > 1 Then ' beim ersten Versuch gibt es kein Diff
                            If CInt(row(0)!Diff) + addition < 0 Then
                                msg = "Das gewünschte Hantelgewicht ist kleiner" & vbNewLine & "als beim vorherigen Versuch (war " & CStr(CInt(row(0)!HLast) - CInt(row(0)!Diff)) & " kg)."
                            ElseIf Wettkampf.IncreaseAfterNoLift AndAlso CInt(row(0)!Diff) + addition = 0 Then
                                ' wenn nach ungültig nicht gesteigert werden darf (IncreaseAfterNoLift=False), ist das AK Schüler 
                                ' --> dann darf auch bei gültig die Last wiederholt werden
                                msg = "Das gewünschte Hantelgewicht ist dasselbe" & vbNewLine & "wie beim vorherigen Versuch."
                            End If
                        End If
                    End If

                    If Not msg.Equals(String.Empty) Then
                        Using New Centered_MessageBox(Me, {"Ignorieren"})
                            If MessageBox.Show(msg, msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then Return
                        End Using
                        Ignore_Validation = True
                    End If

                    If Not Ignore_Validation Then
                        Dim minLast As Integer
                        Try
                            ' versuche, MinLast auf letzte gehobene & nicht gespeicherte (RowNum=0) Last setzen
                            minLast = (From x In dvLeader(Leader.TableIx).ToTable
                                       Where x.Field(Of Integer)("Reihenfolge") = 0 AndAlso Not IsNothing(x.Field(Of Integer)("Wertung"))
                                       Select x.Field(Of Integer)("HLast")).Max
                        Catch ex As Exception
                            ' MinLast auf letzte gespeicherte Last setzen
                            Try
                                If Not chkBH.Checked Then
                                    minLast = (From x In dtDelete(Leader.TableIx)
                                               Select x.Field(Of Integer)("HLast")).Max
                                Else
                                    minLast = (From x In dtDelete(Leader.TableIx)
                                               Where x.Field(Of Integer)("Versuch") = CInt(row(0)!Versuch)
                                               Select x.Field(Of Integer)("HLast")).Max
                                End If
                            Catch
                            End Try
                        End Try
                        If minLast > MindestLast Then MindestLast = minLast

                        ' der steigernde Heber
                        Dim IncreaseHeber = row(0) 'dvLeader(Leader.TableIx)(bsLeader.Position)

                        ' der vorherige Heber
                        Dim PrevHeber As DataRow = Nothing
                        Dim alle_gewerteten_Versuche = New DataView(dvDelete(Leader.TableIx).ToTable) With {.RowFilter = "Wertung > -2"} ' RowCount-1 = ist PrevHeber

                        If Leader.RowNum = 1 Then
                            PrevHeber = dvLeader(Leader.TableIx)(0).Row
                        ElseIf alle_gewerteten_Versuche.Count > 0 Then
                            PrevHeber = alle_gewerteten_Versuche(alle_gewerteten_Versuche.Count - 1).Row
                        Else
                            ' keine gewerteten Versuche vorhanden --> beliebige Steigerung möglich
                            Write_Steigerung(CInt(addition), row)
                            pnlSteigerung.Visible = False
                            Return
                        End If

                        If Not chkBH.Checked AndAlso CInt(PrevHeber!HLast) > 0 Then

                            ' normales Heben
                            If CInt(IncreaseHeber!HLast) + addition = CInt(PrevHeber!HLast) Then
                                If CInt(IncreaseHeber!Versuch) = CInt(PrevHeber!Versuch) Then
                                    If CInt(IncreaseHeber!Diff) = CInt(PrevHeber!Diff) Then
                                        If CInt(IncreaseHeber!Losnummer) < CInt(PrevHeber!Losnummer) Then
                                            msg = "Das Hantelgewicht muss mindestens " & MindestLast + 1 & " kg betragen." & vbNewLine & "(Sortierung: Reihenfolge der Losnummern)"
                                        End If
                                    ElseIf CInt(IncreaseHeber!Diff) > CInt(PrevHeber!Diff) Then
                                        msg = "Das Hantelgewicht muss mindestens " & MindestLast + 1 & " kg betragen." & vbNewLine & "(Sortierung: größere Differenz aus Vorversuch)"
                                    End If
                                ElseIf CInt(IncreaseHeber!Versuch) < CInt(PrevHeber!Versuch) Then
                                    msg = "Das Hantelgewicht muss mindestens " & MindestLast + 1 & " kg betragen." & vbNewLine & "(Sortierung: Reihenfolge der Versuche)"
                                End If
                            End If

                        ElseIf chkBH.Checked Then

                            ' Blockheben
                            If CInt(IncreaseHeber!Versuch) = CInt(PrevHeber!Versuch) Then
                                If CInt(IncreaseHeber!HLast) + addition = CInt(PrevHeber!HLast) Then
                                    If CInt(IncreaseHeber!Losnummer) < CInt(PrevHeber!Losnummer) Then
                                        msg = "Das Hantelgewicht muss mindestens " & MindestLast + 1 & " kg betragen." & vbNewLine & "(Sortierung: Reihenfolge der Losnummern)"
                                    End If
                                ElseIf CInt(IncreaseHeber!HLast) < CInt(PrevHeber!HLast) Then
                                    msg = "Das Hantelgewicht muss mindestens " & MindestLast + 1 & " kg betragen." & vbNewLine & "(Sortierung: gehobene Last)"
                                End If
                            ElseIf CInt(IncreaseHeber!Versuch) < CInt(PrevHeber!Versuch) Then
                                msg = "Das Hantelgewicht muss mindestens " & MindestLast + 1 & " kg betragen." & vbNewLine & "(letzte gehobene Last unterschritten)"
                            End If
                        End If

                        If Not msg = String.Empty Then
                            Using New Centered_MessageBox(Me, {"Ignorieren"})
                                If MessageBox.Show(msg, msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.Cancel Then Return
                            End Using
                        End If

                    End If
                End If
            End If
#End Region
        End If

        ' Steigerung durchführen
        Write_Steigerung(CInt(addition), row)
        pnlSteigerung.Visible = False

        'btnAufruf.Enabled = Not btnNext.Enabled AndAlso (IsNothing(ZN_Pad) OrElse (Not ZN_Pad.Connected AndAlso Not ZN_Pad.IsAttemptRated)) ' AndAlso Heber.Id > 0

    End Sub
    Private Sub pnlSteigerung_VisibleChanged(sender As Object, e As EventArgs) Handles pnlSteigerung.VisibleChanged
        If Not pnlSteigerung.Visible Then
            pnlSteigerung.Tag = Nothing
        Else
            pnlSteigerung.Location = PanelsLocation("Steigerung")
            pnlSteigerung.BringToFront()
            nudHantellast.Select(0, nudHantellast.Value.ToString.Length)
            nudHantellast.Focus()
        End If
        btnSteigerung.Enabled = Not pnlSteigerung.Visible AndAlso dgvLeader.Rows.Count > 0
    End Sub

    '' KR Initialisierung
    Private Sub mnuKR_Init_Click(sender As Object, e As EventArgs) Handles mnuKR_Init.Click
        Cursor = Cursors.WaitCursor
        'WA_Message.Update("Wertungsanlage initialisieren")
        Steuerung.KR_Steuerung_Initialize()
        Cursor = Cursors.Default
    End Sub

    '' MouseWheel ausschalten
    Dim CtlCaptured As Boolean
    Private Sub Prohibited_MouseWheel(sender As Object, e As MouseEventArgs) Handles cboDurchgang.MouseWheel, nudHantellast.MouseWheel
        If Not CType(sender, Control).Capture Then
            Dim HMEA As HandledMouseEventArgs = DirectCast(e, HandledMouseEventArgs)
            HMEA.Handled = True
        End If
    End Sub
    Private Sub Allowed_MouseWheel(sender As Object, e As MouseEventArgs)
        If Not CtlCaptured Then
            Dim HMEA As HandledMouseEventArgs = DirectCast(e, HandledMouseEventArgs)
            HMEA.Handled = True
        End If
    End Sub
    Private Sub Allowed_MouseEnter(sender As Object, e As EventArgs)
        CtlCaptured = True
    End Sub
    Private Sub Allowed_MouseLeave(sender As Object, e As EventArgs)
        CtlCaptured = False
    End Sub

    ' Gruppen-Auswahl
    Private Sub btnGruppe_Click(sender As Object, e As EventArgs) Handles btnGruppe.Click, lblGruppe.Click
        With dgvGruppe
            .Visible = Not .Visible
            .ClearSelection()
            If .Visible Then
                If Not IsNothing(.Tag) Then
                    Dim lst = CType(.Tag, List(Of Integer))
                    For Each item In lst
                        Dim pos = bsGruppen.Find("WG", item)
                        If pos > -1 Then .Rows(pos).Selected = True
                    Next
                    If .Rows.Count > 0 AndAlso .SelectedRows.Count = 0 Then .Rows(0).Selected = True
                ElseIf .Rows.Count > 0 Then
                    .Rows(0).Selected = True
                End If
                .BringToFront()
                .Focus()
            Else
                dgvLeader.Focus()
            End If
        End With
    End Sub
    Private Sub btnGruppe_EnabledChanged(sender As Object, e As EventArgs) Handles btnGruppe.EnabledChanged
        lblGruppe.Enabled = btnGruppe.Enabled
        If Not btnGruppe.Enabled Then dgvGruppe.Visible = False
    End Sub
    Private Sub lblGruppe_TextChanged(sender As Object, e As EventArgs) Handles lblGruppe.TextChanged
        cboDurchgang.Enabled = Not String.IsNullOrEmpty(lblGruppe.Text)
    End Sub
    Private Sub dgvGruppe_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvGruppe.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Change_Gruppe(Get_Gruppen(dgvGruppe.SelectedRows))
        End If
    End Sub
    Private Sub dgvGruppe_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGruppe.CellClick
        Change_Gruppe(Get_Gruppen(dgvGruppe.SelectedRows))
    End Sub
    Private Function Get_Gruppen(SelectedRows As DataGridViewSelectedRowCollection) As List(Of String)
        ' ReturnValue           = Liste der selektierten Gruppen-Nummern
        ' setzt dgvGruppen.Tag  = Liste der selektierten WG-Nummern

        Dim lstWG As New List(Of Integer)
        Dim lstGruppen As New List(Of String)

        For Each row As DataGridViewRow In SelectedRows
            lstWG.Add(CInt(row.Cells("WG").Value))
            lstGruppen.AddRange(Split(row.Cells("Gruppe").Value.ToString, ", ").ToList)
        Next

        dgvGruppe.Visible = False
        lstWG.Sort()
        dgvGruppe.Tag = lstWG
        lstGruppen.Sort()
        Return lstGruppen

    End Function

    ' Prognose
    Private Sub mnuPrognose_Click(sender As Object, e As EventArgs) Handles mnuPrognose.Click
        Cursor = Cursors.WaitCursor
        With frmPrognose
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub

    ' Wertung des Hebers anzeigen
    Private Sub dgvHeber_LostFocus(sender As Object, e As EventArgs) Handles dgvHeber.LostFocus
        dgvHeber.Visible = False
    End Sub
    Private Sub dgvHeber_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgvHeber.CellPainting
        Try
            Dim d = {"RW_", "SW_"}
            Dim ix = bsResults.Find("Teilnehmer", dvLeader(Leader.TableIx)(bsLeader.Position)!Teilnehmer)
            Dim row = dvResults(ix).Row
            If e.RowIndex > -1 AndAlso e.RowIndex < 2 Then
                If dgvHeber.Columns(e.ColumnIndex).Name.Contains("D") Then
                    e.Handled = True
                    Dim img As Image = Nothing
                    Dim col = Val(dgvHeber.Columns(e.ColumnIndex).Name(1))
                    If Not IsDBNull(row(d(e.RowIndex) & col)) Then
                        e.CellStyle.ForeColor = Color.White
                        Select Case CInt(row(d(e.RowIndex) & col))
                            Case 1
                                img = ImageList2.Images("SquareBlue.jpg")
                                e.CellStyle.Font = New Font(e.CellStyle.Font.FontFamily, e.CellStyle.Font.Size, FontStyle.Bold)
                            Case -1
                                img = ImageList2.Images("SquareRed.jpg")
                                e.CellStyle.Font = New Font(e.CellStyle.Font.FontFamily, e.CellStyle.Font.Size, FontStyle.Bold)
                            Case -2
                                dgvHeber.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = "---"
                                e.CellStyle.ForeColor = dgvHeber.DefaultCellStyle.ForeColor
                        End Select
                    End If
                    e.PaintBackground(e.CellBounds, False)
                    If Not IsNothing(img) Then
                        Dim pX = 1
                        Dim pY = 1
                        e.Graphics.DrawImage(img, e.CellBounds.Left + 1, e.CellBounds.Top + 1, e.CellBounds.Width - 3, e.CellBounds.Height - 3)
                    End If
                    e.PaintContent(e.CellBounds)
                ElseIf dgvHeber.Columns(e.ColumnIndex).Name.Contains("T") Then
                    e.CellStyle.Font = New Font(e.CellStyle.Font.FontFamily, e.CellStyle.Font.Size, FontStyle.Bold)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvHeber_SelectionChanged(sender As Object, e As EventArgs) Handles dgvHeber.SelectionChanged
        dgvHeber.ClearSelection()
    End Sub

    ' Mamnnschaftswertung
    Private Sub dgvTeam_SelectionChanged(sender As Object, e As EventArgs) Handles dgvTeam.SelectionChanged
        dgvTeam.ClearSelection()
    End Sub
    Private Sub dgvTeam_SizeChanged(sender As Object, e As EventArgs) Handles dgvTeam.SizeChanged
        With dgvTeam
            .Location = New Point(pnlWertung.Left - .Width - 10, xAufruf.Top)
        End With
    End Sub

    ' Emergency Lifters
    Dim dvEmergency As DataView
    Private Sub mnuHeberWechsel_Click(sender As Object, e As EventArgs) Handles mnuHeberWechsel.Click, mnuHeberAuswechseln.Click

        Dim Team = dvLeader(Leader.TableIx)(bsLeader.Position)!idVerein.ToString

        With frmHeberwechsel
            .Gruppe = dvLeader(Leader.TableIx)(bsLeader.Position)!Gruppe.ToString
            .dtGruppen = dtGruppen
            .dvEmergency = New DataView(dvNotGrouped(Leader.TableIx).ToTable(True, {"Nachname", "Vorname", "idVerein", "Teilnehmer"})) With {.RowFilter = "idVerein = " & Team, .Sort = "Nachname, Vorname"}
            .ShowDialog(Me)
            If .DialogResult = DialogResult.OK Then
                Change_Lifters(.NeuerHeber, .Gruppe)
            End If
        End With


        'Dim Team = dvLeader(Leader.TableIx)(bsLeader.Position)!idVerein.ToString
        'dvEmergency = New DataView(dvNotGrouped(Leader.TableIx).ToTable(True, {"Nachname", "Vorname", "idVerein", "Teilnehmer"})) With {.RowFilter = "idVerein = " & Team, .Sort = "Nachname, Vorname"}

        '' Gruppe des auszuwechselnden Hebers
        'Dim Gruppe = dvLeader(Leader.TableIx)(bsLeader.Position)!Gruppe.ToString

        'With cboGruppe
        '    .Parent = pnlAustausch
        '    .DataSource = dtGruppen
        '    .DisplayMember = "Gruppe"
        '    .ValueMember = "Gruppe"
        '    .SelectedIndex = .FindStringExact(Gruppe)
        'End With

        'lblAustauschMessage.Text = "für " & dvLeader(Leader.TableIx)(bsLeader.Position)!Nachname.ToString & ", " & dvLeader(Leader.TableIx)(bsLeader.Position)!Vorname.ToString

        'With pnlAustausch
        '    .Visible = True
        '    .BringToFront()
        'End With

        'With EmergencyLifters
        '    .AutoGenerateColumns = False
        '    .DataSource = dvEmergency
        '    .ClearSelection()
        '    '.Focus()
        '    btnAustauchOK.Enabled = .Rows.Count > 0 AndAlso .SelectedRows.Count > 0
        'End With

    End Sub

    Private Sub Change_Lifters(NeuerHeber As Integer, Gruppe As String)

        Dim AlterHeber = dvLeader(Leader.TableIx)(bsLeader.Position)!Teilnehmer.ToString
        'If NeuerHeber = -1 AndAlso EmergencyLifters.SelectedRows.Count > 0 Then NeuerHeber = CInt(dvEmergency(EmergencyLifters.SelectedRows(0).Index)!Teilnehmer)

        ' AlterHeber für diesen Durchgang verzichten
        Dim AlterheberVerzicht = Verzichten(True, True)
        ' falls Stoßen --> NeuerHeber für Reißen Verzichten
        Dim NeuerheberVerzicht = If(Leader.TableIx = 1, Verzichten(True, True, NeuerHeber, 0), True)

        ' Gruppe für einzuwechselnden Heber
        'Dim Gruppe = CInt(dvLeader(Leader.TableIx)(bsLeader.Position)!Gruppe)

        If AlterheberVerzicht AndAlso NeuerheberVerzicht Then
            If User.UserId = UserID.Wettkampfleiter Then
                ' einzuwechselnden Heber in Meldung speichern
                Using conn As New MySqlConnection(User.ConnString)
                    conn.Open()
                    cmd = New MySqlCommand("UPDATE meldung SET Gruppe = @gr WHERE " & Wettkampf.WhereClause() & " AND Teilnehmer = @tn;", conn)
                    With cmd.Parameters
                        .AddWithValue("@tn", NeuerHeber)
                        .AddWithValue("@gr", Gruppe)
                    End With
                    cmd.ExecuteNonQuery()
                End Using
            Else
                ' Wechsel an Server senden --> A = auszuwechselnd, T = einzuwechselnd, G = Gruppe
                Client_Send(String.Concat("VXA=", AlterHeber, ";T=", NeuerHeber, ";G=", Gruppe))
            End If

#Region "Results für neuer Heber ändern"
            Dim ix = bsResults.Find("Teilnehmer", NeuerHeber)
            dvResults(ix)!Gruppe = Gruppe
            bsResults.EndEdit()
            ' Publikum ändern
            Leader.Exchange_Lifter(NeuerHeber, CInt(Gruppe))
            ' ausgewählten Heber in dvLeader einfügen
            Dim rows = dvNotGrouped(Leader.TableIx).FindRows(NeuerHeber)
            For Each row As DataRowView In rows
                row!Gruppe = Gruppe
            Next
            ' Sortierung
            Dim Heber_Changed = Sortierung()
            If Heber.Id > 0 AndAlso Not btnNext.Enabled Then
                If Heber_Changed Then
                    SendtoBohle()
                    Leader.Change_InfoMessage("Heber-Wechsel", InfoTarget.NotPublic)
                End If
            End If
#End Region
        End If

        'pnlAustausch.Visible = False
    End Sub

    'Private Sub btnAustauchOK_Click(sender As Object, e As EventArgs) Handles btnAustauchOK.Click
    '    Change_Lifters()
    'End Sub
    'Private Sub EmergencyLifters_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles EmergencyLifters.CellDoubleClick
    '    Change_Lifters()
    'End Sub
    'Private Sub EmergencyLifters_KeyDown(sender As Object, e As KeyEventArgs) Handles EmergencyLifters.KeyDown
    '    If e.KeyCode = Keys.Enter Then
    '        Change_Lifters()
    '        e.SuppressKeyPress = True
    '        e.Handled = True
    '    End If
    'End Sub


    Private Sub mnuHeberWechsel_VisibleChanged(sender As Object, e As EventArgs) Handles mnuHeberWechsel.VisibleChanged
        sep5.Visible = mnuHeberWechsel.Visible
        mnuHeberAuswechseln.Visible = mnuHeberWechsel.Visible
    End Sub

    Private Sub mnuClearHupe_Click(sender As Object, e As EventArgs) Handles mnuClearHupe.Click, mnuHupe.Click
        Try
            Steuerung.ZA_Write()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub mnuClearHeber_Click(sender As Object, e As EventArgs) Handles mnuClearHeber.Click
        Try
            'Heber.Change_HeberId(False)
            Heber.Id = 0
        Catch ex As Exception
        End Try
    End Sub

    Private Sub mnuBlaueTaste_Click(sender As Object, e As EventArgs) Handles mnuBlaueTaste.Click
        WK_Reset() ' Wertung zurücksetzen (Blaue Taste in Steuerung)
    End Sub

    Private Sub btnMinuten_Click(sender As Object, e As EventArgs) Handles btnMinute1.Click, btnMinute2.Click
        Dim CountDown = CInt(CType(sender, Button).Tag)
        Leader.CountDown = CountDown
        If Heber.Id > 0 Then Heber.CountDown = CountDown
        ZN_Pad.CountDown = CountDown
    End Sub


End Class