﻿
Imports System.ComponentModel
Imports MySqlConnector

Public Class Drucken_Gruppenliste

    Property Bereich As String

    Dim Query As String
    Dim cmd As MySqlCommand
    Dim dtGruppen As New DataTable
    Dim dtMeldung As New DataTable
    Dim dvGruppe As DataView
    Dim dvMeldung As DataView

    Dim dtVorlage As New DataTable
    Dim dvVorlage As DataView
    Dim Vorlage As String
    Dim report As FastReport.Report

    Private SelectedIndex As Integer

    Private Sub Me_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        NativeMethods.INI_Write(Bereich, "Vorlage", Vorlage, App_IniFile)
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        'Dim tTip As New ToolTip()
        'tTip.AutoPopDelay = 5000
        'tTip.InitialDelay = 1000
        'tTip.ReshowDelay = 500
        'tTip.ShowAlways = True
        'tTip.SetToolTip(chkNewPage, "")

        '        Bereich = Tag.ToString

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Select Case Bereich
            Case "Gruppenliste"
            Case "Starterliste"
            Case Nothing, "Wiegeliste"
                chkNewPage.Enabled = False
                chkNewPage.Checked = True
                chkAK.Enabled = False
        End Select

#Region "Vorlage"
        Vorlage = NativeMethods.INI_Read(Bereich, "Vorlage", App_IniFile)
        If Vorlage.Equals("?") Then Vorlage = String.Empty

        dtVorlage.Columns.Add("FileName", GetType(String))
        dtVorlage.Columns.Add("Directory", GetType(String))
        Dim cols(1) As DataColumn
        cols(0) = dtVorlage.Columns("FileName")
        cols(1) = dtVorlage.Columns("Directory")
        dtVorlage.PrimaryKey = cols

        Dim v = Directory.GetFiles(Path.Combine(Application.StartupPath, "Report"), Bereich + "*.frx", SearchOption.AllDirectories).ToList
        For i = 0 To v.Count - 1
            v(i) = Replace(v(i), Path.Combine(Application.StartupPath, "Report") + "\", "")
            v(i) = Replace(v(i), ".frx", "")
            dtVorlage.Rows.Add(v(i), Path.Combine(Application.StartupPath, "Report"))
        Next
        dvVorlage = New DataView(dtVorlage)
        dvVorlage.Sort = "FileName"

        With cboVorlage
            .DataSource = dvVorlage
            .DisplayMember = "FileName"
            .ValueMember = "Directory"
            .SelectedIndex = .FindStringExact(Vorlage)
        End With
#End Region

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                Query = "select *, concat(Gruppe,' - ', Bezeichnung) Gruppenname " &
                        "from gruppen where Wettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtGruppen = New DataTable
                dtGruppen.Load(cmd.ExecuteReader)
                Query = "Select m.Wettkampf, m.Gruppe, m.Teilnehmer, a.Titel, a.Nachname, a.Vorname, a.Geschlecht Sex, a.Geburtstag, Year(a.Geburtstag) Jahrgang, IfNull(v.Kurzname, v.Vereinsname) Vereinsname, " &
                        "m.Losnummer, m.Startnummer, m.Wiegen, m.idAK, m.AK, m.idGK, m.GK, m.Zweikampf, m.Reissen AV_R, m.Stossen AV_S, m.Laenderwertung, m.Vereinswertung, m.attend, " &
                        "r.region_3 Land, s.state_iso3 Staat, s.state_name Country " &
                        "FROM meldung m " &
                        "LEFT JOIN athleten a On m.Teilnehmer = a.idTeilnehmer " &
                        "Left Join verein v ON a.Verein = v.idVerein " &
                        "Left Join region r On r.region_id = v.Region " &
                        "Left Join staaten s On s.state_id = a.Staat " &
                        "WHERE m.Wettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtMeldung = New DataTable
                dtMeldung.Load(cmd.ExecuteReader)
            Catch ex As MySqlException
                MySQl_Error(Nothing, "Drucken_Gruppe: Me_Shown: " & ex.Message, ex.StackTrace, True)
            End Try
        End Using

        ' testen, ob Gruppenzuordnung erfolgt ist
        Dim tmp As DataView = New DataView(dtMeldung, "Convert(IsNull(Gruppe,''), System.String) = '' ", Nothing, DataViewRowState.CurrentRows)

        If Not Wettkampf.Mannschaft AndAlso (dtGruppen.Rows.Count = 0 OrElse tmp.Count = dtMeldung.Rows.Count) Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Gruppen-Einteilung für <" & Wettkampf.Bezeichnung & "> gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Close()
                Return
            End Using
        End If

        With dgvMeldung
            .AlternatingRowsDefaultCellStyle.BackColor = Color.OldLace
            .RowsDefaultCellStyle.BackColor = Color.LightCyan
            dvMeldung = New DataView(dtMeldung.DefaultView.ToTable(False, {"Gruppe", "Nachname", "Vorname", "Vereinsname"}), "Gruppe = 0", "Nachname, Vorname", DataViewRowState.CurrentRows)
            .DataSource = dvMeldung
            .Columns("Gruppe").Visible = False
            .Columns("Nachname").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("Vorname").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("Gruppe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("Nachname").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("Vorname").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("Vereinsname").SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        dvGruppe = New DataView(dtGruppen.DefaultView.ToTable(True, {"Gruppe", "Gruppenname"}), Nothing, "Gruppe", DataViewRowState.CurrentRows)
        With lstGruppe
            .DataSource = dvGruppe
            .DisplayMember = "Gruppenname"
            .ValueMember = "Gruppe"
            dvMeldung.RowFilter = "Gruppe = " + .SelectedValue.ToString
        End With

        btnVorschau.Enabled = Wettkampf.Mannschaft
        btnPrint.Enabled = Wettkampf.Mannschaft

        formMain.Change_MainProgressBarValue(0)

        Cursor = Cursors.Default

    End Sub

    Private Sub cboVorlage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVorlage.SelectedIndexChanged
        If cboVorlage.Focused Then Vorlage = cboVorlage.Text
    End Sub

    Private Sub chkNewPage_EnabledChanged(sender As Object, e As EventArgs) Handles chkNewPage.EnabledChanged
        If Not chkNewPage.Enabled Then chkNewPage.Checked = False
    End Sub

    Private Sub chkSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkSelectAll.CheckedChanged
        If chkSelectAll.Focused Then
            For i = 0 To lstGruppe.Items.Count - 1
                lstGruppe.SetItemChecked(i, chkSelectAll.Checked)
            Next
            btnVorschau.Enabled = lstGruppe.CheckedItems.Count > 0
        End If
    End Sub

    Private Sub lstGruppe_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles lstGruppe.ItemCheck
        With lstGruppe
            If .Focused Then
                btnVorschau.Enabled = .CheckedItems.Count - e.CurrentValue + e.NewValue > 0
                If .CheckedItems.Count - e.CurrentValue + e.NewValue > 0 AndAlso .CheckedItems.Count - e.CurrentValue + e.NewValue < .Items.Count Then
                    chkSelectAll.CheckState = CheckState.Indeterminate
                Else
                    chkSelectAll.CheckState = CType(Math.Abs(CInt(.CheckedItems.Count - e.CurrentValue + e.NewValue > 0)), CheckState)
                End If
            End If
            Try
                Dim lstCheck As New List(Of String)
                For Each Check As DataRowView In .CheckedItems
                    lstCheck.Add(Check(0).ToString)
                Next

                Dim r As DataRowView = CType(.Items(e.Index), DataRowView)
                If lstCheck.Contains(r(0).ToString) Then
                    lstCheck.Remove(r(0).ToString)
                Else
                    lstCheck.Add(r(0).ToString)
                End If

                dvMeldung.RowFilter = "Gruppe = " + .SelectedValue.ToString

            Catch ex As Exception
            End Try

            'If Not Bereich.Equals("Wiegeliste") Then
            '    chkNewPage.Enabled = lstGruppe.CheckedItems.Count - e.CurrentValue + e.NewValue > 1
            '    chkNewPage.Checked = chkNewPage.Enabled
            'End If
        End With
    End Sub
    Private Sub lstGruppe_MouseDown(sender As Object, e As MouseEventArgs) Handles lstGruppe.MouseClick
        If e.Button = MouseButtons.Left AndAlso e.X < 15 Then
            With lstGruppe
                Dim index = .IndexFromPoint(e.X, e.Y)
                If Not SelectedIndex = index Then
                    SelectedIndex = index
                    Dim checked = .GetItemChecked(index)
                    .SetItemChecked(index, Not checked)
                End If
            End With
        End If
    End Sub
    Private Sub lstGruppe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstGruppe.SelectedIndexChanged
        With lstGruppe
            SelectedIndex = .SelectedIndex
            Try
                dvMeldung.RowFilter = "Gruppe = " + .SelectedValue.ToString
            Catch ex As Exception
            End Try
        End With
    End Sub

    Private Sub dgvMeldung_SelectionChanged(sender As Object, e As EventArgs) Handles dgvMeldung.SelectionChanged
        dgvMeldung.ClearSelection()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Close()
    End Sub
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Cursor = Cursors.WaitCursor

        User.Check_MySQLDriver(Me)

        Using report = New FastReport.Report
            With report
                Try
                    .Load(Path.Combine(CType(cboVorlage.SelectedItem, DataRowView)(1).ToString, CType(cboVorlage.SelectedItem, DataRowView)(0).ToString + ".frx"))
                    .Dictionary.Connections(0).ConnectionString = User.FR_ConnString
                    .Design()
                Catch ex As Exception
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Vorlage nicht gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                    Return
                End Try
            End With
        End Using
        cboVorlage.Focus()
        Cursor = Cursors.Default
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        With OpenFileDialog1
            .Filter = "Druck-Vorlagen (*.frx)|*.frx|Alle Dateien (*.*)|*.*"
            .InitialDirectory = Path.Combine(Application.StartupPath, "Report")
            If .ShowDialog() = DialogResult.Cancel Then Exit Sub
            If Not .FileName.Contains(".frx") Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Die ausgewählte Datei ist keine Druckvorlage.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
            Else
                Try
                    Dim directory = .FileName.Replace(.SafeFileName, "")
                    Vorlage = Split(.SafeFileName, ".")(0)
                    directory = Strings.Left(directory, directory.Length - 1)
                    dtVorlage.Rows.Add(Vorlage, directory)
                    cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage)
                Catch ex As Exception
                End Try
            End If
        End With
        cboVorlage.Focus()
    End Sub
    Private Sub btnVorschau_Click(sender As Object, e As EventArgs) Handles btnVorschau.Click, btnPrint.Click

        If Not Wettkampf.Mannschaft AndAlso lstGruppe.CheckedItems.Count = 0 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Gruppe ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
            Return
        End If

        User.Check_MySQLDriver(Me)

        Cursor = Cursors.WaitCursor

        report = New FastReport.Report
        With report

#Region "Vorlage"
            If Vorlage = String.Empty AndAlso cboVorlage.Text = String.Empty Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Keine Druckvorlage ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                Cursor = Cursors.Default
                Return
            End If
            Try
                .Load(Path.Combine(cboVorlage.SelectedValue.ToString, If(Not String.IsNullOrEmpty(Vorlage), Vorlage, cboVorlage.Text) + ".frx"))
                .Dictionary.Connections(0).ConnectionString = User.FR_ConnString
                btnVorschau.Enabled = False
            Catch ex As Exception
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Vorlage nicht gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
                Cursor = Cursors.Default
                Return
            End Try
#End Region

            Dim tmp = New DataView(dtMeldung)

            Dim _filter = String.Empty

            If Not Wettkampf.Mannschaft Then
                If chkAnwesend.Checked Then _filter = "(attend = True OR Convert(IsNull(attend,''), System.String) = '') AND ("
                For Each Check As DataRowView In lstGruppe.CheckedItems
                    _filter += "Gruppe = " + Check(0).ToString + " OR "
                Next
                _filter = Strings.Left(_filter, _filter.Length - 4)
                If chkAnwesend.Checked Then _filter += ")"
            End If

            tmp.RowFilter = _filter
            tmp.Sort = "Losnummer, Nachname, Vorname"

            Dim tn = (From x In tmp.ToTable
                      Group x By Sex = x.Field(Of String)("Sex") Into Group
                      Order By Sex Ascending).ToList

            Try
                .SetParameterValue("EachGroupNewPage", chkNewPage.Checked)
                .SetParameterValue("Sort_AK", chkAK.Checked)
                .SetParameterValue("International", Wettkampf.International)
                .SetParameterValue("TN_gesamt", tmp.Count)
                If tn.Count > 1 Then
                    .SetParameterValue("TN_männlich", tn(0).Group.Count)
                    .SetParameterValue("TN_weiblich", tn(1).Group.Count)
                Else
                    .SetParameterValue("TN_" & If(tn(0).Sex = "m", "männlich", "weiblich"), tn(0).Group.Count)
                End If

                .RegisterData(tmp.ToTable, "Heber")
                .RegisterData(Glob.dtWK, "wk")
                .RegisterData(dtGruppen, "gruppen")

            Catch ex As Exception
                LogMessage("Drucken_Gruppenliste: btnVorschau: Vorlage: " & ex.Message, ex.StackTrace)
            End Try

            Try
                Dim s = New FastReport.EnvironmentSettings
                s.ReportSettings.ShowProgress = False

                .Prepare()

                If sender.Equals(btnVorschau) Then
                    .ShowPrepared()
                Else
                    .PrintPrepared()
                End If
            Catch ex As Exception
                LogMessage("Drucken_Gruppenliste: btnVorschau: Prepare: " & ex.Message, ex.StackTrace)
            End Try

        End With
        Cursor = Cursors.Default
        btnVorschau.Enabled = True
        lstGruppe.Focus()

    End Sub
    Private Sub btnVorschau_EnabledChanged(sender As Object, e As EventArgs) Handles btnVorschau.EnabledChanged
        btnPrint.Enabled = btnVorschau.Enabled
    End Sub

    Private Sub CheckBox_GotFocus(sender As Object, e As EventArgs) Handles chkAK.GotFocus, chkAnwesend.GotFocus, chkNewPage.GotFocus
        lstGruppe.Focus()
    End Sub
End Class