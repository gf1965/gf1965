﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class BVDG
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grpServer = New System.Windows.Forms.GroupBox()
        Me.btnServer = New System.Windows.Forms.Button()
        Me.btnTestConn = New System.Windows.Forms.Button()
        Me.txtPort = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkPasswort = New System.Windows.Forms.CheckBox()
        Me.txtDatenbank = New System.Windows.Forms.TextBox()
        Me.btnSaveLogin = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPasswort = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtServer = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnWettkampf = New System.Windows.Forms.Button()
        Me.dgvWK = New System.Windows.Forms.DataGridView()
        Me.Datum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.wk_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ort = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Heim = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gast = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gast2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Heim_Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gast_Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gast2_Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.dgvHeber = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Los = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nachname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vorname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sex = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Staat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Verein = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chkIgnoreDate = New System.Windows.Forms.CheckBox()
        Me.cmnuVerein = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuVerein = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTeam = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.cboStaaten = New System.Windows.Forms.ComboBox()
        Me.chkProceedApi = New System.Windows.Forms.CheckBox()
        Me.grpServer.SuspendLayout()
        CType(Me.dgvWK, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmnuVerein.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpServer
        '
        Me.grpServer.Controls.Add(Me.btnServer)
        Me.grpServer.Controls.Add(Me.btnTestConn)
        Me.grpServer.Controls.Add(Me.txtPort)
        Me.grpServer.Controls.Add(Me.Label1)
        Me.grpServer.Controls.Add(Me.chkPasswort)
        Me.grpServer.Controls.Add(Me.txtDatenbank)
        Me.grpServer.Controls.Add(Me.btnSaveLogin)
        Me.grpServer.Controls.Add(Me.Label5)
        Me.grpServer.Controls.Add(Me.txtPasswort)
        Me.grpServer.Controls.Add(Me.Label4)
        Me.grpServer.Controls.Add(Me.txtUser)
        Me.grpServer.Controls.Add(Me.Label3)
        Me.grpServer.Controls.Add(Me.txtServer)
        Me.grpServer.Controls.Add(Me.Label2)
        Me.grpServer.Enabled = False
        Me.grpServer.Location = New System.Drawing.Point(19, 16)
        Me.grpServer.Name = "grpServer"
        Me.grpServer.Size = New System.Drawing.Size(368, 182)
        Me.grpServer.TabIndex = 101
        Me.grpServer.TabStop = False
        Me.grpServer.Text = "&Server-Anmeldung"
        '
        'btnServer
        '
        Me.btnServer.BackColor = System.Drawing.SystemColors.Window
        Me.btnServer.BackgroundImage = Global.Gewichtheben.My.Resources.Resources.edit
        Me.btnServer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnServer.Location = New System.Drawing.Point(324, 136)
        Me.btnServer.Name = "btnServer"
        Me.btnServer.Size = New System.Drawing.Size(30, 30)
        Me.btnServer.TabIndex = 114
        Me.btnServer.UseVisualStyleBackColor = False
        '
        'btnTestConn
        '
        Me.btnTestConn.Location = New System.Drawing.Point(252, 52)
        Me.btnTestConn.Name = "btnTestConn"
        Me.btnTestConn.Size = New System.Drawing.Size(103, 22)
        Me.btnTestConn.TabIndex = 122
        Me.btnTestConn.TabStop = False
        Me.btnTestConn.Text = "Verbindung testen"
        Me.btnTestConn.UseVisualStyleBackColor = True
        '
        'txtPort
        '
        Me.txtPort.Location = New System.Drawing.Point(278, 27)
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(76, 20)
        Me.txtPort.TabIndex = 40
        Me.txtPort.Tag = "3306"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(249, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(26, 13)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "&Port"
        '
        'chkPasswort
        '
        Me.chkPasswort.AutoSize = True
        Me.chkPasswort.Location = New System.Drawing.Point(252, 82)
        Me.chkPasswort.Name = "chkPasswort"
        Me.chkPasswort.Size = New System.Drawing.Size(109, 17)
        Me.chkPasswort.TabIndex = 90
        Me.chkPasswort.TabStop = False
        Me.chkPasswort.Text = "Passwort sichtbar"
        Me.chkPasswort.UseVisualStyleBackColor = True
        '
        'txtDatenbank
        '
        Me.txtDatenbank.Location = New System.Drawing.Point(79, 105)
        Me.txtDatenbank.Name = "txtDatenbank"
        Me.txtDatenbank.Size = New System.Drawing.Size(155, 20)
        Me.txtDatenbank.TabIndex = 110
        Me.txtDatenbank.Tag = "api_bvdg"
        '
        'btnSaveLogin
        '
        Me.btnSaveLogin.Enabled = False
        Me.btnSaveLogin.Location = New System.Drawing.Point(24, 141)
        Me.btnSaveLogin.Name = "btnSaveLogin"
        Me.btnSaveLogin.Size = New System.Drawing.Size(212, 25)
        Me.btnSaveLogin.TabIndex = 120
        Me.btnSaveLogin.TabStop = False
        Me.btnSaveLogin.Text = "Anmelde-Daten speichern"
        Me.btnSaveLogin.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(18, 108)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 13)
        Me.Label5.TabIndex = 100
        Me.Label5.Text = "&Datenbank"
        '
        'txtPasswort
        '
        Me.txtPasswort.Location = New System.Drawing.Point(79, 79)
        Me.txtPasswort.Name = "txtPasswort"
        Me.txtPasswort.Size = New System.Drawing.Size(155, 20)
        Me.txtPasswort.TabIndex = 80
        Me.txtPasswort.Tag = "hantel123!"
        Me.txtPasswort.UseSystemPasswordChar = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(26, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 13)
        Me.Label4.TabIndex = 70
        Me.Label4.Text = "Pass&wort"
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(79, 53)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(155, 20)
        Me.txtUser.TabIndex = 60
        Me.txtUser.Tag = "wettkampfsoftware"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(28, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 50
        Me.Label3.Text = "&Benutzer"
        '
        'txtServer
        '
        Me.txtServer.Location = New System.Drawing.Point(79, 27)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(155, 20)
        Me.txtServer.TabIndex = 20
        Me.txtServer.Tag = "onlineportal.bvdg-online.de"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(18, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "IP-&Adresse"
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(416, 83)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(157, 25)
        Me.btnExit.TabIndex = 103
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "Schließen"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnWettkampf
        '
        Me.btnWettkampf.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnWettkampf.Enabled = False
        Me.btnWettkampf.Location = New System.Drawing.Point(416, 21)
        Me.btnWettkampf.Name = "btnWettkampf"
        Me.btnWettkampf.Size = New System.Drawing.Size(157, 25)
        Me.btnWettkampf.TabIndex = 110
        Me.btnWettkampf.TabStop = False
        Me.btnWettkampf.Text = "Wettkämpfe"
        Me.btnWettkampf.UseVisualStyleBackColor = True
        '
        'dgvWK
        '
        Me.dgvWK.AllowUserToAddRows = False
        Me.dgvWK.AllowUserToDeleteRows = False
        Me.dgvWK.AllowUserToOrderColumns = True
        Me.dgvWK.AllowUserToResizeColumns = False
        Me.dgvWK.AllowUserToResizeRows = False
        Me.dgvWK.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvWK.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvWK.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvWK.ColumnHeadersHeight = 20
        Me.dgvWK.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Datum, Me.wk_name, Me.Ort, Me.Heim, Me.Gast, Me.Gast2, Me.Heim_Id, Me.Gast_Id, Me.Gast2_Id})
        Me.dgvWK.Location = New System.Drawing.Point(19, 219)
        Me.dgvWK.MultiSelect = False
        Me.dgvWK.Name = "dgvWK"
        Me.dgvWK.ReadOnly = True
        Me.dgvWK.RowHeadersVisible = False
        Me.dgvWK.RowHeadersWidth = 62
        Me.dgvWK.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvWK.ShowCellErrors = False
        Me.dgvWK.ShowEditingIcon = False
        Me.dgvWK.ShowRowErrors = False
        Me.dgvWK.Size = New System.Drawing.Size(554, 242)
        Me.dgvWK.TabIndex = 105
        Me.dgvWK.Tag = "28; 337"
        '
        'Datum
        '
        Me.Datum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Datum.DataPropertyName = "Datum"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.Format = "d"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.Datum.DefaultCellStyle = DataGridViewCellStyle11
        Me.Datum.FillWeight = 80.0!
        Me.Datum.Frozen = True
        Me.Datum.HeaderText = "Datum"
        Me.Datum.MinimumWidth = 8
        Me.Datum.Name = "Datum"
        Me.Datum.ReadOnly = True
        Me.Datum.Width = 80
        '
        'wk_name
        '
        Me.wk_name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.wk_name.DataPropertyName = "wk_name"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.wk_name.DefaultCellStyle = DataGridViewCellStyle12
        Me.wk_name.FillWeight = 80.0!
        Me.wk_name.Frozen = True
        Me.wk_name.HeaderText = "Bezeichnung"
        Me.wk_name.MinimumWidth = 8
        Me.wk_name.Name = "wk_name"
        Me.wk_name.ReadOnly = True
        Me.wk_name.Width = 245
        '
        'Ort
        '
        Me.Ort.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Ort.DataPropertyName = "Ort"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.Ort.DefaultCellStyle = DataGridViewCellStyle13
        Me.Ort.FillWeight = 225.0!
        Me.Ort.HeaderText = "Ort"
        Me.Ort.MinimumWidth = 225
        Me.Ort.Name = "Ort"
        Me.Ort.ReadOnly = True
        Me.Ort.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Ort.Width = 225
        '
        'Heim
        '
        Me.Heim.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Heim.DataPropertyName = "Heim"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.Heim.DefaultCellStyle = DataGridViewCellStyle14
        Me.Heim.FillWeight = 80.0!
        Me.Heim.HeaderText = "Heim"
        Me.Heim.MinimumWidth = 8
        Me.Heim.Name = "Heim"
        Me.Heim.ReadOnly = True
        Me.Heim.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Heim.Width = 200
        '
        'Gast
        '
        Me.Gast.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gast.DataPropertyName = "Gast"
        Me.Gast.FillWeight = 80.0!
        Me.Gast.HeaderText = "Gast 1"
        Me.Gast.MinimumWidth = 8
        Me.Gast.Name = "Gast"
        Me.Gast.ReadOnly = True
        Me.Gast.Width = 200
        '
        'Gast2
        '
        Me.Gast2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gast2.DataPropertyName = "Gast2"
        Me.Gast2.FillWeight = 80.0!
        Me.Gast2.HeaderText = "Gast 2"
        Me.Gast2.MinimumWidth = 8
        Me.Gast2.Name = "Gast2"
        Me.Gast2.ReadOnly = True
        Me.Gast2.Width = 200
        '
        'Heim_Id
        '
        Me.Heim_Id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Heim_Id.DataPropertyName = "Heim_Id"
        Me.Heim_Id.HeaderText = "Heim Id"
        Me.Heim_Id.MinimumWidth = 65
        Me.Heim_Id.Name = "Heim_Id"
        Me.Heim_Id.ReadOnly = True
        Me.Heim_Id.Width = 65
        '
        'Gast_Id
        '
        Me.Gast_Id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gast_Id.DataPropertyName = "Gast_Id"
        Me.Gast_Id.HeaderText = "Gast Id"
        Me.Gast_Id.MinimumWidth = 65
        Me.Gast_Id.Name = "Gast_Id"
        Me.Gast_Id.ReadOnly = True
        Me.Gast_Id.Width = 65
        '
        'Gast2_Id
        '
        Me.Gast2_Id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gast2_Id.DataPropertyName = "Gast2_Id"
        Me.Gast2_Id.HeaderText = "Gast2 Id"
        Me.Gast2_Id.MinimumWidth = 65
        Me.Gast2_Id.Name = "Gast2_Id"
        Me.Gast2_Id.ReadOnly = True
        Me.Gast2_Id.Width = 65
        '
        'Timer1
        '
        Me.Timer1.Interval = 300
        '
        'dgvHeber
        '
        Me.dgvHeber.AllowUserToAddRows = False
        Me.dgvHeber.AllowUserToDeleteRows = False
        Me.dgvHeber.AllowUserToOrderColumns = True
        Me.dgvHeber.AllowUserToResizeColumns = False
        Me.dgvHeber.AllowUserToResizeRows = False
        Me.dgvHeber.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvHeber.BackgroundColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvHeber.ColumnHeadersHeight = 20
        Me.dgvHeber.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.Los, Me.Nachname, Me.Vorname, Me.Sex, Me.JG, Me.Staat, Me.Verein})
        Me.dgvHeber.Location = New System.Drawing.Point(19, 219)
        Me.dgvHeber.MultiSelect = False
        Me.dgvHeber.Name = "dgvHeber"
        Me.dgvHeber.ReadOnly = True
        Me.dgvHeber.RowHeadersVisible = False
        Me.dgvHeber.RowHeadersWidth = 62
        Me.dgvHeber.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHeber.Size = New System.Drawing.Size(554, 242)
        Me.dgvHeber.TabIndex = 111
        Me.dgvHeber.Visible = False
        '
        'Id
        '
        Me.Id.DataPropertyName = "athlet_id"
        Me.Id.HeaderText = "Id"
        Me.Id.MinimumWidth = 8
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        Me.Id.Width = 150
        '
        'Los
        '
        Me.Los.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Los.DataPropertyName = "Los_Nr"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Los.DefaultCellStyle = DataGridViewCellStyle16
        Me.Los.HeaderText = "Los"
        Me.Los.Name = "Los"
        Me.Los.ReadOnly = True
        Me.Los.Width = 35
        '
        'Nachname
        '
        Me.Nachname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Nachname.DataPropertyName = "athlet_name"
        Me.Nachname.FillWeight = 80.0!
        Me.Nachname.HeaderText = "Nachname"
        Me.Nachname.MinimumWidth = 8
        Me.Nachname.Name = "Nachname"
        Me.Nachname.ReadOnly = True
        '
        'Vorname
        '
        Me.Vorname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Vorname.DataPropertyName = "athlet_vorname"
        Me.Vorname.FillWeight = 80.0!
        Me.Vorname.HeaderText = "Vorname"
        Me.Vorname.MinimumWidth = 8
        Me.Vorname.Name = "Vorname"
        Me.Vorname.ReadOnly = True
        '
        'Sex
        '
        Me.Sex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Sex.DataPropertyName = "athlet_geschlecht"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Sex.DefaultCellStyle = DataGridViewCellStyle17
        Me.Sex.HeaderText = "m/w"
        Me.Sex.MinimumWidth = 8
        Me.Sex.Name = "Sex"
        Me.Sex.ReadOnly = True
        Me.Sex.Width = 30
        '
        'JG
        '
        Me.JG.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.JG.DataPropertyName = "athlet_gebjahr"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.JG.DefaultCellStyle = DataGridViewCellStyle18
        Me.JG.HeaderText = "JG"
        Me.JG.MinimumWidth = 8
        Me.JG.Name = "JG"
        Me.JG.ReadOnly = True
        Me.JG.Visible = False
        Me.JG.Width = 40
        '
        'Staat
        '
        Me.Staat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Staat.DataPropertyName = "athlet_nation"
        Me.Staat.HeaderText = "Nation"
        Me.Staat.MinimumWidth = 8
        Me.Staat.Name = "Staat"
        Me.Staat.ReadOnly = True
        '
        'Verein
        '
        Me.Verein.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Verein.DataPropertyName = "Verein"
        Me.Verein.HeaderText = "Verein"
        Me.Verein.MinimumWidth = 8
        Me.Verein.Name = "Verein"
        Me.Verein.ReadOnly = True
        '
        'chkIgnoreDate
        '
        Me.chkIgnoreDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkIgnoreDate.AutoSize = True
        Me.chkIgnoreDate.Location = New System.Drawing.Point(416, 180)
        Me.chkIgnoreDate.Name = "chkIgnoreDate"
        Me.chkIgnoreDate.Size = New System.Drawing.Size(149, 17)
        Me.chkIgnoreDate.TabIndex = 113
        Me.chkIgnoreDate.Text = "alle Wettkämpfe anzeigen"
        Me.chkIgnoreDate.UseVisualStyleBackColor = True
        '
        'cmnuVerein
        '
        Me.cmnuVerein.AllowMerge = False
        Me.cmnuVerein.AutoSize = False
        Me.cmnuVerein.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.cmnuVerein.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuVerein, Me.mnuTeam})
        Me.cmnuVerein.Name = "cmnuVerein"
        Me.cmnuVerein.ShowImageMargin = False
        Me.cmnuVerein.Size = New System.Drawing.Size(132, 48)
        '
        'mnuVerein
        '
        Me.mnuVerein.Name = "mnuVerein"
        Me.mnuVerein.Size = New System.Drawing.Size(158, 22)
        Me.mnuVerein.Text = "Stammdaten Vereine"
        '
        'mnuTeam
        '
        Me.mnuTeam.Name = "mnuTeam"
        Me.mnuTeam.Size = New System.Drawing.Size(158, 22)
        Me.mnuTeam.Text = "Stammdaten Teams "
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.Enabled = False
        Me.btnImport.Location = New System.Drawing.Point(416, 52)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(157, 25)
        Me.btnImport.TabIndex = 114
        Me.btnImport.TabStop = False
        Me.btnImport.Text = "Teilnehmer importieren"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'cboStaaten
        '
        Me.cboStaaten.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cboStaaten.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboStaaten.FormattingEnabled = True
        Me.cboStaaten.Location = New System.Drawing.Point(393, 115)
        Me.cboStaaten.Name = "cboStaaten"
        Me.cboStaaten.Size = New System.Drawing.Size(179, 21)
        Me.cboStaaten.TabIndex = 115
        Me.cboStaaten.Visible = False
        '
        'chkProceedApi
        '
        Me.chkProceedApi.AutoSize = True
        Me.chkProceedApi.ForeColor = System.Drawing.Color.Red
        Me.chkProceedApi.Location = New System.Drawing.Point(416, 157)
        Me.chkProceedApi.Name = "chkProceedApi"
        Me.chkProceedApi.Size = New System.Drawing.Size(152, 17)
        Me.chkProceedApi.TabIndex = 116
        Me.chkProceedApi.Text = "Status auf Ergebnis setzen"
        Me.chkProceedApi.UseVisualStyleBackColor = True
        Me.chkProceedApi.Visible = False
        '
        'BVDG
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(591, 482)
        Me.Controls.Add(Me.chkProceedApi)
        Me.Controls.Add(Me.cboStaaten)
        Me.Controls.Add(Me.btnImport)
        Me.Controls.Add(Me.chkIgnoreDate)
        Me.Controls.Add(Me.btnWettkampf)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.grpServer)
        Me.Controls.Add(Me.dgvWK)
        Me.Controls.Add(Me.dgvHeber)
        Me.DoubleBuffered = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(605, 257)
        Me.Name = "BVDG"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "BVDG-Wettkampf importieren"
        Me.grpServer.ResumeLayout(False)
        Me.grpServer.PerformLayout()
        CType(Me.dgvWK, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmnuVerein.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents grpServer As GroupBox
    Friend WithEvents chkPasswort As CheckBox
    Friend WithEvents txtDatenbank As TextBox
    Friend WithEvents btnSaveLogin As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents txtPasswort As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtUser As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtServer As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtPort As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnExit As Button
    Friend WithEvents btnWettkampf As Button
    Friend WithEvents dgvWK As DataGridView
    Friend WithEvents Timer1 As Timer
    Friend WithEvents btnTestConn As Button
    Friend WithEvents dgvHeber As DataGridView
    Friend WithEvents chkIgnoreDate As CheckBox
    Friend WithEvents btnServer As Button
    Friend WithEvents cmnuVerein As ContextMenuStrip
    Friend WithEvents mnuVerein As ToolStripMenuItem
    Friend WithEvents mnuTeam As ToolStripMenuItem
    Friend WithEvents Datum As DataGridViewTextBoxColumn
    Friend WithEvents wk_name As DataGridViewTextBoxColumn
    Friend WithEvents Ort As DataGridViewTextBoxColumn
    Friend WithEvents Heim As DataGridViewTextBoxColumn
    Friend WithEvents Gast As DataGridViewTextBoxColumn
    Friend WithEvents Gast2 As DataGridViewTextBoxColumn
    Friend WithEvents Heim_Id As DataGridViewTextBoxColumn
    Friend WithEvents Gast_Id As DataGridViewTextBoxColumn
    Friend WithEvents Gast2_Id As DataGridViewTextBoxColumn
    Friend WithEvents btnImport As Button
    Friend WithEvents Id As DataGridViewTextBoxColumn
    Friend WithEvents Los As DataGridViewTextBoxColumn
    Friend WithEvents Nachname As DataGridViewTextBoxColumn
    Friend WithEvents Vorname As DataGridViewTextBoxColumn
    Friend WithEvents Sex As DataGridViewTextBoxColumn
    Friend WithEvents JG As DataGridViewTextBoxColumn
    Friend WithEvents Staat As DataGridViewTextBoxColumn
    Friend WithEvents Verein As DataGridViewTextBoxColumn
    Friend WithEvents cboStaaten As ComboBox
    Friend WithEvents chkProceedApi As CheckBox
End Class
