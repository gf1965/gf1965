﻿
Module Optionen

    Public Ansicht_Options As New myAnsicht
    Public Program_Options As New myProgram
    Public WK_Options As New myWK
    Public WK_Options_Tmp As New myWK
    Public WK_Modus As New myModus
    Public SendLog As Boolean = False
    Public UpdateDir As String = "https://gfhsoft.de/releases"


    Enum NameFormat
        Standard        ' Nachname, Vorname
        Standard_UCase  ' NACHNAME, Vorname
        International   ' NACHNAME Vorname
    End Enum

    Class myAnsicht
        Private _nameformat As Integer

        Public Event NameFormat_Changed()
        Property NameFormat As Integer
            Get
                Return _nameformat
            End Get
            Set(value As Integer)
                _nameformat = value
                RaiseEvent NameFormat_Changed()
            End Set
        End Property

        Public Name_Color As Color = Color.FromArgb(-10496)
        Public Verein_Color As Color = Color.FromArgb(-10185235)
        Public AK_Color As Color = Color.FromArgb(-10185235)
        Public Durchgang_Color As Color = Color.FromArgb(-32944)

        Public Event ShadeChanged()
        Private _ShadeColor As Color
        Property Shade_Color As Color ' = Color.FromArgb(-13092808) ' Farbe der abgeblendeten Wertungsanzeige
            Get
                Return _ShadeColor
            End Get
            Set(value As Color)
                _ShadeColor = value
                RaiseEvent ShadeChanged()
            End Set
        End Property

        Public Event FontSizeChanged(Increase As Single, Cancel As Boolean)
        Public Sub Change_FontSize(Increase As Single, Optional Cancel As Boolean = False)
            RaiseEvent FontSizeChanged(Increase, Cancel)
        End Sub

        'Private _FontSize As Single
        'Property FontSize As Single
        '    Get
        '        Return _FontSize
        '    End Get
        '    Set(value As Single)
        '        _FontSize = value
        '        RaiseEvent FontSizeChanged()
        '    End Set
        'End Property

        Public Event Ansicht_Changed()
        Private _Changed As Boolean
        Property Changed As Boolean
            Get
                Return _Changed
            End Get
            Set(value As Boolean)
                _Changed = value
                If value = True Then RaiseEvent Ansicht_Changed()
            End Set
        End Property
    End Class
    Class myProgram
        Public PrimaryBounds As Boolean = False ' Mauszeiger auf Programm-Monitor begrenzen
        Public SavePosition As Boolean = True 'Position des Programmfensters beim Beenden speichern
        Public AutoUpdate As Boolean = True
        Public ShowMessages As Boolean = False
    End Class
    Class myWK
        Private _mark As Integer = 0
        Public Event MarkRows_Changed(Row As Integer)
        Public DauerAbzeichen As Integer = 10       ' Anzeigedauer Ab-Zeichen 
        Public DauerHupe As Integer = 3             ' Dauer des Huptons der Uhr für Aufruf 
        Public WertungDelay As Integer = 3          ' Verzögerung der Anzeige der Wertung nach Ab-Zeichen (Sekunden)
        Public WertungAnzeigedauer As Integer = 5   ' Dauer der Anzeige der Wertung (Sekunden)
        Public LimitTechniknote As Single = 1.5     ' bei Überschreiten dieses Wertes in der Differenz der Techniknoten Meldung an JB_Dev
        Property Markierung As Integer            ' formLeader: alle Versuche des Hebers 
            Get
                Return _mark
            End Get
            Set(value As Integer)
                _mark = value
                RaiseEvent MarkRows_Changed(Leader.RowNum)
            End Set
        End Property
        Public COM_Sleep As Integer = 500          ' Zeit in ms, die der Thread bei der Initialisierung auf den COM-Anschluss wartet
        Public ShowMessages As Boolean = True       ' Meldungen beim Initialisieren der Bohle anzeigen
        Public Decimals As Integer = 1              ' 1/10 oder 5/10 für Techniknote
        Public Changed As Boolean = False
        Public Overwrite As Boolean = False
    End Class
    Class myModus
        Public MaxGruppen As Integer = 5
        Public TeilerMasters As Integer = 18
        Public TeilerZweikampf As Integer = 18
    End Class
    Public Sub Get_Optionen(Optional Changed As Boolean = False)
        Dim inp As String
        'If File.Exists(App_IniFile) Then

        With Program_Options
            inp = NativeMethods.INI_Read("Programm", "SavePosition", App_IniFile)
            If inp <> "?" Then .SavePosition = CBool(inp)
            inp = NativeMethods.INI_Read("Programm", "Primary", App_IniFile)
            If inp <> "?" Then
                .PrimaryBounds = CBool(inp)
                If .PrimaryBounds Then
                    Cursor.Clip = New Rectangle(Screen.FromControl(formMain).Bounds.Location, Screen.FromControl(formMain).Bounds.Size)
                Else
                    Cursor.Clip = Nothing
                End If
            End If
            inp = NativeMethods.INI_Read("Programm", "AutoUpdate", App_IniFile)
            If inp <> "?" Then
                .AutoUpdate = CBool(inp)
            Else
                .AutoUpdate = True
            End If
        End With

        With WK_Modus
            inp = NativeMethods.INI_Read("Modus", "MaxGruppen", App_IniFile)
            If inp <> "?" Then .MaxGruppen = CInt(inp)
            inp = NativeMethods.INI_Read("Modus", "TeilerMasters", App_IniFile)
            If inp <> "?" Then .TeilerMasters = CInt(inp)
            inp = NativeMethods.INI_Read("Modus", "TeilerZweikampf", App_IniFile)
            If inp <> "?" Then .TeilerZweikampf = CInt(inp)
        End With

        With Ansicht_Options
            inp = NativeMethods.INI_Read("Format", "NameAnzeige", App_IniFile)
            If inp <> "?" Then .NameFormat = CInt(inp)
            inp = NativeMethods.INI_Read("Format", "NameColor", App_IniFile)
            If inp <> "?" Then .Name_Color = Color.FromArgb(CInt(inp))
            inp = NativeMethods.INI_Read("Format", "VereinColor", App_IniFile)
            If inp <> "?" Then .Verein_Color = Color.FromArgb(CInt(inp))
            inp = NativeMethods.INI_Read("Format", "AkColor", App_IniFile)
            If inp <> "?" Then .AK_Color = Color.FromArgb(CInt(inp))
            inp = NativeMethods.INI_Read("Format", "DurchgangColor", App_IniFile)
            If inp <> "?" Then .Durchgang_Color = Color.FromArgb(CInt(inp))
            inp = NativeMethods.INI_Read("Format", "ShadeColor", App_IniFile)
            If inp <> "?" Then
                .Shade_Color = Color.FromArgb(CInt(inp))
            Else
                .Shade_Color = Color.FromArgb(-13092808)
            End If
            .Changed = Changed
        End With

        With WK_Options_Tmp ' alle Optionen, die nur zu einem bestimmten Zeitpunkt geändert werden dürfen
            inp = NativeMethods.INI_Read("Wettkampf", "DauerAbzeichen", App_IniFile)
            If inp <> "?" Then .DauerAbzeichen = CInt(inp)
            inp = NativeMethods.INI_Read("Wettkampf", "DauerHupe", App_IniFile)
            If inp <> "?" Then .DauerHupe = CInt(inp)
            inp = NativeMethods.INI_Read("Wettkampf", "WertungDelay", App_IniFile)
            If inp <> "?" Then .WertungDelay = CInt(inp)
            inp = NativeMethods.INI_Read("Wettkampf", "AnzeigeWertung", App_IniFile)
            If inp <> "?" Then .WertungAnzeigedauer = CInt(inp)
            inp = NativeMethods.INI_Read("Wettkampf", "LimitTechniknote", App_IniFile)
            If inp <> "?" Then .LimitTechniknote = CSng(inp)
            inp = NativeMethods.INI_Read("Wettkampf", "COM_Sleep", App_IniFile)
            If inp <> "?" Then .COM_Sleep = CInt(inp)
            inp = NativeMethods.INI_Read("Wettkampf", "ShowMessages", App_IniFile)
            If inp <> "?" Then .ShowMessages = CBool(inp)
            inp = NativeMethods.INI_Read("Wettkampf", "Decimals", App_IniFile)
            If inp <> "?" Then .Decimals = CInt(inp)
            inp = NativeMethods.INI_Read("Wettkampf", "Overwrite", App_IniFile)
            If inp <> "?" Then .Overwrite = CBool(inp)
            .Changed = Changed
            If Not Changed Then WK_Options = WK_Options_Tmp
        End With
        With WK_Options ' alle Optionen, die sofort geändert werden
            inp = NativeMethods.INI_Read("Wettkampf", "MarkRows", App_IniFile)
            If inp <> "?" Then
                .Markierung = CInt(inp)
                'Else
                '    .Markierung = 0
            End If
        End With

        inp = NativeMethods.INI_Read("FastReport", "MySQLDriver", App_IniFile)
        If Not inp.Equals("?") Then
            User.MySQL_Driver = inp
        ElseIf User.Get_Driver.Contains(FR_Driver) Then
            User.MySQL_Driver = FR_Driver
        End If

        'End If

    End Sub

End Module
