﻿
Imports System.ComponentModel
Imports MySqlConnector

Public Class Drucken_Protokoll

    Dim dicBereich As New Dictionary(Of String, Integer)
    Dim Bereich As String = "Protokoll"
    Dim Vorlage As New List(Of String)
    'Dim Vorlage As String
    Dim dtVorlage As New DataTable
    Dim dvVorlage As DataView

    Dim Query As String
    Dim cmd As MySqlCommand
    Dim dtProtokoll As DataTable
    Dim dtGruppen As DataTable
    'Dim dtKampfrichter As DataTable
    Dim dtAthletik As DataTable
    Dim dtTeams As DataTable

    Dim report As FastReport.Report

    Private IxAK As Integer ' für MouseDown-Event der lstAK_
    Private IxGK As Integer ' für MouseDown-Event der lstGK_
    Private IxWK As Integer ' für MouseDown-Event der lstWK_

    Dim dicLists As Dictionary(Of String, List(Of CheckedListBox))

    Dim dvAK_m As DataView
    Dim dvAK_w As DataView
    Dim dvGK_m As DataView
    Dim dvGK_w As DataView

    Dim dvTeams(1) As DataView
    Dim dvHeber(1) As DataView
    Dim bsTeams(1) As BindingSource
    Dim bsHeber(1) As BindingSource

    Dim Filter() As String = {"Verein", "Region"}

    Private Sub Me_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If btnCancel.Text = "Abbrechen" Then
            e.Cancel = True
        Else
            For i = 0 To dicBereich.Count - 1
                If Not String.IsNullOrEmpty(Vorlage(i)) Then
                    NativeMethods.INI_Write(dicBereich.Keys(i), "Vorlage", Vorlage(i), App_IniFile)
                End If
            Next
        End If
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

#Region "Vorlage"
        dicBereich.Add("Protokoll_Verein", 0)
        dicBereich.Add("Protokoll_Länder", 1)
        If Wettkampf.Athletik Then
            Bereich = "Protokoll_Athletik"
            chkHide.Text = "Kampfrichter ausblenden"
        ElseIf Wettkampf.Mannschaft Then
            Bereich = "Protokoll_Mannschaft"
        Else
            Bereich = "Protokoll"
        End If
        dicBereich.Add(Bereich, 2)

        For i = 0 To 2
            Dim x = NativeMethods.INI_Read(dicBereich.Keys(i), "Vorlage", App_IniFile)
            If x.Equals("?") Then x = String.Empty
            Vorlage.Add(x)
        Next

        dtVorlage.Columns.Add("FileName", GetType(String))
        dtVorlage.Columns.Add("Directory", GetType(String))
        'Dim cols(1) As DataColumn
        'cols(0) = dtVorlage.Columns("FileName")
        'cols(1) = dtVorlage.Columns("Directory")
        'dtVorlage.PrimaryKey = cols

        Dim v = Directory.GetFiles(Path.Combine(Application.StartupPath, "Report"), Bereich + "*.frx", SearchOption.AllDirectories).ToList
        For i = 0 To v.Count - 1
            v(i) = Replace(v(i), Path.Combine(Application.StartupPath, "Report") + "\", "")
            v(i) = Replace(v(i), ".frx", "")
            dtVorlage.Rows.Add(v(i), Path.Combine(Application.StartupPath, "Report"))
        Next
        dvVorlage = New DataView(dtVorlage)
        dvVorlage.Sort = "FileName"

        With cboVorlage
            .DataSource = dvVorlage
            .DisplayMember = "FileName"
            .ValueMember = "Directory"
        End With
#End Region

        dicLists = New Dictionary(Of String, List(Of CheckedListBox))
        Dim lstm = New List(Of CheckedListBox)
        lstm.AddRange({lstAK_m, lstGK_m})
        dicLists.Add("m", lstm)
        Dim lstw = New List(Of CheckedListBox)
        lstw.AddRange({lstAK_w, lstGK_w})
        dicLists.Add("w", lstw)

        Dim tTipText = "Protokoll für Gruppe(n) erstellen"
        If Val(CStr(Wettkampf.Platzierung)(0)) = 1 Then tTipText = "Zum Erstellen des Protokolls über den gesamten Wettkampf keine Gruppe auswählen."

        Dim tTip As New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        tTip.SetToolTip(lstGruppe, tTipText)

        TabControl1.TabPages(0).ToolTipText = tTipText

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

#Region "Data"
        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                ' Gruppen & Kampfrichter
                Query = "select g.Wettkampf, g.Gruppe, g.Bezeichnung, concat(g.Gruppe,' - ', g.Bezeichnung) Gruppenname, g.Datum, g.Reissen, g.Stossen, " &
                        "g.DatumW, g.WiegenBeginn, g.WiegenEnde, g.DatumA, g.Athletik, " &
                        "r.KR_1, r.KR_2, r.KR_3, r.ZN, r.TC, r.CM, r.Sec " &
                        "from gruppen g " &
                        "left join gruppen_kampfrichter r on r.Wettkampf = g.Wettkampf AND r.Gruppe = g.Gruppe " &
                        "where g.Wettkampf = " & Wettkampf.ID & ";"
                cmd = New MySqlCommand(Query, conn)
                dtGruppen = New DataTable
                dtGruppen.Load(cmd.ExecuteReader)
                ' Athletik
                Query = "SELECT d.idDisziplin, d.Bezeichnung, a.Disziplin " &
                        "FROM athletik_disziplinen d " &
                        "LEFT JOIN wettkampf_athletik a ON d.idDisziplin = a.Disziplin AND a.Wettkampf = " & Wettkampf.ID &
                        " WHERE Not IsNull(a.Disziplin) " &
                        "ORDER BY d.idDisziplin;"
                cmd = New MySqlCommand(Query, conn)
                dtAthletik = New DataTable
                dtAthletik.Load(cmd.ExecuteReader())
                '' Protokoll
                'Query = Glob.Get_QueryResults("'---'", Wettkampf.Athletik)
                'cmd = New MySqlCommand(Query, conn)
                'dtProtokoll = New DataTable
                'dtProtokoll.Load(cmd.ExecuteReader)
                ' Vereine / Länder
                Query = "SELECT a.Nachname, a.Vorname, a.Geschlecht, a.Jahrgang, a.Verein, IfNull(v.Kurzname, v.Vereinsname) Vereinsname, v.Region, reg.region_3, reg.region_name, " &
                        "IfNull(m.Wiegen, m.Gewicht) Wiegen, m.Laenderwertung, m.Vereinswertung, R_Last, S_Last, rs.Heben, rs.Gesamt, rs.Wertung2 " &
                        "FROM meldung m " &
                        "LEFT JOIN (SELECT r.Teilnehmer, r.HLast R_Last, r.Note R_Note " &
                        "FROM reissen r " &
                        "WHERE r.Wettkampf = Wettkampf And r.HLast = (SELECT MAX(HLast*Wertung) FROM reissen WHERE Teilnehmer = r.Teilnehmer AND r.Wertung = 1)) r " &
                        "ON m.Teilnehmer = r.Teilnehmer " &
                        "LEFT JOIN (SELECT s.Teilnehmer, s.HLast S_Last, s.Note S_Note " &
                        "FROM stossen s " &
                        "WHERE s.Wettkampf = Wettkampf And s.HLast = (SELECT MAX(HLast*Wertung) FROM stossen WHERE Teilnehmer = s.Teilnehmer AND s.Wertung = 1)) s " &
                        "ON m.Teilnehmer = s.Teilnehmer " &
                        "LEFT JOIN athleten a ON a.idTeilnehmer = m.Teilnehmer " &
                        "LEFT JOIN results rs ON rs.Wettkampf = m.Wettkampf And rs.Teilnehmer = m.Teilnehmer " &
                        "LEFT JOIN verein v ON v.idVerein = a.Verein " &
                        "LEFT JOIN region reg ON reg.region_id = v.Region " &
                        "WHERE m.Wettkampf = " & Wettkampf.ID & " AND m.attend = True AND m.a_K = False;"
                cmd = New MySqlCommand(Query, conn)
                dtTeams = New DataTable
                dtTeams.Load(cmd.ExecuteReader())
                ' Kommentare zum WK
                Using DS As New DataService
                    txtKommentar.Text = DS.Get_Wettkampf_Comments({Wettkampf.ID}, conn)
                End Using
            Catch ex As Exception
            End Try
        End Using
#End Region

        chkWertung2.Enabled = Wettkampf.Platzierung > 9

        If dtGruppen.Rows.Count = 0 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Gruppen-Einteilung für <" & Wettkampf.Bezeichnung & "> gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Close()
                Return
            End Using
        End If

        For Each row As DataRow In dtAthletik.Rows
            'If row!Bezeichnung.ToString.Equals("Bankdrücken") Then row!Bezeichnung = "Bank-drücken"
            If row!Bezeichnung.ToString.Equals("Schlussweitsprung") Then row!Bezeichnung = "Schlussweit-sprung"
            If row!Bezeichnung.ToString.Equals("Schlussdreisprung") Then row!Bezeichnung = "Schlussdrei-sprung"
            If row!Bezeichnung.ToString.Equals("CounterMoveJump") Then row!Bezeichnung = "Counter-Move-Jump"
        Next
        dtAthletik.AcceptChanges()

        If Wettkampf.Identities.Count = 1 Then
            TabControl1.TabPages.Remove(tabWettkampf)
        Else
            With lstWettkampf
                .DataSource = Glob.dtWK
                .ValueMember = "Wettkampf"
                .DisplayMember = "Bez1"
                .SetItemChecked(0, True)
            End With
        End If
        Get_Data(Wettkampf.Identities.Keys(0))

        'dtProtokoll.Columns.Add(New DataColumn With {.ColumnName = "Platz", .DataType = GetType(Integer)})
        'For Each row As DataRow In dtProtokoll.Rows
        '    If Wettkampf.Athletik Then
        '        If IsDBNull(row!Gesamt_Platz) Then
        '            row!Platz = 1000
        '        Else
        '            row!Platz = row!Gesamt_Platz
        '        End If
        '    Else
        '        If IsDBNull(row!Heben_Platz) Then
        '            row!Platz = 1000
        '        Else
        '            row!Platz = row!Heben_Platz
        '        End If
        '    End If
        'Next

        Dim dvG = New DataView(dtGruppen.DefaultView.ToTable(True, {"Gruppe", "Gruppenname"}), Nothing, "Gruppe", DataViewRowState.CurrentRows)
        With lstGruppe
            .Focus()
            .DataSource = dvG
            .DisplayMember = "Gruppenname"
            .ValueMember = "Gruppe"
        End With

        If Wettkampf.Technikwertung OrElse Wettkampf.Mannschaft Then
            TabControl1.TabPages.Remove(tabMännlich)
            TabControl1.TabPages.Remove(tabWeiblich)
        End If

        If Not Wettkampf.Mannschaft Then
            'dvGK_m = New DataView(dtProtokoll.DefaultView.ToTable(True, {"Sex", "idAK", "AK", "idGK", "GK"}), "Sex = 'm'", "idAK, idGK", DataViewRowState.CurrentRows)
            'dvGK_w = New DataView(dtProtokoll.DefaultView.ToTable(True, {"Sex", "idAK", "AK", "idGK", "GK"}), "Sex = 'w'", "idAK, idGK", DataViewRowState.CurrentRows)
            'dvAK_m = New DataView(dvGK_m.ToTable(True, {"Sex", "idAK", "AK"}), "Sex = 'm'", "idAK", DataViewRowState.CurrentRows)
            'dvAK_w = New DataView(dvGK_w.ToTable(True, {"Sex", "idAK", "AK"}), "Sex = 'w'", "idAK", DataViewRowState.CurrentRows)
            'With lstAK_m
            '    .DataSource = dvAK_m
            '    .ValueMember = "idAK"
            '    .DisplayMember = "AK"
            'End With
            'With lstAK_w
            '    .DataSource = dvAK_w
            '    .ValueMember = "idAK"
            '    .DisplayMember = "AK"
            'End With
            'With lstGK_m
            '    .DataSource = dvGK_m
            '    .ValueMember = "idGK"
            '    .DisplayMember = "GK"
            'End With
            'With lstGK_w
            '    .DataSource = dvGK_w
            '    .ValueMember = "idGK"
            '    .DisplayMember = "GK"
            'End With
            'If dvAK_m.Count > 0 Then dvGK_m.RowFilter = "idAK = " & dvAK_m(0)("idAK").ToString
            'If dvAK_w.Count > 0 Then dvGK_w.RowFilter = "idAK = " & dvAK_w(0)("idAK").ToString

            ' Vereine / Länder
            For i = 0 To 1
                Dim ReturnTable = Glob.Get_DGJ_Mannschaftsliste(i, New DataView(dtTeams)) ' ReturnTable(0) = Mannschaften, (1) = Heber
                If IsNothing(ReturnTable) Then
                    TabControl1.TabPages.RemoveByKey(If(i = 0, "tabVereine", "tabLänder"))
                Else
                    dvTeams(i) = New DataView(ReturnTable(0).ToTable)
                    bsTeams(i) = New BindingSource With {.DataSource = dvTeams(i)}
                    bsTeams(i).Sort = "Platz"
                    dvHeber(i) = New DataView(ReturnTable(1).ToTable)
                    bsHeber(i) = New BindingSource With {.DataSource = dvHeber(i)}
                    bsHeber(i).Sort = "Punkte DESC"
                End If
            Next
            If TabControl1.Contains(tabVereine) Then
                With dgvVerein
                    .DefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor
                    .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
                    .AutoGenerateColumns = False
                    .DataSource = bsTeams(0)
                    .Columns.Insert(0, New DataGridViewButtonColumn With {.Name = "Expand", .HeaderText = "", .UseColumnTextForButtonValue = True, .Text = "+"})
                End With
            End If
            If TabControl1.Contains(tabLänder) Then
                With dgvLänder
                    .DefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor
                    .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
                    .AutoGenerateColumns = False
                    .DataSource = bsTeams(1)
                    .Columns.Insert(0, New DataGridViewButtonColumn With {.Name = "Expand", .HeaderText = "", .UseColumnTextForButtonValue = True, .Text = "+"})
                End With
            End If
            chkHide.Enabled = chkHide.Text.Contains("Kampfrichter") OrElse CStr(Wettkampf.Platzierung)(0) <> "1" ' wenn Platzierung über ganzen WK geht, gibt es keine Header
            If Wettkampf.Identities.Count = 1 Then
                TabControl1.SelectTab("tabGruppen")
            Else
                TabControl1.SelectTab("tabWettkampf")
            End If
            'TabControl1.TabPages.Remove(tabKommentar)
        Else
            lstGruppe.Enabled = False
            chkHide.Enabled = False
            chkSelectAll.Enabled = False
            TabControl1.TabPages.Remove(tabVereine)
            TabControl1.TabPages.Remove(tabLänder)
            TabControl1.TabPages("tabGruppen").Enabled = False
            TabControl1.SelectTab("tabKommentar")
            If String.IsNullOrEmpty(txtKommentar.Text) Then
                Using GF As New myFunction
                    txtKommentar.Text = GF.Get_Comment(dtResults.Select("a_k = true"))
                End Using
            End If
        End If

        Bereich = dicBereich.Keys(2)
        cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage(dicBereich(Bereich)))

        btnVorschau.Enabled = CStr(Wettkampf.Platzierung)(0) = "1" OrElse lstGruppe.Enabled = False  ' wenn Platzierung über ganzen WK geht oder bei Bundesliga ist keine Auswahl nötig

        formMain.Change_MainProgressBarValue(0)

        Cursor = Cursors.Default

    End Sub

    Private Sub Get_Data(WK As String)
        ' Protokoll
        Cursor = Cursors.WaitCursor
        dtProtokoll = New DataTable
        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                Query = Glob.Get_QueryResults("'---'", Wettkampf.Athletik, WK)
                cmd = New MySqlCommand(Query, conn)
                dtProtokoll.Load(cmd.ExecuteReader)
            Catch ex As Exception
            End Try
        End Using

        If dtProtokoll.Rows.Count > 0 Then
            dtProtokoll.Columns.Add(New DataColumn With {.ColumnName = "Platz", .DataType = GetType(Integer)})
            For Each row As DataRow In dtProtokoll.Rows
                If Wettkampf.Athletik Then
                    If IsDBNull(row!Gesamt_Platz) Then
                        row!Platz = 1000
                    Else
                        row!Platz = row!Gesamt_Platz
                    End If
                Else
                    If IsDBNull(row!Heben_Platz) Then
                        row!Platz = 1000
                    Else
                        row!Platz = row!Heben_Platz
                    End If
                End If
            Next
        End If

        If Not Wettkampf.Mannschaft Then
            dvGK_m = New DataView(dtProtokoll.DefaultView.ToTable(True, {"Sex", "idAK", "AK", "idGK", "GK"}), "Sex = 'm'", "idAK, idGK", DataViewRowState.CurrentRows)
            dvGK_w = New DataView(dtProtokoll.DefaultView.ToTable(True, {"Sex", "idAK", "AK", "idGK", "GK"}), "Sex = 'w'", "idAK, idGK", DataViewRowState.CurrentRows)
            dvAK_m = New DataView(dvGK_m.ToTable(True, {"Sex", "idAK", "AK"}), "Sex = 'm'", "idAK", DataViewRowState.CurrentRows)
            dvAK_w = New DataView(dvGK_w.ToTable(True, {"Sex", "idAK", "AK"}), "Sex = 'w'", "idAK", DataViewRowState.CurrentRows)
            With lstAK_m
                .DataSource = dvAK_m
                .ValueMember = "idAK"
                .DisplayMember = "AK"
            End With
            With lstAK_w
                .DataSource = dvAK_w
                .ValueMember = "idAK"
                .DisplayMember = "AK"
            End With
            With lstGK_m
                .DataSource = dvGK_m
                .ValueMember = "idGK"
                .DisplayMember = "GK"
            End With
            With lstGK_w
                .DataSource = dvGK_w
                .ValueMember = "idGK"
                .DisplayMember = "GK"
            End With
            If dvAK_m.Count > 0 Then dvGK_m.RowFilter = "idAK = " & dvAK_m(0)("idAK").ToString
            If dvAK_w.Count > 0 Then dvGK_w.RowFilter = "idAK = " & dvAK_w(0)("idAK").ToString
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged

        'chkNewPage.Text = "jede " & TabControl1.SelectedTab.Tag.ToString & " auf neuer Seite beginnen"
        'chkSingleAKs.Text = "jede " & TabControl1.SelectedTab.Tag.ToString & " einzeln drucken"

        chkSelectAll.Enabled = Not TabControl1.SelectedTab.Equals(tabWettkampf)

        If TabControl1.SelectedTab.Equals(tabGruppen) Then
            If Wettkampf.Mannschaft Then
                TabControl1.SelectTab("tabKommentar")
                Return
            End If
            Bereich = dicBereich.Keys(2)
            cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage(dicBereich(Bereich)))
            With lstGruppe
                chkNewPage.Enabled = .CheckedItems.Count > 1
                chkHide.Enabled = chkHide.Text.Contains("Kampfrichter") OrElse CStr(Wettkampf.Platzierung)(0) <> "1"
                If .CheckedItems.Count > 0 AndAlso .CheckedItems.Count < .Items.Count Then
                    chkSelectAll.CheckState = CheckState.Indeterminate
                Else
                    chkSelectAll.CheckState = If(.CheckedItems.Count = .Items.Count, CheckState.Checked, CheckState.Unchecked)
                End If
                btnVorschau.Enabled = CStr(Wettkampf.Platzierung)(0) = "1" OrElse .CheckedItems.Count > 0
                IxAK = .SelectedIndex
            End With
            chkSelectAll.Enabled = True
        ElseIf TabControl1.SelectedTab.Equals(tabVereine) Then
            Bereich = dicBereich.Keys(0)
            cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage(dicBereich(Bereich)))
            btnVorschau.Enabled = True
            chkNewPage.Enabled = False
            chkSelectAll.Enabled = False
            chkHide.Enabled = False
            dgvHeber.DataSource = bsHeber(0)
        ElseIf TabControl1.SelectedTab.Equals(tabLänder) Then
            Bereich = dicBereich.Keys(1)
            cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage(dicBereich(Bereich)))
            btnVorschau.Enabled = True
            chkNewPage.Enabled = False
            chkSelectAll.Enabled = False
            chkHide.Enabled = False
            dgvHeber.DataSource = bsHeber(1)
        ElseIf Not TabControl1.SelectedTab.Equals(tabKommentar) Then
            Bereich = dicBereich.Keys(2)
            Dim Sex = "m"
            If TabControl1.SelectedTab.Equals(tabWeiblich) Then Sex = "w"
            With dicLists(Sex)(0)
                chkNewPage.Enabled = .CheckedItems.Count > 1 OrElse dicLists(Sex)(1).CheckedItems.Count > 1
                btnVorschau.Enabled = .CheckedItems.Count > 0
                If .CheckedItems.Count > 0 AndAlso .CheckedItems.Count < .Items.Count Then
                    chkSelectAll.CheckState = CheckState.Indeterminate
                Else
                    chkSelectAll.CheckState = If(.CheckedItems.Count = .Items.Count, CheckState.Checked, CheckState.Unchecked)
                End If
                dicLists(Sex)(1).Enabled = .CheckedItems.Count = 1 AndAlso .GetItemChecked(.SelectedIndex) = True
                IxAK = dicLists(Sex)(0).SelectedIndex
                IxGK = dicLists(Sex)(1).SelectedIndex
            End With
            chkSelectAll.Enabled = True
            chkHide.Enabled = False
        End If
    End Sub

    Private Sub cboVorlage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVorlage.SelectedIndexChanged
        If cboVorlage.Focused Then Vorlage(dicBereich(Bereich)) = cboVorlage.Text
    End Sub

    Private Sub chkSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkSelectAll.CheckedChanged
        If chkSelectAll.Focused Then
            If TabControl1.SelectedTab.Equals(tabGruppen) Then
                For i = 0 To lstGruppe.Items.Count - 1
                    lstGruppe.SetItemChecked(i, chkSelectAll.Checked)
                Next
                btnVorschau.Enabled = CStr(Wettkampf.Platzierung)(0) = "1" OrElse lstGruppe.CheckedItems.Count > 0
            ElseIf TabControl1.SelectedTab.Equals(tabMännlich) Then
                For i = 0 To lstAK_m.Items.Count - 1
                    lstAK_m.SetItemChecked(i, chkSelectAll.Checked)
                Next
                btnVorschau.Enabled = CStr(Wettkampf.Platzierung)(0) = "1" OrElse lstAK_m.CheckedItems.Count > 0
                lstGK_m.Enabled = False
            ElseIf TabControl1.SelectedTab.Equals(tabWeiblich) Then
                For i = 0 To lstAK_w.Items.Count - 1
                    lstAK_w.SetItemChecked(i, chkSelectAll.Checked)
                Next
                btnVorschau.Enabled = CStr(Wettkampf.Platzierung)(0) = "1" OrElse lstAK_w.CheckedItems.Count > 0
                lstGK_w.Enabled = False
            End If
        End If
    End Sub

    Private Sub chkNewPage_EnabledChanged(sender As Object, e As EventArgs) Handles chkNewPage.EnabledChanged
        'If Not chkNewPage.Enabled Then chkNewPage.Checked = False
    End Sub

    Private Sub CheckedListBox_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles lstGruppe.ItemCheck, lstAK_m.ItemCheck, lstAK_w.ItemCheck

        With CType(sender, CheckedListBox)
            If .Focused Then

                btnVorschau.Enabled = CStr(Wettkampf.Platzierung)(0) = "1" OrElse .CheckedItems.Count - e.CurrentValue + e.NewValue > 0

                If .CheckedItems.Count - e.CurrentValue + e.NewValue > 0 AndAlso .CheckedItems.Count - e.CurrentValue + e.NewValue < .Items.Count Then
                    chkSelectAll.CheckState = CheckState.Indeterminate
                Else
                    chkSelectAll.CheckState = CType(Math.Abs(CInt(.CheckedItems.Count - e.CurrentValue + e.NewValue > 0)), CheckState)
                End If

            End If

            chkNewPage.Enabled = .CheckedItems.Count - e.CurrentValue + e.NewValue > 1
        End With
    End Sub
    Private Sub CheckedListBox_MouseDown(sender As Object, e As MouseEventArgs) Handles lstGruppe.MouseDown, lstAK_m.MouseDown, lstAK_w.MouseDown
        If e.Button = MouseButtons.Left AndAlso e.X < 15 Then
            With CType(sender, CheckedListBox)
                Dim index = .IndexFromPoint(e.X, e.Y)
                If Not IxAK = index Then
                    IxAK = index
                    Dim checked = .GetItemChecked(index)
                    .SetItemChecked(index, Not checked)
                End If
            End With
        End If
    End Sub

    Private Sub lstAK_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstAK_m.SelectedIndexChanged, lstAK_w.SelectedIndexChanged
        Try
            With CType(sender, CheckedListBox)
                If Not .Focused Then Return
                Dim Sex = Split(.Name, "_")(1)
                Dim _Filter = "Sex = '" & Sex & "' AND idAK = " & .SelectedValue.ToString
                dicLists(Sex)(1).Enabled = .CheckedItems.Count = 1 AndAlso .GetItemChecked(.SelectedIndex) = True
                CType(dicLists(Sex)(1).DataSource, DataView).RowFilter = _Filter
                IxAK = .SelectedIndex
            End With
        Catch ex As Exception
        End Try
    End Sub
    Private Sub lstGK_EnabledChanged(sender As Object, e As EventArgs) Handles lstGK_m.EnabledChanged, lstGK_w.EnabledChanged
        With CType(sender, CheckedListBox) ' lstGK_m
            Controls().Find(.Name.Replace("lst", "lbl"), True)(0).Enabled = .Enabled
            If Not .Enabled Then
                For i = 0 To .CheckedItems.Count - 1
                    .SetItemChecked(i, False)
                Next
            End If
        End With
    End Sub
    Private Sub lstGK_MouseDown(sender As Object, e As MouseEventArgs) Handles lstGK_m.MouseDown, lstGK_w.MouseDown
        If e.Button = MouseButtons.Left AndAlso e.X < 15 Then
            With CType(sender, CheckedListBox)
                Dim index = .IndexFromPoint(e.X, e.Y)
                If Not IxGK = index Then
                    IxGK = index
                    Dim checked = .GetItemChecked(index)
                    .SetItemChecked(index, Not checked)
                End If
            End With
        End If
    End Sub
    Private Sub lstGK_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstGK_m.SelectedIndexChanged, lstGK_w.SelectedIndexChanged
        IxGK = CType(sender, CheckedListBox).SelectedIndex
    End Sub
    Private Sub lstGK_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles lstGK_m.ItemCheck, lstGK_w.ItemCheck
        chkNewPage.Enabled = CType(sender, CheckedListBox).CheckedItems.Count - e.CurrentValue + e.NewValue > 1
    End Sub
    Private Sub lstWettkampf_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstWettkampf.SelectedIndexChanged
        If Not lstWettkampf.Focused Then Return
        With lstWettkampf
            For i = 0 To .Items.Count - 1
                .SetItemChecked(i, False)
            Next
            .SetItemChecked(.SelectedIndex, True)
            If Not IxWK = .SelectedIndex Then
                IxWK = .SelectedIndex
                Get_Data(Wettkampf.Identities.Keys(IxWK))
            End If
        End With
    End Sub
    Private Sub lstWettkampf_MouseDown(sender As Object, e As MouseEventArgs) Handles lstWettkampf.MouseDown
        'If e.Button = MouseButtons.Left AndAlso e.X < 15 Then
        '    With CType(sender, CheckedListBox)
        '        Dim index = .IndexFromPoint(e.X, e.Y)
        '        If Not IxWK = index Then
        '            .SetItemChecked(IxWK, False)
        '            IxWK = index
        '            Dim checked = .GetItemChecked(index)
        '            .SetItemChecked(index, Not checked)
        '            If checked Then
        '                Get_Data(Wettkampf.Identities.Keys(index))
        '            End If
        '        End If
        '    End With
        'End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If btnCancel.Text = "Beenden" Then Close()
    End Sub
    Private Sub btnKR_Click(sender As Object, e As EventArgs) Handles btnKR.Click
        Dim changed As Boolean ' Änderungen an table "gruppen_kampfrichter"
        Cursor = Cursors.WaitCursor
        With frmKampfgericht
            .Text = "Kampfgericht - " & Wettkampf.Bezeichnung
            .ShowDialog(Me)
            changed = Not IsNothing(.Tag)
            .Dispose()
        End With

        If changed Then
            Using conn As New MySqlConnection(User.ConnString)
                Try
                    Query = "select g.Wettkampf, g.Gruppe, g.Bezeichnung, concat(g.Gruppe,' - ', g.Bezeichnung) Gruppenname, g.Datum, g.Reissen, g.Stossen, " &
                            "g.DatumW, g.WiegenBeginn, g.WiegenEnde, g.DatumA, g.Athletik, " &
                            "r.KR_1, r.KR_2, r.KR_3, r.ZN, r.TC, r.CM, r.Sec " &
                            "from gruppen g " &
                            "left join gruppen_kampfrichter r on r.Wettkampf = g.Wettkampf AND r.Gruppe = g.Gruppe " &
                            "where g.Wettkampf = " & Wettkampf.ID & ";"
                    conn.Open()
                    cmd = New MySqlCommand(Query, conn)
                    dtGruppen = New DataTable
                    dtGruppen.Load(cmd.ExecuteReader)
                Catch ex As Exception
                End Try
            End Using
        End If
        Cursor = Cursors.Default
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        With OpenFileDialog1
            .Filter = "Druck-Vorlagen (*.frx)|*.frx|Alle Dateien (*.*)|*.*"
            .InitialDirectory = Path.Combine(Application.StartupPath, "Report")
            If .ShowDialog() = DialogResult.Cancel Then Exit Sub
            If Not .FileName.Contains(".frx") Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Die ausgewählte Datei ist keine Druckvorlage.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
            Else
                Try
                    Dim directory = .FileName.Replace(.SafeFileName, "")
                    directory = Strings.Left(directory, directory.Length - 1)
                    Vorlage(dicBereich(Bereich)) = Split(.SafeFileName, ".")(0)
                    dtVorlage.Rows.Add(Vorlage(dicBereich(Bereich)), directory)
                    cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage(dicBereich(Bereich)))
                Catch ex As Exception
                End Try
            End If
        End With
        TabControl1.SelectedTab.Focus()
    End Sub
    Private Sub btnVorschau_Click(sender As Object, e As EventArgs) Handles btnVorschau.Click, btnPrint.Click, btnEdit.Click

        If Not CType(sender, Button).Name.Equals("btnEdit") AndAlso Not Wettkampf.Mannschaft Then
            If Wettkampf.Platzierung = 0 AndAlso lstGruppe.CheckedItems.Count = 0 Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Keine Gruppe ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                Return
            End If
        End If

        User.Check_MySQLDriver(Me)

        Cursor = Cursors.WaitCursor

        report = New FastReport.Report
        With report

#Region "Vorlage"
            If Vorlage(dicBereich(Bereich)) = String.Empty AndAlso cboVorlage.Text = String.Empty Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Keine Druckvorlage ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                Cursor = Cursors.Default
                Return
            End If
            Try
                .Load(Path.Combine(cboVorlage.SelectedValue.ToString, If(Not String.IsNullOrEmpty(Vorlage(dicBereich(Bereich))), Vorlage(dicBereich(Bereich)), cboVorlage.Text) + ".frx"))
                .Dictionary.Connections(0).ConnectionString = User.FR_ConnString

                btnVorschau.Enabled = False
            Catch ex As Exception
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Vorlage nicht gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
                Cursor = Cursors.Default
                Return
            End Try
#End Region

#Region "Save Comment"
            Using DS As New DataService
                DS.Save_Wettkampf_Comments({Wettkampf.ID}, txtKommentar.Text, Me)
            End Using
#End Region

#Region "Daten Einzelauswertung"
            Dim dvGruppe = New DataView(dtGruppen)
            Dim dvDocs = New DataView(dtProtokoll)

            Dim _filter = String.Empty
            Dim Einteilung = 0
            'Dim WhereClause = String.Empty
            'Dim OrderClause = String.Empty

            If Wettkampf.Mannschaft Then
                ' keinerlei Auswahlmöglichkeit, a_k rausfiltern
                dvDocs.Sort = "TeamID, Gruppe, Heben DESC, Startnummer, Nachname, Vorname"
                'dvDocs.RowFilter = "a_K = False"
                dvGruppe.RowFilter = "Gruppe = 1"
            ElseIf TabControl1.SelectedTab.Equals(tabGruppen) AndAlso CStr(Wettkampf.Platzierung)(0) = "1" AndAlso lstGruppe.CheckedItems.Count = 0 Then
                ' Protokoll für gesamten WK --> keine Gruppe ausgewählt
                dvDocs.RowFilter = "attend = True"
                dvDocs.Sort = "Platz"
                dvGruppe.RowFilter = Nothing
                Einteilung = 1
            ElseIf TabControl1.SelectedTab.Equals(tabGruppen) AndAlso lstGruppe.CheckedItems.Count > 0 Then
                ' Protokoll für ausgewählte Gruppe(n) drucken
                For Each Check As DataRowView In lstGruppe.CheckedItems
                    _filter += "Gruppe = " + Check(0).ToString + " OR "
                Next
                _filter = Strings.Left(_filter, _filter.Length - 4)
                dvDocs.RowFilter = "attend = True AND (" + _filter + ")"
                Dim _sort = "Gruppe"
                If Wettkampf.Alterseinteilung.Equals("Altersklassen") Then _sort += ", Sex DESC, idAK"
                If Wettkampf.Gewichtseinteilung.Equals("Gewichtsklassen") Then _sort += ", idGK"
                _sort += ", Platz"
                dvDocs.Sort = _sort ' & If(Wettkampf.Alterseinteilung.Equals("Jahrgang"), ", Jahrgang", "")
                dvGruppe.RowFilter = _filter
                Einteilung = 2
            ElseIf Not TabControl1.SelectedTab.Equals(tabVereine) AndAlso Not TabControl1.SelectedTab.Equals(tabLänder) Then
                ' Protokoll für ausgewählte Altersklasse(n) und/oder ausgewählte Gewichtsklasse(n)
                Dim sex = "m"
                If TabControl1.SelectedTab.Equals(tabWeiblich) Then sex = "w"
                If dicLists(sex)(1).CheckedItems.Count > 0 Then
                    ' Gewichtsklasse(n) ausgewählt --> es ist nur eine AK ausgewählt
                    For Each Check As DataRowView In dicLists(sex)(1).CheckedItems
                        _filter += "idGK = " + Check(3).ToString + " Or "
                    Next
                    _filter = Strings.Left(_filter, _filter.Length - 4)
                    dvDocs.RowFilter = "attend = True And Sex = '" & sex & "' AND idAK = " & CType(dicLists(sex)(0).CheckedItems(0), DataRowView)(1).ToString & " AND (" + _filter + ")"
                    dvDocs.Sort = "idAK, idGK, Platz"
                    Einteilung = 4
                ElseIf dicLists(sex)(0).CheckedItems.Count > 0 Then
                    ' keine Gewichtsklasse(n) ausgewählt --> Altersklasse(n) 
                    For Each Check As DataRowView In dicLists(sex)(0).CheckedItems
                        _filter += "idAK = " + Check(1).ToString + " Or "
                    Next
                    _filter = Strings.Left(_filter, _filter.Length - 4)
                    dvDocs.RowFilter = "attend = True And Sex = '" & sex & "' AND (" + _filter + ")"
                    dvDocs.Sort = "idAK, Platz"
                    Einteilung = 3
                End If
                Dim x As List(Of Integer) = dvDocs.ToTable.AsEnumerable().Select(Function(s) CInt(s("Gruppe"))).ToList()
                x.Sort()
                Dim _f = String.Empty
                For Each g In x
                    _f += "Gruppe = " + g.ToString + " OR "
                Next
                _f = Strings.Left(_f, _f.Length - 4)
                dvGruppe.RowFilter = _f
            End If
            dvGruppe.Sort = "Gruppe"
#End Region

#Region "Create"
            Dim Wertung = CInt(Glob.dtModus.Select("idModus = " & Wettkampf.Modus)(0)("Wertung"))
            Dim NK_Stellen = Get_NK_Stellen(Wertung)
            Try
                ' WK-Tabelle
                If Wettkampf.Identities.Count = 1 Then
                    .RegisterData(Glob.dtWK, "wk")
                Else
                    Dim wk = New DataView(Glob.dtWK, "Wettkampf = " & lstWettkampf.SelectedValue.ToString, Nothing, DataViewRowState.CurrentRows)
                    .RegisterData(wk.ToTable, "wk")
                End If

                If Wettkampf.Mannschaft Then
                    .SetParameterValue("Wertung", Wertung)
                    .SetParameterValue("NK_Stellen", NK_Stellen)

                    .RegisterData(dvDocs.ToTable, "protokoll")
                    .RegisterData(dvGruppe.ToTable, "gruppen")
                    .RegisterData(Glob.dtTeams, "teams")

                ElseIf TabControl1.SelectedTab.Equals(tabVereine) OrElse TabControl1.SelectedTab.Equals(tabLänder) Then
                    Dim ix = CInt(TabControl1.SelectedTab.Tag)
                    .RegisterData(dvTeams(ix).ToTable, "team")
                    .RegisterData(dvHeber(ix).ToTable, "heber")
                Else
                    Dim Platzierung = Wettkampf.Platzierung.ToString

                    .SetParameterValue("Wertung", Wertung)
                    .SetParameterValue("NurWertung", chkWertung2.Checked) ' Wertung2 ausblenden
                    .SetParameterValue("Platzierung", Wettkampf.Platzierung.ToString)
                    .SetParameterValue("NK_Stellen", NK_Stellen)
                    .SetParameterValue("GroupCount", dvGruppe.Count) ' Anzahl Spalten für Kampfrichter
                    .SetParameterValue("NewPage", If(chkNewPage.Enabled, chkNewPage.Checked, False))
                    .SetParameterValue("Einteilung", Einteilung)
                    .SetParameterValue("Alterseinteilung", Wettkampf.Alterseinteilung)

                    If Wettkampf.Athletik Then
                        .SetParameterValue("Athletik_1", If(dtAthletik.Rows.Count > 0, dtAthletik.Rows(0)!Bezeichnung, String.Empty))
                        .SetParameterValue("Athletik_2", If(dtAthletik.Rows.Count > 1, dtAthletik.Rows(1)!Bezeichnung, String.Empty))
                        .SetParameterValue("Athletik_3", If(dtAthletik.Rows.Count > 2, dtAthletik.Rows(2)!Bezeichnung, String.Empty))
                        .SetParameterValue("Athletik_4", If(dtAthletik.Rows.Count > 3, dtAthletik.Rows(3)!Bezeichnung, String.Empty))
                        If dtAthletik.Rows.Count = 3 AndAlso Not dtProtokoll.Columns.Contains("A_4") Then
                            dtProtokoll.Columns.Add(New DataColumn With {.ColumnName = "A_4", .DataType = GetType(Double)})
                            dtProtokoll.Columns.Add(New DataColumn With {.ColumnName = "AW_4", .DataType = GetType(Double)})
                        End If
                        Dim _format = {0, 0, 0, 0}
                        For i = 0 To 3
                            Try
                                Select Case CInt(dtAthletik.Rows(i)!Disziplin)
                                    Case 65, 80, 120
                                        _format(i) = 2
                                End Select
                            Catch ex As Exception
                            End Try
                        Next
                        .SetParameterValue("NK_Stellen", String.Join(",", _format))
                        .SetParameterValue("Hide_KR", Not chkHide.Enabled OrElse chkHide.Checked)
                    Else
                        .SetParameterValue("HideHeaders", chkHide.Enabled AndAlso chkHide.Checked)
                    End If
                    .SetParameterValue("Technik", Wettkampf.Technikwertung)

                    .RegisterData(dvDocs.ToTable, "protokoll")
                    .RegisterData(dvGruppe.ToTable, "gruppen")
                    .RegisterData(Glob.dtGewicht, "gewichtsteiler")

                End If
            Catch ex As Exception
                LogMessage("Drucken_Protokoll: Daten: " & Bereich & ": " & ex.Message, ex.StackTrace)
            End Try
#End Region

#Region "Print"
            Try
                Dim s = New FastReport.EnvironmentSettings
                s.ReportSettings.ShowProgress = False
                If sender.Equals(btnEdit) Then
                    .Design()
                Else
                    .Prepare(True)
                    Try
                        If CType(sender, Button).Tag.ToString.Equals("Show") Then
                            .ShowPrepared()
                        ElseIf CType(sender, Button).Tag.ToString.Equals("Print") Then
                            .PrintPrepared()
                        ElseIf CType(sender, Button).Tag.ToString.Equals("Edit") Then
                            .Design()
                        End If
                    Catch ex As Exception
                        Using New Centered_MessageBox(Me)
                            MessageBox.Show("Abbruch wegen Fehler im Formular." & vbNewLine & "Weitere Informationen im Log-File.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        LogMessage("Drucken_Protokoll: Print: " & Bereich & ": " & ex.Message, ex.StackTrace)
                    End Try
                End If
            Catch ex As Exception
                LogMessage("Drucken_Protokoll: Prepare :" & Bereich & ": " & ex.Message, ex.StackTrace)
            End Try
#End Region
        End With

        btnVorschau.Enabled = True
        Cursor = Cursors.Default
    End Sub
    Private Sub btnVorschau_EnabledChanged(sender As Object, e As EventArgs) Handles btnVorschau.EnabledChanged
        btnPrint.Enabled = btnVorschau.Enabled
    End Sub

    Dim CurrentRow(1) As Integer
    Dim RowExpanded(1) As Integer
    Private Sub dgvJoin_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvVerein.CellClick, dgvLänder.CellClick
        Dim ctl = CType(sender, DataGridView)
        Dim ix = CInt(ctl.Tag)
        If e.RowIndex > -1 AndAlso ctl.Columns(e.ColumnIndex).Name.Equals("Expand") Then
            bsHeber(ix).Filter = Filter(ix) & " = " & dvTeams(ix)(bsTeams(ix).Position)(Filter(ix)).ToString
            ctl.Rows(CurrentRow(ix)).DividerHeight = 0
            With dgvHeber
                .Visible = RowExpanded(ix) <> e.RowIndex OrElse Not .Visible
                If Not .Visible Then Return
                .Height = .Rows.Count * 22 + 3
                Dim w = 0
                For i = 0 To .Columns.Count - 1
                    If .Columns(i).Visible Then w += .Columns(i).Width
                Next
                .Width = w + 1
                RowExpanded(ix) = e.RowIndex
                Dim rect = ctl.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, True)
                Dim x = ctl.Left + rect.X + rect.Width
                Dim y = ctl.Top + rect.Y + rect.Height
                ctl.Rows(e.RowIndex).DividerHeight = .Height
                CurrentRow(ix) = e.RowIndex
                .Location = New Point(x, y - 1)
                .ClearSelection()
                .BringToFront()
            End With
        End If
    End Sub
    Private Sub dgvJoin_Scroll(sender As Object, e As ScrollEventArgs) Handles dgvVerein.Scroll, dgvLänder.Scroll
        Dim ctl = CType(sender, DataGridView)
        Dim ix = CInt(ctl.Tag)
        With dgvHeber
            If .Visible OrElse ctl.Rows(CurrentRow(ix)).DividerHeight > 0 Then
                Dim Scroll = e.OldValue - e.NewValue
                Dim y = .Top + ctl.RowTemplate.Height * Scroll
                .Visible = CurrentRow(ix) >= ctl.FirstDisplayedScrollingRowIndex
                .Location = New Point(.Left, y)
            End If
        End With
    End Sub

    Private Sub dgvHeber_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvHeber.CellEnter
        dgvHeber.ClearSelection()
    End Sub
    Private Sub dgvHeber_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvHeber.KeyDown
        If e.KeyCode = Keys.Escape Then
            e.Handled = True
            dgvHeber.Visible = False
        End If
    End Sub

    Private Sub Control_GotFocus(sender As Object, e As EventArgs) Handles btnCancel.GotFocus, btnEdit.GotFocus, btnKR.GotFocus, btnPrint.GotFocus, btnSearch.GotFocus, btnVorschau.GotFocus, cboVorlage.GotFocus
        Try
            dgvHeber.Visible = False
            If TabControl1.SelectedTab.Equals(tabVereine) Then
                dgvVerein.Rows(CurrentRow(0)).DividerHeight = 0
            ElseIf TabControl1.SelectedTab.Equals(tabLänder) Then
                dgvLänder.Rows(CurrentRow(1)).DividerHeight = 0
            End If
        Catch ex As Exception
        End Try
    End Sub

End Class