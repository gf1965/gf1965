﻿
Imports System.Runtime.InteropServices

Module Ini
    'Public App_IniFile As String = Path.Combine(Application.StartupPath, "Gewichtheben.ini")
    Const MAX_ENTRY = 1024
    Public NotInheritable Class Interaction
        Private Sub New()
        End Sub
        Public Shared Sub INI_Write(ByRef strBereich As String, ByRef strKey As String, ByRef strWert As String, ByRef strIniDatei As String)
            INI_Write(strBereich, strKey, strWert, strIniDatei)
        End Sub
        Public Shared Function INI_Read(ByRef strBereich As String, ByRef strKey As String, ByRef strIniDatei As String) As String
            Try
                Return INI_Read(strBereich, strKey, strIniDatei)
            Catch ex As Exception
                Return "?"
            End Try
        End Function

        Public Shared Function INI_GetSectionNames(ByRef strIniDatei As String) As ArrayList
            Return INI_GetSectionNames(strIniDatei)
        End Function

    End Class

    Friend NotInheritable Class NativeMethods
        Private Sub New()
        End Sub

        Private Declare Ansi Function INI_WritePrivateProfileString Lib "kernel32.dll" Alias "WritePrivateProfileStringA" (
                         lpApplicationName As String,
                         lpKeyName As String,
                         lpString As String,
                         lpFileName As String) As <MarshalAs(UnmanagedType.I4)> Integer

        Private Declare Ansi Function INI_GetPrivateProfileString Lib "kernel32.dll" Alias "GetPrivateProfileStringA" (
                         lpApplicationName As String,
                         lpKeyName As String,
                         lpDefault As String,
                         lpReturnedString As StringBuilder,
                         nSize As Integer,
                         lpFileName As String) As <MarshalAs(UnmanagedType.I4)> Integer

        Private Declare Ansi Function GetPrivateProfileSectionNames Lib "kernel32" Alias "GetPrivateProfileSectionNamesA" (
                         lpszReturnBuffer() As Byte,
                         nSize As Integer,
                         lpFileName As String) As <MarshalAs(UnmanagedType.I4)> Integer

        Private Declare Ansi Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (
                         Section As String,
                         sReturnedString As String,
                         lSize As Long,
                         FileName As String) As <MarshalAs(UnmanagedType.I4)> Integer

        Private Declare Ansi Function GetPrivateProfileNames Lib "kernel32.dll" Alias "GetPrivateProfileStringA" (
                         lpApplicationName As String,
                         lpKeyName As Integer,
                         lpDefault As String,
                         lpszReturnBuffer() As Byte,
                         nSize As Integer,
                         lpFileName As String) As <MarshalAs(UnmanagedType.I4)> Integer

        Friend Shared Sub INI_Write(ByRef strBereich As String, ByRef strKey As String, ByRef strWert As String, ByRef strIniDatei As String)
            On Error Resume Next
            Call INI_WritePrivateProfileString(strBereich, strKey, strWert, strIniDatei)
        End Sub

        Friend Shared Function INI_Read(ByRef strBereich As String, ByRef strKey As String, ByRef strIniDatei As String) As String
            'On Error Resume Next
            Try
                Dim sb As New StringBuilder(10000)
                Dim Ret As Integer = INI_GetPrivateProfileString(strBereich, strKey, "?", sb, 10000, strIniDatei)
                Return sb.ToString
            Catch ex As Exception
                Return "?"
            End Try
        End Function

        Friend Shared Function INI_GetSectionNames(ByRef strIniDatei As String) As ArrayList
            Try
                Dim buffer(MAX_ENTRY) As Byte
                GetPrivateProfileSectionNames(buffer, MAX_ENTRY, strIniDatei)
                Dim parts() As String = Encoding.ASCII.GetString(buffer).Trim(ControlChars.NullChar).Split(ControlChars.NullChar)
                Return New ArrayList(parts)
            Catch
            End Try
            Return Nothing
        End Function

        Friend Shared Function INI_GetKeyNames(section As String, strIniFile As String) As ArrayList
            Const Max_Entry = 1024
            Try
                Dim buffer(Max_Entry) As Byte
                GetPrivateProfileNames(section, 0, "#empty#", buffer, Max_Entry, strIniFile)
                Dim parts() As String = Encoding.ASCII.GetString(buffer).Trim(ControlChars.NullChar).Split(ControlChars.NullChar)
                Return New ArrayList(parts)
            Catch
            End Try
            Return Nothing
        End Function

    End Class


End Module
