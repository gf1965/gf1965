﻿Imports MySqlConnector

Public Class Import_Athleten

    Private Event Connect_Database()
    Private InetState As String
    Private TableImport As DataTable
    Private TableStates As DataTable
    Private TableFailState As DataTable

    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        RemoveHandler Connect_Database, AddressOf Database_Connect
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
        AddHandler Connect_Database, AddressOf Database_Connect
    End Sub

    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        TableStates = New DataTable
        TableFailState = New DataTable
        TableFailState.Columns.Add(New DataColumn With {.ColumnName = "athlet_nation", .DataType = GetType(String)})
        TableFailState.Columns.Add(New DataColumn With {.ColumnName = "state_id", .DataType = GetType(Integer), .DefaultValue = DBNull.Value})

        Using conn As New MySqlConnection(User.ConnString)
            conn.Open()
            Dim cmd As New MySqlCommand("select * from staaten;", conn)
            TableStates.Load(cmd.ExecuteReader)
        End Using

        Timer1.Start()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        InetState = CheckInetConnection()
        If Not String.IsNullOrEmpty(InetState) Then
            Timer1.Stop()
            RaiseEvent Connect_Database()
        End If
    End Sub

    Private Sub Database_Connect()
        If Timer1.Enabled Then Return
        Cursor = Cursors.Default
        Dim msg = "Status der Internetverbindung: " & InetState.ToUpper
        ListBox1.Items.Add("- " & msg)
        Do
            Select Case InetState
                Case "Offline", "Not Connected", "Failed"
                    Using New Centered_MessageBox(Me)
                        If MessageBox.Show("Der Vorgang kann nicht abgeschlossen werden." & vbNewLine & "(" & msg & ")",
                                            msgCaption,
                                            MessageBoxButtons.RetryCancel,
                                            MessageBoxIcon.Exclamation) = DialogResult.Retry Then
                            InetState = String.Empty
                            ListBox1.Items.Add("- teste Internet-Verbindung")
                            Cursor = Cursors.WaitCursor
                            Timer1.Start()
                        Else
                            Close()
                        End If
                    End Using
                    Return
                Case Else
                    Exit Do
            End Select
        Loop
        Button1.Enabled = True
        Button1.Focus()
        Get_Athlets()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Cursor = Cursors.WaitCursor
        ListBox1.Items.Add("- verarbeite Tabelle <Athleten>")
        ProgressBar1.Maximum = TableImport.Rows.Count
        ProgressBar1.Value = 0

        Dim Query = "INSERT INTO athleten (idTeilnehmer, GUID, Nachname, Vorname, Verein, ESR, MSR, Jahrgang, Geburtstag, Geschlecht, Staat) 
                    VALUES (@id, @guid, @nn, @vn, @verein, @esr, @msr, @yob, @dob, @sex, @staat) 
                    ON DUPLICATE KEY UPDATE Nachname = @nn, 
                                            Vorname = @vn, 
                                            Verein = @verein, 
                                            ESR = @esr, 
                                            MSR = @msr, 
                                            Jahrgang = @yob, 
                                            Geburtstag = @dob, 
                                            Geschlecht = @sex, 
                                            Staat = @staat;"
        Try
            Using conn As New MySqlConnection(User.ConnString)
                conn.Open()
                For Each row As DataRow In TableImport.Rows
                    Dim cmd = New MySqlCommand(Query, conn)
                    Dim Verein_Id = Get_Verein(conn, row!verein_cas_id.ToString)
                    With cmd.Parameters
                        .AddWithValue("@id", row!athleten_id)
                        .AddWithValue("@guid", row!athleten_cas_id)
                        .AddWithValue("@nn", row!athlet_name)
                        .AddWithValue("@vn", row!athlet_vorname)
                        .AddWithValue("@dob", row!geburtstag)
                        .AddWithValue("@yob", row!athlet_gebjahr)
                        .AddWithValue("@sex", row!athlet_geschlecht.ToString(0))
                        '.AddWithValue("@v_uid", row!verein_cas_id)
                        .AddWithValue("@verein", If(CInt(row!startrecht_typ) = 2, DBNull.Value, Verein_Id))
                        .AddWithValue("@esr", If(CInt(row!startrecht_typ) = 2, DBNull.Value, Verein_Id))
                        .AddWithValue("@msr", Verein_Id)
                        .AddWithValue("@staat", Get_State(row!athlet_nation.ToString))
                    End With
                    'cmd.ExecuteNonQuery()
                    ProgressBar1.Value += 1
                Next
            End Using
            If TableFailState.Rows.Count = 0 Then
                ListBox1.Items.Add("- Verarbeitung abgeschlossen")
            Else
                ListBox1.Items.Add("- Fehler bei der Verarbeitung (Staat nicht gefunden)")
            End If
        Catch ex As Exception
            ListBox1.Items.Add("- Fehler bei der Verarbeitung (abgebrochen)")
        End Try
        Cursor = Cursors.Default
    End Sub

    Private Sub Get_Athlets()
        Cursor = Cursors.WaitCursor
        ListBox1.Items.Add("- lade Tabelle <Athleten>")
        TableImport = New DataTable
        Try
            Using conn As New MySqlConnection(User.BVDG_ConnString.Result)
                conn.Open()
                Dim cmd = New MySqlCommand("select * from athleten where startrecht_typ > 1;", conn)
                TableImport.Load(cmd.ExecuteReader())
            End Using
            Using conn As New MySqlConnection(User.ConnString)
                conn.Open()
                Dim cmd = New MySqlCommand("select count(*) from athleten;", conn)
                Dim reader = cmd.ExecuteReader
                If reader.HasRows Then
                    reader.Read()
                    If CInt(reader(0)) > 0 Then Button1.Text = "Aktualisieren"
                End If
                reader.Close()
            End Using
        Catch ex As Exception
        End Try
        ListBox1.Items.Add("- " & TableImport.Rows.Count & " Athleten bereit für Import/Update")
        Cursor = Cursors.Default
    End Sub

    Private Function Get_Verein(conn As MySqlConnection, guid As String) As Object
        Dim id As Object = 0
        Dim cmd = New MySqlCommand("select idVerein from verein where GUID = @guid;", conn)
        cmd.Parameters.AddWithValue("@guid", guid)
        Dim reader = cmd.ExecuteReader
        If reader.HasRows Then
            reader.Read()
            If Not IsDBNull(reader(0)) Then id = reader(0)
        End If
        reader.Close()
        Return id
    End Function

    Private Function Get_State(Nation As String, Optional DataField As String = "state_name") As Object
        Dim StateId As Object = 0
        For i = 1 To Nation.Length
            Dim p = i
            Dim Ids = From x In TableStates
                      Where x.Field(Of String)(DataField).ToLower Like Nation.ToLower.Substring(0, p) & "*"
            If Ids.Count = 1 Then
                Return Ids(0)("state_id")
            ElseIf Ids.Count = 0 AndAlso DataField = "state_name" Then
                Get_State(Nation, "state_long")
                Exit For
            ElseIf Ids.Count = 0 AndAlso DataField = "state_long" Then
                Get_State(Nation, "state_iso3")
                Exit For
            ElseIf Ids.Count = 0 AndAlso DataField = "state_iso3" Then
                TableFailState.Rows.Add({Nation})
                Exit For
            End If
        Next
        Return 0 'StateId
    End Function
End Class