﻿Module ControlScaling

    Public PanelScale As New ControlScale

    Class ControlScale
        Private X, Y As Double 'Vergrößerungsfaktor

        Public Sub SetWindow(X_Faktor As Single, Y_Faktor As Single, pnl As Panel, Location As Point)
            X = X_Faktor * 0.8
            Y = Y_Faktor * 0.8
            With pnl
                Dim X_Resolution As Integer = .Width
                Dim Y_Resolution As Integer = .Height
                'Panel skalieren
                .SetBounds(.Top, .Left, CInt(X * .MinimumSize.Width), CInt(Y * .MinimumSize.Height))
                'Skalierung der Controls berechnen
                X = .Width / X_Resolution
                Y = .Height / Y_Resolution
                'Controls skalieren 
                For Each ctl As Control In .Controls
                    CtlLoop(ctl)
                Next ctl
            End With
        End Sub

        Public Sub CtlLoop(ByVal Ctl As Control)
            ControlResize(Ctl)
            If Ctl.HasChildren = True Then
                ' vorhandene UnterControls rekursiv durchlaufen
                For Each c As Control In Ctl.Controls
                    ControlResize(c)
                Next
            End If
        End Sub

        Public Sub ControlResize(ByVal Ctl As Control)
            With Ctl
                Try
                    If Not Ctl.GetType.ToString.Contains("UpDownBase") And Not Ctl.GetType = GetType(Panel) Then
                        If TypeOf Ctl IsNot RichTextBox Then
                            .Font = New Font(.Font.FontFamily, .Font.Size * CSng(X), .Font.Style, GraphicsUnit.Point)
                        End If
                    End If
                Catch
                End Try
                .SetBounds(CInt(.Left * X), CInt(.Top * Y), CInt(.Width * X), CInt(.Height * Y))
            End With
        End Sub
    End Class

End Module
