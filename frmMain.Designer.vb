﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel()
        Me.mnuMain = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLogin = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSperren = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem12 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuPasswort = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuVorbereitung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWettkampf = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWK_New = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWK_Edit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWK_Delete = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuWK_Select = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuClearMeldung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuClearWiegen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuClearHeben = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuMeldung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuGruppen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuZeitplan = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKampfgericht = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWiegen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDruck = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMeldeliste = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuGruppenlisten = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuQuittungen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStarterlisten = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStartkarten = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWiegelisten = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuZeitplan_Print = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuGW = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuGewichtheben = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKorrektur = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSiegerehrung = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem15 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuEasyModus = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuPause = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAthletik = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAnristen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBankdrücken = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBeugestütz = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDifferenzsprung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKlimmzug = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPendellauf = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDreisprung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWeitsprung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSprint = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSchocken = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAuswertung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProtokoll = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuUrkunden = New System.Windows.Forms.ToolStripMenuItem()
        Me.div1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuLänderwertung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMannschaftswertung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuVereinswertung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAnzeige = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExport_Bundesliga = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExportMeisterschaft = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem14 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuExport_Ergebnisse = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExport_Wiegeliste = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuImport = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuImport_Bundesliga = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuImport_Meisterschaft = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem11 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuImport_Meldeliste = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuImport_Ergebnisse = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuImport_Heber = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuImport_Wiegeliste = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDatenbank = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuAthleten = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAthletenAktualisieren = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem18 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuTeams = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuVerein = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWorldRecords = New System.Windows.Forms.ToolStripMenuItem()
        Me.nmuModus = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem9 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuCalculator = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOnlineVerwaltung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDruckVorlagen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuHideMessages = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTopMost = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAbZeichen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHantelTest = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuJuryBoard = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKR_Pad = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuMauszeiger = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem16 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuWerbung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTVTest = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHilfe = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHilfeAnzeigen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSupport = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWebShop = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem10 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuUpdate = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem13 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuRegistrieren = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuInfo = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.StatusStrip2 = New System.Windows.Forms.StatusStrip()
        Me.lblDBConnection = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblExtern = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblWettkampf = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblUser = New System.Windows.Forms.ToolStripStatusLabel()
        Me.mnuMain.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "btnGgrey.png")
        Me.ImageList1.Images.SetKeyName(1, "btnGreen.png")
        Me.ImageList1.Images.SetKeyName(2, "btnRed.png")
        '
        'OpenFileDialog
        '
        Me.OpenFileDialog.SupportMultiDottedExtensions = True
        '
        'BottomToolStripPanel
        '
        resources.ApplyResources(Me.BottomToolStripPanel, "BottomToolStripPanel")
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        '
        'TopToolStripPanel
        '
        resources.ApplyResources(Me.TopToolStripPanel, "TopToolStripPanel")
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        '
        'RightToolStripPanel
        '
        resources.ApplyResources(Me.RightToolStripPanel, "RightToolStripPanel")
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        '
        'LeftToolStripPanel
        '
        resources.ApplyResources(Me.LeftToolStripPanel, "LeftToolStripPanel")
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        '
        'ContentPanel
        '
        resources.ApplyResources(Me.ContentPanel, "ContentPanel")
        '
        'mnuMain
        '
        Me.mnuMain.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuVorbereitung, Me.mnuGW, Me.mnuAthletik, Me.mnuAuswertung, Me.mnuAnzeige, Me.mnuExport, Me.mnuImport, Me.mnuDatenbank, Me.mnuOptions, Me.mnuExtras, Me.mnuHilfe})
        Me.mnuMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        resources.ApplyResources(Me.mnuMain, "mnuMain")
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.mnuMain.ShowItemToolTips = True
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLogin, Me.mnuSperren, Me.ToolStripMenuItem12, Me.mnuPasswort, Me.ToolStripMenuItem8, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        resources.ApplyResources(Me.mnuDatei, "mnuDatei")
        '
        'mnuLogin
        '
        Me.mnuLogin.Name = "mnuLogin"
        resources.ApplyResources(Me.mnuLogin, "mnuLogin")
        '
        'mnuSperren
        '
        Me.mnuSperren.Name = "mnuSperren"
        resources.ApplyResources(Me.mnuSperren, "mnuSperren")
        '
        'ToolStripMenuItem12
        '
        Me.ToolStripMenuItem12.Name = "ToolStripMenuItem12"
        resources.ApplyResources(Me.ToolStripMenuItem12, "ToolStripMenuItem12")
        '
        'mnuPasswort
        '
        Me.mnuPasswort.Name = "mnuPasswort"
        resources.ApplyResources(Me.mnuPasswort, "mnuPasswort")
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        resources.ApplyResources(Me.ToolStripMenuItem8, "ToolStripMenuItem8")
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        resources.ApplyResources(Me.mnuBeenden, "mnuBeenden")
        '
        'mnuVorbereitung
        '
        Me.mnuVorbereitung.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuWettkampf, Me.ToolStripMenuItem2, Me.mnuMeldung, Me.mnuGruppen, Me.mnuZeitplan, Me.mnuKampfgericht, Me.mnuWiegen, Me.ToolStripMenuItem1, Me.mnuDruck})
        Me.mnuVorbereitung.Name = "mnuVorbereitung"
        resources.ApplyResources(Me.mnuVorbereitung, "mnuVorbereitung")
        '
        'mnuWettkampf
        '
        Me.mnuWettkampf.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuWK_New, Me.mnuWK_Edit, Me.mnuWK_Delete, Me.ToolStripMenuItem4, Me.mnuWK_Select, Me.ToolStripMenuItem3, Me.mnuClearMeldung, Me.mnuClearWiegen, Me.mnuClearHeben})
        Me.mnuWettkampf.Name = "mnuWettkampf"
        resources.ApplyResources(Me.mnuWettkampf, "mnuWettkampf")
        '
        'mnuWK_New
        '
        Me.mnuWK_New.Image = Global.Gewichtheben.My.Resources.Resources.Add2
        Me.mnuWK_New.Name = "mnuWK_New"
        resources.ApplyResources(Me.mnuWK_New, "mnuWK_New")
        '
        'mnuWK_Edit
        '
        Me.mnuWK_Edit.Image = Global.Gewichtheben.My.Resources.Resources.edit16
        Me.mnuWK_Edit.Name = "mnuWK_Edit"
        resources.ApplyResources(Me.mnuWK_Edit, "mnuWK_Edit")
        '
        'mnuWK_Delete
        '
        Me.mnuWK_Delete.Image = Global.Gewichtheben.My.Resources.Resources.Delete21
        Me.mnuWK_Delete.Name = "mnuWK_Delete"
        resources.ApplyResources(Me.mnuWK_Delete, "mnuWK_Delete")
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        resources.ApplyResources(Me.ToolStripMenuItem4, "ToolStripMenuItem4")
        '
        'mnuWK_Select
        '
        Me.mnuWK_Select.Image = Global.Gewichtheben.My.Resources.Resources.Zoom16
        Me.mnuWK_Select.Name = "mnuWK_Select"
        resources.ApplyResources(Me.mnuWK_Select, "mnuWK_Select")
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        resources.ApplyResources(Me.ToolStripMenuItem3, "ToolStripMenuItem3")
        '
        'mnuClearMeldung
        '
        Me.mnuClearMeldung.Image = Global.Gewichtheben.My.Resources.Resources.Exclamation24
        Me.mnuClearMeldung.Name = "mnuClearMeldung"
        resources.ApplyResources(Me.mnuClearMeldung, "mnuClearMeldung")
        '
        'mnuClearWiegen
        '
        Me.mnuClearWiegen.Image = Global.Gewichtheben.My.Resources.Resources.Exclamation24
        Me.mnuClearWiegen.Name = "mnuClearWiegen"
        resources.ApplyResources(Me.mnuClearWiegen, "mnuClearWiegen")
        '
        'mnuClearHeben
        '
        Me.mnuClearHeben.Image = Global.Gewichtheben.My.Resources.Resources.Exclamation24
        Me.mnuClearHeben.Name = "mnuClearHeben"
        resources.ApplyResources(Me.mnuClearHeben, "mnuClearHeben")
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        resources.ApplyResources(Me.ToolStripMenuItem2, "ToolStripMenuItem2")
        '
        'mnuMeldung
        '
        Me.mnuMeldung.Name = "mnuMeldung"
        resources.ApplyResources(Me.mnuMeldung, "mnuMeldung")
        '
        'mnuGruppen
        '
        Me.mnuGruppen.Name = "mnuGruppen"
        resources.ApplyResources(Me.mnuGruppen, "mnuGruppen")
        '
        'mnuZeitplan
        '
        Me.mnuZeitplan.Name = "mnuZeitplan"
        resources.ApplyResources(Me.mnuZeitplan, "mnuZeitplan")
        '
        'mnuKampfgericht
        '
        Me.mnuKampfgericht.Name = "mnuKampfgericht"
        resources.ApplyResources(Me.mnuKampfgericht, "mnuKampfgericht")
        '
        'mnuWiegen
        '
        Me.mnuWiegen.Name = "mnuWiegen"
        resources.ApplyResources(Me.mnuWiegen, "mnuWiegen")
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        resources.ApplyResources(Me.ToolStripMenuItem1, "ToolStripMenuItem1")
        '
        'mnuDruck
        '
        Me.mnuDruck.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMeldeliste, Me.mnuGruppenlisten, Me.mnuQuittungen, Me.mnuStarterlisten, Me.mnuStartkarten, Me.mnuWiegelisten, Me.mnuZeitplan_Print})
        Me.mnuDruck.Name = "mnuDruck"
        resources.ApplyResources(Me.mnuDruck, "mnuDruck")
        '
        'mnuMeldeliste
        '
        Me.mnuMeldeliste.Name = "mnuMeldeliste"
        resources.ApplyResources(Me.mnuMeldeliste, "mnuMeldeliste")
        Me.mnuMeldeliste.Tag = "Meldeliste"
        '
        'mnuGruppenlisten
        '
        Me.mnuGruppenlisten.Name = "mnuGruppenlisten"
        resources.ApplyResources(Me.mnuGruppenlisten, "mnuGruppenlisten")
        Me.mnuGruppenlisten.Tag = "Gruppenliste"
        '
        'mnuQuittungen
        '
        Me.mnuQuittungen.Name = "mnuQuittungen"
        resources.ApplyResources(Me.mnuQuittungen, "mnuQuittungen")
        Me.mnuQuittungen.Tag = "Quittung"
        '
        'mnuStarterlisten
        '
        Me.mnuStarterlisten.Name = "mnuStarterlisten"
        resources.ApplyResources(Me.mnuStarterlisten, "mnuStarterlisten")
        Me.mnuStarterlisten.Tag = "Starterliste"
        '
        'mnuStartkarten
        '
        Me.mnuStartkarten.Name = "mnuStartkarten"
        resources.ApplyResources(Me.mnuStartkarten, "mnuStartkarten")
        Me.mnuStartkarten.Tag = "Startkarte"
        '
        'mnuWiegelisten
        '
        Me.mnuWiegelisten.Name = "mnuWiegelisten"
        resources.ApplyResources(Me.mnuWiegelisten, "mnuWiegelisten")
        Me.mnuWiegelisten.Tag = "Wiegeliste"
        '
        'mnuZeitplan_Print
        '
        Me.mnuZeitplan_Print.Name = "mnuZeitplan_Print"
        resources.ApplyResources(Me.mnuZeitplan_Print, "mnuZeitplan_Print")
        Me.mnuZeitplan_Print.Tag = "Zeitplan"
        '
        'mnuGW
        '
        Me.mnuGW.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.mnuGW.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuGewichtheben, Me.mnuKorrektur, Me.mnuSiegerehrung, Me.ToolStripMenuItem15, Me.mnuEasyModus, Me.ToolStripMenuItem7, Me.mnuPause})
        resources.ApplyResources(Me.mnuGW, "mnuGW")
        Me.mnuGW.ForeColor = System.Drawing.Color.Firebrick
        Me.mnuGW.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.mnuGW.MergeIndex = 2
        Me.mnuGW.Name = "mnuGW"
        '
        'mnuGewichtheben
        '
        Me.mnuGewichtheben.ForeColor = System.Drawing.Color.Firebrick
        Me.mnuGewichtheben.Name = "mnuGewichtheben"
        resources.ApplyResources(Me.mnuGewichtheben, "mnuGewichtheben")
        '
        'mnuKorrektur
        '
        Me.mnuKorrektur.Name = "mnuKorrektur"
        resources.ApplyResources(Me.mnuKorrektur, "mnuKorrektur")
        '
        'mnuSiegerehrung
        '
        Me.mnuSiegerehrung.Name = "mnuSiegerehrung"
        resources.ApplyResources(Me.mnuSiegerehrung, "mnuSiegerehrung")
        '
        'ToolStripMenuItem15
        '
        Me.ToolStripMenuItem15.Name = "ToolStripMenuItem15"
        resources.ApplyResources(Me.ToolStripMenuItem15, "ToolStripMenuItem15")
        '
        'mnuEasyModus
        '
        Me.mnuEasyModus.Name = "mnuEasyModus"
        resources.ApplyResources(Me.mnuEasyModus, "mnuEasyModus")
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        resources.ApplyResources(Me.ToolStripMenuItem7, "ToolStripMenuItem7")
        '
        'mnuPause
        '
        Me.mnuPause.Name = "mnuPause"
        resources.ApplyResources(Me.mnuPause, "mnuPause")
        '
        'mnuAthletik
        '
        Me.mnuAthletik.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAnristen, Me.mnuBankdrücken, Me.mnuBeugestütz, Me.mnuDifferenzsprung, Me.mnuKlimmzug, Me.mnuPendellauf, Me.mnuDreisprung, Me.mnuWeitsprung, Me.mnuSprint, Me.mnuSchocken})
        Me.mnuAthletik.Name = "mnuAthletik"
        resources.ApplyResources(Me.mnuAthletik, "mnuAthletik")
        '
        'mnuAnristen
        '
        Me.mnuAnristen.Name = "mnuAnristen"
        resources.ApplyResources(Me.mnuAnristen, "mnuAnristen")
        '
        'mnuBankdrücken
        '
        Me.mnuBankdrücken.Name = "mnuBankdrücken"
        resources.ApplyResources(Me.mnuBankdrücken, "mnuBankdrücken")
        '
        'mnuBeugestütz
        '
        Me.mnuBeugestütz.Name = "mnuBeugestütz"
        resources.ApplyResources(Me.mnuBeugestütz, "mnuBeugestütz")
        '
        'mnuDifferenzsprung
        '
        Me.mnuDifferenzsprung.Name = "mnuDifferenzsprung"
        resources.ApplyResources(Me.mnuDifferenzsprung, "mnuDifferenzsprung")
        '
        'mnuKlimmzug
        '
        Me.mnuKlimmzug.Name = "mnuKlimmzug"
        resources.ApplyResources(Me.mnuKlimmzug, "mnuKlimmzug")
        '
        'mnuPendellauf
        '
        Me.mnuPendellauf.Name = "mnuPendellauf"
        resources.ApplyResources(Me.mnuPendellauf, "mnuPendellauf")
        '
        'mnuDreisprung
        '
        Me.mnuDreisprung.Name = "mnuDreisprung"
        resources.ApplyResources(Me.mnuDreisprung, "mnuDreisprung")
        '
        'mnuWeitsprung
        '
        Me.mnuWeitsprung.Name = "mnuWeitsprung"
        resources.ApplyResources(Me.mnuWeitsprung, "mnuWeitsprung")
        '
        'mnuSprint
        '
        Me.mnuSprint.Name = "mnuSprint"
        resources.ApplyResources(Me.mnuSprint, "mnuSprint")
        '
        'mnuSchocken
        '
        Me.mnuSchocken.Name = "mnuSchocken"
        resources.ApplyResources(Me.mnuSchocken, "mnuSchocken")
        '
        'mnuAuswertung
        '
        Me.mnuAuswertung.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuProtokoll, Me.mnuUrkunden, Me.div1, Me.mnuLänderwertung, Me.mnuMannschaftswertung, Me.mnuVereinswertung})
        Me.mnuAuswertung.Name = "mnuAuswertung"
        resources.ApplyResources(Me.mnuAuswertung, "mnuAuswertung")
        '
        'mnuProtokoll
        '
        Me.mnuProtokoll.Name = "mnuProtokoll"
        resources.ApplyResources(Me.mnuProtokoll, "mnuProtokoll")
        '
        'mnuUrkunden
        '
        Me.mnuUrkunden.Name = "mnuUrkunden"
        resources.ApplyResources(Me.mnuUrkunden, "mnuUrkunden")
        '
        'div1
        '
        Me.div1.Name = "div1"
        resources.ApplyResources(Me.div1, "div1")
        '
        'mnuLänderwertung
        '
        Me.mnuLänderwertung.Name = "mnuLänderwertung"
        resources.ApplyResources(Me.mnuLänderwertung, "mnuLänderwertung")
        Me.mnuLänderwertung.Tag = "Länderwertung"
        '
        'mnuMannschaftswertung
        '
        Me.mnuMannschaftswertung.Name = "mnuMannschaftswertung"
        resources.ApplyResources(Me.mnuMannschaftswertung, "mnuMannschaftswertung")
        Me.mnuMannschaftswertung.Tag = "Mannschaftswertung"
        '
        'mnuVereinswertung
        '
        Me.mnuVereinswertung.Name = "mnuVereinswertung"
        resources.ApplyResources(Me.mnuVereinswertung, "mnuVereinswertung")
        Me.mnuVereinswertung.Tag = "Vereinswertung"
        '
        'mnuAnzeige
        '
        Me.mnuAnzeige.BackColor = System.Drawing.SystemColors.AppWorkspace
        resources.ApplyResources(Me.mnuAnzeige, "mnuAnzeige")
        Me.mnuAnzeige.ForeColor = System.Drawing.Color.Firebrick
        Me.mnuAnzeige.Name = "mnuAnzeige"
        '
        'mnuExport
        '
        Me.mnuExport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExport_Bundesliga, Me.mnuExportMeisterschaft, Me.ToolStripMenuItem14, Me.mnuExport_Ergebnisse, Me.mnuExport_Wiegeliste})
        Me.mnuExport.Name = "mnuExport"
        resources.ApplyResources(Me.mnuExport, "mnuExport")
        '
        'mnuExport_Bundesliga
        '
        Me.mnuExport_Bundesliga.Name = "mnuExport_Bundesliga"
        resources.ApplyResources(Me.mnuExport_Bundesliga, "mnuExport_Bundesliga")
        '
        'mnuExportMeisterschaft
        '
        Me.mnuExportMeisterschaft.Name = "mnuExportMeisterschaft"
        resources.ApplyResources(Me.mnuExportMeisterschaft, "mnuExportMeisterschaft")
        '
        'ToolStripMenuItem14
        '
        Me.ToolStripMenuItem14.Name = "ToolStripMenuItem14"
        resources.ApplyResources(Me.ToolStripMenuItem14, "ToolStripMenuItem14")
        '
        'mnuExport_Ergebnisse
        '
        resources.ApplyResources(Me.mnuExport_Ergebnisse, "mnuExport_Ergebnisse")
        Me.mnuExport_Ergebnisse.Name = "mnuExport_Ergebnisse"
        '
        'mnuExport_Wiegeliste
        '
        Me.mnuExport_Wiegeliste.Name = "mnuExport_Wiegeliste"
        resources.ApplyResources(Me.mnuExport_Wiegeliste, "mnuExport_Wiegeliste")
        '
        'mnuImport
        '
        Me.mnuImport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuImport_Bundesliga, Me.mnuImport_Meisterschaft, Me.ToolStripMenuItem11, Me.mnuImport_Meldeliste, Me.mnuImport_Ergebnisse, Me.mnuImport_Heber, Me.mnuImport_Wiegeliste})
        Me.mnuImport.Name = "mnuImport"
        resources.ApplyResources(Me.mnuImport, "mnuImport")
        '
        'mnuImport_Bundesliga
        '
        Me.mnuImport_Bundesliga.Name = "mnuImport_Bundesliga"
        resources.ApplyResources(Me.mnuImport_Bundesliga, "mnuImport_Bundesliga")
        '
        'mnuImport_Meisterschaft
        '
        Me.mnuImport_Meisterschaft.Name = "mnuImport_Meisterschaft"
        resources.ApplyResources(Me.mnuImport_Meisterschaft, "mnuImport_Meisterschaft")
        '
        'ToolStripMenuItem11
        '
        Me.ToolStripMenuItem11.Name = "ToolStripMenuItem11"
        resources.ApplyResources(Me.ToolStripMenuItem11, "ToolStripMenuItem11")
        '
        'mnuImport_Meldeliste
        '
        Me.mnuImport_Meldeliste.Name = "mnuImport_Meldeliste"
        resources.ApplyResources(Me.mnuImport_Meldeliste, "mnuImport_Meldeliste")
        '
        'mnuImport_Ergebnisse
        '
        Me.mnuImport_Ergebnisse.Name = "mnuImport_Ergebnisse"
        resources.ApplyResources(Me.mnuImport_Ergebnisse, "mnuImport_Ergebnisse")
        '
        'mnuImport_Heber
        '
        Me.mnuImport_Heber.Name = "mnuImport_Heber"
        resources.ApplyResources(Me.mnuImport_Heber, "mnuImport_Heber")
        '
        'mnuImport_Wiegeliste
        '
        Me.mnuImport_Wiegeliste.Name = "mnuImport_Wiegeliste"
        resources.ApplyResources(Me.mnuImport_Wiegeliste, "mnuImport_Wiegeliste")
        '
        'mnuDatenbank
        '
        Me.mnuDatenbank.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator4, Me.mnuAthleten, Me.mnuAthletenAktualisieren, Me.ToolStripMenuItem18, Me.mnuTeams, Me.mnuVerein, Me.mnuWorldRecords, Me.nmuModus, Me.ToolStripMenuItem9, Me.mnuCalculator, Me.mnuOnlineVerwaltung})
        Me.mnuDatenbank.Name = "mnuDatenbank"
        resources.ApplyResources(Me.mnuDatenbank, "mnuDatenbank")
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        resources.ApplyResources(Me.ToolStripSeparator4, "ToolStripSeparator4")
        '
        'mnuAthleten
        '
        Me.mnuAthleten.Name = "mnuAthleten"
        resources.ApplyResources(Me.mnuAthleten, "mnuAthleten")
        '
        'mnuAthletenAktualisieren
        '
        Me.mnuAthletenAktualisieren.Name = "mnuAthletenAktualisieren"
        resources.ApplyResources(Me.mnuAthletenAktualisieren, "mnuAthletenAktualisieren")
        '
        'ToolStripMenuItem18
        '
        Me.ToolStripMenuItem18.Name = "ToolStripMenuItem18"
        resources.ApplyResources(Me.ToolStripMenuItem18, "ToolStripMenuItem18")
        '
        'mnuTeams
        '
        Me.mnuTeams.Name = "mnuTeams"
        resources.ApplyResources(Me.mnuTeams, "mnuTeams")
        '
        'mnuVerein
        '
        Me.mnuVerein.Name = "mnuVerein"
        resources.ApplyResources(Me.mnuVerein, "mnuVerein")
        '
        'mnuWorldRecords
        '
        Me.mnuWorldRecords.Name = "mnuWorldRecords"
        resources.ApplyResources(Me.mnuWorldRecords, "mnuWorldRecords")
        '
        'nmuModus
        '
        Me.nmuModus.Name = "nmuModus"
        resources.ApplyResources(Me.nmuModus, "nmuModus")
        '
        'ToolStripMenuItem9
        '
        Me.ToolStripMenuItem9.Name = "ToolStripMenuItem9"
        resources.ApplyResources(Me.ToolStripMenuItem9, "ToolStripMenuItem9")
        '
        'mnuCalculator
        '
        Me.mnuCalculator.Name = "mnuCalculator"
        resources.ApplyResources(Me.mnuCalculator, "mnuCalculator")
        '
        'mnuOnlineVerwaltung
        '
        Me.mnuOnlineVerwaltung.Name = "mnuOnlineVerwaltung"
        resources.ApplyResources(Me.mnuOnlineVerwaltung, "mnuOnlineVerwaltung")
        '
        'mnuOptions
        '
        Me.mnuOptions.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOptionen, Me.mnuDruckVorlagen, Me.ToolStripMenuItem6, Me.mnuHideMessages, Me.mnuTopMost})
        Me.mnuOptions.Name = "mnuOptions"
        resources.ApplyResources(Me.mnuOptions, "mnuOptions")
        '
        'mnuOptionen
        '
        Me.mnuOptionen.Name = "mnuOptionen"
        resources.ApplyResources(Me.mnuOptionen, "mnuOptionen")
        '
        'mnuDruckVorlagen
        '
        Me.mnuDruckVorlagen.Name = "mnuDruckVorlagen"
        resources.ApplyResources(Me.mnuDruckVorlagen, "mnuDruckVorlagen")
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        resources.ApplyResources(Me.ToolStripMenuItem6, "ToolStripMenuItem6")
        '
        'mnuHideMessages
        '
        Me.mnuHideMessages.Checked = True
        Me.mnuHideMessages.CheckOnClick = True
        Me.mnuHideMessages.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuHideMessages.Name = "mnuHideMessages"
        resources.ApplyResources(Me.mnuHideMessages, "mnuHideMessages")
        '
        'mnuTopMost
        '
        Me.mnuTopMost.CheckOnClick = True
        Me.mnuTopMost.Name = "mnuTopMost"
        resources.ApplyResources(Me.mnuTopMost, "mnuTopMost")
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAbZeichen, Me.mnuHantelTest, Me.mnuJuryBoard, Me.mnuKR_Pad, Me.ToolStripMenuItem5, Me.mnuMauszeiger, Me.ToolStripMenuItem16, Me.mnuWerbung, Me.mnuTVTest})
        Me.mnuExtras.Name = "mnuExtras"
        resources.ApplyResources(Me.mnuExtras, "mnuExtras")
        '
        'mnuAbZeichen
        '
        Me.mnuAbZeichen.Name = "mnuAbZeichen"
        resources.ApplyResources(Me.mnuAbZeichen, "mnuAbZeichen")
        '
        'mnuHantelTest
        '
        Me.mnuHantelTest.Name = "mnuHantelTest"
        resources.ApplyResources(Me.mnuHantelTest, "mnuHantelTest")
        '
        'mnuJuryBoard
        '
        Me.mnuJuryBoard.Name = "mnuJuryBoard"
        resources.ApplyResources(Me.mnuJuryBoard, "mnuJuryBoard")
        '
        'mnuKR_Pad
        '
        Me.mnuKR_Pad.Name = "mnuKR_Pad"
        resources.ApplyResources(Me.mnuKR_Pad, "mnuKR_Pad")
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        resources.ApplyResources(Me.ToolStripMenuItem5, "ToolStripMenuItem5")
        '
        'mnuMauszeiger
        '
        Me.mnuMauszeiger.Name = "mnuMauszeiger"
        resources.ApplyResources(Me.mnuMauszeiger, "mnuMauszeiger")
        '
        'ToolStripMenuItem16
        '
        Me.ToolStripMenuItem16.Name = "ToolStripMenuItem16"
        resources.ApplyResources(Me.ToolStripMenuItem16, "ToolStripMenuItem16")
        '
        'mnuWerbung
        '
        Me.mnuWerbung.Name = "mnuWerbung"
        resources.ApplyResources(Me.mnuWerbung, "mnuWerbung")
        '
        'mnuTVTest
        '
        Me.mnuTVTest.Name = "mnuTVTest"
        resources.ApplyResources(Me.mnuTVTest, "mnuTVTest")
        '
        'mnuHilfe
        '
        Me.mnuHilfe.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHilfeAnzeigen, Me.mnuSupport, Me.mnuWebShop, Me.ToolStripMenuItem10, Me.mnuUpdate, Me.ToolStripMenuItem13, Me.mnuRegistrieren, Me.mnuInfo})
        Me.mnuHilfe.Name = "mnuHilfe"
        resources.ApplyResources(Me.mnuHilfe, "mnuHilfe")
        '
        'mnuHilfeAnzeigen
        '
        resources.ApplyResources(Me.mnuHilfeAnzeigen, "mnuHilfeAnzeigen")
        Me.mnuHilfeAnzeigen.Name = "mnuHilfeAnzeigen"
        '
        'mnuSupport
        '
        Me.mnuSupport.Name = "mnuSupport"
        resources.ApplyResources(Me.mnuSupport, "mnuSupport")
        '
        'mnuWebShop
        '
        resources.ApplyResources(Me.mnuWebShop, "mnuWebShop")
        Me.mnuWebShop.Name = "mnuWebShop"
        '
        'ToolStripMenuItem10
        '
        Me.ToolStripMenuItem10.Name = "ToolStripMenuItem10"
        resources.ApplyResources(Me.ToolStripMenuItem10, "ToolStripMenuItem10")
        '
        'mnuUpdate
        '
        Me.mnuUpdate.Name = "mnuUpdate"
        resources.ApplyResources(Me.mnuUpdate, "mnuUpdate")
        '
        'ToolStripMenuItem13
        '
        Me.ToolStripMenuItem13.Name = "ToolStripMenuItem13"
        resources.ApplyResources(Me.ToolStripMenuItem13, "ToolStripMenuItem13")
        '
        'mnuRegistrieren
        '
        Me.mnuRegistrieren.Name = "mnuRegistrieren"
        resources.ApplyResources(Me.mnuRegistrieren, "mnuRegistrieren")
        '
        'mnuInfo
        '
        Me.mnuInfo.Name = "mnuInfo"
        resources.ApplyResources(Me.mnuInfo, "mnuInfo")
        '
        'PictureBox1
        '
        resources.ApplyResources(Me.PictureBox1, "PictureBox1")
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.TabStop = False
        '
        'StatusStrip2
        '
        Me.StatusStrip2.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblDBConnection, Me.lblExtern, Me.ProgressBar1, Me.ToolStripStatusLabel3, Me.lblWettkampf, Me.lblUser})
        resources.ApplyResources(Me.StatusStrip2, "StatusStrip2")
        Me.StatusStrip2.Name = "StatusStrip2"
        '
        'lblDBConnection
        '
        Me.lblDBConnection.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.lblDBConnection, "lblDBConnection")
        Me.lblDBConnection.Name = "lblDBConnection"
        '
        'lblExtern
        '
        resources.ApplyResources(Me.lblExtern, "lblExtern")
        Me.lblExtern.BackColor = System.Drawing.Color.Transparent
        Me.lblExtern.Name = "lblExtern"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Name = "ProgressBar1"
        resources.ApplyResources(Me.ProgressBar1, "ProgressBar1")
        Me.ProgressBar1.Step = 1
        Me.ProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        resources.ApplyResources(Me.ToolStripStatusLabel3, "ToolStripStatusLabel3")
        Me.ToolStripStatusLabel3.Spring = True
        '
        'lblWettkampf
        '
        Me.lblWettkampf.BackColor = System.Drawing.Color.Transparent
        Me.lblWettkampf.ForeColor = System.Drawing.Color.Firebrick
        Me.lblWettkampf.Margin = New System.Windows.Forms.Padding(0, 3, 1, 2)
        Me.lblWettkampf.Name = "lblWettkampf"
        Me.lblWettkampf.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        resources.ApplyResources(Me.lblWettkampf, "lblWettkampf")
        '
        'lblUser
        '
        Me.lblUser.BackColor = System.Drawing.Color.Transparent
        Me.lblUser.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        resources.ApplyResources(Me.lblUser, "lblUser")
        '
        'frmMain
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Controls.Add(Me.StatusStrip2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.mnuMain)
        Me.DoubleBuffered = True
        Me.IsMdiContainer = True
        Me.Name = "frmMain"
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip2.ResumeLayout(False)
        Me.StatusStrip2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents OpenFileDialog As OpenFileDialog
    Friend WithEvents mnuMain As MenuStrip
    Friend WithEvents mnuDatei As ToolStripMenuItem
    Friend WithEvents mnuBeenden As ToolStripMenuItem
    Friend WithEvents mnuVorbereitung As ToolStripMenuItem
    Friend WithEvents mnuWettkampf As ToolStripMenuItem
    Friend WithEvents mnuWK_New As ToolStripMenuItem
    Friend WithEvents mnuWK_Edit As ToolStripMenuItem
    Friend WithEvents mnuWK_Delete As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As ToolStripSeparator
    Friend WithEvents mnuWK_Select As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents mnuMeldung As ToolStripMenuItem
    Friend WithEvents mnuWiegen As ToolStripMenuItem
    Friend WithEvents mnuGruppen As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents mnuDruck As ToolStripMenuItem
    Friend WithEvents mnuStartkarten As ToolStripMenuItem
    Friend WithEvents mnuQuittungen As ToolStripMenuItem
    Friend WithEvents mnuWiegelisten As ToolStripMenuItem
    Friend WithEvents mnuGruppenlisten As ToolStripMenuItem
    Friend WithEvents mnuZeitplan_Print As ToolStripMenuItem
    Friend WithEvents mnuGW As ToolStripMenuItem
    Friend WithEvents mnuAthletik As ToolStripMenuItem
    Friend WithEvents mnuAnristen As ToolStripMenuItem
    Friend WithEvents mnuBankdrücken As ToolStripMenuItem
    Friend WithEvents mnuBeugestütz As ToolStripMenuItem
    Friend WithEvents mnuDifferenzsprung As ToolStripMenuItem
    Friend WithEvents mnuKlimmzug As ToolStripMenuItem
    Friend WithEvents mnuPendellauf As ToolStripMenuItem
    Friend WithEvents mnuDreisprung As ToolStripMenuItem
    Friend WithEvents mnuWeitsprung As ToolStripMenuItem
    Friend WithEvents mnuSchocken As ToolStripMenuItem
    Friend WithEvents mnuSprint As ToolStripMenuItem
    Friend WithEvents mnuAuswertung As ToolStripMenuItem
    Friend WithEvents mnuProtokoll As ToolStripMenuItem
    Friend WithEvents mnuUrkunden As ToolStripMenuItem
    Friend WithEvents mnuAnzeige As ToolStripMenuItem
    Friend WithEvents mnuDatenbank As ToolStripMenuItem
    Friend WithEvents mnuAthleten As ToolStripMenuItem
    Friend WithEvents nmuModus As ToolStripMenuItem
    Friend WithEvents mnuTeams As ToolStripMenuItem
    Friend WithEvents mnuVerein As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As ToolStripSeparator
    Friend WithEvents mnuExtras As ToolStripMenuItem
    Friend WithEvents mnuAbZeichen As ToolStripMenuItem
    Friend WithEvents mnuKR_Pad As ToolStripMenuItem
    Friend WithEvents mnuJuryBoard As ToolStripMenuItem
    Friend WithEvents mnuHantelTest As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As ToolStripSeparator
    Friend WithEvents mnuHilfe As ToolStripMenuItem
    Friend WithEvents mnuRegistrieren As ToolStripMenuItem
    Friend WithEvents mnuSupport As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem10 As ToolStripSeparator
    Friend WithEvents mnuInfo As ToolStripMenuItem
    Friend WithEvents BottomToolStripPanel As ToolStripPanel
    Friend WithEvents TopToolStripPanel As ToolStripPanel
    Friend WithEvents RightToolStripPanel As ToolStripPanel
    Friend WithEvents LeftToolStripPanel As ToolStripPanel
    Friend WithEvents ContentPanel As ToolStripContentPanel
    Friend WithEvents mnuSperren As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem12 As ToolStripSeparator
    Friend WithEvents mnuHilfeAnzeigen As ToolStripMenuItem
    Friend WithEvents mnuWebShop As ToolStripMenuItem
    Friend WithEvents mnuUpdate As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem13 As ToolStripSeparator
    Friend WithEvents mnuWerbung As ToolStripMenuItem
    Friend WithEvents mnuGewichtheben As ToolStripMenuItem
    Friend WithEvents mnuKorrektur As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem15 As ToolStripSeparator
    Friend WithEvents mnuEasyModus As ToolStripMenuItem
    Friend WithEvents mnuLänderwertung As ToolStripMenuItem
    Friend WithEvents mnuVereinswertung As ToolStripMenuItem
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ToolStripMenuItem3 As ToolStripSeparator
    Friend WithEvents mnuClearMeldung As ToolStripMenuItem
    Friend WithEvents mnuClearHeben As ToolStripMenuItem
    Friend WithEvents mnuSiegerehrung As ToolStripMenuItem
    Friend WithEvents mnuKampfgericht As ToolStripMenuItem
    Friend WithEvents mnuMeldeliste As ToolStripMenuItem
    Friend WithEvents mnuZeitplan As ToolStripMenuItem
    Friend WithEvents mnuStarterlisten As ToolStripMenuItem
    Friend WithEvents mnuLogin As ToolStripMenuItem
    Friend WithEvents div1 As ToolStripSeparator
    Friend WithEvents mnuOptions As ToolStripMenuItem
    Friend WithEvents mnuOptionen As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As ToolStripSeparator
    Friend WithEvents mnuHideMessages As ToolStripMenuItem
    Friend WithEvents mnuDruckVorlagen As ToolStripMenuItem
    Friend WithEvents mnuPasswort As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem8 As ToolStripSeparator
    Friend WithEvents mnuMauszeiger As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem16 As ToolStripSeparator
    Friend WithEvents ToolStripMenuItem7 As ToolStripSeparator
    Friend WithEvents mnuPause As ToolStripMenuItem
    Friend WithEvents mnuExport As ToolStripMenuItem
    Friend WithEvents mnuExport_Bundesliga As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem14 As ToolStripSeparator
    Friend WithEvents mnuExport_Ergebnisse As ToolStripMenuItem
    Friend WithEvents mnuExport_Wiegeliste As ToolStripMenuItem
    Friend WithEvents mnuImport As ToolStripMenuItem
    Friend WithEvents mnuImport_Bundesliga As ToolStripMenuItem
    Friend WithEvents mnuImport_Ergebnisse As ToolStripMenuItem
    Friend WithEvents mnuImport_Heber As ToolStripMenuItem
    Friend WithEvents mnuImport_Wiegeliste As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem11 As ToolStripSeparator
    Friend WithEvents mnuImport_Meisterschaft As ToolStripMenuItem
    Friend WithEvents mnuImport_Meldeliste As ToolStripMenuItem
    Friend WithEvents mnuExportMeisterschaft As ToolStripMenuItem
    Friend WithEvents mnuCalculator As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem9 As ToolStripSeparator
    Friend WithEvents mnuWorldRecords As ToolStripMenuItem
    Friend WithEvents mnuTVTest As ToolStripMenuItem
    Friend WithEvents mnuMannschaftswertung As ToolStripMenuItem
    Friend WithEvents StatusStrip2 As StatusStrip
    Friend WithEvents lblDBConnection As ToolStripStatusLabel
    Friend WithEvents lblExtern As ToolStripStatusLabel
    Friend WithEvents lblWettkampf As ToolStripStatusLabel
    Friend WithEvents lblUser As ToolStripStatusLabel
    Friend WithEvents ProgressBar1 As ToolStripProgressBar
    Friend WithEvents ToolStripStatusLabel3 As ToolStripStatusLabel
    Friend WithEvents mnuClearWiegen As ToolStripMenuItem
    Friend WithEvents mnuTopMost As ToolStripMenuItem
    Friend WithEvents mnuAthletenAktualisieren As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem18 As ToolStripSeparator
    Friend WithEvents mnuOnlineVerwaltung As ToolStripMenuItem
End Class
