﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPublikum
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPublikum))
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvJoin = New System.Windows.Forms.DataGridView()
        Me.Gruppe = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.Startnummer = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.Nachname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vorname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sex = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.Flagge = New System.Windows.Forms.DataGridViewImageColumn()
        Me.Verein = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.Jahrgang = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.KG = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.GK = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.R_1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RT_1 = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.R_2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RT_2 = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.R_3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RT_3 = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.Reissen = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.S_1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ST_1 = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.S_2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ST_2 = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.S_3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ST_3 = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.Stossen = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.ZK = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.Heben = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.Heben_Platz = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.Wertung2 = New Gewichtheben.frmPublikum.CustomColumn_RightBorder()
        Me.Wertung2_Platz = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblDurchgang = New System.Windows.Forms.Label()
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.lblGruppe = New System.Windows.Forms.Label()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.lblVerein1 = New System.Windows.Forms.Label()
        Me.dgvBest1 = New System.Windows.Forms.DataGridView()
        Me.pnlBest1 = New System.Windows.Forms.Panel()
        Me.lblBest1 = New System.Windows.Forms.Label()
        Me.pnlBest2 = New System.Windows.Forms.Panel()
        Me.lblBest2 = New System.Windows.Forms.Label()
        Me.dgvBest2 = New System.Windows.Forms.DataGridView()
        Me.dgvTeam = New System.Windows.Forms.DataGridView()
        Me.Rolle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Team = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gegner = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mReissen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mStossen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mHeben = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mPunkte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Punkte2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblVerein3 = New System.Windows.Forms.Label()
        Me.lblVerein2 = New System.Windows.Forms.Label()
        Me.lblVerein6 = New System.Windows.Forms.Label()
        Me.lblVerein5 = New System.Windows.Forms.Label()
        Me.lblVerein4 = New System.Windows.Forms.Label()
        Me.xAufruf = New System.Windows.Forms.Label()
        Me.lblJury = New System.Windows.Forms.Label()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBest1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBest1.SuspendLayout()
        Me.pnlBest2.SuspendLayout()
        CType(Me.dgvBest2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTeam, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvJoin
        '
        Me.dgvJoin.AllowUserToAddRows = False
        Me.dgvJoin.AllowUserToDeleteRows = False
        Me.dgvJoin.AllowUserToResizeColumns = False
        Me.dgvJoin.AllowUserToResizeRows = False
        Me.dgvJoin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dgvJoin.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.dgvJoin.BackgroundColor = System.Drawing.Color.Black
        Me.dgvJoin.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvJoin.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvJoin.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvJoin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvJoin.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Gruppe, Me.Startnummer, Me.Nachname, Me.Vorname, Me.Sex, Me.Flagge, Me.Verein, Me.Jahrgang, Me.KG, Me.GK, Me.R_1, Me.RT_1, Me.R_2, Me.RT_2, Me.R_3, Me.RT_3, Me.Reissen, Me.S_1, Me.ST_1, Me.S_2, Me.ST_2, Me.S_3, Me.ST_3, Me.Stossen, Me.ZK, Me.Heben, Me.Heben_Platz, Me.Wertung2, Me.Wertung2_Platz})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Padding = New System.Windows.Forms.Padding(0, 1, 0, 1)
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvJoin.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvJoin.EnableHeadersVisualStyles = False
        Me.dgvJoin.Location = New System.Drawing.Point(0, 33)
        Me.dgvJoin.Name = "dgvJoin"
        Me.dgvJoin.ReadOnly = True
        Me.dgvJoin.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvJoin.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvJoin.RowHeadersVisible = False
        Me.dgvJoin.RowHeadersWidth = 4
        Me.dgvJoin.RowTemplate.Height = 23
        Me.dgvJoin.RowTemplate.ReadOnly = True
        Me.dgvJoin.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvJoin.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvJoin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvJoin.ShowCellErrors = False
        Me.dgvJoin.ShowCellToolTips = False
        Me.dgvJoin.ShowEditingIcon = False
        Me.dgvJoin.Size = New System.Drawing.Size(804, 313)
        Me.dgvJoin.TabIndex = 3
        Me.dgvJoin.Tag = "8,25"
        '
        'Gruppe
        '
        Me.Gruppe.DataPropertyName = "Gruppe"
        Me.Gruppe.FillWeight = 20.0!
        Me.Gruppe.HeaderText = "Gr."
        Me.Gruppe.MinimumWidth = 20
        Me.Gruppe.Name = "Gruppe"
        Me.Gruppe.ReadOnly = True
        Me.Gruppe.Visible = False
        Me.Gruppe.Width = 20
        '
        'Startnummer
        '
        Me.Startnummer.DataPropertyName = "Startnummer"
        Me.Startnummer.FillWeight = 20.0!
        Me.Startnummer.HeaderText = "Nr."
        Me.Startnummer.MinimumWidth = 20
        Me.Startnummer.Name = "Startnummer"
        Me.Startnummer.ReadOnly = True
        Me.Startnummer.Width = 20
        '
        'Nachname
        '
        Me.Nachname.DataPropertyName = "Nachname"
        Me.Nachname.FillWeight = 90.0!
        Me.Nachname.HeaderText = "Name de la Nachname"
        Me.Nachname.MinimumWidth = 90
        Me.Nachname.Name = "Nachname"
        Me.Nachname.ReadOnly = True
        Me.Nachname.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Nachname.Width = 90
        '
        'Vorname
        '
        Me.Vorname.DataPropertyName = "Vorname"
        Me.Vorname.FillWeight = 90.0!
        Me.Vorname.HeaderText = "Maria Magdalena"
        Me.Vorname.MinimumWidth = 90
        Me.Vorname.Name = "Vorname"
        Me.Vorname.ReadOnly = True
        Me.Vorname.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Vorname.Width = 90
        '
        'Sex
        '
        Me.Sex.DataPropertyName = "Sex"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Sex.DefaultCellStyle = DataGridViewCellStyle2
        Me.Sex.FillWeight = 15.0!
        Me.Sex.HeaderText = "w"
        Me.Sex.MinimumWidth = 15
        Me.Sex.Name = "Sex"
        Me.Sex.ReadOnly = True
        Me.Sex.Width = 15
        '
        'Flagge
        '
        Me.Flagge.FillWeight = 25.0!
        Me.Flagge.HeaderText = ""
        Me.Flagge.MinimumWidth = 25
        Me.Flagge.Name = "Flagge"
        Me.Flagge.ReadOnly = True
        Me.Flagge.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Flagge.Visible = False
        Me.Flagge.Width = 25
        '
        'Verein
        '
        Me.Verein.DataPropertyName = "Verein"
        Me.Verein.FillWeight = 90.0!
        Me.Verein.HeaderText = "Verein"
        Me.Verein.MinimumWidth = 90
        Me.Verein.Name = "Verein"
        Me.Verein.ReadOnly = True
        Me.Verein.Width = 90
        '
        'Jahrgang
        '
        Me.Jahrgang.DataPropertyName = "Jahrgang"
        Me.Jahrgang.FillWeight = 25.0!
        Me.Jahrgang.HeaderText = "JG"
        Me.Jahrgang.MinimumWidth = 25
        Me.Jahrgang.Name = "Jahrgang"
        Me.Jahrgang.ReadOnly = True
        Me.Jahrgang.Width = 25
        '
        'KG
        '
        Me.KG.DataPropertyName = "KG"
        Me.KG.FillWeight = 32.0!
        Me.KG.HeaderText = "KG"
        Me.KG.MinimumWidth = 32
        Me.KG.Name = "KG"
        Me.KG.ReadOnly = True
        Me.KG.Width = 32
        '
        'GK
        '
        Me.GK.DataPropertyName = "GK"
        Me.GK.FillWeight = 30.0!
        Me.GK.HeaderText = "GK"
        Me.GK.MinimumWidth = 30
        Me.GK.Name = "GK"
        Me.GK.ReadOnly = True
        Me.GK.Width = 30
        '
        'R_1
        '
        Me.R_1.DataPropertyName = "R_1"
        Me.R_1.FillWeight = 22.0!
        Me.R_1.HeaderText = "R 1"
        Me.R_1.MinimumWidth = 22
        Me.R_1.Name = "R_1"
        Me.R_1.ReadOnly = True
        Me.R_1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.R_1.Width = 22
        '
        'RT_1
        '
        Me.RT_1.DataPropertyName = "RT_1"
        Me.RT_1.FillWeight = 22.0!
        Me.RT_1.HeaderText = "T 1"
        Me.RT_1.MinimumWidth = 22
        Me.RT_1.Name = "RT_1"
        Me.RT_1.ReadOnly = True
        Me.RT_1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RT_1.Width = 22
        '
        'R_2
        '
        Me.R_2.DataPropertyName = "R_2"
        Me.R_2.FillWeight = 22.0!
        Me.R_2.HeaderText = "R 2"
        Me.R_2.MinimumWidth = 22
        Me.R_2.Name = "R_2"
        Me.R_2.ReadOnly = True
        Me.R_2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.R_2.Width = 22
        '
        'RT_2
        '
        Me.RT_2.DataPropertyName = "RT_2"
        Me.RT_2.FillWeight = 22.0!
        Me.RT_2.HeaderText = "T 2"
        Me.RT_2.MinimumWidth = 22
        Me.RT_2.Name = "RT_2"
        Me.RT_2.ReadOnly = True
        Me.RT_2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RT_2.Width = 22
        '
        'R_3
        '
        Me.R_3.DataPropertyName = "R_3"
        Me.R_3.FillWeight = 22.0!
        Me.R_3.HeaderText = "R 3"
        Me.R_3.MinimumWidth = 22
        Me.R_3.Name = "R_3"
        Me.R_3.ReadOnly = True
        Me.R_3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.R_3.Width = 22
        '
        'RT_3
        '
        Me.RT_3.DataPropertyName = "RT_3"
        Me.RT_3.FillWeight = 22.0!
        Me.RT_3.HeaderText = "T 3"
        Me.RT_3.MinimumWidth = 22
        Me.RT_3.Name = "RT_3"
        Me.RT_3.ReadOnly = True
        Me.RT_3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RT_3.Width = 22
        '
        'Reissen
        '
        Me.Reissen.DataPropertyName = "Reissen"
        Me.Reissen.FillWeight = 34.0!
        Me.Reissen.HeaderText = "Reißen"
        Me.Reissen.MinimumWidth = 34
        Me.Reissen.Name = "Reissen"
        Me.Reissen.ReadOnly = True
        Me.Reissen.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Reissen.Width = 34
        '
        'S_1
        '
        Me.S_1.DataPropertyName = "S_1"
        Me.S_1.FillWeight = 22.0!
        Me.S_1.HeaderText = "S 1"
        Me.S_1.MinimumWidth = 22
        Me.S_1.Name = "S_1"
        Me.S_1.ReadOnly = True
        Me.S_1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.S_1.Width = 22
        '
        'ST_1
        '
        Me.ST_1.DataPropertyName = "ST_1"
        Me.ST_1.FillWeight = 22.0!
        Me.ST_1.HeaderText = "T 1"
        Me.ST_1.MinimumWidth = 22
        Me.ST_1.Name = "ST_1"
        Me.ST_1.ReadOnly = True
        Me.ST_1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ST_1.Width = 22
        '
        'S_2
        '
        Me.S_2.DataPropertyName = "S_2"
        Me.S_2.FillWeight = 22.0!
        Me.S_2.HeaderText = "S 2"
        Me.S_2.MinimumWidth = 22
        Me.S_2.Name = "S_2"
        Me.S_2.ReadOnly = True
        Me.S_2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.S_2.Width = 22
        '
        'ST_2
        '
        Me.ST_2.DataPropertyName = "ST_2"
        Me.ST_2.FillWeight = 22.0!
        Me.ST_2.HeaderText = "T 2"
        Me.ST_2.MinimumWidth = 22
        Me.ST_2.Name = "ST_2"
        Me.ST_2.ReadOnly = True
        Me.ST_2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ST_2.Width = 22
        '
        'S_3
        '
        Me.S_3.DataPropertyName = "S_3"
        Me.S_3.FillWeight = 22.0!
        Me.S_3.HeaderText = "S 3"
        Me.S_3.MinimumWidth = 22
        Me.S_3.Name = "S_3"
        Me.S_3.ReadOnly = True
        Me.S_3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.S_3.Width = 22
        '
        'ST_3
        '
        Me.ST_3.DataPropertyName = "T_3"
        Me.ST_3.FillWeight = 22.0!
        Me.ST_3.HeaderText = "T 3"
        Me.ST_3.MinimumWidth = 22
        Me.ST_3.Name = "ST_3"
        Me.ST_3.ReadOnly = True
        Me.ST_3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ST_3.Width = 22
        '
        'Stossen
        '
        Me.Stossen.DataPropertyName = "Stossen"
        Me.Stossen.FillWeight = 34.0!
        Me.Stossen.HeaderText = "Stoßen"
        Me.Stossen.MinimumWidth = 34
        Me.Stossen.Name = "Stossen"
        Me.Stossen.ReadOnly = True
        Me.Stossen.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Stossen.Width = 34
        '
        'ZK
        '
        Me.ZK.DataPropertyName = "ZK"
        Me.ZK.FillWeight = 30.0!
        Me.ZK.HeaderText = "ZK"
        Me.ZK.MinimumWidth = 30
        Me.ZK.Name = "ZK"
        Me.ZK.ReadOnly = True
        Me.ZK.Visible = False
        Me.ZK.Width = 30
        '
        'Heben
        '
        Me.Heben.DataPropertyName = "Heben"
        Me.Heben.FillWeight = 34.0!
        Me.Heben.HeaderText = "Heben"
        Me.Heben.MinimumWidth = 34
        Me.Heben.Name = "Heben"
        Me.Heben.ReadOnly = True
        Me.Heben.Width = 34
        '
        'Heben_Platz
        '
        Me.Heben_Platz.DataPropertyName = "Heben_Platz"
        Me.Heben_Platz.FillWeight = 30.0!
        Me.Heben_Platz.HeaderText = "Pl."
        Me.Heben_Platz.MinimumWidth = 30
        Me.Heben_Platz.Name = "Heben_Platz"
        Me.Heben_Platz.ReadOnly = True
        Me.Heben_Platz.Width = 30
        '
        'Wertung2
        '
        Me.Wertung2.DataPropertyName = "Wertung2"
        Me.Wertung2.FillWeight = 34.0!
        Me.Wertung2.HeaderText = "variabel"
        Me.Wertung2.MinimumWidth = 34
        Me.Wertung2.Name = "Wertung2"
        Me.Wertung2.ReadOnly = True
        Me.Wertung2.Visible = False
        Me.Wertung2.Width = 36
        '
        'Wertung2_Platz
        '
        Me.Wertung2_Platz.DataPropertyName = "Wertung2_Platz"
        Me.Wertung2_Platz.FillWeight = 20.0!
        Me.Wertung2_Platz.HeaderText = "2."
        Me.Wertung2_Platz.MinimumWidth = 20
        Me.Wertung2_Platz.Name = "Wertung2_Platz"
        Me.Wertung2_Platz.ReadOnly = True
        Me.Wertung2_Platz.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Wertung2_Platz.Visible = False
        Me.Wertung2_Platz.Width = 20
        '
        'lblDurchgang
        '
        Me.lblDurchgang.AutoSize = True
        Me.lblDurchgang.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDurchgang.ForeColor = System.Drawing.Color.Gold
        Me.lblDurchgang.Location = New System.Drawing.Point(94, 6)
        Me.lblDurchgang.Name = "lblDurchgang"
        Me.lblDurchgang.Size = New System.Drawing.Size(34, 20)
        Me.lblDurchgang.TabIndex = 3
        Me.lblDurchgang.Text = "DG"
        Me.lblDurchgang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption
        '
        Me.lblCaption.AutoSize = True
        Me.lblCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption.ForeColor = System.Drawing.Color.Gold
        Me.lblCaption.Location = New System.Drawing.Point(357, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(68, 26)
        Me.lblCaption.TabIndex = 2
        Me.lblCaption.Text = "Bohle"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblGruppe
        '
        Me.lblGruppe.AutoSize = True
        Me.lblGruppe.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGruppe.ForeColor = System.Drawing.Color.Gold
        Me.lblGruppe.Location = New System.Drawing.Point(12, 6)
        Me.lblGruppe.Name = "lblGruppe"
        Me.lblGruppe.Size = New System.Drawing.Size(63, 20)
        Me.lblGruppe.TabIndex = 0
        Me.lblGruppe.Text = "Gruppe"
        Me.lblGruppe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "SquareBlue.jpg")
        Me.ImageList1.Images.SetKeyName(1, "SquareRed.jpg")
        '
        'lblVerein1
        '
        Me.lblVerein1.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblVerein1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVerein1.ForeColor = System.Drawing.Color.Gold
        Me.lblVerein1.Location = New System.Drawing.Point(500, 67)
        Me.lblVerein1.Name = "lblVerein1"
        Me.lblVerein1.Padding = New System.Windows.Forms.Padding(50, 0, 0, 0)
        Me.lblVerein1.Size = New System.Drawing.Size(245, 21)
        Me.lblVerein1.TabIndex = 3
        Me.lblVerein1.Text = "Verein"
        Me.lblVerein1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblVerein1.Visible = False
        '
        'dgvBest1
        '
        Me.dgvBest1.AllowUserToAddRows = False
        Me.dgvBest1.AllowUserToDeleteRows = False
        Me.dgvBest1.AllowUserToResizeColumns = False
        Me.dgvBest1.AllowUserToResizeRows = False
        Me.dgvBest1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvBest1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvBest1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(180, Byte), Integer))
        Me.dgvBest1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvBest1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvBest1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.AppWorkspace
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBest1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvBest1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle6.Padding = New System.Windows.Forms.Padding(0, 1, 0, 1)
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBest1.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvBest1.EnableHeadersVisualStyles = False
        Me.dgvBest1.Location = New System.Drawing.Point(0, 25)
        Me.dgvBest1.Name = "dgvBest1"
        Me.dgvBest1.ReadOnly = True
        Me.dgvBest1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBest1.RowHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvBest1.RowHeadersVisible = False
        Me.dgvBest1.RowHeadersWidth = 4
        Me.dgvBest1.RowTemplate.Height = 23
        Me.dgvBest1.RowTemplate.ReadOnly = True
        Me.dgvBest1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBest1.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvBest1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvBest1.ShowCellErrors = False
        Me.dgvBest1.ShowCellToolTips = False
        Me.dgvBest1.ShowEditingIcon = False
        Me.dgvBest1.Size = New System.Drawing.Size(235, 46)
        Me.dgvBest1.TabIndex = 30
        Me.dgvBest1.Tag = "8,25"
        '
        'pnlBest1
        '
        Me.pnlBest1.BackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(180, Byte), Integer))
        Me.pnlBest1.Controls.Add(Me.lblBest1)
        Me.pnlBest1.Controls.Add(Me.dgvBest1)
        Me.pnlBest1.Location = New System.Drawing.Point(45, 356)
        Me.pnlBest1.Name = "pnlBest1"
        Me.pnlBest1.Size = New System.Drawing.Size(235, 82)
        Me.pnlBest1.TabIndex = 32
        Me.pnlBest1.Visible = False
        '
        'lblBest1
        '
        Me.lblBest1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.lblBest1.ForeColor = System.Drawing.Color.White
        Me.lblBest1.Location = New System.Drawing.Point(3, 4)
        Me.lblBest1.Name = "lblBest1"
        Me.lblBest1.Size = New System.Drawing.Size(235, 20)
        Me.lblBest1.TabIndex = 31
        Me.lblBest1.Text = "BESTENLISTE"
        Me.lblBest1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'pnlBest2
        '
        Me.pnlBest2.BackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(180, Byte), Integer))
        Me.pnlBest2.Controls.Add(Me.lblBest2)
        Me.pnlBest2.Controls.Add(Me.dgvBest2)
        Me.pnlBest2.Location = New System.Drawing.Point(329, 356)
        Me.pnlBest2.Name = "pnlBest2"
        Me.pnlBest2.Size = New System.Drawing.Size(235, 82)
        Me.pnlBest2.TabIndex = 38
        Me.pnlBest2.Visible = False
        '
        'lblBest2
        '
        Me.lblBest2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.lblBest2.ForeColor = System.Drawing.Color.White
        Me.lblBest2.Location = New System.Drawing.Point(3, 4)
        Me.lblBest2.Name = "lblBest2"
        Me.lblBest2.Size = New System.Drawing.Size(235, 20)
        Me.lblBest2.TabIndex = 34
        Me.lblBest2.Text = "BESTENLISTE 2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "bla"
        Me.lblBest2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'dgvBest2
        '
        Me.dgvBest2.AllowUserToAddRows = False
        Me.dgvBest2.AllowUserToDeleteRows = False
        Me.dgvBest2.AllowUserToResizeColumns = False
        Me.dgvBest2.AllowUserToResizeRows = False
        Me.dgvBest2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvBest2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvBest2.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(180, Byte), Integer))
        Me.dgvBest2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvBest2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvBest2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.AppWorkspace
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBest2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvBest2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle9.Padding = New System.Windows.Forms.Padding(0, 1, 0, 1)
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBest2.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgvBest2.EnableHeadersVisualStyles = False
        Me.dgvBest2.Location = New System.Drawing.Point(0, 25)
        Me.dgvBest2.Name = "dgvBest2"
        Me.dgvBest2.ReadOnly = True
        Me.dgvBest2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBest2.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvBest2.RowHeadersVisible = False
        Me.dgvBest2.RowHeadersWidth = 4
        Me.dgvBest2.RowTemplate.Height = 23
        Me.dgvBest2.RowTemplate.ReadOnly = True
        Me.dgvBest2.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBest2.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvBest2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvBest2.ShowCellErrors = False
        Me.dgvBest2.ShowCellToolTips = False
        Me.dgvBest2.ShowEditingIcon = False
        Me.dgvBest2.Size = New System.Drawing.Size(235, 46)
        Me.dgvBest2.TabIndex = 33
        Me.dgvBest2.Tag = "8,25"
        '
        'dgvTeam
        '
        Me.dgvTeam.AllowUserToAddRows = False
        Me.dgvTeam.AllowUserToDeleteRows = False
        Me.dgvTeam.AllowUserToResizeColumns = False
        Me.dgvTeam.AllowUserToResizeRows = False
        Me.dgvTeam.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvTeam.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvTeam.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.dgvTeam.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTeam.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvTeam.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle11.Padding = New System.Windows.Forms.Padding(0, 0, 0, 3)
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTeam.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvTeam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTeam.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Rolle, Me.Team, Me.Gegner, Me.mReissen, Me.mStossen, Me.mHeben, Me.mPunkte, Me.Punkte2})
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle19.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTeam.DefaultCellStyle = DataGridViewCellStyle19
        Me.dgvTeam.EnableHeadersVisualStyles = False
        Me.dgvTeam.Location = New System.Drawing.Point(136, 86)
        Me.dgvTeam.Name = "dgvTeam"
        Me.dgvTeam.ReadOnly = True
        Me.dgvTeam.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvTeam.RowHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.dgvTeam.RowHeadersVisible = False
        Me.dgvTeam.RowHeadersWidth = 62
        Me.dgvTeam.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvTeam.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTeam.ShowCellToolTips = False
        Me.dgvTeam.ShowEditingIcon = False
        Me.dgvTeam.ShowRowErrors = False
        Me.dgvTeam.Size = New System.Drawing.Size(309, 42)
        Me.dgvTeam.TabIndex = 39
        Me.dgvTeam.TabStop = False
        Me.dgvTeam.Visible = False
        '
        'Rolle
        '
        Me.Rolle.DataPropertyName = "Rolle"
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Gold
        Me.Rolle.DefaultCellStyle = DataGridViewCellStyle12
        Me.Rolle.HeaderText = ""
        Me.Rolle.MinimumWidth = 8
        Me.Rolle.Name = "Rolle"
        Me.Rolle.ReadOnly = True
        Me.Rolle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Rolle.Visible = False
        Me.Rolle.Width = 8
        '
        'Team
        '
        Me.Team.DataPropertyName = "Vereinsname"
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Gold
        Me.Team.DefaultCellStyle = DataGridViewCellStyle13
        Me.Team.HeaderText = ""
        Me.Team.MinimumWidth = 8
        Me.Team.Name = "Team"
        Me.Team.ReadOnly = True
        Me.Team.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Team.Width = 8
        '
        'Gegner
        '
        Me.Gegner.DataPropertyName = "Gegner"
        Me.Gegner.HeaderText = "Gegner"
        Me.Gegner.MinimumWidth = 8
        Me.Gegner.Name = "Gegner"
        Me.Gegner.ReadOnly = True
        Me.Gegner.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Gegner.Visible = False
        Me.Gegner.Width = 63
        '
        'mReissen
        '
        Me.mReissen.DataPropertyName = "Reissen"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mReissen.DefaultCellStyle = DataGridViewCellStyle14
        Me.mReissen.HeaderText = "Reißen"
        Me.mReissen.MinimumWidth = 8
        Me.mReissen.Name = "mReissen"
        Me.mReissen.ReadOnly = True
        Me.mReissen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mReissen.Width = 62
        '
        'mStossen
        '
        Me.mStossen.DataPropertyName = "Stossen"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mStossen.DefaultCellStyle = DataGridViewCellStyle15
        Me.mStossen.HeaderText = "Stoßen"
        Me.mStossen.MinimumWidth = 8
        Me.mStossen.Name = "mStossen"
        Me.mStossen.ReadOnly = True
        Me.mStossen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mStossen.Width = 61
        '
        'mHeben
        '
        Me.mHeben.DataPropertyName = "Heben"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mHeben.DefaultCellStyle = DataGridViewCellStyle16
        Me.mHeben.HeaderText = "Gesamt"
        Me.mHeben.MinimumWidth = 8
        Me.mHeben.Name = "mHeben"
        Me.mHeben.ReadOnly = True
        Me.mHeben.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mHeben.Width = 65
        '
        'mPunkte
        '
        Me.mPunkte.DataPropertyName = "Punkte"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle17.Format = "N0"
        Me.mPunkte.DefaultCellStyle = DataGridViewCellStyle17
        Me.mPunkte.HeaderText = "Pkt."
        Me.mPunkte.MinimumWidth = 8
        Me.mPunkte.Name = "mPunkte"
        Me.mPunkte.ReadOnly = True
        Me.mPunkte.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mPunkte.Width = 38
        '
        'Punkte2
        '
        Me.Punkte2.DataPropertyName = "Punkte2"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle18.Format = "N0"
        Me.Punkte2.DefaultCellStyle = DataGridViewCellStyle18
        Me.Punkte2.HeaderText = "Pkt."
        Me.Punkte2.MinimumWidth = 8
        Me.Punkte2.Name = "Punkte2"
        Me.Punkte2.ReadOnly = True
        Me.Punkte2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Punkte2.Visible = False
        Me.Punkte2.Width = 38
        '
        'lblVerein3
        '
        Me.lblVerein3.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblVerein3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVerein3.ForeColor = System.Drawing.Color.Gold
        Me.lblVerein3.Location = New System.Drawing.Point(500, 109)
        Me.lblVerein3.Name = "lblVerein3"
        Me.lblVerein3.Padding = New System.Windows.Forms.Padding(50, 0, 0, 0)
        Me.lblVerein3.Size = New System.Drawing.Size(245, 21)
        Me.lblVerein3.TabIndex = 40
        Me.lblVerein3.Text = "Verein"
        Me.lblVerein3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblVerein3.Visible = False
        '
        'lblVerein2
        '
        Me.lblVerein2.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblVerein2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVerein2.ForeColor = System.Drawing.Color.Gold
        Me.lblVerein2.Location = New System.Drawing.Point(500, 88)
        Me.lblVerein2.Name = "lblVerein2"
        Me.lblVerein2.Padding = New System.Windows.Forms.Padding(50, 0, 0, 0)
        Me.lblVerein2.Size = New System.Drawing.Size(245, 21)
        Me.lblVerein2.TabIndex = 41
        Me.lblVerein2.Text = "Verein"
        Me.lblVerein2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblVerein2.Visible = False
        '
        'lblVerein6
        '
        Me.lblVerein6.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblVerein6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVerein6.ForeColor = System.Drawing.Color.Gold
        Me.lblVerein6.Location = New System.Drawing.Point(500, 172)
        Me.lblVerein6.Name = "lblVerein6"
        Me.lblVerein6.Padding = New System.Windows.Forms.Padding(50, 0, 0, 0)
        Me.lblVerein6.Size = New System.Drawing.Size(245, 21)
        Me.lblVerein6.TabIndex = 42
        Me.lblVerein6.Text = "Verein"
        Me.lblVerein6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblVerein6.Visible = False
        '
        'lblVerein5
        '
        Me.lblVerein5.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblVerein5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVerein5.ForeColor = System.Drawing.Color.Gold
        Me.lblVerein5.Location = New System.Drawing.Point(500, 151)
        Me.lblVerein5.Name = "lblVerein5"
        Me.lblVerein5.Padding = New System.Windows.Forms.Padding(50, 0, 0, 0)
        Me.lblVerein5.Size = New System.Drawing.Size(245, 21)
        Me.lblVerein5.TabIndex = 43
        Me.lblVerein5.Text = "Verein"
        Me.lblVerein5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblVerein5.Visible = False
        '
        'lblVerein4
        '
        Me.lblVerein4.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblVerein4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVerein4.ForeColor = System.Drawing.Color.Gold
        Me.lblVerein4.Location = New System.Drawing.Point(500, 130)
        Me.lblVerein4.Name = "lblVerein4"
        Me.lblVerein4.Padding = New System.Windows.Forms.Padding(50, 0, 0, 0)
        Me.lblVerein4.Size = New System.Drawing.Size(245, 21)
        Me.lblVerein4.TabIndex = 44
        Me.lblVerein4.Text = "Verein"
        Me.lblVerein4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblVerein4.Visible = False
        '
        'xAufruf
        '
        Me.xAufruf.Font = New System.Drawing.Font("Microsoft Sans Serif", 19.0!)
        Me.xAufruf.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf.Location = New System.Drawing.Point(713, 2)
        Me.xAufruf.Name = "xAufruf"
        Me.xAufruf.Size = New System.Drawing.Size(90, 28)
        Me.xAufruf.TabIndex = 46
        Me.xAufruf.Text = "0:00"
        Me.xAufruf.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblJury
        '
        Me.lblJury.BackColor = System.Drawing.Color.Red
        Me.lblJury.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.lblJury.Font = New System.Drawing.Font("Microsoft Sans Serif", 45.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJury.ForeColor = System.Drawing.Color.Yellow
        Me.lblJury.Location = New System.Drawing.Point(14, 145)
        Me.lblJury.Name = "lblJury"
        Me.lblJury.Size = New System.Drawing.Size(61, 165)
        Me.lblJury.TabIndex = 47
        Me.lblJury.Text = "JURY-ENTSCHEIDUNG"
        Me.lblJury.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblJury.Visible = False
        '
        'frmPublikum
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(804, 454)
        Me.Controls.Add(Me.lblJury)
        Me.Controls.Add(Me.lblVerein4)
        Me.Controls.Add(Me.lblVerein5)
        Me.Controls.Add(Me.lblVerein6)
        Me.Controls.Add(Me.lblVerein2)
        Me.Controls.Add(Me.lblVerein3)
        Me.Controls.Add(Me.lblVerein1)
        Me.Controls.Add(Me.dgvTeam)
        Me.Controls.Add(Me.pnlBest2)
        Me.Controls.Add(Me.pnlBest1)
        Me.Controls.Add(Me.lblGruppe)
        Me.Controls.Add(Me.lblDurchgang)
        Me.Controls.Add(Me.lblCaption)
        Me.Controls.Add(Me.dgvJoin)
        Me.Controls.Add(Me.xAufruf)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(804, 454)
        Me.Name = "frmPublikum"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Wertung"
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBest1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBest1.ResumeLayout(False)
        Me.pnlBest2.ResumeLayout(False)
        CType(Me.dgvBest2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTeam, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvJoin As DataGridView
    Friend WithEvents lblGruppe As Label
    Friend WithEvents lblCaption As Label
    Friend WithEvents lblDurchgang As Label
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents lblVerein1 As Label
    Friend WithEvents dgvBest1 As DataGridView
    Friend WithEvents pnlBest1 As Panel
    Friend WithEvents lblBest1 As Label
    Friend WithEvents pnlBest2 As Panel
    Friend WithEvents lblBest2 As Label
    Friend WithEvents dgvBest2 As DataGridView
    Friend WithEvents dgvTeam As DataGridView
    Friend WithEvents lblVerein3 As Label
    Friend WithEvents lblVerein2 As Label
    Friend WithEvents lblVerein6 As Label
    Friend WithEvents lblVerein5 As Label
    Friend WithEvents lblVerein4 As Label
    Friend WithEvents xAufruf As Label
    Friend WithEvents lblJury As Label
    Friend WithEvents Rolle As DataGridViewTextBoxColumn
    Friend WithEvents Team As DataGridViewTextBoxColumn
    Friend WithEvents Gegner As DataGridViewTextBoxColumn
    Friend WithEvents mReissen As DataGridViewTextBoxColumn
    Friend WithEvents mStossen As DataGridViewTextBoxColumn
    Friend WithEvents mHeben As DataGridViewTextBoxColumn
    Friend WithEvents mPunkte As DataGridViewTextBoxColumn
    Friend WithEvents Punkte2 As DataGridViewTextBoxColumn
    Friend Gruppe As CustomColumn_RightBorder
    Friend Startnummer As CustomColumn_RightBorder
    Friend WithEvents Nachname As DataGridViewTextBoxColumn
    Friend WithEvents Vorname As DataGridViewTextBoxColumn
    Friend Sex As CustomColumn_RightBorder
    Friend WithEvents Flagge As DataGridViewImageColumn
    Friend Verein As CustomColumn_RightBorder
    Friend Jahrgang As CustomColumn_RightBorder
    Friend KG As CustomColumn_RightBorder
    Friend GK As CustomColumn_RightBorder
    Friend WithEvents R_1 As DataGridViewTextBoxColumn
    Friend RT_1 As CustomColumn_RightBorder
    Friend WithEvents R_2 As DataGridViewTextBoxColumn
    Friend RT_2 As CustomColumn_RightBorder
    Friend WithEvents R_3 As DataGridViewTextBoxColumn
    Friend RT_3 As CustomColumn_RightBorder
    Friend Reissen As CustomColumn_RightBorder
    Friend WithEvents S_1 As DataGridViewTextBoxColumn
    Friend ST_1 As CustomColumn_RightBorder
    Friend WithEvents S_2 As DataGridViewTextBoxColumn
    Friend ST_2 As CustomColumn_RightBorder
    Friend WithEvents S_3 As DataGridViewTextBoxColumn
    Friend ST_3 As CustomColumn_RightBorder
    Friend Stossen As CustomColumn_RightBorder
    Friend ZK As CustomColumn_RightBorder
    Friend Heben As CustomColumn_RightBorder
    Friend Heben_Platz As CustomColumn_RightBorder
    Friend Wertung2 As CustomColumn_RightBorder
    Friend WithEvents Wertung2_Platz As DataGridViewTextBoxColumn
End Class
