﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Auswertung_Protokoll
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuProtokoll = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLänderwertung = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuVereinswertung = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboAK = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboGK = New System.Windows.Forms.ComboBox()
        Me.dgvJoin = New System.Windows.Forms.DataGridView()
        Me.cboGruppe = New System.Windows.Forms.ComboBox()
        Me.lblGruppe = New System.Windows.Forms.Label()
        Me.btnProtokoll = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1160, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrint, Me.ToolStripMenuItem1, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 20)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuPrint
        '
        Me.mnuPrint.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuProtokoll, Me.mnuLänderwertung, Me.mnuVereinswertung})
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.mnuPrint.Size = New System.Drawing.Size(162, 22)
        Me.mnuPrint.Text = "&Drucken"
        '
        'mnuProtokoll
        '
        Me.mnuProtokoll.Name = "mnuProtokoll"
        Me.mnuProtokoll.Size = New System.Drawing.Size(156, 22)
        Me.mnuProtokoll.Text = "WK-&Protokoll"
        '
        'mnuLänderwertung
        '
        Me.mnuLänderwertung.Name = "mnuLänderwertung"
        Me.mnuLänderwertung.Size = New System.Drawing.Size(156, 22)
        Me.mnuLänderwertung.Text = "&Länderwertung"
        '
        'mnuVereinswertung
        '
        Me.mnuVereinswertung.Name = "mnuVereinswertung"
        Me.mnuVereinswertung.Size = New System.Drawing.Size(156, 22)
        Me.mnuVereinswertung.Text = "&Vereinswertung"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(159, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(162, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'lblCaption
        '
        Me.lblCaption.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption.ForeColor = System.Drawing.Color.Gold
        Me.lblCaption.Location = New System.Drawing.Point(0, 24)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(1160, 30)
        Me.lblCaption.TabIndex = 1
        Me.lblCaption.Text = "Wettkampf"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(600, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "&Altersklasse"
        Me.Label1.Visible = False
        '
        'cboAK
        '
        Me.cboAK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboAK.FormattingEnabled = True
        Me.cboAK.Location = New System.Drawing.Point(669, 66)
        Me.cboAK.Name = "cboAK"
        Me.cboAK.Size = New System.Drawing.Size(70, 21)
        Me.cboAK.TabIndex = 3
        Me.cboAK.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(778, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Gewicht&klasse"
        Me.Label2.Visible = False
        '
        'cboGK
        '
        Me.cboGK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboGK.FormattingEnabled = True
        Me.cboGK.Location = New System.Drawing.Point(860, 66)
        Me.cboGK.Name = "cboGK"
        Me.cboGK.Size = New System.Drawing.Size(70, 21)
        Me.cboGK.TabIndex = 5
        Me.cboGK.Visible = False
        '
        'dgvJoin
        '
        Me.dgvJoin.AllowUserToAddRows = False
        Me.dgvJoin.AllowUserToDeleteRows = False
        Me.dgvJoin.AllowUserToResizeColumns = False
        Me.dgvJoin.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Honeydew
        Me.dgvJoin.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvJoin.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvJoin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvJoin.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvJoin.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvJoin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightCyan
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvJoin.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvJoin.Location = New System.Drawing.Point(0, 98)
        Me.dgvJoin.Name = "dgvJoin"
        Me.dgvJoin.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvJoin.RowHeadersWidth = 25
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.dgvJoin.RowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvJoin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvJoin.Size = New System.Drawing.Size(1160, 399)
        Me.dgvJoin.TabIndex = 6
        '
        'cboGruppe
        '
        Me.cboGruppe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGruppe.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboGruppe.FormattingEnabled = True
        Me.cboGruppe.Location = New System.Drawing.Point(113, 66)
        Me.cboGruppe.Name = "cboGruppe"
        Me.cboGruppe.Size = New System.Drawing.Size(141, 21)
        Me.cboGruppe.TabIndex = 1
        '
        'lblGruppe
        '
        Me.lblGruppe.Location = New System.Drawing.Point(39, 69)
        Me.lblGruppe.Name = "lblGruppe"
        Me.lblGruppe.Size = New System.Drawing.Size(68, 18)
        Me.lblGruppe.TabIndex = 8
        Me.lblGruppe.Text = "&Gruppe"
        Me.lblGruppe.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnProtokoll
        '
        Me.btnProtokoll.Location = New System.Drawing.Point(298, 64)
        Me.btnProtokoll.Name = "btnProtokoll"
        Me.btnProtokoll.Size = New System.Drawing.Size(144, 23)
        Me.btnProtokoll.TabIndex = 9
        Me.btnProtokoll.Text = "Protokoll drucken"
        Me.btnProtokoll.UseVisualStyleBackColor = True
        '
        'Auswertung_Protokoll
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1160, 497)
        Me.Controls.Add(Me.btnProtokoll)
        Me.Controls.Add(Me.lblGruppe)
        Me.Controls.Add(Me.cboGruppe)
        Me.Controls.Add(Me.dgvJoin)
        Me.Controls.Add(Me.cboGK)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboAK)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblCaption)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1176, 535)
        Me.Name = "Auswertung_Protokoll"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Protokoll"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents lblCaption As Label
    Friend WithEvents mnuDatei As ToolStripMenuItem
    Friend WithEvents mnuPrint As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents mnuBeenden As ToolStripMenuItem
    Friend WithEvents Label1 As Label
    Friend WithEvents cboAK As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents cboGK As ComboBox
    Friend WithEvents dgvJoin As DataGridView
    Friend WithEvents cboGruppe As ComboBox
    Friend WithEvents lblGruppe As Label
    Friend WithEvents mnuProtokoll As ToolStripMenuItem
    Friend WithEvents mnuLänderwertung As ToolStripMenuItem
    Friend WithEvents mnuVereinswertung As ToolStripMenuItem
    Friend WithEvents btnProtokoll As Button
End Class
