﻿
Imports System.ComponentModel
Imports MySqlConnector

Public Class Drucken_Meldeliste

    Property Bereich As String
    Dim Func As New myFunction

    Dim Query As String
    Dim cmd As MySqlCommand
    Dim dtMeldung As DataTable
    Dim dvMeldung As DataView

    Dim dtVorlage As New DataTable
    Dim dvVorlage As DataView
    Dim Vorlage As String
    Dim report As FastReport.Report

    Private Sub Me_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If btnCancel.Text = "Abbrechen" Then
            e.Cancel = True
        Else
            Dim _Layout = String.Empty
            If grpLayout.Visible Then
                _Layout += ", " + If(optHoch.Checked, "hoch", "quer")
                _Layout += ", " + If(optAcrossDown.Checked, "AcrossDown", "DownAcross")
                _Layout += ", " + nudColCount.Value.ToString
            End If
            NativeMethods.INI_Write(Bereich, "Vorlage" + _Layout, Vorlage, App_IniFile)
        End If
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        'Dim txt = Split(Text, " ")(0)
        'Bereich = txt.Substring(0, 1) + LCase(txt.Substring(1))

        Dim tTip As New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        tTip.SetToolTip(btnSearch, "Druckvorlage suchen")
        tTip.SetToolTip(optAcrossDown, "Anordung der Ereignisse in Reihen")
        tTip.SetToolTip(optDownAcross, "Anordnung der Ereignisse in Spalten")

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        If Bereich.Equals("Meldeliste") Then
            grpLayout.Visible = False
            grpGruppierung.Location = New Point(22, 22)
            'Query = "SELECT m.Wettkampf, m.attend, m.Teilnehmer, IfNull(m.Wiegen, m.Gewicht) Wiegen, m.Meldedatum, " &
            '            "if(m.idAK < 100, m.idAK, 100) AKsort, if(m.idAK < 100, m.AK, 'Masters') AKtext, " &
            '            "m.idAK, m.AK, m.idGK, m.GK, m.Reissen, m.Stossen, m.Zweikampf, " &
            '            "Concat(a.Titel, ' ', a.Nachname) Nachname, a.Vorname, Year(a.Geburtstag) Jahrgang, a.Geschlecht, IfNull(v.Kurzname, v.Vereinsname) Vereinsname, IfNull(r.Kurz, r.region_3) Land, s.state_iso3 Staat, s.state_name Country " &
            '            "FROM meldung m " &
            '            "LEFT JOIN athleten a ON a.idTeilnehmer = m.Teilnehmer " &
            '            "LEFT JOIN verein v ON v.idVerein = a.Verein " &
            '            "LEFT JOIN region r ON r.region_id = v.Region " &
            '            "LEFT JOIN staaten s ON s.state_id = a.Staat " &
            '            "WHERE m.Wettkampf = " & Wettkampf.ID & ";"
            'Using conn As New MySqlConnection(User.ConnString)
            '    Try
            '        conn.Open()
            '        cmd = New MySqlCommand(Query, conn)
            '        dtMeldung.Load(cmd.ExecuteReader)
            '    Catch ex As Exception
            '    End Try
            'End Using
            dtMeldung = Get_Data(Wettkampf.ID.ToString)
        ElseIf Bereich.Equals("Zeitplan") Then
            grpGruppierung.Visible = False
            grpLayout.Location = New Point(22, 22)
            dtMeldung = New DataTable
            If Not IsNothing(Tag) Then
                dtMeldung = CType(Tag, DataTable)
            Else
                dtMeldung = Func.Get_Schedule
            End If
        End If

        If IsNothing(dtMeldung) OrElse dtMeldung.Rows.Count = 0 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Keine Daten für " & Wettkampf.Bezeichnung & " gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Close()
                Return
            End Using
        End If

        dvMeldung = New DataView(dtMeldung)

#Region "Vorlage"
        Vorlage = NativeMethods.INI_Read(Bereich, "Vorlage", App_IniFile)
        If Vorlage.Equals("?") Then Vorlage = String.Empty

        dtVorlage.Columns.Add("FileName", GetType(String))
        dtVorlage.Columns.Add("Directory", GetType(String))
        Dim cols(1) As DataColumn
        cols(0) = dtVorlage.Columns("FileName")
        cols(1) = dtVorlage.Columns("Directory")
        dtVorlage.PrimaryKey = cols

        Dim v = Directory.GetFiles(Path.Combine(Application.StartupPath, "Report"), Bereich & "*.frx", SearchOption.AllDirectories).ToList
        For i = 0 To v.Count - 1
            v(i) = Replace(v(i), Path.Combine(Application.StartupPath, "Report") + "\", "")
            v(i) = Replace(v(i), ".frx", "")
            dtVorlage.Rows.Add(v(i), Path.Combine(Application.StartupPath, "Report"))
        Next
        dvVorlage = New DataView(dtVorlage)
        dvVorlage.Sort = "FileName"

        With cboVorlage
            .DataSource = dvVorlage
            .DisplayMember = "FileName"
            .ValueMember = "Directory"
            .SelectedIndex = .FindStringExact(Vorlage)
        End With

        If Vorlage.Contains(", ") Then
            Dim _Layout = Split(Vorlage, ", ")
            Vorlage = _Layout(0)
            If _Layout.Count > 1 Then
                If _Layout(1) = "hoch" Then
                    optHoch.Checked = True
                Else
                    optQuer.Checked = True
                End If
            End If
            If _Layout.Count > 2 Then
                If _Layout(2) = "AcrossDown" Then
                    optAcrossDown.Checked = True
                Else
                    optDownAcross.Checked = True
                End If
            End If
            If _Layout.Count > 3 Then nudColCount.Value = CInt(_Layout(3))
        End If
#End Region

        formMain.Change_MainProgressBarValue(0)

        Cursor = Cursors.Default

    End Sub

    Private Sub cboVorlage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVorlage.SelectedIndexChanged
        If cboVorlage.Focused Then Vorlage = cboVorlage.Text
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If btnCancel.Text = "Beenden" Then Close()
    End Sub
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        User.Check_MySQLDriver(Me)

        Cursor = Cursors.WaitCursor
        Using report = New FastReport.Report
            With report
                Try
                    .Load(Path.Combine(CType(cboVorlage.SelectedItem, DataRowView)(1).ToString, CType(cboVorlage.SelectedItem, DataRowView)(0).ToString + ".frx"))
                    .Dictionary.Connections(0).ConnectionString = User.FR_ConnString
                    .Design()
                Catch ex As Exception
                    Using New Centered_MessageBox(Me)
                        MessageBox.Show("Vorlage nicht gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End Using
                    Return
                End Try
            End With
        End Using
        Cursor = Cursors.Default
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        With OpenFileDialog1
            .Filter = "Druck-Vorlagen (*.frx)|*.frx|Alle Dateien (*.*)|*.*"
            .InitialDirectory = Path.Combine(Application.StartupPath, "Report")
            If .ShowDialog() = DialogResult.Cancel Then Exit Sub
            If Not .FileName.Contains(".frx") Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Die ausgewählte Datei ist keine Druckvorlage.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
            Else
                Try
                    Dim directory = .FileName.Replace(.SafeFileName, "")
                    Vorlage = Split(.SafeFileName, ".")(0)
                    directory = Strings.Left(directory, directory.Length - 1)
                    dtVorlage.Rows.Add(Vorlage, directory)
                    cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage)
                Catch ex As Exception
                End Try
            End If
        End With
        cboVorlage.Focus()
    End Sub
    Private Sub btnVorschau_EnabledChanged(sender As Object, e As EventArgs) Handles btnVorschau.EnabledChanged
        btnPrint.Enabled = btnVorschau.Enabled
    End Sub
    Private Sub btnVorschau_Click(sender As Object, e As EventArgs) Handles btnVorschau.Click, btnPrint.Click

        User.Check_MySQLDriver(Me)

        Cursor = Cursors.WaitCursor

        Dim dvWettkampf = New DataView(Glob.dtWK)

        report = New FastReport.Report
        With report

#Region "Vorlage"
            If Vorlage = String.Empty AndAlso cboVorlage.Text = String.Empty Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Keine Druckvorlage ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                Cursor = Cursors.Default
                Return
            End If
            Try
                .Load(Path.Combine(cboVorlage.SelectedValue.ToString, If(Not String.IsNullOrEmpty(Vorlage), Vorlage, cboVorlage.Text) + ".frx"))
                .Dictionary.Connections(0).ConnectionString = User.FR_ConnString
                btnVorschau.Enabled = False
            Catch ex As Exception
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Vorlage nicht gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
                Cursor = Cursors.Default
                Return
            End Try
#End Region

            For Each WK In Wettkampf.Identities.Keys

                dvWettkampf.RowFilter = "Wettkampf = " & WK

#Region "Create"
                If Bereich.Equals("Meldeliste") Then

                    dvMeldung = New DataView(Get_Data(WK))

                    If optSex.Checked Then
                        .SetParameterValue("Header2_Condition", "[Meldeliste.Geschlecht]")
                        .SetParameterValue("Header3_Condition", "[Meldeliste.Geschlecht]")
                        .SetParameterValue("AK_Visible", False)
                        .SetParameterValue("GK_Visible", False)
                        .SetParameterValue("Header2_NewPage", False)
                        .SetParameterValue("Header3_NewPage", False)
                    ElseIf optAK.Checked Then
                        .SetParameterValue("Header2_Condition", "[Meldeliste.AKsort]")
                        .SetParameterValue("Header3_Condition", "[Meldeliste.AKsort]") '"IIf([Meldeliste.idAK]<100,[Meldeliste.idAK],Substring([Meldeliste.AK],0,6)='Master'")
                        .SetParameterValue("AK_Visible", True)
                        .SetParameterValue("GK_Visible", False)
                        .SetParameterValue("Header2_NewPage", chkNewPage.Checked)
                        .SetParameterValue("Header3_NewPage", False)
                    ElseIf optAK_GK.Checked Then
                        .SetParameterValue("Header2_Condition", "[Meldeliste.idAK]")
                        .SetParameterValue("Header3_Condition", "[Meldeliste.idGK]")
                        .SetParameterValue("AK_Visible", True)
                        .SetParameterValue("GK_Visible", True)
                        .SetParameterValue("Header2_NewPage", chkNewPage.Checked)
                        .SetParameterValue("Header3_NewPage", False)
                    ElseIf optGK.Checked Then
                        .SetParameterValue("Header2_Condition", "[Meldeliste.idGK]")
                        .SetParameterValue("Header3_Condition", "[Meldeliste.idGK]")
                        .SetParameterValue("AK_Visible", False)
                        .SetParameterValue("GK_Visible", True)
                        .SetParameterValue("Header2_NewPage", chkNewPage.Checked)
                        .SetParameterValue("Header3_NewPage", chkNewPage.Checked)
                    End If

                    .SetParameterValue("Header1_NewPage", chkNewPage.Checked)
                    dvMeldung.RowFilter = If(chkAnwesend.Checked, "attend = True OR Convert(IsNull(attend,''), System.String) = ''", String.Empty)
                    dvMeldung.Sort = "Nachname, Vorname, Jahrgang"

                    Dim tn = (From x In dvMeldung.ToTable
                              Group x By Sex = x.Field(Of String)("Geschlecht") Into Group
                              Order By Sex Ascending).ToList

                    .SetParameterValue("TN_gesamt", dvMeldung.Count)
                    If tn.Count > 1 Then
                        .SetParameterValue("TN_männlich", tn(0).Group.Count)
                        .SetParameterValue("TN_weiblich", tn(1).Group.Count)
                    Else
                        .SetParameterValue("TN_" & If(tn(0).Sex = "m", "männlich", "weiblich"), tn(0).Group.Count)
                    End If

                ElseIf Bereich.Equals("Zeitplan") Then

                    .SetParameterValue("Landscape", optQuer.Checked)
                    .SetParameterValue("DataColumns", CInt(nudColCount.Value))
                    .SetParameterValue("ColumnsLayout", If(optAcrossDown.Checked, 0, 1))
                    dvMeldung.Sort = "Datum, Beginn, Ende"
                End If


                Try
                    .SetParameterValue("International", Wettkampf.International)

                    .RegisterData(dvMeldung.ToTable, Bereich)
                    '.RegisterData(Glob.dtWK, "wk")
                    .RegisterData(dvWettkampf.ToTable, "wk")

                Catch ex As Exception
                    LogMessage("Drucken_Listen: Register: " & Bereich & ": " & ex.Message, ex.StackTrace)
                End Try
#End Region

#Region "Print"
                Try
                    Dim s = New FastReport.EnvironmentSettings
                    s.ReportSettings.ShowProgress = False
                    .Prepare(True)
                Catch ex As Exception
                    LogMessage("Drucken_Listen: Prepare: " & Bereich & ": " & ex.Message, ex.StackTrace)
                End Try
            Next

            Try
            If CType(sender, Button).Equals(btnVorschau) Then
                .ShowPrepared()
            ElseIf CType(sender, Button).Equals(btnPrint) Then
                .PrintPrepared()
            End If
        Catch ex As Exception
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Abbruch wegen Fehler im Formular." & vbNewLine & "Weitere Informationen im Log-File.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            LogMessage("Drucken_Listen: Print: " & Bereich & ": " & ex.Message, ex.StackTrace)
        End Try
#End Region
        End With

        btnVorschau.Enabled = True
        Cursor = Cursors.Default
    End Sub

    Private Sub optHoch_CheckedChanged(sender As Object, e As EventArgs) Handles optHoch.CheckedChanged
        nudColCount.Maximum = 2
    End Sub

    Private Sub optQuer_CheckedChanged(sender As Object, e As EventArgs) Handles optQuer.CheckedChanged
        nudColCount.Maximum = 3
    End Sub

    Private Function Get_Data(WK As String) As DataTable
        Dim dt As New DataTable
        Query = "SELECT m.Wettkampf, m.attend, m.Teilnehmer, IfNull(m.Wiegen, m.Gewicht) Wiegen, m.Meldedatum, " &
                        "if(m.idAK < 100, m.idAK, 100) AKsort, if(m.idAK < 100, m.AK, 'Masters') AKtext, " &
                        "m.idAK, m.AK, m.idGK, m.GK, m.Reissen, m.Stossen, m.Zweikampf, " &
                        "Concat(a.Titel, ' ', a.Nachname) Nachname, a.Vorname, Year(a.Geburtstag) Jahrgang, a.Geschlecht, IfNull(v.Kurzname, v.Vereinsname) Vereinsname, IfNull(r.Kurz, r.region_3) Land, s.state_iso3 Staat, s.state_name Country " &
                        "FROM meldung m " &
                        "LEFT JOIN athleten a ON a.idTeilnehmer = m.Teilnehmer " &
                        "LEFT JOIN verein v ON v.idVerein = a.Verein " &
                        "LEFT JOIN region r ON r.region_id = v.Region " &
                        "LEFT JOIN staaten s ON s.state_id = a.Staat " &
                        "WHERE m.Wettkampf = " & WK & ";"
        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                cmd = New MySqlCommand(Query, conn)
                dt.Load(cmd.ExecuteReader)
            Catch ex As Exception
            End Try
        End Using
        Return dt
    End Function

End Class