﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmGruppenMasters
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboGeschlecht = New System.Windows.Forms.ComboBox()
        Me.cboGruppe = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lstGK1 = New System.Windows.Forms.CheckedListBox()
        Me.btnGK1 = New System.Windows.Forms.Button()
        Me.btnGK2 = New System.Windows.Forms.Button()
        Me.lstGK2 = New System.Windows.Forms.CheckedListBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnGK3 = New System.Windows.Forms.Button()
        Me.lstGK3 = New System.Windows.Forms.CheckedListBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnGK4 = New System.Windows.Forms.Button()
        Me.lstGK4 = New System.Windows.Forms.CheckedListBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnGK5 = New System.Windows.Forms.Button()
        Me.lstGK5 = New System.Windows.Forms.CheckedListBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnGK6 = New System.Windows.Forms.Button()
        Me.lstGK6 = New System.Windows.Forms.CheckedListBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnGK7 = New System.Windows.Forms.Button()
        Me.lstGK7 = New System.Windows.Forms.CheckedListBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnGK8 = New System.Windows.Forms.Button()
        Me.lstGK8 = New System.Windows.Forms.CheckedListBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnGK9 = New System.Windows.Forms.Button()
        Me.lstGK9 = New System.Windows.Forms.CheckedListBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btnGk10 = New System.Windows.Forms.Button()
        Me.lstGK10 = New System.Windows.Forms.CheckedListBox()
        Me.txtGr1 = New System.Windows.Forms.TextBox()
        Me.txtGr2 = New System.Windows.Forms.TextBox()
        Me.txtGr3 = New System.Windows.Forms.TextBox()
        Me.txtGr4 = New System.Windows.Forms.TextBox()
        Me.txtGr5 = New System.Windows.Forms.TextBox()
        Me.txtGr6 = New System.Windows.Forms.TextBox()
        Me.txtGr7 = New System.Windows.Forms.TextBox()
        Me.txtGr8 = New System.Windows.Forms.TextBox()
        Me.txtGr9 = New System.Windows.Forms.TextBox()
        Me.txtGr10 = New System.Windows.Forms.TextBox()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDrucken = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLayout = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuMeldungen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKonflikte = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Geschlecht"
        '
        'cboGeschlecht
        '
        Me.cboGeschlecht.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGeschlecht.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboGeschlecht.FormattingEnabled = True
        Me.cboGeschlecht.Items.AddRange(New Object() {"weiblich", "männlich"})
        Me.cboGeschlecht.Location = New System.Drawing.Point(20, 57)
        Me.cboGeschlecht.Name = "cboGeschlecht"
        Me.cboGeschlecht.Size = New System.Drawing.Size(128, 21)
        Me.cboGeschlecht.TabIndex = 1
        '
        'cboGruppe
        '
        Me.cboGruppe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGruppe.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboGruppe.FormattingEnabled = True
        Me.cboGruppe.Location = New System.Drawing.Point(173, 57)
        Me.cboGruppe.Name = "cboGruppe"
        Me.cboGruppe.Size = New System.Drawing.Size(60, 21)
        Me.cboGruppe.Sorted = True
        Me.cboGruppe.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(170, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Gruppe"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 105)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "AK 1"
        '
        'lstGK1
        '
        Me.lstGK1.CheckOnClick = True
        Me.lstGK1.FormattingEnabled = True
        Me.lstGK1.Location = New System.Drawing.Point(58, 121)
        Me.lstGK1.Name = "lstGK1"
        Me.lstGK1.Size = New System.Drawing.Size(78, 94)
        Me.lstGK1.TabIndex = 5
        Me.lstGK1.Tag = "1"
        Me.lstGK1.Visible = False
        '
        'btnGK1
        '
        Me.btnGK1.Enabled = False
        Me.btnGK1.Location = New System.Drawing.Point(53, 100)
        Me.btnGK1.Name = "btnGK1"
        Me.btnGK1.Size = New System.Drawing.Size(95, 22)
        Me.btnGK1.TabIndex = 7
        Me.btnGK1.Tag = "1"
        Me.btnGK1.Text = "Gewichtsklassen"
        Me.btnGK1.UseVisualStyleBackColor = True
        '
        'btnGK2
        '
        Me.btnGK2.Enabled = False
        Me.btnGK2.Location = New System.Drawing.Point(53, 128)
        Me.btnGK2.Name = "btnGK2"
        Me.btnGK2.Size = New System.Drawing.Size(95, 22)
        Me.btnGK2.TabIndex = 10
        Me.btnGK2.Tag = "2"
        Me.btnGK2.Text = "Gewichtsklassen"
        Me.btnGK2.UseVisualStyleBackColor = True
        '
        'lstGK2
        '
        Me.lstGK2.CheckOnClick = True
        Me.lstGK2.FormattingEnabled = True
        Me.lstGK2.Location = New System.Drawing.Point(58, 149)
        Me.lstGK2.Name = "lstGK2"
        Me.lstGK2.Size = New System.Drawing.Size(78, 94)
        Me.lstGK2.TabIndex = 9
        Me.lstGK2.Tag = "2"
        Me.lstGK2.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 133)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "AK 2"
        '
        'btnGK3
        '
        Me.btnGK3.Enabled = False
        Me.btnGK3.Location = New System.Drawing.Point(53, 156)
        Me.btnGK3.Name = "btnGK3"
        Me.btnGK3.Size = New System.Drawing.Size(95, 22)
        Me.btnGK3.TabIndex = 13
        Me.btnGK3.Tag = "3"
        Me.btnGK3.Text = "Gewichtsklassen"
        Me.btnGK3.UseVisualStyleBackColor = True
        '
        'lstGK3
        '
        Me.lstGK3.CheckOnClick = True
        Me.lstGK3.FormattingEnabled = True
        Me.lstGK3.Location = New System.Drawing.Point(58, 177)
        Me.lstGK3.Name = "lstGK3"
        Me.lstGK3.Size = New System.Drawing.Size(78, 94)
        Me.lstGK3.TabIndex = 12
        Me.lstGK3.Tag = "3"
        Me.lstGK3.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(17, 161)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(30, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "AK 3"
        '
        'btnGK4
        '
        Me.btnGK4.Enabled = False
        Me.btnGK4.Location = New System.Drawing.Point(53, 184)
        Me.btnGK4.Name = "btnGK4"
        Me.btnGK4.Size = New System.Drawing.Size(95, 22)
        Me.btnGK4.TabIndex = 16
        Me.btnGK4.Tag = "4"
        Me.btnGK4.Text = "Gewichtsklassen"
        Me.btnGK4.UseVisualStyleBackColor = True
        '
        'lstGK4
        '
        Me.lstGK4.CheckOnClick = True
        Me.lstGK4.FormattingEnabled = True
        Me.lstGK4.Location = New System.Drawing.Point(58, 205)
        Me.lstGK4.Name = "lstGK4"
        Me.lstGK4.Size = New System.Drawing.Size(78, 94)
        Me.lstGK4.TabIndex = 15
        Me.lstGK4.Tag = "4"
        Me.lstGK4.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 189)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(30, 13)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "AK 4"
        '
        'btnGK5
        '
        Me.btnGK5.Enabled = False
        Me.btnGK5.Location = New System.Drawing.Point(53, 212)
        Me.btnGK5.Name = "btnGK5"
        Me.btnGK5.Size = New System.Drawing.Size(95, 22)
        Me.btnGK5.TabIndex = 19
        Me.btnGK5.Tag = "5"
        Me.btnGK5.Text = "Gewichtsklassen"
        Me.btnGK5.UseVisualStyleBackColor = True
        '
        'lstGK5
        '
        Me.lstGK5.CheckOnClick = True
        Me.lstGK5.FormattingEnabled = True
        Me.lstGK5.Location = New System.Drawing.Point(58, 233)
        Me.lstGK5.Name = "lstGK5"
        Me.lstGK5.Size = New System.Drawing.Size(78, 94)
        Me.lstGK5.TabIndex = 18
        Me.lstGK5.Tag = "5"
        Me.lstGK5.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(17, 217)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(30, 13)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "AK 5"
        '
        'btnGK6
        '
        Me.btnGK6.Enabled = False
        Me.btnGK6.Location = New System.Drawing.Point(53, 240)
        Me.btnGK6.Name = "btnGK6"
        Me.btnGK6.Size = New System.Drawing.Size(95, 22)
        Me.btnGK6.TabIndex = 22
        Me.btnGK6.Tag = "6"
        Me.btnGK6.Text = "Gewichtsklassen"
        Me.btnGK6.UseVisualStyleBackColor = True
        '
        'lstGK6
        '
        Me.lstGK6.CheckOnClick = True
        Me.lstGK6.FormattingEnabled = True
        Me.lstGK6.Location = New System.Drawing.Point(58, 261)
        Me.lstGK6.Name = "lstGK6"
        Me.lstGK6.Size = New System.Drawing.Size(78, 94)
        Me.lstGK6.TabIndex = 21
        Me.lstGK6.Tag = "6"
        Me.lstGK6.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(17, 245)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(30, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "AK 6"
        '
        'btnGK7
        '
        Me.btnGK7.Enabled = False
        Me.btnGK7.Location = New System.Drawing.Point(53, 268)
        Me.btnGK7.Name = "btnGK7"
        Me.btnGK7.Size = New System.Drawing.Size(95, 22)
        Me.btnGK7.TabIndex = 25
        Me.btnGK7.Tag = "7"
        Me.btnGK7.Text = "Gewichtsklassen"
        Me.btnGK7.UseVisualStyleBackColor = True
        '
        'lstGK7
        '
        Me.lstGK7.CheckOnClick = True
        Me.lstGK7.FormattingEnabled = True
        Me.lstGK7.Location = New System.Drawing.Point(58, 289)
        Me.lstGK7.Name = "lstGK7"
        Me.lstGK7.Size = New System.Drawing.Size(78, 94)
        Me.lstGK7.TabIndex = 24
        Me.lstGK7.Tag = "7"
        Me.lstGK7.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(17, 273)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(30, 13)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "AK 7"
        '
        'btnGK8
        '
        Me.btnGK8.Enabled = False
        Me.btnGK8.Location = New System.Drawing.Point(53, 296)
        Me.btnGK8.Name = "btnGK8"
        Me.btnGK8.Size = New System.Drawing.Size(95, 22)
        Me.btnGK8.TabIndex = 28
        Me.btnGK8.Tag = "8"
        Me.btnGK8.Text = "Gewichtsklassen"
        Me.btnGK8.UseVisualStyleBackColor = True
        '
        'lstGK8
        '
        Me.lstGK8.CheckOnClick = True
        Me.lstGK8.FormattingEnabled = True
        Me.lstGK8.Location = New System.Drawing.Point(58, 317)
        Me.lstGK8.Name = "lstGK8"
        Me.lstGK8.Size = New System.Drawing.Size(78, 94)
        Me.lstGK8.TabIndex = 27
        Me.lstGK8.Tag = "8"
        Me.lstGK8.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(17, 301)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(30, 13)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "AK 8"
        '
        'btnGK9
        '
        Me.btnGK9.Enabled = False
        Me.btnGK9.Location = New System.Drawing.Point(53, 324)
        Me.btnGK9.Name = "btnGK9"
        Me.btnGK9.Size = New System.Drawing.Size(95, 22)
        Me.btnGK9.TabIndex = 31
        Me.btnGK9.Tag = "9"
        Me.btnGK9.Text = "Gewichtsklassen"
        Me.btnGK9.UseVisualStyleBackColor = True
        '
        'lstGK9
        '
        Me.lstGK9.CheckOnClick = True
        Me.lstGK9.FormattingEnabled = True
        Me.lstGK9.Location = New System.Drawing.Point(58, 345)
        Me.lstGK9.Name = "lstGK9"
        Me.lstGK9.Size = New System.Drawing.Size(78, 94)
        Me.lstGK9.TabIndex = 30
        Me.lstGK9.Tag = "9"
        Me.lstGK9.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(17, 329)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(30, 13)
        Me.Label11.TabIndex = 29
        Me.Label11.Text = "AK 9"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(17, 357)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(36, 13)
        Me.Label12.TabIndex = 32
        Me.Label12.Text = "AK 10"
        '
        'btnGk10
        '
        Me.btnGk10.Enabled = False
        Me.btnGk10.Location = New System.Drawing.Point(53, 352)
        Me.btnGk10.Name = "btnGk10"
        Me.btnGk10.Size = New System.Drawing.Size(95, 22)
        Me.btnGk10.TabIndex = 34
        Me.btnGk10.Tag = "10"
        Me.btnGk10.Text = "Gewichtsklassen"
        Me.btnGk10.UseVisualStyleBackColor = True
        '
        'lstGK10
        '
        Me.lstGK10.CheckOnClick = True
        Me.lstGK10.FormattingEnabled = True
        Me.lstGK10.Location = New System.Drawing.Point(58, 373)
        Me.lstGK10.Name = "lstGK10"
        Me.lstGK10.Size = New System.Drawing.Size(78, 94)
        Me.lstGK10.TabIndex = 33
        Me.lstGK10.Tag = "10"
        Me.lstGK10.Visible = False
        '
        'txtGr1
        '
        Me.txtGr1.Enabled = False
        Me.txtGr1.Location = New System.Drawing.Point(173, 100)
        Me.txtGr1.Name = "txtGr1"
        Me.txtGr1.Size = New System.Drawing.Size(40, 20)
        Me.txtGr1.TabIndex = 35
        Me.txtGr1.Tag = "1"
        Me.txtGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGr2
        '
        Me.txtGr2.Enabled = False
        Me.txtGr2.Location = New System.Drawing.Point(173, 128)
        Me.txtGr2.Name = "txtGr2"
        Me.txtGr2.Size = New System.Drawing.Size(40, 20)
        Me.txtGr2.TabIndex = 36
        Me.txtGr2.Tag = "2"
        Me.txtGr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGr3
        '
        Me.txtGr3.Enabled = False
        Me.txtGr3.Location = New System.Drawing.Point(173, 156)
        Me.txtGr3.Name = "txtGr3"
        Me.txtGr3.Size = New System.Drawing.Size(40, 20)
        Me.txtGr3.TabIndex = 37
        Me.txtGr3.Tag = "3"
        Me.txtGr3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGr4
        '
        Me.txtGr4.Enabled = False
        Me.txtGr4.Location = New System.Drawing.Point(173, 184)
        Me.txtGr4.Name = "txtGr4"
        Me.txtGr4.Size = New System.Drawing.Size(40, 20)
        Me.txtGr4.TabIndex = 38
        Me.txtGr4.Tag = "4"
        Me.txtGr4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGr5
        '
        Me.txtGr5.Enabled = False
        Me.txtGr5.Location = New System.Drawing.Point(173, 212)
        Me.txtGr5.Name = "txtGr5"
        Me.txtGr5.Size = New System.Drawing.Size(40, 20)
        Me.txtGr5.TabIndex = 39
        Me.txtGr5.Tag = "5"
        Me.txtGr5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGr6
        '
        Me.txtGr6.Enabled = False
        Me.txtGr6.Location = New System.Drawing.Point(173, 240)
        Me.txtGr6.Name = "txtGr6"
        Me.txtGr6.Size = New System.Drawing.Size(40, 20)
        Me.txtGr6.TabIndex = 40
        Me.txtGr6.Tag = "6"
        Me.txtGr6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGr7
        '
        Me.txtGr7.Enabled = False
        Me.txtGr7.Location = New System.Drawing.Point(173, 268)
        Me.txtGr7.Name = "txtGr7"
        Me.txtGr7.Size = New System.Drawing.Size(40, 20)
        Me.txtGr7.TabIndex = 41
        Me.txtGr7.Tag = "7"
        Me.txtGr7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGr8
        '
        Me.txtGr8.Enabled = False
        Me.txtGr8.Location = New System.Drawing.Point(173, 296)
        Me.txtGr8.Name = "txtGr8"
        Me.txtGr8.Size = New System.Drawing.Size(40, 20)
        Me.txtGr8.TabIndex = 42
        Me.txtGr8.Tag = "8"
        Me.txtGr8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGr9
        '
        Me.txtGr9.Enabled = False
        Me.txtGr9.Location = New System.Drawing.Point(173, 324)
        Me.txtGr9.Name = "txtGr9"
        Me.txtGr9.Size = New System.Drawing.Size(40, 20)
        Me.txtGr9.TabIndex = 43
        Me.txtGr9.Tag = "9"
        Me.txtGr9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGr10
        '
        Me.txtGr10.Enabled = False
        Me.txtGr10.Location = New System.Drawing.Point(173, 352)
        Me.txtGr10.Name = "txtGr10"
        Me.txtGr10.Size = New System.Drawing.Size(40, 20)
        Me.txtGr10.TabIndex = 44
        Me.txtGr10.Tag = "10"
        Me.txtGr10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgv
        '
        Me.dgv.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgv.Location = New System.Drawing.Point(259, 57)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgv.Size = New System.Drawing.Size(520, 375)
        Me.dgv.TabIndex = 45
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(256, 40)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(36, 13)
        Me.Label13.TabIndex = 47
        Me.Label13.Text = "Heber"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.MenuBar
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuExtras})
        Me.MenuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.MenuStrip1.Size = New System.Drawing.Size(809, 24)
        Me.MenuStrip1.TabIndex = 200
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSpeichern, Me.mnuDrucken, Me.ToolStripMenuItem1, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 20)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuSpeichern
        '
        Me.mnuSpeichern.Name = "mnuSpeichern"
        Me.mnuSpeichern.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSpeichern.Size = New System.Drawing.Size(168, 22)
        Me.mnuSpeichern.Text = "&Speichern"
        '
        'mnuDrucken
        '
        Me.mnuDrucken.Name = "mnuDrucken"
        Me.mnuDrucken.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.mnuDrucken.Size = New System.Drawing.Size(168, 22)
        Me.mnuDrucken.Text = "&Drucken"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(165, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(168, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLayout, Me.ToolStripMenuItem2, Me.mnuMeldungen, Me.mnuKonflikte})
        Me.mnuExtras.Name = "mnuExtras"
        Me.mnuExtras.Size = New System.Drawing.Size(49, 20)
        Me.mnuExtras.Text = "&Extras"
        '
        'mnuLayout
        '
        Me.mnuLayout.Name = "mnuLayout"
        Me.mnuLayout.Size = New System.Drawing.Size(210, 22)
        Me.mnuLayout.Text = "&Tabellen-Layout"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(207, 6)
        '
        'mnuMeldungen
        '
        Me.mnuMeldungen.CheckOnClick = True
        Me.mnuMeldungen.Name = "mnuMeldungen"
        Me.mnuMeldungen.Size = New System.Drawing.Size(210, 22)
        Me.mnuMeldungen.Text = "&Meldungen ausblenden"
        '
        'mnuKonflikte
        '
        Me.mnuKonflikte.Checked = True
        Me.mnuKonflikte.CheckOnClick = True
        Me.mnuKonflikte.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuKonflikte.Name = "mnuKonflikte"
        Me.mnuKonflikte.Size = New System.Drawing.Size(210, 22)
        Me.mnuKonflikte.Text = "bei &Konflikten nachfragen"
        '
        'frmGruppenMasters
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(809, 474)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.txtGr10)
        Me.Controls.Add(Me.txtGr9)
        Me.Controls.Add(Me.txtGr8)
        Me.Controls.Add(Me.txtGr7)
        Me.Controls.Add(Me.txtGr6)
        Me.Controls.Add(Me.txtGr5)
        Me.Controls.Add(Me.txtGr4)
        Me.Controls.Add(Me.txtGr3)
        Me.Controls.Add(Me.txtGr2)
        Me.Controls.Add(Me.txtGr1)
        Me.Controls.Add(Me.btnGk10)
        Me.Controls.Add(Me.lstGK10)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.btnGK9)
        Me.Controls.Add(Me.lstGK9)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.btnGK8)
        Me.Controls.Add(Me.lstGK8)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.btnGK7)
        Me.Controls.Add(Me.lstGK7)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.btnGK6)
        Me.Controls.Add(Me.lstGK6)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.btnGK5)
        Me.Controls.Add(Me.lstGK5)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.btnGK4)
        Me.Controls.Add(Me.lstGK4)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnGK3)
        Me.Controls.Add(Me.lstGK3)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnGK2)
        Me.Controls.Add(Me.lstGK2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnGK1)
        Me.Controls.Add(Me.lstGK1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboGruppe)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboGeschlecht)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGruppenMasters"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Masters-Gruppen"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents cboGeschlecht As ComboBox
    Friend WithEvents cboGruppe As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lstGK1 As CheckedListBox
    Friend WithEvents btnGK1 As Button
    Friend WithEvents btnGK2 As Button
    Friend WithEvents lstGK2 As CheckedListBox
    Friend WithEvents Label4 As Label
    Friend WithEvents btnGK3 As Button
    Friend WithEvents lstGK3 As CheckedListBox
    Friend WithEvents Label5 As Label
    Friend WithEvents btnGK4 As Button
    Friend WithEvents lstGK4 As CheckedListBox
    Friend WithEvents Label6 As Label
    Friend WithEvents btnGK5 As Button
    Friend WithEvents lstGK5 As CheckedListBox
    Friend WithEvents Label7 As Label
    Friend WithEvents btnGK6 As Button
    Friend WithEvents lstGK6 As CheckedListBox
    Friend WithEvents Label8 As Label
    Friend WithEvents btnGK7 As Button
    Friend WithEvents lstGK7 As CheckedListBox
    Friend WithEvents Label9 As Label
    Friend WithEvents btnGK8 As Button
    Friend WithEvents lstGK8 As CheckedListBox
    Friend WithEvents Label10 As Label
    Friend WithEvents btnGK9 As Button
    Friend WithEvents lstGK9 As CheckedListBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents btnGk10 As Button
    Friend WithEvents lstGK10 As CheckedListBox
    Friend WithEvents txtGr1 As TextBox
    Friend WithEvents txtGr2 As TextBox
    Friend WithEvents txtGr3 As TextBox
    Friend WithEvents txtGr4 As TextBox
    Friend WithEvents txtGr5 As TextBox
    Friend WithEvents txtGr6 As TextBox
    Friend WithEvents txtGr7 As TextBox
    Friend WithEvents txtGr8 As TextBox
    Friend WithEvents txtGr9 As TextBox
    Friend WithEvents txtGr10 As TextBox
    Friend WithEvents dgv As DataGridView
    Friend WithEvents Label13 As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents mnuDatei As ToolStripMenuItem
    Friend WithEvents mnuSpeichern As ToolStripMenuItem
    Friend WithEvents mnuDrucken As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents mnuBeenden As ToolStripMenuItem
    Friend WithEvents mnuExtras As ToolStripMenuItem
    Friend WithEvents mnuLayout As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents mnuMeldungen As ToolStripMenuItem
    Friend WithEvents mnuKonflikte As ToolStripMenuItem
End Class
