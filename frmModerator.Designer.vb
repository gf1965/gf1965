﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModerator
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmModerator))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.lblDurchgang = New System.Windows.Forms.Label()
        Me.lblGruppe = New System.Windows.Forms.Label()
        Me.lblCaption = New System.Windows.Forms.Label()
        Me.rtfNotiz = New System.Windows.Forms.RichTextBox()
        Me.dgvJoin = New System.Windows.Forms.DataGridView()
        Me.imgListLampen = New System.Windows.Forms.ImageList(Me.components)
        Me.pnlWertung = New System.Windows.Forms.Panel()
        Me.xTechniknote = New System.Windows.Forms.TextBox()
        Me.xGültig3 = New System.Windows.Forms.TextBox()
        Me.xGültig2 = New System.Windows.Forms.TextBox()
        Me.xGültig1 = New System.Windows.Forms.TextBox()
        Me.dgv2 = New System.Windows.Forms.DataGridView()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.dgvTeam = New System.Windows.Forms.DataGridView()
        Me.lblName = New System.Windows.Forms.Label()
        Me.dgvHeber = New System.Windows.Forms.DataGridView()
        Me.RSG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.D1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.T1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.D2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.T2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.D3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.T3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Wert = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.xAufruf = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Rolle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Team = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gegner = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mReissen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mStossen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mHeben = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.mPunkte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Punkte2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlWertung.SuspendLayout()
        CType(Me.dgv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTeam, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblInfo
        '
        Me.lblInfo.AutoSize = True
        Me.lblInfo.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.lblInfo.Font = New System.Drawing.Font("Arial", 22.0!)
        Me.lblInfo.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblInfo.Location = New System.Drawing.Point(237, 169)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Padding = New System.Windows.Forms.Padding(12, 8, 10, 8)
        Me.lblInfo.Size = New System.Drawing.Size(342, 51)
        Me.lblInfo.TabIndex = 7
        Me.lblInfo.Text = "neue Hantellast 228 kg"
        Me.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblInfo.Visible = False
        '
        'lblDurchgang
        '
        Me.lblDurchgang.AutoSize = True
        Me.lblDurchgang.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDurchgang.ForeColor = System.Drawing.Color.White
        Me.lblDurchgang.Location = New System.Drawing.Point(94, 6)
        Me.lblDurchgang.Name = "lblDurchgang"
        Me.lblDurchgang.Size = New System.Drawing.Size(34, 20)
        Me.lblDurchgang.TabIndex = 6
        Me.lblDurchgang.Text = "DG"
        Me.lblDurchgang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGruppe
        '
        Me.lblGruppe.AutoSize = True
        Me.lblGruppe.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGruppe.ForeColor = System.Drawing.Color.White
        Me.lblGruppe.Location = New System.Drawing.Point(12, 6)
        Me.lblGruppe.Name = "lblGruppe"
        Me.lblGruppe.Size = New System.Drawing.Size(63, 20)
        Me.lblGruppe.TabIndex = 5
        Me.lblGruppe.Text = "Gruppe"
        Me.lblGruppe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption
        '
        Me.lblCaption.AutoSize = True
        Me.lblCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption.ForeColor = System.Drawing.Color.Gold
        Me.lblCaption.Location = New System.Drawing.Point(366, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(67, 25)
        Me.lblCaption.TabIndex = 2
        Me.lblCaption.Text = "Bohle"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'rtfNotiz
        '
        Me.rtfNotiz.BackColor = System.Drawing.SystemColors.Window
        Me.rtfNotiz.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtfNotiz.DetectUrls = False
        Me.rtfNotiz.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtfNotiz.Location = New System.Drawing.Point(0, 406)
        Me.rtfNotiz.Name = "rtfNotiz"
        Me.rtfNotiz.ReadOnly = True
        Me.rtfNotiz.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.rtfNotiz.ShowSelectionMargin = True
        Me.rtfNotiz.Size = New System.Drawing.Size(210, 47)
        Me.rtfNotiz.TabIndex = 502
        Me.rtfNotiz.Text = "Hier ist ein Text."
        '
        'dgvJoin
        '
        Me.dgvJoin.AllowUserToAddRows = False
        Me.dgvJoin.AllowUserToDeleteRows = False
        Me.dgvJoin.AllowUserToResizeColumns = False
        Me.dgvJoin.AllowUserToResizeRows = False
        Me.dgvJoin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvJoin.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.dgvJoin.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvJoin.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.AppWorkspace
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.Padding = New System.Windows.Forms.Padding(0, 2, 0, 2)
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.dgvJoin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.Padding = New System.Windows.Forms.Padding(0, 4, 0, 5)
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightCyan
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvJoin.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvJoin.EnableHeadersVisualStyles = False
        Me.dgvJoin.Location = New System.Drawing.Point(0, 32)
        Me.dgvJoin.Name = "dgvJoin"
        Me.dgvJoin.ReadOnly = True
        Me.dgvJoin.RowHeadersVisible = False
        Me.dgvJoin.RowHeadersWidth = 30
        Me.dgvJoin.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvJoin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvJoin.Size = New System.Drawing.Size(803, 421)
        Me.dgvJoin.TabIndex = 0
        Me.dgvJoin.Tag = "12"
        '
        'imgListLampen
        '
        Me.imgListLampen.ImageStream = CType(resources.GetObject("imgListLampen.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgListLampen.TransparentColor = System.Drawing.Color.Silver
        Me.imgListLampen.Images.SetKeyName(0, "777.png")
        Me.imgListLampen.Images.SetKeyName(1, "000.png")
        Me.imgListLampen.Images.SetKeyName(2, "005.png")
        Me.imgListLampen.Images.SetKeyName(3, "007.png")
        Me.imgListLampen.Images.SetKeyName(4, "050.png")
        Me.imgListLampen.Images.SetKeyName(5, "070.png")
        Me.imgListLampen.Images.SetKeyName(6, "500.png")
        Me.imgListLampen.Images.SetKeyName(7, "555.png")
        Me.imgListLampen.Images.SetKeyName(8, "557.png")
        Me.imgListLampen.Images.SetKeyName(9, "575.png")
        Me.imgListLampen.Images.SetKeyName(10, "577.png")
        Me.imgListLampen.Images.SetKeyName(11, "700.png")
        Me.imgListLampen.Images.SetKeyName(12, "755.png")
        Me.imgListLampen.Images.SetKeyName(13, "757.png")
        Me.imgListLampen.Images.SetKeyName(14, "775.png")
        Me.imgListLampen.Images.SetKeyName(15, "Empty.png")
        '
        'pnlWertung
        '
        Me.pnlWertung.BackColor = System.Drawing.Color.Black
        Me.pnlWertung.Controls.Add(Me.xTechniknote)
        Me.pnlWertung.Controls.Add(Me.xGültig3)
        Me.pnlWertung.Controls.Add(Me.xGültig2)
        Me.pnlWertung.Controls.Add(Me.xGültig1)
        Me.pnlWertung.Location = New System.Drawing.Point(528, 408)
        Me.pnlWertung.MinimumSize = New System.Drawing.Size(134, 47)
        Me.pnlWertung.Name = "pnlWertung"
        Me.pnlWertung.Size = New System.Drawing.Size(185, 47)
        Me.pnlWertung.TabIndex = 503
        '
        'xTechniknote
        '
        Me.xTechniknote.BackColor = System.Drawing.Color.Black
        Me.xTechniknote.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xTechniknote.Font = New System.Drawing.Font("DS-Digital", 31.0!)
        Me.xTechniknote.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xTechniknote.Location = New System.Drawing.Point(113, 2)
        Me.xTechniknote.Name = "xTechniknote"
        Me.xTechniknote.ReadOnly = True
        Me.xTechniknote.Size = New System.Drawing.Size(72, 42)
        Me.xTechniknote.TabIndex = 22
        Me.xTechniknote.Text = "X,XX"
        Me.xTechniknote.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.xTechniknote.WordWrap = False
        '
        'xGültig3
        '
        Me.xGültig3.BackColor = System.Drawing.Color.Black
        Me.xGültig3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig3.Font = New System.Drawing.Font("Wingdings", 34.0!)
        Me.xGültig3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig3.Location = New System.Drawing.Point(70, -1)
        Me.xGültig3.Name = "xGültig3"
        Me.xGültig3.ReadOnly = True
        Me.xGültig3.Size = New System.Drawing.Size(32, 51)
        Me.xGültig3.TabIndex = 17
        Me.xGültig3.Text = "l"
        Me.xGültig3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xGültig2
        '
        Me.xGültig2.BackColor = System.Drawing.Color.Black
        Me.xGültig2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig2.Font = New System.Drawing.Font("Wingdings", 34.0!)
        Me.xGültig2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig2.Location = New System.Drawing.Point(38, -1)
        Me.xGültig2.Name = "xGültig2"
        Me.xGültig2.ReadOnly = True
        Me.xGültig2.Size = New System.Drawing.Size(32, 51)
        Me.xGültig2.TabIndex = 16
        Me.xGültig2.Text = "l"
        Me.xGültig2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'xGültig1
        '
        Me.xGültig1.BackColor = System.Drawing.Color.Black
        Me.xGültig1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.xGültig1.Font = New System.Drawing.Font("Wingdings", 34.0!)
        Me.xGültig1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.xGültig1.Location = New System.Drawing.Point(6, -1)
        Me.xGültig1.Name = "xGültig1"
        Me.xGültig1.ReadOnly = True
        Me.xGültig1.Size = New System.Drawing.Size(32, 51)
        Me.xGültig1.TabIndex = 15
        Me.xGültig1.Text = "l"
        Me.xGültig1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dgv2
        '
        Me.dgv2.AllowUserToAddRows = False
        Me.dgv2.AllowUserToDeleteRows = False
        Me.dgv2.AllowUserToResizeColumns = False
        Me.dgv2.AllowUserToResizeRows = False
        Me.dgv2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.dgv2.BackgroundColor = System.Drawing.Color.Black
        Me.dgv2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.AppWorkspace
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.Padding = New System.Windows.Forms.Padding(0, 2, 0, 2)
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.dgv2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgv2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightCyan
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv2.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgv2.EnableHeadersVisualStyles = False
        Me.dgv2.Location = New System.Drawing.Point(0, 238)
        Me.dgv2.Name = "dgv2"
        Me.dgv2.ReadOnly = True
        Me.dgv2.RowHeadersVisible = False
        Me.dgv2.RowHeadersWidth = 30
        Me.dgv2.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgv2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgv2.Size = New System.Drawing.Size(804, 53)
        Me.dgv2.TabIndex = 505
        Me.dgv2.Tag = "12"
        Me.dgv2.Visible = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "SquareBlue.jpg")
        Me.ImageList1.Images.SetKeyName(1, "SquareRed.jpg")
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "SquareBlue.jpg")
        Me.ImageList2.Images.SetKeyName(1, "SquareRed.jpg")
        '
        'dgvTeam
        '
        Me.dgvTeam.AllowUserToAddRows = False
        Me.dgvTeam.AllowUserToDeleteRows = False
        Me.dgvTeam.AllowUserToResizeColumns = False
        Me.dgvTeam.AllowUserToResizeRows = False
        Me.dgvTeam.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvTeam.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvTeam.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvTeam.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTeam.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvTeam.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle5.Padding = New System.Windows.Forms.Padding(0, 2, 0, 2)
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTeam.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvTeam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTeam.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Rolle, Me.Team, Me.Gegner, Me.mReissen, Me.mStossen, Me.mHeben, Me.mPunkte, Me.Punkte2})
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.Padding = New System.Windows.Forms.Padding(2, 0, 2, 2)
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTeam.DefaultCellStyle = DataGridViewCellStyle13
        Me.dgvTeam.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvTeam.EnableHeadersVisualStyles = False
        Me.dgvTeam.Location = New System.Drawing.Point(321, 408)
        Me.dgvTeam.Name = "dgvTeam"
        Me.dgvTeam.ReadOnly = True
        Me.dgvTeam.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvTeam.RowHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvTeam.RowHeadersVisible = False
        Me.dgvTeam.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvTeam.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTeam.ShowCellToolTips = False
        Me.dgvTeam.ShowEditingIcon = False
        Me.dgvTeam.ShowRowErrors = False
        Me.dgvTeam.Size = New System.Drawing.Size(207, 42)
        Me.dgvTeam.TabIndex = 510
        Me.dgvTeam.TabStop = False
        Me.dgvTeam.Visible = False
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.Color.LimeGreen
        Me.lblName.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.lblName.ForeColor = System.Drawing.Color.White
        Me.lblName.Location = New System.Drawing.Point(0, 385)
        Me.lblName.Name = "lblName"
        Me.lblName.Padding = New System.Windows.Forms.Padding(6, 2, 0, 0)
        Me.lblName.Size = New System.Drawing.Size(210, 19)
        Me.lblName.TabIndex = 511
        Me.lblName.Text = "Name.Vorname"
        '
        'dgvHeber
        '
        Me.dgvHeber.AllowUserToAddRows = False
        Me.dgvHeber.AllowUserToDeleteRows = False
        Me.dgvHeber.AllowUserToResizeColumns = False
        Me.dgvHeber.AllowUserToResizeRows = False
        Me.dgvHeber.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader
        Me.dgvHeber.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvHeber.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvHeber.ColumnHeadersHeight = 28
        Me.dgvHeber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHeber.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.RSG, Me.D1, Me.T1, Me.D2, Me.T2, Me.D3, Me.T3, Me.Wert})
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvHeber.DefaultCellStyle = DataGridViewCellStyle20
        Me.dgvHeber.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvHeber.EnableHeadersVisualStyles = False
        Me.dgvHeber.Location = New System.Drawing.Point(150, 45)
        Me.dgvHeber.MultiSelect = False
        Me.dgvHeber.Name = "dgvHeber"
        Me.dgvHeber.ReadOnly = True
        Me.dgvHeber.RowHeadersVisible = False
        Me.dgvHeber.RowTemplate.Height = 26
        Me.dgvHeber.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHeber.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvHeber.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHeber.Size = New System.Drawing.Size(519, 110)
        Me.dgvHeber.TabIndex = 512
        Me.dgvHeber.TabStop = False
        Me.dgvHeber.Visible = False
        '
        'RSG
        '
        Me.RSG.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.LemonChiffon
        Me.RSG.DefaultCellStyle = DataGridViewCellStyle16
        Me.RSG.HeaderText = ""
        Me.RSG.Name = "RSG"
        Me.RSG.ReadOnly = True
        Me.RSG.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RSG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.RSG.Width = 80
        '
        'D1
        '
        Me.D1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.D1.HeaderText = "1"
        Me.D1.Name = "D1"
        Me.D1.ReadOnly = True
        Me.D1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.D1.Width = 50
        '
        'T1
        '
        Me.T1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle17.Format = "N2"
        DataGridViewCellStyle17.NullValue = Nothing
        Me.T1.DefaultCellStyle = DataGridViewCellStyle17
        Me.T1.DividerWidth = 1
        Me.T1.HeaderText = ""
        Me.T1.Name = "T1"
        Me.T1.ReadOnly = True
        Me.T1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.T1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.T1.Visible = False
        Me.T1.Width = 55
        '
        'D2
        '
        Me.D2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.D2.HeaderText = "2"
        Me.D2.Name = "D2"
        Me.D2.ReadOnly = True
        Me.D2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.D2.Width = 50
        '
        'T2
        '
        Me.T2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle18.Format = "N2"
        Me.T2.DefaultCellStyle = DataGridViewCellStyle18
        Me.T2.DividerWidth = 1
        Me.T2.HeaderText = ""
        Me.T2.Name = "T2"
        Me.T2.ReadOnly = True
        Me.T2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.T2.Visible = False
        Me.T2.Width = 55
        '
        'D3
        '
        Me.D3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.D3.HeaderText = "3"
        Me.D3.Name = "D3"
        Me.D3.ReadOnly = True
        Me.D3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.D3.Width = 50
        '
        'T3
        '
        Me.T3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle19.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle19.Format = "N2"
        Me.T3.DefaultCellStyle = DataGridViewCellStyle19
        Me.T3.DividerWidth = 1
        Me.T3.HeaderText = ""
        Me.T3.Name = "T3"
        Me.T3.ReadOnly = True
        Me.T3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.T3.Visible = False
        Me.T3.Width = 55
        '
        'Wert
        '
        Me.Wert.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Wert.HeaderText = "Wertung"
        Me.Wert.Name = "Wert"
        Me.Wert.ReadOnly = True
        Me.Wert.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Wert.Width = 120
        '
        'xAufruf
        '
        Me.xAufruf.BackColor = System.Drawing.Color.Black
        Me.xAufruf.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!)
        Me.xAufruf.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.xAufruf.Location = New System.Drawing.Point(664, 408)
        Me.xAufruf.Name = "xAufruf"
        Me.xAufruf.Size = New System.Drawing.Size(142, 46)
        Me.xAufruf.TabIndex = 513
        Me.xAufruf.Text = "0:00"
        Me.xAufruf.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.Label1.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.Label1.Location = New System.Drawing.Point(626, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Padding = New System.Windows.Forms.Padding(6, 4, 5, 4)
        Me.Label1.Size = New System.Drawing.Size(177, 26)
        Me.Label1.TabIndex = 514
        Me.Label1.Text = "neue Hantellast 228 kg"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label1.Visible = False
        '
        'Rolle
        '
        Me.Rolle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Rolle.DataPropertyName = "Rolle"
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Gold
        Me.Rolle.DefaultCellStyle = DataGridViewCellStyle6
        Me.Rolle.HeaderText = ""
        Me.Rolle.Name = "Rolle"
        Me.Rolle.ReadOnly = True
        Me.Rolle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Rolle.Visible = False
        Me.Rolle.Width = 5
        '
        'Team
        '
        Me.Team.DataPropertyName = "Kurzname"
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Gold
        Me.Team.DefaultCellStyle = DataGridViewCellStyle7
        Me.Team.HeaderText = ""
        Me.Team.Name = "Team"
        Me.Team.ReadOnly = True
        Me.Team.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Team.Width = 5
        '
        'Gegner
        '
        Me.Gegner.DataPropertyName = "Gegner"
        Me.Gegner.HeaderText = "Gegner"
        Me.Gegner.Name = "Gegner"
        Me.Gegner.ReadOnly = True
        Me.Gegner.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Gegner.Visible = False
        Me.Gegner.Width = 52
        '
        'mReissen
        '
        Me.mReissen.DataPropertyName = "Reissen"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mReissen.DefaultCellStyle = DataGridViewCellStyle8
        Me.mReissen.HeaderText = "Reißen"
        Me.mReissen.Name = "mReissen"
        Me.mReissen.ReadOnly = True
        Me.mReissen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mReissen.Width = 52
        '
        'mStossen
        '
        Me.mStossen.DataPropertyName = "Stossen"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mStossen.DefaultCellStyle = DataGridViewCellStyle9
        Me.mStossen.HeaderText = "Stoßen"
        Me.mStossen.Name = "mStossen"
        Me.mStossen.ReadOnly = True
        Me.mStossen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mStossen.Width = 51
        '
        'mHeben
        '
        Me.mHeben.DataPropertyName = "Heben"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.mHeben.DefaultCellStyle = DataGridViewCellStyle10
        Me.mHeben.HeaderText = "Gesamt"
        Me.mHeben.Name = "mHeben"
        Me.mHeben.ReadOnly = True
        Me.mHeben.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mHeben.Width = 55
        '
        'mPunkte
        '
        Me.mPunkte.DataPropertyName = "Punkte"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.Format = "N0"
        Me.mPunkte.DefaultCellStyle = DataGridViewCellStyle11
        Me.mPunkte.HeaderText = "Pkt."
        Me.mPunkte.Name = "mPunkte"
        Me.mPunkte.ReadOnly = True
        Me.mPunkte.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.mPunkte.Width = 31
        '
        'Punkte2
        '
        Me.Punkte2.DataPropertyName = "Punkte2"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.Format = "N0"
        Me.Punkte2.DefaultCellStyle = DataGridViewCellStyle12
        Me.Punkte2.HeaderText = "Pkt."
        Me.Punkte2.Name = "Punkte2"
        Me.Punkte2.ReadOnly = True
        Me.Punkte2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Punkte2.Visible = False
        Me.Punkte2.Width = 31
        '
        'frmModerator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(15, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(804, 454)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvHeber)
        Me.Controls.Add(Me.rtfNotiz)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.dgvTeam)
        Me.Controls.Add(Me.dgv2)
        Me.Controls.Add(Me.pnlWertung)
        Me.Controls.Add(Me.lblGruppe)
        Me.Controls.Add(Me.lblDurchgang)
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.lblCaption)
        Me.Controls.Add(Me.xAufruf)
        Me.Controls.Add(Me.dgvJoin)
        Me.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(804, 454)
        Me.Name = "frmModerator"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Moderator"
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlWertung.ResumeLayout(False)
        Me.pnlWertung.PerformLayout()
        CType(Me.dgv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTeam, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvHeber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCaption As Label
    Friend WithEvents lblDurchgang As Label
    Friend WithEvents lblGruppe As Label
    Friend WithEvents rtfNotiz As RichTextBox
    Friend WithEvents dgvJoin As DataGridView
    Friend WithEvents lblInfo As Label
    Friend WithEvents imgListLampen As ImageList
    Friend WithEvents pnlWertung As Panel
    Friend WithEvents xGültig3 As TextBox
    Friend WithEvents xGültig2 As TextBox
    Friend WithEvents dgv2 As DataGridView
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents xTechniknote As TextBox
    Friend WithEvents ImageList2 As ImageList
    Friend WithEvents xGültig1 As TextBox
    Friend WithEvents dgvTeam As DataGridView
    Friend WithEvents lblName As Label
    Friend WithEvents dgvHeber As DataGridView
    Friend WithEvents RSG As DataGridViewTextBoxColumn
    Friend WithEvents D1 As DataGridViewTextBoxColumn
    Friend WithEvents T1 As DataGridViewTextBoxColumn
    Friend WithEvents D2 As DataGridViewTextBoxColumn
    Friend WithEvents T2 As DataGridViewTextBoxColumn
    Friend WithEvents D3 As DataGridViewTextBoxColumn
    Friend WithEvents T3 As DataGridViewTextBoxColumn
    Friend WithEvents Wert As DataGridViewTextBoxColumn
    Friend WithEvents xAufruf As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Rolle As DataGridViewTextBoxColumn
    Friend WithEvents Team As DataGridViewTextBoxColumn
    Friend WithEvents Gegner As DataGridViewTextBoxColumn
    Friend WithEvents mReissen As DataGridViewTextBoxColumn
    Friend WithEvents mStossen As DataGridViewTextBoxColumn
    Friend WithEvents mHeben As DataGridViewTextBoxColumn
    Friend WithEvents mPunkte As DataGridViewTextBoxColumn
    Friend WithEvents Punkte2 As DataGridViewTextBoxColumn
End Class
