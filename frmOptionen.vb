﻿Public Class frmOptionen

    'relevante Forms für "Meldung ausblenden"
    Private sections() As String = {"Gruppen_JG", "Gruppen", "Leitung", "Programm", "Mannschaften", "Meldung", "Teams", "Wettkampf", "Wiegen"}

    Private nonNumberEntered As Boolean = False
    Dim loading As Boolean = True

    Private Sub TextboxNum_KeyDown(sender As Object, e As KeyEventArgs) Handles txtDisplay.KeyDown, txtSize.KeyDown, txtCom.KeyDown,
        txtAbZeichen.KeyDown, txtHupe.KeyDown, txtDelay_Wertung.KeyDown, txtAnzeige_Wertung.KeyDown

        nonNumberEntered = False
        If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
            If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                If e.KeyCode <> Keys.Back Then
                    nonNumberEntered = True
                End If
            End If
        End If
        If ModifierKeys = Keys.Shift Then
            nonNumberEntered = True
        End If
    End Sub
    Private Sub TextboxNumDec_KeyDown(sender As Object, e As KeyEventArgs) Handles txtLimitTechnik.KeyDown

        nonNumberEntered = False
        If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
            If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                If e.KeyCode <> Keys.Oemcomma And e.KeyCode <> Keys.Decimal Then
                    If e.KeyCode <> Keys.Back Then
                        nonNumberEntered = True
                    End If
                End If
            End If
        End If
        If ModifierKeys = Keys.Shift Then
            nonNumberEntered = True
        End If
    End Sub
    Private Sub TextboxNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDisplay.KeyPress, txtSize.KeyPress, txtCom.KeyPress,
        txtAbZeichen.KeyPress, txtHupe.KeyPress, txtDelay_Wertung.KeyPress, txtAnzeige_Wertung.KeyPress, txtLimitTechnik.KeyPress

        If nonNumberEntered = True Then
            e.Handled = True
        End If
    End Sub

    Private Sub frmOptionen_Closed(sender As Object, e As EventArgs) Handles Me.Closed
    End Sub
    Private Sub frmOptionen_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing AndAlso btnExit.Enabled = True Then
            ' ESC, Abbrechen oder Schließen gedrückt
            If btnApply.Enabled Then
                Using New Centered_MessageBox(Me)
                    Select Case MessageBox.Show("Es wurden Änderungen vorgenommen." & vbNewLine & "Sollen die geänderten Werte gespeichert werden?", msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                        Case DialogResult.Yes
                            btnApply.PerformClick()
                        Case DialogResult.Cancel
                            e.Cancel = True
                    End Select
                End Using
            End If
        End If
    End Sub
    Private Sub frmOptionen_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then Close()
    End Sub
    Private Sub frmOptionen_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        cboDriver.Items.AddRange(User.Get_Driver())

        Dim x As String
        If File.Exists(App_IniFile) Then
            For Each t In Controls
                If TypeOf t Is TabControl Then
                    For Each tp As TabPage In CType(t, TabControl).Controls
                        For Each ctl As Control In tp.Controls
                            If Not IsNothing(ctl.Tag) Then
                                If Not String.IsNullOrEmpty(ctl.Tag.ToString) Then
                                    Dim keys() As String = Split(CStr(ctl.Tag), ":")
                                    If keys(0) = "Programm" And keys(1) = "ShowMessages" Then
                                        'Dim sections As ArrayList = NativeMethods.INI_GetSectionNames(App_IniFile)
                                        Dim value As String = String.Empty
                                        Dim i As Integer
                                        For i = 0 To sections.Count - 1
                                            x = NativeMethods.INI_Read(sections(i).ToString, keys(1), App_IniFile)
                                            If x <> "?" Then
                                                If String.IsNullOrEmpty(value) Then value = x
                                                If x <> value Then Exit For
                                            End If
                                        Next
                                        If value = String.Empty Then
                                            chkMessages.Checked = True
                                        ElseIf i < sections.Count Then
                                            chkMessages.CheckState = CheckState.Indeterminate
                                        Else
                                            chkMessages.Checked = CBool(value)
                                        End If
                                    ElseIf keys(0) = "Programm" And keys(1) = "Konflikte" Then
                                        Dim value As String = String.Empty
                                        Dim i As Integer
                                        For i = 0 To sections.Count - 1
                                            x = NativeMethods.INI_Read(sections(i).ToString, keys(1), App_IniFile)
                                            If x <> "?" Then
                                                If x = String.Empty Then value = x
                                                If x <> value Then Exit For
                                            End If
                                        Next
                                        If value = String.Empty Then
                                            chkKonflikt.Checked = True
                                        ElseIf i < sections.Count Then
                                            chkKonflikt.CheckState = CheckState.Indeterminate
                                        Else
                                            chkKonflikt.Checked = CBool(value)
                                        End If
                                    ElseIf keys(0) = "FastReport" And keys(1) = "MySQLDriver" Then
                                        x = NativeMethods.INI_Read(keys(0), keys(1), App_IniFile)
                                        If x <> "?" Then
                                            cboDriver.SelectedIndex = cboDriver.Items.IndexOf(x)
                                        ElseIf Not String.IsNullOrEmpty(User.MySQL_Driver) AndAlso cboDriver.Items.Contains(User.MySQL_Driver) Then
                                            cboDriver.SelectedIndex = cboDriver.Items.IndexOf(User.MySQL_Driver)
                                        ElseIf cboDriver.Items.Contains(FR_Driver) Then
                                            cboDriver.SelectedIndex = cboDriver.Items.IndexOf(FR_Driver)
                                        End If
                                    Else
                                        x = NativeMethods.INI_Read(keys(0), keys(1), App_IniFile)
                                        If TypeOf (ctl) Is TextBox Then
                                            If x <> "?" Then ctl.Text = x
                                        ElseIf TypeOf (ctl) Is CheckBox Then
                                            If x <> "?" Then CType(ctl, CheckBox).Checked = CBool(x)
                                        ElseIf TypeOf (ctl) Is ComboBox Then
                                            If x <> "?" Then CType(ctl, ComboBox).SelectedIndex = CInt(x)
                                            If CType(ctl, ComboBox).SelectedIndex = -1 Then CType(ctl, ComboBox).SelectedIndex = 0
                                        ElseIf TypeOf (ctl) Is Label Then
                                            If x <> "?" Then ctl.ForeColor = Color.FromArgb(CInt(x))
                                        ElseIf TypeOf (ctl) Is NumericUpDown Then
                                            If x <> "?" Then CType(ctl, NumericUpDown).Value = CInt(x)
                                        End If
                                    End If
                                End If
                            End If
                        Next
                    Next
                End If
            Next
        End If
        loading = False
        Cursor = Cursors.Default
    End Sub



    Private Sub btnApply_Click(sender As Object, e As EventArgs) Handles btnApply.Click
        Cursor = Cursors.WaitCursor

        For Each t In Controls
            If TypeOf t Is TabControl Then
                For Each tp As TabPage In CType(t, TabControl).Controls
                    For Each ctl As Control In tp.Controls
                        If Not IsNothing(ctl.Tag) Then
                            If Not String.IsNullOrEmpty(ctl.Tag.ToString) Then
                                Dim keys() As String = Split(CStr(ctl.Tag), ":")
                                If keys(0) = "Programm" And keys(1) = "ShowMessages" Then
                                    For i = 0 To sections.Count - 1
                                        NativeMethods.INI_Write(sections(i).ToString, keys(1), chkMessages.Checked.ToString, App_IniFile)
                                    Next
                                ElseIf keys(0) = "Programm" And keys(1) = "Konflikte" Then
                                    For i = 0 To sections.Count - 1
                                        NativeMethods.INI_Write(sections(i).ToString, keys(1), chkKonflikt.Checked.ToString, App_IniFile)
                                    Next
                                ElseIf keys(0) = "FastReport" And keys(1) = "MySQLDriver" Then
                                    NativeMethods.INI_Write(keys(0), keys(1), ctl.Text, App_IniFile)
                                Else
                                    If TypeOf (ctl) Is TextBox Then
                                        If ctl.Text = "" Or Not IsNumeric(ctl.Text) Then ctl.Text = "0"
                                        NativeMethods.INI_Write(keys(0), keys(1), ctl.Text, App_IniFile)
                                    ElseIf TypeOf (ctl) Is CheckBox Then
                                        NativeMethods.INI_Write(keys(0), keys(1), CStr(CType(ctl, CheckBox).Checked), App_IniFile)
                                    ElseIf TypeOf (ctl) Is ComboBox Then
                                        NativeMethods.INI_Write(keys(0), keys(1), CStr(CType(ctl, ComboBox).SelectedIndex), App_IniFile)
                                    ElseIf TypeOf (ctl) Is Label Then
                                        NativeMethods.INI_Write(keys(0), keys(1), ctl.ForeColor.ToArgb.ToString, App_IniFile)
                                    ElseIf TypeOf (ctl) Is NumericUpDown Then
                                        NativeMethods.INI_Write(keys(0), keys(1), ctl.Text, App_IniFile)
                                    End If
                                End If
                            End If
                        End If
                    Next
                Next
            End If
        Next

        Get_Optionen(True) 'geänderte Werte ins laufende Programm übernehmen

        btnApply.Enabled = False
        Cursor = Cursors.Default
    End Sub
    Private Sub btnDefault_Click(sender As Object, e As EventArgs) Handles btnDefault.Click
        Cursor = Cursors.WaitCursor

        txtCom.Text = "1000"
        chkSavePosition.Checked = True
        cboName.SelectedIndex = 0
        txtAbZeichen.Text = "7"
        txtHupe.Text = "3"
        txtDelay_Wertung.Text = "3"
        txtAnzeige_Wertung.Text = "5"
        txtLimitTechnik.Text = "1,5"

        txtDisplay.Text = "5"
        txtSize.Text = "50"
        chkPrimary.Checked = False
        chkUpdate.Checked = True
        chkMessages.Checked = True
        chkKonflikt.Checked = True
        btnApply.Enabled = True
        cboMarkRows.SelectedIndex = 0
        cboDecimals.SelectedIndex = 1
        chkMessageBohle.Checked = True
        chkOverwrite.Checked = False
        nudTeilerMasters.Value = 18
        nudTeilerZweikampf.Value = 18
        nudMaxGruppen.Value = 5
        cboDriver.SelectedIndex = cboDriver.FindString("MySQL ODBC 8.0 Unicode Driver")

        Cursor = Cursors.Default
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        btnExit.Enabled = False
        Close()
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If btnApply.Enabled Then btnApply.PerformClick()
        Close()
    End Sub

    Private Sub Changed(sender As Object, e As EventArgs) Handles chkUpdate.CheckedChanged, chkPrimary.CheckedChanged, chkSavePosition.CheckedChanged,
        chkMessages.CheckStateChanged, chkKonflikt.CheckStateChanged, chkMessageBohle.CheckedChanged,
        txtSize.TextChanged, txtDisplay.TextChanged, txtCom.TextChanged, txtAbZeichen.TextChanged, txtHupe.TextChanged, txtDelay_Wertung.TextChanged,
        txtAnzeige_Wertung.TextChanged, txtLimitTechnik.TextChanged, cboDecimals.SelectedIndexChanged, cboMarkRows.SelectedIndexChanged, chkOverwrite.CheckedChanged,
        nudMaxGruppen.TextChanged, nudTeilerMasters.TextChanged, nudTeilerZweikampf.TextChanged, cboDriver.SelectedValueChanged

        If Not loading Then btnApply.Enabled = True
    End Sub

    Private Sub cboName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboName.SelectedIndexChanged
        If cboName.Focused Then btnApply.Enabled = True
    End Sub

    Private Sub txtSize_TextChanged(sender As Object, e As EventArgs) Handles txtSize.TextChanged
        If CInt(txtSize.Text) > 100 Then
            If MsgBox("Die Anzeige reicht über das Anzeigegerät hinaus." & Chr(13) & "Auf maximale Größe setzen?", CType(MsgBoxStyle.Information + MsgBoxStyle.YesNo, MsgBoxStyle)) = MsgBoxResult.Yes Then
                txtSize.Text = "100"
            End If
        End If
    End Sub

    Private Sub btnFontSizeLarger_Click(sender As Object, e As EventArgs) Handles btnFontSizeLarger.Click
        Ansicht_Options.Change_FontSize(1)
    End Sub

    Private Sub btnFontSizeSmaller_Click(sender As Object, e As EventArgs) Handles btnFontSizeSmaller.Click
        Ansicht_Options.Change_FontSize(-1)
    End Sub

End Class