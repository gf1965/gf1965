﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class gvWettkampf
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cmdExit = New System.Windows.Forms.Button()
        Me.btnÜbernehmen = New System.Windows.Forms.Button()
        Me.dgvWK = New System.Windows.Forms.DataGridView()
        Me.WK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Datum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ort = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Verein = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Modus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLayout = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMauszeiger = New System.Windows.Forms.ToolStripMenuItem()
        Me.bs = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnBundesliga_Import = New System.Windows.Forms.Button()
        Me.mnuÜbernehmen = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.dgvWK, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdExit
        '
        Me.cmdExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdExit.Location = New System.Drawing.Point(771, 449)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.Size = New System.Drawing.Size(89, 27)
        Me.cmdExit.TabIndex = 11
        Me.cmdExit.TabStop = False
        Me.cmdExit.Text = "Abbrechen"
        Me.cmdExit.UseVisualStyleBackColor = True
        '
        'btnÜbernehmen
        '
        Me.btnÜbernehmen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnÜbernehmen.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnÜbernehmen.Location = New System.Drawing.Point(676, 449)
        Me.btnÜbernehmen.Name = "btnÜbernehmen"
        Me.btnÜbernehmen.Size = New System.Drawing.Size(89, 27)
        Me.btnÜbernehmen.TabIndex = 10
        Me.btnÜbernehmen.TabStop = False
        Me.btnÜbernehmen.Text = "Übernehmen"
        Me.btnÜbernehmen.UseVisualStyleBackColor = True
        '
        'dgvWK
        '
        Me.dgvWK.AllowUserToAddRows = False
        Me.dgvWK.AllowUserToDeleteRows = False
        Me.dgvWK.AllowUserToOrderColumns = True
        Me.dgvWK.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dgvWK.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvWK.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvWK.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvWK.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ActiveCaption
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvWK.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvWK.ColumnHeadersHeight = 21
        Me.dgvWK.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvWK.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WK, Me.Datum, Me.Bezeichnung, Me.Ort, Me.Verein, Me.Modus})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvWK.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvWK.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvWK.EnableHeadersVisualStyles = False
        Me.dgvWK.Location = New System.Drawing.Point(12, 40)
        Me.dgvWK.Name = "dgvWK"
        Me.dgvWK.ReadOnly = True
        Me.dgvWK.RowHeadersWidth = 24
        Me.dgvWK.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White
        Me.dgvWK.RowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvWK.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvWK.Size = New System.Drawing.Size(848, 397)
        Me.dgvWK.StandardTab = True
        Me.dgvWK.TabIndex = 0
        '
        'WK
        '
        Me.WK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.WK.DataPropertyName = "Wettkampf"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.WK.DefaultCellStyle = DataGridViewCellStyle3
        Me.WK.HeaderText = "WK"
        Me.WK.Name = "WK"
        Me.WK.ReadOnly = True
        Me.WK.Width = 35
        '
        'Datum
        '
        Me.Datum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Datum.DataPropertyName = "Datum"
        Me.Datum.HeaderText = "Datum"
        Me.Datum.Name = "Datum"
        Me.Datum.ReadOnly = True
        Me.Datum.Width = 75
        '
        'Bezeichnung
        '
        Me.Bezeichnung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Bezeichnung.DataPropertyName = "Bez1"
        Me.Bezeichnung.HeaderText = "Bezeichnung"
        Me.Bezeichnung.Name = "Bezeichnung"
        Me.Bezeichnung.ReadOnly = True
        '
        'Ort
        '
        Me.Ort.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Ort.DataPropertyName = "Ort"
        Me.Ort.HeaderText = "Ort"
        Me.Ort.Name = "Ort"
        Me.Ort.ReadOnly = True
        Me.Ort.Width = 160
        '
        'Verein
        '
        Me.Verein.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Verein.DataPropertyName = "Vereinsname"
        Me.Verein.HeaderText = "Verein"
        Me.Verein.Name = "Verein"
        Me.Verein.ReadOnly = True
        Me.Verein.Width = 130
        '
        'Modus
        '
        Me.Modus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Modus.DataPropertyName = "Bezeichnung"
        Me.Modus.HeaderText = "Modus"
        Me.Modus.Name = "Modus"
        Me.Modus.ReadOnly = True
        Me.Modus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Modus.Width = 145
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuOptionen, Me.mnuExtras})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(5, 1, 0, 1)
        Me.MenuStrip1.Size = New System.Drawing.Size(872, 24)
        Me.MenuStrip1.TabIndex = 4
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuÜbernehmen, Me.ToolStripMenuItem1, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 22)
        Me.mnuDatei.Text = "&Datei"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(177, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(180, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLayout})
        Me.mnuOptionen.Name = "mnuOptionen"
        Me.mnuOptionen.Size = New System.Drawing.Size(69, 22)
        Me.mnuOptionen.Text = "&Optionen"
        Me.mnuOptionen.Visible = False
        '
        'mnuLayout
        '
        Me.mnuLayout.Name = "mnuLayout"
        Me.mnuLayout.Size = New System.Drawing.Size(180, 22)
        Me.mnuLayout.Text = "&Tabellen-Layout"
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMauszeiger})
        Me.mnuExtras.Name = "mnuExtras"
        Me.mnuExtras.Size = New System.Drawing.Size(50, 22)
        Me.mnuExtras.Text = "&Extras"
        '
        'mnuMauszeiger
        '
        Me.mnuMauszeiger.Name = "mnuMauszeiger"
        Me.mnuMauszeiger.ShortcutKeys = System.Windows.Forms.Keys.F4
        Me.mnuMauszeiger.Size = New System.Drawing.Size(186, 22)
        Me.mnuMauszeiger.Text = "Mauszeiger holen"
        '
        'bs
        '
        Me.bs.AllowNew = True
        '
        'btnNew
        '
        Me.btnNew.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Blue
        Me.btnNew.Location = New System.Drawing.Point(12, 449)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(138, 27)
        Me.btnNew.TabIndex = 12
        Me.btnNew.TabStop = False
        Me.btnNew.Text = "neuer Einzel-Wettkampf"
        Me.btnNew.UseVisualStyleBackColor = True
        Me.btnNew.Visible = False
        '
        'btnBundesliga_Import
        '
        Me.btnBundesliga_Import.ForeColor = System.Drawing.Color.Firebrick
        Me.btnBundesliga_Import.Location = New System.Drawing.Point(156, 449)
        Me.btnBundesliga_Import.Name = "btnBundesliga_Import"
        Me.btnBundesliga_Import.Size = New System.Drawing.Size(164, 27)
        Me.btnBundesliga_Import.TabIndex = 13
        Me.btnBundesliga_Import.TabStop = False
        Me.btnBundesliga_Import.Text = "neuer Bundesliga-Wettkampf"
        Me.btnBundesliga_Import.UseVisualStyleBackColor = True
        Me.btnBundesliga_Import.Visible = False
        '
        'mnuÜbernehmen
        '
        Me.mnuÜbernehmen.Name = "mnuÜbernehmen"
        Me.mnuÜbernehmen.Size = New System.Drawing.Size(180, 22)
        Me.mnuÜbernehmen.Text = "&Übernehmen"
        '
        'gvWettkampf
        '
        Me.AcceptButton = Me.btnÜbernehmen
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.cmdExit
        Me.ClientSize = New System.Drawing.Size(872, 486)
        Me.Controls.Add(Me.btnBundesliga_Import)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.dgvWK)
        Me.Controls.Add(Me.btnÜbernehmen)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "gvWettkampf"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Wettkampf auswählen"
        CType(Me.dgvWK, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdExit As System.Windows.Forms.Button
    Friend WithEvents btnÜbernehmen As System.Windows.Forms.Button
    Friend WithEvents dgvWK As System.Windows.Forms.DataGridView
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDatei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBeenden As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOptionen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLayout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bs As System.Windows.Forms.BindingSource
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents btnNew As Button
    Friend WithEvents mnuExtras As ToolStripMenuItem
    Friend WithEvents mnuMauszeiger As ToolStripMenuItem
    Friend WithEvents WK As DataGridViewTextBoxColumn
    Friend WithEvents Datum As DataGridViewTextBoxColumn
    Friend WithEvents Bezeichnung As DataGridViewTextBoxColumn
    Friend WithEvents Ort As DataGridViewTextBoxColumn
    Friend WithEvents Verein As DataGridViewTextBoxColumn
    Friend WithEvents Modus As DataGridViewTextBoxColumn
    Friend WithEvents btnBundesliga_Import As Button
    Friend WithEvents mnuÜbernehmen As ToolStripMenuItem
End Class
