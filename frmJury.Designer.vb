﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJury
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuServerStarten = New System.Windows.Forms.ToolStripMenuItem()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Technik1 = New System.Windows.Forms.TextBox()
        Me.LampeReferee1 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Technik2 = New System.Windows.Forms.TextBox()
        Me.Technik3 = New System.Windows.Forms.TextBox()
        Me.LampeJury1 = New System.Windows.Forms.TextBox()
        Me.LampeReferee2 = New System.Windows.Forms.TextBox()
        Me.LampeJury2 = New System.Windows.Forms.TextBox()
        Me.LampeJury3 = New System.Windows.Forms.TextBox()
        Me.LampeJury4 = New System.Windows.Forms.TextBox()
        Me.LampeJury5 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LampeReferee3 = New System.Windows.Forms.TextBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1259, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 20)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(150, 26)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuServerStarten})
        Me.mnuOptionen.Name = "mnuOptionen"
        Me.mnuOptionen.Size = New System.Drawing.Size(69, 20)
        Me.mnuOptionen.Text = "&Optionen"
        '
        'mnuServerStarten
        '
        Me.mnuServerStarten.Name = "mnuServerStarten"
        Me.mnuServerStarten.Size = New System.Drawing.Size(182, 26)
        Me.mnuServerStarten.Text = "&Server starten"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 20
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Technik1, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.LampeReferee1, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 11, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Technik2, 6, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Technik3, 11, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.LampeJury1, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.LampeReferee2, 6, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.LampeJury2, 4, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.LampeJury3, 8, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.LampeJury4, 12, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.LampeJury5, 16, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 6, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.LampeReferee3, 11, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(16, 50)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.366071!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.03571!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.51786!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.580357!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.27679!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1227, 551)
        Me.TableLayoutPanel1.TabIndex = 26
        '
        'Technik1
        '
        Me.Technik1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Technik1.BackColor = System.Drawing.Color.Black
        Me.Technik1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TableLayoutPanel1.SetColumnSpan(Me.Technik1, 4)
        Me.Technik1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Technik1.Font = New System.Drawing.Font("DS-Digital", 50.0!)
        Me.Technik1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Technik1.Location = New System.Drawing.Point(65, 226)
        Me.Technik1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Technik1.Name = "Technik1"
        Me.Technik1.ReadOnly = True
        Me.Technik1.Size = New System.Drawing.Size(236, 91)
        Me.Technik1.TabIndex = 24
        Me.Technik1.TabStop = False
        Me.Technik1.Text = "X,XX"
        Me.Technik1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Technik1.WordWrap = False
        '
        'LampeReferee1
        '
        Me.LampeReferee1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LampeReferee1.BackColor = System.Drawing.Color.Black
        Me.LampeReferee1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TableLayoutPanel1.SetColumnSpan(Me.LampeReferee1, 4)
        Me.LampeReferee1.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.LampeReferee1.Font = New System.Drawing.Font("Wingdings", 120.0!)
        Me.LampeReferee1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.LampeReferee1.Location = New System.Drawing.Point(65, 44)
        Me.LampeReferee1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LampeReferee1.Name = "LampeReferee1"
        Me.LampeReferee1.ReadOnly = True
        Me.LampeReferee1.Size = New System.Drawing.Size(236, 229)
        Me.LampeReferee1.TabIndex = 39
        Me.LampeReferee1.TabStop = False
        Me.LampeReferee1.Tag = "32"
        Me.LampeReferee1.Text = "l"
        Me.LampeReferee1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.TableLayoutPanel1.SetColumnSpan(Me.Label3, 4)
        Me.Label3.Font = New System.Drawing.Font("Arial", 18.0!)
        Me.Label3.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label3.Location = New System.Drawing.Point(675, 0)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(236, 40)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "Referee 3"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Technik2
        '
        Me.Technik2.BackColor = System.Drawing.Color.Black
        Me.Technik2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TableLayoutPanel1.SetColumnSpan(Me.Technik2, 4)
        Me.Technik2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Technik2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Technik2.Font = New System.Drawing.Font("DS-Digital", 50.0!)
        Me.Technik2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Technik2.Location = New System.Drawing.Point(370, 226)
        Me.Technik2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Technik2.Name = "Technik2"
        Me.Technik2.ReadOnly = True
        Me.Technik2.Size = New System.Drawing.Size(236, 91)
        Me.Technik2.TabIndex = 22
        Me.Technik2.TabStop = False
        Me.Technik2.Text = "X,XX"
        Me.Technik2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Technik2.WordWrap = False
        '
        'Technik3
        '
        Me.Technik3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Technik3.BackColor = System.Drawing.Color.Black
        Me.Technik3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TableLayoutPanel1.SetColumnSpan(Me.Technik3, 4)
        Me.Technik3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Technik3.Font = New System.Drawing.Font("DS-Digital", 50.0!)
        Me.Technik3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.Technik3.Location = New System.Drawing.Point(675, 226)
        Me.Technik3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Technik3.Name = "Technik3"
        Me.Technik3.ReadOnly = True
        Me.Technik3.Size = New System.Drawing.Size(236, 91)
        Me.Technik3.TabIndex = 23
        Me.Technik3.TabStop = False
        Me.Technik3.Text = "X,XX"
        Me.Technik3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Technik3.WordWrap = False
        '
        'LampeJury1
        '
        Me.LampeJury1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LampeJury1.BackColor = System.Drawing.Color.Black
        Me.LampeJury1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TableLayoutPanel1.SetColumnSpan(Me.LampeJury1, 4)
        Me.LampeJury1.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.LampeJury1.Font = New System.Drawing.Font("Wingdings", 120.0!)
        Me.LampeJury1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.LampeJury1.Location = New System.Drawing.Point(4, 347)
        Me.LampeJury1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LampeJury1.Name = "LampeJury1"
        Me.LampeJury1.ReadOnly = True
        Me.LampeJury1.Size = New System.Drawing.Size(236, 222)
        Me.LampeJury1.TabIndex = 35
        Me.LampeJury1.TabStop = False
        Me.LampeJury1.Tag = "8"
        Me.LampeJury1.Text = "l"
        Me.LampeJury1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LampeReferee2
        '
        Me.LampeReferee2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LampeReferee2.BackColor = System.Drawing.Color.Black
        Me.LampeReferee2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TableLayoutPanel1.SetColumnSpan(Me.LampeReferee2, 4)
        Me.LampeReferee2.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.LampeReferee2.Font = New System.Drawing.Font("Wingdings", 120.0!)
        Me.LampeReferee2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.LampeReferee2.Location = New System.Drawing.Point(370, 44)
        Me.LampeReferee2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LampeReferee2.Name = "LampeReferee2"
        Me.LampeReferee2.ReadOnly = True
        Me.LampeReferee2.Size = New System.Drawing.Size(236, 229)
        Me.LampeReferee2.TabIndex = 21
        Me.LampeReferee2.TabStop = False
        Me.LampeReferee2.Tag = "32"
        Me.LampeReferee2.Text = "l"
        Me.LampeReferee2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LampeJury2
        '
        Me.LampeJury2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LampeJury2.BackColor = System.Drawing.Color.Black
        Me.LampeJury2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TableLayoutPanel1.SetColumnSpan(Me.LampeJury2, 4)
        Me.LampeJury2.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.LampeJury2.Font = New System.Drawing.Font("Wingdings", 120.0!)
        Me.LampeJury2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.LampeJury2.Location = New System.Drawing.Point(248, 347)
        Me.LampeJury2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LampeJury2.Name = "LampeJury2"
        Me.LampeJury2.ReadOnly = True
        Me.LampeJury2.Size = New System.Drawing.Size(236, 222)
        Me.LampeJury2.TabIndex = 29
        Me.LampeJury2.TabStop = False
        Me.LampeJury2.Tag = "8"
        Me.LampeJury2.Text = "l"
        Me.LampeJury2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LampeJury3
        '
        Me.LampeJury3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LampeJury3.BackColor = System.Drawing.Color.Black
        Me.LampeJury3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TableLayoutPanel1.SetColumnSpan(Me.LampeJury3, 4)
        Me.LampeJury3.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.LampeJury3.Font = New System.Drawing.Font("Wingdings", 120.0!)
        Me.LampeJury3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.LampeJury3.Location = New System.Drawing.Point(492, 347)
        Me.LampeJury3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LampeJury3.Name = "LampeJury3"
        Me.LampeJury3.ReadOnly = True
        Me.LampeJury3.Size = New System.Drawing.Size(236, 222)
        Me.LampeJury3.TabIndex = 25
        Me.LampeJury3.TabStop = False
        Me.LampeJury3.Tag = "8"
        Me.LampeJury3.Text = "l"
        Me.LampeJury3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LampeJury4
        '
        Me.LampeJury4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LampeJury4.BackColor = System.Drawing.Color.Black
        Me.LampeJury4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TableLayoutPanel1.SetColumnSpan(Me.LampeJury4, 4)
        Me.LampeJury4.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.LampeJury4.Font = New System.Drawing.Font("Wingdings", 120.0!)
        Me.LampeJury4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.LampeJury4.Location = New System.Drawing.Point(736, 347)
        Me.LampeJury4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LampeJury4.Name = "LampeJury4"
        Me.LampeJury4.ReadOnly = True
        Me.LampeJury4.Size = New System.Drawing.Size(236, 222)
        Me.LampeJury4.TabIndex = 28
        Me.LampeJury4.TabStop = False
        Me.LampeJury4.Tag = "8"
        Me.LampeJury4.Text = "l"
        Me.LampeJury4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LampeJury5
        '
        Me.LampeJury5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LampeJury5.BackColor = System.Drawing.Color.Black
        Me.LampeJury5.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TableLayoutPanel1.SetColumnSpan(Me.LampeJury5, 4)
        Me.LampeJury5.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.LampeJury5.Font = New System.Drawing.Font("Wingdings", 120.0!)
        Me.LampeJury5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.LampeJury5.Location = New System.Drawing.Point(980, 347)
        Me.LampeJury5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LampeJury5.Name = "LampeJury5"
        Me.LampeJury5.ReadOnly = True
        Me.LampeJury5.Size = New System.Drawing.Size(243, 222)
        Me.LampeJury5.TabIndex = 30
        Me.LampeJury5.TabStop = False
        Me.LampeJury5.Tag = "8"
        Me.LampeJury5.Text = "l"
        Me.LampeJury5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.TableLayoutPanel1.SetColumnSpan(Me.Label1, 4)
        Me.Label1.Font = New System.Drawing.Font("Arial", 18.0!)
        Me.Label1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Location = New System.Drawing.Point(65, 0)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(236, 40)
        Me.Label1.TabIndex = 36
        Me.Label1.Text = "Referee 1"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.TableLayoutPanel1.SetColumnSpan(Me.Label2, 4)
        Me.Label2.Font = New System.Drawing.Font("Arial", 18.0!)
        Me.Label2.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Location = New System.Drawing.Point(370, 0)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(236, 40)
        Me.Label2.TabIndex = 37
        Me.Label2.Text = "Referee 2"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LampeReferee3
        '
        Me.LampeReferee3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LampeReferee3.BackColor = System.Drawing.Color.Black
        Me.LampeReferee3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TableLayoutPanel1.SetColumnSpan(Me.LampeReferee3, 4)
        Me.LampeReferee3.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.LampeReferee3.Font = New System.Drawing.Font("Wingdings", 120.0!)
        Me.LampeReferee3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.LampeReferee3.Location = New System.Drawing.Point(675, 44)
        Me.LampeReferee3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LampeReferee3.Name = "LampeReferee3"
        Me.LampeReferee3.ReadOnly = True
        Me.LampeReferee3.Size = New System.Drawing.Size(236, 229)
        Me.LampeReferee3.TabIndex = 19
        Me.LampeReferee3.TabStop = False
        Me.LampeReferee3.Tag = "2"
        Me.LampeReferee3.Text = "l"
        Me.LampeReferee3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Arial", 14.0!)
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 26
        Me.ListBox1.Items.AddRange(New Object() {"Ziehen aus dem Hang", "Abheben der rollenden Hantel", "Berühren der Plattform", "Pause während des Ausstoßens", "Nachdrücken beim Beenden", "Beugen & Strecken der Ellbogen beim Aufstehen", "Verlassen der Plattform während des Versuchs", "Fallenlassen der Hantel ", "Absetzen der Hantel außerhalb der Plattform", "Blickrichtung bei Beginn nicht zum Hauptkampfrichter", "Absetzen der Hantel vor dem Ab-Zeichen", "Unvollständiges/ungleiches Strecken der Arme", "Füße & Hantel nicht in einer Linie/parallel zur Körperachse", "Knie beim Beenden nicht durchgedrückt", "Ablegen der Hantel vor der Endposition", "Berühren von Oberschenkel/Knie mit Ellbogen/Oberarm", "Unvollständiger Ausstoß-Versuch", "Absichtliches Schwingen der Hantel"})
        Me.ListBox1.Location = New System.Drawing.Point(0, 33)
        Me.ListBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(685, 472)
        Me.ListBox1.TabIndex = 27
        '
        'frmJury
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1259, 617)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.DoubleBuffered = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1274, 654)
        Me.Name = "frmJury"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Jury"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents mnuDatei As ToolStripMenuItem
    Friend WithEvents mnuBeenden As ToolStripMenuItem
    Friend WithEvents mnuOptionen As ToolStripMenuItem
    Friend WithEvents mnuServerStarten As ToolStripMenuItem
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents LampeReferee2 As TextBox
    Friend WithEvents LampeReferee3 As TextBox
    Friend WithEvents LampeJury2 As TextBox
    Friend WithEvents LampeJury4 As TextBox
    Friend WithEvents LampeJury3 As TextBox
    Friend WithEvents Technik1 As TextBox
    Friend WithEvents Technik3 As TextBox
    Friend WithEvents Technik2 As TextBox
    Friend WithEvents LampeJury5 As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents LampeJury1 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents LampeReferee1 As TextBox
    Friend WithEvents ListBox1 As ListBox
End Class
