﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPause
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.pnlForm = New System.Windows.Forms.Panel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabIntervall = New System.Windows.Forms.TabPage()
        Me.optCustom = New System.Windows.Forms.RadioButton()
        Me.pnlCustom = New System.Windows.Forms.Panel()
        Me.nudMinuten = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nudStunden = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.opt120 = New System.Windows.Forms.RadioButton()
        Me.opt60 = New System.Windows.Forms.RadioButton()
        Me.opt5 = New System.Windows.Forms.RadioButton()
        Me.opt10 = New System.Windows.Forms.RadioButton()
        Me.opt30 = New System.Windows.Forms.RadioButton()
        Me.opt15 = New System.Windows.Forms.RadioButton()
        Me.tabUhrzeit = New System.Windows.Forms.TabPage()
        Me.dtpZeit = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnEscape = New System.Windows.Forms.Button()
        Me.lblTitleBar = New System.Windows.Forms.Label()
        Me.chkHideAds = New System.Windows.Forms.CheckBox()
        Me.pnlForm.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tabIntervall.SuspendLayout()
        Me.pnlCustom.SuspendLayout()
        CType(Me.nudMinuten, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudStunden, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabUhrzeit.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnStart
        '
        Me.btnStart.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnStart.Location = New System.Drawing.Point(18, 311)
        Me.btnStart.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(225, 57)
        Me.btnStart.TabIndex = 5
        Me.btnStart.Text = "Starten"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'pnlForm
        '
        Me.pnlForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlForm.Controls.Add(Me.chkHideAds)
        Me.pnlForm.Controls.Add(Me.TabControl1)
        Me.pnlForm.Controls.Add(Me.btnEscape)
        Me.pnlForm.Controls.Add(Me.lblTitleBar)
        Me.pnlForm.Controls.Add(Me.btnStart)
        Me.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlForm.Location = New System.Drawing.Point(0, 0)
        Me.pnlForm.Name = "pnlForm"
        Me.pnlForm.Size = New System.Drawing.Size(265, 388)
        Me.pnlForm.TabIndex = 7
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabIntervall)
        Me.TabControl1.Controls.Add(Me.tabUhrzeit)
        Me.TabControl1.Location = New System.Drawing.Point(14, 47)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(233, 214)
        Me.TabControl1.TabIndex = 568
        '
        'tabIntervall
        '
        Me.tabIntervall.Controls.Add(Me.optCustom)
        Me.tabIntervall.Controls.Add(Me.pnlCustom)
        Me.tabIntervall.Controls.Add(Me.opt120)
        Me.tabIntervall.Controls.Add(Me.opt60)
        Me.tabIntervall.Controls.Add(Me.opt5)
        Me.tabIntervall.Controls.Add(Me.opt10)
        Me.tabIntervall.Controls.Add(Me.opt30)
        Me.tabIntervall.Controls.Add(Me.opt15)
        Me.tabIntervall.Location = New System.Drawing.Point(4, 29)
        Me.tabIntervall.Name = "tabIntervall"
        Me.tabIntervall.Size = New System.Drawing.Size(225, 181)
        Me.tabIntervall.TabIndex = 2
        Me.tabIntervall.Text = "Intervall"
        Me.tabIntervall.ToolTipText = "feste Pausen-Intervalle"
        Me.tabIntervall.UseVisualStyleBackColor = True
        '
        'optCustom
        '
        Me.optCustom.AutoSize = True
        Me.optCustom.Location = New System.Drawing.Point(16, 108)
        Me.optCustom.Name = "optCustom"
        Me.optCustom.Size = New System.Drawing.Size(14, 13)
        Me.optCustom.TabIndex = 13
        Me.optCustom.UseVisualStyleBackColor = True
        '
        'pnlCustom
        '
        Me.pnlCustom.Controls.Add(Me.nudMinuten)
        Me.pnlCustom.Controls.Add(Me.Label2)
        Me.pnlCustom.Controls.Add(Me.nudStunden)
        Me.pnlCustom.Controls.Add(Me.Label1)
        Me.pnlCustom.Enabled = False
        Me.pnlCustom.Location = New System.Drawing.Point(33, 103)
        Me.pnlCustom.Name = "pnlCustom"
        Me.pnlCustom.Size = New System.Drawing.Size(177, 63)
        Me.pnlCustom.TabIndex = 12
        '
        'nudMinuten
        '
        Me.nudMinuten.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.nudMinuten.Location = New System.Drawing.Point(90, 28)
        Me.nudMinuten.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.nudMinuten.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.nudMinuten.Name = "nudMinuten"
        Me.nudMinuten.Size = New System.Drawing.Size(75, 26)
        Me.nudMinuten.TabIndex = 11
        Me.nudMinuten.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudMinuten.Value = New Decimal(New Integer() {15, 0, 0, 0})
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label2.Location = New System.Drawing.Point(88, 3)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 20)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Minuten"
        '
        'nudStunden
        '
        Me.nudStunden.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.nudStunden.Location = New System.Drawing.Point(5, 28)
        Me.nudStunden.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.nudStunden.Maximum = New Decimal(New Integer() {23, 0, 0, 0})
        Me.nudStunden.Name = "nudStunden"
        Me.nudStunden.Size = New System.Drawing.Size(75, 26)
        Me.nudStunden.TabIndex = 9
        Me.nudStunden.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label1.Location = New System.Drawing.Point(2, 3)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 20)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Stunden"
        '
        'opt120
        '
        Me.opt120.AutoSize = True
        Me.opt120.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.opt120.Location = New System.Drawing.Point(129, 65)
        Me.opt120.Name = "opt120"
        Me.opt120.Size = New System.Drawing.Size(73, 24)
        Me.opt120.TabIndex = 5
        Me.opt120.Text = " 2 Std."
        Me.opt120.UseVisualStyleBackColor = True
        '
        'opt60
        '
        Me.opt60.AutoSize = True
        Me.opt60.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.opt60.Location = New System.Drawing.Point(129, 38)
        Me.opt60.Name = "opt60"
        Me.opt60.Size = New System.Drawing.Size(73, 24)
        Me.opt60.TabIndex = 4
        Me.opt60.Text = " 1 Std."
        Me.opt60.UseVisualStyleBackColor = True
        '
        'opt5
        '
        Me.opt5.AutoSize = True
        Me.opt5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.opt5.Location = New System.Drawing.Point(16, 11)
        Me.opt5.Name = "opt5"
        Me.opt5.Size = New System.Drawing.Size(77, 24)
        Me.opt5.TabIndex = 0
        Me.opt5.Text = "  5 Min."
        Me.opt5.UseVisualStyleBackColor = True
        '
        'opt10
        '
        Me.opt10.AutoSize = True
        Me.opt10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.opt10.Location = New System.Drawing.Point(16, 38)
        Me.opt10.Name = "opt10"
        Me.opt10.Size = New System.Drawing.Size(78, 24)
        Me.opt10.TabIndex = 1
        Me.opt10.TabStop = True
        Me.opt10.Text = "10 Min."
        Me.opt10.UseVisualStyleBackColor = True
        '
        'opt30
        '
        Me.opt30.AutoSize = True
        Me.opt30.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.opt30.Location = New System.Drawing.Point(129, 11)
        Me.opt30.Name = "opt30"
        Me.opt30.Size = New System.Drawing.Size(72, 24)
        Me.opt30.TabIndex = 3
        Me.opt30.Text = "½ Std."
        Me.opt30.UseVisualStyleBackColor = True
        '
        'opt15
        '
        Me.opt15.AutoSize = True
        Me.opt15.Checked = True
        Me.opt15.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.opt15.Location = New System.Drawing.Point(16, 65)
        Me.opt15.Name = "opt15"
        Me.opt15.Size = New System.Drawing.Size(78, 24)
        Me.opt15.TabIndex = 2
        Me.opt15.TabStop = True
        Me.opt15.Text = "15 Min."
        Me.opt15.UseVisualStyleBackColor = True
        '
        'tabUhrzeit
        '
        Me.tabUhrzeit.Controls.Add(Me.dtpZeit)
        Me.tabUhrzeit.Controls.Add(Me.Label3)
        Me.tabUhrzeit.Location = New System.Drawing.Point(4, 29)
        Me.tabUhrzeit.Name = "tabUhrzeit"
        Me.tabUhrzeit.Padding = New System.Windows.Forms.Padding(3)
        Me.tabUhrzeit.Size = New System.Drawing.Size(225, 181)
        Me.tabUhrzeit.TabIndex = 1
        Me.tabUhrzeit.Text = "Uhrzeit"
        Me.tabUhrzeit.ToolTipText = "Uhrzeit Pausen-Ende"
        Me.tabUhrzeit.UseVisualStyleBackColor = True
        '
        'dtpZeit
        '
        Me.dtpZeit.CustomFormat = "HH:mm"
        Me.dtpZeit.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpZeit.Location = New System.Drawing.Point(76, 32)
        Me.dtpZeit.Name = "dtpZeit"
        Me.dtpZeit.ShowUpDown = True
        Me.dtpZeit.Size = New System.Drawing.Size(75, 26)
        Me.dtpZeit.TabIndex = 567
        Me.dtpZeit.Value = New Date(2020, 7, 9, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(41, 36)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 20)
        Me.Label3.TabIndex = 566
        Me.Label3.Text = "bis"
        '
        'btnEscape
        '
        Me.btnEscape.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEscape.BackColor = System.Drawing.Color.Maroon
        Me.btnEscape.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnEscape.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnEscape.FlatAppearance.BorderSize = 0
        Me.btnEscape.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnEscape.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnEscape.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEscape.Font = New System.Drawing.Font("Wingdings 2", 14.0!, System.Drawing.FontStyle.Bold)
        Me.btnEscape.ForeColor = System.Drawing.Color.DarkGray
        Me.btnEscape.Location = New System.Drawing.Point(225, 0)
        Me.btnEscape.Name = "btnEscape"
        Me.btnEscape.Size = New System.Drawing.Size(38, 30)
        Me.btnEscape.TabIndex = 564
        Me.btnEscape.TabStop = False
        Me.btnEscape.Text = ""
        Me.btnEscape.UseVisualStyleBackColor = False
        '
        'lblTitleBar
        '
        Me.lblTitleBar.BackColor = System.Drawing.Color.Maroon
        Me.lblTitleBar.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTitleBar.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitleBar.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitleBar.ForeColor = System.Drawing.Color.White
        Me.lblTitleBar.Location = New System.Drawing.Point(0, 0)
        Me.lblTitleBar.Name = "lblTitleBar"
        Me.lblTitleBar.Padding = New System.Windows.Forms.Padding(7, 0, 0, 0)
        Me.lblTitleBar.Size = New System.Drawing.Size(263, 30)
        Me.lblTitleBar.TabIndex = 502
        Me.lblTitleBar.Text = "Pause"
        Me.lblTitleBar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkHideAds
        '
        Me.chkHideAds.AutoSize = True
        Me.chkHideAds.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkHideAds.Location = New System.Drawing.Point(23, 272)
        Me.chkHideAds.Name = "chkHideAds"
        Me.chkHideAds.Size = New System.Drawing.Size(223, 22)
        Me.chkHideAds.TabIndex = 569
        Me.chkHideAds.Text = "Werbung in Bohle ausblenden"
        Me.chkHideAds.UseVisualStyleBackColor = True
        '
        'frmPause
        '
        Me.AcceptButton = Me.btnStart
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnEscape
        Me.ClientSize = New System.Drawing.Size(265, 388)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlForm)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPause"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Pause"
        Me.TopMost = True
        Me.pnlForm.ResumeLayout(False)
        Me.pnlForm.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tabIntervall.ResumeLayout(False)
        Me.tabIntervall.PerformLayout()
        Me.pnlCustom.ResumeLayout(False)
        Me.pnlCustom.PerformLayout()
        CType(Me.nudMinuten, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudStunden, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabUhrzeit.ResumeLayout(False)
        Me.tabUhrzeit.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnStart As Button
    Friend WithEvents pnlForm As Panel
    Friend WithEvents lblTitleBar As Label
    Friend WithEvents btnEscape As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents tabUhrzeit As TabPage
    Friend WithEvents dtpZeit As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents tabIntervall As TabPage
    Friend WithEvents opt120 As RadioButton
    Friend WithEvents opt60 As RadioButton
    Friend WithEvents opt5 As RadioButton
    Friend WithEvents opt10 As RadioButton
    Friend WithEvents opt30 As RadioButton
    Friend WithEvents opt15 As RadioButton
    Friend WithEvents optCustom As RadioButton
    Friend WithEvents pnlCustom As Panel
    Friend WithEvents nudMinuten As NumericUpDown
    Friend WithEvents Label2 As Label
    Friend WithEvents nudStunden As NumericUpDown
    Friend WithEvents Label1 As Label
    Friend WithEvents chkHideAds As CheckBox
End Class
