﻿
Public Class frmPrognose

    Private WithEvents bs As BindingSource
    Private dv As DataView
    Dim Wertung As Integer
    Dim NK_Stellen As Integer

    Private Sub frmPrognose_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        Wertung = CInt(Glob.dtModus.Select("idModus = " & Wettkampf.Modus)(0)("Wertung"))
        NK_Stellen = Get_NK_Stellen(Wertung)

        With dgvResult
            .Columns("Reissen").DefaultCellStyle.Format = "N" & NK_Stellen
            .Columns("Stossen").DefaultCellStyle.Format = "N" & NK_Stellen
            .Columns("Gesamt").DefaultCellStyle.Format = "N" & NK_Stellen
        End With

        If Glob.dtPrognose.Columns.Count = 0 Then
            ' Tabelle wird nur einmal erstellt, Änderungen werden darin gespeichert
            Glob.dtPrognose = dtResults.DefaultView.ToTable(True, {"TeamID", "Teilnehmer", "Nachname", "Vorname", "idVerein", "Verein", "a_k", "Gruppe"})
            Glob.dtPrognose.Columns.Add(New DataColumn With {.ColumnName = "Reißen", .DataType = GetType(Boolean), .DefaultValue = True})
            Glob.dtPrognose.Columns.Add(New DataColumn With {.ColumnName = "Stoßen", .DataType = GetType(Boolean), .DefaultValue = True})

            For Each row As DataRow In Glob.dtPrognose.Rows
                If CBool(row!a_k) OrElse CInt(row!Gruppe) = 1000 Then
                    row!Reißen = False
                    row!Stoßen = False
                End If
            Next
            Glob.dtPrognose.AcceptChanges()
        End If

    End Sub

    Private Sub frmPrognose_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Height += (Glob.dtTeams.Rows.Count - 2) * dgvResult.RowTemplate.Height

        dv = New DataView(Glob.dtPrognose)
        bs = New BindingSource With {.DataSource = dv, .Sort = "TeamID, Nachname, Vorname"}
        With dgvHeber
            .AutoGenerateColumns = False
            .DataSource = bs
        End With

        btnRefresh.PerformClick()

        Cursor = Cursors.Default
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Cursor = Cursors.WaitCursor
        bs.EndEdit()

        Dim dtResultsCopy = dtResults.Copy
        dtResultsCopy.AcceptChanges()

#Region "alle HLasten einlesen"
        If Not IsNothing(dvLeader(0)) Then
            Dim HLast As Integer
            Dim Durchgang = {{"R", "Reißen", "Reissen"}, {"S", "Stoßen", "Stossen"}}

            For Each row As DataRow In dtResultsCopy.Rows
                Dim pos = bs.Find("Teilnehmer", row!Teilnehmer)

                'If CInt(dv(pos)!Gruppe) <> 1000 AndAlso Not CBool(dv(pos)(Durchgang(Leader.TableIx, 1))) Then
                '    ' Heber wurde eingewechselt
                '    dv(pos)(Durchgang(Leader.TableIx, 1)) = True
                '    bs.EndEdit()
                'End If

                For dg = 0 To 1
                    If CBool(dv(pos)(Durchgang(dg, 1))) Then
                        For Each d In Durchgang(dg, 0)
                            HLast = 0
                            For i = 1 To 3
                                If Not IsDBNull(row(d & "_" & i)) Then
                                    ' HLast vorhanden (auch bei ungültigem Vorversuch durch automatische Steigerung)
                                    If IsDBNull(row(d & "W_" & i)) OrElse CInt(row(d & "W_" & i)) = 1 Then ' gültiger oder ungewerteter Versuch
                                        HLast = CInt(row(d & "_" & i))
                                        If IsDBNull(row(d & "W_" & i)) Then row(d & "W_" & i) = 1
                                    End If
                                Else
                                    ' keine HLast
                                    If i > 1 Then HLast += Wettkampf.Steigerung(i - 2)
                                    row(d & "_" & i) = HLast
                                    row(d & "W_" & i) = 1
                                End If
                            Next
                            ' Wertung entsprechend des Modus berechnen
                            Select Case Wertung
                                Case 2
                                    row(Durchgang(dg, 2)) = HLast - CDbl(row!Relativ)
                                    If CDbl(row(Durchgang(dg, 2))) < 0 Then row(Durchgang(dg, 2)) = 0
                                Case 3
                                    row(Durchgang(dg, 2)) = Math.Round(HLast * CDbl(row!Sinclair), NK_Stellen)
                            End Select
                        Next
                        If Not IsDBNull(row!Reissen) AndAlso Not IsDBNull(row!Stossen) Then row!Heben = CDbl(row!Reissen) + CDbl(row!Stossen)
                    Else
                        row!Reissen = DBNull.Value
                        row!Stossen = DBNull.Value
                        row!Heben = DBNull.Value
                    End If
                Next
            Next
            dtResultsCopy.AcceptChanges()
        End If
#End Region

        Dim dtTeams As DataTable

        Using RS As New Results
            dtTeams = RS.TeamWertung(dtResultsCopy, True)
        End Using

        With dgvResult
            .AutoGenerateColumns = False
            If Wettkampf.Finale Then
                .Columns("Gegner").Visible = True
                .Columns("Punkte2").Visible = True
            End If
            .DataSource = dtTeams
            .ClearSelection()
        End With

        btnRefresh.Enabled = False
        dgvHeber.Focus()
        Cursor = Cursors.Default
    End Sub

    Private Sub bs_CurrentItemChanged(sender As Object, e As EventArgs) Handles bs.CurrentItemChanged
        bs.EndEdit()
    End Sub

    Private Sub dgvResult_SelectionChanged(sender As Object, e As EventArgs) Handles dgvResult.SelectionChanged
        dgvResult.ClearSelection()
    End Sub

    Private Sub dgvHeber_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvHeber.CellContentClick
        If dgvHeber.Columns(e.ColumnIndex).Name.Contains("ßen") Then ' Reißen, Stoßen
            dgvHeber.EndEdit()
            btnRefresh.Enabled = True
        End If
    End Sub


End Class