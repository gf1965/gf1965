Public Class frmInputBox
    Property Message As String
    Overloads Property Left As Integer = -1
    Overloads Property Top As Integer = -1
    Property Response As String
    Property AllowEmptyText As Boolean
    Property MaxLength As Integer

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Response = txtInput.Text
        Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response = Nothing
        Close()
    End Sub

    Private Sub frmPasswort_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        lblMessage.Text = Message

        If Left = -1 AndAlso Top = -1 Then
            StartPosition = FormStartPosition.CenterParent
        Else
            StartPosition = FormStartPosition.Manual
        End If

        BringToFront()

    End Sub

    Private Sub chkShowPassword_CheckedChanged(sender As Object, e As EventArgs) Handles chkShowPassword.CheckedChanged
        txtInput.PasswordChar = If(chkShowPassword.Checked, CChar(""), CChar("*"))
    End Sub

    Private Sub txtInput_TextChanged(sender As Object, e As EventArgs) Handles txtInput.TextChanged

        btnOK.Enabled = AllowEmptyText OrElse Not String.IsNullOrEmpty(txtInput.Text)

        If MaxLength > 0 AndAlso txtInput.Text.Length > MaxLength Then
            txtInput.Text = Strings.Left(txtInput.Text, MaxLength)
            Beep()
        End If
    End Sub

End Class

Public Class Custom_InputBox
    Implements IDisposable

    ' Usage:        Using ctl as New Custom_InputBox(Message, [Caption], [Left], [Top], [IsPassword], [MultiLine])
    ' ReturnValue:  ctl.Response As String (Nothing --> Cancel/Exit, String.Empty --> no input)

    Property Response As String
    Public Sub New(Message As String,
                   Optional Title As String = "GFHsoft Gewichtheben",
                   Optional DefaultResponse As String = "",
                   Optional Left As Integer = -1,
                   Optional Top As Integer = -1,
                   Optional IsPassword As Boolean = False,
                   Optional MultiLine As Boolean = False,
                   Optional AllowEmptyText As Boolean = False,
                   Optional MaxLength As Integer = 0)

        Dim frm As New frmInputBox With {.Message = Message,
                                         .Text = Title,
                                         .StartPosition = If(Top = -1 OrElse Left = -1, FormStartPosition.CenterParent, FormStartPosition.Manual),
                                         .Location = New Point(Left, Top),
                                         .AllowEmptyText = AllowEmptyText,
                                         .MaxLength = MaxLength}
        With frm
            .Height = If(MultiLine, .MaximumSize.Height, .MinimumSize.Height)
            .AcceptButton = If(MultiLine, Nothing, .btnOK)
            With .txtInput
                .PasswordChar = CChar(If(IsPassword, "*", ""))
                .Multiline = MultiLine
                .Width = If(MultiLine OrElse Not IsPassword, .MaximumSize.Width, .MinimumSize.Width)
                .Text = DefaultResponse
            End With
            .chkShowPassword.Visible = IsPassword
            .btnOK.Enabled = AllowEmptyText OrElse Not String.IsNullOrEmpty(DefaultResponse)
            .ShowDialog()
            Response = .Response
        End With
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
            End If

            ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten �berschreiben.
            ' TODO: gro�e Felder auf Null setzen.
        End If
        disposedValue = True
    End Sub

    ' TODO: Finalize() nur �berschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enth�lt.
    'Protected Overrides Sub Finalize()
    '    ' �ndern Sie diesen Code nicht. F�gen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' Dieser Code wird von Visual Basic hinzugef�gt, um das Dispose-Muster richtig zu implementieren.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' �ndern Sie diesen Code nicht. F�gen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        Dispose(True)
        ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben �berschrieben wird.
        ' GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
