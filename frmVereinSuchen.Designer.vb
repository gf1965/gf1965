﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVereinSuchen
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnVereinSubmit = New System.Windows.Forms.Button()
        Me.btnVereinCancel = New System.Windows.Forms.Button()
        Me.cboVereinDatenbank = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblVereinOnline = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnVereinSubmit
        '
        Me.btnVereinSubmit.Location = New System.Drawing.Point(65, 131)
        Me.btnVereinSubmit.Name = "btnVereinSubmit"
        Me.btnVereinSubmit.Size = New System.Drawing.Size(80, 25)
        Me.btnVereinSubmit.TabIndex = 10
        Me.btnVereinSubmit.Text = "Übernehmen"
        Me.btnVereinSubmit.UseVisualStyleBackColor = True
        '
        'btnVereinCancel
        '
        Me.btnVereinCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnVereinCancel.Location = New System.Drawing.Point(151, 131)
        Me.btnVereinCancel.Name = "btnVereinCancel"
        Me.btnVereinCancel.Size = New System.Drawing.Size(80, 25)
        Me.btnVereinCancel.TabIndex = 11
        Me.btnVereinCancel.Text = "Abbrechen"
        Me.btnVereinCancel.UseVisualStyleBackColor = True
        '
        'cboVereinDatenbank
        '
        Me.cboVereinDatenbank.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cboVereinDatenbank.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboVereinDatenbank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVereinDatenbank.FormattingEnabled = True
        Me.cboVereinDatenbank.Location = New System.Drawing.Point(20, 88)
        Me.cboVereinDatenbank.Name = "cboVereinDatenbank"
        Me.cboVereinDatenbank.Size = New System.Drawing.Size(210, 21)
        Me.cboVereinDatenbank.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(23, 70)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(135, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Vereinsname in Datenbank"
        '
        'lblVereinOnline
        '
        Me.lblVereinOnline.BackColor = System.Drawing.SystemColors.Window
        Me.lblVereinOnline.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVereinOnline.Location = New System.Drawing.Point(20, 38)
        Me.lblVereinOnline.Name = "lblVereinOnline"
        Me.lblVereinOnline.Padding = New System.Windows.Forms.Padding(2, 0, 0, 0)
        Me.lblVereinOnline.Size = New System.Drawing.Size(210, 20)
        Me.lblVereinOnline.TabIndex = 7
        Me.lblVereinOnline.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(23, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(156, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Vereinsname in Online-Meldung"
        '
        'frmVereinSuchen
        '
        Me.AcceptButton = Me.btnVereinSubmit
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnVereinCancel
        Me.ClientSize = New System.Drawing.Size(251, 171)
        Me.Controls.Add(Me.btnVereinSubmit)
        Me.Controls.Add(Me.btnVereinCancel)
        Me.Controls.Add(Me.cboVereinDatenbank)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblVereinOnline)
        Me.Controls.Add(Me.Label6)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVereinSuchen"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Verein auswählen"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnVereinSubmit As Button
    Friend WithEvents btnVereinCancel As Button
    Friend WithEvents cboVereinDatenbank As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents lblVereinOnline As Label
    Friend WithEvents Label6 As Label
End Class
