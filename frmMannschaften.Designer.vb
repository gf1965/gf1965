﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMannschaften
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnlG1 = New System.Windows.Forms.Panel()
        Me.lblWK = New System.Windows.Forms.Label()
        Me.lblWiegen = New System.Windows.Forms.Label()
        Me.cboB1 = New System.Windows.Forms.ComboBox()
        Me.lblBohle = New System.Windows.Forms.Label()
        Me.chkBH1 = New System.Windows.Forms.CheckBox()
        Me.lblG1 = New System.Windows.Forms.Label()
        Me.dgvG1 = New System.Windows.Forms.DataGridView()
        Me.dpkWiegen = New System.Windows.Forms.DateTimePicker()
        Me.lblDatum = New System.Windows.Forms.Label()
        Me.dpkWK = New System.Windows.Forms.DateTimePicker()
        Me.dpkDatum = New System.Windows.Forms.DateTimePicker()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDrucken = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLayout = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuMeldungen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKonflikte = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.cboArt = New System.Windows.Forms.ToolStripComboBox()
        Me.bgwData = New System.ComponentModel.BackgroundWorker()
        Me.pnlBtn = New System.Windows.Forms.Panel()
        Me.btnInvert = New System.Windows.Forms.Button()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.pnlForm = New System.Windows.Forms.Panel()
        Me.pnlG1.SuspendLayout()
        CType(Me.dgvG1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.pnlBtn.SuspendLayout()
        Me.pnlForm.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlG1
        '
        Me.pnlG1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnlG1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.pnlG1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlG1.Controls.Add(Me.lblWK)
        Me.pnlG1.Controls.Add(Me.lblWiegen)
        Me.pnlG1.Controls.Add(Me.cboB1)
        Me.pnlG1.Controls.Add(Me.lblBohle)
        Me.pnlG1.Controls.Add(Me.chkBH1)
        Me.pnlG1.Controls.Add(Me.lblG1)
        Me.pnlG1.Controls.Add(Me.dgvG1)
        Me.pnlG1.Controls.Add(Me.dpkWiegen)
        Me.pnlG1.Controls.Add(Me.lblDatum)
        Me.pnlG1.Controls.Add(Me.dpkWK)
        Me.pnlG1.Controls.Add(Me.dpkDatum)
        Me.pnlG1.Location = New System.Drawing.Point(10, 10)
        Me.pnlG1.Name = "pnlG1"
        Me.pnlG1.Size = New System.Drawing.Size(314, 476)
        Me.pnlG1.TabIndex = 1
        '
        'lblWK
        '
        Me.lblWK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblWK.AutoSize = True
        Me.lblWK.Location = New System.Drawing.Point(171, 427)
        Me.lblWK.Name = "lblWK"
        Me.lblWK.Size = New System.Drawing.Size(61, 13)
        Me.lblWK.TabIndex = 219
        Me.lblWK.Text = "WK-Beginn"
        Me.lblWK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWiegen
        '
        Me.lblWiegen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblWiegen.AutoSize = True
        Me.lblWiegen.Location = New System.Drawing.Point(101, 427)
        Me.lblWiegen.Name = "lblWiegen"
        Me.lblWiegen.Size = New System.Drawing.Size(44, 13)
        Me.lblWiegen.TabIndex = 218
        Me.lblWiegen.Text = "Wiegen"
        Me.lblWiegen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboB1
        '
        Me.cboB1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboB1.FormattingEnabled = True
        Me.cboB1.Items.AddRange(New Object() {"1", "2"})
        Me.cboB1.Location = New System.Drawing.Point(266, 443)
        Me.cboB1.Name = "cboB1"
        Me.cboB1.Size = New System.Drawing.Size(30, 21)
        Me.cboB1.TabIndex = 10
        '
        'lblBohle
        '
        Me.lblBohle.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblBohle.AutoSize = True
        Me.lblBohle.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lblBohle.Location = New System.Drawing.Point(264, 427)
        Me.lblBohle.Name = "lblBohle"
        Me.lblBohle.Size = New System.Drawing.Size(34, 13)
        Me.lblBohle.TabIndex = 213
        Me.lblBohle.Text = "Bohle"
        Me.lblBohle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkBH1
        '
        Me.chkBH1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkBH1.AutoSize = True
        Me.chkBH1.BackColor = System.Drawing.Color.SteelBlue
        Me.chkBH1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkBH1.ForeColor = System.Drawing.Color.White
        Me.chkBH1.Location = New System.Drawing.Point(213, 10)
        Me.chkBH1.Name = "chkBH1"
        Me.chkBH1.Size = New System.Drawing.Size(83, 17)
        Me.chkBH1.TabIndex = 3
        Me.chkBH1.Text = "Blockheben"
        Me.chkBH1.UseVisualStyleBackColor = False
        '
        'lblG1
        '
        Me.lblG1.BackColor = System.Drawing.Color.SteelBlue
        Me.lblG1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblG1.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblG1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblG1.ForeColor = System.Drawing.Color.White
        Me.lblG1.Location = New System.Drawing.Point(0, 0)
        Me.lblG1.Name = "lblG1"
        Me.lblG1.Padding = New System.Windows.Forms.Padding(23, 0, 0, 0)
        Me.lblG1.Size = New System.Drawing.Size(310, 35)
        Me.lblG1.TabIndex = 217
        Me.lblG1.Text = "Gruppe 1"
        Me.lblG1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvG1
        '
        Me.dgvG1.AllowDrop = True
        Me.dgvG1.AllowUserToAddRows = False
        Me.dgvG1.AllowUserToDeleteRows = False
        Me.dgvG1.AllowUserToOrderColumns = True
        Me.dgvG1.AllowUserToResizeColumns = False
        Me.dgvG1.AllowUserToResizeRows = False
        Me.dgvG1.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvG1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvG1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvG1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvG1.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvG1.Location = New System.Drawing.Point(0, 42)
        Me.dgvG1.Name = "dgvG1"
        Me.dgvG1.ReadOnly = True
        Me.dgvG1.RowHeadersVisible = False
        Me.dgvG1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvG1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvG1.Size = New System.Drawing.Size(310, 374)
        Me.dgvG1.TabIndex = 4
        '
        'dpkWiegen
        '
        Me.dpkWiegen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dpkWiegen.Checked = False
        Me.dpkWiegen.CustomFormat = "HH:mm"
        Me.dpkWiegen.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dpkWiegen.Location = New System.Drawing.Point(101, 443)
        Me.dpkWiegen.Name = "dpkWiegen"
        Me.dpkWiegen.ShowUpDown = True
        Me.dpkWiegen.Size = New System.Drawing.Size(56, 20)
        Me.dpkWiegen.TabIndex = 6
        '
        'lblDatum
        '
        Me.lblDatum.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblDatum.AutoSize = True
        Me.lblDatum.Location = New System.Drawing.Point(7, 427)
        Me.lblDatum.Name = "lblDatum"
        Me.lblDatum.Size = New System.Drawing.Size(59, 13)
        Me.lblDatum.TabIndex = 33
        Me.lblDatum.Text = "WK-Datum"
        Me.lblDatum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dpkWK
        '
        Me.dpkWK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dpkWK.Checked = False
        Me.dpkWK.CustomFormat = "HH:mm"
        Me.dpkWK.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dpkWK.Location = New System.Drawing.Point(171, 443)
        Me.dpkWK.Name = "dpkWK"
        Me.dpkWK.ShowUpDown = True
        Me.dpkWK.Size = New System.Drawing.Size(56, 20)
        Me.dpkWK.TabIndex = 7
        '
        'dpkDatum
        '
        Me.dpkDatum.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dpkDatum.Checked = False
        Me.dpkDatum.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dpkDatum.Location = New System.Drawing.Point(7, 443)
        Me.dpkDatum.Name = "dpkDatum"
        Me.dpkDatum.ShowUpDown = True
        Me.dpkDatum.Size = New System.Drawing.Size(80, 20)
        Me.dpkDatum.TabIndex = 5
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.MenuBar
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuExtras, Me.ToolStripTextBox1, Me.cboArt})
        Me.MenuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.MenuStrip1.Size = New System.Drawing.Size(1014, 27)
        Me.MenuStrip1.TabIndex = 199
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSpeichern, Me.mnuDrucken, Me.ToolStripMenuItem1, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 23)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuSpeichern
        '
        Me.mnuSpeichern.Name = "mnuSpeichern"
        Me.mnuSpeichern.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSpeichern.Size = New System.Drawing.Size(168, 22)
        Me.mnuSpeichern.Text = "&Speichern"
        '
        'mnuDrucken
        '
        Me.mnuDrucken.Name = "mnuDrucken"
        Me.mnuDrucken.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.mnuDrucken.Size = New System.Drawing.Size(168, 22)
        Me.mnuDrucken.Text = "&Drucken"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(165, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(168, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLayout, Me.ToolStripMenuItem2, Me.mnuMeldungen, Me.mnuKonflikte})
        Me.mnuExtras.Name = "mnuExtras"
        Me.mnuExtras.Size = New System.Drawing.Size(49, 23)
        Me.mnuExtras.Text = "&Extras"
        '
        'mnuLayout
        '
        Me.mnuLayout.Name = "mnuLayout"
        Me.mnuLayout.Size = New System.Drawing.Size(210, 22)
        Me.mnuLayout.Text = "&Tabellen-Layout"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(207, 6)
        '
        'mnuMeldungen
        '
        Me.mnuMeldungen.CheckOnClick = True
        Me.mnuMeldungen.Name = "mnuMeldungen"
        Me.mnuMeldungen.Size = New System.Drawing.Size(210, 22)
        Me.mnuMeldungen.Text = "&Meldungen ausblenden"
        '
        'mnuKonflikte
        '
        Me.mnuKonflikte.Checked = True
        Me.mnuKonflikte.CheckOnClick = True
        Me.mnuKonflikte.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuKonflikte.Name = "mnuKonflikte"
        Me.mnuKonflikte.Size = New System.Drawing.Size(210, 22)
        Me.mnuKonflikte.Text = "bei &Konflikten nachfragen"
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.BackColor = System.Drawing.SystemColors.MenuBar
        Me.ToolStripTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(200, 16)
        '
        'cboArt
        '
        Me.cboArt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboArt.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cboArt.Items.AddRange(New Object() {"Reißen", "Stoßen"})
        Me.cboArt.Name = "cboArt"
        Me.cboArt.Size = New System.Drawing.Size(121, 23)
        '
        'bgwData
        '
        Me.bgwData.WorkerReportsProgress = True
        '
        'pnlBtn
        '
        Me.pnlBtn.Controls.Add(Me.btnInvert)
        Me.pnlBtn.Controls.Add(Me.btnRemove)
        Me.pnlBtn.Controls.Add(Me.btnAdd)
        Me.pnlBtn.Location = New System.Drawing.Point(331, 17)
        Me.pnlBtn.Name = "pnlBtn"
        Me.pnlBtn.Size = New System.Drawing.Size(35, 229)
        Me.pnlBtn.TabIndex = 200
        '
        'btnInvert
        '
        Me.btnInvert.Enabled = False
        Me.btnInvert.Image = Global.Gewichtheben.My.Resources.Resources.SendtoBack
        Me.btnInvert.Location = New System.Drawing.Point(0, 134)
        Me.btnInvert.Name = "btnInvert"
        Me.btnInvert.Size = New System.Drawing.Size(25, 25)
        Me.btnInvert.TabIndex = 203
        Me.btnInvert.TabStop = False
        Me.btnInvert.UseVisualStyleBackColor = True
        Me.btnInvert.Visible = False
        '
        'btnRemove
        '
        Me.btnRemove.Enabled = False
        Me.btnRemove.Image = Global.Gewichtheben.My.Resources.Resources.Delete21
        Me.btnRemove.Location = New System.Drawing.Point(0, 103)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(25, 25)
        Me.btnRemove.TabIndex = 202
        Me.btnRemove.TabStop = False
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Image = Global.Gewichtheben.My.Resources.Resources.Add
        Me.btnAdd.Location = New System.Drawing.Point(0, 72)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(25, 25)
        Me.btnAdd.TabIndex = 201
        Me.btnAdd.TabStop = False
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'pnlForm
        '
        Me.pnlForm.AutoScroll = True
        Me.pnlForm.Controls.Add(Me.pnlBtn)
        Me.pnlForm.Controls.Add(Me.pnlG1)
        Me.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlForm.Location = New System.Drawing.Point(0, 27)
        Me.pnlForm.Name = "pnlForm"
        Me.pnlForm.Size = New System.Drawing.Size(1014, 495)
        Me.pnlForm.TabIndex = 201
        '
        'frmMannschaften
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.ClientSize = New System.Drawing.Size(1014, 522)
        Me.Controls.Add(Me.pnlForm)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1030, 560)
        Me.Name = "frmMannschaften"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Mannschaften einteilen"
        Me.pnlG1.ResumeLayout(False)
        Me.pnlG1.PerformLayout()
        CType(Me.dgvG1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.pnlBtn.ResumeLayout(False)
        Me.pnlForm.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnlG1 As System.Windows.Forms.Panel
    Friend WithEvents chkBH1 As System.Windows.Forms.CheckBox
    Friend WithEvents dgvG1 As System.Windows.Forms.DataGridView
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDatei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeichern As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuBeenden As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtras As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLayout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bgwData As System.ComponentModel.BackgroundWorker
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuMeldungen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuKonflikte As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dpkWK As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDatum As System.Windows.Forms.Label
    Friend WithEvents dpkDatum As System.Windows.Forms.DateTimePicker
    Friend WithEvents dpkWiegen As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblBohle As System.Windows.Forms.Label
    Friend WithEvents cboB1 As System.Windows.Forms.ComboBox
    Friend WithEvents pnlBtn As System.Windows.Forms.Panel
    Friend WithEvents pnlForm As System.Windows.Forms.Panel
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents lblG1 As System.Windows.Forms.Label
    Friend WithEvents btnInvert As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents mnuDrucken As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents cboArt As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents lblWK As System.Windows.Forms.Label
    Friend WithEvents lblWiegen As System.Windows.Forms.Label
End Class
