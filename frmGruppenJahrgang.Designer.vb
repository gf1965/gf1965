﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGruppenJahrgang
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblG1 = New System.Windows.Forms.Label()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.dateA1 = New System.Windows.Forms.DateTimePicker()
        Me.pnl1 = New System.Windows.Forms.Panel()
        Me.cboB1 = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.timeA1 = New System.Windows.Forms.DateTimePicker()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.timeR1 = New System.Windows.Forms.DateTimePicker()
        Me.date1 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.timeS1 = New System.Windows.Forms.DateTimePicker()
        Me.cbo1 = New System.Windows.Forms.ComboBox()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.dgvGK1 = New System.Windows.Forms.DataGridView()
        Me.chkBH1 = New System.Windows.Forms.CheckBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblG2 = New System.Windows.Forms.Label()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.dateA2 = New System.Windows.Forms.DateTimePicker()
        Me.pnl2 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.cboB2 = New System.Windows.Forms.ComboBox()
        Me.timeA2 = New System.Windows.Forms.DateTimePicker()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.timeR2 = New System.Windows.Forms.DateTimePicker()
        Me.date2 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.timeS2 = New System.Windows.Forms.DateTimePicker()
        Me.cbo2 = New System.Windows.Forms.ComboBox()
        Me.lbl2 = New System.Windows.Forms.Label()
        Me.dgvGK2 = New System.Windows.Forms.DataGridView()
        Me.chkBH2 = New System.Windows.Forms.CheckBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblG3 = New System.Windows.Forms.Label()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.pnl3 = New System.Windows.Forms.Panel()
        Me.cboB3 = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.dateA3 = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.timeA3 = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.timeR3 = New System.Windows.Forms.DateTimePicker()
        Me.date3 = New System.Windows.Forms.DateTimePicker()
        Me.timeS3 = New System.Windows.Forms.DateTimePicker()
        Me.cbo3 = New System.Windows.Forms.ComboBox()
        Me.lbl3 = New System.Windows.Forms.Label()
        Me.dgvGK3 = New System.Windows.Forms.DataGridView()
        Me.chkBH3 = New System.Windows.Forms.CheckBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblG4 = New System.Windows.Forms.Label()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.dateA4 = New System.Windows.Forms.DateTimePicker()
        Me.pnl4 = New System.Windows.Forms.Panel()
        Me.cboB4 = New System.Windows.Forms.ComboBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.timeA4 = New System.Windows.Forms.DateTimePicker()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.timeR4 = New System.Windows.Forms.DateTimePicker()
        Me.date4 = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.timeS4 = New System.Windows.Forms.DateTimePicker()
        Me.cbo4 = New System.Windows.Forms.ComboBox()
        Me.lbl4 = New System.Windows.Forms.Label()
        Me.dgvGK4 = New System.Windows.Forms.DataGridView()
        Me.chkBH4 = New System.Windows.Forms.CheckBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblG5 = New System.Windows.Forms.Label()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.dateA5 = New System.Windows.Forms.DateTimePicker()
        Me.pnl5 = New System.Windows.Forms.Panel()
        Me.cboB5 = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.timeA5 = New System.Windows.Forms.DateTimePicker()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.timeR5 = New System.Windows.Forms.DateTimePicker()
        Me.date5 = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.timeS5 = New System.Windows.Forms.DateTimePicker()
        Me.cbo5 = New System.Windows.Forms.ComboBox()
        Me.lbl5 = New System.Windows.Forms.Label()
        Me.dgvGK5 = New System.Windows.Forms.DataGridView()
        Me.chkBH5 = New System.Windows.Forms.CheckBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDrucken = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLayout = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuMeldungen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKonflikte = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblGruppe = New System.Windows.Forms.ToolStripTextBox()
        Me.cboGruppe = New System.Windows.Forms.ToolStripComboBox()
        Me.lblJahrgang = New System.Windows.Forms.ToolStripTextBox()
        Me.cboJahrgang = New System.Windows.Forms.ToolStripComboBox()
        Me.lblGeschlecht = New System.Windows.Forms.ToolStripTextBox()
        Me.cboGeschlecht = New System.Windows.Forms.ToolStripComboBox()
        Me.bgwGK = New System.ComponentModel.BackgroundWorker()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnlForm = New System.Windows.Forms.Panel()
        Me.lstJahrgang = New System.Windows.Forms.CheckedListBox()
        Me.Panel1.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.pnl1.SuspendLayout()
        CType(Me.dgvGK1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.pnl2.SuspendLayout()
        CType(Me.dgvGK2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.pnl3.SuspendLayout()
        CType(Me.dgvGK3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.pnl4.SuspendLayout()
        CType(Me.dgvGK4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.Panel15.SuspendLayout()
        Me.pnl5.SuspendLayout()
        CType(Me.dgvGK5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.pnlForm.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.lblG1)
        Me.Panel1.Controls.Add(Me.Panel11)
        Me.Panel1.Controls.Add(Me.cbo1)
        Me.Panel1.Controls.Add(Me.lbl1)
        Me.Panel1.Controls.Add(Me.dgvGK1)
        Me.Panel1.Controls.Add(Me.chkBH1)
        Me.Panel1.Location = New System.Drawing.Point(11, 40)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(237, 476)
        Me.Panel1.TabIndex = 1
        '
        'lblG1
        '
        Me.lblG1.BackColor = System.Drawing.Color.SteelBlue
        Me.lblG1.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblG1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblG1.ForeColor = System.Drawing.Color.White
        Me.lblG1.Location = New System.Drawing.Point(0, 0)
        Me.lblG1.Name = "lblG1"
        Me.lblG1.Size = New System.Drawing.Size(233, 29)
        Me.lblG1.TabIndex = 217
        Me.lblG1.Text = "Feder"
        Me.lblG1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.Label27)
        Me.Panel11.Controls.Add(Me.dateA1)
        Me.Panel11.Controls.Add(Me.pnl1)
        Me.Panel11.Controls.Add(Me.timeA1)
        Me.Panel11.Controls.Add(Me.Label17)
        Me.Panel11.Controls.Add(Me.Label7)
        Me.Panel11.Controls.Add(Me.Label6)
        Me.Panel11.Controls.Add(Me.timeR1)
        Me.Panel11.Controls.Add(Me.date1)
        Me.Panel11.Controls.Add(Me.Label1)
        Me.Panel11.Controls.Add(Me.timeS1)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel11.Location = New System.Drawing.Point(0, 374)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(233, 98)
        Me.Panel11.TabIndex = 216
        '
        'Label27
        '
        Me.Label27.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label27.AutoSize = True
        Me.Label27.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label27.Location = New System.Drawing.Point(4, 52)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(26, 13)
        Me.Label27.TabIndex = 223
        Me.Label27.Text = "Tag"
        '
        'dateA1
        '
        Me.dateA1.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.dateA1.Checked = False
        Me.dateA1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateA1.Location = New System.Drawing.Point(7, 68)
        Me.dateA1.Name = "dateA1"
        Me.dateA1.ShowUpDown = True
        Me.dateA1.Size = New System.Drawing.Size(80, 20)
        Me.dateA1.TabIndex = 8
        '
        'pnl1
        '
        Me.pnl1.BackColor = System.Drawing.Color.SteelBlue
        Me.pnl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl1.Controls.Add(Me.cboB1)
        Me.pnl1.Controls.Add(Me.Label21)
        Me.pnl1.Location = New System.Drawing.Point(171, 49)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(61, 48)
        Me.pnl1.TabIndex = 221
        '
        'cboB1
        '
        Me.cboB1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboB1.FormattingEnabled = True
        Me.cboB1.Items.AddRange(New Object() {"1", "2"})
        Me.cboB1.Location = New System.Drawing.Point(16, 19)
        Me.cboB1.Name = "cboB1"
        Me.cboB1.Size = New System.Drawing.Size(30, 21)
        Me.cboB1.TabIndex = 10
        '
        'Label21
        '
        Me.Label21.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(13, 4)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(34, 13)
        Me.Label21.TabIndex = 213
        Me.Label21.Text = "Bohle"
        '
        'timeA1
        '
        Me.timeA1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeA1.Checked = False
        Me.timeA1.CustomFormat = "HH:mm"
        Me.timeA1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeA1.Location = New System.Drawing.Point(101, 68)
        Me.timeA1.Name = "timeA1"
        Me.timeA1.ShowUpDown = True
        Me.timeA1.Size = New System.Drawing.Size(56, 20)
        Me.timeA1.TabIndex = 9
        '
        'Label17
        '
        Me.Label17.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(98, 52)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(42, 13)
        Me.Label17.TabIndex = 211
        Me.Label17.Text = "Athletik"
        '
        'Label7
        '
        Me.Label7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(168, 6)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 13)
        Me.Label7.TabIndex = 40
        Me.Label7.Text = "Stoßen"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(98, 6)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 13)
        Me.Label6.TabIndex = 39
        Me.Label6.Text = "Reißen"
        '
        'timeR1
        '
        Me.timeR1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeR1.Checked = False
        Me.timeR1.CustomFormat = "HH:mm"
        Me.timeR1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeR1.Location = New System.Drawing.Point(101, 22)
        Me.timeR1.Name = "timeR1"
        Me.timeR1.ShowUpDown = True
        Me.timeR1.Size = New System.Drawing.Size(56, 20)
        Me.timeR1.TabIndex = 6
        '
        'date1
        '
        Me.date1.Checked = False
        Me.date1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date1.Location = New System.Drawing.Point(7, 22)
        Me.date1.Name = "date1"
        Me.date1.ShowUpDown = True
        Me.date1.Size = New System.Drawing.Size(80, 20)
        Me.date1.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(26, 13)
        Me.Label1.TabIndex = 33
        Me.Label1.Text = "Tag"
        '
        'timeS1
        '
        Me.timeS1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeS1.Checked = False
        Me.timeS1.CustomFormat = "HH:mm"
        Me.timeS1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeS1.Location = New System.Drawing.Point(171, 22)
        Me.timeS1.Name = "timeS1"
        Me.timeS1.ShowUpDown = True
        Me.timeS1.Size = New System.Drawing.Size(56, 20)
        Me.timeS1.TabIndex = 7
        '
        'cbo1
        '
        Me.cbo1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo1.DropDownWidth = 35
        Me.cbo1.FormattingEnabled = True
        Me.cbo1.Location = New System.Drawing.Point(55, 34)
        Me.cbo1.MaxDropDownItems = 6
        Me.cbo1.Name = "cbo1"
        Me.cbo1.Size = New System.Drawing.Size(41, 21)
        Me.cbo1.TabIndex = 2
        '
        'lbl1
        '
        Me.lbl1.AutoSize = True
        Me.lbl1.BackColor = System.Drawing.Color.Transparent
        Me.lbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl1.Location = New System.Drawing.Point(10, 36)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(42, 13)
        Me.lbl1.TabIndex = 30
        Me.lbl1.Text = "Gruppe"
        Me.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvGK1
        '
        Me.dgvGK1.AllowDrop = True
        Me.dgvGK1.AllowUserToAddRows = False
        Me.dgvGK1.AllowUserToDeleteRows = False
        Me.dgvGK1.AllowUserToOrderColumns = True
        Me.dgvGK1.AllowUserToResizeColumns = False
        Me.dgvGK1.AllowUserToResizeRows = False
        Me.dgvGK1.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvGK1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGK1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvGK1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGK1.Location = New System.Drawing.Point(1, 60)
        Me.dgvGK1.Name = "dgvGK1"
        Me.dgvGK1.ReadOnly = True
        Me.dgvGK1.RowHeadersVisible = False
        Me.dgvGK1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGK1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGK1.ShowEditingIcon = False
        Me.dgvGK1.Size = New System.Drawing.Size(231, 309)
        Me.dgvGK1.TabIndex = 4
        '
        'chkBH1
        '
        Me.chkBH1.AutoSize = True
        Me.chkBH1.Location = New System.Drawing.Point(141, 36)
        Me.chkBH1.Name = "chkBH1"
        Me.chkBH1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkBH1.Size = New System.Drawing.Size(83, 17)
        Me.chkBH1.TabIndex = 3
        Me.chkBH1.Text = "Blockheben"
        Me.chkBH1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkBH1.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel2.Controls.Add(Me.lblG2)
        Me.Panel2.Controls.Add(Me.Panel12)
        Me.Panel2.Controls.Add(Me.cbo2)
        Me.Panel2.Controls.Add(Me.lbl2)
        Me.Panel2.Controls.Add(Me.dgvGK2)
        Me.Panel2.Controls.Add(Me.chkBH2)
        Me.Panel2.Location = New System.Drawing.Point(254, 40)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(237, 476)
        Me.Panel2.TabIndex = 5
        '
        'lblG2
        '
        Me.lblG2.BackColor = System.Drawing.Color.SteelBlue
        Me.lblG2.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblG2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblG2.ForeColor = System.Drawing.Color.White
        Me.lblG2.Location = New System.Drawing.Point(0, 0)
        Me.lblG2.Name = "lblG2"
        Me.lblG2.Size = New System.Drawing.Size(233, 29)
        Me.lblG2.TabIndex = 219
        Me.lblG2.Text = "Leicht"
        Me.lblG2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.Label28)
        Me.Panel12.Controls.Add(Me.dateA2)
        Me.Panel12.Controls.Add(Me.pnl2)
        Me.Panel12.Controls.Add(Me.timeA2)
        Me.Panel12.Controls.Add(Me.Label18)
        Me.Panel12.Controls.Add(Me.Label8)
        Me.Panel12.Controls.Add(Me.Label9)
        Me.Panel12.Controls.Add(Me.timeR2)
        Me.Panel12.Controls.Add(Me.date2)
        Me.Panel12.Controls.Add(Me.Label2)
        Me.Panel12.Controls.Add(Me.timeS2)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel12.Location = New System.Drawing.Point(0, 374)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(233, 98)
        Me.Panel12.TabIndex = 218
        '
        'Label28
        '
        Me.Label28.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        Me.Label28.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label28.Location = New System.Drawing.Point(4, 52)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(26, 13)
        Me.Label28.TabIndex = 223
        Me.Label28.Text = "Tag"
        '
        'dateA2
        '
        Me.dateA2.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.dateA2.Checked = False
        Me.dateA2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateA2.Location = New System.Drawing.Point(7, 68)
        Me.dateA2.Name = "dateA2"
        Me.dateA2.ShowUpDown = True
        Me.dateA2.Size = New System.Drawing.Size(80, 20)
        Me.dateA2.TabIndex = 18
        '
        'pnl2
        '
        Me.pnl2.BackColor = System.Drawing.Color.SteelBlue
        Me.pnl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl2.Controls.Add(Me.Label22)
        Me.pnl2.Controls.Add(Me.cboB2)
        Me.pnl2.Location = New System.Drawing.Point(171, 49)
        Me.pnl2.Name = "pnl2"
        Me.pnl2.Size = New System.Drawing.Size(61, 48)
        Me.pnl2.TabIndex = 221
        '
        'Label22
        '
        Me.Label22.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label22.AutoSize = True
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(13, 4)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(34, 13)
        Me.Label22.TabIndex = 216
        Me.Label22.Text = "Bohle"
        '
        'cboB2
        '
        Me.cboB2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboB2.FormattingEnabled = True
        Me.cboB2.Items.AddRange(New Object() {"1", "2"})
        Me.cboB2.Location = New System.Drawing.Point(16, 19)
        Me.cboB2.Name = "cboB2"
        Me.cboB2.Size = New System.Drawing.Size(30, 21)
        Me.cboB2.TabIndex = 20
        '
        'timeA2
        '
        Me.timeA2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeA2.Checked = False
        Me.timeA2.CustomFormat = "HH:mm"
        Me.timeA2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeA2.Location = New System.Drawing.Point(101, 68)
        Me.timeA2.Name = "timeA2"
        Me.timeA2.ShowUpDown = True
        Me.timeA2.Size = New System.Drawing.Size(56, 20)
        Me.timeA2.TabIndex = 19
        '
        'Label18
        '
        Me.Label18.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(98, 52)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(42, 13)
        Me.Label18.TabIndex = 211
        Me.Label18.Text = "Athletik"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(168, 6)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 13)
        Me.Label8.TabIndex = 43
        Me.Label8.Text = "Stoßen"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(98, 6)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(41, 13)
        Me.Label9.TabIndex = 42
        Me.Label9.Text = "Reißen"
        '
        'timeR2
        '
        Me.timeR2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeR2.Checked = False
        Me.timeR2.CustomFormat = "HH:mm"
        Me.timeR2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeR2.Location = New System.Drawing.Point(101, 22)
        Me.timeR2.Name = "timeR2"
        Me.timeR2.ShowUpDown = True
        Me.timeR2.Size = New System.Drawing.Size(56, 20)
        Me.timeR2.TabIndex = 16
        '
        'date2
        '
        Me.date2.Checked = False
        Me.date2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date2.Location = New System.Drawing.Point(7, 22)
        Me.date2.Name = "date2"
        Me.date2.ShowUpDown = True
        Me.date2.Size = New System.Drawing.Size(80, 20)
        Me.date2.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(4, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(26, 13)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Tag"
        '
        'timeS2
        '
        Me.timeS2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeS2.Checked = False
        Me.timeS2.CustomFormat = "HH:mm"
        Me.timeS2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeS2.Location = New System.Drawing.Point(171, 22)
        Me.timeS2.Name = "timeS2"
        Me.timeS2.ShowUpDown = True
        Me.timeS2.Size = New System.Drawing.Size(56, 20)
        Me.timeS2.TabIndex = 17
        '
        'cbo2
        '
        Me.cbo2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo2.DropDownWidth = 35
        Me.cbo2.FormattingEnabled = True
        Me.cbo2.Location = New System.Drawing.Point(55, 34)
        Me.cbo2.MaxDropDownItems = 6
        Me.cbo2.Name = "cbo2"
        Me.cbo2.Size = New System.Drawing.Size(41, 21)
        Me.cbo2.TabIndex = 12
        '
        'lbl2
        '
        Me.lbl2.AutoSize = True
        Me.lbl2.BackColor = System.Drawing.Color.Transparent
        Me.lbl2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl2.Location = New System.Drawing.Point(10, 36)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(42, 13)
        Me.lbl2.TabIndex = 28
        Me.lbl2.Text = "Gruppe"
        Me.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvGK2
        '
        Me.dgvGK2.AllowDrop = True
        Me.dgvGK2.AllowUserToAddRows = False
        Me.dgvGK2.AllowUserToDeleteRows = False
        Me.dgvGK2.AllowUserToOrderColumns = True
        Me.dgvGK2.AllowUserToResizeColumns = False
        Me.dgvGK2.AllowUserToResizeRows = False
        Me.dgvGK2.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvGK2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGK2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvGK2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGK2.Location = New System.Drawing.Point(1, 60)
        Me.dgvGK2.Name = "dgvGK2"
        Me.dgvGK2.ReadOnly = True
        Me.dgvGK2.RowHeadersVisible = False
        Me.dgvGK2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGK2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGK2.ShowEditingIcon = False
        Me.dgvGK2.Size = New System.Drawing.Size(231, 309)
        Me.dgvGK2.TabIndex = 14
        '
        'chkBH2
        '
        Me.chkBH2.AutoSize = True
        Me.chkBH2.Location = New System.Drawing.Point(141, 36)
        Me.chkBH2.Name = "chkBH2"
        Me.chkBH2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkBH2.Size = New System.Drawing.Size(83, 17)
        Me.chkBH2.TabIndex = 13
        Me.chkBH2.Text = "Blockheben"
        Me.chkBH2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkBH2.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel3.Controls.Add(Me.lblG3)
        Me.Panel3.Controls.Add(Me.Panel13)
        Me.Panel3.Controls.Add(Me.cbo3)
        Me.Panel3.Controls.Add(Me.lbl3)
        Me.Panel3.Controls.Add(Me.dgvGK3)
        Me.Panel3.Controls.Add(Me.chkBH3)
        Me.Panel3.Location = New System.Drawing.Point(497, 40)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(237, 476)
        Me.Panel3.TabIndex = 6
        '
        'lblG3
        '
        Me.lblG3.BackColor = System.Drawing.Color.SteelBlue
        Me.lblG3.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblG3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblG3.ForeColor = System.Drawing.Color.White
        Me.lblG3.Location = New System.Drawing.Point(0, 0)
        Me.lblG3.Name = "lblG3"
        Me.lblG3.Size = New System.Drawing.Size(233, 29)
        Me.lblG3.TabIndex = 221
        Me.lblG3.Text = "Mittel"
        Me.lblG3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.pnl3)
        Me.Panel13.Controls.Add(Me.Label26)
        Me.Panel13.Controls.Add(Me.dateA3)
        Me.Panel13.Controls.Add(Me.Label3)
        Me.Panel13.Controls.Add(Me.Label16)
        Me.Panel13.Controls.Add(Me.timeA3)
        Me.Panel13.Controls.Add(Me.Label10)
        Me.Panel13.Controls.Add(Me.Label11)
        Me.Panel13.Controls.Add(Me.timeR3)
        Me.Panel13.Controls.Add(Me.date3)
        Me.Panel13.Controls.Add(Me.timeS3)
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel13.Location = New System.Drawing.Point(0, 374)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(233, 98)
        Me.Panel13.TabIndex = 220
        '
        'pnl3
        '
        Me.pnl3.BackColor = System.Drawing.Color.SteelBlue
        Me.pnl3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl3.Controls.Add(Me.cboB3)
        Me.pnl3.Controls.Add(Me.Label23)
        Me.pnl3.Location = New System.Drawing.Point(171, 49)
        Me.pnl3.Name = "pnl3"
        Me.pnl3.Size = New System.Drawing.Size(61, 48)
        Me.pnl3.TabIndex = 220
        '
        'cboB3
        '
        Me.cboB3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboB3.FormattingEnabled = True
        Me.cboB3.Items.AddRange(New Object() {"1", "2"})
        Me.cboB3.Location = New System.Drawing.Point(16, 19)
        Me.cboB3.Name = "cboB3"
        Me.cboB3.Size = New System.Drawing.Size(30, 21)
        Me.cboB3.TabIndex = 30
        '
        'Label23
        '
        Me.Label23.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(13, 4)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(34, 13)
        Me.Label23.TabIndex = 216
        Me.Label23.Text = "Bohle"
        '
        'Label26
        '
        Me.Label26.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label26.AutoSize = True
        Me.Label26.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label26.Location = New System.Drawing.Point(4, 52)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(26, 13)
        Me.Label26.TabIndex = 219
        Me.Label26.Text = "Tag"
        '
        'dateA3
        '
        Me.dateA3.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.dateA3.Checked = False
        Me.dateA3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateA3.Location = New System.Drawing.Point(7, 68)
        Me.dateA3.Name = "dateA3"
        Me.dateA3.ShowUpDown = True
        Me.dateA3.Size = New System.Drawing.Size(80, 20)
        Me.dateA3.TabIndex = 28
        '
        'Label3
        '
        Me.Label3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(4, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(26, 13)
        Me.Label3.TabIndex = 211
        Me.Label3.Text = "Tag"
        '
        'Label16
        '
        Me.Label16.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(98, 52)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(42, 13)
        Me.Label16.TabIndex = 210
        Me.Label16.Text = "Athletik"
        '
        'timeA3
        '
        Me.timeA3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeA3.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.timeA3.Checked = False
        Me.timeA3.CustomFormat = "HH:mm"
        Me.timeA3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeA3.Location = New System.Drawing.Point(101, 68)
        Me.timeA3.Name = "timeA3"
        Me.timeA3.ShowUpDown = True
        Me.timeA3.Size = New System.Drawing.Size(56, 20)
        Me.timeA3.TabIndex = 29
        '
        'Label10
        '
        Me.Label10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(168, 6)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(41, 13)
        Me.Label10.TabIndex = 208
        Me.Label10.Text = "Stoßen"
        '
        'Label11
        '
        Me.Label11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(98, 6)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(41, 13)
        Me.Label11.TabIndex = 207
        Me.Label11.Text = "Reißen"
        '
        'timeR3
        '
        Me.timeR3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeR3.Checked = False
        Me.timeR3.CustomFormat = "HH:mm"
        Me.timeR3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeR3.Location = New System.Drawing.Point(101, 22)
        Me.timeR3.Name = "timeR3"
        Me.timeR3.ShowUpDown = True
        Me.timeR3.Size = New System.Drawing.Size(56, 20)
        Me.timeR3.TabIndex = 26
        '
        'date3
        '
        Me.date3.Checked = False
        Me.date3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date3.Location = New System.Drawing.Point(7, 22)
        Me.date3.Name = "date3"
        Me.date3.ShowUpDown = True
        Me.date3.Size = New System.Drawing.Size(80, 20)
        Me.date3.TabIndex = 25
        '
        'timeS3
        '
        Me.timeS3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeS3.Checked = False
        Me.timeS3.CustomFormat = "HH:mm"
        Me.timeS3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeS3.Location = New System.Drawing.Point(171, 22)
        Me.timeS3.Name = "timeS3"
        Me.timeS3.ShowUpDown = True
        Me.timeS3.Size = New System.Drawing.Size(56, 20)
        Me.timeS3.TabIndex = 27
        '
        'cbo3
        '
        Me.cbo3.BackColor = System.Drawing.SystemColors.Window
        Me.cbo3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo3.DropDownWidth = 35
        Me.cbo3.FormattingEnabled = True
        Me.cbo3.Location = New System.Drawing.Point(50, 34)
        Me.cbo3.MaxDropDownItems = 6
        Me.cbo3.Name = "cbo3"
        Me.cbo3.Size = New System.Drawing.Size(41, 21)
        Me.cbo3.TabIndex = 22
        '
        'lbl3
        '
        Me.lbl3.AutoSize = True
        Me.lbl3.BackColor = System.Drawing.Color.Transparent
        Me.lbl3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl3.Location = New System.Drawing.Point(5, 36)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(42, 13)
        Me.lbl3.TabIndex = 201
        Me.lbl3.Text = "Gruppe"
        Me.lbl3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvGK3
        '
        Me.dgvGK3.AllowDrop = True
        Me.dgvGK3.AllowUserToAddRows = False
        Me.dgvGK3.AllowUserToDeleteRows = False
        Me.dgvGK3.AllowUserToOrderColumns = True
        Me.dgvGK3.AllowUserToResizeColumns = False
        Me.dgvGK3.AllowUserToResizeRows = False
        Me.dgvGK3.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvGK3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGK3.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvGK3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGK3.Location = New System.Drawing.Point(1, 60)
        Me.dgvGK3.Name = "dgvGK3"
        Me.dgvGK3.ReadOnly = True
        Me.dgvGK3.RowHeadersVisible = False
        Me.dgvGK3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGK3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGK3.ShowEditingIcon = False
        Me.dgvGK3.Size = New System.Drawing.Size(231, 309)
        Me.dgvGK3.TabIndex = 24
        '
        'chkBH3
        '
        Me.chkBH3.AutoSize = True
        Me.chkBH3.Location = New System.Drawing.Point(141, 36)
        Me.chkBH3.Name = "chkBH3"
        Me.chkBH3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkBH3.Size = New System.Drawing.Size(83, 17)
        Me.chkBH3.TabIndex = 23
        Me.chkBH3.Text = "Blockheben"
        Me.chkBH3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkBH3.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel4.Controls.Add(Me.lblG4)
        Me.Panel4.Controls.Add(Me.Panel14)
        Me.Panel4.Controls.Add(Me.cbo4)
        Me.Panel4.Controls.Add(Me.lbl4)
        Me.Panel4.Controls.Add(Me.dgvGK4)
        Me.Panel4.Controls.Add(Me.chkBH4)
        Me.Panel4.Location = New System.Drawing.Point(740, 40)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(237, 476)
        Me.Panel4.TabIndex = 13
        '
        'lblG4
        '
        Me.lblG4.BackColor = System.Drawing.Color.SteelBlue
        Me.lblG4.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblG4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblG4.ForeColor = System.Drawing.Color.White
        Me.lblG4.Location = New System.Drawing.Point(0, 0)
        Me.lblG4.Name = "lblG4"
        Me.lblG4.Size = New System.Drawing.Size(233, 29)
        Me.lblG4.TabIndex = 219
        Me.lblG4.Text = "Halbschwer"
        Me.lblG4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.Label29)
        Me.Panel14.Controls.Add(Me.dateA4)
        Me.Panel14.Controls.Add(Me.pnl4)
        Me.Panel14.Controls.Add(Me.timeA4)
        Me.Panel14.Controls.Add(Me.Label19)
        Me.Panel14.Controls.Add(Me.Label12)
        Me.Panel14.Controls.Add(Me.Label13)
        Me.Panel14.Controls.Add(Me.timeR4)
        Me.Panel14.Controls.Add(Me.date4)
        Me.Panel14.Controls.Add(Me.Label4)
        Me.Panel14.Controls.Add(Me.timeS4)
        Me.Panel14.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel14.Location = New System.Drawing.Point(0, 374)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(233, 98)
        Me.Panel14.TabIndex = 218
        '
        'Label29
        '
        Me.Label29.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label29.AutoSize = True
        Me.Label29.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label29.Location = New System.Drawing.Point(4, 52)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(26, 13)
        Me.Label29.TabIndex = 223
        Me.Label29.Text = "Tag"
        '
        'dateA4
        '
        Me.dateA4.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.dateA4.Checked = False
        Me.dateA4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateA4.Location = New System.Drawing.Point(7, 68)
        Me.dateA4.Name = "dateA4"
        Me.dateA4.ShowUpDown = True
        Me.dateA4.Size = New System.Drawing.Size(80, 20)
        Me.dateA4.TabIndex = 38
        '
        'pnl4
        '
        Me.pnl4.BackColor = System.Drawing.Color.SteelBlue
        Me.pnl4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl4.Controls.Add(Me.cboB4)
        Me.pnl4.Controls.Add(Me.Label24)
        Me.pnl4.Location = New System.Drawing.Point(171, 49)
        Me.pnl4.Name = "pnl4"
        Me.pnl4.Size = New System.Drawing.Size(61, 48)
        Me.pnl4.TabIndex = 221
        '
        'cboB4
        '
        Me.cboB4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboB4.FormattingEnabled = True
        Me.cboB4.Items.AddRange(New Object() {"1", "2"})
        Me.cboB4.Location = New System.Drawing.Point(16, 19)
        Me.cboB4.Name = "cboB4"
        Me.cboB4.Size = New System.Drawing.Size(30, 21)
        Me.cboB4.TabIndex = 40
        '
        'Label24
        '
        Me.Label24.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label24.AutoSize = True
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(13, 4)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(34, 13)
        Me.Label24.TabIndex = 216
        Me.Label24.Text = "Bohle"
        '
        'timeA4
        '
        Me.timeA4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeA4.Checked = False
        Me.timeA4.CustomFormat = "HH:mm"
        Me.timeA4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeA4.Location = New System.Drawing.Point(101, 68)
        Me.timeA4.Name = "timeA4"
        Me.timeA4.ShowUpDown = True
        Me.timeA4.Size = New System.Drawing.Size(55, 20)
        Me.timeA4.TabIndex = 39
        '
        'Label19
        '
        Me.Label19.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(98, 52)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(42, 13)
        Me.Label19.TabIndex = 211
        Me.Label19.Text = "Athletik"
        '
        'Label12
        '
        Me.Label12.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(168, 6)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(41, 13)
        Me.Label12.TabIndex = 208
        Me.Label12.Text = "Stoßen"
        '
        'Label13
        '
        Me.Label13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(98, 6)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(41, 13)
        Me.Label13.TabIndex = 207
        Me.Label13.Text = "Reißen"
        '
        'timeR4
        '
        Me.timeR4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeR4.Checked = False
        Me.timeR4.CustomFormat = "HH:mm"
        Me.timeR4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeR4.Location = New System.Drawing.Point(101, 22)
        Me.timeR4.Name = "timeR4"
        Me.timeR4.ShowUpDown = True
        Me.timeR4.Size = New System.Drawing.Size(55, 20)
        Me.timeR4.TabIndex = 36
        '
        'date4
        '
        Me.date4.Checked = False
        Me.date4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date4.Location = New System.Drawing.Point(7, 22)
        Me.date4.Name = "date4"
        Me.date4.ShowUpDown = True
        Me.date4.Size = New System.Drawing.Size(80, 20)
        Me.date4.TabIndex = 35
        '
        'Label4
        '
        Me.Label4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(4, 6)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(26, 13)
        Me.Label4.TabIndex = 204
        Me.Label4.Text = "Tag"
        '
        'timeS4
        '
        Me.timeS4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeS4.Checked = False
        Me.timeS4.CustomFormat = "HH:mm"
        Me.timeS4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeS4.Location = New System.Drawing.Point(171, 22)
        Me.timeS4.Name = "timeS4"
        Me.timeS4.ShowUpDown = True
        Me.timeS4.Size = New System.Drawing.Size(55, 20)
        Me.timeS4.TabIndex = 37
        '
        'cbo4
        '
        Me.cbo4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo4.DropDownWidth = 35
        Me.cbo4.FormattingEnabled = True
        Me.cbo4.Location = New System.Drawing.Point(55, 34)
        Me.cbo4.MaxDropDownItems = 6
        Me.cbo4.Name = "cbo4"
        Me.cbo4.Size = New System.Drawing.Size(41, 21)
        Me.cbo4.TabIndex = 32
        '
        'lbl4
        '
        Me.lbl4.AutoSize = True
        Me.lbl4.BackColor = System.Drawing.Color.Transparent
        Me.lbl4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl4.Location = New System.Drawing.Point(10, 36)
        Me.lbl4.Name = "lbl4"
        Me.lbl4.Size = New System.Drawing.Size(42, 13)
        Me.lbl4.TabIndex = 201
        Me.lbl4.Text = "Gruppe"
        Me.lbl4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvGK4
        '
        Me.dgvGK4.AllowDrop = True
        Me.dgvGK4.AllowUserToAddRows = False
        Me.dgvGK4.AllowUserToDeleteRows = False
        Me.dgvGK4.AllowUserToOrderColumns = True
        Me.dgvGK4.AllowUserToResizeColumns = False
        Me.dgvGK4.AllowUserToResizeRows = False
        Me.dgvGK4.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvGK4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGK4.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvGK4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGK4.Location = New System.Drawing.Point(1, 60)
        Me.dgvGK4.Name = "dgvGK4"
        Me.dgvGK4.ReadOnly = True
        Me.dgvGK4.RowHeadersVisible = False
        Me.dgvGK4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGK4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGK4.ShowEditingIcon = False
        Me.dgvGK4.Size = New System.Drawing.Size(231, 309)
        Me.dgvGK4.TabIndex = 34
        '
        'chkBH4
        '
        Me.chkBH4.AutoSize = True
        Me.chkBH4.Location = New System.Drawing.Point(141, 36)
        Me.chkBH4.Name = "chkBH4"
        Me.chkBH4.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkBH4.Size = New System.Drawing.Size(83, 17)
        Me.chkBH4.TabIndex = 33
        Me.chkBH4.Text = "Blockheben"
        Me.chkBH4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkBH4.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel5.Controls.Add(Me.lblG5)
        Me.Panel5.Controls.Add(Me.Panel15)
        Me.Panel5.Controls.Add(Me.cbo5)
        Me.Panel5.Controls.Add(Me.lbl5)
        Me.Panel5.Controls.Add(Me.dgvGK5)
        Me.Panel5.Controls.Add(Me.chkBH5)
        Me.Panel5.Location = New System.Drawing.Point(983, 40)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(237, 476)
        Me.Panel5.TabIndex = 0
        '
        'lblG5
        '
        Me.lblG5.BackColor = System.Drawing.Color.SteelBlue
        Me.lblG5.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblG5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lblG5.ForeColor = System.Drawing.Color.White
        Me.lblG5.Location = New System.Drawing.Point(0, 0)
        Me.lblG5.Name = "lblG5"
        Me.lblG5.Size = New System.Drawing.Size(233, 29)
        Me.lblG5.TabIndex = 219
        Me.lblG5.Text = "Schwer"
        Me.lblG5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel15
        '
        Me.Panel15.Controls.Add(Me.Label30)
        Me.Panel15.Controls.Add(Me.dateA5)
        Me.Panel15.Controls.Add(Me.pnl5)
        Me.Panel15.Controls.Add(Me.timeA5)
        Me.Panel15.Controls.Add(Me.Label20)
        Me.Panel15.Controls.Add(Me.Label14)
        Me.Panel15.Controls.Add(Me.Label15)
        Me.Panel15.Controls.Add(Me.timeR5)
        Me.Panel15.Controls.Add(Me.date5)
        Me.Panel15.Controls.Add(Me.Label5)
        Me.Panel15.Controls.Add(Me.timeS5)
        Me.Panel15.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel15.Location = New System.Drawing.Point(0, 374)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(233, 98)
        Me.Panel15.TabIndex = 218
        '
        'Label30
        '
        Me.Label30.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label30.AutoSize = True
        Me.Label30.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label30.Location = New System.Drawing.Point(4, 52)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(26, 13)
        Me.Label30.TabIndex = 223
        Me.Label30.Text = "Tag"
        '
        'dateA5
        '
        Me.dateA5.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText
        Me.dateA5.Checked = False
        Me.dateA5.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateA5.Location = New System.Drawing.Point(7, 68)
        Me.dateA5.Name = "dateA5"
        Me.dateA5.ShowUpDown = True
        Me.dateA5.Size = New System.Drawing.Size(80, 20)
        Me.dateA5.TabIndex = 48
        '
        'pnl5
        '
        Me.pnl5.BackColor = System.Drawing.Color.SteelBlue
        Me.pnl5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl5.Controls.Add(Me.cboB5)
        Me.pnl5.Controls.Add(Me.Label25)
        Me.pnl5.Location = New System.Drawing.Point(171, 49)
        Me.pnl5.Name = "pnl5"
        Me.pnl5.Size = New System.Drawing.Size(61, 48)
        Me.pnl5.TabIndex = 221
        '
        'cboB5
        '
        Me.cboB5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboB5.FormattingEnabled = True
        Me.cboB5.Items.AddRange(New Object() {"1", "2"})
        Me.cboB5.Location = New System.Drawing.Point(16, 19)
        Me.cboB5.Name = "cboB5"
        Me.cboB5.Size = New System.Drawing.Size(30, 21)
        Me.cboB5.TabIndex = 50
        '
        'Label25
        '
        Me.Label25.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label25.AutoSize = True
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(13, 4)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(34, 13)
        Me.Label25.TabIndex = 216
        Me.Label25.Text = "Bohle"
        '
        'timeA5
        '
        Me.timeA5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeA5.Checked = False
        Me.timeA5.CustomFormat = "HH:mm"
        Me.timeA5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeA5.Location = New System.Drawing.Point(101, 68)
        Me.timeA5.Name = "timeA5"
        Me.timeA5.ShowUpDown = True
        Me.timeA5.Size = New System.Drawing.Size(53, 20)
        Me.timeA5.TabIndex = 49
        '
        'Label20
        '
        Me.Label20.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(98, 52)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(42, 13)
        Me.Label20.TabIndex = 211
        Me.Label20.Text = "Athletik"
        '
        'Label14
        '
        Me.Label14.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(168, 6)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(41, 13)
        Me.Label14.TabIndex = 210
        Me.Label14.Text = "Stoßen"
        '
        'Label15
        '
        Me.Label15.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(98, 6)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(41, 13)
        Me.Label15.TabIndex = 209
        Me.Label15.Text = "Reißen"
        '
        'timeR5
        '
        Me.timeR5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeR5.Checked = False
        Me.timeR5.CustomFormat = "HH:mm"
        Me.timeR5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeR5.Location = New System.Drawing.Point(101, 22)
        Me.timeR5.Name = "timeR5"
        Me.timeR5.ShowUpDown = True
        Me.timeR5.Size = New System.Drawing.Size(53, 20)
        Me.timeR5.TabIndex = 46
        '
        'date5
        '
        Me.date5.Checked = False
        Me.date5.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date5.Location = New System.Drawing.Point(7, 22)
        Me.date5.Name = "date5"
        Me.date5.ShowUpDown = True
        Me.date5.Size = New System.Drawing.Size(80, 20)
        Me.date5.TabIndex = 45
        '
        'Label5
        '
        Me.Label5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(4, 6)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(26, 13)
        Me.Label5.TabIndex = 206
        Me.Label5.Text = "Tag"
        '
        'timeS5
        '
        Me.timeS5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timeS5.Checked = False
        Me.timeS5.CustomFormat = "HH:mm"
        Me.timeS5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.timeS5.Location = New System.Drawing.Point(171, 22)
        Me.timeS5.Name = "timeS5"
        Me.timeS5.ShowUpDown = True
        Me.timeS5.Size = New System.Drawing.Size(53, 20)
        Me.timeS5.TabIndex = 47
        '
        'cbo5
        '
        Me.cbo5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbo5.DropDownWidth = 35
        Me.cbo5.FormattingEnabled = True
        Me.cbo5.Location = New System.Drawing.Point(55, 34)
        Me.cbo5.MaxDropDownItems = 6
        Me.cbo5.Name = "cbo5"
        Me.cbo5.Size = New System.Drawing.Size(41, 21)
        Me.cbo5.TabIndex = 42
        '
        'lbl5
        '
        Me.lbl5.AutoSize = True
        Me.lbl5.BackColor = System.Drawing.Color.Transparent
        Me.lbl5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl5.Location = New System.Drawing.Point(10, 36)
        Me.lbl5.Name = "lbl5"
        Me.lbl5.Size = New System.Drawing.Size(42, 13)
        Me.lbl5.TabIndex = 203
        Me.lbl5.Text = "Gruppe"
        Me.lbl5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvGK5
        '
        Me.dgvGK5.AllowDrop = True
        Me.dgvGK5.AllowUserToAddRows = False
        Me.dgvGK5.AllowUserToDeleteRows = False
        Me.dgvGK5.AllowUserToOrderColumns = True
        Me.dgvGK5.AllowUserToResizeColumns = False
        Me.dgvGK5.AllowUserToResizeRows = False
        Me.dgvGK5.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvGK5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGK5.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvGK5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGK5.Location = New System.Drawing.Point(1, 60)
        Me.dgvGK5.Name = "dgvGK5"
        Me.dgvGK5.ReadOnly = True
        Me.dgvGK5.RowHeadersVisible = False
        Me.dgvGK5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGK5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGK5.ShowEditingIcon = False
        Me.dgvGK5.Size = New System.Drawing.Size(231, 309)
        Me.dgvGK5.TabIndex = 44
        '
        'chkBH5
        '
        Me.chkBH5.AutoSize = True
        Me.chkBH5.Location = New System.Drawing.Point(141, 36)
        Me.chkBH5.Name = "chkBH5"
        Me.chkBH5.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkBH5.Size = New System.Drawing.Size(83, 17)
        Me.chkBH5.TabIndex = 43
        Me.chkBH5.Text = "Blockheben"
        Me.chkBH5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkBH5.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.MenuBar
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuExtras, Me.lblGruppe, Me.cboGruppe, Me.lblJahrgang, Me.cboJahrgang, Me.lblGeschlecht, Me.cboGeschlecht})
        Me.MenuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.MenuStrip1.Size = New System.Drawing.Size(1232, 27)
        Me.MenuStrip1.TabIndex = 199
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSpeichern, Me.mnuDrucken, Me.ToolStripMenuItem1, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 23)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuSpeichern
        '
        Me.mnuSpeichern.Name = "mnuSpeichern"
        Me.mnuSpeichern.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSpeichern.Size = New System.Drawing.Size(168, 22)
        Me.mnuSpeichern.Text = "&Speichern"
        '
        'mnuDrucken
        '
        Me.mnuDrucken.Name = "mnuDrucken"
        Me.mnuDrucken.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.mnuDrucken.Size = New System.Drawing.Size(168, 22)
        Me.mnuDrucken.Text = "&Drucken"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(165, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(168, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLayout, Me.ToolStripMenuItem2, Me.mnuMeldungen, Me.mnuKonflikte})
        Me.mnuExtras.Name = "mnuExtras"
        Me.mnuExtras.Size = New System.Drawing.Size(49, 23)
        Me.mnuExtras.Text = "&Extras"
        '
        'mnuLayout
        '
        Me.mnuLayout.Name = "mnuLayout"
        Me.mnuLayout.Size = New System.Drawing.Size(210, 22)
        Me.mnuLayout.Text = "&Tabellen-Layout"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(207, 6)
        '
        'mnuMeldungen
        '
        Me.mnuMeldungen.CheckOnClick = True
        Me.mnuMeldungen.Name = "mnuMeldungen"
        Me.mnuMeldungen.Size = New System.Drawing.Size(210, 22)
        Me.mnuMeldungen.Text = "&Meldungen ausblenden"
        '
        'mnuKonflikte
        '
        Me.mnuKonflikte.Checked = True
        Me.mnuKonflikte.CheckOnClick = True
        Me.mnuKonflikte.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuKonflikte.Name = "mnuKonflikte"
        Me.mnuKonflikte.Size = New System.Drawing.Size(210, 22)
        Me.mnuKonflikte.Text = "bei &Konflikten nachfragen"
        '
        'lblGruppe
        '
        Me.lblGruppe.BackColor = System.Drawing.SystemColors.MenuBar
        Me.lblGruppe.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblGruppe.ForeColor = System.Drawing.Color.SeaGreen
        Me.lblGruppe.MergeAction = System.Windows.Forms.MergeAction.Insert
        Me.lblGruppe.Name = "lblGruppe"
        Me.lblGruppe.ReadOnly = True
        Me.lblGruppe.Size = New System.Drawing.Size(375, 23)
        Me.lblGruppe.Text = "Gruppen (gespeichert)  "
        Me.lblGruppe.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboGruppe
        '
        Me.cboGruppe.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboGruppe.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboGruppe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGruppe.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cboGruppe.Name = "cboGruppe"
        Me.cboGruppe.Size = New System.Drawing.Size(80, 23)
        '
        'lblJahrgang
        '
        Me.lblJahrgang.BackColor = System.Drawing.SystemColors.MenuBar
        Me.lblJahrgang.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblJahrgang.ForeColor = System.Drawing.Color.SeaGreen
        Me.lblJahrgang.Name = "lblJahrgang"
        Me.lblJahrgang.ReadOnly = True
        Me.lblJahrgang.Size = New System.Drawing.Size(95, 23)
        Me.lblJahrgang.Text = "Jahrgang  "
        Me.lblJahrgang.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboJahrgang
        '
        Me.cboJahrgang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboJahrgang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboJahrgang.DropDownHeight = 1
        Me.cboJahrgang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJahrgang.DropDownWidth = 50
        Me.cboJahrgang.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cboJahrgang.IntegralHeight = False
        Me.cboJahrgang.Items.AddRange(New Object() {" "})
        Me.cboJahrgang.MaxDropDownItems = 1
        Me.cboJahrgang.Name = "cboJahrgang"
        Me.cboJahrgang.Size = New System.Drawing.Size(80, 23)
        '
        'lblGeschlecht
        '
        Me.lblGeschlecht.BackColor = System.Drawing.SystemColors.MenuBar
        Me.lblGeschlecht.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblGeschlecht.ForeColor = System.Drawing.Color.SeaGreen
        Me.lblGeschlecht.Name = "lblGeschlecht"
        Me.lblGeschlecht.ReadOnly = True
        Me.lblGeschlecht.Size = New System.Drawing.Size(100, 23)
        Me.lblGeschlecht.Text = "Geschlecht  "
        Me.lblGeschlecht.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboGeschlecht
        '
        Me.cboGeschlecht.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGeschlecht.FlatStyle = System.Windows.Forms.FlatStyle.Standard
        Me.cboGeschlecht.Items.AddRange(New Object() {"weiblich", "männlich", "gemischt"})
        Me.cboGeschlecht.Name = "cboGeschlecht"
        Me.cboGeschlecht.Size = New System.Drawing.Size(80, 23)
        '
        'bgwGK
        '
        Me.bgwGK.WorkerReportsProgress = True
        '
        'Panel6
        '
        Me.Panel6.Location = New System.Drawing.Point(1222, 56)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(10, 23)
        Me.Panel6.TabIndex = 200
        '
        'pnlForm
        '
        Me.pnlForm.AutoScroll = True
        Me.pnlForm.Controls.Add(Me.Panel6)
        Me.pnlForm.Controls.Add(Me.Panel5)
        Me.pnlForm.Controls.Add(Me.Panel4)
        Me.pnlForm.Controls.Add(Me.Panel3)
        Me.pnlForm.Controls.Add(Me.Panel2)
        Me.pnlForm.Controls.Add(Me.Panel1)
        Me.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlForm.Location = New System.Drawing.Point(0, 0)
        Me.pnlForm.Name = "pnlForm"
        Me.pnlForm.Size = New System.Drawing.Size(1232, 529)
        Me.pnlForm.TabIndex = 201
        '
        'lstJahrgang
        '
        Me.lstJahrgang.CheckOnClick = True
        Me.lstJahrgang.Location = New System.Drawing.Point(658, 25)
        Me.lstJahrgang.Name = "lstJahrgang"
        Me.lstJahrgang.Size = New System.Drawing.Size(80, 49)
        Me.lstJahrgang.TabIndex = 203
        Me.lstJahrgang.TabStop = False
        Me.lstJahrgang.Visible = False
        '
        'frmGruppenJahrgang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.ClientSize = New System.Drawing.Size(1232, 529)
        Me.Controls.Add(Me.lstJahrgang)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.pnlForm)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGruppenJahrgang"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Gruppen einteilen"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.pnl1.ResumeLayout(False)
        Me.pnl1.PerformLayout()
        CType(Me.dgvGK1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.pnl2.ResumeLayout(False)
        Me.pnl2.PerformLayout()
        CType(Me.dgvGK2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.pnl3.ResumeLayout(False)
        Me.pnl3.PerformLayout()
        CType(Me.dgvGK3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.pnl4.ResumeLayout(False)
        Me.pnl4.PerformLayout()
        CType(Me.dgvGK4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        Me.pnl5.ResumeLayout(False)
        Me.pnl5.PerformLayout()
        CType(Me.dgvGK5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.pnlForm.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkBH1 As System.Windows.Forms.CheckBox
    Friend WithEvents dgvGK1 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgvGK2 As System.Windows.Forms.DataGridView
    Friend WithEvents chkBH2 As System.Windows.Forms.CheckBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents dgvGK3 As System.Windows.Forms.DataGridView
    Friend WithEvents chkBH3 As System.Windows.Forms.CheckBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents dgvGK4 As System.Windows.Forms.DataGridView
    Friend WithEvents chkBH4 As System.Windows.Forms.CheckBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents dgvGK5 As System.Windows.Forms.DataGridView
    Friend WithEvents chkBH5 As System.Windows.Forms.CheckBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDatei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeichern As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuBeenden As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtras As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLayout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblGruppe As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents lblJahrgang As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents cboJahrgang As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents lblGeschlecht As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents bgwGK As System.ComponentModel.BackgroundWorker
    Friend WithEvents lbl2 As System.Windows.Forms.Label
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents lbl3 As System.Windows.Forms.Label
    Friend WithEvents lbl4 As System.Windows.Forms.Label
    Friend WithEvents lbl5 As System.Windows.Forms.Label
    Friend WithEvents cbo2 As System.Windows.Forms.ComboBox
    Friend WithEvents cbo1 As System.Windows.Forms.ComboBox
    Friend WithEvents cbo3 As System.Windows.Forms.ComboBox
    Friend WithEvents cbo4 As System.Windows.Forms.ComboBox
    Friend WithEvents cbo5 As System.Windows.Forms.ComboBox
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuMeldungen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuKonflikte As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents timeS1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents date1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents timeS2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents timeS3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents timeS4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents date5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents timeS5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents timeR1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents timeR2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents timeR3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents timeR4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents timeR5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents timeA3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents timeA1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents timeA2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents timeA4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents timeA5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cboB1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboB2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents cboB3 As System.Windows.Forms.ComboBox
    Friend WithEvents cboB4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents cboB5 As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents dateA3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    Friend WithEvents pnl3 As System.Windows.Forms.Panel
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents pnl1 As System.Windows.Forms.Panel
    Friend WithEvents pnl2 As System.Windows.Forms.Panel
    Friend WithEvents pnl4 As System.Windows.Forms.Panel
    Friend WithEvents pnl5 As System.Windows.Forms.Panel
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents dateA1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents dateA2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents dateA4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents dateA5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnlForm As System.Windows.Forms.Panel
    Friend WithEvents lstJahrgang As System.Windows.Forms.CheckedListBox
    Friend WithEvents cboGruppe As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents cboGeschlecht As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents lblG1 As System.Windows.Forms.Label
    Friend WithEvents lblG2 As System.Windows.Forms.Label
    Friend WithEvents lblG3 As System.Windows.Forms.Label
    Friend WithEvents lblG4 As System.Windows.Forms.Label
    Friend WithEvents lblG5 As System.Windows.Forms.Label
    Friend WithEvents mnuDrucken As ToolStripMenuItem
End Class
