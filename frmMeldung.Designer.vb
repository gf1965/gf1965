﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMeldung
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMeldung))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.lblAnzahl = New System.Windows.Forms.Label()
        Me.btnMeldung_Verein = New System.Windows.Forms.Button()
        Me.btnMeldung_Name = New System.Windows.Forms.Button()
        Me.txtMeldung_Verein = New System.Windows.Forms.TextBox()
        Me.txtMeldung_Name = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dgvJoin = New System.Windows.Forms.DataGridView()
        Me.ContextMenuGrid = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmnuNeu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmnuHeber = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmnuVerein = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmnuLöschen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuÜbernehmen = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckLokaleDB = New System.Windows.Forms.CheckBox()
        Me.pgbCheckAll = New System.Windows.Forms.ProgressBar()
        Me.chkAlle_Athlet = New System.Windows.Forms.CheckBox()
        Me.btnSpeichern = New System.Windows.Forms.Button()
        Me.btnBeenden = New System.Windows.Forms.Button()
        Me.btnAthlet_Übernehmen = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboAK = New System.Windows.Forms.ComboBox()
        Me.txtLosnummer = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.pnlMeldedatum = New System.Windows.Forms.Panel()
        Me.chkMeldedatum = New System.Windows.Forms.CheckBox()
        Me.txtMeldedatum = New System.Windows.Forms.MaskedTextBox()
        Me.btnZurück = New System.Windows.Forms.Button()
        Me.btnWeiter = New System.Windows.Forms.Button()
        Me.cboVereinsmannschaft = New System.Windows.Forms.ComboBox()
        Me.lblVereinsmannschaft = New System.Windows.Forms.Label()
        Me.cboGK = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkDatumMerken = New System.Windows.Forms.CheckBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtStossen = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtReissen = New System.Windows.Forms.TextBox()
        Me.chkLändermannschaft = New System.Windows.Forms.CheckBox()
        Me.chkA_K = New System.Windows.Forms.CheckBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtGewicht = New System.Windows.Forms.TextBox()
        Me.txtZweikampf = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dpkDatum = New System.Windows.Forms.DateTimePicker()
        Me.txtAthlet_Name = New System.Windows.Forms.TextBox()
        Me.btnAthlet_Verein = New System.Windows.Forms.Button()
        Me.dgvAuswahl = New System.Windows.Forms.DataGridView()
        Me.Übernehmen = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Teilnehmer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nachname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vorname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sex = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jahrgang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Verein = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nation = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtAthlet_Verein = New System.Windows.Forms.TextBox()
        Me.btnAthlet_Name = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSperren = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBearbeiten = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuNeu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuHeber = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuVerein = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuLöschen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuIDs = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem9 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuLayout = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLayoutA = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuNoMessage = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKonflikte = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMauszeiger = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem10 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuLokaleDatenbank = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLosnummer = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataGridViewImageColumn1 = New System.Windows.Forms.DataGridViewImageColumn()
        Me.bsA = New System.Windows.Forms.BindingSource(Me.components)
        Me.bs = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsV = New System.Windows.Forms.BindingSource(Me.components)
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuGrid.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.pnlMeldedatum.SuspendLayout()
        CType(Me.dgvAuswahl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.bsA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 27)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblAnzahl)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnMeldung_Verein)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnMeldung_Name)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtMeldung_Verein)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtMeldung_Name)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.dgvJoin)
        Me.SplitContainer1.Panel1MinSize = 280
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.SplitContainer1.Panel2.Controls.Add(Me.CheckLokaleDB)
        Me.SplitContainer1.Panel2.Controls.Add(Me.pgbCheckAll)
        Me.SplitContainer1.Panel2.Controls.Add(Me.chkAlle_Athlet)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnSpeichern)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnBeenden)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnAthlet_Übernehmen)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtAthlet_Name)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnAthlet_Verein)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgvAuswahl)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtAthlet_Verein)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnAthlet_Name)
        Me.SplitContainer1.Panel2MinSize = 210
        Me.SplitContainer1.Size = New System.Drawing.Size(978, 537)
        Me.SplitContainer1.SplitterDistance = 280
        Me.SplitContainer1.TabIndex = 185
        '
        'lblAnzahl
        '
        Me.lblAnzahl.AutoSize = True
        Me.lblAnzahl.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnzahl.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblAnzahl.Location = New System.Drawing.Point(886, 9)
        Me.lblAnzahl.Name = "lblAnzahl"
        Me.lblAnzahl.Size = New System.Drawing.Size(39, 17)
        Me.lblAnzahl.TabIndex = 26
        Me.lblAnzahl.Text = "TN: "
        Me.lblAnzahl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnMeldung_Verein
        '
        Me.btnMeldung_Verein.Image = CType(resources.GetObject("btnMeldung_Verein.Image"), System.Drawing.Image)
        Me.btnMeldung_Verein.Location = New System.Drawing.Point(497, 6)
        Me.btnMeldung_Verein.Name = "btnMeldung_Verein"
        Me.btnMeldung_Verein.Size = New System.Drawing.Size(23, 23)
        Me.btnMeldung_Verein.TabIndex = 4
        Me.btnMeldung_Verein.TabStop = False
        Me.btnMeldung_Verein.UseVisualStyleBackColor = True
        '
        'btnMeldung_Name
        '
        Me.btnMeldung_Name.Image = CType(resources.GetObject("btnMeldung_Name.Image"), System.Drawing.Image)
        Me.btnMeldung_Name.Location = New System.Drawing.Point(306, 6)
        Me.btnMeldung_Name.Name = "btnMeldung_Name"
        Me.btnMeldung_Name.Size = New System.Drawing.Size(23, 23)
        Me.btnMeldung_Name.TabIndex = 6
        Me.btnMeldung_Name.TabStop = False
        Me.btnMeldung_Name.UseVisualStyleBackColor = True
        '
        'txtMeldung_Verein
        '
        Me.txtMeldung_Verein.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtMeldung_Verein.ForeColor = System.Drawing.Color.DarkGray
        Me.txtMeldung_Verein.Location = New System.Drawing.Point(368, 8)
        Me.txtMeldung_Verein.Name = "txtMeldung_Verein"
        Me.txtMeldung_Verein.Size = New System.Drawing.Size(128, 20)
        Me.txtMeldung_Verein.TabIndex = 3
        Me.txtMeldung_Verein.Tag = "Verein"
        Me.txtMeldung_Verein.Text = "Verein"
        '
        'txtMeldung_Name
        '
        Me.txtMeldung_Name.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtMeldung_Name.ForeColor = System.Drawing.Color.DarkGray
        Me.txtMeldung_Name.Location = New System.Drawing.Point(177, 8)
        Me.txtMeldung_Name.Name = "txtMeldung_Name"
        Me.txtMeldung_Name.Size = New System.Drawing.Size(128, 20)
        Me.txtMeldung_Name.TabIndex = 5
        Me.txtMeldung_Name.Tag = "Nachname"
        Me.txtMeldung_Name.Text = "Nachname"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Firebrick
        Me.Label6.Location = New System.Drawing.Point(15, 9)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(89, 18)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "&Meldungen"
        '
        'dgvJoin
        '
        Me.dgvJoin.AllowUserToAddRows = False
        Me.dgvJoin.AllowUserToDeleteRows = False
        Me.dgvJoin.AllowUserToResizeRows = False
        Me.dgvJoin.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvJoin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvJoin.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvJoin.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvJoin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvJoin.ColumnHeadersHeight = 21
        Me.dgvJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvJoin.ContextMenuStrip = Me.ContextMenuGrid
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvJoin.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvJoin.Location = New System.Drawing.Point(0, 37)
        Me.dgvJoin.MultiSelect = False
        Me.dgvJoin.Name = "dgvJoin"
        Me.dgvJoin.RowHeadersVisible = False
        Me.dgvJoin.RowHeadersWidth = 24
        Me.dgvJoin.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvJoin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvJoin.Size = New System.Drawing.Size(979, 241)
        Me.dgvJoin.StandardTab = True
        Me.dgvJoin.TabIndex = 10
        '
        'ContextMenuGrid
        '
        Me.ContextMenuGrid.BackColor = System.Drawing.SystemColors.Info
        Me.ContextMenuGrid.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuGrid.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmnuNeu, Me.ToolStripMenuItem1, Me.cmnuHeber, Me.cmnuVerein, Me.ToolStripMenuItem6, Me.cmnuLöschen, Me.ToolStripMenuItem8, Me.mnuÜbernehmen})
        Me.ContextMenuGrid.Name = "ContextMenu"
        Me.ContextMenuGrid.ShowImageMargin = False
        Me.ContextMenuGrid.Size = New System.Drawing.Size(199, 132)
        '
        'cmnuNeu
        '
        Me.cmnuNeu.Name = "cmnuNeu"
        Me.cmnuNeu.Size = New System.Drawing.Size(198, 22)
        Me.cmnuNeu.Text = "Neuer Heber"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(195, 6)
        '
        'cmnuHeber
        '
        Me.cmnuHeber.Name = "cmnuHeber"
        Me.cmnuHeber.Size = New System.Drawing.Size(198, 22)
        Me.cmnuHeber.Text = "Stammdaten Heber"
        '
        'cmnuVerein
        '
        Me.cmnuVerein.Name = "cmnuVerein"
        Me.cmnuVerein.Size = New System.Drawing.Size(198, 22)
        Me.cmnuVerein.Text = "Stammdaten Verein"
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(195, 6)
        '
        'cmnuLöschen
        '
        Me.cmnuLöschen.Name = "cmnuLöschen"
        Me.cmnuLöschen.Size = New System.Drawing.Size(198, 22)
        Me.cmnuLöschen.Text = "Löschen"
        Me.cmnuLöschen.ToolTipText = "Automatische Steigerung bestätigen"
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        Me.ToolStripMenuItem8.Size = New System.Drawing.Size(195, 6)
        Me.ToolStripMenuItem8.Visible = False
        '
        'mnuÜbernehmen
        '
        Me.mnuÜbernehmen.ForeColor = System.Drawing.Color.Firebrick
        Me.mnuÜbernehmen.Name = "mnuÜbernehmen"
        Me.mnuÜbernehmen.Size = New System.Drawing.Size(198, 22)
        Me.mnuÜbernehmen.Text = "Übernehmen in anderen WK"
        Me.mnuÜbernehmen.Visible = False
        '
        'CheckLokaleDB
        '
        Me.CheckLokaleDB.AutoSize = True
        Me.CheckLokaleDB.BackColor = System.Drawing.Color.Firebrick
        Me.CheckLokaleDB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckLokaleDB.ForeColor = System.Drawing.Color.White
        Me.CheckLokaleDB.Location = New System.Drawing.Point(18, 8)
        Me.CheckLokaleDB.Name = "CheckLokaleDB"
        Me.CheckLokaleDB.Padding = New System.Windows.Forms.Padding(4, 3, 0, 1)
        Me.CheckLokaleDB.Size = New System.Drawing.Size(114, 21)
        Me.CheckLokaleDB.TabIndex = 112
        Me.CheckLokaleDB.Text = "lokale Datenbank"
        Me.ToolTip1.SetToolTip(Me.CheckLokaleDB, "Athleten aus lokaler oder BVDG-Datenbank anzeigen")
        Me.CheckLokaleDB.UseVisualStyleBackColor = False
        '
        'pgbCheckAll
        '
        Me.pgbCheckAll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pgbCheckAll.Location = New System.Drawing.Point(180, 221)
        Me.pgbCheckAll.Name = "pgbCheckAll"
        Me.pgbCheckAll.Size = New System.Drawing.Size(187, 23)
        Me.pgbCheckAll.Step = 1
        Me.pgbCheckAll.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pgbCheckAll.TabIndex = 111
        Me.pgbCheckAll.Visible = False
        '
        'chkAlle_Athlet
        '
        Me.chkAlle_Athlet.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkAlle_Athlet.AutoCheck = False
        Me.chkAlle_Athlet.AutoSize = True
        Me.chkAlle_Athlet.Location = New System.Drawing.Point(24, 221)
        Me.chkAlle_Athlet.Name = "chkAlle_Athlet"
        Me.chkAlle_Athlet.Size = New System.Drawing.Size(96, 17)
        Me.chkAlle_Athlet.TabIndex = 47
        Me.chkAlle_Athlet.Text = "alle ausw&ählen"
        Me.chkAlle_Athlet.ThreeState = True
        Me.chkAlle_Athlet.UseVisualStyleBackColor = True
        '
        'btnSpeichern
        '
        Me.btnSpeichern.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSpeichern.Location = New System.Drawing.Point(774, 226)
        Me.btnSpeichern.Name = "btnSpeichern"
        Me.btnSpeichern.Size = New System.Drawing.Size(90, 25)
        Me.btnSpeichern.TabIndex = 100
        Me.btnSpeichern.Text = "Speichern"
        Me.btnSpeichern.UseCompatibleTextRendering = True
        Me.btnSpeichern.UseVisualStyleBackColor = True
        '
        'btnBeenden
        '
        Me.btnBeenden.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBeenden.Location = New System.Drawing.Point(870, 226)
        Me.btnBeenden.Name = "btnBeenden"
        Me.btnBeenden.Size = New System.Drawing.Size(90, 25)
        Me.btnBeenden.TabIndex = 110
        Me.btnBeenden.Text = "Beenden"
        Me.btnBeenden.UseCompatibleTextRendering = True
        Me.btnBeenden.UseVisualStyleBackColor = True
        '
        'btnAthlet_Übernehmen
        '
        Me.btnAthlet_Übernehmen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAthlet_Übernehmen.Location = New System.Drawing.Point(451, 220)
        Me.btnAthlet_Übernehmen.Name = "btnAthlet_Übernehmen"
        Me.btnAthlet_Übernehmen.Size = New System.Drawing.Size(95, 25)
        Me.btnAthlet_Übernehmen.TabIndex = 48
        Me.btnAthlet_Übernehmen.Text = "Über&nehmen"
        Me.btnAthlet_Übernehmen.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.cboAK)
        Me.GroupBox1.Controls.Add(Me.txtLosnummer)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.pnlMeldedatum)
        Me.GroupBox1.Controls.Add(Me.btnZurück)
        Me.GroupBox1.Controls.Add(Me.btnWeiter)
        Me.GroupBox1.Controls.Add(Me.cboVereinsmannschaft)
        Me.GroupBox1.Controls.Add(Me.lblVereinsmannschaft)
        Me.GroupBox1.Controls.Add(Me.cboGK)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.chkDatumMerken)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtStossen)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtReissen)
        Me.GroupBox1.Controls.Add(Me.chkLändermannschaft)
        Me.GroupBox1.Controls.Add(Me.chkA_K)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtGewicht)
        Me.GroupBox1.Controls.Add(Me.txtZweikampf)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.dpkDatum)
        Me.GroupBox1.Location = New System.Drawing.Point(557, 31)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(401, 188)
        Me.GroupBox1.TabIndex = 50
        Me.GroupBox1.TabStop = False
        '
        'cboAK
        '
        Me.cboAK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAK.FormattingEnabled = True
        Me.cboAK.Location = New System.Drawing.Point(94, 101)
        Me.cboAK.Name = "cboAK"
        Me.cboAK.Size = New System.Drawing.Size(107, 21)
        Me.cboAK.TabIndex = 59
        Me.cboAK.TabStop = False
        '
        'txtLosnummer
        '
        Me.txtLosnummer.Location = New System.Drawing.Point(348, 100)
        Me.txtLosnummer.Name = "txtLosnummer"
        Me.txtLosnummer.Size = New System.Drawing.Size(35, 20)
        Me.txtLosnummer.TabIndex = 69
        Me.txtLosnummer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(283, 103)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 68
        Me.Label2.Text = "&Losnummer"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMeldedatum
        '
        Me.pnlMeldedatum.BackColor = System.Drawing.SystemColors.Window
        Me.pnlMeldedatum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMeldedatum.Controls.Add(Me.chkMeldedatum)
        Me.pnlMeldedatum.Controls.Add(Me.txtMeldedatum)
        Me.pnlMeldedatum.Location = New System.Drawing.Point(161, 49)
        Me.pnlMeldedatum.Name = "pnlMeldedatum"
        Me.pnlMeldedatum.Size = New System.Drawing.Size(93, 20)
        Me.pnlMeldedatum.TabIndex = 112
        Me.pnlMeldedatum.Visible = False
        '
        'chkMeldedatum
        '
        Me.chkMeldedatum.AutoSize = True
        Me.chkMeldedatum.Location = New System.Drawing.Point(3, 2)
        Me.chkMeldedatum.Name = "chkMeldedatum"
        Me.chkMeldedatum.Size = New System.Drawing.Size(15, 14)
        Me.chkMeldedatum.TabIndex = 73
        Me.chkMeldedatum.UseVisualStyleBackColor = True
        '
        'txtMeldedatum
        '
        Me.txtMeldedatum.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMeldedatum.Culture = New System.Globalization.CultureInfo("")
        Me.txtMeldedatum.HidePromptOnLeave = True
        Me.txtMeldedatum.HideSelection = False
        Me.txtMeldedatum.Location = New System.Drawing.Point(24, 2)
        Me.txtMeldedatum.Mask = "00.00.0000"
        Me.txtMeldedatum.Name = "txtMeldedatum"
        Me.txtMeldedatum.Size = New System.Drawing.Size(64, 13)
        Me.txtMeldedatum.TabIndex = 72
        Me.txtMeldedatum.Text = "10102022"
        Me.txtMeldedatum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnZurück
        '
        Me.btnZurück.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnZurück.Image = CType(resources.GetObject("btnZurück.Image"), System.Drawing.Image)
        Me.btnZurück.Location = New System.Drawing.Point(298, 136)
        Me.btnZurück.Name = "btnZurück"
        Me.btnZurück.Size = New System.Drawing.Size(40, 35)
        Me.btnZurück.TabIndex = 100
        Me.btnZurück.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnZurück.UseVisualStyleBackColor = True
        '
        'btnWeiter
        '
        Me.btnWeiter.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnWeiter.Image = CType(resources.GetObject("btnWeiter.Image"), System.Drawing.Image)
        Me.btnWeiter.Location = New System.Drawing.Point(344, 136)
        Me.btnWeiter.Name = "btnWeiter"
        Me.btnWeiter.Size = New System.Drawing.Size(40, 35)
        Me.btnWeiter.TabIndex = 99
        Me.btnWeiter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnWeiter.UseVisualStyleBackColor = True
        '
        'cboVereinsmannschaft
        '
        Me.cboVereinsmannschaft.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboVereinsmannschaft.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVereinsmannschaft.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboVereinsmannschaft.FormattingEnabled = True
        Me.cboVereinsmannschaft.Location = New System.Drawing.Point(348, 71)
        Me.cboVereinsmannschaft.Name = "cboVereinsmannschaft"
        Me.cboVereinsmannschaft.Size = New System.Drawing.Size(35, 21)
        Me.cboVereinsmannschaft.TabIndex = 67
        Me.cboVereinsmannschaft.Visible = False
        '
        'lblVereinsmannschaft
        '
        Me.lblVereinsmannschaft.AutoSize = True
        Me.lblVereinsmannschaft.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblVereinsmannschaft.Location = New System.Drawing.Point(243, 72)
        Me.lblVereinsmannschaft.Name = "lblVereinsmannschaft"
        Me.lblVereinsmannschaft.Size = New System.Drawing.Size(101, 17)
        Me.lblVereinsmannschaft.TabIndex = 66
        Me.lblVereinsmannschaft.Text = "&Vereinsmannschaft"
        Me.lblVereinsmannschaft.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblVereinsmannschaft.UseCompatibleTextRendering = True
        Me.lblVereinsmannschaft.Visible = False
        '
        'cboGK
        '
        Me.cboGK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cboGK.FormattingEnabled = True
        Me.cboGK.Location = New System.Drawing.Point(94, 74)
        Me.cboGK.Name = "cboGK"
        Me.cboGK.Size = New System.Drawing.Size(50, 21)
        Me.cboGK.TabIndex = 57
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 78)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 13)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "&Gewichtsklasse"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkDatumMerken
        '
        Me.chkDatumMerken.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkDatumMerken.Image = CType(resources.GetObject("chkDatumMerken.Image"), System.Drawing.Image)
        Me.chkDatumMerken.Location = New System.Drawing.Point(190, 19)
        Me.chkDatumMerken.Name = "chkDatumMerken"
        Me.chkDatumMerken.Size = New System.Drawing.Size(26, 26)
        Me.chkDatumMerken.TabIndex = 53
        Me.chkDatumMerken.TabStop = False
        Me.ToolTip1.SetToolTip(Me.chkDatumMerken, "wenn aktiviert, wird das eingestellte Datum beibehalten")
        Me.chkDatumMerken.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(58, 130)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(33, 13)
        Me.Label12.TabIndex = 60
        Me.Label12.Text = "&R / S"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.Label12, "gemeldete Lasten im Reißen / Stoßen")
        '
        'txtStossen
        '
        Me.txtStossen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtStossen.Location = New System.Drawing.Point(150, 127)
        Me.txtStossen.Name = "txtStossen"
        Me.txtStossen.Size = New System.Drawing.Size(51, 20)
        Me.txtStossen.TabIndex = 61
        Me.ToolTip1.SetToolTip(Me.txtStossen, "gemeldete Last im Stoßen")
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(28, 105)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(63, 13)
        Me.Label11.TabIndex = 58
        Me.Label11.Text = "&Altersklasse"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtReissen
        '
        Me.txtReissen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtReissen.Location = New System.Drawing.Point(94, 127)
        Me.txtReissen.Name = "txtReissen"
        Me.txtReissen.Size = New System.Drawing.Size(51, 20)
        Me.txtReissen.TabIndex = 60
        Me.ToolTip1.SetToolTip(Me.txtReissen, "gemeldete Last im Reißen")
        '
        'chkLändermannschaft
        '
        Me.chkLändermannschaft.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkLändermannschaft.AutoCheck = False
        Me.chkLändermannschaft.AutoSize = True
        Me.chkLändermannschaft.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLändermannschaft.Location = New System.Drawing.Point(270, 47)
        Me.chkLändermannschaft.Name = "chkLändermannschaft"
        Me.chkLändermannschaft.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLändermannschaft.Size = New System.Drawing.Size(114, 17)
        Me.chkLändermannschaft.TabIndex = 65
        Me.chkLändermannschaft.Text = "&Ländermannschaft"
        Me.chkLändermannschaft.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLändermannschaft.UseVisualStyleBackColor = True
        Me.chkLändermannschaft.Visible = False
        '
        'chkA_K
        '
        Me.chkA_K.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkA_K.AutoCheck = False
        Me.chkA_K.AutoSize = True
        Me.chkA_K.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkA_K.Location = New System.Drawing.Point(274, 25)
        Me.chkA_K.Name = "chkA_K"
        Me.chkA_K.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkA_K.Size = New System.Drawing.Size(110, 17)
        Me.chkA_K.TabIndex = 64
        Me.chkA_K.Text = "außer Konkurren&z"
        Me.chkA_K.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkA_K.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(18, 51)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 13)
        Me.Label8.TabIndex = 54
        Me.Label8.Text = "&Meldegewicht"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(27, 155)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(64, 13)
        Me.Label9.TabIndex = 62
        Me.Label9.Text = "&ZK-Leistung"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.Label9, "gemeldete Zweikampf-Leistung")
        '
        'txtGewicht
        '
        Me.txtGewicht.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtGewicht.Location = New System.Drawing.Point(94, 48)
        Me.txtGewicht.Name = "txtGewicht"
        Me.txtGewicht.Size = New System.Drawing.Size(51, 20)
        Me.txtGewicht.TabIndex = 55
        '
        'txtZweikampf
        '
        Me.txtZweikampf.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtZweikampf.Location = New System.Drawing.Point(94, 152)
        Me.txtZweikampf.Name = "txtZweikampf"
        Me.txtZweikampf.Size = New System.Drawing.Size(51, 20)
        Me.txtZweikampf.TabIndex = 63
        Me.ToolTip1.SetToolTip(Me.txtZweikampf, "gemeldete Zweikampf-Leistung")
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(26, 26)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(65, 13)
        Me.Label10.TabIndex = 51
        Me.Label10.Text = "Mel&dung am"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dpkDatum
        '
        Me.dpkDatum.Checked = False
        Me.dpkDatum.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dpkDatum.Location = New System.Drawing.Point(94, 22)
        Me.dpkDatum.Name = "dpkDatum"
        Me.dpkDatum.ShowCheckBox = True
        Me.dpkDatum.Size = New System.Drawing.Size(93, 20)
        Me.dpkDatum.TabIndex = 52
        '
        'txtAthlet_Name
        '
        Me.txtAthlet_Name.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtAthlet_Name.ForeColor = System.Drawing.Color.DarkGray
        Me.txtAthlet_Name.Location = New System.Drawing.Point(177, 8)
        Me.txtAthlet_Name.Name = "txtAthlet_Name"
        Me.txtAthlet_Name.Size = New System.Drawing.Size(128, 20)
        Me.txtAthlet_Name.TabIndex = 31
        Me.txtAthlet_Name.Tag = "Nachname"
        Me.txtAthlet_Name.Text = "Nachname"
        '
        'btnAthlet_Verein
        '
        Me.btnAthlet_Verein.Image = CType(resources.GetObject("btnAthlet_Verein.Image"), System.Drawing.Image)
        Me.btnAthlet_Verein.Location = New System.Drawing.Point(497, 6)
        Me.btnAthlet_Verein.Name = "btnAthlet_Verein"
        Me.btnAthlet_Verein.Size = New System.Drawing.Size(23, 23)
        Me.btnAthlet_Verein.TabIndex = 46
        Me.btnAthlet_Verein.TabStop = False
        Me.btnAthlet_Verein.UseVisualStyleBackColor = True
        '
        'dgvAuswahl
        '
        Me.dgvAuswahl.AllowUserToAddRows = False
        Me.dgvAuswahl.AllowUserToDeleteRows = False
        Me.dgvAuswahl.AllowUserToResizeRows = False
        Me.dgvAuswahl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAuswahl.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvAuswahl.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAuswahl.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvAuswahl.ColumnHeadersHeight = 21
        Me.dgvAuswahl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAuswahl.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Übernehmen, Me.Teilnehmer, Me.Nachname, Me.Vorname, Me.Sex, Me.Jahrgang, Me.Verein, Me.Nation, Me.AK})
        Me.dgvAuswahl.Location = New System.Drawing.Point(17, 36)
        Me.dgvAuswahl.MinimumSize = New System.Drawing.Size(527, 169)
        Me.dgvAuswahl.MultiSelect = False
        Me.dgvAuswahl.Name = "dgvAuswahl"
        Me.dgvAuswahl.RowHeadersVisible = False
        Me.dgvAuswahl.RowHeadersWidth = 24
        Me.dgvAuswahl.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAuswahl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAuswahl.Size = New System.Drawing.Size(527, 177)
        Me.dgvAuswahl.StandardTab = True
        Me.dgvAuswahl.TabIndex = 33
        '
        'Übernehmen
        '
        Me.Übernehmen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Übernehmen.DataPropertyName = "Übernehmen"
        Me.Übernehmen.HeaderText = ""
        Me.Übernehmen.Name = "Übernehmen"
        Me.Übernehmen.ReadOnly = True
        Me.Übernehmen.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Übernehmen.Width = 24
        '
        'Teilnehmer
        '
        Me.Teilnehmer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Teilnehmer.DataPropertyName = "Teilnehmer"
        Me.Teilnehmer.HeaderText = "Nr."
        Me.Teilnehmer.Name = "Teilnehmer"
        Me.Teilnehmer.ReadOnly = True
        Me.Teilnehmer.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Teilnehmer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Teilnehmer.Visible = False
        Me.Teilnehmer.Width = 27
        '
        'Nachname
        '
        Me.Nachname.DataPropertyName = "Nachname"
        Me.Nachname.HeaderText = "Nachname"
        Me.Nachname.Name = "Nachname"
        Me.Nachname.ReadOnly = True
        '
        'Vorname
        '
        Me.Vorname.DataPropertyName = "Vorname"
        Me.Vorname.HeaderText = "Vorname"
        Me.Vorname.Name = "Vorname"
        Me.Vorname.ReadOnly = True
        '
        'Sex
        '
        Me.Sex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Sex.DataPropertyName = "Sex"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Sex.DefaultCellStyle = DataGridViewCellStyle4
        Me.Sex.HeaderText = "m/w"
        Me.Sex.Name = "Sex"
        Me.Sex.ReadOnly = True
        Me.Sex.Width = 30
        '
        'Jahrgang
        '
        Me.Jahrgang.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Jahrgang.DataPropertyName = "Jahrgang"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Jahrgang.DefaultCellStyle = DataGridViewCellStyle5
        Me.Jahrgang.HeaderText = "Jahrgang"
        Me.Jahrgang.Name = "Jahrgang"
        Me.Jahrgang.ReadOnly = True
        Me.Jahrgang.Width = 50
        '
        'Verein
        '
        Me.Verein.DataPropertyName = "Verein"
        Me.Verein.HeaderText = "Verein"
        Me.Verein.Name = "Verein"
        Me.Verein.ReadOnly = True
        '
        'Nation
        '
        Me.Nation.DataPropertyName = "state_name"
        Me.Nation.FillWeight = 80.0!
        Me.Nation.HeaderText = "Nation"
        Me.Nation.Name = "Nation"
        Me.Nation.ReadOnly = True
        '
        'AK
        '
        Me.AK.DataPropertyName = "AK"
        Me.AK.FillWeight = 50.0!
        Me.AK.HeaderText = "AK"
        Me.AK.Name = "AK"
        Me.AK.ReadOnly = True
        '
        'txtAthlet_Verein
        '
        Me.txtAthlet_Verein.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtAthlet_Verein.ForeColor = System.Drawing.Color.DarkGray
        Me.txtAthlet_Verein.Location = New System.Drawing.Point(368, 8)
        Me.txtAthlet_Verein.Name = "txtAthlet_Verein"
        Me.txtAthlet_Verein.Size = New System.Drawing.Size(128, 20)
        Me.txtAthlet_Verein.TabIndex = 31
        Me.txtAthlet_Verein.Tag = "Verein"
        Me.txtAthlet_Verein.Text = "Verein"
        '
        'btnAthlet_Name
        '
        Me.btnAthlet_Name.Image = CType(resources.GetObject("btnAthlet_Name.Image"), System.Drawing.Image)
        Me.btnAthlet_Name.Location = New System.Drawing.Point(306, 6)
        Me.btnAthlet_Name.Name = "btnAthlet_Name"
        Me.btnAthlet_Name.Size = New System.Drawing.Size(23, 23)
        Me.btnAthlet_Name.TabIndex = 44
        Me.btnAthlet_Name.TabStop = False
        Me.btnAthlet_Name.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuBearbeiten, Me.mnuOptionen, Me.mnuExtras, Me.mnuLosnummer})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(5, 1, 0, 1)
        Me.MenuStrip1.ShowItemToolTips = True
        Me.MenuStrip1.Size = New System.Drawing.Size(981, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSpeichern, Me.ToolStripMenuItem3, Me.mnuSperren, Me.ToolStripMenuItem7, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 22)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuSpeichern
        '
        Me.mnuSpeichern.Name = "mnuSpeichern"
        Me.mnuSpeichern.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSpeichern.Size = New System.Drawing.Size(168, 22)
        Me.mnuSpeichern.Text = "&Speichern"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(165, 6)
        '
        'mnuSperren
        '
        Me.mnuSperren.Name = "mnuSperren"
        Me.mnuSperren.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.L), System.Windows.Forms.Keys)
        Me.mnuSperren.Size = New System.Drawing.Size(168, 22)
        Me.mnuSperren.Text = "Sperren"
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(165, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(168, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuBearbeiten
        '
        Me.mnuBearbeiten.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNeu, Me.ToolStripMenuItem4, Me.mnuHeber, Me.mnuVerein, Me.ToolStripMenuItem5, Me.mnuLöschen})
        Me.mnuBearbeiten.Name = "mnuBearbeiten"
        Me.mnuBearbeiten.Size = New System.Drawing.Size(75, 22)
        Me.mnuBearbeiten.Text = "&Bearbeiten"
        '
        'mnuNeu
        '
        Me.mnuNeu.Name = "mnuNeu"
        Me.mnuNeu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuNeu.Size = New System.Drawing.Size(186, 22)
        Me.mnuNeu.Text = "&Neuer Heber"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(183, 6)
        '
        'mnuHeber
        '
        Me.mnuHeber.Name = "mnuHeber"
        Me.mnuHeber.Size = New System.Drawing.Size(186, 22)
        Me.mnuHeber.Text = "Stammdaten &Heber"
        '
        'mnuVerein
        '
        Me.mnuVerein.Name = "mnuVerein"
        Me.mnuVerein.Size = New System.Drawing.Size(186, 22)
        Me.mnuVerein.Text = "Stammdaten &Verein"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(183, 6)
        '
        'mnuLöschen
        '
        Me.mnuLöschen.Name = "mnuLöschen"
        Me.mnuLöschen.Size = New System.Drawing.Size(186, 22)
        Me.mnuLöschen.Text = "Teilnehmer &löschen"
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuIDs, Me.ToolStripMenuItem9, Me.mnuLayout, Me.mnuLayoutA, Me.ToolStripMenuItem2, Me.mnuNoMessage, Me.mnuKonflikte})
        Me.mnuOptionen.Name = "mnuOptionen"
        Me.mnuOptionen.Size = New System.Drawing.Size(69, 22)
        Me.mnuOptionen.Text = "&Optionen"
        '
        'mnuIDs
        '
        Me.mnuIDs.Name = "mnuIDs"
        Me.mnuIDs.Size = New System.Drawing.Size(212, 22)
        Me.mnuIDs.Text = "IDs anzeigen"
        '
        'ToolStripMenuItem9
        '
        Me.ToolStripMenuItem9.Name = "ToolStripMenuItem9"
        Me.ToolStripMenuItem9.Size = New System.Drawing.Size(209, 6)
        '
        'mnuLayout
        '
        Me.mnuLayout.Name = "mnuLayout"
        Me.mnuLayout.Size = New System.Drawing.Size(212, 22)
        Me.mnuLayout.Text = "Tabellen-Layout: &Meldung"
        Me.mnuLayout.Visible = False
        '
        'mnuLayoutA
        '
        Me.mnuLayoutA.Name = "mnuLayoutA"
        Me.mnuLayoutA.Size = New System.Drawing.Size(212, 22)
        Me.mnuLayoutA.Text = "Tabellen-Layout: &Athleten"
        Me.mnuLayoutA.Visible = False
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(209, 6)
        Me.ToolStripMenuItem2.Visible = False
        '
        'mnuNoMessage
        '
        Me.mnuNoMessage.CheckOnClick = True
        Me.mnuNoMessage.Name = "mnuNoMessage"
        Me.mnuNoMessage.Size = New System.Drawing.Size(212, 22)
        Me.mnuNoMessage.Text = "Mel&dungen ausblenden"
        '
        'mnuKonflikte
        '
        Me.mnuKonflikte.Checked = True
        Me.mnuKonflikte.CheckOnClick = True
        Me.mnuKonflikte.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuKonflikte.Name = "mnuKonflikte"
        Me.mnuKonflikte.Size = New System.Drawing.Size(212, 22)
        Me.mnuKonflikte.Text = "bei &Konflikten nachfragen"
        '
        'mnuExtras
        '
        Me.mnuExtras.Checked = True
        Me.mnuExtras.CheckOnClick = True
        Me.mnuExtras.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMauszeiger, Me.ToolStripMenuItem10, Me.mnuLokaleDatenbank})
        Me.mnuExtras.Name = "mnuExtras"
        Me.mnuExtras.Size = New System.Drawing.Size(50, 22)
        Me.mnuExtras.Text = "&Extras"
        '
        'mnuMauszeiger
        '
        Me.mnuMauszeiger.Name = "mnuMauszeiger"
        Me.mnuMauszeiger.ShortcutKeys = System.Windows.Forms.Keys.F4
        Me.mnuMauszeiger.Size = New System.Drawing.Size(186, 22)
        Me.mnuMauszeiger.Text = "&Mauszeiger holen"
        '
        'ToolStripMenuItem10
        '
        Me.ToolStripMenuItem10.Name = "ToolStripMenuItem10"
        Me.ToolStripMenuItem10.Size = New System.Drawing.Size(183, 6)
        '
        'mnuLokaleDatenbank
        '
        Me.mnuLokaleDatenbank.CheckOnClick = True
        Me.mnuLokaleDatenbank.Name = "mnuLokaleDatenbank"
        Me.mnuLokaleDatenbank.Size = New System.Drawing.Size(186, 22)
        Me.mnuLokaleDatenbank.Text = "&lokale Datenbank"
        Me.mnuLokaleDatenbank.ToolTipText = "Athleten aus lokaler oder BVDG-Datenbank anzeigen"
        '
        'mnuLosnummer
        '
        Me.mnuLosnummer.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.mnuLosnummer.Enabled = False
        Me.mnuLosnummer.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mnuLosnummer.ForeColor = System.Drawing.Color.MediumSeaGreen
        Me.mnuLosnummer.Name = "mnuLosnummer"
        Me.mnuLosnummer.Size = New System.Drawing.Size(90, 22)
        Me.mnuLosnummer.Text = "&Losnummern"
        Me.mnuLosnummer.ToolTipText = "Losnummern für alle Teilnehmer aktualisieren "
        '
        'DataGridViewImageColumn1
        '
        Me.DataGridViewImageColumn1.HeaderText = ""
        Me.DataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.DataGridViewImageColumn1.MinimumWidth = 6
        Me.DataGridViewImageColumn1.Name = "DataGridViewImageColumn1"
        Me.DataGridViewImageColumn1.Width = 125
        '
        'bs
        '
        Me.bs.AllowNew = True
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'frmMeldung
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.ClientSize = New System.Drawing.Size(981, 577)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.SplitContainer1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(997, 616)
        Me.Name = "frmMeldung"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Meldung zum Wettkampf"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuGrid.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.pnlMeldedatum.ResumeLayout(False)
        Me.pnlMeldedatum.PerformLayout()
        CType(Me.dgvAuswahl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.bsA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents dgvJoin As System.Windows.Forms.DataGridView
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuBearbeiten As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOptionen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLayout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents mnuLayoutA As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtZweikampf As System.Windows.Forms.TextBox
    Friend WithEvents txtGewicht As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents chkA_K As System.Windows.Forms.CheckBox
    Friend WithEvents chkLändermannschaft As System.Windows.Forms.CheckBox
    Friend WithEvents dpkDatum As System.Windows.Forms.DateTimePicker
    Friend WithEvents DataGridViewImageColumn1 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents bsA As System.Windows.Forms.BindingSource
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnWeiter As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents mnuNoMessage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnMeldung_Verein As System.Windows.Forms.Button
    Friend WithEvents btnMeldung_Name As System.Windows.Forms.Button
    Friend WithEvents txtMeldung_Verein As System.Windows.Forms.TextBox
    Friend WithEvents txtMeldung_Name As System.Windows.Forms.TextBox
    Friend WithEvents mnuKonflikte As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label12 As Label
    Friend WithEvents txtStossen As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtReissen As TextBox
    Friend WithEvents lblAnzahl As Label
    Friend WithEvents ContextMenuGrid As ContextMenuStrip
    Friend WithEvents cmnuLöschen As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As ToolStripSeparator
    Friend WithEvents cmnuHeber As ToolStripMenuItem
    Friend WithEvents cmnuVerein As ToolStripMenuItem
    Friend WithEvents btnBeenden As Button
    Friend WithEvents btnSpeichern As Button
    Friend WithEvents btnZurück As Button
    Friend WithEvents chkDatumMerken As CheckBox
    Friend WithEvents bs As BindingSource
    Friend WithEvents Label1 As Label
    Friend WithEvents cboGK As ComboBox
    Friend WithEvents dgvAuswahl As DataGridView
    Friend WithEvents chkAlle_Athlet As CheckBox
    Friend WithEvents btnAthlet_Übernehmen As Button
    Friend WithEvents btnAthlet_Verein As Button
    Friend WithEvents txtAthlet_Name As TextBox
    Friend WithEvents btnAthlet_Name As Button
    Friend WithEvents txtAthlet_Verein As TextBox
    Friend WithEvents bsV As BindingSource
    Friend WithEvents pgbCheckAll As ProgressBar
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents cmnuNeu As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents mnuHeber As ToolStripMenuItem
    Friend WithEvents mnuLöschen As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As ToolStripSeparator
    Friend WithEvents mnuVerein As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As ToolStripSeparator
    Friend WithEvents mnuNeu As ToolStripMenuItem
    Friend WithEvents mnuDatei As ToolStripMenuItem
    Friend WithEvents mnuSpeichern As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem7 As ToolStripSeparator
    Friend WithEvents mnuBeenden As ToolStripMenuItem
    Friend WithEvents mnuLosnummer As ToolStripMenuItem
    Friend WithEvents mnuSperren As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripSeparator
    Friend WithEvents cboVereinsmannschaft As ComboBox
    Friend WithEvents lblVereinsmannschaft As Label
    Friend WithEvents ToolStripMenuItem8 As ToolStripSeparator
    Friend WithEvents mnuÜbernehmen As ToolStripMenuItem
    Friend WithEvents mnuExtras As ToolStripMenuItem
    Friend WithEvents mnuMauszeiger As ToolStripMenuItem
    Friend WithEvents mnuIDs As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem9 As ToolStripSeparator
    Friend WithEvents txtMeldedatum As MaskedTextBox
    Friend WithEvents pnlMeldedatum As Panel
    Friend WithEvents chkMeldedatum As CheckBox
    Friend WithEvents txtLosnummer As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents cboAK As ComboBox
    Friend WithEvents CheckLokaleDB As CheckBox
    Friend WithEvents ToolStripMenuItem10 As ToolStripSeparator
    Friend WithEvents mnuLokaleDatenbank As ToolStripMenuItem
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Übernehmen As DataGridViewCheckBoxColumn
    Friend WithEvents Teilnehmer As DataGridViewTextBoxColumn
    Friend WithEvents Nachname As DataGridViewTextBoxColumn
    Friend WithEvents Vorname As DataGridViewTextBoxColumn
    Friend WithEvents Sex As DataGridViewTextBoxColumn
    Friend WithEvents Jahrgang As DataGridViewTextBoxColumn
    Friend WithEvents Verein As DataGridViewTextBoxColumn
    Friend WithEvents Nation As DataGridViewTextBoxColumn
    Friend WithEvents AK As DataGridViewTextBoxColumn
End Class
