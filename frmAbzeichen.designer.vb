﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAbzeichen
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Con = New System.Windows.Forms.Button()
        Me.portList = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkHupe = New System.Windows.Forms.CheckBox()
        Me.btnUhr = New System.Windows.Forms.Button()
        Me.numSek = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.numMin = New System.Windows.Forms.NumericUpDown()
        Me.chkAZ = New System.Windows.Forms.CheckBox()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.numSek, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numMin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Con
        '
        Me.Con.Location = New System.Drawing.Point(140, 11)
        Me.Con.Name = "Con"
        Me.Con.Size = New System.Drawing.Size(110, 23)
        Me.Con.TabIndex = 17
        Me.Con.Text = "Verbinden"
        Me.Con.UseVisualStyleBackColor = True
        '
        'portList
        '
        Me.portList.FormattingEnabled = True
        Me.portList.Location = New System.Drawing.Point(12, 12)
        Me.portList.Name = "portList"
        Me.portList.Size = New System.Drawing.Size(121, 21)
        Me.portList.TabIndex = 16
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.chkAZ)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(3, 53)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(257, 154)
        Me.Panel1.TabIndex = 18
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkHupe)
        Me.GroupBox1.Controls.Add(Me.btnUhr)
        Me.GroupBox1.Controls.Add(Me.numSek)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.numMin)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 45)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(238, 103)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Zeitanzeige"
        '
        'chkHupe
        '
        Me.chkHupe.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkHupe.AutoCheck = False
        Me.chkHupe.AutoEllipsis = True
        Me.chkHupe.Location = New System.Drawing.Point(124, 27)
        Me.chkHupe.Name = "chkHupe"
        Me.chkHupe.Size = New System.Drawing.Size(100, 23)
        Me.chkHupe.TabIndex = 5
        Me.chkHupe.Text = "Signal [Hupe]"
        Me.chkHupe.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkHupe.UseVisualStyleBackColor = True
        '
        'btnUhr
        '
        Me.btnUhr.Location = New System.Drawing.Point(13, 27)
        Me.btnUhr.Name = "btnUhr"
        Me.btnUhr.Size = New System.Drawing.Size(89, 23)
        Me.btnUhr.TabIndex = 4
        Me.btnUhr.Text = "Einschalten"
        Me.btnUhr.UseVisualStyleBackColor = True
        '
        'numSek
        '
        Me.numSek.Enabled = False
        Me.numSek.Location = New System.Drawing.Point(183, 64)
        Me.numSek.Maximum = New Decimal(New Integer() {59, 0, 0, 0})
        Me.numSek.Name = "numSek"
        Me.numSek.Size = New System.Drawing.Size(41, 20)
        Me.numSek.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(121, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Sekunden"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 66)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Minuten"
        '
        'numMin
        '
        Me.numMin.Enabled = False
        Me.numMin.Location = New System.Drawing.Point(61, 64)
        Me.numMin.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.numMin.Name = "numMin"
        Me.numMin.Size = New System.Drawing.Size(41, 20)
        Me.numMin.TabIndex = 0
        '
        'chkAZ
        '
        Me.chkAZ.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkAZ.AutoCheck = False
        Me.chkAZ.Location = New System.Drawing.Point(9, 6)
        Me.chkAZ.Name = "chkAZ"
        Me.chkAZ.Size = New System.Drawing.Size(238, 24)
        Me.chkAZ.TabIndex = 0
        Me.chkAZ.Text = "Ab-Zeichen [akustisch/optisch]"
        Me.chkAZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkAZ.UseVisualStyleBackColor = True
        '
        'frmAbzeichen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(262, 209)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Con)
        Me.Controls.Add(Me.portList)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAbzeichen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Test Ab-Zeichen / Zeitanzeige"
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.numSek, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numMin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Con As Button
    Friend WithEvents portList As ComboBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents chkAZ As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents numSek As NumericUpDown
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents numMin As NumericUpDown
    Friend WithEvents btnUhr As Button
    Friend WithEvents chkHupe As CheckBox
End Class
