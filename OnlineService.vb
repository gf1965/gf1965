﻿Imports System.Threading.Tasks

Module OnlineServiceModdule
    Class OnlineService
        Implements IDisposable

        Function Check_Wettkampf() As Integer
            Dim dt As New DataTable
            Using conn As New MySqlConnection(User.APP_ConnString)
                Try
                    Dim Query = "SELECT Wettkampf, Bezeichnung FROM wettkampf where" & Wettkampf.WhereClause & ";"
                    Dim cmd = New MySqlCommand(Query, conn)
                    conn.Open()
                    dt.Load(cmd.ExecuteReader)
                    If dt.Rows.Count > 0 Then Return ConnState.Available
                Catch ex As Exception
                    MySQl_Error(Nothing, "OS CheckWettkampf: " & ex.Message, ex.StackTrace, True)
                    Return ConnState.NotAvailable
                End Try
            End Using
            If AppConnState = ConnState.Connected Then Return ConnState.Connected
            Return ConnState.NotAvailable
        End Function
        Function DeleteAll() As Integer
            Try
                Using conn As New MySqlConnection(User.APP_ConnString)
                    Dim Query = "TRUNCATE athleten;
                                 TRUNCATE wettkampf;
                                 TRUNCATE wettkampf_wertung;
                                 TRUNCATE gruppen;
                                 TRUNCATE meldung;
                                 TRUNCATE reissen;
                                 TRUNCATE stossen;
                                 TRUNCATE results;"
                    Dim cmd = New MySqlCommand(Query, conn)
                    conn.Open()
                    cmd.ExecuteNonQuery()
                End Using
                Return ConnState.Connected
            Catch ex As Exception
                MySQl_Error(Nothing, "OS DeleteUnused: " & ex.Message, ex.StackTrace, True)
                Return ConnState.Disconnected
            End Try
        End Function
        Sub UpdateWettkampf(Optional conn As MySqlConnection = Nothing)
            If AppConnState = ConnState.Disconnected Then Return

            'If IsNothing(conn) Then
            '    AppConnState = Await Task.Run(Function() _UpdateWettkampf(conn))
            'Else
            AppConnState = _UpdateWettkampf(conn)
            'End If
        End Sub
        Sub UpdateWertung(Optional conn As MySqlConnection = Nothing)

            If Not AppConnState = ConnState.Connected Then Return

            Dim Query = "select * from wettkampf_wertung where " & Wettkampf.WhereClause() & ";"
            Dim dt = New DataTable

            Using _conn As New MySqlConnection(User.ConnString)
                Try
                    Dim cmd = New MySqlCommand(Query, _conn)
                    _conn.Open()
                    dt.Load(cmd.ExecuteReader)

                    If dt.Rows.Count = 0 Then
                        Query = "select w.Wettkampf, m.Wertung, 0 idAK, null Wertung2, null Gruppierung
                             From Wettkampf w
                             Left Join modus m on m.idModus = w.modus
                             where " & Wettkampf.WhereClause & ";"
                        cmd = New MySqlCommand(Query, _conn)
                        dt = New DataTable
                        dt.Load(cmd.ExecuteReader)
                    End If
                Catch ex As Exception
                    MySQl_Error(Nothing, "OS UpdateWertung: " & ex.Message, ex.StackTrace, True)
                    Return
                End Try
            End Using

            'ExternCounter.Return_Success(Await Task.Run(Function() _UpdateWertung(dt)))
            'IsAppConnected = Await Task.Run(Function() _UpdateWertung(dt, conn))
            AppConnState = _UpdateWertung(dt, conn)
        End Sub
        Async Sub UpdateGruppen(Optional conn As MySqlConnection = Nothing)

            If Not AppConnState = ConnState.Connected Then Return

            Dim Query = "select * from gruppen where " & Wettkampf.WhereClause() & ";"
            Dim dt = New DataTable

            Using _conn As New MySqlConnection(User.ConnString)
                Try
                    Dim cmd = New MySqlCommand(Query, _conn)
                    _conn.Open()
                    dt.Load(cmd.ExecuteReader)

                    If dt.Rows.Count = 0 Then
                        Query = "select Wettkampf, Gruppe, Bezeichnung
                             From gruppen
                             where " & Wettkampf.WhereClause & ";"
                        cmd = New MySqlCommand(Query, _conn)
                        dt = New DataTable
                        dt.Load(cmd.ExecuteReader)
                    End If
                Catch ex As Exception
                    MySQl_Error(Nothing, "OS UpdateGruppen: " & ex.Message, ex.StackTrace, True)
                    Return
                End Try
            End Using

            If IsNothing(conn) Then
                AppConnState = Await Task.Run(Function() _UpdateGruppen(dt, conn))
            Else
                AppConnState = _UpdateGruppen(dt, conn)
            End If
        End Sub
        Async Sub UpdateMeldung(Optional conn As MySqlConnection = Nothing)

            If Not AppConnState = ConnState.Connected Then Return

            Dim Query = "select * from meldung where " & Wettkampf.WhereClause & " and (isnull(attend) or attend = true);"
            Dim dt = New DataTable

            Using _conn As New MySqlConnection(User.ConnString)
                Try
                    Dim cmd = New MySqlCommand(Query, _conn)
                    _conn.Open()
                    dt.Load(cmd.ExecuteReader)
                Catch ex As Exception
                    MySQl_Error(Nothing, "OS UpdateMeldung: " & ex.Message, ex.StackTrace, True)
                    Return
                End Try
            End Using

            'ExternCounter.Return_Success(Await Task.Run(Function() _UpdateMeldung(dt)))
            If IsNothing(conn) Then
                AppConnState = Await Task.Run(Function() _UpdateMeldung(dt, conn))
            Else
                AppConnState = _UpdateMeldung(dt, conn)
            End If
        End Sub
        Async Sub UpdateHeben(Optional Tables() As String = Nothing,
                              Optional TN As Integer = -1,
                              Optional conn As MySqlConnection = Nothing)

            If Not AppConnState = ConnState.Connected Then Return

            Dim dts As New Dictionary(Of String, DataTable)

            If IsNothing(Tables) Then Tables = {"reissen", "stossen"}

            Using _conn As New MySqlConnection(User.ConnString)
                Try
                    _conn.Open()
                    For Each Table In Tables
                        'Dim Query = "select * " &
                        '            "from meldung m " &
                        '            "left join " & Table & " x on x.Teilnehmer = m.Teilnehmer and x.Wettkampf = m.Wettkampf " &
                        '            "where " & Wettkampf.WhereClause("m") & "and (isnull(m.attend) or m.attend = true) and not isnull(x.Versuch);"

                        Dim Query = "select * from " & Table & " where " & Wettkampf.WhereClause() & ";"

                        Dim cmd = New MySqlCommand(Query, _conn)
                        dts(Table) = New DataTable
                        dts(Table).Load(cmd.ExecuteReader)
                    Next
                Catch ex As Exception
                    MySQl_Error(Nothing, "OS UpdateHeben: " & ex.Message, ex.StackTrace, True)
                    Return
                End Try
            End Using

            If TN = -1 Then
                AppConnState = _UpdateHeben(dts, conn)
            Else
                AppConnState = Await Task.Run(Function() _UpdateHeben(dts, conn))
            End If
        End Sub
        Async Sub UpdateResults(Optional conn As MySqlConnection = Nothing)

            If Not AppConnState = ConnState.Connected Then Return

            Dim dt As New DataTable
            Dim Query = "select * from results where " & Wettkampf.WhereClause() & ";"

            Using _conn As New MySqlConnection(User.ConnString)
                Try
                    _conn.Open()
                    Dim cmd = New MySqlCommand(Query, _conn)
                    dt.Load(cmd.ExecuteReader)
                Catch ex As Exception
                    MySQl_Error(Nothing, "OS UpdateResults: " & ex.Message, ex.StackTrace, True)
                    Return
                End Try
            End Using

            If Not IsNothing(conn) Then
                AppConnState = _UpdateResult(dt, conn)
            Else
                AppConnState = Await Task.Run(Function() _UpdateResult(dt, conn))
            End If
        End Sub
        Sub UpdateAthlet(Optional TN As Integer = -1, Optional conn As MySqlConnection = Nothing)

            If Not AppConnState = ConnState.Connected Then Return

            Dim dt = New DataTable
            Dim Query As String

            Using _conn As New MySqlConnection(User.ConnString)
                Try
                    _conn.Open()
                    If TN = -1 Then
                        Query = "select distinct a.* 
                            from meldung m
                            left join athleten a on a.idTeilnehmer = m.Teilnehmer
                            where " & Wettkampf.WhereClause("m") & " and not isnull(a.idTeilnehmer);"
                    Else
                        Query = "select * from athleten where idTeilnehmer = " & TN & ";"
                    End If
                    Dim cmd = New MySqlCommand(Query, _conn)
                    dt.Load(cmd.ExecuteReader)
                Catch ex As Exception
                    MySQl_Error(Nothing, "OS UpdateAthlet: " & ex.Message, ex.StackTrace, True)
                    Return
                End Try
            End Using

            'ExternCounter.Return_Success(Await Task.Run(Function() _UpdateAthlet(dt)))
            'IsAppConnected = Await Task.Run(Function() _UpdateAthlet(dt, conn))
            AppConnState = _UpdateAthlet(dt, conn)
        End Sub

        '' Tasks
        Private Function _UpdateWettkampf(conn As MySqlConnection) As Integer

            Dim ThisConn As MySqlConnection = conn
            Dim Query = "insert into wettkampf (Wettkampf, Bezeichnung, Datum, International, Platzierung, SameAK, Faktor_m, Faktor_w) 
                            values (@wk, @bez, @date, @int, @score, @same, @f_m, @f_w) 
                            on duplicate key update Bezeichnung = @bez, Datum = @date, International = @int, Platzierung = @score, SameAK = @same, Faktor_m = @f_m, Faktor_w = @f_w;"
            Try
                If IsNothing(ThisConn) Then
                    ThisConn = New MySqlConnection(User.APP_ConnString)
                    ThisConn.Open()
                End If
                For Each Item In Wettkampf.Identities
                    Dim cmd = New MySqlCommand(Query, ThisConn)
                    With cmd.Parameters
                        .AddWithValue("@wk", Item.Key)
                        .AddWithValue("@bez", Item.Value)
                        .AddWithValue("@date", Wettkampf.Datum)
                        .AddWithValue("@int", Wettkampf.International)
                        .AddWithValue("@score", Wettkampf.Platzierung)
                        .AddWithValue("@same", Wettkampf.SameAK)
                        .AddWithValue("@f_m", Wettkampf.Wertungsfaktor("m"))
                        .AddWithValue("@f_w", Wettkampf.Wertungsfaktor("w"))
                    End With
                    cmd.ExecuteNonQuery()
                Next
                Return ConnState.Connected
            Catch ex As Exception
                MySQl_Error(Nothing, "OS _UpdateWettkampf: " & ex.Message, ex.StackTrace, True)
                Return ConnState.Disconnected
            Finally
                If IsNothing(conn) Then
                    ThisConn.Close()
                    ThisConn.Dispose()
                End If
            End Try
        End Function
        Private Function _UpdateWertung(dt As DataTable, conn As MySqlConnection) As Integer

            Dim ThisConn As MySqlConnection = conn
            Dim Query = "insert into wettkampf_wertung (Wettkampf, idAK, Wertung, Wertung2, Gruppierung) " &
                        "values (@wk, @idAK, @wert, @wert2, @group) " &
                        "on duplicate key update Wertung = @wert, Wertung2 = @wert2, Gruppierung = @group;"
            Try
                If IsNothing(ThisConn) Then
                    ThisConn = New MySqlConnection(User.APP_ConnString)
                    ThisConn.Open()
                End If
                For Each r As DataRow In dt.Rows
                    Dim cmd = New MySqlCommand(Query, ThisConn)
                    With cmd.Parameters
                        .AddWithValue("@wk", r!Wettkampf)
                        .AddWithValue("@idAK", r!idAK)
                        .AddWithValue("@wert", r!Wertung)
                        .AddWithValue("@wert2", r!Wertung2)
                        .AddWithValue("@group", r!Gruppierung)
                    End With
                    cmd.ExecuteNonQuery()
                Next
                Return ConnState.Connected
            Catch ex As Exception
                MySQl_Error(Nothing, "OS _UpdateWertung: " & ex.Message, ex.StackTrace, True)
                Return ConnState.Disconnected
            Finally
                If IsNothing(conn) Then
                    ThisConn.Close()
                    ThisConn.Dispose()
                End If
            End Try
        End Function
        Private Function _UpdateGruppen(dt As DataTable, conn As MySqlConnection) As Integer

            Dim ThisConn As MySqlConnection = conn
            Dim Query = "insert into gruppen (Wettkampf, Gruppe, Bezeichnung) " &
                        "values (@wk, @group, @bez) " &
                        "on duplicate key update Bezeichnung = @bez;"
            Try
                If IsNothing(ThisConn) Then
                    ThisConn = New MySqlConnection(User.APP_ConnString)
                    ThisConn.Open()
                End If
                For Each r As DataRow In dt.Rows
                    Dim cmd = New MySqlCommand(Query, ThisConn)
                    With cmd.Parameters
                        .AddWithValue("@wk", r!Wettkampf)
                        .AddWithValue("@group", r!Gruppe)
                        .AddWithValue("@bez", r!Bezeichnung)
                    End With
                    cmd.ExecuteNonQuery()
                Next
                Return ConnState.Connected
            Catch ex As Exception
                MySQl_Error(Nothing, "OS _UpdateGruppen: " & ex.Message, ex.StackTrace, True)
                Return ConnState.Disconnected
            Finally
                If IsNothing(conn) Then
                    ThisConn.Close()
                    ThisConn.Dispose()
                End If
            End Try
        End Function
        Private Function _UpdateMeldung(dt As DataTable, conn As MySqlConnection) As Integer

            Dim ThisConn As MySqlConnection = conn
            Dim Query = "insert into meldung (Wettkampf, Teilnehmer, Gewicht, a_K, Wiegen, Startnummer, idAK, AK, idGK, GK, Gruppe, Zweikampf, Reissen, Stossen, attend) " &
                        "values (@wk, @tn, @gewicht, @a_k, @wiegen, @startnr, @idAK, @ak, @idGK, @gk, @gruppe, @zk, @r, @s, @att) " &
                        "on duplicate key update Gewicht = @gewicht, a_K = @a_k, Wiegen = @wiegen, Startnummer = @startnr, idAK = @idAK, AK = @ak, idGK = @idGK, GK = @gk, Gruppe = @gruppe, Zweikampf = @zk, Reissen = @r, Stossen = @s, attend = @att;"
            Try
                If IsNothing(ThisConn) Then
                    ThisConn = New MySqlConnection(User.APP_ConnString)
                    ThisConn.Open()
                End If
                For Each r As DataRow In dt.Rows
                    Dim cmd = New MySqlCommand(Query, ThisConn)
                    With cmd.Parameters
                        .AddWithValue("@wk", r!Wettkampf)
                        .AddWithValue("@tn", r!Teilnehmer)
                        .AddWithValue("@gewicht", r!Gewicht)
                        .AddWithValue("@a_k", r!a_K)
                        .AddWithValue("@wiegen", r!Wiegen)
                        .AddWithValue("@startnr", r!Startnummer)
                        .AddWithValue("@idAK", r!idAK)
                        .AddWithValue("@ak", r!AK)
                        .AddWithValue("@idGK", r!idGK)
                        .AddWithValue("@gk", r!GK)
                        .AddWithValue("@gruppe", r!Gruppe)
                        .AddWithValue("@zk", r!Zweikampf)
                        .AddWithValue("@r", r!Reissen)
                        .AddWithValue("@s", r!Stossen)
                        .AddWithValue("@att", r!attend)
                    End With
                    cmd.ExecuteNonQuery()
                Next
                Return ConnState.Connected
            Catch ex As Exception
                MySQl_Error(Nothing, "OS _UpdateMeldung: " & ex.Message, ex.StackTrace, True)
                Return ConnState.Disconnected
            Finally
                If IsNothing(conn) Then
                    ThisConn.Close()
                    ThisConn.Dispose()
                End If
            End Try
        End Function
        Private Function _UpdateHeben(dts As Dictionary(Of String, DataTable), conn As MySqlConnection) As Integer

            Dim ThisConn As MySqlConnection = conn
            Try
                If IsNothing(ThisConn) Then
                    ThisConn = New MySqlConnection(User.APP_ConnString)
                    ThisConn.Open()
                End If
                For Each dt In dts.Keys
                    Dim Query = "insert into " & dt & " (Wettkampf, Teilnehmer, Versuch, HLast, Diff, Wertung, Zeit) " &
                                "values (@wk, @tn, @vers, @last, @diff, @wert, @zeit) " &
                                "on duplicate key update HLast = @last, Diff = @diff, Wertung = @wert, Zeit = @zeit;"
                    For Each r As DataRow In dts(dt).Rows
                        Dim cmd = New MySqlCommand(Query, ThisConn)
                        With cmd.Parameters
                            .AddWithValue("@wk", r!Wettkampf)
                            .AddWithValue("@tn", r!Teilnehmer)
                            .AddWithValue("@vers", r!Versuch)
                            .AddWithValue("@last", r!HLast)
                            .AddWithValue("@diff", r!Diff)
                            .AddWithValue("@wert", r!Wertung)
                            .AddWithValue("@zeit", r!Zeit)
                        End With
                        cmd.ExecuteNonQuery()
                    Next
                Next
                Return ConnState.Connected
            Catch ex As Exception
                MySQl_Error(Nothing, "OS _UpdateHeben: " & ex.Message, ex.StackTrace, True)
                Return ConnState.Disconnected
            Finally
                If IsNothing(conn) Then
                    ThisConn.Close()
                    ThisConn.Dispose()
                End If
            End Try
        End Function
        Private Function _UpdateResult(dt As DataTable, conn As MySqlConnection) As Integer

            Dim ThisConn As MySqlConnection = conn
            Dim Query = "insert into results (Wettkampf, Teilnehmer, Reissen, Reissen_Zeit, Reissen_Platz, Stossen, Stossen_Zeit, Stossen_Platz, ZK, Heben, Heben_platz, Wertung2, Wertung2_Platz" &
                        If(Wettkampf.Athletik, ", Athletik, Gesamt, Gesamt_Platz) ", ") ") &
                        "values (@wk, @tn, @r, @r_z, @r_p, @s, @s_z, @s_p, @zk, @h, @h_p, @w2, @w2_p" &
                        If(Wettkampf.Athletik, ", @a, @g, @g_p) ", ") ") &
                        "on duplicate key update Reissen = @r, Reissen_Zeit = @r_z, Reissen_Platz = @r_p, Stossen = @s, Stossen_Zeit = @s_z, Stossen_Platz = @s_p, ZK = @zk, Heben = @h, Heben_platz = @h_p, Wertung2 = @w2, Wertung2_Platz = @w2_p" &
                        If(Wettkampf.Athletik, ", Athletik = @a, Gesamt = @g, Gesamt_Platz = @g_p;", ";")
            Try
                If IsNothing(ThisConn) Then
                    ThisConn = New MySqlConnection(User.APP_ConnString)
                    ThisConn.Open()
                End If
                For Each r As DataRow In dt.Rows
                    Dim cmd = New MySqlCommand(Query, ThisConn)
                    With cmd.Parameters
                        .AddWithValue("@wk", r!Wettkampf)
                        .AddWithValue("@tn", r!Teilnehmer)
                        .AddWithValue("@r", r!Reissen)
                        .AddWithValue("@r_z", r!Reissen_Zeit)
                        .AddWithValue("@r_p", r!Reissen_Platz)
                        .AddWithValue("@s", r!Stossen)
                        .AddWithValue("@s_z", r!Stossen_Zeit)
                        .AddWithValue("@s_p", r!Stossen_Platz)
                        .AddWithValue("@zk", r!ZK)
                        .AddWithValue("@h", r!Heben)
                        .AddWithValue("@h_p", r!Heben_Platz)
                        .AddWithValue("@w2", r!Wertung2)
                        .AddWithValue("@w2_p", r!Wertung2_Platz)
                        If Wettkampf.Athletik Then
                            .AddWithValue("@a", r!Athletik)
                            .AddWithValue("@g", r!Gesamt)
                            .AddWithValue("@g_p", r!Gesamt_Platz)
                        End If
                        '.AddWithValue("@u", r!Updated)
                    End With
                    cmd.ExecuteNonQuery()
                Next
                Return ConnState.Connected
            Catch ex As Exception
                MySQl_Error(Nothing, "OS _UpdateResult: " & ex.Message, ex.StackTrace, True)
                Return ConnState.Disconnected
            Finally
                If IsNothing(conn) Then
                    ThisConn.Close()
                    ThisConn.Dispose()
                End If
            End Try
        End Function
        Private Function _UpdateAthlet(dt As DataTable, conn As MySqlConnection) As Integer

            Dim ThisConn As MySqlConnection = conn
            Dim Query = "insert into athleten (idTeilnehmer, Nachname, Vorname, Jahrgang, Geschlecht) " &
                        "values (@tn, @nn, @vn, @jg, @sex) " &
                        "on duplicate key update Nachname = @nn, Vorname = @vn, Jahrgang = @jg, Geschlecht = @sex;"
            Try
                If IsNothing(ThisConn) Then
                    ThisConn = New MySqlConnection(User.APP_ConnString)
                    ThisConn.Open()
                End If
                For Each r As DataRow In dt.Rows
                    Dim cmd = New MySqlCommand(Query, ThisConn)
                    With cmd.Parameters
                        .AddWithValue("@tn", r!idTeilnehmer)
                        .AddWithValue("@nn", r!Nachname)
                        .AddWithValue("@vn", r!Vorname)
                        .AddWithValue("@jg", r!Jahrgang)
                        .AddWithValue("@sex", r!Geschlecht)
                    End With
                    cmd.ExecuteNonQuery()
                Next
                Return ConnState.Connected
            Catch ex As Exception
                MySQl_Error(Nothing, "OS _UpdateAthlet: " & ex.Message, ex.StackTrace, True)
                Return ConnState.Disconnected
            Finally
                If IsNothing(conn) Then
                    ThisConn.Close()
                    ThisConn.Dispose()
                End If
            End Try
        End Function

#Region "Licence"
        Function Verify_Licence(RegKeys As Dictionary(Of String, String)) As String
            Dim dt As New DataTable
            Dim Query = "SELECT * FROM licence WHERE serial = '" & RegKeys("Serial") & "';"
            Try
                Using Conn As New MySqlConnection(User.Lizenz_ConnString)
                    Dim cmd = New MySqlCommand(Query, Conn)
                    Conn.Open()
                    dt.Load(cmd.ExecuteReader)
                End Using
            Catch ex As Exception
                MySQl_Error(Nothing, "OnlineService: Get_Licence: " & ex.Message, "", True)
                Return "Error" ' Fehler bei der DB-Abfrage
            End Try

            If dt.Rows.Count = 1 Then
                If RegKeys("Activation").Equals(dt(0)!activation.ToString) Then
                    Return "Verified"
                ElseIf String.IsNullOrEmpty(dt(0)!activation.ToString) Then
                    Return "Pending"
                End If
            End If
            Return "Invalid" ' Serial nicht in DB ODER Serial in DB, aber nicht valid
        End Function
        Function Set_Activation(RegKeys As Dictionary(Of String, String)) As Boolean
            Dim Query = "UPDATE licence SET activation = '" & RegKeys("Activation") & "' WHERE serial = '" & RegKeys("Serial") & "';"
            Try
                Using Conn As New MySqlConnection(User.Lizenz_ConnString)
                    Dim cmd = New MySqlCommand(Query, Conn)
                    Conn.Open()
                    cmd.ExecuteNonQuery()
                End Using
            Catch ex As Exception
                MySQl_Error(Nothing, "OnlineService: Set_Activation: " & ex.Message, "", True)
                Return False
            End Try
            Return True
        End Function
#End Region

#Region "IDisposable Support"
        Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.
        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
                End If

                ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
                ' TODO: große Felder auf Null setzen.
            End If
            disposedValue = True
        End Sub
        ' TODO: Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
        'Protected Overrides Sub Finalize()
        '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
            Dispose(True)
            ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
            ' GC.SuppressFinalize(Me)
        End Sub
#End Region
    End Class
End Module
