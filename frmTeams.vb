﻿
Imports MySqlConnector

Public Class frmTeams
    Private Bereich As String = "Teams"

    Private cmd As MySqlCommand

    Private dtTeams As DataTable
    Private dtVereine As DataTable

    Private dvTeams As DataView
    Private dvVereine As DataView

    Property TeamId As Integer = -1
    Property TeamName As String
    Property TeamNew As Boolean
    Property TeamMembers As Integer

    Private Sub Me_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If mnuSave.Enabled AndAlso Not mnuSave.Text.Equals("Übernehmen") Then
            Select Case MsgBox("Änderungen speichern?", CType(MsgBoxStyle.YesNoCancel + vbQuestion, MsgBoxStyle), "Kampfgemeinschaften")
                Case MsgBoxResult.Yes
                    mnuSave.PerformClick()
                Case MsgBoxResult.Cancel
                    e.Cancel = True
            End Select
        End If
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()

                dtTeams = New DataTable
                Dim Query = "select v.idVerein, v.Vereinsname, group_concat(t.idVerein) Members
                        from teams t
                        left join verein v on v.idVerein = t.idTeam
                        group by t.idTeam
                        order by v.Vereinsname;"
                cmd = New MySqlCommand(Query, conn)
                dtTeams.Load(cmd.ExecuteReader())

                dtVereine = New DataTable
                Query = "select * from verein 
                        where idVerein > 0 and Team = false 
                        order by Vereinsname;"
                cmd = New MySqlCommand(Query, conn)
                dtVereine.Load(cmd.ExecuteReader())

            Catch ex As MySqlException
                MessageBox.Show(ex.Message)
                ConnError = True
            End Try
        End Using

        dvTeams = New DataView(dtTeams)
        bsTeams = New BindingSource With {.DataSource = dvTeams, .Filter = If(TeamId > -1, "idVerein = " & TeamId, "")}

        With dgvTeams
            .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
            .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor
            .AutoGenerateColumns = False
            .DataSource = bsTeams
        End With

        dvVereine = New DataView(dtVereine)
        bsVereine = New BindingSource With {.DataSource = dvVereine}
        With dgvVereine
            .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
            .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor
            .AutoGenerateColumns = False
            .DataSource = bsVereine
        End With

        bsAuswahl = New BindingSource With {.DataSource = dtVereine}
        With lstVereine
            .DataSource = bsAuswahl
            .DisplayMember = "Vereinsname"
        End With

        Set_Vereinsfilter()
        Set_Auswahlfilter()

        mnuDelete.Enabled = bsTeams.Count > 0 AndAlso TeamId = -1

        If TeamId > -1 OrElse Not String.IsNullOrEmpty(TeamName) Then
            mnuSave.Text = "Übernehmen"
            mnuSave.Enabled = True
            dgvTeams.ReadOnly = True
        End If

        mnuNew.Enabled = TeamNew OrElse TeamId = -1
        'If TeamNew Then mnuNew.PerformClick()
        If mnuNew.Enabled Then mnuNew.PerformClick()

        Cursor = Cursors.Default
    End Sub

    Private Sub mnuClose_Click(sender As Object, e As EventArgs) Handles mnuClose.Click, btnClose.Click
        Close()
    End Sub
    Private Sub mnuDelete_Click(sender As Object, e As EventArgs) Handles mnuDelete.Click
        bsTeams.RemoveCurrent()
        mnuSave.Enabled = True
    End Sub
    Private Sub mnuMauszeiger_Click(sender As Object, e As EventArgs) Handles mnuMauszeiger.Click
        Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
        Cursor.Show()
    End Sub
    Private Sub mnuNew_Click(sender As Object, e As EventArgs) Handles mnuNew.Click, btnNew.Click
        TeamId = Get_NextId()
        bsTeams.AddNew()
        dvTeams(bsTeams.Position)!idVerein = TeamId
        dvTeams(bsTeams.Position)!Vereinsname = TeamName
        bsTeams.EndEdit()
        With dgvTeams
            .Rows(.SelectedRows(0).Index).Cells("Bezeichnung").Selected = True
            .FirstDisplayedCell = .CurrentCell
            If String.IsNullOrEmpty(TeamName) Then .BeginEdit(True)
        End With
        mnuNew.Enabled = False
        If Not String.IsNullOrEmpty(TeamName) Then txtVerein.Focus()
    End Sub
    Private Sub mnuNew_EnabledChanged(sender As Object, e As EventArgs) Handles mnuNew.EnabledChanged
        btnNew.Enabled = mnuNew.Enabled
        mnuDelete.Enabled = mnuNew.Enabled AndAlso bsTeams.Count > 0
    End Sub
    Private Sub mnuSave_Click(sender As Object, e As EventArgs) Handles mnuSave.Click, btnSave.Click

        Cursor = Cursors.WaitCursor

        Validate()
        bsTeams.EndEdit()
        Dim changes = dtTeams.GetChanges
        If Not IsNothing(changes) Then
            Using conn As New MySqlConnection(User.ConnString)
                For Each row As DataRow In changes.Rows
                    Dim Query = "delete from teams where idTeam = @team;"
                    If row.RowState.Equals(DataRowState.Deleted) Then
                        Query += "delete from verein where idVerein = @team;"
                    Else
                        Dim members = row!Members.ToString.Split(CChar(",")).ToList()
                        If members.Count > 0 AndAlso String.IsNullOrEmpty(members(0)) Then members.RemoveAt(0)
                        For Each member In members
                            Query += "insert into teams (idTeam, idVerein) values (@team, " & member & ");"
                        Next
                        If row.RowState.Equals(DataRowState.Added) Then
                            Query += "insert into verein (idVerein, Vereinsname, Team) values (@team, @bez, true)
                                      on duplicate key update Vereinsname = @bez, Team = true;"
                        End If
                    End If
                    cmd = New MySqlCommand(Query, conn)
                    With cmd.Parameters
                        If row.RowState.Equals(DataRowState.Deleted) Then
                            .AddWithValue("@team", row("idVerein", DataRowVersion.Original))
                        Else
                            .AddWithValue("@team", row!idVerein)
                            .AddWithValue("@bez", row!Vereinsname)
                        End If
                    End With
                    Do
                        Try
                            If conn.State = ConnectionState.Closed Then conn.Open()
                            cmd.ExecuteNonQuery()
                            ConnError = False
                        Catch ex As Exception
                            If MySQl_Error(Me, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                        End Try
                    Loop While ConnError
                Next
            End Using
        End If

        Cursor = Cursors.Default
        If ConnError Then Return

        mnuSave.Enabled = False
        mnuNew.Enabled = True

        If mnuSave.Text.Equals("Übernehmen") Then
            TeamMembers = bsVereine.Count
            TeamId = CInt(dvTeams(bsTeams.Position)!idVerein)
            DialogResult = DialogResult.OK
            Close()
        End If

    End Sub
    Private Sub mnuSave_EnabledChanged(sender As Object, e As EventArgs) Handles mnuSave.EnabledChanged
        btnSave.Enabled = mnuSave.Enabled
        mnuDelete.Enabled = mnuNew.Enabled AndAlso bsTeams.Count > 0
    End Sub
    Private Sub mnuSave_TextChanged(sender As Object, e As EventArgs) Handles mnuSave.TextChanged
        btnSave.Text = mnuSave.Text
    End Sub

    Private Sub bsTeams_PositionChanged(sender As Object, e As EventArgs) Handles bsTeams.PositionChanged
        Set_Vereinsfilter()
        Set_Auswahlfilter()
        dgvVereine.ClearSelection()
        mnuDelete.Enabled = mnuNew.Enabled AndAlso bsTeams.Count > 0
    End Sub

    Private Function Get_NextId() As Integer
        Dim id = -1
        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                Dim Query = "SELECT max(idTeam) idTeam, max(v.idVerein) idVerein,
                        (select idVerein from verein
                        where idVerein = (SELECT max(idTeam) FROM teams) + 1) newId_inVerein
                        FROM teams, verein v;"
                cmd = New MySqlCommand(Query, conn)
                Dim reader = cmd.ExecuteReader()
                If reader.HasRows Then
                    reader.Read()
                    If IsDBNull(reader!newId_inVerein) Then
                        If IsDBNull(reader!idTeam) Then
                            id = 1
                        Else
                            id = CInt(reader!idTeam) + 1
                        End If
                    Else
                        id = CInt(reader!idVerein) + 1
                    End If
                End If
                reader.Close()
                Return id
            Catch ex As MySqlException
                MessageBox.Show(ex.Message, "Teams:Get_NextId")
            End Try
        End Using
        Return id
    End Function

    Private Sub Set_Vereinsfilter()
        If IsNothing(bsTeam) OrElse bsTeam.Position = -1 Then Return

        If IsDBNull(dvTeams(bsTeams.Position)!Members) Then
            bsVereine.Filter = "idVerein = " & TeamId
        Else
            Dim members = dvTeams(bsTeams.Position)!Members.ToString.Split(CChar(",")).ToList()
            If members.Count > 0 AndAlso String.IsNullOrEmpty(members(0)) Then members.RemoveAt(0)
            bsVereine.Filter = "idVerein = " & Join(members.ToArray(), " or idVerein = ")
            dgvVereine.ClearSelection()
        End If
    End Sub
    Private Sub Set_Auswahlfilter()
        If bsTeams.Position = -1 Then Return
        If IsDBNull(dvTeams(bsTeams.Position)!Members) Then
            bsAuswahl.Filter = "Vereinsname like '%" & txtVerein.Text & "%'"
        Else
            Dim members = dvTeams(bsTeams.Position)!Members.ToString.Split(CChar(",")).ToList()
            If members.Count > 0 AndAlso String.IsNullOrEmpty(members(0)) Then members.RemoveAt(0)
            bsAuswahl.Filter = "idVerein <> " & Join(members.ToArray(), " and idVerein <> ")
            If Not String.IsNullOrEmpty(txtVerein.Text) Then bsAuswahl.Filter += " and Vereinsname like '%" & txtVerein.Text & "%'"
        End If
    End Sub

    ' Vereine
    Private Sub dgvVereine_GotFocus(sender As Object, e As EventArgs) Handles dgvVereine.GotFocus
        btnRemove.Enabled = dgvVereine.SelectedRows.Count > 0
    End Sub
    Private Sub dgvVereine_LostFocus(sender As Object, e As EventArgs) Handles dgvVereine.LostFocus
        If Not btnRemove.Focused Then
            dgvVereine.ClearSelection()
            btnRemove.Enabled = False
        End If
    End Sub
    Private Sub dgvVereine_SelectionChanged(sender As Object, e As EventArgs) Handles dgvVereine.SelectionChanged
        btnRemove.Enabled = dgvVereine.SelectedRows.Count > 0
    End Sub

    ' Auswahl
    Private Sub txtVerein_GotFocus(sender As Object, e As EventArgs) Handles txtVerein.GotFocus
        txtVerein.SelectAll()
    End Sub
    Private Sub txtVerein_KeyDown(sender As Object, e As KeyEventArgs) Handles txtVerein.KeyDown
        Select Case e.KeyCode
            Case Keys.Down
                lstVereine.Focus()
                If bsAuswahl.Position = 0 Then
                    bsAuswahl.MoveNext()
                Else
                    bsAuswahl.MoveFirst()
                End If
            Case Keys.Enter
                lstVereine.Focus()
            Case Keys.Escape
                txtVerein.Text = String.Empty
            Case Else
                Return
        End Select
        e.SuppressKeyPress = True
    End Sub
    Private Sub txtVerein_TextChanged(sender As Object, e As EventArgs) Handles txtVerein.TextChanged
        Set_Auswahlfilter()
    End Sub
    Private Sub lstVereine_DoubleClick(sender As Object, e As EventArgs) Handles lstVereine.DoubleClick
        btnAdd.PerformClick()
    End Sub
    Private Sub lstVereine_GotFocus(sender As Object, e As EventArgs) Handles lstVereine.GotFocus
        btnAdd.Enabled = True
    End Sub
    Private Sub lstVereine_KeyDown(sender As Object, e As KeyEventArgs) Handles lstVereine.KeyDown
        If e.KeyCode.Equals(Keys.Enter) Then
            e.SuppressKeyPress = True
            btnAdd.PerformClick()
        End If
    End Sub
    Private Sub lstVereine_LostFocus(sender As Object, e As EventArgs) Handles lstVereine.LostFocus
        If Not btnAdd.Focused Then btnAdd.Enabled = False
    End Sub

    ' Add/Remove
    Private Sub btnAdd_LostFocus(sender As Object, e As EventArgs) Handles btnAdd.LostFocus
        If Not lstVereine.Focused Then btnAdd.Enabled = False
    End Sub
    Private Sub btn_Click(sender As Object, e As EventArgs) Handles btnAdd.Click, btnRemove.Click
        Dim members = dvTeams(bsTeams.Position)!Members.ToString.Split(CChar(",")).ToList()
        If members(0).Equals(String.Empty) Then members.RemoveAt(0)

        If sender.Equals(btnAdd) Then
            members.Add(DirectCast(lstVereine.SelectedItem, DataRowView)!idVerein.ToString())
        Else
            members.Remove(dvVereine(bsVereine.Position)!idVerein.ToString())
        End If

        dvTeams(bsTeams.Position)!Members = String.Join(",", members)
        Set_Vereinsfilter()
        Set_Auswahlfilter()
        mnuSave.Enabled = members.Count > 0

        txtVerein.Text = String.Empty
    End Sub

End Class