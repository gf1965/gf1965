﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class dlgLanDevices
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(dlgLanDevices))
        Me.btnExit = New System.Windows.Forms.Button()
        Me.dgvDevices = New System.Windows.Forms.DataGridView()
        Me.Device = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MAC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuDbServer = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuComServer = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeide = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnCopy = New System.Windows.Forms.Button()
        CType(Me.dgvDevices, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(248, 147)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(85, 25)
        Me.btnExit.TabIndex = 1
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "Schließen"
        '
        'dgvDevices
        '
        Me.dgvDevices.AllowUserToAddRows = False
        Me.dgvDevices.AllowUserToDeleteRows = False
        Me.dgvDevices.AllowUserToOrderColumns = True
        Me.dgvDevices.AllowUserToResizeRows = False
        Me.dgvDevices.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDevices.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDevices.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDevices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvDevices.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Device, Me.IP, Me.MAC})
        Me.dgvDevices.ContextMenuStrip = Me.ContextMenuStrip1
        Me.dgvDevices.Location = New System.Drawing.Point(0, 0)
        Me.dgvDevices.MultiSelect = False
        Me.dgvDevices.Name = "dgvDevices"
        Me.dgvDevices.RowHeadersVisible = False
        Me.dgvDevices.RowHeadersWidth = 25
        Me.dgvDevices.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvDevices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvDevices.Size = New System.Drawing.Size(344, 135)
        Me.dgvDevices.TabIndex = 18
        '
        'Device
        '
        Me.Device.DataPropertyName = "Device"
        DataGridViewCellStyle2.Padding = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.Device.DefaultCellStyle = DataGridViewCellStyle2
        Me.Device.HeaderText = "Geräte-Name"
        Me.Device.Name = "Device"
        Me.Device.ReadOnly = True
        '
        'IP
        '
        Me.IP.DataPropertyName = "IP"
        DataGridViewCellStyle3.Padding = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.IP.DefaultCellStyle = DataGridViewCellStyle3
        Me.IP.FillWeight = 60.0!
        Me.IP.HeaderText = "IP-Adresse"
        Me.IP.Name = "IP"
        Me.IP.ReadOnly = True
        '
        'MAC
        '
        Me.MAC.DataPropertyName = "MAC"
        DataGridViewCellStyle4.Padding = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.MAC.DefaultCellStyle = DataGridViewCellStyle4
        Me.MAC.FillWeight = 70.0!
        Me.MAC.HeaderText = "MAC-Adresse"
        Me.MAC.Name = "MAC"
        Me.MAC.ReadOnly = True
        Me.MAC.Visible = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDbServer, Me.mnuComServer, Me.ToolStripMenuItem1, Me.mnuBeide})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(203, 76)
        '
        'mnuDbServer
        '
        Me.mnuDbServer.Image = Global.Gewichtheben.My.Resources.Resources.sqr_orange
        Me.mnuDbServer.Name = "mnuDbServer"
        Me.mnuDbServer.Size = New System.Drawing.Size(202, 22)
        Me.mnuDbServer.Text = "Datenbank-Server"
        '
        'mnuComServer
        '
        Me.mnuComServer.Image = Global.Gewichtheben.My.Resources.Resources.sqr_green
        Me.mnuComServer.Name = "mnuComServer"
        Me.mnuComServer.Size = New System.Drawing.Size(202, 22)
        Me.mnuComServer.Text = "Kommunikations-Server"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(199, 6)
        '
        'mnuBeide
        '
        Me.mnuBeide.CheckOnClick = True
        Me.mnuBeide.Name = "mnuBeide"
        Me.mnuBeide.Size = New System.Drawing.Size(202, 22)
        Me.mnuBeide.Text = "Gerät für beide Server"
        '
        'btnCopy
        '
        Me.btnCopy.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCopy.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnCopy.Enabled = False
        Me.btnCopy.Location = New System.Drawing.Point(157, 147)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(85, 25)
        Me.btnCopy.TabIndex = 20
        Me.btnCopy.TabStop = False
        Me.btnCopy.Text = "Übernehmen"
        Me.btnCopy.UseVisualStyleBackColor = True
        '
        'dlgLanDevices
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(344, 184)
        Me.Controls.Add(Me.btnCopy)
        Me.Controls.Add(Me.dgvDevices)
        Me.Controls.Add(Me.btnExit)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(360, 222)
        Me.Name = "dlgLanDevices"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Geräte im Netzwerk"
        CType(Me.dgvDevices, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents dgvDevices As DataGridView
    Friend WithEvents btnCopy As Button
    Friend WithEvents Device As DataGridViewTextBoxColumn
    Friend WithEvents IP As DataGridViewTextBoxColumn
    Friend WithEvents MAC As DataGridViewTextBoxColumn
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents mnuComServer As ToolStripMenuItem
    Friend WithEvents mnuDbServer As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents mnuBeide As ToolStripMenuItem
End Class
