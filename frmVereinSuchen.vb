﻿Public Class frmVereinSuchen

    Public Property dtVerein As New DataTable
    Public Property Verein As Integer
    Public Property Staat As Integer

    Private Sub frmVereinSuchen_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor
    End Sub
    Private Sub frmVereinSuchen_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        With cboVereinDatenbank
            .DataSource = dtVerein
            .ValueMember = "idVerein"
            .DisplayMember = "Vereinsname"
            .Focus()
        End With

        Cursor = Cursors.Default
    End Sub

    Private Sub btnVereinSubmit_Click(sender As Object, e As EventArgs) Handles btnVereinSubmit.Click

        Verein = CInt(cboVereinDatenbank.SelectedValue)
        Staat = CInt(CType(cboVereinDatenbank.SelectedItem, DataRowView)!Staat)

        DialogResult = DialogResult.OK
        Close()
    End Sub

End Class