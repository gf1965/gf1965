﻿
Imports System.Timers
Imports System.Drawing.Text
Imports AxWMPLib


Public Class frmBohleTV

    Private Hantel As New myHantel

    Dim xValid As New Dictionary(Of Integer, TextBox) 'Container für Gültig

    Private Delegate Sub DelegateSub()
    Private Delegate Sub DelegateSub0(ByVal status As Boolean)
    Private Delegate Sub DelegateSub1(ByVal Value As String)
    Private Delegate Sub DelegateSub2(ByVal Status As Boolean, ByVal Value As String)
    Private Delegate Sub DelegateSub3(ByVal Value As Integer)
    Private Delegate Sub DelegateSub4(ByVal status As Integer, ByVal value As Double)
    Private Delegate Sub DelegateSub5(ByVal Staat As String, ByVal Flagge As Byte())
    Private Delegate Sub DelegateSub6(ByVal Hantellast As String, ByVal Anzeigen As Boolean)
    Private Delegate Sub DelegateSub7(ByVal Nachname As String, ByVal Vorname As String)
    Private Delegate Sub DelegateSub8(ByVal status As Integer, ByVal value As String)
    Private Delegate Sub DelegateColor(ByVal Value As Color)

    Dim PrivateFonts As New PrivateFontCollection
    Dim fontFamilies() As FontFamily

    Private WithEvents TimerMsg As New Timer With {.Interval = 500, .AutoReset = True, .SynchronizingObject = Me}   ' Blinken von Jury-Entscheid
    Private timerMsg_Running As Boolean

    Private Sub Change_Heber() 'sender As Object, e As EventArgs)
        If InvokeRequired Then
            Dim d As New DelegateSub(AddressOf Change_Heber)
            Invoke(d, New Object() {})
        Else
            'If _pause Then
            '    ' wenn Leader.btnNext geklickt wird ohne Pause mit CLEAR zu beenden
            '    ZN_Input(vbCr)
            '    Set_ZA(Heber.CountDown)
            'End If
            Try
                Dim v As Boolean = Heber.Id > 0
                xTeilnehmer.Visible = v
                xVerein.Visible = v
                xAK.Visible = v
                xGewichtsklasse.Visible = v
                xDurchgang.Visible = v
                xHantelgewicht.Visible = v
                xVersuch.Visible = v
                xVersuchText.Visible = v
                picFlagge.Visible = False
                pnlHantel.Visible = False
                'KR_BlaueTaste()
                Set_ForeColors(False)
            Catch
            End Try
        End If
    End Sub

    Sub Set_AK(Optional ByVal Value As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_AK)
                Invoke(d, New Object() {Value})
            Else
                xAK.Text = Value & " " & Heber.Geschlecht
                xAK.SendToBack()
            End If
        Catch
        End Try
    End Sub
    Sub Set_Durchgang(value As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_Durchgang)
                Invoke(d, New Object() {value})
            Else
                xDurchgang.Text = value
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub Set_Flagge(Optional ByVal Staat As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_Flagge)
                Invoke(d, New Object() {Staat})
            Else
                With picFlagge
                    .Visible = False
                    xVerein.Left = xTeilnehmer.Left
                    If Wettkampf.Flagge = True AndAlso Staat > "" Then
                        Dim File As String = Path.Combine(Application.StartupPath, "flag", Staat)
                        Try
                            .ImageLocation = File
                            xVerein.Left = .Left + .Width + 20
                            .Visible = True
                        Catch ex As Exception
                        End Try
                    End If
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub Set_ForeColors(Optional Heber_Present As Boolean = True)
        If InvokeRequired Then
            Dim d As New DelegateSub0(AddressOf Set_ForeColors)
            Invoke(d, New Object() {Heber_Present})
        Else
            Try
                If Heber_Present Then
                    xTeilnehmer.ForeColor = Ansicht_Options.Name_Color
                    xVerein.ForeColor = Ansicht_Options.Verein_Color
                    xAK.ForeColor = Ansicht_Options.AK_Color
                    xGewichtsklasse.ForeColor = Ansicht_Options.AK_Color
                    xDurchgang.ForeColor = Ansicht_Options.Durchgang_Color
                    xHantelgewicht.ForeColor = Ansicht_Options.Durchgang_Color
                    xDurchgang.ForeColor = Ansicht_Options.Durchgang_Color
                    xVersuch.ForeColor = Ansicht_Options.Durchgang_Color
                    xVersuchText.ForeColor = Ansicht_Options.Durchgang_Color
                Else ' Wertungen zurücksetzen
                    For i = 1 To 3
                        xValid(i).ForeColor = Ansicht_Options.Shade_Color
                    Next
                    xTechniknote.ForeColor = Ansicht_Options.Shade_Color
                    xTechniknote.Text = "X,XX"
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Sub Change_ForeColors()
        If InvokeRequired Then
            Dim d As New DelegateSub(AddressOf Change_ForeColors)
            Invoke(d, New Object() {})
        Else
            Try
                xTeilnehmer.ForeColor = Ansicht_Options.Name_Color
                xVerein.ForeColor = Ansicht_Options.Verein_Color
                xAK.ForeColor = Ansicht_Options.AK_Color
                xGewichtsklasse.ForeColor = Ansicht_Options.AK_Color
                xDurchgang.ForeColor = Ansicht_Options.Durchgang_Color
                xHantelgewicht.ForeColor = Ansicht_Options.Durchgang_Color
                xDurchgang.ForeColor = Ansicht_Options.Durchgang_Color
                xVersuch.ForeColor = Ansicht_Options.Durchgang_Color
                xVersuchText.ForeColor = Ansicht_Options.Durchgang_Color
                Change_ShadeColors()
            Catch ex As Exception
            End Try
        End If
    End Sub
    Sub Change_ShadeColors()
        If InvokeRequired Then
            Dim d As New DelegateSub(AddressOf Change_ShadeColors)
            Invoke(d, New Object() {})
        Else
            For i = 1 To 3
                If xValid(i).ForeColor <> Color.Red AndAlso xValid(i).ForeColor <> Color.White Then xValid(i).ForeColor = Ansicht_Options.Shade_Color
            Next
            If xTechniknote.ForeColor <> Color.LimeGreen Then xTechniknote.ForeColor = Ansicht_Options.Shade_Color
        End If
    End Sub
    Sub Set_GK(Optional ByVal value As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_GK)
                Invoke(d, New Object() {value})
            Else
                xGewichtsklasse.Text = value
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub Edit_Hantelgewicht(HLast As Integer)
        xHantelgewicht.Text = HLast.ToString + " kg"
    End Sub
    Sub Set_Hantelgewicht(Value As Integer)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub3(AddressOf Set_Hantelgewicht)
                Invoke(d, New Object() {Value})
            Else
                With Heber
                    Draw_Hantel(.Hantellast, .Hantelstange) ' pnlHantel aktualisieren
                    xHantelgewicht.Text = .Hantellast.ToString + " kg"
                End With
            End If
        Catch
        End Try
    End Sub
    Sub Set_Teilnehmer(Optional ByVal Nachname As String = "", Optional ByVal Vorname As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub7(AddressOf Set_Teilnehmer)
                Invoke(d, New Object() {Nachname, Vorname})
            Else
                Dim value As String = String.Empty
                If Vorname.Equals("-1") Then
                    value = Nachname
                    xTeilnehmer.BringToFront()
                ElseIf String.IsNullOrEmpty(Nachname) And String.IsNullOrEmpty(Vorname) Then
                    value = String.Empty
                Else
                    Select Case Ansicht_Options.Name_Format
                        Case 0 'Nachname, Vorname (Standard-Formatierung)
                            value = Nachname + ", " + Vorname
                        Case 1 'Nachname, V.
                            value = Nachname + ", " + Strings.Left(Vorname, 1) + "."
                        Case 2 'Vorname Nachname
                            value = Vorname + " " + Nachname
                        Case 3 ' V. Nachname
                            value = Strings.Left(Vorname, 1) + ". " + Nachname
                    End Select
                End If
                xTeilnehmer.Text = value
                xTeilnehmer.SendToBack()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub Set_Verein(Optional ByVal value As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_Verein)
                Invoke(d, New Object() {value})
            Else
                xVerein.Text = value
                xVerein.SendToBack()
            End If
        Catch
        End Try
    End Sub
    Sub Set_Versuch(Optional ByVal value As String = "")
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Set_Versuch)
                Invoke(d, New Object() {value})
            Else
                xVersuch.Text = value + "."
            End If
        Catch ex As Exception
            'Stop
        End Try
    End Sub

    '' Me
    Private Sub frmBohle_Closed(sender As Object, e As EventArgs) Handles Me.Closed

        RemoveHandler Heber.Id_Changed, AddressOf Change_Heber
        RemoveHandler Heber.Name_Changed, AddressOf Set_Teilnehmer
        RemoveHandler Heber.Verein_Changed, AddressOf Set_Verein
        RemoveHandler Heber.Land_Changed, AddressOf Set_Verein
        RemoveHandler Heber.Flagge_Changed, AddressOf Set_Flagge
        RemoveHandler Heber.AK_Changed, AddressOf Set_AK
        RemoveHandler Heber.GK_Changed, AddressOf Set_GK
        RemoveHandler Heber.Versuch_Changed, AddressOf Set_Versuch
        RemoveHandler Heber.Hantellast_Changed, AddressOf Set_Hantelgewicht
        RemoveHandler Leader.Durchgang_Changed, AddressOf Set_Durchgang
        RemoveHandler Leader.Edit_Hantellast, AddressOf Edit_Hantelgewicht
        RemoveHandler Leader.Korrektur, AddressOf Set_Korrektur

        RemoveHandler Anzeige.Stunden_Changed, AddressOf Stunden_Changed
        RemoveHandler Anzeige.DplPunkt_Changed, AddressOf DplPunkt_Changed
        RemoveHandler Anzeige.Minuten_Changed, AddressOf Minuten_Changed
        RemoveHandler Anzeige.Sekunden_Changed, AddressOf Sekunden_Changed
        RemoveHandler Anzeige.Uhr_Color_Changed, AddressOf UhrColor_Changed
        RemoveHandler Anzeige.Gültig_Changed, AddressOf Gültig_Changed
        RemoveHandler Anzeige.Ungültig_Changed, AddressOf Ungültig_Changed
        RemoveHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        RemoveHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed
        RemoveHandler Anzeige.Technik_Visible_Changed, AddressOf TechnikVisible_Changed

        RemoveHandler Wettkampf.WKModus_Changed, AddressOf Change_WKModus
        RemoveHandler JuryMsg_Change, AddressOf Call_TimerMsg
        RemoveHandler ForeColors_Change, AddressOf Set_ForeColors
        'RemoveHandler Leader.Werbung_VisibleChanged, AddressOf pnlWerbung_VisibleChange
        RemoveHandler Clear_Hantel, AddressOf Hide_Hantel
        RemoveHandler Refresh_Bohle, AddressOf Bohle_Refresh

        RemoveHandler Ansicht_Options.Ansicht_Changed, AddressOf Change_ForeColors
        RemoveHandler Ansicht_Options.ShadeChanged, AddressOf Change_ShadeColors

        RemoveHandler Hantel.Draw_Bar, AddressOf Draw_Hantel

        PrivateFonts.Dispose()
    End Sub
    Private Sub frmBohle_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Using sc As New ScreenScale
            Dim dn = sc.GetDeviceNumber(Screen.FromControl(Me))
            For Each item In Screens
                If item.Value.Value = dn Then
                    Screens.Remove(item.Key)
                    Exit For
                End If
            Next
        End Using
        Try
            'If Wettkampf.WK_Modus <> Modus.Normal Then
            '    formEasyMode.Close()
            '    formHantelbeladung.Close()
            'End If
            'formHantel.Close()
        Catch ex As Exception
        End Try
        For i = 0 To Screens.Count - 1
            Try
                If Screens.ElementAt(i).Value.Key.Text.Equals("Hantelbeladung") Then
                    Screens.ElementAt(i).Value.Key.Close()
                    i -= 1
                End If
            Catch ex As Exception
            End Try
        Next

        WA_Message.Status = 73 ' = Bohle geschlossen
        KR_IsInitialized = False
        Cursor = Cursors.Default
        formMain.Cursor = Cursors.Default
    End Sub
    Private Sub frmBohle_Load(sender As Object, e As EventArgs) Handles Me.Load

        Cursor = Cursors.WaitCursor

        pnlWertung.Visible = False
        xTechniknote.Visible = False

        'Wertung
        Try
            xValid.Add(1, xGültig1)
            xValid.Add(2, xGültig2)
            xValid.Add(3, xGültig3)
        Catch ex As Exception
        End Try

        Try
            PrivateFonts.AddFontFile(Path.Combine(Application.StartupPath, "fnt", "digital-7 (mono).ttf"))
            PrivateFonts.AddFontFile(Path.Combine(Application.StartupPath, "fnt", "DS-DIGI.TTF"))
            PrivateFonts.AddFontFile(Path.Combine(Application.StartupPath, "fnt", "wingding.ttf"))
            fontFamilies = PrivateFonts.Families
            For i As Integer = 1 To 3
                xValid(i).Font = New Font(fontFamilies(2), xValid(i).Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            Next
            xTechniknote.Font = New Font(fontFamilies(1), xTechniknote.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf_std.Font = New Font(fontFamilies(0), xAufruf_min.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf_min.Font = New Font(fontFamilies(0), xAufruf_min.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf_sek.Font = New Font(fontFamilies(0), xAufruf_sek.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
            xAufruf_pkt.Font = New Font(fontFamilies(0), xAufruf_pkt.Font.Size, FontStyle.Bold, GraphicsUnit.Point)
            xAufruf_p_s.Font = New Font(fontFamilies(0), xAufruf_sek.Font.Size, FontStyle.Regular, GraphicsUnit.Point)
        Catch ex As Exception
        End Try

        If Wettkampf.WK_Modus > Modus.Normal Then Change_WKModus() ' falls EasyMode vor dem Öffnen der Bohle geöffnet wurde

        xAufruf_std.ForeColor = Anzeige.Clock_Colors(Clock_Status.Waiting)
        xAufruf_p_s.ForeColor = Anzeige.Clock_Colors(Clock_Status.Waiting)
        xAufruf_min.ForeColor = Anzeige.Clock_Colors(Clock_Status.Waiting)
        xAufruf_sek.ForeColor = Anzeige.Clock_Colors(Clock_Status.Waiting)
        xAufruf_pkt.ForeColor = Anzeige.Clock_Colors(Clock_Status.Waiting)

        xAufruf_std.Text = ""
        xAufruf_p_s.Text = ""
        xAufruf_min.Text = "0"
        xAufruf_sek.Text = "00"

        AddHandler Heber.Id_Changed, AddressOf Change_Heber
        AddHandler Heber.Name_Changed, AddressOf Set_Teilnehmer
        AddHandler Heber.Verein_Changed, AddressOf Set_Verein
        AddHandler Heber.Land_Changed, AddressOf Set_Verein
        AddHandler Heber.Flagge_Changed, AddressOf Set_Flagge
        AddHandler Heber.AK_Changed, AddressOf Set_AK
        AddHandler Heber.GK_Changed, AddressOf Set_GK
        AddHandler Heber.Versuch_Changed, AddressOf Set_Versuch
        AddHandler Heber.Hantellast_Changed, AddressOf Set_Hantelgewicht
        AddHandler Leader.Durchgang_Changed, AddressOf Set_Durchgang
        AddHandler Leader.Edit_Hantellast, AddressOf Edit_Hantelgewicht
        AddHandler Leader.Korrektur, AddressOf Set_Korrektur
        AddHandler Anzeige.Stunden_Changed, AddressOf Stunden_Changed
        AddHandler Anzeige.DplPunkt_Changed, AddressOf DplPunkt_Changed
        AddHandler Anzeige.Minuten_Changed, AddressOf Minuten_Changed
        AddHandler Anzeige.Sekunden_Changed, AddressOf Sekunden_Changed
        AddHandler Anzeige.Uhr_Color_Changed, AddressOf UhrColor_Changed
        AddHandler Anzeige.Gültig_Changed, AddressOf Gültig_Changed
        AddHandler Anzeige.Ungültig_Changed, AddressOf Ungültig_Changed
        AddHandler Anzeige.Technik_Wert_Changed, AddressOf TechnikWert_Changed
        AddHandler Anzeige.Technik_Color_Changed, AddressOf TechnikFarbe_Changed
        AddHandler Anzeige.Technik_Visible_Changed, AddressOf TechnikVisible_Changed

        AddHandler Wettkampf.WKModus_Changed, AddressOf Change_WKModus
        AddHandler JuryMsg_Change, AddressOf Call_TimerMsg
        AddHandler ForeColors_Change, AddressOf Set_ForeColors
        'AddHandler Leader.Werbung_VisibleChanged, AddressOf pnlWerbung_VisibleChange
        AddHandler Clear_Hantel, AddressOf Hide_Hantel
        AddHandler Refresh_Bohle, AddressOf Bohle_Refresh

        AddHandler Ansicht_Options.Ansicht_Changed, AddressOf Change_ForeColors
        AddHandler Ansicht_Options.ShadeChanged, AddressOf Change_ShadeColors

        AddHandler Hantel.Draw_Bar, AddressOf Draw_Hantel
    End Sub
    Private Sub frmBohle_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        pnlHantel.Visible = False
        Set_ForeColors()

        pnlUhr.Visible = True
        pnlWertung.Visible = True
        If Wettkampf.WK_Modus <> Modus.Normal Then Heber.Id = 0
        If Wettkampf.Technikwertung Then
            With xTechniknote
                .ForeColor = Ansicht_Options.Shade_Color
                .Text = "X,XX"
                .Visible = True
                .BringToFront()
            End With
        End If

        With Heber ' für ReLoad
            If Heber.Id > 0 Then
                .Id = .Id
                .Set_Name(.Nachname, .Vorname)
                .Verein = .Verein
                .Flagge = .Flagge
                .Geschlecht = .Geschlecht
                .AK = .AK
                .GK = .GK
                .Versuch = .Versuch
                .Hantellast = .Hantellast
                If .Wertung <> 0 Then Set_Korrektur(.Lampen)
                If Wettkampf.Technikwertung AndAlso .Wertung = 1 Then
                    TechnikFarbe_Changed(Color.Lime)
                    TechnikVisible_Changed(True)
                    TechnikWert_Changed(.T_Note.ToString("0.00"))
                End If
            End If
        End With

        Dummy.Focus()
        Cursor = Cursors.Default
        formMain.Cursor = Cursors.Default
    End Sub
    Private Sub Bohle_Refresh()
        If InvokeRequired Then
            Dim d As New DelegateSub(AddressOf Bohle_Refresh)
            Invoke(d, New Object() {})
        Else
            Refresh()
        End If
    End Sub

    Private Sub Change_WKModus()
        If Wettkampf.WK_Modus > Modus.Normal Then
            ' Anzeige für EasyModus ändern
            pnlUhr.SetBounds(453, 275, 298, 169)
            pnlWertung.SetBounds(45, 25, 361, 241)
            pnlHantel.SetBounds(419, 21, 367, 229)
            xAufruf_sek.SetBounds(161, 5, 145, 145)
            xAufruf_sek.Font = New Font(fontFamilies(0), 110)
            xAufruf_min.SetBounds(0, 5, 137, 145)
            xAufruf_min.Font = New Font(fontFamilies(0), 110)
            xAufruf_pkt.SetBounds(125, 17, 31, 106)
            xAufruf_pkt.Font = New Font(fontFamilies(0), 80)
            xTechniknote.SetBounds(22, 250, 372, 200)
            xTechniknote.Font = New Font(fontFamilies(1), 150)
            For i As Integer = 1 To 3
                With xValid(i)
                    .Font = New Font(fontFamilies(2), 100)
                    .SetBounds((i - 1) * 110, -18, 103, 148)
                End With
            Next
            xAufruf_p_s.Visible = False
            xAufruf_std.Visible = False
            '' Zoom-Faktor
            'Dim X = Width / MinimumSize.Width
            'Dim Y = Height / MinimumSize.Height
            '' Anzeige für EasyModus ändern
            'pnlUhr.SetBounds(CInt(453 * X), CInt(275 * Y), CInt(298 * X), CInt(169 * Y))
            'pnlWertung.SetBounds(CInt(45 * X), CInt(25 * Y), CInt(361 * X), CInt(241 * Y))
            'pnlHantel.SetBounds(CInt(419 * X), CInt(21 * Y), CInt(367 * X), CInt(229 * Y))
            'xAufruf_sek.SetBounds(CInt(161 * X), CInt(5 * Y), CInt(145 * X), CInt(145 * Y))
            'xAufruf_sek.Font = New Font(fontFamilies(0), 110)
            'xAufruf_min.SetBounds(CInt(0 * X), CInt(5 * Y), CInt(137 * X), CInt(145 * Y))
            'xAufruf_min.Font = New Font(fontFamilies(0), 110)
            'xAufruf_pkt.SetBounds(CInt(125 * X), CInt(17 * Y), CInt(31 * X), CInt(106 * Y))
            'xAufruf_pkt.Font = New Font(fontFamilies(0), 80)
            'xTechniknote.SetBounds(CInt(22 * X), CInt(250 * Y), CInt(372 * X), CInt(200 * Y))
            'xTechniknote.Font = New Font(fontFamilies(1), 150)
            'For i As Integer = 1 To 3
            '    With xValid(i)
            '        .Font = New Font(fontFamilies(2), 100)
            '        .SetBounds(CInt((i - 1) * 110 * X), CInt(-18 * Y), CInt(103 * X), CInt(148 * Y))
            '    End With
            '    With xInvalid(i)
            '        .Font = New Font(fontFamilies(2), 100)
            '        .SetBounds(CInt((i - 1) * 110 * X), CInt(103 * Y), CInt(103 * X), CInt(148 * Y))
            '    End With
            'Next

            'ElseIf Wettkampf.WK_Modus = Modus.Normal Then
            ' normaler Modus
        End If
    End Sub
    Private Sub Call_TimerMsg(Value As Boolean)
        With lblJury
            If Value Then
                .Left = 0
                .Width = Width
                .Visible = True
                .BringToFront()
                timerMsg_Running = True
                TimerMsg.Start()
            Else
                .Visible = False
                timerMsg_Running = False
                TimerMsg.Stop()
            End If
        End With
    End Sub
    Private Sub TimerMsg_Tick(sender As Object, e As ElapsedEventArgs) Handles TimerMsg.Elapsed
        If Not timerMsg_Running Then
            TimerMsg.Stop()
            lblJury.Visible = False
        Else
            With lblJury
                If .ForeColor = Color.Yellow Then
                    .BackColor = Color.Yellow
                    .ForeColor = Color.Red
                Else
                    .ForeColor = Color.Yellow
                    .BackColor = Color.Red
                End If
                .BringToFront()
            End With
        End If
    End Sub

    Private Sub Set_Korrektur(Lampen As String)
        For i = 1 To Lampen.Length
            Try
                If Lampen.Substring(i - 1, 1) = "5" Then
                    xValid(i).ForeColor = Color.White
                ElseIf Lampen.Substring(i - 1, 1) = "7" Then
                    xValid(i).ForeColor = Color.Red
                ElseIf Lampen.Substring(i - 1, 1) = "0" Then
                    xValid(i).ForeColor = Ansicht_Options.Shade_Color
                End If
            Catch ex As Exception
                xValid(i).ForeColor = Ansicht_Options.Shade_Color
            End Try
        Next
    End Sub

    '' pnlHantel aktualisieren
    Public Sub Draw_Hantel(Gewicht As Integer, Stange As String)
        Try
            xTechniknote.Visible = False
            pnlHantel.Controls.Clear()
            Dim pnl As New Panel
            pnl = Hantel.DrawHantel(pnlHantel.Width, pnlHantel.Height, Gewicht, Stange, Wettkampf.BigDisc)
            If Not IsNothing(pnl) Then
                pnl.Parent = pnlHantel
                pnl.Parent.Controls.Add(pnl)
                pnlHantel.Visible = True
                pnlHantel.BringToFront()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Hide_Hantel()
        If InvokeRequired Then
            Dim d As New DelegateSub(AddressOf Hide_Hantel)
            Invoke(d, New Object() {})
        Else
            pnlHantel.Visible = False
        End If
    End Sub

    '' Events der Anzeige
    Private Sub Stunden_Changed(Stunden As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Stunden_Changed)
                Invoke(d, New Object() {Stunden})
            Else
                xAufruf_std.Text = Stunden
            End If
        Catch
        End Try
    End Sub
    Private Sub DplPunkt_Changed(DplPunkt As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf DplPunkt_Changed)
                Invoke(d, New Object() {DplPunkt})
            Else
                xAufruf_p_s.Text = DplPunkt
            End If
        Catch
        End Try
    End Sub
    Private Sub Minuten_Changed(Minuten As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Minuten_Changed)
                Invoke(d, New Object() {Minuten})
            Else
                xAufruf_min.Text = Minuten
            End If
        Catch
        End Try
    End Sub
    Private Sub Sekunden_Changed(Sekunden As String)
        Try
            If InvokeRequired Then
                Dim d As New DelegateSub1(AddressOf Sekunden_Changed)
                Invoke(d, New Object() {Sekunden})
            Else
                xAufruf_sek.Text = Sekunden
            End If
        Catch
        End Try
    End Sub
    Private Sub UhrColor_Changed(Color As Color)
        xAufruf_std.ForeColor = Color
        xAufruf_p_s.ForeColor = Color
        xAufruf_min.ForeColor = Color
        xAufruf_sek.ForeColor = Color
        xAufruf_pkt.ForeColor = Color
    End Sub
    Private Sub Gültig_Changed(Gültig As Color, Index As Integer)
        xValid(Index).ForeColor = Gültig
    End Sub
    Private Sub Ungültig_Changed(Ungültig As Color, Index As Integer)
        xValid(Index).ForeColor = Ungültig
    End Sub
    Private Sub TechnikWert_Changed(Wert As String)
        If InvokeRequired Then
            Dim d As New DelegateSub1(AddressOf TechnikWert_Changed)
            Invoke(d, New Object() {Wert})
        Else
            xTechniknote.Text = Wert
        End If
    End Sub
    Private Sub TechnikFarbe_Changed(Farbe As Color)
        If InvokeRequired Then
            Dim d As New DelegateColor(AddressOf TechnikFarbe_Changed)
            Invoke(d, New Object() {Farbe})
        Else
            xTechniknote.ForeColor = Farbe
        End If
    End Sub
    Private Sub TechnikVisible_Changed(Value As Boolean)
        If InvokeRequired Then
            Dim d As New DelegateSub0(AddressOf TechnikVisible_Changed)
            Invoke(d, New Object() {Value})
        Else
            xTechniknote.Visible = Value
            If Value Then
                pnlHantel.Visible = False
                xTechniknote.BringToFront()
            End If
        End If
    End Sub

    Private Sub Control_GotFocus(sender As Object, e As EventArgs) Handles picFlagge.GotFocus, pnlHantel.GotFocus, pnlUhr.GotFocus, pnlWertung.GotFocus, xAufruf_min.GotFocus, xAufruf_pkt.GotFocus, xAufruf_sek.GotFocus, xAufruf_std.GotFocus, xAufruf_p_s.GotFocus,
        xGewichtsklasse.GotFocus, xGültig1.GotFocus, xGültig2.GotFocus, xGültig3.GotFocus, xTechniknote.GotFocus

        Dummy.Focus()
    End Sub

    'Private Sub pnlWerbung_VisibleChange(Value As Boolean)
    '    Try
    '        If InvokeRequired Then
    '            Dim d As New DelegateSub0(AddressOf pnlWerbung_VisibleChange)
    '            Invoke(d, New Object() {Value})
    '        Else
    '            With pnlWerbung
    '                .Visible = Value
    '                If Not .Visible Then
    '                    pnlUhr.SendToBack()
    '                    'Change_Heber()
    '                Else
    '                    .BringToFront()
    '                    .SetBounds(0, 0, ClientSize.Width, ClientSize.Height)
    '                    pnlUhr.BringToFront()
    '                End If
    '            End With
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub
    Private Sub xTeilnehmer_SizeChanged(sender As Object, e As EventArgs) Handles xTeilnehmer.SizeChanged
        xDurchgang.Visible = xTeilnehmer.Visible AndAlso xTeilnehmer.Left + xTeilnehmer.Width < xDurchgang.Left
    End Sub

End Class