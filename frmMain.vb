﻿
Imports System.Resources




Public Class frmMain
    Private Bereich As String = "Programm"
    Private Func As New myFunction
    Private def_Mon As Integer = 0
    Private UpdateFailed As Boolean = False

    Shared Event Change_ProgressBar(Value As Integer)

    Private Delegate Sub Delegate_String(Value As String)

    Dim WK_Idents As New Dictionary(Of String, String)

    Dim RM As New ResourceManager("Gewichtheben.WinFormStrings", GetType(frmMain).Assembly)

    Public Sub Change_MainProgressBarValue(Value As Integer)
        RaiseEvent Change_ProgressBar(Value)
    End Sub
    Private Sub ProgressBar_Changed(Value As Integer)
        With ProgressBar1
            .Value = Value
            .Visible = Value > 0
            Cursor = If(.Visible, Cursors.WaitCursor, Cursors.Default)
        End With
        Refresh()
    End Sub

    Private Function IsWettkampfAusgewählt(Caption As String) As Boolean

        If NoCompetition() Then
            Dim WK_ID = Func.Wettkampf_Auswahl(Me, , True)
            If IsNothing(WK_ID.Key) OrElse WK_ID.Key.Equals("0") Then Return False
            If Not Load_WettkampfProperties(WK_ID.Key) Then Return False
        End If

        If Not Caption.Equals("Online Punkte-Rechner") Then
            Get_Online(Caption)
        End If

        Return True

    End Function

    Private Sub Get_Online(Optional Caption As String = "")

        If AppConnState = ConnState.Connected OrElse
        AppConnState = ConnState.UserClosed OrElse
        AppConnState = ConnState.NotAvailable Then
            Return
        End If

        If User.UserId = UserID.Versuchsermittler Then
            AppConnState = ConnState.NotAvailable
        End If

        'If AppConnState = ConnState.Disconnected Then
        '    Update_Online()
        '    Return
        'End If

        If Not Caption.Equals("Online Punkte-Rechner") Then
            Using OS As New OnlineService
                If OS.Check_Wettkampf() = ConnState.Available Then
                    Using New Centered_MessageBox(Me)
                        If MessageBox.Show("Der Wettkampf ist in der Online-App verfügbar." & vbNewLine &
                                           "Datenübertragung für diesen Wettkampf jetzt aktivieren?" & vbNewLine & vbNewLine &
                                           "Diese Einstellung kann unter Datenbank/Online-Punktrechner jederzeit geändert werden.",
                                            msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                            mnuCalculator.PerformClick()
                        Else
                            AppConnState = ConnState.UserClosed
                        End If
                    End Using
                End If
            End Using
        End If
    End Sub

    Private Function Load_WettkampfProperties(WK_ID As String) As Boolean
        Dim Success As Boolean

        Success = Wettkampf.Set_Properties(Me, WK_ID) > 0
        Success = Success AndAlso Glob.Load_Wettkampf_Tables(Me, Wettkampf.Identities.Keys.ToArray)

        If Not Success Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Fehler beim Laden des Wettkampfs.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        End If

        Return Success
    End Function
    Private Function DeletionPermission(Part As String, Identities As Dictionary(Of String, String)) As Boolean
        Dim msg As String
        Dim bez = vbNewLine & "- " & Join(Identities.Values.ToArray, vbNewLine & "- ")

        If Part.Contains("Wettkampf") Then
            msg = "Der komplette Wettkampf " & bez & vbNewLine & " wird"
        Else
            msg = "Alle " & Part & " von " & bez & vbNewLine & " werden"
        End If
        msg += " gelöscht."

        Using New Centered_MessageBox(Me)
            If MessageBox.Show(msg, msgCaption, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) = DialogResult.OK Then
                Return True
            End If
        End Using

        Return False
    End Function

    Private Sub WK_BezeichungChanged(Bezeichnung As String)
        If InvokeRequired Then
            Dim d As New Delegate_String(AddressOf WK_BezeichungChanged)
            Invoke(d, New Object() {Bezeichnung})
        Else
            'lblWettkampf.Text = Bezeichnung
            lblWettkampf.Text = Join(Bezeichnung.Split(CChar(vbCrLf)), " | ")
        End If
    End Sub
    Private Sub Change_User()

        Dim bohle = String.Empty
        If User.Bohle > 0 Then bohle = " (Bohle " + User.Bohle.ToString + ")"

        Select Case User.UserId
            Case 0
                lblUser.Text = "Wettkampfleiter" + bohle
            Case 1
                lblUser.Text = "Versuchsermittler" + bohle
            Case 2
                lblUser.Text = "Administrator"
            Case 3
                lblUser.Text = "Jury" + bohle
            Case Else
                lblUser.Text = ""
        End Select
    End Sub
    Private Sub ConnError_Change(ConnErr As Boolean?)
        Select Case ConnErr
            Case False
                lblDBConnection.Image = ImageList1.Images("btnGreen.png")
            Case True
                lblDBConnection.Image = ImageList1.Images("btnRed.png")
            Case Else
                lblDBConnection.Image = ImageList1.Images("btnGrey.png")
        End Select
    End Sub

    '' Me
    Private Sub frmMain_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        RemoveHandler Wettkampf.Bezeichnung_Changed, AddressOf WK_BezeichungChanged
        RemoveHandler User.Bohle_Changed, AddressOf Change_User
        RemoveHandler User.User_Changed, AddressOf Change_User
        RemoveHandler ConnError_Changed, AddressOf ConnError_Change
        RemoveHandler Change_ProgressBar, AddressOf ProgressBar_Changed
        RemoveHandler AppConnectionChanged, AddressOf lblExtern_Update
        'RemoveHandler ExternCounter.Init, AddressOf lblExtern_Init
        'RemoveHandler ExternCounter.Change, AddressOf lblExtern_Change
        'RemoveHandler ExternCounter.Clear, AddressOf lblExtern_Finish

        Application.Exit()
    End Sub
    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        If e.CloseReason = CloseReason.ApplicationExitCall AndAlso Not User.UserId = UserID.Versuchsermittler Then Return
        If Not mnuHideMessages.Checked Then
            Using New Centered_MessageBox(Me)
                e.Cancel = MessageBox.Show("Soll die Anwendung beendet werden?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No
                If e.Cancel Then Return
            End Using
        End If
        NativeMethods.INI_Write("Programm", "HideMessages", mnuHideMessages.Checked.ToString, App_IniFile)
        'NativeMethods.INI_Write("Programm", "TopMost", mnuTopMost.Checked.ToString, App_IniFile)
        If Program_Options.SavePosition Then NativeMethods.INI_Write("Programm", "Location", Location.X & ";" & Location.Y & ";" & Width & ";" & Height & ";" & WindowState, App_IniFile)

    End Sub
    Public Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        Cursor = Cursors.WaitCursor

        AddHandler Wettkampf.Bezeichnung_Changed, AddressOf WK_BezeichungChanged
        AddHandler User.Bohle_Changed, AddressOf Change_User
        AddHandler User.User_Changed, AddressOf Change_User
        AddHandler ConnError_Changed, AddressOf ConnError_Change
        AddHandler Change_ProgressBar, AddressOf ProgressBar_Changed
        'AddHandler ExternCounter.Init, AddressOf lblExtern_Init
        'AddHandler ExternCounter.Change, AddressOf lblExtern_Change
        'AddHandler ExternCounter.Clear, AddressOf lblExtern_Finish
        AddHandler AppConnectionChanged, AddressOf lblExtern_Update
        Dim x = NativeMethods.INI_Read(Bereich, "HideMessages", App_IniFile)
        If x <> "?" Then
            mnuHideMessages.Checked = CBool(x)
            HideMessages = CBool(x)
        End If
        'x = NativeMethods.INI_Read(Bereich, "TopMost", App_IniFile)
        'If x <> "?" Then mnuTopMost.Checked = CBool(x)

        If Program_Options.AutoUpdate Then
            GetUpdateInfo()
        End If

    End Sub
    Private Async Sub GetUpdateInfo()
        Try
            Dim Response = Await CheckUpdates()
            If Response > 0 Then
                Using Updater As New frmUpdate
                    With Updater
                        .ShowDialog(Me)
                        If .DialogResult = DialogResult.Cancel Then UpdateFailed = True
                    End With
                End Using
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub frmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        'Teste Bildschirmauflösung (min. 1280x720)
        'If Screen.AllScreens(def_Mon).Bounds.Width < 1280 Or Screen.AllScreens(def_Mon).Bounds.Height < 720 Then
        If Screen.FromControl(Me).Bounds.Width < 1280 Or Screen.FromControl(Me).Bounds.Height < 720 Then
            MsgBox("Die Bildschirmauflösung muss mindestens 1280 x 720 betragen.", MsgBoxStyle.Critical, msgCaption)
            Application.Exit()
            Return
        End If

        If File.Exists(App_IniFile) Then
            ' Position und WindowState 
            Dim Pos() As String = Split(NativeMethods.INI_Read("Programm", "Location", App_IniFile), ";")
            If Pos(0).ToString <> "?" Then

                If CInt(Pos(1)) + CInt(Pos(3)) > Screen.FromControl(Me).Bounds.Height Or CInt(Pos(0)) + CInt(Pos(2)) > Screen.FromControl(Me).Bounds.Width Then
                    WindowState = FormWindowState.Maximized
                ElseIf CInt(Pos(1)) < 0 OrElse CInt(Pos(2)) < 0 Then
                    Location = New Point(0, 0)
                    WindowState = FormWindowState.Normal
                Else
                    Location = New Point(CInt(Pos(0)), CInt(Pos(1)))
                    WindowState = CType(CInt(Pos(4)), FormWindowState)
                End If
            End If
        End If

        Refresh()

        Using KeyGen As New FKeygen
            Dim Dlg As DialogResult = DialogResult.None
            With KeyGen
                Select Case .Check_LicenceInRegistry()
                    Case "Verified"
                        Using OS As New OnlineService
                            If OS.Verify_Licence(.Get_RegKeys()).Equals("Invalid") Then
                                If Not Register(KeyGen, "Die Lizenz für das Programm ist ungültig.").Equals("Verified") Then
                                    Application.Exit()
                                End If
                            End If
                        End Using
                    Case "Test"
                        ' Test-Zeitraum prüfen
                        Dim Decrypter As New Crypter("GFHsoft")
                        Dim Valid = CDate(Decrypter.DecryptData(.Get_RegKeys("Valid")))
                        If Valid < Now Then
                            If Not Register(KeyGen, "Der Test-Zeitraum ist abgelaufen.").Equals("Verified") Then
                                Application.Exit()
                            End If
                        End If
                    Case "Invalid"
                        If Not Register(KeyGen, "Die Lizenz für das Programm ist ungültig.").Equals("Verified") Then
                            Application.Exit()
                        End If
                End Select
            End With
        End Using

        Using formAnmeldung As New frmAnmeldung
            With formAnmeldung
                .OwnerForm = Me
                .Height = .MinimumSize.Height
                .ShowDialog(Me)
                If .DialogResult = DialogResult.Cancel Or .DialogResult = DialogResult.Abort Then
                    Cursor = Cursors.WaitCursor
                    Application.Exit()
                    Return
                End If
            End With
        End Using

        If Not UpdateFailed Then
            Update_Database(True)
        Else
            UpdateFailed = False
        End If

        Select Case User.UserId
            Case UserID.Versuchsermittler
                mnuCalculator.Enabled = False
                Wettkampf.ID = -1
                mnuGewichtheben.PerformClick()

                'Case UserID.Jury
                '    Cursor = Cursors.WaitCursor
                '    With frmJury
                '        .ShowDialog(Me)
                '        .Dispose()
                '    End With
                '    Cursor = Cursors.Default
        End Select

        Cursor = Cursors.Default

    End Sub

    Private Function Register(KeyGen As FKeygen, Optional Msg As String = "") As String
        If Not String.IsNullOrEmpty(Msg) Then
            Using New Centered_MessageBox(Me, {"Lizenz", "Beenden"})
                If MessageBox.Show(Msg, msgCaption,
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning) = DialogResult.Cancel Then
                    'Application.Exit()
                    Return "Invalid"
                End If
            End Using
        End If
        With KeyGen
            .ShowDialog(Me)
            If .Status.Equals("Success") Then
                Using OS As New OnlineService
                    Select Case OS.Verify_Licence(.Get_RegKeys)
                        Case "Verified"
                            Return "Verified"
                        Case "Pending"
                            If OS.Set_Activation(.Get_RegKeys()) Then
                                Return "Verified"
                            End If
                    End Select
                End Using
            End If
        End With
        Return "Invalid"
    End Function

    Private Function Update_Database(Optional Silent As Boolean = False) As Boolean

        Dim Icon = MessageBoxIcon.Information

        Dim Response = Database_Update(Me)

        If Response.Key < 1 Then Icon = MessageBoxIcon.Warning

        If Not String.IsNullOrEmpty(Response.Value) Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show(Response.Value, msgCaption, MessageBoxButtons.OK, Icon)
            End Using
        End If

        If Not IsNothing(ConnError) AndAlso Not Glob.Load_Global_Tables(Me) Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Die globalen Daten wurden nicht geladen." & vbNewLine &
                                "Die Anwendung wird beendet." & StrDup(2, vbNewLine) &
                                "Sollte dieser Fehler wiederholt auftreten, wenden Sie sich bitte an den Hersteller.",
                                msgCaption,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error)
            End Using
            Application.Exit()
        End If

        Return Response.Key = 1
    End Function

    '' Menü Datei
    Private Sub mnuLogin_Click(sender As Object, e As EventArgs) Handles mnuLogin.Click
        Using formAnmeldung As New frmAnmeldung
            With formAnmeldung
                .Lock = Nothing
                .OwnerForm = Me
                .btnAnmelden.Text = "Übernehmen"
                .btnServer.Visible = False
                .Height = .MaximumSize.Height
                .ShowDialog(Me)
                If .DialogResult = DialogResult.Cancel OrElse .DialogResult = DialogResult.Abort Then Return
            End With
        End Using

        If Not Glob.Load_Global_Tables(Me) Then
            Using New Centered_MessageBox(Me, {"Prüfen", "Beenden"})
                If MessageBox.Show("Die globalen Daten wurden nicht geladen." & StrDup(2, vbNewLine) &
                                   "Mögliche Ursache ist ein fehlendes Datenbank-Update.",
                                   msgCaption,
                                   MessageBoxButtons.OKCancel,
                                   MessageBoxIcon.Error) = DialogResult.Cancel Then
                    Application.Exit()
                End If
            End Using
            Update_Database()
        End If

        If User.UserId = UserID.Versuchsermittler Then
            Wettkampf.ID = -1
            mnuGewichtheben.PerformClick()
            'ElseIf User.UserId = UserID.Jury Then
            '    Cursor = Cursors.WaitCursor
            '    With frmJury
            '        .ShowDialog(Me)
            '        .Dispose()
            '    End With
            '    Cursor = Cursors.Default
        End If

    End Sub
    Private Sub mnuSperren_Click(sender As Object, e As EventArgs) Handles mnuSperren.Click
        Lock_Application(Me)
    End Sub
    Private Sub mnuBeenden_Click(sender As Object, e As EventArgs) Handles mnuBeenden.Click
        Close()
    End Sub


    '' Menü Vorbereitung - WK
    Private Sub mnuWK_New_Click(sender As Object, e As EventArgs) Handles mnuWK_New.Click

        Dim WK_ID As String = "0"

        Cursor = Cursors.WaitCursor

        If NoCompetition() Then
            Wettkampf.Clear()
            Glob.Load_Wettkampf_Tables(Me, {"0"}) ' alle WK-spezifischen Tables und DataViews leeren
        End If

        With frmWettkampf
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 65)
            .WK_ID = WK_ID
            .ShowDialog(Me)
            If Not .DialogResult = DialogResult.Cancel Then WK_ID = .WK_ID
            .Dispose()
        End With

        If Not WK_ID.Equals("0") Then Load_WettkampfProperties(WK_ID)

        Cursor = Cursors.Default
    End Sub
    Private Sub mnuWK_Edit_Click(sender As Object, e As EventArgs) Handles mnuWK_Edit.Click

        Dim Caption = RM.GetString("WK_Edit")

        Dim WK As KeyValuePair(Of String, String)

        If NoCompetition() OrElse Wettkampf.Identities.Count > 1 Then
            WK = Func.Wettkampf_Auswahl(Me, Caption & " bearbeiten",, True)
            If IsNothing(WK.Key) OrElse WK.Key.Equals("0") Then Return
            Glob.Load_Wettkampf_Tables(Me, {WK.Key})
        Else
            WK = Wettkampf.Identities.AsEnumerable(0)
        End If

        Cursor = Cursors.WaitCursor

        With frmWettkampf
            .Text = UCase(Caption) & " (" & WK.Value & ")"
            .Location = New Point(Bounds.Left + 1, Bounds.Top + 65)
            .WK_ID = WK.Key
            .ShowDialog(Me)
            If .DialogResult = DialogResult.Cancel Then WK = New KeyValuePair(Of String, String)("0", String.Empty)
            .Dispose()
        End With

        Cursor = Cursors.Default

        If WK.Key.Equals("0") Then Return

        Using ds As New DataService
            Dim Identities = ds.Get_Wettkampf_Identities(WK.Key)
            If Not Identities.Equals(Wettkampf.Identities) Then
                ' der bearbeitete WK ist NICHT geladen
                Using New Centered_MessageBox(Me)
                    If MessageBox.Show("Diesen Wettkampf laden?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        Load_WettkampfProperties(WK.Key)
                    End If
                End Using
            End If
        End Using
    End Sub
    Private Sub mnuWK_Select_Click(sender As Object, e As EventArgs) Handles mnuWK_Select.Click
        Dim WK_ID = Func.Wettkampf_Auswahl(Me,, True)
        If IsNothing(WK_ID.Key) OrElse WK_ID.Key = "0" Then Return
        If Load_WettkampfProperties(WK_ID.Key) Then
            WK_Idents = Wettkampf.Identities
            Get_Online()
        End If
    End Sub
    Private Sub mnuWK_Delete_Click(sender As Object, e As EventArgs) Handles mnuWK_Delete.Click

        Using ds As New DataService
            If NoCompetition() Then
                Dim WK_ID = Func.Wettkampf_Auswahl(Me, "Wettkampf löschen", True, True)
                If IsNothing(WK_ID.Key) OrElse WK_ID.Key.Equals("0") Then Return
                WK_Idents = ds.Get_Wettkampf_Identities(WK_ID.Key)
            Else
                WK_Idents = Wettkampf.Identities
            End If
        End Using

        If Not DeletionPermission("Kompletten Wettkampf", WK_Idents) Then
            WK_Idents.Clear()
            Return
        End If

        Cursor = Cursors.WaitCursor

        Using conn As New MySqlConnection(User.ConnString)
            Dim Query = "Delete From wettkampf                  Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From meldung                       Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From gruppen                       Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From gruppen_kampfrichter          Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From reissen                       Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From stossen                       Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From results                       Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From athletik                      Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From athletik_results              Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_altersgruppen       Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_athletik            Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_athletik_defaults   Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_bezeichnung         Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_details             Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_gewichtsgruppen     Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_gewichtsteiler      Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_mannschaft          Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_meldefrist          Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_restriction         Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_teams               Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_wertung             Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From wettkampf_zeitplan            Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"

            Dim cmd As MySqlCommand
            cmd = New MySqlCommand(Query, conn)
            Try
                conn.Open()
                cmd.ExecuteNonQuery()
            Catch ex As Exception
            End Try
        End Using

        If AppConnState = ConnState.Connected Then
            Using conn As New MySqlConnection(User.APP_ConnString)
                Dim Query = "Delete From wettkampf Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
                Query += "Delete From meldung      Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
                Query += "Delete From gruppen      Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
                Query += "Delete From reissen      Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
                Query += "Delete From stossen      Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
                Query += "Delete From results      Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"

                Dim cmd As MySqlCommand
                cmd = New MySqlCommand(Query, conn)
                Try
                    conn.Open()
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                End Try
            End Using
        End If

        If WK_Idents.Equals(Wettkampf.Identities) Then
            ' aktiven WK löschen
            Wettkampf.Clear()
            Glob.Load_Wettkampf_Tables(Me, {"0"}) ' alle WK-spezifischen Tables und DataViews leeren
            Try
                Leader.RowNum_Offset = 0
                dvResults = Nothing
                dtResults = Nothing
                dvLeader(0) = Nothing
                dvLeader(1) = Nothing
            Catch ex As Exception
            End Try
        End If

        WK_Idents.Clear()

        Cursor = Cursors.Default
    End Sub
    Private Sub mnuClearMeldung_Click(sender As Object, e As EventArgs) Handles mnuClearMeldung.Click

        Using ds As New DataService
            If NoCompetition() Then
                Dim WK_ID = Func.Wettkampf_Auswahl(Me, "Meldungen und Resultate löschen", True, True)
                If IsNothing(WK_ID.Key) OrElse WK_ID.Key.Equals("0") Then Return
                WK_Idents = ds.Get_Wettkampf_Identities(WK_ID.Key)
            Else
                WK_Idents = Wettkampf.Identities
            End If
        End Using

        If Not DeletionPermission("Meldungen und Resultate", WK_Idents) Then
            WK_Idents.Clear()
            Return
        End If

        Cursor = Cursors.WaitCursor

        Dim Query = "Delete From meldung Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"

        Using conn As New MySqlConnection(User.ConnString)
            Dim cmd As MySqlCommand
            cmd = New MySqlCommand(Query, conn)
            Try
                conn.Open()
                cmd.ExecuteNonQuery()
            Catch ex As Exception
            End Try
        End Using

        If AppConnState = ConnState.Connected Then
            Using conn As New MySqlConnection(User.APP_ConnString)
                Dim cmd As MySqlCommand
                cmd = New MySqlCommand(Query, conn)
                Try
                    conn.Open()
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                End Try
            End Using
        End If

        mnuClearHeben.PerformClick()

        Cursor = Cursors.Default
    End Sub
    Private Sub mnuClearWiegen_Click(sender As Object, e As EventArgs) Handles mnuClearWiegen.Click

        If WK_Idents.Count = 0 Then
            Using ds As New DataService
                If NoCompetition() Then
                    Dim WK_ID = Func.Wettkampf_Auswahl(Me, "Wiege-Ergebisse und Resultate löschen", True, True)
                    If IsNothing(WK_ID.Key) OrElse WK_ID.Key.Equals("0") Then Return
                    WK_Idents = ds.Get_Wettkampf_Identities(WK_ID.Key)
                Else
                    WK_Idents = Wettkampf.Identities
                End If
            End Using
        End If

        If Not DeletionPermission("Wiege-Ergebnisse", WK_Idents) Then
            WK_Idents.Clear()
            Return
        End If

        Cursor = Cursors.WaitCursor

        Dim Query = "update meldung 
                     set Laenderwertung = 0, 
                         Vereinswertung = null, 
                         Relativ_W = 0, 
                         a_K = 0, 
                         Wiegen = null, 
                         Startnummer = null, 
                         idGK = if(isnull(Gewicht), null, idGK), 
                         GK = if(isnull(Gewicht), null, GK), 
                         Reissen = null, 
                         Stossen = null, 
                         Zweikampf = null,
                         attend = null 
                    Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"

        Dim cmd As MySqlCommand
        Using conn As New MySqlConnection(User.ConnString)
            cmd = New MySqlCommand(Query, conn)
            Try
                conn.Open()
                cmd.ExecuteNonQuery()
                ConnError = False
            Catch ex As MySqlException
                MySQl_Error(Me, ex.Message, ex.StackTrace)
            End Try
        End Using

        If AppConnState = ConnState.Connected Then
            Query = "update meldung 
                     set a_K = 0, 
                         Wiegen = null, 
                         Startnummer = null, 
                         idGK = if(isnull(Gewicht), null, idGK), 
                         GK = if(isnull(Gewicht), null, GK), 
                         attend = null 
                    Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Using conn As New MySqlConnection(User.APP_ConnString)
                cmd = New MySqlCommand(Query, conn)
                Try
                    conn.Open()
                    cmd.ExecuteNonQuery()
                Catch ex As MySqlException
                    MySQl_Error(Me, ex.Message, ex.StackTrace)
                End Try
            End Using
        End If

        mnuClearHeben.PerformClick()

        Cursor = Cursors.Default

    End Sub
    Private Sub mnuClearHeben_Click(sender As Object, e As EventArgs) Handles mnuClearHeben.Click

        If WK_Idents.Count = 0 Then
            Using ds As New DataService
                If NoCompetition() Then
                    Dim WK_ID = Func.Wettkampf_Auswahl(Me, "Resultate löschen", True, True)
                    If IsNothing(WK_ID.Key) OrElse WK_ID.Key.Equals("0") Then Return
                    WK_Idents = ds.Get_Wettkampf_Identities(WK_ID.Key)
                Else
                    WK_Idents = Wettkampf.Identities
                End If
            End Using
        End If

        If Not DeletionPermission("Resultate", WK_Idents) Then
            WK_Idents.Clear()
            Return
        End If

        Cursor = Cursors.WaitCursor

        Dim cmd As MySqlCommand
        Using conn As New MySqlConnection(User.ConnString)
            Dim Query = "Delete From reissen Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From stossen Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From results Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From athletik Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Delete From athletik_results Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            Query += "Update wettkampf_teams Set Punkte = null, Reissen = null, Stossen = null, Heben = null, Platz = null, Sieger = False Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
            cmd = New MySqlCommand(Query, conn)
            Try
                conn.Open()
                cmd.ExecuteNonQuery()
                ConnError = False
            Catch ex As MySqlException
                MySQl_Error(Me, ex.Message, ex.StackTrace)
            End Try
        End Using

        If AppConnState = ConnState.Connected Then
            Using conn As New MySqlConnection(User.APP_ConnString)
                Dim Query = "Delete From reissen Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
                Query += "Delete From stossen Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
                Query += "Delete From results Where " & Wettkampf.WhereClause(, WK_Idents.Keys.ToArray) & ";"
                cmd = New MySqlCommand(Query, conn)
                Try
                    conn.Open()
                    cmd.ExecuteNonQuery()
                    ConnError = False
                Catch ex As MySqlException
                    MySQl_Error(Me, ex.Message, ex.StackTrace)
                End Try
            End Using
        End If

        Cursor = Cursors.Default

        If ConnError Then Return

        If WK_Idents.Equals(Wettkampf.Identities) Then
            Try
                Leader.RowNum_Offset = 0
                dvResults = Nothing
                dtResults = Nothing
                dvLeader(0) = Nothing
                dvLeader(1) = Nothing
                Glob.Load_Wettkampf_Tables(Me, WK_Idents.Keys.ToArray)
            Catch ex As Exception
            End Try
        Else
            Using New Centered_MessageBox(Me)
                If MessageBox.Show("Diesen Wettkampf jetzt laden?", msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    Load_WettkampfProperties(WK_Idents.AsEnumerable(0).Key)
                End If
            End Using
        End If

        'WK_Idents.Clear()

    End Sub

    '' Menü Vorbereitung
    Private Sub mnuMeldung_Click(sender As Object, e As EventArgs) Handles mnuMeldung.Click
        Dim Caption = "Meldungen"

        If Not IsWettkampfAusgewählt(Caption) Then Return

        Cursor = Cursors.WaitCursor

        With frmMeldung
            .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 60)
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = DefaultCursor
    End Sub
    Private Sub mnuGruppen_Click(sender As Object, e As EventArgs) Handles mnuGruppen.Click
        Dim Caption = "Gruppen"

        If Not IsWettkampfAusgewählt(Caption) Then Return

        Cursor = Cursors.WaitCursor

        With frmGruppen
            .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
            .Location = New Point(Bounds.Left + 1, Bounds.Top + 57)
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = Cursors.Default
    End Sub
    Private Sub mnuZeitplan_Click(sender As Object, e As EventArgs) Handles mnuZeitplan.Click
        Dim Caption = "Zeitplan"

        If Not IsWettkampfAusgewählt(Caption) Then Return

        Cursor = Cursors.WaitCursor

        With frmZeitplan
            .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
            .Location = New Point(Bounds.Left + 15, Bounds.Top + 60)
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = Cursors.Default

    End Sub
    Private Sub mnuKampfgericht_Click(sender As Object, e As EventArgs) Handles mnuKampfgericht.Click
        Dim Caption = "Kampfgericht"

        If Not IsWettkampfAusgewählt(Caption) Then Return

        Cursor = Cursors.WaitCursor

        With frmKampfgericht
            .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = Cursors.Default
    End Sub
    Private Sub mnuWiegen_Click(sender As Object, e As EventArgs) Handles mnuWiegen.Click
        Dim Caption = "Wiegelisten"

        If Not IsWettkampfAusgewählt(Caption) Then Return

        Cursor = Cursors.WaitCursor

        With frmWiegen
            .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
            '.MdiParent = Me
            '.Show()
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = DefaultCursor
    End Sub

    '' Menü Vorbereitung - Drucken
    Private Sub mnuMeldeliste_Click(sender As Object, e As EventArgs) Handles mnuMeldeliste.Click, mnuZeitplan_Print.Click

        Dim Caption = CType(sender, ToolStripMenuItem).Text
        'Caption += " " & "drucken"

        If Not IsWettkampfAusgewählt(Caption) Then Return

        Glob.Refresh_ResultsTable() ' dtResults neu einlesen

        Cursor = Cursors.WaitCursor

        With Drucken_Meldeliste
            .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
            .Bereich = CType(sender, ToolStripMenuItem).Tag.ToString
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = Cursors.Default

    End Sub
    Private Sub mnuQuittungen_Click(sender As Object, e As EventArgs) Handles mnuQuittungen.Click

        Dim Caption = CType(sender, ToolStripMenuItem).Text
        'Caption += " " & "drucken"

        If Not IsWettkampfAusgewählt(Caption) Then Return

        Cursor = Cursors.WaitCursor

        With Drucken_Quittung
            .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
            '.Tag = Caption
            .Bereich = CType(sender, ToolStripMenuItem).Tag.ToString
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = Cursors.Default
    End Sub
    Private Sub mnuStartkarten_Click(sender As Object, e As EventArgs) Handles mnuStartkarten.Click

        Dim Caption = CType(sender, ToolStripMenuItem).Text
        'Caption += " " & "drucken"

        If Not IsWettkampfAusgewählt(Caption) Then Return

        Glob.Refresh_ResultsTable() ' dtResults neu einlesen

        Cursor = Cursors.WaitCursor

        With Drucken_Startkarten
            .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
            '.Tag = Caption
            .Bereich = CType(sender, ToolStripMenuItem).Tag.ToString
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = Cursors.Default
    End Sub
    Private Sub mnuWiegelisten_Click(sender As Object, e As EventArgs) Handles mnuWiegelisten.Click, mnuGruppenlisten.Click, mnuStarterlisten.Click

        Dim Caption = CType(sender, ToolStripMenuItem).Text
        'Caption += " " & "drucken"

        If Not IsWettkampfAusgewählt(Caption) Then Return

        Glob.Refresh_ResultsTable() ' dtResults neu einlesen

        Cursor = Cursors.WaitCursor
        Try
            With Drucken_Gruppenliste
                .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
                .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
                '.Tag = Replace(Caption, "listen", "liste")
                .Bereich = CType(sender, ToolStripMenuItem).Tag.ToString
                .ShowDialog(Me)
                .Dispose()
            End With
        Catch ex As Exception
            LogMessage("Main: mnuWiegelisten: " & ex.Message, ex.StackTrace)
        End Try

        Cursor = DefaultCursor
    End Sub

    '' Menü Gewichtheben
    Private Sub mnuGewichtheben_Click(sender As Object, e As EventArgs) Handles mnuGewichtheben.Click

        If Not IsWettkampfAusgewählt(String.Empty) Then Return

        Cursor = Cursors.WaitCursor

        'Glob.Refresh_ResultsTable(True)

        If IsNothing(formLeader) OrElse formLeader.IsDisposed Then formLeader = New frmLeitung 'frmLeader
        With formLeader
            .MdiParent = Me
            .Show()
            .BringToFront()
        End With
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuKorrektur_Click(sender As Object, e As EventArgs) Handles mnuKorrektur.Click

        If Not IsWettkampfAusgewählt(String.Empty) Then Return

        Cursor = Cursors.WaitCursor

        With frmKorrektur
            .Location = New Point(120, 60)
            '.MdiParent = Me
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuEasyModus_Click(sender As Object, e As EventArgs) Handles mnuEasyModus.Click
        Cursor = Cursors.WaitCursor
        Try
            formEasyMode.Dispose()
        Catch ex As Exception
        End Try

        If IsNothing(formEasyMode) OrElse formEasyMode.IsDisposed Then formEasyMode = New frmEasyMode
        With formEasyMode
            '.Location = New Point(Bounds.Left + 20, Bounds.Top + 60)
            .MdiParent = Me
            .Show()
            '.ShowDialog(Me)
            '.Dispose()
        End With
        Cursor = Cursors.Default
    End Sub
    Private Sub PauseToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuPause.Click
        frmPause.Show()
    End Sub

    '' Menü Athletik
    Private Sub mnuAthletik_Click(sender As Object, e As EventArgs) Handles mnuAthletik.Click

        If Not IsWettkampfAusgewählt(String.Empty) Then Return

        Cursor = Cursors.WaitCursor

        With frmAthletik
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 65)
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub

    '' Menü Auswertung
    Private Sub mnuUrkunden_Click(sender As Object, e As EventArgs) Handles mnuUrkunden.Click

        If Not IsWettkampfAusgewählt(String.Empty) Then Return

        Cursor = Cursors.WaitCursor

        With Auswertung_Urkunden
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 65)
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuProtokoll_Click(sender As Object, e As EventArgs) Handles mnuProtokoll.Click

        If Not IsWettkampfAusgewählt(String.Empty) Then Return

        Cursor = Cursors.WaitCursor

        With Drucken_Protokoll
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = Cursors.Default
    End Sub
    Private Sub mnuLänderwertung_Click(sender As Object, e As EventArgs) Handles mnuLänderwertung.Click, mnuVereinswertung.Click, mnuMannschaftswertung.Click

        Dim Caption = CType(sender, ToolStripMenuItem).Tag.ToString

        If NoCompetition() Then
            Dim WK_ID = Func.Wettkampf_Auswahl(Me)
            If IsNothing(WK_ID.Key) OrElse WK_ID.Key.Equals("0") Then Return
            If Not Load_WettkampfProperties(WK_ID.Key) Then Return
        ElseIf Wettkampf.Identities.Count > 1 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Mehr als ein Wettkampf ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End Using
        End If

        Cursor = Cursors.WaitCursor

        With Auswertung_Länder
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 65)
            .Tag = Caption
            '.Bereich = Caption
            .Text = Caption
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuLänderwertung_VisibleChanged(sender As Object, e As EventArgs) Handles mnuLänderwertung.VisibleChanged, mnuVereinswertung.VisibleChanged
        ' div1.Visible = mnuLänderwertung.Visible Or mnuVereinswertung.Visible
    End Sub

    '' Menü Anzeige
    Private Sub mnuAnzeige_Click(sender As Object, e As EventArgs) Handles mnuAnzeige.Click
        Cursor = Cursors.WaitCursor
        With frmScreens
            .Location = New Point(Width \ 2 - .Width, (Height - .Height) \ 2)
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub

    '' Menü Datenbank - Export
    Private Sub mnuExport_Ergebnisse_Click(sender As Object, e As EventArgs) Handles mnuExport_Ergebnisse.Click
        'If Wettkampf.ID = 0 Then
        '    If Not Func.Wettkampf_Auswahl(Me, "Ergebnisse Meisterschaft exportieren") Then Return
        'End If
        'Cursor = Cursors.WaitCursor
        'With Export_Ergebnisse
        '    .Text += Wettkampf.Bezeichnung
        '    .Location = New Point(Bounds.Left + 300, Bounds.Top + 80)
        '    .ShowDialog(Me)
        '    .Dispose()
        'End With
        'Cursor = DefaultCursor
    End Sub
    Private Sub mnuExport_Wiegeliste_Click(sender As Object, e As EventArgs) Handles mnuExport_Wiegeliste.Click
        Dim Caption = "Wiegeliste"

        If Not IsWettkampfAusgewählt(Caption) Then Return

        Cursor = Cursors.WaitCursor

        With frmExport_Wiegeliste
            .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = DefaultCursor
    End Sub
    Private Sub mnuExport_Bundesliga_Click(sender As Object, e As EventArgs) Handles mnuExport_Bundesliga.Click

        Dim Caption = "Bundesliga"

        If Not IsWettkampfAusgewählt(Caption) Then Return

        Cursor = Cursors.WaitCursor

        For Each Ident In Wettkampf.Identities
            With BVDG
                .Text = UCase(Caption) & " (" & Ident.Value & ")"
                .Target = New KeyValuePair(Of String, String)("Export", "Bundesliga")
                .WK_ID = Ident.Key
                .ShowDialog(Me)
                .Dispose()
            End With
        Next

        Cursor = DefaultCursor
    End Sub
    Private Sub mnuExportMeisterschaft_Click(sender As Object, e As EventArgs) Handles mnuExportMeisterschaft.Click
    End Sub

    '' Menü Datenbank - Import
    Private Sub mnuImport_Meldeliste_Click(sender As Object, e As EventArgs) Handles mnuImport_Meldeliste.Click
        Dim Caption = "Meldeliste"

        If NoCompetition() Then
            Dim WK_ID = Func.Wettkampf_Auswahl(Me)
            If IsNothing(WK_ID.Key) OrElse WK_ID.Key.Equals("0") Then Return
            If Not Load_WettkampfProperties(WK_ID.Key) Then Return
        ElseIf Wettkampf.Identities.Count > 1 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Mehr als ein Wettkampf ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End Using
        End If

        Cursor = Cursors.WaitCursor

        'With Import_Meldeliste
        With Import_Heber
            .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
            .Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = DefaultCursor
    End Sub
    Private Sub mnuImport_Meisterschaft_Click(sender As Object, e As EventArgs) Handles mnuImport_Meisterschaft.Click
        Dim Caption = "Meisterschaft"

        If NoCompetition() Then
            Dim WK_ID = Func.Wettkampf_Auswahl(Me, Caption & " importieren")
            If IsNothing(WK_ID.Key) OrElse WK_ID.Key.Equals("0") Then Return
            If Not Load_WettkampfProperties(WK_ID.Key) Then Return
        ElseIf Wettkampf.Identities.Count > 1 Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show("Mehr als ein Wettkampf ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End Using
        End If

        Cursor = Cursors.WaitCursor

        With BVDG
            .Text = UCase(Caption) & " (" & Join(Wettkampf.Identities.Values.ToArray, ", ") & ")"
            .Target = New KeyValuePair(Of String, String)("Import", "Meisterschaft")
            '.Location = New Point(Bounds.Left + 30, Bounds.Top + 80)
            .ShowDialog(Me)
            .Dispose()
        End With

        Cursor = DefaultCursor
    End Sub
    Private Sub mnuImport_Bundesliga_Click(sender As Object, e As EventArgs) Handles mnuImport_Bundesliga.Click

        Dim Caption = "Import Bundesliga"

        Dim Idents As New Dictionary(Of String, String)
        Dim WK_ID As String = String.Empty

        If Not NoCompetition() Then
            Idents = Wettkampf.Identities

            Dim bez = "Der Import erfolgt in:" & vbNewLine & "     - " & Join(Idents.Values.ToArray, vbNewLine & "     - ") &
                vbNewLine & vbNewLine & "Abbrechen, um Auswahl zu ändern."

            Using New Centered_MessageBox(Me, {"Bestätigen", "Neu"})
                Select Case MessageBox.Show(bez, msgCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information)
                    Case DialogResult.Yes
                        'in ausgewählten WK importieren
                    Case DialogResult.No
                        'ausgewählten WK entladen
                        Wettkampf.Clear()
                        Idents.Clear()
                        Idents("0") = "Neuer Wettkampf"
                    Case DialogResult.Cancel
                        ' WK-Auswahl aufrufen
                        Return
                End Select
            End Using
        Else
            Idents("0") = "Neuer Wettkampf"
        End If

        Cursor = Cursors.WaitCursor
        Dim OpenMeldung = False

        For Each Ident In Idents
            With BVDG
                .Text = UCase(Caption) & " (" & Ident.Value & ")"
                .Target = New KeyValuePair(Of String, String)("Import", "Bundesliga")
                .WK_ID = Ident.Key
                .ShowDialog(Me)
                If Not .DialogResult = DialogResult.Cancel Then WK_ID = .WK_ID
                OpenMeldung = .DialogResult = DialogResult.OK
                .Dispose()
            End With
            If OpenMeldung Then mnuMeldung.PerformClick()
        Next

        If NoCompetition() AndAlso Not String.IsNullOrEmpty(WK_ID) Then
            Load_WettkampfProperties(WK_ID)
        End If

        Cursor = DefaultCursor
    End Sub
    Private Sub mnuImport_Ergebnisse_Click(sender As Object, e As EventArgs) Handles mnuImport_Ergebnisse.Click
        'If Wettkampf.ID = 0 Then
        '    If IsNothing(Func.Wettkampf_Auswahl(Me).Value) Then Return
        'End If

        'Cursor = Cursors.WaitCursor

        'With Import_Ergebnisse
        '    '.Text += Wettkampf.Bezeichnung
        '    .Location = New Point(Bounds.Left + 300, Bounds.Top + 80)
        '    .ShowDialog(Me)
        '    .Dispose()
        'End With

        'Cursor = DefaultCursor
    End Sub
    Private Sub mnuImport_Heber_Click(sender As Object, e As EventArgs) Handles mnuImport_Heber.Click
        With Import_Heber
            .ShowDialog(Me)
            .Dispose()
        End With
    End Sub
    Private Sub mnuImport_Wiegeliste_Click(sender As Object, e As EventArgs) Handles mnuImport_Wiegeliste.Click
    End Sub


    '' Menü Datenbank
    Private Sub mnuAthleten_Click(sender As Object, e As EventArgs) Handles mnuAthleten.Click
        Cursor = Cursors.WaitCursor
        With frmAthleten
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuAthletenAktualisieren_Click(sender As Object, e As EventArgs) Handles mnuAthletenAktualisieren.Click
        Cursor = Cursors.WaitCursor
        With Import_Athleten
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub
    Private Sub nmuModus_Click(sender As Object, e As EventArgs) Handles nmuModus.Click
        Cursor = Cursors.WaitCursor
        With frmModus
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = DefaultCursor
    End Sub
    Private Sub mnuTeams_Click(sender As Object, e As EventArgs) Handles mnuTeams.Click
        Cursor = Cursors.WaitCursor
        With frmTeams
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuVerein_Click(sender As Object, e As EventArgs) Handles mnuVerein.Click
        Cursor = Cursors.WaitCursor
        With frmVerein
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuWorldRecords_Click(sender As Object, e As EventArgs) Handles mnuWorldRecords.Click
        Cursor = Cursors.WaitCursor
        With frmWorldRecords
            .ShowDialog(Me)
            .Dispose()
        End With
        Cursor = Cursors.Default
    End Sub

    '' Menü Extras - Test
    Private Sub mnuAbZeichen_Click(sender As Object, e As EventArgs) Handles mnuAbZeichen.Click
        With frmAbzeichen
            .ShowDialog(Me)
            .Dispose()
        End With
    End Sub
    Private Sub mnuHantelTest_Click(sender As Object, e As EventArgs) Handles mnuHantelTest.Click
        With frmHantelBeladung
            .Text = "Test Hantelbeladung"
            .btnClear.Enabled = False
            .ShowDialog(Me)
            .Dispose()
        End With
    End Sub
    Private Sub mnuJuryBoard_Click(sender As Object, e As EventArgs) Handles mnuJuryBoard.Click
        frmJuryBoard.Show(Me)
    End Sub
    Private Sub mnuKR_Pad_Click(sender As Object, e As EventArgs) Handles mnuKR_Pad.Click
        With Me
            .Cursor = Cursors.WaitCursor
            frmKR_Pad.Location = New Point(.Bounds.Left + 200, .Bounds.Top + 100)
            frmKR_Pad.Show(Me)
            .Cursor = .DefaultCursor
        End With
    End Sub
    '' Menü Extras 
    Private Sub mnuHideMessages_CheckedChanged(sender As Object, e As EventArgs) Handles mnuHideMessages.CheckedChanged
        HideMessages = mnuHideMessages.Checked
    End Sub
    Private Sub mnuOptionen_Click(sender As Object, e As EventArgs) Handles mnuOptionen.Click
        With frmOptionen
            .ShowDialog(Me)
            .Dispose()
        End With
    End Sub
    Private Sub mnuDruckVorlagen_Click(sender As Object, e As EventArgs) Handles mnuDruckVorlagen.Click

        Dim FileName = Get_FileName(Me, "Druckvorlage bearbeiten")

        If Not IsNothing(FileName) Then
            User.Check_MySQLDriver(Me)
            Try
                Cursor = Cursors.WaitCursor
                Using report = New FastReport.Report
                    With report
                        .Load(FileName)
                        .Dictionary.Connections(0).ConnectionString = User.FR_ConnString
                        .Design()
                    End With
                End Using
            Catch ex As Exception
                'Stop
            End Try
        End If
        Cursor = Cursors.Default
    End Sub
    Private Sub mnuMauszeiger_Click(sender As Object, e As EventArgs) Handles mnuMauszeiger.Click
        Cursor.Position = New Point(Left + Width \ 2, Top + Height \ 2)
        Cursor.Show()
    End Sub
    Private Sub mnuWerbung_Click(sender As Object, e As EventArgs) Handles mnuWerbung.Click
        Using New Centered_MessageBox(Me)
            MessageBox.Show("Modul nicht installiert", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Using
        '    Try
        '        formBohle.Dispose()
        '    Catch ex As Exception
        '    End Try
        '    If IsNothing(formBohle) OrElse formBohle.IsDisposed Then formBohle = New frmBohle
        '    formBohle.Show()
        '    With formBohle.wmpWerbung
        '        .URL = "E:\Dokumente\Sammlungen\Talmud Questions\Talmud_Questions.mp4"

        '        .Ctlcontrols.play()
        '    End With
    End Sub


    '' mnuHilfe
    Private Sub mnuSupport_Click(sender As Object, e As EventArgs) Handles mnuSupport.Click
        frmSupport.ShowDialog(Me)
    End Sub
    Private Sub mnuInfo_Click(sender As Object, e As EventArgs) Handles mnuInfo.Click
        With frmInfo
            .lblVersion.Text = "Version " + Application.ProductVersion
            .ShowDialog(Me)
            .Dispose()
        End With
    End Sub
    Private Sub mnuUpdate_Click(sender As Object, e As EventArgs) Handles mnuUpdate.Click
        Using Updater As New frmUpdate
            With Updater
                .ShowDialog(Me)
                If .DialogResult = DialogResult.Cancel Then UpdateFailed = True
            End With
        End Using
        If Not UpdateFailed Then
            Update_Database()
        Else
            UpdateFailed = False
        End If
    End Sub

    'Private Function UpdateAvailable(Optional silent As Boolean = False) As Boolean?

    '    If File.Exists(Path.Combine(Application.StartupPath, "versions.ver")) Then File.Delete(Path.Combine(Application.StartupPath, "versions.ver"))

    '    Try
    '        With Computer.Network
    '            If .IsAvailable Then
    '                .DownloadFile("http://forte-publications.com/GFHsoft/weightlifting/versions.ver", Path.Combine(Application.StartupPath, "versions.ver"))
    '            Else
    '                If Not silent Then
    '                    Using New Centered_MessageBox(Me)
    '                        MessageBox.Show("Netzwerk nicht verfügbar.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '                    End Using
    '                Else
    '                    Return True
    '                End If
    '                Return Nothing
    '            End If
    '        End With
    '    Catch ex As Exception
    '        If Not silent Then
    '            Using New Centered_MessageBox(Me)
    '                MessageBox.Show("Fehler im Netzwerk oder beim Download.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '            End Using
    '        Else
    '            Return True
    '        End If
    '        Return Nothing
    '    End Try

    '    If File.Exists(Path.Combine(Application.StartupPath, "versions.ver")) Then
    '        Dim sReader As New StreamReader(Path.Combine(Application.StartupPath, "versions.ver"))
    '        Dim NewVersion = sReader.ReadLine
    '        Dim Url = sReader.ReadLine
    '        Dim action = sReader.ReadLine
    '        sReader.Close()
    '        File.Delete(Path.Combine(Application.StartupPath, "versions.ver"))
    '        'If UpdateTest(Split(AppVersion, "."), Split(NewVersion, "."), action.Contains("stringently")) Then
    '        '    Dim proc As New Process
    '        '    Try
    '        '        proc.StartInfo = New ProcessStartInfo(Path.Combine(Application.StartupPath, "Update.exe"),
    '        '                                              AppVersion + " " + Application.StartupPath + " " + AutoUpdate.ToString + " " &
    '        '                                              NewVersion + " " + Url + " " + action)
    '        '        proc.Start()
    '        '        If action.Contains("Update") OrElse action.Contains("restart") Then Application.Exit() ' bei Programm-Update oder expliziter Neustart-Forderung
    '        '    Catch ex As Exception
    '        '        If Not silent Then
    '        '            Using New Centered_MessageBox(Me)
    '        '                MessageBox.Show("Updater nicht gefunden:" & vbNewLine & Path.Combine(Application.StartupPath, "Update.exe"), msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        '            End Using
    '        '        Else
    '        '            Return True
    '        '        End If
    '        '    End Try
    '        'End If
    '        Return Nothing
    '    End If
    '    Return False
    'End Function
    'Private Function UpdateTest(oVersion() As String, nVersion() As String, stringently As Boolean) As Boolean

    '    If oVersion.Length = 0 Then Return False
    '    Dim msg = String.Empty

    '    Try
    '        For i = 0 To oVersion.Length - 1
    '            If CInt(nVersion(i)) > CInt(oVersion(i)) Then
    '                If stringently Then
    '                    msg = "Ein erforderliches Update ist verfügbar." + vbNewLine + "Zur Gewährleistung der Regel-Konformität sollte dieses Update unbedingt installiert werden."
    '                ElseIf Not nVersion.Equals(oVersion) Then
    '                    msg = "Für die installierte Programm-Version ist ein Update verfügbar."
    '                End If
    '                Exit For
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Return False
    '    End Try

    '    If msg = String.Empty Then Return False

    '    msg += vbNewLine + vbNewLine + "Update jetzt durchführen?"

    '    Using New Centered_MessageBox(Me, {"Ja", "Nein", "Ausblenden"})
    '        Dim result = MessageBox.Show(msg, msgCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ' für "Ausblenden" YesNoCancel
    '        If result = DialogResult.Yes Then
    '            Return True
    '        ElseIf result = DialogResult.No Then
    '            Return False
    '        Else
    '            NativeMethods.INI_Write(Bereich, "AutoUpdate", "False", App_IniFile)
    '            Return False
    '        End If
    '    End Using

    'End Function

    Private Sub mnuPasswort_Click(sender As Object, e As EventArgs) Handles mnuPasswort.Click

        Using CipherOld As New Custom_InputBox("Bitte das bisherige Benutzer-Passwort eingeben:", msgCaption,, Left + 50, Top + 100, True)
            If CipherOld.Response = User.Passwort Then

                Using CipherNew As New Custom_InputBox("Bitte das neue Benutzer-Passwort eingeben:", msgCaption,, Left + 50, Top + 100, True)

                    If Not IsNothing(CipherNew.Response) AndAlso Not String.IsNullOrEmpty(CipherNew.Response) Then
                        Cursor = Cursors.WaitCursor

                        Dim field = {"Anmeldung", "BVDG"}
                        Dim connstr As New Dictionary(Of String, String)

                        Dim CrypterOld As New Crypter(User.Passwort)
                        Dim CrypterNew As New Crypter(CipherNew.Response)

                        For i = 0 To field.Length - 1
                            Dim CipherText = NativeMethods.INI_Read(field(i), "Connection", App_IniFile)
                            If Not CipherText.Equals("?") AndAlso Not String.IsNullOrEmpty(CipherText) Then
                                connstr.Add(field(i), CrypterOld.DecryptData(CipherText))
                            End If
                        Next

                        For Each item In connstr
                            NativeMethods.INI_Write(item.Key, "Connection", CrypterNew.EncryptData(item.Value), App_IniFile)
                        Next

                        User.Passwort = CipherNew.Response
                        Cursor = Cursors.Default
                    End If
                End Using
            End If
        End Using
    End Sub

    Private Sub mnuCalculator_Click(sender As Object, e As EventArgs) Handles mnuCalculator.Click
        Dim Msg As String = String.Empty
        Dim Caption = "Online Punkte-Rechner"

        If mnuCalculator.Checked Then
            AppConnState = ConnState.Disconnected
            Return
        End If

        If Wettkampf.Identities.Count > 0 AndAlso
                        Not Wettkampf.IsSinclairWertung AndAlso Not Wettkampf.IsSinclairWertung2 AndAlso
                        Not Wettkampf.IsRobiWertung AndAlso Not Wettkampf.IsRobiWertung2 Then
            Msg = "Keine Punkt-Wertung im Wettkampf"
        ElseIf Wettkampf.Identities.Count = 0 Then

            If Not IsWettkampfAusgewählt(Caption) Then Return

            If Not Wettkampf.IsSinclairWertung AndAlso Not Wettkampf.IsSinclairWertung2 AndAlso
                        Not Wettkampf.IsRobiWertung AndAlso Not Wettkampf.IsRobiWertung2 Then
                Msg = "Keine Punkte-Wertung im ausgewählten Wettkampf"
            End If
        End If

        If Not String.IsNullOrEmpty(Msg) Then
            Using New Centered_MessageBox(Me)
                MessageBox.Show(Msg, Caption, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
            AppConnState = ConnState.Available
        Else
            Update_Online()
        End If
    End Sub

    Private Sub mnuOnlineVerwaltung_Click(sender As Object, e As EventArgs) Handles mnuOnlineVerwaltung.Click

        Dim DeleteUnused As Boolean = True
        Dim Msg As String = String.Empty
        Dim Caption = "Online-Wettkämpfe verwalten"
        Dim Icon = MessageBoxIcon.Information

        If DeleteUnused Then
            Using OS As New OnlineService
                AppConnState = OS.DeleteAll()
            End Using
            If AppConnState = ConnState.Disconnected Then
                Msg = "Keine Verbindung zum Server."
                Icon = MessageBoxIcon.Warning
            End If
        End If

        Using New Centered_MessageBox(Me)
            MessageBox.Show(Msg, Caption, MessageBoxButtons.OK, Icon)
        End Using

    End Sub

    Private Sub Update_Online()
        Dim Msg As String = String.Empty
        Dim Caption = "Online Punkte-Rechner"
        Dim Icon = MessageBoxIcon.Warning

        'Using New Centered_MessageBox(Me)
        '    Select Case MessageBox.Show("Alle Online-Wettkämpfe löschen?" & vbNewLine &
        '                                "(Der aktuelle Wettkampf wird anschließend neu eingelesen.)",
        '                                "Datenbank-Bereinigung", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
        '        Case DialogResult.Yes
        '            DeleteUnused = True
        '        Case DialogResult.No
        '            DeleteUnused = False
        '        Case DialogResult.Cancel
        '            AppConnState = ConnState.UserClosed
        '            Return
        '    End Select
        'End Using

        Cursor = Cursors.WaitCursor


        'ExternCounter.Start("Punkte-Rechner", 6)

        If String.IsNullOrEmpty(Msg) Then
            AppConnState = ConnState.Available
            Try
                formMain.Change_MainProgressBarValue(5)
                Using conn As New MySqlConnection(User.APP_ConnString)
                    conn.Open()
                    Using OS As New OnlineService
                        OS.UpdateWettkampf(conn)
                        formMain.Change_MainProgressBarValue(15)
                        OS.UpdateWertung(conn)
                        formMain.Change_MainProgressBarValue(30)
                        OS.UpdateAthlet(, conn)
                        formMain.Change_MainProgressBarValue(45)
                        OS.UpdateGruppen(conn)
                        formMain.Change_MainProgressBarValue(60)
                        OS.UpdateMeldung(conn)
                        formMain.Change_MainProgressBarValue(75)
                        OS.UpdateHeben(,, conn)
                        formMain.Change_MainProgressBarValue(90)
                        OS.UpdateResults(conn)
                        formMain.Change_MainProgressBarValue(100)
                    End Using
                End Using
            Catch ex As Exception
                MySQl_Error(Me, "Online-Rechner Datenbankupdate; " & ex.Message, ex.StackTrace, True)
            End Try

            If AppConnState = ConnState.Connected Then
                Msg = "Die Online-Datenbank wurde aktualisiert."
                Icon = MessageBoxIcon.Information
            Else
                Msg = "Fehler beim Aktualisieren der Online-Datenbank."
            End If
        End If

        Using New Centered_MessageBox(Me)
            MessageBox.Show(Msg, Caption, MessageBoxButtons.OK, Icon)
        End Using

        formMain.Change_MainProgressBarValue(0)
        Cursor = Cursors.Default
    End Sub
    Private Sub lblExtern_Init(CalcName As String, EventsCount As Integer)
        lblExtern.ForeColor = Color.FromKnownColor(KnownColor.ControlText)
        lblExtern.Text = CalcName & " 0/" & EventsCount
    End Sub
    Private Sub lblExtern_Change(Count As String)
        If String.IsNullOrEmpty(lblExtern.Text) Then Return
        Try
            Dim s = Split(lblExtern.Text, " ")
            Dim p = Split(s(1), "/")
            p(0) = Count
            lblExtern.Text = s(0) & " " & Join(p, "/")
        Catch ex As Exception
            LogMessage("Main: lblExtern_Change: " & ex.Message, ex.StackTrace)
        End Try
    End Sub
    Private Sub lblExtern_Finish(Text As String) 'Success As Boolean)
        If Not String.IsNullOrEmpty(Text) Then lblExtern.ForeColor = Color.Red
        lblExtern.Text = Text
    End Sub
    Private Sub lblExtern_Update(Value As Integer)
        Select Case Value
            Case ConnState.UserClosed, ConnState.Available, ConnState.NotAvailable
                lblExtern.Text = String.Empty
                mnuCalculator.Checked = False
                mnuCalculator.Enabled = Not Value = ConnState.NotAvailable
            Case ConnState.Connected
                lblExtern.Text = "App connected"
                lblExtern.ForeColor = Color.Green
                mnuCalculator.Checked = True
            Case ConnState.Disconnected ' Error
                lblExtern.Text = "App disconnected"
                lblExtern.ForeColor = Color.Red
                mnuCalculator.Checked = False
        End Select
    End Sub

    Private Sub mnuTVTest_Click(sender As Object, e As EventArgs) Handles mnuTVTest.Click
        With frmHeberWertung
            .Closable = True
            .Show()
        End With
    End Sub

    Private Sub mnuTopMost_Click(sender As Object, e As EventArgs) Handles mnuTopMost.Click
        TopMost = mnuTopMost.Checked
    End Sub

    Private Sub mnuRegistrieren_Click(sender As Object, e As EventArgs) Handles mnuRegistrieren.Click
        Using KeyGen As New FKeygen
            Register(KeyGen)
        End Using
    End Sub

End Class


