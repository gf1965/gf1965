﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmGruppen
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDrucken = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKampfgericht = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMeldungen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKonflikte = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTeilen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWG = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuExcelExport = New System.Windows.Forms.ToolStripMenuItem()
        Me.dgvGruppen = New System.Windows.Forms.DataGridView()
        Me.Gruppe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Teilgruppe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Einteilung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Datum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DatumW = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WiegenBeginn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WiegenEnde = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Reissen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Stossen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DatumA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Athletik = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bohle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Blockheben = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.cmnuGruppe = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmnuTeilen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmnuWG = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.dgvMeldung = New System.Windows.Forms.DataGridView()
        Me.Teilnehmer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nachname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vorname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sex = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Team = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Wiegen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ZK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nr = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmnuHeber = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuRemoveLifter = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.btnUp = New System.Windows.Forms.Button()
        Me.btnDown = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.dtpDatum = New System.Windows.Forms.DateTimePicker()
        Me.dtpZeit = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pnlDragRowIndicator = New System.Windows.Forms.Panel()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.lblOptionen = New System.Windows.Forms.Label()
        Me.btnApply = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.optAK_Group = New System.Windows.Forms.RadioButton()
        Me.optAK_Keep = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.optGK_Group = New System.Windows.Forms.RadioButton()
        Me.optGK_Keep = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nudMaxCount = New System.Windows.Forms.NumericUpDown()
        Me.pnlOptionen = New System.Windows.Forms.Panel()
        Me.lblWG = New System.Windows.Forms.Label()
        Me.btnWG_Apply = New System.Windows.Forms.Button()
        Me.btnWG_Exit = New System.Windows.Forms.Button()
        Me.btnWG_Cancel = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboWG_1 = New System.Windows.Forms.ComboBox()
        Me.cboWG_2 = New System.Windows.Forms.ComboBox()
        Me.pnlWG = New System.Windows.Forms.Panel()
        Me.optZugewiesen = New System.Windows.Forms.RadioButton()
        Me.optNichtZugewiesen = New System.Windows.Forms.RadioButton()
        Me.pnlTN = New System.Windows.Forms.Panel()
        Me.chkShowAll = New System.Windows.Forms.CheckBox()
        Me.dgvUnallocated = New System.Windows.Forms.DataGridView()
        Me.uTeilnehmer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.uNachname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.uVorname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.uSex = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.uTeam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.uAK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.uJG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.uGK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.uWiegen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.uZK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.uNr = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnMoveLeft = New System.Windows.Forms.Button()
        Me.btnMoveRight = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1.SuspendLayout()
        CType(Me.dgvGruppen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmnuGruppe.SuspendLayout()
        CType(Me.dgvMeldung, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmnuHeber.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudMaxCount, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOptionen.SuspendLayout()
        Me.pnlWG.SuspendLayout()
        Me.pnlTN.SuspendLayout()
        CType(Me.dgvUnallocated, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.MenuBar
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuKampfgericht, Me.mnuOptionen, Me.mnuExtras})
        Me.MenuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(4, 1, 0, 1)
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.MenuStrip1.Size = New System.Drawing.Size(1221, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSpeichern, Me.mnuDrucken, Me.ToolStripMenuItem1, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 22)
        Me.mnuDatei.Text = "&Datei"
        '
        'mnuSpeichern
        '
        Me.mnuSpeichern.Enabled = False
        Me.mnuSpeichern.Name = "mnuSpeichern"
        Me.mnuSpeichern.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuSpeichern.Size = New System.Drawing.Size(168, 22)
        Me.mnuSpeichern.Text = "&Speichern"
        '
        'mnuDrucken
        '
        Me.mnuDrucken.Name = "mnuDrucken"
        Me.mnuDrucken.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.mnuDrucken.Size = New System.Drawing.Size(168, 22)
        Me.mnuDrucken.Text = "&Drucken"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(165, 6)
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        Me.mnuBeenden.Size = New System.Drawing.Size(168, 22)
        Me.mnuBeenden.Text = "&Beenden"
        '
        'mnuKampfgericht
        '
        Me.mnuKampfgericht.Name = "mnuKampfgericht"
        Me.mnuKampfgericht.Size = New System.Drawing.Size(91, 22)
        Me.mnuKampfgericht.Text = "&Kampfgericht"
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMeldungen, Me.mnuKonflikte})
        Me.mnuOptionen.Name = "mnuOptionen"
        Me.mnuOptionen.Size = New System.Drawing.Size(69, 22)
        Me.mnuOptionen.Text = "&Optionen"
        Me.mnuOptionen.Visible = False
        '
        'mnuMeldungen
        '
        Me.mnuMeldungen.CheckOnClick = True
        Me.mnuMeldungen.Name = "mnuMeldungen"
        Me.mnuMeldungen.Size = New System.Drawing.Size(210, 22)
        Me.mnuMeldungen.Text = "&Meldungen anzeigen"
        '
        'mnuKonflikte
        '
        Me.mnuKonflikte.Checked = True
        Me.mnuKonflikte.CheckOnClick = True
        Me.mnuKonflikte.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuKonflikte.Name = "mnuKonflikte"
        Me.mnuKonflikte.Size = New System.Drawing.Size(210, 22)
        Me.mnuKonflikte.Text = "bei &Konflikten nachfragen"
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuTeilen, Me.mnuWG, Me.ToolStripMenuItem4, Me.mnuExcelExport})
        Me.mnuExtras.Name = "mnuExtras"
        Me.mnuExtras.Size = New System.Drawing.Size(50, 22)
        Me.mnuExtras.Text = "&Extras"
        '
        'mnuTeilen
        '
        Me.mnuTeilen.Name = "mnuTeilen"
        Me.mnuTeilen.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.T), System.Windows.Forms.Keys)
        Me.mnuTeilen.Size = New System.Drawing.Size(300, 22)
        Me.mnuTeilen.Text = "Gruppe nach Gewicht &teilen"
        Me.mnuTeilen.Visible = False
        '
        'mnuWG
        '
        Me.mnuWG.Name = "mnuWG"
        Me.mnuWG.Size = New System.Drawing.Size(300, 22)
        Me.mnuWG.Text = "Wettkampf-Gruppen"
        Me.mnuWG.Visible = False
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(297, 6)
        Me.ToolStripMenuItem4.Visible = False
        '
        'mnuExcelExport
        '
        Me.mnuExcelExport.Name = "mnuExcelExport"
        Me.mnuExcelExport.Size = New System.Drawing.Size(300, 22)
        Me.mnuExcelExport.Text = "Gruppen-Einteilung nach Excel exportieren"
        '
        'dgvGruppen
        '
        Me.dgvGruppen.AllowDrop = True
        Me.dgvGruppen.AllowUserToAddRows = False
        Me.dgvGruppen.AllowUserToDeleteRows = False
        Me.dgvGruppen.AllowUserToResizeColumns = False
        Me.dgvGruppen.AllowUserToResizeRows = False
        Me.dgvGruppen.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvGruppen.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvGruppen.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGruppen.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvGruppen.ColumnHeadersHeight = 37
        Me.dgvGruppen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvGruppen.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Gruppe, Me.Teilgruppe, Me.WG, Me.Einteilung, Me.Bezeichnung, Me.TN, Me.Datum, Me.DatumW, Me.WiegenBeginn, Me.WiegenEnde, Me.Reissen, Me.Stossen, Me.DatumA, Me.Athletik, Me.Bohle, Me.Blockheben})
        Me.dgvGruppen.ContextMenuStrip = Me.cmnuGruppe
        Me.dgvGruppen.Location = New System.Drawing.Point(12, 57)
        Me.dgvGruppen.MultiSelect = False
        Me.dgvGruppen.Name = "dgvGruppen"
        Me.dgvGruppen.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvGruppen.RowHeadersWidth = 23
        Me.dgvGruppen.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvGruppen.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvGruppen.Size = New System.Drawing.Size(706, 479)
        Me.dgvGruppen.TabIndex = 20
        '
        'Gruppe
        '
        Me.Gruppe.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gruppe.DataPropertyName = "Gruppe"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Gruppe.DefaultCellStyle = DataGridViewCellStyle2
        Me.Gruppe.FillWeight = 30.0!
        Me.Gruppe.HeaderText = "Nr."
        Me.Gruppe.MinimumWidth = 8
        Me.Gruppe.Name = "Gruppe"
        Me.Gruppe.ReadOnly = True
        Me.Gruppe.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Gruppe.ToolTipText = "fortlaufende Nummer der Gruppe"
        Me.Gruppe.Width = 30
        '
        'Teilgruppe
        '
        Me.Teilgruppe.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Teilgruppe.DataPropertyName = "Teilgruppe"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Teilgruppe.DefaultCellStyle = DataGridViewCellStyle3
        Me.Teilgruppe.FillWeight = 20.0!
        Me.Teilgruppe.HeaderText = ""
        Me.Teilgruppe.MinimumWidth = 8
        Me.Teilgruppe.Name = "Teilgruppe"
        Me.Teilgruppe.ToolTipText = "Teil-Gruppe der GK"
        Me.Teilgruppe.Visible = False
        Me.Teilgruppe.Width = 20
        '
        'WG
        '
        Me.WG.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.WG.DataPropertyName = "WG"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.WG.DefaultCellStyle = DataGridViewCellStyle4
        Me.WG.FillWeight = 30.0!
        Me.WG.HeaderText = "WG"
        Me.WG.MinimumWidth = 8
        Me.WG.Name = "WG"
        Me.WG.ToolTipText = "Wettkampf- (Wertungs-) Gruppe"
        Me.WG.Width = 30
        '
        'Einteilung
        '
        Me.Einteilung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Einteilung.DataPropertyName = "Einteilung"
        Me.Einteilung.HeaderText = "Einteilung"
        Me.Einteilung.MinimumWidth = 160
        Me.Einteilung.Name = "Einteilung"
        Me.Einteilung.ReadOnly = True
        Me.Einteilung.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Einteilung.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Einteilung.ToolTipText = "Gruppeneinteilung"
        '
        'Bezeichnung
        '
        Me.Bezeichnung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Bezeichnung.DataPropertyName = "Bezeichnung"
        Me.Bezeichnung.HeaderText = "Bezeichnung"
        Me.Bezeichnung.MinimumWidth = 160
        Me.Bezeichnung.Name = "Bezeichnung"
        Me.Bezeichnung.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Bezeichnung.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Bezeichnung.ToolTipText = "Gruppen-Bezeichnung"
        '
        'TN
        '
        Me.TN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.TN.DataPropertyName = "TN"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TN.DefaultCellStyle = DataGridViewCellStyle5
        Me.TN.FillWeight = 30.0!
        Me.TN.HeaderText = "TN"
        Me.TN.MinimumWidth = 8
        Me.TN.Name = "TN"
        Me.TN.ReadOnly = True
        Me.TN.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.TN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.TN.ToolTipText = "Teilnehmer-Anzahl"
        Me.TN.Width = 30
        '
        'Datum
        '
        Me.Datum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Datum.DataPropertyName = "Datum"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Format = "d"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.Datum.DefaultCellStyle = DataGridViewCellStyle6
        Me.Datum.FillWeight = 80.0!
        Me.Datum.HeaderText = "WK-Datum"
        Me.Datum.MinimumWidth = 8
        Me.Datum.Name = "Datum"
        Me.Datum.ReadOnly = True
        Me.Datum.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Datum.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Datum.ToolTipText = "Wettkampf-Datum"
        Me.Datum.Width = 80
        '
        'DatumW
        '
        Me.DatumW.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DatumW.DataPropertyName = "DatumW"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.Format = "d"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.DatumW.DefaultCellStyle = DataGridViewCellStyle7
        Me.DatumW.FillWeight = 80.0!
        Me.DatumW.HeaderText = "Wiegen Datum"
        Me.DatumW.MinimumWidth = 8
        Me.DatumW.Name = "DatumW"
        Me.DatumW.ReadOnly = True
        Me.DatumW.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DatumW.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DatumW.ToolTipText = "Datum des Wiegens (falls abweichend vom WK-Datum)"
        Me.DatumW.Visible = False
        Me.DatumW.Width = 80
        '
        'WiegenBeginn
        '
        Me.WiegenBeginn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.WiegenBeginn.DataPropertyName = "WiegenBeginn"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.Format = "t"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.WiegenBeginn.DefaultCellStyle = DataGridViewCellStyle8
        Me.WiegenBeginn.FillWeight = 55.0!
        Me.WiegenBeginn.HeaderText = "Wiegen Beginn"
        Me.WiegenBeginn.MinimumWidth = 8
        Me.WiegenBeginn.Name = "WiegenBeginn"
        Me.WiegenBeginn.ReadOnly = True
        Me.WiegenBeginn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.WiegenBeginn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.WiegenBeginn.ToolTipText = "Uhrzeit des Wiegens"
        Me.WiegenBeginn.Width = 55
        '
        'WiegenEnde
        '
        Me.WiegenEnde.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.WiegenEnde.DataPropertyName = "WiegenEnde"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Format = "t"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.WiegenEnde.DefaultCellStyle = DataGridViewCellStyle9
        Me.WiegenEnde.FillWeight = 55.0!
        Me.WiegenEnde.HeaderText = "Wiegen Ende"
        Me.WiegenEnde.MinimumWidth = 8
        Me.WiegenEnde.Name = "WiegenEnde"
        Me.WiegenEnde.ReadOnly = True
        Me.WiegenEnde.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.WiegenEnde.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.WiegenEnde.ToolTipText = "Ende des Wiegens"
        Me.WiegenEnde.Visible = False
        Me.WiegenEnde.Width = 55
        '
        'Reissen
        '
        Me.Reissen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Reissen.DataPropertyName = "Reissen"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.Format = "t"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.Reissen.DefaultCellStyle = DataGridViewCellStyle10
        Me.Reissen.FillWeight = 55.0!
        Me.Reissen.HeaderText = "Reißen Beginn"
        Me.Reissen.MinimumWidth = 8
        Me.Reissen.Name = "Reissen"
        Me.Reissen.ReadOnly = True
        Me.Reissen.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Reissen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Reissen.ToolTipText = "Uhrzeit des WK-Beginns"
        Me.Reissen.Width = 55
        '
        'Stossen
        '
        Me.Stossen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Stossen.DataPropertyName = "Stossen"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.Format = "t"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.Stossen.DefaultCellStyle = DataGridViewCellStyle11
        Me.Stossen.FillWeight = 55.0!
        Me.Stossen.HeaderText = "Stoßen Beginn"
        Me.Stossen.MinimumWidth = 8
        Me.Stossen.Name = "Stossen"
        Me.Stossen.ReadOnly = True
        Me.Stossen.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Stossen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Stossen.ToolTipText = "Beginn des Stoßens"
        Me.Stossen.Visible = False
        Me.Stossen.Width = 55
        '
        'DatumA
        '
        Me.DatumA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DatumA.DataPropertyName = "DatumA"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.Format = "d"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.DatumA.DefaultCellStyle = DataGridViewCellStyle12
        Me.DatumA.FillWeight = 80.0!
        Me.DatumA.HeaderText = "Athletik Datum"
        Me.DatumA.MinimumWidth = 8
        Me.DatumA.Name = "DatumA"
        Me.DatumA.ReadOnly = True
        Me.DatumA.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DatumA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DatumA.ToolTipText = "Datum des Athletik-Wettkampfs (falls abweichend vom WK-Datum)"
        Me.DatumA.Visible = False
        Me.DatumA.Width = 80
        '
        'Athletik
        '
        Me.Athletik.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Athletik.DataPropertyName = "Athletik"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.Format = "t"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.Athletik.DefaultCellStyle = DataGridViewCellStyle13
        Me.Athletik.FillWeight = 55.0!
        Me.Athletik.HeaderText = "Athletik Beginn"
        Me.Athletik.MinimumWidth = 8
        Me.Athletik.Name = "Athletik"
        Me.Athletik.ReadOnly = True
        Me.Athletik.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Athletik.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Athletik.ToolTipText = "Beginn des Athletik-Wettkampfs"
        Me.Athletik.Visible = False
        Me.Athletik.Width = 55
        '
        'Bohle
        '
        Me.Bohle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Bohle.DataPropertyName = "Bohle"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Bohle.DefaultCellStyle = DataGridViewCellStyle14
        Me.Bohle.FillWeight = 45.0!
        Me.Bohle.HeaderText = "Bohle"
        Me.Bohle.MinimumWidth = 8
        Me.Bohle.Name = "Bohle"
        Me.Bohle.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Bohle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Bohle.ToolTipText = "Nummer der Bohle"
        Me.Bohle.Width = 45
        '
        'Blockheben
        '
        Me.Blockheben.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Blockheben.DataPropertyName = "Blockheben"
        Me.Blockheben.FillWeight = 30.0!
        Me.Blockheben.HeaderText = "BH"
        Me.Blockheben.MinimumWidth = 8
        Me.Blockheben.Name = "Blockheben"
        Me.Blockheben.ToolTipText = "Blockheben"
        Me.Blockheben.Width = 30
        '
        'cmnuGruppe
        '
        Me.cmnuGruppe.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.cmnuGruppe.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmnuTeilen, Me.ToolStripMenuItem2, Me.cmnuWG})
        Me.cmnuGruppe.Name = "ContextMenuStrip1"
        Me.cmnuGruppe.Size = New System.Drawing.Size(221, 54)
        '
        'cmnuTeilen
        '
        Me.cmnuTeilen.Name = "cmnuTeilen"
        Me.cmnuTeilen.Size = New System.Drawing.Size(220, 22)
        Me.cmnuTeilen.Text = "Gruppe nach Gewicht teilen"
        Me.cmnuTeilen.Visible = False
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(217, 6)
        Me.ToolStripMenuItem2.Visible = False
        '
        'cmnuWG
        '
        Me.cmnuWG.Name = "cmnuWG"
        Me.cmnuWG.Size = New System.Drawing.Size(220, 22)
        Me.cmnuWG.Text = "Wettkampf-Gruppen"
        Me.cmnuWG.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Enabled = False
        Me.btnSave.Image = Global.Gewichtheben.My.Resources.Resources.saveHS
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(1039, 548)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(85, 29)
        Me.btnSave.TabIndex = 201
        Me.btnSave.TabStop = False
        Me.btnSave.Text = " Speichern"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.UseCompatibleTextRendering = True
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'dgvMeldung
        '
        Me.dgvMeldung.AllowUserToAddRows = False
        Me.dgvMeldung.AllowUserToDeleteRows = False
        Me.dgvMeldung.AllowUserToResizeRows = False
        Me.dgvMeldung.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvMeldung.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMeldung.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvMeldung.ColumnHeadersHeight = 24
        Me.dgvMeldung.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvMeldung.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Teilnehmer, Me.Nachname, Me.Vorname, Me.Sex, Me.Team, Me.AK, Me.JG, Me.GK, Me.Wiegen, Me.ZK, Me.Nr})
        Me.dgvMeldung.ContextMenuStrip = Me.cmnuHeber
        Me.dgvMeldung.Location = New System.Drawing.Point(753, 71)
        Me.dgvMeldung.Name = "dgvMeldung"
        Me.dgvMeldung.ReadOnly = True
        Me.dgvMeldung.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvMeldung.RowHeadersVisible = False
        Me.dgvMeldung.RowHeadersWidth = 62
        Me.dgvMeldung.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvMeldung.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMeldung.Size = New System.Drawing.Size(456, 465)
        Me.dgvMeldung.TabIndex = 203
        Me.dgvMeldung.TabStop = False
        '
        'Teilnehmer
        '
        Me.Teilnehmer.DataPropertyName = "Teilnehmer"
        Me.Teilnehmer.HeaderText = "TN"
        Me.Teilnehmer.MinimumWidth = 8
        Me.Teilnehmer.Name = "Teilnehmer"
        Me.Teilnehmer.ReadOnly = True
        Me.Teilnehmer.Visible = False
        Me.Teilnehmer.Width = 150
        '
        'Nachname
        '
        Me.Nachname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Nachname.DataPropertyName = "Nachname"
        Me.Nachname.FillWeight = 80.0!
        Me.Nachname.HeaderText = "Nachname"
        Me.Nachname.MinimumWidth = 8
        Me.Nachname.Name = "Nachname"
        Me.Nachname.ReadOnly = True
        '
        'Vorname
        '
        Me.Vorname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Vorname.DataPropertyName = "Vorname"
        Me.Vorname.FillWeight = 80.0!
        Me.Vorname.HeaderText = "Vorname"
        Me.Vorname.MinimumWidth = 8
        Me.Vorname.Name = "Vorname"
        Me.Vorname.ReadOnly = True
        '
        'Sex
        '
        Me.Sex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Sex.DataPropertyName = "Sex"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Sex.DefaultCellStyle = DataGridViewCellStyle16
        Me.Sex.FillWeight = 30.0!
        Me.Sex.HeaderText = "m/w"
        Me.Sex.MinimumWidth = 8
        Me.Sex.Name = "Sex"
        Me.Sex.ReadOnly = True
        Me.Sex.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Sex.ToolTipText = "Geschlecht"
        Me.Sex.Width = 30
        '
        'Team
        '
        Me.Team.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Team.DataPropertyName = "Vereinsname"
        Me.Team.FillWeight = 120.0!
        Me.Team.HeaderText = "Verein/Team"
        Me.Team.MinimumWidth = 8
        Me.Team.Name = "Team"
        Me.Team.ReadOnly = True
        Me.Team.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Team.Visible = False
        '
        'AK
        '
        Me.AK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.AK.DataPropertyName = "AK"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.AK.DefaultCellStyle = DataGridViewCellStyle17
        Me.AK.FillWeight = 70.0!
        Me.AK.HeaderText = "AK"
        Me.AK.MinimumWidth = 8
        Me.AK.Name = "AK"
        Me.AK.ReadOnly = True
        Me.AK.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.AK.ToolTipText = "Altersklasse"
        Me.AK.Width = 70
        '
        'JG
        '
        Me.JG.DataPropertyName = "JG"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.JG.DefaultCellStyle = DataGridViewCellStyle18
        Me.JG.FillWeight = 50.0!
        Me.JG.HeaderText = "JG"
        Me.JG.MinimumWidth = 8
        Me.JG.Name = "JG"
        Me.JG.ReadOnly = True
        Me.JG.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.JG.ToolTipText = "Jahrgang"
        Me.JG.Visible = False
        Me.JG.Width = 50
        '
        'GK
        '
        Me.GK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.GK.DataPropertyName = "GK"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle19.NullValue = Nothing
        DataGridViewCellStyle19.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.GK.DefaultCellStyle = DataGridViewCellStyle19
        Me.GK.FillWeight = 40.0!
        Me.GK.HeaderText = "GK"
        Me.GK.MinimumWidth = 8
        Me.GK.Name = "GK"
        Me.GK.ReadOnly = True
        Me.GK.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GK.ToolTipText = "Gewichtsklasse"
        Me.GK.Width = 40
        '
        'Wiegen
        '
        Me.Wiegen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Wiegen.DataPropertyName = "Wiegen"
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle20.Format = "0.00"
        DataGridViewCellStyle20.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.Wiegen.DefaultCellStyle = DataGridViewCellStyle20
        Me.Wiegen.FillWeight = 50.0!
        Me.Wiegen.HeaderText = "KG"
        Me.Wiegen.MinimumWidth = 8
        Me.Wiegen.Name = "Wiegen"
        Me.Wiegen.ReadOnly = True
        Me.Wiegen.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Wiegen.ToolTipText = "Körpergewicht"
        Me.Wiegen.Width = 50
        '
        'ZK
        '
        Me.ZK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.ZK.DataPropertyName = "Zweikampf"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle21.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.ZK.DefaultCellStyle = DataGridViewCellStyle21
        Me.ZK.FillWeight = 40.0!
        Me.ZK.HeaderText = "ZK"
        Me.ZK.MinimumWidth = 8
        Me.ZK.Name = "ZK"
        Me.ZK.ReadOnly = True
        Me.ZK.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ZK.ToolTipText = "Zweikampf-Leistung"
        Me.ZK.Visible = False
        Me.ZK.Width = 40
        '
        'Nr
        '
        Me.Nr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Nr.DataPropertyName = "Losnummer"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Nr.DefaultCellStyle = DataGridViewCellStyle22
        Me.Nr.FillWeight = 40.0!
        Me.Nr.HeaderText = "Nr"
        Me.Nr.MinimumWidth = 8
        Me.Nr.Name = "Nr"
        Me.Nr.ReadOnly = True
        Me.Nr.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Nr.ToolTipText = "Losnummer"
        Me.Nr.Visible = False
        Me.Nr.Width = 40
        '
        'cmnuHeber
        '
        Me.cmnuHeber.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.cmnuHeber.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuRemoveLifter})
        Me.cmnuHeber.Name = "cmnuHeber"
        Me.cmnuHeber.Size = New System.Drawing.Size(217, 26)
        '
        'mnuRemoveLifter
        '
        Me.mnuRemoveLifter.Image = Global.Gewichtheben.My.Resources.Resources.Delete21
        Me.mnuRemoveLifter.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.mnuRemoveLifter.Name = "mnuRemoveLifter"
        Me.mnuRemoveLifter.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.mnuRemoveLifter.Size = New System.Drawing.Size(216, 22)
        Me.mnuRemoveLifter.Text = "aus Gruppe entfernen"
        Me.mnuRemoveLifter.ToolTipText = "markierte Teilnehmer zu nicht ausgewählt verschieben"
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Image = Global.Gewichtheben.My.Resources.Resources.Add
        Me.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAdd.Location = New System.Drawing.Point(10, 548)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(124, 29)
        Me.btnAdd.TabIndex = 204
        Me.btnAdd.TabStop = False
        Me.btnAdd.Text = " Gruppe hinzufügen"
        Me.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAdd.UseCompatibleTextRendering = True
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.Enabled = False
        Me.btnRemove.Image = Global.Gewichtheben.My.Resources.Resources.Delete21
        Me.btnRemove.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRemove.Location = New System.Drawing.Point(140, 548)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(112, 29)
        Me.btnRemove.TabIndex = 204
        Me.btnRemove.TabStop = False
        Me.btnRemove.Text = " Gruppe löschen"
        Me.btnRemove.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRemove.UseCompatibleTextRendering = True
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnUp
        '
        Me.btnUp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnUp.Enabled = False
        Me.btnUp.Image = Global.Gewichtheben.My.Resources.Resources.arrow_up_16
        Me.btnUp.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUp.Location = New System.Drawing.Point(258, 548)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(124, 29)
        Me.btnUp.TabIndex = 205
        Me.btnUp.TabStop = False
        Me.btnUp.Tag = "-1"
        Me.btnUp.Text = " Gruppe nach vorn"
        Me.btnUp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUp.UseCompatibleTextRendering = True
        Me.btnUp.UseVisualStyleBackColor = True
        '
        'btnDown
        '
        Me.btnDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDown.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnDown.Enabled = False
        Me.btnDown.Image = Global.Gewichtheben.My.Resources.Resources.arrow_down_16
        Me.btnDown.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDown.Location = New System.Drawing.Point(388, 548)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(130, 29)
        Me.btnDown.TabIndex = 205
        Me.btnDown.TabStop = False
        Me.btnDown.Tag = "1"
        Me.btnDown.Text = " Gruppe nach hinten"
        Me.btnDown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDown.UseCompatibleTextRendering = True
        Me.btnDown.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Image = Global.Gewichtheben.My.Resources.Resources.Exit_icon
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(1130, 548)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(81, 29)
        Me.btnClose.TabIndex = 201
        Me.btnClose.TabStop = False
        Me.btnClose.Text = "Schließen"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.UseCompatibleTextRendering = True
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dtpDatum
        '
        Me.dtpDatum.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDatum.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDatum.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDatum.Location = New System.Drawing.Point(516, 109)
        Me.dtpDatum.Name = "dtpDatum"
        Me.dtpDatum.Size = New System.Drawing.Size(92, 20)
        Me.dtpDatum.TabIndex = 207
        Me.dtpDatum.TabStop = False
        Me.ToolTip1.SetToolTip(Me.dtpDatum, "Eingabeformat = DD.MM.YYYY")
        Me.dtpDatum.Visible = False
        '
        'dtpZeit
        '
        Me.dtpZeit.CustomFormat = "HH:mm"
        Me.dtpZeit.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpZeit.Location = New System.Drawing.Point(630, 109)
        Me.dtpZeit.Name = "dtpZeit"
        Me.dtpZeit.ShowUpDown = True
        Me.dtpZeit.Size = New System.Drawing.Size(54, 20)
        Me.dtpZeit.TabIndex = 208
        Me.dtpZeit.TabStop = False
        Me.ToolTip1.SetToolTip(Me.dtpZeit, "Eingabeformat = HH:MM")
        Me.dtpZeit.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "&Gruppen-Einteilung"
        '
        'pnlDragRowIndicator
        '
        Me.pnlDragRowIndicator.AllowDrop = True
        Me.pnlDragRowIndicator.BackColor = System.Drawing.Color.Black
        Me.pnlDragRowIndicator.Location = New System.Drawing.Point(556, 548)
        Me.pnlDragRowIndicator.Name = "pnlDragRowIndicator"
        Me.pnlDragRowIndicator.Size = New System.Drawing.Size(97, 2)
        Me.pnlDragRowIndicator.TabIndex = 222
        Me.pnlDragRowIndicator.Visible = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Title = "Speichern unter..."
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.CheckPathExists = False
        Me.SaveFileDialog1.Filter = "CSV-Datei|*.csv|Alle Dateien|*.*"
        Me.SaveFileDialog1.FilterIndex = 0
        Me.SaveFileDialog1.Title = "Gruppenliste speichern"
        '
        'lblOptionen
        '
        Me.lblOptionen.BackColor = System.Drawing.Color.RoyalBlue
        Me.lblOptionen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblOptionen.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblOptionen.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblOptionen.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblOptionen.ForeColor = System.Drawing.Color.White
        Me.lblOptionen.Location = New System.Drawing.Point(0, 0)
        Me.lblOptionen.Name = "lblOptionen"
        Me.lblOptionen.Padding = New System.Windows.Forms.Padding(7, 0, 0, 0)
        Me.lblOptionen.Size = New System.Drawing.Size(307, 36)
        Me.lblOptionen.TabIndex = 501
        Me.lblOptionen.Text = "Gruppen-Einteilung"
        Me.lblOptionen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblOptionen.UseCompatibleTextRendering = True
        '
        'btnApply
        '
        Me.btnApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApply.Enabled = False
        Me.btnApply.Location = New System.Drawing.Point(94, 185)
        Me.btnApply.MinimumSize = New System.Drawing.Size(77, 25)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(96, 29)
        Me.btnApply.TabIndex = 535
        Me.btnApply.Text = "Übernehmen"
        Me.btnApply.UseCompatibleTextRendering = True
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(196, 185)
        Me.btnCancel.MinimumSize = New System.Drawing.Size(77, 25)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(96, 29)
        Me.btnCancel.TabIndex = 536
        Me.btnCancel.Text = "Abbrechen"
        Me.btnCancel.UseCompatibleTextRendering = True
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.btnExit.ForeColor = System.Drawing.Color.White
        Me.btnExit.Location = New System.Drawing.Point(278, 6)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(23, 23)
        Me.btnExit.TabIndex = 502
        Me.btnExit.TabStop = False
        Me.btnExit.Text = "X"
        Me.btnExit.UseCompatibleTextRendering = True
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.optAK_Group)
        Me.GroupBox1.Controls.Add(Me.optAK_Keep)
        Me.GroupBox1.Location = New System.Drawing.Point(19, 50)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(126, 76)
        Me.GroupBox1.TabIndex = 537
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Altersklassen"
        Me.GroupBox1.UseCompatibleTextRendering = True
        '
        'optAK_Group
        '
        Me.optAK_Group.AutoSize = True
        Me.optAK_Group.Location = New System.Drawing.Point(13, 47)
        Me.optAK_Group.Name = "optAK_Group"
        Me.optAK_Group.Size = New System.Drawing.Size(93, 18)
        Me.optAK_Group.TabIndex = 1
        Me.optAK_Group.TabStop = True
        Me.optAK_Group.Text = "Altersgruppen"
        Me.optAK_Group.UseCompatibleTextRendering = True
        Me.optAK_Group.UseVisualStyleBackColor = True
        '
        'optAK_Keep
        '
        Me.optAK_Keep.AutoSize = True
        Me.optAK_Keep.Location = New System.Drawing.Point(13, 24)
        Me.optAK_Keep.Name = "optAK_Keep"
        Me.optAK_Keep.Size = New System.Drawing.Size(109, 18)
        Me.optAK_Keep.TabIndex = 0
        Me.optAK_Keep.TabStop = True
        Me.optAK_Keep.Text = "zusammenhalten"
        Me.optAK_Keep.UseCompatibleTextRendering = True
        Me.optAK_Keep.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.optGK_Group)
        Me.GroupBox2.Controls.Add(Me.optGK_Keep)
        Me.GroupBox2.Location = New System.Drawing.Point(162, 50)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(130, 76)
        Me.GroupBox2.TabIndex = 538
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Gewichtsklassen"
        Me.GroupBox2.UseCompatibleTextRendering = True
        '
        'optGK_Group
        '
        Me.optGK_Group.AutoSize = True
        Me.optGK_Group.Location = New System.Drawing.Point(13, 47)
        Me.optGK_Group.Name = "optGK_Group"
        Me.optGK_Group.Size = New System.Drawing.Size(110, 18)
        Me.optGK_Group.TabIndex = 1
        Me.optGK_Group.TabStop = True
        Me.optGK_Group.Text = "Gewichtsgruppen"
        Me.optGK_Group.UseCompatibleTextRendering = True
        Me.optGK_Group.UseVisualStyleBackColor = True
        '
        'optGK_Keep
        '
        Me.optGK_Keep.AutoSize = True
        Me.optGK_Keep.Location = New System.Drawing.Point(13, 24)
        Me.optGK_Keep.Name = "optGK_Keep"
        Me.optGK_Keep.Size = New System.Drawing.Size(109, 18)
        Me.optGK_Keep.TabIndex = 0
        Me.optGK_Keep.TabStop = True
        Me.optGK_Keep.Text = "zusammenhalten"
        Me.optGK_Keep.UseCompatibleTextRendering = True
        Me.optGK_Keep.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 146)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(162, 17)
        Me.Label2.TabIndex = 539
        Me.Label2.Text = "maximale TN-Anzahl in Gruppe"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label2.UseCompatibleTextRendering = True
        '
        'nudMaxCount
        '
        Me.nudMaxCount.Location = New System.Drawing.Point(188, 146)
        Me.nudMaxCount.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudMaxCount.Name = "nudMaxCount"
        Me.nudMaxCount.Size = New System.Drawing.Size(54, 20)
        Me.nudMaxCount.TabIndex = 540
        Me.nudMaxCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudMaxCount.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'pnlOptionen
        '
        Me.pnlOptionen.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pnlOptionen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlOptionen.Controls.Add(Me.nudMaxCount)
        Me.pnlOptionen.Controls.Add(Me.Label2)
        Me.pnlOptionen.Controls.Add(Me.GroupBox2)
        Me.pnlOptionen.Controls.Add(Me.GroupBox1)
        Me.pnlOptionen.Controls.Add(Me.btnExit)
        Me.pnlOptionen.Controls.Add(Me.btnCancel)
        Me.pnlOptionen.Controls.Add(Me.btnApply)
        Me.pnlOptionen.Controls.Add(Me.lblOptionen)
        Me.pnlOptionen.Location = New System.Drawing.Point(828, 282)
        Me.pnlOptionen.Name = "pnlOptionen"
        Me.pnlOptionen.Size = New System.Drawing.Size(309, 230)
        Me.pnlOptionen.TabIndex = 501
        Me.pnlOptionen.Visible = False
        '
        'lblWG
        '
        Me.lblWG.BackColor = System.Drawing.Color.RoyalBlue
        Me.lblWG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWG.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWG.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblWG.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblWG.ForeColor = System.Drawing.Color.White
        Me.lblWG.Location = New System.Drawing.Point(0, 0)
        Me.lblWG.Name = "lblWG"
        Me.lblWG.Padding = New System.Windows.Forms.Padding(7, 0, 0, 0)
        Me.lblWG.Size = New System.Drawing.Size(287, 36)
        Me.lblWG.TabIndex = 501
        Me.lblWG.Text = "Wettkampf-Gruppe"
        Me.lblWG.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblWG.UseCompatibleTextRendering = True
        '
        'btnWG_Apply
        '
        Me.btnWG_Apply.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnWG_Apply.Enabled = False
        Me.btnWG_Apply.Location = New System.Drawing.Point(171, 54)
        Me.btnWG_Apply.MinimumSize = New System.Drawing.Size(77, 25)
        Me.btnWG_Apply.Name = "btnWG_Apply"
        Me.btnWG_Apply.Size = New System.Drawing.Size(96, 29)
        Me.btnWG_Apply.TabIndex = 535
        Me.btnWG_Apply.Text = "Übernehmen"
        Me.btnWG_Apply.UseCompatibleTextRendering = True
        Me.btnWG_Apply.UseVisualStyleBackColor = True
        '
        'btnWG_Exit
        '
        Me.btnWG_Exit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnWG_Exit.Location = New System.Drawing.Point(171, 89)
        Me.btnWG_Exit.MinimumSize = New System.Drawing.Size(77, 25)
        Me.btnWG_Exit.Name = "btnWG_Exit"
        Me.btnWG_Exit.Size = New System.Drawing.Size(96, 29)
        Me.btnWG_Exit.TabIndex = 536
        Me.btnWG_Exit.Text = "Abbrechen"
        Me.btnWG_Exit.UseCompatibleTextRendering = True
        Me.btnWG_Exit.UseVisualStyleBackColor = True
        '
        'btnWG_Cancel
        '
        Me.btnWG_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnWG_Cancel.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnWG_Cancel.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnWG_Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnWG_Cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.btnWG_Cancel.ForeColor = System.Drawing.Color.White
        Me.btnWG_Cancel.Location = New System.Drawing.Point(258, 6)
        Me.btnWG_Cancel.Name = "btnWG_Cancel"
        Me.btnWG_Cancel.Size = New System.Drawing.Size(23, 23)
        Me.btnWG_Cancel.TabIndex = 502
        Me.btnWG_Cancel.TabStop = False
        Me.btnWG_Cancel.Text = "X"
        Me.btnWG_Cancel.UseCompatibleTextRendering = True
        Me.btnWG_Cancel.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(26, 55)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 13)
        Me.Label3.TabIndex = 537
        Me.Label3.Text = "1. Wertungsgruppe"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(27, 106)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 13)
        Me.Label5.TabIndex = 538
        Me.Label5.Text = "2. Wertungsgruppe"
        '
        'cboWG_1
        '
        Me.cboWG_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWG_1.FormattingEnabled = True
        Me.cboWG_1.Location = New System.Drawing.Point(25, 73)
        Me.cboWG_1.Name = "cboWG_1"
        Me.cboWG_1.Size = New System.Drawing.Size(121, 21)
        Me.cboWG_1.TabIndex = 539
        '
        'cboWG_2
        '
        Me.cboWG_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWG_2.FormattingEnabled = True
        Me.cboWG_2.Location = New System.Drawing.Point(25, 123)
        Me.cboWG_2.Name = "cboWG_2"
        Me.cboWG_2.Size = New System.Drawing.Size(121, 21)
        Me.cboWG_2.TabIndex = 540
        '
        'pnlWG
        '
        Me.pnlWG.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pnlWG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlWG.Controls.Add(Me.cboWG_2)
        Me.pnlWG.Controls.Add(Me.cboWG_1)
        Me.pnlWG.Controls.Add(Me.Label5)
        Me.pnlWG.Controls.Add(Me.Label3)
        Me.pnlWG.Controls.Add(Me.btnWG_Cancel)
        Me.pnlWG.Controls.Add(Me.btnWG_Exit)
        Me.pnlWG.Controls.Add(Me.btnWG_Apply)
        Me.pnlWG.Controls.Add(Me.lblWG)
        Me.pnlWG.Location = New System.Drawing.Point(111, 227)
        Me.pnlWG.Name = "pnlWG"
        Me.pnlWG.Size = New System.Drawing.Size(289, 167)
        Me.pnlWG.TabIndex = 502
        Me.pnlWG.Visible = False
        '
        'optZugewiesen
        '
        Me.optZugewiesen.Appearance = System.Windows.Forms.Appearance.Button
        Me.optZugewiesen.Location = New System.Drawing.Point(41, 5)
        Me.optZugewiesen.Margin = New System.Windows.Forms.Padding(2)
        Me.optZugewiesen.Name = "optZugewiesen"
        Me.optZugewiesen.Size = New System.Drawing.Size(142, 29)
        Me.optZugewiesen.TabIndex = 505
        Me.optZugewiesen.Text = "Teilnehmer der Gruppe"
        Me.optZugewiesen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.optZugewiesen.UseVisualStyleBackColor = True
        '
        'optNichtZugewiesen
        '
        Me.optNichtZugewiesen.Appearance = System.Windows.Forms.Appearance.Button
        Me.optNichtZugewiesen.Location = New System.Drawing.Point(187, 6)
        Me.optNichtZugewiesen.Margin = New System.Windows.Forms.Padding(2)
        Me.optNichtZugewiesen.Name = "optNichtZugewiesen"
        Me.optNichtZugewiesen.Size = New System.Drawing.Size(141, 29)
        Me.optNichtZugewiesen.TabIndex = 506
        Me.optNichtZugewiesen.Text = "nicht zugewiesene TN"
        Me.optNichtZugewiesen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.optNichtZugewiesen.UseVisualStyleBackColor = True
        '
        'pnlTN
        '
        Me.pnlTN.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlTN.Controls.Add(Me.optNichtZugewiesen)
        Me.pnlTN.Controls.Add(Me.optZugewiesen)
        Me.pnlTN.Location = New System.Drawing.Point(883, 27)
        Me.pnlTN.Margin = New System.Windows.Forms.Padding(2)
        Me.pnlTN.Name = "pnlTN"
        Me.pnlTN.Size = New System.Drawing.Size(338, 39)
        Me.pnlTN.TabIndex = 507
        '
        'chkShowAll
        '
        Me.chkShowAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkShowAll.AutoSize = True
        Me.chkShowAll.Location = New System.Drawing.Point(754, 555)
        Me.chkShowAll.Name = "chkShowAll"
        Me.chkShowAll.Size = New System.Drawing.Size(170, 17)
        Me.chkShowAll.TabIndex = 508
        Me.chkShowAll.Text = "nich angetretene TN anzeigen"
        Me.chkShowAll.UseVisualStyleBackColor = True
        '
        'dgvUnallocated
        '
        Me.dgvUnallocated.AllowUserToAddRows = False
        Me.dgvUnallocated.AllowUserToDeleteRows = False
        Me.dgvUnallocated.AllowUserToResizeColumns = False
        Me.dgvUnallocated.AllowUserToResizeRows = False
        DataGridViewCellStyle23.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.dgvUnallocated.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle23
        Me.dgvUnallocated.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvUnallocated.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        Me.dgvUnallocated.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvUnallocated.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle24
        Me.dgvUnallocated.ColumnHeadersHeight = 24
        Me.dgvUnallocated.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvUnallocated.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.uTeilnehmer, Me.uNachname, Me.uVorname, Me.uSex, Me.uTeam, Me.uAK, Me.uJG, Me.uGK, Me.uWiegen, Me.uZK, Me.uNr})
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle32.BackColor = System.Drawing.Color.Orange
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvUnallocated.DefaultCellStyle = DataGridViewCellStyle32
        Me.dgvUnallocated.Location = New System.Drawing.Point(753, 130)
        Me.dgvUnallocated.Name = "dgvUnallocated"
        Me.dgvUnallocated.ReadOnly = True
        Me.dgvUnallocated.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvUnallocated.RowHeadersVisible = False
        Me.dgvUnallocated.RowHeadersWidth = 62
        Me.dgvUnallocated.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvUnallocated.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvUnallocated.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvUnallocated.Size = New System.Drawing.Size(456, 79)
        Me.dgvUnallocated.TabIndex = 509
        Me.dgvUnallocated.TabStop = False
        Me.dgvUnallocated.Visible = False
        '
        'uTeilnehmer
        '
        Me.uTeilnehmer.DataPropertyName = "Teilnehmer"
        Me.uTeilnehmer.HeaderText = "TN"
        Me.uTeilnehmer.MinimumWidth = 8
        Me.uTeilnehmer.Name = "uTeilnehmer"
        Me.uTeilnehmer.ReadOnly = True
        Me.uTeilnehmer.Visible = False
        Me.uTeilnehmer.Width = 150
        '
        'uNachname
        '
        Me.uNachname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.uNachname.DataPropertyName = "Nachname"
        Me.uNachname.FillWeight = 80.0!
        Me.uNachname.HeaderText = "Nachname"
        Me.uNachname.MinimumWidth = 8
        Me.uNachname.Name = "uNachname"
        Me.uNachname.ReadOnly = True
        '
        'uVorname
        '
        Me.uVorname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.uVorname.DataPropertyName = "Vorname"
        Me.uVorname.FillWeight = 80.0!
        Me.uVorname.HeaderText = "Vorname"
        Me.uVorname.MinimumWidth = 8
        Me.uVorname.Name = "uVorname"
        Me.uVorname.ReadOnly = True
        '
        'uSex
        '
        Me.uSex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.uSex.DataPropertyName = "Sex"
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.uSex.DefaultCellStyle = DataGridViewCellStyle25
        Me.uSex.FillWeight = 30.0!
        Me.uSex.HeaderText = "m/w"
        Me.uSex.MinimumWidth = 8
        Me.uSex.Name = "uSex"
        Me.uSex.ReadOnly = True
        Me.uSex.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.uSex.ToolTipText = "Geschlecht"
        Me.uSex.Width = 30
        '
        'uTeam
        '
        Me.uTeam.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.uTeam.DataPropertyName = "Vereinsname"
        Me.uTeam.FillWeight = 120.0!
        Me.uTeam.HeaderText = "Verein/Team"
        Me.uTeam.MinimumWidth = 8
        Me.uTeam.Name = "uTeam"
        Me.uTeam.ReadOnly = True
        Me.uTeam.Visible = False
        '
        'uAK
        '
        Me.uAK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.uAK.DataPropertyName = "AK"
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.uAK.DefaultCellStyle = DataGridViewCellStyle26
        Me.uAK.FillWeight = 65.0!
        Me.uAK.HeaderText = "AK"
        Me.uAK.MinimumWidth = 8
        Me.uAK.Name = "uAK"
        Me.uAK.ReadOnly = True
        Me.uAK.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.uAK.ToolTipText = "Altersklasse"
        Me.uAK.Width = 65
        '
        'uJG
        '
        Me.uJG.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.uJG.DataPropertyName = "JG"
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.uJG.DefaultCellStyle = DataGridViewCellStyle27
        Me.uJG.FillWeight = 50.0!
        Me.uJG.HeaderText = "JG"
        Me.uJG.MinimumWidth = 8
        Me.uJG.Name = "uJG"
        Me.uJG.ReadOnly = True
        Me.uJG.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.uJG.ToolTipText = "Jahrgang"
        Me.uJG.Visible = False
        Me.uJG.Width = 50
        '
        'uGK
        '
        Me.uGK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.uGK.DataPropertyName = "GK"
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle28.NullValue = Nothing
        DataGridViewCellStyle28.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.uGK.DefaultCellStyle = DataGridViewCellStyle28
        Me.uGK.FillWeight = 40.0!
        Me.uGK.HeaderText = "GK"
        Me.uGK.MinimumWidth = 8
        Me.uGK.Name = "uGK"
        Me.uGK.ReadOnly = True
        Me.uGK.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.uGK.ToolTipText = "Gewichtsklasse"
        Me.uGK.Width = 40
        '
        'uWiegen
        '
        Me.uWiegen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.uWiegen.DataPropertyName = "Wiegen"
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle29.Format = "0.00"
        DataGridViewCellStyle29.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.uWiegen.DefaultCellStyle = DataGridViewCellStyle29
        Me.uWiegen.FillWeight = 50.0!
        Me.uWiegen.HeaderText = "KG"
        Me.uWiegen.MinimumWidth = 8
        Me.uWiegen.Name = "uWiegen"
        Me.uWiegen.ReadOnly = True
        Me.uWiegen.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.uWiegen.ToolTipText = "Körpergewicht"
        Me.uWiegen.Width = 50
        '
        'uZK
        '
        Me.uZK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.uZK.DataPropertyName = "Zweikampf"
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle30.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.uZK.DefaultCellStyle = DataGridViewCellStyle30
        Me.uZK.FillWeight = 40.0!
        Me.uZK.HeaderText = "ZK"
        Me.uZK.MinimumWidth = 8
        Me.uZK.Name = "uZK"
        Me.uZK.ReadOnly = True
        Me.uZK.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.uZK.ToolTipText = "Zweikampf-Leistung"
        Me.uZK.Visible = False
        Me.uZK.Width = 40
        '
        'uNr
        '
        Me.uNr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.uNr.DataPropertyName = "Losnummer"
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.uNr.DefaultCellStyle = DataGridViewCellStyle31
        Me.uNr.FillWeight = 40.0!
        Me.uNr.HeaderText = "Nr"
        Me.uNr.MinimumWidth = 8
        Me.uNr.Name = "uNr"
        Me.uNr.ReadOnly = True
        Me.uNr.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.uNr.ToolTipText = "Losnummer"
        Me.uNr.Visible = False
        Me.uNr.Width = 40
        '
        'btnMoveLeft
        '
        Me.btnMoveLeft.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMoveLeft.Enabled = False
        Me.btnMoveLeft.Image = Global.Gewichtheben.My.Resources.Resources.arrow_left_16
        Me.btnMoveLeft.Location = New System.Drawing.Point(724, 72)
        Me.btnMoveLeft.Name = "btnMoveLeft"
        Me.btnMoveLeft.Size = New System.Drawing.Size(23, 23)
        Me.btnMoveLeft.TabIndex = 510
        Me.btnMoveLeft.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnMoveLeft, "markierte Teilnehmer in ausgewählte Gruppe verschieben")
        Me.btnMoveLeft.UseVisualStyleBackColor = True
        '
        'btnMoveRight
        '
        Me.btnMoveRight.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMoveRight.Enabled = False
        Me.btnMoveRight.Image = Global.Gewichtheben.My.Resources.Resources.arrow_right_16
        Me.btnMoveRight.Location = New System.Drawing.Point(724, 260)
        Me.btnMoveRight.Name = "btnMoveRight"
        Me.btnMoveRight.Size = New System.Drawing.Size(23, 23)
        Me.btnMoveRight.TabIndex = 511
        Me.btnMoveRight.TabStop = False
        Me.btnMoveRight.UseVisualStyleBackColor = True
        Me.btnMoveRight.Visible = False
        '
        'frmGruppen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(234, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1221, 588)
        Me.Controls.Add(Me.btnMoveRight)
        Me.Controls.Add(Me.btnMoveLeft)
        Me.Controls.Add(Me.dgvUnallocated)
        Me.Controls.Add(Me.chkShowAll)
        Me.Controls.Add(Me.pnlTN)
        Me.Controls.Add(Me.pnlWG)
        Me.Controls.Add(Me.pnlOptionen)
        Me.Controls.Add(Me.pnlDragRowIndicator)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtpZeit)
        Me.Controls.Add(Me.dtpDatum)
        Me.Controls.Add(Me.btnDown)
        Me.Controls.Add(Me.btnUp)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.dgvMeldung)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.dgvGruppen)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1237, 627)
        Me.MinimizeBox = False
        Me.Name = "frmGruppen"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Gruppen einteilen"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.dgvGruppen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmnuGruppe.ResumeLayout(False)
        CType(Me.dgvMeldung, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmnuHeber.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudMaxCount, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOptionen.ResumeLayout(False)
        Me.pnlOptionen.PerformLayout()
        Me.pnlWG.ResumeLayout(False)
        Me.pnlWG.PerformLayout()
        Me.pnlTN.ResumeLayout(False)
        CType(Me.dgvUnallocated, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDatei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpeichern As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuBeenden As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtras As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgvGruppen As DataGridView
    Friend WithEvents btnSave As Button
    Friend WithEvents dgvMeldung As DataGridView
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnRemove As Button
    Friend WithEvents btnUp As Button
    Friend WithEvents btnDown As Button
    Friend WithEvents btnClose As Button
    Friend WithEvents dtpDatum As DateTimePicker
    Friend WithEvents dtpZeit As DateTimePicker
    Friend WithEvents Label1 As Label
    Friend WithEvents pnlDragRowIndicator As Panel
    Friend WithEvents mnuKampfgericht As ToolStripMenuItem
    Friend WithEvents mnuOptionen As ToolStripMenuItem
    Friend WithEvents cmnuGruppe As ContextMenuStrip
    Friend WithEvents cmnuTeilen As ToolStripMenuItem
    Friend WithEvents cmnuHeber As ContextMenuStrip
    Friend WithEvents mnuMeldungen As ToolStripMenuItem
    Friend WithEvents mnuKonflikte As ToolStripMenuItem
    Friend WithEvents mnuTeilen As ToolStripMenuItem
    Friend WithEvents mnuExcelExport As ToolStripMenuItem
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents mnuWG As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As ToolStripSeparator
    Friend WithEvents cmnuWG As ToolStripMenuItem
    Friend WithEvents mnuDrucken As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents lblOptionen As Label
    Friend WithEvents btnApply As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents optAK_Group As RadioButton
    Friend WithEvents optAK_Keep As RadioButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents optGK_Group As RadioButton
    Friend WithEvents optGK_Keep As RadioButton
    Friend WithEvents Label2 As Label
    Friend WithEvents nudMaxCount As NumericUpDown
    Friend WithEvents pnlOptionen As Panel
    Friend WithEvents lblWG As Label
    Friend WithEvents btnWG_Apply As Button
    Friend WithEvents btnWG_Exit As Button
    Friend WithEvents btnWG_Cancel As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents cboWG_1 As ComboBox
    Friend WithEvents cboWG_2 As ComboBox
    Friend WithEvents pnlWG As Panel
    Friend WithEvents optZugewiesen As RadioButton
    Friend WithEvents optNichtZugewiesen As RadioButton
    Friend WithEvents pnlTN As Panel
    Friend WithEvents chkShowAll As CheckBox
    Friend WithEvents Teilnehmer As DataGridViewTextBoxColumn
    Friend WithEvents Nachname As DataGridViewTextBoxColumn
    Friend WithEvents Vorname As DataGridViewTextBoxColumn
    Friend WithEvents Sex As DataGridViewTextBoxColumn
    Friend WithEvents Team As DataGridViewTextBoxColumn
    Friend WithEvents AK As DataGridViewTextBoxColumn
    Friend WithEvents JG As DataGridViewTextBoxColumn
    Friend WithEvents GK As DataGridViewTextBoxColumn
    Friend WithEvents Wiegen As DataGridViewTextBoxColumn
    Friend WithEvents ZK As DataGridViewTextBoxColumn
    Friend WithEvents Nr As DataGridViewTextBoxColumn
    Friend WithEvents dgvUnallocated As DataGridView
    Friend WithEvents uTeilnehmer As DataGridViewTextBoxColumn
    Friend WithEvents uNachname As DataGridViewTextBoxColumn
    Friend WithEvents uVorname As DataGridViewTextBoxColumn
    Friend WithEvents uSex As DataGridViewTextBoxColumn
    Friend WithEvents uTeam As DataGridViewTextBoxColumn
    Friend WithEvents uAK As DataGridViewTextBoxColumn
    Friend WithEvents uJG As DataGridViewTextBoxColumn
    Friend WithEvents uGK As DataGridViewTextBoxColumn
    Friend WithEvents uWiegen As DataGridViewTextBoxColumn
    Friend WithEvents uZK As DataGridViewTextBoxColumn
    Friend WithEvents uNr As DataGridViewTextBoxColumn
    Friend WithEvents btnMoveLeft As Button
    Friend WithEvents btnMoveRight As Button
    Friend WithEvents mnuRemoveLifter As ToolStripMenuItem
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Gruppe As DataGridViewTextBoxColumn
    Friend WithEvents Teilgruppe As DataGridViewTextBoxColumn
    Friend WithEvents WG As DataGridViewTextBoxColumn
    Friend WithEvents Einteilung As DataGridViewTextBoxColumn
    Friend WithEvents Bezeichnung As DataGridViewTextBoxColumn
    Friend WithEvents TN As DataGridViewTextBoxColumn
    Friend WithEvents Datum As DataGridViewTextBoxColumn
    Friend WithEvents DatumW As DataGridViewTextBoxColumn
    Friend WithEvents WiegenBeginn As DataGridViewTextBoxColumn
    Friend WithEvents WiegenEnde As DataGridViewTextBoxColumn
    Friend WithEvents Reissen As DataGridViewTextBoxColumn
    Friend WithEvents Stossen As DataGridViewTextBoxColumn
    Friend WithEvents DatumA As DataGridViewTextBoxColumn
    Friend WithEvents Athletik As DataGridViewTextBoxColumn
    Friend WithEvents Bohle As DataGridViewTextBoxColumn
    Friend WithEvents Blockheben As DataGridViewCheckBoxColumn
End Class
