﻿
Imports System.Net.NetworkInformation
Imports System.Runtime.InteropServices

Public Class dlgLanDevices

    Private tTip As ToolTip
    Property InVisible As Boolean = False
    Property ReturnAtClick As Boolean = False
    Property Devices As DataTable

    Private _ix(1) As Integer
    Property devIndex(ix As Integer) As Integer
        Get
            Return _ix(ix)
        End Get
        Set(ByVal value As Integer)
            _ix(ix) = value
            btnCopy.Enabled = _ix(0) > -1 OrElse _ix(1) > -1
        End Set
    End Property

    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load

        Cursor = Cursors.WaitCursor

        devIndex(0) = -1
        devIndex(1) = -1

        Using GetDevices As New GetAllDevicesFromLan
            Devices = GetDevices.Get_Devices()
        End Using

        If InVisible Then
            Close()
            Return
        End If

        tTip = New ToolTip()
        tTip.AutoPopDelay = 5000
        tTip.InitialDelay = 1000
        tTip.ReshowDelay = 500
        tTip.ShowAlways = True
        tTip.SetToolTip(btnCopy, "ausgewähltes Gerät übernehmen")
    End Sub
    Private Sub Me_Show(sender As Object, e As EventArgs) Handles Me.Shown
        dgvDevices.AutoGenerateColumns = False
        dgvDevices.DataSource = Devices
        Cursor = Cursors.Default
    End Sub

    Private Sub btnCopy_Click(sender As Object, e As EventArgs) Handles btnCopy.Click
        'Dim Pfad = "\\GFHsoft-Leader\Gerd Friedrich\gfhtmp"
        'Dim Datei = "server.gfh"
        'If Not Directory.Exists(Pfad) Then Directory.CreateDirectory(Pfad)
        'File.WriteAllText(Path.Combine(Pfad, Datei), "192.168.178.20")

        Close()
    End Sub

    Private Sub dgvDevices_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvDevices.CellMouseDown
        If e.Button = MouseButtons.Right Then
            If e.RowIndex > -1 Then dgvDevices.CurrentCell = dgvDevices(0, e.RowIndex)
        End If
    End Sub

    Private Sub mnuDbServer_Click(sender As Object, e As EventArgs) Handles mnuDbServer.Click
        With dgvDevices
            .ClearSelection()
            If devIndex(0) = -1 Then
                devIndex(0) = .CurrentRow.Index
                .CurrentRow.DefaultCellStyle.BackColor = Color.FromArgb(255, 192, 128)
            ElseIf devIndex(0) <> .CurrentRow.Index Then
                .Rows(devIndex(0)).DefaultCellStyle.BackColor = If(devIndex(0) Mod 2 = 0, .RowsDefaultCellStyle.BackColor, .RowsDefaultCellStyle.SelectionBackColor)
                .CurrentRow.DefaultCellStyle.BackColor = Color.FromArgb(255, 192, 128)
                devIndex(0) = .CurrentRow.Index
            ElseIf devIndex(0) = .CurrentRow.Index Then
                .Rows(devIndex(0)).DefaultCellStyle.BackColor = If(devIndex(0) Mod 2 = 0, .RowsDefaultCellStyle.BackColor, .RowsDefaultCellStyle.SelectionBackColor)
                devIndex(0) = -1
            End If
        End With
    End Sub
    Private Sub mnuDbServer_EnabledChanged(sender As Object, e As EventArgs) Handles mnuDbServer.EnabledChanged
        Try
            With dgvDevices
                .ClearSelection()
                If devIndex(0) > -1 Then
                    .Rows(devIndex(0)).DefaultCellStyle.BackColor = If(devIndex(0) Mod 2 = 0, .RowsDefaultCellStyle.BackColor, .RowsDefaultCellStyle.SelectionBackColor)
                    devIndex(0) = -1
                End If
            End With
        Catch ex As Exception
        End Try
    End Sub
    Private Sub mnuComServer_Click(sender As Object, e As EventArgs) Handles mnuComServer.Click
        With dgvDevices
            .ClearSelection()
            If devIndex(1) = -1 Then
                devIndex(1) = .CurrentRow.Index
                .CurrentRow.DefaultCellStyle.BackColor = Color.LightGreen
            ElseIf devIndex(1) <> .CurrentRow.Index Then
                .Rows(devIndex(1)).DefaultCellStyle.BackColor = If(devIndex(1) Mod 2 = 0, .RowsDefaultCellStyle.BackColor, .RowsDefaultCellStyle.SelectionBackColor)
                .CurrentRow.DefaultCellStyle.BackColor = Color.LightGreen
                devIndex(1) = .CurrentRow.Index
            ElseIf devIndex(1) = .CurrentRow.Index Then
                .Rows(devIndex(1)).DefaultCellStyle.BackColor = If(devIndex(1) Mod 2 = 0, .RowsDefaultCellStyle.BackColor, .RowsDefaultCellStyle.SelectionBackColor)
                devIndex(1) = -1
            End If
        End With
    End Sub
    Private Sub mnuComServer_EnabledChanged(sender As Object, e As EventArgs) Handles mnuComServer.EnabledChanged
        With dgvDevices
            .ClearSelection()
            If devIndex(1) > -1 Then
                .Rows(devIndex(1)).DefaultCellStyle.BackColor = If(devIndex(1) Mod 2 = 0, .RowsDefaultCellStyle.BackColor, .RowsDefaultCellStyle.SelectionBackColor)
                devIndex(1) = -1
            End If
        End With
    End Sub
    Private Sub mnuBeide_Click(sender As Object, e As EventArgs) Handles mnuBeide.Click
        mnuComServer.Enabled = Not mnuBeide.Checked
        mnuDbServer.Enabled = Not mnuBeide.Checked
        With dgvDevices
            .ClearSelection()
            If mnuBeide.Checked Then
                If devIndex(0) > -1 Then
                    .Rows(devIndex(0)).DefaultCellStyle.BackColor = If(devIndex(0) Mod 2 = 0, .RowsDefaultCellStyle.BackColor, .RowsDefaultCellStyle.SelectionBackColor)
                    devIndex(0) = -1
                    devIndex(1) = -1
                End If
            Else
                .CurrentRow.DefaultCellStyle.BackColor = Color.Yellow
                devIndex(0) = .CurrentRow.Index
                devIndex(1) = .CurrentRow.Index
            End If
        End With
    End Sub

    Private Sub dgvDevices_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDevices.CellContentClick

    End Sub
End Class

Class GetAllDevicesFromLan
    Implements IDisposable

    <StructLayout(LayoutKind.Sequential)>
    Structure MIB_IPNETROW
        <MarshalAs(UnmanagedType.U4)>
        Public dwIndex As Integer
        <MarshalAs(UnmanagedType.U4)>
        Public dwPhysAddrLen As Integer
        <MarshalAs(UnmanagedType.U1)>
        Public mac0 As Byte
        <MarshalAs(UnmanagedType.U1)>
        Public mac1 As Byte
        <MarshalAs(UnmanagedType.U1)>
        Public mac2 As Byte
        <MarshalAs(UnmanagedType.U1)>
        Public mac3 As Byte
        <MarshalAs(UnmanagedType.U1)>
        Public mac4 As Byte
        <MarshalAs(UnmanagedType.U1)>
        Public mac5 As Byte
        <MarshalAs(UnmanagedType.U1)>
        Public mac6 As Byte
        <MarshalAs(UnmanagedType.U1)>
        Public mac7 As Byte
        <MarshalAs(UnmanagedType.U4)>
        Public dwAddr As Integer
        <MarshalAs(UnmanagedType.U4)>
        Public dwType As Integer
    End Structure

    Declare Function GetIpNetTable Lib "IpHlpApi.dll" (ByVal pIpNetTable As IntPtr, <MarshalAs(UnmanagedType.U4)> ByRef pdwSize As Integer, ByVal bOrder As Boolean) As Integer
    Const ERROR_INSUFFICIENT_BUFFER As Integer = 122

    Public Function Get_Devices() As DataTable
        Dim dtDevices As New DataTable
        Dim cols(2) As DataColumn
        cols(0) = New DataColumn With {.ColumnName = "Device", .DataType = GetType(String)}
        cols(1) = New DataColumn With {.ColumnName = "IP", .DataType = GetType(String)}
        cols(2) = New DataColumn With {.ColumnName = "MAC", .DataType = GetType(String)}
        dtDevices.Columns.AddRange(cols)
        dtDevices.PrimaryKey = {cols(1)}

        Dim strHost As String = Dns.GetHostName()                 ' Host
        Dim strRouter = Dns.GetHostEntry(GetIPAddress()).HostName ' Host.Router
        Dim intRemove = strRouter.Length - strHost.Length

        dtDevices.Rows.Add({strHost, GetIPAddress(), "[Host]"}) 'GetMacAddress()})

        Dim spaceForNetTable As Integer = 0
        GetIpNetTable(IntPtr.Zero, spaceForNetTable, False)
        Dim rawTable As IntPtr = IntPtr.Zero

        Try
            rawTable = Marshal.AllocCoTaskMem(spaceForNetTable)
            Dim errorCode As Integer = GetIpNetTable(rawTable, spaceForNetTable, False)

            If errorCode <> 0 Then
                Throw New Exception(String.Format("Kann Netzwerk-Tabelle nicht lesen. Error {0}", errorCode))
            End If

            Dim rowsCount As Integer = Marshal.ReadInt32(rawTable)
            Dim currentBuffer As IntPtr = New IntPtr(rawTable.ToInt64() + Marshal.SizeOf(GetType(Int32)))
            Dim rows As MIB_IPNETROW() = New MIB_IPNETROW(rowsCount - 1) {}

            For index As Integer = 0 To rowsCount - 1
                rows(index) = CType(Marshal.PtrToStructure(New IntPtr(currentBuffer.ToInt64() + (index * Marshal.SizeOf(GetType(MIB_IPNETROW)))), GetType(MIB_IPNETROW)), MIB_IPNETROW)
            Next

            Dim virtualMAC As PhysicalAddress = New PhysicalAddress(New Byte() {0, 0, 0, 0, 0, 0})
            Dim broadcastMAC As PhysicalAddress = New PhysicalAddress(New Byte() {255, 255, 255, 255, 255, 255})

            For Each row As MIB_IPNETROW In rows
                Dim ip As IPAddress = New IPAddress(BitConverter.GetBytes(row.dwAddr))
                Dim rawMAC As Byte() = New Byte() {row.mac0, row.mac1, row.mac2, row.mac3, row.mac4, row.mac5}
                Dim pa As PhysicalAddress = New PhysicalAddress(rawMAC)

                If Not pa.Equals(virtualMAC) AndAlso Not pa.Equals(broadcastMAC) AndAlso Not IsMulticast(ip) Then

                    Dim _mac = New List(Of String)
                    If Not IsNothing(pa) Then
                        For i = 0 To pa.ToString.Length - 2 Step 2
                            _mac.Add(pa.ToString.Substring(i, 2))
                        Next
                    Else
                        _mac.Add("[Host]") ' für Host ist keine MAC-Adresse verfügbar
                    End If

                    Try
                        dtDevices.Rows.Add({GetHostName(ip, intRemove), ip.ToString, String.Join(":", _mac.ToArray)})
                    Catch ex As Exception
                    End Try

                End If
            Next

        Finally
            Marshal.FreeCoTaskMem(rawTable)
        End Try

        Return dtDevices
    End Function

    Private Function GetIPAddress() As String
        Dim strHostName As String = Dns.GetHostName()
        Dim ipEntry As IPHostEntry = Dns.GetHostEntry(strHostName)
        Dim addr As IPAddress() = ipEntry.AddressList

        For Each ip As IPAddress In addr
            If Not ip.IsIPv6LinkLocal Then
                Return ip.ToString
            End If
        Next
        Return If(addr.Length > 0, addr(0).ToString, Nothing)
    End Function
    Private Function GetMacAddress() As String
        For Each nic As NetworkInterface In NetworkInterface.GetAllNetworkInterfaces()

            If nic.NetworkInterfaceType = NetworkInterfaceType.Ethernet AndAlso nic.OperationalStatus = OperationalStatus.Up Then
                Return nic.GetPhysicalAddress().ToString
            End If
        Next

        Return Nothing
    End Function
    Private Function GetHostName(IP As IPAddress, remove As Integer) As String
        Try
            Dim ipEntry As IPHostEntry = Dns.GetHostEntry(IP)

            Return If(ipEntry.HostName.Length - remove < 0, ipEntry.HostName, ipEntry.HostName.Remove(ipEntry.HostName.Length - remove))
        Catch ex As Exception
            Return "[unbekannt]"
        End Try

    End Function

    Private Function IsMulticast(ByVal ip As IPAddress) As Boolean
        Dim result As Boolean = True

        If Not ip.IsIPv6Multicast Then
            Dim highIP As Byte = ip.GetAddressBytes()(0)

            If highIP < 224 OrElse highIP > 239 Then
                result = False
            End If
        End If

        Return result
    End Function
    Public Sub Dispose() Implements IDisposable.Dispose
    End Sub

End Class

