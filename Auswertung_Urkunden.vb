﻿
Imports System.ComponentModel
Imports MySqlConnector

Public Class Auswertung_Urkunden

    Dim dicBereich As New Dictionary(Of String, Integer)
    Dim Bereich As String = "Urkunde"
    Dim Vorlage As New List(Of String)
    Dim dtVorlage As New DataTable
    Dim dvVorlage As DataView

    Dim report As FastReport.Report

    Dim dtMeldung As DataTable
    Dim dtGruppen As DataTable

    Dim dvU As DataView
    Dim dvM As DataView
    Dim dvH As DataView

    Dim bsU As BindingSource
    Dim bsM As BindingSource
    Dim bsH As BindingSource

    Dim Query As String
    Dim cmd As MySqlCommand

    'Dim SelectedCount As Integer = 3 ' Anzahl der Plätze, die Urkunden erhalten --> 1, 3, nudBis.Value, -1, -2
    Dim RowExpanded As Integer
    Dim Filter() As String = {"Verein", "Region"}

    Dim dvTeams(1) As DataView
    Dim dvHeber(1) As DataView
    Dim bsTeams(1) As BindingSource
    Dim bsHeber(1) As BindingSource

    ' Optionen
    Dim Design As String = "Standard"

    Private Sub Me_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        For i = 0 To dicBereich.Count - 1
            If Not String.IsNullOrEmpty(Vorlage(i)) Then
                NativeMethods.INI_Write(dicBereich.Keys(i), "Vorlage", Vorlage(i), App_IniFile)
            End If
        Next
    End Sub
    Private Sub Me_Load(sender As Object, e As EventArgs) Handles Me.Load

        Cursor = Cursors.WaitCursor

#Region "Vorlage"
        dicBereich.Add("Urkunde_Verein", 0)
        dicBereich.Add("Urkunde_Länder", 1)
        dicBereich.Add("Urkunde_Einzel", 2)

        For i = 0 To 2
            Dim x = NativeMethods.INI_Read(dicBereich.Keys(i), "Vorlage", App_IniFile)
            If x.Equals("?") Then x = String.Empty
            Vorlage.Add(x)
        Next

        dtVorlage.Columns.Add("FileName", GetType(String))
        dtVorlage.Columns.Add("Directory", GetType(String))
        Dim cols(1) As DataColumn
        cols(0) = dtVorlage.Columns("FileName")
        cols(1) = dtVorlage.Columns("Directory")
        dtVorlage.PrimaryKey = cols

        Dim v = Directory.GetFiles(Path.Combine(Application.StartupPath, "Report"), Bereich + "*.frx", SearchOption.AllDirectories).ToList
        For i = 0 To v.Count - 1
            v(i) = Replace(v(i), Path.Combine(Application.StartupPath, "Report") + "\", "")
            v(i) = Replace(v(i), ".frx", "")
            dtVorlage.Rows.Add(v(i), Path.Combine(Application.StartupPath, "Report"))
        Next
        dvVorlage = New DataView(dtVorlage)
        dvVorlage.Sort = "FileName"

        With cboVorlage
            .DataSource = dvVorlage
            .DisplayMember = "FileName"
            .ValueMember = "Directory"
        End With
#End Region

    End Sub
    Private Sub Me_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                Query = "Select Gruppe, Teilgruppe, Bezeichnung, Datum, Concat(Gruppe, ' - ', Bezeichnung) Label " &
                        "FROM gruppen WHERE Wettkampf = " & Wettkampf.ID & " ORDER BY Gruppe;"
                cmd = New MySqlCommand(Query, conn)
                dtGruppen = New DataTable
                dtGruppen.Load(cmd.ExecuteReader)
                Query = "SELECT m.*, 
                                a.Titel, a.Nachname, a.Vorname, a.Geschlecht, 
                                v.Vereinsname, v.Kurzname, 
                                r.ZK, r.Heben, r.Heben_Platz, r.Wertung2, r.Wertung2_Platz,  
                                (select Gruppierung from wettkampf_wertung where Wettkampf = m.Wettkampf and idAk = m.idAK) Gruppierungen, 
                                (select group_concat(distinct substring_index(e.AK, ' ', 1) order by e.idAK separator ', ') 
                                from wettkampf_wertung w 
                                right join meldung e on e.Wettkampf = w.Wettkampf and e.idAk = w.idAK 
                                where w.Wettkampf = m.Wettkampf and w.Gruppierung = Gruppierungen 
                                group by w.Wettkampf, w.Gruppierung 
                                having not isnull(group_concat(distinct w.idAK))) Altersklassen 
                        FROM meldung m 
                        LEFT JOIN athleten a ON a.idTeilnehmer = m.Teilnehmer 
                        LEFT JOIN verein v ON v.idVerein = a.Verein 
                        LEFT JOIN results r ON r.Teilnehmer = m.Teilnehmer AND r.Wettkampf = m.Wettkampf 
                        WHERE " & Wettkampf.WhereClause("m") & "AND m.attend = True AND m.a_K = False;"
                cmd = New MySqlCommand(Query, conn)
                dtMeldung = New DataTable
                dtMeldung.Load(cmd.ExecuteReader)

                'Query = "
                '                SELECT distinct m.Gruppe, a.Titel, a.Nachname, a.Vorname, a.Geschlecht, a.Jahrgang, a.Verein, IfNull(v.Kurzname, v.Vereinsname) Vereinsname, m.idAK, m.AK, m.idGK, m.GK, v.Region, reg.region_3, reg.region_name, 
                '                IfNull(m.Wiegen, m.Gewicht) Wiegen, m.Laenderwertung, m.Vereinswertung, R_Last, R_Note, S_Last, S_Note, rs.ZK, 
                '                rs.Reissen, rs.Reissen_Platz, rs.Stossen, rs.Stossen_Platz, rs.Heben, rs.Heben_Platz, rs.Athletik, rs.Gesamt, rs.Gesamt_Platz, rs.Wertung2, rs.Wertung2_Platz,
                '                (select Gruppierung from wettkampf_wertung where Wettkampf = m.Wettkampf and idAk = m.idAK) Gruppierungen,
                '                (
                '                select group_concat(distinct substring_index(m.AK, ' ', 1) order by m.idAK separator ', ') Altersklasse
                '                from wettkampf_wertung w
                '                right join meldung m on m.Wettkampf = w.Wettkampf and m.idAk = w.idAK 
                '                where w.Wettkampf = " & Wettkampf.ID & " and w.Gruppierung = Gruppierungen
                '                group by w.Wettkampf, Gruppierung
                '                having not isnull(group_concat(distinct w.idAK))
                '                ) Altersklassen
                '                FROM meldung m 
                '                LEFT JOIN (SELECT r.Teilnehmer, r.HLast R_Last, r.Note R_Note 
                '                FROM reissen r 
                '                WHERE r.Wettkampf = Wettkampf And r.HLast = (SELECT MAX(HLast*Wertung) FROM reissen WHERE Teilnehmer = r.Teilnehmer AND r.Wertung = 1)) r 
                '                ON m.Teilnehmer = r.Teilnehmer 
                '                LEFT JOIN (SELECT s.Teilnehmer, s.HLast S_Last, s.Note S_Note 
                '                FROM stossen s 
                '                WHERE s.Wettkampf = Wettkampf And s.HLast = (SELECT MAX(HLast*Wertung) FROM stossen WHERE Teilnehmer = s.Teilnehmer AND s.Wertung = 1)) s 
                '                ON m.Teilnehmer = s.Teilnehmer 
                '                LEFT JOIN athleten a ON a.idTeilnehmer = m.Teilnehmer 
                '                LEFT JOIN gruppen g ON g.Wettkampf = m.Wettkampf and g.Gruppe = m.Gruppe 
                '                LEFT JOIN results rs ON rs.Wettkampf = m.Wettkampf And rs.Teilnehmer = m.Teilnehmer 
                '                LEFT JOIN verein v ON v.idVerein = a.Verein 
                '                LEFT JOIN region reg ON reg.region_id = v.Region 
                '                WHERE m.Wettkampf = " & Wettkampf.ID & " AND m.attend = True AND m.a_K = False
                '                ORDER BY a.Nachname, a.Vorname, a.Jahrgang, Wiegen;"
                'cmd = New MySqlCommand(Query, conn)
                'dtMeldung = New DataTable
                'dtMeldung.Load(cmd.ExecuteReader)
                '        '' Auswertung Mannschaften
                '        'Query = "SELECT a.Titel, a.Nachname, a.Vorname, a.Geschlecht, a.Jahrgang, v.idVerein, IfNull(v.Kurzname, v.Vereinsname) Vereinsname, reg.region_id, reg.region_3, reg.region_name, " &
                '        '        "m.Laenderwertung, m.Vereinswertung, rs.Heben, rs.Gesamt, rs.Wertung2 " &
                '        '        "FROM meldung m " &
                '        '        "LEFT JOIN athleten a On a.idTeilnehmer = m.Teilnehmer " &
                '        '        "LEFT JOIN results rs On rs.Wettkampf = m.Wettkampf And rs.Teilnehmer = m.Teilnehmer " &
                '        '        "LEFT JOIN verein v On v.idVerein = a.Verein " &
                '        '        "LEFT JOIN region reg On reg.region_id = v.Region " &
                '        '        "WHERE m.Wettkampf = " & Wettkampf.ID & '" And m.attend = True And m.a_K = False " &
                '        '        " ORDER BY Wertung2, Gesamt, Heben, Nachname, Vorname;"
                '        'cmd = New MySqlCommand(Query, conn)
                '        'dtM.Load(cmd.ExecuteReader())


            Catch ex As Exception
                Return
            End Try
        End Using

        ' Urkunden
        dvU = New DataView(dtMeldung)
        bsU = New BindingSource With {.DataSource = dvU, .Filter = "Wettkampf = " & Wettkampf.ID}
        'bsU.Filter = "Convert(IsNull(Heben_Platz,''), System.String) = ''" & If(Wettkampf.Athletik, " AND Convert(IsNull(Gesamt_Platz,''), System.String) = '' ", "")

        Dim tmpSx = New DataView(dtMeldung.DefaultView.ToTable(True, "Geschlecht"))
        With cboSex
            .DataSource = tmpSx
            .DisplayMember = "Geschlecht"
            .SelectedIndex = -1
        End With

        Dim tmpGrp = New DataView(dtMeldung.DefaultView.ToTable(True, {"Gruppierungen", "Altersklassen"}))
        tmpGrp.Sort = "Gruppierungen"
        With cboGruppierung
            .DataSource = tmpGrp
            .DisplayMember = "Altersklassen"
            .ValueMember = "Gruppierungen"
            .SelectedIndex = -1
        End With

        With cboGruppe
            .DataSource = dtGruppen
            .DisplayMember = "Label"
            .ValueMember = "Gruppe"
            .SelectedIndex = -1
        End With

        With cboWettkampf
            .DataSource = Glob.dtWK
            .ValueMember = "Wettkampf"
            .DisplayMember = "Bez1"
            .SelectedValue = Wettkampf.ID
        End With

        'Get_Data(Wettkampf.ID.ToString)

        '   lblCaption.Text = Wettkampf.Bezeichnung

        'dvM = New DataView(dtM)
        'bsM = New BindingSource With {.DataSource = dvM}

        cboAuswertung.SelectedIndex = 0

        Cursor = Cursors.Default
    End Sub

    Private Sub Get_Data(Id As String)
        Using conn As New MySqlConnection(User.ConnString)
            Try
                conn.Open()
                'Query = "
                '        SELECT distinct m.Gruppe, a.Titel, a.Nachname, a.Vorname, a.Geschlecht, a.Jahrgang, a.Verein, IfNull(v.Kurzname, v.Vereinsname) Vereinsname, m.idAK, m.AK, m.idGK, m.GK, v.Region, reg.region_3, reg.region_name, 
                '        IfNull(m.Wiegen, m.Gewicht) Wiegen, m.Laenderwertung, m.Vereinswertung, R_Last, R_Note, S_Last, S_Note, rs.ZK, 
                '        rs.Reissen, rs.Reissen_Platz, rs.Stossen, rs.Stossen_Platz, rs.Heben, rs.Heben_Platz, rs.Athletik, rs.Gesamt, rs.Gesamt_Platz, rs.Wertung2, rs.Wertung2_Platz,
                '        (select Gruppierung from wettkampf_wertung where Wettkampf = m.Wettkampf and idAk = m.idAK) Gruppierungen,
                '        (
                '        select group_concat(distinct substring_index(m.AK, ' ', 1) order by m.idAK separator ', ') Altersklasse
                '        from wettkampf_wertung w
                '        right join meldung m on m.Wettkampf = w.Wettkampf and m.idAk = w.idAK 
                '        where w.Wettkampf = " & Id & " and w.Gruppierung = Gruppierungen
                '        group by w.Wettkampf, Gruppierung
                '        having not isnull(group_concat(distinct w.idAK))
                '        ) Altersklassen
                '        FROM meldung m 
                '        LEFT JOIN (SELECT r.Teilnehmer, r.HLast R_Last, r.Note R_Note 
                '        FROM reissen r 
                '        WHERE r.Wettkampf = Wettkampf And r.HLast = (SELECT MAX(HLast*Wertung) FROM reissen WHERE Teilnehmer = r.Teilnehmer AND r.Wertung = 1)) r 
                '        ON m.Teilnehmer = r.Teilnehmer 
                '        LEFT JOIN (SELECT s.Teilnehmer, s.HLast S_Last, s.Note S_Note 
                '        FROM stossen s 
                '        WHERE s.Wettkampf = Wettkampf And s.HLast = (SELECT MAX(HLast*Wertung) FROM stossen WHERE Teilnehmer = s.Teilnehmer AND s.Wertung = 1)) s 
                '        ON m.Teilnehmer = s.Teilnehmer 
                '        LEFT JOIN athleten a ON a.idTeilnehmer = m.Teilnehmer 
                '        LEFT JOIN gruppen g ON g.Wettkampf = m.Wettkampf and g.Gruppe = m.Gruppe 
                '        LEFT JOIN results rs ON rs.Wettkampf = m.Wettkampf And rs.Teilnehmer = m.Teilnehmer 
                '        LEFT JOIN verein v ON v.idVerein = a.Verein 
                '        LEFT JOIN region reg ON reg.region_id = v.Region 
                '        WHERE m.Wettkampf = " & Id & " AND m.attend = True AND m.a_K = False
                '        ORDER BY a.Nachname, a.Vorname, a.Jahrgang, Wiegen;"
                Query = "SELECT m.Wettkampf, m.idAK, m.AK, m.idGK, m.GK, m.Gruppe, a.Titel, a.Nachname, a.Vorname, a.Geschlecht, v.Vereinsname, v.Kurzname, r.ZK, r.Heben, r.Heben_Platz, r.Wertung2, r.Wertung2_Platz,
(select Gruppierung from wettkampf_wertung where Wettkampf = m.Wettkampf and idAk = m.idAK) Gruppierungen,
(select group_concat(distinct substring_index(e.AK, ' ', 1) order by e.idAK separator ', ') 
from wettkampf_wertung w
right join meldung e on e.Wettkampf = w.Wettkampf and e.idAk = w.idAK 
where w.Wettkampf = m.Wettkampf and w.Gruppierung = Gruppierungen
group by w.Wettkampf, w.Gruppierung
having not isnull(group_concat(distinct w.idAK))) Altersklassen
FROM meldung m
LEFT JOIN athleten a ON a.idTeilnehmer = m.Teilnehmer
LEFT JOIN verein v ON v.idVerein = a.Verein
LEFT JOIN results r ON r.Teilnehmer = m.Teilnehmer AND r.Wettkampf = m.Wettkampf
WHERE " & Wettkampf.WhereClause("m") & ";"
                cmd = New MySqlCommand(Query, conn)
                dtMeldung = New DataTable
                dtMeldung.Load(cmd.ExecuteReader())
            Catch ex As Exception
            End Try
        End Using

        dvU = New DataView(dtMeldung)
        bsU = New BindingSource With {.DataSource = dvU}

        Dim tmpSx = New DataView(dtMeldung.DefaultView.ToTable(True, "Geschlecht"))
        With cboSex
            .DataSource = tmpSx
            .DisplayMember = "Geschlecht"
            .SelectedIndex = -1
        End With

        Dim tmpGrp = New DataView(dtMeldung.DefaultView.ToTable(True, {"Gruppierungen", "Altersklassen"}))
        tmpGrp.Sort = "Gruppierungen"
        With cboGruppierung
            .DataSource = tmpGrp
            .DisplayMember = "Altersklassen"
            .ValueMember = "Gruppierungen"
            .SelectedIndex = -1
        End With
    End Sub

    Private Sub Build_Grid(Bereich As String)

        If IsNothing(Bereich) Then Return
        Cursor = Cursors.WaitCursor

        Dim ColBez As New List(Of String)
        Dim ColData As New List(Of String)
        Dim ColCenter As New List(Of String)
        Dim ColRight As New List(Of String)

        With dgvJoin
            .AutoGenerateColumns = False
            Select Case Design
                Case "Standard"
                    .AlternatingRowsDefaultCellStyle.BackColor = AlternatingRowsDefaultCellStyle_BackColor
                    .RowsDefaultCellStyle.BackColor = RowsDefaultCellStyle_BackColor
                    .BackgroundColor = SystemColors.AppWorkspace
                    .GridColor = SystemColors.ControlDark
            End Select
            .Columns.Clear()

            If Bereich.Equals("Urkunde_Einzel") Then

                ColBez.AddRange({"Nachname", "Vorname", "Verein", "BL", "Reißen", "Stoßen", "Heben", "Athletik", "Gesamt", "2.Wertung"})
                ColData.AddRange({"Nachname", "Vorname", "Vereinsname", "region_3", "Reissen_Platz", "Stossen_Platz", "Heben_Platz", "Athletik_Platz", "Gesamt_Platz", "Wertung2_Platz"})
                ColCenter.AddRange({"BL", "Reißen", "Stoßen", "Heben", "Athletik", "Gesamt", "Wertung2"})

                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Nachname", .FillWeight = 90, .SortMode = DataGridViewColumnSortMode.NotSortable})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Vorname", .FillWeight = 90, .SortMode = DataGridViewColumnSortMode.NotSortable})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Verein", .FillWeight = 100, .SortMode = DataGridViewColumnSortMode.NotSortable})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "BL", .FillWeight = 40, .SortMode = DataGridViewColumnSortMode.NotSortable})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Reißen", .FillWeight = 50, .SortMode = DataGridViewColumnSortMode.Automatic})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Stoßen", .FillWeight = 50, .SortMode = DataGridViewColumnSortMode.Automatic})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Heben", .FillWeight = 50, .SortMode = DataGridViewColumnSortMode.Automatic})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Athletik", .FillWeight = 50, .Visible = Wettkampf.Athletik, .SortMode = DataGridViewColumnSortMode.Automatic})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Gesamt", .FillWeight = 50, .Visible = Wettkampf.Athletik, .SortMode = DataGridViewColumnSortMode.Automatic})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Wertung2", .FillWeight = 50, .Visible = Wettkampf.Platzierung > 9, .SortMode = DataGridViewColumnSortMode.Automatic})

            Else

                Dim NK_Stellen = Get_NK_Stellen(Glob.Get_Wertung.Key)

                ColBez.AddRange({"Verein", "Land", "BL", "Punkte", "Platz"})
                ColData.AddRange({"Vereinsname", "region_name", "region_3", "Punkte", "Platz"})
                ColCenter.AddRange({"BL", "Platz"})
                ColRight.AddRange({"Punkte"})

                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Verein", .FillWeight = 100})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Land", .FillWeight = 100})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "BL", .FillWeight = 40})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Punkte", .FillWeight = 50})
                .Columns.Add(New DataGridViewTextBoxColumn With {.Name = "Platz", .FillWeight = 40})

                .Columns("Verein").Visible = Bereich.Contains("Verein")
                Dim FormatString = "0." & StrDup(NK_Stellen, "0")
                .Columns("Punkte").DefaultCellStyle.Format = FormatString

            End If

            For i = 0 To ColBez.Count - 1
                .Columns(i).HeaderText = ColBez(i)
                .Columns(i).DataPropertyName = ColData(i)
                .Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            Next
            For i = 0 To ColCenter.Count - 1
                .Columns(ColCenter(i)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            Next
            For i = 0 To ColRight.Count - 1
                .Columns(ColRight(i)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            Next

        End With
        Cursor = Cursors.Default
    End Sub

    Private Sub cboAuswertung_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAuswertung.SelectedIndexChanged

        If IsNothing(bsU) Then Return

        pnlFilter.Enabled = cboAuswertung.SelectedIndex < 4
        chkSelectAll.Enabled = cboAuswertung.SelectedIndex < 4

        If cboAuswertung.SelectedIndex < 4 OrElse cboAuswertung.Text.Equals("2. Wertung") Then
            Bereich = dicBereich.Keys(2) ' Einzel
            Build_Grid(Bereich)
            dgvJoin.DataSource = bsU
            dgvJoin.MultiSelect = True
        Else
            Bereich = dicBereich.Keys(cboAuswertung.SelectedIndex - 4) ' 0 = Verein, 1 = Länder
            Build_Grid(Bereich)
            dgvJoin.MultiSelect = False
        End If

        cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage(dicBereich(Bereich)))

        lblGruppierung.Enabled = cboAuswertung.Text.Equals("2. Wertung")
        cboGruppierung.Enabled = cboAuswertung.Text.Equals("2. Wertung")

        Select Case cboAuswertung.Text
            Case "Gesamt"
                'bsU.Sort = If(Wettkampf.Alterseinteilung.Equals("Jahrgang"), "Jahrgang DESC, ", "") & If(Wettkampf.Athletik, "Gesamt DESC", "Heben DESC")
                bsU.Filter = "Wettkampf = " & cboWettkampf.SelectedValue.ToString & " And Not Convert(IsNull(" & If(Wettkampf.Athletik, "Gesamt_Platz", "Heben_Platz") & ", ''), System.String) = ''"
                bsU.Sort = If(Wettkampf.Alterseinteilung.Equals("Jahrgang"), "Jahrgang DESC, ", "") & If(Wettkampf.Athletik, "Gesamt_Platz", "Heben_Platz")
            Case "Reißen"
                'bsU.Sort = "Reissen DESC"
                bsU.Filter = "Wettkampf = " & cboWettkampf.SelectedValue.ToString & " And Not Convert(IsNull(Reissen_Platz, ''), System.String) = ''"
                bsU.Sort = "Reissen_Platz"
            Case "Stoßen"
                'bsU.Sort = "Stossen DESC"
                bsU.Filter = "Wettkampf = " & cboWettkampf.SelectedValue.ToString & " And Not Convert(IsNull(Stossen_Platz, ''), System.String) = ''"
                bsU.Sort = "Stossen_Platz"
            Case "Punktbeste Heber"
                If cboSex.Items.Count > 0 Then cboSex.SelectedIndex = 0
                bsU.Sort = If(Wettkampf.Athletik, "Gesamt", "Heben") & " DESC"
            Case "2. Wertung"
                bsU.Filter = "Wettkampf = " & cboWettkampf.SelectedValue.ToString & " And Not Convert(IsNull(Wertung2_Platz, ''), System.String) = ''"
                bsU.Sort = "Wertung2_Platz"
                cboGruppierung.SelectedIndex = 0
            Case Else
                ' dgv Mannschaften
                dvM = New DataView(dtMeldung.DefaultView.ToTable(False, {"Nachname", "Vorname", "Geschlecht", "Jahrgang", "Verein", "Vereinsname", "Region", "region_3", "region_name", "Laenderwertung", "Vereinswertung", "Heben", "Gesamt", "Wertung2", "Wiegen", "R_Last", "S_Last"}))
                Dim ReturnTable = Glob.Get_DGJ_Mannschaftsliste(cboAuswertung.SelectedIndex - 4, dvM)
                If IsNothing(ReturnTable) Then Return

                dgvJoin.Columns.Insert(0, New DataGridViewButtonColumn With {.Name = "Expand", .HeaderText = "", .UseColumnTextForButtonValue = True, .Text = "+"})
                dvM = New DataView(ReturnTable(0).ToTable)
                bsM = New BindingSource With {.DataSource = dvM}
                bsM.Sort = "Platz"
                dgvJoin.DataSource = bsM
                Dim col = New DataGridViewTextBoxColumn With {.Name = "Team", .HeaderText = "Team", .DataPropertyName = "Team"}
                col.SortMode = DataGridViewColumnSortMode.NotSortable
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                dgvJoin.Columns.Insert(4, col)
                ' dgvHeber
                dvH = New DataView(ReturnTable(1).ToTable)
                bsH = New BindingSource With {.DataSource = dvH}
                bsH.Sort = "Punkte DESC"
                With dgvHeber
                    .DataSource = bsH
                    .Columns("Verein").Visible = False
                    .Columns("Region").Visible = False
                    .Columns("Vereinsname").Visible = dicBereich(Bereich) = 1
                    .Columns("Geschlecht").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns("Punkte").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .Columns("Punkte").DefaultCellStyle.Format = dgvJoin.Columns("Punkte").DefaultCellStyle.Format
                End With
        End Select
    End Sub

    Private Sub cboGruppierung_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGruppierung.SelectedIndexChanged
        If IsNothing(bsU) OrElse IsNothing(cboGruppierung.SelectedValue) Then Return
        Try
            'bsU.Filter = "Not Convert(IsNull(Wertung2_Platz, ''), System.String) = '' And Gruppierungen = " & DirectCast(cboGruppierung.SelectedValue, DataRowView)!Gruppierungen.ToString
            bsU.Filter = "Wettkampf = " & cboWettkampf.SelectedValue.ToString & " And Not Convert(IsNull(Wertung2_Platz, ''), System.String) = '' And Gruppierungen = " & cboGruppierung.SelectedValue.ToString
        Catch ex As Exception
        End Try
    End Sub
    Private Sub cboGruppe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGruppe.SelectedIndexChanged
        If IsNothing(bsU) Then Return
        Try
            If cboGruppe.SelectedIndex = -1 Then
                bsU.Filter = Nothing
            Else
                cboSex.SelectedIndex = -1
                cboAK.SelectedIndex = -1
                cboGK.SelectedIndex = -1
                bsU.Filter = "Wettkampf = " & cboWettkampf.SelectedValue.ToString & " And Gruppe = " & cboGruppe.SelectedValue.ToString
                chkSelectAll.Checked = False
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub cboAKcboGK_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAK.SelectedIndexChanged, cboGK.SelectedIndexChanged
        If Not IsNothing(bsU) Then bsU.Filter = Set_Filter()
    End Sub
    Private Sub cboSex_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSex.SelectedIndexChanged
        If cboSex.SelectedIndex = -1 Then
            cboGK.DataSource = Nothing
            cboAK.DataSource = Nothing
            If Not IsNothing(bsU) Then bsU.Filter = Nothing
        Else
            cboGruppe.SelectedIndex = -1
            Dim tmpAK = New DataView(dtMeldung.DefaultView.ToTable(True, {"Geschlecht", "idAK", "AK"}), "Geschlecht = '" & cboSex.Text & "'", "idAK", DataViewRowState.CurrentRows)
            With cboAK
                .DataSource = tmpAK
                .DisplayMember = "AK"
                .ValueMember = "idAK"
                .SelectedIndex = -1
            End With
            Dim tmpGK = New DataView(dtMeldung.DefaultView.ToTable(True, {"Geschlecht", "idGK", "GK"}), "Geschlecht = '" & cboSex.Text & "'", "idGK", DataViewRowState.CurrentRows)
            With cboGK
                .DataSource = tmpGK
                .DisplayMember = "GK"
                .ValueMember = "idGK"
                .SelectedIndex = -1
            End With
            'If Not IsNothing(bsU) Then bsU.Filter = Set_Filter()
        End If
    End Sub
    Private Sub cboVorlage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVorlage.SelectedIndexChanged
        If cboVorlage.Focused Then Vorlage(dicBereich(Bereich)) = cboVorlage.Text
    End Sub

    Private Function Set_Filter() As String
        If cboGruppe.SelectedIndex > -1 Then Return "Wettkampf = " & cboWettkampf.SelectedValue.ToString & " And Gruppe = " & cboGruppe.SelectedValue.ToString
        Dim lst As New List(Of String)
        lst.Add("Wettkampf = " & cboWettkampf.SelectedValue.ToString)
        If cboAK.SelectedIndex > -1 Then lst.Add("AK = '" & cboAK.Text & "'")
        If cboSex.SelectedIndex > -1 Then lst.Add("Geschlecht = '" & cboSex.Text & "'")
        If cboGK.SelectedIndex > -1 Then lst.Add("GK = '" & cboGK.Text & "'")
        Return Join(lst.ToArray, " AND ")
    End Function

    Private Sub chkSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkSelectAll.CheckedChanged
        If chkSelectAll.Checked Then cboGruppe.SelectedIndex = -1
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Close()
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        With OpenFileDialog1
            .Filter = "Druck-Vorlagen (*.frx)|*.frx|Alle Dateien (*.*)|*.*"
            .InitialDirectory = Path.Combine(Application.StartupPath, "Report")
            If .ShowDialog() = DialogResult.Cancel Then Exit Sub
            If Not .FileName.Contains(".frx") Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Die ausgewählte Datei ist keine Druckvorlage.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
            Else
                Try
                    Dim directory = .FileName.Replace(.SafeFileName, "")
                    directory = Strings.Left(directory, directory.Length - 1)
                    Vorlage(dicBereich(Bereich)) = Split(.SafeFileName, ".")(0)
                    dtVorlage.Rows.Add(Vorlage(dicBereich(Bereich)), directory)
                    cboVorlage.SelectedIndex = cboVorlage.FindStringExact(Vorlage(dicBereich(Bereich)))
                Catch ex As Exception
                End Try
            End If
        End With
        cboVorlage.Focus()
    End Sub
    Private Sub btnVorschau_Click(sender As Object, e As EventArgs) Handles btnVorschau.Click, btnPrint.Click, btnEdit.Click

        'If cboGruppe.SelectedIndex = -1 And Not chkSelectAll.Checked Then
        '    Using New Centered_MessageBox(Me)
        '        MessageBox.Show("Keine Gruppe ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    End Using
        '    Return
        'End If

        User.Check_MySQLDriver(Me)

        report = New FastReport.Report
        With report

#Region "Vorlage"
            If Vorlage(dicBereich(Bereich)) = String.Empty AndAlso cboVorlage.Text = String.Empty Then
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Keine Druckvorlage ausgewählt.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
                Cursor = Cursors.Default
                Return
            End If
            Try
                .Load(Path.Combine(cboVorlage.SelectedValue.ToString, If(Not String.IsNullOrEmpty(Vorlage(dicBereich(Bereich))), Vorlage(dicBereich(Bereich)), cboVorlage.Text) + ".frx"))
                .Dictionary.Connections(0).ConnectionString = User.FR_ConnString
                btnVorschau.Enabled = False
                Cursor = Cursors.WaitCursor
            Catch ex As Exception
                Using New Centered_MessageBox(Me)
                    MessageBox.Show("Vorlage nicht gefunden.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
                Return
            End Try
#End Region

            .RegisterData(Glob.dtWK, "wk")

            If dicBereich(Bereich) = 2 Then
                ' Einzelwertung
                'Dim field = cboAuswertung.Text
                'If field.Contains("Punktbest") Then field = "Gesamt"
                'If field.Equals("Gesamt") AndAlso Not Wettkampf.Athletik Then field = "Heben"
                Dim tmp = dtMeldung.Clone()
                For Each row As DataGridViewRow In dgvJoin.SelectedRows
                    tmp.ImportRow(dvU(row.Index).Row)
                Next
                'Dim tmp = New DataView(dvU.ToTable)
                '    'temp.RowFilter = "Not Convert(IsNull(" & field & "_Platz,''), System.String) = ''"
                '    Dim tmp = temp.ToTable
                '    If SelectedCount > -1 Then
                '        tmp.Rows.Clear()
                '        Dim i = 0
                '        Do While i < dvU.Count AndAlso Not IsDBNull(dvU(i)(field & "_Platz")) AndAlso CInt(dvU(i)(field & "_Platz")) <= SelectedCount
                '            tmp.ImportRow(dvU(i).Row)
                '            i += 1
                '        Loop
                '    ElseIf SelectedCount = -2 Then
                '        tmp.Rows.Clear()
                '        tmp.ImportRow(dvU(bsU.Position).Row)
                '    End If
                .RegisterData(dtGruppen, "group")
                .RegisterData(tmp, "urkunde")
            Else
                ' Mannschaftswertung
                Dim tmpM = dvM.ToTable
                'Dim tmpH = New DataView(dtMeldung)

                Dim lstValue As New List(Of String)
                'If SelectedCount > -1 Then
                '    tmpM.Rows.Clear()
                '    Dim i = 0
                '    Do While i < dvM.Count AndAlso CInt(dvM(i)!Platz) <= SelectedCount
                '        tmpM.ImportRow(dvM(i).Row)
                '        lstValue.Add(dvM(i)(Filter(dicBereich(Bereich))).ToString)
                '        i += 1
                '    Loop
                '    'bsH.Filter = Filter(dicBereich(Bereich)) & " = " & Join(lstValue.ToArray, " OR " & Filter(dicBereich(Bereich)) & " = ")

                '    'Dim _filter = If(Filter(dicBereich(Bereich)).Equals("Verein"), "Vereinswertung", "Laenderwertung")
                '    '_filter += " AND (" + Filter(dicBereich(Bereich)) & " = " & Join(lstValue.ToArray, " OR " & Filter(dicBereich(Bereich)) & " = ") + ")"

                '    'tmpH.RowFilter = _filter 'Filter(dicBereich(Bereich)) & " = " & Join(lstValue.ToArray, " OR " & Filter(dicBereich(Bereich)) & " = ")

                'End If
                .RegisterData(tmpM, "team")
                .RegisterData(dvH.ToTable, "heber")
            End If

            Try
                Dim s = New FastReport.EnvironmentSettings
                s.ReportSettings.ShowProgress = False

                If sender.Equals(btnEdit) Then
                    .Design()
                Else
                    .Prepare(True)
                    If sender.Equals(btnVorschau) Then
                        .ShowPrepared()
                    Else
                        .PrintPrepared()
                    End If
                End If
            Catch ex As Exception
                Stop
            End Try
        End With

        Cursor = Cursors.Default
        btnVorschau.Enabled = True
        dgvJoin.Focus()

    End Sub
    Private Sub btnVorschau_EnabledChanged(sender As Object, e As EventArgs) Handles btnVorschau.EnabledChanged
        btnPrint.Enabled = btnVorschau.Enabled
    End Sub

    Private Sub nudBis_GotFocus(sender As Object, e As EventArgs) Handles nudBis.GotFocus
        'nudBis.Select(0, 2)
        optBis.Checked = True
    End Sub
    Private Sub nudBis_ValueChanged(sender As Object, e As EventArgs) Handles nudBis.ValueChanged
        If nudBis.Focused Then
            dgvJoin.ClearSelection()
            For i = 0 To nudBis.Value - 1
                dgvJoin.Rows(CInt(i)).Selected = True
            Next
        End If
    End Sub

    Private Sub optErster_CheckedChanged(sender As Object, e As EventArgs) Handles optErster.CheckedChanged
        If optErster.Focused AndAlso optErster.Checked Then
            dgvJoin.ClearSelection()
            dgvJoin.Rows(0).Selected = True
        End If
    End Sub
    Private Sub optDrei_CheckedChanged(sender As Object, e As EventArgs) Handles optDrei.CheckedChanged
        If optDrei.Focused AndAlso optDrei.Checked Then
            dgvJoin.ClearSelection()
            For i = 0 To 2
                dgvJoin.Rows(i).Selected = True
            Next
        End If
    End Sub
    Private Sub optBis_CheckedChanged(sender As Object, e As EventArgs) Handles optBis.CheckedChanged
        If optBis.Focused AndAlso optBis.Checked Then
            dgvJoin.ClearSelection()
            For i = 0 To nudBis.Value - 1
                dgvJoin.Rows(CInt(i)).Selected = True
            Next
            nudBis.Focus()
        End If
    End Sub
    Private Sub optAlle_CheckedChanged(sender As Object, e As EventArgs) Handles optAlle.CheckedChanged
        If optAlle.Focused AndAlso optAlle.Checked Then
            For i = 0 To dgvJoin.Rows.Count - 1
                dgvJoin.Rows(i).Selected = True
            Next
        End If
    End Sub
    Private Sub optSel_CheckedChanged(sender As Object, e As EventArgs) Handles optSel.CheckedChanged
        If optSel.Checked Then
        End If
    End Sub
    Dim CurrentRow As Integer
    Private Sub dgvJoin_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvJoin.CellClick
        If IsNothing(bsH) Then Return
        If e.RowIndex > -1 AndAlso dgvJoin.Columns(e.ColumnIndex).Name.Equals("Expand") Then
            bsH.Filter = Filter(dicBereich(Bereich)) & " = " & dvM(bsM.Position)(Filter(dicBereich(Bereich))).ToString
            dgvJoin.Rows(CurrentRow).DividerHeight = 0
            With dgvHeber
                .Visible = RowExpanded <> e.RowIndex OrElse Not .Visible
                If Not .Visible Then Return
                .Height = .Rows.Count * 22 + 3
                Dim w = 0
                For i = 0 To .Columns.Count - 1
                    If .Columns(i).Visible Then w += .Columns(i).Width
                Next
                .Width = w + 1
                RowExpanded = e.RowIndex
                Dim rect = dgvJoin.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, True)
                Dim x = dgvJoin.Left + rect.X + rect.Width
                Dim y = dgvJoin.Top + rect.Y + rect.Height
                dgvJoin.Rows(e.RowIndex).DividerHeight = .Height
                CurrentRow = e.RowIndex
                .Location = New Point(x, y - 1)
                .ClearSelection()
                .BringToFront()
            End With
        End If
    End Sub
    Private Sub dgvJoin_ColumnHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvJoin.ColumnHeaderMouseClick
        'If dgvJoin.Columns(e.ColumnIndex).SortMode = DataGridViewColumnSortMode.Automatic Then
        '    Dim data = dgvJoin.Columns(e.ColumnIndex).DataPropertyName
        '    bsU.Filter = "Not Convert(IsNull(" & data & ", ''), System.String) = ''"
        'End If
    End Sub
    Private Sub dgvJoin_SelectionChanged(sender As Object, e As EventArgs) Handles dgvJoin.SelectionChanged
        'If Not dgvJoin.Focused Then Return
        If dgvJoin.SelectedRows.Count = 1 AndAlso dgvJoin.SelectedRows(0).Index = 0 Then
            optErster.Checked = True
        ElseIf dgvJoin.SelectedRows.Count = 3 AndAlso CheckSelection(0, 2) Then
            optDrei.Checked = True
        ElseIf dgvJoin.SelectedRows.Count = dgvJoin.Rows.Count Then
            optAlle.Checked = True
        ElseIf dgvJoin.SelectedRows.Count > 1 AndAlso CheckSelection(0, dgvJoin.SelectedRows.Count - 1) Then
            optBis.Checked = True
            nudBis.Value = dgvJoin.SelectedRows.Count
        Else
            optSel.Checked = True
        End If
    End Sub
    Private Sub dgvJoin_Scroll(sender As Object, e As ScrollEventArgs) Handles dgvJoin.Scroll
        With dgvHeber
            If .Visible OrElse dgvJoin.Rows(CurrentRow).DividerHeight > 0 Then
                Dim Scroll = e.OldValue - e.NewValue
                Dim y = .Top + dgvJoin.RowTemplate.Height * Scroll
                .Visible = CurrentRow >= dgvJoin.FirstDisplayedScrollingRowIndex
                .Location = New Point(.Left, y)
            End If
        End With
    End Sub
    Private Sub DataGridView1_SortCompare(sender As Object, e As DataGridViewSortCompareEventArgs) Handles dgvJoin.SortCompare
        'If e.CellValue1 Is Nothing Then e.SortResult += 1 : e.Handled = True
        'If e.CellValue2 Is Nothing Then e.SortResult -= 1 : e.Handled = True
        'If e.Column.DataGridView.SortOrder = SortOrder.Descending Then e.SortResult = -e.SortResult
    End Sub

    Private Sub dgvHeber_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvHeber.CellEnter
        dgvHeber.ClearSelection()
    End Sub
    Private Sub dgvHeber_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvHeber.KeyDown
        If e.KeyCode = Keys.Escape Then
            e.Handled = True
            dgvHeber.Visible = False
        End If
    End Sub

    Private Sub Control_GotFocus(sender As Object, e As EventArgs) Handles btnCancel.GotFocus,
                                                                           btnEdit.GotFocus,
                                                                           btnPrint.GotFocus,
                                                                           btnSearch.GotFocus,
                                                                           btnVorschau.GotFocus,
                                                                           nudBis.GotFocus,
                                                                           cboAuswertung.GotFocus,
                                                                           cboVorlage.GotFocus,
                                                                           optAlle.GotFocus,
                                                                           optBis.GotFocus,
                                                                           optDrei.GotFocus,
                                                                           optErster.GotFocus
        Try
            dgvHeber.Visible = False
            dgvJoin.Rows(CurrentRow).DividerHeight = 0
        Catch ex As Exception
        End Try
    End Sub

    Private Function CheckSelection(Start As Integer, Ende As Integer) As Boolean
        For i = Start To Ende
            If Not dgvJoin.Rows(i).Selected Then Return False
        Next
        Return True
    End Function

    Private Sub cboWettkampf_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboWettkampf.SelectedValueChanged
        If cboWettkampf.Focused Then
            'Get_Data(cboWettkampf.SelectedValue.ToString)
            bsU.Filter = "Wettkampf = " & cboWettkampf.SelectedValue.ToString
            cboSex.SelectedIndex = -1
            cboAK.SelectedIndex = -1
            cboGK.SelectedIndex = -1
            chkSelectAll.Checked = False
        End If
    End Sub

End Class