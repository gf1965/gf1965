﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Export_Ergebnisse
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnExport = New System.Windows.Forms.Button()
        Me.btnWK = New System.Windows.Forms.Button()
        Me.dgvErgebnisse = New System.Windows.Forms.DataGridView()
        Me.Nummer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gruppe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Heben = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Athletik = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkHeben = New System.Windows.Forms.CheckBox()
        Me.chkAthletik = New System.Windows.Forms.CheckBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        CType(Me.dgvErgebnisse, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.Location = New System.Drawing.Point(372, 24)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(112, 25)
        Me.btnExport.TabIndex = 2
        Me.btnExport.TabStop = False
        Me.btnExport.Text = "Exportieren"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnWK
        '
        Me.btnWK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnWK.Location = New System.Drawing.Point(372, 259)
        Me.btnWK.Name = "btnWK"
        Me.btnWK.Size = New System.Drawing.Size(112, 25)
        Me.btnWK.TabIndex = 4
        Me.btnWK.TabStop = False
        Me.btnWK.Text = "Wettkampf"
        Me.ToolTip1.SetToolTip(Me.btnWK, "anderen Wettkmapf auswählen")
        Me.btnWK.UseVisualStyleBackColor = True
        '
        'dgvErgebnisse
        '
        Me.dgvErgebnisse.AllowUserToAddRows = False
        Me.dgvErgebnisse.AllowUserToDeleteRows = False
        Me.dgvErgebnisse.AllowUserToOrderColumns = True
        Me.dgvErgebnisse.AllowUserToResizeColumns = False
        Me.dgvErgebnisse.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Beige
        Me.dgvErgebnisse.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvErgebnisse.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvErgebnisse.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvErgebnisse.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvErgebnisse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvErgebnisse.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Nummer, Me.Gruppe, Me.Heben, Me.Athletik})
        Me.dgvErgebnisse.Location = New System.Drawing.Point(22, 24)
        Me.dgvErgebnisse.Name = "dgvErgebnisse"
        Me.dgvErgebnisse.RowHeadersVisible = False
        Me.dgvErgebnisse.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.dgvErgebnisse.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvErgebnisse.Size = New System.Drawing.Size(326, 223)
        Me.dgvErgebnisse.TabIndex = 1
        '
        'Nummer
        '
        Me.Nummer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Nummer.DefaultCellStyle = DataGridViewCellStyle3
        Me.Nummer.HeaderText = "Nr"
        Me.Nummer.Name = "Nummer"
        Me.Nummer.ReadOnly = True
        Me.Nummer.Width = 35
        '
        'Gruppe
        '
        Me.Gruppe.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Gruppe.HeaderText = "Gruppe"
        Me.Gruppe.Name = "Gruppe"
        Me.Gruppe.ReadOnly = True
        '
        'Heben
        '
        Me.Heben.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.Heben.HeaderText = "Heben"
        Me.Heben.Name = "Heben"
        Me.Heben.Width = 45
        '
        'Athletik
        '
        Me.Athletik.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.Athletik.HeaderText = "Athletik"
        Me.Athletik.Name = "Athletik"
        Me.Athletik.Width = 48
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(372, 55)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(112, 25)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.TabStop = False
        Me.btnCancel.Text = "Schließen"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'chkHeben
        '
        Me.chkHeben.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkHeben.AutoSize = True
        Me.chkHeben.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.chkHeben.Location = New System.Drawing.Point(255, 253)
        Me.chkHeben.Name = "chkHeben"
        Me.chkHeben.Size = New System.Drawing.Size(43, 31)
        Me.chkHeben.TabIndex = 5
        Me.chkHeben.TabStop = False
        Me.chkHeben.Text = "Heben"
        Me.ToolTip1.SetToolTip(Me.chkHeben, "Auswahl für alle Gruppen setzen")
        Me.chkHeben.UseVisualStyleBackColor = True
        '
        'chkAthletik
        '
        Me.chkAthletik.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkAthletik.AutoSize = True
        Me.chkAthletik.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.chkAthletik.Location = New System.Drawing.Point(300, 253)
        Me.chkAthletik.Name = "chkAthletik"
        Me.chkAthletik.Size = New System.Drawing.Size(46, 31)
        Me.chkAthletik.TabIndex = 6
        Me.chkAthletik.TabStop = False
        Me.chkAthletik.Text = "Athletik"
        Me.ToolTip1.SetToolTip(Me.chkAthletik, "Auswahl für alle Gruppen setzen")
        Me.chkAthletik.UseVisualStyleBackColor = True
        '
        'Export_Ergebnisse
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(503, 303)
        Me.Controls.Add(Me.chkAthletik)
        Me.Controls.Add(Me.chkHeben)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.dgvErgebnisse)
        Me.Controls.Add(Me.btnWK)
        Me.Controls.Add(Me.btnExport)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Export_Ergebnisse"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Ergebnisse exportieren - "
        CType(Me.dgvErgebnisse, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnExport As Button
    Friend WithEvents btnWK As Button
    Friend WithEvents dgvErgebnisse As DataGridView
    Friend WithEvents btnCancel As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Nummer As DataGridViewTextBoxColumn
    Friend WithEvents Gruppe As DataGridViewTextBoxColumn
    Friend WithEvents Heben As DataGridViewCheckBoxColumn
    Friend WithEvents Athletik As DataGridViewCheckBoxColumn
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents chkHeben As CheckBox
    Friend WithEvents chkAthletik As CheckBox
End Class
