﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmWiegen
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label12 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWiegen))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.dgvJoin = New System.Windows.Forms.DataGridView()
        Me.WK = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Sortierung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Losnummer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Startnummer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Teilnehmer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nachname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vorname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sex = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jahrgang = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Geburtstag = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nation = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Verein = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Land = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gewicht = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Wiegen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gruppe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Zweikampf = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Reißen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Stoßen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Länderwertung = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Vereinswertung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RelativW = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmnuNotAppeared = New System.Windows.Forms.ToolStripMenuItem()
        Me.ctsm1 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmnuExclude = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmnuInclude = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmnuHeber = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmnuVerein = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmnuÜbernahme = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSperren = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBearbeiten = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuNotAppeared = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsm1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuExclude = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuInclude = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuHeber = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuVerein = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuListen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStartliste_Druck = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuGruppenliste_Druck = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWiegeliste_Druck = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSkipRules = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWiegeliste_Export = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWiegeliste_Import = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuMauszeiger = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStartnummern = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataGridViewImageColumn1 = New System.Windows.Forms.DataGridViewImageColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboVereinswertung = New System.Windows.Forms.ComboBox()
        Me.lblVereinswertung = New System.Windows.Forms.Label()
        Me.chkRelativ = New System.Windows.Forms.CheckBox()
        Me.btnWeiter = New System.Windows.Forms.Button()
        Me.chkLänderwertung = New System.Windows.Forms.CheckBox()
        Me.txtReissen = New System.Windows.Forms.TextBox()
        Me.txtStossen = New System.Windows.Forms.TextBox()
        Me.txtWiegen = New System.Windows.Forms.TextBox()
        Me.txtStartnummer = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblGruppen = New System.Windows.Forms.Label()
        Me.cboGruppenZuweisung = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboJG = New System.Windows.Forms.ComboBox()
        Me.lblVerein = New System.Windows.Forms.Label()
        Me.cboVerein = New System.Windows.Forms.ComboBox()
        Me.cboSex = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboGruppe = New System.Windows.Forms.ComboBox()
        Me.btnName = New System.Windows.Forms.Button()
        Me.bs = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnJG = New System.Windows.Forms.Button()
        Me.btnVerein = New System.Windows.Forms.Button()
        Me.btnSex = New System.Windows.Forms.Button()
        Me.btnGruppe = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Label12 = New System.Windows.Forms.Label()
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label12
        '
        resources.ApplyResources(Label12, "Label12")
        Label12.Name = "Label12"
        Label12.UseCompatibleTextRendering = True
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Silver
        resources.ApplyResources(Me.txtName, "txtName")
        Me.txtName.Name = "txtName"
        '
        'dgvJoin
        '
        Me.dgvJoin.AllowUserToAddRows = False
        Me.dgvJoin.AllowUserToDeleteRows = False
        Me.dgvJoin.AllowUserToResizeRows = False
        resources.ApplyResources(Me.dgvJoin, "dgvJoin")
        Me.dgvJoin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvJoin.BackgroundColor = System.Drawing.SystemColors.ControlLight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvJoin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvJoin.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WK, Me.Sortierung, Me.Losnummer, Me.Startnummer, Me.Teilnehmer, Me.Nachname, Me.Vorname, Me.Sex, Me.Jahrgang, Me.Geburtstag, Me.Nation, Me.Verein, Me.Land, Me.AK, Me.Gewicht, Me.Wiegen, Me.GK, Me.Gruppe, Me.Zweikampf, Me.Reißen, Me.Stoßen, Me.Länderwertung, Me.Vereinswertung, Me.RelativW})
        Me.dgvJoin.ContextMenuStrip = Me.ContextMenuStrip1
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvJoin.DefaultCellStyle = DataGridViewCellStyle20
        Me.dgvJoin.Name = "dgvJoin"
        Me.dgvJoin.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvJoin.RowHeadersVisible = False
        Me.dgvJoin.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvJoin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvJoin.StandardTab = True
        '
        'WK
        '
        Me.WK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.WK.DataPropertyName = "attend"
        Me.WK.FillWeight = 257.4005!
        resources.ApplyResources(Me.WK, "WK")
        Me.WK.Name = "WK"
        Me.WK.ReadOnly = True
        Me.WK.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.WK.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.WK.ThreeState = True
        '
        'Sortierung
        '
        Me.Sortierung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Sortierung.DataPropertyName = "Sortierung"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.Sortierung.DefaultCellStyle = DataGridViewCellStyle2
        resources.ApplyResources(Me.Sortierung, "Sortierung")
        Me.Sortierung.Name = "Sortierung"
        '
        'Losnummer
        '
        Me.Losnummer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Losnummer.DataPropertyName = "Losnummer"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.Losnummer.DefaultCellStyle = DataGridViewCellStyle3
        Me.Losnummer.FillWeight = 289.34!
        resources.ApplyResources(Me.Losnummer, "Losnummer")
        Me.Losnummer.Name = "Losnummer"
        Me.Losnummer.ReadOnly = True
        Me.Losnummer.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Startnummer
        '
        Me.Startnummer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Startnummer.DataPropertyName = "Startnummer"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.Startnummer.DefaultCellStyle = DataGridViewCellStyle4
        resources.ApplyResources(Me.Startnummer, "Startnummer")
        Me.Startnummer.Name = "Startnummer"
        '
        'Teilnehmer
        '
        Me.Teilnehmer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Teilnehmer.DataPropertyName = "Teilnehmer"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Teilnehmer.DefaultCellStyle = DataGridViewCellStyle5
        Me.Teilnehmer.FillWeight = 79.6035!
        resources.ApplyResources(Me.Teilnehmer, "Teilnehmer")
        Me.Teilnehmer.Name = "Teilnehmer"
        Me.Teilnehmer.ReadOnly = True
        Me.Teilnehmer.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Nachname
        '
        Me.Nachname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Nachname.DataPropertyName = "Nachname"
        Me.Nachname.FillWeight = 80.0!
        resources.ApplyResources(Me.Nachname, "Nachname")
        Me.Nachname.Name = "Nachname"
        Me.Nachname.ReadOnly = True
        Me.Nachname.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Vorname
        '
        Me.Vorname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Vorname.DataPropertyName = "Vorname"
        Me.Vorname.FillWeight = 80.0!
        resources.ApplyResources(Me.Vorname, "Vorname")
        Me.Vorname.Name = "Vorname"
        Me.Vorname.ReadOnly = True
        Me.Vorname.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Sex
        '
        Me.Sex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Sex.DataPropertyName = "Sex"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Sex.DefaultCellStyle = DataGridViewCellStyle6
        Me.Sex.FillWeight = 43.76223!
        resources.ApplyResources(Me.Sex, "Sex")
        Me.Sex.Name = "Sex"
        Me.Sex.ReadOnly = True
        Me.Sex.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Jahrgang
        '
        Me.Jahrgang.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Jahrgang.DataPropertyName = "Jahrgang"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Jahrgang.DefaultCellStyle = DataGridViewCellStyle7
        Me.Jahrgang.FillWeight = 41.25167!
        resources.ApplyResources(Me.Jahrgang, "Jahrgang")
        Me.Jahrgang.Name = "Jahrgang"
        Me.Jahrgang.ReadOnly = True
        Me.Jahrgang.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Geburtstag
        '
        Me.Geburtstag.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Geburtstag.DataPropertyName = "Geburtstag"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.Format = "d"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.Geburtstag.DefaultCellStyle = DataGridViewCellStyle8
        resources.ApplyResources(Me.Geburtstag, "Geburtstag")
        Me.Geburtstag.Name = "Geburtstag"
        Me.Geburtstag.ReadOnly = True
        '
        'Nation
        '
        Me.Nation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Nation.DataPropertyName = "state_name"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.Nation.DefaultCellStyle = DataGridViewCellStyle9
        Me.Nation.FillWeight = 41.25167!
        resources.ApplyResources(Me.Nation, "Nation")
        Me.Nation.Name = "Nation"
        Me.Nation.ReadOnly = True
        '
        'Verein
        '
        Me.Verein.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Verein.DataPropertyName = "Vereinsname"
        resources.ApplyResources(Me.Verein, "Verein")
        Me.Verein.Name = "Verein"
        Me.Verein.ReadOnly = True
        Me.Verein.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Land
        '
        Me.Land.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Land.DataPropertyName = "region_3"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Land.DefaultCellStyle = DataGridViewCellStyle10
        Me.Land.FillWeight = 41.25167!
        resources.ApplyResources(Me.Land, "Land")
        Me.Land.Name = "Land"
        Me.Land.ReadOnly = True
        Me.Land.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'AK
        '
        Me.AK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.AK.DataPropertyName = "AK"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.AK.DefaultCellStyle = DataGridViewCellStyle11
        Me.AK.FillWeight = 41.25167!
        resources.ApplyResources(Me.AK, "AK")
        Me.AK.Name = "AK"
        Me.AK.ReadOnly = True
        Me.AK.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Gewicht
        '
        Me.Gewicht.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gewicht.DataPropertyName = "Gewicht"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "N2"
        DataGridViewCellStyle12.NullValue = Nothing
        DataGridViewCellStyle12.Padding = New System.Windows.Forms.Padding(0, 0, 4, 0)
        Me.Gewicht.DefaultCellStyle = DataGridViewCellStyle12
        Me.Gewicht.FillWeight = 41.25167!
        resources.ApplyResources(Me.Gewicht, "Gewicht")
        Me.Gewicht.Name = "Gewicht"
        Me.Gewicht.ReadOnly = True
        Me.Gewicht.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Wiegen
        '
        Me.Wiegen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Wiegen.DataPropertyName = "Wiegen"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N2"
        DataGridViewCellStyle13.NullValue = Nothing
        DataGridViewCellStyle13.Padding = New System.Windows.Forms.Padding(0, 0, 4, 0)
        Me.Wiegen.DefaultCellStyle = DataGridViewCellStyle13
        Me.Wiegen.FillWeight = 41.25167!
        resources.ApplyResources(Me.Wiegen, "Wiegen")
        Me.Wiegen.Name = "Wiegen"
        Me.Wiegen.ReadOnly = True
        Me.Wiegen.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'GK
        '
        Me.GK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.GK.DataPropertyName = "GK"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Padding = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.GK.DefaultCellStyle = DataGridViewCellStyle14
        Me.GK.FillWeight = 41.25167!
        resources.ApplyResources(Me.GK, "GK")
        Me.GK.Name = "GK"
        Me.GK.ReadOnly = True
        Me.GK.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Gruppe
        '
        Me.Gruppe.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Gruppe.DataPropertyName = "Gruppe"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Gruppe.DefaultCellStyle = DataGridViewCellStyle15
        Me.Gruppe.FillWeight = 41.25167!
        resources.ApplyResources(Me.Gruppe, "Gruppe")
        Me.Gruppe.Name = "Gruppe"
        Me.Gruppe.ReadOnly = True
        Me.Gruppe.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Zweikampf
        '
        Me.Zweikampf.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Zweikampf.DataPropertyName = "Zweikampf"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle16.Padding = New System.Windows.Forms.Padding(0, 0, 4, 0)
        Me.Zweikampf.DefaultCellStyle = DataGridViewCellStyle16
        resources.ApplyResources(Me.Zweikampf, "Zweikampf")
        Me.Zweikampf.Name = "Zweikampf"
        Me.Zweikampf.ReadOnly = True
        '
        'Reißen
        '
        Me.Reißen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Reißen.DataPropertyName = "Reissen"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle17.Padding = New System.Windows.Forms.Padding(0, 0, 4, 0)
        Me.Reißen.DefaultCellStyle = DataGridViewCellStyle17
        Me.Reißen.FillWeight = 41.25167!
        resources.ApplyResources(Me.Reißen, "Reißen")
        Me.Reißen.Name = "Reißen"
        Me.Reißen.ReadOnly = True
        Me.Reißen.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Stoßen
        '
        Me.Stoßen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Stoßen.DataPropertyName = "Stossen"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle18.Padding = New System.Windows.Forms.Padding(0, 0, 4, 0)
        Me.Stoßen.DefaultCellStyle = DataGridViewCellStyle18
        Me.Stoßen.FillWeight = 41.25167!
        resources.ApplyResources(Me.Stoßen, "Stoßen")
        Me.Stoßen.Name = "Stoßen"
        Me.Stoßen.ReadOnly = True
        Me.Stoßen.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Länderwertung
        '
        Me.Länderwertung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Länderwertung.DataPropertyName = "Laenderwertung"
        Me.Länderwertung.FillWeight = 41.25167!
        resources.ApplyResources(Me.Länderwertung, "Länderwertung")
        Me.Länderwertung.Name = "Länderwertung"
        Me.Länderwertung.ReadOnly = True
        Me.Länderwertung.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Länderwertung.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'Vereinswertung
        '
        Me.Vereinswertung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Vereinswertung.DataPropertyName = "Vereinswertung"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Vereinswertung.DefaultCellStyle = DataGridViewCellStyle19
        Me.Vereinswertung.FillWeight = 41.25167!
        resources.ApplyResources(Me.Vereinswertung, "Vereinswertung")
        Me.Vereinswertung.Name = "Vereinswertung"
        Me.Vereinswertung.ReadOnly = True
        Me.Vereinswertung.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'RelativW
        '
        Me.RelativW.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.RelativW.DataPropertyName = "Relativ_W"
        Me.RelativW.FalseValue = ""
        Me.RelativW.FillWeight = 41.25167!
        resources.ApplyResources(Me.RelativW, "RelativW")
        Me.RelativW.IndeterminateValue = ""
        Me.RelativW.Name = "RelativW"
        Me.RelativW.ReadOnly = True
        Me.RelativW.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.RelativW.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.RelativW.ThreeState = True
        Me.RelativW.TrueValue = ""
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmnuNotAppeared, Me.ctsm1, Me.cmnuExclude, Me.cmnuInclude, Me.ToolStripMenuItem7, Me.cmnuHeber, Me.cmnuVerein, Me.ToolStripMenuItem3, Me.cmnuÜbernahme})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        resources.ApplyResources(Me.ContextMenuStrip1, "ContextMenuStrip1")
        '
        'cmnuNotAppeared
        '
        Me.cmnuNotAppeared.Name = "cmnuNotAppeared"
        resources.ApplyResources(Me.cmnuNotAppeared, "cmnuNotAppeared")
        '
        'ctsm1
        '
        Me.ctsm1.Name = "ctsm1"
        resources.ApplyResources(Me.ctsm1, "ctsm1")
        '
        'cmnuExclude
        '
        Me.cmnuExclude.Name = "cmnuExclude"
        resources.ApplyResources(Me.cmnuExclude, "cmnuExclude")
        '
        'cmnuInclude
        '
        Me.cmnuInclude.Name = "cmnuInclude"
        resources.ApplyResources(Me.cmnuInclude, "cmnuInclude")
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        resources.ApplyResources(Me.ToolStripMenuItem7, "ToolStripMenuItem7")
        '
        'cmnuHeber
        '
        Me.cmnuHeber.Name = "cmnuHeber"
        resources.ApplyResources(Me.cmnuHeber, "cmnuHeber")
        '
        'cmnuVerein
        '
        Me.cmnuVerein.Name = "cmnuVerein"
        resources.ApplyResources(Me.cmnuVerein, "cmnuVerein")
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        resources.ApplyResources(Me.ToolStripMenuItem3, "ToolStripMenuItem3")
        '
        'cmnuÜbernahme
        '
        Me.cmnuÜbernahme.ForeColor = System.Drawing.Color.Firebrick
        Me.cmnuÜbernahme.Name = "cmnuÜbernahme"
        resources.ApplyResources(Me.cmnuÜbernahme, "cmnuÜbernahme")
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuBearbeiten, Me.mnuListen, Me.mnuOptionen, Me.mnuExtras, Me.mnuStartnummern})
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.Name = "MenuStrip1"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSpeichern, Me.ToolStripMenuItem4, Me.mnuSperren, Me.mnuBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        resources.ApplyResources(Me.mnuDatei, "mnuDatei")
        '
        'mnuSpeichern
        '
        Me.mnuSpeichern.Name = "mnuSpeichern"
        resources.ApplyResources(Me.mnuSpeichern, "mnuSpeichern")
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        resources.ApplyResources(Me.ToolStripMenuItem4, "ToolStripMenuItem4")
        '
        'mnuSperren
        '
        Me.mnuSperren.Name = "mnuSperren"
        resources.ApplyResources(Me.mnuSperren, "mnuSperren")
        '
        'mnuBeenden
        '
        Me.mnuBeenden.Name = "mnuBeenden"
        resources.ApplyResources(Me.mnuBeenden, "mnuBeenden")
        '
        'mnuBearbeiten
        '
        Me.mnuBearbeiten.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNotAppeared, Me.tsm1, Me.mnuExclude, Me.mnuInclude, Me.ToolStripMenuItem1, Me.mnuHeber, Me.mnuVerein})
        Me.mnuBearbeiten.Name = "mnuBearbeiten"
        resources.ApplyResources(Me.mnuBearbeiten, "mnuBearbeiten")
        '
        'mnuNotAppeared
        '
        Me.mnuNotAppeared.Name = "mnuNotAppeared"
        resources.ApplyResources(Me.mnuNotAppeared, "mnuNotAppeared")
        '
        'tsm1
        '
        Me.tsm1.Name = "tsm1"
        resources.ApplyResources(Me.tsm1, "tsm1")
        '
        'mnuExclude
        '
        Me.mnuExclude.Name = "mnuExclude"
        resources.ApplyResources(Me.mnuExclude, "mnuExclude")
        '
        'mnuInclude
        '
        Me.mnuInclude.Name = "mnuInclude"
        resources.ApplyResources(Me.mnuInclude, "mnuInclude")
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        resources.ApplyResources(Me.ToolStripMenuItem1, "ToolStripMenuItem1")
        '
        'mnuHeber
        '
        Me.mnuHeber.Name = "mnuHeber"
        resources.ApplyResources(Me.mnuHeber, "mnuHeber")
        '
        'mnuVerein
        '
        Me.mnuVerein.Name = "mnuVerein"
        resources.ApplyResources(Me.mnuVerein, "mnuVerein")
        '
        'mnuListen
        '
        Me.mnuListen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuStartliste_Druck, Me.mnuGruppenliste_Druck, Me.mnuWiegeliste_Druck})
        Me.mnuListen.Name = "mnuListen"
        resources.ApplyResources(Me.mnuListen, "mnuListen")
        '
        'mnuStartliste_Druck
        '
        Me.mnuStartliste_Druck.Name = "mnuStartliste_Druck"
        resources.ApplyResources(Me.mnuStartliste_Druck, "mnuStartliste_Druck")
        Me.mnuStartliste_Druck.Tag = "Starterlisten"
        '
        'mnuGruppenliste_Druck
        '
        Me.mnuGruppenliste_Druck.Name = "mnuGruppenliste_Druck"
        resources.ApplyResources(Me.mnuGruppenliste_Druck, "mnuGruppenliste_Druck")
        Me.mnuGruppenliste_Druck.Tag = "Gruppenlisten"
        '
        'mnuWiegeliste_Druck
        '
        Me.mnuWiegeliste_Druck.Name = "mnuWiegeliste_Druck"
        resources.ApplyResources(Me.mnuWiegeliste_Druck, "mnuWiegeliste_Druck")
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSkipRules})
        Me.mnuOptionen.Name = "mnuOptionen"
        resources.ApplyResources(Me.mnuOptionen, "mnuOptionen")
        '
        'mnuSkipRules
        '
        Me.mnuSkipRules.CheckOnClick = True
        Me.mnuSkipRules.Name = "mnuSkipRules"
        resources.ApplyResources(Me.mnuSkipRules, "mnuSkipRules")
        '
        'mnuExtras
        '
        Me.mnuExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuWiegeliste_Export, Me.mnuWiegeliste_Import, Me.ToolStripMenuItem5, Me.mnuMauszeiger})
        Me.mnuExtras.Name = "mnuExtras"
        resources.ApplyResources(Me.mnuExtras, "mnuExtras")
        '
        'mnuWiegeliste_Export
        '
        Me.mnuWiegeliste_Export.Name = "mnuWiegeliste_Export"
        resources.ApplyResources(Me.mnuWiegeliste_Export, "mnuWiegeliste_Export")
        '
        'mnuWiegeliste_Import
        '
        Me.mnuWiegeliste_Import.Name = "mnuWiegeliste_Import"
        resources.ApplyResources(Me.mnuWiegeliste_Import, "mnuWiegeliste_Import")
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        resources.ApplyResources(Me.ToolStripMenuItem5, "ToolStripMenuItem5")
        '
        'mnuMauszeiger
        '
        Me.mnuMauszeiger.Name = "mnuMauszeiger"
        resources.ApplyResources(Me.mnuMauszeiger, "mnuMauszeiger")
        '
        'mnuStartnummern
        '
        resources.ApplyResources(Me.mnuStartnummern, "mnuStartnummern")
        Me.mnuStartnummern.ForeColor = System.Drawing.Color.Firebrick
        Me.mnuStartnummern.Name = "mnuStartnummern"
        '
        'DataGridViewImageColumn1
        '
        resources.ApplyResources(Me.DataGridViewImageColumn1, "DataGridViewImageColumn1")
        Me.DataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.DataGridViewImageColumn1.Name = "DataGridViewImageColumn1"
        '
        'GroupBox1
        '
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Controls.Add(Me.cboVereinswertung)
        Me.GroupBox1.Controls.Add(Me.lblVereinswertung)
        Me.GroupBox1.Controls.Add(Me.chkRelativ)
        Me.GroupBox1.Controls.Add(Me.btnWeiter)
        Me.GroupBox1.Controls.Add(Me.chkLänderwertung)
        Me.GroupBox1.Controls.Add(Me.txtReissen)
        Me.GroupBox1.Controls.Add(Me.txtStossen)
        Me.GroupBox1.Controls.Add(Me.txtWiegen)
        Me.GroupBox1.Controls.Add(Me.txtStartnummer)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.lblGruppen)
        Me.GroupBox1.Controls.Add(Me.cboGruppenZuweisung)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'cboVereinswertung
        '
        Me.cboVereinswertung.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboVereinswertung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.cboVereinswertung, "cboVereinswertung")
        Me.cboVereinswertung.FormattingEnabled = True
        Me.cboVereinswertung.Name = "cboVereinswertung"
        '
        'lblVereinswertung
        '
        resources.ApplyResources(Me.lblVereinswertung, "lblVereinswertung")
        Me.lblVereinswertung.Name = "lblVereinswertung"
        Me.lblVereinswertung.UseCompatibleTextRendering = True
        '
        'chkRelativ
        '
        resources.ApplyResources(Me.chkRelativ, "chkRelativ")
        Me.chkRelativ.Name = "chkRelativ"
        Me.chkRelativ.ThreeState = True
        Me.chkRelativ.UseVisualStyleBackColor = True
        '
        'btnWeiter
        '
        resources.ApplyResources(Me.btnWeiter, "btnWeiter")
        Me.btnWeiter.Name = "btnWeiter"
        Me.btnWeiter.UseVisualStyleBackColor = True
        '
        'chkLänderwertung
        '
        Me.chkLänderwertung.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        resources.ApplyResources(Me.chkLänderwertung, "chkLänderwertung")
        Me.chkLänderwertung.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLänderwertung.Name = "chkLänderwertung"
        Me.chkLänderwertung.UseVisualStyleBackColor = False
        '
        'txtReissen
        '
        resources.ApplyResources(Me.txtReissen, "txtReissen")
        Me.txtReissen.Name = "txtReissen"
        '
        'txtStossen
        '
        resources.ApplyResources(Me.txtStossen, "txtStossen")
        Me.txtStossen.Name = "txtStossen"
        '
        'txtWiegen
        '
        resources.ApplyResources(Me.txtWiegen, "txtWiegen")
        Me.txtWiegen.Name = "txtWiegen"
        '
        'txtStartnummer
        '
        resources.ApplyResources(Me.txtStartnummer, "txtStartnummer")
        Me.txtStartnummer.Name = "txtStartnummer"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'lblGruppen
        '
        resources.ApplyResources(Me.lblGruppen, "lblGruppen")
        Me.lblGruppen.Name = "lblGruppen"
        '
        'cboGruppenZuweisung
        '
        resources.ApplyResources(Me.cboGruppenZuweisung, "cboGruppenZuweisung")
        Me.cboGruppenZuweisung.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboGruppenZuweisung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGruppenZuweisung.FormattingEnabled = True
        Me.cboGruppenZuweisung.Name = "cboGruppenZuweisung"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        Me.Label6.UseCompatibleTextRendering = True
        '
        'cboJG
        '
        Me.cboJG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJG.FormattingEnabled = True
        resources.ApplyResources(Me.cboJG, "cboJG")
        Me.cboJG.Name = "cboJG"
        '
        'lblVerein
        '
        resources.ApplyResources(Me.lblVerein, "lblVerein")
        Me.lblVerein.Name = "lblVerein"
        Me.lblVerein.UseCompatibleTextRendering = True
        '
        'cboVerein
        '
        Me.cboVerein.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVerein.FormattingEnabled = True
        resources.ApplyResources(Me.cboVerein, "cboVerein")
        Me.cboVerein.Name = "cboVerein"
        Me.cboVerein.Tag = "idVerein"
        '
        'cboSex
        '
        Me.cboSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSex.FormattingEnabled = True
        resources.ApplyResources(Me.cboSex, "cboSex")
        Me.cboSex.Name = "cboSex"
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.Name = "Label13"
        Me.Label13.UseCompatibleTextRendering = True
        '
        'cboGruppe
        '
        Me.cboGruppe.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cboGruppe.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboGruppe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.cboGruppe, "cboGruppe")
        Me.cboGruppe.FormattingEnabled = True
        Me.cboGruppe.Name = "cboGruppe"
        '
        'btnName
        '
        resources.ApplyResources(Me.btnName, "btnName")
        Me.btnName.Name = "btnName"
        Me.btnName.TabStop = False
        Me.btnName.UseVisualStyleBackColor = True
        '
        'bs
        '
        Me.bs.AllowNew = False
        '
        'btnJG
        '
        resources.ApplyResources(Me.btnJG, "btnJG")
        Me.btnJG.Name = "btnJG"
        Me.btnJG.TabStop = False
        Me.btnJG.UseVisualStyleBackColor = True
        '
        'btnVerein
        '
        resources.ApplyResources(Me.btnVerein, "btnVerein")
        Me.btnVerein.Name = "btnVerein"
        Me.btnVerein.TabStop = False
        Me.btnVerein.UseVisualStyleBackColor = True
        '
        'btnSex
        '
        resources.ApplyResources(Me.btnSex, "btnSex")
        Me.btnSex.Name = "btnSex"
        Me.btnSex.TabStop = False
        Me.btnSex.UseVisualStyleBackColor = True
        '
        'btnGruppe
        '
        resources.ApplyResources(Me.btnGruppe, "btnGruppe")
        Me.btnGruppe.Name = "btnGruppe"
        Me.btnGruppe.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnGruppe, resources.GetString("btnGruppe.ToolTip"))
        Me.btnGruppe.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        resources.ApplyResources(Me.btnSave, "btnSave")
        Me.btnSave.Name = "btnSave"
        Me.btnSave.TabStop = False
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        resources.ApplyResources(Me.btnExit, "btnExit")
        Me.btnExit.Name = "btnExit"
        Me.btnExit.TabStop = False
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnGruppe)
        Me.Panel1.Controls.Add(Me.btnSex)
        Me.Panel1.Controls.Add(Me.btnVerein)
        Me.Panel1.Controls.Add(Me.btnJG)
        Me.Panel1.Controls.Add(Me.cboSex)
        Me.Panel1.Controls.Add(Label12)
        Me.Panel1.Controls.Add(Me.cboGruppe)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.cboVerein)
        Me.Panel1.Controls.Add(Me.lblVerein)
        Me.Panel1.Controls.Add(Me.cboJG)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.btnName)
        Me.Panel1.Controls.Add(Me.txtName)
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Name = "Panel1"
        '
        'Panel2
        '
        resources.ApplyResources(Me.Panel2, "Panel2")
        Me.Panel2.Controls.Add(Me.btnExit)
        Me.Panel2.Controls.Add(Me.btnSave)
        Me.Panel2.Name = "Panel2"
        '
        'frmWiegen
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.dgvJoin)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimizeBox = False
        Me.Name = "frmWiegen"
        Me.ShowInTaskbar = False
        CType(Me.dgvJoin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvJoin As System.Windows.Forms.DataGridView
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDatei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBeenden As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOptionen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bs As System.Windows.Forms.BindingSource
    Friend WithEvents DataGridViewImageColumn1 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents mnuSpeichern As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnName As System.Windows.Forms.Button
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtWiegen As System.Windows.Forms.TextBox
    Friend WithEvents txtStartnummer As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cboJG As System.Windows.Forms.ComboBox
    Friend WithEvents lblVerein As System.Windows.Forms.Label
    Friend WithEvents cboVerein As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtReissen As System.Windows.Forms.TextBox
    Friend WithEvents txtStossen As System.Windows.Forms.TextBox
    Friend WithEvents cboSex As System.Windows.Forms.ComboBox
    Friend WithEvents btnWeiter As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents cboGruppe As ComboBox
    Friend WithEvents chkLänderwertung As CheckBox
    Friend WithEvents btnJG As Button
    Friend WithEvents btnVerein As Button
    Friend WithEvents cboGruppenZuweisung As ComboBox
    Friend WithEvents lblGruppen As Label
    Friend WithEvents btnSex As Button
    Friend WithEvents btnGruppe As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents mnuSperren As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As ToolStripSeparator
    Friend WithEvents mnuExtras As ToolStripMenuItem
    Friend WithEvents mnuWiegeliste_Export As ToolStripMenuItem
    Friend WithEvents mnuWiegeliste_Import As ToolStripMenuItem
    Friend WithEvents mnuListen As ToolStripMenuItem
    Friend WithEvents mnuStartliste_Druck As ToolStripMenuItem
    Friend WithEvents mnuGruppenliste_Druck As ToolStripMenuItem
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents cmnuNotAppeared As ToolStripMenuItem
    Friend WithEvents ctsm1 As ToolStripSeparator
    Friend WithEvents cmnuExclude As ToolStripMenuItem
    Friend WithEvents mnuBearbeiten As ToolStripMenuItem
    Friend WithEvents mnuNotAppeared As ToolStripMenuItem
    Friend WithEvents tsm1 As ToolStripSeparator
    Friend WithEvents mnuExclude As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem7 As ToolStripSeparator
    Friend WithEvents cmnuHeber As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents mnuHeber As ToolStripMenuItem
    Friend WithEvents cmnuInclude As ToolStripMenuItem
    Friend WithEvents mnuInclude As ToolStripMenuItem
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents mnuVerein As ToolStripMenuItem
    Friend WithEvents cmnuVerein As ToolStripMenuItem
    Friend WithEvents chkRelativ As CheckBox
    Friend WithEvents cboVereinswertung As ComboBox
    Friend WithEvents lblVereinswertung As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ToolStripMenuItem3 As ToolStripSeparator
    Friend WithEvents cmnuÜbernahme As ToolStripMenuItem
    Friend WithEvents mnuWiegeliste_Druck As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As ToolStripSeparator
    Friend WithEvents mnuMauszeiger As ToolStripMenuItem
    Friend WithEvents mnuStartnummern As ToolStripMenuItem
    Friend WithEvents mnuSkipRules As ToolStripMenuItem
    Friend WithEvents WK As DataGridViewCheckBoxColumn
    Friend WithEvents Sortierung As DataGridViewTextBoxColumn
    Friend WithEvents Losnummer As DataGridViewTextBoxColumn
    Friend WithEvents Startnummer As DataGridViewTextBoxColumn
    Friend WithEvents Teilnehmer As DataGridViewTextBoxColumn
    Friend WithEvents Nachname As DataGridViewTextBoxColumn
    Friend WithEvents Vorname As DataGridViewTextBoxColumn
    Friend WithEvents Sex As DataGridViewTextBoxColumn
    Friend WithEvents Jahrgang As DataGridViewTextBoxColumn
    Friend WithEvents Geburtstag As DataGridViewTextBoxColumn
    Friend WithEvents Nation As DataGridViewTextBoxColumn
    Friend WithEvents Verein As DataGridViewTextBoxColumn
    Friend WithEvents Land As DataGridViewTextBoxColumn
    Friend WithEvents AK As DataGridViewTextBoxColumn
    Friend WithEvents Gewicht As DataGridViewTextBoxColumn
    Friend WithEvents Wiegen As DataGridViewTextBoxColumn
    Friend WithEvents GK As DataGridViewTextBoxColumn
    Friend WithEvents Gruppe As DataGridViewTextBoxColumn
    Friend WithEvents Zweikampf As DataGridViewTextBoxColumn
    Friend WithEvents Reißen As DataGridViewTextBoxColumn
    Friend WithEvents Stoßen As DataGridViewTextBoxColumn
    Friend WithEvents Länderwertung As DataGridViewCheckBoxColumn
    Friend WithEvents Vereinswertung As DataGridViewTextBoxColumn
    Friend WithEvents RelativW As DataGridViewCheckBoxColumn
End Class
