﻿
Imports System.ComponentModel

Public Class frmScreenIdent

    Private Timer1 As Threading.Timer
    Private duration As Integer = 5

    Private Sub frmScreenIdent_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Timer1.Dispose()
    End Sub
    Private Sub frmScreenIdentifier_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            duration = CInt(NativeMethods.INI_Read("Display", "Duration", App_IniFile))
        Catch
        End Try
    End Sub
    Private Sub frmScreenIdentifier_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Timer1 = New Threading.Timer(AddressOf Timer1_Tick)
        Timer1.Change(0, 1000)
    End Sub

    Private Delegate Sub DelegateTimer(State As Object)
    Private Sub Timer1_Tick(State As Object)
        Try
            If InvokeRequired Then
                Dim d As New DelegateTimer(AddressOf Timer1_Tick)
                Invoke(d, New Object() {State})
            Else
                duration -= 1
                If duration = 0 Then
                    Close()
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

End Class