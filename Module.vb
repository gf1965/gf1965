﻿
Imports System.Drawing.Text
Imports System.Management.Automation
Imports System.Management.Automation.Runspaces
Imports System.Threading.Tasks
Imports Microsoft.Win32
Imports MySqlConnector
Imports Squirrel

Module Module1

    Public FR_Driver As String = "MySQL ODBC 8.0 Unicode Driver"

    Public AlternatingRowsDefaultCellStyle_BackColor As Color = Color.FromArgb(235, 255, 255)
    Public RowsDefaultCellStyle_BackColor As Color = Color.FromArgb(222, 255, 232)

    Public GlobalFonts As New PrivateFontCollection
    Public FontFamilies() As FontFamily

    Public msgCaption As String = Application.CompanyName & " " & Application.ProductName

    Public HideMessages As Boolean
    Public RedirectToLocalhost As Boolean

    'Public ExternCounter As New Extern

    Public App_IniFile As String
    Public ScreensFile As String

    Class Extern
        Private _Success As Integer
        Private _Fail As Integer
        Private _Count As Integer
        Private _Name As String

        Public Event Init(CalcName As String, EventsCount As Integer)
        Public Event Clear(Text As String)
        Public Event Change(Response As String)

        Public Sub Start(CalcName As String, EventsCount As Integer)
            _Success = 0
            _Fail = 0
            _Count = EventsCount
            _Name = CalcName
            RaiseEvent Init(CalcName, EventsCount)
        End Sub

        Public Sub Return_Success(Response As Boolean)
            If Response Then
                _Success += 1
            Else
                _Fail += 1
            End If
            RaiseEvent Change(_Success.ToString)
            If _Success + _Fail = _Count Then
                RaiseEvent Clear(If(_Fail = 0, "", _Name & "-Update " & _Fail & " Fehler"))
            End If
        End Sub

    End Class

    Public Anzeige As myAnzeige

    'Public Event KR_Initialize()
    'Private _init As Boolean
    Property KR_IsInitialized As Boolean

    '    Get
    '        KR_IsInitialized = _init
    '    End Get
    '    Set(value As Boolean)
    '        If Not _init.Equals(value) Then
    '            _init = value
    '            RaiseEvent KR_Initialize()
    '        End If
    '    End Set
    'End Property

    Public Event ConnError_Changed(ConnError As Boolean?)
    Private _ConnErr As Boolean?
    Property ConnError As Boolean?
        Get
            ConnError = _ConnErr
        End Get
        Set(value As Boolean?)
            If Not _ConnErr.Equals(value) Then
                _ConnErr = value
                RaiseEvent ConnError_Changed(_ConnErr)
            End If
        End Set
    End Property


    Friend formMain As frmMain
    Friend formLeader As frmLeitung 'frmLeader
    Friend formHantelbeladung As frmHantelBeladung
    Friend formEasyMode As frmEasyMode

    Structure ValueList
        Friend Form As Form
        Friend Display As Integer
        Friend Fontsize As Single
    End Structure
    ' Key = Nummer aus Screen.DeviceName, Value = {Form, AnzeigeNummer}
    'Public dicScreens As New Dictionary(Of Integer, KeyValuePair(Of Form, Integer))
    Public dicScreens As New Dictionary(Of Integer, ValueList)

    '' Key = {Nummer aus Screen.DeviceName, Wettkampf-Nr.}, Value = {Form, AnzeigeNummer}
    'Public dicScreens As New Dictionary(Of KeyValuePair(Of Integer, Integer), KeyValuePair(Of Form, Integer))

    Public dtUser As DataTable
    Public User As New myUser
    'Public dtHosts As DataTable
    'Public dvHosts As DataView

    Enum UserID
        Wettkampfleiter
        Versuchsermittler
        'Jury
        Administrator
    End Enum

    Public Function ConvertTextToRTF(PlainText As String) As String
        If PlainText.StartsWith("{\rtf1") Then Return PlainText
        Dim RTF As New RichTextBox
        RTF.Text = PlainText
        Return RTF.Rtf
    End Function
    Public Function NoCompetition() As Boolean
        Return IsNothing(Wettkampf.Identities) OrElse Wettkampf.Identities.Count = 0 OrElse Wettkampf.Identities.Keys(0).Equals("0")
    End Function
    Public Function ConnTest(ConnString As String, UsePassword As Boolean, Optional Form As Form = Nothing) As Boolean

        Dim result = False
        Dim msg = String.Empty
        Dim icon = MessageBoxIcon.Information

        Using conn As New MySqlConnection(ConnString)
            Try
                conn.Open()
                msg = "Verbindung zur Datenbank erfolgreich hergestellt"
                result = True
            Catch ex As MySqlException
                msg = "Fehler beim Verbinden mit der Datenbank"
                icon = MessageBoxIcon.Error
                LogMessage("ConnTest: Source=" & If(IsNothing(Form), "Extern", Form.Name) & ": " & ex.Message, ex.StackTrace &
                            If(Not IsNothing(ex.InnerException), vbNewLine & ex.InnerException.Message + " ErrorCode:  " + Err.Number.ToString, String.Empty) &
                            vbNewLine & "Connection = " & ConnString)
            End Try
        End Using

        If Not IsNothing(Form) Then
            msg += vbNewLine + "Benutze Passwort: " & If(UsePassword, "Ja", "Nein")

            Using New Centered_MessageBox(Form)
                MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, icon)
            End Using
        End If

        Return result

    End Function

    Private Declare Function InternetGetConnectedState Lib "wininet.dll" (ByRef lpSFlags As Integer, dwReserved As Integer) As Boolean
    Enum InetConnState
        modem = &H1
        lan = &H2
        proxy = &H4
        ras = &H10
        offline = &H20
        configured = &H40
    End Enum
    Public Function CheckInetConnection() As String
        Dim lngFlags As Integer
        If InternetGetConnectedState(lngFlags, 0&) Then
            ' verbunden
            If CBool(lngFlags And InetConnState.lan) Then
                Return "LAN"
            ElseIf CBool(lngFlags And InetConnState.modem) Then
                Return "Modem"
            ElseIf CBool(lngFlags And InetConnState.configured) Then
                Return "Configured"
            ElseIf CBool(lngFlags And InetConnState.proxy) Then
                Return "Proxy"
            ElseIf CBool(lngFlags And InetConnState.ras) Then
                Return "RAS"
            ElseIf CBool(lngFlags And InetConnState.offline) Then
                Return "Offline"
            End If
        Else
            ' nicht verbunden
            Return "Not Connected"
        End If
        Return "Failed"
    End Function

    Public Function Get_NK_Stellen(Wertung As Integer) As Integer
        Dim rows = Glob.dtWertung.Select("idWertung = " & Wertung)
        If rows.Count = 1 Then Return CInt(rows(0)!Genauigkeit)
        Return 3
    End Function

    Class myUser
        Implements IDisposable

        Property Guid As String

#Region "User"
        Public Event User_Changed()
        Public Event Bohle_Changed()

        Private _UserId As Integer = -1
        Private _Bohle As Integer = -1
        Private _ConnString As String

        Property UserId As Integer ' 0=WK-Leiter, 1=Versuchsermittler, 2=Administrator
            Get
                Return _UserId
            End Get
            Set(value As Integer)
                _UserId = value
                RaiseEvent User_Changed()
            End Set
        End Property
        Property Bohle As Integer
            Get
                Return _Bohle
            End Get
            Set(value As Integer)
                _Bohle = value
                RaiseEvent Bohle_Changed()
            End Set
        End Property
        Property ServerIP As String
        Property ServerPort As String
        Property NetworkID As Integer = -1 ' bei mehreren Netzwerken auf dem PC muss eins für die Connection ausgewählt werden
        Property Passwort As String ' = "GFHsoft"
        Property ConnString As String
            Get
                Return _ConnString
            End Get
            Set(value As String)
                _ConnString = value
                FR_ConnString = Create_FR_Connstring()
                APP_ConnString = Create_APP_Connstring()
                BVDG_ConnString = Create_BVDG_Connstring()
            End Set
        End Property
        Property Database As String
        Property MySQL_Driver As String
        Property FR_ConnString As String            ' FastReport Connection including MySQL-Driver
        'Property APP_ConnString As Task(Of String)  ' Connection to calculator.gfhsoft.de Database
        Property APP_ConnString As String  ' Connection to calculator.gfhsoft.de Database
        Property BVDG_ConnString As Task(Of String) ' Connection to BVDG-Database
        Property Lizenz_ConnString As String = Create_LizenzServer_ConnString()
#End Region

        Property IPs() As List(Of HostIP)
        Public Sub New()
            IPs = New List(Of HostIP)
        End Sub

#Region "FastReport Connection"
        Function Get_Driver() As String()
            Dim Drivers As String() = Nothing
            Dim Keys As String() = {"SOFTWARE\ODBC\ODBC.INI\ODBC Drivers", "SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers"}
            Try
                For Each KeyString In Keys
                    Using Key As RegistryKey = Registry.LocalMachine.OpenSubKey(KeyString, RegistryKeyPermissionCheck.ReadSubTree)
                        If Not IsNothing(Key) Then
                            Drivers = Key.GetValueNames()
                            If Drivers.Count > 0 Then Exit For
                        End If
                    End Using
                Next
            Catch ex As Exception
            End Try

            Return Drivers
        End Function
        Private Function Create_FR_Connstring() As String
            Dim conn As New List(Of String)
            Dim ps = Split(User.ConnString, ";")
            conn.Add("Driver={" & User.MySQL_Driver & "}")
            'For Each p In ps
            '    If p.StartsWith("server") Or p.StartsWith("database") Or p.StartsWith("port") Then
            '        conn.Add(p)
            '    ElseIf p.StartsWith("user") Then
            '        conn.Add("uid=" & Split(p, "=")(1))
            '    ElseIf p.StartsWith("pass") Then
            '        conn.Add("pwd=" & Split(p, "=")(1))
            '    End If
            'Next
            'Return Join(conn.ToArray, ";") & ";"
            For Each p In ps
                If p.StartsWith("user") Then
                    conn.Add("uid=" & Split(p, "=")(1))
                ElseIf p.StartsWith("pass") Then
                    conn.Add("pwd=" & Split(p, "=")(1))
                Else
                    conn.Add(p)
                End If
            Next
            Return Join(conn.ToArray, ";")
        End Function
        Function Check_MySQLDriver(Form As Form) As Boolean
            If IsNothing(User.MySQL_Driver) OrElse User.MySQL_Driver.Equals(String.Empty) Then
                Using New Centered_MessageBox(Form)
                    MessageBox.Show("Kein Treiber für die Datenverbindung zum Report." & vbNewLine & vbNewLine &
                                 "Das führt zu Problemen bei Nutzung von Daten aus der Datenbank im Report.",
                                 msgCaption,
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Warning) ', MessageBoxDefaultButton.Button1, 0, True)
                End Using
                Return False
            End If
            Return True
        End Function
#End Region

#Region "calculator.gfhsoft.de"
        'Async Function Create_APP_Connstring(Optional local As Boolean = False) As Task(Of String)
        Function Create_APP_Connstring() As String

            'If local Then
            '    Dim con = Split(User.ConnString, ";").ToList
            '    Dim ix = con.FindIndex(Function(value As String)
            '                               Return value.StartsWith("database=")
            '                           End Function)
            '    con(ix) = con(ix) & "_sinclair"
            '    Return Join(con.ToArray, "; ") & ";"
            'End If

            Dim conn() As String = {"server=89.22.102.231",
                                    "database=weightlift",
                                    "port=3306",
                                    "userid=Admin",
                                    "password=zv9Aal0m5aDM3eyUXgrD",
                                    "Convert Zero Datetime=True"} ',
            '"connect timeout=1"}
            'Dim test = Await Task.Run(Function() ConnTest(Join(conn, "; "), True))
            'If test Then
            Return Join(conn, "; ") & ";"
            'Else
            '    Return String.Empty
            'End If
        End Function
#End Region

#Region "BVDG-API"
        Async Function Create_BVDG_Connstring() As Task(Of String)

            Dim conn() As String = {"server=onlineportal.bvdg-online.de",
                                    "database=api_bvdg",
                                    "port=3306",
                                    "userid=wettkampfsoftware",
                                    "password=hantel123!",
                                    "Convert Zero Datetime=True",
                                    "connect timeout=1"}
            Dim test = Await Task.Run(Function() ConnTest(Join(conn, "; "), True))
            If test Then
                Return Join(conn, "; ") & ";"
            Else
                Return String.Empty
            End If
        End Function
#End Region

#Region "Lizenz-Server"
        Function Create_LizenzServer_ConnString() As String
            Dim conn() As String = {"server=89.22.102.231",
                                    "database=gfhsoft",
                                    "port=3306",
                                    "userid=GFHsoft_Admin",
                                    "password=GFHsoft_1965",
                                    "Convert Zero Datetime=True"}
            Return Join(conn, "; ") & ";"
        End Function
#End Region

#Region "IDisposable Support"
        Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.
        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
                End If

                ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
                ' TODO: große Felder auf Null setzen.
            End If
            disposedValue = True
        End Sub
        ' TODO: Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
        'Protected Overrides Sub Finalize()
        '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
            Dispose(True)
            ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
            ' GC.SuppressFinalize(Me)
        End Sub
#End Region
    End Class

    Declare Function SetForegroundWindow Lib "user32" Alias "SetForegroundWindow" (ByVal hwnd As IntPtr) As Integer

    Public Function IsFormPresent(Caption As String()) As SortedList(Of Integer, Integer)
        ' Einträge in der Liste
        '   Key   = DisplayNummer --> Screen(x).DisplayName
        '   Value = ScreenNummer  --> Sreens.AllScreens(x)

        Dim KeyList As New SortedList(Of Integer, Integer)
        For Each item In dicScreens
            For Each c In Caption
                If item.Value.Form.Text.StartsWith(c) Then KeyList.Add(item.Key, item.Value.Display)
            Next
        Next
        Return KeyList
    End Function

    Public Function Get_FileName(Form As Form, Optional Title As String = "") As String
        Dim Result = DialogResult.Cancel
        Dim FileName = String.Empty

        Using FileDialog As New OpenFileDialog
            With FileDialog
                If Not String.IsNullOrEmpty(Title) Then .Title = Title
                .Filter = "Druckvorlagen (*.frx)|*.frx|Alle Dateien (*.*)|*.*"
                .InitialDirectory = Path.Combine(Application.StartupPath, "Report")
                Result = .ShowDialog()
                FileName = .FileName
                .Dispose()
            End With
        End Using

        If Result = DialogResult.Cancel Then Return Nothing

        If Not FileName.Contains(".frx") Then
            Using New Centered_MessageBox(Form)
                MessageBox.Show("Die ausgewählte Datei ist keine Druckvorlage.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            Return Nothing
        End If

        Return FileName
    End Function

    Public Enum Fonts
        Zeitanzeige7 = 0
        Zeitanzeige7mono = 1
        Techniknote = 2
        Zeitanzeige = 3
        Wertungsanzeige = 4
    End Enum

    Public Function Database_Update(OwnerForm As Form) As KeyValuePair(Of Integer, String)

        Dim SuccessCount = -1
        Dim UpdateFile = Path.Combine(Application.StartupPath, "update.sql")
        If Not File.Exists(UpdateFile) Then Return New KeyValuePair(Of Integer, String)(1, String.Empty)

        'Set_Progress(0)
        'Status.Text = String.Empty

        Dim SysTable = Get_DataBases(User.ConnString)

        Dim Databases = (From x In SysTable
                         Select x.Field(Of String)("schema_name")).ToArray

#Region "Advanced"
        'DatabasesCount = Databases.Count

        'If Databases.Count > 1 Then
        '    Using DB_Select As New frmDatabaseSelect
        '        With DB_Select
        '            .Databases = Databases
        '            .ShowDialog(OwnerForm)
        '            Databases = .Databases
        '        End With
        '    End Using
        'End If
        'If Databases.Count = 0 Then Return String.Empty

        'Status.Text = "Datenbank" & If(Databases.Count = 1, "", "en") & " aktualisieren"
#End Region

        ' User.Connstring analysieren
        Dim Parts = Split(User.ConnString, ";").ToList
        ' suche aktuell verbundene Datenbank
        Dim Pos = Parts.FindIndex(Function(value As String)
                                      Return LCase(value).StartsWith("database")
                                  End Function)
        'Dim PartValue = 100 \ Databases.Count
        'Dim ProgressValue = 0

        Dim AllText = File.ReadAllText(UpdateFile)
        ' example first row: -- CREATION_TIME = 2024-06-12 00:00:00#
        Dim Contents = Split(AllText, "-- CREATION_TIME = ").ToList
        If Contents.Count > 0 AndAlso String.IsNullOrEmpty(Contents(0)) Then Contents.RemoveAt(0)

        For Each Database In Databases
            Dim Found As DataRow() = SysTable.Select("schema_name = '" & Database & "'")

            If Found.Length > 0 Then

                Parts(Pos) = "database=" & Database
                'ProgressValue += PartValue
                'Set_Progress(ProgressValue)

                Dim LastUpdated As Date = CDate(Found(0)!last_updated)

                For Each Content In Contents
                    Dim Cont = Split(Content, "#")
                    Dim UpdateCreated = CDate(Cont(0))
                    Dim Query = Cont(1)

                    If LastUpdated <= UpdateCreated Then
                        If SuccessCount = -1 Then SuccessCount = 0
                        Try
                            Using conn As New MySqlConnection(Join(Parts.ToArray, ";"))
                                Dim cmd = New MySqlCommand(Query, conn)
                                conn.Open()
                                cmd.ExecuteNonQuery()
                                ConnError = False
                            End Using
                            SuccessCount += 1
                        Catch ex As MySqlException
                            MySQl_Error(Nothing, "Module: Database_Update (" & Parts(Pos) & "): " & ex.Message, ex.StackTrace, True)
                        End Try
                    End If
                Next
            End If
        Next

        'Status.Text = String.Empty
        'Info.Text = vbNewLine & vbNewLine & "Datenbank-Update"

        If SuccessCount <> 0 Then File.Delete(UpdateFile)

        Dim Status = 1
        Dim Message = "Datenbank-Update für " & SuccessCount & " Objekt" & If(SuccessCount = 1, "", "e") & " abgeschlossen"

        If Databases.Count > SuccessCount Then
            Status = -1
            Message += vbNewLine & "(für " & Databases.Count - SuccessCount & " Objekt" & If(Databases.Count - SuccessCount = 1, "", "e") & " fehlgeschlagen)"
        End If

        'Return Info.Text
        Return New KeyValuePair(Of Integer, String)(Status, Message)
    End Function
    Public Function Get_DataBases(ConnString As String, Optional SelectAll As Boolean = False) As DataTable
        Dim table As New DataTable
        Using conn As New MySqlConnection(ConnString)
            Try
                conn.Open()
                Dim query = "Select table_schema `schema_name`, max(create_time) last_updated
                             From information_schema.tables "
                If SelectAll Then
                    query += "table_schema Not In('information_schema', 'mysql', 'performance_schema','sys', 'phpmyadmin') 
                              and table_type ='BASE TABLE' "
                Else
                    query += "Where table_schema Like 'weightlift%' "
                End If
                query += "group by table_schema
                          order by last_updated desc, `schema_name` asc;"
                Dim cmd = New MySqlCommand(query, conn)
                table.Load(cmd.ExecuteReader())
                Return table
            Catch ex As MySqlException
                MySQl_Error(Nothing, ex.Message, ex.StackTrace, True)
            End Try
        End Using
        Return Nothing
    End Function

    Public Async Function CheckUpdates() As Task(Of Integer)
        Dim x = NativeMethods.INI_Read("Programm", "UpdateDir", App_IniFile)
        If x <> "?" Then UpdateDir = x.ToString

        Dim Releases As New List(Of ReleaseEntry)

        Using Manager As New UpdateManager(UpdateDir)
            If Directory.Exists(UpdateDir) OrElse UpdateDir.StartsWith("http") Then
                Dim Updates = Await Task.Run(Function() Manager.CheckForUpdate())
                Releases = Updates.ReleasesToApply
            End If
        End Using
        Return Releases.Count
    End Function

    Public Async Function CheckForUpdates(Optional Owner As Form = Nothing, Optional Silent As Boolean = False) As Task(Of String)

        Dim x = NativeMethods.INI_Read("Programm", "UpdateDir", App_IniFile)
        If x <> "?" Then UpdateDir = x.ToString

        Dim NewVersion As String = String.Empty
        Dim AppVersion = New Dictionary(Of String, Integer)
        Dim msg = String.Empty
        Dim icon = MessageBoxIcon.Information
        Dim buttons = MessageBoxButtons.YesNo
        Dim Labels() As String = {}

        Dim Updates As UpdateInfo = Nothing
        Dim CurrentVersion As NuGet.SemanticVersion = Nothing
        Dim FutureVersion As NuGet.SemanticVersion = Nothing
        Dim Releases As New List(Of ReleaseEntry)
        Dim PackageName As String = String.Empty
        Dim Filesize As Long = 0
        Dim Filename As String = String.Empty

        Dim LogMsg As New List(Of String)

        Using Manager As New UpdateManager(UpdateDir)
            Try
                If Directory.Exists(UpdateDir) OrElse UpdateDir.StartsWith("http") Then

                    Updates = Await Task.Run(Function() Manager.CheckForUpdate())
                    LogMsg.Add("Updates: " & Updates.ToString)

                    Dim Current = Updates.CurrentlyInstalledVersion
                    If Not IsNothing(Current) Then CurrentVersion = Current.Version
                    LogMsg.Add("CurrentVersion: " & If(IsNothing(Current), "", CurrentVersion.ToString))

                    Dim Future = Updates.FutureReleaseEntry
                    If Not IsNothing(Future) Then FutureVersion = Future.Version
                    LogMsg.Add("FutureVersion: " & FutureVersion.ToString)

                    PackageName = Updates.FutureReleaseEntry.PackageName
                    LogMsg.Add("PackageName: " & PackageName)

                    Filesize = Updates.FutureReleaseEntry.Filesize  'is Byte --> Format(Filesize / 1048576, "#,##0.00 MB")
                    LogMsg.Add("Filesize: " & Format(Filesize / 1048576, "#,##0.00 MB"))

                    Filename = Updates.FutureReleaseEntry.Filename
                    LogMsg.Add("Filename: " & Filename)

                    LogUpdate(Join(LogMsg.ToArray, vbNewLine))

                    Dim Versions = {"Major", "Minor", "Build"}
                    Dim p = Split(Application.ProductVersion, ".")
                    For i = 0 To p.Length - 1
                        AppVersion(Versions(i)) = CInt(p(i))
                    Next
                    If FutureVersion.Version.Major > AppVersion("Major") Then
                        msg = "Ein erforderliches Update ist verfügbar."
                        icon = MessageBoxIcon.Warning
                    ElseIf FutureVersion.Version.Minor > AppVersion("Minor") Then
                        msg = "Ein wichtiges Update ist verfügbar."
                    ElseIf FutureVersion.Version.Build > AppVersion("Build") Then
                        msg = "Ein Update ist verfügbar."
                    Else
                        buttons = MessageBoxButtons.OK
                    End If

                    'If Releases.Count = 0 Then
                    '    buttons = MessageBoxButtons.OK
                    '    If Silent Then Return
                    '    msg = "Das Programm ist auf dem neuesten Stand."
                    'End If

                    If String.IsNullOrEmpty(msg) Then
                        If Silent Then Return Nothing
                        msg = "Das Programm ist auf dem neuesten Stand."
                    Else
                        msg += vbNewLine + vbTab + "Version installiert: " + Application.ProductVersion
                        msg += vbNewLine + vbTab + "Version verfügbar: " + FutureVersion.ToString +
                        " (Größe: " + Format(Filesize / 1048576, "#,##0.00 MB") + ")"
                        msg += vbNewLine + vbNewLine + "Wann soll die Installation gestartet werden?"
                        Labels = {"Jetzt", "Später"}
                    End If
                Else
                    LogUpdate("Warning: Update-Informationen nicht gefunden.")
                    msg = "Update-Informationen nicht gefunden."
                    If Silent Then Return msg
                    buttons = MessageBoxButtons.OK
                    icon = MessageBoxIcon.Warning
                End If
            Catch ex As Exception
                LogUpdate("Error at Update: " & ex.Message & vbNewLine & "StackTrace " &
                          If(InStr(ex.StackTrace, "Zeile") > 0, Mid(ex.StackTrace, InStr(ex.StackTrace, "Zeile")), ex.StackTrace))
                msg = "Fehler beim Update." & vbNewLine & ex.Message
                If Silent Then Return msg
                buttons = MessageBoxButtons.OK
                icon = MessageBoxIcon.Error
            End Try

            Releases = Updates.ReleasesToApply
            LogUpdate("Releases.Count: " & Releases.Count)

            Using New Centered_MessageBox(Owner, Labels)
                Dim Result = MessageBox.Show(msg, msgCaption, buttons, icon)
                If Result = DialogResult.Yes Then
                    Dim ProgressDelegate As Action(Of Integer) = AddressOf Progress
                    Try
                        If Releases.Count > 0 Then
                            Await Task.Run(Function() Manager.DownloadReleases(Releases, ProgressDelegate))
                            NewVersion = Await Task.Run(Function() Manager.ApplyReleases(Updates, ProgressDelegate).Result)
                            LogUpdate("New Version: " & NewVersion)
                            UpdateManager.RestartApp()
                        End If
                    Catch ex As Exception
                        msg = "Error at Update: " & ex.Message
                        LogUpdate(msg)
                        If Silent Then Return msg
                    End Try
                ElseIf Releases.Count > 0 Then
                    Return "Update pending"
                End If
            End Using
        End Using

        Return Nothing
    End Function
    Private Sub Progress(Value As Integer)
        Debug.WriteLine(Value.ToString)
    End Sub

    ' Sub Main
    <STAThread()>
    Public Sub Main(commandLineArgs As String())

        If Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName).Length > 1 Then
            Dim p As Process
            Dim Ret As Integer
            Dim hWndMain As IntPtr
            Dim MyProcesses() As Process = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName)
            For Each p In MyProcesses
                ' eigenen Prozeß ausschließen 
                If (p.Id <> Process.GetCurrentProcess().Id) Then
                    hWndMain = p.MainWindowHandle()
                    Ret = SetForegroundWindow(hWndMain)
                    MessageBox.Show("Das Programm wurde bereits gestartet." & vbNewLine & "Alle Vorkommen von 'Gewichtheben' im Task-Manager schließen und erneut versuchen.", msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Application.Exit()
                End If
            Next
        Else
            Application.EnableVisualStyles()
            Application.SetCompatibleTextRenderingDefault(False)

            AddHandler Application.ThreadException, AddressOf GeneralErrorHandler

            'Dim Encoders = {"Unicode", "ANSI"}
            'Dim Platform = If(Environment.Is64BitOperatingSystem, "64-bit", "32-bit")
            'Dim DriversPath = Path.Combine(Application.StartupPath, "MySQLConnectorODBC", Platform)
            'Dim OdbcDrivers As New List(Of String)
            'Dim invoker As RunspaceInvoke = New RunspaceInvoke()

            'For Each Encoder In Encoders
            '    Dim ScriptText = "Get-OdbcDriver ""MySQL ODBC 8.0 " & Encoder & " Driver"" -Platform " & Platform
            '    For Each obj As PSObject In invoker.Invoke(ScriptText)
            '        OdbcDrivers.Add(obj.BaseObject.ToString)
            '    Next
            'Next
            'invoker.Dispose()

            'If OdbcDrivers.Count = 0 Then
            '    'invoker = New RunspaceInvoke()
            '    'invoker.Invoke("Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope Process")
            '    'If Platform.Equals("64-bit") Then
            '    '    invoker.Invoke(Path.Combine(DriversPath, "VC_redist.x64.exe /install /quiet /norestart"))
            '    '    invoker.Invoke("MsiExec.exe /i " & Path.Combine(DriversPath, "mysql-connector-odbc-8.1.0-winx64.msi /qn"))
            '    'End If
            '    'invoker.Dispose()
            'End If

            FastReport.Utils.Res.LoadLocale(Path.Combine(Application.StartupPath, "Report\Localization\German.frl"))

            GlobalFonts.AddFontFile(Path.Combine(Application.StartupPath, "fnt", "digital-7-neu.ttf"))              ' Zeitanzeige mit anderer 7    
            GlobalFonts.AddFontFile(Path.Combine(Application.StartupPath, "fnt", "Digital-7Mono-neu.ttf"))          ' Zeitanzeige mit anderer 7 MonoSpace   
            GlobalFonts.AddFontFile(Path.Combine(Application.StartupPath, "fnt", "DS-Digital.ttf"))                 ' Technik-Note
            GlobalFonts.AddFontFile(Path.Combine(Application.StartupPath, "fnt", "Technology-Regular-mono1.ttf"))   ' Zeitanzeige
            GlobalFonts.AddFontFile(Path.Combine(Application.StartupPath, "fnt", "wingding.ttf"))                   ' Wertungsanzeige
            FontFamilies = GlobalFonts.Families

            'App_IniFile = Path.Combine(Application.StartupPath, "Gewichtheben.ini")
            'App_IniFile = Path.Combine(Application.StartupPath, Application.ProductName & ".ini")
            App_IniFile = Application.ExecutablePath.Replace("exe", "ini")
            ScreensFile = Path.Combine(Application.StartupPath, "Screens.src")

            Get_Optionen()
            Anzeige = New myAnzeige
            formMain = New frmMain
            'formMain.Text += " " + Application.ProductVersion
            Application.Run(formMain)

            Steuerung.Dispose()

            Dim cmd As MySqlCommand
            Using conn As New MySqlConnection(User.ConnString)
                Try
                    conn.Open()
                    cmd = New MySqlCommand("DELETE FROM users WHERE UserId = " & User.UserId & " AND Bohle = " & User.Bohle & ";", conn)
                    cmd.ExecuteNonQuery()
                Catch ex As MySqlException
                End Try
            End Using

            ' falls MySQL während Anmeldung gestartet wwurde --> wieder schließen
            If IsMySQL_Started Then
                Dim proc As Process = New Process()
                With proc
                    .StartInfo = New ProcessStartInfo("C:\xampp\mysql_stop.bat")
                    .Start()
                    .WaitForExit()
                End With
            End If
        End If

    End Sub

    Private Sub GeneralErrorHandler(sender As Object, e As Threading.ThreadExceptionEventArgs)

        Dim msg = "Unbehandelter Anwendungsfehler: " & vbNewLine &
                   e.Exception.Message.ToString & vbNewLine & vbNewLine &
                  "StackTrace:" & vbNewLine &
                   Trim(Split(e.Exception.StackTrace.ToString, "bei")(1)) & vbNewLine &
                  "Bitte kontaktieren Sie den Hersteller."

        MessageBox.Show(msg, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)

        LogMessage("GeneralErrorHandler: Unbehandelter Anwendungsfehler: " & e.Exception.Message.ToString, e.Exception.StackTrace)
    End Sub
    Public Sub LogUpdate(Message As String)
        Dim LogFile = Path.Combine(Application.StartupPath, "log", "update_" & Format(Now, "yyyy-MM-dd") & ".log")
        If Not File.Exists(LogFile) Then
            If Not Directory.Exists(Path.Combine(Application.StartupPath, "log")) Then Directory.CreateDirectory(Path.Combine(Application.StartupPath, "log"))
        End If
        Try
            Dim Stream As StreamWriter = File.AppendText(LogFile)
            Stream.WriteLine(Format(Now, "HH:mm:ss.fff"))
            Stream.WriteLine(Message & vbNewLine)
            Stream.Flush()
            Stream.Close()
        Catch ex As Exception
        End Try
    End Sub
    Public Sub LogMessage(Message As String, Optional StackTrace As String = "", Optional LogFile As String = "")
        If Not File.Exists(LogFile) Then
            If String.IsNullOrEmpty(LogFile) Then
                ' Dateiname anhand des aktuellen Datums festlegen
                LogFile = Path.Combine(Application.StartupPath, "log", "err_" & Format(Now, "yyyy-MM-dd") & ".log")
            End If
            If Not Directory.Exists(Path.Combine(Application.StartupPath, "log")) Then Directory.CreateDirectory(Path.Combine(Application.StartupPath, "log"))
        End If

        Try
            ' Datei öffen (Text anhängen)
            Dim Stream As StreamWriter = File.AppendText(LogFile)
            ' Datum und Uhrzeit autom. eintragen
            Stream.WriteLine(Format(Now, "HH:mm:ss.fff"))
            ' Message (Fehlermeldung) speicherrn
            Stream.WriteLine(Message & vbNewLine)
            ' StackTrace speicherrn
            If Not String.IsNullOrEmpty(StackTrace) Then
                Stream.WriteLine("StackTrace: " & If(InStr(StackTrace, "Zeile") > 0, Mid(StackTrace, InStr(StackTrace, "Zeile")), StackTrace) & vbNewLine)
            End If
            ' Datei schließen
            Stream.Flush()
            Stream.Close()
        Catch ex As Exception
        End Try
    End Sub
    Public Sub LogDatabaseError(Message As String, StackTrace As String, Optional Folder As String = "")
        LogMessage(
            Message & " / StackTrace " & If(InStr(StackTrace, "Zeile") > 0, Mid(StackTrace, InStr(StackTrace, "Zeile")), String.Empty),
            Path.Combine(Application.StartupPath, If(String.IsNullOrEmpty(Folder), "log", Folder), "sql_" & Format(Now, "yyyy-MM-dd") & ".log"))
    End Sub

    Public Sub Lock_Application(myForm As Form)
        Using FormAnmeldung As New frmAnmeldung
            With FormAnmeldung
                .cboUser.Enabled = False
                .cboBohle.Enabled = False
                .btnServer.Visible = False
                .Height = .MinimumSize.Height
                .Lock = True
                .OwnerForm = myForm
                .ShowDialog(myForm)
                If .DialogResult = DialogResult.Cancel Then Application.Exit()
                .Dispose()
            End With
        End Using
    End Sub

    Public Function Get_AppRoot() As String
        Dim Parts = Split(Path.Combine(Application.StartupPath), "\").ToList
        Parts.Remove(Parts.LastOrDefault)
        If Parts(0).EndsWith(":") Then Parts(0) += "\"
        Return Path.Combine(Parts.ToArray)
    End Function

    Class myMsg
        Private _msg As String
        Event Msg_Changed(Value As String)
        Property msg As String
            Get
                Return _msg
            End Get
            Set(value As String)
                _msg = value
                RaiseEvent Msg_Changed(_msg)
            End Set
        End Property
    End Class

    Enum Modus
        Indeterminate
        Normal
        Easy
        EasyHantel
    End Enum

    Public Wettkampf As New myWettkampf
    Class myWettkampf
        Private _id As Integer
        Private _ak(1) As Integer
        'Private _bezeichnung As String
        Private _identities As New Dictionary(Of String, String)
        Private _minlast(1, 1) As Integer
        Private _zklast(1) As Integer
        Private _steigerung(1) As Integer
        Private _technik As Boolean
        Private _kr(2) As Integer
        Private _wkmodus As Integer
        Private _international As Boolean
        Private _multibohle As Boolean
        Private _wertungsfaktor(1) As Double

        Public Event ID_Changed(ID As Integer)
        Public Event Bezeichnung_Changed(Bezeichung As String)
        Public Event Technik_Changed(Technikwertung As Boolean) ' ändert die Breite der Wertungsanzeige in Leader, Aufwärmen, Moderator; Spalten-Anzahl in Publikum
        Public Event WKModus_Changed()
        Public Event MultiBohle_Changed()

        'Public Event Change_Settings(AtWeighIn As Boolean)
        'Public Event Bohle_Changed(Bohle As Integer)
        'Public Event KR_Changed(KR As Integer)
        'Public Event Mannschaft_Changed(Mannschaft As Boolean) ' Mannschafts-WK (Liga)

        Property ID As Integer
            Get
                Return _id
            End Get
            Set(value As Integer)
                _id = value
                RaiseEvent ID_Changed(_id)
            End Set
        End Property
        Property Bezeichnung As String
        '    Get
        '        Return _bezeichnung
        '    End Get
        '    Set(value As String)
        '        If Not (value = _bezeichnung) Then
        '            _bezeichnung = value
        '            RaiseEvent Bezeichnung_Changed(_bezeichnung)
        '        End If
        '    End Set
        'End Property
        Property Identities As Dictionary(Of String, String) ' WK, Bez1
            Get
                Return _identities
            End Get
            Set(value As Dictionary(Of String, String))
                If Not value.Equals(_identities) Then
                    _identities = value
                    If value.Count > 0 Then RaiseEvent Bezeichnung_Changed(Join(_identities.Values.ToArray, ", "))
                End If
            End Set
        End Property
        Property GUID As String
        Property Kurz As String
        Property Datum As Date
        Property KR(Bohle As Integer) As Integer
            Get
                Return _kr(Bohle)
            End Get
            Set(value As Integer)
                If value <> _kr(Bohle) Then
                    _kr(Bohle) = value
                    WA_Message.Status = 1
                End If
            End Set
        End Property
        Property Steigerung(Index As Integer) As Integer
            Get
                Return _steigerung(Index)
            End Get
            Set(value As Integer)
                _steigerung(Index) = value
            End Set
        End Property
        Property Mannschaft As Boolean ' Liga-WK
        Property BigDisc As Boolean
        Property Flagge As Boolean
        Property ShowHantel As Boolean
        Property WK_Modus As Integer ' Wettkampf-Modus (Easy/Normal)
            Get
                Return _wkmodus
            End Get
            Set(value As Integer)
                If Not _wkmodus.Equals(value) Then
                    _wkmodus = value
                    RaiseEvent WKModus_Changed()
                End If
            End Set
        End Property
        Property Hantelstange As String
        Property AKs(Sex As String) As Integer ' Masters-AKs / GKs bei DGJ
            Get
                If Sex = "m" Then
                    Return _ak(0)
                Else
                    Return _ak(1)
                End If
            End Get
            Set(value As Integer)
                If Sex = "m" Then
                    _ak(0) = value
                Else
                    _ak(1) = value
                End If
            End Set
        End Property
        Property MinLast(Sex As String, Index As Integer) As Integer ' Sex = zk/m/w; Index: Reißen=0, Stoßen=1
            Get
                If Sex = "m" Then
                    If Index = 0 Then
                        Return _minlast(0, 0)
                    Else
                        Return _minlast(0, 1)
                    End If
                Else
                    If Index = 0 Then
                        Return _minlast(1, 0)
                    Else
                        Return _minlast(1, 1)
                    End If
                End If
            End Get
            Set(value As Integer)
                If Sex = "m" Then
                    If Index = 0 Then
                        _minlast(0, 0) = value
                    Else
                        _minlast(0, 1) = value
                    End If
                Else
                    If Index = 0 Then
                        _minlast(1, 0) = value
                    Else
                        _minlast(1, 1) = value
                    End If
                End If
            End Set
        End Property
        Property ZKLast(Sex As String) As Integer
            Get
                If Sex = "m" Then
                    Return _zklast(0)
                Else
                    Return _zklast(1)
                End If
            End Get
            Set(value As Integer)
                If Sex = "m" Then
                    _zklast(0) = value
                Else
                    _zklast(1) = value
                End If
            End Set
        End Property
        Property Technikwertung As Boolean
            Get
                Return _technik
            End Get
            Set(value As Boolean)
                'If Not _technik = value Then
                _technik = value
                RaiseEvent Technik_Changed(_technik)
                'End If
            End Set
        End Property
        Property Athletik As Boolean
        Property Modus As Integer
        Property IsSinclairWertung As Boolean
        Property IsSinclairWertung2 As Boolean
        Property IsRobiWertung As Boolean
        Property IsRobiWertung2 As Boolean
        Property Alterseinteilung As String ' Alterseinteilung aus Modus (Jahrgang, Altersklassen, Altersgruppen, "")
        Property Gewichtseinteilung As String ' Gewichtseinteilung aus Modus (Gewichtsklassen, Gewichtsgruppen, "")
        Property Regel20 As Boolean
        Property International As Boolean
            Get
                Return _international
            End Get
            Set(value As Boolean)
                _international = value
                If value Then Ansicht_Options.NameFormat = NameFormat.International
            End Set
        End Property

        'Property nurZK As Boolean
        Property IncreaseAfterNoLift As Boolean ' Steigerung nach Ungültig
        Property AutoLosnummern As Boolean
        Property Platzierung As Integer ' Berechnung der Platzierung: 0 = jede Gruppe (Default), 1 = gleiche GK, 2 = WK, 3 = WK + 2.Woman
        Property ThirdAttempt As Boolean
        Property OrderByLifter As Boolean
        Property Finale As Boolean
        Property ShareTeam As Integer
        Property SameAK As Integer
        Property UseAgeGroups As Boolean
        Property MultiBohle As Boolean
            Get
                Return _multibohle
            End Get
            Set(value As Boolean)
                _multibohle = value
                RaiseEvent MultiBohle_Changed()
            End Set
        End Property
        Property Wertungsfaktor(Sex As String) As Double
            Get
                If Sex = "m" Then
                    Return _wertungsfaktor(0)
                Else
                    Return _wertungsfaktor(1)
                End If
            End Get
            Set(value As Double)
                If Sex = "m" Then
                    _wertungsfaktor(0) = value
                Else
                    _wertungsfaktor(1) = value
                End If
            End Set
        End Property
        Property IgnoreSex As Boolean

        Sub Clear()
            ID = 0
            GUID = String.Empty
            Kurz = String.Empty
            Bezeichnung = String.Empty
            Datum = Nothing
            KR(1) = 0
            KR(2) = 0
            'Bohle = 0
            Steigerung(0) = 0
            Steigerung(1) = 0
            'SettingsChanged = False
            Mannschaft = False
            BigDisc = False
            Flagge = False
            ShowHantel = True
            WK_Modus = 0
            Hantelstange = String.Empty
            AKs("w") = 0
            AKs("m") = 0
            MinLast("w", 0) = 0
            MinLast("w", 1) = 0
            MinLast("m", 0) = 0
            MinLast("m", 1) = 0
            ZKLast("m") = 0
            ZKLast("w") = 0
            MinLast("zk", 0) = 0
            MinLast("zk", 1) = 0
            Technikwertung = False
            Athletik = False
            Modus = 0
            IsSinclairWertung = False
            IsSinclairWertung2 = False
            IsRobiWertung = False
            IsRobiWertung2 = False
            Alterseinteilung = String.Empty
            Gewichtseinteilung = String.Empty
            Regel20 = False
            International = False
            'nurZK = False
            IncreaseAfterNoLift = True
            AutoLosnummern = False
            Platzierung = 0
            ThirdAttempt = False
            OrderByLifter = False
            Finale = False
            ShareTeam = 0
            Identities = New Dictionary(Of String, String)
            SameAK = 0
            UseAgeGroups = False
            Wertungsfaktor("m") = 1
            Wertungsfaktor("w") = 1
            IgnoreSex = False
        End Sub

        Public Function WhereClause(Optional prefix As String = "", Optional WKs As Object() = Nothing) As String

            If prefix.Length > 0 Then
                prefix = Split(prefix, ".")(0) & "." ' falls Prefix mit "." angegeben wurde
            End If

            If Not IsNothing(WKs) AndAlso WKs.Length > 0 Then
                Return " " & prefix & "Wettkampf IN (" & Join(WKs, ",") & ") "
                'Return "(" & prefix & "Wettkampf = " & Join(WKs, " or " & prefix & "Wettkampf = ") & ") "
            ElseIf IsNothing(Identities) OrElse Identities.Count = 0 Then
                Return prefix & "Wettkampf = " & ID & " "
            Else
                Return " " & prefix & "Wettkampf IN (" & Join(Identities.Keys.ToArray, ",") & ") "
                'Return "(" & prefix & "Wettkampf = " & Join(Identities.Keys.ToArray, " or " & prefix & "Wettkampf = ") & ") "
            End If
        End Function

        Public Function Set_Properties(form As Form, WK_ID As String) As Integer
            ' Properties für Wettkampf setzen

            If WK_ID = "0" Then Return 0

            'Dim wk As DataTable = Nothing
            'Dim w2 As DataTable = Nothing

            Dim IsSinclair As Boolean = False
            Dim IsRobi As Boolean = False

            Clear()

            Using conn As New MySqlConnection(User.ConnString)
                Do
                    Try
                        If Not conn.State = ConnectionState.Open Then conn.Open()

                        Using ds As New DataService
                            Identities = ds.Get_Wettkampf_Identities(WK_ID, form, conn)
                            MultiBohle = ds.Get_MultiBohle(conn)
                        End Using
                        ID = CInt(Identities.Keys(0))

                        'Dim Query = "select w.Wettkampf, w.GUID, w.Modus, m.Wertung, w.Platzierung, w.Datum, w.Kurz, w.Bigdisc, w.Kampfrichter, w.Mannschaft, w.Flagge, w.Finale, w.SameAK, 
                        '                b.Bez1, 
                        '                d.ZK_m, d.ZK_w, d.R_m, d.S_m, d.R_w, d.S_w, d.Hantelstange, d.Steigerung1, d.Steigerung2, d.AKs_m, d.AKs_w, d.Regel20, d.International, d.nurZK, d.AutoLosnummer, d.ThirdAttempt, d.OrderByLifter, 
                        '                m.Technikwertung, m.Athletik, m.Alterseinteilung, m.Gewichtseinteilung, m.IncAfterNoLift 
                        '             from wettkampf w 
                        '             left join wettkampf_bezeichnung b on w.Wettkampf = b.Wettkampf 
                        '             left join wettkampf_details d on w.Wettkampf = d.Wettkampf 
                        '             Left join modus m on w.Modus = m.idModus 
                        '             where w.Wettkampf = " & WK_ID & ";"
                        Dim Query = "select w.*, b.Bez1, d.*, 
                                        m.Wertung, m.Technikwertung, m.Athletik, m.Alterseinteilung, m.Gewichtseinteilung, m.IncAfterNoLift 
                                     from wettkampf w 
                                     left join wettkampf_bezeichnung b on w.Wettkampf = b.Wettkampf 
                                     left join wettkampf_details d on w.Wettkampf = d.Wettkampf 
                                     Left join modus m on w.Modus = m.idModus 
                                     where w.Wettkampf = " & ID & ";"
                        Dim cmd = New MySqlCommand(Query, conn)
                        Dim wk As New DataTable
                        wk.Load(cmd.ExecuteReader)

                        Query = "SELECT count(*) 
                                FROM wettkampf_wertung
                                where Wettkampf = " & ID & " and Wertung2 between 3 and 4;"
                        cmd = New MySqlCommand(Query, conn)
                        Dim reader = cmd.ExecuteReader
                        If reader.HasRows Then
                            reader.Read()
                            IsSinclair = CInt(reader("count(*)")) > 0
                        End If
                        reader.Close()
                        Query = "SELECT count(*) 
                                FROM wettkampf_wertung
                                where Wettkampf = " & ID & " and Wertung2 = 1;"
                        cmd = New MySqlCommand(Query, conn)
                        reader = cmd.ExecuteReader
                        If reader.HasRows Then
                            reader.Read()
                            IsRobi = CInt(reader("count(*)")) > 0
                        End If
                        reader.Close()

                        GUID = wk(0)!GUID.ToString
                        Kurz = wk(0)!Kurz.ToString
                        Bezeichnung = wk(0)!Bez1.ToString
                        Datum = CDate(wk(0)!Datum)
                        If Not (IsDBNull(wk(0)!Kampfrichter) OrElse String.IsNullOrEmpty(wk(0)!Kampfrichter.ToString)) Then ' Kampfrichter den Bohlen zuordnen
                            Dim KRs = Split(wk(0)!Kampfrichter.ToString(), ", ")
                            KR(1) = CInt(KRs(0))
                            KR(2) = CInt(KRs(KRs.Count - 1))
                        Else
                            KR(1) = 1
                            KR(2) = 1
                        End If
                        Steigerung(0) = CInt(wk(0)!Steigerung1)
                        Steigerung(1) = CInt(wk(0)!Steigerung2)
                        BigDisc = CBool(wk(0)!BigDisc)
                        Flagge = CBool(wk(0)!Flagge)
                        Hantelstange = wk(0)!Hantelstange.ToString
                        AKs("w") = CInt(wk(0)!Aks_w)
                        AKs("m") = CInt(wk(0)!Aks_m)
                        MinLast("w", 0) = CInt(wk(0)!R_w)
                        MinLast("w", 1) = CInt(wk(0)!S_w)
                        MinLast("m", 0) = CInt(wk(0)!R_m)
                        MinLast("m", 1) = CInt(wk(0)!S_m)
                        ZKLast("m") = CInt(wk(0)!ZK_w)
                        ZKLast("w") = CInt(wk(0)!ZK_w)
                        Technikwertung = CBool(wk(0)!Technikwertung)
                        Athletik = CBool(wk(0)!Athletik)
                        Modus = CInt(wk(0)!Modus)
                        IsSinclairWertung = CInt(wk(0)!Wertung) = 3 Or CInt(wk(0)!Wertung) = 4
                        IsSinclairWertung2 = wk(0)!PLatzierung.ToString.Length = 3 AndAlso
                            ((wk(0)!PLatzierung.ToString.Substring(2, 1) = "3" Or wk(0)!PLatzierung.ToString.Substring(2, 1) = "4") OrElse IsSinclair)
                        IsRobiWertung = CInt(wk(0)!Wertung) = 1
                        IsRobiWertung2 = wk(0)!PLatzierung.ToString.Length = 3 AndAlso
                            (wk(0)!PLatzierung.ToString.Substring(2, 1) = "1" OrElse IsRobi)
                        Alterseinteilung = wk(0)!Alterseinteilung.ToString
                        Gewichtseinteilung = wk(0)!Gewichtseinteilung.ToString
                        Regel20 = CBool(wk(0)!Regel20)
                        International = CBool(wk(0)!International)
                        'nurZK = CBool(wk(0)!nurZK)
                        Mannschaft = CBool(wk(0)!Mannschaft) ' == Liga
                        IncreaseAfterNoLift = CBool(wk(0)!IncAfterNoLift)
                        AutoLosnummern = CBool(wk(0)!AutoLosnummer)
                        Platzierung = CInt(wk(0)!Platzierung)
                        ThirdAttempt = CBool(wk(0)!ThirdAttempt)
                        OrderByLifter = CBool(wk(0)!OrderByLifter)
                        Finale = CBool(wk(0)!Finale)
                        '  Identities = Nothing ' New Dictionary(Of String, String)
                        'Using ds As New DataService
                        '    Wettkampf.Identities = ds.Get_Wettkampf_Identities(wk(0)!Wettkampf.ToString, form, conn)
                        '    Wettkampf.MultiBohle = ds.Get_MultiBohle(conn)
                        'End Using
                        'ID = CInt(Identities.AsEnumerable(0).Key)
                        If Not IsDBNull(wk(0)!SameAK) Then SameAK = CInt(wk(0)!SameAK)
                        UseAgeGroups = CBool(wk(0)!UseAgeGroups)
                        Wertungsfaktor("m") = CDbl(wk(0)!Faktor_m)
                        Wertungsfaktor("w") = CDbl(wk(0)!Faktor_w)
                        IgnoreSex = CBool(wk(0)!IgnoreSex)

                        ConnError = False
                        Return ID 'CInt(Identities.AsEnumerable(0).Key)
                    Catch ex As MySqlException
                        If MySQl_Error(form, ex.Message, ex.StackTrace) = DialogResult.Cancel Then Exit Do
                    End Try
                Loop While ConnError
            End Using

            Return 0
        End Function

    End Class

    Class myPad ' KR_Pad-Struktur wird auch in frmLeader verwendet, muss also auch bei späteren Versionen von KR-Pads erhalten bleiben
        Private _index As Integer
        Property Index As Integer
            Get
                Return _index
            End Get
            Set(value As Integer)
                _index = value
                Name = "KR-Pad " & _index
            End Set
        End Property
        Property AufrufZeit As Date 'Countdown der gestoppten Uhr 
        Property Wertung As Integer 'Gültig=5, Ungültig=7
        Property Techniknote As String
        Property WertungZeit As Date
        Property TechnikZeit As Date
        Property Connected As Boolean
        Property Verified As Boolean
        Property Name As String = "KR-Pad"

    End Class

    Class myTPad
        Public Event Change_btnAufruf_Enabled(SetEnabled As Boolean)

        Private _connected As Boolean
        Property Connected As Boolean ' ZN_Pad angeschlossen
            Get
                Return _connected
            End Get
            Set(value As Boolean)
                _connected = value
                RaiseEvent Change_btnAufruf_Enabled(Not _connected)
            End Set
        End Property
        Property CountDown As Integer ' Countdown-Zeit in Sekunden
        Property IsAttemptRated As Boolean ' wenn True hat min. 1 KR gewertet
        Property Name As String = "ZN-Pad"
    End Class

    Class myZA
        Property Name As String = "Ab-Zeichen"
        Property Connected As Boolean
    End Class

    Enum DevType
        None
        Raspberry
        ControlUnit
    End Enum
    Class myJB
        Property Name As String = "Jury-Board"
        Property Connected As Boolean
        Property RevPressed As Boolean
        Property KR_Pressed As Boolean() = {False, False, False, False}
        Property Device As DevType
    End Class

    Public WA_Message As New myWA_Message
    Public Class myWA_Message
        Private _status As Integer
        Public Event Status_Changed(Status As Integer, Msg As String) ' Event zum Ändern der StatusMessages
        Public Event Initialize_KR()
        'Public Sub Update(Optional Info As String = "", Optional Get_Status As Boolean = True)

        Private Function Get_NoDevices(Optional CheckPads As Boolean = True) As List(Of String)()

            Dim NoDevices As New List(Of String)
            Dim NoPads As New List(Of String)

            If CheckPads And Not IsNothing(KR_Pad) Then
                NoPads = (From pad In KR_Pad
                          Where Not pad.Value.Connected
                          Order By pad.Key
                          Select pad.Value.Name.Split(CChar(" "))(0) & " " & pad.Key).ToList
                NoDevices.AddRange(NoPads)
            End If

            If Not IsNothing(ZN_Pad) AndAlso Not ZN_Pad.Connected Then NoDevices.Add(ZN_Pad.Name)
            If Not IsNothing(JB_Dev) AndAlso Not JB_Dev.Connected Then NoDevices.Add(JB_Dev.Name)
            If Not IsNothing(ZA_Dev) AndAlso Not ZA_Dev.Connected Then NoDevices.Add(ZA_Dev.Name)

            Return {NoDevices, NoPads}

        End Function
        Property Status As Integer
            Get
                Return _status
            End Get
            Set(value As Integer)
                If _status.Equals(value) Then Return
                Dim msg = String.Empty
                Try
                    Select Case value
                        Case 0
                            msg = "keine Wertungsanlage, Wertung am Computer"
                        Case 1
                            Dim Dev = 0, Pad = 1 ' all Devices, Pads
                            If Not IsNothing(KR_Pad) Then
                                Dim NoDevices = Get_NoDevices()
                                Dim Pads = (From _pad In KR_Pad
                                            Where _pad.Value.Connected
                                            Order By _pad.Key
                                            Select _pad.Value.Name).ToList
                                If Pads.Count = 2 Then
                                    msg = "Wertung mit 2 KR-Pads nicht möglich"
                                    value = 2
                                ElseIf Pads.Count > 0 Then
                                    'Dim NoPads = (From pad In NoDevices Where pad.Split(CChar(" "))(0).Equals("KR-Pad")).Count
                                    If Wettkampf.KR(User.Bohle) + NoDevices(Pad).Count = 3 Then
                                        If NoDevices(Dev).Count - NoDevices(Pad).Count > 0 Then
                                            For Each p In NoDevices(Pad)
                                                NoDevices(Dev).Remove(p)
                                            Next
                                            msg = "kein " + Join(NoDevices(Dev).ToArray, ", ")
                                        Else
                                            msg = "Wertungsanlage bereit"
                                        End If
                                    Else
                                        Wettkampf.KR(User.Bohle) = Pads.Count
                                    End If
                                Else
                                    If NoDevices(Dev).Count - NoDevices(Pad).Count = 3 Then
                                        msg = "keine Wertungsanlage"
                                    Else
                                        msg = "kein " + Join(NoDevices(Dev).ToArray, ", ")
                                    End If
                                    value = 0
                                End If
                            Else
                                msg = "keine Wertungsanlage"
                                value = 0
                            End If
                        Case 1000
                            msg = "Wertungsanlage initialisieren"
                        Case 2000
                            ' Manuelle Wertung ausgewählt
                            Dim NoDevices = Get_NoDevices(False)
                            If NoDevices(0).Count < 3 Then
                                msg = "kein " + Join(NoDevices(0).ToArray, ", ") + ", "
                            ElseIf NoDevices(0).Count = 3 Then
                                msg = "keine Wertungsanlage, "
                                value = 0
                            End If
                            msg += vbNewLine & "Wertung am Computer"
                        Case 3000
                            msg = "Gerät entfernt"
                        Case Else
                            Return
                    End Select

                    _status = value
                    RaiseEvent Status_Changed(_status, msg)

                Catch ex As Exception
                    LogMessage("Module: Class myWA_Message: Status: " & ex.Message, ex.StackTrace)
                End Try
            End Set
        End Property
    End Class

    'Public Const Gültig = 5
    'Public Const Ungültig = 7

    '' Wertungsanzeige
    'Public Anzeige As New myAnzeige
    Public Enum Clock_Status
        Waiting
        Started
        Stopped
    End Enum
    Public Class myAnzeige
        Implements IDisposable

        'Public Event Stunden_Changed(ByVal Stunden As String)
        'Public Event DplPunkt_Changed(ByVal DplPunkt As String)
        'Public Event Minuten_Changed(ByVal Minuten As String)
        'Public Event Sekunden_Changed(ByVal Sekunden As String)

        Public Event CountDown_Changed(ByVal Value As String) ' Uhr in Publikum

        Public Event ClockColor_Changed(ByVal Color As Color)
        'Public Event Clock_Stopped(ByVal State As Boolean)
        'Public Event Gültig_Changed(ByVal Gültig As Color, Index As Integer)
        'Public Event Ungültig_Changed(ByVal Ungültig As Color, Index As Integer)
        Public Event Technik_Wert_Changed(ByVal T_Wert As String)
        Public Event Technik_Color_Changed(ByVal T_Farbe As Color)
        Public Event Technik_Visible_Changed(ByVal T_Shown As Boolean)

        Public Clock_Colors As List(Of Color)
        Public ScoreColors As Dictionary(Of Integer, Color)
        Public LampeColor As Dictionary(Of Integer, Color)
        Public TechnikColors As Dictionary(Of Integer, Color)

        Public Sub New()

            Clock_Colors = New List(Of Color)
            Clock_Colors.AddRange({Color.WhiteSmoke, Color.LimeGreen, Color.Red})

            ClockColor = Clock_Colors(Clock_Status.Waiting)
            TmpClockColor = Clock_Colors(Clock_Status.Waiting)

            ScoreColors = New Dictionary(Of Integer, Color)
            'ScoreColors.Add(-2, Ansicht_Options.Shade_Color)
            ScoreColors.Add(-1, Ansicht_Options.Shade_Color) 'Color.Red)
            'ScoreColors.Add(1, Color.White)
            ScoreColors.Add(0, Ansicht_Options.Shade_Color)
            ScoreColors.Add(5, Color.White)
            ScoreColors.Add(7, Color.Red)

            LampeColor = New Dictionary(Of Integer, Color)
            LampeColor.Add(1, Ansicht_Options.Shade_Color)
            LampeColor.Add(2, Ansicht_Options.Shade_Color)
            LampeColor.Add(3, Ansicht_Options.Shade_Color)

            TechnikColors = New Dictionary(Of Integer, Color)
            TechnikColors.Add(-2, Ansicht_Options.Shade_Color)
            TechnikColors.Add(-1, Ansicht_Options.Shade_Color)
            TechnikColors.Add(0, Ansicht_Options.Shade_Color)
            TechnikColors.Add(1, Color.Lime)

            AddHandler Ansicht_Options.ShadeChanged, AddressOf shadeChanged
        End Sub

#Region "IDisposable Support"
        Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
                End If

                ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
                ' TODO: große Felder auf Null setzen.

                RemoveHandler Ansicht_Options.ShadeChanged, AddressOf shadeChanged

            End If
            disposedValue = True
        End Sub

        ' TODO: Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
        'Protected Overrides Sub Finalize()
        '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
            Dispose(True)
            ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
            ' GC.SuppressFinalize(Me)
        End Sub
#End Region
        Private Sub shadeChanged()
            ScoreColors(-1) = Ansicht_Options.Shade_Color
            ScoreColors(0) = Ansicht_Options.Shade_Color
        End Sub

        Private _Countdown As String
        'Private _Std As String = "0"
        'Private _dpp As String
        'Private _Min As String = "0"
        'Private _Sek As String = "00"
        Private _Color As Color
        'Private _stop As Boolean

        Property CountDown As String
            Get
                Return _Countdown
            End Get
            Set(value As String)
                _Countdown = value
                RaiseEvent CountDown_Changed(_Countdown)
            End Set
        End Property

        Property ClockColor As Color
            Get
                Return _Color
            End Get
            Set(value As Color)
                If Not _Color = value Then
                    _Color = value
                    RaiseEvent ClockColor_Changed(_Color)
                End If
            End Set
        End Property
        Property TmpClockColor As Color

        Public Event Lampe_Changed(Index As Integer, Color As Color)
        Public Event Lampe_Changed_Sofort(Index As Integer, Color As Color)

        Public Sub Change_Lampe(Index As Integer, Color As Color)
            RaiseEvent Lampe_Changed(Index, Color)
        End Sub
        Public Sub Change_Lampe_Sofort(Index As Integer, Color As Color)
            RaiseEvent Lampe_Changed_Sofort(Index, Color)
        End Sub

        'Public Event Wertung_Changed(Index As Integer, Wertung As Integer)
        'Public Sub Change_Wertung(Index As Integer, Wertung As Integer)
        '    RaiseEvent Wertung_Changed(Index, Wertung)
        'End Sub

        Private t_note As String
        Private t_color As Color
        Private t_visible As Boolean
        Property T_Wert As String
            Get
                Return t_note
            End Get
            Set(value As String)
                If Not t_note = value Then
                    t_note = value
                    RaiseEvent Technik_Wert_Changed(t_note)
                End If
            End Set
        End Property
        Property T_Farbe As Color
            Get
                Return t_color
            End Get
            Set(value As Color)
                If Not t_color = value Then
                    t_color = value
                    RaiseEvent Technik_Color_Changed(t_color)
                End If
            End Set
        End Property
        Property T_Shown As Boolean
            Get
                Return t_visible
            End Get
            Set(value As Boolean)
                t_visible = value
                RaiseEvent Technik_Visible_Changed(t_visible)
            End Set
        End Property

    End Class

    '' WK-Ablauf
    Public Leader As New myLeader
    Public Enum InfoTarget
        All
        Moderator
        NotPublic
    End Enum

    Class myLeader

        Public Event Mark_Rows(Zeile As Integer)
        Public Event Dim_Rows(Zeile As Integer)
        Public Event Gruppe_Changed(Gruppe As List(Of String))
        Public Event Durchgang_Changed(Durchgang As String)
        Public Event Scoring_Changed(Row As Integer, Col As String, HLast As Integer, Wertung As Integer, Note As Double, ZK As String, IsCurrent As Boolean) ', Refreshing As Boolean) ', nCol As String, Refreshing As Boolean)

        'Public Event Wertung_Changed(Index As Integer, Color As Color) ' Wertung für Bohle, Aufwärmen

        Public Event NächsterHeber(CountDown As Integer)
        ' Public Event Korrektur(Lampen As String)
        'Public Event Edit_Hantellast(Hantellast As Integer) ' schreibt Hantellast in Bohle
        'Public Event Score_Changed(Zeile As Integer, Platz As Integer)
        'Public Event Set_Hantel(Stange As String, Last As Integer) ' aktualisiert frmHantel
        Public Event Team_Result(Team As Integer) ' neue Wertung für Team in Mannschafts-WK
        Public Event Notiz_Changed(Id As Integer) ' neue rtfNotiz an Moderator mit Id=Teilnehmer
        Public Event InfoMessage_Changed(Info As String, Target As Integer) ' neue Last an Moderator

        'Public Event Pause_Changed(active As Boolean)

        Public Event NextHeber_Mark(Position As List(Of KeyValuePair(Of Integer, String))) ' markiert die nächsten Versuche in Publikum
        Public Event Set_AV()
        Public Event Change_AV(HLast As Integer, Row As Integer, Col As String, Declared As Boolean?, Panik As Boolean)
        Public Event Call_Aufruf(Status As Boolean?, Msg As String, Pause As Boolean)

        Public Event MasterData_Changed(pos As Integer, Nachname As String, Vorname As String, Sex As String, Verein As String, Jahrgang As Integer, AK As String)

        Public Event Teamwertung_Changed() 'Reißen As Boolean) ' rechnet Punktestand im Mannschafts-WK
        Public Event Change_Lifter(TN As Integer, Grp As Integer) ' Mannschafts-WK: Heber einwechseln

        Public Event Techniknote_Changed(Index As Integer, Note As String)
        Public Event OutOfRange_Changed(R1 As Boolean, R2 As Boolean, R3 As Boolean)
        'Public Event Ab_Zeichen(Wertung As Integer)
        Public Event Jury_Entscheidung(Info As String)

        Public Event Blockheben_Changed(Blockheben As Boolean)

        Public Property RowNum As Integer
        Public Table As String
        Public TableIx As Integer

        Public Event Add_Rows()
        Public Sub RowsAdded()
            RaiseEvent Add_Rows()
        End Sub

        Public Sub Change_JuryEntscheid(Info As String) ' in Change_InfoMessage integrieren
            RaiseEvent Jury_Entscheidung(Info)
        End Sub

        'Public Sub Change_AbZeichen(Wertung As Integer)
        '    RaiseEvent Ab_Zeichen(Wertung)
        'End Sub
        Public Sub Change_OutOfRange(R1 As Boolean, R2 As Boolean, R3 As Boolean)
            RaiseEvent OutOfRange_Changed(R1, R2, R3)
        End Sub
        Public Sub Change_Techniknote(Index As Integer, Note As String)
            RaiseEvent Techniknote_Changed(Index, Note)
        End Sub
        Public Sub Change_MasterData(pos As Integer, Nachname As String, Vorname As String, Sex As String, Verein As String, Jahrgang As Integer, AK As String)
            RaiseEvent MasterData_Changed(pos, Nachname, Vorname, Sex, Verein, Jahrgang, AK)
        End Sub
        Public Sub Set_Notiz(Id As Integer)
            RaiseEvent Notiz_Changed(Id)
        End Sub
        Public Sub Change_InfoMessage(Info As String, Optional Target As Integer = InfoTarget.All)
            RaiseEvent InfoMessage_Changed(Info, Target)
        End Sub
        Public Property ManuelleWertung As Boolean

        Private H_Zeile As Integer
        Property Highlight As Integer
            Get
                Return H_Zeile
            End Get
            Set(value As Integer)
                H_Zeile = value
                RaiseEvent Mark_Rows(H_Zeile)
            End Set
        End Property

        Private D_Zeile As Integer
        Property DimOut As Integer
            Get
                Return D_Zeile
            End Get
            Set(value As Integer)
                D_Zeile = value
                RaiseEvent Dim_Rows(D_Zeile)
            End Set
        End Property
        Property Gruppe As List(Of String) ' Liste, weil mehrere Gruppen in einer Veranstaltung zusammengefasst werden können
        Public Sub Change_Gruppe() 'NewGroup As List(Of String))
            RaiseEvent Gruppe_Changed(Gruppe)
        End Sub

        Private _dg As String
        Property Durchgang As String
            Get
                Return _dg
            End Get
            Set(value As String)
                _dg = value
                RaiseEvent Durchgang_Changed(_dg)
            End Set
        End Property
        Property Row As Integer
        Property Col As String

        Private _countdown As Integer
        Property CountDown As Integer
            Get
                Return _countdown
            End Get
            Set(value As Integer)
                _countdown = value
                RaiseEvent NächsterHeber(_countdown)
            End Set
        End Property

        ' Pause *********************************
        Private _IsPause As Boolean
        'Private _Time As Integer
        Private _AdsVisible As Boolean

        Public Event Pause_Changed(Start As Boolean)
        Public Event AdsVisible_Changed(Value As Boolean)
        Property IsPause As Boolean
            Get
                Return _IsPause
            End Get
            Set(value As Boolean)
                _IsPause = value
                If Not _IsPause Then Pause_Countdown = 0
                RaiseEvent Pause_Changed(_IsPause)
            End Set
        End Property
        Property Pause_Countdown As Integer
        Property AdsVisible As Boolean
            Get
                Return _AdsVisible
            End Get
            Set(value As Boolean)
                _AdsVisible = value
                RaiseEvent AdsVisible_Changed(_AdsVisible)
            End Set
        End Property
        ' End Pause *******************************

        Private _ButtonState As New Dictionary(Of String, Boolean)
        Public Sub Get_ButtonState()
            If Not IsNothing(formLeader) AndAlso Not formLeader.IsDisposed Then
                With formLeader
                    _ButtonState("Next") = .btnNext.Enabled
                    _ButtonState("Start") = .btnAufruf.Enabled
                End With
            End If
        End Sub
        Public Sub Set_ButtonState(Optional Disable As Boolean = False)
            If Not IsNothing(formLeader) AndAlso Not formLeader.IsDisposed Then
                With formLeader
                    With .btnNext
                        .Enabled = Not Disable AndAlso _ButtonState("Next")
                    End With
                    With .btnAufruf
                        .Enabled = Not Disable AndAlso _ButtonState("Start")
                    End With
                End With
            End If
        End Sub

        'WriteOnly Property SortChanged As Boolean
        '    Set(value As Boolean)
        '        If value = True Then RaiseEvent Sort_Changed(Leader.RowNum)
        '    End Set
        'End Property

        Property MaxCalledLifters As Integer = 2 ' in Optionen anlegen
        Property RowNum_Offset As Integer = 0 ' Anzahl der Zeilen, um die Leader.RowNum bei "Manueller Aufruf" verschoben wird

        Private _Blockheben As Boolean
        Property Blockheben As Boolean
            Get
                Return _Blockheben
            End Get
            Set(value As Boolean)
                _Blockheben = value
                RaiseEvent Blockheben_Changed(value)
            End Set
        End Property

        Public Sub Mark_CalledLifters(LiftersList As List(Of KeyValuePair(Of Integer, String)))
            If IsNothing(LiftersList) Then
                RaiseEvent NextHeber_Mark(Nothing)
            Else
                Dim Positions = Get_Called_Lifters()
                RaiseEvent NextHeber_Mark(Positions)
            End If
        End Sub

        Public Function Get_Called_Lifters() As List(Of KeyValuePair(Of Integer, String))
            ' aufzurufende Versuche in Publikum auslesen

            Dim row As Integer
            Dim col As String
            Dim Positions As New List(Of KeyValuePair(Of Integer, String))

            If Leader.RowNum = 0 AndAlso Heber.Id <> CInt(dvLeader(Leader.TableIx)(0)("Teilnehmer")) Then
                ' Heber nicht mehr in Zeile 0
                Try
                    row = bsResults.Find("Teilnehmer", Heber.Id)
                    col = UCase(Left(Leader.Table, 1)) + "_" + Heber.Versuch.ToString
                    Positions.Add(New KeyValuePair(Of Integer, String)(row, col))
                Catch ex As Exception
                End Try
            End If

            Try
                Dim i = Leader.RowNum + Leader.RowNum_Offset
                Do While Positions.Count < MaxCalledLifters
                    If Not IsDBNull(dvLeader(Leader.TableIx)(i)("Reihenfolge")) AndAlso
                                Positions.Count = 0 OrElse Not CInt(dvLeader(Leader.TableIx)(i)("Teilnehmer")) = CInt(dvLeader(Leader.TableIx)(Leader.RowNum + Leader.RowNum_Offset)("Teilnehmer")) Then
                        row = bsResults.Find("Teilnehmer", dvLeader(Leader.TableIx)(i + Leader.RowNum)("Teilnehmer"))
                        col = UCase(Left(Leader.Table, 1)) + "_" + dvLeader(Leader.TableIx)(i + Leader.RowNum)("Versuch").ToString
                        Positions.Add(New KeyValuePair(Of Integer, String)(row, col))
                    End If
                    i += 1
                Loop
            Catch ex As Exception
            End Try

            Return Positions

        End Function

        Public Sub Write_Scoring(Row As Integer,
                                 Col As String,
                                 Optional HLast As Integer = -1,
                                 Optional Wertung As Integer = 0,
                                 Optional Note As Double = 0,
                                 Optional ZK As String = "",
                                 Optional IsCurrent As Boolean = True)
            RaiseEvent Scoring_Changed(Row, Col, HLast, Wertung, Note, ZK, IsCurrent)
        End Sub
        'Public Sub Change_Wertung(Index As Integer, Color As Color)
        '    RaiseEvent Wertung_Changed(Index, Color)
        'End Sub
        'Public Sub Change_Korrektur(Lampen As String)
        '    RaiseEvent Korrektur(Lampen)
        'End Sub
        'Public Sub Edit_HLast(Hantellast As Integer)
        '    RaiseEvent Edit_Hantellast(Hantellast)
        'End Sub
        'Public Sub Update_Hantel(Stange As String, Last As Integer)
        '    RaiseEvent Set_Hantel(Stange, Last)
        'End Sub

        'Public Sub Set_Score(Zeile As Integer, Platz As Integer)
        '    RaiseEvent Score_Changed(Zeile, Platz)
        'End Sub
        Public Sub New_TeamResult(Team As Integer)
            RaiseEvent Team_Result(Team)
        End Sub
        Public Sub New_TeamWertung() 'Reißen As Boolean)
            RaiseEvent Teamwertung_Changed() 'Reißen)
        End Sub
        Public Sub Set_Anfangslast()
            RaiseEvent Set_AV()
        End Sub
        Public Sub Change_Anfangslast(HLast As Integer, Row As Integer, Col As String, Optional Declared As Boolean? = False, Optional Panik As Boolean = False)
            RaiseEvent Change_AV(HLast, Row, Col, Declared, Panik)
        End Sub
        Public Sub Change_Aufruf(Status As Boolean?, Optional Msg As String = "", Optional Pause As Boolean = False)
            RaiseEvent Call_Aufruf(Status, Msg, Pause)
        End Sub
        Public Sub Exchange_Lifter(TN As Integer, Grp As Integer)
            RaiseEvent Change_Lifter(TN, Grp)
        End Sub
    End Class

    Public Heber As New myHeber
    'Public Heber_Prev As New myHeber
    Public Class myHeber
        Private _ix As Integer
        Private _verein As String
        Private _land As String
        Private _flagge As String
        Private _ak As String
        Private _gk As String
        Private _versuch As Integer
        Private _hantellast As Integer
        Private _startnummer As String
        'Private _wertung As Integer
        'Private _note As Double

        Public Event Id_Changed(visible As Boolean)
        Public Sub Set_Name(Nachname As String, Vorname As String)
            RaiseEvent Name_Changed(Nachname, Vorname)
        End Sub
        Public Event Name_Changed(Nachname As String, Vorname As String)
        Public Event Verein_Changed(Verein As String)
        Public Event Land_Changed(Land As String)
        Public Event Flagge_Changed(Flagge As String)
        Public Event AK_Changed(AK As String)
        Public Event GK_Changed(GK As String)
        Public Event Versuch_Changed(Versuch As String)
        Public Event Hantellast_Changed()
        Public Event Startnummer_Changed(Startnummer As String)

        Property Id As Integer
            Get
                Return _ix
            End Get
            Set(ByVal value As Integer)
                'If Not value = _ix Then
                _ix = value
                Change_HeberId()
                'End If
            End Set
        End Property
        Property Nachname As String
        Property Vorname As String
        Property Auswertung As Integer
        Property Auswertung2 As Integer
        Property Gruppierung2 As Integer
        Property Verein As String
            Get
                Return _verein
            End Get
            Set(value As String)
                'If Not _verein = value Then
                _verein = value
                RaiseEvent Verein_Changed(_verein)
                'End If
            End Set
        End Property
        Property Land As String
            Get
                Return _land
            End Get
            Set(value As String)
                'If Not _land = value Then
                _land = value
                RaiseEvent Land_Changed(_land)
                'End If
            End Set
        End Property
        Property Flagge As String
            Get
                Return _flagge
            End Get
            Set(value As String)
                'If Not _flagge = value Then
                _flagge = value
                RaiseEvent Flagge_Changed(_flagge)
                'End If
            End Set
        End Property
        Property AK As String
            Get
                Return _ak
            End Get
            Set(value As String)
                'If Not _ak = value Then
                _ak = value
                RaiseEvent AK_Changed(_ak)
                'End If
            End Set
        End Property
        Property GK As String
            Get
                Return _gk
            End Get
            Set(value As String)
                'If Not _gk = value Then
                _gk = value
                RaiseEvent GK_Changed(_gk)
                'End If
            End Set
        End Property
        Property Wiegen As Double
        Property Sex As String
        Property JG As Integer
        Property Versuch As Integer
            Get
                Return _versuch
            End Get
            Set(value As Integer)
                'If Not _versuch = value Then
                _versuch = value
                RaiseEvent Versuch_Changed(_versuch.ToString)
                'End If
            End Set
        End Property
        Property Hantellast As Integer
            Get
                Return _hantellast
            End Get
            Set(value As Integer)
                _hantellast = value
                RaiseEvent Hantellast_Changed() ' Bohle
                Hantel.Change_Hantel(_hantellast,, Sex, JG, Wettkampf.BigDisc) ' Hantelbeladung
            End Set
        End Property
        Property Hantelstange As String
        Property Wertung As Integer
        Property Startnummer As String
            Get
                Return _startnummer
            End Get
            Set(value As String)
                _startnummer = value
                RaiseEvent Startnummer_Changed(value)
            End Set
        End Property
        Property T_Note As Double
        Property CountDown As Integer
        Property Lampen As String

        Public Sub Clear()
            Id = 0
            Nachname = String.Empty
            Vorname = String.Empty
            Startnummer = String.Empty
            Auswertung = 0
            Auswertung2 = 0
            Gruppierung2 = 0
            Verein = String.Empty
            Land = String.Empty
            Flagge = String.Empty
            AK = String.Empty
            GK = String.Empty
            Wiegen = 0
            Sex = String.Empty
            JG = 0
            Versuch = 0
            Hantellast = 0
            Hantelstange = String.Empty
            Wertung = 0
            T_Note = 0
            CountDown = 0
            Lampen = String.Empty
        End Sub

        Public Sub Change_HeberId(Optional visible As Boolean = True)
            RaiseEvent Id_Changed(visible)
        End Sub

    End Class

    Public WA_Events As New myEvents
    Class myEvents
        Public Event Set_ZN(Data As String)
        Public Event Set_KR(Index As Integer, Data As String)
        'Public Event Suspend_ZN(Suspended As Boolean)
        Public Sub Push_ZN(Data As String)
            RaiseEvent Set_ZN(Data)
        End Sub
        Public Sub Push_KR(Index As Integer, Data As String)
            RaiseEvent Set_KR(Index, Data)
        End Sub

        'Public Sub Set_ZN_Suspended(Suspended As Boolean)
        '    RaiseEvent Suspend_ZN(Suspended)
        'End Sub
    End Class

    Public Sub Read_TableToLayout(grid As DataGridView, Bereich As String)
        Dim row As String()
        With grid
            For i = 0 To .Columns.Count - 1
                row = { .Columns(i).Name, .Columns(i).HeaderText, CStr(.Columns(i).Visible), .Columns(i).Width.ToString}
                Try
                    frmTableLayout.grdColums.Rows.Insert(.Columns(i).DisplayIndex, row)
                Catch ex As Exception
                    frmTableLayout.grdColums.Rows.Add(row)
                End Try
            Next
            frmTableLayout.cboForeColor.BackColor = .DefaultCellStyle.ForeColor
            frmTableLayout.cboBackColor.BackColor = .DefaultCellStyle.BackColor
            frmTableLayout.cboSelBackColor.BackColor = .DefaultCellStyle.SelectionBackColor
            frmTableLayout.cboSelForeColor.BackColor = .DefaultCellStyle.SelectionForeColor
            frmTableLayout.cboFont.Text = .DefaultCellStyle.Font.Name & ";" & .DefaultCellStyle.Font.Size & CStr(IIf(.DefaultCellStyle.Font.Bold, ";Fett", "")) & CStr(IIf(.DefaultCellStyle.Font.Italic, ";Kursiv", ""))
        End With
    End Sub

    Public Sub Write_ColumnLayout_ToINI(grid As DataGridView, Bereich As String)
        Dim r, c As Integer, x As String
        With grid
            For r = 0 To .Rows.Count - 1
                x = ""
                For c = 0 To .Columns.Count - 1
                    x = x & CStr(grid(c, r).Value)
                    If c < .Columns.Count - 1 Then x = x & ";"
                Next
                NativeMethods.INI_Write(Bereich, CStr(r + 1), x, App_IniFile)
            Next
        End With
    End Sub

    Public Sub Format_Grid(bereich As String, grid As DataGridView)
        'prüft, ob Bereich in INI vorhanden
        Dim s As String = NativeMethods.INI_Read(bereich, "ForeColor", App_IniFile)
        'falls nicht, EXIT
        If s = "?" Then Exit Sub
        With grid
            Try
                With .DefaultCellStyle
                    .ForeColor = Color.FromArgb(CInt(NativeMethods.INI_Read(bereich, "ForeColor", App_IniFile)))
                    .BackColor = Color.FromArgb(CInt(NativeMethods.INI_Read(bereich, "BackColor", App_IniFile)))
                    .SelectionForeColor = Color.FromArgb(CInt(NativeMethods.INI_Read(bereich, "SelForeColor", App_IniFile)))
                    .SelectionBackColor = Color.FromArgb(CInt(NativeMethods.INI_Read(bereich, "SelBackColor", App_IniFile)))
                    Dim entries() As String = NativeMethods.INI_Read(bereich, "Font", App_IniFile).Split(CChar(";"))
                    Dim style As New FontStyle
                    If entries.GetUpperBound(0) > 2 Then
                        style = FontStyle.Bold Or FontStyle.Italic
                    ElseIf entries.GetUpperBound(0) > 1 Then
                        If entries(2).Contains("Fett") Then
                            style = FontStyle.Bold
                        Else
                            style = FontStyle.Italic
                        End If
                    End If
                    .Font = New Font(entries(0), CSng(entries(1)), style)
                End With
                Dim r As Integer = 1
                Dim x() As String
                Do
                    x = NativeMethods.INI_Read(bereich, CStr(r + 1), App_IniFile).Split(CChar(";"))
                    If x(0) <> "?" Then
                        With .Columns(x(0))
                            .DisplayIndex = r
                            .HeaderText = x(1)
                            .Visible = CBool(x(2))
                            .Width = CInt(x(3))
                        End With
                        r = r + 1
                    End If
                Loop While x(0) <> "?"
            Catch ex As Exception
                'kein Eintrag in INI
                Exit Sub
            End Try
        End With
    End Sub

    Public Function MySQl_Error(Form As Form, Message As String, StackTrace As String, Optional Cancel As Boolean = False, Optional Folder As String = "") As DialogResult

        '' Client wartet auf IP vom Server
        'If Not IsNothing(Form) Then
        '    Using New Centered_MessageBox(Form)
        '        MessageBox.Show(Message, msgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    End Using
        '    Return DialogResult.Cancel
        'End If

        Dim FullStack = Split(StackTrace, "bei ")
        Dim Stack As New List(Of String)

        For i = FullStack.Count - 1 To 0 Step -1
            If FullStack(i).Contains("Zeile") Then Stack.Add(FullStack(i))
        Next

        If Stack.Count = 0 Then Stack = FullStack.ToList

        LogMessage(Message & " / StackTrace " & Join(Stack.ToArray, vbNewLine),
                   Path.Combine(Application.StartupPath,
                                If(String.IsNullOrEmpty(Folder), "log", Folder),
                                "sql_" & Format(Now, "yyyy-MM-dd") & ".log"))

        ConnError = True

        If IsNothing(Form) Then             ' keine MessageBox
            If Cancel Then
                ConnError = False
                Return DialogResult.Cancel  ' falls Schleife --> abbrechen
            Else
                Return DialogResult.None    ' falls Schleife --> nicht abbrechen
            End If
        End If

        Dim Result = DialogResult.None

        Dim msg As String
        Do
            If LCase(Message).Contains("unable to connect") Then
                msg = "Verbindung zur Datenbank unterbrochen."
            Else
                msg = "Datenbank-Fehler" & vbNewLine & Message
            End If

            Dim lbl() = {"Wiederholen"}
            Dim btn = MessageBoxButtons.OKCancel

            If Cancel Then
                lbl = Nothing
                btn = MessageBoxButtons.OK ' meaning Cancel pressed
            End If

            Using New Centered_MessageBox(Form, lbl)
                Result = MessageBox.Show(msg, msgCaption, btn, MessageBoxIcon.Error)
            End Using

            If Result = DialogResult.Cancel OrElse Cancel Then
                Return DialogResult.Cancel
            ElseIf Not Result = DialogResult.OK Then ' = Wiederholen
                Return DialogResult.None
            End If

            If LCase(Message).Contains("unable to connect") Then
                Try ' testen, ob eine Verbindung möglich ist
                    Using conn As New MySqlConnection(User.ConnString)
                        conn.Open()
                    End Using
                    ConnError = False
                Catch ex As Exception
                End Try
            Else
                Exit Do
            End If
        Loop While ConnError

        Return DialogResult.None
    End Function

    ' Global ProgressBar-Text
    Enum ProgressBarTextLocation
        Left
        Centered
    End Enum
    Public Sub SetProgressBarText(Target As ProgressBar, Text As String, Location As ProgressBarTextLocation, TextColor As Color, TextFont As Font)
        If IsNothing(Target) Then Return

        If String.IsNullOrEmpty(Text) Then
            Dim percent As Integer = CInt((((Target.Value - Target.Minimum) / (Target.Maximum - Target.Minimum)) * 100))
            Text = percent.ToString() & "%"
        End If

        Using gr As Graphics = Target.CreateGraphics()
            gr.DrawString(Text, TextFont, New SolidBrush(TextColor), New PointF(CSng(If(Location = ProgressBarTextLocation.Left, 5, Target.Width / 2 - (gr.MeasureString(Text, TextFont).Width / 2.0F))), CSng(Target.Height / 2 - (gr.MeasureString(Text, TextFont).Height / 2.0F))))
        End Using
    End Sub

    Public TCP_Protocol As New myProtocol
    Class myProtocol
        Private Class myTimeVars
            Private Zeit As String = "time"
            Private Stunden As String = "std"
            Private Minuten As String = "min"
            Private Sekunden As String = "sek"
            Private Stopp As String = "stop"
            Private Farbe As String
        End Class
        Private Class myWertung

        End Class

        Public Property Sender As String
        Public Property Target As String

        Public Sub New()
            Dim TimeVars As New myTimeVars
        End Sub

    End Class

End Module
